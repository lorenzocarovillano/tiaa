/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:09:49 AM
**        * FROM NATURAL SUBPROGRAM : Scin8100
************************************************************
**        * FILE NAME            : Scin8100.java
**        * CLASS NAME           : Scin8100
**        * INSTANCE NAME        : Scin8100
************************************************************
**----------------------------------------------------------------------
*
* PROGRAM-ID    :  SCIN8100
* DESCRIPTION   :  THIS PROGRAM WILL FORMAT THE FUNDS BY ASSET CLASS
*                  BY READING THE NEW EXTERNALIZATION FILES
*                  THIS PROGRAM WILL BE CALLED BY THE NATURAL PROGRAM
*                  APPB100
*                  THIS WILL RETURN ALL THE AVAILABLE FUNDS FOR A
*                  GIVEN SUNGARD PLAN AND SUBPLAN
* DATE WRITTEN  :  06/01/04
* AUTHOR        :  B. ELLO
*
* MOD DATE    MOD BY     DESCRIPTION OF CHANGES
**--------   ----------  -----------------------------------------------
**01-08-05   B ELLO      USE NEW LINKAGE OF PSG9070.
**09-08-04   R WILLIS    REL 3 CHANGES- CALL PSG9070 INSTEAD OF PSG9030.
* 12/21/05  J.CAMPBELL   CHANGES FOR DFA BLOCK - CALLNAT ACIN3009 TO
*                        CHECK IF FUND IS DISCONTINUED.  (JC1)
**04-11-06   K GATES     CHANGES FOR STABLE RETURN - MOVE LEGAL ESCAPE
**                       AFTER EVALUATING FUNDS.  (SGRDSR KG)
* 06/23/06  K.GATES      PRODUCTION FIX RESTOW FOR SCIA8100
* 06/23/06  K.GATES      RESTOW FOR SCIA8100 FOR ANNUITY WRAP
* 08/21/06  D.MARPURI    RESTOW FOR SCIA8100 FOR ROTH PROJECT
* 03/15/07  O SOTTO      RESTRUCTURE TO ELIMINATE THE 500 OCCURS TABLE.
*                        SC 031507.
* 03/30/07  O SOTTO    INCLUDE *INIT-USER TO THE KEY IN ORDER TO MAKE IT
*                      UNIQUE. SC 033007.
* 04/02/07  O SOTTO INCLUDE ON ERROR ROUTINE TO RETRY IF RC=3145.
*                  ALSO USE GET COMMAND TO DELETE THE RECORD. SC 52787.
* 06/11/08  K.GATES      ADD CALSTRS CLIENT ID FOR DFEMX FUND RESTRICT
*                        - SEE CALSTRS KG
* 07/01/11  L.SHU        RESTOW FOR SCIA8100 FOR TRANSFER IN CREDIT
* 07/18/13  L.SHU        REMOVE COMMENTED LINES                   TNGSUB
*                        RESTOW FOR SCIA8100
* 07/18/13  K.GATES      BYPASS FUND LIST FOR IRA SUB CONTRACTS - TNGSUB
* 06/20/14  BUDDY NEWSOM USING NEW PARM FILE FROM APPB100/APPB110 (CREA)
*                        CONTAINING THE FUND/TICKER INFORMATION
*                        NEEDED TO PROCESS THE RECORDS. THIS ELIMINATES
*                        THE HARDCODING OF THE TICKER SYMBOLS
*                        NOTE: THE TRANSLTE-FUND-SEQ 1 IS FOR "TIAA"
*                              AND TRANSLTE-FUND-SEQ 11 IS FOR "TSAF#"
* 08/1/2014 B. NEWSOM
*              INVESTMENT MENU BY SUBPLAN                 (IMBS)
*              REMOVE THE SPECIAL FUND INDICTOR
*              THE PROCESS FOR THE SPECIAL FUND INDICTOR WAS MOVED TO
*              PROGRAM PSG9000.
* 03/25/21  RJ FRANKLIN                                 (EXIT SAG)
*              CHANGE PARAMETER VARIABLE TABLE TO STATIC TABLE
*              CONVERSION TO JAVA REQUIRES STATIC TABLE.
**----------------------------------------------------------------------
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Scin8100 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaScia8100 pdaScia8100;
    private PdaAcia3009 pdaAcia3009;
    private PdaPsta9200 pdaPsta9200;
    private LdaPstl9902 ldaPstl9902;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField temp_Input_Parm;
    private DbsField ap_Tiaa_Cntrct;
    private DbsField ap_Cref_Cert;
    private DbsField ap_Lob;
    private DbsField ap_Lob_Type;
    private DbsField ap_Region_Code;
    private DbsField ap_Coll_Code;
    private DbsField ap_Client_Id;
    private DbsField ap_Substitution_Contract_Ind;

    private DbsGroup pnd_Translte_File;
    private DbsField pnd_Translte_File_Pnd_Translte_Ticker;
    private DbsField pnd_Translte_File_Pnd_F1;
    private DbsField pnd_Translte_File_Pnd_Translte_Ticker_Name;
    private DbsField pnd_Translte_File_Pnd_F2;
    private DbsField pnd_Translte_File_Pnd_Translte_Fund_Seq;
    private DbsField pnd_Translte_File_Pnd_F3;
    private DbsField pnd_Translte_File_Pnd_Translte_Fund_First_4;
    private DbsField pnd_Translte_File_Pnd_F4;
    private DbsField pnd_Translte_File_Pnd_Translte_Fund_Code;
    private DbsField pnd_Translte_File_Pnd_F5;
    private DbsField pnd_Translte_File_Pnd_Translte_Error_Message;

    private DbsGroup workfile_Rec;
    private DbsField workfile_Rec_P_Plan_No;
    private DbsField workfile_Rec_P_Subplan_No;
    private DbsField workfile_Rec_P_Tiaa_Cntrct;
    private DbsField workfile_Rec_P_Cref_Cert;
    private DbsField workfile_Rec_P_Lob;
    private DbsField workfile_Rec_P_Lob_Type;
    private DbsField workfile_Rec_P_Region_Code;
    private DbsField workfile_Rec_P_Coll_Code;
    private DbsField workfile_Rec_P_Category;
    private DbsField workfile_Rec_P_Fund_Num_Cde;
    private DbsField workfile_Rec_P_Fund_Name;
    private DbsField workfile_Rec_P_Category_Cnt;
    private DbsField p_Return_Code;
    private DbsField p_Return_Msg;

    private DataAccessProgramView vw_new_Ext_Cntrl_Fnd_View;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Table_Cde;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Ticker_Symbol;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Fund_Family_Cde;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Fund_Style;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Fund_Size;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Fund_Nme;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Fund_Inception_Dte;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Fund_Cusip;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Category_Class_Cde;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Category_Rptng_Seq;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Category_Fund_Seq;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Alpha_Fund_Cde;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Num_Fund_Cde;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Reporting_Seq;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Unit_Rate_Ind;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Alpha_Fund_Seq;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_V1_Num_Fund_Cde;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_V1_Alpha_Fund_Cde;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Abbr_Factor_Key;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Act_Unit_Acct_Nm;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_V2_Alpha_Cde;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_V2_Rt_1;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_V2_Rt_2;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_V2_Rt_3;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Ivc_Company_Cde;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_V2_Active_Ind;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Dividend_Ind;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Rate_Basis_Points;
    private DbsGroup new_Ext_Cntrl_Fnd_View_Nec_Fund_SummaryMuGroup;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Fund_Summary;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Modified_By;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Modified_Dte;

    private DataAccessProgramView vw_new_Ext_Cntrl_Cat_View;
    private DbsField new_Ext_Cntrl_Cat_View_Nec_Table_Cde;
    private DbsField new_Ext_Cntrl_Cat_View_Nec_Category_Class_Cde;
    private DbsField new_Ext_Cntrl_Cat_View_Nec_Category_Class_Desc;
    private DbsField new_Ext_Cntrl_Cat_View_Nec_Category_Rptng_Seq;
    private DbsField new_Ext_Cntrl_Cat_View_Nec_Modified_By;
    private DbsField new_Ext_Cntrl_Cat_View_Nec_Modified_Dte;
    private DbsField new_Ext_Cntrl_Cat_View_Nec_Cat_Super;

    private DataAccessProgramView vw_app_Table_Entry;
    private DbsField app_Table_Entry_Entry_Actve_Ind;
    private DbsField app_Table_Entry_Entry_Table_Id_Nbr;
    private DbsField app_Table_Entry_Entry_Table_Sub_Id;
    private DbsField app_Table_Entry_Entry_Cde;
    private DbsField app_Table_Entry_Entry_Dscrptn_Txt;
    private DbsGroup app_Table_Entry_Addtnl_Dscrptn_TxtMuGroup;
    private DbsField app_Table_Entry_Addtnl_Dscrptn_Txt;

    private DataAccessProgramView vw_app_Table_Upd;
    private DbsField app_Table_Upd_Entry_Actve_Ind;
    private DbsField pnd_Table_Id_Entry_Cde;

    private DbsGroup pnd_Table_Id_Entry_Cde__R_Field_1;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde;
    private DbsField pnd_Nec_Fnd_Super1;

    private DbsGroup pnd_Nec_Fnd_Super1__R_Field_2;
    private DbsField pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde1;
    private DbsField pnd_Nec_Fnd_Super1_Pnd_Nec_Tkr_Symbl;
    private DbsField pnd_Nec_Fnd_Super5;

    private DbsGroup pnd_Nec_Fnd_Super5__R_Field_3;
    private DbsField pnd_Nec_Fnd_Super5_Pnd_Nec_Table_Cde;
    private DbsField pnd_Nec_Fnd_Super5_Pnd_Nec_Category_Rptng_Seq;
    private DbsField pnd_Nec_Fnd_Super5_Pnd_Nec_Category_Fund_Seq;
    private DbsField pnd_Nec_Cat_Super;

    private DbsGroup pnd_Nec_Cat_Super__R_Field_4;
    private DbsField pnd_Nec_Cat_Super_Pnd_Nec_Table_Cde_2;
    private DbsField pnd_Nec_Cat_Super_Pnd_Nec_Category_Class_Cde;
    private DbsField pnd_First_4;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Cnt;
    private DbsField pnd_Idx;
    private DbsField pnd_T_Fund_Code;
    private DbsField pnd_T_Fund_Num_Cde;
    private DbsField pnd_T_Fund_Category;
    private DbsField pnd_T_Fund_Name;
    private DbsField pnd_Prev_Fund_Category;
    private DbsField pnd_Fund_Cnt;
    private DbsField pnd_Fund_Idx;
    private DbsField pnd_Index;
    private DbsField pnd_Index2;
    private DbsField pnd_Ctr;
    private DbsField p_Category_Unique;
    private DbsField p_Fund_Code;
    private DbsField pnd_Return_Err_Msg;

    private DbsGroup pnd_Return_Err_Msg__R_Field_5;
    private DbsField pnd_Return_Err_Msg_Pnd_Return_Err_Plan;
    private DbsField pnd_Return_Err_Msg_Pnd_Return_Spc_1;
    private DbsField pnd_Return_Err_Msg_Pnd_Return_Err_Subplan;
    private DbsField pnd_Return_Err_Msg_Pnd_Return_Spc_2;
    private DbsField pnd_Return_Err_Msg_Pnd_Return_Err_Module;
    private DbsField pnd_Return_Err_Msg_Pnd_Return_Spc_3;
    private DbsField pnd_Return_Err_Msg_Pnd_Return_Err_File_Op;
    private DbsField pnd_Return_Err_Msg_Pnd_Return_Spc_4;
    private DbsField pnd_Return_Err_Msg_Pnd_Return_Err_Message;
    private DbsField pnd_Debug;
    private DbsField pnd_Max_Occurs;
    private DbsField pnd_W_Ticker;
    private DbsField pnd_Added;
    private DbsField pnd_Deleted;
    private int psg9070ReturnCode;
    private int cmrollReturnCode;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAcia3009 = new PdaAcia3009(localVariables);
        pdaPsta9200 = new PdaPsta9200(localVariables);
        ldaPstl9902 = new LdaPstl9902();
        registerRecord(ldaPstl9902);

        // parameters
        parameters = new DbsRecord();
        temp_Input_Parm = parameters.newFieldInRecord("temp_Input_Parm", "TEMP-INPUT-PARM", FieldType.STRING, 8);
        temp_Input_Parm.setParameterOption(ParameterOption.ByReference);
        ap_Tiaa_Cntrct = parameters.newFieldInRecord("ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", FieldType.STRING, 10);
        ap_Tiaa_Cntrct.setParameterOption(ParameterOption.ByReference);
        ap_Cref_Cert = parameters.newFieldInRecord("ap_Cref_Cert", "AP-CREF-CERT", FieldType.STRING, 10);
        ap_Cref_Cert.setParameterOption(ParameterOption.ByReference);
        ap_Lob = parameters.newFieldInRecord("ap_Lob", "AP-LOB", FieldType.STRING, 1);
        ap_Lob.setParameterOption(ParameterOption.ByReference);
        ap_Lob_Type = parameters.newFieldInRecord("ap_Lob_Type", "AP-LOB-TYPE", FieldType.STRING, 1);
        ap_Lob_Type.setParameterOption(ParameterOption.ByReference);
        ap_Region_Code = parameters.newFieldInRecord("ap_Region_Code", "AP-REGION-CODE", FieldType.STRING, 3);
        ap_Region_Code.setParameterOption(ParameterOption.ByReference);
        ap_Coll_Code = parameters.newFieldInRecord("ap_Coll_Code", "AP-COLL-CODE", FieldType.STRING, 6);
        ap_Coll_Code.setParameterOption(ParameterOption.ByReference);
        ap_Client_Id = parameters.newFieldInRecord("ap_Client_Id", "AP-CLIENT-ID", FieldType.STRING, 6);
        ap_Client_Id.setParameterOption(ParameterOption.ByReference);
        ap_Substitution_Contract_Ind = parameters.newFieldInRecord("ap_Substitution_Contract_Ind", "AP-SUBSTITUTION-CONTRACT-IND", FieldType.STRING, 1);
        ap_Substitution_Contract_Ind.setParameterOption(ParameterOption.ByReference);

        pnd_Translte_File = parameters.newGroupArrayInRecord("pnd_Translte_File", "#TRANSLTE-FILE", new DbsArrayController(1, 15));
        pnd_Translte_File.setParameterOption(ParameterOption.ByReference);
        pnd_Translte_File_Pnd_Translte_Ticker = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Ticker", "#TRANSLTE-TICKER", FieldType.STRING, 
            10);
        pnd_Translte_File_Pnd_F1 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_F1", "#F1", FieldType.STRING, 1);
        pnd_Translte_File_Pnd_Translte_Ticker_Name = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Ticker_Name", "#TRANSLTE-TICKER-NAME", 
            FieldType.STRING, 26);
        pnd_Translte_File_Pnd_F2 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Translte_File_Pnd_Translte_Fund_Seq = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Fund_Seq", "#TRANSLTE-FUND-SEQ", FieldType.NUMERIC, 
            2);
        pnd_Translte_File_Pnd_F3 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_F3", "#F3", FieldType.STRING, 1);
        pnd_Translte_File_Pnd_Translte_Fund_First_4 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Fund_First_4", "#TRANSLTE-FUND-FIRST-4", 
            FieldType.STRING, 4);
        pnd_Translte_File_Pnd_F4 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_F4", "#F4", FieldType.STRING, 1);
        pnd_Translte_File_Pnd_Translte_Fund_Code = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Fund_Code", "#TRANSLTE-FUND-CODE", 
            FieldType.STRING, 3);
        pnd_Translte_File_Pnd_F5 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_F5", "#F5", FieldType.STRING, 1);
        pnd_Translte_File_Pnd_Translte_Error_Message = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Error_Message", "#TRANSLTE-ERROR-MESSAGE", 
            FieldType.STRING, 40);
        pdaScia8100 = new PdaScia8100(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        workfile_Rec = localVariables.newGroupInRecord("workfile_Rec", "WORKFILE-REC");
        workfile_Rec_P_Plan_No = workfile_Rec.newFieldInGroup("workfile_Rec_P_Plan_No", "P-PLAN-NO", FieldType.STRING, 6);
        workfile_Rec_P_Subplan_No = workfile_Rec.newFieldInGroup("workfile_Rec_P_Subplan_No", "P-SUBPLAN-NO", FieldType.STRING, 6);
        workfile_Rec_P_Tiaa_Cntrct = workfile_Rec.newFieldInGroup("workfile_Rec_P_Tiaa_Cntrct", "P-TIAA-CNTRCT", FieldType.STRING, 10);
        workfile_Rec_P_Cref_Cert = workfile_Rec.newFieldInGroup("workfile_Rec_P_Cref_Cert", "P-CREF-CERT", FieldType.STRING, 10);
        workfile_Rec_P_Lob = workfile_Rec.newFieldInGroup("workfile_Rec_P_Lob", "P-LOB", FieldType.STRING, 1);
        workfile_Rec_P_Lob_Type = workfile_Rec.newFieldInGroup("workfile_Rec_P_Lob_Type", "P-LOB-TYPE", FieldType.STRING, 1);
        workfile_Rec_P_Region_Code = workfile_Rec.newFieldInGroup("workfile_Rec_P_Region_Code", "P-REGION-CODE", FieldType.STRING, 3);
        workfile_Rec_P_Coll_Code = workfile_Rec.newFieldInGroup("workfile_Rec_P_Coll_Code", "P-COLL-CODE", FieldType.STRING, 4);
        workfile_Rec_P_Category = workfile_Rec.newFieldArrayInGroup("workfile_Rec_P_Category", "P-CATEGORY", FieldType.STRING, 35, new DbsArrayController(1, 
            106));
        workfile_Rec_P_Fund_Num_Cde = workfile_Rec.newFieldArrayInGroup("workfile_Rec_P_Fund_Num_Cde", "P-FUND-NUM-CDE", FieldType.STRING, 5, new DbsArrayController(1, 
            106));
        workfile_Rec_P_Fund_Name = workfile_Rec.newFieldArrayInGroup("workfile_Rec_P_Fund_Name", "P-FUND-NAME", FieldType.STRING, 50, new DbsArrayController(1, 
            106));
        workfile_Rec_P_Category_Cnt = workfile_Rec.newFieldInGroup("workfile_Rec_P_Category_Cnt", "P-CATEGORY-CNT", FieldType.NUMERIC, 6);
        p_Return_Code = localVariables.newFieldInRecord("p_Return_Code", "P-RETURN-CODE", FieldType.NUMERIC, 2);
        p_Return_Msg = localVariables.newFieldInRecord("p_Return_Msg", "P-RETURN-MSG", FieldType.STRING, 79);

        vw_new_Ext_Cntrl_Fnd_View = new DataAccessProgramView(new NameInfo("vw_new_Ext_Cntrl_Fnd_View", "NEW-EXT-CNTRL-FND-VIEW"), "NEW_EXT_CNTRL_FND", 
            "NEW_EXT_CNTRL");
        new_Ext_Cntrl_Fnd_View_Nec_Table_Cde = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Table_Cde", "NEC-TABLE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NEC_TABLE_CDE");
        new_Ext_Cntrl_Fnd_View_Nec_Ticker_Symbol = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Ticker_Symbol", "NEC-TICKER-SYMBOL", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "NEC_TICKER_SYMBOL");
        new_Ext_Cntrl_Fnd_View_Nec_Fund_Family_Cde = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Fund_Family_Cde", 
            "NEC-FUND-FAMILY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "NEC_FUND_FAMILY_CDE");
        new_Ext_Cntrl_Fnd_View_Nec_Fund_Style = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Fund_Style", "NEC-FUND-STYLE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NEC_FUND_STYLE");
        new_Ext_Cntrl_Fnd_View_Nec_Fund_Size = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Fund_Size", "NEC-FUND-SIZE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NEC_FUND_SIZE");
        new_Ext_Cntrl_Fnd_View_Nec_Fund_Nme = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Fund_Nme", "NEC-FUND-NME", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NEC_FUND_NME");
        new_Ext_Cntrl_Fnd_View_Nec_Fund_Inception_Dte = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Fund_Inception_Dte", 
            "NEC-FUND-INCEPTION-DTE", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "NEC_FUND_INCEPTION_DTE");
        new_Ext_Cntrl_Fnd_View_Nec_Fund_Cusip = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Fund_Cusip", "NEC-FUND-CUSIP", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "NEC_FUND_CUSIP");
        new_Ext_Cntrl_Fnd_View_Nec_Category_Class_Cde = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Category_Class_Cde", 
            "NEC-CATEGORY-CLASS-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "NEC_CATEGORY_CLASS_CDE");
        new_Ext_Cntrl_Fnd_View_Nec_Category_Rptng_Seq = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Category_Rptng_Seq", 
            "NEC-CATEGORY-RPTNG-SEQ", FieldType.STRING, 5, RepeatingFieldStrategy.None, "NEC_CATEGORY_RPTNG_SEQ");
        new_Ext_Cntrl_Fnd_View_Nec_Category_Fund_Seq = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Category_Fund_Seq", 
            "NEC-CATEGORY-FUND-SEQ", FieldType.STRING, 5, RepeatingFieldStrategy.None, "NEC_CATEGORY_FUND_SEQ");
        new_Ext_Cntrl_Fnd_View_Nec_Alpha_Fund_Cde = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Alpha_Fund_Cde", 
            "NEC-ALPHA-FUND-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "NEC_ALPHA_FUND_CDE");
        new_Ext_Cntrl_Fnd_View_Nec_Num_Fund_Cde = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Num_Fund_Cde", "NEC-NUM-FUND-CDE", 
            FieldType.NUMERIC, 5, RepeatingFieldStrategy.None, "NEC_NUM_FUND_CDE");
        new_Ext_Cntrl_Fnd_View_Nec_Reporting_Seq = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Reporting_Seq", "NEC-REPORTING-SEQ", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "NEC_REPORTING_SEQ");
        new_Ext_Cntrl_Fnd_View_Nec_Unit_Rate_Ind = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Unit_Rate_Ind", "NEC-UNIT-RATE-IND", 
            FieldType.PACKED_DECIMAL, 3, RepeatingFieldStrategy.None, "NEC_UNIT_RATE_IND");
        new_Ext_Cntrl_Fnd_View_Nec_Alpha_Fund_Seq = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Alpha_Fund_Seq", 
            "NEC-ALPHA-FUND-SEQ", FieldType.STRING, 2, RepeatingFieldStrategy.None, "NEC_ALPHA_FUND_SEQ");
        new_Ext_Cntrl_Fnd_View_Nec_V1_Num_Fund_Cde = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_V1_Num_Fund_Cde", 
            "NEC-V1-NUM-FUND-CDE", FieldType.NUMERIC, 5, RepeatingFieldStrategy.None, "NEC_V1_NUM_FUND_CDE");
        new_Ext_Cntrl_Fnd_View_Nec_V1_Alpha_Fund_Cde = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_V1_Alpha_Fund_Cde", 
            "NEC-V1-ALPHA-FUND-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "NEC_V1_ALPHA_FUND_CDE");
        new_Ext_Cntrl_Fnd_View_Nec_Abbr_Factor_Key = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Abbr_Factor_Key", 
            "NEC-ABBR-FACTOR-KEY", FieldType.STRING, 2, RepeatingFieldStrategy.None, "NEC_ABBR_FACTOR_KEY");
        new_Ext_Cntrl_Fnd_View_Nec_Act_Unit_Acct_Nm = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Act_Unit_Acct_Nm", 
            "NEC-ACT-UNIT-ACCT-NM", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "NEC_ACT_UNIT_ACCT_NM");
        new_Ext_Cntrl_Fnd_View_Nec_V2_Alpha_Cde = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_V2_Alpha_Cde", "NEC-V2-ALPHA-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "NEC_V2_ALPHA_CDE");
        new_Ext_Cntrl_Fnd_View_Nec_V2_Rt_1 = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_V2_Rt_1", "NEC-V2-RT-1", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "NEC_V2_RT_1");
        new_Ext_Cntrl_Fnd_View_Nec_V2_Rt_2 = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_V2_Rt_2", "NEC-V2-RT-2", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "NEC_V2_RT_2");
        new_Ext_Cntrl_Fnd_View_Nec_V2_Rt_3 = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_V2_Rt_3", "NEC-V2-RT-3", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "NEC_V2_RT_3");
        new_Ext_Cntrl_Fnd_View_Nec_Ivc_Company_Cde = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Ivc_Company_Cde", 
            "NEC-IVC-COMPANY-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "NEC_IVC_COMPANY_CDE");
        new_Ext_Cntrl_Fnd_View_Nec_V2_Active_Ind = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_V2_Active_Ind", "NEC-V2-ACTIVE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NEC_V2_ACTIVE_IND");
        new_Ext_Cntrl_Fnd_View_Nec_Dividend_Ind = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Dividend_Ind", "NEC-DIVIDEND-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NEC_DIVIDEND_IND");
        new_Ext_Cntrl_Fnd_View_Nec_Rate_Basis_Points = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Rate_Basis_Points", 
            "NEC-RATE-BASIS-POINTS", FieldType.NUMERIC, 6, 3, RepeatingFieldStrategy.None, "NEC_RATE_BASIS_POINTS");
        new_Ext_Cntrl_Fnd_View_Nec_Fund_SummaryMuGroup = vw_new_Ext_Cntrl_Fnd_View.getRecord().newGroupInGroup("NEW_EXT_CNTRL_FND_VIEW_NEC_FUND_SUMMARYMuGroup", 
            "NEC_FUND_SUMMARYMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NEW_EXT_CNTRL_NEC_FUND_SUMMARY");
        new_Ext_Cntrl_Fnd_View_Nec_Fund_Summary = new_Ext_Cntrl_Fnd_View_Nec_Fund_SummaryMuGroup.newFieldArrayInGroup("new_Ext_Cntrl_Fnd_View_Nec_Fund_Summary", 
            "NEC-FUND-SUMMARY", FieldType.STRING, 60, new DbsArrayController(1, 17), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NEC_FUND_SUMMARY");
        new_Ext_Cntrl_Fnd_View_Nec_Modified_By = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Modified_By", "NEC-MODIFIED-BY", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "NEC_MODIFIED_BY");
        new_Ext_Cntrl_Fnd_View_Nec_Modified_Dte = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Modified_Dte", "NEC-MODIFIED-DTE", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "NEC_MODIFIED_DTE");
        registerRecord(vw_new_Ext_Cntrl_Fnd_View);

        vw_new_Ext_Cntrl_Cat_View = new DataAccessProgramView(new NameInfo("vw_new_Ext_Cntrl_Cat_View", "NEW-EXT-CNTRL-CAT-VIEW"), "NEW_EXT_CNTRL_CAT", 
            "NEW_EXT_CNTRL");
        new_Ext_Cntrl_Cat_View_Nec_Table_Cde = vw_new_Ext_Cntrl_Cat_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Cat_View_Nec_Table_Cde", "NEC-TABLE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NEC_TABLE_CDE");
        new_Ext_Cntrl_Cat_View_Nec_Category_Class_Cde = vw_new_Ext_Cntrl_Cat_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Cat_View_Nec_Category_Class_Cde", 
            "NEC-CATEGORY-CLASS-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "NEC_CATEGORY_CLASS_CDE");
        new_Ext_Cntrl_Cat_View_Nec_Category_Class_Desc = vw_new_Ext_Cntrl_Cat_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Cat_View_Nec_Category_Class_Desc", 
            "NEC-CATEGORY-CLASS-DESC", FieldType.STRING, 60, RepeatingFieldStrategy.None, "NEC_CATEGORY_CLASS_DESC");
        new_Ext_Cntrl_Cat_View_Nec_Category_Rptng_Seq = vw_new_Ext_Cntrl_Cat_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Cat_View_Nec_Category_Rptng_Seq", 
            "NEC-CATEGORY-RPTNG-SEQ", FieldType.STRING, 5, RepeatingFieldStrategy.None, "NEC_CATEGORY_RPTNG_SEQ");
        new_Ext_Cntrl_Cat_View_Nec_Modified_By = vw_new_Ext_Cntrl_Cat_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Cat_View_Nec_Modified_By", "NEC-MODIFIED-BY", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "NEC_MODIFIED_BY");
        new_Ext_Cntrl_Cat_View_Nec_Modified_Dte = vw_new_Ext_Cntrl_Cat_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Cat_View_Nec_Modified_Dte", "NEC-MODIFIED-DTE", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "NEC_MODIFIED_DTE");
        new_Ext_Cntrl_Cat_View_Nec_Cat_Super = vw_new_Ext_Cntrl_Cat_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Cat_View_Nec_Cat_Super", "NEC-CAT-SUPER", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NEC_CAT_SUPER");
        new_Ext_Cntrl_Cat_View_Nec_Cat_Super.setSuperDescriptor(true);
        registerRecord(vw_new_Ext_Cntrl_Cat_View);

        vw_app_Table_Entry = new DataAccessProgramView(new NameInfo("vw_app_Table_Entry", "APP-TABLE-ENTRY"), "APP_TABLE_ENTRY", "ACIS_APP_TBL_ENT");
        app_Table_Entry_Entry_Actve_Ind = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Actve_Ind", "ENTRY-ACTVE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ENTRY_ACTVE_IND");
        app_Table_Entry_Entry_Table_Id_Nbr = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "ENTRY_TABLE_ID_NBR");
        app_Table_Entry_Entry_Table_Sub_Id = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ENTRY_TABLE_SUB_ID");
        app_Table_Entry_Entry_Cde = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "ENTRY_CDE");
        app_Table_Entry_Entry_Dscrptn_Txt = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "ENTRY_DSCRPTN_TXT");
        app_Table_Entry_Addtnl_Dscrptn_TxtMuGroup = vw_app_Table_Entry.getRecord().newGroupInGroup("APP_TABLE_ENTRY_ADDTNL_DSCRPTN_TXTMuGroup", "ADDTNL_DSCRPTN_TXTMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ACIS_APP_TBL_ENT_ADDTNL_DSCRPTN_TXT");
        app_Table_Entry_Addtnl_Dscrptn_Txt = app_Table_Entry_Addtnl_Dscrptn_TxtMuGroup.newFieldArrayInGroup("app_Table_Entry_Addtnl_Dscrptn_Txt", "ADDTNL-DSCRPTN-TXT", 
            FieldType.STRING, 60, new DbsArrayController(1, 3), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADDTNL_DSCRPTN_TXT");
        registerRecord(vw_app_Table_Entry);

        vw_app_Table_Upd = new DataAccessProgramView(new NameInfo("vw_app_Table_Upd", "APP-TABLE-UPD"), "APP_TABLE_ENTRY", "ACIS_APP_TBL_ENT");
        app_Table_Upd_Entry_Actve_Ind = vw_app_Table_Upd.getRecord().newFieldInGroup("app_Table_Upd_Entry_Actve_Ind", "ENTRY-ACTVE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ENTRY_ACTVE_IND");
        registerRecord(vw_app_Table_Upd);

        pnd_Table_Id_Entry_Cde = localVariables.newFieldInRecord("pnd_Table_Id_Entry_Cde", "#TABLE-ID-ENTRY-CDE", FieldType.STRING, 28);

        pnd_Table_Id_Entry_Cde__R_Field_1 = localVariables.newGroupInRecord("pnd_Table_Id_Entry_Cde__R_Field_1", "REDEFINE", pnd_Table_Id_Entry_Cde);
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr = pnd_Table_Id_Entry_Cde__R_Field_1.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr", 
            "#ENTRY-TABLE-ID-NBR", FieldType.NUMERIC, 6);
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id = pnd_Table_Id_Entry_Cde__R_Field_1.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id", 
            "#ENTRY-TABLE-SUB-ID", FieldType.STRING, 2);
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde = pnd_Table_Id_Entry_Cde__R_Field_1.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde", "#ENTRY-CDE", 
            FieldType.STRING, 20);
        pnd_Nec_Fnd_Super1 = localVariables.newFieldInRecord("pnd_Nec_Fnd_Super1", "#NEC-FND-SUPER1", FieldType.STRING, 13);

        pnd_Nec_Fnd_Super1__R_Field_2 = localVariables.newGroupInRecord("pnd_Nec_Fnd_Super1__R_Field_2", "REDEFINE", pnd_Nec_Fnd_Super1);
        pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde1 = pnd_Nec_Fnd_Super1__R_Field_2.newFieldInGroup("pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde1", "#NEC-TABLE-CDE1", 
            FieldType.STRING, 3);
        pnd_Nec_Fnd_Super1_Pnd_Nec_Tkr_Symbl = pnd_Nec_Fnd_Super1__R_Field_2.newFieldInGroup("pnd_Nec_Fnd_Super1_Pnd_Nec_Tkr_Symbl", "#NEC-TKR-SYMBL", 
            FieldType.STRING, 10);
        pnd_Nec_Fnd_Super5 = localVariables.newFieldInRecord("pnd_Nec_Fnd_Super5", "#NEC-FND-SUPER5", FieldType.STRING, 13);

        pnd_Nec_Fnd_Super5__R_Field_3 = localVariables.newGroupInRecord("pnd_Nec_Fnd_Super5__R_Field_3", "REDEFINE", pnd_Nec_Fnd_Super5);
        pnd_Nec_Fnd_Super5_Pnd_Nec_Table_Cde = pnd_Nec_Fnd_Super5__R_Field_3.newFieldInGroup("pnd_Nec_Fnd_Super5_Pnd_Nec_Table_Cde", "#NEC-TABLE-CDE", 
            FieldType.STRING, 3);
        pnd_Nec_Fnd_Super5_Pnd_Nec_Category_Rptng_Seq = pnd_Nec_Fnd_Super5__R_Field_3.newFieldInGroup("pnd_Nec_Fnd_Super5_Pnd_Nec_Category_Rptng_Seq", 
            "#NEC-CATEGORY-RPTNG-SEQ", FieldType.STRING, 5);
        pnd_Nec_Fnd_Super5_Pnd_Nec_Category_Fund_Seq = pnd_Nec_Fnd_Super5__R_Field_3.newFieldInGroup("pnd_Nec_Fnd_Super5_Pnd_Nec_Category_Fund_Seq", "#NEC-CATEGORY-FUND-SEQ", 
            FieldType.STRING, 5);
        pnd_Nec_Cat_Super = localVariables.newFieldInRecord("pnd_Nec_Cat_Super", "#NEC-CAT-SUPER", FieldType.STRING, 6);

        pnd_Nec_Cat_Super__R_Field_4 = localVariables.newGroupInRecord("pnd_Nec_Cat_Super__R_Field_4", "REDEFINE", pnd_Nec_Cat_Super);
        pnd_Nec_Cat_Super_Pnd_Nec_Table_Cde_2 = pnd_Nec_Cat_Super__R_Field_4.newFieldInGroup("pnd_Nec_Cat_Super_Pnd_Nec_Table_Cde_2", "#NEC-TABLE-CDE-2", 
            FieldType.STRING, 3);
        pnd_Nec_Cat_Super_Pnd_Nec_Category_Class_Cde = pnd_Nec_Cat_Super__R_Field_4.newFieldInGroup("pnd_Nec_Cat_Super_Pnd_Nec_Category_Class_Cde", "#NEC-CATEGORY-CLASS-CDE", 
            FieldType.STRING, 3);
        pnd_First_4 = localVariables.newFieldInRecord("pnd_First_4", "#FIRST-4", FieldType.STRING, 4);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 3);
        pnd_Idx = localVariables.newFieldInRecord("pnd_Idx", "#IDX", FieldType.NUMERIC, 3);
        pnd_T_Fund_Code = localVariables.newFieldArrayInRecord("pnd_T_Fund_Code", "#T-FUND-CODE", FieldType.STRING, 10, new DbsArrayController(1, 106));
        pnd_T_Fund_Num_Cde = localVariables.newFieldArrayInRecord("pnd_T_Fund_Num_Cde", "#T-FUND-NUM-CDE", FieldType.STRING, 5, new DbsArrayController(1, 
            106));
        pnd_T_Fund_Category = localVariables.newFieldArrayInRecord("pnd_T_Fund_Category", "#T-FUND-CATEGORY", FieldType.STRING, 60, new DbsArrayController(1, 
            106));
        pnd_T_Fund_Name = localVariables.newFieldArrayInRecord("pnd_T_Fund_Name", "#T-FUND-NAME", FieldType.STRING, 60, new DbsArrayController(1, 106));
        pnd_Prev_Fund_Category = localVariables.newFieldInRecord("pnd_Prev_Fund_Category", "#PREV-FUND-CATEGORY", FieldType.STRING, 60);
        pnd_Fund_Cnt = localVariables.newFieldInRecord("pnd_Fund_Cnt", "#FUND-CNT", FieldType.NUMERIC, 3);
        pnd_Fund_Idx = localVariables.newFieldInRecord("pnd_Fund_Idx", "#FUND-IDX", FieldType.NUMERIC, 3);
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.NUMERIC, 6);
        pnd_Index2 = localVariables.newFieldInRecord("pnd_Index2", "#INDEX2", FieldType.NUMERIC, 6);
        pnd_Ctr = localVariables.newFieldInRecord("pnd_Ctr", "#CTR", FieldType.NUMERIC, 6);
        p_Category_Unique = localVariables.newFieldArrayInRecord("p_Category_Unique", "P-CATEGORY-UNIQUE", FieldType.STRING, 60, new DbsArrayController(1, 
            106));
        p_Fund_Code = localVariables.newFieldArrayInRecord("p_Fund_Code", "P-FUND-CODE", FieldType.STRING, 10, new DbsArrayController(1, 106));
        pnd_Return_Err_Msg = localVariables.newFieldInRecord("pnd_Return_Err_Msg", "#RETURN-ERR-MSG", FieldType.STRING, 79);

        pnd_Return_Err_Msg__R_Field_5 = localVariables.newGroupInRecord("pnd_Return_Err_Msg__R_Field_5", "REDEFINE", pnd_Return_Err_Msg);
        pnd_Return_Err_Msg_Pnd_Return_Err_Plan = pnd_Return_Err_Msg__R_Field_5.newFieldInGroup("pnd_Return_Err_Msg_Pnd_Return_Err_Plan", "#RETURN-ERR-PLAN", 
            FieldType.STRING, 6);
        pnd_Return_Err_Msg_Pnd_Return_Spc_1 = pnd_Return_Err_Msg__R_Field_5.newFieldInGroup("pnd_Return_Err_Msg_Pnd_Return_Spc_1", "#RETURN-SPC-1", FieldType.STRING, 
            2);
        pnd_Return_Err_Msg_Pnd_Return_Err_Subplan = pnd_Return_Err_Msg__R_Field_5.newFieldInGroup("pnd_Return_Err_Msg_Pnd_Return_Err_Subplan", "#RETURN-ERR-SUBPLAN", 
            FieldType.STRING, 6);
        pnd_Return_Err_Msg_Pnd_Return_Spc_2 = pnd_Return_Err_Msg__R_Field_5.newFieldInGroup("pnd_Return_Err_Msg_Pnd_Return_Spc_2", "#RETURN-SPC-2", FieldType.STRING, 
            2);
        pnd_Return_Err_Msg_Pnd_Return_Err_Module = pnd_Return_Err_Msg__R_Field_5.newFieldInGroup("pnd_Return_Err_Msg_Pnd_Return_Err_Module", "#RETURN-ERR-MODULE", 
            FieldType.STRING, 8);
        pnd_Return_Err_Msg_Pnd_Return_Spc_3 = pnd_Return_Err_Msg__R_Field_5.newFieldInGroup("pnd_Return_Err_Msg_Pnd_Return_Spc_3", "#RETURN-SPC-3", FieldType.STRING, 
            2);
        pnd_Return_Err_Msg_Pnd_Return_Err_File_Op = pnd_Return_Err_Msg__R_Field_5.newFieldInGroup("pnd_Return_Err_Msg_Pnd_Return_Err_File_Op", "#RETURN-ERR-FILE-OP", 
            FieldType.STRING, 4);
        pnd_Return_Err_Msg_Pnd_Return_Spc_4 = pnd_Return_Err_Msg__R_Field_5.newFieldInGroup("pnd_Return_Err_Msg_Pnd_Return_Spc_4", "#RETURN-SPC-4", FieldType.STRING, 
            2);
        pnd_Return_Err_Msg_Pnd_Return_Err_Message = pnd_Return_Err_Msg__R_Field_5.newFieldInGroup("pnd_Return_Err_Msg_Pnd_Return_Err_Message", "#RETURN-ERR-MESSAGE", 
            FieldType.STRING, 27);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Max_Occurs = localVariables.newFieldInRecord("pnd_Max_Occurs", "#MAX-OCCURS", FieldType.PACKED_DECIMAL, 3);
        pnd_W_Ticker = localVariables.newFieldInRecord("pnd_W_Ticker", "#W-TICKER", FieldType.STRING, 10);
        pnd_Added = localVariables.newFieldInRecord("pnd_Added", "#ADDED", FieldType.PACKED_DECIMAL, 5);
        pnd_Deleted = localVariables.newFieldInRecord("pnd_Deleted", "#DELETED", FieldType.PACKED_DECIMAL, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_new_Ext_Cntrl_Fnd_View.reset();
        vw_new_Ext_Cntrl_Cat_View.reset();
        vw_app_Table_Entry.reset();
        vw_app_Table_Upd.reset();

        ldaPstl9902.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Max_Occurs.setInitialValue(100);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Scin8100() throws Exception
    {
        super("Scin8100");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("SCIN8100", onError);
        //* *----------------------------------------------------------------------
        //*                             MAINLINE
        //* *----------------------------------------------------------------------
        //*  CALL PSG9070 TO GET OMNI PLUS PLAN DATA                /* 09-08-04
        //*  ---------------------------------------                /* 09-08-04
        //*  WRITE '***CALING PSG9070 ***'
        //*  WRITE 'LINKAGE: ' '=' DOC-FILE-OPEN-SW
        //*   '=' DOC-REQUEST-TYPE
        //*   '=' DOC-SENT-DATA
        //*   '=' DOC-EOF
        //*   '=' DOC-PLAN-NUM
        //*   '=' DOC-PART-ID
        //*   '=' DOC-PART-NUM
        //*   '=' DOC-SUBPLAN-NUM
        //*   '=' DOC-ISSUE-DATE
        //*  09-08-04
        psg9070ReturnCode = DbsUtil.callExternalProgram("PSG9070",pdaScia8100.getDoc_File_Open_Sw(),pdaScia8100.getDoc_Rec_Doc_Request_Type(),pdaScia8100.getDoc_Rec_Doc_Eof(), //Natural: CALL 'PSG9070' USING DOC-FILE-OPEN-SW DOC-REC
            pdaScia8100.getDoc_Rec_Doc_Plan_Num(),pdaScia8100.getDoc_Rec_Doc_Part_Num(),pdaScia8100.getDoc_Rec_Doc_Subplan_Num(),pdaScia8100.getDoc_Rec_Doc_Issue_Date(),
            pdaScia8100.getDoc_Rec_Doc_Inst_Name(),pdaScia8100.getDoc_Rec_Doc_Inst_Address().getValue(1,":",4),pdaScia8100.getDoc_Rec_Doc_Inst_Zip(),pdaScia8100.getDoc_Rec_Doc_Inst_Ext_Xfer_Ind(),
            pdaScia8100.getDoc_Rec_Doc_Inst_Gsra_Loans_Ind(),pdaScia8100.getDoc_Rec_Doc_Inst_Asrs_Ind(),pdaScia8100.getDoc_Rec_Doc_Inst_Erisa_Ind(),pdaScia8100.getDoc_Rec_Doc_Inst_Oia_Is_Ind(),
            pdaScia8100.getDoc_Rec_Doc_Inst_Oia_Is_Type(),pdaScia8100.getDoc_Rec_Doc_Inst_Total_Funds(),pdaScia8100.getDoc_Rec_Doc_Ticker_Symbol().getValue(1,
            ":",100),pdaScia8100.getDoc_Rec_Doc_Cusip_Number().getValue(1,":",100),pdaScia8100.getDoc_Rec_Doc_Fund_Status().getValue(1,":",100),pdaScia8100.getDoc_Rec_Doc_Return_Code(),
            pdaScia8100.getDoc_Rec_Doc_Return_Msg_Module(),pdaScia8100.getDoc_Rec_Doc_Return_Error_Sw(),pdaScia8100.getDoc_Rec_Doc_Return_File_Oper(),pdaScia8100.getDoc_Rec_Doc_Return_Message(),
            pdaScia8100.getDoc_Rec_Doc_Plan_Id(),pdaScia8100.getDoc_Rec_Doc_Guar_Int_Rate_Out(),pdaScia8100.getDoc_Rec_Doc_Employer_Name(),pdaScia8100.getDoc_Rec_Doc_Int_Rate_Return_Code(),
            pdaScia8100.getDoc_Rec_Doc_Int_Rate_Return_Message(),pdaScia8100.getDoc_Rec_Doc_Environment_Ind(),pdaScia8100.getDoc_Rec_Doc_Product_Pricing_Level(),
            pdaScia8100.getDoc_Rec_Doc_Roth_Ind(),pdaScia8100.getDoc_Rec_Doc_Plan_Name(),pdaScia8100.getDoc_Rec_Doc_Tic_Startdate(),pdaScia8100.getDoc_Rec_Doc_Tic_Enddate(),
            pdaScia8100.getDoc_Rec_Doc_Tic_Percentage(),pdaScia8100.getDoc_Rec_Doc_Tic_Postdays(),pdaScia8100.getDoc_Rec_Doc_Tic_Limit(),pdaScia8100.getDoc_Rec_Doc_Tic_Postfreq(),
            pdaScia8100.getDoc_Rec_Doc_Tic_Pl_Level(),pdaScia8100.getDoc_Rec_Doc_Tic_Windowdays(),pdaScia8100.getDoc_Rec_Doc_Tic_Reqdlywindow(),pdaScia8100.getDoc_Rec_Doc_Tic_Recap_Prov(),
            pdaScia8100.getDoc_Rec_Fill1());
        //*   IF TEMP-INPUT-PARM = 'LEGAL' OR DOC-EOF = 'Y'         /* SGRDSR KG
        if (condition(pdaScia8100.getDoc_Rec_Doc_Eof().equals("Y")))                                                                                                      //Natural: IF DOC-EOF = 'Y'
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 100
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(100)); pnd_I.nadd(1))
        {
            if (condition(pdaScia8100.getDoc_Rec_Doc_Ticker_Symbol().getValue(pnd_I).equals(" ")))                                                                        //Natural: IF DOC-TICKER-SYMBOL ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  CREF REA STARTS REMOVE HARD CODING OF TICKER SYMBOL          /* CREA
            //*  ---------------------------------------------------------------------*
            //*  IN THE PARAMATER FILE, TRANSLTE-FUND-SEQ = 1 IS FOR 'TIAA' HARD CODING
            //*  AND TRANSLTE-FUND-SEQ = 11 IS FOR 'TSAF#' HARD CODING WHEN ASSIGNING
            //*  'SRF' TO TEMP-SPEC-FUND
            //*  NOTE: IF THE TRANSLTE-FUND-SEQ FOR 1 AND 11 CHANGED, THE LOGIC BELOW
            //*        WILL NEED TO MODIFY
            //*  ---------------------------------------------------------------------*
            pnd_First_4.setValue(pdaScia8100.getDoc_Rec_Doc_Ticker_Symbol().getValue(pnd_I).getSubstring(1,4));                                                           //Natural: MOVE SUBSTRING ( DOC-REC.DOC-TICKER-SYMBOL ( #I ) ,1,4 ) TO #FIRST-4
            DbsUtil.examine(new ExamineSource(pnd_Translte_File_Pnd_Translte_Fund_First_4.getValue("*")), new ExamineSearch(pnd_First_4), new ExamineGivingIndex(pnd_Fund_Idx)); //Natural: EXAMINE #TRANSLTE-FUND-FIRST-4 ( * ) #FIRST-4 GIVING INDEX #FUND-IDX
            if (condition(pnd_Fund_Idx.greater(getZero())))                                                                                                               //Natural: IF #FUND-IDX > 0
            {
                if (condition(pnd_Translte_File_Pnd_Translte_Fund_Seq.getValue(pnd_Fund_Idx).equals(1)))                                                                  //Natural: IF #TRANSLTE-FUND-SEQ ( #FUND-IDX ) = 1
                {
                    pdaScia8100.getDoc_Rec_Doc_Ticker_Symbol().getValue(pnd_I).setValue(pnd_Translte_File_Pnd_Translte_Ticker.getValue(pnd_Fund_Idx));                    //Natural: ASSIGN DOC-REC.DOC-TICKER-SYMBOL ( #I ) := #TRANSLTE-TICKER ( #FUND-IDX )
                    //*    ELSE                                                         /* IMBS
                    //*      IF #TRANSLTE-FUND-SEQ(#FUND-IDX) = 11                      /* IMBS
                    //*         IF SUBSTRING(DOC-REC.DOC-TICKER-SYMBOL(#I),1,5) =       /* IMBS
                    //*            SUBSTRING(#TRANSLTE-TICKER(#FUND-IDX),1,5)           /* IMBS
                    //*            TEMP-SPEC-FUND := #TRANSLTE-FUND-CODE(#FUND-IDX)     /* IMBS
                    //*         END-IF                                                  /* IMBS
                    //*      END-IF    /* (2580)                                        /* IMBS
                    //*  (2550)
                }                                                                                                                                                         //Natural: END-IF
                //*  (2540) IF #FUND-IDX > 0                /* CREA  END <<<
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  SGRDSR KG
        //*  KG TNGSUB
        if (condition(temp_Input_Parm.equals("LEGAL") || ap_Substitution_Contract_Ind.notEquals(" ")))                                                                    //Natural: IF TEMP-INPUT-PARM = 'LEGAL' OR AP-SUBSTITUTION-CONTRACT-IND NE ' '
        {
            //*  SGRDSR KG
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
            //*  SGRDSR KG
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaScia8100.getDoc_Rec_Doc_Return_Code().notEquals(getZero())))                                                                                     //Natural: IF DOC-RETURN-CODE NE 0
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM READ-EXTERNALIZATION
        sub_Read_Externalization();
        if (condition(Global.isEscape())) {return;}
        //*  FUND INFO + EXTERNALIZATION
        getWorkFiles().write(2, false, workfile_Rec);                                                                                                                     //Natural: WRITE WORK FILE 2 WORKFILE-REC
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-EXTERNALIZATION
        //*   RETRIEVE ALL FUNDS FROM NEW-EXT-CNTRL-FND
        //*  COUNT FUNDS PER CATEGORY
        //*      P-CATEGORY-UNIQUE (#J) (AL=30)
        //* **************************************************031507***************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-FUND-INFO
        //*  52787 START
        //*                                                  /* 52787 END                                                                                                 //Natural: ON ERROR
    }
    private void sub_Read_Externalization() throws Exception                                                                                                              //Natural: READ-EXTERNALIZATION
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        p_Return_Code.reset();                                                                                                                                            //Natural: RESET P-RETURN-CODE P-RETURN-MSG P-CATEGORY-UNIQUE ( * ) P-FUND-CODE ( * ) P-FUND-NUM-CDE ( * ) P-FUND-NAME ( * ) P-CATEGORY-CNT P-LOB P-LOB-TYPE P-REGION-CODE P-COLL-CODE
        p_Return_Msg.reset();
        p_Category_Unique.getValue("*").reset();
        p_Fund_Code.getValue("*").reset();
        workfile_Rec_P_Fund_Num_Cde.getValue("*").reset();
        workfile_Rec_P_Fund_Name.getValue("*").reset();
        workfile_Rec_P_Category_Cnt.reset();
        workfile_Rec_P_Lob.reset();
        workfile_Rec_P_Lob_Type.reset();
        workfile_Rec_P_Region_Code.reset();
        workfile_Rec_P_Coll_Code.reset();
        workfile_Rec_P_Plan_No.setValue(pdaScia8100.getDoc_Rec_Doc_Plan_Num());                                                                                           //Natural: ASSIGN P-PLAN-NO := DOC-PLAN-NUM
        workfile_Rec_P_Subplan_No.setValue(pdaScia8100.getDoc_Rec_Doc_Subplan_Num());                                                                                     //Natural: ASSIGN P-SUBPLAN-NO := DOC-SUBPLAN-NUM
        p_Fund_Code.getValue(1,":",100).setValue(pdaScia8100.getDoc_Rec_Doc_Ticker_Symbol().getValue(1,":",100));                                                         //Natural: ASSIGN P-FUND-CODE ( 1:100 ) := DOC-TICKER-SYMBOL ( 1:100 )
        workfile_Rec_P_Tiaa_Cntrct.setValue(ap_Tiaa_Cntrct);                                                                                                              //Natural: ASSIGN P-TIAA-CNTRCT := AP-TIAA-CNTRCT
        workfile_Rec_P_Cref_Cert.setValue(ap_Cref_Cert);                                                                                                                  //Natural: ASSIGN P-CREF-CERT := AP-CREF-CERT
        workfile_Rec_P_Lob.setValue(ap_Lob);                                                                                                                              //Natural: ASSIGN P-LOB := AP-LOB
        workfile_Rec_P_Lob_Type.setValue(ap_Lob_Type);                                                                                                                    //Natural: ASSIGN P-LOB-TYPE := AP-LOB-TYPE
        workfile_Rec_P_Region_Code.setValue(ap_Region_Code);                                                                                                              //Natural: ASSIGN P-REGION-CODE := AP-REGION-CODE
        if (condition(ap_Coll_Code.greater(" ")))                                                                                                                         //Natural: IF AP-COLL-CODE GT ' '
        {
            workfile_Rec_P_Coll_Code.setValue(ap_Coll_Code.getSubstring(1,4));                                                                                            //Natural: MOVE SUBSTR ( AP-COLL-CODE,1,4 ) TO P-COLL-CODE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Nec_Fnd_Super5_Pnd_Nec_Table_Cde.setValue("FND");                                                                                                             //Natural: ASSIGN #NEC-TABLE-CDE := 'FND'
        pnd_Nec_Fnd_Super5_Pnd_Nec_Category_Rptng_Seq.reset();                                                                                                            //Natural: RESET #NEC-CATEGORY-RPTNG-SEQ #NEC-CATEGORY-FUND-SEQ #I
        pnd_Nec_Fnd_Super5_Pnd_Nec_Category_Fund_Seq.reset();
        pnd_I.reset();
        //*  MAKE SURE THERE ARE NO EXISTING RECORDS
        //*  033007 START
        pnd_Cnt.reset();                                                                                                                                                  //Natural: RESET #CNT #TABLE-ID-ENTRY-CDE
        pnd_Table_Id_Entry_Cde.reset();
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr.setValue(999999);                                                                                                   //Natural: ASSIGN #ENTRY-TABLE-ID-NBR := 999999
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id.setValue("FN");                                                                                                     //Natural: ASSIGN #ENTRY-TABLE-SUB-ID := 'FN'
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde.setValue(Global.getINIT_USER());                                                                                             //Natural: ASSIGN #ENTRY-CDE := *INIT-USER
        vw_app_Table_Entry.startDatabaseRead                                                                                                                              //Natural: READ APP-TABLE-ENTRY BY TABLE-ID-ENTRY-CDE STARTING FROM #TABLE-ID-ENTRY-CDE
        (
        "RD1",
        new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", ">=", pnd_Table_Id_Entry_Cde, WcType.BY) },
        new Oc[] { new Oc("TABLE_ID_ENTRY_CDE", "ASC") }
        );
        RD1:
        while (condition(vw_app_Table_Entry.readNextRow("RD1")))
        {
            if (condition(app_Table_Entry_Entry_Table_Id_Nbr.notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr) || app_Table_Entry_Entry_Table_Sub_Id.notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id)  //Natural: IF ENTRY-TABLE-ID-NBR NE #ENTRY-TABLE-ID-NBR OR ENTRY-TABLE-SUB-ID NE #ENTRY-TABLE-SUB-ID OR SUBSTR ( ENTRY-CDE,1,8 ) NE #ENTRY-CDE
                || !app_Table_Entry_Entry_Cde.getSubstring(1,8).equals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  033007 END
            }                                                                                                                                                             //Natural: END-IF
            //*  52787 START
            GT1:                                                                                                                                                          //Natural: GET APP-TABLE-UPD *ISN ( RD1. )
            vw_app_Table_Upd.readByID(vw_app_Table_Entry.getAstISN("RD1"), "GT1");
            //*  52787 END
            vw_app_Table_Upd.deleteDBRow("GT1");                                                                                                                          //Natural: DELETE ( GT1. )
            pnd_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #CNT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Cnt.greater(getZero())))                                                                                                                        //Natural: IF #CNT GT 0
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde1.setValue("FND");                                                                                                            //Natural: ASSIGN #NEC-TABLE-CDE1 := 'FND'
        pnd_Nec_Cat_Super_Pnd_Nec_Table_Cde_2.setValue("CAT");                                                                                                            //Natural: ASSIGN #NEC-TABLE-CDE-2 := 'CAT'
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 #MAX-OCCURS
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Occurs)); pnd_I.nadd(1))
        {
            if (condition(p_Fund_Code.getValue(pnd_I).equals(" ")))                                                                                                       //Natural: IF P-FUND-CODE ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Nec_Fnd_Super1_Pnd_Nec_Tkr_Symbl.setValue(p_Fund_Code.getValue(pnd_I));                                                                                   //Natural: ASSIGN #NEC-TKR-SYMBL := P-FUND-CODE ( #I )
            //*  FIND IF FUND IS VALID OR ACTIVE
            pdaAcia3009.getAcia3009().reset();                                                                                                                            //Natural: RESET ACIA3009
            pdaAcia3009.getAcia3009_Pnd_Ticker_Symbol().setValue(pnd_Nec_Fnd_Super1_Pnd_Nec_Tkr_Symbl);                                                                   //Natural: ASSIGN ACIA3009.#TICKER-SYMBOL := #NEC-TKR-SYMBL
            DbsUtil.callnat(Acin3009.class , getCurrentProcessState(), pdaAcia3009.getAcia3009(), pnd_Debug);                                                             //Natural: CALLNAT 'ACIN3009' ACIA3009 #DEBUG
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            if (condition(pdaAcia3009.getAcia3009_Pnd_Return_Code().notEquals(getZero())))                                                                                //Natural: IF ACIA3009.#RETURN-CODE NE 0
            {
                if (condition(ap_Client_Id.equals("064157") && pdaAcia3009.getAcia3009_Pnd_Ticker_Symbol().equals("DFEMX")))                                              //Natural: IF AP-CLIENT-ID = '064157' AND ACIA3009.#TICKER-SYMBOL = 'DFEMX'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, pdaAcia3009.getAcia3009_Pnd_Return_Txt());                                                                                      //Natural: WRITE ACIA3009.#RETURN-TXT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            vw_new_Ext_Cntrl_Fnd_View.startDatabaseFind                                                                                                                   //Natural: FIND NEW-EXT-CNTRL-FND-VIEW WITH NEC-FND-SUPER1 = #NEC-FND-SUPER1
            (
            "FIND01",
            new Wc[] { new Wc("NEC_FND_SUPER1", "=", pnd_Nec_Fnd_Super1, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_new_Ext_Cntrl_Fnd_View.readNextRow("FIND01")))
            {
                vw_new_Ext_Cntrl_Fnd_View.setIfNotFoundControlFlag(false);
                pnd_Nec_Cat_Super_Pnd_Nec_Category_Class_Cde.setValue(new_Ext_Cntrl_Fnd_View_Nec_Category_Class_Cde);                                                     //Natural: ASSIGN #NEC-CATEGORY-CLASS-CDE := NEW-EXT-CNTRL-FND-VIEW.NEC-CATEGORY-CLASS-CDE
                vw_new_Ext_Cntrl_Cat_View.startDatabaseFind                                                                                                               //Natural: FIND NEW-EXT-CNTRL-CAT-VIEW WITH NEC-CAT-SUPER = #NEC-CAT-SUPER
                (
                "FIND02",
                new Wc[] { new Wc("NEC_CAT_SUPER", "=", pnd_Nec_Cat_Super, WcType.WITH) }
                );
                FIND02:
                while (condition(vw_new_Ext_Cntrl_Cat_View.readNextRow("FIND02")))
                {
                    vw_new_Ext_Cntrl_Cat_View.setIfNotFoundControlFlag(false);
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM STORE-FUND-INFO
                sub_Store_Fund_Info();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  RE-ORDER THE FUNDS BASED ON CTGRY-NAME
        workfile_Rec_P_Category_Cnt.reset();                                                                                                                              //Natural: RESET P-CATEGORY-CNT
        //*  033007 START
        pnd_Cnt.reset();                                                                                                                                                  //Natural: RESET #CNT #TABLE-ID-ENTRY-CDE
        pnd_Table_Id_Entry_Cde.reset();
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr.setValue(999999);                                                                                                   //Natural: ASSIGN #ENTRY-TABLE-ID-NBR := 999999
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id.setValue("FN");                                                                                                     //Natural: ASSIGN #ENTRY-TABLE-SUB-ID := 'FN'
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde.setValue(Global.getINIT_USER());                                                                                             //Natural: ASSIGN #ENTRY-CDE := *INIT-USER
        vw_app_Table_Entry.startDatabaseRead                                                                                                                              //Natural: READ APP-TABLE-ENTRY BY TABLE-ID-ENTRY-CDE STARTING FROM #TABLE-ID-ENTRY-CDE
        (
        "RDTBLE",
        new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", ">=", pnd_Table_Id_Entry_Cde, WcType.BY) },
        new Oc[] { new Oc("TABLE_ID_ENTRY_CDE", "ASC") }
        );
        RDTBLE:
        while (condition(vw_app_Table_Entry.readNextRow("RDTBLE")))
        {
            if (condition(app_Table_Entry_Entry_Table_Id_Nbr.notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr) || app_Table_Entry_Entry_Table_Sub_Id.notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id)  //Natural: IF ENTRY-TABLE-ID-NBR NE #ENTRY-TABLE-ID-NBR OR ENTRY-TABLE-SUB-ID NE #ENTRY-TABLE-SUB-ID OR SUBSTR ( ENTRY-CDE,1,8 ) NE #ENTRY-CDE
                || !app_Table_Entry_Entry_Cde.getSubstring(1,8).equals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  033007 END
            }                                                                                                                                                             //Natural: END-IF
            pnd_W_Ticker.setValue(app_Table_Entry_Addtnl_Dscrptn_Txt.getValue(1));                                                                                        //Natural: ASSIGN #W-TICKER := ADDTNL-DSCRPTN-TXT ( 1 )
            DbsUtil.examine(new ExamineSource(p_Fund_Code.getValue("*"),true), new ExamineSearch(pnd_W_Ticker), new ExamineGivingIndex(pnd_Idx));                         //Natural: EXAMINE FULL P-FUND-CODE ( * ) FOR #W-TICKER GIVING INDEX #IDX
            if (condition(pnd_Idx.greater(getZero())))                                                                                                                    //Natural: IF #IDX GT 0
            {
                if (condition(pnd_Prev_Fund_Category.greater(" ") && pnd_Prev_Fund_Category.notEquals(app_Table_Entry_Entry_Dscrptn_Txt)))                                //Natural: IF #PREV-FUND-CATEGORY GT ' ' AND #PREV-FUND-CATEGORY NE ENTRY-DSCRPTN-TXT
                {
                    pnd_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CNT
                }                                                                                                                                                         //Natural: END-IF
                pnd_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #CNT
                pnd_T_Fund_Code.getValue(pnd_Cnt).setValue(pnd_W_Ticker);                                                                                                 //Natural: ASSIGN #T-FUND-CODE ( #CNT ) := #W-TICKER
                pnd_T_Fund_Num_Cde.getValue(pnd_Cnt).setValue(app_Table_Entry_Addtnl_Dscrptn_Txt.getValue(3));                                                            //Natural: ASSIGN #T-FUND-NUM-CDE ( #CNT ) := ADDTNL-DSCRPTN-TXT ( 3 )
                pnd_T_Fund_Name.getValue(pnd_Cnt).setValue(app_Table_Entry_Addtnl_Dscrptn_Txt.getValue(2));                                                               //Natural: ASSIGN #T-FUND-NAME ( #CNT ) := ADDTNL-DSCRPTN-TXT ( 2 )
                workfile_Rec_P_Category.getValue(pnd_Cnt).setValue(app_Table_Entry_Entry_Dscrptn_Txt);                                                                    //Natural: ASSIGN P-CATEGORY ( #CNT ) := #T-FUND-CATEGORY ( #CNT ) := ENTRY-DSCRPTN-TXT
                pnd_T_Fund_Category.getValue(pnd_Cnt).setValue(app_Table_Entry_Entry_Dscrptn_Txt);
                if (condition(pnd_Prev_Fund_Category.equals(app_Table_Entry_Entry_Dscrptn_Txt)))                                                                          //Natural: IF #PREV-FUND-CATEGORY = ENTRY-DSCRPTN-TXT
                {
                    pnd_T_Fund_Category.getValue(pnd_Cnt).reset();                                                                                                        //Natural: RESET #T-FUND-CATEGORY ( #CNT )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    workfile_Rec_P_Category_Cnt.nadd(1);                                                                                                                  //Natural: ADD 1 TO P-CATEGORY-CNT
                    p_Category_Unique.getValue(workfile_Rec_P_Category_Cnt).setValue(app_Table_Entry_Entry_Dscrptn_Txt);                                                  //Natural: ASSIGN P-CATEGORY-UNIQUE ( P-CATEGORY-CNT ) := ENTRY-DSCRPTN-TXT
                }                                                                                                                                                         //Natural: END-IF
                pnd_Prev_Fund_Category.setValue(app_Table_Entry_Entry_Dscrptn_Txt);                                                                                       //Natural: ASSIGN #PREV-FUND-CATEGORY := ENTRY-DSCRPTN-TXT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(Global.getINIT_USER(),"'T'")))                                                                                              //Natural: IF *INIT-USER = MASK ( 'T' )
            {
                getReports().write(0, "**DELETING**",NEWLINE,"=",app_Table_Entry_Entry_Table_Id_Nbr,"=",app_Table_Entry_Entry_Table_Sub_Id,"=",app_Table_Entry_Entry_Cde); //Natural: WRITE '**DELETING**' / '=' ENTRY-TABLE-ID-NBR '=' ENTRY-TABLE-SUB-ID '=' ENTRY-CDE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RDTBLE"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RDTBLE"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  52787 START
            GT2:                                                                                                                                                          //Natural: GET APP-TABLE-UPD *ISN ( RDTBLE. )
            vw_app_Table_Upd.readByID(vw_app_Table_Entry.getAstISN("RDTBLE"), "GT2");
            //*  52787 END
            vw_app_Table_Upd.deleteDBRow("GT2");                                                                                                                          //Natural: DELETE ( GT2. )
            pnd_Deleted.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #DELETED
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "APP TABLE ENTRY ADDED  :",pnd_Added);                                                                                                      //Natural: WRITE 'APP TABLE ENTRY ADDED  :' #ADDED
        if (Global.isEscape()) return;
        getReports().write(0, "APP TABLE ENTRY DELETED:",pnd_Deleted);                                                                                                    //Natural: WRITE 'APP TABLE ENTRY DELETED:' #DELETED
        if (Global.isEscape()) return;
        //*  031407 END
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO 106
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(106)); pnd_I.nadd(1))
        {
            if (condition(p_Category_Unique.getValue(pnd_I).equals(" ")))                                                                                                 //Natural: IF P-CATEGORY-UNIQUE ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(workfile_Rec_P_Category.getValue("*"),true), new ExamineSearch(p_Category_Unique.getValue(pnd_I), true),                    //Natural: EXAMINE FULL P-CATEGORY ( * ) FOR FULL P-CATEGORY-UNIQUE ( #I ) ABSOLUTE GIVING NUMBER #CNT INDEX #INDEX
                new ExamineGivingNumber(pnd_Cnt), new ExamineGivingIndex(pnd_Index));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        p_Fund_Code.getValue(1,":",106).setValue(pnd_T_Fund_Code.getValue(1,":",106));                                                                                    //Natural: ASSIGN P-FUND-CODE ( 1:106 ) := #T-FUND-CODE ( 1:106 )
        workfile_Rec_P_Fund_Num_Cde.getValue(1,":",106).setValue(pnd_T_Fund_Num_Cde.getValue(1,":",106));                                                                 //Natural: ASSIGN P-FUND-NUM-CDE ( 1:106 ) := #T-FUND-NUM-CDE ( 1:106 )
        workfile_Rec_P_Fund_Name.getValue(1,":",106).setValue(pnd_T_Fund_Name.getValue(1,":",106));                                                                       //Natural: ASSIGN P-FUND-NAME ( 1:106 ) := #T-FUND-NAME ( 1:106 )
        workfile_Rec_P_Category.getValue(1,":",106).setValue(pnd_T_Fund_Category.getValue(1,":",106));                                                                    //Natural: ASSIGN P-CATEGORY ( 1:106 ) := #T-FUND-CATEGORY ( 1:106 )
        //*  TEST JOB   031407
        if (condition(Global.getDEVICE().notEquals("BATCH") || DbsUtil.maskMatches(Global.getINIT_USER(),"'T'")))                                                         //Natural: IF *DEVICE NE 'BATCH' OR *INIT-USER = MASK ( 'T' )
        {
            FOR04:                                                                                                                                                        //Natural: FOR #J = 1 TO 106
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(106)); pnd_J.nadd(1))
            {
                //*  031407 START
                if (condition(p_Fund_Code.getValue(pnd_J).equals(" ")))                                                                                                   //Natural: IF P-FUND-CODE ( #J ) = ' '
                {
                    if (condition(pnd_J.greater(1) && p_Fund_Code.getValue(pnd_J.getDec().subtract(1)).equals(" ")))                                                      //Natural: IF #J GT 1 AND P-FUND-CODE ( #J - 1 ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    //*  031407 END
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(0, workfile_Rec_P_Category.getValue(pnd_J), new AlphanumericLength (30),workfile_Rec_P_Fund_Num_Cde.getValue(pnd_J),workfile_Rec_P_Fund_Name.getValue(pnd_J),  //Natural: WRITE P-CATEGORY ( #J ) ( AL = 30 ) P-FUND-NUM-CDE ( #J ) P-FUND-NAME ( #J ) ( AL = 30 )
                    new AlphanumericLength (30));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            //* **  WRITE / 'TOTAL CATEGORIES: ' P-CATEGORY-CNT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Store_Fund_Info() throws Exception                                                                                                                   //Natural: STORE-FUND-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_app_Table_Entry.reset();                                                                                                                                       //Natural: RESET APP-TABLE-ENTRY
        app_Table_Entry_Entry_Table_Id_Nbr.setValue(999999);                                                                                                              //Natural: ASSIGN ENTRY-TABLE-ID-NBR := 999999
        app_Table_Entry_Entry_Table_Sub_Id.setValue("FN");                                                                                                                //Natural: ASSIGN ENTRY-TABLE-SUB-ID := 'FN'
        app_Table_Entry_Entry_Cde.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, Global.getINIT_USER(), new_Ext_Cntrl_Fnd_View_Nec_Category_Rptng_Seq,          //Natural: COMPRESS *INIT-USER NEW-EXT-CNTRL-FND-VIEW.NEC-CATEGORY-RPTNG-SEQ NEW-EXT-CNTRL-FND-VIEW.NEC-CATEGORY-FUND-SEQ INTO ENTRY-CDE LEAVING NO
            new_Ext_Cntrl_Fnd_View_Nec_Category_Fund_Seq));
        app_Table_Entry_Entry_Dscrptn_Txt.setValue(new_Ext_Cntrl_Cat_View_Nec_Category_Class_Desc);                                                                       //Natural: ASSIGN ENTRY-DSCRPTN-TXT := NEW-EXT-CNTRL-CAT-VIEW.NEC-CATEGORY-CLASS-DESC
        app_Table_Entry_Addtnl_Dscrptn_Txt.getValue(1).setValue(pnd_Nec_Fnd_Super1_Pnd_Nec_Tkr_Symbl);                                                                    //Natural: ASSIGN ADDTNL-DSCRPTN-TXT ( 1 ) := #NEC-TKR-SYMBL
        app_Table_Entry_Addtnl_Dscrptn_Txt.getValue(2).setValue(new_Ext_Cntrl_Fnd_View_Nec_Fund_Nme);                                                                     //Natural: ASSIGN ADDTNL-DSCRPTN-TXT ( 2 ) := NEW-EXT-CNTRL-FND-VIEW.NEC-FUND-NME
        app_Table_Entry_Addtnl_Dscrptn_Txt.getValue(3).setValueEdited(new_Ext_Cntrl_Fnd_View_Nec_Num_Fund_Cde,new ReportEditMask("999"));                                 //Natural: MOVE EDITED NEW-EXT-CNTRL-FND-VIEW.NEC-NUM-FUND-CDE ( EM = 999 ) TO ADDTNL-DSCRPTN-TXT ( 3 )
        vw_app_Table_Entry.insertDBRow();                                                                                                                                 //Natural: STORE APP-TABLE-ENTRY
        pnd_Added.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #ADDED
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        short decideConditionsMet453 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF *ERROR-NR;//Natural: VALUE 3145
        if (condition((Global.getERROR_NR().equals(3145))))
        {
            decideConditionsMet453++;
            //* ***********************************************************************
            //*    START OF COPY CODE PSTC9902   : RETRY LOGIC
            //*  - FIRST CHECK FOR SUBSEQUENT RETRY - SAME LINE NUMBER.
            //*  - IF SUBSEQUENT RETRY, CHECK TOTAL ELAPSED TIME
            //*  - IF NEW RETRY, SET DEFAULT VALUES FOR WAIT PARAMETERS IF NONE SPECI-
            //*    FIED, START CLOCK.
            //*  - CALL CMROLL TO ACTIVATE WAITING PERIOD
            //*  - RETRY
            //*  ----------------------------------------------------------------------
            //*         PROGRAM USING THIS COPYCODE MUST USE LDA PSTL9902
            //*                                          AND PDA PSTA9200 AS AN LDA.
            //*         #WAIT-TIME & #TOTAL-WAITING-TIME MAY BE SPECIFIED BY THE
            //*         PROGRAM USING THIS COPY CODE, JUST BEFORE THE INCLUDE STATEMENT
            //*         THEY ARE BOTH IN T FORMAT. TO INITIALIZE A T FIELD, USE
            //*         MOVE T'00:mm:ss' TO #WAIT-TIME
            //*         WHERE SS = SECONDS  (MAX OF 99 SECONDS)
            //*         (THE LIMITATION IS BECAUSE CMROLL ACCEPT UP TO 99 SECONDS ONLY)
            //*  ----------------------------------------------------------------------
            //* ***********************************************************************
            if (condition(Global.getERROR_NR().equals(3145)))                                                                                                             //Natural: IF *ERROR-NR = 3145
            {
                ldaPstl9902.getPstl9902_Pnd_Start_Time_This_Error().setValue(Global.getTIMX());                                                                           //Natural: ASSIGN PSTL9902.#START-TIME-THIS-ERROR := *TIMX
                if (condition(ldaPstl9902.getPstl9902_Pnd_Prev_Error_Line().equals(Global.getERROR_LINE())))                                                              //Natural: IF PSTL9902.#PREV-ERROR-LINE = *ERROR-LINE
                {
                    ldaPstl9902.getPstl9902_Pnd_Total_Elapsed_Time().compute(new ComputeParameters(false, ldaPstl9902.getPstl9902_Pnd_Total_Elapsed_Time()),              //Natural: ASSIGN PSTL9902.#TOTAL-ELAPSED-TIME := *TIMX - PSTL9902.#TIME-STARTED
                        Global.getTIMX().subtract(ldaPstl9902.getPstl9902_Pnd_Time_Started()));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl9902.getPstl9902_Pnd_Time_Started().setValue(Global.getTIMX());                                                                                //Natural: ASSIGN PSTL9902.#TIME-STARTED := *TIMX
                    ldaPstl9902.getPstl9902_Pnd_Prev_Error_Line().setValue(Global.getERROR_LINE());                                                                       //Natural: ASSIGN PSTL9902.#PREV-ERROR-LINE := *ERROR-LINE
                    short decideConditionsMet490 = 0;                                                                                                                     //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN PSTL9902.#WAIT-TIME LE 0
                    if (condition(ldaPstl9902.getPstl9902_Pnd_Wait_Time().lessOrEqual(getZero())))
                    {
                        decideConditionsMet490++;
                        if (condition(DbsUtil.maskMatches(Global.getINIT_USER(),"'RPC'.....") || DbsUtil.maskMatches(Global.getINIT_USER(),"'SNY'.....")))                //Natural: IF *INIT-USER = MASK ( 'RPC'..... ) OR = MASK ( 'SNY'..... )
                        {
                            ldaPstl9902.getPstl9902_Pnd_Wait_Time().setValue(20);                                                                                         //Natural: MOVE T'00:00:02' TO PSTL9902.#WAIT-TIME
                            getReports().write(0, "Setting WAIT TIME to",ldaPstl9902.getPstl9902_Pnd_Wait_Time());                                                        //Natural: WRITE 'Setting WAIT TIME to' PSTL9902.#WAIT-TIME
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(Global.getDEVICE().equals("BATCH")))                                                                                            //Natural: IF *DEVICE = 'BATCH'
                            {
                                ldaPstl9902.getPstl9902_Pnd_Wait_Time().setValue(150);                                                                                    //Natural: MOVE T'00:00:15' TO PSTL9902.#WAIT-TIME
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                ldaPstl9902.getPstl9902_Pnd_Wait_Time().setValue(40);                                                                                     //Natural: MOVE T'00:00:04' TO PSTL9902.#WAIT-TIME
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN PSTL9902.#TOTAL-WAITING-TIME LE 0
                    if (condition(ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time().lessOrEqual(getZero())))
                    {
                        decideConditionsMet490++;
                        if (condition(DbsUtil.maskMatches(Global.getINIT_USER(),"'RPC'.....") || DbsUtil.maskMatches(Global.getINIT_USER(),"'SNY'.....")))                //Natural: IF *INIT-USER = MASK ( 'RPC'..... ) OR = MASK ( 'SNY'..... )
                        {
                            ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time().setValue(100);                                                                               //Natural: MOVE T'00:00:10' TO PSTL9902.#TOTAL-WAITING-TIME
                            getReports().write(0, "Setting TOTAL WAIT TIME to",ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time());                                         //Natural: WRITE 'Setting TOTAL WAIT TIME to' PSTL9902.#TOTAL-WAITING-TIME
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(Global.getDEVICE().equals("BATCH")))                                                                                            //Natural: IF *DEVICE = 'BATCH'
                            {
                                ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time().setValue(18000);                                                                         //Natural: MOVE T'00:30:00' TO PSTL9902.#TOTAL-WAITING-TIME
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time().setValue(900);                                                                           //Natural: MOVE T'00:01:30' TO PSTL9902.#TOTAL-WAITING-TIME
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN NONE
                    if (condition(decideConditionsMet490 == 0))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaPstl9902.getPstl9902_Pnd_Total_Elapsed_Time().greater(ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time())))                                //Natural: IF PSTL9902.#TOTAL-ELAPSED-TIME GT PSTL9902.#TOTAL-WAITING-TIME
                {
                    if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                    //Natural: IF *DEVICE = 'BATCH'
                    {
                        getReports().write(0, "Requested record in program",Global.getPROGRAM(),"line",Global.getERROR_LINE(),"was still on hold after",NEWLINE,ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time(),  //Natural: WRITE 'Requested record in program' *PROGRAM 'line' *ERROR-LINE 'was still on hold after' / PSTL9902.#TOTAL-WAITING-TIME ( EM = HH:II:SS )
                            new ReportEditMask ("HH:II:SS"));
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaPsta9200.getPsta9200_Message_Text().setValue(DbsUtil.compress("Requested record in program", Global.getPROGRAM(), "line", Global.getERROR_LINE(),  //Natural: COMPRESS 'Requested record in program' *PROGRAM 'line' *ERROR-LINE 'exceeded hold time limit' INTO PSTA9200.MESSAGE-TEXT
                            "exceeded hold time limit"));
                        pdaPsta9200.getPsta9200_Enter_Allowed().reset();                                                                                                  //Natural: RESET PSTA9200.ENTER-ALLOWED PSTA9200.CONFIRM-ALLOWED
                        pdaPsta9200.getPsta9200_Confirm_Allowed().reset();
                        pdaPsta9200.getPsta9200_Exit_Allowed().setValue(true);                                                                                            //Natural: ASSIGN PSTA9200.EXIT-ALLOWED := TRUE
                        DbsUtil.callnat(Pstn9200.class , getCurrentProcessState(), pdaPsta9200.getPsta9200());                                                            //Natural: CALLNAT 'PSTN9200' PSTA9200
                        if (condition(Global.isEscape())) return;
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl9902.getPstl9902_Pnd_Time_Elapsed().compute(new ComputeParameters(false, ldaPstl9902.getPstl9902_Pnd_Time_Elapsed()), Global.getTIMX().subtract(ldaPstl9902.getPstl9902_Pnd_Start_Time_This_Error())); //Natural: ASSIGN PSTL9902.#TIME-ELAPSED := *TIMX - PSTL9902.#START-TIME-THIS-ERROR
                    if (condition(ldaPstl9902.getPstl9902_Pnd_Time_Elapsed().less(ldaPstl9902.getPstl9902_Pnd_Wait_Time())))                                              //Natural: IF PSTL9902.#TIME-ELAPSED LT PSTL9902.#WAIT-TIME
                    {
                        ldaPstl9902.getPstl9902_Pnd_Wait_Time_Left().compute(new ComputeParameters(false, ldaPstl9902.getPstl9902_Pnd_Wait_Time_Left()),                  //Natural: ASSIGN PSTL9902.#WAIT-TIME-LEFT := PSTL9902.#WAIT-TIME - PSTL9902.#TIME-ELAPSED
                            ldaPstl9902.getPstl9902_Pnd_Wait_Time().subtract(ldaPstl9902.getPstl9902_Pnd_Time_Elapsed()));
                        ldaPstl9902.getPstl9902_Pnd_Seconds_Left_A().setValueEdited(ldaPstl9902.getPstl9902_Pnd_Wait_Time_Left(),new ReportEditMask("SS"));               //Natural: MOVE EDITED PSTL9902.#WAIT-TIME-LEFT ( EM = SS ) TO PSTL9902.#SECONDS-LEFT-A
                        ldaPstl9902.getPstl9902_Pnd_Wait_Seconds_Left().setValue(ldaPstl9902.getPstl9902_Pnd_Seconds_Left_N());                                           //Natural: ASSIGN PSTL9902.#WAIT-SECONDS-LEFT := PSTL9902.#SECONDS-LEFT-N
                        cmrollReturnCode = DbsUtil.callExternalProgram("CMROLL",ldaPstl9902.getPstl9902_Pnd_Wait_Seconds_Left(),ldaPstl9902.getPstl9902_Pnd_Cics_Rqst()); //Natural: CALL 'CMROLL' PSTL9902.#WAIT-SECONDS-LEFT PSTL9902.#CICS-RQST
                    }                                                                                                                                                     //Natural: END-IF
                    OnErrorManager.popRetry();                                                                                                                            //Natural: RETRY
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* ***********************************************************************
            //*     END OF COPYCODE PSTC9902
            //* ***********************************************************************
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            getReports().write(0, ReportOption.NOHDR,"ERROR IN     ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER ",Global.getERROR_NR(),NEWLINE,"ERROR LINE   ",            //Natural: WRITE NOHDR 'ERROR IN     ' *PROGRAM / 'ERROR NUMBER ' *ERROR-NR / 'ERROR LINE   ' *ERROR-LINE / 'KEY' #TABLE-ID-ENTRY-CDE / '=' ENTRY-TABLE-ID-NBR / '=' ENTRY-TABLE-SUB-ID / '=' ENTRY-CDE
                Global.getERROR_LINE(),NEWLINE,"KEY",pnd_Table_Id_Entry_Cde,NEWLINE,"=",app_Table_Entry_Entry_Table_Id_Nbr,NEWLINE,"=",app_Table_Entry_Entry_Table_Sub_Id,
                NEWLINE,"=",app_Table_Entry_Entry_Cde);
            DbsUtil.terminate(4);  if (true) return;                                                                                                                      //Natural: TERMINATE 4
        }                                                                                                                                                                 //Natural: END-DECIDE
    };                                                                                                                                                                    //Natural: END-ERROR
}
