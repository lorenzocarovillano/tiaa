/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:59:05 PM
**        * FROM NATURAL SUBPROGRAM : Appn211
************************************************************
**        * FILE NAME            : Appn211.java
**        * CLASS NAME           : Appn211
**        * INSTANCE NAME        : Appn211
************************************************************
************************************************************************
*  THIS MODULE IS CALLED BY APPB500
*
* PROGRAM NAME : APPN211 (SUBPROGRAM)
* DATE WRITTEN : APPROXIMATELY SAME TIME AS APPB500
* DESCRIPTION  : FUNCTION OF THIS PROGRAM IS TO FIND THE SSN IN MDM
*                AND RETURN THE NAME AND PIN TO THE CALLING MODULE.
*  MODIFIED:
* 09/01/15 L SHU  COR NAAD RETIREMENT
*                 REPLACE COR ADABAS ACCESS WITH MDM ACCESS BY CALLING
*                 THE MDM NATURAL APIS                        (CNR)
* 06/22/17 BABRE  PIN EXPANSION CHANGES. CHG425939            (PINE)
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appn211 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAppa211 pdaAppa211;
    private PdaMdma101 pdaMdma101;

    // Local Variables
    public DbsRecord localVariables;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaAppa211 = new PdaAppa211(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Appn211() throws Exception
    {
        super("Appn211");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  -------------------------------------
        //*  COR FILE WILL BE ACCESSED USING SP COR-SUPPER-SSN-RCDTYPE
        //*  IF RECORD EXIST #PAR-FM-HIS-IND = 'Y'
        //*  ALL NAME FIELDS WILL BE PASSED TO APPS210
        //*  #PAR-FM-PRFX-NME
        //*  #PAR-FM-LAST-NME
        //*  #PAR-FM-FIRST-NME
        //*  #PAR-FM-MDDLE-NME
        //*  #PAR-FM-SFFX-NME
        //*  #PAR-FM-HIS-IND = 'Y'
        //* * RESET COR-XREF-PH-VIEW
                                                                                                                                                                          //Natural: PERFORM FIND-COR-SOC-SEC
        sub_Find_Cor_Soc_Sec();
        if (condition(Global.isEscape())) {return;}
        //*  *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-COR-SOC-SEC
        //*  *********************************
        //* *MOVE #PAR-TO-SOCIAL-SECURITY-NO TO #SSN-FLD
        //* * MOVE 01                         TO #RCD-TYPE-FLD
        //* * FIND (1) COR-XREF-PH-VIEW WITH
        //*                           COR-SUPER-SSN-RCDTYPE = #COR-PH-KEY
        //*   IF NO RECORD FOUND
        //*     MOVE ' ' TO #PAR-FM-HIS-IND
        //*   ESCAPE ROUTINE
        //*   END-NOREC
        //*   MOVE 'Y' TO #PAR-FM-HIS-IND
        //*   PERFORM SETUP-TRANSFER-DATA
        //*  END-FIND
        //*  '=' #I-SSN '=' #O-PIN
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-TRANSFER-DATA
    }
    //*  CNR
    private void sub_Find_Cor_Soc_Sec() throws Exception                                                                                                                  //Natural: FIND-COR-SOC-SEC
    {
        if (BLNatReinput.isReinput()) return;

        pdaMdma101.getPnd_Mdma101_Pnd_I_Ssn().setValue(pdaAppa211.getPnd_Pda_Cor_Batch_Pnd_Par_To_Social_Security_No());                                                  //Natural: ASSIGN #I-SSN := #PAR-TO-SOCIAL-SECURITY-NO
        //*  CALLNAT 'MDMN100A' #MDMA100                            /* CNR /* PINE
        //*  PINE
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        //*  WRITE 'BACK FROM MDMN100A' #O-RETURN-CODE           /* PINE >>
        //*  PINE <<
        getReports().write(0, "BACK FROM MDMN101A",pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code(),"=",pdaMdma101.getPnd_Mdma101_Pnd_I_Ssn(),"=",pdaMdma101.getPnd_Mdma101_Pnd_O_Pin_N12(), //Natural: WRITE 'BACK FROM MDMN101A' #O-RETURN-CODE '=' #I-SSN '=' #O-PIN-N12 '=' #O-LAST-NAME '=' #O-FIRST-NAME
            "=",pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name(),"=",pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name());
        if (Global.isEscape()) return;
        //*  CNR
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().notEquals("0000")))                                                                                   //Natural: IF #O-RETURN-CODE NE '0000'
        {
            pdaAppa211.getPnd_Pda_Cor_Batch_Pnd_Par_Fm_His_Ind().setValue(" ");                                                                                           //Natural: MOVE ' ' TO #PAR-FM-HIS-IND
            //*  CNR
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  CNR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM SETUP-TRANSFER-DATA
            sub_Setup_Transfer_Data();
            if (condition(Global.isEscape())) {return;}
            //*  CNR
        }                                                                                                                                                                 //Natural: END-IF
        //*  END-FIND
    }
    private void sub_Setup_Transfer_Data() throws Exception                                                                                                               //Natural: SETUP-TRANSFER-DATA
    {
        if (BLNatReinput.isReinput()) return;

        pdaAppa211.getPnd_Pda_Cor_Batch_Pnd_Par_Fm_Prfx_Nme().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix());                                                         //Natural: MOVE #O-PREFIX TO #PAR-FM-PRFX-NME
        pdaAppa211.getPnd_Pda_Cor_Batch_Pnd_Par_Fm_Last_Nme().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name());                                                      //Natural: MOVE #O-LAST-NAME TO #PAR-FM-LAST-NME
        pdaAppa211.getPnd_Pda_Cor_Batch_Pnd_Par_Fm_First_Nme().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name());                                                    //Natural: MOVE #O-FIRST-NAME TO #PAR-FM-FIRST-NME
        pdaAppa211.getPnd_Pda_Cor_Batch_Pnd_Par_Fm_Mddle_Nme().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name());                                                   //Natural: MOVE #O-MIDDLE-NAME TO #PAR-FM-MDDLE-NME
        pdaAppa211.getPnd_Pda_Cor_Batch_Pnd_Par_Fm_Sffx_Nme().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Suffix());                                                         //Natural: MOVE #O-SUFFIX TO #PAR-FM-SFFX-NME
        //*  MOVE #O-PIN            TO  #PAR-FM-UNIQUE-ID-NUMBER    /* PINE
        //*  PINE
        pdaAppa211.getPnd_Pda_Cor_Batch_Pnd_Par_Fm_Unique_Id_Number().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Pin_N12());                                                //Natural: MOVE #O-PIN-N12 TO #PAR-FM-UNIQUE-ID-NUMBER
        //*  MOVE PH-PRFX-NME       TO  #PAR-FM-PRFX-NME
        //*  MOVE PH-LAST-NME       TO  #PAR-FM-LAST-NME
        //*  MOVE PH-FIRST-NME      TO  #PAR-FM-FIRST-NME
        //*  MOVE PH-MDDLE-NME      TO  #PAR-FM-MDDLE-NME
        //*  MOVE PH-SFFX-NME       TO  #PAR-FM-SFFX-NME
        //*  MOVE PH-UNIQUE-ID-NBR  TO  #PAR-FM-UNIQUE-ID-NUMBER   /* HK
    }

    //
}
