/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:09:06 AM
**        * FROM NATURAL SUBPROGRAM : Scin3050
************************************************************
**        * FILE NAME            : Scin3050.java
**        * CLASS NAME           : Scin3050
**        * INSTANCE NAME        : Scin3050
************************************************************
************************************************************************
* PROGRAM:  SCIN3050 - SUBPROGRAM VERSION OF COUNTRY CODE
*
*
* FUNCTION: THIS SUBPROGRAM USES COUNTRY CODE TO RETRIEVE COUNTRY NAME
*      FROM  NST-TABLE-ENTRY.
*
*
* CREATED:  08/05/2011    GUY SUDSATAYA
*
* HISTORY
*
*
*
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Scin3050 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Parm_Country_Code;
    private DbsField pnd_Parm_Description;
    private DbsField pnd_Parm_Return_Code;
    private DbsField pnd_Parm_Return_Msg;
    private DbsField pnd_Country_Code;
    private DbsField pnd_Description;
    private DbsField pnd_Line_Cv;

    private DataAccessProgramView vw_nst_Table_Entry;
    private DbsField nst_Table_Entry_Entry_Table_Id_Nbr;
    private DbsField nst_Table_Entry_Entry_Table_Sub_Id;
    private DbsField nst_Table_Entry_Entry_Cde;
    private DbsField nst_Table_Entry_Entry_Dscrptn_Txt;

    private DbsGroup nst_Table_Entry__R_Field_1;
    private DbsField nst_Table_Entry_Pnd_Description_25;
    private DbsField pnd_Start;
    private DbsField pnd_Total_Pages;
    private DbsField pnd_Lines_Per_Page;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Table_Id_Entry_Cde;

    private DbsGroup pnd_Table_Id_Entry_Cde__R_Field_2;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Table_Id;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Table_Sub_Id;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Entry_Code;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Parm_Country_Code = parameters.newFieldInRecord("pnd_Parm_Country_Code", "#PARM-COUNTRY-CODE", FieldType.STRING, 2);
        pnd_Parm_Country_Code.setParameterOption(ParameterOption.ByReference);
        pnd_Parm_Description = parameters.newFieldInRecord("pnd_Parm_Description", "#PARM-DESCRIPTION", FieldType.STRING, 25);
        pnd_Parm_Description.setParameterOption(ParameterOption.ByReference);
        pnd_Parm_Return_Code = parameters.newFieldInRecord("pnd_Parm_Return_Code", "#PARM-RETURN-CODE", FieldType.STRING, 4);
        pnd_Parm_Return_Code.setParameterOption(ParameterOption.ByReference);
        pnd_Parm_Return_Msg = parameters.newFieldInRecord("pnd_Parm_Return_Msg", "#PARM-RETURN-MSG", FieldType.STRING, 30);
        pnd_Parm_Return_Msg.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Country_Code = localVariables.newFieldArrayInRecord("pnd_Country_Code", "#COUNTRY-CODE", FieldType.STRING, 2, new DbsArrayController(1, 500));
        pnd_Description = localVariables.newFieldArrayInRecord("pnd_Description", "#DESCRIPTION", FieldType.STRING, 25, new DbsArrayController(1, 500));
        pnd_Line_Cv = localVariables.newFieldArrayInRecord("pnd_Line_Cv", "#LINE-CV", FieldType.ATTRIBUTE_CONTROL, 2, new DbsArrayController(1, 500));

        vw_nst_Table_Entry = new DataAccessProgramView(new NameInfo("vw_nst_Table_Entry", "NST-TABLE-ENTRY"), "NST_TABLE_ENTRY", "NST_TABLE_ENTRY");
        nst_Table_Entry_Entry_Table_Id_Nbr = vw_nst_Table_Entry.getRecord().newFieldInGroup("nst_Table_Entry_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "ENTRY_TABLE_ID_NBR");
        nst_Table_Entry_Entry_Table_Id_Nbr.setDdmHeader("TABLE ID");
        nst_Table_Entry_Entry_Table_Sub_Id = vw_nst_Table_Entry.getRecord().newFieldInGroup("nst_Table_Entry_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ENTRY_TABLE_SUB_ID");
        nst_Table_Entry_Entry_Cde = vw_nst_Table_Entry.getRecord().newFieldInGroup("nst_Table_Entry_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "ENTRY_CDE");
        nst_Table_Entry_Entry_Cde.setDdmHeader("ENTRY CODE");
        nst_Table_Entry_Entry_Dscrptn_Txt = vw_nst_Table_Entry.getRecord().newFieldInGroup("nst_Table_Entry_Entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "ENTRY_DSCRPTN_TXT");

        nst_Table_Entry__R_Field_1 = vw_nst_Table_Entry.getRecord().newGroupInGroup("nst_Table_Entry__R_Field_1", "REDEFINE", nst_Table_Entry_Entry_Dscrptn_Txt);
        nst_Table_Entry_Pnd_Description_25 = nst_Table_Entry__R_Field_1.newFieldInGroup("nst_Table_Entry_Pnd_Description_25", "#DESCRIPTION-25", FieldType.STRING, 
            25);
        registerRecord(vw_nst_Table_Entry);

        pnd_Start = localVariables.newFieldInRecord("pnd_Start", "#START", FieldType.NUMERIC, 7);
        pnd_Total_Pages = localVariables.newFieldInRecord("pnd_Total_Pages", "#TOTAL-PAGES", FieldType.NUMERIC, 2);
        pnd_Lines_Per_Page = localVariables.newFieldInRecord("pnd_Lines_Per_Page", "#LINES-PER-PAGE", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Table_Id_Entry_Cde = localVariables.newFieldInRecord("pnd_Table_Id_Entry_Cde", "#TABLE-ID-ENTRY-CDE", FieldType.STRING, 28);

        pnd_Table_Id_Entry_Cde__R_Field_2 = localVariables.newGroupInRecord("pnd_Table_Id_Entry_Cde__R_Field_2", "REDEFINE", pnd_Table_Id_Entry_Cde);
        pnd_Table_Id_Entry_Cde_Pnd_Table_Id = pnd_Table_Id_Entry_Cde__R_Field_2.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Table_Id", "#TABLE-ID", FieldType.NUMERIC, 
            6);
        pnd_Table_Id_Entry_Cde_Pnd_Table_Sub_Id = pnd_Table_Id_Entry_Cde__R_Field_2.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Table_Sub_Id", "#TABLE-SUB-ID", 
            FieldType.STRING, 2);
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Code = pnd_Table_Id_Entry_Cde__R_Field_2.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Entry_Code", "#ENTRY-CODE", 
            FieldType.STRING, 20);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_nst_Table_Entry.reset();

        parameters.reset();
        localVariables.reset();
        pnd_Start.setInitialValue(1);
        pnd_Total_Pages.setInitialValue(0);
        pnd_Lines_Per_Page.setInitialValue(10);
        pnd_I.setInitialValue(0);
        pnd_J.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Scin3050() throws Exception
    {
        super("Scin3050");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Code.reset();                                                                                                                    //Natural: RESET #ENTRY-CODE
        pnd_Parm_Description.reset();                                                                                                                                     //Natural: RESET #PARM-DESCRIPTION
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Code.setValue(pnd_Parm_Country_Code);                                                                                            //Natural: ASSIGN #ENTRY-CODE := #PARM-COUNTRY-CODE
                                                                                                                                                                          //Natural: PERFORM READ-COUNTRY-TABLE
        sub_Read_Country_Table();
        if (condition(Global.isEscape())) {return;}
        //* ****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-COUNTRY-TABLE
    }
    private void sub_Read_Country_Table() throws Exception                                                                                                                //Natural: READ-COUNTRY-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************************************
        pnd_I.resetInitial();                                                                                                                                             //Natural: RESET INITIAL #I #PARM-RETURN-CODE #PARM-RETURN-MSG
        pnd_Parm_Return_Code.resetInitial();
        pnd_Parm_Return_Msg.resetInitial();
        pnd_Table_Id_Entry_Cde_Pnd_Table_Id.setValue(19);                                                                                                                 //Natural: ASSIGN #TABLE-ID := 19
        pnd_Table_Id_Entry_Cde_Pnd_Table_Sub_Id.setValue("RC");                                                                                                           //Natural: ASSIGN #TABLE-SUB-ID := 'RC'
        vw_nst_Table_Entry.startDatabaseRead                                                                                                                              //Natural: READ NST-TABLE-ENTRY BY TABLE-ID-ENTRY-CDE STARTING FROM #TABLE-ID-ENTRY-CDE
        (
        "READ01",
        new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", ">=", pnd_Table_Id_Entry_Cde, WcType.BY) },
        new Oc[] { new Oc("TABLE_ID_ENTRY_CDE", "ASC") }
        );
        READ01:
        while (condition(vw_nst_Table_Entry.readNextRow("READ01")))
        {
            //*  STATES CODES BEGIN WITH 00
            if (condition(nst_Table_Entry_Entry_Table_Id_Nbr.notEquals(pnd_Table_Id_Entry_Cde_Pnd_Table_Id) || nst_Table_Entry_Entry_Table_Sub_Id.notEquals(pnd_Table_Id_Entry_Cde_Pnd_Table_Sub_Id)  //Natural: IF NST-TABLE-ENTRY.ENTRY-TABLE-ID-NBR NE #TABLE-ID OR ENTRY-TABLE-SUB-ID NE #TABLE-SUB-ID OR ENTRY-CDE GE '0'
                || nst_Table_Entry_Entry_Cde.greaterOrEqual("0")))
            {
                pnd_Parm_Return_Code.setValue("1111");                                                                                                                    //Natural: ASSIGN #PARM-RETURN-CODE := '1111'
                pnd_Parm_Return_Msg.setValue("ENTRY CODE IS ZERO");                                                                                                       //Natural: ASSIGN #PARM-RETURN-MSG := 'ENTRY CODE IS ZERO'
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Table_Id_Entry_Cde_Pnd_Entry_Code.equals(nst_Table_Entry_Entry_Cde)))                                                                       //Natural: IF #ENTRY-CODE = NST-TABLE-ENTRY.ENTRY-CDE
            {
                pnd_Parm_Description.setValue(nst_Table_Entry_Pnd_Description_25);                                                                                        //Natural: ASSIGN #PARM-DESCRIPTION := #DESCRIPTION-25
                pnd_Parm_Return_Code.setValue("0");                                                                                                                       //Natural: ASSIGN #PARM-RETURN-CODE := '0'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  READ-COUNTRY-TABLE
    }

    //
}
