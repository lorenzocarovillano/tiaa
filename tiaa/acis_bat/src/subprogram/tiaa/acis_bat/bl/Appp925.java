/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:59:18 PM
**        * FROM NATURAL SUBPROGRAM : Appp925
************************************************************
**        * FILE NAME            : Appp925.java
**        * CLASS NAME           : Appp925
**        * INSTANCE NAME        : Appp925
************************************************************
*  SUBPROGRAM NAME: APPP925
*
**----------------------------------------------------------------------
*  UPDATED: 04/09/97 V.C.RAQUENO - ADD NEW CONTRACT RANGES      (VR1)
*           01/25/99 L SHU       - ADD NEW CONTRACT RANGES      (LS1)
*           02/22/99 L SHU       - ADD NEW CONTRACT RANGES &    (LS2)
*                                  TYPE  'KEOGH'
*           03/21/00 L SHU       - ADD NEW CONTRACT RANGES      (LS3)
*           12/22/04 R WILLIS    - ADD NEW PRODUCTS FOR TNT/REL4. (RW1)
*           12/22/04 R WILLIS    - ADD NEW PRODUCTS FOR TNT/REL4. (RW1)
*           04/22/05 R WILLIS    - ADD NEW PRODUCTS FOR TNT/REL5. (RW2)
*           09/09/06 D MARPURI   - ADD NEW PRODUCTS FOR IRA/REL6. (RL6)
*           10/05/06 K GATES     - ADD NEW PRODUCTS FOR DCA    (DCA KG)
*           09/10/08 A RONDEAU   - RHSP MODIFICATIONS - SCAN "AER"
*           03/12/10 R SACHARNY  - PROCESS NEW PRODUCTS RS0
**----------------------------------------------------------------------
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appp925 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAppa925 pdaAppa925;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField num_Prod_Recs;
    private DbsField coll_St_Cd;
    private DbsField sux;
    private DbsField sub;

    private DbsGroup product_Table;
    private DbsField product_Table_R1;
    private DbsField product_Table_R2;
    private DbsField product_Table_R3;
    private DbsField product_Table_R4;
    private DbsField product_Table_R5;
    private DbsField product_Table_R6q;
    private DbsField product_Table_R7;
    private DbsField product_Table_R8;
    private DbsField product_Table_R9m;
    private DbsField product_Table_R10;
    private DbsField product_Table_R10a;
    private DbsField product_Table_R11;
    private DbsField product_Table_R12;
    private DbsField product_Table_R13;
    private DbsField product_Table_R14;
    private DbsField product_Table_R15;
    private DbsField product_Table_R16;
    private DbsField product_Table_R17;
    private DbsField product_Table_R18;
    private DbsField product_Table_R19;
    private DbsField product_Table_R20;
    private DbsField product_Table_R21m;
    private DbsField product_Table_R22;
    private DbsField product_Table_R23m;
    private DbsField product_Table_R24;
    private DbsField product_Table_R24a;
    private DbsField product_Table_R24b;
    private DbsField product_Table_R24c;
    private DbsField product_Table_R25;
    private DbsField product_Table_R26;
    private DbsField product_Table_R27;
    private DbsField product_Table_R28;
    private DbsField product_Table_R29;
    private DbsField product_Table_R30;
    private DbsField product_Table_R31;
    private DbsField product_Table_R32;
    private DbsField product_Table_R33;
    private DbsField product_Table_R38;
    private DbsField product_Table_R39;
    private DbsField product_Table_R40;
    private DbsField product_Table_R41;
    private DbsField product_Table_R42;
    private DbsField product_Table_R43;
    private DbsField product_Table_R44;

    private DbsGroup product_Table__R_Field_1;
    private DbsField product_Table_Product_Fields;

    private DbsGroup product_Table__R_Field_2;

    private DbsGroup product_Table_Contract_Fields;
    private DbsField product_Table_Contract_Type;
    private DbsField product_Table_Contract_Plus;
    private DbsField product_Table_Product_Lob;
    private DbsField product_Table_Product_Lob_Type;
    private DbsField product_Table_Product_Code;
    private DbsField product_Table_Filler;
    private DbsField product_Table_Contract_Start;

    private DbsGroup product_Table__R_Field_3;
    private DbsField product_Table_Contract_Start_Pfx;
    private DbsField product_Table_Contract_Start_Num;

    private DbsGroup product_Table__R_Field_4;
    private DbsField product_Table_Contract_Start_Pfx2;

    private DbsGroup product_Table__R_Field_5;
    private DbsField product_Table_Contract_Start_Pfx2_1;
    private DbsField product_Table_Contract_Start_Pfx2_2;
    private DbsField product_Table_Contract_Start_Num5;
    private DbsField product_Table_Contract_Start_Pfx_C;
    private DbsField product_Table_Contract_End;

    private DbsGroup product_Table__R_Field_6;
    private DbsField product_Table_Contract_End_Pfx;
    private DbsField product_Table_Contract_End_Num;

    private DbsGroup product_Table__R_Field_7;
    private DbsField product_Table_Contract_End_Pfx2;
    private DbsField product_Table_Contract_End_Num5;
    private DbsField product_Table_Contract_End_Pfx_C;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaAppa925 = new PdaAppa925(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        num_Prod_Recs = localVariables.newFieldInRecord("num_Prod_Recs", "NUM-PROD-RECS", FieldType.NUMERIC, 4);
        coll_St_Cd = localVariables.newFieldInRecord("coll_St_Cd", "COLL-ST-CD", FieldType.STRING, 2);
        sux = localVariables.newFieldInRecord("sux", "SUX", FieldType.NUMERIC, 2);
        sub = localVariables.newFieldInRecord("sub", "SUB", FieldType.NUMERIC, 2);

        product_Table = localVariables.newGroupInRecord("product_Table", "PRODUCT-TABLE");
        product_Table_R1 = product_Table.newFieldInGroup("product_Table_R1", "R1", FieldType.STRING, 44);
        product_Table_R2 = product_Table.newFieldInGroup("product_Table_R2", "R2", FieldType.STRING, 44);
        product_Table_R3 = product_Table.newFieldInGroup("product_Table_R3", "R3", FieldType.STRING, 44);
        product_Table_R4 = product_Table.newFieldInGroup("product_Table_R4", "R4", FieldType.STRING, 44);
        product_Table_R5 = product_Table.newFieldInGroup("product_Table_R5", "R5", FieldType.STRING, 44);
        product_Table_R6q = product_Table.newFieldInGroup("product_Table_R6q", "R6Q", FieldType.STRING, 44);
        product_Table_R7 = product_Table.newFieldInGroup("product_Table_R7", "R7", FieldType.STRING, 44);
        product_Table_R8 = product_Table.newFieldInGroup("product_Table_R8", "R8", FieldType.STRING, 44);
        product_Table_R9m = product_Table.newFieldInGroup("product_Table_R9m", "R9M", FieldType.STRING, 44);
        product_Table_R10 = product_Table.newFieldInGroup("product_Table_R10", "R10", FieldType.STRING, 44);
        product_Table_R10a = product_Table.newFieldInGroup("product_Table_R10a", "R10A", FieldType.STRING, 44);
        product_Table_R11 = product_Table.newFieldInGroup("product_Table_R11", "R11", FieldType.STRING, 44);
        product_Table_R12 = product_Table.newFieldInGroup("product_Table_R12", "R12", FieldType.STRING, 44);
        product_Table_R13 = product_Table.newFieldInGroup("product_Table_R13", "R13", FieldType.STRING, 44);
        product_Table_R14 = product_Table.newFieldInGroup("product_Table_R14", "R14", FieldType.STRING, 44);
        product_Table_R15 = product_Table.newFieldInGroup("product_Table_R15", "R15", FieldType.STRING, 44);
        product_Table_R16 = product_Table.newFieldInGroup("product_Table_R16", "R16", FieldType.STRING, 44);
        product_Table_R17 = product_Table.newFieldInGroup("product_Table_R17", "R17", FieldType.STRING, 44);
        product_Table_R18 = product_Table.newFieldInGroup("product_Table_R18", "R18", FieldType.STRING, 44);
        product_Table_R19 = product_Table.newFieldInGroup("product_Table_R19", "R19", FieldType.STRING, 44);
        product_Table_R20 = product_Table.newFieldInGroup("product_Table_R20", "R20", FieldType.STRING, 44);
        product_Table_R21m = product_Table.newFieldInGroup("product_Table_R21m", "R21M", FieldType.STRING, 44);
        product_Table_R22 = product_Table.newFieldInGroup("product_Table_R22", "R22", FieldType.STRING, 44);
        product_Table_R23m = product_Table.newFieldInGroup("product_Table_R23m", "R23M", FieldType.STRING, 44);
        product_Table_R24 = product_Table.newFieldInGroup("product_Table_R24", "R24", FieldType.STRING, 44);
        product_Table_R24a = product_Table.newFieldInGroup("product_Table_R24a", "R24A", FieldType.STRING, 44);
        product_Table_R24b = product_Table.newFieldInGroup("product_Table_R24b", "R24B", FieldType.STRING, 44);
        product_Table_R24c = product_Table.newFieldInGroup("product_Table_R24c", "R24C", FieldType.STRING, 44);
        product_Table_R25 = product_Table.newFieldInGroup("product_Table_R25", "R25", FieldType.STRING, 44);
        product_Table_R26 = product_Table.newFieldInGroup("product_Table_R26", "R26", FieldType.STRING, 44);
        product_Table_R27 = product_Table.newFieldInGroup("product_Table_R27", "R27", FieldType.STRING, 44);
        product_Table_R28 = product_Table.newFieldInGroup("product_Table_R28", "R28", FieldType.STRING, 44);
        product_Table_R29 = product_Table.newFieldInGroup("product_Table_R29", "R29", FieldType.STRING, 44);
        product_Table_R30 = product_Table.newFieldInGroup("product_Table_R30", "R30", FieldType.STRING, 44);
        product_Table_R31 = product_Table.newFieldInGroup("product_Table_R31", "R31", FieldType.STRING, 44);
        product_Table_R32 = product_Table.newFieldInGroup("product_Table_R32", "R32", FieldType.STRING, 44);
        product_Table_R33 = product_Table.newFieldInGroup("product_Table_R33", "R33", FieldType.STRING, 44);
        product_Table_R38 = product_Table.newFieldInGroup("product_Table_R38", "R38", FieldType.STRING, 44);
        product_Table_R39 = product_Table.newFieldInGroup("product_Table_R39", "R39", FieldType.STRING, 44);
        product_Table_R40 = product_Table.newFieldInGroup("product_Table_R40", "R40", FieldType.STRING, 44);
        product_Table_R41 = product_Table.newFieldInGroup("product_Table_R41", "R41", FieldType.STRING, 44);
        product_Table_R42 = product_Table.newFieldInGroup("product_Table_R42", "R42", FieldType.STRING, 44);
        product_Table_R43 = product_Table.newFieldInGroup("product_Table_R43", "R43", FieldType.STRING, 44);
        product_Table_R44 = product_Table.newFieldInGroup("product_Table_R44", "R44", FieldType.STRING, 44);

        product_Table__R_Field_1 = localVariables.newGroupInRecord("product_Table__R_Field_1", "REDEFINE", product_Table);
        product_Table_Product_Fields = product_Table__R_Field_1.newFieldArrayInGroup("product_Table_Product_Fields", "PRODUCT-FIELDS", FieldType.STRING, 
            44, new DbsArrayController(1, 43));

        product_Table__R_Field_2 = localVariables.newGroupInRecord("product_Table__R_Field_2", "REDEFINE", product_Table);

        product_Table_Contract_Fields = product_Table__R_Field_2.newGroupArrayInGroup("product_Table_Contract_Fields", "CONTRACT-FIELDS", new DbsArrayController(1, 
            43));
        product_Table_Contract_Type = product_Table_Contract_Fields.newFieldInGroup("product_Table_Contract_Type", "CONTRACT-TYPE", FieldType.STRING, 
            10);
        product_Table_Contract_Plus = product_Table_Contract_Fields.newFieldInGroup("product_Table_Contract_Plus", "CONTRACT-PLUS", FieldType.STRING, 
            10);
        product_Table_Product_Lob = product_Table_Contract_Fields.newFieldInGroup("product_Table_Product_Lob", "PRODUCT-LOB", FieldType.STRING, 1);
        product_Table_Product_Lob_Type = product_Table_Contract_Fields.newFieldInGroup("product_Table_Product_Lob_Type", "PRODUCT-LOB-TYPE", FieldType.STRING, 
            2);
        product_Table_Product_Code = product_Table_Contract_Fields.newFieldInGroup("product_Table_Product_Code", "PRODUCT-CODE", FieldType.STRING, 4);
        product_Table_Filler = product_Table_Contract_Fields.newFieldInGroup("product_Table_Filler", "FILLER", FieldType.STRING, 1);
        product_Table_Contract_Start = product_Table_Contract_Fields.newFieldInGroup("product_Table_Contract_Start", "CONTRACT-START", FieldType.STRING, 
            7);

        product_Table__R_Field_3 = product_Table_Contract_Fields.newGroupInGroup("product_Table__R_Field_3", "REDEFINE", product_Table_Contract_Start);
        product_Table_Contract_Start_Pfx = product_Table__R_Field_3.newFieldInGroup("product_Table_Contract_Start_Pfx", "CONTRACT-START-PFX", FieldType.STRING, 
            1);
        product_Table_Contract_Start_Num = product_Table__R_Field_3.newFieldInGroup("product_Table_Contract_Start_Num", "CONTRACT-START-NUM", FieldType.STRING, 
            6);

        product_Table__R_Field_4 = product_Table_Contract_Fields.newGroupInGroup("product_Table__R_Field_4", "REDEFINE", product_Table_Contract_Start);
        product_Table_Contract_Start_Pfx2 = product_Table__R_Field_4.newFieldInGroup("product_Table_Contract_Start_Pfx2", "CONTRACT-START-PFX2", FieldType.STRING, 
            2);

        product_Table__R_Field_5 = product_Table__R_Field_4.newGroupInGroup("product_Table__R_Field_5", "REDEFINE", product_Table_Contract_Start_Pfx2);
        product_Table_Contract_Start_Pfx2_1 = product_Table__R_Field_5.newFieldInGroup("product_Table_Contract_Start_Pfx2_1", "CONTRACT-START-PFX2-1", 
            FieldType.STRING, 1);
        product_Table_Contract_Start_Pfx2_2 = product_Table__R_Field_5.newFieldInGroup("product_Table_Contract_Start_Pfx2_2", "CONTRACT-START-PFX2-2", 
            FieldType.STRING, 1);
        product_Table_Contract_Start_Num5 = product_Table__R_Field_4.newFieldInGroup("product_Table_Contract_Start_Num5", "CONTRACT-START-NUM5", FieldType.STRING, 
            5);
        product_Table_Contract_Start_Pfx_C = product_Table_Contract_Fields.newFieldInGroup("product_Table_Contract_Start_Pfx_C", "CONTRACT-START-PFX-C", 
            FieldType.STRING, 1);
        product_Table_Contract_End = product_Table_Contract_Fields.newFieldInGroup("product_Table_Contract_End", "CONTRACT-END", FieldType.STRING, 7);

        product_Table__R_Field_6 = product_Table_Contract_Fields.newGroupInGroup("product_Table__R_Field_6", "REDEFINE", product_Table_Contract_End);
        product_Table_Contract_End_Pfx = product_Table__R_Field_6.newFieldInGroup("product_Table_Contract_End_Pfx", "CONTRACT-END-PFX", FieldType.STRING, 
            1);
        product_Table_Contract_End_Num = product_Table__R_Field_6.newFieldInGroup("product_Table_Contract_End_Num", "CONTRACT-END-NUM", FieldType.STRING, 
            6);

        product_Table__R_Field_7 = product_Table_Contract_Fields.newGroupInGroup("product_Table__R_Field_7", "REDEFINE", product_Table_Contract_End);
        product_Table_Contract_End_Pfx2 = product_Table__R_Field_7.newFieldInGroup("product_Table_Contract_End_Pfx2", "CONTRACT-END-PFX2", FieldType.STRING, 
            2);
        product_Table_Contract_End_Num5 = product_Table__R_Field_7.newFieldInGroup("product_Table_Contract_End_Num5", "CONTRACT-END-NUM5", FieldType.STRING, 
            5);
        product_Table_Contract_End_Pfx_C = product_Table_Contract_Fields.newFieldInGroup("product_Table_Contract_End_Pfx_C", "CONTRACT-END-PFX-C", FieldType.STRING, 
            1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        num_Prod_Recs.setInitialValue(43);
        product_Table_R1.setInitialValue("RA        PRE 41    D01RA   A000001PA031624P");
        product_Table_R2.setInitialValue("RA        POST 41   D02RA   A031625PA899999P");
        product_Table_R3.setInitialValue("RA        CAN PEN   D03CANP A900000PA969999P");
        product_Table_R4.setInitialValue("RA        CAN SAV   D04CANS A970000PA989999P");
        product_Table_R5.setInitialValue("RA                  D02RA   B000001QB799999Q");
        product_Table_R6q.setInitialValue("RA        QVEC-DA   D08RAQV B800001QB801499Q");
        product_Table_R7.setInitialValue("RA                  D02RA   B801500QB999999Q");
        product_Table_R8.setInitialValue("RA                  D02RA   C010000UC799999U");
        product_Table_R9m.setInitialValue("RA        MDO       D02MDRA C800000UC873999U");
        product_Table_R10.setInitialValue("GRA       MDO       D02MDGR C874000UC899999U");
        product_Table_R10a.setInitialValue("RA                  D02RA   C900000UC999999U");
        product_Table_R11.setInitialValue("RA                  D02RA   D010000VD999999V");
        product_Table_R12.setInitialValue("RA                  D02RA   E010000XE999999X");
        product_Table_R13.setInitialValue("GRA                 D07GRA  2000001120099991");
        product_Table_R14.setInitialValue("GRA       II        D09GRA2 2010000120249991");
        product_Table_R15.setInitialValue("GRA                 D07GRA  2025000128999991");
        product_Table_R16.setInitialValue("GRA       LGA       D06LGA  2900000129999991");
        product_Table_R17.setInitialValue("SRA                 S02SRA  K000000JK499999J");
        product_Table_R18.setInitialValue("GSRA                S03GSRA K500000JK799999J");
        product_Table_R19.setInitialValue("SRA       QVEC      S08SRAQ K800000JK809999J");
        product_Table_R20.setInitialValue("SRA       SPECIAL   S02SSRA K810000 K829999 ");
        product_Table_R21m.setInitialValue("IRA       MDO       S02MDIR K830000JK849999J");
        product_Table_R22.setInitialValue("SRA       MDO       S02MDSR K850000JK885999J");
        product_Table_R23m.setInitialValue("GSRA      MDO       S02MDGS K886000JK899999J");
        product_Table_R24.setInitialValue("IRA       ROLLOVER  S02IRA  K900000JK999999J");
        product_Table_R24a.setInitialValue("GSRA                S03GSRA L000010ML849999M");
        product_Table_R24b.setInitialValue("KEOGH               I05KEOG N000010TN199999T");
        product_Table_R24c.setInitialValue("GRA                 D07GRA  3000010439499994");
        product_Table_R25.setInitialValue("IRACLAS R CLASSIC   I04IRAC N700000TN999999T");
        product_Table_R26.setInitialValue("IRACLAS C CLASSIC   I04IRAC N700000TN999999T");
        product_Table_R27.setInitialValue("IRAROTH R ROTH      I03IRAR N200000TN699999T");
        product_Table_R28.setInitialValue("IRAROTH C ROTH      I03IRAR N200000TN599999T");
        product_Table_R29.setInitialValue("SRA                 S02SRA  L900000ML989999M");
        product_Table_R30.setInitialValue("SRA       MDO       S02MDSR L990000ML999999M");
        product_Table_R31.setInitialValue("GA        GA CO     S04GA   L850000ML899999M");
        product_Table_R32.setInitialValue("RC                  D ARC   F000000 F499999 ");
        product_Table_R33.setInitialValue("RCP                 S07RCP  F500000 F749999 ");
        product_Table_R38.setInitialValue("IRA SEP             I06IRAS N600000TN699999T");
        product_Table_R39.setInitialValue("TGA                 S09TGA  3950000439998994");
        product_Table_R40.setInitialValue("RHSP                DB RHSP W100000WW499999W");
        product_Table_R41.setInitialValue("IRIC      IRIC      I08IRIC NE00000TNF99999T");
        product_Table_R42.setInitialValue("IRIR      IRIR      I07IRIR NA00000TNB99999T");
        product_Table_R43.setInitialValue("IRIS      IRIS      I09IRIS NC00000TNC99999T");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Appp925() throws Exception
    {
        super("Appp925");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  ------------PRODUCT TABLE SEARCH------------------------
        //*  IF THE SECOND CHAR OF CONTRACT IS ALPHA (INDXD PRODUCT)- CHECK
        //*  THE FIRST 2 CHAR AGAINST THE TABLE ELSE DO THE REGULAR CHECK
        pdaAppa925.getContract_Info().reset();                                                                                                                            //Natural: RESET CONTRACT-INFO
        //*  RS0
        if (condition(DbsUtil.maskMatches(pdaAppa925.getContract_Contract_Pfx2(),".A")))                                                                                  //Natural: IF CONTRACT-PFX2 = MASK ( .A )
        {
            FOR01:                                                                                                                                                        //Natural: FOR SUB 1 TO NUM-PROD-RECS
            for (sub.setValue(1); condition(sub.lessOrEqual(num_Prod_Recs)); sub.nadd(1))
            {
                if (condition((product_Table_Contract_Start_Pfx2.getValue(sub).equals(pdaAppa925.getContract_Contract_Pfx2()) || (product_Table_Contract_Start_Pfx_C.getValue(sub).equals(pdaAppa925.getContract_Contract_Pfx())  //Natural: IF ( CONTRACT-START-PFX2 ( SUB ) = CONTRACT-PFX2 OR ( CONTRACT-START-PFX-C ( SUB ) = CONTRACT-PFX AND CONTRACT-START-PFX2-2 ( SUB ) = CONTRACT-PFX2-2 ) )
                    && product_Table_Contract_Start_Pfx2_2.getValue(sub).equals(pdaAppa925.getContract_Contract_Pfx2_2())))))
                {
                    if (condition((pdaAppa925.getContract_Contract_Num5().greaterOrEqual(product_Table_Contract_Start_Num5.getValue(sub)) && pdaAppa925.getContract_Contract_Num5().lessOrEqual(product_Table_Contract_End_Num5.getValue(sub))))) //Natural: IF ( CONTRACT-NUM5 GE CONTRACT-START-NUM5 ( SUB ) AND CONTRACT-NUM5 LE CONTRACT-END-NUM5 ( SUB ) )
                    {
                        pdaAppa925.getContract_Info().setValue(product_Table_Product_Fields.getValue(sub));                                                               //Natural: MOVE PRODUCT-FIELDS ( SUB ) TO CONTRACT-INFO
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR02:                                                                                                                                                        //Natural: FOR SUB 1 TO NUM-PROD-RECS
            for (sub.setValue(1); condition(sub.lessOrEqual(num_Prod_Recs)); sub.nadd(1))
            {
                if (condition((product_Table_Contract_Start_Pfx.getValue(sub).equals(pdaAppa925.getContract_Contract_Pfx()) || product_Table_Contract_Start_Pfx_C.getValue(sub).equals(pdaAppa925.getContract_Contract_Pfx())))) //Natural: IF ( CONTRACT-START-PFX ( SUB ) = CONTRACT-PFX OR CONTRACT-START-PFX-C ( SUB ) = CONTRACT-PFX )
                {
                    if (condition((pdaAppa925.getContract_Contract_Num().greaterOrEqual(product_Table_Contract_Start_Num.getValue(sub)) && pdaAppa925.getContract_Contract_Num().lessOrEqual(product_Table_Contract_End_Num.getValue(sub))))) //Natural: IF ( CONTRACT-NUM GE CONTRACT-START-NUM ( SUB ) AND CONTRACT-NUM LE CONTRACT-END-NUM ( SUB ) )
                    {
                        pdaAppa925.getContract_Info().setValue(product_Table_Product_Fields.getValue(sub));                                                               //Natural: MOVE PRODUCT-FIELDS ( SUB ) TO CONTRACT-INFO
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  -------------------------------------------------------------
    }

    //
}
