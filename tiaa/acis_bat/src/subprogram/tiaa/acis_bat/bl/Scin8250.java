/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:09:55 AM
**        * FROM NATURAL SUBPROGRAM : Scin8250
************************************************************
**        * FILE NAME            : Scin8250.java
**        * CLASS NAME           : Scin8250
**        * INSTANCE NAME        : Scin8250
************************************************************
************************************************************************
* PROGRAM : SCIN8250
* SYSTEM  : SUBPROGRAM TO CALL THE COBOL PROGRAM TO RETURN PLAN INFO
* AUTHOR  : KRIS GATES/DENNIS MARPURI
* DATE    : JULY 2005
* PURPOSE : PASS COBOL PROGRAM PLAN, SUBPLAN, AND PART SSN AND RECEIVE
*         : CONTRIBUTION INFO FROM SUNGARD.
* HISTORY :
* MOD DATE    MOD BY     DESCRIPTION OF CHANGES
* --------   ----------  -----------------------------------------------
* 06-06-05   K GATES     COMMENTED OUT TEST WRITE STMT.
* 07-14-09   G GUERRERO  RECOMPILE FOR OMNI UPGRADE PROJECT - NEW VERSIO
*                        SCIA8250.
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Scin8250 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaScia8250 pdaScia8250;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField w01_Open_File_Sw;
    private int psg9070ReturnCode;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaScia8250 = new PdaScia8250(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        w01_Open_File_Sw = localVariables.newFieldInRecord("w01_Open_File_Sw", "W01-OPEN-FILE-SW", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Scin8250() throws Exception
    {
        super("Scin8250");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  WRITE 'BEFORE CALL IN SCIN8250 - DISPLAY VARIABLES'
        //*  / '=' PPT-REQUEST-TYPE
        //*  / '=' PPT-PLAN-NUM
        //*  / '=' PPT-PART-NUMBER
        //*  / '=' PPT-PART-SUB-PLAN
        //*  08/24/05
        psg9070ReturnCode = DbsUtil.callExternalProgram("PSG9070",pdaScia8250.getPpt_File_Open_Sw(),pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Request_Type(),                  //Natural: CALL 'PSG9070' USING PPT-FILE-OPEN-SW PPT-PART-FUND-REC
            pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Sent_Data(),pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Returned_Data(),pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Return_Msg_Parms(),
            pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Filler().getValue(1,":",2512));
        //*  WRITE 'IN SCIN8250 - DISPLAY VARIABLES - AFTER CALL TO PSG9070'
        //*  / '=' PPT-REQUEST-TYPE
        //*  / '** SENT-DATA **'
        //*  / '=' PPT-EOF-SW
        //*  / '=' PPT-PLAN-NUM
        //*  / '** PART-ID **'
        //*  / '=' PPT-PART-NUMBER
        //*  / '=' PPT-PART-SUB-PLAN
        //*  / '** RETURN DATA **'
        //*  / '=' PPT-FIRST-CONTRIB-DATE
        //*  / '=' PPT-LAST-CONTRIB-DATE
        //*  / '=' PPT-STATUS
        //*  / '=' PPT-NUM-FUNDS
        //*  / '=' PPT-RETURN-CODE
        //*  / '=' PPT-RETURN-MSG-MODULE
        //*  / '=' PPT-RETURN-ERROR-SW
        //*  / '=' PPT-RETURN-FILE-OPER
        //*  / '=' PPT-RETURN-MESSAGE
    }

    //
}
