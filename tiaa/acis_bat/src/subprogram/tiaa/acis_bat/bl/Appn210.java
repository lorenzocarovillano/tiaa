/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:59:03 PM
**        * FROM NATURAL SUBPROGRAM : Appn210
************************************************************
**        * FILE NAME            : Appn210.java
**        * CLASS NAME           : Appn210
**        * INSTANCE NAME        : Appn210
************************************************************
******************************************************************
* YR2000 COMPLIANT FIX APPLIED    -->L BALAN 02/19/98-TSTD ?/?/97*
******************************************************************
**Y2CHLB

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appn210 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAppa100 pdaAppa100;
    private LdaAppl185 ldaAppl185;
    private LdaAppl190 ldaAppl190;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_L;
    private DbsField pnd_M;
    private DbsField pnd_Tiaa_Ra_Suffix;
    private DbsField pnd_Cref_Ra_Suffix;
    private DbsField pnd_Tiaa_Sra_Suffix;
    private DbsField pnd_Cref_Sra_Suffix;
    private DbsField pnd_Y2_Ccyymmdd_Area1;
    private DbsField pnd_Y2_Ccyymmdd_Area2;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl185 = new LdaAppl185();
        registerRecord(ldaAppl185);
        ldaAppl190 = new LdaAppl190();
        registerRecord(ldaAppl190);

        // parameters
        parameters = new DbsRecord();
        pdaAppa100 = new PdaAppa100(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.NUMERIC, 3);
        pnd_M = localVariables.newFieldInRecord("pnd_M", "#M", FieldType.NUMERIC, 3);
        pnd_Tiaa_Ra_Suffix = localVariables.newFieldInRecord("pnd_Tiaa_Ra_Suffix", "#TIAA-RA-SUFFIX", FieldType.STRING, 1);
        pnd_Cref_Ra_Suffix = localVariables.newFieldInRecord("pnd_Cref_Ra_Suffix", "#CREF-RA-SUFFIX", FieldType.STRING, 1);
        pnd_Tiaa_Sra_Suffix = localVariables.newFieldInRecord("pnd_Tiaa_Sra_Suffix", "#TIAA-SRA-SUFFIX", FieldType.STRING, 1);
        pnd_Cref_Sra_Suffix = localVariables.newFieldInRecord("pnd_Cref_Sra_Suffix", "#CREF-SRA-SUFFIX", FieldType.STRING, 1);
        pnd_Y2_Ccyymmdd_Area1 = localVariables.newFieldInRecord("pnd_Y2_Ccyymmdd_Area1", "#Y2-CCYYMMDD-AREA1", FieldType.NUMERIC, 8);
        pnd_Y2_Ccyymmdd_Area2 = localVariables.newFieldInRecord("pnd_Y2_Ccyymmdd_Area2", "#Y2-CCYYMMDD-AREA2", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl185.initializeValues();
        ldaAppl190.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Appn210() throws Exception
    {
        super("Appn210");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Tiaa_Ra_Suffix.reset();                                                                                                                                       //Natural: RESET #TIAA-RA-SUFFIX #CREF-SRA-SUFFIX #TIAA-RA-SUFFIX #CREF-SRA-SUFFIX #PAR-GRM-TIAA-FORM-NUM #PAR-GRM-CREF-FORM-NUM
        pnd_Cref_Sra_Suffix.reset();
        pnd_Tiaa_Ra_Suffix.reset();
        pnd_Cref_Sra_Suffix.reset();
        pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Tiaa_Form_Num().reset();
        pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Cref_Form_Num().reset();
        FOR01:                                                                                                                                                            //Natural: FOR #I = 10 TO 1 STEP -1
        for (pnd_I.setValue(10); condition(pnd_I.greaterOrEqual(1)); pnd_I.nsubtract(1))
        {
            //* *Y2NCLB
            if (condition(ldaAppl185.getPnd_Grm_Table_Pnd_Grm_State_Eff_Date().getValue(pnd_I).notEquals(getZero())))                                                     //Natural: IF #GRM-STATE-EFF-DATE ( #I ) NE 0
            {
                //* ******  Y2K FIX START 02/21/98
                pnd_Y2_Ccyymmdd_Area1.setValue(pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Tiaa_Doi());                                                                  //Natural: ASSIGN #Y2-CCYYMMDD-AREA1 := #PAR-TIAA-DOI
                pnd_Y2_Ccyymmdd_Area2.setValue(ldaAppl185.getPnd_Grm_Table_Pnd_Grm_State_Eff_Date().getValue(pnd_I));                                                     //Natural: ASSIGN #Y2-CCYYMMDD-AREA2 := #GRM-STATE-EFF-DATE ( #I )
                DbsUtil.callnat(Y2dat1bn.class , getCurrentProcessState(), pnd_Y2_Ccyymmdd_Area1, pnd_Y2_Ccyymmdd_Area2);                                                 //Natural: CALLNAT 'Y2DAT1BN' #Y2-CCYYMMDD-AREA1 #Y2-CCYYMMDD-AREA2
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                //* *Y2CHLB
                if (condition(pnd_Y2_Ccyymmdd_Area1.greaterOrEqual(pnd_Y2_Ccyymmdd_Area2)))                                                                               //Natural: IF #Y2-CCYYMMDD-AREA1 GE #Y2-CCYYMMDD-AREA2
                {
                    //* *  IF #PAR-TIAA-DOI GE #GRM-STATE-EFF-DATE(#I)
                    //* ******  Y2K FIX END
                    DbsUtil.examine(new ExamineSource(ldaAppl190.getPnd_Grm_State_Table_Pnd_Grm_State_Cd().getValue("*")), new ExamineSearch(pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_State_Cd()),  //Natural: EXAMINE #GRM-STATE-CD ( * ) FOR #PAR-GRM-STATE-CD GIVING INDEX #L
                        new ExamineGivingIndex(pnd_L));
                    if (condition(pnd_L.greater(getZero())))                                                                                                              //Natural: IF #L GT 0
                    {
                        if (condition(pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Lob().equals("D")))                                                                //Natural: IF #PAR-GRM-LOB = 'D'
                        {
                            pnd_Tiaa_Ra_Suffix.setValue(ldaAppl190.getPnd_Grm_State_Table_Pnd_Grm_Form_Suffix().getValue(pnd_L,pnd_I).getSubstring(1,1));                 //Natural: MOVE SUBSTRING ( #GRM-FORM-SUFFIX ( #L,#I ) ,1,1 ) TO #TIAA-RA-SUFFIX
                            pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Tiaa_Form_Num().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,                     //Natural: COMPRESS #GRM-T-C-RA-FORM-NUM ( #I ) #TIAA-RA-SUFFIX INTO #PAR-GRM-TIAA-FORM-NUM LEAVING NO SPACE
                                ldaAppl185.getPnd_Grm_Table_Pnd_Grm_T_C_Ra_Form_Num().getValue(pnd_I), pnd_Tiaa_Ra_Suffix));
                            pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Return_Cd().setValue(1);                                                                     //Natural: MOVE 1 TO #PAR-GRM-RETURN-CD
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Lob().equals("S")))                                                            //Natural: IF #PAR-GRM-LOB = 'S'
                            {
                                pnd_Tiaa_Sra_Suffix.setValue(ldaAppl190.getPnd_Grm_State_Table_Pnd_Grm_Form_Suffix().getValue(pnd_L,pnd_I).getSubstring(2,                //Natural: MOVE SUBSTRING ( #GRM-FORM-SUFFIX ( #L,#I ) ,2,1 ) TO #TIAA-SRA-SUFFIX
                                    1));
                                pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Tiaa_Form_Num().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,                 //Natural: COMPRESS #GRM-T-C-SRA-FORM-NUM ( #I ) #TIAA-SRA-SUFFIX INTO #PAR-GRM-TIAA-FORM-NUM LEAVING NO SPACE
                                    ldaAppl185.getPnd_Grm_Table_Pnd_Grm_T_C_Sra_Form_Num().getValue(pnd_I), pnd_Tiaa_Sra_Suffix));
                                pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Return_Cd().setValue(1);                                                                 //Natural: MOVE 1 TO #PAR-GRM-RETURN-CD
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *
        FOR02:                                                                                                                                                            //Natural: FOR #I = 20 TO 11 STEP -1
        for (pnd_I.setValue(20); condition(pnd_I.greaterOrEqual(11)); pnd_I.nsubtract(1))
        {
            //* *Y2NCLB
            if (condition(ldaAppl185.getPnd_Grm_Table_Pnd_Grm_State_Eff_Date().getValue(pnd_I).notEquals(getZero())))                                                     //Natural: IF #GRM-STATE-EFF-DATE ( #I ) NE 0
            {
                //* ******  Y2K FIX START 02/21/98
                pnd_Y2_Ccyymmdd_Area1.setValue(pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Cref_Doi());                                                                  //Natural: ASSIGN #Y2-CCYYMMDD-AREA1 := #PAR-CREF-DOI
                pnd_Y2_Ccyymmdd_Area2.setValue(ldaAppl185.getPnd_Grm_Table_Pnd_Grm_State_Eff_Date().getValue(pnd_I));                                                     //Natural: ASSIGN #Y2-CCYYMMDD-AREA2 := #GRM-STATE-EFF-DATE ( #I )
                DbsUtil.callnat(Y2dat1bn.class , getCurrentProcessState(), pnd_Y2_Ccyymmdd_Area1, pnd_Y2_Ccyymmdd_Area2);                                                 //Natural: CALLNAT 'Y2DAT1BN' #Y2-CCYYMMDD-AREA1 #Y2-CCYYMMDD-AREA2
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                //* *Y2CHLB
                if (condition(pnd_Y2_Ccyymmdd_Area1.greaterOrEqual(pnd_Y2_Ccyymmdd_Area2)))                                                                               //Natural: IF #Y2-CCYYMMDD-AREA1 GE #Y2-CCYYMMDD-AREA2
                {
                    //* *  IF #PAR-CREF-DOI GE #GRM-STATE-EFF-DATE(#I)
                    //* ******  Y2K FIX END
                    DbsUtil.examine(new ExamineSource(ldaAppl190.getPnd_Grm_State_Table_Pnd_Grm_State_Cd().getValue("*")), new ExamineSearch(pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_State_Cd()),  //Natural: EXAMINE #GRM-STATE-CD ( * ) FOR #PAR-GRM-STATE-CD GIVING INDEX #L
                        new ExamineGivingIndex(pnd_L));
                    if (condition(pnd_L.greater(getZero())))                                                                                                              //Natural: IF #L GT 0
                    {
                        if (condition(pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Lob().equals("D")))                                                                //Natural: IF #PAR-GRM-LOB = 'D'
                        {
                            pnd_Cref_Ra_Suffix.setValue(ldaAppl190.getPnd_Grm_State_Table_Pnd_Grm_Form_Suffix().getValue(pnd_L,pnd_I).getSubstring(1,1));                 //Natural: MOVE SUBSTRING ( #GRM-FORM-SUFFIX ( #L,#I ) ,1,1 ) TO #CREF-RA-SUFFIX
                            pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Cref_Form_Num().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,                     //Natural: COMPRESS #GRM-T-C-RA-FORM-NUM ( #I ) #CREF-RA-SUFFIX INTO #PAR-GRM-CREF-FORM-NUM LEAVING NO SPACE
                                ldaAppl185.getPnd_Grm_Table_Pnd_Grm_T_C_Ra_Form_Num().getValue(pnd_I), pnd_Cref_Ra_Suffix));
                            pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Return_Cd().setValue(1);                                                                     //Natural: MOVE 1 TO #PAR-GRM-RETURN-CD
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Lob().equals("S")))                                                            //Natural: IF #PAR-GRM-LOB = 'S'
                            {
                                pnd_Cref_Sra_Suffix.setValue(ldaAppl190.getPnd_Grm_State_Table_Pnd_Grm_Form_Suffix().getValue(pnd_L,pnd_I).getSubstring(2,                //Natural: MOVE SUBSTRING ( #GRM-FORM-SUFFIX ( #L,#I ) ,2,1 ) TO #CREF-SRA-SUFFIX
                                    1));
                                pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Cref_Form_Num().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,                 //Natural: COMPRESS #GRM-T-C-SRA-FORM-NUM ( #I ) #CREF-SRA-SUFFIX INTO #PAR-GRM-CREF-FORM-NUM LEAVING NO SPACE
                                    ldaAppl185.getPnd_Grm_Table_Pnd_Grm_T_C_Sra_Form_Num().getValue(pnd_I), pnd_Cref_Sra_Suffix));
                                pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Return_Cd().setValue(1);                                                                 //Natural: MOVE 1 TO #PAR-GRM-RETURN-CD
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* * WRITE '=' #PAR-GRM-LOB
        //* *  /    '=' #PAR-GRM-STATE-CD
        //* *  /    '=' #PAR-TIAA-DOI
        //* *  /    '=' #PAR-CREF-DOI
        //* *  /    '=' #PAR-GRM-TIAA-FORM-NUM
        //* *  /    '=' #PAR-GRM-CREF-FORM-NUM
    }

    //
}
