/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:58:57 PM
**        * FROM NATURAL SUBPROGRAM : Appn1155
************************************************************
**        * FILE NAME            : Appn1155.java
**        * CLASS NAME           : Appn1155
**        * INSTANCE NAME        : Appn1155
************************************************************
**----------------------------------------------------------------------
*
* PROGRAM-ID    :  APPN1155 - COPY OF APPB6500
* DESCRIPTION   :  THIS PROGRAM WILL FORMAT THE FUNDS BY ASSET CLASS
*                  BY READING THE NEW EXTERNALIZATION FILES
*                  THIS PROGRAM WILL BE CALLED BY APPB1152 WHICH IS THE
*                  DIALOGUE FEED FORMAT PROGRAM.
* DATE WRITTEN  :  07/29/08
* AUTHOR        :  DEVELBISS
**----------------------------------------------------------------------
* MOD DATE    MOD BY     DESCRIPTION OF CHANGES
**--------   ----------  -----------------------------------------------
**06-20-14   L SHU       RESTOW FOR NEW EXTERNALISATION          (CREA)
**----------------------------------------------------------------------
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appn1155 extends BLNatBase
{
    // Data Areas
    private PdaPsta9200 pdaPsta9200;
    private LdaPstl9902 ldaPstl9902;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField p_Fund_Code;
    private DbsField p_Fund_Alloc_Pct;

    private DbsGroup p_Fund_Alloc_Pct__R_Field_1;
    private DbsField p_Fund_Alloc_Pct_P_Fund_Alloc_Pct_N;
    private DbsField p_Category;
    private DbsField p_Fund_Name;
    private DbsField p_Category_Cnt;
    private DbsField p_Return_Code;
    private DbsField p_Return_Msg;

    private DataAccessProgramView vw_new_Ext_Cntrl_Fnd_View;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Table_Cde;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Ticker_Symbol;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Fund_Family_Cde;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Fund_Style;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Fund_Size;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Fund_Nme;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Fund_Inception_Dte;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Fund_Cusip;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Category_Class_Cde;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Category_Rptng_Seq;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Category_Fund_Seq;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Alpha_Fund_Cde;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Num_Fund_Cde;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Reporting_Seq;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Unit_Rate_Ind;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Alpha_Fund_Seq;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_V1_Num_Fund_Cde;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_V1_Alpha_Fund_Cde;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Abbr_Factor_Key;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Act_Unit_Acct_Nm;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_V2_Alpha_Cde;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_V2_Rt_1;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_V2_Rt_2;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_V2_Rt_3;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Ivc_Company_Cde;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_V2_Active_Ind;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Dividend_Ind;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Rate_Basis_Points;
    private DbsGroup new_Ext_Cntrl_Fnd_View_Nec_Fund_SummaryMuGroup;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Fund_Summary;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Modified_By;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Modified_Dte;

    private DataAccessProgramView vw_new_Ext_Cntrl_Cat_View;
    private DbsField new_Ext_Cntrl_Cat_View_Nec_Table_Cde;
    private DbsField new_Ext_Cntrl_Cat_View_Nec_Category_Class_Cde;
    private DbsField new_Ext_Cntrl_Cat_View_Nec_Category_Class_Desc;
    private DbsField new_Ext_Cntrl_Cat_View_Nec_Category_Rptng_Seq;
    private DbsField new_Ext_Cntrl_Cat_View_Nec_Modified_By;
    private DbsField new_Ext_Cntrl_Cat_View_Nec_Modified_Dte;
    private DbsField new_Ext_Cntrl_Cat_View_Nec_Cat_Super;

    private DataAccessProgramView vw_app_Table_Entry;
    private DbsField app_Table_Entry_Entry_Actve_Ind;
    private DbsField app_Table_Entry_Entry_Table_Id_Nbr;
    private DbsField app_Table_Entry_Entry_Table_Sub_Id;
    private DbsField app_Table_Entry_Entry_Cde;
    private DbsField app_Table_Entry_Entry_Dscrptn_Txt;
    private DbsGroup app_Table_Entry_Addtnl_Dscrptn_TxtMuGroup;
    private DbsField app_Table_Entry_Addtnl_Dscrptn_Txt;

    private DataAccessProgramView vw_app_Table_Upd;
    private DbsField app_Table_Upd_Entry_Actve_Ind;
    private DbsField pnd_Table_Id_Entry_Cde;

    private DbsGroup pnd_Table_Id_Entry_Cde__R_Field_2;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde;
    private DbsField pnd_Nec_Fnd_Super1;

    private DbsGroup pnd_Nec_Fnd_Super1__R_Field_3;
    private DbsField pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde1;
    private DbsField pnd_Nec_Fnd_Super1_Pnd_Nec_Tkr_Symbl;
    private DbsField pnd_Nec_Fnd_Super5;

    private DbsGroup pnd_Nec_Fnd_Super5__R_Field_4;
    private DbsField pnd_Nec_Fnd_Super5_Pnd_Nec_Table_Cde;
    private DbsField pnd_Nec_Fnd_Super5_Pnd_Nec_Category_Rptng_Seq;
    private DbsField pnd_Nec_Fnd_Super5_Pnd_Nec_Category_Fund_Seq;
    private DbsField pnd_Nec_Cat_Super;

    private DbsGroup pnd_Nec_Cat_Super__R_Field_5;
    private DbsField pnd_Nec_Cat_Super_Pnd_Nec_Table_Cde_2;
    private DbsField pnd_Nec_Cat_Super_Pnd_Nec_Category_Class_Cde;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Cnt;
    private DbsField pnd_Idx;
    private DbsField pnd_T_Fund_Code;
    private DbsField pnd_T_Fund_Alloc_Pct;
    private DbsField pnd_T_Fund_Category;
    private DbsField pnd_T_Fund_Name;
    private DbsField pnd_Prev_Fund_Category;
    private DbsField pnd_Fund_Cnt;
    private DbsField pnd_Index;
    private DbsField p_Category_Unique;
    private DbsField pnd_Debug;
    private DbsField pnd_Max_Occurs;
    private DbsField pnd_W_Ticker;
    private DbsField p_Category_Unique_Fund_Cnt;
    private int cmrollReturnCode;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaPsta9200 = new PdaPsta9200(localVariables);
        ldaPstl9902 = new LdaPstl9902();
        registerRecord(ldaPstl9902);

        // parameters
        parameters = new DbsRecord();
        p_Fund_Code = parameters.newFieldArrayInRecord("p_Fund_Code", "P-FUND-CODE", FieldType.STRING, 10, new DbsArrayController(1, 100));
        p_Fund_Code.setParameterOption(ParameterOption.ByReference);
        p_Fund_Alloc_Pct = parameters.newFieldArrayInRecord("p_Fund_Alloc_Pct", "P-FUND-ALLOC-PCT", FieldType.STRING, 3, new DbsArrayController(1, 100));
        p_Fund_Alloc_Pct.setParameterOption(ParameterOption.ByReference);

        p_Fund_Alloc_Pct__R_Field_1 = parameters.newGroupInRecord("p_Fund_Alloc_Pct__R_Field_1", "REDEFINE", p_Fund_Alloc_Pct);
        p_Fund_Alloc_Pct_P_Fund_Alloc_Pct_N = p_Fund_Alloc_Pct__R_Field_1.newFieldArrayInGroup("p_Fund_Alloc_Pct_P_Fund_Alloc_Pct_N", "P-FUND-ALLOC-PCT-N", 
            FieldType.NUMERIC, 3, new DbsArrayController(1, 100));
        p_Category = parameters.newFieldArrayInRecord("p_Category", "P-CATEGORY", FieldType.STRING, 60, new DbsArrayController(1, 100));
        p_Category.setParameterOption(ParameterOption.ByReference);
        p_Fund_Name = parameters.newFieldArrayInRecord("p_Fund_Name", "P-FUND-NAME", FieldType.STRING, 60, new DbsArrayController(1, 100));
        p_Fund_Name.setParameterOption(ParameterOption.ByReference);
        p_Category_Cnt = parameters.newFieldInRecord("p_Category_Cnt", "P-CATEGORY-CNT", FieldType.NUMERIC, 6);
        p_Category_Cnt.setParameterOption(ParameterOption.ByReference);
        p_Return_Code = parameters.newFieldInRecord("p_Return_Code", "P-RETURN-CODE", FieldType.NUMERIC, 2);
        p_Return_Code.setParameterOption(ParameterOption.ByReference);
        p_Return_Msg = parameters.newFieldInRecord("p_Return_Msg", "P-RETURN-MSG", FieldType.STRING, 79);
        p_Return_Msg.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_new_Ext_Cntrl_Fnd_View = new DataAccessProgramView(new NameInfo("vw_new_Ext_Cntrl_Fnd_View", "NEW-EXT-CNTRL-FND-VIEW"), "NEW_EXT_CNTRL_FND", 
            "NEW_EXT_CNTRL");
        new_Ext_Cntrl_Fnd_View_Nec_Table_Cde = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Table_Cde", "NEC-TABLE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NEC_TABLE_CDE");
        new_Ext_Cntrl_Fnd_View_Nec_Ticker_Symbol = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Ticker_Symbol", "NEC-TICKER-SYMBOL", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "NEC_TICKER_SYMBOL");
        new_Ext_Cntrl_Fnd_View_Nec_Fund_Family_Cde = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Fund_Family_Cde", 
            "NEC-FUND-FAMILY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "NEC_FUND_FAMILY_CDE");
        new_Ext_Cntrl_Fnd_View_Nec_Fund_Style = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Fund_Style", "NEC-FUND-STYLE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NEC_FUND_STYLE");
        new_Ext_Cntrl_Fnd_View_Nec_Fund_Size = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Fund_Size", "NEC-FUND-SIZE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NEC_FUND_SIZE");
        new_Ext_Cntrl_Fnd_View_Nec_Fund_Nme = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Fund_Nme", "NEC-FUND-NME", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NEC_FUND_NME");
        new_Ext_Cntrl_Fnd_View_Nec_Fund_Inception_Dte = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Fund_Inception_Dte", 
            "NEC-FUND-INCEPTION-DTE", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "NEC_FUND_INCEPTION_DTE");
        new_Ext_Cntrl_Fnd_View_Nec_Fund_Cusip = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Fund_Cusip", "NEC-FUND-CUSIP", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "NEC_FUND_CUSIP");
        new_Ext_Cntrl_Fnd_View_Nec_Category_Class_Cde = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Category_Class_Cde", 
            "NEC-CATEGORY-CLASS-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "NEC_CATEGORY_CLASS_CDE");
        new_Ext_Cntrl_Fnd_View_Nec_Category_Rptng_Seq = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Category_Rptng_Seq", 
            "NEC-CATEGORY-RPTNG-SEQ", FieldType.STRING, 5, RepeatingFieldStrategy.None, "NEC_CATEGORY_RPTNG_SEQ");
        new_Ext_Cntrl_Fnd_View_Nec_Category_Fund_Seq = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Category_Fund_Seq", 
            "NEC-CATEGORY-FUND-SEQ", FieldType.STRING, 5, RepeatingFieldStrategy.None, "NEC_CATEGORY_FUND_SEQ");
        new_Ext_Cntrl_Fnd_View_Nec_Alpha_Fund_Cde = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Alpha_Fund_Cde", 
            "NEC-ALPHA-FUND-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "NEC_ALPHA_FUND_CDE");
        new_Ext_Cntrl_Fnd_View_Nec_Num_Fund_Cde = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Num_Fund_Cde", "NEC-NUM-FUND-CDE", 
            FieldType.NUMERIC, 5, RepeatingFieldStrategy.None, "NEC_NUM_FUND_CDE");
        new_Ext_Cntrl_Fnd_View_Nec_Reporting_Seq = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Reporting_Seq", "NEC-REPORTING-SEQ", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "NEC_REPORTING_SEQ");
        new_Ext_Cntrl_Fnd_View_Nec_Unit_Rate_Ind = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Unit_Rate_Ind", "NEC-UNIT-RATE-IND", 
            FieldType.PACKED_DECIMAL, 3, RepeatingFieldStrategy.None, "NEC_UNIT_RATE_IND");
        new_Ext_Cntrl_Fnd_View_Nec_Alpha_Fund_Seq = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Alpha_Fund_Seq", 
            "NEC-ALPHA-FUND-SEQ", FieldType.STRING, 2, RepeatingFieldStrategy.None, "NEC_ALPHA_FUND_SEQ");
        new_Ext_Cntrl_Fnd_View_Nec_V1_Num_Fund_Cde = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_V1_Num_Fund_Cde", 
            "NEC-V1-NUM-FUND-CDE", FieldType.NUMERIC, 5, RepeatingFieldStrategy.None, "NEC_V1_NUM_FUND_CDE");
        new_Ext_Cntrl_Fnd_View_Nec_V1_Alpha_Fund_Cde = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_V1_Alpha_Fund_Cde", 
            "NEC-V1-ALPHA-FUND-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "NEC_V1_ALPHA_FUND_CDE");
        new_Ext_Cntrl_Fnd_View_Nec_Abbr_Factor_Key = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Abbr_Factor_Key", 
            "NEC-ABBR-FACTOR-KEY", FieldType.STRING, 2, RepeatingFieldStrategy.None, "NEC_ABBR_FACTOR_KEY");
        new_Ext_Cntrl_Fnd_View_Nec_Act_Unit_Acct_Nm = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Act_Unit_Acct_Nm", 
            "NEC-ACT-UNIT-ACCT-NM", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "NEC_ACT_UNIT_ACCT_NM");
        new_Ext_Cntrl_Fnd_View_Nec_V2_Alpha_Cde = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_V2_Alpha_Cde", "NEC-V2-ALPHA-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "NEC_V2_ALPHA_CDE");
        new_Ext_Cntrl_Fnd_View_Nec_V2_Rt_1 = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_V2_Rt_1", "NEC-V2-RT-1", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "NEC_V2_RT_1");
        new_Ext_Cntrl_Fnd_View_Nec_V2_Rt_2 = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_V2_Rt_2", "NEC-V2-RT-2", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "NEC_V2_RT_2");
        new_Ext_Cntrl_Fnd_View_Nec_V2_Rt_3 = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_V2_Rt_3", "NEC-V2-RT-3", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "NEC_V2_RT_3");
        new_Ext_Cntrl_Fnd_View_Nec_Ivc_Company_Cde = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Ivc_Company_Cde", 
            "NEC-IVC-COMPANY-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "NEC_IVC_COMPANY_CDE");
        new_Ext_Cntrl_Fnd_View_Nec_V2_Active_Ind = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_V2_Active_Ind", "NEC-V2-ACTIVE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NEC_V2_ACTIVE_IND");
        new_Ext_Cntrl_Fnd_View_Nec_Dividend_Ind = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Dividend_Ind", "NEC-DIVIDEND-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NEC_DIVIDEND_IND");
        new_Ext_Cntrl_Fnd_View_Nec_Rate_Basis_Points = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Rate_Basis_Points", 
            "NEC-RATE-BASIS-POINTS", FieldType.NUMERIC, 6, 3, RepeatingFieldStrategy.None, "NEC_RATE_BASIS_POINTS");
        new_Ext_Cntrl_Fnd_View_Nec_Fund_SummaryMuGroup = vw_new_Ext_Cntrl_Fnd_View.getRecord().newGroupInGroup("NEW_EXT_CNTRL_FND_VIEW_NEC_FUND_SUMMARYMuGroup", 
            "NEC_FUND_SUMMARYMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NEW_EXT_CNTRL_NEC_FUND_SUMMARY");
        new_Ext_Cntrl_Fnd_View_Nec_Fund_Summary = new_Ext_Cntrl_Fnd_View_Nec_Fund_SummaryMuGroup.newFieldArrayInGroup("new_Ext_Cntrl_Fnd_View_Nec_Fund_Summary", 
            "NEC-FUND-SUMMARY", FieldType.STRING, 60, new DbsArrayController(1, 17), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NEC_FUND_SUMMARY");
        new_Ext_Cntrl_Fnd_View_Nec_Modified_By = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Modified_By", "NEC-MODIFIED-BY", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "NEC_MODIFIED_BY");
        new_Ext_Cntrl_Fnd_View_Nec_Modified_Dte = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Modified_Dte", "NEC-MODIFIED-DTE", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "NEC_MODIFIED_DTE");
        registerRecord(vw_new_Ext_Cntrl_Fnd_View);

        vw_new_Ext_Cntrl_Cat_View = new DataAccessProgramView(new NameInfo("vw_new_Ext_Cntrl_Cat_View", "NEW-EXT-CNTRL-CAT-VIEW"), "NEW_EXT_CNTRL_CAT", 
            "NEW_EXT_CNTRL");
        new_Ext_Cntrl_Cat_View_Nec_Table_Cde = vw_new_Ext_Cntrl_Cat_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Cat_View_Nec_Table_Cde", "NEC-TABLE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NEC_TABLE_CDE");
        new_Ext_Cntrl_Cat_View_Nec_Category_Class_Cde = vw_new_Ext_Cntrl_Cat_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Cat_View_Nec_Category_Class_Cde", 
            "NEC-CATEGORY-CLASS-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "NEC_CATEGORY_CLASS_CDE");
        new_Ext_Cntrl_Cat_View_Nec_Category_Class_Desc = vw_new_Ext_Cntrl_Cat_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Cat_View_Nec_Category_Class_Desc", 
            "NEC-CATEGORY-CLASS-DESC", FieldType.STRING, 60, RepeatingFieldStrategy.None, "NEC_CATEGORY_CLASS_DESC");
        new_Ext_Cntrl_Cat_View_Nec_Category_Rptng_Seq = vw_new_Ext_Cntrl_Cat_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Cat_View_Nec_Category_Rptng_Seq", 
            "NEC-CATEGORY-RPTNG-SEQ", FieldType.STRING, 5, RepeatingFieldStrategy.None, "NEC_CATEGORY_RPTNG_SEQ");
        new_Ext_Cntrl_Cat_View_Nec_Modified_By = vw_new_Ext_Cntrl_Cat_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Cat_View_Nec_Modified_By", "NEC-MODIFIED-BY", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "NEC_MODIFIED_BY");
        new_Ext_Cntrl_Cat_View_Nec_Modified_Dte = vw_new_Ext_Cntrl_Cat_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Cat_View_Nec_Modified_Dte", "NEC-MODIFIED-DTE", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "NEC_MODIFIED_DTE");
        new_Ext_Cntrl_Cat_View_Nec_Cat_Super = vw_new_Ext_Cntrl_Cat_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Cat_View_Nec_Cat_Super", "NEC-CAT-SUPER", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NEC_CAT_SUPER");
        new_Ext_Cntrl_Cat_View_Nec_Cat_Super.setSuperDescriptor(true);
        registerRecord(vw_new_Ext_Cntrl_Cat_View);

        vw_app_Table_Entry = new DataAccessProgramView(new NameInfo("vw_app_Table_Entry", "APP-TABLE-ENTRY"), "APP_TABLE_ENTRY", "ACIS_APP_TBL_ENT");
        app_Table_Entry_Entry_Actve_Ind = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Actve_Ind", "ENTRY-ACTVE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ENTRY_ACTVE_IND");
        app_Table_Entry_Entry_Table_Id_Nbr = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "ENTRY_TABLE_ID_NBR");
        app_Table_Entry_Entry_Table_Sub_Id = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ENTRY_TABLE_SUB_ID");
        app_Table_Entry_Entry_Cde = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "ENTRY_CDE");
        app_Table_Entry_Entry_Dscrptn_Txt = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "ENTRY_DSCRPTN_TXT");
        app_Table_Entry_Addtnl_Dscrptn_TxtMuGroup = vw_app_Table_Entry.getRecord().newGroupInGroup("APP_TABLE_ENTRY_ADDTNL_DSCRPTN_TXTMuGroup", "ADDTNL_DSCRPTN_TXTMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ACIS_APP_TBL_ENT_ADDTNL_DSCRPTN_TXT");
        app_Table_Entry_Addtnl_Dscrptn_Txt = app_Table_Entry_Addtnl_Dscrptn_TxtMuGroup.newFieldArrayInGroup("app_Table_Entry_Addtnl_Dscrptn_Txt", "ADDTNL-DSCRPTN-TXT", 
            FieldType.STRING, 60, new DbsArrayController(1, 2), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADDTNL_DSCRPTN_TXT");
        registerRecord(vw_app_Table_Entry);

        vw_app_Table_Upd = new DataAccessProgramView(new NameInfo("vw_app_Table_Upd", "APP-TABLE-UPD"), "APP_TABLE_ENTRY", "ACIS_APP_TBL_ENT");
        app_Table_Upd_Entry_Actve_Ind = vw_app_Table_Upd.getRecord().newFieldInGroup("app_Table_Upd_Entry_Actve_Ind", "ENTRY-ACTVE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ENTRY_ACTVE_IND");
        registerRecord(vw_app_Table_Upd);

        pnd_Table_Id_Entry_Cde = localVariables.newFieldInRecord("pnd_Table_Id_Entry_Cde", "#TABLE-ID-ENTRY-CDE", FieldType.STRING, 28);

        pnd_Table_Id_Entry_Cde__R_Field_2 = localVariables.newGroupInRecord("pnd_Table_Id_Entry_Cde__R_Field_2", "REDEFINE", pnd_Table_Id_Entry_Cde);
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr = pnd_Table_Id_Entry_Cde__R_Field_2.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr", 
            "#ENTRY-TABLE-ID-NBR", FieldType.NUMERIC, 6);
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id = pnd_Table_Id_Entry_Cde__R_Field_2.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id", 
            "#ENTRY-TABLE-SUB-ID", FieldType.STRING, 2);
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde = pnd_Table_Id_Entry_Cde__R_Field_2.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde", "#ENTRY-CDE", 
            FieldType.STRING, 20);
        pnd_Nec_Fnd_Super1 = localVariables.newFieldInRecord("pnd_Nec_Fnd_Super1", "#NEC-FND-SUPER1", FieldType.STRING, 13);

        pnd_Nec_Fnd_Super1__R_Field_3 = localVariables.newGroupInRecord("pnd_Nec_Fnd_Super1__R_Field_3", "REDEFINE", pnd_Nec_Fnd_Super1);
        pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde1 = pnd_Nec_Fnd_Super1__R_Field_3.newFieldInGroup("pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde1", "#NEC-TABLE-CDE1", 
            FieldType.STRING, 3);
        pnd_Nec_Fnd_Super1_Pnd_Nec_Tkr_Symbl = pnd_Nec_Fnd_Super1__R_Field_3.newFieldInGroup("pnd_Nec_Fnd_Super1_Pnd_Nec_Tkr_Symbl", "#NEC-TKR-SYMBL", 
            FieldType.STRING, 10);
        pnd_Nec_Fnd_Super5 = localVariables.newFieldInRecord("pnd_Nec_Fnd_Super5", "#NEC-FND-SUPER5", FieldType.STRING, 13);

        pnd_Nec_Fnd_Super5__R_Field_4 = localVariables.newGroupInRecord("pnd_Nec_Fnd_Super5__R_Field_4", "REDEFINE", pnd_Nec_Fnd_Super5);
        pnd_Nec_Fnd_Super5_Pnd_Nec_Table_Cde = pnd_Nec_Fnd_Super5__R_Field_4.newFieldInGroup("pnd_Nec_Fnd_Super5_Pnd_Nec_Table_Cde", "#NEC-TABLE-CDE", 
            FieldType.STRING, 3);
        pnd_Nec_Fnd_Super5_Pnd_Nec_Category_Rptng_Seq = pnd_Nec_Fnd_Super5__R_Field_4.newFieldInGroup("pnd_Nec_Fnd_Super5_Pnd_Nec_Category_Rptng_Seq", 
            "#NEC-CATEGORY-RPTNG-SEQ", FieldType.STRING, 5);
        pnd_Nec_Fnd_Super5_Pnd_Nec_Category_Fund_Seq = pnd_Nec_Fnd_Super5__R_Field_4.newFieldInGroup("pnd_Nec_Fnd_Super5_Pnd_Nec_Category_Fund_Seq", "#NEC-CATEGORY-FUND-SEQ", 
            FieldType.STRING, 5);
        pnd_Nec_Cat_Super = localVariables.newFieldInRecord("pnd_Nec_Cat_Super", "#NEC-CAT-SUPER", FieldType.STRING, 6);

        pnd_Nec_Cat_Super__R_Field_5 = localVariables.newGroupInRecord("pnd_Nec_Cat_Super__R_Field_5", "REDEFINE", pnd_Nec_Cat_Super);
        pnd_Nec_Cat_Super_Pnd_Nec_Table_Cde_2 = pnd_Nec_Cat_Super__R_Field_5.newFieldInGroup("pnd_Nec_Cat_Super_Pnd_Nec_Table_Cde_2", "#NEC-TABLE-CDE-2", 
            FieldType.STRING, 3);
        pnd_Nec_Cat_Super_Pnd_Nec_Category_Class_Cde = pnd_Nec_Cat_Super__R_Field_5.newFieldInGroup("pnd_Nec_Cat_Super_Pnd_Nec_Category_Class_Cde", "#NEC-CATEGORY-CLASS-CDE", 
            FieldType.STRING, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 3);
        pnd_Idx = localVariables.newFieldInRecord("pnd_Idx", "#IDX", FieldType.NUMERIC, 3);
        pnd_T_Fund_Code = localVariables.newFieldArrayInRecord("pnd_T_Fund_Code", "#T-FUND-CODE", FieldType.STRING, 10, new DbsArrayController(1, 100));
        pnd_T_Fund_Alloc_Pct = localVariables.newFieldArrayInRecord("pnd_T_Fund_Alloc_Pct", "#T-FUND-ALLOC-PCT", FieldType.STRING, 3, new DbsArrayController(1, 
            100));
        pnd_T_Fund_Category = localVariables.newFieldArrayInRecord("pnd_T_Fund_Category", "#T-FUND-CATEGORY", FieldType.STRING, 60, new DbsArrayController(1, 
            100));
        pnd_T_Fund_Name = localVariables.newFieldArrayInRecord("pnd_T_Fund_Name", "#T-FUND-NAME", FieldType.STRING, 60, new DbsArrayController(1, 100));
        pnd_Prev_Fund_Category = localVariables.newFieldInRecord("pnd_Prev_Fund_Category", "#PREV-FUND-CATEGORY", FieldType.STRING, 60);
        pnd_Fund_Cnt = localVariables.newFieldInRecord("pnd_Fund_Cnt", "#FUND-CNT", FieldType.NUMERIC, 6);
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.NUMERIC, 6);
        p_Category_Unique = localVariables.newFieldArrayInRecord("p_Category_Unique", "P-CATEGORY-UNIQUE", FieldType.STRING, 60, new DbsArrayController(1, 
            100));
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Max_Occurs = localVariables.newFieldInRecord("pnd_Max_Occurs", "#MAX-OCCURS", FieldType.PACKED_DECIMAL, 3);
        pnd_W_Ticker = localVariables.newFieldInRecord("pnd_W_Ticker", "#W-TICKER", FieldType.STRING, 10);
        p_Category_Unique_Fund_Cnt = localVariables.newFieldArrayInRecord("p_Category_Unique_Fund_Cnt", "P-CATEGORY-UNIQUE-FUND-CNT", FieldType.NUMERIC, 
            6, new DbsArrayController(1, 100));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_new_Ext_Cntrl_Fnd_View.reset();
        vw_new_Ext_Cntrl_Cat_View.reset();
        vw_app_Table_Entry.reset();
        vw_app_Table_Upd.reset();

        ldaPstl9902.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Max_Occurs.setInitialValue(100);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Appn1155() throws Exception
    {
        super("Appn1155");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPN1155", onError);
        p_Return_Code.reset();                                                                                                                                            //Natural: RESET P-RETURN-CODE P-RETURN-MSG
        p_Return_Msg.reset();
        //*  IF *INIT-USER = MASK ('T')  /* TEST JOB
        //*   #DEBUG := TRUE
        //*  END-IF
        //* *----------------------------------------------------------------------
        //*                             MAINLINE
        //* *----------------------------------------------------------------------
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM(),"(1360) START OF MAINLINE ...");                                                                                    //Natural: WRITE *PROGRAM '(1360) START OF MAINLINE ...'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        p_Category_Unique.getValue("*").reset();                                                                                                                          //Natural: RESET P-CATEGORY-UNIQUE ( * ) P-FUND-NAME ( * ) P-RETURN-CODE P-RETURN-MSG
        p_Fund_Name.getValue("*").reset();
        p_Return_Code.reset();
        p_Return_Msg.reset();
        //*   RETRIEVE ALL FUNDS FROM NEW-EXT-CNTRL-FND
        pnd_Nec_Fnd_Super5_Pnd_Nec_Table_Cde.setValue("FND");                                                                                                             //Natural: ASSIGN #NEC-TABLE-CDE := 'FND'
        pnd_Nec_Fnd_Super5_Pnd_Nec_Category_Rptng_Seq.reset();                                                                                                            //Natural: RESET #NEC-CATEGORY-RPTNG-SEQ #NEC-CATEGORY-FUND-SEQ
        pnd_Nec_Fnd_Super5_Pnd_Nec_Category_Fund_Seq.reset();
        //*  MAKE SURE THERE ARE NO EXISTING RECORDS
        pnd_Cnt.reset();                                                                                                                                                  //Natural: RESET #CNT #TABLE-ID-ENTRY-CDE
        pnd_Table_Id_Entry_Cde.reset();
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr.setValue(999999);                                                                                                   //Natural: ASSIGN #ENTRY-TABLE-ID-NBR := 999999
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id.setValue("FN");                                                                                                     //Natural: ASSIGN #ENTRY-TABLE-SUB-ID := 'FN'
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde.setValue(Global.getINIT_USER());                                                                                             //Natural: ASSIGN #ENTRY-CDE := *INIT-USER
        vw_app_Table_Entry.startDatabaseRead                                                                                                                              //Natural: READ APP-TABLE-ENTRY BY TABLE-ID-ENTRY-CDE STARTING FROM #TABLE-ID-ENTRY-CDE
        (
        "RD1",
        new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", ">=", pnd_Table_Id_Entry_Cde, WcType.BY) },
        new Oc[] { new Oc("TABLE_ID_ENTRY_CDE", "ASC") }
        );
        RD1:
        while (condition(vw_app_Table_Entry.readNextRow("RD1")))
        {
            if (condition(app_Table_Entry_Entry_Table_Id_Nbr.notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr) || app_Table_Entry_Entry_Table_Sub_Id.notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id)  //Natural: IF ENTRY-TABLE-ID-NBR NE #ENTRY-TABLE-ID-NBR OR ENTRY-TABLE-SUB-ID NE #ENTRY-TABLE-SUB-ID OR SUBSTR ( ENTRY-CDE,1,8 ) NE #ENTRY-CDE
                || !app_Table_Entry_Entry_Cde.getSubstring(1,8).equals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            GT1:                                                                                                                                                          //Natural: GET APP-TABLE-UPD *ISN ( RD1. )
            vw_app_Table_Upd.readByID(vw_app_Table_Entry.getAstISN("RD1"), "GT1");
            vw_app_Table_Upd.deleteDBRow("GT1");                                                                                                                          //Natural: DELETE ( GT1. )
            pnd_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #CNT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Cnt.greater(getZero())))                                                                                                                        //Natural: IF #CNT GT 0
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde1.setValue("FND");                                                                                                            //Natural: ASSIGN #NEC-TABLE-CDE1 := 'FND'
        pnd_Nec_Cat_Super_Pnd_Nec_Table_Cde_2.setValue("CAT");                                                                                                            //Natural: ASSIGN #NEC-TABLE-CDE-2 := 'CAT'
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 #MAX-OCCURS
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Occurs)); pnd_I.nadd(1))
        {
            if (condition(p_Fund_Code.getValue(pnd_I).equals(" ")))                                                                                                       //Natural: IF P-FUND-CODE ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Nec_Fnd_Super1_Pnd_Nec_Tkr_Symbl.setValue(p_Fund_Code.getValue(pnd_I));                                                                                   //Natural: ASSIGN #NEC-TKR-SYMBL := P-FUND-CODE ( #I )
            vw_new_Ext_Cntrl_Fnd_View.startDatabaseFind                                                                                                                   //Natural: FIND NEW-EXT-CNTRL-FND-VIEW WITH NEC-FND-SUPER1 = #NEC-FND-SUPER1
            (
            "FIND01",
            new Wc[] { new Wc("NEC_FND_SUPER1", "=", pnd_Nec_Fnd_Super1, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_new_Ext_Cntrl_Fnd_View.readNextRow("FIND01")))
            {
                vw_new_Ext_Cntrl_Fnd_View.setIfNotFoundControlFlag(false);
                pnd_Nec_Cat_Super_Pnd_Nec_Category_Class_Cde.setValue(new_Ext_Cntrl_Fnd_View_Nec_Category_Class_Cde);                                                     //Natural: ASSIGN #NEC-CATEGORY-CLASS-CDE := NEW-EXT-CNTRL-FND-VIEW.NEC-CATEGORY-CLASS-CDE
                vw_new_Ext_Cntrl_Cat_View.startDatabaseFind                                                                                                               //Natural: FIND NEW-EXT-CNTRL-CAT-VIEW WITH NEC-CAT-SUPER = #NEC-CAT-SUPER
                (
                "FIND02",
                new Wc[] { new Wc("NEC_CAT_SUPER", "=", pnd_Nec_Cat_Super, WcType.WITH) }
                );
                FIND02:
                while (condition(vw_new_Ext_Cntrl_Cat_View.readNextRow("FIND02")))
                {
                    vw_new_Ext_Cntrl_Cat_View.setIfNotFoundControlFlag(false);
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM STORE-FUND-INFO
                sub_Store_Fund_Info();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  RE-ORDER THE FUNDS BASED ON CTGRY-NAME
        p_Category_Cnt.reset();                                                                                                                                           //Natural: RESET P-CATEGORY-CNT
        pnd_Cnt.reset();                                                                                                                                                  //Natural: RESET #CNT #TABLE-ID-ENTRY-CDE
        pnd_Table_Id_Entry_Cde.reset();
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr.setValue(999999);                                                                                                   //Natural: ASSIGN #ENTRY-TABLE-ID-NBR := 999999
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id.setValue("FN");                                                                                                     //Natural: ASSIGN #ENTRY-TABLE-SUB-ID := 'FN'
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde.setValue(Global.getINIT_USER());                                                                                             //Natural: ASSIGN #ENTRY-CDE := *INIT-USER
        vw_app_Table_Entry.startDatabaseRead                                                                                                                              //Natural: READ APP-TABLE-ENTRY BY TABLE-ID-ENTRY-CDE STARTING FROM #TABLE-ID-ENTRY-CDE
        (
        "RDTBLE",
        new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", ">=", pnd_Table_Id_Entry_Cde, WcType.BY) },
        new Oc[] { new Oc("TABLE_ID_ENTRY_CDE", "ASC") }
        );
        RDTBLE:
        while (condition(vw_app_Table_Entry.readNextRow("RDTBLE")))
        {
            if (condition(app_Table_Entry_Entry_Table_Id_Nbr.notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr) || app_Table_Entry_Entry_Table_Sub_Id.notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id)  //Natural: IF ENTRY-TABLE-ID-NBR NE #ENTRY-TABLE-ID-NBR OR ENTRY-TABLE-SUB-ID NE #ENTRY-TABLE-SUB-ID OR SUBSTR ( ENTRY-CDE,1,8 ) NE #ENTRY-CDE
                || !app_Table_Entry_Entry_Cde.getSubstring(1,8).equals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_W_Ticker.setValue(app_Table_Entry_Addtnl_Dscrptn_Txt.getValue(1));                                                                                        //Natural: ASSIGN #W-TICKER := ADDTNL-DSCRPTN-TXT ( 1 )
            DbsUtil.examine(new ExamineSource(p_Fund_Code.getValue("*"),true), new ExamineSearch(pnd_W_Ticker), new ExamineGivingIndex(pnd_Idx));                         //Natural: EXAMINE FULL P-FUND-CODE ( * ) FOR #W-TICKER GIVING INDEX #IDX
            if (condition(pnd_Idx.greater(getZero())))                                                                                                                    //Natural: IF #IDX GT 0
            {
                pnd_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #CNT
                pnd_T_Fund_Code.getValue(pnd_Cnt).setValue(pnd_W_Ticker);                                                                                                 //Natural: ASSIGN #T-FUND-CODE ( #CNT ) := #W-TICKER
                pnd_T_Fund_Alloc_Pct.getValue(pnd_Cnt).setValueEdited(p_Fund_Alloc_Pct_P_Fund_Alloc_Pct_N.getValue(pnd_Idx),new ReportEditMask("ZZ9"));                   //Natural: MOVE EDITED P-FUND-ALLOC-PCT-N ( #IDX ) ( EM = ZZ9 ) TO #T-FUND-ALLOC-PCT ( #CNT )
                pnd_T_Fund_Name.getValue(pnd_Cnt).setValue(app_Table_Entry_Addtnl_Dscrptn_Txt.getValue(2));                                                               //Natural: ASSIGN #T-FUND-NAME ( #CNT ) := ADDTNL-DSCRPTN-TXT ( 2 )
                p_Category.getValue(pnd_Cnt).setValue(app_Table_Entry_Entry_Dscrptn_Txt);                                                                                 //Natural: ASSIGN P-CATEGORY ( #CNT ) := #T-FUND-CATEGORY ( #CNT ) := ENTRY-DSCRPTN-TXT
                pnd_T_Fund_Category.getValue(pnd_Cnt).setValue(app_Table_Entry_Entry_Dscrptn_Txt);
                if (condition(pnd_Prev_Fund_Category.equals(app_Table_Entry_Entry_Dscrptn_Txt)))                                                                          //Natural: IF #PREV-FUND-CATEGORY = ENTRY-DSCRPTN-TXT
                {
                    pnd_T_Fund_Category.getValue(pnd_Cnt).reset();                                                                                                        //Natural: RESET #T-FUND-CATEGORY ( #CNT )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    p_Category_Cnt.nadd(1);                                                                                                                               //Natural: ADD 1 TO P-CATEGORY-CNT
                    p_Category_Unique.getValue(p_Category_Cnt).setValue(app_Table_Entry_Entry_Dscrptn_Txt);                                                               //Natural: ASSIGN P-CATEGORY-UNIQUE ( P-CATEGORY-CNT ) := ENTRY-DSCRPTN-TXT
                }                                                                                                                                                         //Natural: END-IF
                pnd_Prev_Fund_Category.setValue(app_Table_Entry_Entry_Dscrptn_Txt);                                                                                       //Natural: ASSIGN #PREV-FUND-CATEGORY := ENTRY-DSCRPTN-TXT
            }                                                                                                                                                             //Natural: END-IF
            //*  IF *INIT-USER = MASK('T')
            //*    WRITE '**DELETING**' /
            //*      '=' ENTRY-TABLE-ID-NBR
            //*      '=' ENTRY-TABLE-SUB-ID
            //*      '=' ENTRY-CDE
            //*  END-IF
            GT2:                                                                                                                                                          //Natural: GET APP-TABLE-UPD *ISN ( RDTBLE. )
            vw_app_Table_Upd.readByID(vw_app_Table_Entry.getAstISN("RDTBLE"), "GT2");
            vw_app_Table_Upd.deleteDBRow("GT2");                                                                                                                          //Natural: DELETE ( GT2. )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  COUNT FUNDS PER CATEGORY
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 100
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(100)); pnd_I.nadd(1))
        {
            if (condition(p_Category_Unique.getValue(pnd_I).equals(" ")))                                                                                                 //Natural: IF P-CATEGORY-UNIQUE ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(p_Category.getValue("*"),true), new ExamineSearch(p_Category_Unique.getValue(pnd_I), true), new ExamineGivingNumber(pnd_Cnt),  //Natural: EXAMINE FULL P-CATEGORY ( * ) FOR FULL P-CATEGORY-UNIQUE ( #I ) ABSOLUTE GIVING NUMBER #CNT INDEX #INDEX
                new ExamineGivingIndex(pnd_Index));
            if (condition(pnd_Index.greater(getZero())))                                                                                                                  //Natural: IF #INDEX GT 0
            {
                p_Category_Unique_Fund_Cnt.getValue(pnd_I).setValue(pnd_Cnt);                                                                                             //Natural: ASSIGN P-CATEGORY-UNIQUE-FUND-CNT ( #I ) := #CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        p_Fund_Code.getValue(1,":",100).setValue(pnd_T_Fund_Code.getValue(1,":",100));                                                                                    //Natural: ASSIGN P-FUND-CODE ( 1:100 ) := #T-FUND-CODE ( 1:100 )
        p_Fund_Alloc_Pct.getValue(1,":",100).setValue(pnd_T_Fund_Alloc_Pct.getValue(1,":",100));                                                                          //Natural: ASSIGN P-FUND-ALLOC-PCT ( 1:100 ) := #T-FUND-ALLOC-PCT ( 1:100 )
        p_Fund_Name.getValue(1,":",100).setValue(pnd_T_Fund_Name.getValue(1,":",100));                                                                                    //Natural: ASSIGN P-FUND-NAME ( 1:100 ) := #T-FUND-NAME ( 1:100 )
        p_Category.getValue(1,":",100).setValue(pnd_T_Fund_Category.getValue(1,":",100));                                                                                 //Natural: ASSIGN P-CATEGORY ( 1:100 ) := #T-FUND-CATEGORY ( 1:100 )
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
                                                                                                                                                                          //Natural: PERFORM RTN-99-DEBUG
            sub_Rtn_99_Debug();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* **************************************************031407***************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-FUND-INFO
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-99-DEBUG
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Store_Fund_Info() throws Exception                                                                                                                   //Natural: STORE-FUND-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_app_Table_Entry.reset();                                                                                                                                       //Natural: RESET APP-TABLE-ENTRY
        app_Table_Entry_Entry_Table_Id_Nbr.setValue(999999);                                                                                                              //Natural: ASSIGN APP-TABLE-ENTRY.ENTRY-TABLE-ID-NBR := 999999
        app_Table_Entry_Entry_Table_Sub_Id.setValue("FN");                                                                                                                //Natural: ASSIGN APP-TABLE-ENTRY.ENTRY-TABLE-SUB-ID := 'FN'
        app_Table_Entry_Entry_Cde.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, Global.getINIT_USER(), new_Ext_Cntrl_Fnd_View_Nec_Category_Rptng_Seq,          //Natural: COMPRESS *INIT-USER NEW-EXT-CNTRL-FND-VIEW.NEC-CATEGORY-RPTNG-SEQ NEW-EXT-CNTRL-FND-VIEW.NEC-CATEGORY-FUND-SEQ INTO APP-TABLE-ENTRY.ENTRY-CDE LEAVING NO
            new_Ext_Cntrl_Fnd_View_Nec_Category_Fund_Seq));
        app_Table_Entry_Entry_Dscrptn_Txt.setValue(new_Ext_Cntrl_Cat_View_Nec_Category_Class_Desc);                                                                       //Natural: ASSIGN APP-TABLE-ENTRY.ENTRY-DSCRPTN-TXT := NEW-EXT-CNTRL-CAT-VIEW.NEC-CATEGORY-CLASS-DESC
        app_Table_Entry_Addtnl_Dscrptn_Txt.getValue(1).setValue(pnd_Nec_Fnd_Super1_Pnd_Nec_Tkr_Symbl);                                                                    //Natural: ASSIGN APP-TABLE-ENTRY.ADDTNL-DSCRPTN-TXT ( 1 ) := #NEC-TKR-SYMBL
        app_Table_Entry_Addtnl_Dscrptn_Txt.getValue(2).setValue(new_Ext_Cntrl_Fnd_View_Nec_Fund_Nme);                                                                     //Natural: ASSIGN APP-TABLE-ENTRY.ADDTNL-DSCRPTN-TXT ( 2 ) := NEW-EXT-CNTRL-FND-VIEW.NEC-FUND-NME
        vw_app_Table_Entry.insertDBRow();                                                                                                                                 //Natural: STORE APP-TABLE-ENTRY
    }
    private void sub_Rtn_99_Debug() throws Exception                                                                                                                      //Natural: RTN-99-DEBUG
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        getReports().write(0, "-",new RepeatItem(20),Global.getPROGRAM(),"(2770)","-",new RepeatItem(20));                                                                //Natural: WRITE '-' ( 20 ) *PROGRAM '(2770)' '-' ( 20 )
        if (Global.isEscape()) return;
        getReports().write(0, "APPA6500");                                                                                                                                //Natural: WRITE 'APPA6500'
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #J = 1 TO 100
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(100)); pnd_J.nadd(1))
        {
            if (condition(p_Fund_Code.getValue(pnd_J).equals(" ")))                                                                                                       //Natural: IF P-FUND-CODE ( #J ) = ' '
            {
                if (condition(pnd_J.greater(1) && p_Fund_Code.getValue(pnd_J.getDec().subtract(1)).equals(" ")))                                                          //Natural: IF #J GT 1 AND P-FUND-CODE ( #J - 1 ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, p_Category.getValue(pnd_J), new AlphanumericLength (30),p_Fund_Name.getValue(pnd_J), new AlphanumericLength (30),p_Fund_Alloc_Pct.getValue(pnd_J), //Natural: WRITE P-CATEGORY ( #J ) ( AL = 30 ) P-FUND-NAME ( #J ) ( AL = 30 ) P-FUND-ALLOC-PCT ( #J ) ' ' P-FUND-CODE ( #J )
                " ",p_Fund_Code.getValue(pnd_J));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 TO 50
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(50)); pnd_I.nadd(1))
        {
            if (condition(p_Category_Unique.getValue(pnd_I).equals(" ")))                                                                                                 //Natural: IF P-CATEGORY-UNIQUE ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, p_Category_Unique.getValue(pnd_I),p_Category_Unique_Fund_Cnt.getValue(pnd_I));                                                          //Natural: WRITE P-CATEGORY-UNIQUE ( #I ) P-CATEGORY-UNIQUE-FUND-CNT ( #I )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"Total Categories: ",p_Category_Cnt);                                                                                               //Natural: WRITE / 'Total Categories: ' P-CATEGORY-CNT
        if (Global.isEscape()) return;
        //*  RTN-99-DEBUG
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        short decideConditionsMet282 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF *ERROR-NR;//Natural: VALUE 3145
        if (condition((Global.getERROR_NR().equals(3145))))
        {
            decideConditionsMet282++;
            //* ***********************************************************************
            //*    START OF COPY CODE PSTC9902   : RETRY LOGIC
            //*  - FIRST CHECK FOR SUBSEQUENT RETRY - SAME LINE NUMBER.
            //*  - IF SUBSEQUENT RETRY, CHECK TOTAL ELAPSED TIME
            //*  - IF NEW RETRY, SET DEFAULT VALUES FOR WAIT PARAMETERS IF NONE SPECI-
            //*    FIED, START CLOCK.
            //*  - CALL CMROLL TO ACTIVATE WAITING PERIOD
            //*  - RETRY
            //*  ----------------------------------------------------------------------
            //*         PROGRAM USING THIS COPYCODE MUST USE LDA PSTL9902
            //*                                          AND PDA PSTA9200 AS AN LDA.
            //*         #WAIT-TIME & #TOTAL-WAITING-TIME MAY BE SPECIFIED BY THE
            //*         PROGRAM USING THIS COPY CODE, JUST BEFORE THE INCLUDE STATEMENT
            //*         THEY ARE BOTH IN T FORMAT. TO INITIALIZE A T FIELD, USE
            //*         MOVE T'00:mm:ss' TO #WAIT-TIME
            //*         WHERE SS = SECONDS  (MAX OF 99 SECONDS)
            //*         (THE LIMITATION IS BECAUSE CMROLL ACCEPT UP TO 99 SECONDS ONLY)
            //*  ----------------------------------------------------------------------
            //* ***********************************************************************
            if (condition(Global.getERROR_NR().equals(3145)))                                                                                                             //Natural: IF *ERROR-NR = 3145
            {
                ldaPstl9902.getPstl9902_Pnd_Start_Time_This_Error().setValue(Global.getTIMX());                                                                           //Natural: ASSIGN PSTL9902.#START-TIME-THIS-ERROR := *TIMX
                if (condition(ldaPstl9902.getPstl9902_Pnd_Prev_Error_Line().equals(Global.getERROR_LINE())))                                                              //Natural: IF PSTL9902.#PREV-ERROR-LINE = *ERROR-LINE
                {
                    ldaPstl9902.getPstl9902_Pnd_Total_Elapsed_Time().compute(new ComputeParameters(false, ldaPstl9902.getPstl9902_Pnd_Total_Elapsed_Time()),              //Natural: ASSIGN PSTL9902.#TOTAL-ELAPSED-TIME := *TIMX - PSTL9902.#TIME-STARTED
                        Global.getTIMX().subtract(ldaPstl9902.getPstl9902_Pnd_Time_Started()));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl9902.getPstl9902_Pnd_Time_Started().setValue(Global.getTIMX());                                                                                //Natural: ASSIGN PSTL9902.#TIME-STARTED := *TIMX
                    ldaPstl9902.getPstl9902_Pnd_Prev_Error_Line().setValue(Global.getERROR_LINE());                                                                       //Natural: ASSIGN PSTL9902.#PREV-ERROR-LINE := *ERROR-LINE
                    short decideConditionsMet319 = 0;                                                                                                                     //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN PSTL9902.#WAIT-TIME LE 0
                    if (condition(ldaPstl9902.getPstl9902_Pnd_Wait_Time().lessOrEqual(getZero())))
                    {
                        decideConditionsMet319++;
                        if (condition(DbsUtil.maskMatches(Global.getINIT_USER(),"'RPC'.....") || DbsUtil.maskMatches(Global.getINIT_USER(),"'SNY'.....")))                //Natural: IF *INIT-USER = MASK ( 'RPC'..... ) OR = MASK ( 'SNY'..... )
                        {
                            ldaPstl9902.getPstl9902_Pnd_Wait_Time().setValue(20);                                                                                         //Natural: MOVE T'00:00:02' TO PSTL9902.#WAIT-TIME
                            getReports().write(0, "Setting WAIT TIME to",ldaPstl9902.getPstl9902_Pnd_Wait_Time());                                                        //Natural: WRITE 'Setting WAIT TIME to' PSTL9902.#WAIT-TIME
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(Global.getDEVICE().equals("BATCH")))                                                                                            //Natural: IF *DEVICE = 'BATCH'
                            {
                                ldaPstl9902.getPstl9902_Pnd_Wait_Time().setValue(150);                                                                                    //Natural: MOVE T'00:00:15' TO PSTL9902.#WAIT-TIME
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                ldaPstl9902.getPstl9902_Pnd_Wait_Time().setValue(40);                                                                                     //Natural: MOVE T'00:00:04' TO PSTL9902.#WAIT-TIME
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN PSTL9902.#TOTAL-WAITING-TIME LE 0
                    if (condition(ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time().lessOrEqual(getZero())))
                    {
                        decideConditionsMet319++;
                        if (condition(DbsUtil.maskMatches(Global.getINIT_USER(),"'RPC'.....") || DbsUtil.maskMatches(Global.getINIT_USER(),"'SNY'.....")))                //Natural: IF *INIT-USER = MASK ( 'RPC'..... ) OR = MASK ( 'SNY'..... )
                        {
                            ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time().setValue(100);                                                                               //Natural: MOVE T'00:00:10' TO PSTL9902.#TOTAL-WAITING-TIME
                            getReports().write(0, "Setting TOTAL WAIT TIME to",ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time());                                         //Natural: WRITE 'Setting TOTAL WAIT TIME to' PSTL9902.#TOTAL-WAITING-TIME
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(Global.getDEVICE().equals("BATCH")))                                                                                            //Natural: IF *DEVICE = 'BATCH'
                            {
                                ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time().setValue(18000);                                                                         //Natural: MOVE T'00:30:00' TO PSTL9902.#TOTAL-WAITING-TIME
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time().setValue(900);                                                                           //Natural: MOVE T'00:01:30' TO PSTL9902.#TOTAL-WAITING-TIME
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN NONE
                    if (condition(decideConditionsMet319 == 0))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaPstl9902.getPstl9902_Pnd_Total_Elapsed_Time().greater(ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time())))                                //Natural: IF PSTL9902.#TOTAL-ELAPSED-TIME GT PSTL9902.#TOTAL-WAITING-TIME
                {
                    if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                    //Natural: IF *DEVICE = 'BATCH'
                    {
                        getReports().write(0, "Requested record in program",Global.getPROGRAM(),"line",Global.getERROR_LINE(),"was still on hold after",NEWLINE,ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time(),  //Natural: WRITE 'Requested record in program' *PROGRAM 'line' *ERROR-LINE 'was still on hold after' / PSTL9902.#TOTAL-WAITING-TIME ( EM = HH:II:SS )
                            new ReportEditMask ("HH:II:SS"));
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaPsta9200.getPsta9200_Message_Text().setValue(DbsUtil.compress("Requested record in program", Global.getPROGRAM(), "line", Global.getERROR_LINE(),  //Natural: COMPRESS 'Requested record in program' *PROGRAM 'line' *ERROR-LINE 'exceeded hold time limit' INTO PSTA9200.MESSAGE-TEXT
                            "exceeded hold time limit"));
                        pdaPsta9200.getPsta9200_Enter_Allowed().reset();                                                                                                  //Natural: RESET PSTA9200.ENTER-ALLOWED PSTA9200.CONFIRM-ALLOWED
                        pdaPsta9200.getPsta9200_Confirm_Allowed().reset();
                        pdaPsta9200.getPsta9200_Exit_Allowed().setValue(true);                                                                                            //Natural: ASSIGN PSTA9200.EXIT-ALLOWED := TRUE
                        DbsUtil.callnat(Pstn9200.class , getCurrentProcessState(), pdaPsta9200.getPsta9200());                                                            //Natural: CALLNAT 'PSTN9200' PSTA9200
                        if (condition(Global.isEscape())) return;
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl9902.getPstl9902_Pnd_Time_Elapsed().compute(new ComputeParameters(false, ldaPstl9902.getPstl9902_Pnd_Time_Elapsed()), Global.getTIMX().subtract(ldaPstl9902.getPstl9902_Pnd_Start_Time_This_Error())); //Natural: ASSIGN PSTL9902.#TIME-ELAPSED := *TIMX - PSTL9902.#START-TIME-THIS-ERROR
                    if (condition(ldaPstl9902.getPstl9902_Pnd_Time_Elapsed().less(ldaPstl9902.getPstl9902_Pnd_Wait_Time())))                                              //Natural: IF PSTL9902.#TIME-ELAPSED LT PSTL9902.#WAIT-TIME
                    {
                        ldaPstl9902.getPstl9902_Pnd_Wait_Time_Left().compute(new ComputeParameters(false, ldaPstl9902.getPstl9902_Pnd_Wait_Time_Left()),                  //Natural: ASSIGN PSTL9902.#WAIT-TIME-LEFT := PSTL9902.#WAIT-TIME - PSTL9902.#TIME-ELAPSED
                            ldaPstl9902.getPstl9902_Pnd_Wait_Time().subtract(ldaPstl9902.getPstl9902_Pnd_Time_Elapsed()));
                        ldaPstl9902.getPstl9902_Pnd_Seconds_Left_A().setValueEdited(ldaPstl9902.getPstl9902_Pnd_Wait_Time_Left(),new ReportEditMask("SS"));               //Natural: MOVE EDITED PSTL9902.#WAIT-TIME-LEFT ( EM = SS ) TO PSTL9902.#SECONDS-LEFT-A
                        ldaPstl9902.getPstl9902_Pnd_Wait_Seconds_Left().setValue(ldaPstl9902.getPstl9902_Pnd_Seconds_Left_N());                                           //Natural: ASSIGN PSTL9902.#WAIT-SECONDS-LEFT := PSTL9902.#SECONDS-LEFT-N
                        cmrollReturnCode = DbsUtil.callExternalProgram("CMROLL",ldaPstl9902.getPstl9902_Pnd_Wait_Seconds_Left(),ldaPstl9902.getPstl9902_Pnd_Cics_Rqst()); //Natural: CALL 'CMROLL' PSTL9902.#WAIT-SECONDS-LEFT PSTL9902.#CICS-RQST
                    }                                                                                                                                                     //Natural: END-IF
                    OnErrorManager.popRetry();                                                                                                                            //Natural: RETRY
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* ***********************************************************************
            //*     END OF COPYCODE PSTC9902
            //* ***********************************************************************
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            getReports().write(0, ReportOption.NOHDR,"ERROR IN     ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER ",Global.getERROR_NR(),NEWLINE,"ERROR LINE   ",            //Natural: WRITE NOHDR 'ERROR IN     ' *PROGRAM / 'ERROR NUMBER ' *ERROR-NR / 'ERROR LINE   ' *ERROR-LINE / 'KEY' #TABLE-ID-ENTRY-CDE / '=' ENTRY-TABLE-ID-NBR / '=' ENTRY-TABLE-SUB-ID / '=' ENTRY-CDE
                Global.getERROR_LINE(),NEWLINE,"KEY",pnd_Table_Id_Entry_Cde,NEWLINE,"=",app_Table_Entry_Entry_Table_Id_Nbr,NEWLINE,"=",app_Table_Entry_Entry_Table_Sub_Id,
                NEWLINE,"=",app_Table_Entry_Entry_Cde);
            DbsUtil.terminate(4);  if (true) return;                                                                                                                      //Natural: TERMINATE 4
        }                                                                                                                                                                 //Natural: END-DECIDE
    };                                                                                                                                                                    //Natural: END-ERROR
}
