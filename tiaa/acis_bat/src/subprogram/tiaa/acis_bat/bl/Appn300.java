/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:59:06 PM
**        * FROM NATURAL SUBPROGRAM : Appn300
************************************************************
**        * FILE NAME            : Appn300.java
**        * CLASS NAME           : Appn300
**        * INSTANCE NAME        : Appn300
************************************************************
*
* MODIFIED ON 02/10/99 : RESTOW DUE TO ADDITIONAL PARAMETER     (LS)
*
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appn300 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAppa010 pdaAppa010;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField error_Found_1;
    private DbsField error_Found_2;
    private DbsField error_Found_3;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaAppa010 = new PdaAppa010(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        error_Found_1 = localVariables.newFieldInRecord("error_Found_1", "ERROR-FOUND-1", FieldType.NUMERIC, 5);
        error_Found_2 = localVariables.newFieldInRecord("error_Found_2", "ERROR-FOUND-2", FieldType.NUMERIC, 5);
        error_Found_3 = localVariables.newFieldInRecord("error_Found_3", "ERROR-FOUND-3", FieldType.NUMERIC, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Appn300() throws Exception
    {
        super("Appn300");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        DbsUtil.examine(new ExamineSource(pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message()), new ExamineSearch("CRITICAL ERROR"), new ExamineGivingNumber(error_Found_1)); //Natural: EXAMINE #OTHER-ACTIVITY-MESSAGE 'CRITICAL ERROR' GIVING NUMBER ERROR-FOUND-1
        DbsUtil.examine(new ExamineSource(pdaAppa010.getPnd_Parameter_Pnd_Add_To_Buckets_Message()), new ExamineSearch("ILOG"), new ExamineGivingNumber(error_Found_2));  //Natural: EXAMINE #ADD-TO-BUCKETS-MESSAGE 'ILOG' GIVING NUMBER ERROR-FOUND-2
        DbsUtil.examine(new ExamineSource(pdaAppa010.getPnd_Parameter_Pnd_Get_Contract_Message()), new ExamineSearch("ILOG"), new ExamineGivingNumber(error_Found_3));    //Natural: EXAMINE #GET-CONTRACT-MESSAGE 'ILOG' GIVING NUMBER ERROR-FOUND-3
        if (condition(error_Found_1.greater(getZero()) || error_Found_2.greater(getZero()) || error_Found_3.greater(getZero())))                                          //Natural: IF ERROR-FOUND-1 > 0 OR ERROR-FOUND-2 > 0 OR ERROR-FOUND-3 > 0
        {
            setControl("WB");                                                                                                                                             //Natural: SET CONTROL 'WB'
            getReports().write(0, pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message(),pdaAppa010.getPnd_Parameter_Pnd_Get_Contract_Message(),pdaAppa010.getPnd_Parameter_Pnd_Add_To_Buckets_Message()); //Natural: WRITE #OTHER-ACTIVITY-MESSAGE #GET-CONTRACT-MESSAGE #ADD-TO-BUCKETS-MESSAGE
            if (Global.isEscape()) return;
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-IF
        //*  -------------------------------------------------------------
        //*  *--#GET-CONTRACT-MESSAGE
        //*  'ILOG400 NO CHECKPOINT'
        //*  'ILOG405 NO CURRENT JUMPER'
        //*  'ILOG407 PARM INVALID DATE'
        //*  'ILOG410 NO CURRENT BUCKET'
        //*  'ILOG420 NO CURRENT ACCUM'
        //*  'ILOG430 NO CURRENT JUMPER'
        //*  'ILOG440 NO NEXT JUMPER'
        //*  'ILOG444 JU RECS NOT SYNC'
        //*  'ILOG445 NO CURRENT JUMPER'
        //*  'ILOG447 PARM INVALID DATE'
        //*  'PUT LAST RUN DT'
        //*  'CONTRACT GOTTEN'
        //*  'NO CONTRACT REQUESTED'
        //*  *--#OTHER-ACTIVITY-MESSAGE
        //*  'CRITICAL ERROR'
        //*  '1ST NEW DAY /'
        //*  '/CRITICAL ERROR'
        //*  '/GOT NEW RANGE '
        //*  *--#ADD-TO-BUCKETS-MESSAGE
        //*  'ILOG450 NO CURRENT JUMPER'
        //*  'ILOG470 NO CURRENT ACCUM'
        //*  'ILOG480 NO CURRENT BUCKET'
        //*  'BUCKETS ADDED'
        //*  'NO BUCKETS REQUESTED'
    }

    //
}
