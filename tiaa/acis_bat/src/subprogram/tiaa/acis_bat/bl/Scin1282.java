/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:08:22 AM
**        * FROM NATURAL SUBPROGRAM : Scin1282
************************************************************
**        * FILE NAME            : Scin1282.java
**        * CLASS NAME           : Scin1282
**        * INSTANCE NAME        : Scin1282
************************************************************
************************************************************************
* PROGRAM NAME : SCIN1282 - 3.2.2 ENHANCEMENT - OMNI TO ACIS INTERFACE
* DESCRIPTION  : ACIS RETRIEVE OVERRIDE DATA FROM
*                ANNTY-ACTVTY-ACCPTD-APPCTN AND ACIS BENE FILE
* WRITTEN BY   : BUDDY NEWSOM
* DATE WRITTEN : NOV. 4, 2011
************************************************************************
*    DATE      USERID                   DESCRIPTION
* MM/DD/YYYY  XXXXXXXX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* 11/03/2011  NEWSOB   ORIGINAL VERSION OF THE SUBPROGRAM
* 07/18/2013  NEWSOB   CHANGE THE RETRIEVAL OF THE OVERRIDE DATA INCLUDE
*                      THE ADDITIONAL FIELD THAT ARE NEEDED FOR NY200
* 03/17/2017  ELLO     CHANGE THE PDA FROM SCIA1290 TO SCIA1295 TO
*                      INCLUDE BENE ALLOCATION FRACTIONS FOR ONEIRA
* 05/23/17 B.ELLO      PIN EXPANSION CHANGES
*                      RESTOWED FOR UPDATED PDAS AND LDAS   (PINE)
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Scin1282 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaScia1295 pdaScia1295;
    private LdaScil1280 ldaScil1280;
    private PdaErla1000 pdaErla1000;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Hold_Ssn;
    private DbsField pnd_I;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaScil1280 = new LdaScil1280();
        registerRecord(ldaScil1280);
        registerRecord(ldaScil1280.getVw_accepted_App_View());
        registerRecord(ldaScil1280.getVw_bene_View());
        localVariables = new DbsRecord();
        pdaErla1000 = new PdaErla1000(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaScia1295 = new PdaScia1295(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Hold_Ssn = localVariables.newFieldInRecord("pnd_Hold_Ssn", "#HOLD-SSN", FieldType.NUMERIC, 9);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaScil1280.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Scin1282() throws Exception
    {
        super("Scin1282");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("SCIN1282", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ZP = ON SG = OFF
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
        //*                        M A I N   R O U T I N E                        *
        //* ***********************************************************************
        pdaScia1295.getPnd_Scia1295_Out().reset();                                                                                                                        //Natural: RESET #SCIA1295-OUT
        pdaScia1295.getPnd_Scia1295_Out_Pnd_Response_Code().setValue("0000");                                                                                             //Natural: ASSIGN #RESPONSE-CODE := '0000'
        pdaScia1295.getPnd_Scia1295_Out_Pnd_Response_Text().setValue(" ");                                                                                                //Natural: ASSIGN #RESPONSE-TEXT := ' '
        //*  USE THE SSN AND/OR ORCHESTRATION ID AS YOUR KEY TO FIND THE RECORDS
        //*  IN THE ANNTY-ACTVTY-ACCPTD-APPLCTN AND THE ACIS-BENE-FILE.
        pnd_Hold_Ssn.setValueEdited(new ReportEditMask("999999999"),pdaScia1295.getPnd_Scia1295_In_Pnd_Ssn());                                                            //Natural: MOVE EDITED #SSN TO #HOLD-SSN ( EM = 999999999 )
        ldaScil1280.getVw_accepted_App_View().startDatabaseFind                                                                                                           //Natural: FIND ACCEPTED-APP-VIEW WITH AAT-SOC-SEC = #HOLD-SSN WHERE ACCEPTED-APP-VIEW.AAT-ORCHESTRATION-ID = #ORCHESTRATION-ID
        (
        "FIND01",
        new Wc[] { new Wc("AAT_SOC_SEC", "=", pnd_Hold_Ssn, WcType.WITH) ,
        new Wc("AAT_ORCHESTRATION_ID", "=", pdaScia1295.getPnd_Scia1295_In_Pnd_Orchestration_Id(), WcType.WHERE) }
        );
        FIND01:
        while (condition(ldaScil1280.getVw_accepted_App_View().readNextRow("FIND01", true)))
        {
            ldaScil1280.getVw_accepted_App_View().setIfNotFoundControlFlag(false);
            if (condition(ldaScil1280.getVw_accepted_App_View().getAstCOUNTER().equals(0)))                                                                               //Natural: IF NO RECORDS FOUND
            {
                pdaScia1295.getPnd_Scia1295_Out_Pnd_Response_Code().setValue("0353");                                                                                     //Natural: ASSIGN #RESPONSE-CODE := '0353'
                pdaScia1295.getPnd_Scia1295_Out_Pnd_Response_Text().setValue(DbsUtil.compress("NO RECS FOUND ACCEPTED APPLCNT FILE FOR SSN: ", pnd_Hold_Ssn));            //Natural: COMPRESS 'NO RECS FOUND ACCEPTED APPLCNT FILE FOR SSN: ' #HOLD-SSN INTO #RESPONSE-TEXT
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pdaScia1295.getPnd_Scia1295_In_Pnd_Plan_Number().setValue(ldaScil1280.getAccepted_App_View_Aat_Sgrd_Plan_No());                                               //Natural: ASSIGN #PLAN-NUMBER := ACCEPTED-APP-VIEW.AAT-SGRD-PLAN-NO
            pdaScia1295.getPnd_Scia1295_In_Pnd_Subplan_Number().setValue(ldaScil1280.getAccepted_App_View_Aat_Sgrd_Subplan_No());                                         //Natural: ASSIGN #SUBPLAN-NUMBER := ACCEPTED-APP-VIEW.AAT-SGRD-SUBPLAN-NO
            pdaScia1295.getPnd_Scia1295_In_Pnd_Location_Code().setValue(ldaScil1280.getAccepted_App_View_Aat_Sgrd_Divsub());                                              //Natural: ASSIGN #LOCATION-CODE := ACCEPTED-APP-VIEW.AAT-SGRD-DIVSUB
            pdaScia1295.getPnd_Scia1295_In_Pnd_Name_Prefix().setValue(ldaScil1280.getAccepted_App_View_Aat_Cor_Prfx_Nme());                                               //Natural: ASSIGN #NAME-PREFIX := ACCEPTED-APP-VIEW.AAT-COR-PRFX-NME
            pdaScia1295.getPnd_Scia1295_In_Pnd_Last_Name().setValue(ldaScil1280.getAccepted_App_View_Aat_Cor_Last_Nme());                                                 //Natural: ASSIGN #LAST-NAME := ACCEPTED-APP-VIEW.AAT-COR-LAST-NME
            pdaScia1295.getPnd_Scia1295_In_Pnd_First_Name().setValue(ldaScil1280.getAccepted_App_View_Aat_Cor_First_Nme());                                               //Natural: ASSIGN #FIRST-NAME := ACCEPTED-APP-VIEW.AAT-COR-FIRST-NME
            pdaScia1295.getPnd_Scia1295_In_Pnd_Middle_Name().setValue(ldaScil1280.getAccepted_App_View_Aat_Cor_Mddle_Nme());                                              //Natural: ASSIGN #MIDDLE-NAME := ACCEPTED-APP-VIEW.AAT-COR-MDDLE-NME
            pdaScia1295.getPnd_Scia1295_In_Pnd_Name_Suffix().setValue(ldaScil1280.getAccepted_App_View_Aat_Cor_Sffx_Nme());                                               //Natural: ASSIGN #NAME-SUFFIX := ACCEPTED-APP-VIEW.AAT-COR-SFFX-NME
            pdaScia1295.getPnd_Scia1295_In_Pnd_Birth_Date().setValue(ldaScil1280.getAccepted_App_View_Aat_Dob());                                                         //Natural: ASSIGN #BIRTH-DATE := ACCEPTED-APP-VIEW.AAT-DOB
            if (condition(ldaScil1280.getAccepted_App_View_Aat_Soc_Sec().greater(getZero())))                                                                             //Natural: IF ACCEPTED-APP-VIEW.AAT-SOC-SEC > 0
            {
                pdaScia1295.getPnd_Scia1295_In_Pnd_Ssn().setValueEdited(ldaScil1280.getAccepted_App_View_Aat_Soc_Sec(),new ReportEditMask("999999999"));                  //Natural: MOVE EDITED ACCEPTED-APP-VIEW.AAT-SOC-SEC ( EM = 999999999 ) TO #SSN
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1295.getPnd_Scia1295_In_Pnd_Pin().setValue(ldaScil1280.getAccepted_App_View_Aat_Pin_Nbr());                                                            //Natural: ASSIGN #PIN := ACCEPTED-APP-VIEW.AAT-PIN-NBR
            pdaScia1295.getPnd_Scia1295_In_Pnd_Email().setValue(ldaScil1280.getAccepted_App_View_Aat_Email_Address());                                                    //Natural: ASSIGN #EMAIL := ACCEPTED-APP-VIEW.AAT-EMAIL-ADDRESS
            pdaScia1295.getPnd_Scia1295_In_Pnd_Daytime_Phone().setValue(ldaScil1280.getAccepted_App_View_Aat_Phone_Num_Work());                                           //Natural: ASSIGN #DAYTIME-PHONE := ACCEPTED-APP-VIEW.AAT-PHONE-NUM-WORK
            pdaScia1295.getPnd_Scia1295_In_Pnd_Evening_Phone().setValue(ldaScil1280.getAccepted_App_View_Aat_Phone_Num_Home());                                           //Natural: ASSIGN #EVENING-PHONE := ACCEPTED-APP-VIEW.AAT-PHONE-NUM-HOME
            pdaScia1295.getPnd_Scia1295_In_Pnd_Mail_Address_Line1().setValue(ldaScil1280.getAccepted_App_View_Aat_Addrss_Line().getValue(1));                             //Natural: ASSIGN #MAIL-ADDRESS-LINE1 := ACCEPTED-APP-VIEW.AAT-ADDRSS-LINE ( 1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Mail_Address_Line2().setValue(ldaScil1280.getAccepted_App_View_Aat_Addrss_Line().getValue(2));                             //Natural: ASSIGN #MAIL-ADDRESS-LINE2 := ACCEPTED-APP-VIEW.AAT-ADDRSS-LINE ( 2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Mail_Address_Line3().setValue(ldaScil1280.getAccepted_App_View_Aat_Addrss_Line().getValue(3));                             //Natural: ASSIGN #MAIL-ADDRESS-LINE3 := ACCEPTED-APP-VIEW.AAT-ADDRSS-LINE ( 3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Mail_Address_Line4().setValue(ldaScil1280.getAccepted_App_View_Aat_Addrss_Line().getValue(4));                             //Natural: ASSIGN #MAIL-ADDRESS-LINE4 := ACCEPTED-APP-VIEW.AAT-ADDRSS-LINE ( 4 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Mail_Address_Line5().setValue(ldaScil1280.getAccepted_App_View_Aat_Addrss_Line().getValue(5));                             //Natural: ASSIGN #MAIL-ADDRESS-LINE5 := ACCEPTED-APP-VIEW.AAT-ADDRSS-LINE ( 5 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Mail_City().setValue(ldaScil1280.getAccepted_App_View_Aat_City());                                                         //Natural: ASSIGN #MAIL-CITY := ACCEPTED-APP-VIEW.AAT-CITY
            pdaScia1295.getPnd_Scia1295_In_Pnd_Mail_State().setValue(ldaScil1280.getAccepted_App_View_Aat_State());                                                       //Natural: ASSIGN #MAIL-STATE := ACCEPTED-APP-VIEW.AAT-STATE
            pdaScia1295.getPnd_Scia1295_In_Pnd_Mail_Zip().setValue(ldaScil1280.getAccepted_App_View_Aat_Zip());                                                           //Natural: ASSIGN #MAIL-ZIP := ACCEPTED-APP-VIEW.AAT-ZIP
            pdaScia1295.getPnd_Scia1295_In_Pnd_Mail_Iso_Country_Code().setValue(ldaScil1280.getAccepted_App_View_Aat_Mail_Addr_Country_Cd());                             //Natural: ASSIGN #MAIL-ISO-COUNTRY-CODE := ACCEPTED-APP-VIEW.AAT-MAIL-ADDR-COUNTRY-CD
            pdaScia1295.getPnd_Scia1295_In_Pnd_Res_Address_Line1().setValue(ldaScil1280.getAccepted_App_View_Aat_Address_Txt().getValue(1));                              //Natural: ASSIGN #RES-ADDRESS-LINE1 := ACCEPTED-APP-VIEW.AAT-ADDRESS-TXT ( 1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Res_Address_Line2().setValue(ldaScil1280.getAccepted_App_View_Aat_Address_Txt().getValue(2));                              //Natural: ASSIGN #RES-ADDRESS-LINE2 := ACCEPTED-APP-VIEW.AAT-ADDRESS-TXT ( 2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Res_Address_Line3().setValue(ldaScil1280.getAccepted_App_View_Aat_Address_Txt().getValue(3));                              //Natural: ASSIGN #RES-ADDRESS-LINE3 := ACCEPTED-APP-VIEW.AAT-ADDRESS-TXT ( 3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Res_City().setValue(ldaScil1280.getAccepted_App_View_Aat_Address_Txt().getValue(4));                                       //Natural: ASSIGN #RES-CITY := ACCEPTED-APP-VIEW.AAT-ADDRESS-TXT ( 4 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Res_State().setValue(ldaScil1280.getAccepted_App_View_Aat_Address_Txt().getValue(5));                                      //Natural: ASSIGN #RES-STATE := ACCEPTED-APP-VIEW.AAT-ADDRESS-TXT ( 5 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Res_Zip().setValue(ldaScil1280.getAccepted_App_View_Aat_Zip_Code());                                                       //Natural: ASSIGN #RES-ZIP := ACCEPTED-APP-VIEW.AAT-ZIP-CODE
            pdaScia1295.getPnd_Scia1295_In_Pnd_Res_Iso_Country_Code().setValue(ldaScil1280.getAccepted_App_View_Aat_Na_Code());                                           //Natural: ASSIGN #RES-ISO-COUNTRY-CODE := ACCEPTED-APP-VIEW.AAT-NA-CODE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        ldaScil1280.getVw_bene_View().startDatabaseFind                                                                                                                   //Natural: FIND BENE-VIEW WITH BENE-ORCHESTRATION-ID = #ORCHESTRATION-ID WHERE BENE-VIEW.BENE-PART-SSN = #HOLD-SSN
        (
        "FIND02",
        new Wc[] { new Wc("BENE_ORCHESTRATION_ID", "=", pdaScia1295.getPnd_Scia1295_In_Pnd_Orchestration_Id(), WcType.WITH) ,
        new Wc("BENE_PART_SSN", "=", pnd_Hold_Ssn, WcType.WHERE) }
        );
        FIND02:
        while (condition(ldaScil1280.getVw_bene_View().readNextRow("FIND02", true)))
        {
            ldaScil1280.getVw_bene_View().setIfNotFoundControlFlag(false);
            if (condition(ldaScil1280.getVw_bene_View().getAstCOUNTER().equals(0)))                                                                                       //Natural: IF NO RECORDS FOUND
            {
                pdaScia1295.getPnd_Scia1295_Out_Pnd_Response_Code().setValue("0311");                                                                                     //Natural: ASSIGN #RESPONSE-CODE := '0311'
                pdaScia1295.getPnd_Scia1295_Out_Pnd_Response_Text().setValue(DbsUtil.compress("NO RECS FOUND BENE FILE FOR ORCHESTRATION-ID: ", pdaScia1295.getPnd_Scia1295_In_Pnd_Orchestration_Id())); //Natural: COMPRESS 'NO RECS FOUND BENE FILE FOR ORCHESTRATION-ID: ' #ORCHESTRATION-ID INTO #RESPONSE-TEXT
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            pdaScia1295.getPnd_Scia1295_In_Pnd_Default_To_Estate().setValue(ldaScil1280.getBene_View_Bene_Estate());                                                      //Natural: ASSIGN #DEFAULT-TO-ESTATE := BENE-VIEW.BENE-ESTATE
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Type().setValue(ldaScil1280.getBene_View_Bene_Type().getValue(1));                                                   //Natural: ASSIGN #BENE1-TYPE := BENE-VIEW.BENE-TYPE ( 1 )
            if (condition(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(1).greater(getZero())))                                                                        //Natural: IF BENE-VIEW.BENE-SSN-NBR ( 1 ) > 0
            {
                pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Ssn().setValueEdited(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(1),new ReportEditMask("999999999"));       //Natural: MOVE EDITED BENE-VIEW.BENE-SSN-NBR ( 1 ) ( EM = 999999999 ) TO #BENE1-SSN
                //*  1IRA
                //*  1IRA
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Name().setValue(ldaScil1280.getBene_View_Bene_Name().getValue(1));                                                   //Natural: ASSIGN #BENE1-NAME := BENE-VIEW.BENE-NAME ( 1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Name2().setValue(ldaScil1280.getBene_View_Bene_Extended_Name().getValue(1));                                         //Natural: ASSIGN #BENE1-NAME2 := BENE-VIEW.BENE-EXTENDED-NAME ( 1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Rel_Code().setValue(ldaScil1280.getBene_View_Bene_Relationship_Cde().getValue(1));                                   //Natural: ASSIGN #BENE1-REL-CODE := BENE-VIEW.BENE-RELATIONSHIP-CDE ( 1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Rel_Desc().setValue(ldaScil1280.getBene_View_Bene_Relationship().getValue(1));                                       //Natural: ASSIGN #BENE1-REL-DESC := BENE-VIEW.BENE-RELATIONSHIP ( 1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Birthdate().setValue(ldaScil1280.getBene_View_Bene_Dob().getValue(1));                                               //Natural: ASSIGN #BENE1-BIRTHDATE := BENE-VIEW.BENE-DOB ( 1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Text_Line1().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(1,1));                                       //Natural: ASSIGN #BENE1-TEXT-LINE1 := BENE-VIEW.BENE-SPCL-TXT ( 1,1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Text_Line2().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(1,2));                                       //Natural: ASSIGN #BENE1-TEXT-LINE2 := BENE-VIEW.BENE-SPCL-TXT ( 1,2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Text_Line3().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(1,3));                                       //Natural: ASSIGN #BENE1-TEXT-LINE3 := BENE-VIEW.BENE-SPCL-TXT ( 1,3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Alloc_Method().setValue(ldaScil1280.getBene_View_Bene_Std_Txt_Ind().getValue(1));                                    //Natural: ASSIGN #BENE1-ALLOC-METHOD := BENE-VIEW.BENE-STD-TXT-IND ( 1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Alloc_Pct().setValue(ldaScil1280.getBene_View_Bene_Alloc_Pct().getValue(1));                                         //Natural: ASSIGN #BENE1-ALLOC-PCT := BENE-VIEW.BENE-ALLOC-PCT ( 1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Alloc_Num().setValue(ldaScil1280.getBene_View_Bene_Nmrtr_Nbr().getValue(1));                                         //Natural: ASSIGN #BENE1-ALLOC-NUM := BENE-VIEW.BENE-NMRTR-NBR ( 1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Alloc_Denum().setValue(ldaScil1280.getBene_View_Bene_Dnmntr_Nbr().getValue(1));                                      //Natural: ASSIGN #BENE1-ALLOC-DENUM := BENE-VIEW.BENE-DNMNTR-NBR ( 1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Addr_Line_1().setValue(ldaScil1280.getBene_View_Bene_Addr1().getValue(1));                                           //Natural: ASSIGN #BENE1-ADDR-LINE-1 := BENE-VIEW.BENE-ADDR1 ( 1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Addr_Line_2().setValue(ldaScil1280.getBene_View_Bene_Addr2().getValue(1));                                           //Natural: ASSIGN #BENE1-ADDR-LINE-2 := BENE-VIEW.BENE-ADDR2 ( 1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Addr_Line_3_City().setValue(ldaScil1280.getBene_View_Bene_Addr3_City().getValue(1));                                 //Natural: ASSIGN #BENE1-ADDR-LINE-3-CITY := BENE-VIEW.BENE-ADDR3-CITY ( 1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_State().setValue(ldaScil1280.getBene_View_Bene_State().getValue(1));                                                 //Natural: ASSIGN #BENE1-STATE := BENE-VIEW.BENE-STATE ( 1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Zip().setValue(ldaScil1280.getBene_View_Bene_Zip().getValue(1));                                                     //Natural: ASSIGN #BENE1-ZIP := BENE-VIEW.BENE-ZIP ( 1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Phone().setValue(ldaScil1280.getBene_View_Bene_Phone().getValue(1));                                                 //Natural: ASSIGN #BENE1-PHONE := BENE-VIEW.BENE-PHONE ( 1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Gender().setValue(ldaScil1280.getBene_View_Bene_Gender().getValue(1));                                               //Natural: ASSIGN #BENE1-GENDER := BENE-VIEW.BENE-GENDER ( 1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Country().setValue(ldaScil1280.getBene_View_Bene_Country().getValue(1));                                             //Natural: ASSIGN #BENE1-COUNTRY := BENE-VIEW.BENE-COUNTRY ( 1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Type().setValue(ldaScil1280.getBene_View_Bene_Type().getValue(2));                                                   //Natural: ASSIGN #BENE2-TYPE := BENE-VIEW.BENE-TYPE ( 2 )
            if (condition(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(2).greater(getZero())))                                                                        //Natural: IF BENE-VIEW.BENE-SSN-NBR ( 2 ) > 0
            {
                pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Ssn().setValueEdited(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(2),new ReportEditMask("999999999"));       //Natural: MOVE EDITED BENE-VIEW.BENE-SSN-NBR ( 2 ) ( EM = 999999999 ) TO #BENE2-SSN
                //*  1IRA
                //*  1IRA
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Name().setValue(ldaScil1280.getBene_View_Bene_Name().getValue(2));                                                   //Natural: ASSIGN #BENE2-NAME := BENE-VIEW.BENE-NAME ( 2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Name2().setValue(ldaScil1280.getBene_View_Bene_Extended_Name().getValue(2));                                         //Natural: ASSIGN #BENE2-NAME2 := BENE-VIEW.BENE-EXTENDED-NAME ( 2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Rel_Code().setValue(ldaScil1280.getBene_View_Bene_Relationship_Cde().getValue(2));                                   //Natural: ASSIGN #BENE2-REL-CODE := BENE-VIEW.BENE-RELATIONSHIP-CDE ( 2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Rel_Desc().setValue(ldaScil1280.getBene_View_Bene_Relationship().getValue(2));                                       //Natural: ASSIGN #BENE2-REL-DESC := BENE-VIEW.BENE-RELATIONSHIP ( 2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Birthdate().setValue(ldaScil1280.getBene_View_Bene_Dob().getValue(2));                                               //Natural: ASSIGN #BENE2-BIRTHDATE := BENE-VIEW.BENE-DOB ( 2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Text_Line1().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(2,1));                                       //Natural: ASSIGN #BENE2-TEXT-LINE1 := BENE-VIEW.BENE-SPCL-TXT ( 2,1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Text_Line2().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(2,2));                                       //Natural: ASSIGN #BENE2-TEXT-LINE2 := BENE-VIEW.BENE-SPCL-TXT ( 2,2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Text_Line3().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(2,3));                                       //Natural: ASSIGN #BENE2-TEXT-LINE3 := BENE-VIEW.BENE-SPCL-TXT ( 2,3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Alloc_Method().setValue(ldaScil1280.getBene_View_Bene_Std_Txt_Ind().getValue(2));                                    //Natural: ASSIGN #BENE2-ALLOC-METHOD := BENE-VIEW.BENE-STD-TXT-IND ( 2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Alloc_Pct().setValue(ldaScil1280.getBene_View_Bene_Alloc_Pct().getValue(2));                                         //Natural: ASSIGN #BENE2-ALLOC-PCT := BENE-VIEW.BENE-ALLOC-PCT ( 2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Alloc_Num().setValue(ldaScil1280.getBene_View_Bene_Nmrtr_Nbr().getValue(2));                                         //Natural: ASSIGN #BENE2-ALLOC-NUM := BENE-VIEW.BENE-NMRTR-NBR ( 2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Alloc_Denum().setValue(ldaScil1280.getBene_View_Bene_Dnmntr_Nbr().getValue(2));                                      //Natural: ASSIGN #BENE2-ALLOC-DENUM := BENE-VIEW.BENE-DNMNTR-NBR ( 2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Addr_Line_1().setValue(ldaScil1280.getBene_View_Bene_Addr1().getValue(2));                                           //Natural: ASSIGN #BENE2-ADDR-LINE-1 := BENE-VIEW.BENE-ADDR1 ( 2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Addr_Line_2().setValue(ldaScil1280.getBene_View_Bene_Addr2().getValue(2));                                           //Natural: ASSIGN #BENE2-ADDR-LINE-2 := BENE-VIEW.BENE-ADDR2 ( 2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Addr_Line_3_City().setValue(ldaScil1280.getBene_View_Bene_Addr3_City().getValue(2));                                 //Natural: ASSIGN #BENE2-ADDR-LINE-3-CITY := BENE-VIEW.BENE-ADDR3-CITY ( 2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_State().setValue(ldaScil1280.getBene_View_Bene_State().getValue(2));                                                 //Natural: ASSIGN #BENE2-STATE := BENE-VIEW.BENE-STATE ( 2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Zip().setValue(ldaScil1280.getBene_View_Bene_Zip().getValue(2));                                                     //Natural: ASSIGN #BENE2-ZIP := BENE-VIEW.BENE-ZIP ( 2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Phone().setValue(ldaScil1280.getBene_View_Bene_Phone().getValue(2));                                                 //Natural: ASSIGN #BENE2-PHONE := BENE-VIEW.BENE-PHONE ( 2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Gender().setValue(ldaScil1280.getBene_View_Bene_Gender().getValue(2));                                               //Natural: ASSIGN #BENE2-GENDER := BENE-VIEW.BENE-GENDER ( 2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Country().setValue(ldaScil1280.getBene_View_Bene_Country().getValue(2));                                             //Natural: ASSIGN #BENE2-COUNTRY := BENE-VIEW.BENE-COUNTRY ( 2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Type().setValue(ldaScil1280.getBene_View_Bene_Type().getValue(3));                                                   //Natural: ASSIGN #BENE3-TYPE := BENE-VIEW.BENE-TYPE ( 3 )
            if (condition(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(3).greater(getZero())))                                                                        //Natural: IF BENE-VIEW.BENE-SSN-NBR ( 3 ) > 0
            {
                pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Ssn().setValueEdited(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(3),new ReportEditMask("999999999"));       //Natural: MOVE EDITED BENE-VIEW.BENE-SSN-NBR ( 3 ) ( EM = 999999999 ) TO #BENE3-SSN
                //*  1IRA
                //*  1IRA
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Name().setValue(ldaScil1280.getBene_View_Bene_Name().getValue(3));                                                   //Natural: ASSIGN #BENE3-NAME := BENE-VIEW.BENE-NAME ( 3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Name2().setValue(ldaScil1280.getBene_View_Bene_Extended_Name().getValue(3));                                         //Natural: ASSIGN #BENE3-NAME2 := BENE-VIEW.BENE-EXTENDED-NAME ( 3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Rel_Code().setValue(ldaScil1280.getBene_View_Bene_Relationship_Cde().getValue(3));                                   //Natural: ASSIGN #BENE3-REL-CODE := BENE-VIEW.BENE-RELATIONSHIP-CDE ( 3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Rel_Desc().setValue(ldaScil1280.getBene_View_Bene_Relationship().getValue(3));                                       //Natural: ASSIGN #BENE3-REL-DESC := BENE-VIEW.BENE-RELATIONSHIP ( 3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Birthdate().setValue(ldaScil1280.getBene_View_Bene_Dob().getValue(3));                                               //Natural: ASSIGN #BENE3-BIRTHDATE := BENE-VIEW.BENE-DOB ( 3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Text_Line1().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(3,1));                                       //Natural: ASSIGN #BENE3-TEXT-LINE1 := BENE-VIEW.BENE-SPCL-TXT ( 3,1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Text_Line2().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(3,2));                                       //Natural: ASSIGN #BENE3-TEXT-LINE2 := BENE-VIEW.BENE-SPCL-TXT ( 3,2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Text_Line3().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(3,3));                                       //Natural: ASSIGN #BENE3-TEXT-LINE3 := BENE-VIEW.BENE-SPCL-TXT ( 3,3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Alloc_Method().setValue(ldaScil1280.getBene_View_Bene_Std_Txt_Ind().getValue(3));                                    //Natural: ASSIGN #BENE3-ALLOC-METHOD := BENE-VIEW.BENE-STD-TXT-IND ( 3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Alloc_Pct().setValue(ldaScil1280.getBene_View_Bene_Alloc_Pct().getValue(3));                                         //Natural: ASSIGN #BENE3-ALLOC-PCT := BENE-VIEW.BENE-ALLOC-PCT ( 3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Alloc_Num().setValue(ldaScil1280.getBene_View_Bene_Nmrtr_Nbr().getValue(3));                                         //Natural: ASSIGN #BENE3-ALLOC-NUM := BENE-VIEW.BENE-NMRTR-NBR ( 3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Alloc_Denum().setValue(ldaScil1280.getBene_View_Bene_Dnmntr_Nbr().getValue(3));                                      //Natural: ASSIGN #BENE3-ALLOC-DENUM := BENE-VIEW.BENE-DNMNTR-NBR ( 3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Addr_Line_1().setValue(ldaScil1280.getBene_View_Bene_Addr1().getValue(3));                                           //Natural: ASSIGN #BENE3-ADDR-LINE-1 := BENE-VIEW.BENE-ADDR1 ( 3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Addr_Line_2().setValue(ldaScil1280.getBene_View_Bene_Addr2().getValue(3));                                           //Natural: ASSIGN #BENE3-ADDR-LINE-2 := BENE-VIEW.BENE-ADDR2 ( 3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Addr_Line_3_City().setValue(ldaScil1280.getBene_View_Bene_Addr3_City().getValue(3));                                 //Natural: ASSIGN #BENE3-ADDR-LINE-3-CITY := BENE-VIEW.BENE-ADDR3-CITY ( 3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_State().setValue(ldaScil1280.getBene_View_Bene_State().getValue(3));                                                 //Natural: ASSIGN #BENE3-STATE := BENE-VIEW.BENE-STATE ( 3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Zip().setValue(ldaScil1280.getBene_View_Bene_Zip().getValue(3));                                                     //Natural: ASSIGN #BENE3-ZIP := BENE-VIEW.BENE-ZIP ( 3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Phone().setValue(ldaScil1280.getBene_View_Bene_Phone().getValue(3));                                                 //Natural: ASSIGN #BENE3-PHONE := BENE-VIEW.BENE-PHONE ( 3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Gender().setValue(ldaScil1280.getBene_View_Bene_Gender().getValue(3));                                               //Natural: ASSIGN #BENE3-GENDER := BENE-VIEW.BENE-GENDER ( 3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Country().setValue(ldaScil1280.getBene_View_Bene_Country().getValue(3));                                             //Natural: ASSIGN #BENE3-COUNTRY := BENE-VIEW.BENE-COUNTRY ( 3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Type().setValue(ldaScil1280.getBene_View_Bene_Type().getValue(4));                                                   //Natural: ASSIGN #BENE4-TYPE := BENE-VIEW.BENE-TYPE ( 4 )
            if (condition(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(4).greater(getZero())))                                                                        //Natural: IF BENE-VIEW.BENE-SSN-NBR ( 4 ) > 0
            {
                pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Ssn().setValueEdited(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(4),new ReportEditMask("999999999"));       //Natural: MOVE EDITED BENE-VIEW.BENE-SSN-NBR ( 4 ) ( EM = 999999999 ) TO #BENE4-SSN
                //*  1IRA
                //*  1IRA
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Name().setValue(ldaScil1280.getBene_View_Bene_Name().getValue(4));                                                   //Natural: ASSIGN #BENE4-NAME := BENE-VIEW.BENE-NAME ( 4 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Name2().setValue(ldaScil1280.getBene_View_Bene_Extended_Name().getValue(4));                                         //Natural: ASSIGN #BENE4-NAME2 := BENE-VIEW.BENE-EXTENDED-NAME ( 4 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Rel_Code().setValue(ldaScil1280.getBene_View_Bene_Relationship_Cde().getValue(4));                                   //Natural: ASSIGN #BENE4-REL-CODE := BENE-VIEW.BENE-RELATIONSHIP-CDE ( 4 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Rel_Desc().setValue(ldaScil1280.getBene_View_Bene_Relationship().getValue(4));                                       //Natural: ASSIGN #BENE4-REL-DESC := BENE-VIEW.BENE-RELATIONSHIP ( 4 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Birthdate().setValue(ldaScil1280.getBene_View_Bene_Dob().getValue(4));                                               //Natural: ASSIGN #BENE4-BIRTHDATE := BENE-VIEW.BENE-DOB ( 4 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Text_Line1().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(4,1));                                       //Natural: ASSIGN #BENE4-TEXT-LINE1 := BENE-VIEW.BENE-SPCL-TXT ( 4,1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Text_Line2().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(4,2));                                       //Natural: ASSIGN #BENE4-TEXT-LINE2 := BENE-VIEW.BENE-SPCL-TXT ( 4,2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Text_Line3().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(4,3));                                       //Natural: ASSIGN #BENE4-TEXT-LINE3 := BENE-VIEW.BENE-SPCL-TXT ( 4,3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Alloc_Method().setValue(ldaScil1280.getBene_View_Bene_Std_Txt_Ind().getValue(4));                                    //Natural: ASSIGN #BENE4-ALLOC-METHOD := BENE-VIEW.BENE-STD-TXT-IND ( 4 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Alloc_Pct().setValue(ldaScil1280.getBene_View_Bene_Alloc_Pct().getValue(4));                                         //Natural: ASSIGN #BENE4-ALLOC-PCT := BENE-VIEW.BENE-ALLOC-PCT ( 4 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Alloc_Num().setValue(ldaScil1280.getBene_View_Bene_Nmrtr_Nbr().getValue(4));                                         //Natural: ASSIGN #BENE4-ALLOC-NUM := BENE-VIEW.BENE-NMRTR-NBR ( 4 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Alloc_Denum().setValue(ldaScil1280.getBene_View_Bene_Dnmntr_Nbr().getValue(4));                                      //Natural: ASSIGN #BENE4-ALLOC-DENUM := BENE-VIEW.BENE-DNMNTR-NBR ( 4 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Addr_Line_1().setValue(ldaScil1280.getBene_View_Bene_Addr1().getValue(4));                                           //Natural: ASSIGN #BENE4-ADDR-LINE-1 := BENE-VIEW.BENE-ADDR1 ( 4 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Addr_Line_2().setValue(ldaScil1280.getBene_View_Bene_Addr2().getValue(4));                                           //Natural: ASSIGN #BENE4-ADDR-LINE-2 := BENE-VIEW.BENE-ADDR2 ( 4 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Addr_Line_3_City().setValue(ldaScil1280.getBene_View_Bene_Addr3_City().getValue(4));                                 //Natural: ASSIGN #BENE4-ADDR-LINE-3-CITY := BENE-VIEW.BENE-ADDR3-CITY ( 4 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_State().setValue(ldaScil1280.getBene_View_Bene_State().getValue(4));                                                 //Natural: ASSIGN #BENE4-STATE := BENE-VIEW.BENE-STATE ( 4 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Zip().setValue(ldaScil1280.getBene_View_Bene_Zip().getValue(4));                                                     //Natural: ASSIGN #BENE4-ZIP := BENE-VIEW.BENE-ZIP ( 4 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Phone().setValue(ldaScil1280.getBene_View_Bene_Phone().getValue(4));                                                 //Natural: ASSIGN #BENE4-PHONE := BENE-VIEW.BENE-PHONE ( 4 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Gender().setValue(ldaScil1280.getBene_View_Bene_Gender().getValue(4));                                               //Natural: ASSIGN #BENE4-GENDER := BENE-VIEW.BENE-GENDER ( 4 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Country().setValue(ldaScil1280.getBene_View_Bene_Country().getValue(4));                                             //Natural: ASSIGN #BENE4-COUNTRY := BENE-VIEW.BENE-COUNTRY ( 4 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Type().setValue(ldaScil1280.getBene_View_Bene_Type().getValue(5));                                                   //Natural: ASSIGN #BENE5-TYPE := BENE-VIEW.BENE-TYPE ( 5 )
            if (condition(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(5).greater(getZero())))                                                                        //Natural: IF BENE-VIEW.BENE-SSN-NBR ( 5 ) > 0
            {
                pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Ssn().setValueEdited(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(5),new ReportEditMask("999999999"));       //Natural: MOVE EDITED BENE-VIEW.BENE-SSN-NBR ( 5 ) ( EM = 999999999 ) TO #BENE5-SSN
                //*  1IRA
                //*  1IRA
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Name().setValue(ldaScil1280.getBene_View_Bene_Name().getValue(5));                                                   //Natural: ASSIGN #BENE5-NAME := BENE-VIEW.BENE-NAME ( 5 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Name2().setValue(ldaScil1280.getBene_View_Bene_Extended_Name().getValue(5));                                         //Natural: ASSIGN #BENE5-NAME2 := BENE-VIEW.BENE-EXTENDED-NAME ( 5 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Rel_Code().setValue(ldaScil1280.getBene_View_Bene_Relationship_Cde().getValue(5));                                   //Natural: ASSIGN #BENE5-REL-CODE := BENE-VIEW.BENE-RELATIONSHIP-CDE ( 5 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Rel_Desc().setValue(ldaScil1280.getBene_View_Bene_Relationship().getValue(5));                                       //Natural: ASSIGN #BENE5-REL-DESC := BENE-VIEW.BENE-RELATIONSHIP ( 5 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Birthdate().setValue(ldaScil1280.getBene_View_Bene_Dob().getValue(5));                                               //Natural: ASSIGN #BENE5-BIRTHDATE := BENE-VIEW.BENE-DOB ( 5 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Text_Line1().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(5,1));                                       //Natural: ASSIGN #BENE5-TEXT-LINE1 := BENE-VIEW.BENE-SPCL-TXT ( 5,1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Text_Line2().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(5,2));                                       //Natural: ASSIGN #BENE5-TEXT-LINE2 := BENE-VIEW.BENE-SPCL-TXT ( 5,2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Text_Line3().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(5,3));                                       //Natural: ASSIGN #BENE5-TEXT-LINE3 := BENE-VIEW.BENE-SPCL-TXT ( 5,3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Alloc_Method().setValue(ldaScil1280.getBene_View_Bene_Std_Txt_Ind().getValue(5));                                    //Natural: ASSIGN #BENE5-ALLOC-METHOD := BENE-VIEW.BENE-STD-TXT-IND ( 5 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Alloc_Pct().setValue(ldaScil1280.getBene_View_Bene_Alloc_Pct().getValue(5));                                         //Natural: ASSIGN #BENE5-ALLOC-PCT := BENE-VIEW.BENE-ALLOC-PCT ( 5 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Alloc_Num().setValue(ldaScil1280.getBene_View_Bene_Nmrtr_Nbr().getValue(5));                                         //Natural: ASSIGN #BENE5-ALLOC-NUM := BENE-VIEW.BENE-NMRTR-NBR ( 5 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Alloc_Denum().setValue(ldaScil1280.getBene_View_Bene_Dnmntr_Nbr().getValue(5));                                      //Natural: ASSIGN #BENE5-ALLOC-DENUM := BENE-VIEW.BENE-DNMNTR-NBR ( 5 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Addr_Line_1().setValue(ldaScil1280.getBene_View_Bene_Addr1().getValue(5));                                           //Natural: ASSIGN #BENE5-ADDR-LINE-1 := BENE-VIEW.BENE-ADDR1 ( 5 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Addr_Line_2().setValue(ldaScil1280.getBene_View_Bene_Addr2().getValue(5));                                           //Natural: ASSIGN #BENE5-ADDR-LINE-2 := BENE-VIEW.BENE-ADDR2 ( 5 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Addr_Line_3_City().setValue(ldaScil1280.getBene_View_Bene_Addr3_City().getValue(5));                                 //Natural: ASSIGN #BENE5-ADDR-LINE-3-CITY := BENE-VIEW.BENE-ADDR3-CITY ( 5 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_State().setValue(ldaScil1280.getBene_View_Bene_State().getValue(5));                                                 //Natural: ASSIGN #BENE5-STATE := BENE-VIEW.BENE-STATE ( 5 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Zip().setValue(ldaScil1280.getBene_View_Bene_Zip().getValue(5));                                                     //Natural: ASSIGN #BENE5-ZIP := BENE-VIEW.BENE-ZIP ( 5 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Phone().setValue(ldaScil1280.getBene_View_Bene_Phone().getValue(5));                                                 //Natural: ASSIGN #BENE5-PHONE := BENE-VIEW.BENE-PHONE ( 5 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Gender().setValue(ldaScil1280.getBene_View_Bene_Gender().getValue(5));                                               //Natural: ASSIGN #BENE5-GENDER := BENE-VIEW.BENE-GENDER ( 5 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Country().setValue(ldaScil1280.getBene_View_Bene_Country().getValue(5));                                             //Natural: ASSIGN #BENE5-COUNTRY := BENE-VIEW.BENE-COUNTRY ( 5 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Type().setValue(ldaScil1280.getBene_View_Bene_Type().getValue(6));                                                   //Natural: ASSIGN #BENE6-TYPE := BENE-VIEW.BENE-TYPE ( 6 )
            if (condition(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(6).greater(getZero())))                                                                        //Natural: IF BENE-VIEW.BENE-SSN-NBR ( 6 ) > 0
            {
                pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Ssn().setValueEdited(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(6),new ReportEditMask("999999999"));       //Natural: MOVE EDITED BENE-VIEW.BENE-SSN-NBR ( 6 ) ( EM = 999999999 ) TO #BENE6-SSN
                //*  1IRA
                //*  1IRA
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Name().setValue(ldaScil1280.getBene_View_Bene_Name().getValue(6));                                                   //Natural: ASSIGN #BENE6-NAME := BENE-VIEW.BENE-NAME ( 6 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Name2().setValue(ldaScil1280.getBene_View_Bene_Extended_Name().getValue(6));                                         //Natural: ASSIGN #BENE6-NAME2 := BENE-VIEW.BENE-EXTENDED-NAME ( 6 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Rel_Code().setValue(ldaScil1280.getBene_View_Bene_Relationship_Cde().getValue(6));                                   //Natural: ASSIGN #BENE6-REL-CODE := BENE-VIEW.BENE-RELATIONSHIP-CDE ( 6 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Rel_Desc().setValue(ldaScil1280.getBene_View_Bene_Relationship().getValue(6));                                       //Natural: ASSIGN #BENE6-REL-DESC := BENE-VIEW.BENE-RELATIONSHIP ( 6 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Birthdate().setValue(ldaScil1280.getBene_View_Bene_Dob().getValue(6));                                               //Natural: ASSIGN #BENE6-BIRTHDATE := BENE-VIEW.BENE-DOB ( 6 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Text_Line1().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(6,1));                                       //Natural: ASSIGN #BENE6-TEXT-LINE1 := BENE-VIEW.BENE-SPCL-TXT ( 6,1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Text_Line2().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(6,2));                                       //Natural: ASSIGN #BENE6-TEXT-LINE2 := BENE-VIEW.BENE-SPCL-TXT ( 6,2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Text_Line3().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(6,3));                                       //Natural: ASSIGN #BENE6-TEXT-LINE3 := BENE-VIEW.BENE-SPCL-TXT ( 6,3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Alloc_Method().setValue(ldaScil1280.getBene_View_Bene_Std_Txt_Ind().getValue(6));                                    //Natural: ASSIGN #BENE6-ALLOC-METHOD := BENE-VIEW.BENE-STD-TXT-IND ( 6 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Alloc_Pct().setValue(ldaScil1280.getBene_View_Bene_Alloc_Pct().getValue(6));                                         //Natural: ASSIGN #BENE6-ALLOC-PCT := BENE-VIEW.BENE-ALLOC-PCT ( 6 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Alloc_Num().setValue(ldaScil1280.getBene_View_Bene_Nmrtr_Nbr().getValue(6));                                         //Natural: ASSIGN #BENE6-ALLOC-NUM := BENE-VIEW.BENE-NMRTR-NBR ( 6 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Alloc_Denum().setValue(ldaScil1280.getBene_View_Bene_Dnmntr_Nbr().getValue(6));                                      //Natural: ASSIGN #BENE6-ALLOC-DENUM := BENE-VIEW.BENE-DNMNTR-NBR ( 6 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Addr_Line_1().setValue(ldaScil1280.getBene_View_Bene_Addr1().getValue(6));                                           //Natural: ASSIGN #BENE6-ADDR-LINE-1 := BENE-VIEW.BENE-ADDR1 ( 6 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Addr_Line_2().setValue(ldaScil1280.getBene_View_Bene_Addr2().getValue(6));                                           //Natural: ASSIGN #BENE6-ADDR-LINE-2 := BENE-VIEW.BENE-ADDR2 ( 6 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Addr_Line_3_City().setValue(ldaScil1280.getBene_View_Bene_Addr3_City().getValue(6));                                 //Natural: ASSIGN #BENE6-ADDR-LINE-3-CITY := BENE-VIEW.BENE-ADDR3-CITY ( 6 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_State().setValue(ldaScil1280.getBene_View_Bene_State().getValue(6));                                                 //Natural: ASSIGN #BENE6-STATE := BENE-VIEW.BENE-STATE ( 6 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Zip().setValue(ldaScil1280.getBene_View_Bene_Zip().getValue(6));                                                     //Natural: ASSIGN #BENE6-ZIP := BENE-VIEW.BENE-ZIP ( 6 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Phone().setValue(ldaScil1280.getBene_View_Bene_Phone().getValue(6));                                                 //Natural: ASSIGN #BENE6-PHONE := BENE-VIEW.BENE-PHONE ( 6 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Gender().setValue(ldaScil1280.getBene_View_Bene_Gender().getValue(6));                                               //Natural: ASSIGN #BENE6-GENDER := BENE-VIEW.BENE-GENDER ( 6 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Country().setValue(ldaScil1280.getBene_View_Bene_Country().getValue(6));                                             //Natural: ASSIGN #BENE6-COUNTRY := BENE-VIEW.BENE-COUNTRY ( 6 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Type().setValue(ldaScil1280.getBene_View_Bene_Type().getValue(7));                                                   //Natural: ASSIGN #BENE7-TYPE := BENE-VIEW.BENE-TYPE ( 7 )
            if (condition(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(7).greater(getZero())))                                                                        //Natural: IF BENE-VIEW.BENE-SSN-NBR ( 7 ) > 0
            {
                pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Ssn().setValueEdited(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(7),new ReportEditMask("999999999"));       //Natural: MOVE EDITED BENE-VIEW.BENE-SSN-NBR ( 7 ) ( EM = 999999999 ) TO #BENE7-SSN
                //*  1IRA
                //*  1IRA
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Name().setValue(ldaScil1280.getBene_View_Bene_Name().getValue(7));                                                   //Natural: ASSIGN #BENE7-NAME := BENE-VIEW.BENE-NAME ( 7 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Name2().setValue(ldaScil1280.getBene_View_Bene_Extended_Name().getValue(7));                                         //Natural: ASSIGN #BENE7-NAME2 := BENE-VIEW.BENE-EXTENDED-NAME ( 7 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Rel_Code().setValue(ldaScil1280.getBene_View_Bene_Relationship_Cde().getValue(7));                                   //Natural: ASSIGN #BENE7-REL-CODE := BENE-VIEW.BENE-RELATIONSHIP-CDE ( 7 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Rel_Desc().setValue(ldaScil1280.getBene_View_Bene_Relationship().getValue(7));                                       //Natural: ASSIGN #BENE7-REL-DESC := BENE-VIEW.BENE-RELATIONSHIP ( 7 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Birthdate().setValue(ldaScil1280.getBene_View_Bene_Dob().getValue(7));                                               //Natural: ASSIGN #BENE7-BIRTHDATE := BENE-VIEW.BENE-DOB ( 7 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Text_Line1().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(7,1));                                       //Natural: ASSIGN #BENE7-TEXT-LINE1 := BENE-VIEW.BENE-SPCL-TXT ( 7,1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Text_Line2().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(7,2));                                       //Natural: ASSIGN #BENE7-TEXT-LINE2 := BENE-VIEW.BENE-SPCL-TXT ( 7,2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Text_Line3().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(7,3));                                       //Natural: ASSIGN #BENE7-TEXT-LINE3 := BENE-VIEW.BENE-SPCL-TXT ( 7,3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Alloc_Method().setValue(ldaScil1280.getBene_View_Bene_Std_Txt_Ind().getValue(7));                                    //Natural: ASSIGN #BENE7-ALLOC-METHOD := BENE-VIEW.BENE-STD-TXT-IND ( 7 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Alloc_Pct().setValue(ldaScil1280.getBene_View_Bene_Alloc_Pct().getValue(7));                                         //Natural: ASSIGN #BENE7-ALLOC-PCT := BENE-VIEW.BENE-ALLOC-PCT ( 7 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Alloc_Num().setValue(ldaScil1280.getBene_View_Bene_Nmrtr_Nbr().getValue(7));                                         //Natural: ASSIGN #BENE7-ALLOC-NUM := BENE-VIEW.BENE-NMRTR-NBR ( 7 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Alloc_Denum().setValue(ldaScil1280.getBene_View_Bene_Dnmntr_Nbr().getValue(7));                                      //Natural: ASSIGN #BENE7-ALLOC-DENUM := BENE-VIEW.BENE-DNMNTR-NBR ( 7 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Addr_Line_1().setValue(ldaScil1280.getBene_View_Bene_Addr1().getValue(7));                                           //Natural: ASSIGN #BENE7-ADDR-LINE-1 := BENE-VIEW.BENE-ADDR1 ( 7 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Addr_Line_2().setValue(ldaScil1280.getBene_View_Bene_Addr2().getValue(7));                                           //Natural: ASSIGN #BENE7-ADDR-LINE-2 := BENE-VIEW.BENE-ADDR2 ( 7 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Addr_Line_3_City().setValue(ldaScil1280.getBene_View_Bene_Addr3_City().getValue(7));                                 //Natural: ASSIGN #BENE7-ADDR-LINE-3-CITY := BENE-VIEW.BENE-ADDR3-CITY ( 7 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_State().setValue(ldaScil1280.getBene_View_Bene_State().getValue(7));                                                 //Natural: ASSIGN #BENE7-STATE := BENE-VIEW.BENE-STATE ( 7 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Zip().setValue(ldaScil1280.getBene_View_Bene_Zip().getValue(7));                                                     //Natural: ASSIGN #BENE7-ZIP := BENE-VIEW.BENE-ZIP ( 7 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Phone().setValue(ldaScil1280.getBene_View_Bene_Phone().getValue(7));                                                 //Natural: ASSIGN #BENE7-PHONE := BENE-VIEW.BENE-PHONE ( 7 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Gender().setValue(ldaScil1280.getBene_View_Bene_Gender().getValue(7));                                               //Natural: ASSIGN #BENE7-GENDER := BENE-VIEW.BENE-GENDER ( 7 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Country().setValue(ldaScil1280.getBene_View_Bene_Country().getValue(7));                                             //Natural: ASSIGN #BENE7-COUNTRY := BENE-VIEW.BENE-COUNTRY ( 7 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Type().setValue(ldaScil1280.getBene_View_Bene_Type().getValue(8));                                                   //Natural: ASSIGN #BENE8-TYPE := BENE-VIEW.BENE-TYPE ( 8 )
            if (condition(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(8).greater(getZero())))                                                                        //Natural: IF BENE-VIEW.BENE-SSN-NBR ( 8 ) > 0
            {
                pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Ssn().setValueEdited(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(8),new ReportEditMask("999999999"));       //Natural: MOVE EDITED BENE-VIEW.BENE-SSN-NBR ( 8 ) ( EM = 999999999 ) TO #BENE8-SSN
                //*  1IRA
                //*  1IRA
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Name().setValue(ldaScil1280.getBene_View_Bene_Name().getValue(8));                                                   //Natural: ASSIGN #BENE8-NAME := BENE-VIEW.BENE-NAME ( 8 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Name2().setValue(ldaScil1280.getBene_View_Bene_Extended_Name().getValue(8));                                         //Natural: ASSIGN #BENE8-NAME2 := BENE-VIEW.BENE-EXTENDED-NAME ( 8 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Rel_Code().setValue(ldaScil1280.getBene_View_Bene_Relationship_Cde().getValue(8));                                   //Natural: ASSIGN #BENE8-REL-CODE := BENE-VIEW.BENE-RELATIONSHIP-CDE ( 8 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Rel_Desc().setValue(ldaScil1280.getBene_View_Bene_Relationship().getValue(8));                                       //Natural: ASSIGN #BENE8-REL-DESC := BENE-VIEW.BENE-RELATIONSHIP ( 8 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Birthdate().setValue(ldaScil1280.getBene_View_Bene_Dob().getValue(8));                                               //Natural: ASSIGN #BENE8-BIRTHDATE := BENE-VIEW.BENE-DOB ( 8 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Text_Line1().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(8,1));                                       //Natural: ASSIGN #BENE8-TEXT-LINE1 := BENE-VIEW.BENE-SPCL-TXT ( 8,1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Text_Line2().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(8,2));                                       //Natural: ASSIGN #BENE8-TEXT-LINE2 := BENE-VIEW.BENE-SPCL-TXT ( 8,2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Text_Line3().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(8,3));                                       //Natural: ASSIGN #BENE8-TEXT-LINE3 := BENE-VIEW.BENE-SPCL-TXT ( 8,3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Alloc_Method().setValue(ldaScil1280.getBene_View_Bene_Std_Txt_Ind().getValue(8));                                    //Natural: ASSIGN #BENE8-ALLOC-METHOD := BENE-VIEW.BENE-STD-TXT-IND ( 8 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Alloc_Pct().setValue(ldaScil1280.getBene_View_Bene_Alloc_Pct().getValue(8));                                         //Natural: ASSIGN #BENE8-ALLOC-PCT := BENE-VIEW.BENE-ALLOC-PCT ( 8 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Alloc_Num().setValue(ldaScil1280.getBene_View_Bene_Nmrtr_Nbr().getValue(8));                                         //Natural: ASSIGN #BENE8-ALLOC-NUM := BENE-VIEW.BENE-NMRTR-NBR ( 8 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Alloc_Denum().setValue(ldaScil1280.getBene_View_Bene_Dnmntr_Nbr().getValue(8));                                      //Natural: ASSIGN #BENE8-ALLOC-DENUM := BENE-VIEW.BENE-DNMNTR-NBR ( 8 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Addr_Line_1().setValue(ldaScil1280.getBene_View_Bene_Addr1().getValue(8));                                           //Natural: ASSIGN #BENE8-ADDR-LINE-1 := BENE-VIEW.BENE-ADDR1 ( 8 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Addr_Line_2().setValue(ldaScil1280.getBene_View_Bene_Addr2().getValue(8));                                           //Natural: ASSIGN #BENE8-ADDR-LINE-2 := BENE-VIEW.BENE-ADDR2 ( 8 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Addr_Line_3_City().setValue(ldaScil1280.getBene_View_Bene_Addr3_City().getValue(8));                                 //Natural: ASSIGN #BENE8-ADDR-LINE-3-CITY := BENE-VIEW.BENE-ADDR3-CITY ( 8 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_State().setValue(ldaScil1280.getBene_View_Bene_State().getValue(8));                                                 //Natural: ASSIGN #BENE8-STATE := BENE-VIEW.BENE-STATE ( 8 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Zip().setValue(ldaScil1280.getBene_View_Bene_Zip().getValue(8));                                                     //Natural: ASSIGN #BENE8-ZIP := BENE-VIEW.BENE-ZIP ( 8 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Phone().setValue(ldaScil1280.getBene_View_Bene_Phone().getValue(8));                                                 //Natural: ASSIGN #BENE8-PHONE := BENE-VIEW.BENE-PHONE ( 8 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Gender().setValue(ldaScil1280.getBene_View_Bene_Gender().getValue(8));                                               //Natural: ASSIGN #BENE8-GENDER := BENE-VIEW.BENE-GENDER ( 8 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Country().setValue(ldaScil1280.getBene_View_Bene_Country().getValue(8));                                             //Natural: ASSIGN #BENE8-COUNTRY := BENE-VIEW.BENE-COUNTRY ( 8 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Type().setValue(ldaScil1280.getBene_View_Bene_Type().getValue(9));                                                   //Natural: ASSIGN #BENE9-TYPE := BENE-VIEW.BENE-TYPE ( 9 )
            if (condition(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(9).greater(getZero())))                                                                        //Natural: IF BENE-VIEW.BENE-SSN-NBR ( 9 ) > 0
            {
                pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Ssn().setValueEdited(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(9),new ReportEditMask("999999999"));       //Natural: MOVE EDITED BENE-VIEW.BENE-SSN-NBR ( 9 ) ( EM = 999999999 ) TO #BENE9-SSN
                //*  1IRA
                //*  1IRA
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Name().setValue(ldaScil1280.getBene_View_Bene_Name().getValue(9));                                                   //Natural: ASSIGN #BENE9-NAME := BENE-VIEW.BENE-NAME ( 9 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Name2().setValue(ldaScil1280.getBene_View_Bene_Extended_Name().getValue(9));                                         //Natural: ASSIGN #BENE9-NAME2 := BENE-VIEW.BENE-EXTENDED-NAME ( 9 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Rel_Code().setValue(ldaScil1280.getBene_View_Bene_Relationship_Cde().getValue(9));                                   //Natural: ASSIGN #BENE9-REL-CODE := BENE-VIEW.BENE-RELATIONSHIP-CDE ( 9 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Rel_Desc().setValue(ldaScil1280.getBene_View_Bene_Relationship().getValue(9));                                       //Natural: ASSIGN #BENE9-REL-DESC := BENE-VIEW.BENE-RELATIONSHIP ( 9 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Birthdate().setValue(ldaScil1280.getBene_View_Bene_Dob().getValue(9));                                               //Natural: ASSIGN #BENE9-BIRTHDATE := BENE-VIEW.BENE-DOB ( 9 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Text_Line1().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(9,1));                                       //Natural: ASSIGN #BENE9-TEXT-LINE1 := BENE-VIEW.BENE-SPCL-TXT ( 9,1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Text_Line2().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(9,2));                                       //Natural: ASSIGN #BENE9-TEXT-LINE2 := BENE-VIEW.BENE-SPCL-TXT ( 9,2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Text_Line3().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(9,3));                                       //Natural: ASSIGN #BENE9-TEXT-LINE3 := BENE-VIEW.BENE-SPCL-TXT ( 9,3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Alloc_Method().setValue(ldaScil1280.getBene_View_Bene_Std_Txt_Ind().getValue(9));                                    //Natural: ASSIGN #BENE9-ALLOC-METHOD := BENE-VIEW.BENE-STD-TXT-IND ( 9 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Alloc_Pct().setValue(ldaScil1280.getBene_View_Bene_Alloc_Pct().getValue(9));                                         //Natural: ASSIGN #BENE9-ALLOC-PCT := BENE-VIEW.BENE-ALLOC-PCT ( 9 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Alloc_Num().setValue(ldaScil1280.getBene_View_Bene_Nmrtr_Nbr().getValue(9));                                         //Natural: ASSIGN #BENE9-ALLOC-NUM := BENE-VIEW.BENE-NMRTR-NBR ( 9 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Alloc_Denum().setValue(ldaScil1280.getBene_View_Bene_Dnmntr_Nbr().getValue(9));                                      //Natural: ASSIGN #BENE9-ALLOC-DENUM := BENE-VIEW.BENE-DNMNTR-NBR ( 9 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Addr_Line_1().setValue(ldaScil1280.getBene_View_Bene_Addr1().getValue(9));                                           //Natural: ASSIGN #BENE9-ADDR-LINE-1 := BENE-VIEW.BENE-ADDR1 ( 9 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Addr_Line_2().setValue(ldaScil1280.getBene_View_Bene_Addr2().getValue(9));                                           //Natural: ASSIGN #BENE9-ADDR-LINE-2 := BENE-VIEW.BENE-ADDR2 ( 9 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Addr_Line_3_City().setValue(ldaScil1280.getBene_View_Bene_Addr3_City().getValue(9));                                 //Natural: ASSIGN #BENE9-ADDR-LINE-3-CITY := BENE-VIEW.BENE-ADDR3-CITY ( 9 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_State().setValue(ldaScil1280.getBene_View_Bene_State().getValue(9));                                                 //Natural: ASSIGN #BENE9-STATE := BENE-VIEW.BENE-STATE ( 9 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Zip().setValue(ldaScil1280.getBene_View_Bene_Zip().getValue(9));                                                     //Natural: ASSIGN #BENE9-ZIP := BENE-VIEW.BENE-ZIP ( 9 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Phone().setValue(ldaScil1280.getBene_View_Bene_Phone().getValue(9));                                                 //Natural: ASSIGN #BENE9-PHONE := BENE-VIEW.BENE-PHONE ( 9 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Gender().setValue(ldaScil1280.getBene_View_Bene_Gender().getValue(9));                                               //Natural: ASSIGN #BENE9-GENDER := BENE-VIEW.BENE-GENDER ( 9 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Country().setValue(ldaScil1280.getBene_View_Bene_Country().getValue(9));                                             //Natural: ASSIGN #BENE9-COUNTRY := BENE-VIEW.BENE-COUNTRY ( 9 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Type().setValue(ldaScil1280.getBene_View_Bene_Type().getValue(10));                                                 //Natural: ASSIGN #BENE10-TYPE := BENE-VIEW.BENE-TYPE ( 10 )
            if (condition(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(10).greater(getZero())))                                                                       //Natural: IF BENE-VIEW.BENE-SSN-NBR ( 10 ) > 0
            {
                pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Ssn().setValueEdited(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(10),new ReportEditMask("999999999"));     //Natural: MOVE EDITED BENE-VIEW.BENE-SSN-NBR ( 10 ) ( EM = 999999999 ) TO #BENE10-SSN
                //*  1IRA
                //*  1IRA
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Name().setValue(ldaScil1280.getBene_View_Bene_Name().getValue(10));                                                 //Natural: ASSIGN #BENE10-NAME := BENE-VIEW.BENE-NAME ( 10 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Name2().setValue(ldaScil1280.getBene_View_Bene_Extended_Name().getValue(10));                                       //Natural: ASSIGN #BENE10-NAME2 := BENE-VIEW.BENE-EXTENDED-NAME ( 10 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Rel_Code().setValue(ldaScil1280.getBene_View_Bene_Relationship_Cde().getValue(10));                                 //Natural: ASSIGN #BENE10-REL-CODE := BENE-VIEW.BENE-RELATIONSHIP-CDE ( 10 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Rel_Desc().setValue(ldaScil1280.getBene_View_Bene_Relationship().getValue(10));                                     //Natural: ASSIGN #BENE10-REL-DESC := BENE-VIEW.BENE-RELATIONSHIP ( 10 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Birthdate().setValue(ldaScil1280.getBene_View_Bene_Dob().getValue(10));                                             //Natural: ASSIGN #BENE10-BIRTHDATE := BENE-VIEW.BENE-DOB ( 10 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Text_Line1().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(10,1));                                     //Natural: ASSIGN #BENE10-TEXT-LINE1 := BENE-VIEW.BENE-SPCL-TXT ( 10,1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Text_Line2().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(10,2));                                     //Natural: ASSIGN #BENE10-TEXT-LINE2 := BENE-VIEW.BENE-SPCL-TXT ( 10,2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Text_Line3().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(10,3));                                     //Natural: ASSIGN #BENE10-TEXT-LINE3 := BENE-VIEW.BENE-SPCL-TXT ( 10,3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Alloc_Method().setValue(ldaScil1280.getBene_View_Bene_Std_Txt_Ind().getValue(10));                                  //Natural: ASSIGN #BENE10-ALLOC-METHOD := BENE-VIEW.BENE-STD-TXT-IND ( 10 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Alloc_Pct().setValue(ldaScil1280.getBene_View_Bene_Alloc_Pct().getValue(10));                                       //Natural: ASSIGN #BENE10-ALLOC-PCT := BENE-VIEW.BENE-ALLOC-PCT ( 10 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Alloc_Num().setValue(ldaScil1280.getBene_View_Bene_Nmrtr_Nbr().getValue(10));                                       //Natural: ASSIGN #BENE10-ALLOC-NUM := BENE-VIEW.BENE-NMRTR-NBR ( 10 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Alloc_Denum().setValue(ldaScil1280.getBene_View_Bene_Dnmntr_Nbr().getValue(10));                                    //Natural: ASSIGN #BENE10-ALLOC-DENUM := BENE-VIEW.BENE-DNMNTR-NBR ( 10 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Addr_Line_1().setValue(ldaScil1280.getBene_View_Bene_Addr1().getValue(10));                                         //Natural: ASSIGN #BENE10-ADDR-LINE-1 := BENE-VIEW.BENE-ADDR1 ( 10 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Addr_Line_2().setValue(ldaScil1280.getBene_View_Bene_Addr2().getValue(10));                                         //Natural: ASSIGN #BENE10-ADDR-LINE-2 := BENE-VIEW.BENE-ADDR2 ( 10 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Addr_Line_3_City().setValue(ldaScil1280.getBene_View_Bene_Addr3_City().getValue(10));                               //Natural: ASSIGN #BENE10-ADDR-LINE-3-CITY := BENE-VIEW.BENE-ADDR3-CITY ( 10 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_State().setValue(ldaScil1280.getBene_View_Bene_State().getValue(10));                                               //Natural: ASSIGN #BENE10-STATE := BENE-VIEW.BENE-STATE ( 10 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Zip().setValue(ldaScil1280.getBene_View_Bene_Zip().getValue(10));                                                   //Natural: ASSIGN #BENE10-ZIP := BENE-VIEW.BENE-ZIP ( 10 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Phone().setValue(ldaScil1280.getBene_View_Bene_Phone().getValue(10));                                               //Natural: ASSIGN #BENE10-PHONE := BENE-VIEW.BENE-PHONE ( 10 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Gender().setValue(ldaScil1280.getBene_View_Bene_Gender().getValue(10));                                             //Natural: ASSIGN #BENE10-GENDER := BENE-VIEW.BENE-GENDER ( 10 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Country().setValue(ldaScil1280.getBene_View_Bene_Country().getValue(10));                                           //Natural: ASSIGN #BENE10-COUNTRY := BENE-VIEW.BENE-COUNTRY ( 10 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Type().setValue(ldaScil1280.getBene_View_Bene_Type().getValue(11));                                                 //Natural: ASSIGN #BENE11-TYPE := BENE-VIEW.BENE-TYPE ( 11 )
            if (condition(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(11).greater(getZero())))                                                                       //Natural: IF BENE-VIEW.BENE-SSN-NBR ( 11 ) > 0
            {
                pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Ssn().setValueEdited(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(11),new ReportEditMask("999999999"));     //Natural: MOVE EDITED BENE-VIEW.BENE-SSN-NBR ( 11 ) ( EM = 999999999 ) TO #BENE11-SSN
                //*  1IRA
                //*  1IRA
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Name().setValue(ldaScil1280.getBene_View_Bene_Name().getValue(11));                                                 //Natural: ASSIGN #BENE11-NAME := BENE-VIEW.BENE-NAME ( 11 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Name2().setValue(ldaScil1280.getBene_View_Bene_Extended_Name().getValue(11));                                       //Natural: ASSIGN #BENE11-NAME2 := BENE-VIEW.BENE-EXTENDED-NAME ( 11 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Rel_Code().setValue(ldaScil1280.getBene_View_Bene_Relationship_Cde().getValue(11));                                 //Natural: ASSIGN #BENE11-REL-CODE := BENE-VIEW.BENE-RELATIONSHIP-CDE ( 11 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Rel_Desc().setValue(ldaScil1280.getBene_View_Bene_Relationship().getValue(11));                                     //Natural: ASSIGN #BENE11-REL-DESC := BENE-VIEW.BENE-RELATIONSHIP ( 11 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Birthdate().setValue(ldaScil1280.getBene_View_Bene_Dob().getValue(11));                                             //Natural: ASSIGN #BENE11-BIRTHDATE := BENE-VIEW.BENE-DOB ( 11 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Text_Line1().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(11,1));                                     //Natural: ASSIGN #BENE11-TEXT-LINE1 := BENE-VIEW.BENE-SPCL-TXT ( 11,1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Text_Line2().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(11,2));                                     //Natural: ASSIGN #BENE11-TEXT-LINE2 := BENE-VIEW.BENE-SPCL-TXT ( 11,2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Text_Line3().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(11,3));                                     //Natural: ASSIGN #BENE11-TEXT-LINE3 := BENE-VIEW.BENE-SPCL-TXT ( 11,3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Alloc_Method().setValue(ldaScil1280.getBene_View_Bene_Std_Txt_Ind().getValue(11));                                  //Natural: ASSIGN #BENE11-ALLOC-METHOD := BENE-VIEW.BENE-STD-TXT-IND ( 11 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Alloc_Pct().setValue(ldaScil1280.getBene_View_Bene_Alloc_Pct().getValue(11));                                       //Natural: ASSIGN #BENE11-ALLOC-PCT := BENE-VIEW.BENE-ALLOC-PCT ( 11 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Alloc_Num().setValue(ldaScil1280.getBene_View_Bene_Nmrtr_Nbr().getValue(11));                                       //Natural: ASSIGN #BENE11-ALLOC-NUM := BENE-VIEW.BENE-NMRTR-NBR ( 11 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Alloc_Denum().setValue(ldaScil1280.getBene_View_Bene_Dnmntr_Nbr().getValue(11));                                    //Natural: ASSIGN #BENE11-ALLOC-DENUM := BENE-VIEW.BENE-DNMNTR-NBR ( 11 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Addr_Line_1().setValue(ldaScil1280.getBene_View_Bene_Addr1().getValue(11));                                         //Natural: ASSIGN #BENE11-ADDR-LINE-1 := BENE-VIEW.BENE-ADDR1 ( 11 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Addr_Line_2().setValue(ldaScil1280.getBene_View_Bene_Addr2().getValue(11));                                         //Natural: ASSIGN #BENE11-ADDR-LINE-2 := BENE-VIEW.BENE-ADDR2 ( 11 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Addr_Line_3_City().setValue(ldaScil1280.getBene_View_Bene_Addr3_City().getValue(11));                               //Natural: ASSIGN #BENE11-ADDR-LINE-3-CITY := BENE-VIEW.BENE-ADDR3-CITY ( 11 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_State().setValue(ldaScil1280.getBene_View_Bene_State().getValue(11));                                               //Natural: ASSIGN #BENE11-STATE := BENE-VIEW.BENE-STATE ( 11 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Zip().setValue(ldaScil1280.getBene_View_Bene_Zip().getValue(11));                                                   //Natural: ASSIGN #BENE11-ZIP := BENE-VIEW.BENE-ZIP ( 11 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Phone().setValue(ldaScil1280.getBene_View_Bene_Phone().getValue(11));                                               //Natural: ASSIGN #BENE11-PHONE := BENE-VIEW.BENE-PHONE ( 11 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Gender().setValue(ldaScil1280.getBene_View_Bene_Gender().getValue(11));                                             //Natural: ASSIGN #BENE11-GENDER := BENE-VIEW.BENE-GENDER ( 11 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Country().setValue(ldaScil1280.getBene_View_Bene_Country().getValue(11));                                           //Natural: ASSIGN #BENE11-COUNTRY := BENE-VIEW.BENE-COUNTRY ( 11 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Type().setValue(ldaScil1280.getBene_View_Bene_Type().getValue(12));                                                 //Natural: ASSIGN #BENE12-TYPE := BENE-VIEW.BENE-TYPE ( 12 )
            if (condition(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(12).greater(getZero())))                                                                       //Natural: IF BENE-VIEW.BENE-SSN-NBR ( 12 ) > 0
            {
                pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Ssn().setValueEdited(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(12),new ReportEditMask("999999999"));     //Natural: MOVE EDITED BENE-VIEW.BENE-SSN-NBR ( 12 ) ( EM = 999999999 ) TO #BENE12-SSN
                //*  1IRA
                //*  1IRA
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Name().setValue(ldaScil1280.getBene_View_Bene_Name().getValue(12));                                                 //Natural: ASSIGN #BENE12-NAME := BENE-VIEW.BENE-NAME ( 12 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Name2().setValue(ldaScil1280.getBene_View_Bene_Extended_Name().getValue(12));                                       //Natural: ASSIGN #BENE12-NAME2 := BENE-VIEW.BENE-EXTENDED-NAME ( 12 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Rel_Code().setValue(ldaScil1280.getBene_View_Bene_Relationship_Cde().getValue(12));                                 //Natural: ASSIGN #BENE12-REL-CODE := BENE-VIEW.BENE-RELATIONSHIP-CDE ( 12 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Rel_Desc().setValue(ldaScil1280.getBene_View_Bene_Relationship().getValue(12));                                     //Natural: ASSIGN #BENE12-REL-DESC := BENE-VIEW.BENE-RELATIONSHIP ( 12 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Birthdate().setValue(ldaScil1280.getBene_View_Bene_Dob().getValue(12));                                             //Natural: ASSIGN #BENE12-BIRTHDATE := BENE-VIEW.BENE-DOB ( 12 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Text_Line1().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(12,1));                                     //Natural: ASSIGN #BENE12-TEXT-LINE1 := BENE-VIEW.BENE-SPCL-TXT ( 12,1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Text_Line2().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(12,2));                                     //Natural: ASSIGN #BENE12-TEXT-LINE2 := BENE-VIEW.BENE-SPCL-TXT ( 12,2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Text_Line3().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(12,3));                                     //Natural: ASSIGN #BENE12-TEXT-LINE3 := BENE-VIEW.BENE-SPCL-TXT ( 12,3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Alloc_Method().setValue(ldaScil1280.getBene_View_Bene_Std_Txt_Ind().getValue(12));                                  //Natural: ASSIGN #BENE12-ALLOC-METHOD := BENE-VIEW.BENE-STD-TXT-IND ( 12 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Alloc_Pct().setValue(ldaScil1280.getBene_View_Bene_Alloc_Pct().getValue(12));                                       //Natural: ASSIGN #BENE12-ALLOC-PCT := BENE-VIEW.BENE-ALLOC-PCT ( 12 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Alloc_Num().setValue(ldaScil1280.getBene_View_Bene_Nmrtr_Nbr().getValue(12));                                       //Natural: ASSIGN #BENE12-ALLOC-NUM := BENE-VIEW.BENE-NMRTR-NBR ( 12 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Alloc_Denum().setValue(ldaScil1280.getBene_View_Bene_Dnmntr_Nbr().getValue(12));                                    //Natural: ASSIGN #BENE12-ALLOC-DENUM := BENE-VIEW.BENE-DNMNTR-NBR ( 12 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Addr_Line_1().setValue(ldaScil1280.getBene_View_Bene_Addr1().getValue(12));                                         //Natural: ASSIGN #BENE12-ADDR-LINE-1 := BENE-VIEW.BENE-ADDR1 ( 12 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Addr_Line_2().setValue(ldaScil1280.getBene_View_Bene_Addr2().getValue(12));                                         //Natural: ASSIGN #BENE12-ADDR-LINE-2 := BENE-VIEW.BENE-ADDR2 ( 12 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Addr_Line_3_City().setValue(ldaScil1280.getBene_View_Bene_Addr3_City().getValue(12));                               //Natural: ASSIGN #BENE12-ADDR-LINE-3-CITY := BENE-VIEW.BENE-ADDR3-CITY ( 12 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_State().setValue(ldaScil1280.getBene_View_Bene_State().getValue(12));                                               //Natural: ASSIGN #BENE12-STATE := BENE-VIEW.BENE-STATE ( 12 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Zip().setValue(ldaScil1280.getBene_View_Bene_Zip().getValue(12));                                                   //Natural: ASSIGN #BENE12-ZIP := BENE-VIEW.BENE-ZIP ( 12 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Phone().setValue(ldaScil1280.getBene_View_Bene_Phone().getValue(12));                                               //Natural: ASSIGN #BENE12-PHONE := BENE-VIEW.BENE-PHONE ( 12 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Gender().setValue(ldaScil1280.getBene_View_Bene_Gender().getValue(12));                                             //Natural: ASSIGN #BENE12-GENDER := BENE-VIEW.BENE-GENDER ( 12 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Country().setValue(ldaScil1280.getBene_View_Bene_Country().getValue(12));                                           //Natural: ASSIGN #BENE12-COUNTRY := BENE-VIEW.BENE-COUNTRY ( 12 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Type().setValue(ldaScil1280.getBene_View_Bene_Type().getValue(13));                                                 //Natural: ASSIGN #BENE13-TYPE := BENE-VIEW.BENE-TYPE ( 13 )
            if (condition(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(13).greater(getZero())))                                                                       //Natural: IF BENE-VIEW.BENE-SSN-NBR ( 13 ) > 0
            {
                pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Ssn().setValueEdited(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(13),new ReportEditMask("999999999"));     //Natural: MOVE EDITED BENE-VIEW.BENE-SSN-NBR ( 13 ) ( EM = 999999999 ) TO #BENE13-SSN
                //*  1IRA
                //*  1IRA
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Name().setValue(ldaScil1280.getBene_View_Bene_Name().getValue(13));                                                 //Natural: ASSIGN #BENE13-NAME := BENE-VIEW.BENE-NAME ( 13 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Name2().setValue(ldaScil1280.getBene_View_Bene_Extended_Name().getValue(13));                                       //Natural: ASSIGN #BENE13-NAME2 := BENE-VIEW.BENE-EXTENDED-NAME ( 13 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Rel_Code().setValue(ldaScil1280.getBene_View_Bene_Relationship_Cde().getValue(13));                                 //Natural: ASSIGN #BENE13-REL-CODE := BENE-VIEW.BENE-RELATIONSHIP-CDE ( 13 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Rel_Desc().setValue(ldaScil1280.getBene_View_Bene_Relationship().getValue(13));                                     //Natural: ASSIGN #BENE13-REL-DESC := BENE-VIEW.BENE-RELATIONSHIP ( 13 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Birthdate().setValue(ldaScil1280.getBene_View_Bene_Dob().getValue(13));                                             //Natural: ASSIGN #BENE13-BIRTHDATE := BENE-VIEW.BENE-DOB ( 13 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Text_Line1().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(13,1));                                     //Natural: ASSIGN #BENE13-TEXT-LINE1 := BENE-VIEW.BENE-SPCL-TXT ( 13,1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Text_Line2().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(13,2));                                     //Natural: ASSIGN #BENE13-TEXT-LINE2 := BENE-VIEW.BENE-SPCL-TXT ( 13,2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Text_Line3().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(13,3));                                     //Natural: ASSIGN #BENE13-TEXT-LINE3 := BENE-VIEW.BENE-SPCL-TXT ( 13,3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Alloc_Method().setValue(ldaScil1280.getBene_View_Bene_Std_Txt_Ind().getValue(13));                                  //Natural: ASSIGN #BENE13-ALLOC-METHOD := BENE-VIEW.BENE-STD-TXT-IND ( 13 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Alloc_Pct().setValue(ldaScil1280.getBene_View_Bene_Alloc_Pct().getValue(13));                                       //Natural: ASSIGN #BENE13-ALLOC-PCT := BENE-VIEW.BENE-ALLOC-PCT ( 13 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Alloc_Num().setValue(ldaScil1280.getBene_View_Bene_Nmrtr_Nbr().getValue(13));                                       //Natural: ASSIGN #BENE13-ALLOC-NUM := BENE-VIEW.BENE-NMRTR-NBR ( 13 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Alloc_Denum().setValue(ldaScil1280.getBene_View_Bene_Dnmntr_Nbr().getValue(13));                                    //Natural: ASSIGN #BENE13-ALLOC-DENUM := BENE-VIEW.BENE-DNMNTR-NBR ( 13 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Addr_Line_1().setValue(ldaScil1280.getBene_View_Bene_Addr1().getValue(13));                                         //Natural: ASSIGN #BENE13-ADDR-LINE-1 := BENE-VIEW.BENE-ADDR1 ( 13 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Addr_Line_2().setValue(ldaScil1280.getBene_View_Bene_Addr2().getValue(13));                                         //Natural: ASSIGN #BENE13-ADDR-LINE-2 := BENE-VIEW.BENE-ADDR2 ( 13 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Addr_Line_3_City().setValue(ldaScil1280.getBene_View_Bene_Addr3_City().getValue(13));                               //Natural: ASSIGN #BENE13-ADDR-LINE-3-CITY := BENE-VIEW.BENE-ADDR3-CITY ( 13 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_State().setValue(ldaScil1280.getBene_View_Bene_State().getValue(13));                                               //Natural: ASSIGN #BENE13-STATE := BENE-VIEW.BENE-STATE ( 13 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Zip().setValue(ldaScil1280.getBene_View_Bene_Zip().getValue(13));                                                   //Natural: ASSIGN #BENE13-ZIP := BENE-VIEW.BENE-ZIP ( 13 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Phone().setValue(ldaScil1280.getBene_View_Bene_Phone().getValue(13));                                               //Natural: ASSIGN #BENE13-PHONE := BENE-VIEW.BENE-PHONE ( 13 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Gender().setValue(ldaScil1280.getBene_View_Bene_Gender().getValue(13));                                             //Natural: ASSIGN #BENE13-GENDER := BENE-VIEW.BENE-GENDER ( 13 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Country().setValue(ldaScil1280.getBene_View_Bene_Country().getValue(13));                                           //Natural: ASSIGN #BENE13-COUNTRY := BENE-VIEW.BENE-COUNTRY ( 13 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Type().setValue(ldaScil1280.getBene_View_Bene_Type().getValue(14));                                                 //Natural: ASSIGN #BENE14-TYPE := BENE-VIEW.BENE-TYPE ( 14 )
            if (condition(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(14).greater(getZero())))                                                                       //Natural: IF BENE-VIEW.BENE-SSN-NBR ( 14 ) > 0
            {
                pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Ssn().setValueEdited(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(14),new ReportEditMask("999999999"));     //Natural: MOVE EDITED BENE-VIEW.BENE-SSN-NBR ( 14 ) ( EM = 999999999 ) TO #BENE14-SSN
                //*  1IRA
                //*  1IRA
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Name().setValue(ldaScil1280.getBene_View_Bene_Name().getValue(14));                                                 //Natural: ASSIGN #BENE14-NAME := BENE-VIEW.BENE-NAME ( 14 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Name2().setValue(ldaScil1280.getBene_View_Bene_Extended_Name().getValue(14));                                       //Natural: ASSIGN #BENE14-NAME2 := BENE-VIEW.BENE-EXTENDED-NAME ( 14 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Rel_Code().setValue(ldaScil1280.getBene_View_Bene_Relationship_Cde().getValue(14));                                 //Natural: ASSIGN #BENE14-REL-CODE := BENE-VIEW.BENE-RELATIONSHIP-CDE ( 14 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Rel_Desc().setValue(ldaScil1280.getBene_View_Bene_Relationship().getValue(14));                                     //Natural: ASSIGN #BENE14-REL-DESC := BENE-VIEW.BENE-RELATIONSHIP ( 14 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Birthdate().setValue(ldaScil1280.getBene_View_Bene_Dob().getValue(14));                                             //Natural: ASSIGN #BENE14-BIRTHDATE := BENE-VIEW.BENE-DOB ( 14 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Text_Line1().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(14,1));                                     //Natural: ASSIGN #BENE14-TEXT-LINE1 := BENE-VIEW.BENE-SPCL-TXT ( 14,1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Text_Line2().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(14,2));                                     //Natural: ASSIGN #BENE14-TEXT-LINE2 := BENE-VIEW.BENE-SPCL-TXT ( 14,2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Text_Line3().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(14,3));                                     //Natural: ASSIGN #BENE14-TEXT-LINE3 := BENE-VIEW.BENE-SPCL-TXT ( 14,3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Alloc_Method().setValue(ldaScil1280.getBene_View_Bene_Std_Txt_Ind().getValue(14));                                  //Natural: ASSIGN #BENE14-ALLOC-METHOD := BENE-VIEW.BENE-STD-TXT-IND ( 14 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Alloc_Pct().setValue(ldaScil1280.getBene_View_Bene_Alloc_Pct().getValue(14));                                       //Natural: ASSIGN #BENE14-ALLOC-PCT := BENE-VIEW.BENE-ALLOC-PCT ( 14 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Alloc_Num().setValue(ldaScil1280.getBene_View_Bene_Nmrtr_Nbr().getValue(14));                                       //Natural: ASSIGN #BENE14-ALLOC-NUM := BENE-VIEW.BENE-NMRTR-NBR ( 14 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Alloc_Denum().setValue(ldaScil1280.getBene_View_Bene_Dnmntr_Nbr().getValue(14));                                    //Natural: ASSIGN #BENE14-ALLOC-DENUM := BENE-VIEW.BENE-DNMNTR-NBR ( 14 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Addr_Line_1().setValue(ldaScil1280.getBene_View_Bene_Addr1().getValue(14));                                         //Natural: ASSIGN #BENE14-ADDR-LINE-1 := BENE-VIEW.BENE-ADDR1 ( 14 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Addr_Line_2().setValue(ldaScil1280.getBene_View_Bene_Addr2().getValue(14));                                         //Natural: ASSIGN #BENE14-ADDR-LINE-2 := BENE-VIEW.BENE-ADDR2 ( 14 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Addr_Line_3_City().setValue(ldaScil1280.getBene_View_Bene_Addr3_City().getValue(14));                               //Natural: ASSIGN #BENE14-ADDR-LINE-3-CITY := BENE-VIEW.BENE-ADDR3-CITY ( 14 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_State().setValue(ldaScil1280.getBene_View_Bene_State().getValue(14));                                               //Natural: ASSIGN #BENE14-STATE := BENE-VIEW.BENE-STATE ( 14 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Zip().setValue(ldaScil1280.getBene_View_Bene_Zip().getValue(14));                                                   //Natural: ASSIGN #BENE14-ZIP := BENE-VIEW.BENE-ZIP ( 14 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Phone().setValue(ldaScil1280.getBene_View_Bene_Phone().getValue(14));                                               //Natural: ASSIGN #BENE14-PHONE := BENE-VIEW.BENE-PHONE ( 14 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Gender().setValue(ldaScil1280.getBene_View_Bene_Gender().getValue(14));                                             //Natural: ASSIGN #BENE14-GENDER := BENE-VIEW.BENE-GENDER ( 14 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Country().setValue(ldaScil1280.getBene_View_Bene_Country().getValue(14));                                           //Natural: ASSIGN #BENE14-COUNTRY := BENE-VIEW.BENE-COUNTRY ( 14 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Type().setValue(ldaScil1280.getBene_View_Bene_Type().getValue(15));                                                 //Natural: ASSIGN #BENE15-TYPE := BENE-VIEW.BENE-TYPE ( 15 )
            if (condition(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(15).greater(getZero())))                                                                       //Natural: IF BENE-VIEW.BENE-SSN-NBR ( 15 ) > 0
            {
                pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Ssn().setValueEdited(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(15),new ReportEditMask("999999999"));     //Natural: MOVE EDITED BENE-VIEW.BENE-SSN-NBR ( 15 ) ( EM = 999999999 ) TO #BENE15-SSN
                //*  1IRA
                //*  1IRA
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Name().setValue(ldaScil1280.getBene_View_Bene_Name().getValue(15));                                                 //Natural: ASSIGN #BENE15-NAME := BENE-VIEW.BENE-NAME ( 15 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Name2().setValue(ldaScil1280.getBene_View_Bene_Extended_Name().getValue(15));                                       //Natural: ASSIGN #BENE15-NAME2 := BENE-VIEW.BENE-EXTENDED-NAME ( 15 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Rel_Code().setValue(ldaScil1280.getBene_View_Bene_Relationship_Cde().getValue(15));                                 //Natural: ASSIGN #BENE15-REL-CODE := BENE-VIEW.BENE-RELATIONSHIP-CDE ( 15 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Rel_Desc().setValue(ldaScil1280.getBene_View_Bene_Relationship().getValue(15));                                     //Natural: ASSIGN #BENE15-REL-DESC := BENE-VIEW.BENE-RELATIONSHIP ( 15 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Birthdate().setValue(ldaScil1280.getBene_View_Bene_Dob().getValue(15));                                             //Natural: ASSIGN #BENE15-BIRTHDATE := BENE-VIEW.BENE-DOB ( 15 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Text_Line1().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(15,1));                                     //Natural: ASSIGN #BENE15-TEXT-LINE1 := BENE-VIEW.BENE-SPCL-TXT ( 15,1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Text_Line2().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(15,2));                                     //Natural: ASSIGN #BENE15-TEXT-LINE2 := BENE-VIEW.BENE-SPCL-TXT ( 15,2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Text_Line3().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(15,3));                                     //Natural: ASSIGN #BENE15-TEXT-LINE3 := BENE-VIEW.BENE-SPCL-TXT ( 15,3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Alloc_Method().setValue(ldaScil1280.getBene_View_Bene_Std_Txt_Ind().getValue(15));                                  //Natural: ASSIGN #BENE15-ALLOC-METHOD := BENE-VIEW.BENE-STD-TXT-IND ( 15 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Alloc_Pct().setValue(ldaScil1280.getBene_View_Bene_Alloc_Pct().getValue(15));                                       //Natural: ASSIGN #BENE15-ALLOC-PCT := BENE-VIEW.BENE-ALLOC-PCT ( 15 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Alloc_Num().setValue(ldaScil1280.getBene_View_Bene_Nmrtr_Nbr().getValue(15));                                       //Natural: ASSIGN #BENE15-ALLOC-NUM := BENE-VIEW.BENE-NMRTR-NBR ( 15 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Alloc_Denum().setValue(ldaScil1280.getBene_View_Bene_Dnmntr_Nbr().getValue(15));                                    //Natural: ASSIGN #BENE15-ALLOC-DENUM := BENE-VIEW.BENE-DNMNTR-NBR ( 15 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Addr_Line_1().setValue(ldaScil1280.getBene_View_Bene_Addr1().getValue(15));                                         //Natural: ASSIGN #BENE15-ADDR-LINE-1 := BENE-VIEW.BENE-ADDR1 ( 15 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Addr_Line_2().setValue(ldaScil1280.getBene_View_Bene_Addr2().getValue(15));                                         //Natural: ASSIGN #BENE15-ADDR-LINE-2 := BENE-VIEW.BENE-ADDR2 ( 15 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Addr_Line_3_City().setValue(ldaScil1280.getBene_View_Bene_Addr3_City().getValue(15));                               //Natural: ASSIGN #BENE15-ADDR-LINE-3-CITY := BENE-VIEW.BENE-ADDR3-CITY ( 15 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_State().setValue(ldaScil1280.getBene_View_Bene_State().getValue(15));                                               //Natural: ASSIGN #BENE15-STATE := BENE-VIEW.BENE-STATE ( 15 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Zip().setValue(ldaScil1280.getBene_View_Bene_Zip().getValue(15));                                                   //Natural: ASSIGN #BENE15-ZIP := BENE-VIEW.BENE-ZIP ( 15 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Phone().setValue(ldaScil1280.getBene_View_Bene_Phone().getValue(15));                                               //Natural: ASSIGN #BENE15-PHONE := BENE-VIEW.BENE-PHONE ( 15 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Gender().setValue(ldaScil1280.getBene_View_Bene_Gender().getValue(15));                                             //Natural: ASSIGN #BENE15-GENDER := BENE-VIEW.BENE-GENDER ( 15 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Country().setValue(ldaScil1280.getBene_View_Bene_Country().getValue(15));                                           //Natural: ASSIGN #BENE15-COUNTRY := BENE-VIEW.BENE-COUNTRY ( 15 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Type().setValue(ldaScil1280.getBene_View_Bene_Type().getValue(16));                                                 //Natural: ASSIGN #BENE16-TYPE := BENE-VIEW.BENE-TYPE ( 16 )
            if (condition(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(16).greater(getZero())))                                                                       //Natural: IF BENE-VIEW.BENE-SSN-NBR ( 16 ) > 0
            {
                pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Ssn().setValueEdited(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(16),new ReportEditMask("999999999"));     //Natural: MOVE EDITED BENE-VIEW.BENE-SSN-NBR ( 16 ) ( EM = 999999999 ) TO #BENE16-SSN
                //*  1IRA
                //*  1IRA
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Name().setValue(ldaScil1280.getBene_View_Bene_Name().getValue(16));                                                 //Natural: ASSIGN #BENE16-NAME := BENE-VIEW.BENE-NAME ( 16 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Name2().setValue(ldaScil1280.getBene_View_Bene_Extended_Name().getValue(16));                                       //Natural: ASSIGN #BENE16-NAME2 := BENE-VIEW.BENE-EXTENDED-NAME ( 16 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Rel_Code().setValue(ldaScil1280.getBene_View_Bene_Relationship_Cde().getValue(16));                                 //Natural: ASSIGN #BENE16-REL-CODE := BENE-VIEW.BENE-RELATIONSHIP-CDE ( 16 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Rel_Desc().setValue(ldaScil1280.getBene_View_Bene_Relationship().getValue(16));                                     //Natural: ASSIGN #BENE16-REL-DESC := BENE-VIEW.BENE-RELATIONSHIP ( 16 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Birthdate().setValue(ldaScil1280.getBene_View_Bene_Dob().getValue(16));                                             //Natural: ASSIGN #BENE16-BIRTHDATE := BENE-VIEW.BENE-DOB ( 16 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Text_Line1().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(16,1));                                     //Natural: ASSIGN #BENE16-TEXT-LINE1 := BENE-VIEW.BENE-SPCL-TXT ( 16,1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Text_Line2().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(16,2));                                     //Natural: ASSIGN #BENE16-TEXT-LINE2 := BENE-VIEW.BENE-SPCL-TXT ( 16,2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Text_Line3().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(16,3));                                     //Natural: ASSIGN #BENE16-TEXT-LINE3 := BENE-VIEW.BENE-SPCL-TXT ( 16,3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Alloc_Method().setValue(ldaScil1280.getBene_View_Bene_Std_Txt_Ind().getValue(16));                                  //Natural: ASSIGN #BENE16-ALLOC-METHOD := BENE-VIEW.BENE-STD-TXT-IND ( 16 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Alloc_Pct().setValue(ldaScil1280.getBene_View_Bene_Alloc_Pct().getValue(16));                                       //Natural: ASSIGN #BENE16-ALLOC-PCT := BENE-VIEW.BENE-ALLOC-PCT ( 16 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Alloc_Num().setValue(ldaScil1280.getBene_View_Bene_Nmrtr_Nbr().getValue(16));                                       //Natural: ASSIGN #BENE16-ALLOC-NUM := BENE-VIEW.BENE-NMRTR-NBR ( 16 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Alloc_Denum().setValue(ldaScil1280.getBene_View_Bene_Dnmntr_Nbr().getValue(16));                                    //Natural: ASSIGN #BENE16-ALLOC-DENUM := BENE-VIEW.BENE-DNMNTR-NBR ( 16 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Addr_Line_1().setValue(ldaScil1280.getBene_View_Bene_Addr1().getValue(16));                                         //Natural: ASSIGN #BENE16-ADDR-LINE-1 := BENE-VIEW.BENE-ADDR1 ( 16 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Addr_Line_2().setValue(ldaScil1280.getBene_View_Bene_Addr2().getValue(16));                                         //Natural: ASSIGN #BENE16-ADDR-LINE-2 := BENE-VIEW.BENE-ADDR2 ( 16 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Addr_Line_3_City().setValue(ldaScil1280.getBene_View_Bene_Addr3_City().getValue(16));                               //Natural: ASSIGN #BENE16-ADDR-LINE-3-CITY := BENE-VIEW.BENE-ADDR3-CITY ( 16 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_State().setValue(ldaScil1280.getBene_View_Bene_State().getValue(16));                                               //Natural: ASSIGN #BENE16-STATE := BENE-VIEW.BENE-STATE ( 16 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Zip().setValue(ldaScil1280.getBene_View_Bene_Zip().getValue(16));                                                   //Natural: ASSIGN #BENE16-ZIP := BENE-VIEW.BENE-ZIP ( 16 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Phone().setValue(ldaScil1280.getBene_View_Bene_Phone().getValue(16));                                               //Natural: ASSIGN #BENE16-PHONE := BENE-VIEW.BENE-PHONE ( 16 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Gender().setValue(ldaScil1280.getBene_View_Bene_Gender().getValue(16));                                             //Natural: ASSIGN #BENE16-GENDER := BENE-VIEW.BENE-GENDER ( 16 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Country().setValue(ldaScil1280.getBene_View_Bene_Country().getValue(16));                                           //Natural: ASSIGN #BENE16-COUNTRY := BENE-VIEW.BENE-COUNTRY ( 16 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Type().setValue(ldaScil1280.getBene_View_Bene_Type().getValue(17));                                                 //Natural: ASSIGN #BENE17-TYPE := BENE-VIEW.BENE-TYPE ( 17 )
            if (condition(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(17).greater(getZero())))                                                                       //Natural: IF BENE-VIEW.BENE-SSN-NBR ( 17 ) > 0
            {
                pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Ssn().setValueEdited(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(17),new ReportEditMask("999999999"));     //Natural: MOVE EDITED BENE-VIEW.BENE-SSN-NBR ( 17 ) ( EM = 999999999 ) TO #BENE17-SSN
                //*  1IRA
                //*  1IRA
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Name().setValue(ldaScil1280.getBene_View_Bene_Name().getValue(17));                                                 //Natural: ASSIGN #BENE17-NAME := BENE-VIEW.BENE-NAME ( 17 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Name2().setValue(ldaScil1280.getBene_View_Bene_Extended_Name().getValue(17));                                       //Natural: ASSIGN #BENE17-NAME2 := BENE-VIEW.BENE-EXTENDED-NAME ( 17 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Rel_Code().setValue(ldaScil1280.getBene_View_Bene_Relationship_Cde().getValue(17));                                 //Natural: ASSIGN #BENE17-REL-CODE := BENE-VIEW.BENE-RELATIONSHIP-CDE ( 17 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Rel_Desc().setValue(ldaScil1280.getBene_View_Bene_Relationship().getValue(17));                                     //Natural: ASSIGN #BENE17-REL-DESC := BENE-VIEW.BENE-RELATIONSHIP ( 17 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Birthdate().setValue(ldaScil1280.getBene_View_Bene_Dob().getValue(17));                                             //Natural: ASSIGN #BENE17-BIRTHDATE := BENE-VIEW.BENE-DOB ( 17 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Text_Line1().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(17,1));                                     //Natural: ASSIGN #BENE17-TEXT-LINE1 := BENE-VIEW.BENE-SPCL-TXT ( 17,1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Text_Line2().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(17,2));                                     //Natural: ASSIGN #BENE17-TEXT-LINE2 := BENE-VIEW.BENE-SPCL-TXT ( 17,2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Text_Line3().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(17,3));                                     //Natural: ASSIGN #BENE17-TEXT-LINE3 := BENE-VIEW.BENE-SPCL-TXT ( 17,3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Alloc_Method().setValue(ldaScil1280.getBene_View_Bene_Std_Txt_Ind().getValue(17));                                  //Natural: ASSIGN #BENE17-ALLOC-METHOD := BENE-VIEW.BENE-STD-TXT-IND ( 17 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Alloc_Pct().setValue(ldaScil1280.getBene_View_Bene_Alloc_Pct().getValue(17));                                       //Natural: ASSIGN #BENE17-ALLOC-PCT := BENE-VIEW.BENE-ALLOC-PCT ( 17 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Alloc_Num().setValue(ldaScil1280.getBene_View_Bene_Nmrtr_Nbr().getValue(17));                                       //Natural: ASSIGN #BENE17-ALLOC-NUM := BENE-VIEW.BENE-NMRTR-NBR ( 17 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Alloc_Denum().setValue(ldaScil1280.getBene_View_Bene_Dnmntr_Nbr().getValue(17));                                    //Natural: ASSIGN #BENE17-ALLOC-DENUM := BENE-VIEW.BENE-DNMNTR-NBR ( 17 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Addr_Line_1().setValue(ldaScil1280.getBene_View_Bene_Addr1().getValue(17));                                         //Natural: ASSIGN #BENE17-ADDR-LINE-1 := BENE-VIEW.BENE-ADDR1 ( 17 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Addr_Line_2().setValue(ldaScil1280.getBene_View_Bene_Addr2().getValue(17));                                         //Natural: ASSIGN #BENE17-ADDR-LINE-2 := BENE-VIEW.BENE-ADDR2 ( 17 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Addr_Line_3_City().setValue(ldaScil1280.getBene_View_Bene_Addr3_City().getValue(17));                               //Natural: ASSIGN #BENE17-ADDR-LINE-3-CITY := BENE-VIEW.BENE-ADDR3-CITY ( 17 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_State().setValue(ldaScil1280.getBene_View_Bene_State().getValue(17));                                               //Natural: ASSIGN #BENE17-STATE := BENE-VIEW.BENE-STATE ( 17 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Zip().setValue(ldaScil1280.getBene_View_Bene_Zip().getValue(17));                                                   //Natural: ASSIGN #BENE17-ZIP := BENE-VIEW.BENE-ZIP ( 17 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Phone().setValue(ldaScil1280.getBene_View_Bene_Phone().getValue(17));                                               //Natural: ASSIGN #BENE17-PHONE := BENE-VIEW.BENE-PHONE ( 17 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Gender().setValue(ldaScil1280.getBene_View_Bene_Gender().getValue(17));                                             //Natural: ASSIGN #BENE17-GENDER := BENE-VIEW.BENE-GENDER ( 17 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Country().setValue(ldaScil1280.getBene_View_Bene_Country().getValue(17));                                           //Natural: ASSIGN #BENE17-COUNTRY := BENE-VIEW.BENE-COUNTRY ( 17 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Type().setValue(ldaScil1280.getBene_View_Bene_Type().getValue(18));                                                 //Natural: ASSIGN #BENE18-TYPE := BENE-VIEW.BENE-TYPE ( 18 )
            if (condition(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(18).greater(getZero())))                                                                       //Natural: IF BENE-VIEW.BENE-SSN-NBR ( 18 ) > 0
            {
                pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Ssn().setValueEdited(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(18),new ReportEditMask("999999999"));     //Natural: MOVE EDITED BENE-VIEW.BENE-SSN-NBR ( 18 ) ( EM = 999999999 ) TO #BENE18-SSN
                //*  1IRA
                //*  1IRA
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Name().setValue(ldaScil1280.getBene_View_Bene_Name().getValue(18));                                                 //Natural: ASSIGN #BENE18-NAME := BENE-VIEW.BENE-NAME ( 18 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Name2().setValue(ldaScil1280.getBene_View_Bene_Extended_Name().getValue(18));                                       //Natural: ASSIGN #BENE18-NAME2 := BENE-VIEW.BENE-EXTENDED-NAME ( 18 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Rel_Code().setValue(ldaScil1280.getBene_View_Bene_Relationship_Cde().getValue(18));                                 //Natural: ASSIGN #BENE18-REL-CODE := BENE-VIEW.BENE-RELATIONSHIP-CDE ( 18 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Rel_Desc().setValue(ldaScil1280.getBene_View_Bene_Relationship().getValue(18));                                     //Natural: ASSIGN #BENE18-REL-DESC := BENE-VIEW.BENE-RELATIONSHIP ( 18 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Birthdate().setValue(ldaScil1280.getBene_View_Bene_Dob().getValue(18));                                             //Natural: ASSIGN #BENE18-BIRTHDATE := BENE-VIEW.BENE-DOB ( 18 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Text_Line1().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(18,1));                                     //Natural: ASSIGN #BENE18-TEXT-LINE1 := BENE-VIEW.BENE-SPCL-TXT ( 18,1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Text_Line2().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(18,2));                                     //Natural: ASSIGN #BENE18-TEXT-LINE2 := BENE-VIEW.BENE-SPCL-TXT ( 18,2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Text_Line3().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(18,3));                                     //Natural: ASSIGN #BENE18-TEXT-LINE3 := BENE-VIEW.BENE-SPCL-TXT ( 18,3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Alloc_Method().setValue(ldaScil1280.getBene_View_Bene_Std_Txt_Ind().getValue(18));                                  //Natural: ASSIGN #BENE18-ALLOC-METHOD := BENE-VIEW.BENE-STD-TXT-IND ( 18 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Alloc_Pct().setValue(ldaScil1280.getBene_View_Bene_Alloc_Pct().getValue(18));                                       //Natural: ASSIGN #BENE18-ALLOC-PCT := BENE-VIEW.BENE-ALLOC-PCT ( 18 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Alloc_Num().setValue(ldaScil1280.getBene_View_Bene_Nmrtr_Nbr().getValue(18));                                       //Natural: ASSIGN #BENE18-ALLOC-NUM := BENE-VIEW.BENE-NMRTR-NBR ( 18 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Alloc_Denum().setValue(ldaScil1280.getBene_View_Bene_Dnmntr_Nbr().getValue(18));                                    //Natural: ASSIGN #BENE18-ALLOC-DENUM := BENE-VIEW.BENE-DNMNTR-NBR ( 18 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Addr_Line_1().setValue(ldaScil1280.getBene_View_Bene_Addr1().getValue(18));                                         //Natural: ASSIGN #BENE18-ADDR-LINE-1 := BENE-VIEW.BENE-ADDR1 ( 18 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Addr_Line_2().setValue(ldaScil1280.getBene_View_Bene_Addr2().getValue(18));                                         //Natural: ASSIGN #BENE18-ADDR-LINE-2 := BENE-VIEW.BENE-ADDR2 ( 18 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Addr_Line_3_City().setValue(ldaScil1280.getBene_View_Bene_Addr3_City().getValue(18));                               //Natural: ASSIGN #BENE18-ADDR-LINE-3-CITY := BENE-VIEW.BENE-ADDR3-CITY ( 18 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_State().setValue(ldaScil1280.getBene_View_Bene_State().getValue(18));                                               //Natural: ASSIGN #BENE18-STATE := BENE-VIEW.BENE-STATE ( 18 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Zip().setValue(ldaScil1280.getBene_View_Bene_Zip().getValue(18));                                                   //Natural: ASSIGN #BENE18-ZIP := BENE-VIEW.BENE-ZIP ( 18 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Phone().setValue(ldaScil1280.getBene_View_Bene_Phone().getValue(18));                                               //Natural: ASSIGN #BENE18-PHONE := BENE-VIEW.BENE-PHONE ( 18 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Gender().setValue(ldaScil1280.getBene_View_Bene_Gender().getValue(18));                                             //Natural: ASSIGN #BENE18-GENDER := BENE-VIEW.BENE-GENDER ( 18 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Country().setValue(ldaScil1280.getBene_View_Bene_Country().getValue(18));                                           //Natural: ASSIGN #BENE18-COUNTRY := BENE-VIEW.BENE-COUNTRY ( 18 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Type().setValue(ldaScil1280.getBene_View_Bene_Type().getValue(19));                                                 //Natural: ASSIGN #BENE19-TYPE := BENE-VIEW.BENE-TYPE ( 19 )
            if (condition(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(19).greater(getZero())))                                                                       //Natural: IF BENE-VIEW.BENE-SSN-NBR ( 19 ) > 0
            {
                pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Ssn().setValueEdited(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(19),new ReportEditMask("999999999"));     //Natural: MOVE EDITED BENE-VIEW.BENE-SSN-NBR ( 19 ) ( EM = 999999999 ) TO #BENE19-SSN
                //*  1IRA
                //*  1IRA
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Name().setValue(ldaScil1280.getBene_View_Bene_Name().getValue(19));                                                 //Natural: ASSIGN #BENE19-NAME := BENE-VIEW.BENE-NAME ( 19 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Name2().setValue(ldaScil1280.getBene_View_Bene_Extended_Name().getValue(19));                                       //Natural: ASSIGN #BENE19-NAME2 := BENE-VIEW.BENE-EXTENDED-NAME ( 19 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Rel_Code().setValue(ldaScil1280.getBene_View_Bene_Relationship_Cde().getValue(19));                                 //Natural: ASSIGN #BENE19-REL-CODE := BENE-VIEW.BENE-RELATIONSHIP-CDE ( 19 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Rel_Desc().setValue(ldaScil1280.getBene_View_Bene_Relationship().getValue(19));                                     //Natural: ASSIGN #BENE19-REL-DESC := BENE-VIEW.BENE-RELATIONSHIP ( 19 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Birthdate().setValue(ldaScil1280.getBene_View_Bene_Dob().getValue(19));                                             //Natural: ASSIGN #BENE19-BIRTHDATE := BENE-VIEW.BENE-DOB ( 19 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Text_Line1().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(19,1));                                     //Natural: ASSIGN #BENE19-TEXT-LINE1 := BENE-VIEW.BENE-SPCL-TXT ( 19,1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Text_Line2().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(19,2));                                     //Natural: ASSIGN #BENE19-TEXT-LINE2 := BENE-VIEW.BENE-SPCL-TXT ( 19,2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Text_Line3().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(19,3));                                     //Natural: ASSIGN #BENE19-TEXT-LINE3 := BENE-VIEW.BENE-SPCL-TXT ( 19,3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Alloc_Method().setValue(ldaScil1280.getBene_View_Bene_Std_Txt_Ind().getValue(19));                                  //Natural: ASSIGN #BENE19-ALLOC-METHOD := BENE-VIEW.BENE-STD-TXT-IND ( 19 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Alloc_Pct().setValue(ldaScil1280.getBene_View_Bene_Alloc_Pct().getValue(19));                                       //Natural: ASSIGN #BENE19-ALLOC-PCT := BENE-VIEW.BENE-ALLOC-PCT ( 19 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Alloc_Num().setValue(ldaScil1280.getBene_View_Bene_Nmrtr_Nbr().getValue(19));                                       //Natural: ASSIGN #BENE19-ALLOC-NUM := BENE-VIEW.BENE-NMRTR-NBR ( 19 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Alloc_Denum().setValue(ldaScil1280.getBene_View_Bene_Dnmntr_Nbr().getValue(19));                                    //Natural: ASSIGN #BENE19-ALLOC-DENUM := BENE-VIEW.BENE-DNMNTR-NBR ( 19 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Addr_Line_1().setValue(ldaScil1280.getBene_View_Bene_Addr1().getValue(19));                                         //Natural: ASSIGN #BENE19-ADDR-LINE-1 := BENE-VIEW.BENE-ADDR1 ( 19 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Addr_Line_2().setValue(ldaScil1280.getBene_View_Bene_Addr2().getValue(19));                                         //Natural: ASSIGN #BENE19-ADDR-LINE-2 := BENE-VIEW.BENE-ADDR2 ( 19 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Addr_Line_3_City().setValue(ldaScil1280.getBene_View_Bene_Addr3_City().getValue(19));                               //Natural: ASSIGN #BENE19-ADDR-LINE-3-CITY := BENE-VIEW.BENE-ADDR3-CITY ( 19 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_State().setValue(ldaScil1280.getBene_View_Bene_State().getValue(19));                                               //Natural: ASSIGN #BENE19-STATE := BENE-VIEW.BENE-STATE ( 19 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Zip().setValue(ldaScil1280.getBene_View_Bene_Zip().getValue(19));                                                   //Natural: ASSIGN #BENE19-ZIP := BENE-VIEW.BENE-ZIP ( 19 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Phone().setValue(ldaScil1280.getBene_View_Bene_Phone().getValue(19));                                               //Natural: ASSIGN #BENE19-PHONE := BENE-VIEW.BENE-PHONE ( 19 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Gender().setValue(ldaScil1280.getBene_View_Bene_Gender().getValue(19));                                             //Natural: ASSIGN #BENE19-GENDER := BENE-VIEW.BENE-GENDER ( 19 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Country().setValue(ldaScil1280.getBene_View_Bene_Country().getValue(19));                                           //Natural: ASSIGN #BENE19-COUNTRY := BENE-VIEW.BENE-COUNTRY ( 19 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Type().setValue(ldaScil1280.getBene_View_Bene_Type().getValue(20));                                                 //Natural: ASSIGN #BENE20-TYPE := BENE-VIEW.BENE-TYPE ( 20 )
            if (condition(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(20).greater(getZero())))                                                                       //Natural: IF BENE-VIEW.BENE-SSN-NBR ( 20 ) > 0
            {
                pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Ssn().setValueEdited(ldaScil1280.getBene_View_Bene_Ssn_Nbr().getValue(20),new ReportEditMask("999999999"));     //Natural: MOVE EDITED BENE-VIEW.BENE-SSN-NBR ( 20 ) ( EM = 999999999 ) TO #BENE20-SSN
                //*  1IRA
                //*  1IRA
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
                //*  NY200
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Name().setValue(ldaScil1280.getBene_View_Bene_Name().getValue(20));                                                 //Natural: ASSIGN #BENE20-NAME := BENE-VIEW.BENE-NAME ( 20 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Name2().setValue(ldaScil1280.getBene_View_Bene_Extended_Name().getValue(20));                                       //Natural: ASSIGN #BENE20-NAME2 := BENE-VIEW.BENE-EXTENDED-NAME ( 20 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Rel_Code().setValue(ldaScil1280.getBene_View_Bene_Relationship_Cde().getValue(20));                                 //Natural: ASSIGN #BENE20-REL-CODE := BENE-VIEW.BENE-RELATIONSHIP-CDE ( 20 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Rel_Desc().setValue(ldaScil1280.getBene_View_Bene_Relationship().getValue(20));                                     //Natural: ASSIGN #BENE20-REL-DESC := BENE-VIEW.BENE-RELATIONSHIP ( 20 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Birthdate().setValue(ldaScil1280.getBene_View_Bene_Dob().getValue(20));                                             //Natural: ASSIGN #BENE20-BIRTHDATE := BENE-VIEW.BENE-DOB ( 20 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Text_Line1().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(20,1));                                     //Natural: ASSIGN #BENE20-TEXT-LINE1 := BENE-VIEW.BENE-SPCL-TXT ( 20,1 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Text_Line2().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(20,2));                                     //Natural: ASSIGN #BENE20-TEXT-LINE2 := BENE-VIEW.BENE-SPCL-TXT ( 20,2 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Text_Line3().setValue(ldaScil1280.getBene_View_Bene_Spcl_Txt().getValue(20,3));                                     //Natural: ASSIGN #BENE20-TEXT-LINE3 := BENE-VIEW.BENE-SPCL-TXT ( 20,3 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Alloc_Method().setValue(ldaScil1280.getBene_View_Bene_Std_Txt_Ind().getValue(20));                                  //Natural: ASSIGN #BENE20-ALLOC-METHOD := BENE-VIEW.BENE-STD-TXT-IND ( 20 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Alloc_Pct().setValue(ldaScil1280.getBene_View_Bene_Alloc_Pct().getValue(20));                                       //Natural: ASSIGN #BENE20-ALLOC-PCT := BENE-VIEW.BENE-ALLOC-PCT ( 20 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Alloc_Num().setValue(ldaScil1280.getBene_View_Bene_Nmrtr_Nbr().getValue(20));                                       //Natural: ASSIGN #BENE20-ALLOC-NUM := BENE-VIEW.BENE-NMRTR-NBR ( 20 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Alloc_Denum().setValue(ldaScil1280.getBene_View_Bene_Dnmntr_Nbr().getValue(20));                                    //Natural: ASSIGN #BENE20-ALLOC-DENUM := BENE-VIEW.BENE-DNMNTR-NBR ( 20 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Addr_Line_1().setValue(ldaScil1280.getBene_View_Bene_Addr1().getValue(20));                                         //Natural: ASSIGN #BENE20-ADDR-LINE-1 := BENE-VIEW.BENE-ADDR1 ( 20 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Addr_Line_2().setValue(ldaScil1280.getBene_View_Bene_Addr2().getValue(20));                                         //Natural: ASSIGN #BENE20-ADDR-LINE-2 := BENE-VIEW.BENE-ADDR2 ( 20 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Addr_Line_3_City().setValue(ldaScil1280.getBene_View_Bene_Addr3_City().getValue(20));                               //Natural: ASSIGN #BENE20-ADDR-LINE-3-CITY := BENE-VIEW.BENE-ADDR3-CITY ( 20 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_State().setValue(ldaScil1280.getBene_View_Bene_State().getValue(20));                                               //Natural: ASSIGN #BENE20-STATE := BENE-VIEW.BENE-STATE ( 20 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Zip().setValue(ldaScil1280.getBene_View_Bene_Zip().getValue(20));                                                   //Natural: ASSIGN #BENE20-ZIP := BENE-VIEW.BENE-ZIP ( 20 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Phone().setValue(ldaScil1280.getBene_View_Bene_Phone().getValue(20));                                               //Natural: ASSIGN #BENE20-PHONE := BENE-VIEW.BENE-PHONE ( 20 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Gender().setValue(ldaScil1280.getBene_View_Bene_Gender().getValue(20));                                             //Natural: ASSIGN #BENE20-GENDER := BENE-VIEW.BENE-GENDER ( 20 )
            pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Country().setValue(ldaScil1280.getBene_View_Bene_Country().getValue(20));                                           //Natural: ASSIGN #BENE20-COUNTRY := BENE-VIEW.BENE-COUNTRY ( 20 )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        pdaErla1000.getErla1000_Err_Pgm_Name().setValue(Global.getPROGRAM());                                                                                             //Natural: ASSIGN ERLA1000.ERR-PGM-NAME := *PROGRAM
        pdaErla1000.getErla1000_Err_Pgm_Level().setValue(Global.getLEVEL());                                                                                              //Natural: ASSIGN ERLA1000.ERR-PGM-LEVEL := *LEVEL
        pdaErla1000.getErla1000_Err_Nbr().setValue(Global.getERROR_NR());                                                                                                 //Natural: ASSIGN ERLA1000.ERR-NBR := *ERROR-NR
        pdaErla1000.getErla1000_Err_Line_Nbr().setValue(Global.getERROR_LINE());                                                                                          //Natural: ASSIGN ERLA1000.ERR-LINE-NBR := *ERROR-LINE
        pdaErla1000.getErla1000_Err_Status_Cde().setValue("U");                                                                                                           //Natural: ASSIGN ERLA1000.ERR-STATUS-CDE := 'U'
        pdaErla1000.getErla1000_Err_Type_Cde().setValue("N");                                                                                                             //Natural: ASSIGN ERLA1000.ERR-TYPE-CDE := 'N'
        pdaErla1000.getErla1000_Err_Notes().setValue(Global.getERROR());                                                                                                  //Natural: ASSIGN ERLA1000.ERR-NOTES := *ERROR
        DbsUtil.callnat(Erln1000.class , getCurrentProcessState(), pdaErla1000.getErla1000());                                                                            //Natural: CALLNAT 'ERLN1000' ERLA1000
        if (condition(Global.isEscape())) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        pdaScia1295.getPnd_Scia1295_Out_Pnd_Response_Code().setValue("9999");                                                                                             //Natural: ASSIGN #RESPONSE-CODE := '9999'
        pdaScia1295.getPnd_Scia1295_Out_Pnd_Response_Text().setValue(DbsUtil.compress("Natural error", Global.getERROR_NR(), "in program", Global.getPROGRAM(),           //Natural: COMPRESS 'Natural error' *ERROR-NR 'in program' *PROGRAM 'line number' *ERROR-LINE INTO #RESPONSE-TEXT
            "line number", Global.getERROR_LINE()));
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "ZP=ON SG=OFF");
    }
}
