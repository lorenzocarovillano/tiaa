/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:10:27 AM
**        * FROM NATURAL SUBPROGRAM : Scin9003
************************************************************
**        * FILE NAME            : Scin9003.java
**        * CLASS NAME           : Scin9003
**        * INSTANCE NAME        : Scin9003
************************************************************
************************************************************************
* PROGRAM  : SCIN9003
* SYSTEM   : ENROLLMENT PROCESSING
* AUTHOR   : CHARLES SINGLETON
* GENERATED: APRIL 18,2005
* FUNCTION : SUBPROGRAM FINDS PRAP RECORD TO MATCH AND RELEASE
*            TO CREATE THE LEGAL CONTRACT PACKAGE.
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* 5/7/2007 BILLIODEAU ADDED NEW FIELDS TO PRAP-VIEW: INSURER-1,
*                     INSURER-2, INSURER-3, 1035-EXCH-IND-1,
*                     1035-EXCH-IND-2, 1035-EXCH-IND-3
* 06/20/2017 ELLO     POINT TO THE NEW VIEW OF ANNTY-ACTVTY-PRAP_12 FOR
*                     EXPANDED PIN.  CHG425939   (PINE)
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Scin9003 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaScia9003 pdaScia9003;
    private PdaAcipda_D pdaAcipda_D;
    private PdaAcipda_M pdaAcipda_M;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_prap_View;
    private DbsField prap_View_Ap_Coll_Code;
    private DbsField prap_View_Ap_Soc_Sec;
    private DbsField prap_View_Ap_Status;
    private DbsField prap_View_Ap_Record_Type;
    private DbsField prap_View_Ap_Release_Ind;
    private DbsField prap_View_Ap_Tiaa_Cntrct;
    private DbsField prap_View_Ap_Cref_Cert;
    private DbsField prap_View_Ap_Cor_Last_Nme;
    private DbsField prap_View_Ap_Cor_First_Nme;
    private DbsField prap_View_Ap_Cor_Mddle_Nme;
    private DbsField prap_View_Ap_Sgrd_Plan_No;
    private DbsField prap_View_Ap_Sgrd_Subplan_No;
    private DbsField prap_View_Ap_Sgrd_Divsub;
    private DbsField prap_View_Ap_Text_Udf_1;
    private DbsField prap_View_Ap_Text_Udf_2;
    private DbsField prap_View_Ap_Text_Udf_3;
    private DbsField prap_View_Ap_Replacement_Ind;
    private DbsField prap_View_Ap_Register_Id;
    private DbsField prap_View_Ap_Agent_Or_Racf_Id;
    private DbsField prap_View_Ap_Exempt_Ind;
    private DbsField prap_View_Ap_Arr_Insurer_1;
    private DbsField prap_View_Ap_Arr_Insurer_2;
    private DbsField prap_View_Ap_Arr_Insurer_3;
    private DbsField prap_View_Ap_Arr_1035_Exch_Ind_1;
    private DbsField prap_View_Ap_Arr_1035_Exch_Ind_2;
    private DbsField prap_View_Ap_Arr_1035_Exch_Ind_3;
    private DbsField pnd_Selected_Isn;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAcipda_D = new PdaAcipda_D(localVariables);
        pdaAcipda_M = new PdaAcipda_M(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaScia9003 = new PdaScia9003(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_prap_View = new DataAccessProgramView(new NameInfo("vw_prap_View", "PRAP-VIEW"), "ANNTY_ACTVTY_PRAP_12", "ACIS_ANNTY_PRAP");
        prap_View_Ap_Coll_Code = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Coll_Code", "AP-COLL-CODE", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_COLL_CODE");
        prap_View_Ap_Soc_Sec = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, 
            "AP_SOC_SEC");
        prap_View_Ap_Status = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Status", "AP-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_STATUS");
        prap_View_Ap_Record_Type = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Record_Type", "AP-RECORD-TYPE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_RECORD_TYPE");
        prap_View_Ap_Release_Ind = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Release_Ind", "AP-RELEASE-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_RELEASE_IND");
        prap_View_Ap_Tiaa_Cntrct = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TIAA_CNTRCT");
        prap_View_Ap_Cref_Cert = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Cref_Cert", "AP-CREF-CERT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_CREF_CERT");
        prap_View_Ap_Cor_Last_Nme = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "AP_COR_LAST_NME");
        prap_View_Ap_Cor_First_Nme = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "AP_COR_FIRST_NME");
        prap_View_Ap_Cor_Mddle_Nme = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Cor_Mddle_Nme", "AP-COR-MDDLE-NME", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "AP_COR_MDDLE_NME");
        prap_View_Ap_Sgrd_Plan_No = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_SGRD_PLAN_NO");
        prap_View_Ap_Sgrd_Subplan_No = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Sgrd_Subplan_No", "AP-SGRD-SUBPLAN-NO", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "AP_SGRD_SUBPLAN_NO");
        prap_View_Ap_Sgrd_Divsub = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Sgrd_Divsub", "AP-SGRD-DIVSUB", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "AP_SGRD_DIVSUB");
        prap_View_Ap_Text_Udf_1 = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Text_Udf_1", "AP-TEXT-UDF-1", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TEXT_UDF_1");
        prap_View_Ap_Text_Udf_2 = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Text_Udf_2", "AP-TEXT-UDF-2", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TEXT_UDF_2");
        prap_View_Ap_Text_Udf_3 = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Text_Udf_3", "AP-TEXT-UDF-3", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TEXT_UDF_3");
        prap_View_Ap_Replacement_Ind = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Replacement_Ind", "AP-REPLACEMENT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_REPLACEMENT_IND");
        prap_View_Ap_Register_Id = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Register_Id", "AP-REGISTER-ID", FieldType.STRING, 11, RepeatingFieldStrategy.None, 
            "AP_REGISTER_ID");
        prap_View_Ap_Agent_Or_Racf_Id = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Agent_Or_Racf_Id", "AP-AGENT-OR-RACF-ID", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "AP_AGENT_OR_RACF_ID");
        prap_View_Ap_Exempt_Ind = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Exempt_Ind", "AP-EXEMPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_EXEMPT_IND");
        prap_View_Ap_Arr_Insurer_1 = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Arr_Insurer_1", "AP-ARR-INSURER-1", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "AP_ARR_INSURER_1");
        prap_View_Ap_Arr_Insurer_2 = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Arr_Insurer_2", "AP-ARR-INSURER-2", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "AP_ARR_INSURER_2");
        prap_View_Ap_Arr_Insurer_3 = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Arr_Insurer_3", "AP-ARR-INSURER-3", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "AP_ARR_INSURER_3");
        prap_View_Ap_Arr_1035_Exch_Ind_1 = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Arr_1035_Exch_Ind_1", "AP-ARR-1035-EXCH-IND-1", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_ARR_1035_EXCH_IND_1");
        prap_View_Ap_Arr_1035_Exch_Ind_2 = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Arr_1035_Exch_Ind_2", "AP-ARR-1035-EXCH-IND-2", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_ARR_1035_EXCH_IND_2");
        prap_View_Ap_Arr_1035_Exch_Ind_3 = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Arr_1035_Exch_Ind_3", "AP-ARR-1035-EXCH-IND-3", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_ARR_1035_EXCH_IND_3");
        registerRecord(vw_prap_View);

        pnd_Selected_Isn = localVariables.newFieldInRecord("pnd_Selected_Isn", "#SELECTED-ISN", FieldType.PACKED_DECIMAL, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_prap_View.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Scin9003() throws Exception
    {
        super("Scin9003");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* *======================================================================
        //* *    M A I N    P R O C E S S
        //* *======================================================================
        vw_prap_View.startDatabaseFind                                                                                                                                    //Natural: FIND PRAP-VIEW WITH AP-TIAA-CNTRCT EQ SCIA9003.#TIAA-CNTRCT WHERE AP-RECORD-TYPE EQ 1
        (
        "FINDREC",
        new Wc[] { new Wc("AP_TIAA_CNTRCT", "=", pdaScia9003.getScia9003_Pnd_Tiaa_Cntrct(), WcType.WITH) ,
        new Wc("AP_RECORD_TYPE", "=", 1, WcType.WHERE) }
        );
        FINDREC:
        while (condition(vw_prap_View.readNextRow("FINDREC", true)))
        {
            vw_prap_View.setIfNotFoundControlFlag(false);
            if (condition(vw_prap_View.getAstCOUNTER().equals(0)))                                                                                                        //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "NO PRAP RECORDS FOUND FOR: ",pdaScia9003.getScia9003_Pnd_Tiaa_Cntrct());                                                           //Natural: WRITE 'NO PRAP RECORDS FOUND FOR: 'SCIA9003.#TIAA-CNTRCT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FINDREC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FINDREC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(((prap_View_Ap_Sgrd_Plan_No.notEquals(pdaScia9003.getScia9003_Pnd_Plan_No())) || (prap_View_Ap_Sgrd_Subplan_No.notEquals(pdaScia9003.getScia9003_Pnd_Sub_Plan_No()))  //Natural: IF ( ( AP-SGRD-PLAN-NO NE SCIA9003.#PLAN-NO ) OR ( AP-SGRD-SUBPLAN-NO NE SCIA9003.#SUB-PLAN-NO ) OR ( AP-SOC-SEC NE SCIA9003.#SSN-N ) )
                || (prap_View_Ap_Soc_Sec.notEquals(pdaScia9003.getScia9003_Pnd_Ssn_N())))))
            {
                pdaScia9003.getScia9003_Pnd_Return_Msg().setValue("Plan/Subplan or SSN Do Not Match");                                                                    //Natural: MOVE 'Plan/Subplan or SSN Do Not Match' TO #RETURN-MSG
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  DELETED OR MATCH&RELEASE
            if (condition(prap_View_Ap_Status.equals("A") || prap_View_Ap_Status.equals("C")))                                                                            //Natural: IF AP-STATUS = 'A' OR = 'C'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Selected_Isn.setValue(vw_prap_View.getAstISN("FINDREC"));                                                                                                 //Natural: ASSIGN #SELECTED-ISN := *ISN
                                                                                                                                                                          //Natural: PERFORM UPDATE-PRAP-STATUS
            sub_Update_Prap_Status();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FINDREC"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FINDREC"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-PRAP-STATUS
        //* *======================================================================
    }
    private void sub_Update_Prap_Status() throws Exception                                                                                                                //Natural: UPDATE-PRAP-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        //* * CALL SCIN5200 TO MATCH & RELEASE PRAP RECORD **
        //* *==============================================**
        DbsUtil.callnat(Scin5200.class , getCurrentProcessState(), pnd_Selected_Isn, pdaAcipda_D.getDialog_Info_Sub(), pdaAcipda_M.getMsg_Info_Sub());                    //Natural: CALLNAT 'SCIN5200' #SELECTED-ISN DIALOG-INFO-SUB MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        if (condition(pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals("    ")))                                                                               //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE '    '
        {
            pdaScia9003.getScia9003_Pnd_Return_Code().setValue(pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code());                                                        //Natural: ASSIGN SCIA9003.#RETURN-CODE := MSG-INFO-SUB.##RETURN-CODE
            pdaScia9003.getScia9003_Pnd_Return_Msg().setValue(pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                 //Natural: ASSIGN SCIA9003.#RETURN-MSG := MSG-INFO-SUB.##MSG
        }                                                                                                                                                                 //Natural: END-IF
        //*  UPDATE-PRAP-STATUS
    }

    //
}
