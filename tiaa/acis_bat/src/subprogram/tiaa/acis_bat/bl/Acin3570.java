/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:48:26 PM
**        * FROM NATURAL SUBPROGRAM : Acin3570
************************************************************
**        * FILE NAME            : Acin3570.java
**        * CLASS NAME           : Acin3570
**        * INSTANCE NAME        : Acin3570
************************************************************
************************************************************************
* PROGRAM  : ACIN3570
* DATE     : 01/19/99
* FUNCTION : THIS SUBPROGRAM WILL RETURN BUCKET AND ACCUM CODE.
*            CALLING PROGRAM : ACIN3500, ACIN4000 AND ACIN3560
*
* MOD DATE  MOD BY    DESCRIPTION OF CHANGES
* --------  --------  --------------------------------------------------
*
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Acin3570 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAcia3570 pdaAcia3570;
    private LdaAcil7070 ldaAcil7070;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Table_Id_Entry_Cde;

    private DbsGroup pnd_Table_Id_Entry_Cde__R_Field_1;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde;
    private DbsField pnd_Entry_Dscrptn_Txt;

    private DbsGroup pnd_Entry_Dscrptn_Txt__R_Field_2;
    private DbsField pnd_Entry_Dscrptn_Txt_Pnd_Entry_Bucket_Accum;

    private DbsGroup pnd_Entry_Dscrptn_Txt__R_Field_3;
    private DbsField pnd_Entry_Dscrptn_Txt_Pnd_Entry_Bucket;
    private DbsField pnd_Entry_Dscrptn_Txt_Pnd_Entry_Accum;
    private DbsField pnd_Entry_Dscrptn_Txt_Pnd_Entry_Prap_Team;

    private DbsGroup pnd_Bucket_Accum_Array;
    private DbsField pnd_Bucket_Accum_Array_Pnd_Bucket_Accum;

    private DbsGroup pnd_Bucket_Accum_Array__R_Field_4;
    private DbsField pnd_Bucket_Accum_Array_Pnd_Bucket;
    private DbsField pnd_Bucket_Accum_Array_Pnd_Accum;
    private DbsField pnd_Prev_Bucket;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Index;
    private DbsField pnd_Debug;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAcil7070 = new LdaAcil7070();
        registerRecord(ldaAcil7070);
        registerRecord(ldaAcil7070.getVw_app_Table_Entry_View());

        // parameters
        parameters = new DbsRecord();
        pdaAcia3570 = new PdaAcia3570(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Table_Id_Entry_Cde = localVariables.newFieldInRecord("pnd_Table_Id_Entry_Cde", "#TABLE-ID-ENTRY-CDE", FieldType.STRING, 28);

        pnd_Table_Id_Entry_Cde__R_Field_1 = localVariables.newGroupInRecord("pnd_Table_Id_Entry_Cde__R_Field_1", "REDEFINE", pnd_Table_Id_Entry_Cde);
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr = pnd_Table_Id_Entry_Cde__R_Field_1.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr", 
            "#ENTRY-TABLE-ID-NBR", FieldType.NUMERIC, 6);
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id = pnd_Table_Id_Entry_Cde__R_Field_1.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id", 
            "#ENTRY-TABLE-SUB-ID", FieldType.STRING, 2);
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde = pnd_Table_Id_Entry_Cde__R_Field_1.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde", "#ENTRY-CDE", 
            FieldType.STRING, 8);
        pnd_Entry_Dscrptn_Txt = localVariables.newFieldInRecord("pnd_Entry_Dscrptn_Txt", "#ENTRY-DSCRPTN-TXT", FieldType.STRING, 60);

        pnd_Entry_Dscrptn_Txt__R_Field_2 = localVariables.newGroupInRecord("pnd_Entry_Dscrptn_Txt__R_Field_2", "REDEFINE", pnd_Entry_Dscrptn_Txt);
        pnd_Entry_Dscrptn_Txt_Pnd_Entry_Bucket_Accum = pnd_Entry_Dscrptn_Txt__R_Field_2.newFieldInGroup("pnd_Entry_Dscrptn_Txt_Pnd_Entry_Bucket_Accum", 
            "#ENTRY-BUCKET-ACCUM", FieldType.STRING, 4);

        pnd_Entry_Dscrptn_Txt__R_Field_3 = pnd_Entry_Dscrptn_Txt__R_Field_2.newGroupInGroup("pnd_Entry_Dscrptn_Txt__R_Field_3", "REDEFINE", pnd_Entry_Dscrptn_Txt_Pnd_Entry_Bucket_Accum);
        pnd_Entry_Dscrptn_Txt_Pnd_Entry_Bucket = pnd_Entry_Dscrptn_Txt__R_Field_3.newFieldInGroup("pnd_Entry_Dscrptn_Txt_Pnd_Entry_Bucket", "#ENTRY-BUCKET", 
            FieldType.STRING, 2);
        pnd_Entry_Dscrptn_Txt_Pnd_Entry_Accum = pnd_Entry_Dscrptn_Txt__R_Field_3.newFieldInGroup("pnd_Entry_Dscrptn_Txt_Pnd_Entry_Accum", "#ENTRY-ACCUM", 
            FieldType.STRING, 2);
        pnd_Entry_Dscrptn_Txt_Pnd_Entry_Prap_Team = pnd_Entry_Dscrptn_Txt__R_Field_2.newFieldInGroup("pnd_Entry_Dscrptn_Txt_Pnd_Entry_Prap_Team", "#ENTRY-PRAP-TEAM", 
            FieldType.STRING, 2);

        pnd_Bucket_Accum_Array = localVariables.newGroupArrayInRecord("pnd_Bucket_Accum_Array", "#BUCKET-ACCUM-ARRAY", new DbsArrayController(1, 99));
        pnd_Bucket_Accum_Array_Pnd_Bucket_Accum = pnd_Bucket_Accum_Array.newFieldInGroup("pnd_Bucket_Accum_Array_Pnd_Bucket_Accum", "#BUCKET-ACCUM", FieldType.STRING, 
            4);

        pnd_Bucket_Accum_Array__R_Field_4 = pnd_Bucket_Accum_Array.newGroupInGroup("pnd_Bucket_Accum_Array__R_Field_4", "REDEFINE", pnd_Bucket_Accum_Array_Pnd_Bucket_Accum);
        pnd_Bucket_Accum_Array_Pnd_Bucket = pnd_Bucket_Accum_Array__R_Field_4.newFieldInGroup("pnd_Bucket_Accum_Array_Pnd_Bucket", "#BUCKET", FieldType.STRING, 
            2);
        pnd_Bucket_Accum_Array_Pnd_Accum = pnd_Bucket_Accum_Array__R_Field_4.newFieldInGroup("pnd_Bucket_Accum_Array_Pnd_Accum", "#ACCUM", FieldType.STRING, 
            2);
        pnd_Prev_Bucket = localVariables.newFieldInRecord("pnd_Prev_Bucket", "#PREV-BUCKET", FieldType.STRING, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.PACKED_DECIMAL, 5);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAcil7070.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Acin3570() throws Exception
    {
        super("Acin3570");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Entry_Dscrptn_Txt.reset();                                                                                                                                    //Natural: RESET #ENTRY-DSCRPTN-TXT #BUCKET-ACCUM-ARRAY ( * ) #I #PREV-BUCKET
        pnd_Bucket_Accum_Array.getValue("*").reset();
        pnd_I.reset();
        pnd_Prev_Bucket.reset();
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr.setValue(100);                                                                                                      //Natural: ASSIGN #ENTRY-TABLE-ID-NBR = 000100
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id.setValue("TM");                                                                                                     //Natural: ASSIGN #ENTRY-TABLE-SUB-ID = 'TM'
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde.setValue(" ");                                                                                                               //Natural: ASSIGN #ENTRY-CDE = ' '
        if (condition(pdaAcia3570.getAcia3570_Parameter_Pnd_P_Get_All_Buckets_Sw().equals("Y")))                                                                          //Natural: IF #P-GET-ALL-BUCKETS-SW = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM GET-ALL-BUCKET-ACCUM
            sub_Get_All_Bucket_Accum();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM GET-PREMIUM-TEAM-CDE
            sub_Get_Premium_Team_Cde();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ALL-BUCKET-ACCUM
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PREMIUM-TEAM-CDE
    }
    private void sub_Get_All_Bucket_Accum() throws Exception                                                                                                              //Natural: GET-ALL-BUCKET-ACCUM
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        ldaAcil7070.getVw_app_Table_Entry_View().startDatabaseRead                                                                                                        //Natural: READ APP-TABLE-ENTRY-VIEW BY TABLE-ID-ENTRY-CDE STARTING FROM #TABLE-ID-ENTRY-CDE
        (
        "READ01",
        new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", ">=", pnd_Table_Id_Entry_Cde, WcType.BY) },
        new Oc[] { new Oc("TABLE_ID_ENTRY_CDE", "ASC") }
        );
        READ01:
        while (condition(ldaAcil7070.getVw_app_Table_Entry_View().readNextRow("READ01")))
        {
            if (condition(ldaAcil7070.getApp_Table_Entry_View_Entry_Table_Id_Nbr().notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr) || ldaAcil7070.getApp_Table_Entry_View_Entry_Table_Sub_Id().notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id))) //Natural: IF ENTRY-TABLE-ID-NBR NE #ENTRY-TABLE-ID-NBR OR ENTRY-TABLE-SUB-ID NE #ENTRY-TABLE-SUB-ID
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Entry_Dscrptn_Txt.setValue(ldaAcil7070.getApp_Table_Entry_View_Entry_Dscrptn_Txt());                                                                      //Natural: ASSIGN #ENTRY-DSCRPTN-TXT = ENTRY-DSCRPTN-TXT
            //* *     IF  #DEBUG
            //* *         WRITE '    --- READ APP-TABLE-ENTRY - GET ALL BUCKET ACCUM '
            //* *        / '    * BEFORE #ENTRY-BUCKET-ACCUM : ' #ENTRY-BUCKET-ACCUM
            //* *     END-IF
            DbsUtil.examine(new ExamineSource(pnd_Bucket_Accum_Array_Pnd_Bucket_Accum.getValue("*"),true), new ExamineSearch(pnd_Entry_Dscrptn_Txt_Pnd_Entry_Bucket_Accum),  //Natural: EXAMINE FULL #BUCKET-ACCUM ( * ) FOR #ENTRY-BUCKET-ACCUM GIVING #INDEX
                new ExamineGivingNumber(pnd_Index));
            //* *     IF  #DEBUG
            //* *         WRITE '    EXAMINE  GIVING #INDEX : ' #INDEX
            //* *         / '=' #ENTRY-BUCKET-ACCUM
            //* *         / '=' #BUCKET-ACCUM (1)  #BUCKET-ACCUM (2)  #BUCKET-ACCUM (3)
            //* *     END-IF
            if (condition(pnd_Index.equals(getZero())))                                                                                                                   //Natural: IF #INDEX = 0
            {
                FOR01:                                                                                                                                                    //Natural: FOR #I = 1 TO 99
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(99)); pnd_I.nadd(1))
                {
                    if (condition(pnd_Bucket_Accum_Array_Pnd_Bucket.getValue(pnd_I).equals(" ")))                                                                         //Natural: IF #BUCKET ( #I ) = ' '
                    {
                        pnd_Bucket_Accum_Array_Pnd_Bucket.getValue(pnd_I).setValue(pnd_Entry_Dscrptn_Txt_Pnd_Entry_Bucket);                                               //Natural: ASSIGN #BUCKET ( #I ) = #ENTRY-BUCKET
                        pnd_Bucket_Accum_Array_Pnd_Accum.getValue(pnd_I).setValue(pnd_Entry_Dscrptn_Txt_Pnd_Entry_Accum);                                                 //Natural: ASSIGN #ACCUM ( #I ) = #ENTRY-ACCUM
                        //* *                IF  #DEBUG
                        //* *                   WRITE '   AFTER  #BUCKET(#I) ' #I  '-' #BUCKET (#I)
                        //* *                       / '          #ACCUM (#I) ' #I  '-' #ACCUM (#I)
                        //* *                END-IF
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pdaAcia3570.getAcia3570_Parameter_Pnd_P_Bucket_Accum().getValue("*").setValue(pnd_Bucket_Accum_Array_Pnd_Bucket_Accum.getValue("*"));                             //Natural: ASSIGN #P-BUCKET-ACCUM ( * ) = #BUCKET-ACCUM ( * )
    }
    private void sub_Get_Premium_Team_Cde() throws Exception                                                                                                              //Natural: GET-PREMIUM-TEAM-CDE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde.setValue(pdaAcia3570.getAcia3570_Parameter_Pnd_P_Iis_Premium_Team_Cde());                                                    //Natural: ASSIGN #ENTRY-CDE = #P-IIS-PREMIUM-TEAM-CDE
        ldaAcil7070.getVw_app_Table_Entry_View().startDatabaseRead                                                                                                        //Natural: READ APP-TABLE-ENTRY-VIEW BY TABLE-ID-ENTRY-CDE STARTING FROM #TABLE-ID-ENTRY-CDE
        (
        "READ02",
        new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", ">=", pnd_Table_Id_Entry_Cde, WcType.BY) },
        new Oc[] { new Oc("TABLE_ID_ENTRY_CDE", "ASC") }
        );
        READ02:
        while (condition(ldaAcil7070.getVw_app_Table_Entry_View().readNextRow("READ02")))
        {
            if (condition(ldaAcil7070.getApp_Table_Entry_View_Entry_Table_Id_Nbr().notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr) || ldaAcil7070.getApp_Table_Entry_View_Entry_Table_Sub_Id().notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id)  //Natural: IF ENTRY-TABLE-ID-NBR NE #ENTRY-TABLE-ID-NBR OR ENTRY-TABLE-SUB-ID NE #ENTRY-TABLE-SUB-ID OR ENTRY-CDE NE #ENTRY-CDE
                || ldaAcil7070.getApp_Table_Entry_View_Entry_Cde().notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pdaAcia3570.getAcia3570_Parameter_Pnd_P_Entry_Dscrptn_Txt().setValue(ldaAcil7070.getApp_Table_Entry_View_Entry_Dscrptn_Txt());                                //Natural: ASSIGN #P-ENTRY-DSCRPTN-TXT = ENTRY-DSCRPTN-TXT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
