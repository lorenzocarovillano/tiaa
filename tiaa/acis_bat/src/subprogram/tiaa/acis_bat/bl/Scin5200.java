/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:09:41 AM
**        * FROM NATURAL SUBPROGRAM : Scin5200
************************************************************
**        * FILE NAME            : Scin5200.java
**        * CLASS NAME           : Scin5200
**        * INSTANCE NAME        : Scin5200
************************************************************
************************************************************************
* PROGRAM      : SCIN5200
* DATE WRITTEN : MAR 25,2004
* DESCRIPTION  : UPDATE PRAP RECORD WITH A M & R STATUS FOR SGRD
*                CONTRACTS.
* HISTORY
* 05/23/17 B.ELLO     PIN EXPANSION CHANGES
*                     CHANGED TO POINT TO THE NEW VIEW
*                     ANNTY-ACTVTY-PRAP_12 (PINE)
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Scin5200 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAcipda_D pdaAcipda_D;
    private PdaAcipda_M pdaAcipda_M;
    private PdaAcia1050 pdaAcia1050;
    private PdaAcia3521 pdaAcia3521;
    private PdaAcia2100 pdaAcia2100;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Selected_Isn;

    private DataAccessProgramView vw_annty_Actvty_Prap;
    private DbsField annty_Actvty_Prap_Ap_Coll_Code;
    private DbsField annty_Actvty_Prap_Ap_G_Key;
    private DbsField annty_Actvty_Prap_Ap_G_Ind;
    private DbsField annty_Actvty_Prap_Ap_Soc_Sec;
    private DbsField annty_Actvty_Prap_Ap_Dob;
    private DbsField annty_Actvty_Prap_Ap_Lob;
    private DbsField annty_Actvty_Prap_Ap_Bill_Code;
    private DbsField annty_Actvty_Prap_Ap_T_Doi;
    private DbsField annty_Actvty_Prap_Ap_C_Doi;
    private DbsField annty_Actvty_Prap_Ap_Curr;
    private DbsField annty_Actvty_Prap_Ap_T_Age_1st;
    private DbsField annty_Actvty_Prap_Ap_C_Age_1st;
    private DbsField annty_Actvty_Prap_Ap_Ownership;
    private DbsField annty_Actvty_Prap_Ap_Sex;
    private DbsField annty_Actvty_Prap_Ap_Mail_Zip;
    private DbsField annty_Actvty_Prap_Ap_Name_Addr_Cd;
    private DbsField annty_Actvty_Prap_Ap_Dt_Ent_Sys;
    private DbsField annty_Actvty_Prap_Ap_Dt_Released;
    private DbsField annty_Actvty_Prap_Ap_Dt_Matched;
    private DbsField annty_Actvty_Prap_Ap_Dt_Deleted;
    private DbsField annty_Actvty_Prap_Ap_Dt_Withdrawn;
    private DbsField annty_Actvty_Prap_Ap_Alloc_Discr;
    private DbsField annty_Actvty_Prap_Ap_Release_Ind;
    private DbsField annty_Actvty_Prap_Ap_Type;
    private DbsField annty_Actvty_Prap_Ap_Other_Pols;
    private DbsField annty_Actvty_Prap_Ap_Record_Type;
    private DbsField annty_Actvty_Prap_Ap_Status;
    private DbsField annty_Actvty_Prap_Ap_Numb_Dailys;
    private DbsField annty_Actvty_Prap_Ap_Dt_App_Recvd;
    private DbsField annty_Actvty_Prap_Ap_App_Source;
    private DbsField annty_Actvty_Prap_Ap_Region_Code;

    private DbsGroup annty_Actvty_Prap__R_Field_1;
    private DbsField annty_Actvty_Prap_Ap_Region_Code_1_1;
    private DbsField annty_Actvty_Prap_Ap_Region_Code_2_2;
    private DbsField annty_Actvty_Prap_Ap_Region_Code_3_3;
    private DbsField annty_Actvty_Prap_Ap_Orig_Issue_State;
    private DbsField annty_Actvty_Prap_Ap_Ownership_Type;
    private DbsField annty_Actvty_Prap_Ap_Lob_Type;
    private DbsField annty_Actvty_Prap_Ap_Split_Code;

    private DbsGroup annty_Actvty_Prap_Ap_Address_Info;
    private DbsField annty_Actvty_Prap_Ap_Address_Line;
    private DbsField annty_Actvty_Prap_Ap_City;
    private DbsField annty_Actvty_Prap_Ap_Current_State_Code;
    private DbsField annty_Actvty_Prap_Ap_Dana_Transaction_Cd;
    private DbsField annty_Actvty_Prap_Ap_Dana_Stndrd_Rtn_Cd;
    private DbsField annty_Actvty_Prap_Ap_Dana_Finalist_Reason_Cd;
    private DbsField annty_Actvty_Prap_Ap_Dana_Addr_Stndrd_Code;
    private DbsField annty_Actvty_Prap_Ap_Dana_Stndrd_Overide;
    private DbsField annty_Actvty_Prap_Ap_Dana_Postal_Data_Fields;
    private DbsField annty_Actvty_Prap_Ap_Dana_Addr_Geographic_Code;
    private DbsField annty_Actvty_Prap_Ap_Annuity_Start_Date;

    private DbsGroup annty_Actvty_Prap_Ap_Maximum_Alloc_Pct;
    private DbsField annty_Actvty_Prap_Ap_Max_Alloc_Pct;
    private DbsField annty_Actvty_Prap_Ap_Max_Alloc_Pct_Sign;
    private DbsField annty_Actvty_Prap_Ap_Bene_Info_Type;
    private DbsField annty_Actvty_Prap_Ap_Primary_Std_Ent;

    private DbsGroup annty_Actvty_Prap_Ap_Bene_Primary_Info;
    private DbsField annty_Actvty_Prap_Ap_Primary_Bene_Info;
    private DbsField annty_Actvty_Prap_Ap_Contingent_Std_Ent;

    private DbsGroup annty_Actvty_Prap_Ap_Bene_Contingent_Info;
    private DbsField annty_Actvty_Prap_Ap_Contingent_Bene_Info;
    private DbsField annty_Actvty_Prap_Ap_Bene_Estate;
    private DbsField annty_Actvty_Prap_Ap_Bene_Trust;
    private DbsField annty_Actvty_Prap_Ap_Bene_Category;
    private DbsField annty_Actvty_Prap_Ap_Mail_Instructions;
    private DbsField annty_Actvty_Prap_Ap_Eop_Addl_Cref_Request;
    private DbsField annty_Actvty_Prap_Ap_Process_Status;
    private DbsField annty_Actvty_Prap_Ap_Coll_St_Cd;
    private DbsField annty_Actvty_Prap_Ap_Csm_Sec_Seg;
    private DbsField annty_Actvty_Prap_Ap_Rlc_College;
    private DbsField annty_Actvty_Prap_Ap_Cor_Prfx_Nme;
    private DbsField annty_Actvty_Prap_Ap_Cor_Last_Nme;

    private DbsGroup annty_Actvty_Prap__R_Field_2;
    private DbsField annty_Actvty_Prap_Ap_Cor_Last_Nme_1_12;
    private DbsField annty_Actvty_Prap_Ap_Cor_First_Nme;

    private DbsGroup annty_Actvty_Prap__R_Field_3;
    private DbsField annty_Actvty_Prap_Ap_Cor_First_Nme_1_5;
    private DbsField annty_Actvty_Prap_Ap_Cor_Mddle_Nme;
    private DbsField annty_Actvty_Prap_Ap_Cor_Sffx_Nme;
    private DbsField annty_Actvty_Prap_Ap_Ph_Hist_Ind;
    private DbsField annty_Actvty_Prap_Ap_Rcrd_Updt_Tm_Stamp;
    private DbsField annty_Actvty_Prap_Ap_Contact_Mode;
    private DbsField annty_Actvty_Prap_Ap_Racf_Id;
    private DbsField annty_Actvty_Prap_Ap_Pin_Nbr;

    private DbsGroup annty_Actvty_Prap_Ap_Rqst_Log_Dte_Tm;
    private DbsField annty_Actvty_Prap_Ap_Rqst_Log_Dte_Time;
    private DbsField annty_Actvty_Prap_Ap_Sync_Ind;
    private DbsField annty_Actvty_Prap_Ap_Cor_Ind;
    private DbsField annty_Actvty_Prap_Ap_Alloc_Ind;
    private DbsField annty_Actvty_Prap_Ap_Tiaa_Doi;
    private DbsField annty_Actvty_Prap_Ap_Cref_Doi;

    private DbsGroup annty_Actvty_Prap_Ap_Mit_Request;
    private DbsField annty_Actvty_Prap_Ap_Mit_Unit;
    private DbsField annty_Actvty_Prap_Ap_Mit_Wpid;
    private DbsField annty_Actvty_Prap_Ap_Ira_Rollover_Type;
    private DbsField annty_Actvty_Prap_Ap_Ira_Record_Type;
    private DbsField annty_Actvty_Prap_Ap_Mult_App_Status;
    private DbsField annty_Actvty_Prap_Ap_Mult_App_Lob;
    private DbsField annty_Actvty_Prap_Ap_Mult_App_Lob_Type;
    private DbsField annty_Actvty_Prap_Ap_Mult_App_Ppg;
    private DbsField annty_Actvty_Prap_Ap_Print_Date;

    private DbsGroup annty_Actvty_Prap_Ap_Cntrct_Info;
    private DbsField annty_Actvty_Prap_Ap_Cntrct_Type;
    private DbsField annty_Actvty_Prap_Ap_Cntrct_Nbr;
    private DbsField annty_Actvty_Prap_Ap_Cntrct_Proceeds_Amt;

    private DbsGroup annty_Actvty_Prap_Ap_Addr_Info;
    private DbsField annty_Actvty_Prap_Ap_Address_Txt;
    private DbsField annty_Actvty_Prap_Ap_Address_Dest_Name;
    private DbsField annty_Actvty_Prap_Ap_Zip_Code;
    private DbsField annty_Actvty_Prap_Ap_Bank_Pymnt_Acct_Nmbr;
    private DbsField annty_Actvty_Prap_Ap_Bank_Aba_Acct_Nmbr;
    private DbsField annty_Actvty_Prap_Ap_Addr_Usage_Code;
    private DbsField annty_Actvty_Prap_Ap_Init_Paymt_Amt;
    private DbsField annty_Actvty_Prap_Ap_Init_Paymt_Pct;
    private DbsField annty_Actvty_Prap_Ap_Financial_1;
    private DbsField annty_Actvty_Prap_Ap_Financial_2;
    private DbsField annty_Actvty_Prap_Ap_Financial_3;
    private DbsField annty_Actvty_Prap_Ap_Financial_4;
    private DbsField annty_Actvty_Prap_Ap_Financial_5;
    private DbsField annty_Actvty_Prap_Ap_Tiaa_Cntrct;
    private DbsField annty_Actvty_Prap_Ap_Cref_Cert;
    private DbsField annty_Actvty_Prap_Ap_Rlc_Cref_Cert;
    private DbsField annty_Actvty_Prap_Ap_Allocation_Model_Type;
    private DbsField annty_Actvty_Prap_Ap_Divorce_Ind;
    private DbsField annty_Actvty_Prap_Ap_Email_Address;
    private DbsField annty_Actvty_Prap_Ap_E_Signed_Appl_Ind;
    private DbsField annty_Actvty_Prap_Ap_Eft_Requested_Ind;
    private DbsField annty_Actvty_Prap_Ap_Phone_No;

    private DbsGroup annty_Actvty_Prap_Ap_Fund_Identifier;
    private DbsField annty_Actvty_Prap_Ap_Fund_Cde;
    private DbsField annty_Actvty_Prap_Ap_Allocation_Pct;

    private DbsGroup pnd_Miscellanous_Variable;
    private DbsField pnd_Miscellanous_Variable_Pnd_Date_Mmddyyyy_A;

    private DbsGroup pnd_Miscellanous_Variable__R_Field_4;
    private DbsField pnd_Miscellanous_Variable_Pnd_Date_Mmddyyyy_N;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAcia1050 = new PdaAcia1050(localVariables);
        pdaAcia3521 = new PdaAcia3521(localVariables);
        pdaAcia2100 = new PdaAcia2100(localVariables);

        // parameters
        parameters = new DbsRecord();
        pnd_Selected_Isn = parameters.newFieldInRecord("pnd_Selected_Isn", "#SELECTED-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Selected_Isn.setParameterOption(ParameterOption.ByReference);
        pdaAcipda_D = new PdaAcipda_D(parameters);
        pdaAcipda_M = new PdaAcipda_M(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_annty_Actvty_Prap = new DataAccessProgramView(new NameInfo("vw_annty_Actvty_Prap", "ANNTY-ACTVTY-PRAP"), "ANNTY_ACTVTY_PRAP_12", "ACIS_ANNTY_PRAP", 
            DdmPeriodicGroups.getInstance().getGroups("ANNTY_ACTVTY_PRAP_12"));
        annty_Actvty_Prap_Ap_Coll_Code = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Coll_Code", "AP-COLL-CODE", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "AP_COLL_CODE");
        annty_Actvty_Prap_Ap_G_Key = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_G_Key", "AP-G-KEY", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_G_KEY");
        annty_Actvty_Prap_Ap_G_Ind = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_G_Ind", "AP-G-IND", FieldType.NUMERIC, 1, 
            RepeatingFieldStrategy.None, "AP_G_IND");
        annty_Actvty_Prap_Ap_Soc_Sec = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "AP_SOC_SEC");
        annty_Actvty_Prap_Ap_Dob = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Dob", "AP-DOB", FieldType.PACKED_DECIMAL, 8, 
            RepeatingFieldStrategy.None, "AP_DOB");
        annty_Actvty_Prap_Ap_Lob = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Lob", "AP-LOB", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_LOB");
        annty_Actvty_Prap_Ap_Lob.setDdmHeader("LOB");
        annty_Actvty_Prap_Ap_Bill_Code = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Bill_Code", "AP-BILL-CODE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_BILL_CODE");
        annty_Actvty_Prap_Ap_T_Doi = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_T_Doi", "AP-T-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_T_DOI");
        annty_Actvty_Prap_Ap_C_Doi = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_C_Doi", "AP-C-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_C_DOI");
        annty_Actvty_Prap_Ap_Curr = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Curr", "AP-CURR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_CURR");
        annty_Actvty_Prap_Ap_T_Age_1st = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_T_Age_1st", "AP-T-AGE-1ST", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_T_AGE_1ST");
        annty_Actvty_Prap_Ap_C_Age_1st = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_C_Age_1st", "AP-C-AGE-1ST", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_C_AGE_1ST");
        annty_Actvty_Prap_Ap_Ownership = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Ownership", "AP-OWNERSHIP", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_OWNERSHIP");
        annty_Actvty_Prap_Ap_Sex = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Sex", "AP-SEX", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_SEX");
        annty_Actvty_Prap_Ap_Sex.setDdmHeader("SEX CDE");
        annty_Actvty_Prap_Ap_Mail_Zip = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Mail_Zip", "AP-MAIL-ZIP", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "AP_MAIL_ZIP");
        annty_Actvty_Prap_Ap_Name_Addr_Cd = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Name_Addr_Cd", "AP-NAME-ADDR-CD", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "AP_NAME_ADDR_CD");
        annty_Actvty_Prap_Ap_Dt_Ent_Sys = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Dt_Ent_Sys", "AP-DT-ENT-SYS", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "AP_DT_ENT_SYS");
        annty_Actvty_Prap_Ap_Dt_Released = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Dt_Released", "AP-DT-RELEASED", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "AP_DT_RELEASED");
        annty_Actvty_Prap_Ap_Dt_Matched = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Dt_Matched", "AP-DT-MATCHED", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "AP_DT_MATCHED");
        annty_Actvty_Prap_Ap_Dt_Deleted = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Dt_Deleted", "AP-DT-DELETED", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "AP_DT_DELETED");
        annty_Actvty_Prap_Ap_Dt_Withdrawn = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Dt_Withdrawn", "AP-DT-WITHDRAWN", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "AP_DT_WITHDRAWN");
        annty_Actvty_Prap_Ap_Alloc_Discr = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Alloc_Discr", "AP-ALLOC-DISCR", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_ALLOC_DISCR");
        annty_Actvty_Prap_Ap_Release_Ind = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Release_Ind", "AP-RELEASE-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_RELEASE_IND");
        annty_Actvty_Prap_Ap_Type = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Type", "AP-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_TYPE");
        annty_Actvty_Prap_Ap_Other_Pols = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Other_Pols", "AP-OTHER-POLS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_OTHER_POLS");
        annty_Actvty_Prap_Ap_Record_Type = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Record_Type", "AP-RECORD-TYPE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_RECORD_TYPE");
        annty_Actvty_Prap_Ap_Status = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Status", "AP-STATUS", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "AP_STATUS");
        annty_Actvty_Prap_Ap_Numb_Dailys = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Numb_Dailys", "AP-NUMB-DAILYS", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_NUMB_DAILYS");
        annty_Actvty_Prap_Ap_Dt_App_Recvd = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Dt_App_Recvd", "AP-DT-APP-RECVD", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "AP_DT_APP_RECVD");
        annty_Actvty_Prap_Ap_App_Source = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_App_Source", "AP-APP-SOURCE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_APP_SOURCE");
        annty_Actvty_Prap_Ap_App_Source.setDdmHeader("SOURCE");
        annty_Actvty_Prap_Ap_Region_Code = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Region_Code", "AP-REGION-CODE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "AP_REGION_CODE");

        annty_Actvty_Prap__R_Field_1 = vw_annty_Actvty_Prap.getRecord().newGroupInGroup("annty_Actvty_Prap__R_Field_1", "REDEFINE", annty_Actvty_Prap_Ap_Region_Code);
        annty_Actvty_Prap_Ap_Region_Code_1_1 = annty_Actvty_Prap__R_Field_1.newFieldInGroup("annty_Actvty_Prap_Ap_Region_Code_1_1", "AP-REGION-CODE-1-1", 
            FieldType.STRING, 1);
        annty_Actvty_Prap_Ap_Region_Code_2_2 = annty_Actvty_Prap__R_Field_1.newFieldInGroup("annty_Actvty_Prap_Ap_Region_Code_2_2", "AP-REGION-CODE-2-2", 
            FieldType.STRING, 1);
        annty_Actvty_Prap_Ap_Region_Code_3_3 = annty_Actvty_Prap__R_Field_1.newFieldInGroup("annty_Actvty_Prap_Ap_Region_Code_3_3", "AP-REGION-CODE-3-3", 
            FieldType.STRING, 1);
        annty_Actvty_Prap_Ap_Orig_Issue_State = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Orig_Issue_State", "AP-ORIG-ISSUE-STATE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_ORIG_ISSUE_STATE");
        annty_Actvty_Prap_Ap_Ownership_Type = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Ownership_Type", "AP-OWNERSHIP-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_OWNERSHIP_TYPE");
        annty_Actvty_Prap_Ap_Ownership_Type.setDdmHeader("CNTRCT OWNERSHIP");
        annty_Actvty_Prap_Ap_Lob_Type = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Lob_Type", "AP-LOB-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_LOB_TYPE");
        annty_Actvty_Prap_Ap_Lob_Type.setDdmHeader("LOB/TYPE");
        annty_Actvty_Prap_Ap_Split_Code = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Split_Code", "AP-SPLIT-CODE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "AP_SPLIT_CODE");

        annty_Actvty_Prap_Ap_Address_Info = vw_annty_Actvty_Prap.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_Ap_Address_Info", "AP-ADDRESS-INFO", 
            new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        annty_Actvty_Prap_Ap_Address_Line = annty_Actvty_Prap_Ap_Address_Info.newFieldInGroup("annty_Actvty_Prap_Ap_Address_Line", "AP-ADDRESS-LINE", 
            FieldType.STRING, 35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ADDRESS_LINE", "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        annty_Actvty_Prap_Ap_City = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_City", "AP-CITY", FieldType.STRING, 27, RepeatingFieldStrategy.None, 
            "AP_CITY");
        annty_Actvty_Prap_Ap_Current_State_Code = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Current_State_Code", "AP-CURRENT-STATE-CODE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_CURRENT_STATE_CODE");
        annty_Actvty_Prap_Ap_Dana_Transaction_Cd = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Dana_Transaction_Cd", "AP-DANA-TRANSACTION-CD", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_DANA_TRANSACTION_CD");
        annty_Actvty_Prap_Ap_Dana_Stndrd_Rtn_Cd = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Dana_Stndrd_Rtn_Cd", "AP-DANA-STNDRD-RTN-CD", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_DANA_STNDRD_RTN_CD");
        annty_Actvty_Prap_Ap_Dana_Finalist_Reason_Cd = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Dana_Finalist_Reason_Cd", 
            "AP-DANA-FINALIST-REASON-CD", FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_DANA_FINALIST_REASON_CD");
        annty_Actvty_Prap_Ap_Dana_Addr_Stndrd_Code = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Dana_Addr_Stndrd_Code", "AP-DANA-ADDR-STNDRD-CODE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_DANA_ADDR_STNDRD_CODE");
        annty_Actvty_Prap_Ap_Dana_Stndrd_Overide = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Dana_Stndrd_Overide", "AP-DANA-STNDRD-OVERIDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_DANA_STNDRD_OVERIDE");
        annty_Actvty_Prap_Ap_Dana_Postal_Data_Fields = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Dana_Postal_Data_Fields", 
            "AP-DANA-POSTAL-DATA-FIELDS", FieldType.STRING, 44, RepeatingFieldStrategy.None, "AP_DANA_POSTAL_DATA_FIELDS");
        annty_Actvty_Prap_Ap_Dana_Addr_Geographic_Code = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Dana_Addr_Geographic_Code", 
            "AP-DANA-ADDR-GEOGRAPHIC-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_DANA_ADDR_GEOGRAPHIC_CODE");
        annty_Actvty_Prap_Ap_Annuity_Start_Date = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Annuity_Start_Date", "AP-ANNUITY-START-DATE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_ANNUITY_START_DATE");

        annty_Actvty_Prap_Ap_Maximum_Alloc_Pct = vw_annty_Actvty_Prap.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_Ap_Maximum_Alloc_Pct", "AP-MAXIMUM-ALLOC-PCT", 
            new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        annty_Actvty_Prap_Ap_Max_Alloc_Pct = annty_Actvty_Prap_Ap_Maximum_Alloc_Pct.newFieldInGroup("annty_Actvty_Prap_Ap_Max_Alloc_Pct", "AP-MAX-ALLOC-PCT", 
            FieldType.STRING, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MAX_ALLOC_PCT", "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        annty_Actvty_Prap_Ap_Max_Alloc_Pct_Sign = annty_Actvty_Prap_Ap_Maximum_Alloc_Pct.newFieldInGroup("annty_Actvty_Prap_Ap_Max_Alloc_Pct_Sign", "AP-MAX-ALLOC-PCT-SIGN", 
            FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MAX_ALLOC_PCT_SIGN", "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        annty_Actvty_Prap_Ap_Bene_Info_Type = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Bene_Info_Type", "AP-BENE-INFO-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_INFO_TYPE");
        annty_Actvty_Prap_Ap_Primary_Std_Ent = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Primary_Std_Ent", "AP-PRIMARY-STD-ENT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_PRIMARY_STD_ENT");

        annty_Actvty_Prap_Ap_Bene_Primary_Info = vw_annty_Actvty_Prap.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_Ap_Bene_Primary_Info", "AP-BENE-PRIMARY-INFO", 
            new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_BENE_PRIMARY_INFO");
        annty_Actvty_Prap_Ap_Primary_Bene_Info = annty_Actvty_Prap_Ap_Bene_Primary_Info.newFieldInGroup("annty_Actvty_Prap_Ap_Primary_Bene_Info", "AP-PRIMARY-BENE-INFO", 
            FieldType.STRING, 72, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_PRIMARY_BENE_INFO", "ACIS_ANNTY_PRAP_AP_BENE_PRIMARY_INFO");
        annty_Actvty_Prap_Ap_Contingent_Std_Ent = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Contingent_Std_Ent", "AP-CONTINGENT-STD-ENT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_CONTINGENT_STD_ENT");

        annty_Actvty_Prap_Ap_Bene_Contingent_Info = vw_annty_Actvty_Prap.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_Ap_Bene_Contingent_Info", 
            "AP-BENE-CONTINGENT-INFO", new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_BENE_CONTINGENT_INFO");
        annty_Actvty_Prap_Ap_Contingent_Bene_Info = annty_Actvty_Prap_Ap_Bene_Contingent_Info.newFieldInGroup("annty_Actvty_Prap_Ap_Contingent_Bene_Info", 
            "AP-CONTINGENT-BENE-INFO", FieldType.STRING, 72, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CONTINGENT_BENE_INFO", "ACIS_ANNTY_PRAP_AP_BENE_CONTINGENT_INFO");
        annty_Actvty_Prap_Ap_Bene_Estate = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Bene_Estate", "AP-BENE-ESTATE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_BENE_ESTATE");
        annty_Actvty_Prap_Ap_Bene_Trust = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Bene_Trust", "AP-BENE-TRUST", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_BENE_TRUST");
        annty_Actvty_Prap_Ap_Bene_Category = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Bene_Category", "AP-BENE-CATEGORY", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_CATEGORY");
        annty_Actvty_Prap_Ap_Mail_Instructions = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Mail_Instructions", "AP-MAIL-INSTRUCTIONS", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MAIL_INSTRUCTIONS");
        annty_Actvty_Prap_Ap_Eop_Addl_Cref_Request = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Eop_Addl_Cref_Request", "AP-EOP-ADDL-CREF-REQUEST", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_EOP_ADDL_CREF_REQUEST");
        annty_Actvty_Prap_Ap_Process_Status = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Process_Status", "AP-PROCESS-STATUS", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "AP_PROCESS_STATUS");
        annty_Actvty_Prap_Ap_Coll_St_Cd = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Coll_St_Cd", "AP-COLL-ST-CD", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "AP_COLL_ST_CD");
        annty_Actvty_Prap_Ap_Csm_Sec_Seg = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Csm_Sec_Seg", "AP-CSM-SEC-SEG", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "AP_CSM_SEC_SEG");
        annty_Actvty_Prap_Ap_Rlc_College = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Rlc_College", "AP-RLC-COLLEGE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "AP_RLC_COLLEGE");
        annty_Actvty_Prap_Ap_Cor_Prfx_Nme = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Cor_Prfx_Nme", "AP-COR-PRFX-NME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AP_COR_PRFX_NME");
        annty_Actvty_Prap_Ap_Cor_Last_Nme = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "AP_COR_LAST_NME");

        annty_Actvty_Prap__R_Field_2 = vw_annty_Actvty_Prap.getRecord().newGroupInGroup("annty_Actvty_Prap__R_Field_2", "REDEFINE", annty_Actvty_Prap_Ap_Cor_Last_Nme);
        annty_Actvty_Prap_Ap_Cor_Last_Nme_1_12 = annty_Actvty_Prap__R_Field_2.newFieldInGroup("annty_Actvty_Prap_Ap_Cor_Last_Nme_1_12", "AP-COR-LAST-NME-1-12", 
            FieldType.STRING, 12);
        annty_Actvty_Prap_Ap_Cor_First_Nme = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_FIRST_NME");

        annty_Actvty_Prap__R_Field_3 = vw_annty_Actvty_Prap.getRecord().newGroupInGroup("annty_Actvty_Prap__R_Field_3", "REDEFINE", annty_Actvty_Prap_Ap_Cor_First_Nme);
        annty_Actvty_Prap_Ap_Cor_First_Nme_1_5 = annty_Actvty_Prap__R_Field_3.newFieldInGroup("annty_Actvty_Prap_Ap_Cor_First_Nme_1_5", "AP-COR-FIRST-NME-1-5", 
            FieldType.STRING, 5);
        annty_Actvty_Prap_Ap_Cor_Mddle_Nme = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Cor_Mddle_Nme", "AP-COR-MDDLE-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_MDDLE_NME");
        annty_Actvty_Prap_Ap_Cor_Sffx_Nme = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Cor_Sffx_Nme", "AP-COR-SFFX-NME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AP_COR_SFFX_NME");
        annty_Actvty_Prap_Ap_Ph_Hist_Ind = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Ph_Hist_Ind", "AP-PH-HIST-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_PH_HIST_IND");
        annty_Actvty_Prap_Ap_Rcrd_Updt_Tm_Stamp = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Rcrd_Updt_Tm_Stamp", "AP-RCRD-UPDT-TM-STAMP", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_RCRD_UPDT_TM_STAMP");
        annty_Actvty_Prap_Ap_Contact_Mode = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Contact_Mode", "AP-CONTACT-MODE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_CONTACT_MODE");
        annty_Actvty_Prap_Ap_Racf_Id = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Racf_Id", "AP-RACF-ID", FieldType.STRING, 
            16, RepeatingFieldStrategy.None, "AP_RACF_ID");
        annty_Actvty_Prap_Ap_Pin_Nbr = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "AP_PIN_NBR");

        annty_Actvty_Prap_Ap_Rqst_Log_Dte_Tm = vw_annty_Actvty_Prap.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_Ap_Rqst_Log_Dte_Tm", "AP-RQST-LOG-DTE-TM", 
            new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        annty_Actvty_Prap_Ap_Rqst_Log_Dte_Time = annty_Actvty_Prap_Ap_Rqst_Log_Dte_Tm.newFieldInGroup("annty_Actvty_Prap_Ap_Rqst_Log_Dte_Time", "AP-RQST-LOG-DTE-TIME", 
            FieldType.STRING, 15, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_RQST_LOG_DTE_TIME", "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        annty_Actvty_Prap_Ap_Sync_Ind = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Sync_Ind", "AP-SYNC-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_SYNC_IND");
        annty_Actvty_Prap_Ap_Cor_Ind = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Cor_Ind", "AP-COR-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_COR_IND");
        annty_Actvty_Prap_Ap_Alloc_Ind = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Alloc_Ind", "AP-ALLOC-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_ALLOC_IND");
        annty_Actvty_Prap_Ap_Tiaa_Doi = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Tiaa_Doi", "AP-TIAA-DOI", FieldType.DATE, 
            RepeatingFieldStrategy.None, "AP_TIAA_DOI");
        annty_Actvty_Prap_Ap_Cref_Doi = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Cref_Doi", "AP-CREF-DOI", FieldType.DATE, 
            RepeatingFieldStrategy.None, "AP_CREF_DOI");

        annty_Actvty_Prap_Ap_Mit_Request = vw_annty_Actvty_Prap.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_Ap_Mit_Request", "AP-MIT-REQUEST", 
            new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        annty_Actvty_Prap_Ap_Mit_Unit = annty_Actvty_Prap_Ap_Mit_Request.newFieldInGroup("annty_Actvty_Prap_Ap_Mit_Unit", "AP-MIT-UNIT", FieldType.STRING, 
            8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MIT_UNIT", "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        annty_Actvty_Prap_Ap_Mit_Wpid = annty_Actvty_Prap_Ap_Mit_Request.newFieldInGroup("annty_Actvty_Prap_Ap_Mit_Wpid", "AP-MIT-WPID", FieldType.STRING, 
            6, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MIT_WPID", "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        annty_Actvty_Prap_Ap_Ira_Rollover_Type = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Ira_Rollover_Type", "AP-IRA-ROLLOVER-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_IRA_ROLLOVER_TYPE");
        annty_Actvty_Prap_Ap_Ira_Record_Type = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Ira_Record_Type", "AP-IRA-RECORD-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_IRA_RECORD_TYPE");
        annty_Actvty_Prap_Ap_Mult_App_Status = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Mult_App_Status", "AP-MULT-APP-STATUS", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MULT_APP_STATUS");
        annty_Actvty_Prap_Ap_Mult_App_Lob = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Mult_App_Lob", "AP-MULT-APP-LOB", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_MULT_APP_LOB");
        annty_Actvty_Prap_Ap_Mult_App_Lob_Type = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Mult_App_Lob_Type", "AP-MULT-APP-LOB-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MULT_APP_LOB_TYPE");
        annty_Actvty_Prap_Ap_Mult_App_Ppg = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Mult_App_Ppg", "AP-MULT-APP-PPG", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "AP_MULT_APP_PPG");
        annty_Actvty_Prap_Ap_Print_Date = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Print_Date", "AP-PRINT-DATE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "AP_PRINT_DATE");

        annty_Actvty_Prap_Ap_Cntrct_Info = vw_annty_Actvty_Prap.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_Ap_Cntrct_Info", "AP-CNTRCT-INFO", 
            new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");
        annty_Actvty_Prap_Ap_Cntrct_Type = annty_Actvty_Prap_Ap_Cntrct_Info.newFieldInGroup("annty_Actvty_Prap_Ap_Cntrct_Type", "AP-CNTRCT-TYPE", FieldType.STRING, 
            1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CNTRCT_TYPE", "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");
        annty_Actvty_Prap_Ap_Cntrct_Nbr = annty_Actvty_Prap_Ap_Cntrct_Info.newFieldInGroup("annty_Actvty_Prap_Ap_Cntrct_Nbr", "AP-CNTRCT-NBR", FieldType.STRING, 
            10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CNTRCT_NBR", "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");
        annty_Actvty_Prap_Ap_Cntrct_Proceeds_Amt = annty_Actvty_Prap_Ap_Cntrct_Info.newFieldInGroup("annty_Actvty_Prap_Ap_Cntrct_Proceeds_Amt", "AP-CNTRCT-PROCEEDS-AMT", 
            FieldType.NUMERIC, 10, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CNTRCT_PROCEEDS_AMT", "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");

        annty_Actvty_Prap_Ap_Addr_Info = vw_annty_Actvty_Prap.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_Ap_Addr_Info", "AP-ADDR-INFO", new DbsArrayController(1, 
            5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_ADDR_INFO");
        annty_Actvty_Prap_Ap_Address_Txt = annty_Actvty_Prap_Ap_Addr_Info.newFieldInGroup("annty_Actvty_Prap_Ap_Address_Txt", "AP-ADDRESS-TXT", FieldType.STRING, 
            35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ADDRESS_TXT", "ACIS_ANNTY_PRAP_AP_ADDR_INFO");
        annty_Actvty_Prap_Ap_Address_Dest_Name = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Address_Dest_Name", "AP-ADDRESS-DEST-NAME", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "AP_ADDRESS_DEST_NAME");
        annty_Actvty_Prap_Ap_Zip_Code = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Zip_Code", "AP-ZIP-CODE", FieldType.STRING, 
            9, RepeatingFieldStrategy.None, "AP_ZIP_CODE");
        annty_Actvty_Prap_Ap_Bank_Pymnt_Acct_Nmbr = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Bank_Pymnt_Acct_Nmbr", "AP-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 21, RepeatingFieldStrategy.None, "AP_BANK_PYMNT_ACCT_NMBR");
        annty_Actvty_Prap_Ap_Bank_Aba_Acct_Nmbr = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Bank_Aba_Acct_Nmbr", "AP-BANK-ABA-ACCT-NMBR", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "AP_BANK_ABA_ACCT_NMBR");
        annty_Actvty_Prap_Ap_Addr_Usage_Code = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Addr_Usage_Code", "AP-ADDR-USAGE-CODE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ADDR_USAGE_CODE");
        annty_Actvty_Prap_Ap_Init_Paymt_Amt = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Init_Paymt_Amt", "AP-INIT-PAYMT-AMT", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_INIT_PAYMT_AMT");
        annty_Actvty_Prap_Ap_Init_Paymt_Pct = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Init_Paymt_Pct", "AP-INIT-PAYMT-PCT", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_INIT_PAYMT_PCT");
        annty_Actvty_Prap_Ap_Financial_1 = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Financial_1", "AP-FINANCIAL-1", FieldType.NUMERIC, 
            10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_1");
        annty_Actvty_Prap_Ap_Financial_2 = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Financial_2", "AP-FINANCIAL-2", FieldType.NUMERIC, 
            10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_2");
        annty_Actvty_Prap_Ap_Financial_3 = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Financial_3", "AP-FINANCIAL-3", FieldType.NUMERIC, 
            10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_3");
        annty_Actvty_Prap_Ap_Financial_4 = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Financial_4", "AP-FINANCIAL-4", FieldType.NUMERIC, 
            10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_4");
        annty_Actvty_Prap_Ap_Financial_5 = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Financial_5", "AP-FINANCIAL-5", FieldType.NUMERIC, 
            10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_5");
        annty_Actvty_Prap_Ap_Tiaa_Cntrct = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "AP_TIAA_CNTRCT");
        annty_Actvty_Prap_Ap_Cref_Cert = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Cref_Cert", "AP-CREF-CERT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "AP_CREF_CERT");
        annty_Actvty_Prap_Ap_Rlc_Cref_Cert = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Rlc_Cref_Cert", "AP-RLC-CREF-CERT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_RLC_CREF_CERT");
        annty_Actvty_Prap_Ap_Allocation_Model_Type = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Allocation_Model_Type", "AP-ALLOCATION-MODEL-TYPE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_ALLOCATION_MODEL_TYPE");
        annty_Actvty_Prap_Ap_Divorce_Ind = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Divorce_Ind", "AP-DIVORCE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_DIVORCE_IND");
        annty_Actvty_Prap_Ap_Email_Address = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Email_Address", "AP-EMAIL-ADDRESS", 
            FieldType.STRING, 50, RepeatingFieldStrategy.None, "AP_EMAIL_ADDRESS");
        annty_Actvty_Prap_Ap_E_Signed_Appl_Ind = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_E_Signed_Appl_Ind", "AP-E-SIGNED-APPL-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_E_SIGNED_APPL_IND");
        annty_Actvty_Prap_Ap_Eft_Requested_Ind = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Eft_Requested_Ind", "AP-EFT-REQUESTED-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_EFT_REQUESTED_IND");
        annty_Actvty_Prap_Ap_Phone_No = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Phone_No", "AP-PHONE-NO", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "AP_PHONE_NO");

        annty_Actvty_Prap_Ap_Fund_Identifier = vw_annty_Actvty_Prap.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_Ap_Fund_Identifier", "AP-FUND-IDENTIFIER", 
            new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        annty_Actvty_Prap_Ap_Fund_Cde = annty_Actvty_Prap_Ap_Fund_Identifier.newFieldInGroup("annty_Actvty_Prap_Ap_Fund_Cde", "AP-FUND-CDE", FieldType.STRING, 
            10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_FUND_CDE", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        annty_Actvty_Prap_Ap_Allocation_Pct = annty_Actvty_Prap_Ap_Fund_Identifier.newFieldInGroup("annty_Actvty_Prap_Ap_Allocation_Pct", "AP-ALLOCATION-PCT", 
            FieldType.NUMERIC, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ALLOCATION_PCT", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        registerRecord(vw_annty_Actvty_Prap);

        pnd_Miscellanous_Variable = localVariables.newGroupInRecord("pnd_Miscellanous_Variable", "#MISCELLANOUS-VARIABLE");
        pnd_Miscellanous_Variable_Pnd_Date_Mmddyyyy_A = pnd_Miscellanous_Variable.newFieldInGroup("pnd_Miscellanous_Variable_Pnd_Date_Mmddyyyy_A", "#DATE-MMDDYYYY-A", 
            FieldType.STRING, 8);

        pnd_Miscellanous_Variable__R_Field_4 = pnd_Miscellanous_Variable.newGroupInGroup("pnd_Miscellanous_Variable__R_Field_4", "REDEFINE", pnd_Miscellanous_Variable_Pnd_Date_Mmddyyyy_A);
        pnd_Miscellanous_Variable_Pnd_Date_Mmddyyyy_N = pnd_Miscellanous_Variable__R_Field_4.newFieldInGroup("pnd_Miscellanous_Variable_Pnd_Date_Mmddyyyy_N", 
            "#DATE-MMDDYYYY-N", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_annty_Actvty_Prap.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Scin5200() throws Exception
    {
        super("Scin5200");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        PND_PND_L1620:                                                                                                                                                    //Natural: GET ANNTY-ACTVTY-PRAP #SELECTED-ISN
        vw_annty_Actvty_Prap.readByID(pnd_Selected_Isn.getLong(), "PND_PND_L1620");
        annty_Actvty_Prap_Ap_Status.setValue("C");                                                                                                                        //Natural: ASSIGN AP-STATUS = 'C'
        pnd_Miscellanous_Variable_Pnd_Date_Mmddyyyy_A.setValueEdited(Global.getDATX(),new ReportEditMask("MMDDYYYY"));                                                    //Natural: MOVE EDITED *DATX ( EM = MMDDYYYY ) TO #DATE-MMDDYYYY-A
        annty_Actvty_Prap_Ap_Dt_Matched.setValue(pnd_Miscellanous_Variable_Pnd_Date_Mmddyyyy_N);                                                                          //Natural: MOVE #DATE-MMDDYYYY-N TO AP-DT-MATCHED
        vw_annty_Actvty_Prap.updateDBRow("PND_PND_L1620");                                                                                                                //Natural: UPDATE ( ##L1620. )
        pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("MANUAL M & R APPLIED");                                                                                       //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'MANUAL M & R APPLIED'
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* *----------------------------------------------------------------------
    }

    //
}
