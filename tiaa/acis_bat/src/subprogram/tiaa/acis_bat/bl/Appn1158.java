/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:59:00 PM
**        * FROM NATURAL SUBPROGRAM : Appn1158
************************************************************
**        * FILE NAME            : Appn1158.java
**        * CLASS NAME           : Appn1158
**        * INSTANCE NAME        : Appn1158
************************************************************
***********************************************************************
* PROGRAM-ID:  APPN1158
*
* FUNCTION  :  THIS MODULE IS CALLED TO CHECK THE VALIDITY OF A
*              STATE CODE AGAINST THE VALUES INITIALIZED IN APPL170.
*              IT RETURNS A LOGICAL WITH A VALUE OF TRUE IF THE STATE
*              CODE EXISTS IN APPL170.
*
* AUTHOR    :  BRENT HOLLOWAY
*
* DATE CREATED: 3/16/2012
*
* CHANGE HISTORY
*
* IMPL
* DATE    CHANGED                                               CHANGE
* YY/MM     BY       DESCRIBE THE FUNCTIONAL CHANGE               TAG
**-----  ---------  ------------------------------------------- ------
* 15/12 ELLO        CHANGE LDA FROM APPL170 TO APPL1080 SO THAT  BIP2
*                   CANADA (STATE CODE 'CN') WILL ALSO BE IN THE
*                   TABLE.
***********************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appn1158 extends BLNatBase
{
    // Data Areas
    private LdaAppl1080 ldaAppl1080;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Check_State;
    private DbsField pnd_Invalid_State_Sw;
    private DbsField pnd_I;
    private DbsField pnd_Matched;
    private DbsField pnd_Ws_State_Zip;

    private DbsGroup pnd_Ws_State_Zip__R_Field_1;
    private DbsField pnd_Ws_State_Zip_Pnd_Ws_State_Abrv;
    private DbsField pnd_Ws_State_Zip_Pnd_Ws_State_Code;
    private DbsField pnd_Ws_State_Zip_Pnd_Ws_Zip_Code_Range_Start;
    private DbsField pnd_Ws_State_Zip_Pnd_Ws_Zip_Code_Range_End;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl1080 = new LdaAppl1080();
        registerRecord(ldaAppl1080);

        // parameters
        parameters = new DbsRecord();
        pnd_Check_State = parameters.newFieldInRecord("pnd_Check_State", "#CHECK-STATE", FieldType.STRING, 2);
        pnd_Check_State.setParameterOption(ParameterOption.ByReference);
        pnd_Invalid_State_Sw = parameters.newFieldInRecord("pnd_Invalid_State_Sw", "#INVALID-STATE-SW", FieldType.BOOLEAN, 1);
        pnd_Invalid_State_Sw.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Matched = localVariables.newFieldInRecord("pnd_Matched", "#MATCHED", FieldType.BOOLEAN, 1);
        pnd_Ws_State_Zip = localVariables.newFieldInRecord("pnd_Ws_State_Zip", "#WS-STATE-ZIP", FieldType.STRING, 10);

        pnd_Ws_State_Zip__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_State_Zip__R_Field_1", "REDEFINE", pnd_Ws_State_Zip);
        pnd_Ws_State_Zip_Pnd_Ws_State_Abrv = pnd_Ws_State_Zip__R_Field_1.newFieldInGroup("pnd_Ws_State_Zip_Pnd_Ws_State_Abrv", "#WS-STATE-ABRV", FieldType.STRING, 
            2);
        pnd_Ws_State_Zip_Pnd_Ws_State_Code = pnd_Ws_State_Zip__R_Field_1.newFieldInGroup("pnd_Ws_State_Zip_Pnd_Ws_State_Code", "#WS-STATE-CODE", FieldType.STRING, 
            2);
        pnd_Ws_State_Zip_Pnd_Ws_Zip_Code_Range_Start = pnd_Ws_State_Zip__R_Field_1.newFieldInGroup("pnd_Ws_State_Zip_Pnd_Ws_Zip_Code_Range_Start", "#WS-ZIP-CODE-RANGE-START", 
            FieldType.STRING, 3);
        pnd_Ws_State_Zip_Pnd_Ws_Zip_Code_Range_End = pnd_Ws_State_Zip__R_Field_1.newFieldInGroup("pnd_Ws_State_Zip_Pnd_Ws_Zip_Code_Range_End", "#WS-ZIP-CODE-RANGE-END", 
            FieldType.STRING, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl1080.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Appn1158() throws Exception
    {
        super("Appn1158");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Invalid_State_Sw.reset();                                                                                                                                     //Natural: RESET #INVALID-STATE-SW
        //*  BIP2 COMMENTED STARTS HERE...
        //*  DECIDE FOR FIRST CONDITION
        //*   WHEN #CHECK-STATE = '00' THRU '99'
        //*     EXAMINE FULL #STATE-CODE-N(*) FOR FULL #CHECK-STATE GIVING INDEX #I
        //*   WHEN NONE
        //*     EXAMINE #STATE-CODE-A(*) FOR #CHECK-STATE GIVING INDEX #I
        //*  END-DECIDE
        //*  IF #I = 0
        //*   #INVALID-STATE-SW := TRUE
        //*  END-IF
        //*  BIP2 COMMENTED ENDS HERE.
        //*  BIP2 NEW LOGIC STARTS HERE....
        pnd_Matched.setValue(false);                                                                                                                                      //Natural: ASSIGN #MATCHED := FALSE
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 55
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(55)); pnd_I.nadd(1))
        {
            pnd_Ws_State_Zip.setValue(ldaAppl1080.getPnd_State_Zipcode_Array_Pnd_State_Zip_Idx().getValue(pnd_I));                                                        //Natural: ASSIGN #WS-STATE-ZIP := #STATE-ZIP-IDX ( #I )
            //*  IF VERIFYING NUMERIC STATE CODE
            if (condition(pnd_Check_State.greaterOrEqual("00") && pnd_Check_State.lessOrEqual("99")))                                                                     //Natural: IF #CHECK-STATE = '00' THRU '99'
            {
                if (condition(pnd_Ws_State_Zip_Pnd_Ws_State_Code.equals(pnd_Check_State)))                                                                                //Natural: IF #WS-STATE-CODE = #CHECK-STATE
                {
                    pnd_Matched.setValue(true);                                                                                                                           //Natural: ASSIGN #MATCHED := TRUE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Ws_State_Zip_Pnd_Ws_State_Abrv.equals(pnd_Check_State)))                                                                                //Natural: IF #WS-STATE-ABRV = #CHECK-STATE
                {
                    pnd_Matched.setValue(true);                                                                                                                           //Natural: ASSIGN #MATCHED := TRUE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(! (pnd_Matched.getBoolean())))                                                                                                                      //Natural: IF NOT #MATCHED
        {
            pnd_Invalid_State_Sw.setValue(true);                                                                                                                          //Natural: ASSIGN #INVALID-STATE-SW := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        //*  BIP2 NEW LOGIC ENDS HERE.
    }

    //
}
