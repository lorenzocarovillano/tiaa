/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:59:17 PM
**        * FROM NATURAL SUBPROGRAM : Appp030
************************************************************
**        * FILE NAME            : Appp030.java
**        * CLASS NAME           : Appp030
**        * INSTANCE NAME        : Appp030
************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appp030 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAppa020 pdaAppa020;
    private LdaAppl020 ldaAppl020;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_End_Prod;
    private DbsField pnd_Start_Prod;
    private DbsField pnd_Ver_Val;
    private DbsField pnd_That_Tally;
    private DbsField pnd_Next_Position;
    private DbsField pnd_Acctall_Val;
    private DbsField pnd_Getbuck_Val;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl020 = new LdaAppl020();
        registerRecord(ldaAppl020);
        registerRecord(ldaAppl020.getVw_app_Ilog_B_Rcrd_View());
        registerRecord(ldaAppl020.getVw_app_Ilog_J_Rcrd_View());
        registerRecord(ldaAppl020.getVw_app_Ilog_A_Rcrd_View());

        // parameters
        parameters = new DbsRecord();
        pdaAppa020 = new PdaAppa020(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_End_Prod = localVariables.newFieldInRecord("pnd_End_Prod", "#END-PROD", FieldType.STRING, 4);
        pnd_Start_Prod = localVariables.newFieldInRecord("pnd_Start_Prod", "#START-PROD", FieldType.STRING, 4);
        pnd_Ver_Val = localVariables.newFieldInRecord("pnd_Ver_Val", "#VER-VAL", FieldType.INTEGER, 4);
        pnd_That_Tally = localVariables.newFieldInRecord("pnd_That_Tally", "#THAT-TALLY", FieldType.INTEGER, 4);
        pnd_Next_Position = localVariables.newFieldInRecord("pnd_Next_Position", "#NEXT-POSITION", FieldType.INTEGER, 4);
        pnd_Acctall_Val = localVariables.newFieldInRecord("pnd_Acctall_Val", "#ACCTALL-VAL", FieldType.INTEGER, 4);
        pnd_Getbuck_Val = localVariables.newFieldInRecord("pnd_Getbuck_Val", "#GETBUCK-VAL", FieldType.INTEGER, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl020.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Appp030() throws Exception
    {
        super("Appp030");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  --------------CHECK RANGE END -----------------------------------
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pdaAppa020.getPnd_Parmdata_Pnd_Other_Activity_Message().reset();                                                                                              //Natural: RESET #OTHER-ACTIVITY-MESSAGE #RESPONSE-MESSAGE #NEXT-POSITION #START-PROD #END-PROD #PREV-CONTRACT #LAST-CONTRACT
            pdaAppa020.getPnd_Parmdata_Pnd_Response_Message().reset();
            pnd_Next_Position.reset();
            pnd_Start_Prod.reset();
            pnd_End_Prod.reset();
            pdaAppa020.getPnd_Parmdata_Pnd_Prev_Contract().reset();
            pdaAppa020.getPnd_Parmdata_Pnd_Last_Contract().reset();
            //*  --------------
            if (condition(pdaAppa020.getPnd_Parmdata_Pnd_Get_Cntrct_Flag().equals("GET LAST RUN DT")))                                                                    //Natural: IF #GET-CNTRCT-FLAG = 'GET LAST RUN DT'
            {
                                                                                                                                                                          //Natural: PERFORM LASTRUNDATE
                sub_Lastrundate();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  --------------
            if (condition(pdaAppa020.getPnd_Parmdata_Pnd_End_Date().less(pdaAppa020.getPnd_Parmdata_Pnd_Start_Date())))                                                   //Natural: IF #END-DATE < #START-DATE
            {
                pdaAppa020.getPnd_Parmdata_Pnd_Response_Message().setValue("INVALID DATE RANGE ");                                                                        //Natural: MOVE 'INVALID DATE RANGE ' TO #RESPONSE-MESSAGE
                pdaAppa020.getPnd_Parmdata_Pnd_Other_Activity_Message().setValue("CRITICAL ERROR");                                                                       //Natural: MOVE 'CRITICAL ERROR' TO #OTHER-ACTIVITY-MESSAGE
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  --------------
            if (condition(pdaAppa020.getPnd_Parmdata_Pnd_Accum_Buckets_Flag().equals("GET ALL BUCKETS")))                                                                 //Natural: IF #ACCUM-BUCKETS-FLAG = 'GET ALL BUCKETS'
            {
                                                                                                                                                                          //Natural: PERFORM GETALL
                sub_Getall();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        REPEAT02:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pdaAppa020.getPnd_Parmdata_Pnd_Accum_Buckets_Flag().equals("GET ALL BUCKETS") || pdaAppa020.getPnd_Parmdata_Pnd_Accum_Buckets_Flag().equals("GET NAMED BUCKS"))) //Natural: IF #ACCUM-BUCKETS-FLAG = 'GET ALL BUCKETS' OR #ACCUM-BUCKETS-FLAG = 'GET NAMED BUCKS'
            {
                                                                                                                                                                          //Natural: PERFORM COUNTNAMED
                sub_Countnamed();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        REPEAT03:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pdaAppa020.getPnd_Parmdata_Pnd_Get_Cntrct_Flag().equals("GET LAST CNTRCT")))                                                                    //Natural: IF #GET-CNTRCT-FLAG = 'GET LAST CNTRCT'
            {
                                                                                                                                                                          //Natural: PERFORM LASTCONTRACT
                sub_Lastcontract();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  -----------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GETALL
        //*  -------FINDING THE FIRST IN RANGE OF ACCUM RECORDS   POINTS TO THE
        //*  -------FIRST IN RANGE OF BUCKET RECORDS---------------------------
        //*  -------CYCLE THRU ALL RELEVENT BUCKET RECORDS, GET ALL BUCKET NAMES
        //*  -------CYCLE THRU ALL OCCURS OF BUCKET NAMES, LOAD A HOLD TABLE
        //*  -----------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COUNTNAMED
        //*  -------CYCLE THRU THE RANGE OF ACCUM RECORDS TO GET BUCKET COUNTS--
        //*  -------FIND THE  RELEVENT BUCKET RECORDS, GET COUNTS BY  NAMES---
        //*  -------CYCLE THRU ALL OCCURS OF BUCKET NAMES REQUESTED FOR ACCUMS-
        //*  -------CYCLE THRU ALL OCCURS OF INSTRUCTIONS AS TO WHAT POSITION TO
        //*  -------DROP THE TOKENS FROM THE BUCKETS INTO THE FINAL ACCUMULATORS
        //*  -----------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LASTCONTRACT
        //*  -----------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LASTRUNDATE
        //*  -----------------------------------------------------------
    }
    private void sub_Getall() throws Exception                                                                                                                            //Natural: GETALL
    {
        if (BLNatReinput.isReinput()) return;

        REPEAT04:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pdaAppa020.getPnd_Parmdata_Pnd_Each_Bucket_Name().getValue("*").reset();                                                                                      //Natural: RESET #EACH-BUCKET-NAME ( * )
            ldaAppl020.getVw_app_Ilog_A_Rcrd_View().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) APP-ILOG-A-RCRD-VIEW WITH START-DTE NOT < #START-DATE AND START-DTE NOT > #END-DATE
            (
            "AC1",
            new Wc[] { new Wc("START_DTE", ">=", pdaAppa020.getPnd_Parmdata_Pnd_Start_Date(), "And", WcType.WITH) ,
            new Wc("START_DTE", "<=", pdaAppa020.getPnd_Parmdata_Pnd_End_Date(), WcType.WITH) },
            1
            );
            AC1:
            while (condition(ldaAppl020.getVw_app_Ilog_A_Rcrd_View().readNextRow("AC1", true)))
            {
                ldaAppl020.getVw_app_Ilog_A_Rcrd_View().setIfNotFoundControlFlag(false);
                //*  -------IF NO FIRST IN ACCUM  RANGE ---CRITICAL APPLICATION ERROR
                if (condition(ldaAppl020.getVw_app_Ilog_A_Rcrd_View().getAstCOUNTER().equals(0)))                                                                         //Natural: IF NO RECORDS FOUND
                {
                    pdaAppa020.getPnd_Parmdata_Pnd_Response_Message().setValue("1* AC REC NOT FOUND");                                                                    //Natural: MOVE '1* AC REC NOT FOUND' TO #RESPONSE-MESSAGE
                    pdaAppa020.getPnd_Parmdata_Pnd_Other_Activity_Message().setValue("CRITICAL ERROR");                                                                   //Natural: MOVE 'CRITICAL ERROR' TO #OTHER-ACTIVITY-MESSAGE
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-NOREC
                //*  -------FORMAT KEY FOR RELEVENT BUCKET RECORDS----------------------
                ldaAppl020.getBpnd_Rcrd_Key_Bpnd_Start_Dte().setValue(ldaAppl020.getApp_Ilog_A_Rcrd_View_Rcrd_Revision_Nbr());                                            //Natural: MOVE APP-ILOG-A-RCRD-VIEW.RCRD-REVISION-NBR TO B#START-DTE
                ldaAppl020.getBpnd_Rcrd_Key_Bpnd_Rcrd_Cde().setValue("BU");                                                                                               //Natural: MOVE 'BU' TO B#RCRD-CDE
                ldaAppl020.getBpnd_Rcrd_Key_Bpnd_Rcrd_Nme().setValue(" ");                                                                                                //Natural: MOVE ' ' TO B#RCRD-NME
                ldaAppl020.getBpnd_Rcrd_Key_Bpnd_Prdct_Cde().setValue(" ");                                                                                               //Natural: MOVE ' ' TO B#PRDCT-CDE
                ldaAppl020.getBbpnd_Rcrd_Key().setValue(ldaAppl020.getBpnd_Rcrd_Key());                                                                                   //Natural: MOVE B#RCRD-KEY TO BB#RCRD-KEY
                ldaAppl020.getBbpnd_Rcrd_Key_Bbpnd_Start_Dte().setValue(pdaAppa020.getPnd_Parmdata_Pnd_End_Date());                                                       //Natural: MOVE #END-DATE TO BB#START-DTE
                ldaAppl020.getVw_app_Ilog_B_Rcrd_View().startDatabaseFind                                                                                                 //Natural: FIND APP-ILOG-B-RCRD-VIEW WITH RCRD-KEY NOT < B#RCRD-KEY AND RCRD-KEY NOT > BB#RCRD-KEY
                (
                "BU1",
                new Wc[] { new Wc("RCRD_KEY", ">=", ldaAppl020.getBpnd_Rcrd_Key(), "And", WcType.WITH) ,
                new Wc("RCRD_KEY", "<=", ldaAppl020.getBbpnd_Rcrd_Key(), WcType.WITH) }
                );
                BU1:
                while (condition(ldaAppl020.getVw_app_Ilog_B_Rcrd_View().readNextRow("BU1", true)))
                {
                    ldaAppl020.getVw_app_Ilog_B_Rcrd_View().setIfNotFoundControlFlag(false);
                    //*  -------IF NO BUCKET RECS IN  RANGE ---CRITICAL APPLICATION ERROR
                    if (condition(ldaAppl020.getVw_app_Ilog_B_Rcrd_View().getAstCOUNTER().equals(0)))                                                                     //Natural: IF NO RECORDS FOUND
                    {
                        pdaAppa020.getPnd_Parmdata_Pnd_Response_Message().setValue("1* BU REC NOT FOUND");                                                                //Natural: MOVE '1* BU REC NOT FOUND' TO #RESPONSE-MESSAGE
                        pdaAppa020.getPnd_Parmdata_Pnd_Other_Activity_Message().setValue("CRITICAL ERROR");                                                               //Natural: MOVE 'CRITICAL ERROR' TO #OTHER-ACTIVITY-MESSAGE
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-NOREC
                    FOR01:                                                                                                                                                //Natural: FOR #BUCK-VAL 1 APP-ILOG-B-RCRD-VIEW.C*BUCKETS-GRP
                    for (ldaAppl020.getPnd_Buck_Val().setValue(1); condition(ldaAppl020.getPnd_Buck_Val().lessOrEqual(ldaAppl020.getApp_Ilog_B_Rcrd_View_Count_Castbuckets_Grp())); 
                        ldaAppl020.getPnd_Buck_Val().nadd(1))
                    {
                        if (condition(ldaAppl020.getApp_Ilog_B_Rcrd_View_Buckets_Nme().getValue(ldaAppl020.getPnd_Buck_Val()).notEquals(" ")))                            //Natural: IF BUCKETS-NME ( #BUCK-VAL ) NOT = ' '
                        {
                            DbsUtil.examine(new ExamineSource(pdaAppa020.getPnd_Parmdata_Pnd_Each_Bucket_Name().getValue("*")), new ExamineSearch(ldaAppl020.getApp_Ilog_B_Rcrd_View_Buckets_Nme().getValue(ldaAppl020.getPnd_Buck_Val())),  //Natural: EXAMINE #EACH-BUCKET-NAME ( * ) FOR BUCKETS-NME ( #BUCK-VAL ) GIVING INDEX #VER-VAL
                                new ExamineGivingIndex(pnd_Ver_Val));
                            if (condition(pnd_Ver_Val.equals(getZero())))                                                                                                 //Natural: IF #VER-VAL = 0
                            {
                                pnd_Next_Position.nadd(1);                                                                                                                //Natural: ADD 1 TO #NEXT-POSITION
                                //*  -------THE TABLE IS OVERFULL----------WARNING  APPLICATION ERROR
                                if (condition(pnd_Next_Position.equals(ldaAppl020.getApp_Ilog_B_Rcrd_View_Count_Castbuckets_Grp())))                                      //Natural: IF #NEXT-POSITION = APP-ILOG-B-RCRD-VIEW.C*BUCKETS-GRP
                                {
                                    pdaAppa020.getPnd_Parmdata_Pnd_Response_Message().setValue("TOO MANY TO GET");                                                        //Natural: MOVE 'TOO MANY TO GET' TO #RESPONSE-MESSAGE
                                    pdaAppa020.getPnd_Parmdata_Pnd_Other_Activity_Message().setValue("WARNING ERROR");                                                    //Natural: MOVE 'WARNING ERROR' TO #OTHER-ACTIVITY-MESSAGE
                                    if (true) break BU1;                                                                                                                  //Natural: ESCAPE BOTTOM ( BU1. )
                                }                                                                                                                                         //Natural: END-IF
                                pdaAppa020.getPnd_Parmdata_Pnd_Each_Bucket_Name().getValue(pnd_Next_Position).setValue(ldaAppl020.getApp_Ilog_B_Rcrd_View_Buckets_Nme().getValue(ldaAppl020.getPnd_Buck_Val())); //Natural: MOVE BUCKETS-NME ( #BUCK-VAL ) TO #EACH-BUCKET-NAME ( #NEXT-POSITION )
                                //*  -------THE DEFAULT FINAL ACCUMULATOR POSITION IS PUT IN TABLE----
                                pdaAppa020.getPnd_Parmdata_Pnd_Where_Tally().getValue(pnd_Next_Position,1).setValue(pnd_Next_Position);                                   //Natural: MOVE #NEXT-POSITION TO #WHERE-TALLY ( #NEXT-POSITION, 1 )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("BU1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("BU1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("AC1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("AC1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaAppa020.getPnd_Parmdata_Pnd_Response_Message().setValue("GOT BUCKET NAMES/");                                                                              //Natural: MOVE 'GOT BUCKET NAMES/' TO #RESPONSE-MESSAGE
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                                  //Natural: ESCAPE BOTTOM
        if (true) return;
    }
    private void sub_Countnamed() throws Exception                                                                                                                        //Natural: COUNTNAMED
    {
        if (BLNatReinput.isReinput()) return;

        REPEAT05:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            ldaAppl020.getBpnd_Rcrd_Key_Bpnd_Start_Dte().setValue(pdaAppa020.getPnd_Parmdata_Pnd_Start_Date());                                                           //Natural: MOVE #START-DATE TO B#START-DTE
            if (condition(pdaAppa020.getPnd_Parmdata_Pnd_Use_Product_Code_Flag().equals("GET NAMED PROD")))                                                               //Natural: IF #USE-PRODUCT-CODE-FLAG = 'GET NAMED PROD'
            {
                if (condition(pdaAppa020.getPnd_Parmdata_Pnd_Product_Code().equals(" ")))                                                                                 //Natural: IF #PRODUCT-CODE = ' '
                {
                    pdaAppa020.getPnd_Parmdata_Pnd_Response_Message().setValue("NO PROD NAMED  ");                                                                        //Natural: MOVE 'NO PROD NAMED  ' TO #RESPONSE-MESSAGE
                    pdaAppa020.getPnd_Parmdata_Pnd_Other_Activity_Message().setValue("DUMM    ERROR");                                                                    //Natural: MOVE 'DUMM    ERROR' TO #OTHER-ACTIVITY-MESSAGE
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Start_Prod.setValue(pdaAppa020.getPnd_Parmdata_Pnd_Product_Code());                                                                               //Natural: MOVE #PRODUCT-CODE TO #START-PROD #END-PROD
                    pnd_End_Prod.setValue(pdaAppa020.getPnd_Parmdata_Pnd_Product_Code());
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Start_Prod.setValue("    ");                                                                                                                          //Natural: MOVE '    ' TO #START-PROD
                pnd_End_Prod.setValue("ZZZZ");                                                                                                                            //Natural: MOVE 'ZZZZ' TO #END-PROD
            }                                                                                                                                                             //Natural: END-IF
            ldaAppl020.getVw_app_Ilog_A_Rcrd_View().startDatabaseFind                                                                                                     //Natural: FIND APP-ILOG-A-RCRD-VIEW WITH START-DTE NOT < #START-DATE AND START-DTE NOT > #END-DATE AND PRDCT-CDE GE #START-PROD AND PRDCT-CDE LE #END-PROD
            (
            "AC2",
            new Wc[] { new Wc("START_DTE", ">=", pdaAppa020.getPnd_Parmdata_Pnd_Start_Date(), "And", WcType.WITH) ,
            new Wc("START_DTE", "<=", pdaAppa020.getPnd_Parmdata_Pnd_End_Date(), "And", WcType.WITH) ,
            new Wc("PRDCT_CDE", ">=", pnd_Start_Prod, "And", WcType.WITH) ,
            new Wc("PRDCT_CDE", "<=", pnd_End_Prod, WcType.WITH) }
            );
            AC2:
            while (condition(ldaAppl020.getVw_app_Ilog_A_Rcrd_View().readNextRow("AC2")))
            {
                ldaAppl020.getVw_app_Ilog_A_Rcrd_View().setIfNotFoundControlFlag(false);
                //*  -------IF NO FIRST IN ACCUM  RANGE ---CRITICAL APPLICATION ERROR
                //*    IF NO RECORDS FOUND
                //*  WRITE 'THIS FAR 1'
                //*  STOP
                //*      MOVE '2* AC REC NOT FOUND' TO #RESPONSE-MESSAGE
                //*      MOVE 'CRITICAL ERROR'       TO #OTHER-ACTIVITY-MESSAGE
                //*      ESCAPE ROUTINE
                //*    END-NOREC
                if (condition(pdaAppa020.getPnd_Parmdata_Pnd_Prev_Contract().equals(" ")))                                                                                //Natural: IF #PREV-CONTRACT = ' '
                {
                    pdaAppa020.getPnd_Parmdata_Pnd_Prev_Contract().setValue(ldaAppl020.getApp_Ilog_A_Rcrd_View_Cntrct_Previous_Rcrd());                                   //Natural: MOVE APP-ILOG-A-RCRD-VIEW.CNTRCT-PREVIOUS-RCRD TO #PREV-CONTRACT
                }                                                                                                                                                         //Natural: END-IF
                pdaAppa020.getPnd_Parmdata_Pnd_Last_Contract().setValue(ldaAppl020.getApp_Ilog_A_Rcrd_View_Cntrct_Last_Used());                                           //Natural: MOVE APP-ILOG-A-RCRD-VIEW.CNTRCT-LAST-USED TO #LAST-CONTRACT
                ldaAppl020.getVw_app_Ilog_B_Rcrd_View().startDatabaseFind                                                                                                 //Natural: FIND APP-ILOG-B-RCRD-VIEW WITH START-DTE = APP-ILOG-A-RCRD-VIEW.RCRD-REVISION-NBR AND RCRD-CDE = 'BU'
                (
                "BU2",
                new Wc[] { new Wc("START_DTE", "=", ldaAppl020.getApp_Ilog_A_Rcrd_View_Rcrd_Revision_Nbr(), "And", WcType.WITH) ,
                new Wc("RCRD_CDE", "=", "BU", WcType.WITH) }
                );
                BU2:
                while (condition(ldaAppl020.getVw_app_Ilog_B_Rcrd_View().readNextRow("BU2", true)))
                {
                    ldaAppl020.getVw_app_Ilog_B_Rcrd_View().setIfNotFoundControlFlag(false);
                    //*  -------IF NO BUCKET RECS FOUND ---CRITICAL APPLICATION ERROR
                    if (condition(ldaAppl020.getVw_app_Ilog_B_Rcrd_View().getAstCOUNTER().equals(0)))                                                                     //Natural: IF NO RECORDS FOUND
                    {
                        pdaAppa020.getPnd_Parmdata_Pnd_Response_Message().setValue("2* BU REC NOT FOUND");                                                                //Natural: MOVE '2* BU REC NOT FOUND' TO #RESPONSE-MESSAGE
                        pdaAppa020.getPnd_Parmdata_Pnd_Other_Activity_Message().setValue("CRITICAL ERROR");                                                               //Natural: MOVE 'CRITICAL ERROR' TO #OTHER-ACTIVITY-MESSAGE
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-NOREC
                    FOR02:                                                                                                                                                //Natural: FOR #BUCK-VAL 1 APP-ILOG-B-RCRD-VIEW.C*BUCKETS-GRP
                    for (ldaAppl020.getPnd_Buck_Val().setValue(1); condition(ldaAppl020.getPnd_Buck_Val().lessOrEqual(ldaAppl020.getApp_Ilog_B_Rcrd_View_Count_Castbuckets_Grp())); 
                        ldaAppl020.getPnd_Buck_Val().nadd(1))
                    {
                        FOR03:                                                                                                                                            //Natural: FOR #ACCTALL-VAL 1 5
                        for (pnd_Acctall_Val.setValue(1); condition(pnd_Acctall_Val.lessOrEqual(5)); pnd_Acctall_Val.nadd(1))
                        {
                            if (condition(pdaAppa020.getPnd_Parmdata_Pnd_Each_Bucket_Name().getValue(ldaAppl020.getPnd_Buck_Val()).notEquals("  ")))                      //Natural: IF #EACH-BUCKET-NAME ( #BUCK-VAL ) NOT = '  '
                            {
                                if (condition(pdaAppa020.getPnd_Parmdata_Pnd_Where_Tally().getValue(ldaAppl020.getPnd_Buck_Val(),pnd_Acctall_Val).greater(getZero())))    //Natural: IF #WHERE-TALLY ( #BUCK-VAL,#ACCTALL-VAL ) > 0
                                {
                                    pnd_That_Tally.setValue(pdaAppa020.getPnd_Parmdata_Pnd_Where_Tally().getValue(ldaAppl020.getPnd_Buck_Val(),pnd_Acctall_Val));         //Natural: MOVE #WHERE-TALLY ( #BUCK-VAL,#ACCTALL-VAL ) TO #THAT-TALLY
                                    DbsUtil.examine(new ExamineSource(ldaAppl020.getApp_Ilog_B_Rcrd_View_Buckets_Nme().getValue("*")), new ExamineSearch(pdaAppa020.getPnd_Parmdata_Pnd_Each_Bucket_Name().getValue(ldaAppl020.getPnd_Buck_Val())),  //Natural: EXAMINE BUCKETS-NME ( * ) #EACH-BUCKET-NAME ( #BUCK-VAL ) GIVING INDEX #VER-VAL
                                        new ExamineGivingIndex(pnd_Ver_Val));
                                    if (condition(pnd_Ver_Val.notEquals(getZero())))                                                                                      //Natural: IF #VER-VAL NOT = 0
                                    {
                                        pdaAppa020.getPnd_Parmdata_Pnd_Accum_Tally().getValue(pnd_That_Tally).nadd(ldaAppl020.getApp_Ilog_A_Rcrd_View_Buckets().getValue(pnd_Ver_Val)); //Natural: ADD APP-ILOG-A-RCRD-VIEW.BUCKETS ( #VER-VAL ) TO #ACCUM-TALLY ( #THAT-TALLY )
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("BU2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("BU2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("AC2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("AC2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaAppa020.getPnd_Parmdata_Pnd_Response_Message().setValue(DbsUtil.compress(pdaAppa020.getPnd_Parmdata_Pnd_Response_Message(), "GOT COUNTS/"));               //Natural: COMPRESS #RESPONSE-MESSAGE 'GOT COUNTS/' INTO #RESPONSE-MESSAGE
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                                  //Natural: ESCAPE BOTTOM
        if (true) return;
    }
    private void sub_Lastcontract() throws Exception                                                                                                                      //Natural: LASTCONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        REPEAT06:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            ldaAppl020.getVw_app_Ilog_J_Rcrd_View().startDatabaseFind                                                                                                     //Natural: FIND APP-ILOG-J-RCRD-VIEW WITH RCRD-NME = 'CURRENT' AND RCRD-CDE = 'JU' AND PRDCT-CDE = #PRODUCT-CODE
            (
            "JU1",
            new Wc[] { new Wc("RCRD_NME", "=", "CURRENT", "And", WcType.WITH) ,
            new Wc("RCRD_CDE", "=", "JU", "And", WcType.WITH) ,
            new Wc("PRDCT_CDE", "=", pdaAppa020.getPnd_Parmdata_Pnd_Product_Code(), WcType.WITH) }
            );
            JU1:
            while (condition(ldaAppl020.getVw_app_Ilog_J_Rcrd_View().readNextRow("JU1", true)))
            {
                ldaAppl020.getVw_app_Ilog_J_Rcrd_View().setIfNotFoundControlFlag(false);
                //*  -------IF NO CURRENT JUMPER RECORD---CRITICAL APPLICATION ERROR
                if (condition(ldaAppl020.getVw_app_Ilog_J_Rcrd_View().getAstCOUNTER().equals(0)))                                                                         //Natural: IF NO RECORDS FOUND
                {
                    pdaAppa020.getPnd_Parmdata_Pnd_Response_Message().setValue("APPLICATION ILOG600");                                                                    //Natural: MOVE 'APPLICATION ILOG600' TO #RESPONSE-MESSAGE
                    pdaAppa020.getPnd_Parmdata_Pnd_Other_Activity_Message().setValue(DbsUtil.compress(pdaAppa020.getPnd_Parmdata_Pnd_Other_Activity_Message(),            //Natural: COMPRESS #OTHER-ACTIVITY-MESSAGE 'CRITICAL ERROR/' INTO #OTHER-ACTIVITY-MESSAGE
                        "CRITICAL ERROR/"));
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-NOREC
                //*     IF #START-DATE = CURRENT-RCRD-DTE
                pdaAppa020.getPnd_Parmdata_Pnd_Last_Contract().setValue(ldaAppl020.getApp_Ilog_J_Rcrd_View_Cntrct_Last_Used());                                           //Natural: MOVE CNTRCT-LAST-USED TO #LAST-CONTRACT
                pdaAppa020.getPnd_Parmdata_Pnd_Response_Message().setValue(DbsUtil.compress(pdaAppa020.getPnd_Parmdata_Pnd_Response_Message(), "GOT LAST CNTRCT/"));      //Natural: COMPRESS #RESPONSE-MESSAGE 'GOT LAST CNTRCT/' INTO #RESPONSE-MESSAGE
                if (true) break JU1;                                                                                                                                      //Natural: ESCAPE BOTTOM ( JU1. )
                //*     ELSE
                //*  -------MUST BE CURRENT DAT---------------------
                //*       MOVE 'PARM INVALID DATE'   TO #RESPONSE-MESSAGE
                //*       COMPRESS #OTHER-ACTIVITY-MESSAGE
                //*         'CRITICAL ERROR/' INTO    #OTHER-ACTIVITY-MESSAGE
                //*       ESCAPE ROUTINE
                //*     END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                                  //Natural: ESCAPE BOTTOM
        if (true) return;
    }
    private void sub_Lastrundate() throws Exception                                                                                                                       //Natural: LASTRUNDATE
    {
        if (BLNatReinput.isReinput()) return;

        REPEAT07:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            ldaAppl020.getVw_app_Ilog_J_Rcrd_View().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) APP-ILOG-J-RCRD-VIEW WITH RCRD-NME = #REQUESTOR AND RCRD-CDE = 'CP'
            (
            "CP1",
            new Wc[] { new Wc("RCRD_NME", "=", pdaAppa020.getPnd_Parmdata_Pnd_Requestor(), "And", WcType.WITH) ,
            new Wc("RCRD_CDE", "=", "CP", WcType.WITH) },
            1
            );
            CP1:
            while (condition(ldaAppl020.getVw_app_Ilog_J_Rcrd_View().readNextRow("CP1", true)))
            {
                ldaAppl020.getVw_app_Ilog_J_Rcrd_View().setIfNotFoundControlFlag(false);
                //*  -------IF NO CURRENT JUMPER RECORD---CRITICAL APPLICATION ERROR
                if (condition(ldaAppl020.getVw_app_Ilog_J_Rcrd_View().getAstCOUNTER().equals(0)))                                                                         //Natural: IF NO RECORDS FOUND
                {
                    pdaAppa020.getPnd_Parmdata_Pnd_Response_Message().setValue("NO CP RECORD FOUND ");                                                                    //Natural: MOVE 'NO CP RECORD FOUND ' TO #RESPONSE-MESSAGE
                    pdaAppa020.getPnd_Parmdata_Pnd_Other_Activity_Message().setValue(DbsUtil.compress(pdaAppa020.getPnd_Parmdata_Pnd_Other_Activity_Message(),            //Natural: COMPRESS #OTHER-ACTIVITY-MESSAGE 'CRITICAL ERROR' INTO #OTHER-ACTIVITY-MESSAGE
                        "CRITICAL ERROR"));
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-NOREC
                pdaAppa020.getPnd_Parmdata_Pnd_Start_Date().setValue(ldaAppl020.getApp_Ilog_J_Rcrd_View_Current_Rcrd_Dte());                                              //Natural: MOVE CURRENT-RCRD-DTE TO #START-DATE
                pdaAppa020.getPnd_Parmdata_Pnd_Response_Message().setValue(DbsUtil.compress(pdaAppa020.getPnd_Parmdata_Pnd_Response_Message(), "GOT LAST RUN DT"));       //Natural: COMPRESS #RESPONSE-MESSAGE 'GOT LAST RUN DT' INTO #RESPONSE-MESSAGE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }

    //
}
