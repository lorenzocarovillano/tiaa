/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:10:08 AM
**        * FROM NATURAL SUBPROGRAM : Scin9000
************************************************************
**        * FILE NAME            : Scin9000.java
**        * CLASS NAME           : Scin9000
**        * INSTANCE NAME        : Scin9000
************************************************************
************************************************************************
* PROGRAM : SCIN9000
* SYSTEM  : ENROLLMENT DRIVER PROGRAM - SUNGARD
* AUTHOR  : BUDDY NEWSOM
* DATE    : DEC. 2016
* PURPOSE :
* THIS SUBPROGRAM WAS DESIGNED TO CONSOLIDATE THE LOGIC FOR STORING
* CONTRACT DATA IN ACIS.  IT WAS DEVELOPED FOR THE ONE IRA PROJECT TO
* BYPASS THE CONTRACT NO. ISSUANCE SINCE MDM IS ALREADY ISSUING THE
* CONTRACT NO. AND ALSO BYPASSING SOME VALIDATIONS RELATED TO ISSUANCE.
* ITS USE CAN BE EXPANDED WHEN MDM STARTS ISSUING THE CONTRACT NOS. FOR
* OTHER PAY-IN PRODUCTS.
*
* HISTORY
* 05/23/17 B.ELLO   PIN EXPANSION CHANGES
*                   RESTOWED FOR UPDATED PDAS AND LDAS
*                   CHANGED CALL FROM 'MDMN100'  TO 'MDMN101' (PINE)
* 11/16/19 L.SHU    IISG CHANGES
* 01/29/20 B.NEWSOM PRB94362 - CHANGES FOR IISG IN THIS PROGRAM
*                              INCORRECTLY MOVES AAT-FUND-CDE RATHER
*                              THAN AAT-FUND-CDE-2. THE CONTRACT IN
*                              QUESTION DIDN'T HAVE SECOND ALLOCATIONS.
*                              HENCE THE FIRST ALLOCATION ARE BEING
*                              OVERRIDDEN WITH SPACES. THE CHANGE WILL
*                              BE PLACED IN LINE 4150
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Scin9000 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaScia9000 pdaScia9000;
    private PdaScia1200 pdaScia1200;
    private PdaAcipda_D pdaAcipda_D;
    private PdaAcipda_M pdaAcipda_M;
    private PdaCdaobj pdaCdaobj;
    private PdaCdmntdfa pdaCdmntdfa;
    private PdaScia1100 pdaScia1100;
    private PdaScia1102 pdaScia1102;
    private PdaScia1105 pdaScia1105;
    private PdaScia1203 pdaScia1203;
    private LdaScil1080 ldaScil1080;
    private PdaScia2100 pdaScia2100;
    private PdaScia3000 pdaScia3000;
    private LdaScil3010 ldaScil3010;
    private PdaScia301r pdaScia301r;
    private PdaScia3012 pdaScia3012;
    private PdaScia8510 pdaScia8510;
    private PdaScia3530 pdaScia3530;
    private PdaScia3531 pdaScia3531;
    private PdaScia3535 pdaScia3535;
    private PdaScia3545 pdaScia3545;
    private PdaScia3540 pdaScia3540;
    private PdaScia3560 pdaScia3560;
    private PdaScia3570 pdaScia3570;
    private PdaScia3500 pdaScia3500;
    private PdaScia3600 pdaScia3600;
    private PdaScia3590 pdaScia3590;
    private PdaScia1281 pdaScia1281;
    private PdaScia9004 pdaScia9004;
    private PdaScia904r pdaScia904r;
    private PdaMdma101 pdaMdma101;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup scin9000;
    private DbsField scin9000_Pnd_Field1;

    private DbsGroup scin9000__R_Field_1;
    private DbsField scin9000_Pnd_Acibatbu_9000_Region_Code;
    private DbsField scin9000_Pnd_Acibatbu_9000_Mics;
    private DbsField scin9000_Pnd_Field2;
    private DbsField scin9000_Pnd_Field3;
    private DbsField pnd_A100;

    private DbsGroup pnd_A100__R_Field_2;
    private DbsField pnd_A100_Pnd_N1;

    private DbsGroup pnd_A100__R_Field_3;
    private DbsField pnd_A100_Pnd_A1;

    private DbsGroup pnd_A100__R_Field_4;
    private DbsField pnd_A100_Pnd_N3;

    private DbsGroup pnd_A100__R_Field_5;
    private DbsField pnd_A100_Pnd_A3;

    private DbsGroup pnd_A100__R_Field_6;
    private DbsField pnd_A100_Pnd_N4;

    private DbsGroup pnd_A100__R_Field_7;
    private DbsField pnd_A100_Pnd_A4;

    private DbsGroup pnd_A100__R_Field_8;
    private DbsField pnd_A100_Pnd_N6;

    private DbsGroup pnd_A100__R_Field_9;
    private DbsField pnd_A100_Pnd_A6;

    private DbsGroup pnd_A100__R_Field_10;
    private DbsField pnd_A100_Pnd_N7;

    private DbsGroup pnd_A100__R_Field_11;
    private DbsField pnd_A100_Pnd_A7;

    private DbsGroup pnd_A100__R_Field_12;
    private DbsField pnd_A100_Pnd_N8;

    private DbsGroup pnd_A100__R_Field_13;
    private DbsField pnd_A100_Pnd_A8;

    private DbsGroup pnd_A100__R_Field_14;
    private DbsField pnd_A100_Pnd_N9;

    private DbsGroup pnd_A100__R_Field_15;
    private DbsField pnd_A100_Pnd_A9;
    private DbsField pnd_Temp_Ssn_N;

    private DbsGroup pnd_Temp_Ssn_N__R_Field_16;
    private DbsField pnd_Temp_Ssn_N_Pnd_Temp_Ssn_A;
    private DbsField pnd_Cni_Data;
    private DbsField pnd_D;
    private DbsField pnd_Region_Code;
    private DbsField pnd_New_Alloc_Format;
    private DbsField pnd_From_Online_Enrollment;
    private DbsField pnd_In_Here;
    private DbsField pnd_Default_Enrollment;
    private DbsField pnd_Display_Flag;
    private DbsField pnd_Error;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Pf_Key_A8;
    private DbsField pnd_Vesting_Desc;
    private DbsField pnd_Debug;
    private DbsField pnd_Number;
    private DbsField pnd_Found;
    private DbsField pnd_Temp_Date;

    private DbsGroup pnd_Temp_Date__R_Field_17;
    private DbsField pnd_Temp_Date_Pnd_Temp_Date_A;

    private DbsGroup pnd_Cor_Contract_Data;
    private DbsField pnd_Cor_Contract_Data_Pnd_Cor_Cntrct_Cref_Nbr;
    private DbsField pnd_Cor_Contract_Data_Pnd_Cor_Cor_Cntrct_Add_Dte;
    private DbsField pnd_Mit_Process_Ind;
    private DbsField pnd_Non_T_C_Administered_Plan;
    private DbsField pnd_Cip;
    private DbsField pnd_Prdct_Cde;

    private DbsGroup pnd_Prdct_Cde__R_Field_18;
    private DbsField pnd_Prdct_Cde_Pnd_Prdct_1;
    private DbsField pnd_Prdct_Cde_Pnd_Prdct_2;
    private DbsField pnd_Prdct_Cde_Pnd_Prdct_3;
    private DbsField pnd_Cor_Msg;

    private DbsGroup pnd_Cor_Msg__R_Field_19;
    private DbsField pnd_Cor_Msg_Pnd_Callnat_Cai_Filler;
    private DbsField pnd_Cor_Msg_Pnd_Callnat_Cai_Ind;

    private DbsGroup pnd_Cor_Msg__R_Field_20;
    private DbsField pnd_Cor_Msg_Pnd_Cor_Ppg;
    private DbsField pnd_Cor_Msg_Pnd_Cor_Cai_Ind;
    private DbsField pnd_Cor_Msg_Pnd_Cor_Omni_Account_Issuance;
    private DbsField pnd_Cor_Msg_Pnd_Cor_Tnt_Ind;
    private DbsField pnd_Cor_Msg_Pnd_Cor_Icap_Ind;
    private DbsField pnd_Cor_Msg_Pnd_Cor_Oia_Ind;
    private DbsField pnd_Cor_Msg_Pnd_Cor_Process_Ind;
    private DbsField pnd_Prod_Code;
    private DbsField pnd_Ssn_Alpha;

    private DbsGroup pnd_Ssn_Alpha__R_Field_21;
    private DbsField pnd_Ssn_Alpha_Pnd_Ssn_Numeric;
    private DbsField pnd_Ph_Dob_Dte;

    private DbsGroup pnd_Ph_Dob_Dte__R_Field_22;
    private DbsField pnd_Ph_Dob_Dte_Pnd_Ph_Dob_Dte_Alpha;
    private DbsField pnd_Issue_Date;
    private DbsField pnd_Issue_Date_A;

    private DbsGroup pnd_Issue_Date_A__R_Field_23;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_N;
    private DbsField pnd_Use_Bpel_Data;

    private DataAccessProgramView vw_app_Table_Entry;
    private DbsField app_Table_Entry_Entry_Actve_Ind;
    private DbsField app_Table_Entry_Entry_Table_Id_Nbr;
    private DbsField app_Table_Entry_Entry_Table_Sub_Id;
    private DbsField app_Table_Entry_Entry_Cde;

    private DbsGroup app_Table_Entry__R_Field_24;
    private DbsField app_Table_Entry_Entry_Rule_Category;
    private DbsField app_Table_Entry_Entry_Rule_Plan;

    private DbsGroup app_Table_Entry__R_Field_25;
    private DbsField app_Table_Entry_Contract_Cnt_To_Be_Processed;

    private DbsGroup app_Table_Entry__R_Field_26;
    private DbsField app_Table_Entry_Entry_Plan;
    private DbsField app_Table_Entry_Entry_Sub_Plan_Number;

    private DbsGroup app_Table_Entry__R_Field_27;
    private DbsField app_Table_Entry_Pnd_Rp_Subpl_Mm;
    private DbsField app_Table_Entry_Pnd_Rp_Subpl_Dd;
    private DbsField app_Table_Entry_Pnd_Rp_Subpl_Yy;
    private DbsField app_Table_Entry_Entry_Cde_Addl;
    private DbsField app_Table_Entry_Entry_Dscrptn_Txt;

    private DbsGroup app_Table_Entry__R_Field_28;
    private DbsField app_Table_Entry_Lb_Lob;
    private DbsField app_Table_Entry_Lb_Lob_Type;
    private DbsField app_Table_Entry_Entry_User_Id;
    private DbsGroup app_Table_Entry_Addtnl_Dscrptn_TxtMuGroup;
    private DbsField app_Table_Entry_Addtnl_Dscrptn_Txt;

    private DbsGroup app_Table_Entry__R_Field_29;
    private DbsField app_Table_Entry_Lb_Product_Code;
    private DbsField app_Table_Entry_Lb_Vesting;
    private DbsField pnd_Active_Table_Key;

    private DbsGroup pnd_Active_Table_Key__R_Field_30;
    private DbsField pnd_Active_Table_Key_Pnd_Entry_Actve_Ind;
    private DbsField pnd_Active_Table_Key_Pnd_Entry_Table_Id_Nbr;
    private DbsField pnd_Active_Table_Key_Pnd_Entry_Table_Sub_Id;
    private DbsField pnd_Active_Table_Key_Pnd_Entry_Cde;
    private DbsField pnd_Search_Key;

    private DbsGroup pnd_Search_Key__R_Field_31;
    private DbsField pnd_Search_Key_Pnd_Ky_Entry_Actve_Ind;
    private DbsField pnd_Search_Key_Pnd_Ky_Entry_Table_Id_Nbr;
    private DbsField pnd_Search_Key_Pnd_Ky_Entry_Table_Sub_Id;
    private DbsField pnd_Search_Key_Pnd_Ky_Entry_Cde;

    private DbsGroup pnd_Search_Key__R_Field_32;
    private DbsField pnd_Search_Key_Pnd_Ky_Plan_Number;
    private DbsField pnd_Search_Key_Pnd_Ky_Subplan_Number;
    private DbsField pnd_Search_Key_Pnd_Ky_Addl_Cde;
    private DbsField pnd_Entry_Exist;
    private DbsField pnd_Wk_Date;

    private DbsGroup pnd_Wk_Date__R_Field_33;
    private DbsField pnd_Wk_Date_Pnd_Wk_Dt_Cc;
    private DbsField pnd_Wk_Date_Pnd_Wk_Dt_Yy;
    private DbsField pnd_Wk_Date_Pnd_Wk_Dt_Mm;
    private DbsField pnd_Wk_Date_Pnd_Wk_Dt_Dd;

    private DbsGroup pnd_Edit_Flags;
    private DbsField pnd_Edit_Flags_Pnd_Edit_Communication_Data;
    private DbsField pnd_Edit_Flags_Pnd_Edit_Requestor_Data;
    private DbsField pnd_Edit_Flags_Pnd_Edit_Processing_Data;
    private DbsField pnd_Edit_Flags_Pnd_Edit_Contract_Data;
    private DbsField pnd_Edit_Flags_Pnd_Edit_Mit_Data;
    private DbsField pnd_Edit_Flags_Pnd_Edit_Participant_Data;
    private DbsField pnd_Edit_Flags_Pnd_Edit_Address_Data;
    private DbsField pnd_Edit_Flags_Pnd_Edit_Allocation_Data;
    private DbsField pnd_Edit_Flags_Pnd_Edit_Beneficiary_Data;
    private DbsField pnd_Edit_Flags_Pnd_Edit_Financial_Data;
    private DbsField pnd_W_Vesting_Code_N;

    private DbsGroup pnd_W_Vesting_Code_N__R_Field_34;
    private DbsField pnd_W_Vesting_Code_N_Pnd_W_Vesting_Code_A;
    private DbsField pnd_W_Lob;
    private DbsField pnd_W_Lob_Type;
    private DbsField pnd_W_Premium_Team_Cde;
    private DbsField pnd_W_Contract_Type;
    private DbsField pnd_W_Product_Code_N;

    private DbsGroup pnd_W_Product_Code_N__R_Field_35;
    private DbsField pnd_W_Product_Code_N_Pnd_W_Product_Code_A;
    private DbsField pnd_W_Allocation_Fmt;
    private DbsField pnd_Hold_Sg_Product;
    private DbsField pnd_Null;
    private DbsField pnd_Nbr;
    private DbsField pnd_Scia9000_Field_3;

    private DbsGroup pnd_Scia9000_Field_3__R_Field_36;
    private DbsField pnd_Scia9000_Field_3_Pnd_Scia9000_Orig_Tiaa_Contract;
    private DbsField pnd_Scia9000_Field_3_Pnd_Scia9000_Orig_Cref_Contract;

    private DataAccessProgramView vw_fidelity_Ppg;
    private DbsField fidelity_Ppg_Ppg;
    private DbsField fidelity_Ppg_Effective_Date;

    private DataAccessProgramView vw_fidelity_Contract;
    private DbsField fidelity_Contract_Plan_Num;
    private DbsField fidelity_Contract_Ph_Ssn;
    private DbsField fidelity_Contract_Ph_Tiaa_Contract;
    private DbsField fidelity_Contract_Ph_Cref_Contract;
    private DbsField fidelity_Contract_Issue_Date;
    private DbsField fidelity_Contract_Sg_Plan_No;
    private DbsField fidelity_Contract_Sg_Subplan_No;
    private DbsField fidelity_Contract_Sg_Div_Sub;
    private DbsField pnd_Pnum_Ssn;

    private DbsGroup pnd_Pnum_Ssn__R_Field_37;
    private DbsField pnd_Pnum_Ssn_Pnd_Plan_Num;
    private DbsField pnd_Pnum_Ssn_Pnd_Ph_Ssn;
    private DbsField pnd_Hold_Ap_Process;
    private DbsField pnd_Hold_Ap_Status;
    private DbsField pnd_Hold_Premium_Team_Cde;
    private DbsField pnd_Hold_Bucket_Cde;
    private DbsField pnd_Hold_Accum_Cde;
    private DbsField pnd_Entry_Dscrptn_Text;

    private DbsGroup pnd_Entry_Dscrptn_Text__R_Field_38;
    private DbsField pnd_Entry_Dscrptn_Text_Pnd_Entry_Bucket;
    private DbsField pnd_Entry_Dscrptn_Text_Pnd_Entry_Accum;
    private DbsField pnd_Entry_Dscrptn_Text_Pnd_Entry_Prap_Team;
    private DbsField pnd_Map_Ppg_Code;
    private DbsField pnd_Map_Premium_Team_Code;
    private DbsField pnd_Datn;
    private DbsField pnd_Retry_Count;
    private DbsField pnd_Lob_Type_For_Prap;
    private DbsField pnd_Date_Mmddyy;
    private DbsField pnd_Date_Mmddyy_A;

    private DbsGroup pnd_Date_Mmddyy_A__R_Field_39;
    private DbsField pnd_Date_Mmddyy_A_Pnd_Date_Mmddyy_N;
    private DbsField hold_T_Cont;

    private DbsGroup hold_T_Cont__R_Field_40;
    private DbsField hold_T_Cont_Hold_App_Pref;
    private DbsField hold_T_Cont_Hold_App_Cont;

    private DbsGroup hold_T_Cont__R_Field_41;
    private DbsField hold_T_Cont_Hold_App_Cont_0;
    private DbsField hold_C_Cont;

    private DbsGroup hold_C_Cont__R_Field_42;
    private DbsField hold_C_Cont_Hold_App_C_Pref;
    private DbsField hold_C_Cont_Hold_App_C_Cont;
    private DbsField pnd_New_Contract;

    private DbsGroup pnd_New_Contract__R_Field_43;
    private DbsField pnd_New_Contract_Pnd_New_Pref_Ed;
    private DbsField pnd_New_Contract_Pnd_Dash;
    private DbsField pnd_New_Contract_Pnd_New_Cont_Ed;
    private DbsField pnd_Compare_Index;

    private DbsGroup pnd_Compare_Index__R_Field_44;
    private DbsField pnd_Compare_Index_Pnd_Compare_Index_A;
    private DbsField pnd_Hold_T_Pref;
    private DbsField pnd_Hold_C_Pref;
    private DbsField pnd_New_Object;

    private DbsGroup pnd_Scroll_Vars;
    private DbsField pnd_Scroll_Vars_Pnd_Panel;
    private DbsField pnd_Displayed_Key;
    private DbsField hold_Control_Key;

    private DbsGroup hold_Control_Key__R_Field_45;
    private DbsField hold_Control_Key_Hold_Rectyp_C;
    private DbsField hold_Control_Key_Hold_Recip_Date;
    private DbsField pnd_Contract_No;

    private DbsGroup pnd_Contract_No__R_Field_46;
    private DbsField pnd_Contract_No_Pnd_Contract_No_1_1;
    private DbsField pnd_Contract_No_Pnd_Contract_No_2_8;
    private DbsField pnd_Hold_Date_Mmddyyyy;

    private DbsGroup pnd_Hold_Date_Mmddyyyy__R_Field_47;
    private DbsField pnd_Hold_Date_Mmddyyyy_Pnd_Hold_Date_Mmddyy;

    private DbsGroup pnd_Hold_Date_Mmddyyyy__R_Field_48;
    private DbsField pnd_Hold_Date_Mmddyyyy__Filler1;
    private DbsField pnd_Hold_Date_Mmddyyyy_Pnd_19;
    private DbsField pnd_Hold_Date_Mmddyyyy_Pnd_Yy;
    private DbsField hold_Date_Mmddyy;

    private DbsGroup hold_Date_Mmddyy__R_Field_49;
    private DbsField hold_Date_Mmddyy_Hold_Mm;
    private DbsField hold_Date_Mmddyy_Hold_Dd;
    private DbsField hold_Date_Mmddyy_Hold_Yy;

    private DbsGroup hold_Date_Mmddyy__R_Field_50;
    private DbsField hold_Date_Mmddyy_Hold_Datemmdd;

    private DbsGroup hold_Date_Mmddyy__R_Field_51;
    private DbsField hold_Date_Mmddyy_Hold_Datemm;
    private DbsField hold_Date_Mmddyy_Hold_Date_Ddyy;

    private DbsGroup hold_Date_Mmddyy__R_Field_52;
    private DbsField hold_Date_Mmddyy_Hold_Datedd;
    private DbsField hold_Date_Mmddyy_Hold_Dateyy;
    private DbsField hold_Mmyy;

    private DbsGroup hold_Mmyy__R_Field_53;
    private DbsField hold_Mmyy_Pnd_Hold_Mm;
    private DbsField hold_Mmyy_Pnd_Hold_Yy;
    private DbsField hold_Curr;

    private DbsGroup hold_Curr__R_Field_54;
    private DbsField hold_Curr_Hold_Curr_N;
    private DbsField hold_Date18;

    private DbsGroup hold_Date18__R_Field_55;
    private DbsField hold_Date18_Hold_Date12;
    private DbsField hold_Date18_Hold_Date38;

    private DbsGroup hold_Date18__R_Field_56;
    private DbsField hold_Date18_Hold_Date34;
    private DbsField hold_Date18_Hold_Date58;

    private DbsGroup hold_Date18__R_Field_57;
    private DbsField hold_Date18_Hold_Date56;
    private DbsField hold_Date18_Hold_Date78;
    private DbsField pnd_Aat_Currency_N;

    private DbsGroup pnd_Aat_Currency_N__R_Field_58;
    private DbsField pnd_Aat_Currency_N_Pnd_Aat_Currency_A;
    private DbsField pnd_Current_Date;

    private DbsGroup pnd_Current_Date__R_Field_59;
    private DbsField pnd_Current_Date__Filler2;
    private DbsField pnd_Current_Date_Pnd_Cur_Mmdd;
    private DbsField pnd_Current_Date_Pnd_Cur_Yy;
    private DbsField pnd_System_Date;

    private DbsGroup pnd_System_Date__R_Field_60;
    private DbsField pnd_System_Date_Pnd_Sys_Year;

    private DbsGroup pnd_System_Date__R_Field_61;
    private DbsField pnd_System_Date__Filler3;
    private DbsField pnd_System_Date_Pnd_Sys_Yy;
    private DbsField pnd_System_Date_Pnd_Sys_Mmdd;
    private DbsField pnd_System_Temp_Date;
    private DbsField pnd_Ap_Cor_Ind;
    private DbsField pnd_Prem_Mit_Log_Dte_Tme;
    private DbsField pnd_Policy_Holder;
    private DbsField pnd_Hold_Lob_Type_Full;

    private DbsGroup pnd_Hold_Lob_Type_Full__R_Field_62;
    private DbsField pnd_Hold_Lob_Type_Full_Pnd_Hold_Lob;
    private DbsField pnd_Hold_Lob_Type_Full_Pnd_Hold_Lob_Type;
    private DbsField pnd_Rltnshp_Cde;
    private DbsField pnd_Hold2_Dt;

    private DbsGroup pnd_Hold2_Dt__R_Field_63;
    private DbsField pnd_Hold2_Dt_Pnd_Hold2_Dt_Cc;
    private DbsField pnd_Hold2_Dt_Pnd_Hold2_Dt_Yy;
    private DbsField pnd_Hold2_Dt_Pnd_Hold2_Dt_Mm;
    private DbsField pnd_Hold2_Dt_Pnd_Hold2_Dt_Dd;
    private DbsField pnd_Hold_Dt;

    private DbsGroup pnd_Hold_Dt__R_Field_64;
    private DbsField pnd_Hold_Dt_Pnd_Hold_Dt_Mm;
    private DbsField pnd_Hold_Dt_Pnd_Hold_Dt_Dd;
    private DbsField pnd_Hold_Dt_Pnd_Hold_Dt_Cc;
    private DbsField pnd_Hold_Dt_Pnd_Hold_Dt_Yy;
    private DbsField pnd_Cor_Dob;

    private DbsGroup pnd_Cor_Dob__R_Field_65;
    private DbsField pnd_Cor_Dob_Pnd_Cor_Dob_Cc;
    private DbsField pnd_Cor_Dob_Pnd_Cor_Dob_Yy;
    private DbsField pnd_Cor_Dob_Pnd_Cor_Dob_Mm;
    private DbsField pnd_Cor_Dob_Pnd_Cor_Dob_Dd;
    private DbsField pnd_Cor_T_Doi;

    private DbsGroup pnd_Cor_T_Doi__R_Field_66;
    private DbsField pnd_Cor_T_Doi_Pnd_Cor_T_Doi_Cc;
    private DbsField pnd_Cor_T_Doi_Pnd_Cor_T_Doi_Yy;
    private DbsField pnd_Cor_T_Doi_Pnd_Cor_T_Doi_Mm;
    private DbsField pnd_Cor_T_Doi_Pnd_Cor_T_Doi_Dd;
    private DbsField pnd_Cor_C_Doi;

    private DbsGroup pnd_Cor_C_Doi__R_Field_67;
    private DbsField pnd_Cor_C_Doi_Pnd_Cor_C_Doi_Cc;
    private DbsField pnd_Cor_C_Doi_Pnd_Cor_C_Doi_Yy;
    private DbsField pnd_Cor_C_Doi_Pnd_Cor_C_Doi_Mm;
    private DbsField pnd_Cor_C_Doi_Pnd_Cor_C_Doi_Dd;
    private DbsField pnd_Hold_Dob;

    private DbsGroup pnd_Hold_Dob__R_Field_68;
    private DbsField pnd_Hold_Dob_Pnd_Hold_Dob_Mm;
    private DbsField pnd_Hold_Dob_Pnd_Hold_Dob_Dd;
    private DbsField pnd_Hold_Dob_Pnd_Hold_Dob_Cc;
    private DbsField pnd_Hold_Dob_Pnd_Hold_Dob_Yy;
    private DbsField pnd_Hold_Doi;

    private DbsGroup pnd_Hold_Doi__R_Field_69;
    private DbsField pnd_Hold_Doi_Pnd_Hold_Doi_Dd;
    private DbsField pnd_Hold_Doi_Pnd_Hold_Doi_Mm;
    private DbsField pnd_Hold_Doi_Pnd_Hold_Doi_Yy;
    private DbsField pnd_Cor_T_Contr_A;

    private DbsGroup pnd_Cor_T_Contr_A__R_Field_70;
    private DbsField pnd_Cor_T_Contr_A_Pnd_Cor_T_Prefx;
    private DbsField pnd_Cor_T_Contr_A_Pnd_Cor_T_Numbr;
    private DbsField pnd_Cor_C_Contr_A;

    private DbsGroup pnd_Cor_C_Contr_A__R_Field_71;
    private DbsField pnd_Cor_C_Contr_A_Pnd_Cor_C_Prefx;
    private DbsField pnd_Cor_C_Contr_A_Pnd_Cor_C_Numbr;
    private DbsField pnd_G_Key_Last;
    private DbsField pnd_Cor_Own_Cde;
    private DbsField pnd_Prap_Last_Name;
    private DbsField pnd_Prap_First_Name;
    private DbsField pnd_Prap_Last_First_Name;
    private DbsField pnd_C_Contr;

    private DbsGroup pnd_C_Contr__R_Field_72;
    private DbsField pnd_C_Contr_Pnd_C_Pref;
    private DbsField pnd_C_Contr_Pnd_C_Cont;
    private DbsField pnd_Ap_Tiaa_Contr;

    private DbsGroup pnd_Ap_Tiaa_Contr__R_Field_73;
    private DbsField pnd_Ap_Tiaa_Contr_Pnd_Ap_Tiaa_Contr_1;
    private DbsField pnd_Ap_Tiaa_Contr_Pnd_Ap_Tiaa_Contr_2_7;
    private DbsField pnd_Switch_Line;

    private DbsGroup pnd_Switch_Line__R_Field_74;
    private DbsField pnd_Switch_Line_Pnd_P_Name;
    private DbsField pnd_Switch_Line_Pnd_P_Fill;
    private DbsField pnd_Switch_Line_Pnd_P_Rltship;
    private DbsField pnd_Switch_Line_Pnd_P_Fill2;
    private DbsField pnd_Switch_Line_Pnd_P_Ssn;

    private DbsGroup pnd_Switch_Line__R_Field_75;
    private DbsField pnd_Switch_Line_Pnd_P_Ssn_A;
    private DbsField pnd_Switch_Line_Pnd_P_Fill3;
    private DbsField pnd_Switch_Line_Pnd_P_Dob;

    private DbsGroup pnd_Switch_Line__R_Field_76;
    private DbsField pnd_Switch_Line_Pnd_P_Dob_A;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Plan;
    private DbsField pnd_State_2;
    private DbsField pnd_Db_Up;
    private DbsField pnd_Date_Yyyymmdd;

    private DbsGroup pnd_Date_Yyyymmdd__R_Field_77;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Yyyymmdd_Yyyy;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Yyyymmdd_Mm;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Yyyymmdd_Dd;

    private DbsGroup pnd_Date_Yyyymmdd__R_Field_78;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Yyyymmdd_A;
    private DbsField pnd_Date_Mmddyyyy;

    private DbsGroup pnd_Date_Mmddyyyy__R_Field_79;
    private DbsField pnd_Date_Mmddyyyy_Pnd_Date_Mmddyyyy_Mm;
    private DbsField pnd_Date_Mmddyyyy_Pnd_Date_Mmddyyyy_Dd;
    private DbsField pnd_Date_Mmddyyyy_Pnd_Date_Mmddyyyy_Yyyy;

    private DbsGroup pnd_Date_Mmddyyyy__R_Field_80;
    private DbsField pnd_Date_Mmddyyyy_Pnd_Date_Mmddyyyy_A;
    private DbsField pnd_Escape_Routine;
    private DbsField pnd_Record_Sts;
    private DbsField pnd_Irc_Sec_Cde_N;

    private DbsGroup pnd_Irc_Sec_Cde_N__R_Field_81;
    private DbsField pnd_Irc_Sec_Cde_N_Pnd_Irc_Sec_Cde;
    private DbsField pnd_Irc_Sec_Grp_Cde_N;

    private DbsGroup pnd_Irc_Sec_Grp_Cde_N__R_Field_82;
    private DbsField pnd_Irc_Sec_Grp_Cde_N_Pnd_Irc_Sec_Grp_Cde;
    private DbsField pnd_Addtnl_Txt;

    private DbsGroup pnd_Addtnl_Txt__R_Field_83;
    private DbsField pnd_Addtnl_Txt_Pnd_Prdct_Cd;
    private DbsField pnd_Addtnl_Txt_Pnd_Vesting_Local;
    private DbsField pnd_Dscrptn_Txt;

    private DbsGroup pnd_Dscrptn_Txt__R_Field_84;
    private DbsField pnd_Dscrptn_Txt_Pnd_Lob;
    private DbsField pnd_Dscrptn_Txt_Pnd_Lob_Type;
    private DbsField pnd_Temp_Doi;

    private DbsGroup pnd_Temp_Doi__R_Field_85;
    private DbsField pnd_Temp_Doi_Pnd_Temp_T_Doi_Mm;
    private DbsField pnd_Temp_Doi_Pnd_Temp_T_Doi_Dd;
    private DbsField pnd_Temp_Doi_Pnd_Temp_T_Doi_Cc;
    private DbsField pnd_Temp_Doi_Pnd_Temp_T_Doi_Yy;
    private DbsField pnd_Temp_Doi1;

    private DbsGroup pnd_Temp_Doi1__R_Field_86;
    private DbsField pnd_Temp_Doi1_Pnd_Temp_T_Doi1_Mm;
    private DbsField pnd_Temp_Doi1_Pnd_Temp_T_Doi1_Dd;
    private DbsField pnd_Temp_Doi1_Pnd_Temp_T_Doi1_Yy;
    private DbsField pnd_Address;
    private DbsField pnd_Contract;
    private DbsField pnd_Action;
    private DbsField pnd_Isn_1;
    private DbsField pnd_Tmp_Alloc;
    private DbsField pnd_Tmp_Fund_Cde;
    private DbsField pnd_Tmp_Fund_Source_Cde_1;
    private DbsField pnd_Tmp_Fund_Source_Cde_2;
    private DbsField pnd_Tmp_Alloc_2;
    private DbsField pnd_Tmp_Fund_Cde_2;
    private DbsField pnd_Mit_Update;
    private DbsField pnd_Hold_Mit_Wpid;
    private DbsField pnd_Tmp_Msg;
    private DbsField pnd_Tmp_Msg_Nr;
    private DbsField pnd_Tmp_Process_Error_Ind;
    private DbsField pnd_Ssn_Mayo_A;

    private DbsGroup pnd_Ssn_Mayo_A__R_Field_87;
    private DbsField pnd_Ssn_Mayo_A_Pnd_Ssn_Mayo_N;

    private DbsGroup scia8580;
    private DbsField scia8580_Pnd_Action_Code;
    private DbsField scia8580_Pnd_Register_Id;
    private DbsField scia8580_Pnd_Return_Code;
    private DbsField scia8580_Pnd_Return_Msg;
    private DbsField pnd_Fidelity_Contract_Found;
    private DbsField pnd_Tiaa_Cref_Contract;

    private DbsGroup pnd_Tiaa_Cref_Contract__R_Field_88;
    private DbsField pnd_Tiaa_Cref_Contract_Pnd_Tiaa_Contract;
    private DbsField pnd_Tiaa_Cref_Contract_Pnd_Cref_Contract;
    private DbsField pnd_Z;
    private DbsField pnd_Current_Field;
    private DbsField pnd_Db_Call;
    private DbsField pnd_D1;
    private DbsField pnd_D2;
    private DbsField pnd_Object;
    private DbsField pnd_Old_Rec;
    private DbsField pnd_Save_Rec;
    private DbsField pnd_Update_Performed;

    private DataAccessProgramView vw_next_View;
    private DbsField next_View_Bene_Sts_Lob_Ssn;
    private DbsField pls_Lb_Cnt;
    private DbsField pls_Lb_Contract_Type;
    private DbsField pls_Lb_Lob;
    private DbsField pls_Lb_Lob_Type;
    private DbsField pls_Lb_Product_Code;
    private DbsField pls_Lb_Vesting;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaScia1200 = new PdaScia1200(localVariables);
        pdaAcipda_D = new PdaAcipda_D(localVariables);
        pdaAcipda_M = new PdaAcipda_M(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCdmntdfa = new PdaCdmntdfa(localVariables);
        pdaScia1100 = new PdaScia1100(localVariables);
        pdaScia1102 = new PdaScia1102(localVariables);
        pdaScia1105 = new PdaScia1105(localVariables);
        pdaScia1203 = new PdaScia1203(localVariables);
        ldaScil1080 = new LdaScil1080();
        registerRecord(ldaScil1080);
        pdaScia2100 = new PdaScia2100(localVariables);
        pdaScia3000 = new PdaScia3000(localVariables);
        ldaScil3010 = new LdaScil3010();
        registerRecord(ldaScil3010);
        registerRecord(ldaScil3010.getVw_acis_Bene_File());
        pdaScia301r = new PdaScia301r(localVariables);
        pdaScia3012 = new PdaScia3012(localVariables);
        pdaScia8510 = new PdaScia8510(localVariables);
        pdaScia3530 = new PdaScia3530(localVariables);
        pdaScia3531 = new PdaScia3531(localVariables);
        pdaScia3535 = new PdaScia3535(localVariables);
        pdaScia3545 = new PdaScia3545(localVariables);
        pdaScia3540 = new PdaScia3540(localVariables);
        pdaScia3560 = new PdaScia3560(localVariables);
        pdaScia3570 = new PdaScia3570(localVariables);
        pdaScia3500 = new PdaScia3500(localVariables);
        pdaScia3600 = new PdaScia3600(localVariables);
        pdaScia3590 = new PdaScia3590(localVariables);
        pdaScia1281 = new PdaScia1281(localVariables);
        pdaScia9004 = new PdaScia9004(localVariables);
        pdaScia904r = new PdaScia904r(localVariables);
        pdaMdma101 = new PdaMdma101(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaScia9000 = new PdaScia9000(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        scin9000 = localVariables.newGroupInRecord("scin9000", "SCIN9000");
        scin9000_Pnd_Field1 = scin9000.newFieldInGroup("scin9000_Pnd_Field1", "#FIELD1", FieldType.STRING, 30);

        scin9000__R_Field_1 = scin9000.newGroupInGroup("scin9000__R_Field_1", "REDEFINE", scin9000_Pnd_Field1);
        scin9000_Pnd_Acibatbu_9000_Region_Code = scin9000__R_Field_1.newFieldInGroup("scin9000_Pnd_Acibatbu_9000_Region_Code", "#ACIBATBU-9000-REGION-CODE", 
            FieldType.STRING, 3);
        scin9000_Pnd_Acibatbu_9000_Mics = scin9000__R_Field_1.newFieldInGroup("scin9000_Pnd_Acibatbu_9000_Mics", "#ACIBATBU-9000-MICS", FieldType.STRING, 
            27);
        scin9000_Pnd_Field2 = scin9000.newFieldInGroup("scin9000_Pnd_Field2", "#FIELD2", FieldType.STRING, 30);
        scin9000_Pnd_Field3 = scin9000.newFieldInGroup("scin9000_Pnd_Field3", "#FIELD3", FieldType.STRING, 30);
        pnd_A100 = localVariables.newFieldInRecord("pnd_A100", "#A100", FieldType.STRING, 100);

        pnd_A100__R_Field_2 = localVariables.newGroupInRecord("pnd_A100__R_Field_2", "REDEFINE", pnd_A100);
        pnd_A100_Pnd_N1 = pnd_A100__R_Field_2.newFieldInGroup("pnd_A100_Pnd_N1", "#N1", FieldType.NUMERIC, 1);

        pnd_A100__R_Field_3 = pnd_A100__R_Field_2.newGroupInGroup("pnd_A100__R_Field_3", "REDEFINE", pnd_A100_Pnd_N1);
        pnd_A100_Pnd_A1 = pnd_A100__R_Field_3.newFieldInGroup("pnd_A100_Pnd_A1", "#A1", FieldType.STRING, 1);

        pnd_A100__R_Field_4 = localVariables.newGroupInRecord("pnd_A100__R_Field_4", "REDEFINE", pnd_A100);
        pnd_A100_Pnd_N3 = pnd_A100__R_Field_4.newFieldInGroup("pnd_A100_Pnd_N3", "#N3", FieldType.NUMERIC, 3);

        pnd_A100__R_Field_5 = pnd_A100__R_Field_4.newGroupInGroup("pnd_A100__R_Field_5", "REDEFINE", pnd_A100_Pnd_N3);
        pnd_A100_Pnd_A3 = pnd_A100__R_Field_5.newFieldInGroup("pnd_A100_Pnd_A3", "#A3", FieldType.STRING, 3);

        pnd_A100__R_Field_6 = localVariables.newGroupInRecord("pnd_A100__R_Field_6", "REDEFINE", pnd_A100);
        pnd_A100_Pnd_N4 = pnd_A100__R_Field_6.newFieldInGroup("pnd_A100_Pnd_N4", "#N4", FieldType.NUMERIC, 4);

        pnd_A100__R_Field_7 = pnd_A100__R_Field_6.newGroupInGroup("pnd_A100__R_Field_7", "REDEFINE", pnd_A100_Pnd_N4);
        pnd_A100_Pnd_A4 = pnd_A100__R_Field_7.newFieldInGroup("pnd_A100_Pnd_A4", "#A4", FieldType.STRING, 4);

        pnd_A100__R_Field_8 = localVariables.newGroupInRecord("pnd_A100__R_Field_8", "REDEFINE", pnd_A100);
        pnd_A100_Pnd_N6 = pnd_A100__R_Field_8.newFieldInGroup("pnd_A100_Pnd_N6", "#N6", FieldType.NUMERIC, 6);

        pnd_A100__R_Field_9 = pnd_A100__R_Field_8.newGroupInGroup("pnd_A100__R_Field_9", "REDEFINE", pnd_A100_Pnd_N6);
        pnd_A100_Pnd_A6 = pnd_A100__R_Field_9.newFieldInGroup("pnd_A100_Pnd_A6", "#A6", FieldType.STRING, 6);

        pnd_A100__R_Field_10 = localVariables.newGroupInRecord("pnd_A100__R_Field_10", "REDEFINE", pnd_A100);
        pnd_A100_Pnd_N7 = pnd_A100__R_Field_10.newFieldInGroup("pnd_A100_Pnd_N7", "#N7", FieldType.NUMERIC, 7);

        pnd_A100__R_Field_11 = pnd_A100__R_Field_10.newGroupInGroup("pnd_A100__R_Field_11", "REDEFINE", pnd_A100_Pnd_N7);
        pnd_A100_Pnd_A7 = pnd_A100__R_Field_11.newFieldInGroup("pnd_A100_Pnd_A7", "#A7", FieldType.STRING, 7);

        pnd_A100__R_Field_12 = localVariables.newGroupInRecord("pnd_A100__R_Field_12", "REDEFINE", pnd_A100);
        pnd_A100_Pnd_N8 = pnd_A100__R_Field_12.newFieldInGroup("pnd_A100_Pnd_N8", "#N8", FieldType.NUMERIC, 8);

        pnd_A100__R_Field_13 = pnd_A100__R_Field_12.newGroupInGroup("pnd_A100__R_Field_13", "REDEFINE", pnd_A100_Pnd_N8);
        pnd_A100_Pnd_A8 = pnd_A100__R_Field_13.newFieldInGroup("pnd_A100_Pnd_A8", "#A8", FieldType.STRING, 8);

        pnd_A100__R_Field_14 = localVariables.newGroupInRecord("pnd_A100__R_Field_14", "REDEFINE", pnd_A100);
        pnd_A100_Pnd_N9 = pnd_A100__R_Field_14.newFieldInGroup("pnd_A100_Pnd_N9", "#N9", FieldType.NUMERIC, 9);

        pnd_A100__R_Field_15 = pnd_A100__R_Field_14.newGroupInGroup("pnd_A100__R_Field_15", "REDEFINE", pnd_A100_Pnd_N9);
        pnd_A100_Pnd_A9 = pnd_A100__R_Field_15.newFieldInGroup("pnd_A100_Pnd_A9", "#A9", FieldType.STRING, 9);
        pnd_Temp_Ssn_N = localVariables.newFieldInRecord("pnd_Temp_Ssn_N", "#TEMP-SSN-N", FieldType.NUMERIC, 9);

        pnd_Temp_Ssn_N__R_Field_16 = localVariables.newGroupInRecord("pnd_Temp_Ssn_N__R_Field_16", "REDEFINE", pnd_Temp_Ssn_N);
        pnd_Temp_Ssn_N_Pnd_Temp_Ssn_A = pnd_Temp_Ssn_N__R_Field_16.newFieldInGroup("pnd_Temp_Ssn_N_Pnd_Temp_Ssn_A", "#TEMP-SSN-A", FieldType.STRING, 9);
        pnd_Cni_Data = localVariables.newFieldInRecord("pnd_Cni_Data", "#CNI-DATA", FieldType.STRING, 79);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.DATE);
        pnd_Region_Code = localVariables.newFieldInRecord("pnd_Region_Code", "#REGION-CODE", FieldType.STRING, 3);
        pnd_New_Alloc_Format = localVariables.newFieldInRecord("pnd_New_Alloc_Format", "#NEW-ALLOC-FORMAT", FieldType.STRING, 1);
        pnd_From_Online_Enrollment = localVariables.newFieldInRecord("pnd_From_Online_Enrollment", "#FROM-ONLINE-ENROLLMENT", FieldType.STRING, 1);
        pnd_In_Here = localVariables.newFieldInRecord("pnd_In_Here", "#IN-HERE", FieldType.STRING, 1);
        pnd_Default_Enrollment = localVariables.newFieldInRecord("pnd_Default_Enrollment", "#DEFAULT-ENROLLMENT", FieldType.STRING, 1);
        pnd_Display_Flag = localVariables.newFieldInRecord("pnd_Display_Flag", "#DISPLAY-FLAG", FieldType.STRING, 1);
        pnd_Error = localVariables.newFieldInRecord("pnd_Error", "#ERROR", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 3);
        pnd_Pf_Key_A8 = localVariables.newFieldInRecord("pnd_Pf_Key_A8", "#PF-KEY-A8", FieldType.STRING, 8);
        pnd_Vesting_Desc = localVariables.newFieldArrayInRecord("pnd_Vesting_Desc", "#VESTING-DESC", FieldType.STRING, 13, new DbsArrayController(1, 2));
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Number = localVariables.newFieldInRecord("pnd_Number", "#NUMBER", FieldType.NUMERIC, 2);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.BOOLEAN, 1);
        pnd_Temp_Date = localVariables.newFieldInRecord("pnd_Temp_Date", "#TEMP-DATE", FieldType.NUMERIC, 8);

        pnd_Temp_Date__R_Field_17 = localVariables.newGroupInRecord("pnd_Temp_Date__R_Field_17", "REDEFINE", pnd_Temp_Date);
        pnd_Temp_Date_Pnd_Temp_Date_A = pnd_Temp_Date__R_Field_17.newFieldInGroup("pnd_Temp_Date_Pnd_Temp_Date_A", "#TEMP-DATE-A", FieldType.STRING, 8);

        pnd_Cor_Contract_Data = localVariables.newGroupInRecord("pnd_Cor_Contract_Data", "#COR-CONTRACT-DATA");
        pnd_Cor_Contract_Data_Pnd_Cor_Cntrct_Cref_Nbr = pnd_Cor_Contract_Data.newFieldInGroup("pnd_Cor_Contract_Data_Pnd_Cor_Cntrct_Cref_Nbr", "#COR-CNTRCT-CREF-NBR", 
            FieldType.STRING, 10);
        pnd_Cor_Contract_Data_Pnd_Cor_Cor_Cntrct_Add_Dte = pnd_Cor_Contract_Data.newFieldInGroup("pnd_Cor_Contract_Data_Pnd_Cor_Cor_Cntrct_Add_Dte", "#COR-COR-CNTRCT-ADD-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Mit_Process_Ind = localVariables.newFieldInRecord("pnd_Mit_Process_Ind", "#MIT-PROCESS-IND", FieldType.STRING, 1);
        pnd_Non_T_C_Administered_Plan = localVariables.newFieldInRecord("pnd_Non_T_C_Administered_Plan", "#NON-T-C-ADMINISTERED-PLAN", FieldType.BOOLEAN, 
            1);
        pnd_Cip = localVariables.newFieldInRecord("pnd_Cip", "#CIP", FieldType.STRING, 1);
        pnd_Prdct_Cde = localVariables.newFieldInRecord("pnd_Prdct_Cde", "#PRDCT-CDE", FieldType.STRING, 3);

        pnd_Prdct_Cde__R_Field_18 = localVariables.newGroupInRecord("pnd_Prdct_Cde__R_Field_18", "REDEFINE", pnd_Prdct_Cde);
        pnd_Prdct_Cde_Pnd_Prdct_1 = pnd_Prdct_Cde__R_Field_18.newFieldInGroup("pnd_Prdct_Cde_Pnd_Prdct_1", "#PRDCT-1", FieldType.STRING, 1);
        pnd_Prdct_Cde_Pnd_Prdct_2 = pnd_Prdct_Cde__R_Field_18.newFieldInGroup("pnd_Prdct_Cde_Pnd_Prdct_2", "#PRDCT-2", FieldType.STRING, 1);
        pnd_Prdct_Cde_Pnd_Prdct_3 = pnd_Prdct_Cde__R_Field_18.newFieldInGroup("pnd_Prdct_Cde_Pnd_Prdct_3", "#PRDCT-3", FieldType.STRING, 1);
        pnd_Cor_Msg = localVariables.newFieldInRecord("pnd_Cor_Msg", "#COR-MSG", FieldType.STRING, 79);

        pnd_Cor_Msg__R_Field_19 = localVariables.newGroupInRecord("pnd_Cor_Msg__R_Field_19", "REDEFINE", pnd_Cor_Msg);
        pnd_Cor_Msg_Pnd_Callnat_Cai_Filler = pnd_Cor_Msg__R_Field_19.newFieldInGroup("pnd_Cor_Msg_Pnd_Callnat_Cai_Filler", "#CALLNAT-CAI-FILLER", FieldType.STRING, 
            6);
        pnd_Cor_Msg_Pnd_Callnat_Cai_Ind = pnd_Cor_Msg__R_Field_19.newFieldInGroup("pnd_Cor_Msg_Pnd_Callnat_Cai_Ind", "#CALLNAT-CAI-IND", FieldType.STRING, 
            1);

        pnd_Cor_Msg__R_Field_20 = localVariables.newGroupInRecord("pnd_Cor_Msg__R_Field_20", "REDEFINE", pnd_Cor_Msg);
        pnd_Cor_Msg_Pnd_Cor_Ppg = pnd_Cor_Msg__R_Field_20.newFieldInGroup("pnd_Cor_Msg_Pnd_Cor_Ppg", "#COR-PPG", FieldType.STRING, 6);
        pnd_Cor_Msg_Pnd_Cor_Cai_Ind = pnd_Cor_Msg__R_Field_20.newFieldInGroup("pnd_Cor_Msg_Pnd_Cor_Cai_Ind", "#COR-CAI-IND", FieldType.STRING, 1);
        pnd_Cor_Msg_Pnd_Cor_Omni_Account_Issuance = pnd_Cor_Msg__R_Field_20.newFieldInGroup("pnd_Cor_Msg_Pnd_Cor_Omni_Account_Issuance", "#COR-OMNI-ACCOUNT-ISSUANCE", 
            FieldType.STRING, 1);
        pnd_Cor_Msg_Pnd_Cor_Tnt_Ind = pnd_Cor_Msg__R_Field_20.newFieldInGroup("pnd_Cor_Msg_Pnd_Cor_Tnt_Ind", "#COR-TNT-IND", FieldType.STRING, 1);
        pnd_Cor_Msg_Pnd_Cor_Icap_Ind = pnd_Cor_Msg__R_Field_20.newFieldInGroup("pnd_Cor_Msg_Pnd_Cor_Icap_Ind", "#COR-ICAP-IND", FieldType.STRING, 1);
        pnd_Cor_Msg_Pnd_Cor_Oia_Ind = pnd_Cor_Msg__R_Field_20.newFieldInGroup("pnd_Cor_Msg_Pnd_Cor_Oia_Ind", "#COR-OIA-IND", FieldType.STRING, 2);
        pnd_Cor_Msg_Pnd_Cor_Process_Ind = pnd_Cor_Msg__R_Field_20.newFieldInGroup("pnd_Cor_Msg_Pnd_Cor_Process_Ind", "#COR-PROCESS-IND", FieldType.STRING, 
            1);
        pnd_Prod_Code = localVariables.newFieldInRecord("pnd_Prod_Code", "#PROD-CODE", FieldType.NUMERIC, 3);
        pnd_Ssn_Alpha = localVariables.newFieldInRecord("pnd_Ssn_Alpha", "#SSN-ALPHA", FieldType.STRING, 9);

        pnd_Ssn_Alpha__R_Field_21 = localVariables.newGroupInRecord("pnd_Ssn_Alpha__R_Field_21", "REDEFINE", pnd_Ssn_Alpha);
        pnd_Ssn_Alpha_Pnd_Ssn_Numeric = pnd_Ssn_Alpha__R_Field_21.newFieldInGroup("pnd_Ssn_Alpha_Pnd_Ssn_Numeric", "#SSN-NUMERIC", FieldType.NUMERIC, 
            9);
        pnd_Ph_Dob_Dte = localVariables.newFieldInRecord("pnd_Ph_Dob_Dte", "#PH-DOB-DTE", FieldType.NUMERIC, 8);

        pnd_Ph_Dob_Dte__R_Field_22 = localVariables.newGroupInRecord("pnd_Ph_Dob_Dte__R_Field_22", "REDEFINE", pnd_Ph_Dob_Dte);
        pnd_Ph_Dob_Dte_Pnd_Ph_Dob_Dte_Alpha = pnd_Ph_Dob_Dte__R_Field_22.newFieldInGroup("pnd_Ph_Dob_Dte_Pnd_Ph_Dob_Dte_Alpha", "#PH-DOB-DTE-ALPHA", FieldType.STRING, 
            8);
        pnd_Issue_Date = localVariables.newFieldInRecord("pnd_Issue_Date", "#ISSUE-DATE", FieldType.DATE);
        pnd_Issue_Date_A = localVariables.newFieldInRecord("pnd_Issue_Date_A", "#ISSUE-DATE-A", FieldType.STRING, 4);

        pnd_Issue_Date_A__R_Field_23 = localVariables.newGroupInRecord("pnd_Issue_Date_A__R_Field_23", "REDEFINE", pnd_Issue_Date_A);
        pnd_Issue_Date_A_Pnd_Issue_Date_N = pnd_Issue_Date_A__R_Field_23.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_N", "#ISSUE-DATE-N", FieldType.NUMERIC, 
            4);
        pnd_Use_Bpel_Data = localVariables.newFieldInRecord("pnd_Use_Bpel_Data", "#USE-BPEL-DATA", FieldType.STRING, 1);

        vw_app_Table_Entry = new DataAccessProgramView(new NameInfo("vw_app_Table_Entry", "APP-TABLE-ENTRY"), "APP_TABLE_ENTRY", "ACIS_APP_TBL_ENT");
        app_Table_Entry_Entry_Actve_Ind = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Actve_Ind", "ENTRY-ACTVE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ENTRY_ACTVE_IND");
        app_Table_Entry_Entry_Table_Id_Nbr = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "ENTRY_TABLE_ID_NBR");
        app_Table_Entry_Entry_Table_Sub_Id = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ENTRY_TABLE_SUB_ID");
        app_Table_Entry_Entry_Cde = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "ENTRY_CDE");

        app_Table_Entry__R_Field_24 = vw_app_Table_Entry.getRecord().newGroupInGroup("app_Table_Entry__R_Field_24", "REDEFINE", app_Table_Entry_Entry_Cde);
        app_Table_Entry_Entry_Rule_Category = app_Table_Entry__R_Field_24.newFieldInGroup("app_Table_Entry_Entry_Rule_Category", "ENTRY-RULE-CATEGORY", 
            FieldType.STRING, 14);
        app_Table_Entry_Entry_Rule_Plan = app_Table_Entry__R_Field_24.newFieldInGroup("app_Table_Entry_Entry_Rule_Plan", "ENTRY-RULE-PLAN", FieldType.STRING, 
            6);

        app_Table_Entry__R_Field_25 = vw_app_Table_Entry.getRecord().newGroupInGroup("app_Table_Entry__R_Field_25", "REDEFINE", app_Table_Entry_Entry_Cde);
        app_Table_Entry_Contract_Cnt_To_Be_Processed = app_Table_Entry__R_Field_25.newFieldInGroup("app_Table_Entry_Contract_Cnt_To_Be_Processed", "CONTRACT-CNT-TO-BE-PROCESSED", 
            FieldType.NUMERIC, 6);

        app_Table_Entry__R_Field_26 = vw_app_Table_Entry.getRecord().newGroupInGroup("app_Table_Entry__R_Field_26", "REDEFINE", app_Table_Entry_Entry_Cde);
        app_Table_Entry_Entry_Plan = app_Table_Entry__R_Field_26.newFieldInGroup("app_Table_Entry_Entry_Plan", "ENTRY-PLAN", FieldType.STRING, 6);
        app_Table_Entry_Entry_Sub_Plan_Number = app_Table_Entry__R_Field_26.newFieldInGroup("app_Table_Entry_Entry_Sub_Plan_Number", "ENTRY-SUB-PLAN-NUMBER", 
            FieldType.STRING, 6);

        app_Table_Entry__R_Field_27 = app_Table_Entry__R_Field_26.newGroupInGroup("app_Table_Entry__R_Field_27", "REDEFINE", app_Table_Entry_Entry_Sub_Plan_Number);
        app_Table_Entry_Pnd_Rp_Subpl_Mm = app_Table_Entry__R_Field_27.newFieldInGroup("app_Table_Entry_Pnd_Rp_Subpl_Mm", "#RP-SUBPL-MM", FieldType.STRING, 
            2);
        app_Table_Entry_Pnd_Rp_Subpl_Dd = app_Table_Entry__R_Field_27.newFieldInGroup("app_Table_Entry_Pnd_Rp_Subpl_Dd", "#RP-SUBPL-DD", FieldType.STRING, 
            2);
        app_Table_Entry_Pnd_Rp_Subpl_Yy = app_Table_Entry__R_Field_27.newFieldInGroup("app_Table_Entry_Pnd_Rp_Subpl_Yy", "#RP-SUBPL-YY", FieldType.STRING, 
            2);
        app_Table_Entry_Entry_Cde_Addl = app_Table_Entry__R_Field_26.newFieldInGroup("app_Table_Entry_Entry_Cde_Addl", "ENTRY-CDE-ADDL", FieldType.STRING, 
            8);
        app_Table_Entry_Entry_Dscrptn_Txt = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "ENTRY_DSCRPTN_TXT");

        app_Table_Entry__R_Field_28 = vw_app_Table_Entry.getRecord().newGroupInGroup("app_Table_Entry__R_Field_28", "REDEFINE", app_Table_Entry_Entry_Dscrptn_Txt);
        app_Table_Entry_Lb_Lob = app_Table_Entry__R_Field_28.newFieldInGroup("app_Table_Entry_Lb_Lob", "LB-LOB", FieldType.STRING, 1);
        app_Table_Entry_Lb_Lob_Type = app_Table_Entry__R_Field_28.newFieldInGroup("app_Table_Entry_Lb_Lob_Type", "LB-LOB-TYPE", FieldType.STRING, 1);
        app_Table_Entry_Entry_User_Id = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_User_Id", "ENTRY-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ENTRY_USER_ID");
        app_Table_Entry_Addtnl_Dscrptn_TxtMuGroup = vw_app_Table_Entry.getRecord().newGroupInGroup("APP_TABLE_ENTRY_ADDTNL_DSCRPTN_TXTMuGroup", "ADDTNL_DSCRPTN_TXTMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ACIS_APP_TBL_ENT_ADDTNL_DSCRPTN_TXT");
        app_Table_Entry_Addtnl_Dscrptn_Txt = app_Table_Entry_Addtnl_Dscrptn_TxtMuGroup.newFieldArrayInGroup("app_Table_Entry_Addtnl_Dscrptn_Txt", "ADDTNL-DSCRPTN-TXT", 
            FieldType.STRING, 60, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADDTNL_DSCRPTN_TXT");

        app_Table_Entry__R_Field_29 = vw_app_Table_Entry.getRecord().newGroupInGroup("app_Table_Entry__R_Field_29", "REDEFINE", app_Table_Entry_Addtnl_Dscrptn_Txt);
        app_Table_Entry_Lb_Product_Code = app_Table_Entry__R_Field_29.newFieldInGroup("app_Table_Entry_Lb_Product_Code", "LB-PRODUCT-CODE", FieldType.NUMERIC, 
            3);
        app_Table_Entry_Lb_Vesting = app_Table_Entry__R_Field_29.newFieldInGroup("app_Table_Entry_Lb_Vesting", "LB-VESTING", FieldType.STRING, 1);
        registerRecord(vw_app_Table_Entry);

        pnd_Active_Table_Key = localVariables.newFieldInRecord("pnd_Active_Table_Key", "#ACTIVE-TABLE-KEY", FieldType.STRING, 29);

        pnd_Active_Table_Key__R_Field_30 = localVariables.newGroupInRecord("pnd_Active_Table_Key__R_Field_30", "REDEFINE", pnd_Active_Table_Key);
        pnd_Active_Table_Key_Pnd_Entry_Actve_Ind = pnd_Active_Table_Key__R_Field_30.newFieldInGroup("pnd_Active_Table_Key_Pnd_Entry_Actve_Ind", "#ENTRY-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Active_Table_Key_Pnd_Entry_Table_Id_Nbr = pnd_Active_Table_Key__R_Field_30.newFieldInGroup("pnd_Active_Table_Key_Pnd_Entry_Table_Id_Nbr", 
            "#ENTRY-TABLE-ID-NBR", FieldType.NUMERIC, 6);
        pnd_Active_Table_Key_Pnd_Entry_Table_Sub_Id = pnd_Active_Table_Key__R_Field_30.newFieldInGroup("pnd_Active_Table_Key_Pnd_Entry_Table_Sub_Id", 
            "#ENTRY-TABLE-SUB-ID", FieldType.STRING, 2);
        pnd_Active_Table_Key_Pnd_Entry_Cde = pnd_Active_Table_Key__R_Field_30.newFieldInGroup("pnd_Active_Table_Key_Pnd_Entry_Cde", "#ENTRY-CDE", FieldType.STRING, 
            20);
        pnd_Search_Key = localVariables.newFieldInRecord("pnd_Search_Key", "#SEARCH-KEY", FieldType.STRING, 29);

        pnd_Search_Key__R_Field_31 = localVariables.newGroupInRecord("pnd_Search_Key__R_Field_31", "REDEFINE", pnd_Search_Key);
        pnd_Search_Key_Pnd_Ky_Entry_Actve_Ind = pnd_Search_Key__R_Field_31.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Entry_Actve_Ind", "#KY-ENTRY-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Search_Key_Pnd_Ky_Entry_Table_Id_Nbr = pnd_Search_Key__R_Field_31.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Entry_Table_Id_Nbr", "#KY-ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6);
        pnd_Search_Key_Pnd_Ky_Entry_Table_Sub_Id = pnd_Search_Key__R_Field_31.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Entry_Table_Sub_Id", "#KY-ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2);
        pnd_Search_Key_Pnd_Ky_Entry_Cde = pnd_Search_Key__R_Field_31.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Entry_Cde", "#KY-ENTRY-CDE", FieldType.STRING, 
            20);

        pnd_Search_Key__R_Field_32 = pnd_Search_Key__R_Field_31.newGroupInGroup("pnd_Search_Key__R_Field_32", "REDEFINE", pnd_Search_Key_Pnd_Ky_Entry_Cde);
        pnd_Search_Key_Pnd_Ky_Plan_Number = pnd_Search_Key__R_Field_32.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Plan_Number", "#KY-PLAN-NUMBER", FieldType.STRING, 
            6);
        pnd_Search_Key_Pnd_Ky_Subplan_Number = pnd_Search_Key__R_Field_32.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Subplan_Number", "#KY-SUBPLAN-NUMBER", 
            FieldType.STRING, 6);
        pnd_Search_Key_Pnd_Ky_Addl_Cde = pnd_Search_Key__R_Field_32.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Addl_Cde", "#KY-ADDL-CDE", FieldType.STRING, 
            8);
        pnd_Entry_Exist = localVariables.newFieldInRecord("pnd_Entry_Exist", "#ENTRY-EXIST", FieldType.BOOLEAN, 1);
        pnd_Wk_Date = localVariables.newFieldInRecord("pnd_Wk_Date", "#WK-DATE", FieldType.STRING, 8);

        pnd_Wk_Date__R_Field_33 = localVariables.newGroupInRecord("pnd_Wk_Date__R_Field_33", "REDEFINE", pnd_Wk_Date);
        pnd_Wk_Date_Pnd_Wk_Dt_Cc = pnd_Wk_Date__R_Field_33.newFieldInGroup("pnd_Wk_Date_Pnd_Wk_Dt_Cc", "#WK-DT-CC", FieldType.STRING, 2);
        pnd_Wk_Date_Pnd_Wk_Dt_Yy = pnd_Wk_Date__R_Field_33.newFieldInGroup("pnd_Wk_Date_Pnd_Wk_Dt_Yy", "#WK-DT-YY", FieldType.STRING, 2);
        pnd_Wk_Date_Pnd_Wk_Dt_Mm = pnd_Wk_Date__R_Field_33.newFieldInGroup("pnd_Wk_Date_Pnd_Wk_Dt_Mm", "#WK-DT-MM", FieldType.STRING, 2);
        pnd_Wk_Date_Pnd_Wk_Dt_Dd = pnd_Wk_Date__R_Field_33.newFieldInGroup("pnd_Wk_Date_Pnd_Wk_Dt_Dd", "#WK-DT-DD", FieldType.STRING, 2);

        pnd_Edit_Flags = localVariables.newGroupInRecord("pnd_Edit_Flags", "#EDIT-FLAGS");
        pnd_Edit_Flags_Pnd_Edit_Communication_Data = pnd_Edit_Flags.newFieldInGroup("pnd_Edit_Flags_Pnd_Edit_Communication_Data", "#EDIT-COMMUNICATION-DATA", 
            FieldType.BOOLEAN, 1);
        pnd_Edit_Flags_Pnd_Edit_Requestor_Data = pnd_Edit_Flags.newFieldInGroup("pnd_Edit_Flags_Pnd_Edit_Requestor_Data", "#EDIT-REQUESTOR-DATA", FieldType.BOOLEAN, 
            1);
        pnd_Edit_Flags_Pnd_Edit_Processing_Data = pnd_Edit_Flags.newFieldInGroup("pnd_Edit_Flags_Pnd_Edit_Processing_Data", "#EDIT-PROCESSING-DATA", FieldType.BOOLEAN, 
            1);
        pnd_Edit_Flags_Pnd_Edit_Contract_Data = pnd_Edit_Flags.newFieldInGroup("pnd_Edit_Flags_Pnd_Edit_Contract_Data", "#EDIT-CONTRACT-DATA", FieldType.BOOLEAN, 
            1);
        pnd_Edit_Flags_Pnd_Edit_Mit_Data = pnd_Edit_Flags.newFieldInGroup("pnd_Edit_Flags_Pnd_Edit_Mit_Data", "#EDIT-MIT-DATA", FieldType.BOOLEAN, 1);
        pnd_Edit_Flags_Pnd_Edit_Participant_Data = pnd_Edit_Flags.newFieldInGroup("pnd_Edit_Flags_Pnd_Edit_Participant_Data", "#EDIT-PARTICIPANT-DATA", 
            FieldType.BOOLEAN, 1);
        pnd_Edit_Flags_Pnd_Edit_Address_Data = pnd_Edit_Flags.newFieldInGroup("pnd_Edit_Flags_Pnd_Edit_Address_Data", "#EDIT-ADDRESS-DATA", FieldType.BOOLEAN, 
            1);
        pnd_Edit_Flags_Pnd_Edit_Allocation_Data = pnd_Edit_Flags.newFieldInGroup("pnd_Edit_Flags_Pnd_Edit_Allocation_Data", "#EDIT-ALLOCATION-DATA", FieldType.BOOLEAN, 
            1);
        pnd_Edit_Flags_Pnd_Edit_Beneficiary_Data = pnd_Edit_Flags.newFieldInGroup("pnd_Edit_Flags_Pnd_Edit_Beneficiary_Data", "#EDIT-BENEFICIARY-DATA", 
            FieldType.BOOLEAN, 1);
        pnd_Edit_Flags_Pnd_Edit_Financial_Data = pnd_Edit_Flags.newFieldInGroup("pnd_Edit_Flags_Pnd_Edit_Financial_Data", "#EDIT-FINANCIAL-DATA", FieldType.BOOLEAN, 
            1);
        pnd_W_Vesting_Code_N = localVariables.newFieldInRecord("pnd_W_Vesting_Code_N", "#W-VESTING-CODE-N", FieldType.NUMERIC, 1);

        pnd_W_Vesting_Code_N__R_Field_34 = localVariables.newGroupInRecord("pnd_W_Vesting_Code_N__R_Field_34", "REDEFINE", pnd_W_Vesting_Code_N);
        pnd_W_Vesting_Code_N_Pnd_W_Vesting_Code_A = pnd_W_Vesting_Code_N__R_Field_34.newFieldInGroup("pnd_W_Vesting_Code_N_Pnd_W_Vesting_Code_A", "#W-VESTING-CODE-A", 
            FieldType.STRING, 1);
        pnd_W_Lob = localVariables.newFieldInRecord("pnd_W_Lob", "#W-LOB", FieldType.STRING, 1);
        pnd_W_Lob_Type = localVariables.newFieldInRecord("pnd_W_Lob_Type", "#W-LOB-TYPE", FieldType.STRING, 1);
        pnd_W_Premium_Team_Cde = localVariables.newFieldInRecord("pnd_W_Premium_Team_Cde", "#W-PREMIUM-TEAM-CDE", FieldType.STRING, 8);
        pnd_W_Contract_Type = localVariables.newFieldInRecord("pnd_W_Contract_Type", "#W-CONTRACT-TYPE", FieldType.STRING, 12);
        pnd_W_Product_Code_N = localVariables.newFieldInRecord("pnd_W_Product_Code_N", "#W-PRODUCT-CODE-N", FieldType.NUMERIC, 3);

        pnd_W_Product_Code_N__R_Field_35 = localVariables.newGroupInRecord("pnd_W_Product_Code_N__R_Field_35", "REDEFINE", pnd_W_Product_Code_N);
        pnd_W_Product_Code_N_Pnd_W_Product_Code_A = pnd_W_Product_Code_N__R_Field_35.newFieldInGroup("pnd_W_Product_Code_N_Pnd_W_Product_Code_A", "#W-PRODUCT-CODE-A", 
            FieldType.STRING, 3);
        pnd_W_Allocation_Fmt = localVariables.newFieldInRecord("pnd_W_Allocation_Fmt", "#W-ALLOCATION-FMT", FieldType.STRING, 1);
        pnd_Hold_Sg_Product = localVariables.newFieldInRecord("pnd_Hold_Sg_Product", "#HOLD-SG-PRODUCT", FieldType.STRING, 3);
        pnd_Null = localVariables.newFieldInRecord("pnd_Null", "#NULL", FieldType.STRING, 4);
        pnd_Nbr = localVariables.newFieldInRecord("pnd_Nbr", "#NBR", FieldType.NUMERIC, 2);
        pnd_Scia9000_Field_3 = localVariables.newFieldInRecord("pnd_Scia9000_Field_3", "#SCIA9000-FIELD-3", FieldType.STRING, 16);

        pnd_Scia9000_Field_3__R_Field_36 = localVariables.newGroupInRecord("pnd_Scia9000_Field_3__R_Field_36", "REDEFINE", pnd_Scia9000_Field_3);
        pnd_Scia9000_Field_3_Pnd_Scia9000_Orig_Tiaa_Contract = pnd_Scia9000_Field_3__R_Field_36.newFieldInGroup("pnd_Scia9000_Field_3_Pnd_Scia9000_Orig_Tiaa_Contract", 
            "#SCIA9000-ORIG-TIAA-CONTRACT", FieldType.STRING, 8);
        pnd_Scia9000_Field_3_Pnd_Scia9000_Orig_Cref_Contract = pnd_Scia9000_Field_3__R_Field_36.newFieldInGroup("pnd_Scia9000_Field_3_Pnd_Scia9000_Orig_Cref_Contract", 
            "#SCIA9000-ORIG-CREF-CONTRACT", FieldType.STRING, 8);

        vw_fidelity_Ppg = new DataAccessProgramView(new NameInfo("vw_fidelity_Ppg", "FIDELITY-PPG"), "FIDELITY_PPG_PLAN_TABLE", "FDLTY_PPG_PLANS");
        fidelity_Ppg_Ppg = vw_fidelity_Ppg.getRecord().newFieldInGroup("fidelity_Ppg_Ppg", "PPG", FieldType.STRING, 6, RepeatingFieldStrategy.None, "PPG");
        fidelity_Ppg_Effective_Date = vw_fidelity_Ppg.getRecord().newFieldInGroup("fidelity_Ppg_Effective_Date", "EFFECTIVE-DATE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "EFFECTIVE_DATE");
        registerRecord(vw_fidelity_Ppg);

        vw_fidelity_Contract = new DataAccessProgramView(new NameInfo("vw_fidelity_Contract", "FIDELITY-CONTRACT"), "FIDELITY_CONTRACT_FILE", "FDLTY_CNTRC_FILE");
        fidelity_Contract_Plan_Num = vw_fidelity_Contract.getRecord().newFieldInGroup("fidelity_Contract_Plan_Num", "PLAN-NUM", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "PLAN_NUM");
        fidelity_Contract_Ph_Ssn = vw_fidelity_Contract.getRecord().newFieldInGroup("fidelity_Contract_Ph_Ssn", "PH-SSN", FieldType.STRING, 9, RepeatingFieldStrategy.None, 
            "PH_SSN");
        fidelity_Contract_Ph_Tiaa_Contract = vw_fidelity_Contract.getRecord().newFieldInGroup("fidelity_Contract_Ph_Tiaa_Contract", "PH-TIAA-CONTRACT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "PH_TIAA_CONTRACT");
        fidelity_Contract_Ph_Cref_Contract = vw_fidelity_Contract.getRecord().newFieldInGroup("fidelity_Contract_Ph_Cref_Contract", "PH-CREF-CONTRACT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "PH_CREF_CONTRACT");
        fidelity_Contract_Issue_Date = vw_fidelity_Contract.getRecord().newFieldInGroup("fidelity_Contract_Issue_Date", "ISSUE-DATE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ISSUE_DATE");
        fidelity_Contract_Sg_Plan_No = vw_fidelity_Contract.getRecord().newFieldInGroup("fidelity_Contract_Sg_Plan_No", "SG-PLAN-NO", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "SG_PLAN_NO");
        fidelity_Contract_Sg_Subplan_No = vw_fidelity_Contract.getRecord().newFieldInGroup("fidelity_Contract_Sg_Subplan_No", "SG-SUBPLAN-NO", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "SG_SUBPLAN_NO");
        fidelity_Contract_Sg_Div_Sub = vw_fidelity_Contract.getRecord().newFieldInGroup("fidelity_Contract_Sg_Div_Sub", "SG-DIV-SUB", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "SG_DIV_SUB");
        registerRecord(vw_fidelity_Contract);

        pnd_Pnum_Ssn = localVariables.newFieldInRecord("pnd_Pnum_Ssn", "#PNUM-SSN", FieldType.STRING, 14);

        pnd_Pnum_Ssn__R_Field_37 = localVariables.newGroupInRecord("pnd_Pnum_Ssn__R_Field_37", "REDEFINE", pnd_Pnum_Ssn);
        pnd_Pnum_Ssn_Pnd_Plan_Num = pnd_Pnum_Ssn__R_Field_37.newFieldInGroup("pnd_Pnum_Ssn_Pnd_Plan_Num", "#PLAN-NUM", FieldType.STRING, 5);
        pnd_Pnum_Ssn_Pnd_Ph_Ssn = pnd_Pnum_Ssn__R_Field_37.newFieldInGroup("pnd_Pnum_Ssn_Pnd_Ph_Ssn", "#PH-SSN", FieldType.STRING, 9);
        pnd_Hold_Ap_Process = localVariables.newFieldInRecord("pnd_Hold_Ap_Process", "#HOLD-AP-PROCESS", FieldType.STRING, 4);
        pnd_Hold_Ap_Status = localVariables.newFieldInRecord("pnd_Hold_Ap_Status", "#HOLD-AP-STATUS", FieldType.STRING, 1);
        pnd_Hold_Premium_Team_Cde = localVariables.newFieldInRecord("pnd_Hold_Premium_Team_Cde", "#HOLD-PREMIUM-TEAM-CDE", FieldType.STRING, 2);
        pnd_Hold_Bucket_Cde = localVariables.newFieldInRecord("pnd_Hold_Bucket_Cde", "#HOLD-BUCKET-CDE", FieldType.STRING, 2);
        pnd_Hold_Accum_Cde = localVariables.newFieldInRecord("pnd_Hold_Accum_Cde", "#HOLD-ACCUM-CDE", FieldType.STRING, 2);
        pnd_Entry_Dscrptn_Text = localVariables.newFieldInRecord("pnd_Entry_Dscrptn_Text", "#ENTRY-DSCRPTN-TEXT", FieldType.STRING, 60);

        pnd_Entry_Dscrptn_Text__R_Field_38 = localVariables.newGroupInRecord("pnd_Entry_Dscrptn_Text__R_Field_38", "REDEFINE", pnd_Entry_Dscrptn_Text);
        pnd_Entry_Dscrptn_Text_Pnd_Entry_Bucket = pnd_Entry_Dscrptn_Text__R_Field_38.newFieldInGroup("pnd_Entry_Dscrptn_Text_Pnd_Entry_Bucket", "#ENTRY-BUCKET", 
            FieldType.STRING, 2);
        pnd_Entry_Dscrptn_Text_Pnd_Entry_Accum = pnd_Entry_Dscrptn_Text__R_Field_38.newFieldInGroup("pnd_Entry_Dscrptn_Text_Pnd_Entry_Accum", "#ENTRY-ACCUM", 
            FieldType.STRING, 2);
        pnd_Entry_Dscrptn_Text_Pnd_Entry_Prap_Team = pnd_Entry_Dscrptn_Text__R_Field_38.newFieldInGroup("pnd_Entry_Dscrptn_Text_Pnd_Entry_Prap_Team", 
            "#ENTRY-PRAP-TEAM", FieldType.STRING, 2);
        pnd_Map_Ppg_Code = localVariables.newFieldInRecord("pnd_Map_Ppg_Code", "#MAP-PPG-CODE", FieldType.STRING, 4);
        pnd_Map_Premium_Team_Code = localVariables.newFieldInRecord("pnd_Map_Premium_Team_Code", "#MAP-PREMIUM-TEAM-CODE", FieldType.STRING, 8);
        pnd_Datn = localVariables.newFieldInRecord("pnd_Datn", "#DATN", FieldType.NUMERIC, 8);
        pnd_Retry_Count = localVariables.newFieldInRecord("pnd_Retry_Count", "#RETRY-COUNT", FieldType.NUMERIC, 4);
        pnd_Lob_Type_For_Prap = localVariables.newFieldInRecord("pnd_Lob_Type_For_Prap", "#LOB-TYPE-FOR-PRAP", FieldType.STRING, 1);
        pnd_Date_Mmddyy = localVariables.newFieldInRecord("pnd_Date_Mmddyy", "#DATE-MMDDYY", FieldType.DATE);
        pnd_Date_Mmddyy_A = localVariables.newFieldInRecord("pnd_Date_Mmddyy_A", "#DATE-MMDDYY-A", FieldType.STRING, 6);

        pnd_Date_Mmddyy_A__R_Field_39 = localVariables.newGroupInRecord("pnd_Date_Mmddyy_A__R_Field_39", "REDEFINE", pnd_Date_Mmddyy_A);
        pnd_Date_Mmddyy_A_Pnd_Date_Mmddyy_N = pnd_Date_Mmddyy_A__R_Field_39.newFieldInGroup("pnd_Date_Mmddyy_A_Pnd_Date_Mmddyy_N", "#DATE-MMDDYY-N", FieldType.NUMERIC, 
            6);
        hold_T_Cont = localVariables.newFieldInRecord("hold_T_Cont", "HOLD-T-CONT", FieldType.STRING, 8);

        hold_T_Cont__R_Field_40 = localVariables.newGroupInRecord("hold_T_Cont__R_Field_40", "REDEFINE", hold_T_Cont);
        hold_T_Cont_Hold_App_Pref = hold_T_Cont__R_Field_40.newFieldInGroup("hold_T_Cont_Hold_App_Pref", "HOLD-APP-PREF", FieldType.STRING, 1);
        hold_T_Cont_Hold_App_Cont = hold_T_Cont__R_Field_40.newFieldInGroup("hold_T_Cont_Hold_App_Cont", "HOLD-APP-CONT", FieldType.STRING, 7);

        hold_T_Cont__R_Field_41 = hold_T_Cont__R_Field_40.newGroupInGroup("hold_T_Cont__R_Field_41", "REDEFINE", hold_T_Cont_Hold_App_Cont);
        hold_T_Cont_Hold_App_Cont_0 = hold_T_Cont__R_Field_41.newFieldInGroup("hold_T_Cont_Hold_App_Cont_0", "HOLD-APP-CONT-0", FieldType.STRING, 6);
        hold_C_Cont = localVariables.newFieldInRecord("hold_C_Cont", "HOLD-C-CONT", FieldType.STRING, 8);

        hold_C_Cont__R_Field_42 = localVariables.newGroupInRecord("hold_C_Cont__R_Field_42", "REDEFINE", hold_C_Cont);
        hold_C_Cont_Hold_App_C_Pref = hold_C_Cont__R_Field_42.newFieldInGroup("hold_C_Cont_Hold_App_C_Pref", "HOLD-APP-C-PREF", FieldType.STRING, 1);
        hold_C_Cont_Hold_App_C_Cont = hold_C_Cont__R_Field_42.newFieldInGroup("hold_C_Cont_Hold_App_C_Cont", "HOLD-APP-C-CONT", FieldType.STRING, 7);
        pnd_New_Contract = localVariables.newFieldInRecord("pnd_New_Contract", "#NEW-CONTRACT", FieldType.STRING, 10);

        pnd_New_Contract__R_Field_43 = localVariables.newGroupInRecord("pnd_New_Contract__R_Field_43", "REDEFINE", pnd_New_Contract);
        pnd_New_Contract_Pnd_New_Pref_Ed = pnd_New_Contract__R_Field_43.newFieldInGroup("pnd_New_Contract_Pnd_New_Pref_Ed", "#NEW-PREF-ED", FieldType.STRING, 
            1);
        pnd_New_Contract_Pnd_Dash = pnd_New_Contract__R_Field_43.newFieldInGroup("pnd_New_Contract_Pnd_Dash", "#DASH", FieldType.STRING, 1);
        pnd_New_Contract_Pnd_New_Cont_Ed = pnd_New_Contract__R_Field_43.newFieldInGroup("pnd_New_Contract_Pnd_New_Cont_Ed", "#NEW-CONT-ED", FieldType.STRING, 
            7);
        pnd_Compare_Index = localVariables.newFieldInRecord("pnd_Compare_Index", "#COMPARE-INDEX", FieldType.NUMERIC, 2);

        pnd_Compare_Index__R_Field_44 = localVariables.newGroupInRecord("pnd_Compare_Index__R_Field_44", "REDEFINE", pnd_Compare_Index);
        pnd_Compare_Index_Pnd_Compare_Index_A = pnd_Compare_Index__R_Field_44.newFieldInGroup("pnd_Compare_Index_Pnd_Compare_Index_A", "#COMPARE-INDEX-A", 
            FieldType.STRING, 2);
        pnd_Hold_T_Pref = localVariables.newFieldInRecord("pnd_Hold_T_Pref", "#HOLD-T-PREF", FieldType.STRING, 1);
        pnd_Hold_C_Pref = localVariables.newFieldInRecord("pnd_Hold_C_Pref", "#HOLD-C-PREF", FieldType.STRING, 1);
        pnd_New_Object = localVariables.newFieldInRecord("pnd_New_Object", "#NEW-OBJECT", FieldType.BOOLEAN, 1);

        pnd_Scroll_Vars = localVariables.newGroupInRecord("pnd_Scroll_Vars", "#SCROLL-VARS");
        pnd_Scroll_Vars_Pnd_Panel = pnd_Scroll_Vars.newFieldInGroup("pnd_Scroll_Vars_Pnd_Panel", "#PANEL", FieldType.NUMERIC, 7);
        pnd_Displayed_Key = localVariables.newFieldInRecord("pnd_Displayed_Key", "#DISPLAYED-KEY", FieldType.STRING, 31);
        hold_Control_Key = localVariables.newFieldInRecord("hold_Control_Key", "HOLD-CONTROL-KEY", FieldType.STRING, 7);

        hold_Control_Key__R_Field_45 = localVariables.newGroupInRecord("hold_Control_Key__R_Field_45", "REDEFINE", hold_Control_Key);
        hold_Control_Key_Hold_Rectyp_C = hold_Control_Key__R_Field_45.newFieldInGroup("hold_Control_Key_Hold_Rectyp_C", "HOLD-RECTYP-C", FieldType.STRING, 
            1);
        hold_Control_Key_Hold_Recip_Date = hold_Control_Key__R_Field_45.newFieldInGroup("hold_Control_Key_Hold_Recip_Date", "HOLD-RECIP-DATE", FieldType.NUMERIC, 
            6);
        pnd_Contract_No = localVariables.newFieldInRecord("pnd_Contract_No", "#CONTRACT-NO", FieldType.STRING, 10);

        pnd_Contract_No__R_Field_46 = localVariables.newGroupInRecord("pnd_Contract_No__R_Field_46", "REDEFINE", pnd_Contract_No);
        pnd_Contract_No_Pnd_Contract_No_1_1 = pnd_Contract_No__R_Field_46.newFieldInGroup("pnd_Contract_No_Pnd_Contract_No_1_1", "#CONTRACT-NO-1-1", FieldType.STRING, 
            1);
        pnd_Contract_No_Pnd_Contract_No_2_8 = pnd_Contract_No__R_Field_46.newFieldInGroup("pnd_Contract_No_Pnd_Contract_No_2_8", "#CONTRACT-NO-2-8", FieldType.STRING, 
            7);
        pnd_Hold_Date_Mmddyyyy = localVariables.newFieldInRecord("pnd_Hold_Date_Mmddyyyy", "#HOLD-DATE-MMDDYYYY", FieldType.NUMERIC, 8);

        pnd_Hold_Date_Mmddyyyy__R_Field_47 = localVariables.newGroupInRecord("pnd_Hold_Date_Mmddyyyy__R_Field_47", "REDEFINE", pnd_Hold_Date_Mmddyyyy);
        pnd_Hold_Date_Mmddyyyy_Pnd_Hold_Date_Mmddyy = pnd_Hold_Date_Mmddyyyy__R_Field_47.newFieldInGroup("pnd_Hold_Date_Mmddyyyy_Pnd_Hold_Date_Mmddyy", 
            "#HOLD-DATE-MMDDYY", FieldType.NUMERIC, 6);

        pnd_Hold_Date_Mmddyyyy__R_Field_48 = localVariables.newGroupInRecord("pnd_Hold_Date_Mmddyyyy__R_Field_48", "REDEFINE", pnd_Hold_Date_Mmddyyyy);
        pnd_Hold_Date_Mmddyyyy__Filler1 = pnd_Hold_Date_Mmddyyyy__R_Field_48.newFieldInGroup("pnd_Hold_Date_Mmddyyyy__Filler1", "_FILLER1", FieldType.STRING, 
            4);
        pnd_Hold_Date_Mmddyyyy_Pnd_19 = pnd_Hold_Date_Mmddyyyy__R_Field_48.newFieldInGroup("pnd_Hold_Date_Mmddyyyy_Pnd_19", "#19", FieldType.NUMERIC, 
            2);
        pnd_Hold_Date_Mmddyyyy_Pnd_Yy = pnd_Hold_Date_Mmddyyyy__R_Field_48.newFieldInGroup("pnd_Hold_Date_Mmddyyyy_Pnd_Yy", "#YY", FieldType.NUMERIC, 
            2);
        hold_Date_Mmddyy = localVariables.newFieldInRecord("hold_Date_Mmddyy", "HOLD-DATE-MMDDYY", FieldType.NUMERIC, 6);

        hold_Date_Mmddyy__R_Field_49 = localVariables.newGroupInRecord("hold_Date_Mmddyy__R_Field_49", "REDEFINE", hold_Date_Mmddyy);
        hold_Date_Mmddyy_Hold_Mm = hold_Date_Mmddyy__R_Field_49.newFieldInGroup("hold_Date_Mmddyy_Hold_Mm", "HOLD-MM", FieldType.NUMERIC, 2);
        hold_Date_Mmddyy_Hold_Dd = hold_Date_Mmddyy__R_Field_49.newFieldInGroup("hold_Date_Mmddyy_Hold_Dd", "HOLD-DD", FieldType.NUMERIC, 2);
        hold_Date_Mmddyy_Hold_Yy = hold_Date_Mmddyy__R_Field_49.newFieldInGroup("hold_Date_Mmddyy_Hold_Yy", "HOLD-YY", FieldType.NUMERIC, 2);

        hold_Date_Mmddyy__R_Field_50 = localVariables.newGroupInRecord("hold_Date_Mmddyy__R_Field_50", "REDEFINE", hold_Date_Mmddyy);
        hold_Date_Mmddyy_Hold_Datemmdd = hold_Date_Mmddyy__R_Field_50.newFieldInGroup("hold_Date_Mmddyy_Hold_Datemmdd", "HOLD-DATEMMDD", FieldType.NUMERIC, 
            4);

        hold_Date_Mmddyy__R_Field_51 = localVariables.newGroupInRecord("hold_Date_Mmddyy__R_Field_51", "REDEFINE", hold_Date_Mmddyy);
        hold_Date_Mmddyy_Hold_Datemm = hold_Date_Mmddyy__R_Field_51.newFieldInGroup("hold_Date_Mmddyy_Hold_Datemm", "HOLD-DATEMM", FieldType.NUMERIC, 
            2);
        hold_Date_Mmddyy_Hold_Date_Ddyy = hold_Date_Mmddyy__R_Field_51.newFieldInGroup("hold_Date_Mmddyy_Hold_Date_Ddyy", "HOLD-DATE-DDYY", FieldType.NUMERIC, 
            4);

        hold_Date_Mmddyy__R_Field_52 = hold_Date_Mmddyy__R_Field_51.newGroupInGroup("hold_Date_Mmddyy__R_Field_52", "REDEFINE", hold_Date_Mmddyy_Hold_Date_Ddyy);
        hold_Date_Mmddyy_Hold_Datedd = hold_Date_Mmddyy__R_Field_52.newFieldInGroup("hold_Date_Mmddyy_Hold_Datedd", "HOLD-DATEDD", FieldType.NUMERIC, 
            2);
        hold_Date_Mmddyy_Hold_Dateyy = hold_Date_Mmddyy__R_Field_52.newFieldInGroup("hold_Date_Mmddyy_Hold_Dateyy", "HOLD-DATEYY", FieldType.NUMERIC, 
            2);
        hold_Mmyy = localVariables.newFieldInRecord("hold_Mmyy", "HOLD-MMYY", FieldType.NUMERIC, 4);

        hold_Mmyy__R_Field_53 = localVariables.newGroupInRecord("hold_Mmyy__R_Field_53", "REDEFINE", hold_Mmyy);
        hold_Mmyy_Pnd_Hold_Mm = hold_Mmyy__R_Field_53.newFieldInGroup("hold_Mmyy_Pnd_Hold_Mm", "#HOLD-MM", FieldType.NUMERIC, 2);
        hold_Mmyy_Pnd_Hold_Yy = hold_Mmyy__R_Field_53.newFieldInGroup("hold_Mmyy_Pnd_Hold_Yy", "#HOLD-YY", FieldType.NUMERIC, 2);
        hold_Curr = localVariables.newFieldInRecord("hold_Curr", "HOLD-CURR", FieldType.STRING, 1);

        hold_Curr__R_Field_54 = localVariables.newGroupInRecord("hold_Curr__R_Field_54", "REDEFINE", hold_Curr);
        hold_Curr_Hold_Curr_N = hold_Curr__R_Field_54.newFieldInGroup("hold_Curr_Hold_Curr_N", "HOLD-CURR-N", FieldType.NUMERIC, 1);
        hold_Date18 = localVariables.newFieldInRecord("hold_Date18", "HOLD-DATE18", FieldType.NUMERIC, 8);

        hold_Date18__R_Field_55 = localVariables.newGroupInRecord("hold_Date18__R_Field_55", "REDEFINE", hold_Date18);
        hold_Date18_Hold_Date12 = hold_Date18__R_Field_55.newFieldInGroup("hold_Date18_Hold_Date12", "HOLD-DATE12", FieldType.NUMERIC, 2);
        hold_Date18_Hold_Date38 = hold_Date18__R_Field_55.newFieldInGroup("hold_Date18_Hold_Date38", "HOLD-DATE38", FieldType.NUMERIC, 6);

        hold_Date18__R_Field_56 = hold_Date18__R_Field_55.newGroupInGroup("hold_Date18__R_Field_56", "REDEFINE", hold_Date18_Hold_Date38);
        hold_Date18_Hold_Date34 = hold_Date18__R_Field_56.newFieldInGroup("hold_Date18_Hold_Date34", "HOLD-DATE34", FieldType.NUMERIC, 2);
        hold_Date18_Hold_Date58 = hold_Date18__R_Field_56.newFieldInGroup("hold_Date18_Hold_Date58", "HOLD-DATE58", FieldType.NUMERIC, 4);

        hold_Date18__R_Field_57 = hold_Date18__R_Field_56.newGroupInGroup("hold_Date18__R_Field_57", "REDEFINE", hold_Date18_Hold_Date58);
        hold_Date18_Hold_Date56 = hold_Date18__R_Field_57.newFieldInGroup("hold_Date18_Hold_Date56", "HOLD-DATE56", FieldType.NUMERIC, 2);
        hold_Date18_Hold_Date78 = hold_Date18__R_Field_57.newFieldInGroup("hold_Date18_Hold_Date78", "HOLD-DATE78", FieldType.NUMERIC, 2);
        pnd_Aat_Currency_N = localVariables.newFieldInRecord("pnd_Aat_Currency_N", "#AAT-CURRENCY-N", FieldType.NUMERIC, 1);

        pnd_Aat_Currency_N__R_Field_58 = localVariables.newGroupInRecord("pnd_Aat_Currency_N__R_Field_58", "REDEFINE", pnd_Aat_Currency_N);
        pnd_Aat_Currency_N_Pnd_Aat_Currency_A = pnd_Aat_Currency_N__R_Field_58.newFieldInGroup("pnd_Aat_Currency_N_Pnd_Aat_Currency_A", "#AAT-CURRENCY-A", 
            FieldType.STRING, 1);
        pnd_Current_Date = localVariables.newFieldInRecord("pnd_Current_Date", "#CURRENT-DATE", FieldType.NUMERIC, 8);

        pnd_Current_Date__R_Field_59 = localVariables.newGroupInRecord("pnd_Current_Date__R_Field_59", "REDEFINE", pnd_Current_Date);
        pnd_Current_Date__Filler2 = pnd_Current_Date__R_Field_59.newFieldInGroup("pnd_Current_Date__Filler2", "_FILLER2", FieldType.STRING, 2);
        pnd_Current_Date_Pnd_Cur_Mmdd = pnd_Current_Date__R_Field_59.newFieldInGroup("pnd_Current_Date_Pnd_Cur_Mmdd", "#CUR-MMDD", FieldType.NUMERIC, 
            4);
        pnd_Current_Date_Pnd_Cur_Yy = pnd_Current_Date__R_Field_59.newFieldInGroup("pnd_Current_Date_Pnd_Cur_Yy", "#CUR-YY", FieldType.NUMERIC, 2);
        pnd_System_Date = localVariables.newFieldInRecord("pnd_System_Date", "#SYSTEM-DATE", FieldType.NUMERIC, 8);

        pnd_System_Date__R_Field_60 = localVariables.newGroupInRecord("pnd_System_Date__R_Field_60", "REDEFINE", pnd_System_Date);
        pnd_System_Date_Pnd_Sys_Year = pnd_System_Date__R_Field_60.newFieldInGroup("pnd_System_Date_Pnd_Sys_Year", "#SYS-YEAR", FieldType.NUMERIC, 4);

        pnd_System_Date__R_Field_61 = pnd_System_Date__R_Field_60.newGroupInGroup("pnd_System_Date__R_Field_61", "REDEFINE", pnd_System_Date_Pnd_Sys_Year);
        pnd_System_Date__Filler3 = pnd_System_Date__R_Field_61.newFieldInGroup("pnd_System_Date__Filler3", "_FILLER3", FieldType.STRING, 2);
        pnd_System_Date_Pnd_Sys_Yy = pnd_System_Date__R_Field_61.newFieldInGroup("pnd_System_Date_Pnd_Sys_Yy", "#SYS-YY", FieldType.NUMERIC, 2);
        pnd_System_Date_Pnd_Sys_Mmdd = pnd_System_Date__R_Field_60.newFieldInGroup("pnd_System_Date_Pnd_Sys_Mmdd", "#SYS-MMDD", FieldType.NUMERIC, 4);
        pnd_System_Temp_Date = localVariables.newFieldInRecord("pnd_System_Temp_Date", "#SYSTEM-TEMP-DATE", FieldType.NUMERIC, 4);
        pnd_Ap_Cor_Ind = localVariables.newFieldInRecord("pnd_Ap_Cor_Ind", "#AP-COR-IND", FieldType.STRING, 1);
        pnd_Prem_Mit_Log_Dte_Tme = localVariables.newFieldInRecord("pnd_Prem_Mit_Log_Dte_Tme", "#PREM-MIT-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Policy_Holder = localVariables.newFieldInRecord("pnd_Policy_Holder", "#POLICY-HOLDER", FieldType.STRING, 1);
        pnd_Hold_Lob_Type_Full = localVariables.newFieldInRecord("pnd_Hold_Lob_Type_Full", "#HOLD-LOB-TYPE-FULL", FieldType.STRING, 2);

        pnd_Hold_Lob_Type_Full__R_Field_62 = localVariables.newGroupInRecord("pnd_Hold_Lob_Type_Full__R_Field_62", "REDEFINE", pnd_Hold_Lob_Type_Full);
        pnd_Hold_Lob_Type_Full_Pnd_Hold_Lob = pnd_Hold_Lob_Type_Full__R_Field_62.newFieldInGroup("pnd_Hold_Lob_Type_Full_Pnd_Hold_Lob", "#HOLD-LOB", FieldType.STRING, 
            1);
        pnd_Hold_Lob_Type_Full_Pnd_Hold_Lob_Type = pnd_Hold_Lob_Type_Full__R_Field_62.newFieldInGroup("pnd_Hold_Lob_Type_Full_Pnd_Hold_Lob_Type", "#HOLD-LOB-TYPE", 
            FieldType.STRING, 1);
        pnd_Rltnshp_Cde = localVariables.newFieldInRecord("pnd_Rltnshp_Cde", "#RLTNSHP-CDE", FieldType.STRING, 1);
        pnd_Hold2_Dt = localVariables.newFieldInRecord("pnd_Hold2_Dt", "#HOLD2-DT", FieldType.NUMERIC, 8);

        pnd_Hold2_Dt__R_Field_63 = localVariables.newGroupInRecord("pnd_Hold2_Dt__R_Field_63", "REDEFINE", pnd_Hold2_Dt);
        pnd_Hold2_Dt_Pnd_Hold2_Dt_Cc = pnd_Hold2_Dt__R_Field_63.newFieldInGroup("pnd_Hold2_Dt_Pnd_Hold2_Dt_Cc", "#HOLD2-DT-CC", FieldType.NUMERIC, 2);
        pnd_Hold2_Dt_Pnd_Hold2_Dt_Yy = pnd_Hold2_Dt__R_Field_63.newFieldInGroup("pnd_Hold2_Dt_Pnd_Hold2_Dt_Yy", "#HOLD2-DT-YY", FieldType.NUMERIC, 2);
        pnd_Hold2_Dt_Pnd_Hold2_Dt_Mm = pnd_Hold2_Dt__R_Field_63.newFieldInGroup("pnd_Hold2_Dt_Pnd_Hold2_Dt_Mm", "#HOLD2-DT-MM", FieldType.NUMERIC, 2);
        pnd_Hold2_Dt_Pnd_Hold2_Dt_Dd = pnd_Hold2_Dt__R_Field_63.newFieldInGroup("pnd_Hold2_Dt_Pnd_Hold2_Dt_Dd", "#HOLD2-DT-DD", FieldType.NUMERIC, 2);
        pnd_Hold_Dt = localVariables.newFieldInRecord("pnd_Hold_Dt", "#HOLD-DT", FieldType.NUMERIC, 8);

        pnd_Hold_Dt__R_Field_64 = localVariables.newGroupInRecord("pnd_Hold_Dt__R_Field_64", "REDEFINE", pnd_Hold_Dt);
        pnd_Hold_Dt_Pnd_Hold_Dt_Mm = pnd_Hold_Dt__R_Field_64.newFieldInGroup("pnd_Hold_Dt_Pnd_Hold_Dt_Mm", "#HOLD-DT-MM", FieldType.NUMERIC, 2);
        pnd_Hold_Dt_Pnd_Hold_Dt_Dd = pnd_Hold_Dt__R_Field_64.newFieldInGroup("pnd_Hold_Dt_Pnd_Hold_Dt_Dd", "#HOLD-DT-DD", FieldType.NUMERIC, 2);
        pnd_Hold_Dt_Pnd_Hold_Dt_Cc = pnd_Hold_Dt__R_Field_64.newFieldInGroup("pnd_Hold_Dt_Pnd_Hold_Dt_Cc", "#HOLD-DT-CC", FieldType.NUMERIC, 2);
        pnd_Hold_Dt_Pnd_Hold_Dt_Yy = pnd_Hold_Dt__R_Field_64.newFieldInGroup("pnd_Hold_Dt_Pnd_Hold_Dt_Yy", "#HOLD-DT-YY", FieldType.NUMERIC, 2);
        pnd_Cor_Dob = localVariables.newFieldInRecord("pnd_Cor_Dob", "#COR-DOB", FieldType.NUMERIC, 8);

        pnd_Cor_Dob__R_Field_65 = localVariables.newGroupInRecord("pnd_Cor_Dob__R_Field_65", "REDEFINE", pnd_Cor_Dob);
        pnd_Cor_Dob_Pnd_Cor_Dob_Cc = pnd_Cor_Dob__R_Field_65.newFieldInGroup("pnd_Cor_Dob_Pnd_Cor_Dob_Cc", "#COR-DOB-CC", FieldType.NUMERIC, 2);
        pnd_Cor_Dob_Pnd_Cor_Dob_Yy = pnd_Cor_Dob__R_Field_65.newFieldInGroup("pnd_Cor_Dob_Pnd_Cor_Dob_Yy", "#COR-DOB-YY", FieldType.NUMERIC, 2);
        pnd_Cor_Dob_Pnd_Cor_Dob_Mm = pnd_Cor_Dob__R_Field_65.newFieldInGroup("pnd_Cor_Dob_Pnd_Cor_Dob_Mm", "#COR-DOB-MM", FieldType.NUMERIC, 2);
        pnd_Cor_Dob_Pnd_Cor_Dob_Dd = pnd_Cor_Dob__R_Field_65.newFieldInGroup("pnd_Cor_Dob_Pnd_Cor_Dob_Dd", "#COR-DOB-DD", FieldType.NUMERIC, 2);
        pnd_Cor_T_Doi = localVariables.newFieldInRecord("pnd_Cor_T_Doi", "#COR-T-DOI", FieldType.NUMERIC, 8);

        pnd_Cor_T_Doi__R_Field_66 = localVariables.newGroupInRecord("pnd_Cor_T_Doi__R_Field_66", "REDEFINE", pnd_Cor_T_Doi);
        pnd_Cor_T_Doi_Pnd_Cor_T_Doi_Cc = pnd_Cor_T_Doi__R_Field_66.newFieldInGroup("pnd_Cor_T_Doi_Pnd_Cor_T_Doi_Cc", "#COR-T-DOI-CC", FieldType.NUMERIC, 
            2);
        pnd_Cor_T_Doi_Pnd_Cor_T_Doi_Yy = pnd_Cor_T_Doi__R_Field_66.newFieldInGroup("pnd_Cor_T_Doi_Pnd_Cor_T_Doi_Yy", "#COR-T-DOI-YY", FieldType.NUMERIC, 
            2);
        pnd_Cor_T_Doi_Pnd_Cor_T_Doi_Mm = pnd_Cor_T_Doi__R_Field_66.newFieldInGroup("pnd_Cor_T_Doi_Pnd_Cor_T_Doi_Mm", "#COR-T-DOI-MM", FieldType.NUMERIC, 
            2);
        pnd_Cor_T_Doi_Pnd_Cor_T_Doi_Dd = pnd_Cor_T_Doi__R_Field_66.newFieldInGroup("pnd_Cor_T_Doi_Pnd_Cor_T_Doi_Dd", "#COR-T-DOI-DD", FieldType.NUMERIC, 
            2);
        pnd_Cor_C_Doi = localVariables.newFieldInRecord("pnd_Cor_C_Doi", "#COR-C-DOI", FieldType.NUMERIC, 8);

        pnd_Cor_C_Doi__R_Field_67 = localVariables.newGroupInRecord("pnd_Cor_C_Doi__R_Field_67", "REDEFINE", pnd_Cor_C_Doi);
        pnd_Cor_C_Doi_Pnd_Cor_C_Doi_Cc = pnd_Cor_C_Doi__R_Field_67.newFieldInGroup("pnd_Cor_C_Doi_Pnd_Cor_C_Doi_Cc", "#COR-C-DOI-CC", FieldType.NUMERIC, 
            2);
        pnd_Cor_C_Doi_Pnd_Cor_C_Doi_Yy = pnd_Cor_C_Doi__R_Field_67.newFieldInGroup("pnd_Cor_C_Doi_Pnd_Cor_C_Doi_Yy", "#COR-C-DOI-YY", FieldType.NUMERIC, 
            2);
        pnd_Cor_C_Doi_Pnd_Cor_C_Doi_Mm = pnd_Cor_C_Doi__R_Field_67.newFieldInGroup("pnd_Cor_C_Doi_Pnd_Cor_C_Doi_Mm", "#COR-C-DOI-MM", FieldType.NUMERIC, 
            2);
        pnd_Cor_C_Doi_Pnd_Cor_C_Doi_Dd = pnd_Cor_C_Doi__R_Field_67.newFieldInGroup("pnd_Cor_C_Doi_Pnd_Cor_C_Doi_Dd", "#COR-C-DOI-DD", FieldType.NUMERIC, 
            2);
        pnd_Hold_Dob = localVariables.newFieldInRecord("pnd_Hold_Dob", "#HOLD-DOB", FieldType.NUMERIC, 8);

        pnd_Hold_Dob__R_Field_68 = localVariables.newGroupInRecord("pnd_Hold_Dob__R_Field_68", "REDEFINE", pnd_Hold_Dob);
        pnd_Hold_Dob_Pnd_Hold_Dob_Mm = pnd_Hold_Dob__R_Field_68.newFieldInGroup("pnd_Hold_Dob_Pnd_Hold_Dob_Mm", "#HOLD-DOB-MM", FieldType.NUMERIC, 2);
        pnd_Hold_Dob_Pnd_Hold_Dob_Dd = pnd_Hold_Dob__R_Field_68.newFieldInGroup("pnd_Hold_Dob_Pnd_Hold_Dob_Dd", "#HOLD-DOB-DD", FieldType.NUMERIC, 2);
        pnd_Hold_Dob_Pnd_Hold_Dob_Cc = pnd_Hold_Dob__R_Field_68.newFieldInGroup("pnd_Hold_Dob_Pnd_Hold_Dob_Cc", "#HOLD-DOB-CC", FieldType.NUMERIC, 2);
        pnd_Hold_Dob_Pnd_Hold_Dob_Yy = pnd_Hold_Dob__R_Field_68.newFieldInGroup("pnd_Hold_Dob_Pnd_Hold_Dob_Yy", "#HOLD-DOB-YY", FieldType.NUMERIC, 2);
        pnd_Hold_Doi = localVariables.newFieldInRecord("pnd_Hold_Doi", "#HOLD-DOI", FieldType.NUMERIC, 6);

        pnd_Hold_Doi__R_Field_69 = localVariables.newGroupInRecord("pnd_Hold_Doi__R_Field_69", "REDEFINE", pnd_Hold_Doi);
        pnd_Hold_Doi_Pnd_Hold_Doi_Dd = pnd_Hold_Doi__R_Field_69.newFieldInGroup("pnd_Hold_Doi_Pnd_Hold_Doi_Dd", "#HOLD-DOI-DD", FieldType.NUMERIC, 2);
        pnd_Hold_Doi_Pnd_Hold_Doi_Mm = pnd_Hold_Doi__R_Field_69.newFieldInGroup("pnd_Hold_Doi_Pnd_Hold_Doi_Mm", "#HOLD-DOI-MM", FieldType.NUMERIC, 2);
        pnd_Hold_Doi_Pnd_Hold_Doi_Yy = pnd_Hold_Doi__R_Field_69.newFieldInGroup("pnd_Hold_Doi_Pnd_Hold_Doi_Yy", "#HOLD-DOI-YY", FieldType.NUMERIC, 2);
        pnd_Cor_T_Contr_A = localVariables.newFieldInRecord("pnd_Cor_T_Contr_A", "#COR-T-CONTR-A", FieldType.STRING, 10);

        pnd_Cor_T_Contr_A__R_Field_70 = localVariables.newGroupInRecord("pnd_Cor_T_Contr_A__R_Field_70", "REDEFINE", pnd_Cor_T_Contr_A);
        pnd_Cor_T_Contr_A_Pnd_Cor_T_Prefx = pnd_Cor_T_Contr_A__R_Field_70.newFieldInGroup("pnd_Cor_T_Contr_A_Pnd_Cor_T_Prefx", "#COR-T-PREFX", FieldType.STRING, 
            1);
        pnd_Cor_T_Contr_A_Pnd_Cor_T_Numbr = pnd_Cor_T_Contr_A__R_Field_70.newFieldInGroup("pnd_Cor_T_Contr_A_Pnd_Cor_T_Numbr", "#COR-T-NUMBR", FieldType.STRING, 
            7);
        pnd_Cor_C_Contr_A = localVariables.newFieldInRecord("pnd_Cor_C_Contr_A", "#COR-C-CONTR-A", FieldType.STRING, 10);

        pnd_Cor_C_Contr_A__R_Field_71 = localVariables.newGroupInRecord("pnd_Cor_C_Contr_A__R_Field_71", "REDEFINE", pnd_Cor_C_Contr_A);
        pnd_Cor_C_Contr_A_Pnd_Cor_C_Prefx = pnd_Cor_C_Contr_A__R_Field_71.newFieldInGroup("pnd_Cor_C_Contr_A_Pnd_Cor_C_Prefx", "#COR-C-PREFX", FieldType.STRING, 
            1);
        pnd_Cor_C_Contr_A_Pnd_Cor_C_Numbr = pnd_Cor_C_Contr_A__R_Field_71.newFieldInGroup("pnd_Cor_C_Contr_A_Pnd_Cor_C_Numbr", "#COR-C-NUMBR", FieldType.STRING, 
            7);
        pnd_G_Key_Last = localVariables.newFieldInRecord("pnd_G_Key_Last", "#G-KEY-LAST", FieldType.PACKED_DECIMAL, 4);
        pnd_Cor_Own_Cde = localVariables.newFieldInRecord("pnd_Cor_Own_Cde", "#COR-OWN-CDE", FieldType.STRING, 1);
        pnd_Prap_Last_Name = localVariables.newFieldInRecord("pnd_Prap_Last_Name", "#PRAP-LAST-NAME", FieldType.STRING, 30);
        pnd_Prap_First_Name = localVariables.newFieldInRecord("pnd_Prap_First_Name", "#PRAP-FIRST-NAME", FieldType.STRING, 30);
        pnd_Prap_Last_First_Name = localVariables.newFieldInRecord("pnd_Prap_Last_First_Name", "#PRAP-LAST-FIRST-NAME", FieldType.STRING, 30);
        pnd_C_Contr = localVariables.newFieldInRecord("pnd_C_Contr", "#C-CONTR", FieldType.STRING, 8);

        pnd_C_Contr__R_Field_72 = localVariables.newGroupInRecord("pnd_C_Contr__R_Field_72", "REDEFINE", pnd_C_Contr);
        pnd_C_Contr_Pnd_C_Pref = pnd_C_Contr__R_Field_72.newFieldInGroup("pnd_C_Contr_Pnd_C_Pref", "#C-PREF", FieldType.STRING, 1);
        pnd_C_Contr_Pnd_C_Cont = pnd_C_Contr__R_Field_72.newFieldInGroup("pnd_C_Contr_Pnd_C_Cont", "#C-CONT", FieldType.STRING, 7);
        pnd_Ap_Tiaa_Contr = localVariables.newFieldInRecord("pnd_Ap_Tiaa_Contr", "#AP-TIAA-CONTR", FieldType.STRING, 8);

        pnd_Ap_Tiaa_Contr__R_Field_73 = localVariables.newGroupInRecord("pnd_Ap_Tiaa_Contr__R_Field_73", "REDEFINE", pnd_Ap_Tiaa_Contr);
        pnd_Ap_Tiaa_Contr_Pnd_Ap_Tiaa_Contr_1 = pnd_Ap_Tiaa_Contr__R_Field_73.newFieldInGroup("pnd_Ap_Tiaa_Contr_Pnd_Ap_Tiaa_Contr_1", "#AP-TIAA-CONTR-1", 
            FieldType.STRING, 1);
        pnd_Ap_Tiaa_Contr_Pnd_Ap_Tiaa_Contr_2_7 = pnd_Ap_Tiaa_Contr__R_Field_73.newFieldInGroup("pnd_Ap_Tiaa_Contr_Pnd_Ap_Tiaa_Contr_2_7", "#AP-TIAA-CONTR-2-7", 
            FieldType.STRING, 7);
        pnd_Switch_Line = localVariables.newFieldInRecord("pnd_Switch_Line", "#SWITCH-LINE", FieldType.STRING, 72);

        pnd_Switch_Line__R_Field_74 = localVariables.newGroupInRecord("pnd_Switch_Line__R_Field_74", "REDEFINE", pnd_Switch_Line);
        pnd_Switch_Line_Pnd_P_Name = pnd_Switch_Line__R_Field_74.newFieldInGroup("pnd_Switch_Line_Pnd_P_Name", "#P-NAME", FieldType.STRING, 36);
        pnd_Switch_Line_Pnd_P_Fill = pnd_Switch_Line__R_Field_74.newFieldInGroup("pnd_Switch_Line_Pnd_P_Fill", "#P-FILL", FieldType.STRING, 1);
        pnd_Switch_Line_Pnd_P_Rltship = pnd_Switch_Line__R_Field_74.newFieldInGroup("pnd_Switch_Line_Pnd_P_Rltship", "#P-RLTSHIP", FieldType.STRING, 10);
        pnd_Switch_Line_Pnd_P_Fill2 = pnd_Switch_Line__R_Field_74.newFieldInGroup("pnd_Switch_Line_Pnd_P_Fill2", "#P-FILL2", FieldType.STRING, 2);
        pnd_Switch_Line_Pnd_P_Ssn = pnd_Switch_Line__R_Field_74.newFieldInGroup("pnd_Switch_Line_Pnd_P_Ssn", "#P-SSN", FieldType.NUMERIC, 9);

        pnd_Switch_Line__R_Field_75 = pnd_Switch_Line__R_Field_74.newGroupInGroup("pnd_Switch_Line__R_Field_75", "REDEFINE", pnd_Switch_Line_Pnd_P_Ssn);
        pnd_Switch_Line_Pnd_P_Ssn_A = pnd_Switch_Line__R_Field_75.newFieldInGroup("pnd_Switch_Line_Pnd_P_Ssn_A", "#P-SSN-A", FieldType.STRING, 9);
        pnd_Switch_Line_Pnd_P_Fill3 = pnd_Switch_Line__R_Field_74.newFieldInGroup("pnd_Switch_Line_Pnd_P_Fill3", "#P-FILL3", FieldType.STRING, 6);
        pnd_Switch_Line_Pnd_P_Dob = pnd_Switch_Line__R_Field_74.newFieldInGroup("pnd_Switch_Line_Pnd_P_Dob", "#P-DOB", FieldType.NUMERIC, 8);

        pnd_Switch_Line__R_Field_76 = pnd_Switch_Line__R_Field_74.newGroupInGroup("pnd_Switch_Line__R_Field_76", "REDEFINE", pnd_Switch_Line_Pnd_P_Dob);
        pnd_Switch_Line_Pnd_P_Dob_A = pnd_Switch_Line__R_Field_76.newFieldInGroup("pnd_Switch_Line_Pnd_P_Dob_A", "#P-DOB-A", FieldType.STRING, 8);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 3);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.NUMERIC, 3);
        pnd_Plan = localVariables.newFieldInRecord("pnd_Plan", "#PLAN", FieldType.STRING, 6);
        pnd_State_2 = localVariables.newFieldInRecord("pnd_State_2", "#STATE-2", FieldType.STRING, 2);
        pnd_Db_Up = localVariables.newFieldInRecord("pnd_Db_Up", "#DB-UP", FieldType.BOOLEAN, 1);
        pnd_Date_Yyyymmdd = localVariables.newFieldInRecord("pnd_Date_Yyyymmdd", "#DATE-YYYYMMDD", FieldType.NUMERIC, 8);

        pnd_Date_Yyyymmdd__R_Field_77 = localVariables.newGroupInRecord("pnd_Date_Yyyymmdd__R_Field_77", "REDEFINE", pnd_Date_Yyyymmdd);
        pnd_Date_Yyyymmdd_Pnd_Date_Yyyymmdd_Yyyy = pnd_Date_Yyyymmdd__R_Field_77.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Yyyymmdd_Yyyy", "#DATE-YYYYMMDD-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Date_Yyyymmdd_Pnd_Date_Yyyymmdd_Mm = pnd_Date_Yyyymmdd__R_Field_77.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Yyyymmdd_Mm", "#DATE-YYYYMMDD-MM", 
            FieldType.NUMERIC, 2);
        pnd_Date_Yyyymmdd_Pnd_Date_Yyyymmdd_Dd = pnd_Date_Yyyymmdd__R_Field_77.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Yyyymmdd_Dd", "#DATE-YYYYMMDD-DD", 
            FieldType.NUMERIC, 2);

        pnd_Date_Yyyymmdd__R_Field_78 = localVariables.newGroupInRecord("pnd_Date_Yyyymmdd__R_Field_78", "REDEFINE", pnd_Date_Yyyymmdd);
        pnd_Date_Yyyymmdd_Pnd_Date_Yyyymmdd_A = pnd_Date_Yyyymmdd__R_Field_78.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Yyyymmdd_A", "#DATE-YYYYMMDD-A", 
            FieldType.STRING, 8);
        pnd_Date_Mmddyyyy = localVariables.newFieldInRecord("pnd_Date_Mmddyyyy", "#DATE-MMDDYYYY", FieldType.NUMERIC, 8);

        pnd_Date_Mmddyyyy__R_Field_79 = localVariables.newGroupInRecord("pnd_Date_Mmddyyyy__R_Field_79", "REDEFINE", pnd_Date_Mmddyyyy);
        pnd_Date_Mmddyyyy_Pnd_Date_Mmddyyyy_Mm = pnd_Date_Mmddyyyy__R_Field_79.newFieldInGroup("pnd_Date_Mmddyyyy_Pnd_Date_Mmddyyyy_Mm", "#DATE-MMDDYYYY-MM", 
            FieldType.NUMERIC, 2);
        pnd_Date_Mmddyyyy_Pnd_Date_Mmddyyyy_Dd = pnd_Date_Mmddyyyy__R_Field_79.newFieldInGroup("pnd_Date_Mmddyyyy_Pnd_Date_Mmddyyyy_Dd", "#DATE-MMDDYYYY-DD", 
            FieldType.NUMERIC, 2);
        pnd_Date_Mmddyyyy_Pnd_Date_Mmddyyyy_Yyyy = pnd_Date_Mmddyyyy__R_Field_79.newFieldInGroup("pnd_Date_Mmddyyyy_Pnd_Date_Mmddyyyy_Yyyy", "#DATE-MMDDYYYY-YYYY", 
            FieldType.NUMERIC, 4);

        pnd_Date_Mmddyyyy__R_Field_80 = localVariables.newGroupInRecord("pnd_Date_Mmddyyyy__R_Field_80", "REDEFINE", pnd_Date_Mmddyyyy);
        pnd_Date_Mmddyyyy_Pnd_Date_Mmddyyyy_A = pnd_Date_Mmddyyyy__R_Field_80.newFieldInGroup("pnd_Date_Mmddyyyy_Pnd_Date_Mmddyyyy_A", "#DATE-MMDDYYYY-A", 
            FieldType.STRING, 8);
        pnd_Escape_Routine = localVariables.newFieldInRecord("pnd_Escape_Routine", "#ESCAPE-ROUTINE", FieldType.BOOLEAN, 1);
        pnd_Record_Sts = localVariables.newFieldInRecord("pnd_Record_Sts", "#RECORD-STS", FieldType.STRING, 1);
        pnd_Irc_Sec_Cde_N = localVariables.newFieldInRecord("pnd_Irc_Sec_Cde_N", "#IRC-SEC-CDE-N", FieldType.NUMERIC, 2);

        pnd_Irc_Sec_Cde_N__R_Field_81 = localVariables.newGroupInRecord("pnd_Irc_Sec_Cde_N__R_Field_81", "REDEFINE", pnd_Irc_Sec_Cde_N);
        pnd_Irc_Sec_Cde_N_Pnd_Irc_Sec_Cde = pnd_Irc_Sec_Cde_N__R_Field_81.newFieldInGroup("pnd_Irc_Sec_Cde_N_Pnd_Irc_Sec_Cde", "#IRC-SEC-CDE", FieldType.STRING, 
            2);
        pnd_Irc_Sec_Grp_Cde_N = localVariables.newFieldInRecord("pnd_Irc_Sec_Grp_Cde_N", "#IRC-SEC-GRP-CDE-N", FieldType.NUMERIC, 2);

        pnd_Irc_Sec_Grp_Cde_N__R_Field_82 = localVariables.newGroupInRecord("pnd_Irc_Sec_Grp_Cde_N__R_Field_82", "REDEFINE", pnd_Irc_Sec_Grp_Cde_N);
        pnd_Irc_Sec_Grp_Cde_N_Pnd_Irc_Sec_Grp_Cde = pnd_Irc_Sec_Grp_Cde_N__R_Field_82.newFieldInGroup("pnd_Irc_Sec_Grp_Cde_N_Pnd_Irc_Sec_Grp_Cde", "#IRC-SEC-GRP-CDE", 
            FieldType.STRING, 2);
        pnd_Addtnl_Txt = localVariables.newFieldInRecord("pnd_Addtnl_Txt", "#ADDTNL-TXT", FieldType.STRING, 60);

        pnd_Addtnl_Txt__R_Field_83 = localVariables.newGroupInRecord("pnd_Addtnl_Txt__R_Field_83", "REDEFINE", pnd_Addtnl_Txt);
        pnd_Addtnl_Txt_Pnd_Prdct_Cd = pnd_Addtnl_Txt__R_Field_83.newFieldInGroup("pnd_Addtnl_Txt_Pnd_Prdct_Cd", "#PRDCT-CD", FieldType.NUMERIC, 3);
        pnd_Addtnl_Txt_Pnd_Vesting_Local = pnd_Addtnl_Txt__R_Field_83.newFieldInGroup("pnd_Addtnl_Txt_Pnd_Vesting_Local", "#VESTING-LOCAL", FieldType.NUMERIC, 
            1);
        pnd_Dscrptn_Txt = localVariables.newFieldInRecord("pnd_Dscrptn_Txt", "#DSCRPTN-TXT", FieldType.STRING, 60);

        pnd_Dscrptn_Txt__R_Field_84 = localVariables.newGroupInRecord("pnd_Dscrptn_Txt__R_Field_84", "REDEFINE", pnd_Dscrptn_Txt);
        pnd_Dscrptn_Txt_Pnd_Lob = pnd_Dscrptn_Txt__R_Field_84.newFieldInGroup("pnd_Dscrptn_Txt_Pnd_Lob", "#LOB", FieldType.STRING, 1);
        pnd_Dscrptn_Txt_Pnd_Lob_Type = pnd_Dscrptn_Txt__R_Field_84.newFieldInGroup("pnd_Dscrptn_Txt_Pnd_Lob_Type", "#LOB-TYPE", FieldType.STRING, 1);
        pnd_Temp_Doi = localVariables.newFieldInRecord("pnd_Temp_Doi", "#TEMP-DOI", FieldType.NUMERIC, 8);

        pnd_Temp_Doi__R_Field_85 = localVariables.newGroupInRecord("pnd_Temp_Doi__R_Field_85", "REDEFINE", pnd_Temp_Doi);
        pnd_Temp_Doi_Pnd_Temp_T_Doi_Mm = pnd_Temp_Doi__R_Field_85.newFieldInGroup("pnd_Temp_Doi_Pnd_Temp_T_Doi_Mm", "#TEMP-T-DOI-MM", FieldType.NUMERIC, 
            2);
        pnd_Temp_Doi_Pnd_Temp_T_Doi_Dd = pnd_Temp_Doi__R_Field_85.newFieldInGroup("pnd_Temp_Doi_Pnd_Temp_T_Doi_Dd", "#TEMP-T-DOI-DD", FieldType.NUMERIC, 
            2);
        pnd_Temp_Doi_Pnd_Temp_T_Doi_Cc = pnd_Temp_Doi__R_Field_85.newFieldInGroup("pnd_Temp_Doi_Pnd_Temp_T_Doi_Cc", "#TEMP-T-DOI-CC", FieldType.NUMERIC, 
            2);
        pnd_Temp_Doi_Pnd_Temp_T_Doi_Yy = pnd_Temp_Doi__R_Field_85.newFieldInGroup("pnd_Temp_Doi_Pnd_Temp_T_Doi_Yy", "#TEMP-T-DOI-YY", FieldType.NUMERIC, 
            2);
        pnd_Temp_Doi1 = localVariables.newFieldInRecord("pnd_Temp_Doi1", "#TEMP-DOI1", FieldType.NUMERIC, 6);

        pnd_Temp_Doi1__R_Field_86 = localVariables.newGroupInRecord("pnd_Temp_Doi1__R_Field_86", "REDEFINE", pnd_Temp_Doi1);
        pnd_Temp_Doi1_Pnd_Temp_T_Doi1_Mm = pnd_Temp_Doi1__R_Field_86.newFieldInGroup("pnd_Temp_Doi1_Pnd_Temp_T_Doi1_Mm", "#TEMP-T-DOI1-MM", FieldType.NUMERIC, 
            2);
        pnd_Temp_Doi1_Pnd_Temp_T_Doi1_Dd = pnd_Temp_Doi1__R_Field_86.newFieldInGroup("pnd_Temp_Doi1_Pnd_Temp_T_Doi1_Dd", "#TEMP-T-DOI1-DD", FieldType.NUMERIC, 
            2);
        pnd_Temp_Doi1_Pnd_Temp_T_Doi1_Yy = pnd_Temp_Doi1__R_Field_86.newFieldInGroup("pnd_Temp_Doi1_Pnd_Temp_T_Doi1_Yy", "#TEMP-T-DOI1-YY", FieldType.NUMERIC, 
            2);
        pnd_Address = localVariables.newFieldInRecord("pnd_Address", "#ADDRESS", FieldType.STRING, 35);
        pnd_Contract = localVariables.newFieldInRecord("pnd_Contract", "#CONTRACT", FieldType.STRING, 10);
        pnd_Action = localVariables.newFieldInRecord("pnd_Action", "#ACTION", FieldType.STRING, 1);
        pnd_Isn_1 = localVariables.newFieldInRecord("pnd_Isn_1", "#ISN-1", FieldType.PACKED_DECIMAL, 10);
        pnd_Tmp_Alloc = localVariables.newFieldArrayInRecord("pnd_Tmp_Alloc", "#TMP-ALLOC", FieldType.NUMERIC, 3, new DbsArrayController(1, 100));
        pnd_Tmp_Fund_Cde = localVariables.newFieldArrayInRecord("pnd_Tmp_Fund_Cde", "#TMP-FUND-CDE", FieldType.STRING, 10, new DbsArrayController(1, 100));
        pnd_Tmp_Fund_Source_Cde_1 = localVariables.newFieldInRecord("pnd_Tmp_Fund_Source_Cde_1", "#TMP-FUND-SOURCE-CDE-1", FieldType.STRING, 1);
        pnd_Tmp_Fund_Source_Cde_2 = localVariables.newFieldInRecord("pnd_Tmp_Fund_Source_Cde_2", "#TMP-FUND-SOURCE-CDE-2", FieldType.STRING, 1);
        pnd_Tmp_Alloc_2 = localVariables.newFieldArrayInRecord("pnd_Tmp_Alloc_2", "#TMP-ALLOC-2", FieldType.NUMERIC, 3, new DbsArrayController(1, 100));
        pnd_Tmp_Fund_Cde_2 = localVariables.newFieldArrayInRecord("pnd_Tmp_Fund_Cde_2", "#TMP-FUND-CDE-2", FieldType.STRING, 10, new DbsArrayController(1, 
            100));
        pnd_Mit_Update = localVariables.newFieldInRecord("pnd_Mit_Update", "#MIT-UPDATE", FieldType.BOOLEAN, 1);
        pnd_Hold_Mit_Wpid = localVariables.newFieldArrayInRecord("pnd_Hold_Mit_Wpid", "#HOLD-MIT-WPID", FieldType.STRING, 6, new DbsArrayController(1, 
            5));
        pnd_Tmp_Msg = localVariables.newFieldInRecord("pnd_Tmp_Msg", "#TMP-MSG", FieldType.STRING, 79);
        pnd_Tmp_Msg_Nr = localVariables.newFieldInRecord("pnd_Tmp_Msg_Nr", "#TMP-MSG-NR", FieldType.NUMERIC, 4);
        pnd_Tmp_Process_Error_Ind = localVariables.newFieldInRecord("pnd_Tmp_Process_Error_Ind", "#TMP-PROCESS-ERROR-IND", FieldType.STRING, 1);
        pnd_Ssn_Mayo_A = localVariables.newFieldInRecord("pnd_Ssn_Mayo_A", "#SSN-MAYO-A", FieldType.STRING, 9);

        pnd_Ssn_Mayo_A__R_Field_87 = localVariables.newGroupInRecord("pnd_Ssn_Mayo_A__R_Field_87", "REDEFINE", pnd_Ssn_Mayo_A);
        pnd_Ssn_Mayo_A_Pnd_Ssn_Mayo_N = pnd_Ssn_Mayo_A__R_Field_87.newFieldInGroup("pnd_Ssn_Mayo_A_Pnd_Ssn_Mayo_N", "#SSN-MAYO-N", FieldType.NUMERIC, 
            9);

        scia8580 = localVariables.newGroupInRecord("scia8580", "SCIA8580");
        scia8580_Pnd_Action_Code = scia8580.newFieldInGroup("scia8580_Pnd_Action_Code", "#ACTION-CODE", FieldType.STRING, 1);
        scia8580_Pnd_Register_Id = scia8580.newFieldInGroup("scia8580_Pnd_Register_Id", "#REGISTER-ID", FieldType.STRING, 11);
        scia8580_Pnd_Return_Code = scia8580.newFieldInGroup("scia8580_Pnd_Return_Code", "#RETURN-CODE", FieldType.NUMERIC, 4);
        scia8580_Pnd_Return_Msg = scia8580.newFieldInGroup("scia8580_Pnd_Return_Msg", "#RETURN-MSG", FieldType.STRING, 60);
        pnd_Fidelity_Contract_Found = localVariables.newFieldInRecord("pnd_Fidelity_Contract_Found", "#FIDELITY-CONTRACT-FOUND", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Cref_Contract = localVariables.newFieldInRecord("pnd_Tiaa_Cref_Contract", "#TIAA-CREF-CONTRACT", FieldType.STRING, 16);

        pnd_Tiaa_Cref_Contract__R_Field_88 = localVariables.newGroupInRecord("pnd_Tiaa_Cref_Contract__R_Field_88", "REDEFINE", pnd_Tiaa_Cref_Contract);
        pnd_Tiaa_Cref_Contract_Pnd_Tiaa_Contract = pnd_Tiaa_Cref_Contract__R_Field_88.newFieldInGroup("pnd_Tiaa_Cref_Contract_Pnd_Tiaa_Contract", "#TIAA-CONTRACT", 
            FieldType.STRING, 8);
        pnd_Tiaa_Cref_Contract_Pnd_Cref_Contract = pnd_Tiaa_Cref_Contract__R_Field_88.newFieldInGroup("pnd_Tiaa_Cref_Contract_Pnd_Cref_Contract", "#CREF-CONTRACT", 
            FieldType.STRING, 8);
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.INTEGER, 2);
        pnd_Current_Field = localVariables.newFieldInRecord("pnd_Current_Field", "#CURRENT-FIELD", FieldType.STRING, 32);
        pnd_Db_Call = localVariables.newFieldInRecord("pnd_Db_Call", "#DB-CALL", FieldType.STRING, 1);
        pnd_D1 = localVariables.newFieldInRecord("pnd_D1", "#D1", FieldType.PACKED_DECIMAL, 3);
        pnd_D2 = localVariables.newFieldInRecord("pnd_D2", "#D2", FieldType.PACKED_DECIMAL, 3);
        pnd_Object = localVariables.newFieldInRecord("pnd_Object", "#OBJECT", FieldType.STRING, 20);
        pnd_Old_Rec = localVariables.newFieldInRecord("pnd_Old_Rec", "#OLD-REC", FieldType.BOOLEAN, 1);
        pnd_Save_Rec = localVariables.newFieldInRecord("pnd_Save_Rec", "#SAVE-REC", FieldType.BOOLEAN, 1);
        pnd_Update_Performed = localVariables.newFieldInRecord("pnd_Update_Performed", "#UPDATE-PERFORMED", FieldType.BOOLEAN, 1);

        vw_next_View = new DataAccessProgramView(new NameInfo("vw_next_View", "NEXT-VIEW"), "ACIS_BENE_FILE_12", "ACIS_BENE_FILE");
        next_View_Bene_Sts_Lob_Ssn = vw_next_View.getRecord().newFieldInGroup("next_View_Bene_Sts_Lob_Ssn", "BENE-STS-LOB-SSN", FieldType.STRING, 12, 
            RepeatingFieldStrategy.None, "BENE_STS_LOB_SSN");
        next_View_Bene_Sts_Lob_Ssn.setSuperDescriptor(true);
        registerRecord(vw_next_View);

        pls_Lb_Cnt = WsIndependent.getInstance().newFieldInRecord("pls_Lb_Cnt", "+LB-CNT", FieldType.NUMERIC, 6);
        pls_Lb_Contract_Type = WsIndependent.getInstance().newFieldArrayInRecord("pls_Lb_Contract_Type", "+LB-CONTRACT-TYPE", FieldType.STRING, 20, new 
            DbsArrayController(1, 300));
        pls_Lb_Lob = WsIndependent.getInstance().newFieldArrayInRecord("pls_Lb_Lob", "+LB-LOB", FieldType.STRING, 1, new DbsArrayController(1, 300));
        pls_Lb_Lob_Type = WsIndependent.getInstance().newFieldArrayInRecord("pls_Lb_Lob_Type", "+LB-LOB-TYPE", FieldType.STRING, 1, new DbsArrayController(1, 
            300));
        pls_Lb_Product_Code = WsIndependent.getInstance().newFieldArrayInRecord("pls_Lb_Product_Code", "+LB-PRODUCT-CODE", FieldType.NUMERIC, 3, new DbsArrayController(1, 
            300));
        pls_Lb_Vesting = WsIndependent.getInstance().newFieldArrayInRecord("pls_Lb_Vesting", "+LB-VESTING", FieldType.STRING, 1, new DbsArrayController(1, 
            300));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_app_Table_Entry.reset();
        vw_fidelity_Ppg.reset();
        vw_fidelity_Contract.reset();
        vw_next_View.reset();

        ldaScil1080.initializeValues();
        ldaScil3010.initializeValues();

        localVariables.reset();
        pnd_Null.setInitialValue("null");
        pnd_Scroll_Vars_Pnd_Panel.setInitialValue(1);
        hold_Control_Key.setInitialValue("C");
        pnd_Object.setInitialValue("acia3012");
        pnd_Update_Performed.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Scin9000() throws Exception
    {
        super("Scin9000");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("SCIN9000", onError);
        setupReports();
        pdaScia9000.getScia9000_Pnd_Return_Code().setValue("0000");                                                                                                       //Natural: ASSIGN SCIA9000.#RETURN-CODE := '0000'
        pdaScia9000.getScia9000_Pnd_Return_Msg().reset();                                                                                                                 //Natural: RESET SCIA9000.#RETURN-MSG
        //*                                                                                                                                                               //Natural: FORMAT LS = 132
        //*                                                                                                                                                               //Natural: ON ERROR
        if (condition(pls_Lb_Cnt.equals(getZero())))                                                                                                                      //Natural: IF +LB-CNT = 0
        {
            pnd_Active_Table_Key_Pnd_Entry_Actve_Ind.setValue("Y");                                                                                                       //Natural: ASSIGN #ACTIVE-TABLE-KEY.#ENTRY-ACTVE-IND = 'Y'
            pnd_Active_Table_Key_Pnd_Entry_Table_Id_Nbr.setValue(100);                                                                                                    //Natural: ASSIGN #ACTIVE-TABLE-KEY.#ENTRY-TABLE-ID-NBR = 000100
            pnd_Active_Table_Key_Pnd_Entry_Table_Sub_Id.setValue("LB");                                                                                                   //Natural: ASSIGN #ACTIVE-TABLE-KEY.#ENTRY-TABLE-SUB-ID = 'LB'
            pnd_Active_Table_Key_Pnd_Entry_Cde.setValue(" ");                                                                                                             //Natural: ASSIGN #ACTIVE-TABLE-KEY.#ENTRY-CDE = ' '
            vw_app_Table_Entry.startDatabaseRead                                                                                                                          //Natural: READ APP-TABLE-ENTRY BY ACTVE-IND-TABLE-ID-ENTRY-CDE STARTING FROM #ACTIVE-TABLE-KEY
            (
            "READ01",
            new Wc[] { new Wc("ACTVE_IND_TABLE_ID_ENTRY_CDE", ">=", pnd_Active_Table_Key, WcType.BY) },
            new Oc[] { new Oc("ACTVE_IND_TABLE_ID_ENTRY_CDE", "ASC") }
            );
            READ01:
            while (condition(vw_app_Table_Entry.readNextRow("READ01")))
            {
                if (condition(app_Table_Entry_Entry_Actve_Ind.notEquals("Y") || app_Table_Entry_Entry_Table_Id_Nbr.notEquals(pnd_Active_Table_Key_Pnd_Entry_Table_Id_Nbr) //Natural: IF ENTRY-ACTVE-IND NE 'Y' OR APP-TABLE-ENTRY.ENTRY-TABLE-ID-NBR NE #ACTIVE-TABLE-KEY.#ENTRY-TABLE-ID-NBR OR APP-TABLE-ENTRY.ENTRY-TABLE-SUB-ID NE #ACTIVE-TABLE-KEY.#ENTRY-TABLE-SUB-ID
                    || app_Table_Entry_Entry_Table_Sub_Id.notEquals(pnd_Active_Table_Key_Pnd_Entry_Table_Sub_Id)))
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pls_Lb_Cnt.nadd(1);                                                                                                                                       //Natural: ADD 1 TO +LB-CNT
                if (condition(pls_Lb_Cnt.greater(300)))                                                                                                                   //Natural: IF +LB-CNT GT 300
                {
                    getReports().write(0, "MAXIMUM LOB + LOB TYPE REACHED");                                                                                              //Natural: WRITE 'MAXIMUM LOB + LOB TYPE REACHED'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    DbsUtil.terminate(4);  if (true) return;                                                                                                              //Natural: TERMINATE 4
                }                                                                                                                                                         //Natural: END-IF
                pls_Lb_Contract_Type.getValue(pls_Lb_Cnt).setValue(app_Table_Entry_Entry_Cde);                                                                            //Natural: ASSIGN +LB-CONTRACT-TYPE ( +LB-CNT ) := APP-TABLE-ENTRY.ENTRY-CDE
                pls_Lb_Lob.getValue(pls_Lb_Cnt).setValue(app_Table_Entry_Lb_Lob);                                                                                         //Natural: ASSIGN +LB-LOB ( +LB-CNT ) := APP-TABLE-ENTRY.LB-LOB
                pls_Lb_Lob_Type.getValue(pls_Lb_Cnt).setValue(app_Table_Entry_Lb_Lob_Type);                                                                               //Natural: ASSIGN +LB-LOB-TYPE ( +LB-CNT ) := APP-TABLE-ENTRY.LB-LOB-TYPE
                pls_Lb_Product_Code.getValue(pls_Lb_Cnt).setValue(app_Table_Entry_Lb_Product_Code);                                                                       //Natural: ASSIGN +LB-PRODUCT-CODE ( +LB-CNT ) := APP-TABLE-ENTRY.LB-PRODUCT-CODE
                pls_Lb_Vesting.getValue(pls_Lb_Cnt).setValue(app_Table_Entry_Lb_Vesting);                                                                                 //Natural: ASSIGN +LB-VESTING ( +LB-CNT ) := APP-TABLE-ENTRY.LB-VESTING
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaScia9000.getScia9000_Pnd_Ppg_Code().equals("9004") || pdaScia9000.getScia9000_Pnd_Ppg_Code().equals("9006") || pdaScia9000.getScia9000_Pnd_Ppg_Code().equals("9007")  //Natural: IF SCIA9000.#PPG-CODE = '9004' OR = '9006' OR = '9007' OR = '9008' OR = '9010' OR = '9011'
            || pdaScia9000.getScia9000_Pnd_Ppg_Code().equals("9008") || pdaScia9000.getScia9000_Pnd_Ppg_Code().equals("9010") || pdaScia9000.getScia9000_Pnd_Ppg_Code().equals("9011")))
        {
            pdaScia1203.getScia1203_Pnd_Return_Msg_Fld1().setValue(pdaScia9000.getScia9000_Pnd_Field1());                                                                 //Natural: ASSIGN SCIA1203.#RETURN-MSG-FLD1 := SCIA9000.#FIELD1
        }                                                                                                                                                                 //Natural: END-IF
        //* *----------------------------------------------------------------------
        //*                             MAINLINE
        //* *----------------------------------------------------------------------
        if (condition(pdaScia9000.getScia9000_Pnd_System_Requestor().equals("ACIBATIO") || pdaScia9000.getScia9000_Pnd_System_Requestor().equals("ACIBATSG")              //Natural: IF SCIA9000.#SYSTEM-REQUESTOR = 'ACIBATIO' OR = 'ACIBATSG' OR = 'ACIBATNE'
            || pdaScia9000.getScia9000_Pnd_System_Requestor().equals("ACIBATNE")))
        {
            pnd_Use_Bpel_Data.setValue(pdaScia9000.getScia9000_Pnd_Field2());                                                                                             //Natural: ASSIGN #USE-BPEL-DATA := SCIA9000.#FIELD2
        }                                                                                                                                                                 //Natural: END-IF
        //* *----------------------------------------------------------------------
        //*    IF PIN IS BLANK GO TO NDM USING MDMN100A TO RETRIEVE PIN
        //* *----------------------------------------------------------------------
        if (condition(pdaScia9000.getScia9000_Pnd_Pin_Nbr().equals(" ")))                                                                                                 //Natural: IF SCIA9000.#PIN-NBR = ' '
        {
            pdaMdma101.getPnd_Mdma101_Pnd_I_Ssn().compute(new ComputeParameters(false, pdaMdma101.getPnd_Mdma101_Pnd_I_Ssn()), pdaScia9000.getScia9000_Pnd_Ssn().val());  //Natural: ASSIGN #I-SSN := VAL ( SCIA9000.#SSN )
            //*  CALLNAT 'MDMN100' #MDMA100  /* PINE
            //*  PINE
            DbsUtil.callnat(Mdmn101.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                       //Natural: CALLNAT 'MDMN101' #MDMA101
            if (condition(Global.isEscape())) return;
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().notEquals("0000")))                                                                               //Natural: IF #O-RETURN-CODE NE '0000'
            {
                pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1234");                                                                                               //Natural: ASSIGN SCIA9000.#RETURN-CODE := '1234'
                pdaScia9000.getScia9000_Pnd_Return_Msg().setValue(DbsUtil.compress(Global.getPROGRAM(), "RETURN CODE FROM MDM: ", pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code())); //Natural: COMPRESS *PROGRAM 'RETURN CODE FROM MDM: ' #O-RETURN-CODE INTO SCIA9000.#RETURN-MSG
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Pin_N12().equals(getZero())))                                                                               //Natural: IF #O-PIN-N12 = 0
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1254");                                                                                           //Natural: ASSIGN SCIA9000.#RETURN-CODE := '1254'
                    pdaScia9000.getScia9000_Pnd_Return_Msg().setValue(DbsUtil.compress(Global.getPROGRAM(), "SSN WITH NO PIN:", pdaMdma101.getPnd_Mdma101_Pnd_O_Ssn()));  //Natural: COMPRESS *PROGRAM 'SSN WITH NO PIN:' #O-SSN INTO SCIA9000.#RETURN-MSG
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaScia9000.getScia9000_Pnd_Pin_Nbr().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Pin_N12());                                                            //Natural: ASSIGN SCIA9000.#PIN-NBR := #O-PIN-N12
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CONSTANT VALUE 1 FOR 1ST OCCURENCE OF MULTI-APP
        pnd_K.setValue(1);                                                                                                                                                //Natural: MOVE 1 TO #K
                                                                                                                                                                          //Natural: PERFORM VERIFY-IF-CONTRACT-NEEDS-CIP
        sub_Verify_If_Contract_Needs_Cip();
        if (condition(Global.isEscape())) {return;}
        //* *----------------------------------------------------------------------
        //*                   PROCESS 1 - EDIT INPUT PDA (SCIN1201)
        //* *----------------------------------------------------------------------
        pdaScia9000.getScia9000_Pnd_Return_Code().setValue("9999");                                                                                                       //Natural: MOVE '9999' TO SCIA9000.#RETURN-CODE
        pdaScia9000.getScia9000_Pnd_Return_Msg().reset();                                                                                                                 //Natural: RESET SCIA9000.#RETURN-MSG SCIA9000.#PROCESS-IND
        pdaScia9000.getScia9000_Pnd_Process_Ind().reset();
                                                                                                                                                                          //Natural: PERFORM RTN-1-EDIT-SCIA1201
        sub_Rtn_1_Edit_Scia1201();
        if (condition(Global.isEscape())) {return;}
        //* *----------------------------------------------------------------------
        //*               PROCESS 2 - SET DEFAULTS FOR CALLING SYSTEM
        //* *----------------------------------------------------------------------
        //*   DEFAULT VALUES FOR PDA SCIA9000 SHOULD BE SET IN SCIN1201 (SEE RTN-1)
        //*  MAP NO DISPLAY
        pnd_Display_Flag.setValue("N");                                                                                                                                   //Natural: MOVE 'N' TO #DISPLAY-FLAG
        if (condition(pdaScia9000.getScia9000_Pnd_Processing_Type().equals("AP")))                                                                                        //Natural: IF SCIA9000.#PROCESSING-TYPE = 'AP'
        {
            pnd_Default_Enrollment.setValue("N");                                                                                                                         //Natural: MOVE 'N' TO #DEFAULT-ENROLLMENT
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia1100.getScia1100_Existing_Rqst_Log_Unit().setValue(pdaScia9000.getScia9000_Pnd_Mit_Unit());                                                                //Natural: MOVE SCIA9000.#MIT-UNIT TO SCIA1100.EXISTING-RQST-LOG-UNIT
        pdaScia1100.getScia1100_Option().setValue(pdaScia9000.getScia9000_Pnd_Processing_Type());                                                                         //Natural: MOVE SCIA9000.#PROCESSING-TYPE TO SCIA1100.OPTION
        pdaScia1100.getScia1100_Ppg_Code().getValue(1).setValue(pdaScia9000.getScia9000_Pnd_Ppg_Code());                                                                  //Natural: MOVE SCIA9000.#PPG-CODE TO SCIA1100.PPG-CODE ( 1 )
        pdaScia1100.getScia1100_Entered_Ppg_Code().getValue(1).setValue(pdaScia9000.getScia9000_Pnd_Ppg_Code());                                                          //Natural: MOVE SCIA9000.#PPG-CODE TO SCIA1100.ENTERED-PPG-CODE ( 1 )
        pdaScia1100.getScia1100_Contract_Type().getValue(1).setValue(pdaScia9000.getScia9000_Pnd_Contract_Type());                                                        //Natural: MOVE SCIA9000.#CONTRACT-TYPE TO SCIA1100.CONTRACT-TYPE ( 1 )
        pdaScia1100.getScia1100_Mail_Pc().getValue(1).setValue(pdaScia9000.getScia9000_Pnd_Pull_Code());                                                                  //Natural: MOVE SCIA9000.#PULL-CODE TO SCIA1100.MAIL-PC ( 1 )
        pdaScia1100.getScia1100_Mail_Instrct().setValue(pdaScia9000.getScia9000_Pnd_Pull_Code());                                                                         //Natural: MOVE SCIA9000.#PULL-CODE TO SCIA1100.MAIL-INSTRCT
        pdaScia1100.getScia1100_Bypass_Rules().getValue(1).setValue(pdaScia9000.getScia9000_Pnd_Bypass_Rules());                                                          //Natural: MOVE SCIA9000.#BYPASS-RULES TO SCIA1100.BYPASS-RULES ( 1 )
        pdaScia1100.getScia1100_Companion_Ind().getValue(1).setValue(pdaScia9000.getScia9000_Pnd_Companion_Ind());                                                        //Natural: MOVE SCIA9000.#COMPANION-IND TO SCIA1100.COMPANION-IND ( 1 )
        pdaScia1100.getScia1100_Ppg_State().getValue(1).setValue(pdaScia9000.getScia9000_Pnd_Issue_State());                                                              //Natural: MOVE SCIA9000.#ISSUE-STATE TO SCIA1100.PPG-STATE ( 1 )
        pdaScia1100.getScia1100_Last_Name().setValue(pdaScia9000.getScia9000_Pnd_Last_Name());                                                                            //Natural: MOVE SCIA9000.#LAST-NAME TO SCIA1100.LAST-NAME
        pdaScia1100.getScia1100_First_Name().setValue(pdaScia9000.getScia9000_Pnd_First_Name());                                                                          //Natural: MOVE SCIA9000.#FIRST-NAME TO SCIA1100.FIRST-NAME
        pdaScia1100.getScia1100_Middle_Name().setValue(pdaScia9000.getScia9000_Pnd_Middle_Name());                                                                        //Natural: MOVE SCIA9000.#MIDDLE-NAME TO SCIA1100.MIDDLE-NAME
        pdaScia1100.getScia1100_Prefix().setValue(pdaScia9000.getScia9000_Pnd_Prefix());                                                                                  //Natural: MOVE SCIA9000.#PREFIX TO SCIA1100.PREFIX
        pdaScia1100.getScia1100_Suffix().setValue(pdaScia9000.getScia9000_Pnd_Suffix());                                                                                  //Natural: MOVE SCIA9000.#SUFFIX TO SCIA1100.SUFFIX
        pdaScia1100.getScia1100_Sex_Cd().setValue(pdaScia9000.getScia9000_Pnd_Sex());                                                                                     //Natural: MOVE SCIA9000.#SEX TO SCIA1100.SEX-CD
        pdaScia1100.getScia1100_Lob().getValue(1).setValue(pdaScia9000.getScia9000_Pnd_Lob());                                                                            //Natural: MOVE SCIA9000.#LOB TO SCIA1100.LOB ( 1 )
        pdaScia1100.getScia1100_Lob_Type().getValue(1).setValue(pdaScia9000.getScia9000_Pnd_Lob_Type());                                                                  //Natural: MOVE SCIA9000.#LOB-TYPE TO SCIA1100.LOB-TYPE ( 1 )
        pnd_Prdct_Cde.setValue(pdaScia9000.getScia9000_Pnd_Product_Code());                                                                                               //Natural: ASSIGN #PRDCT-CDE := SCIA9000.#PRODUCT-CODE
        if (condition(pnd_Prdct_Cde_Pnd_Prdct_3.greater(" ")))                                                                                                            //Natural: IF #PRDCT-3 GT ' '
        {
            pdaScia1100.getScia1100_Prdct_Cd().getValue(1).setValueEdited(new ReportEditMask("999"),pnd_Prdct_Cde);                                                       //Natural: MOVE EDITED #PRDCT-CDE TO SCIA1100.PRDCT-CD ( 1 ) ( EM = 999 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Prdct_Cde_Pnd_Prdct_2.greater(" ")))                                                                                                        //Natural: IF #PRDCT-2 GT ' '
            {
                pdaScia1100.getScia1100_Prdct_Cd().getValue(1).setValueEdited(new ReportEditMask("99"),pnd_Prdct_Cde);                                                    //Natural: MOVE EDITED #PRDCT-CDE TO SCIA1100.PRDCT-CD ( 1 ) ( EM = 99 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia1100.getScia1100_Prdct_Cd().getValue(1).setValueEdited(new ReportEditMask("9"),pnd_Prdct_Cde);                                                     //Natural: MOVE EDITED #PRDCT-CDE TO SCIA1100.PRDCT-CD ( 1 ) ( EM = 9 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia1100.getScia1100_In_Racf().setValue(pdaScia9000.getScia9000_Pnd_Racf_Id());                                                                                //Natural: MOVE SCIA9000.#RACF-ID TO SCIA1100.IN-RACF
        pdaScia1100.getScia1100_User().setValue(pdaScia9000.getScia9000_Pnd_Racf_Id());                                                                                   //Natural: MOVE SCIA9000.#RACF-ID TO SCIA1100.USER
        pdaScia1100.getScia1100_Ap_Sgrd_Plan_No().setValue(pdaScia9000.getScia9000_Pnd_Sg_Plan_No());                                                                     //Natural: MOVE SCIA9000.#SG-PLAN-NO TO SCIA1100.AP-SGRD-PLAN-NO
        pdaScia1100.getScia1100_Ap_Sgrd_Subplan_No().setValue(pdaScia9000.getScia9000_Pnd_Sg_Subplan_No());                                                               //Natural: MOVE SCIA9000.#SG-SUBPLAN-NO TO SCIA1100.AP-SGRD-SUBPLAN-NO
        pdaScia1100.getScia1100_Ap_Sgrd_Part_Ext().setValue(pdaScia9000.getScia9000_Pnd_Sg_Part_Ext());                                                                   //Natural: MOVE SCIA9000.#SG-PART-EXT TO SCIA1100.AP-SGRD-PART-EXT
        pdaScia1100.getScia1100_Ap_Sgrd_Divsub().setValue(pdaScia9000.getScia9000_Pnd_Sg_Divsub());                                                                       //Natural: MOVE SCIA9000.#SG-DIVSUB TO SCIA1100.AP-SGRD-DIVSUB
        pdaScia1100.getScia1100_Ap_Text_Udf_1().setValue(pdaScia9000.getScia9000_Pnd_Sg_Text_Udf_1());                                                                    //Natural: MOVE SCIA9000.#SG-TEXT-UDF-1 TO SCIA1100.AP-TEXT-UDF-1
        pdaScia1100.getScia1100_Ap_Text_Udf_2().setValue(pdaScia9000.getScia9000_Pnd_Sg_Text_Udf_2());                                                                    //Natural: MOVE SCIA9000.#SG-TEXT-UDF-2 TO SCIA1100.AP-TEXT-UDF-2
        pdaScia1100.getScia1100_Ap_Text_Udf_3().setValue(pdaScia9000.getScia9000_Pnd_Sg_Text_Udf_3());                                                                    //Natural: MOVE SCIA9000.#SG-TEXT-UDF-3 TO SCIA1100.AP-TEXT-UDF-3
        pdaScia1100.getScia1100_Ap_Replacement_Ind().setValue(pdaScia9000.getScia9000_Pnd_Sg_Replacement_Ind());                                                          //Natural: MOVE SCIA9000.#SG-REPLACEMENT-IND TO SCIA1100.AP-REPLACEMENT-IND
        pdaScia1100.getScia1100_Ap_Register_Id().setValue(pdaScia9000.getScia9000_Pnd_Sg_Register_Id());                                                                  //Natural: MOVE SCIA9000.#SG-REGISTER-ID TO SCIA1100.AP-REGISTER-ID
        pdaScia1100.getScia1100_Ap_Agent_Or_Racf_Id().setValue(pdaScia9000.getScia9000_Pnd_Sg_Agent_Racf_Id());                                                           //Natural: MOVE SCIA9000.#SG-AGENT-RACF-ID TO SCIA1100.AP-AGENT-OR-RACF-ID
        pdaScia1100.getScia1100_Ap_Exempt_Ind().setValue(pdaScia9000.getScia9000_Pnd_Sg_Exempt_Ind());                                                                    //Natural: MOVE SCIA9000.#SG-EXEMPT-IND TO SCIA1100.AP-EXEMPT-IND
        pdaScia1100.getScia1100_Ap_Arr_Insurer_1().setValue(pdaScia9000.getScia9000_Pnd_Sg_Insurer_1());                                                                  //Natural: MOVE SCIA9000.#SG-INSURER-1 TO SCIA1100.AP-ARR-INSURER-1
        pdaScia1100.getScia1100_Ap_Arr_Insurer_2().setValue(pdaScia9000.getScia9000_Pnd_Sg_Insurer_2());                                                                  //Natural: MOVE SCIA9000.#SG-INSURER-2 TO SCIA1100.AP-ARR-INSURER-2
        pdaScia1100.getScia1100_Ap_Arr_Insurer_3().setValue(pdaScia9000.getScia9000_Pnd_Sg_Insurer_3());                                                                  //Natural: MOVE SCIA9000.#SG-INSURER-3 TO SCIA1100.AP-ARR-INSURER-3
        pdaScia1100.getScia1100_Ap_Arr_1035_Exch_Ind_1().setValue(pdaScia9000.getScia9000_Pnd_Sg_1035_Exch_Ind_1());                                                      //Natural: MOVE SCIA9000.#SG-1035-EXCH-IND-1 TO SCIA1100.AP-ARR-1035-EXCH-IND-1
        pdaScia1100.getScia1100_Ap_Arr_1035_Exch_Ind_2().setValue(pdaScia9000.getScia9000_Pnd_Sg_1035_Exch_Ind_2());                                                      //Natural: MOVE SCIA9000.#SG-1035-EXCH-IND-2 TO SCIA1100.AP-ARR-1035-EXCH-IND-2
        pdaScia1100.getScia1100_Ap_Arr_1035_Exch_Ind_3().setValue(pdaScia9000.getScia9000_Pnd_Sg_1035_Exch_Ind_3());                                                      //Natural: MOVE SCIA9000.#SG-1035-EXCH-IND-3 TO SCIA1100.AP-ARR-1035-EXCH-IND-3
        pnd_A100_Pnd_A9.setValue(pdaScia9000.getScia9000_Pnd_Ssn());                                                                                                      //Natural: MOVE SCIA9000.#SSN TO #A9
        pnd_Temp_Ssn_N.setValue(pnd_A100_Pnd_N9);                                                                                                                         //Natural: MOVE #N9 TO #TEMP-SSN-N
        pdaScia1100.getScia1100_Ssn().setValue(pnd_A100_Pnd_N9);                                                                                                          //Natural: MOVE #N9 TO SCIA1100.SSN
        pnd_New_Alloc_Format.setValue(pdaScia9000.getScia9000_Pnd_Allocation_Fmt());                                                                                      //Natural: MOVE SCIA9000.#ALLOCATION-FMT TO #NEW-ALLOC-FORMAT
        if (condition(DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Date_Of_Birth(),"YYYYMMDD")))                                                                       //Natural: IF SCIA9000.#DATE-OF-BIRTH = MASK ( YYYYMMDD )
        {
            pnd_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pdaScia9000.getScia9000_Pnd_Date_Of_Birth());                                                             //Natural: MOVE EDITED SCIA9000.#DATE-OF-BIRTH TO #D ( EM = YYYYMMDD )
            pdaScia1100.getScia1100_Dob_A().setValueEdited(pnd_D,new ReportEditMask("MMDDYYYY"));                                                                         //Natural: MOVE EDITED #D ( EM = MMDDYYYY ) TO SCIA1100.DOB-A
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Date_App_Recvd(),"YYYYMMDD")))                                                                      //Natural: IF SCIA9000.#DATE-APP-RECVD = MASK ( YYYYMMDD )
        {
            pnd_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pdaScia9000.getScia9000_Pnd_Date_App_Recvd());                                                            //Natural: MOVE EDITED SCIA9000.#DATE-APP-RECVD TO #D ( EM = YYYYMMDD )
            pdaScia1100.getScia1100_Received_Dte_A().setValueEdited(pnd_D,new ReportEditMask("MMDDYYYY"));                                                                //Natural: MOVE EDITED #D ( EM = MMDDYYYY ) TO SCIA1100.RECEIVED-DTE-A
            pdaScia1100.getScia1100_Tiaa_Rcvd_Dte_A().setValueEdited(pnd_D,new ReportEditMask("MMDDYYYY"));                                                               //Natural: MOVE EDITED #D ( EM = MMDDYYYY ) TO SCIA1100.TIAA-RCVD-DTE-A
        }                                                                                                                                                                 //Natural: END-IF
        pnd_A100_Pnd_N8.setValue(pdaScia1100.getScia1100_T_Doi());                                                                                                        //Natural: MOVE SCIA1100.T-DOI TO #N8
        if (condition(DbsUtil.maskMatches(pnd_A100_Pnd_A8,"MMDDYYYY")))                                                                                                   //Natural: IF #A8 = MASK ( MMDDYYYY )
        {
            pnd_D.setValueEdited(new ReportEditMask("MMDDYYYY"),pnd_A100_Pnd_A8);                                                                                         //Natural: MOVE EDITED #A8 TO #D ( EM = MMDDYYYY )
            pdaScia9000.getScia9000_Pnd_Tiaa_Date_Of_Issue().setValueEdited(pnd_D,new ReportEditMask("YYYYMMDD"));                                                        //Natural: MOVE EDITED #D ( EM = YYYYMMDD ) TO SCIA9000.#TIAA-DATE-OF-ISSUE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_A100_Pnd_N8.setValue(pdaScia1100.getScia1100_C_Doi());                                                                                                        //Natural: MOVE SCIA1100.C-DOI TO #N8
        if (condition(DbsUtil.maskMatches(pnd_A100_Pnd_A8,"MMDDYYYY")))                                                                                                   //Natural: IF #A8 = MASK ( MMDDYYYY )
        {
            pnd_D.setValueEdited(new ReportEditMask("MMDDYYYY"),pnd_A100_Pnd_A8);                                                                                         //Natural: MOVE EDITED #A8 TO #D ( EM = MMDDYYYY )
            pdaScia9000.getScia9000_Pnd_Cref_Date_Of_Issue().setValueEdited(pnd_D,new ReportEditMask("YYYYMMDD"));                                                        //Natural: MOVE EDITED #D ( EM = YYYYMMDD ) TO SCIA9000.#CREF-DATE-OF-ISSUE
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia9000.getScia9000_Pnd_Return_Code().reset();                                                                                                                //Natural: RESET SCIA9000.#RETURN-CODE SCIA9000.#RETURN-MSG
        pdaScia9000.getScia9000_Pnd_Return_Msg().reset();
        //*  ISSUE OR ASSIGN
        short decideConditionsMet4051 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF SCIA9000.#PROCESS-IND;//Natural: VALUE ' '
        if (condition((pdaScia9000.getScia9000_Pnd_Process_Ind().equals(" "))))
        {
            decideConditionsMet4051++;
            short decideConditionsMet4053 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF SCIA9000.#PROCESSING-TYPE;//Natural: VALUE 'AP'
            if (condition((pdaScia9000.getScia9000_Pnd_Processing_Type().equals("AP"))))
            {
                decideConditionsMet4053++;
                                                                                                                                                                          //Natural: PERFORM RTN-6-APPLICATION-PROCESSING
                sub_Rtn_6_Application_Processing();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
                //*  PREM-APP
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'P'
        else if (condition((pdaScia9000.getScia9000_Pnd_Process_Ind().equals("P"))))
        {
            decideConditionsMet4051++;
            //*  INCOMPLETE-APP
                                                                                                                                                                          //Natural: PERFORM RTN-6-APPLICATION-PROCESSING
            sub_Rtn_6_Application_Processing();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((pdaScia9000.getScia9000_Pnd_Process_Ind().equals("I"))))
        {
            decideConditionsMet4051++;
            //*  TIAA/CREF COMPANION
                                                                                                                                                                          //Natural: PERFORM RTN-6-APPLICATION-PROCESSING
            sub_Rtn_6_Application_Processing();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'T','C'
        else if (condition((pdaScia9000.getScia9000_Pnd_Process_Ind().equals("T") || pdaScia9000.getScia9000_Pnd_Process_Ind().equals("C"))))
        {
            decideConditionsMet4051++;
                                                                                                                                                                          //Natural: PERFORM RTN-6-APPLICATION-PROCESSING
            sub_Rtn_6_Application_Processing();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* *----------------------------------------------------------------------
        //*                 PROCESS 9  - RETURN TO CALLING MODULE
        //* *----------------------------------------------------------------------
                                                                                                                                                                          //Natural: PERFORM RTN-9-PROCESSING-COMPLETE
        sub_Rtn_9_Processing_Complete();
        if (condition(Global.isEscape())) {return;}
        //*  END OF MAINLINE
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-1-EDIT-SCIA1201
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-6-APPLICATION-PROCESSING
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-6A-PRE-PROCESS-ACIN3000
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-6A-PROCESS-ACIN3000
        //* *----------------------------------------------------------------------
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-6A-PROCESS-SCIN3001
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-6A-POST-PROCESS-ACIN3000
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-6B-PROCESS-BENEFICIARY
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-6D-PROCESS-ACIN3500
        //* ******************************* SCIN3500 ****************************
        //*     ADDED IISG 2ND ALLOCATION
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-6D-POST-PROCESS-ACIN3500
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-9-PROCESSING-COMPLETE
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-11B-POST-COMPLETION-ACIBAT
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-13-COMPRESS-ALLOC-ARRAY
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VERIFY-IF-CONTRACT-NEEDS-CIP
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-102-DETERMINE-LOB-LOBTYPE
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-1B-DEFAULTS-ACIBAT
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-OBJECT
        //* *-----------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-ACIN9004
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ERROR
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-OBJECT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-OBJECT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: HOLD-OBJECT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EDIT-OBJECT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-EXISTENCE
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CLEAR
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DB-CALL
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-OBJECT-ID
    }
    private void sub_Rtn_1_Edit_Scia1201() throws Exception                                                                                                               //Natural: RTN-1-EDIT-SCIA1201
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        //*  SUNGARD
        short decideConditionsMet4235 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SCIA9000.#SYSTEM-REQUESTOR = 'ACIBATSG'
        if (condition(pdaScia9000.getScia9000_Pnd_System_Requestor().equals("ACIBATSG")))
        {
            decideConditionsMet4235++;
            pdaScia1203.getScia1203_Pnd_Func_Mit().setValue("N");                                                                                                         //Natural: MOVE 'N' TO SCIA1203.#FUNC-MIT
            pdaScia1203.getScia1203_Pnd_Func_Cor().setValue("Y");                                                                                                         //Natural: MOVE 'Y' TO SCIA1203.#FUNC-COR SCIA1203.#FUNC-COR-GET-CONTRACT SCIA1203.#FUNC-COR-CREATE-PIN SCIA1203.#FUNC-COR-ADD-CONTRACT SCIA1203.#FUNC-NAS SCIA1203.#FUNC-NAS-FINALIST SCIA1203.#FUNC-BENE SCIA1203.#FUNC-PRAP SCIA1203.#FUNC-NON-PREM SCIA1203.#FUNC-STATE
            pdaScia1203.getScia1203_Pnd_Func_Cor_Get_Contract().setValue("Y");
            pdaScia1203.getScia1203_Pnd_Func_Cor_Create_Pin().setValue("Y");
            pdaScia1203.getScia1203_Pnd_Func_Cor_Add_Contract().setValue("Y");
            pdaScia1203.getScia1203_Pnd_Func_Nas().setValue("Y");
            pdaScia1203.getScia1203_Pnd_Func_Nas_Finalist().setValue("Y");
            pdaScia1203.getScia1203_Pnd_Func_Bene().setValue("Y");
            pdaScia1203.getScia1203_Pnd_Func_Prap().setValue("Y");
            pdaScia1203.getScia1203_Pnd_Func_Non_Prem().setValue("Y");
            pdaScia1203.getScia1203_Pnd_Func_State().setValue("Y");
            pdaScia1203.getScia1203_Pnd_Func_Nas_Get_Current_Address().setValue("N");                                                                                     //Natural: MOVE 'N' TO SCIA1203.#FUNC-NAS-GET-CURRENT-ADDRESS SCIA1203.#FUNC-ACCPTD-APPLCTN SCIA1203.#FUNC-ACCPTD-APPLCTN-INET SCIA1203.#FUNC-FLD1 SCIA1203.#FUNC-FLD2 SCIA1203.#FUNC-FLD3
            pdaScia1203.getScia1203_Pnd_Func_Accptd_Applctn().setValue("N");
            pdaScia1203.getScia1203_Pnd_Func_Accptd_Applctn_Inet().setValue("N");
            pdaScia1203.getScia1203_Pnd_Func_Fld1().setValue("N");
            pdaScia1203.getScia1203_Pnd_Func_Fld2().setValue("N");
            pdaScia1203.getScia1203_Pnd_Func_Fld3().setValue("N");
            if (condition(pdaScia9000.getScia9000_Pnd_Processing_Type().equals("AP")))                                                                                    //Natural: IF SCIA9000.#PROCESSING-TYPE = 'AP'
            {
                pdaScia1203.getScia1203_Pnd_Func_Alloc().setValue("Y");                                                                                                   //Natural: MOVE 'Y' TO SCIA1203.#FUNC-ALLOC
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia1203.getScia1203_Pnd_Func_Alloc().setValue("N");                                                                                                   //Natural: MOVE 'N' TO SCIA1203.#FUNC-ALLOC
            }                                                                                                                                                             //Natural: END-IF
            pnd_Edit_Flags_Pnd_Edit_Communication_Data.setValue(true);                                                                                                    //Natural: MOVE TRUE TO #EDIT-COMMUNICATION-DATA #EDIT-REQUESTOR-DATA #EDIT-PROCESSING-DATA #EDIT-CONTRACT-DATA #EDIT-MIT-DATA #EDIT-PARTICIPANT-DATA #EDIT-ALLOCATION-DATA
            pnd_Edit_Flags_Pnd_Edit_Requestor_Data.setValue(true);
            pnd_Edit_Flags_Pnd_Edit_Processing_Data.setValue(true);
            pnd_Edit_Flags_Pnd_Edit_Contract_Data.setValue(true);
            pnd_Edit_Flags_Pnd_Edit_Mit_Data.setValue(true);
            pnd_Edit_Flags_Pnd_Edit_Participant_Data.setValue(true);
            pnd_Edit_Flags_Pnd_Edit_Allocation_Data.setValue(true);
            pnd_Edit_Flags_Pnd_Edit_Financial_Data.reset();                                                                                                               //Natural: RESET #EDIT-FINANCIAL-DATA
            if (condition(pdaScia9000.getScia9000_Pnd_Processing_Type().equals("AP")))                                                                                    //Natural: IF SCIA9000.#PROCESSING-TYPE = 'AP'
            {
                pnd_Edit_Flags_Pnd_Edit_Address_Data.setValue(true);                                                                                                      //Natural: MOVE TRUE TO #EDIT-ADDRESS-DATA #EDIT-BENEFICIARY-DATA
                pnd_Edit_Flags_Pnd_Edit_Beneficiary_Data.setValue(true);
            }                                                                                                                                                             //Natural: END-IF
            //*  SUNGARD PLAN
            if (condition(pdaScia9000.getScia9000_Pnd_Ppg_Code().equals("SGRD")))                                                                                         //Natural: IF SCIA9000.#PPG-CODE = 'SGRD'
            {
                if (condition(pdaScia9000.getScia9000_Pnd_Sg_Plan_No().equals(" ")))                                                                                      //Natural: IF SCIA9000.#SG-PLAN-NO = ' '
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(2350) SUNGARD PLAN IS REQUIRED");                                                   //Natural: WRITE SCIA9000.#RETURN-CODE '(2350) SUNGARD PLAN IS REQUIRED'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaScia9000.getScia9000_Pnd_Sg_Subplan_No().equals(" ")))                                                                                   //Natural: IF SCIA9000.#SG-SUBPLAN-NO = ' '
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(2380) SUNGARD SUBPLAN IS REQUIRED");                                                //Natural: WRITE SCIA9000.#RETURN-CODE '(2380) SUNGARD SUBPLAN IS REQUIRED'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Product_Code().equals(" ") || pdaScia9000.getScia9000_Pnd_Product_Code().equals(pnd_Null)))                         //Natural: IF SCIA9000.#PRODUCT-CODE = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Product_Code().moveAll("0");                                                                                                  //Natural: MOVE ALL '0' TO SCIA9000.#PRODUCT-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Product_Code().setValue(pdaScia9000.getScia9000_Pnd_Product_Code(), MoveOption.RightJustified);                               //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#PRODUCT-CODE TO SCIA9000.#PRODUCT-CODE
                DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Product_Code(),true), new ExamineSearch(" "), new ExamineReplace("0"));                     //Natural: EXAMINE FULL SCIA9000.#PRODUCT-CODE FOR ' ' REPLACE WITH '0'
                if (condition(! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Product_Code(),"NNN"))))                                                                 //Natural: IF SCIA9000.#PRODUCT-CODE NE MASK ( NNN )
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(2450) PRDCT CODE MUST BE NUMERIC");                                                 //Natural: WRITE SCIA9000.#RETURN-CODE '(2450) PRDCT CODE MUST BE NUMERIC'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet4284 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SCIA9000.#CONTRACT-TYPE NE ' '
            if (condition(pdaScia9000.getScia9000_Pnd_Contract_Type().notEquals(" ")))
            {
                decideConditionsMet4284++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN SCIA9000.#LOB = ' ' AND SCIA9000.#LOB-TYPE = ' '
            else if (condition(pdaScia9000.getScia9000_Pnd_Lob().equals(" ") && pdaScia9000.getScia9000_Pnd_Lob_Type().equals(" ")))
            {
                decideConditionsMet4284++;
                pdaScia9000.getScia9000_Pnd_Return_Code().reset();                                                                                                        //Natural: RESET SCIA9000.#RETURN-CODE SCIA9000.#RETURN-MSG
                pdaScia9000.getScia9000_Pnd_Return_Msg().reset();
                                                                                                                                                                          //Natural: PERFORM RTN-102-DETERMINE-LOB-LOBTYPE
                sub_Rtn_102_Determine_Lob_Lobtype();
                if (condition(Global.isEscape())) {return;}
                if (condition(pdaScia9000.getScia9000_Pnd_Process_Ind().equals("E")))                                                                                     //Natural: IF SCIA9000.#PROCESS-IND = 'E'
                {
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),pdaScia9000.getScia9000_Pnd_Return_Msg());                                            //Natural: WRITE SCIA9000.#RETURN-CODE SCIA9000.#RETURN-MSG
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                pdaScia9000.getScia9000_Pnd_Lob().setValue(pnd_W_Lob);                                                                                                    //Natural: MOVE #W-LOB TO SCIA9000.#LOB
                pdaScia9000.getScia9000_Pnd_Lob_Type().setValue(pnd_W_Lob_Type);                                                                                          //Natural: MOVE #W-LOB-TYPE TO SCIA9000.#LOB-TYPE
            }                                                                                                                                                             //Natural: WHEN SCIA9000.#PRODUCT-CODE = '004' OR ( SCIA9000.#LOB = 'S' AND SCIA9000.#LOB-TYPE = '3' )
            else if (condition((pdaScia9000.getScia9000_Pnd_Product_Code().equals("004") || (pdaScia9000.getScia9000_Pnd_Lob().equals("S") && pdaScia9000.getScia9000_Pnd_Lob_Type().equals("3")))))
            {
                decideConditionsMet4284++;
                if (condition(pdaScia9000.getScia9000_Pnd_Irc_Section_Code().equals("17")))                                                                               //Natural: IF SCIA9000.#IRC-SECTION-CODE = '17'
                {
                    pdaScia9000.getScia9000_Pnd_Lob().setValue("S");                                                                                                      //Natural: MOVE 'S' TO SCIA9000.#LOB
                    pdaScia9000.getScia9000_Pnd_Lob_Type().setValue("4");                                                                                                 //Natural: MOVE '4' TO SCIA9000.#LOB-TYPE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  GSRA
                    pdaScia9000.getScia9000_Pnd_Lob().setValue("S");                                                                                                      //Natural: MOVE 'S' TO SCIA9000.#LOB
                    pdaScia9000.getScia9000_Pnd_Lob_Type().setValue("3");                                                                                                 //Natural: MOVE '3' TO SCIA9000.#LOB-TYPE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  INSTL ONL ENRLL
            pdaScia9000.getScia9000_Pnd_Allocation_Fmt().setValue("O");                                                                                                   //Natural: MOVE 'O' TO SCIA9000.#ALLOCATION-FMT
        }                                                                                                                                                                 //Natural: WHEN SCIA9000.#SYSTEM-REQUESTOR = 'ACIBATIO'
        else if (condition(pdaScia9000.getScia9000_Pnd_System_Requestor().equals("ACIBATIO")))
        {
            decideConditionsMet4235++;
            if (condition(pdaScia9000.getScia9000_Pnd_Racf_Id().equals(" ") || pdaScia9000.getScia9000_Pnd_Racf_Id().equals(pnd_Null)))                                   //Natural: IF SCIA9000.#RACF-ID = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Racf_Id().setValue("ENRLUSER");                                                                                               //Natural: MOVE 'ENRLUSER' TO SCIA9000.#RACF-ID
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM RTN-1B-DEFAULTS-ACIBAT
            sub_Rtn_1b_Defaults_Acibat();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN SCIA9000.#SYSTEM-REQUESTOR = MASK ( 'ACIBAT' )
        else if (condition(DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_System_Requestor(),"'ACIBAT'")))
        {
            decideConditionsMet4235++;
                                                                                                                                                                          //Natural: PERFORM RTN-1B-DEFAULTS-ACIBAT
            sub_Rtn_1b_Defaults_Acibat();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                                   //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
            getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(1030) INVALID SYSTEM REQUESTOR");                                                           //Natural: WRITE SCIA9000.#RETURN-CODE '(1030) INVALID SYSTEM REQUESTOR'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Edit_Flags_Pnd_Edit_Communication_Data.getBoolean()))                                                                                           //Natural: IF #EDIT-COMMUNICATION-DATA
        {
            if (condition(pdaScia9000.getScia9000_Pnd_More_Data().equals("Y") || pdaScia9000.getScia9000_Pnd_More_Data().equals("N")))                                    //Natural: IF SCIA9000.#MORE-DATA = 'Y' OR = 'N'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                               //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(2825) MORE DATA MUST BE Y/N");                                                          //Natural: WRITE SCIA9000.#RETURN-CODE '(2825) MORE DATA MUST BE Y/N'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Retry().equals("Y") || pdaScia9000.getScia9000_Pnd_Retry().equals("N")))                                            //Natural: IF SCIA9000.#RETRY = 'Y' OR = 'N'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                               //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(2870) RETRY MUST BE Y/N");                                                              //Natural: WRITE SCIA9000.#RETURN-CODE '(2870) RETRY MUST BE Y/N'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Edit_Flags_Pnd_Edit_Requestor_Data.getBoolean()))                                                                                               //Natural: IF #EDIT-REQUESTOR-DATA
        {
            if (condition(pdaScia9000.getScia9000_Pnd_System_Requestor().equals(" ") || pdaScia9000.getScia9000_Pnd_System_Requestor().equals(pnd_Null)))                 //Natural: IF SCIA9000.#SYSTEM-REQUESTOR = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                               //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(2935) SYSTEM REQUESTOR REQUIRED");                                                      //Natural: WRITE SCIA9000.#RETURN-CODE '(2935) SYSTEM REQUESTOR REQUIRED'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Racf_Id().equals(" ") || pdaScia9000.getScia9000_Pnd_Racf_Id().equals(pnd_Null)))                                   //Natural: IF SCIA9000.#RACF-ID = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                               //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(2970) RACF ID REQUIRED");                                                               //Natural: WRITE SCIA9000.#RETURN-CODE '(2970) RACF ID REQUIRED'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Edit_Flags_Pnd_Edit_Processing_Data.getBoolean()))                                                                                              //Natural: IF #EDIT-PROCESSING-DATA
        {
            if (condition(pdaScia9000.getScia9000_Pnd_Processing_Type().equals("AP") || pdaScia9000.getScia9000_Pnd_Processing_Type().equals("PP")))                      //Natural: IF SCIA9000.#PROCESSING-TYPE = 'AP' OR = 'PP'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                               //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(3045) PROCESSING TYPE MUST BE AP OR PP");                                               //Natural: WRITE SCIA9000.#RETURN-CODE '(3045) PROCESSING TYPE MUST BE AP OR PP'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Ppg_Code().equals(" ") || pdaScia9000.getScia9000_Pnd_Ppg_Code().equals(pnd_Null)))                                 //Natural: IF SCIA9000.#PPG-CODE = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                               //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(3085) PPG CODE REQUIRED");                                                              //Natural: WRITE SCIA9000.#RETURN-CODE '(3085) PPG CODE REQUIRED'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Substitution_Contract_Ind().greater(" ")))                                                                          //Natural: IF SCIA9000.#SUBSTITUTION-CONTRACT-IND GT ' '
            {
                pnd_Scia9000_Field_3.setValue(pdaScia9000.getScia9000_Pnd_Field3());                                                                                      //Natural: ASSIGN #SCIA9000-FIELD-3 := SCIA9000.#FIELD3
                short decideConditionsMet4379 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SCIA9000.#PULL-CODE = 'Y'
                if (condition(pdaScia9000.getScia9000_Pnd_Pull_Code().equals("Y")))
                {
                    decideConditionsMet4379++;
                    getReports().write(0, "1201","CONTRACT PULL CODE MUST BE EQUAL TO 'N'");                                                                              //Natural: WRITE '1201' 'CONTRACT PULL CODE MUST BE EQUAL TO "N"'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: WHEN SCIA9000.#COMPANION-IND = 'Y'
                else if (condition(pdaScia9000.getScia9000_Pnd_Companion_Ind().equals("Y")))
                {
                    decideConditionsMet4379++;
                    //*  ONE-OFF SUB
                    getReports().write(0, "1201","COMPANION INDICATOR MUST BE EQUAL TO 'N'");                                                                             //Natural: WRITE '1201' 'COMPANION INDICATOR MUST BE EQUAL TO "N"'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: WHEN SCIA9000.#DIVORCE-IND = 'Y' AND SCIA9000.#SUBSTITUTION-CONTRACT-IND NE 'O'
                else if (condition(pdaScia9000.getScia9000_Pnd_Divorce_Ind().equals("Y") && pdaScia9000.getScia9000_Pnd_Substitution_Contract_Ind().notEquals("O")))
                {
                    decideConditionsMet4379++;
                    //*  INDEXED RATE IND
                    getReports().write(0, "1201","DIVORCE INDICATOR MUST BE EQUAL TO 'N'");                                                                               //Natural: WRITE '1201' 'DIVORCE INDICATOR MUST BE EQUAL TO "N"'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: WHEN SUBSTR ( SCIA9000.#SG-TEXT-UDF-1,4,1 ) NE 'Y'
                else if (condition(!pdaScia9000.getScia9000_Pnd_Sg_Text_Udf_1().getSubstring(4,1).equals("Y")))
                {
                    decideConditionsMet4379++;
                    getReports().write(0, "1201","INDEXED RATE INDICATOR MUST EQUAL TO 'Y'");                                                                             //Natural: WRITE '1201' 'INDEXED RATE INDICATOR MUST EQUAL TO "Y"'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: WHEN SCIA9000.#TIAA-DATE-OF-ISSUE NE MASK ( YYYYMMDD )
                else if (condition(! ((DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Tiaa_Date_Of_Issue(),"YYYYMMDD")))))
                {
                    decideConditionsMet4379++;
                    getReports().write(0, "1201","TIAA DATE OF ISSUE IS REQUIRED TO BE IN YYYYMMDD FORMAT");                                                              //Natural: WRITE '1201' 'TIAA DATE OF ISSUE IS REQUIRED TO BE IN YYYYMMDD FORMAT'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: WHEN SCIA9000.#CREF-DATE-OF-ISSUE NE MASK ( YYYYMMDD )
                else if (condition(! ((DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Cref_Date_Of_Issue(),"YYYYMMDD")))))
                {
                    decideConditionsMet4379++;
                    getReports().write(0, "1201","CREF DATE OF ISSUE IS REQUIRED TO BE IN YYYYMMDD FORMAT");                                                              //Natural: WRITE '1201' 'CREF DATE OF ISSUE IS REQUIRED TO BE IN YYYYMMDD FORMAT'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: WHEN #SCIA9000-ORIG-TIAA-CONTRACT = ' '
                else if (condition(pnd_Scia9000_Field_3_Pnd_Scia9000_Orig_Tiaa_Contract.equals(" ")))
                {
                    decideConditionsMet4379++;
                    getReports().write(0, "1201","ORIGINATING TIAA CONTRACT NO. IS REQUIRED");                                                                            //Natural: WRITE '1201' 'ORIGINATING TIAA CONTRACT NO. IS REQUIRED'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: WHEN #SCIA9000-ORIG-CREF-CONTRACT = ' '
                else if (condition(pnd_Scia9000_Field_3_Pnd_Scia9000_Orig_Cref_Contract.equals(" ")))
                {
                    decideConditionsMet4379++;
                    getReports().write(0, "1201","ORIGINATING CREF CERTIFICATE NO. IS REQUIRED");                                                                         //Natural: WRITE '1201' 'ORIGINATING CREF CERTIFICATE NO. IS REQUIRED'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: WHEN SCIA9000.#ANNUITY-START-DATE GT ' ' AND SCIA9000.#ANNUITY-START-DATE NE MASK ( YYYYMMDD ) AND SCIA9000.#ANNUITY-START-DATE NE '00000000'
                else if (condition(pdaScia9000.getScia9000_Pnd_Annuity_Start_Date().greater(" ") && ! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Annuity_Start_Date(),"YYYYMMDD")) 
                    && pdaScia9000.getScia9000_Pnd_Annuity_Start_Date().notEquals("00000000")))
                {
                    decideConditionsMet4379++;
                    getReports().write(0, "1201","ANNUITY START DATE IS REQUIRED TO BE IN YYYYMMDD FORMAT");                                                              //Natural: WRITE '1201' 'ANNUITY START DATE IS REQUIRED TO BE IN YYYYMMDD FORMAT'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: WHEN SCIA9000.#CONVERSION-ISSUE-STATE > ' ' AND ( SCIA9000.#CONVERSION-ISSUE-STATE = 'FN' OR = 'CN' ) AND SCIA9000.#DECEDENT-CONTRACT = ' '
                else if (condition(((pdaScia9000.getScia9000_Pnd_Conversion_Issue_State().greater(" ") && (pdaScia9000.getScia9000_Pnd_Conversion_Issue_State().equals("FN") 
                    || pdaScia9000.getScia9000_Pnd_Conversion_Issue_State().equals("CN"))) && pdaScia9000.getScia9000_Pnd_Decedent_Contract().equals(" "))))
                {
                    decideConditionsMet4379++;
                    getReports().write(0, "1201","FOREIGN ADDRESS IS NOT ALLOWED FOR INDIVIDUAL PROD. ENROLLMENT");                                                       //Natural: WRITE '1201' 'FOREIGN ADDRESS IS NOT ALLOWED FOR INDIVIDUAL PROD. ENROLLMENT'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet4379 > 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(((((((pdaScia9000.getScia9000_Pnd_Res_State().equals("FN") || pdaScia9000.getScia9000_Pnd_Res_State().equals("CN")) || (pdaScia9000.getScia9000_Pnd_Res_Country_Code().greater(" ")  //Natural: IF ( ( SCIA9000.#RES-STATE = 'FN' OR = 'CN' ) OR ( SCIA9000.#RES-COUNTRY-CODE GT ' ' AND SCIA9000.#RES-COUNTRY-CODE NE 'US' ) OR ( SCIA9000.#RES-ZIP-CODE = 'FORGN' OR = 'CANAD' ) ) AND ( SUBSTR ( SCIA9000.#SG-SUBPLAN-NO,1,3 ) = 'RA6' OR = 'SR1' OR = 'IR1' OR = 'IR2' OR = 'IR3' OR = 'IR8' OR = 'KE1' OR = 'KE2' OR = 'KE3' OR = 'KE4' OR = 'KE5' OR = 'KE6' OR = 'KE7' OR = 'KE8' OR = 'KE9' ) AND SCIA9000.#DECEDENT-CONTRACT = ' ' AND SCIA9000.#ONEIRA-ACCT-NO = ' '
                    && pdaScia9000.getScia9000_Pnd_Res_Country_Code().notEquals("US"))) || (pdaScia9000.getScia9000_Pnd_Res_Zip_Code().equals("FORGN") || 
                    pdaScia9000.getScia9000_Pnd_Res_Zip_Code().equals("CANAD"))) && ((((((((((((((pdaScia9000.getScia9000_Pnd_Sg_Subplan_No().getSubstring(1,3).equals("RA6") 
                    || pdaScia9000.getScia9000_Pnd_Sg_Subplan_No().getSubstring(1,3).equals("SR1")) || pdaScia9000.getScia9000_Pnd_Sg_Subplan_No().getSubstring(1,3).equals("IR1")) 
                    || pdaScia9000.getScia9000_Pnd_Sg_Subplan_No().getSubstring(1,3).equals("IR2")) || pdaScia9000.getScia9000_Pnd_Sg_Subplan_No().getSubstring(1,3).equals("IR3")) 
                    || pdaScia9000.getScia9000_Pnd_Sg_Subplan_No().getSubstring(1,3).equals("IR8")) || pdaScia9000.getScia9000_Pnd_Sg_Subplan_No().getSubstring(1,3).equals("KE1")) 
                    || pdaScia9000.getScia9000_Pnd_Sg_Subplan_No().getSubstring(1,3).equals("KE2")) || pdaScia9000.getScia9000_Pnd_Sg_Subplan_No().getSubstring(1,3).equals("KE3")) 
                    || pdaScia9000.getScia9000_Pnd_Sg_Subplan_No().getSubstring(1,3).equals("KE4")) || pdaScia9000.getScia9000_Pnd_Sg_Subplan_No().getSubstring(1,3).equals("KE5")) 
                    || pdaScia9000.getScia9000_Pnd_Sg_Subplan_No().getSubstring(1,3).equals("KE6")) || pdaScia9000.getScia9000_Pnd_Sg_Subplan_No().getSubstring(1,3).equals("KE7")) 
                    || pdaScia9000.getScia9000_Pnd_Sg_Subplan_No().getSubstring(1,3).equals("KE8")) || pdaScia9000.getScia9000_Pnd_Sg_Subplan_No().getSubstring(1,3).equals("KE9"))) 
                    && pdaScia9000.getScia9000_Pnd_Decedent_Contract().equals(" ")) && pdaScia9000.getScia9000_Pnd_Oneira_Acct_No().equals(" "))))
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"FOREIGN ADDRESS IS NOT ALLOWED FOR INDIVIDUAL PROD. ENROLLMENT");                    //Natural: WRITE SCIA9000.#RETURN-CODE 'FOREIGN ADDRESS IS NOT ALLOWED FOR INDIVIDUAL PROD. ENROLLMENT'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Oneira_Acct_No().greater(" ")))                                                                                     //Natural: IF SCIA9000.#ONEIRA-ACCT-NO GT ' '
            {
                if (condition(pdaScia9000.getScia9000_Pnd_Tiaa_Contract_No().equals(" ")))                                                                                //Natural: IF SCIA9000.#TIAA-CONTRACT-NO = ' '
                {
                    getReports().write(0, "1201","TIAA CONTRACT MUST BE FILLED FOR MANUAL ONEIRA CNTRCT ISSUANCE");                                                       //Natural: WRITE '1201' 'TIAA CONTRACT MUST BE FILLED FOR MANUAL ONEIRA CNTRCT ISSUANCE'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaScia9000.getScia9000_Pnd_Cref_Certificate_No().equals(" ")))                                                                             //Natural: IF SCIA9000.#CREF-CERTIFICATE-NO = ' '
                {
                    getReports().write(0, "1201","CREF CERT NO. MUST BE FILLED FOR MANUAL ONEIRA CNTRCT ISSUANCE");                                                       //Natural: WRITE '1201' 'CREF CERT NO. MUST BE FILLED FOR MANUAL ONEIRA CNTRCT ISSUANCE'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Bypass_Rules().equals("Y") || pdaScia9000.getScia9000_Pnd_Bypass_Rules().equals("N")))                              //Natural: IF SCIA9000.#BYPASS-RULES = 'Y' OR = 'N'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                               //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(3575) BYPASS RULES MUST BE Y/N");                                                       //Natural: WRITE SCIA9000.#RETURN-CODE '(3575) BYPASS RULES MUST BE Y/N'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Processing_Type().equals("AP")))                                                                                    //Natural: IF SCIA9000.#PROCESSING-TYPE = 'AP'
            {
                if (condition(pdaScia9000.getScia9000_Pnd_Pull_Code().equals(" ") || pdaScia9000.getScia9000_Pnd_Pull_Code().equals("A")))                                //Natural: IF SCIA9000.#PULL-CODE = ' ' OR = 'A'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(3630) PULL CODE MUST BE SPACE OR A");                                               //Natural: WRITE SCIA9000.#RETURN-CODE '(3630) PULL CODE MUST BE SPACE OR A'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaScia9000.getScia9000_Pnd_Companion_Ind().equals("Y") || pdaScia9000.getScia9000_Pnd_Companion_Ind().equals("N")))                        //Natural: IF SCIA9000.#COMPANION-IND = 'Y' OR = 'N'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(3680) COMPANION IND MUST BE Y/N");                                                  //Natural: WRITE SCIA9000.#RETURN-CODE '(3680) COMPANION IND MUST BE Y/N'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaScia9000.getScia9000_Pnd_Divorce_Ind().equals("Y") || pdaScia9000.getScia9000_Pnd_Divorce_Ind().equals("N")))                            //Natural: IF SCIA9000.#DIVORCE-IND = 'Y' OR = 'N'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(3725) DIVORCE IND MUST BE Y/N");                                                    //Natural: WRITE SCIA9000.#RETURN-CODE '(3725) DIVORCE IND MUST BE Y/N'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaScia9000.getScia9000_Pnd_Enrollment_Type().equals("A") || pdaScia9000.getScia9000_Pnd_Enrollment_Type().equals("B") ||                   //Natural: IF SCIA9000.#ENROLLMENT-TYPE = 'A' OR = 'B' OR = 'C' OR = 'I' OR = 'R' OR = 'P' OR = 'X' OR = 'N' OR = 'L' OR = 'E' OR = 'T'
                    pdaScia9000.getScia9000_Pnd_Enrollment_Type().equals("C") || pdaScia9000.getScia9000_Pnd_Enrollment_Type().equals("I") || pdaScia9000.getScia9000_Pnd_Enrollment_Type().equals("R") 
                    || pdaScia9000.getScia9000_Pnd_Enrollment_Type().equals("P") || pdaScia9000.getScia9000_Pnd_Enrollment_Type().equals("X") || pdaScia9000.getScia9000_Pnd_Enrollment_Type().equals("N") 
                    || pdaScia9000.getScia9000_Pnd_Enrollment_Type().equals("L") || pdaScia9000.getScia9000_Pnd_Enrollment_Type().equals("E") || pdaScia9000.getScia9000_Pnd_Enrollment_Type().equals("T")))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaScia9000.getScia9000_Pnd_Enrollment_Type().setValue("U");                                                                                          //Natural: MOVE 'U' TO SCIA9000.#ENROLLMENT-TYPE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaScia9000.getScia9000_Pnd_Additional_Cref_Rqst().equals("Y") || pdaScia9000.getScia9000_Pnd_Additional_Cref_Rqst().equals("N")))          //Natural: IF SCIA9000.#ADDITIONAL-CREF-RQST = 'Y' OR = 'N'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(3845) ADDIT'L CREF MUST BE Y/N");                                                   //Natural: WRITE SCIA9000.#RETURN-CODE '(3845) ADDIT"L CREF MUST BE Y/N'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Entry_Date().equals(" ") || pdaScia9000.getScia9000_Pnd_Entry_Date().equals(pnd_Null)))                             //Natural: IF SCIA9000.#ENTRY-DATE = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Entry_Date().setValue(Global.getDATN());                                                                                      //Natural: MOVE *DATN TO SCIA9000.#ENTRY-DATE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Entry_Date().setValue(pdaScia9000.getScia9000_Pnd_Entry_Date(), MoveOption.RightJustified);                                   //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#ENTRY-DATE TO SCIA9000.#ENTRY-DATE
                DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Entry_Date(),true), new ExamineSearch(" "), new ExamineReplace("0"));                       //Natural: EXAMINE FULL SCIA9000.#ENTRY-DATE FOR ' ' REPLACE WITH '0'
                if (condition(pdaScia9000.getScia9000_Pnd_Entry_Date().equals("00000000")))                                                                               //Natural: IF SCIA9000.#ENTRY-DATE = '00000000'
                {
                    pdaScia9000.getScia9000_Pnd_Entry_Date().setValue(Global.getDATN());                                                                                  //Natural: MOVE *DATN TO SCIA9000.#ENTRY-DATE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Entry_Date(),"YYYYMMDD")) || pdaScia9000.getScia9000_Pnd_Entry_Date().less("19000101"))) //Natural: IF SCIA9000.#ENTRY-DATE NE MASK ( YYYYMMDD ) OR SCIA9000.#ENTRY-DATE LT '19000101'
                    {
                        pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                       //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                        getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(3950) ENTRY DATE MUST BE YYYYMMDD");                                            //Natural: WRITE SCIA9000.#RETURN-CODE '(3950) ENTRY DATE MUST BE YYYYMMDD'
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Date_App_Recvd().equals(" ") || pdaScia9000.getScia9000_Pnd_Date_App_Recvd().equals(pnd_Null)))                     //Natural: IF SCIA9000.#DATE-APP-RECVD = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Date_App_Recvd().moveAll("0");                                                                                                //Natural: MOVE ALL '0' TO SCIA9000.#DATE-APP-RECVD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Date_App_Recvd().setValue(pdaScia9000.getScia9000_Pnd_Date_App_Recvd(), MoveOption.RightJustified);                           //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#DATE-APP-RECVD TO SCIA9000.#DATE-APP-RECVD
                DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Date_App_Recvd(),true), new ExamineSearch(" "), new ExamineReplace("0"));                   //Natural: EXAMINE FULL SCIA9000.#DATE-APP-RECVD FOR ' ' REPLACE WITH '0'
                if (condition((pdaScia9000.getScia9000_Pnd_Date_App_Recvd().notEquals("00000000") && (! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Date_App_Recvd(),"YYYYMMDD"))  //Natural: IF SCIA9000.#DATE-APP-RECVD NE '00000000' AND ( SCIA9000.#DATE-APP-RECVD NE MASK ( YYYYMMDD ) OR SCIA9000.#DATE-APP-RECVD LT '19000101' )
                    || pdaScia9000.getScia9000_Pnd_Date_App_Recvd().less("19000101")))))
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(4050) DATE APP RECVD BE YYYYMMDD");                                                 //Natural: WRITE SCIA9000.#RETURN-CODE '(4050) DATE APP RECVD BE YYYYMMDD'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Date_Prem_Recvd().equals(" ") || pdaScia9000.getScia9000_Pnd_Date_Prem_Recvd().equals(pnd_Null)))                   //Natural: IF SCIA9000.#DATE-PREM-RECVD = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Date_Prem_Recvd().moveAll("0");                                                                                               //Natural: MOVE ALL '0' TO SCIA9000.#DATE-PREM-RECVD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Date_Prem_Recvd().setValue(pdaScia9000.getScia9000_Pnd_Date_Prem_Recvd(), MoveOption.RightJustified);                         //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#DATE-PREM-RECVD TO SCIA9000.#DATE-PREM-RECVD
                DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Date_Prem_Recvd(),true), new ExamineSearch(" "), new ExamineReplace("0"));                  //Natural: EXAMINE FULL SCIA9000.#DATE-PREM-RECVD FOR ' ' REPLACE WITH '0'
                if (condition((pdaScia9000.getScia9000_Pnd_Date_Prem_Recvd().notEquals("00000000") && (! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Date_Prem_Recvd(),"YYYYMMDD"))  //Natural: IF SCIA9000.#DATE-PREM-RECVD NE '00000000' AND ( SCIA9000.#DATE-PREM-RECVD NE MASK ( YYYYMMDD ) OR SCIA9000.#DATE-PREM-RECVD LT '19000101' )
                    || pdaScia9000.getScia9000_Pnd_Date_Prem_Recvd().less("19000101")))))
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(4140)DATE PREM RECVD BE YYYYMMDD");                                                 //Natural: WRITE SCIA9000.#RETURN-CODE '(4140)DATE PREM RECVD BE YYYYMMDD'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Annuity_Start_Date().equals(" ") || pdaScia9000.getScia9000_Pnd_Annuity_Start_Date().equals(pnd_Null)))             //Natural: IF SCIA9000.#ANNUITY-START-DATE = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Annuity_Start_Date().moveAll("0");                                                                                            //Natural: MOVE ALL '0' TO SCIA9000.#ANNUITY-START-DATE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Annuity_Start_Date().setValue(pdaScia9000.getScia9000_Pnd_Annuity_Start_Date(), MoveOption.RightJustified);                   //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#ANNUITY-START-DATE TO SCIA9000.#ANNUITY-START-DATE
                DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Annuity_Start_Date(),true), new ExamineSearch(" "), new ExamineReplace("0"));               //Natural: EXAMINE FULL SCIA9000.#ANNUITY-START-DATE FOR ' ' REPLACE WITH '0'
                if (condition((pdaScia9000.getScia9000_Pnd_Annuity_Start_Date().notEquals("00000000") && (! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Annuity_Start_Date(),"YYYYMMDD"))  //Natural: IF SCIA9000.#ANNUITY-START-DATE NE '00000000' AND ( SCIA9000.#ANNUITY-START-DATE NE MASK ( YYYYMMDD ) OR SCIA9000.#ANNUITY-START-DATE LT '19000101' )
                    || pdaScia9000.getScia9000_Pnd_Annuity_Start_Date().less("19000101")))))
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(4225) ANNUITY START MUST BE YYYYMMDD");                                             //Natural: WRITE SCIA9000.#RETURN-CODE '(4225) ANNUITY START MUST BE YYYYMMDD'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Tiaa_Age_1st_Pymnt().equals(" ") || pdaScia9000.getScia9000_Pnd_Tiaa_Age_1st_Pymnt().equals(pnd_Null)))             //Natural: IF SCIA9000.#TIAA-AGE-1ST-PYMNT = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Tiaa_Age_1st_Pymnt().moveAll("0");                                                                                            //Natural: MOVE ALL '0' TO SCIA9000.#TIAA-AGE-1ST-PYMNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Tiaa_Age_1st_Pymnt().setValue(pdaScia9000.getScia9000_Pnd_Tiaa_Age_1st_Pymnt(), MoveOption.RightJustified);                   //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#TIAA-AGE-1ST-PYMNT TO SCIA9000.#TIAA-AGE-1ST-PYMNT
                DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Tiaa_Age_1st_Pymnt(),true), new ExamineSearch(" "), new ExamineReplace("0"));               //Natural: EXAMINE FULL SCIA9000.#TIAA-AGE-1ST-PYMNT FOR ' ' REPLACE WITH '0'
                if (condition(pdaScia9000.getScia9000_Pnd_Tiaa_Age_1st_Pymnt().notEquals("0000") && ! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Tiaa_Age_1st_Pymnt(), //Natural: IF SCIA9000.#TIAA-AGE-1ST-PYMNT NE '0000' AND SCIA9000.#TIAA-AGE-1ST-PYMNT NE MASK ( YYMM )
                    "YYMM"))))
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(4315) TIAA AGE 1ST PYMNT MUST BE YYMM");                                            //Natural: WRITE SCIA9000.#RETURN-CODE '(4315) TIAA AGE 1ST PYMNT MUST BE YYMM'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Cref_Age_1st_Pymnt().equals(" ") || pdaScia9000.getScia9000_Pnd_Cref_Age_1st_Pymnt().equals(pnd_Null)))             //Natural: IF SCIA9000.#CREF-AGE-1ST-PYMNT = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Cref_Age_1st_Pymnt().moveAll("0");                                                                                            //Natural: MOVE ALL '0' TO SCIA9000.#CREF-AGE-1ST-PYMNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Cref_Age_1st_Pymnt().setValue(pdaScia9000.getScia9000_Pnd_Cref_Age_1st_Pymnt(), MoveOption.RightJustified);                   //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#CREF-AGE-1ST-PYMNT TO SCIA9000.#CREF-AGE-1ST-PYMNT
                DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Cref_Age_1st_Pymnt(),true), new ExamineSearch(" "), new ExamineReplace("0"));               //Natural: EXAMINE FULL SCIA9000.#CREF-AGE-1ST-PYMNT FOR ' ' REPLACE WITH '0'
                if (condition(pdaScia9000.getScia9000_Pnd_Cref_Age_1st_Pymnt().notEquals("0000") && ! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Cref_Age_1st_Pymnt(), //Natural: IF SCIA9000.#CREF-AGE-1ST-PYMNT NE '0000' AND SCIA9000.#CREF-AGE-1ST-PYMNT NE MASK ( YYMM )
                    "YYMM"))))
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(4400) CREF AGE 1ST PYMNT MUST BE YYMM");                                            //Natural: WRITE SCIA9000.#RETURN-CODE '(4400) CREF AGE 1ST PYMNT MUST BE YYMM'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Tiaa_Date_Of_Issue().equals(" ") || pdaScia9000.getScia9000_Pnd_Tiaa_Date_Of_Issue().equals(pnd_Null)))             //Natural: IF SCIA9000.#TIAA-DATE-OF-ISSUE = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Tiaa_Date_Of_Issue().moveAll("0");                                                                                            //Natural: MOVE ALL '0' TO SCIA9000.#TIAA-DATE-OF-ISSUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Tiaa_Date_Of_Issue().setValue(pdaScia9000.getScia9000_Pnd_Tiaa_Date_Of_Issue(), MoveOption.RightJustified);                   //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#TIAA-DATE-OF-ISSUE TO SCIA9000.#TIAA-DATE-OF-ISSUE
                DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Tiaa_Date_Of_Issue(),true), new ExamineSearch(" "), new ExamineReplace("0"));               //Natural: EXAMINE FULL SCIA9000.#TIAA-DATE-OF-ISSUE FOR ' ' REPLACE WITH '0'
                if (condition((pdaScia9000.getScia9000_Pnd_Tiaa_Date_Of_Issue().notEquals("00000000") && (! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Tiaa_Date_Of_Issue(),"YYYYMMDD"))  //Natural: IF SCIA9000.#TIAA-DATE-OF-ISSUE NE '00000000' AND ( SCIA9000.#TIAA-DATE-OF-ISSUE NE MASK ( YYYYMMDD ) OR SCIA9000.#TIAA-DATE-OF-ISSUE LT '19000101' )
                    || pdaScia9000.getScia9000_Pnd_Tiaa_Date_Of_Issue().less("19000101")))))
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(4490) TIAA ISSUE DATE MUST BE YYYYMMDD");                                           //Natural: WRITE SCIA9000.#RETURN-CODE '(4490) TIAA ISSUE DATE MUST BE YYYYMMDD'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Cref_Date_Of_Issue().equals(" ") || pdaScia9000.getScia9000_Pnd_Cref_Date_Of_Issue().equals(pnd_Null)))             //Natural: IF SCIA9000.#CREF-DATE-OF-ISSUE = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Cref_Date_Of_Issue().moveAll("0");                                                                                            //Natural: MOVE ALL '0' TO SCIA9000.#CREF-DATE-OF-ISSUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Cref_Date_Of_Issue().setValue(pdaScia9000.getScia9000_Pnd_Cref_Date_Of_Issue(), MoveOption.RightJustified);                   //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#CREF-DATE-OF-ISSUE TO SCIA9000.#CREF-DATE-OF-ISSUE
                DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Cref_Date_Of_Issue(),true), new ExamineSearch(" "), new ExamineReplace("0"));               //Natural: EXAMINE FULL SCIA9000.#CREF-DATE-OF-ISSUE FOR ' ' REPLACE WITH '0'
                if (condition((pdaScia9000.getScia9000_Pnd_Cref_Date_Of_Issue().notEquals("00000000") && (! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Cref_Date_Of_Issue(),"YYYYMMDD"))  //Natural: IF SCIA9000.#CREF-DATE-OF-ISSUE NE '00000000' AND ( SCIA9000.#CREF-DATE-OF-ISSUE NE MASK ( YYYYMMDD ) OR SCIA9000.#CREF-DATE-OF-ISSUE LT '19000101' )
                    || pdaScia9000.getScia9000_Pnd_Cref_Date_Of_Issue().less("19000101")))))
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(4580) CREF ISSUE DATE MUST BE YYYYMMDD");                                           //Natural: WRITE SCIA9000.#RETURN-CODE '(4580) CREF ISSUE DATE MUST BE YYYYMMDD'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((pdaScia9000.getScia9000_Pnd_Lob().equals("I") && ((pdaScia9000.getScia9000_Pnd_Lob_Type().equals("7") || pdaScia9000.getScia9000_Pnd_Lob_Type().equals("8"))  //Natural: IF SCIA9000.#LOB = 'I' AND ( SCIA9000.#LOB-TYPE = '7' OR = '8' OR = '9' ) AND SCIA9000.#ONEIRA-ACCT-NO = ' '
                || pdaScia9000.getScia9000_Pnd_Lob_Type().equals("9"))) && pdaScia9000.getScia9000_Pnd_Oneira_Acct_No().equals(" "))))
            {
                pnd_Entry_Exist.setValue(false);                                                                                                                          //Natural: MOVE FALSE TO #ENTRY-EXIST
                pnd_Search_Key.reset();                                                                                                                                   //Natural: RESET #SEARCH-KEY
                pnd_Search_Key_Pnd_Ky_Entry_Actve_Ind.setValue("Y");                                                                                                      //Natural: ASSIGN #KY-ENTRY-ACTVE-IND = 'Y'
                pnd_Search_Key_Pnd_Ky_Entry_Table_Id_Nbr.setValue(310);                                                                                                   //Natural: ASSIGN #KY-ENTRY-TABLE-ID-NBR = 310
                pnd_Search_Key_Pnd_Ky_Entry_Table_Sub_Id.setValue("PV");                                                                                                  //Natural: ASSIGN #KY-ENTRY-TABLE-SUB-ID = 'PV'
                pnd_Search_Key_Pnd_Ky_Plan_Number.setValue("TIAIDX");                                                                                                     //Natural: ASSIGN #KY-PLAN-NUMBER = 'TIAIDX'
                pnd_Search_Key_Pnd_Ky_Subplan_Number.setValue("040110");                                                                                                  //Natural: ASSIGN #KY-SUBPLAN-NUMBER = '040110'
                vw_app_Table_Entry.startDatabaseRead                                                                                                                      //Natural: READ APP-TABLE-ENTRY WITH ACTVE-IND-TABLE-ID-ENTRY-CDE STARTING FROM #SEARCH-KEY
                (
                "READ02",
                new Wc[] { new Wc("ACTVE_IND_TABLE_ID_ENTRY_CDE", ">=", pnd_Search_Key, WcType.BY) },
                new Oc[] { new Oc("ACTVE_IND_TABLE_ID_ENTRY_CDE", "ASC") }
                );
                READ02:
                while (condition(vw_app_Table_Entry.readNextRow("READ02")))
                {
                    if (condition(app_Table_Entry_Entry_Actve_Ind.notEquals(pnd_Search_Key_Pnd_Ky_Entry_Actve_Ind) || app_Table_Entry_Entry_Table_Id_Nbr.notEquals(pnd_Search_Key_Pnd_Ky_Entry_Table_Id_Nbr)  //Natural: IF APP-TABLE-ENTRY.ENTRY-ACTVE-IND NE #KY-ENTRY-ACTVE-IND OR APP-TABLE-ENTRY.ENTRY-TABLE-ID-NBR NE #KY-ENTRY-TABLE-ID-NBR OR APP-TABLE-ENTRY.ENTRY-TABLE-SUB-ID NE #KY-ENTRY-TABLE-SUB-ID OR APP-TABLE-ENTRY.ENTRY-PLAN NE #KY-PLAN-NUMBER
                        || app_Table_Entry_Entry_Table_Sub_Id.notEquals(pnd_Search_Key_Pnd_Ky_Entry_Table_Sub_Id) || app_Table_Entry_Entry_Plan.notEquals(pnd_Search_Key_Pnd_Ky_Plan_Number)))
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(DbsUtil.maskMatches(app_Table_Entry_Entry_Sub_Plan_Number,"MMDDYY")))                                                                   //Natural: IF APP-TABLE-ENTRY.ENTRY-SUB-PLAN-NUMBER = MASK ( MMDDYY )
                    {
                        pnd_Wk_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "20", app_Table_Entry_Pnd_Rp_Subpl_Yy, app_Table_Entry_Pnd_Rp_Subpl_Mm,      //Natural: COMPRESS '20' #RP-SUBPL-YY #RP-SUBPL-MM #RP-SUBPL-DD INTO #WK-DATE LEAVING NO
                            app_Table_Entry_Pnd_Rp_Subpl_Dd));
                        pnd_Entry_Exist.setValue(true);                                                                                                                   //Natural: MOVE TRUE TO #ENTRY-EXIST
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(pnd_Entry_Exist.getBoolean()))                                                                                                              //Natural: IF #ENTRY-EXIST
                {
                    //*  FOR TESTING
                    if (condition(pdaScia9000.getScia9000_Pnd_Date_App_Recvd().less(pnd_Wk_Date)))                                                                        //Natural: IF SCIA9000.#DATE-APP-RECVD LT #WK-DATE
                    {
                        pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                       //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                        pdaScia9000.getScia9000_Pnd_Return_Msg().setValue(DbsUtil.compress(pdaScia9000.getScia9000_Pnd_Date_App_Recvd(), "-", "APP RECEIVED DATE FOR GUAR INDX PRODUCT LT ",  //Natural: COMPRESS SCIA9000.#DATE-APP-RECVD '-' 'APP RECEIVED DATE FOR GUAR INDX PRODUCT LT ' #WK-DATE INTO SCIA9000.#RETURN-MSG
                            pnd_Wk_Date));
                        getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),pdaScia9000.getScia9000_Pnd_Date_App_Recvd(),"-","APP RECEIVED DATE FOR GUAR INDX PRODUCT LT ", //Natural: WRITE SCIA9000.#RETURN-CODE SCIA9000.#DATE-APP-RECVD '-' 'APP RECEIVED DATE FOR GUAR INDX PRODUCT LT ' #WK-DATE
                            pnd_Wk_Date);
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  FOR PRODUCTION
                    if (condition(pdaScia9000.getScia9000_Pnd_Date_App_Recvd().less("20101001")))                                                                         //Natural: IF SCIA9000.#DATE-APP-RECVD LT '20101001'
                    {
                        pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                       //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                        pdaScia9000.getScia9000_Pnd_Return_Msg().setValue(DbsUtil.compress(pdaScia9000.getScia9000_Pnd_Date_App_Recvd(), "-", "APP RECEIVED DATE FOR GUAR INDX PRODUCT LT 20101001")); //Natural: COMPRESS SCIA9000.#DATE-APP-RECVD '-' 'APP RECEIVED DATE FOR GUAR INDX PRODUCT LT 20101001' INTO SCIA9000.#RETURN-MSG
                        getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),pdaScia9000.getScia9000_Pnd_Date_App_Recvd(),"-","APP RECEIVED DATE FOR GUAR INDX PRODUCT LT 20101001"); //Natural: WRITE SCIA9000.#RETURN-CODE SCIA9000.#DATE-APP-RECVD '-' 'APP RECEIVED DATE FOR GUAR INDX PRODUCT LT 20101001'
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Edit_Flags_Pnd_Edit_Contract_Data.getBoolean()))                                                                                                //Natural: IF #EDIT-CONTRACT-DATA
        {
            short decideConditionsMet4633 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF SCIA9000.#LOAN-IND;//Natural: VALUE ' '
            if (condition((pdaScia9000.getScia9000_Pnd_Loan_Ind().equals(" "))))
            {
                decideConditionsMet4633++;
                pdaScia9000.getScia9000_Pnd_Loan_Ind().setValue("N");                                                                                                     //Natural: MOVE 'N' TO SCIA9000.#LOAN-IND
            }                                                                                                                                                             //Natural: VALUE 'Y','N'
            else if (condition((pdaScia9000.getScia9000_Pnd_Loan_Ind().equals("Y") || pdaScia9000.getScia9000_Pnd_Loan_Ind().equals("N"))))
            {
                decideConditionsMet4633++;
                ignore();
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                               //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(4955) LOAN-IND MUST BE Y/N");                                                           //Natural: WRITE SCIA9000.#RETURN-CODE '(4955) LOAN-IND MUST BE Y/N'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(pdaScia9000.getScia9000_Pnd_Contract_Type().equals(" ") || pdaScia9000.getScia9000_Pnd_Contract_Type().equals(pnd_Null)))                       //Natural: IF SCIA9000.#CONTRACT-TYPE = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Contract_Type().reset();                                                                                                      //Natural: RESET SCIA9000.#CONTRACT-TYPE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Contract_Type()), new ExamineTranslate(TranslateOption.Upper));                             //Natural: EXAMINE SCIA9000.#CONTRACT-TYPE TRANSLATE INTO UPPER CASE
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet4650 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SCIA9000.#CONTRACT-TYPE NE ' '
            if (condition(pdaScia9000.getScia9000_Pnd_Contract_Type().notEquals(" ")))
            {
                decideConditionsMet4650++;
                pdaScia2100.getScia2100_Pnd_Search_Value().reset();                                                                                                       //Natural: RESET #SEARCH-VALUE #S-OTHER-SEARCH-VALUE #W-LOB #W-LOB-TYPE #W-PRODUCT-CODE-N #W-VESTING-CODE-A
                pdaScia2100.getScia2100_Pnd_S_Other_Search_Value().reset();
                pnd_W_Lob.reset();
                pnd_W_Lob_Type.reset();
                pnd_W_Product_Code_N.reset();
                pnd_W_Vesting_Code_N_Pnd_W_Vesting_Code_A.reset();
                pdaScia2100.getScia2100_Pnd_Search_Mode().setValue("AW");                                                                                                 //Natural: MOVE 'AW' TO SCIA2100.#SEARCH-MODE
                pdaScia2100.getScia2100_Pnd_S_Aw_Entry_Actve_Ind().setValue("Y");                                                                                         //Natural: MOVE 'Y' TO SCIA2100.#S-AW-ENTRY-ACTVE-IND
                pdaScia2100.getScia2100_Pnd_S_Aw_Entry_Table_Id_Nbr().setValue(100);                                                                                      //Natural: MOVE 000100 TO SCIA2100.#S-AW-ENTRY-TABLE-ID-NBR
                pdaScia2100.getScia2100_Pnd_S_Aw_Entry_Table_Sub_Id().setValue("LB");                                                                                     //Natural: MOVE 'LB' TO SCIA2100.#S-AW-ENTRY-TABLE-SUB-ID
                pdaScia2100.getScia2100_Pnd_S_Aw_Entry_Cde().setValue(pdaScia9000.getScia9000_Pnd_Contract_Type());                                                       //Natural: MOVE SCIA9000.#CONTRACT-TYPE TO SCIA2100.#S-AW-ENTRY-CDE
                //*  APP-TABLE-ENTRY ('LB' TABLE)
                DbsUtil.callnat(Scin2100.class , getCurrentProcessState(), pdaScia2100.getScia2100(), pnd_Debug);                                                         //Natural: CALLNAT 'SCIN2100' SCIA2100 #DEBUG
                if (condition(Global.isEscape())) return;
                if (condition(pdaScia2100.getScia2100_Pnd_Return_Code().equals(" ")))                                                                                     //Natural: IF SCIA2100.#RETURN-CODE = ' '
                {
                    pnd_W_Lob.setValue(pdaScia2100.getScia2100_Pnd_Lb_Lob());                                                                                             //Natural: MOVE SCIA2100.#LB-LOB TO #W-LOB
                    pnd_W_Lob_Type.setValue(pdaScia2100.getScia2100_Pnd_Lb_Lob_Type());                                                                                   //Natural: MOVE SCIA2100.#LB-LOB-TYPE TO #W-LOB-TYPE
                    pnd_W_Product_Code_N.setValue(pdaScia2100.getScia2100_Pnd_Lb_Product_Code());                                                                         //Natural: MOVE SCIA2100.#LB-PRODUCT-CODE TO #W-PRODUCT-CODE-N
                    pnd_W_Vesting_Code_N_Pnd_W_Vesting_Code_A.setValue(pdaScia2100.getScia2100_Pnd_Lb_Vesting());                                                         //Natural: MOVE SCIA2100.#LB-VESTING TO #W-VESTING-CODE-A
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(8530) CONTRACT TYPE",pdaScia2100.getScia2100_Pnd_S_Aw_Entry_Cde(),                  //Natural: WRITE SCIA9000.#RETURN-CODE '(8530) CONTRACT TYPE' SCIA2100.#S-AW-ENTRY-CDE 'NOT ON "LB" TABLE'
                        "NOT ON 'LB' TABLE");
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                pdaScia9000.getScia9000_Pnd_Lob().setValue(pnd_W_Lob);                                                                                                    //Natural: MOVE #W-LOB TO SCIA9000.#LOB
                pdaScia9000.getScia9000_Pnd_Lob_Type().setValue(pnd_W_Lob_Type);                                                                                          //Natural: MOVE #W-LOB-TYPE TO SCIA9000.#LOB-TYPE
                pdaScia9000.getScia9000_Pnd_Product_Code().setValue(pnd_W_Product_Code_N_Pnd_W_Product_Code_A);                                                           //Natural: MOVE #W-PRODUCT-CODE-A TO SCIA9000.#PRODUCT-CODE
                pdaScia9000.getScia9000_Pnd_Vesting_Code().setValue(pnd_W_Vesting_Code_N_Pnd_W_Vesting_Code_A);                                                           //Natural: MOVE #W-VESTING-CODE-A TO SCIA9000.#VESTING-CODE
            }                                                                                                                                                             //Natural: WHEN SCIA9000.#LOB NE ' ' AND SCIA9000.#LOB-TYPE NE ' ' AND SCIA9000.#VESTING-CODE NE ' '
            else if (condition(pdaScia9000.getScia9000_Pnd_Lob().notEquals(" ") && pdaScia9000.getScia9000_Pnd_Lob_Type().notEquals(" ") && pdaScia9000.getScia9000_Pnd_Vesting_Code().notEquals(" ")))
            {
                decideConditionsMet4650++;
                pdaScia2100.getScia2100_Pnd_Search_Value().reset();                                                                                                       //Natural: RESET #SEARCH-VALUE #S-OTHER-SEARCH-VALUE #W-CONTRACT-TYPE #W-PRODUCT-CODE-N
                pdaScia2100.getScia2100_Pnd_S_Other_Search_Value().reset();
                pnd_W_Contract_Type.reset();
                pnd_W_Product_Code_N.reset();
                //*  BY LOB/LBTYPE
                pdaScia2100.getScia2100_Pnd_Search_Mode().setValue("LB");                                                                                                 //Natural: MOVE 'LB' TO SCIA2100.#SEARCH-MODE
                pdaScia2100.getScia2100_Pnd_S_Lb_Entry_Actve_Ind().setValue("Y");                                                                                         //Natural: MOVE 'Y' TO SCIA2100.#S-LB-ENTRY-ACTVE-IND
                pdaScia2100.getScia2100_Pnd_S_Lb_Entry_Table_Id_Nbr().setValue(100);                                                                                      //Natural: MOVE 000100 TO SCIA2100.#S-LB-ENTRY-TABLE-ID-NBR
                pdaScia2100.getScia2100_Pnd_S_Lb_Entry_Table_Sub_Id().setValue("LB");                                                                                     //Natural: MOVE 'LB' TO SCIA2100.#S-LB-ENTRY-TABLE-SUB-ID
                pdaScia2100.getScia2100_Pnd_S_Lb_Lob().setValue(pdaScia9000.getScia9000_Pnd_Lob());                                                                       //Natural: MOVE SCIA9000.#LOB TO SCIA2100.#S-LB-LOB
                pdaScia2100.getScia2100_Pnd_S_Lb_Lob_Type().setValue(pdaScia9000.getScia9000_Pnd_Lob_Type());                                                             //Natural: MOVE SCIA9000.#LOB-TYPE TO SCIA2100.#S-LB-LOB-TYPE
                pdaScia2100.getScia2100_Pnd_S_Lb_Vesting().setValue(pdaScia9000.getScia9000_Pnd_Vesting_Code());                                                          //Natural: MOVE SCIA9000.#VESTING-CODE TO SCIA2100.#S-LB-VESTING
                //*  APP-TABLE-ENTRY ('LB' TABLE)
                DbsUtil.callnat(Scin2100.class , getCurrentProcessState(), pdaScia2100.getScia2100(), pnd_Debug);                                                         //Natural: CALLNAT 'SCIN2100' SCIA2100 #DEBUG
                if (condition(Global.isEscape())) return;
                if (condition(pdaScia2100.getScia2100_Pnd_Return_Code().equals(" ")))                                                                                     //Natural: IF SCIA2100.#RETURN-CODE = ' '
                {
                    pnd_W_Contract_Type.setValue(pdaScia2100.getScia2100_Pnd_Entry_Cde());                                                                                //Natural: MOVE SCIA2100.#ENTRY-CDE TO #W-CONTRACT-TYPE
                    pnd_W_Product_Code_N.setValue(pdaScia2100.getScia2100_Pnd_Lb_Product_Code());                                                                         //Natural: MOVE SCIA2100.#LB-PRODUCT-CODE TO #W-PRODUCT-CODE-N
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(8695) LOB",pdaScia2100.getScia2100_Pnd_S_Lb_Lob(),"LOBTYPE",pdaScia2100.getScia2100_Pnd_S_Lb_Lob_Type(), //Natural: WRITE SCIA9000.#RETURN-CODE '(8695) LOB' SCIA2100.#S-LB-LOB 'LOBTYPE' SCIA2100.#S-LB-LOB-TYPE 'VESTING' SCIA2100.#S-LB-VESTING 'NOT ON "LB" TABLE'
                        "VESTING",pdaScia2100.getScia2100_Pnd_S_Lb_Vesting(),"NOT ON 'LB' TABLE");
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                pdaScia9000.getScia9000_Pnd_Contract_Type().setValue(pnd_W_Contract_Type);                                                                                //Natural: MOVE #W-CONTRACT-TYPE TO SCIA9000.#CONTRACT-TYPE
                pdaScia9000.getScia9000_Pnd_Product_Code().setValue(pnd_W_Product_Code_N_Pnd_W_Product_Code_A);                                                           //Natural: MOVE #W-PRODUCT-CODE-A TO SCIA9000.#PRODUCT-CODE
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                               //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(5160) CONTRACT TYPE OR LOB/TYPE/VESTING REQUIRED");                                     //Natural: WRITE SCIA9000.#RETURN-CODE '(5160) CONTRACT TYPE OR LOB/TYPE/VESTING REQUIRED'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(pdaScia9000.getScia9000_Pnd_Product_Code().equals(" ") || pdaScia9000.getScia9000_Pnd_Product_Code().equals(pnd_Null)))                         //Natural: IF SCIA9000.#PRODUCT-CODE = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Product_Code().moveAll("0");                                                                                                  //Natural: MOVE ALL '0' TO SCIA9000.#PRODUCT-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Product_Code().setValue(pdaScia9000.getScia9000_Pnd_Product_Code(), MoveOption.RightJustified);                               //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#PRODUCT-CODE TO SCIA9000.#PRODUCT-CODE
                DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Product_Code(),true), new ExamineSearch(" "), new ExamineReplace("0"));                     //Natural: EXAMINE FULL SCIA9000.#PRODUCT-CODE FOR ' ' REPLACE WITH '0'
                if (condition(! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Product_Code(),"NNN"))))                                                                 //Natural: IF SCIA9000.#PRODUCT-CODE NE MASK ( NNN )
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(5235) PRDCT CODE MUST BE NUMERIC");                                                 //Natural: WRITE SCIA9000.#RETURN-CODE '(5235) PRDCT CODE MUST BE NUMERIC'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Vesting_Code().equals("1") || pdaScia9000.getScia9000_Pnd_Vesting_Code().equals("2") || pdaScia9000.getScia9000_Pnd_Vesting_Code().equals("3"))) //Natural: IF SCIA9000.#VESTING-CODE = '1' OR = '2' OR = '3'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                               //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(5290) VESTING CODE MUST BE 1/2/3");                                                     //Natural: WRITE SCIA9000.#RETURN-CODE '(5290) VESTING CODE MUST BE 1/2/3'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            //*  DO NOT EDIT FOR SUNGARD CONTRACTS
            if (condition(pdaScia9000.getScia9000_Pnd_Ppg_Code().notEquals("SGRD")))                                                                                      //Natural: IF SCIA9000.#PPG-CODE NE 'SGRD'
            {
                short decideConditionsMet4735 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF SCIA9000.#VESTING-CODE-OVERRIDE;//Natural: VALUE 'Y'
                if (condition((pdaScia9000.getScia9000_Pnd_Vesting_Code_Override().equals("Y"))))
                {
                    decideConditionsMet4735++;
                    //*  04-15-03 BP
                    if (condition(pdaScia9000.getScia9000_Pnd_System_Requestor().notEquals("ACIBATNE")))                                                                  //Natural: IF SCIA9000.#SYSTEM-REQUESTOR NE 'ACIBATNE'
                    {
                        //*  DELAYED VESTED
                        if (condition(pdaScia9000.getScia9000_Pnd_Vesting_Code().notEquals("2")))                                                                         //Natural: IF SCIA9000.#VESTING-CODE NE '2'
                        {
                            pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                   //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                            getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(5350) VESTING OVERRIDE ALLOWED ONLY FOR DV");                               //Natural: WRITE SCIA9000.#RETURN-CODE '(5350) VESTING OVERRIDE ALLOWED ONLY FOR DV'
                            if (Global.isEscape()) return;
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 'N'
                else if (condition((pdaScia9000.getScia9000_Pnd_Vesting_Code_Override().equals("N"))))
                {
                    decideConditionsMet4735++;
                    ignore();
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(5410) VESTING OVERRIDE MUST BE Y/N");                                               //Natural: WRITE SCIA9000.#RETURN-CODE '(5410) VESTING OVERRIDE MUST BE Y/N'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Curr_Def_Amt().equals(" ") || pdaScia9000.getScia9000_Pnd_Curr_Def_Amt().equals(pnd_Null)))                         //Natural: IF SCIA9000.#CURR-DEF-AMT = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Curr_Def_Amt().moveAll("0");                                                                                                  //Natural: MOVE ALL '0' TO SCIA9000.#CURR-DEF-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Curr_Def_Amt().setValue(pdaScia9000.getScia9000_Pnd_Curr_Def_Amt(), MoveOption.RightJustified);                               //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#CURR-DEF-AMT TO SCIA9000.#CURR-DEF-AMT
                DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Curr_Def_Amt(),true), new ExamineSearch(" "), new ExamineReplace("0"));                     //Natural: EXAMINE FULL SCIA9000.#CURR-DEF-AMT FOR ' ' REPLACE WITH '0'
                if (condition(! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Curr_Def_Amt(),"NNNNNNN"))))                                                             //Natural: IF SCIA9000.#CURR-DEF-AMT NE MASK ( NNNNNNN )
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(5495) AUTO SAVE CURR DEF AMT NOT NUMERIC");                                         //Natural: WRITE SCIA9000.#RETURN-CODE '(5495) AUTO SAVE CURR DEF AMT NOT NUMERIC'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Incr_Amt().equals(" ") || pdaScia9000.getScia9000_Pnd_Incr_Amt().equals(pnd_Null)))                                 //Natural: IF SCIA9000.#INCR-AMT = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Incr_Amt().moveAll("0");                                                                                                      //Natural: MOVE ALL '0' TO SCIA9000.#INCR-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Incr_Amt().setValue(pdaScia9000.getScia9000_Pnd_Incr_Amt(), MoveOption.RightJustified);                                       //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#INCR-AMT TO SCIA9000.#INCR-AMT
                DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Incr_Amt(),true), new ExamineSearch(" "), new ExamineReplace("0"));                         //Natural: EXAMINE FULL SCIA9000.#INCR-AMT FOR ' ' REPLACE WITH '0'
                if (condition(! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Incr_Amt(),"NNNNNNN"))))                                                                 //Natural: IF SCIA9000.#INCR-AMT NE MASK ( NNNNNNN )
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(5565) AUTO SAVE INCR AMT NOT NUMERIC");                                             //Natural: WRITE SCIA9000.#RETURN-CODE '(5565) AUTO SAVE INCR AMT NOT NUMERIC'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Max_Pct().equals(" ") || pdaScia9000.getScia9000_Pnd_Max_Pct().equals(pnd_Null)))                                   //Natural: IF SCIA9000.#MAX-PCT = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Max_Pct().moveAll("0");                                                                                                       //Natural: MOVE ALL '0' TO SCIA9000.#MAX-PCT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Max_Pct().setValue(pdaScia9000.getScia9000_Pnd_Max_Pct(), MoveOption.RightJustified);                                         //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#MAX-PCT TO SCIA9000.#MAX-PCT
                DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Max_Pct(),true), new ExamineSearch(" "), new ExamineReplace("0"));                          //Natural: EXAMINE FULL SCIA9000.#MAX-PCT FOR ' ' REPLACE WITH '0'
                if (condition(! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Max_Pct(),"NNNNN"))))                                                                    //Natural: IF SCIA9000.#MAX-PCT NE MASK ( NNNNN )
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(5335) AUTO SAVE MAX PERCENT NOT NUMERIC");                                          //Natural: WRITE SCIA9000.#RETURN-CODE '(5335) AUTO SAVE MAX PERCENT NOT NUMERIC'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Optout_Days().equals(" ") || pdaScia9000.getScia9000_Pnd_Optout_Days().equals(pnd_Null)))                           //Natural: IF SCIA9000.#OPTOUT-DAYS = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Optout_Days().moveAll("0");                                                                                                   //Natural: MOVE ALL '0' TO SCIA9000.#OPTOUT-DAYS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Optout_Days().setValue(pdaScia9000.getScia9000_Pnd_Optout_Days(), MoveOption.RightJustified);                                 //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#OPTOUT-DAYS TO SCIA9000.#OPTOUT-DAYS
                DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Optout_Days(),true), new ExamineSearch(" "), new ExamineReplace("0"));                      //Natural: EXAMINE FULL SCIA9000.#OPTOUT-DAYS FOR ' ' REPLACE WITH '0'
                if (condition(! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Optout_Days(),"NN"))))                                                                   //Natural: IF SCIA9000.#OPTOUT-DAYS NE MASK ( NN )
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(5705) AUTO ENROLL OUT OUT DAYS NOT NUMERIC");                                       //Natural: WRITE SCIA9000.#RETURN-CODE '(5705) AUTO ENROLL OUT OUT DAYS NOT NUMERIC'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Edit_Flags_Pnd_Edit_Mit_Data.getBoolean()))                                                                                                     //Natural: IF #EDIT-MIT-DATA
        {
            //*  SUNGARD
            if (condition(pdaScia9000.getScia9000_Pnd_Ppg_Code().equals("SGRD")))                                                                                         //Natural: IF SCIA9000.#PPG-CODE = 'SGRD'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaScia9000.getScia9000_Pnd_Mit_Create_Folder().equals("Y") || pdaScia9000.getScia9000_Pnd_Mit_Create_Folder().equals("N")))                //Natural: IF SCIA9000.#MIT-CREATE-FOLDER = 'Y' OR = 'N'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(5820) CREATE MIT FOLDER MUST BY Y/N");                                              //Natural: WRITE SCIA9000.#RETURN-CODE '(5820) CREATE MIT FOLDER MUST BY Y/N'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaScia9000.getScia9000_Pnd_Mit_Unit().equals(" ") || pdaScia9000.getScia9000_Pnd_Mit_Unit().equals(pnd_Null)))                             //Natural: IF SCIA9000.#MIT-UNIT = ' ' OR = #NULL
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(5855) MIT UNIT MUST NOT BE BLANK");                                                 //Natural: WRITE SCIA9000.#RETURN-CODE '(5855) MIT UNIT MUST NOT BE BLANK'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Edit_Flags_Pnd_Edit_Participant_Data.getBoolean()))                                                                                             //Natural: IF #EDIT-PARTICIPANT-DATA
        {
            if (condition(pdaScia9000.getScia9000_Pnd_Pin_Nbr().equals(" ") || pdaScia9000.getScia9000_Pnd_Pin_Nbr().equals(pnd_Null)))                                   //Natural: IF SCIA9000.#PIN-NBR = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Pin_Nbr().moveAll("0");                                                                                                       //Natural: MOVE ALL '0' TO SCIA9000.#PIN-NBR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Pin_Nbr().setValue(pdaScia9000.getScia9000_Pnd_Pin_Nbr(), MoveOption.RightJustified);                                         //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#PIN-NBR TO SCIA9000.#PIN-NBR
                DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Pin_Nbr(),true), new ExamineSearch(" "), new ExamineReplace("0"));                          //Natural: EXAMINE FULL SCIA9000.#PIN-NBR FOR ' ' REPLACE WITH '0'
                //*  PINE
                if (condition(! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Pin_Nbr(),"NNNNNNNNNNNN"))))                                                             //Natural: IF SCIA9000.#PIN-NBR NE MASK ( NNNNNNNNNNNN )
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(5945) PIN IS NON-NUMERIC");                                                         //Natural: WRITE SCIA9000.#RETURN-CODE '(5945) PIN IS NON-NUMERIC'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Ssn().equals(" ") || pdaScia9000.getScia9000_Pnd_Ssn().equals(pnd_Null)))                                           //Natural: IF SCIA9000.#SSN = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Ssn().moveAll("0");                                                                                                           //Natural: MOVE ALL '0' TO SCIA9000.#SSN
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Ssn().setValue(pdaScia9000.getScia9000_Pnd_Ssn(), MoveOption.RightJustified);                                                 //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#SSN TO SCIA9000.#SSN
                DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Ssn(),true), new ExamineSearch(" "), new ExamineReplace("0"));                              //Natural: EXAMINE FULL SCIA9000.#SSN FOR ' ' REPLACE WITH '0'
                if (condition(! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Ssn(),"NNNNNNNNN"))))                                                                    //Natural: IF SCIA9000.#SSN NE MASK ( NNNNNNNNN )
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(6010) SSN IS NON-NUMERIC");                                                         //Natural: WRITE SCIA9000.#RETURN-CODE '(6010) SSN IS NON-NUMERIC'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Last_Name().equals(" ") || pdaScia9000.getScia9000_Pnd_Last_Name().equals(pnd_Null)))                               //Natural: IF SCIA9000.#LAST-NAME = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                               //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(6050) LAST NAME REQUIRED");                                                             //Natural: WRITE SCIA9000.#RETURN-CODE '(6050) LAST NAME REQUIRED'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_First_Name().equals(" ") || pdaScia9000.getScia9000_Pnd_First_Name().equals(pnd_Null)))                             //Natural: IF SCIA9000.#FIRST-NAME = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                               //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(6085) FIRST NAME REQUIRED");                                                            //Natural: WRITE SCIA9000.#RETURN-CODE '(6085) FIRST NAME REQUIRED'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Date_Of_Birth().equals(" ") || pdaScia9000.getScia9000_Pnd_Date_Of_Birth().equals(pnd_Null)))                       //Natural: IF SCIA9000.#DATE-OF-BIRTH = ' ' OR = #NULL
            {
                pdaScia9000.getScia9000_Pnd_Date_Of_Birth().moveAll("0");                                                                                                 //Natural: MOVE ALL '0' TO SCIA9000.#DATE-OF-BIRTH
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Date_Of_Birth().setValue(pdaScia9000.getScia9000_Pnd_Date_Of_Birth(), MoveOption.RightJustified);                             //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#DATE-OF-BIRTH TO SCIA9000.#DATE-OF-BIRTH
                DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Date_Of_Birth(),true), new ExamineSearch(" "), new ExamineReplace("0"));                    //Natural: EXAMINE FULL SCIA9000.#DATE-OF-BIRTH FOR ' ' REPLACE WITH '0'
                if (condition((pdaScia9000.getScia9000_Pnd_Date_Of_Birth().notEquals("00000000") && (! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Date_Of_Birth(),"YYYYMMDD"))  //Natural: IF SCIA9000.#DATE-OF-BIRTH NE '00000000' AND ( SCIA9000.#DATE-OF-BIRTH NE MASK ( YYYYMMDD ) OR SCIA9000.#DATE-OF-BIRTH LT '19000101' )
                    || pdaScia9000.getScia9000_Pnd_Date_Of_Birth().less("19000101")))))
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(6165) DATE OF BIRTH MUST BE YYYYMMDD");                                             //Natural: WRITE SCIA9000.#RETURN-CODE '(6165) DATE OF BIRTH MUST BE YYYYMMDD'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Processing_Type().equals("AP")))                                                                                    //Natural: IF SCIA9000.#PROCESSING-TYPE = 'AP'
            {
                //*  U ADDED BY ELLO 11/25/02
                if (condition(pdaScia9000.getScia9000_Pnd_Sex().equals("M") || pdaScia9000.getScia9000_Pnd_Sex().equals("F") || pdaScia9000.getScia9000_Pnd_Sex().equals("U"))) //Natural: IF SCIA9000.#SEX = 'M' OR = 'F' OR = 'U'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(6225) SEX MUST BE M/F/U");                                                          //Natural: WRITE SCIA9000.#RETURN-CODE '(6225) SEX MUST BE M/F/U'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Last_Name()), new ExamineTranslate(TranslateOption.Upper));                                     //Natural: EXAMINE SCIA9000.#LAST-NAME TRANSLATE INTO UPPER CASE
            DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_First_Name()), new ExamineTranslate(TranslateOption.Upper));                                    //Natural: EXAMINE SCIA9000.#FIRST-NAME TRANSLATE INTO UPPER CASE
            DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Middle_Name()), new ExamineTranslate(TranslateOption.Upper));                                   //Natural: EXAMINE SCIA9000.#MIDDLE-NAME TRANSLATE INTO UPPER CASE
            DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Prefix()), new ExamineTranslate(TranslateOption.Upper));                                        //Natural: EXAMINE SCIA9000.#PREFIX TRANSLATE INTO UPPER CASE
            DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Suffix()), new ExamineTranslate(TranslateOption.Upper));                                        //Natural: EXAMINE SCIA9000.#SUFFIX TRANSLATE INTO UPPER CASE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Edit_Flags_Pnd_Edit_Address_Data.getBoolean()))                                                                                                 //Natural: IF #EDIT-ADDRESS-DATA
        {
            if (condition((((((pdaScia9000.getScia9000_Pnd_Address_Line().getValue(1).equals(" ") || pdaScia9000.getScia9000_Pnd_Address_Line().getValue(1).equals(pnd_Null))  //Natural: IF ( SCIA9000.#ADDRESS-LINE ( 1 ) = ' ' OR = #NULL ) AND ( SCIA9000.#ADDRESS-LINE ( 2 ) = ' ' OR = #NULL ) AND ( SCIA9000.#ADDRESS-LINE ( 3 ) = ' ' OR = #NULL ) AND ( SCIA9000.#ADDRESS-LINE ( 4 ) = ' ' OR = #NULL ) AND ( SCIA9000.#ADDRESS-LINE ( 5 ) = ' ' OR = #NULL )
                && (pdaScia9000.getScia9000_Pnd_Address_Line().getValue(2).equals(" ") || pdaScia9000.getScia9000_Pnd_Address_Line().getValue(2).equals(pnd_Null))) 
                && (pdaScia9000.getScia9000_Pnd_Address_Line().getValue(3).equals(" ") || pdaScia9000.getScia9000_Pnd_Address_Line().getValue(3).equals(pnd_Null))) 
                && (pdaScia9000.getScia9000_Pnd_Address_Line().getValue(4).equals(" ") || pdaScia9000.getScia9000_Pnd_Address_Line().getValue(4).equals(pnd_Null))) 
                && (pdaScia9000.getScia9000_Pnd_Address_Line().getValue(5).equals(" ") || pdaScia9000.getScia9000_Pnd_Address_Line().getValue(5).equals(pnd_Null)))))
            {
                pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                               //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(6350) ADDRESS LINE REQUIRED");                                                          //Natural: WRITE SCIA9000.#RETURN-CODE '(6350) ADDRESS LINE REQUIRED'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            //*  TNGSUB START
            if (condition(pdaScia9000.getScia9000_Pnd_Substitution_Contract_Ind().greater(" ") && (pdaScia9000.getScia9000_Pnd_Conversion_Issue_State().greater(" ")      //Natural: IF SCIA9000.#SUBSTITUTION-CONTRACT-IND > ' ' AND ( SCIA9000.#CONVERSION-ISSUE-STATE > ' ' AND SCIA9000.#CONVERSION-ISSUE-STATE NOT = 'FN' AND SCIA9000.#CONVERSION-ISSUE-STATE NOT = 'CN' )
                && pdaScia9000.getScia9000_Pnd_Conversion_Issue_State().notEquals("FN") && pdaScia9000.getScia9000_Pnd_Conversion_Issue_State().notEquals("CN"))))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaScia9000.getScia9000_Pnd_City().equals(" ") || pdaScia9000.getScia9000_Pnd_City().equals(pnd_Null)))                                     //Natural: IF SCIA9000.#CITY = ' ' OR = #NULL
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(6415) CITY REQUIRED");                                                              //Natural: WRITE SCIA9000.#RETURN-CODE '(6415) CITY REQUIRED'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //*  2 CHAR ALPHA TNGSUB START
                if (condition(! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_State(),"UU"))))                                                                         //Natural: IF SCIA9000.#STATE NE MASK ( UU )
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(6445) STATE REQUIRED IN UPPERCASE");                                                //Natural: WRITE SCIA9000.#RETURN-CODE '(6445) STATE REQUIRED IN UPPERCASE'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Zip_Code().equals(" ") || pdaScia9000.getScia9000_Pnd_Zip_Code().equals(pnd_Null)))                                 //Natural: IF SCIA9000.#ZIP-CODE = ' ' OR = #NULL
            {
                if (condition((pdaScia9000.getScia9000_Pnd_Substitution_Contract_Ind().greater(" ") && ! (((pdaScia9000.getScia9000_Pnd_Conversion_Issue_State().equals(" ")  //Natural: IF SCIA9000.#SUBSTITUTION-CONTRACT-IND > ' ' AND NOT ( SCIA9000.#CONVERSION-ISSUE-STATE = ' ' OR = 'FN' OR = 'CN' )
                    || pdaScia9000.getScia9000_Pnd_Conversion_Issue_State().equals("FN")) || pdaScia9000.getScia9000_Pnd_Conversion_Issue_State().equals("CN"))))))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(6505) ZIP CODE REQUIRED");                                                          //Natural: WRITE SCIA9000.#RETURN-CODE '(6505) ZIP CODE REQUIRED'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Address_Line().getValue(1,":",5)), new ExamineTranslate(TranslateOption.Upper));                //Natural: EXAMINE SCIA9000.#ADDRESS-LINE ( 1:5 ) TRANSLATE INTO UPPER CASE
            DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_City()), new ExamineTranslate(TranslateOption.Upper));                                          //Natural: EXAMINE SCIA9000.#CITY TRANSLATE INTO UPPER CASE
            DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_State()), new ExamineTranslate(TranslateOption.Upper));                                         //Natural: EXAMINE SCIA9000.#STATE TRANSLATE INTO UPPER CASE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaScia9000.getScia9000_Pnd_Email_Address().notEquals(" ")))                                                                                        //Natural: IF SCIA9000.#EMAIL-ADDRESS NE ' '
        {
            pnd_Nbr.reset();                                                                                                                                              //Natural: RESET #NBR
            DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Email_Address(),true), new ExamineSearch("�"), new ExamineGivingNumber(pnd_Nbr));               //Natural: EXAMINE FULL SCIA9000.#EMAIL-ADDRESS FOR '�' GIVING NUMBER #NBR
            if (condition(pnd_Nbr.greater(getZero()) || ! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Email_Address(),"PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP")))) //Natural: IF #NBR > 0 OR ( SCIA9000.#EMAIL-ADDRESS NOT = MASK ( PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP ) )
            {
                pdaScia9000.getScia9000_Pnd_Email_Address().reset();                                                                                                      //Natural: RESET SCIA9000.#EMAIL-ADDRESS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Edit_Flags_Pnd_Edit_Allocation_Data.getBoolean()))                                                                                              //Natural: IF #EDIT-ALLOCATION-DATA
        {
            if (condition(pdaScia9000.getScia9000_Pnd_Allocation_Fmt().equals("N") || pdaScia9000.getScia9000_Pnd_Allocation_Fmt().equals("Y") || pdaScia9000.getScia9000_Pnd_Allocation_Fmt().equals("O"))) //Natural: IF SCIA9000.#ALLOCATION-FMT = 'N' OR = 'Y' OR = 'O'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                               //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(6605) ALLOCATION FORMAT MUST BE N/Y/O");                                                //Natural: WRITE SCIA9000.#RETURN-CODE '(6605) ALLOCATION FORMAT MUST BE N/Y/O'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 100
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(100)); pnd_I.nadd(1))
            {
                if (condition(pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker().getValue(pnd_I).equals(" ") || pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker().getValue(pnd_I).equals(pnd_Null))) //Natural: IF SCIA9000.#ALLOCATION-FUND-TICKER ( #I ) = ' ' OR = #NULL
                {
                    pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker().getValue(pnd_I).reset();                                                                         //Natural: RESET SCIA9000.#ALLOCATION-FUND-TICKER ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker().getValue(pnd_I)), new ExamineTranslate(TranslateOption.Upper)); //Natural: EXAMINE SCIA9000.#ALLOCATION-FUND-TICKER ( #I ) TRANSLATE INTO UPPER CASE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaScia9000.getScia9000_Pnd_Allocation().getValue(pnd_I).equals(" ")))                                                                      //Natural: IF SCIA9000.#ALLOCATION ( #I ) = ' '
                {
                    pdaScia9000.getScia9000_Pnd_Allocation().getValue(pnd_I).moveAll("0");                                                                                //Natural: MOVE ALL '0' TO SCIA9000.#ALLOCATION ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaScia9000.getScia9000_Pnd_Allocation().getValue(pnd_I).setValue(pdaScia9000.getScia9000_Pnd_Allocation().getValue(pnd_I), MoveOption.RightJustified); //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#ALLOCATION ( #I ) TO SCIA9000.#ALLOCATION ( #I )
                    DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Allocation().getValue(pnd_I),true), new ExamineSearch(" "), new ExamineReplace("0"));   //Natural: EXAMINE FULL SCIA9000.#ALLOCATION ( #I ) FOR ' ' REPLACE WITH '0'
                    if (condition(! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Allocation().getValue(pnd_I),"NNN"))))                                               //Natural: IF SCIA9000.#ALLOCATION ( #I ) NE MASK ( NNN )
                    {
                        pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                       //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                        getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(6820) ALLOCATION (",pnd_I,") NOT NUMERIC",pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker().getValue(pnd_I), //Natural: WRITE SCIA9000.#RETURN-CODE '(6820) ALLOCATION (' #I ') NOT NUMERIC' SCIA9000.#ALLOCATION-FUND-TICKER ( #I ) SCIA9000.#ALLOCATION ( #I )
                            pdaScia9000.getScia9000_Pnd_Allocation().getValue(pnd_I));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaScia9000.getScia9000_Pnd_Allocation().getValue(pnd_I).notEquals("000") && pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker().getValue(pnd_I).equals(" "))) //Natural: IF SCIA9000.#ALLOCATION ( #I ) NE '000' AND SCIA9000.#ALLOCATION-FUND-TICKER ( #I ) = ' '
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(6875) ALLOCATION (",pnd_I,") MUST HAVE FUND TICKER");                               //Natural: WRITE SCIA9000.#RETURN-CODE '(6875) ALLOCATION (' #I ') MUST HAVE FUND TICKER'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  IISG START  >>>
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            FOR02:                                                                                                                                                        //Natural: FOR #I = 1 100
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(100)); pnd_I.nadd(1))
            {
                if (condition(pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker_2().getValue(pnd_I).equals(" ") || pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker_2().getValue(pnd_I).equals(pnd_Null))) //Natural: IF SCIA9000.#ALLOCATION-FUND-TICKER-2 ( #I ) = ' ' OR = #NULL
                {
                    pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker_2().getValue(pnd_I).reset();                                                                       //Natural: RESET SCIA9000.#ALLOCATION-FUND-TICKER-2 ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker_2().getValue(pnd_I)), new ExamineTranslate(TranslateOption.Upper)); //Natural: EXAMINE SCIA9000.#ALLOCATION-FUND-TICKER-2 ( #I ) TRANSLATE INTO UPPER CASE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaScia9000.getScia9000_Pnd_Allocation_2().getValue(pnd_I).equals(" ")))                                                                    //Natural: IF SCIA9000.#ALLOCATION-2 ( #I ) = ' '
                {
                    pdaScia9000.getScia9000_Pnd_Allocation_2().getValue(pnd_I).moveAll("0");                                                                              //Natural: MOVE ALL '0' TO SCIA9000.#ALLOCATION-2 ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaScia9000.getScia9000_Pnd_Allocation_2().getValue(pnd_I).setValue(pdaScia9000.getScia9000_Pnd_Allocation_2().getValue(pnd_I), MoveOption.RightJustified); //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#ALLOCATION-2 ( #I ) TO SCIA9000.#ALLOCATION-2 ( #I )
                    DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Allocation_2().getValue(pnd_I),true), new ExamineSearch(" "), new ExamineReplace("0")); //Natural: EXAMINE FULL SCIA9000.#ALLOCATION-2 ( #I ) FOR ' ' REPLACE WITH '0'
                    if (condition(! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Allocation_2().getValue(pnd_I),"NNN"))))                                             //Natural: IF SCIA9000.#ALLOCATION-2 ( #I ) NE MASK ( NNN )
                    {
                        pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                       //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                        getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(6820) ALLOCATION-2 (",pnd_I,") NOT NUMERIC",pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker_2().getValue(pnd_I), //Natural: WRITE SCIA9000.#RETURN-CODE '(6820) ALLOCATION-2 (' #I ') NOT NUMERIC' SCIA9000.#ALLOCATION-FUND-TICKER-2 ( #I ) SCIA9000.#ALLOCATION-2 ( #I )
                            pdaScia9000.getScia9000_Pnd_Allocation_2().getValue(pnd_I));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaScia9000.getScia9000_Pnd_Allocation_2().getValue(pnd_I).notEquals("000") && pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker_2().getValue(pnd_I).equals(" "))) //Natural: IF SCIA9000.#ALLOCATION-2 ( #I ) NE '000' AND SCIA9000.#ALLOCATION-FUND-TICKER-2 ( #I ) = ' '
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(6875) ALLOCATION-2 (",pnd_I,") MUST HAVE FUND TICKER");                             //Natural: WRITE SCIA9000.#RETURN-CODE '(6875) ALLOCATION-2 (' #I ') MUST HAVE FUND TICKER'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  IISG END <<<
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            if (condition(pdaScia9000.getScia9000_Pnd_Alloc_Discrepancy_Ind().equals(" ")))                                                                               //Natural: IF SCIA9000.#ALLOC-DISCREPANCY-IND = ' '
            {
                pdaScia9000.getScia9000_Pnd_Alloc_Discrepancy_Ind().setValue("0");                                                                                        //Natural: MOVE '0' TO SCIA9000.#ALLOC-DISCREPANCY-IND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Edit_Flags_Pnd_Edit_Beneficiary_Data.getBoolean()))                                                                                             //Natural: IF #EDIT-BENEFICIARY-DATA
        {
            if (condition((pdaScia9000.getScia9000_Pnd_Lob().equals("D") && pdaScia9000.getScia9000_Pnd_Lob_Type().equals("6")) || (pdaScia9000.getScia9000_Pnd_Lob().equals("D")  //Natural: IF ( SCIA9000.#LOB = 'D' AND SCIA9000.#LOB-TYPE = '6' ) OR ( SCIA9000.#LOB = 'D' AND SCIA9000.#LOB-TYPE = 'B' ) OR ( SCIA9000.#SUBSTITUTION-CONTRACT-IND > ' ' )
                && pdaScia9000.getScia9000_Pnd_Lob_Type().equals("B")) || (pdaScia9000.getScia9000_Pnd_Substitution_Contract_Ind().greater(" "))))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                FOR03:                                                                                                                                                    //Natural: FOR #I = 1 20
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
                {
                    if (condition(pdaScia9000.getScia9000_Pnd_Bene_Type().getValue(pnd_I).equals(" ") || pdaScia9000.getScia9000_Pnd_Bene_Type().getValue(pnd_I).equals("P")  //Natural: IF SCIA9000.#BENE-TYPE ( #I ) = ' ' OR = 'P' OR = 'C'
                        || pdaScia9000.getScia9000_Pnd_Bene_Type().getValue(pnd_I).equals("C")))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                       //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                        getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(7040) BENE TYPE (",pnd_I,") MUST BE P/C");                                      //Natural: WRITE SCIA9000.#RETURN-CODE '(7040) BENE TYPE (' #I ') MUST BE P/C'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaScia9000.getScia9000_Pnd_Bene_Ssn().getValue(pnd_I).equals(" ") || pdaScia9000.getScia9000_Pnd_Bene_Ssn().getValue(pnd_I).equals(pnd_Null))) //Natural: IF SCIA9000.#BENE-SSN ( #I ) = ' ' OR = #NULL
                    {
                        pdaScia9000.getScia9000_Pnd_Bene_Ssn().getValue(pnd_I).moveAll("0");                                                                              //Natural: MOVE ALL '0' TO SCIA9000.#BENE-SSN ( #I )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaScia9000.getScia9000_Pnd_Bene_Ssn().getValue(pnd_I).setValue(pdaScia9000.getScia9000_Pnd_Bene_Ssn().getValue(pnd_I), MoveOption.RightJustified); //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#BENE-SSN ( #I ) TO SCIA9000.#BENE-SSN ( #I )
                        DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Bene_Ssn().getValue(pnd_I),true), new ExamineSearch(" "), new ExamineReplace("0")); //Natural: EXAMINE FULL SCIA9000.#BENE-SSN ( #I ) FOR ' ' REPLACE WITH '0'
                        if (condition(! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Bene_Ssn().getValue(pnd_I),"NNNNNNNNN"))))                                       //Natural: IF SCIA9000.#BENE-SSN ( #I ) NE MASK ( NNNNNNNNN )
                        {
                            pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                   //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                            getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(7110) BENE SSN (",pnd_I,") NOT NUMERIC",pdaScia9000.getScia9000_Pnd_Bene_Ssn().getValue(pnd_I)); //Natural: WRITE SCIA9000.#RETURN-CODE '(7110) BENE SSN (' #I ') NOT NUMERIC' SCIA9000.#BENE-SSN ( #I )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaScia9000.getScia9000_Pnd_Bene_Name().getValue(pnd_I).equals(" ") || pdaScia9000.getScia9000_Pnd_Bene_Name().getValue(pnd_I).equals(pnd_Null))) //Natural: IF SCIA9000.#BENE-NAME ( #I ) = ' ' OR = #NULL
                    {
                        pdaScia9000.getScia9000_Pnd_Bene_Name().getValue(pnd_I).reset();                                                                                  //Natural: RESET SCIA9000.#BENE-NAME ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaScia9000.getScia9000_Pnd_Bene_Extended_Name().getValue(pnd_I).equals(" ") || pdaScia9000.getScia9000_Pnd_Bene_Extended_Name().getValue(pnd_I).equals(pnd_Null))) //Natural: IF SCIA9000.#BENE-EXTENDED-NAME ( #I ) = ' ' OR = #NULL
                    {
                        pdaScia9000.getScia9000_Pnd_Bene_Extended_Name().getValue(pnd_I).reset();                                                                         //Natural: RESET SCIA9000.#BENE-EXTENDED-NAME ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaScia9000.getScia9000_Pnd_Bene_Relationship().getValue(pnd_I).equals(" ") || pdaScia9000.getScia9000_Pnd_Bene_Relationship().getValue(pnd_I).equals(pnd_Null))) //Natural: IF SCIA9000.#BENE-RELATIONSHIP ( #I ) = ' ' OR = #NULL
                    {
                        pdaScia9000.getScia9000_Pnd_Bene_Relationship().getValue(pnd_I).reset();                                                                          //Natural: RESET SCIA9000.#BENE-RELATIONSHIP ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaScia9000.getScia9000_Pnd_Bene_Date_Of_Birth().getValue(pnd_I).equals(" ") || pdaScia9000.getScia9000_Pnd_Bene_Date_Of_Birth().getValue(pnd_I).equals(pnd_Null))) //Natural: IF SCIA9000.#BENE-DATE-OF-BIRTH ( #I ) = ' ' OR = #NULL
                    {
                        pdaScia9000.getScia9000_Pnd_Bene_Date_Of_Birth().getValue(pnd_I).moveAll("0");                                                                    //Natural: MOVE ALL '0' TO SCIA9000.#BENE-DATE-OF-BIRTH ( #I )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaScia9000.getScia9000_Pnd_Bene_Date_Of_Birth().getValue(pnd_I).setValue(pdaScia9000.getScia9000_Pnd_Bene_Date_Of_Birth().getValue(pnd_I),       //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#BENE-DATE-OF-BIRTH ( #I ) TO SCIA9000.#BENE-DATE-OF-BIRTH ( #I )
                            MoveOption.RightJustified);
                        DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Bene_Date_Of_Birth().getValue(pnd_I),true), new ExamineSearch(" "),                 //Natural: EXAMINE FULL SCIA9000.#BENE-DATE-OF-BIRTH ( #I ) FOR ' ' REPLACE WITH'0'
                            new ExamineReplace("0"));
                        if (condition((pdaScia9000.getScia9000_Pnd_Bene_Date_Of_Birth().getValue(pnd_I).notEquals("00000000") && (! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Bene_Date_Of_Birth().getValue(pnd_I),"YYYYMMDD"))  //Natural: IF SCIA9000.#BENE-DATE-OF-BIRTH ( #I ) NE '00000000' AND ( SCIA9000.#BENE-DATE-OF-BIRTH ( #I ) NE MASK ( YYYYMMDD ) OR SCIA9000.#BENE-DATE-OF-BIRTH ( #I ) LT '19000101' )
                            || pdaScia9000.getScia9000_Pnd_Bene_Date_Of_Birth().getValue(pnd_I).less("19000101")))))
                        {
                            pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                   //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                            getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(7260) BENE DOB (",pnd_I,") NOT YYYYMMDD",pdaScia9000.getScia9000_Pnd_Bene_Date_Of_Birth().getValue(pnd_I)); //Natural: WRITE SCIA9000.#RETURN-CODE '(7260) BENE DOB (' #I ') NOT YYYYMMDD' SCIA9000.#BENE-DATE-OF-BIRTH ( #I )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaScia9000.getScia9000_Pnd_Bene_Alloc_Pct().getValue(pnd_I).equals(" ") || pdaScia9000.getScia9000_Pnd_Bene_Alloc_Pct().getValue(pnd_I).equals(pnd_Null))) //Natural: IF SCIA9000.#BENE-ALLOC-PCT ( #I ) = ' ' OR = #NULL
                    {
                        pdaScia9000.getScia9000_Pnd_Bene_Alloc_Pct().getValue(pnd_I).moveAll("0");                                                                        //Natural: MOVE ALL '0' TO SCIA9000.#BENE-ALLOC-PCT ( #I )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaScia9000.getScia9000_Pnd_Bene_Alloc_Pct().getValue(pnd_I).setValue(pdaScia9000.getScia9000_Pnd_Bene_Alloc_Pct().getValue(pnd_I),               //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#BENE-ALLOC-PCT ( #I ) TO SCIA9000.#BENE-ALLOC-PCT ( #I )
                            MoveOption.RightJustified);
                        DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Bene_Alloc_Pct().getValue(pnd_I),true), new ExamineSearch(" "),                     //Natural: EXAMINE FULL SCIA9000.#BENE-ALLOC-PCT ( #I ) FOR ' ' REPLACE WITH '0'
                            new ExamineReplace("0"));
                        if (condition(! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Bene_Alloc_Pct().getValue(pnd_I),"NNNNN"))))                                     //Natural: IF SCIA9000.#BENE-ALLOC-PCT ( #I ) NE MASK ( NNNNN )
                        {
                            pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                   //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                            getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(7335) BENE ALLOC PERCENT (",pnd_I,") NOT NUMERIC",pdaScia9000.getScia9000_Pnd_Bene_Alloc_Pct().getValue(pnd_I)); //Natural: WRITE SCIA9000.#RETURN-CODE '(7335) BENE ALLOC PERCENT (' #I ') NOT NUMERIC' SCIA9000.#BENE-ALLOC-PCT ( #I )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaScia9000.getScia9000_Pnd_Bene_Numerator().getValue(pnd_I).equals(" ")))                                                              //Natural: IF SCIA9000.#BENE-NUMERATOR ( #I ) = ' '
                    {
                        pdaScia9000.getScia9000_Pnd_Bene_Numerator().getValue(pnd_I).moveAll("0");                                                                        //Natural: MOVE ALL '0' TO SCIA9000.#BENE-NUMERATOR ( #I )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaScia9000.getScia9000_Pnd_Bene_Numerator().getValue(pnd_I).setValue(pdaScia9000.getScia9000_Pnd_Bene_Numerator().getValue(pnd_I),               //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#BENE-NUMERATOR ( #I ) TO SCIA9000.#BENE-NUMERATOR ( #I )
                            MoveOption.RightJustified);
                        DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Bene_Numerator().getValue(pnd_I),true), new ExamineSearch(" "),                     //Natural: EXAMINE FULL SCIA9000.#BENE-NUMERATOR ( #I ) FOR ' ' REPLACE WITH '0'
                            new ExamineReplace("0"));
                        if (condition(! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Bene_Numerator().getValue(pnd_I),"NNN"))))                                       //Natural: IF SCIA9000.#BENE-NUMERATOR ( #I ) NE MASK ( NNN )
                        {
                            pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                   //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                            getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(7410) BENE NUMERATOR (",pnd_I,") NOT NUMERIC",pdaScia9000.getScia9000_Pnd_Bene_Numerator().getValue(pnd_I)); //Natural: WRITE SCIA9000.#RETURN-CODE '(7410) BENE NUMERATOR (' #I ') NOT NUMERIC' SCIA9000.#BENE-NUMERATOR ( #I )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaScia9000.getScia9000_Pnd_Bene_Denominator().getValue(pnd_I).equals(" ")))                                                            //Natural: IF SCIA9000.#BENE-DENOMINATOR ( #I ) = ' '
                    {
                        pdaScia9000.getScia9000_Pnd_Bene_Denominator().getValue(pnd_I).moveAll("0");                                                                      //Natural: MOVE ALL '0' TO SCIA9000.#BENE-DENOMINATOR ( #I )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaScia9000.getScia9000_Pnd_Bene_Denominator().getValue(pnd_I).setValue(pdaScia9000.getScia9000_Pnd_Bene_Denominator().getValue(pnd_I),           //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#BENE-DENOMINATOR ( #I ) TO SCIA9000.#BENE-DENOMINATOR ( #I )
                            MoveOption.RightJustified);
                        DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Bene_Denominator().getValue(pnd_I),true), new ExamineSearch(" "),                   //Natural: EXAMINE FULL SCIA9000.#BENE-DENOMINATOR ( #I ) FOR ' ' REPLACE WITH '0'
                            new ExamineReplace("0"));
                        if (condition(! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Bene_Denominator().getValue(pnd_I),"NNN"))))                                     //Natural: IF SCIA9000.#BENE-DENOMINATOR ( #I ) NE MASK ( NNN )
                        {
                            pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                   //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                            getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(7305) BENE DENOMINATOR (",pnd_I,") NOT NUMERIC",pdaScia9000.getScia9000_Pnd_Bene_Denominator().getValue(pnd_I)); //Natural: WRITE SCIA9000.#RETURN-CODE '(7305) BENE DENOMINATOR (' #I ') NOT NUMERIC' SCIA9000.#BENE-DENOMINATOR ( #I )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaScia9000.getScia9000_Pnd_Bene_Special_Text_Ind().getValue(pnd_I).equals(" ") || pdaScia9000.getScia9000_Pnd_Bene_Special_Text_Ind().getValue(pnd_I).equals("S")  //Natural: IF SCIA9000.#BENE-SPECIAL-TEXT-IND ( #I ) = ' ' OR = 'S' OR = 'T' OR = 'B'
                        || pdaScia9000.getScia9000_Pnd_Bene_Special_Text_Ind().getValue(pnd_I).equals("T") || pdaScia9000.getScia9000_Pnd_Bene_Special_Text_Ind().getValue(pnd_I).equals("B")))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                       //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                        getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(7540) BENE TEXT IND (",pnd_I,") MUST BE S/T/B");                                //Natural: WRITE SCIA9000.#RETURN-CODE '(7540) BENE TEXT IND (' #I ') MUST BE S/T/B'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Bene_Name().getValue(pnd_I)), new ExamineTranslate(TranslateOption.Upper));             //Natural: EXAMINE SCIA9000.#BENE-NAME ( #I ) TRANSLATE INTO UPPER CASE
                    DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Bene_Extended_Name().getValue(pnd_I)), new ExamineTranslate(TranslateOption.Upper));    //Natural: EXAMINE SCIA9000.#BENE-EXTENDED-NAME ( #I ) TRANSLATE INTO UPPER CASE
                    DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Bene_Relationship().getValue(pnd_I)), new ExamineTranslate(TranslateOption.Upper));     //Natural: EXAMINE SCIA9000.#BENE-RELATIONSHIP ( #I ) TRANSLATE INTO UPPER CASE
                    DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Bene_Relationship_Code().getValue(pnd_I)), new ExamineTranslate(TranslateOption.Upper)); //Natural: EXAMINE SCIA9000.#BENE-RELATIONSHIP-CODE ( #I ) TRANSLATE INTO UPPER CASE
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                if (condition(pdaScia9000.getScia9000_Pnd_Bene_Mos_Ind().equals("Y") || pdaScia9000.getScia9000_Pnd_Bene_Mos_Ind().equals("N")))                          //Natural: IF SCIA9000.#BENE-MOS-IND = 'Y' OR = 'N'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(7640) BENE MOS MUST BE Y/N");                                                       //Natural: WRITE SCIA9000.#RETURN-CODE '(7640) BENE MOS MUST BE Y/N'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaScia9000.getScia9000_Pnd_Bene_Estate().equals("Y") || pdaScia9000.getScia9000_Pnd_Bene_Estate().equals("N")))                            //Natural: IF SCIA9000.#BENE-ESTATE = 'Y' OR = 'N'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(7685) BENE ESTATE MUST BE Y/N");                                                    //Natural: WRITE SCIA9000.#RETURN-CODE '(7685) BENE ESTATE MUST BE Y/N'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaScia9000.getScia9000_Pnd_Bene_Trust().equals("Y") || pdaScia9000.getScia9000_Pnd_Bene_Trust().equals("N")))                              //Natural: IF SCIA9000.#BENE-TRUST = 'Y' OR = 'N'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(7730) BENE TRUST MUST BE Y/N");                                                     //Natural: WRITE SCIA9000.#RETURN-CODE '(7730) BENE TRUST MUST BE Y/N'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaScia9000.getScia9000_Pnd_Bene_Category().equals("1") || pdaScia9000.getScia9000_Pnd_Bene_Category().equals("2") || pdaScia9000.getScia9000_Pnd_Bene_Category().equals("3")  //Natural: IF SCIA9000.#BENE-CATEGORY = '1' OR = '2' OR = '3' OR = '4'
                    || pdaScia9000.getScia9000_Pnd_Bene_Category().equals("4")))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                           //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                    getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),"(7775) BENE CATEGORY MUST BE 1/2/3/4");                                              //Natural: WRITE SCIA9000.#RETURN-CODE '(7775) BENE CATEGORY MUST BE 1/2/3/4'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  RTN-1-EDIT-SCIA1201
    }
    private void sub_Rtn_6_Application_Processing() throws Exception                                                                                                      //Natural: RTN-6-APPLICATION-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        //* *----------------------------------------------------------------------
        //*         PROCESS 6A - PROCESS ADDRESS & ALLOCATION DATA (ACIN3000)
        //* *----------------------------------------------------------------------
                                                                                                                                                                          //Natural: PERFORM RTN-6A-PRE-PROCESS-ACIN3000
        sub_Rtn_6a_Pre_Process_Acin3000();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM RTN-6A-PROCESS-ACIN3000
        sub_Rtn_6a_Process_Acin3000();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaScia9000.getScia9000_Pnd_Process_Ind().equals("E")))                                                                                             //Natural: IF SCIA9000.#PROCESS-IND = 'E'
        {
            getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),pdaScia9000.getScia9000_Pnd_Return_Msg());                                                    //Natural: WRITE SCIA9000.#RETURN-CODE SCIA9000.#RETURN-MSG
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM RTN-6A-PROCESS-SCIN3001
        sub_Rtn_6a_Process_Scin3001();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaScia9000.getScia9000_Pnd_Process_Ind().equals("E")))                                                                                             //Natural: IF SCIA9000.#PROCESS-IND = 'E'
        {
            getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),pdaScia9000.getScia9000_Pnd_Return_Msg());                                                    //Natural: WRITE SCIA9000.#RETURN-CODE SCIA9000.#RETURN-MSG
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM RTN-6A-POST-PROCESS-ACIN3000
        sub_Rtn_6a_Post_Process_Acin3000();
        if (condition(Global.isEscape())) {return;}
        //* *----------------------------------------------------------------------
        //*              PROCESS 6B - EDIT BENEFICIARY DATA (ACIN3017)
        //* *----------------------------------------------------------------------
        //*  DTM
        DbsUtil.callnat(Scin8540.class , getCurrentProcessState(), pdaScia9000.getScia9000_Pnd_Sg_Plan_No(), pnd_Non_T_C_Administered_Plan);                              //Natural: CALLNAT 'SCIN8540' SCIA9000.#SG-PLAN-NO #NON-T-C-ADMINISTERED-PLAN
        if (condition(Global.isEscape())) return;
        //*  CHG46842
        //*  BJD RHSP
        //*  CHG46842
        if (condition((pdaScia9000.getScia9000_Pnd_Lob().equals("D") && pdaScia9000.getScia9000_Pnd_Lob_Type().equals("6")) || (pdaScia9000.getScia9000_Pnd_Lob().equals("D")  //Natural: IF ( SCIA9000.#LOB = 'D' AND SCIA9000.#LOB-TYPE = '6' ) OR ( SCIA9000.#LOB = 'D' AND SCIA9000.#LOB-TYPE = 'B' )
            && pdaScia9000.getScia9000_Pnd_Lob_Type().equals("B"))))
        {
            ignore();
            //*  CHG46842
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  MAYO LD
            if (condition(! (pnd_Non_T_C_Administered_Plan.getBoolean())))                                                                                                //Natural: IF NOT #NON-T-C-ADMINISTERED-PLAN
            {
                pdaScia1203.getScia1203_Pnd_Sub_Function().setValue("EDIT");                                                                                              //Natural: MOVE 'EDIT' TO SCIA1203.#SUB-FUNCTION
                                                                                                                                                                          //Natural: PERFORM RTN-6B-PROCESS-BENEFICIARY
                sub_Rtn_6b_Process_Beneficiary();
                if (condition(Global.isEscape())) {return;}
                //*  MAYO LD
            }                                                                                                                                                             //Natural: END-IF
            //*  CHG46842
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaScia9000.getScia9000_Pnd_Process_Ind().equals("E")))                                                                                             //Natural: IF SCIA9000.#PROCESS-IND = 'E'
        {
            getReports().write(0, pdaScia9000.getScia9000_Pnd_Return_Code(),pdaScia9000.getScia9000_Pnd_Return_Msg());                                                    //Natural: WRITE SCIA9000.#RETURN-CODE SCIA9000.#RETURN-MSG
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *----------------------------------------------------------------------
        //*             PROCESS 6D - PROCESS DATABASE UPDATES (ACIN3500)
        //* *----------------------------------------------------------------------
        pdaScia3500.getScia3500_Pnd_Last_Name().setValue(pdaScia9000.getScia9000_Pnd_Last_Name());                                                                        //Natural: MOVE SCIA9000.#LAST-NAME TO SCIA3500.#LAST-NAME
        pdaScia3500.getScia3500_Pnd_First_Name().setValue(pdaScia9000.getScia9000_Pnd_First_Name());                                                                      //Natural: MOVE SCIA9000.#FIRST-NAME TO SCIA3500.#FIRST-NAME
        //*  MOVE SCIA3000 TO SCIA3500
        DbsUtil.callnat(Scin1124.class , getCurrentProcessState(), pdaScia3000.getScia3000(), pdaScia3500.getScia3500(), pdaScia1100.getScia1100(), pnd_K);               //Natural: CALLNAT 'SCIN1124' SCIA3000 SCIA3500 SCIA1100 #K
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM RTN-6D-PROCESS-ACIN3500
        sub_Rtn_6d_Process_Acin3500();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaScia9000.getScia9000_Pnd_Process_Ind().equals("E")))                                                                                             //Natural: IF SCIA9000.#PROCESS-IND = 'E'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Mit_Process_Ind.reset();                                                                                                                                      //Natural: RESET #MIT-PROCESS-IND
        if (condition(pdaScia9000.getScia9000_Pnd_Process_Ind().equals("X")))                                                                                             //Natural: IF SCIA9000.#PROCESS-IND = 'X'
        {
            pnd_Mit_Process_Ind.setValue(pdaScia9000.getScia9000_Pnd_Process_Ind());                                                                                      //Natural: ASSIGN #MIT-PROCESS-IND := SCIA9000.#PROCESS-IND
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM RTN-6D-POST-PROCESS-ACIN3500
        sub_Rtn_6d_Post_Process_Acin3500();
        if (condition(Global.isEscape())) {return;}
        //* *----------------------------------------------------------------------
        //*             PROCESS 6E - UPDATE BENEFICIARY DATA (ACIN3017)
        //* *----------------------------------------------------------------------
        //*  CHG46842
        //*  BJD RHSP
        //*  CHG46842
        if (condition((pdaScia9000.getScia9000_Pnd_Lob().equals("D") && pdaScia9000.getScia9000_Pnd_Lob_Type().equals("6")) || (pdaScia9000.getScia9000_Pnd_Lob().equals("D")  //Natural: IF ( SCIA9000.#LOB = 'D' AND SCIA9000.#LOB-TYPE = '6' ) OR ( SCIA9000.#LOB = 'D' AND SCIA9000.#LOB-TYPE = 'B' )
            && pdaScia9000.getScia9000_Pnd_Lob_Type().equals("B"))))
        {
            ignore();
            //*  CHG46842
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  MAYO LD
            if (condition(! (pnd_Non_T_C_Administered_Plan.getBoolean())))                                                                                                //Natural: IF NOT #NON-T-C-ADMINISTERED-PLAN
            {
                pdaScia1203.getScia1203_Pnd_Sub_Function().setValue("UPDATE/STORE");                                                                                      //Natural: MOVE 'UPDATE/STORE' TO SCIA1203.#SUB-FUNCTION
                                                                                                                                                                          //Natural: PERFORM RTN-6B-PROCESS-BENEFICIARY
                sub_Rtn_6b_Process_Beneficiary();
                if (condition(Global.isEscape())) {return;}
                //*  MAYO LD
            }                                                                                                                                                             //Natural: END-IF
            //*  CHG46842
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaScia9000.getScia9000_Pnd_Process_Ind().equals("E")))                                                                                             //Natural: IF SCIA9000.#PROCESS-IND = 'E'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  MEANS EVERYTHING IS OK EXCEPT MIT
        if (condition(pnd_Mit_Process_Ind.equals("X")))                                                                                                                   //Natural: IF #MIT-PROCESS-IND = 'X'
        {
            pdaScia9000.getScia9000_Pnd_Process_Ind().setValue("E");                                                                                                      //Natural: ASSIGN SCIA9000.#PROCESS-IND := 'E'
        }                                                                                                                                                                 //Natural: END-IF
        //*  RTN-6-APPLICATION-PROCESSING
    }
    private void sub_Rtn_6a_Pre_Process_Acin3000() throws Exception                                                                                                       //Natural: RTN-6A-PRE-PROCESS-ACIN3000
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        pdaScia3530.getScia3530_Aat_Addrss_Line().getValue(1,":",5).setValue(pdaScia9000.getScia9000_Pnd_Address_Line().getValue(1,":",5));                               //Natural: MOVE SCIA9000.#ADDRESS-LINE ( 1:5 ) TO SCIA3530.AAT-ADDRSS-LINE ( 1:5 )
        pdaScia3530.getScia3530_Aat_City().setValue(pdaScia9000.getScia9000_Pnd_City());                                                                                  //Natural: MOVE SCIA9000.#CITY TO SCIA3530.AAT-CITY
        pdaScia3530.getScia3530_Aat_State().setValue(pdaScia9000.getScia9000_Pnd_State());                                                                                //Natural: MOVE SCIA9000.#STATE TO SCIA3530.AAT-STATE
        pdaScia3530.getScia3530_Aat_Zip().setValue(pdaScia9000.getScia9000_Pnd_Zip_Code());                                                                               //Natural: MOVE SCIA9000.#ZIP-CODE TO SCIA3530.AAT-ZIP
        pdaScia3530.getScia3530_Aat_Sex().setValue(pdaScia9000.getScia9000_Pnd_Sex());                                                                                    //Natural: MOVE SCIA9000.#SEX TO SCIA3530.AAT-SEX
        pdaScia3530.getScia3530_Aat_Email_Address().setValue(pdaScia9000.getScia9000_Pnd_Email_Address());                                                                //Natural: MOVE SCIA9000.#EMAIL-ADDRESS TO SCIA3530.AAT-EMAIL-ADDRESS
        //*  OIA VR
        pdaScia3530.getScia3530_Aat_Phone_No().setValue(pdaScia9000.getScia9000_Pnd_Phone());                                                                             //Natural: MOVE SCIA9000.#PHONE TO SCIA3530.AAT-PHONE-NO
        pdaScia3530.getScia3530_Aat_Divorce_Ind().setValue(pdaScia9000.getScia9000_Pnd_Divorce_Ind());                                                                    //Natural: MOVE SCIA9000.#DIVORCE-IND TO SCIA3530.AAT-DIVORCE-IND
        pdaScia3530.getScia3530_Aat_Eop_Addl_Cref_Request().setValue(pdaScia9000.getScia9000_Pnd_Additional_Cref_Rqst());                                                 //Natural: MOVE SCIA9000.#ADDITIONAL-CREF-RQST TO SCIA3530.AAT-EOP-ADDL-CREF-REQUEST
        pdaScia3530.getScia3530_Aat_Allocation_Model_Type().setValue(pdaScia9000.getScia9000_Pnd_Allocation_Model_Type());                                                //Natural: MOVE SCIA9000.#ALLOCATION-MODEL-TYPE TO SCIA3530.AAT-ALLOCATION-MODEL-TYPE
        pdaScia3530.getScia3530_Aat_Rlc_Cref_Cert().setValue(pdaScia9000.getScia9000_Pnd_Rl_Contract_No());                                                               //Natural: MOVE SCIA9000.#RL-CONTRACT-NO TO SCIA3530.AAT-RLC-CREF-CERT
        pdaScia3530.getScia3530_Aat_Allocation_Fmt().setValue(pdaScia9000.getScia9000_Pnd_Allocation_Fmt());                                                              //Natural: MOVE SCIA9000.#ALLOCATION-FMT TO SCIA3530.AAT-ALLOCATION-FMT
        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 100
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(100)); pnd_I.nadd(1))
        {
            pnd_A100_Pnd_A3.setValue(pdaScia9000.getScia9000_Pnd_Allocation().getValue(pnd_I));                                                                           //Natural: MOVE SCIA9000.#ALLOCATION ( #I ) TO #A3
            pdaScia3530.getScia3530_Aat_Allocation_Pct().getValue(pnd_I).setValue(pnd_A100_Pnd_N3);                                                                       //Natural: MOVE #N3 TO SCIA3530.AAT-ALLOCATION-PCT ( #I )
            pdaScia3530.getScia3530_Aat_Fund_Cde().getValue(pnd_I).setValue(pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker().getValue(pnd_I));                        //Natural: MOVE SCIA9000.#ALLOCATION-FUND-TICKER ( #I ) TO SCIA3530.AAT-FUND-CDE ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*                                 /* IISG  STARTS >>>
        pdaScia3530.getScia3530_Aat_Fund_Source_Cde_1().setValue(pdaScia9000.getScia9000_Pnd_Fund_Source_1());                                                            //Natural: MOVE SCIA9000.#FUND-SOURCE-1 TO SCIA3530.AAT-FUND-SOURCE-CDE-1
        pdaScia3530.getScia3530_Aat_Fund_Source_Cde_2().setValue(pdaScia9000.getScia9000_Pnd_Fund_Source_2());                                                            //Natural: MOVE SCIA9000.#FUND-SOURCE-2 TO SCIA3530.AAT-FUND-SOURCE-CDE-2
        FOR05:                                                                                                                                                            //Natural: FOR #I = 1 100
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(100)); pnd_I.nadd(1))
        {
            pnd_A100_Pnd_A3.setValue(pdaScia9000.getScia9000_Pnd_Allocation_2().getValue(pnd_I));                                                                         //Natural: MOVE SCIA9000.#ALLOCATION-2 ( #I ) TO #A3
            pdaScia3530.getScia3530_Aat_Allocation_Pct_2().getValue(pnd_I).setValue(pnd_A100_Pnd_N3);                                                                     //Natural: MOVE #N3 TO SCIA3530.AAT-ALLOCATION-PCT-2 ( #I )
            //*  PRB94362
            pdaScia3530.getScia3530_Aat_Fund_Cde_2().getValue(pnd_I).setValue(pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker_2().getValue(pnd_I));                    //Natural: MOVE SCIA9000.#ALLOCATION-FUND-TICKER-2 ( #I ) TO SCIA3530.AAT-FUND-CDE-2 ( #I )
            //*  IISG  END  <<<
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pdaScia9000.getScia9000_Pnd_Alloc_Discrepancy_Ind().greater(" ")))                                                                                  //Natural: IF SCIA9000.#ALLOC-DISCREPANCY-IND GT ' '
        {
            pdaScia3530.getScia3530_Aat_Alloc_Discr().compute(new ComputeParameters(false, pdaScia3530.getScia3530_Aat_Alloc_Discr()), pdaScia9000.getScia9000_Pnd_Alloc_Discrepancy_Ind().val()); //Natural: ASSIGN SCIA3530.AAT-ALLOC-DISCR := VAL ( SCIA9000.#ALLOC-DISCREPANCY-IND )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Date_App_Recvd(),"YYYYMMDD")))                                                                      //Natural: IF SCIA9000.#DATE-APP-RECVD = MASK ( YYYYMMDD )
        {
            pnd_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pdaScia9000.getScia9000_Pnd_Date_App_Recvd());                                                            //Natural: MOVE EDITED SCIA9000.#DATE-APP-RECVD TO #D ( EM = YYYYMMDD )
            pnd_A100_Pnd_A8.setValueEdited(pnd_D,new ReportEditMask("MMDDYYYY"));                                                                                         //Natural: MOVE EDITED #D ( EM = MMDDYYYY ) TO #A8
            pdaScia3530.getScia3530_Aat_Dt_App_Recvd().setValue(pnd_A100_Pnd_N8);                                                                                         //Natural: MOVE #N8 TO SCIA3530.AAT-DT-APP-RECVD
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3530.getScia3530_Aat_Sgrd_Plan_No().setValue(pdaScia9000.getScia9000_Pnd_Sg_Plan_No());                                                                    //Natural: MOVE SCIA9000.#SG-PLAN-NO TO SCIA3530.AAT-SGRD-PLAN-NO
        pdaScia3530.getScia3530_Aat_Sgrd_Subplan_No().setValue(pdaScia9000.getScia9000_Pnd_Sg_Subplan_No());                                                              //Natural: MOVE SCIA9000.#SG-SUBPLAN-NO TO SCIA3530.AAT-SGRD-SUBPLAN-NO
        pdaScia3530.getScia3530_Aat_Sgrd_Part_Ext().setValue(pdaScia9000.getScia9000_Pnd_Sg_Part_Ext());                                                                  //Natural: MOVE SCIA9000.#SG-PART-EXT TO SCIA3530.AAT-SGRD-PART-EXT
        pdaScia3530.getScia3530_Aat_Sgrd_Divsub().setValue(pdaScia9000.getScia9000_Pnd_Sg_Divsub());                                                                      //Natural: MOVE SCIA9000.#SG-DIVSUB TO SCIA3530.AAT-SGRD-DIVSUB
        pdaScia3530.getScia3530_Aat_Sgrd_Replacement_Ind().setValue(pdaScia9000.getScia9000_Pnd_Sg_Replacement_Ind());                                                    //Natural: MOVE SCIA9000.#SG-REPLACEMENT-IND TO SCIA3530.AAT-SGRD-REPLACEMENT-IND
        pdaScia3530.getScia3530_Aat_Sgrd_Register_Id().setValue(pdaScia9000.getScia9000_Pnd_Sg_Register_Id());                                                            //Natural: MOVE SCIA9000.#SG-REGISTER-ID TO SCIA3530.AAT-SGRD-REGISTER-ID
        pdaScia3530.getScia3530_Aat_Sgrd_Agent_Racf_Id().setValue(pdaScia9000.getScia9000_Pnd_Sg_Agent_Racf_Id());                                                        //Natural: MOVE SCIA9000.#SG-AGENT-RACF-ID TO SCIA3530.AAT-SGRD-AGENT-RACF-ID
        pdaScia3530.getScia3530_Aat_Sgrd_Exempt_Ind().setValue(pdaScia9000.getScia9000_Pnd_Sg_Exempt_Ind());                                                              //Natural: MOVE SCIA9000.#SG-EXEMPT-IND TO SCIA3530.AAT-SGRD-EXEMPT-IND
        pdaScia3530.getScia3530_Aat_Arr_Insurer_1().setValue(pdaScia9000.getScia9000_Pnd_Sg_Insurer_1());                                                                 //Natural: MOVE SCIA9000.#SG-INSURER-1 TO SCIA3530.AAT-ARR-INSURER-1
        pdaScia3530.getScia3530_Aat_Arr_Insurer_2().setValue(pdaScia9000.getScia9000_Pnd_Sg_Insurer_2());                                                                 //Natural: MOVE SCIA9000.#SG-INSURER-2 TO SCIA3530.AAT-ARR-INSURER-2
        pdaScia3530.getScia3530_Aat_Arr_Insurer_3().setValue(pdaScia9000.getScia9000_Pnd_Sg_Insurer_3());                                                                 //Natural: MOVE SCIA9000.#SG-INSURER-3 TO SCIA3530.AAT-ARR-INSURER-3
        pdaScia3530.getScia3530_Aat_Arr_1035_Exch_Ind_1().setValue(pdaScia9000.getScia9000_Pnd_Sg_1035_Exch_Ind_1());                                                     //Natural: MOVE SCIA9000.#SG-1035-EXCH-IND-1 TO SCIA3530.AAT-ARR-1035-EXCH-IND-1
        pdaScia3530.getScia3530_Aat_Arr_1035_Exch_Ind_2().setValue(pdaScia9000.getScia9000_Pnd_Sg_1035_Exch_Ind_2());                                                     //Natural: MOVE SCIA9000.#SG-1035-EXCH-IND-2 TO SCIA3530.AAT-ARR-1035-EXCH-IND-2
        pdaScia3530.getScia3530_Aat_Arr_1035_Exch_Ind_3().setValue(pdaScia9000.getScia9000_Pnd_Sg_1035_Exch_Ind_3());                                                     //Natural: MOVE SCIA9000.#SG-1035-EXCH-IND-3 TO SCIA3530.AAT-ARR-1035-EXCH-IND-3
        if (condition(pdaScia9000.getScia9000_Pnd_Irc_Section_Code().greater(" ")))                                                                                       //Natural: IF SCIA9000.#IRC-SECTION-CODE GT ' '
        {
            pdaScia3530.getScia3530_Aat_Irc_Sectn_Cde().compute(new ComputeParameters(false, pdaScia3530.getScia3530_Aat_Irc_Sectn_Cde()), pdaScia9000.getScia9000_Pnd_Irc_Section_Code().val()); //Natural: ASSIGN SCIA3530.AAT-IRC-SECTN-CDE := VAL ( SCIA9000.#IRC-SECTION-CODE )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaScia9000.getScia9000_Pnd_Irc_Section_Grp_Code().greater(" ")))                                                                                   //Natural: IF SCIA9000.#IRC-SECTION-GRP-CODE GT ' '
        {
            pdaScia3530.getScia3530_Aat_Irc_Sectn_Grp_Cde().compute(new ComputeParameters(false, pdaScia3530.getScia3530_Aat_Irc_Sectn_Grp_Cde()), pdaScia9000.getScia9000_Pnd_Irc_Section_Grp_Code().val()); //Natural: ASSIGN SCIA3530.AAT-IRC-SECTN-GRP-CDE := VAL ( SCIA9000.#IRC-SECTION-GRP-CODE )
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3530.getScia3530_Aat_Address_Txt().getValue(1,":",3).setValue(pdaScia9000.getScia9000_Pnd_Res_Address_Line().getValue(1,":",3));                           //Natural: MOVE SCIA9000.#RES-ADDRESS-LINE ( 1:3 ) TO SCIA3530.AAT-ADDRESS-TXT ( 1:3 )
        pdaScia3530.getScia3530_Aat_Address_Txt().getValue(4).setValue(pdaScia9000.getScia9000_Pnd_Res_City());                                                           //Natural: MOVE SCIA9000.#RES-CITY TO SCIA3530.AAT-ADDRESS-TXT ( 4 )
        pdaScia3530.getScia3530_Aat_Address_Txt().getValue(5).setValue(pdaScia9000.getScia9000_Pnd_Res_State());                                                          //Natural: MOVE SCIA9000.#RES-STATE TO SCIA3530.AAT-ADDRESS-TXT ( 5 )
        //*  B.E. 3/16
        pdaScia3530.getScia3530_Aat_Na_Code().setValue(pdaScia9000.getScia9000_Pnd_Res_Country_Code());                                                                   //Natural: MOVE SCIA9000.#RES-COUNTRY-CODE TO SCIA3530.AAT-NA-CODE
        pdaScia3530.getScia3530_Aat_Zip_Code().setValue(pdaScia9000.getScia9000_Pnd_Res_Zip_Code());                                                                      //Natural: MOVE SCIA9000.#RES-ZIP-CODE TO SCIA3530.AAT-ZIP-CODE
        pdaScia3530.getScia3530_Aat_Bank_Pymnt_Acct_Nmbr().setValue(pdaScia9000.getScia9000_Pnd_Res_Phone());                                                             //Natural: MOVE SCIA9000.#RES-PHONE TO SCIA3530.AAT-BANK-PYMNT-ACCT-NMBR
        pdaScia3530.getScia3530_Aat_Cai_Ind().setValue(pdaScia9000.getScia9000_Pnd_Ica_Ind());                                                                            //Natural: MOVE SCIA9000.#ICA-IND TO SCIA3530.AAT-CAI-IND
        pdaScia3530.getScia3530_Aat_Cip().setValue(pdaScia9000.getScia9000_Pnd_Cip_Ind());                                                                                //Natural: MOVE SCIA9000.#CIP-IND TO SCIA3530.AAT-CIP
        pdaScia3530.getScia3530_Aat_Erisa_Ind().setValue(pdaScia9000.getScia9000_Pnd_Erisa_Ind());                                                                        //Natural: MOVE SCIA9000.#ERISA-IND TO SCIA3530.AAT-ERISA-IND
        pdaScia3530.getScia3530_Aat_Mail_Instructions().setValue(pdaScia9000.getScia9000_Pnd_Mailing_Instr());                                                            //Natural: MOVE SCIA9000.#MAILING-INSTR TO SCIA3530.AAT-MAIL-INSTRUCTIONS
        pdaScia3530.getScia3530_Aat_Rqst_Log_Dte_Time().setValue(pdaScia9000.getScia9000_Pnd_Mit_Log_Date_Time_Parent());                                                 //Natural: MOVE SCIA9000.#MIT-LOG-DATE-TIME-PARENT TO SCIA3530.AAT-RQST-LOG-DTE-TIME
        pdaScia3530.getScia3530_Aat_Ppg_Team_Cde().setValue(pdaScia9000.getScia9000_Pnd_Mit_Unit());                                                                      //Natural: MOVE SCIA9000.#MIT-UNIT TO SCIA3530.AAT-PPG-TEAM-CDE
        pdaScia9000.getScia9000_Pnd_Sg_Plan_Id().setValue(pdaScia9000.getScia9000_Pnd_Sg_Plan_Id(), MoveOption.RightJustified);                                           //Natural: MOVE RIGHT SCIA9000.#SG-PLAN-ID TO SCIA9000.#SG-PLAN-ID
        DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Sg_Plan_Id(),true), new ExamineSearch(" "), new ExamineReplace("0"));                               //Natural: EXAMINE FULL SCIA9000.#SG-PLAN-ID FOR ' ' REPLACE WITH '0'
        if (condition(DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Sg_Plan_Id(),"NNNNNN")))                                                                            //Natural: IF SCIA9000.#SG-PLAN-ID = MASK ( NNNNNN )
        {
            pdaScia3530.getScia3530_Aat_Inst_Link_Cde().compute(new ComputeParameters(false, pdaScia3530.getScia3530_Aat_Inst_Link_Cde()), pdaScia9000.getScia9000_Pnd_Sg_Plan_Id().val()); //Natural: ASSIGN SCIA3530.AAT-INST-LINK-CDE := VAL ( SCIA9000.#SG-PLAN-ID )
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3530.getScia3530_Aat_Omni_Account_Issuance().setValue(pdaScia9000.getScia9000_Pnd_Omni_Account_Issuance());                                                //Natural: MOVE SCIA9000.#OMNI-ACCOUNT-ISSUANCE TO SCIA3530.AAT-OMNI-ACCOUNT-ISSUANCE
        pdaScia3530.getScia3530_Aat_Oia_Ind().setValue(pdaScia9000.getScia9000_Pnd_Oia_Ind());                                                                            //Natural: MOVE SCIA9000.#OIA-IND TO SCIA3530.AAT-OIA-IND
        pdaScia3530.getScia3530_Aat_Text_Udf_1().setValue(pdaScia9000.getScia9000_Pnd_Sg_Text_Udf_1());                                                                   //Natural: MOVE SCIA9000.#SG-TEXT-UDF-1 TO SCIA3530.AAT-TEXT-UDF-1
        pdaScia3530.getScia3530_Aat_Text_Udf_2().setValue(pdaScia9000.getScia9000_Pnd_Sg_Text_Udf_2());                                                                   //Natural: MOVE SCIA9000.#SG-TEXT-UDF-2 TO SCIA3530.AAT-TEXT-UDF-2
        pdaScia3530.getScia3530_Aat_Text_Udf_3().setValue(pdaScia9000.getScia9000_Pnd_Sg_Text_Udf_3());                                                                   //Natural: MOVE SCIA9000.#SG-TEXT-UDF-3 TO SCIA3530.AAT-TEXT-UDF-3
        pdaScia3530.getScia3530_Aat_Incmpl_Acct_Ind().setValue(pdaScia9000.getScia9000_Pnd_Incmpl_Acct_Ind());                                                            //Natural: ASSIGN SCIA3530.AAT-INCMPL-ACCT-IND := SCIA9000.#INCMPL-ACCT-IND
        pdaScia3530.getScia3530_Aat_Autosave_Ind().setValue(pdaScia9000.getScia9000_Pnd_As_Ind());                                                                        //Natural: ASSIGN SCIA3530.AAT-AUTOSAVE-IND := SCIA9000.#AS-IND
        pdaScia3530.getScia3530_Aat_As_Cur_Dflt_Opt().setValue(pdaScia9000.getScia9000_Pnd_Curr_Def_Option());                                                            //Natural: ASSIGN SCIA3530.AAT-AS-CUR-DFLT-OPT := SCIA9000.#CURR-DEF-OPTION
        pdaScia3530.getScia3530_Aat_As_Cur_Dflt_Amt().compute(new ComputeParameters(false, pdaScia3530.getScia3530_Aat_As_Cur_Dflt_Amt()), pdaScia9000.getScia9000_Pnd_Curr_Def_Amt().val().divide(100)); //Natural: ASSIGN SCIA3530.AAT-AS-CUR-DFLT-AMT := VAL ( SCIA9000.#CURR-DEF-AMT ) / 100
        pdaScia3530.getScia3530_Aat_As_Incr_Opt().setValue(pdaScia9000.getScia9000_Pnd_Incr_Option());                                                                    //Natural: ASSIGN SCIA3530.AAT-AS-INCR-OPT := SCIA9000.#INCR-OPTION
        pdaScia3530.getScia3530_Aat_As_Incr_Amt().compute(new ComputeParameters(false, pdaScia3530.getScia3530_Aat_As_Incr_Amt()), pdaScia9000.getScia9000_Pnd_Incr_Amt().val().divide(100)); //Natural: ASSIGN SCIA3530.AAT-AS-INCR-AMT := VAL ( SCIA9000.#INCR-AMT ) / 100
        pdaScia3530.getScia3530_Aat_As_Max_Pct().compute(new ComputeParameters(false, pdaScia3530.getScia3530_Aat_As_Max_Pct()), pdaScia9000.getScia9000_Pnd_Max_Pct().val().divide(100)); //Natural: ASSIGN SCIA3530.AAT-AS-MAX-PCT := VAL ( SCIA9000.#MAX-PCT ) / 100
        pdaScia3530.getScia3530_Aat_Ae_Opt_Out_Days().compute(new ComputeParameters(false, pdaScia3530.getScia3530_Aat_Ae_Opt_Out_Days()), pdaScia9000.getScia9000_Pnd_Optout_Days().val()); //Natural: ASSIGN SCIA3530.AAT-AE-OPT-OUT-DAYS := VAL ( SCIA9000.#OPTOUT-DAYS )
        pdaScia3530.getScia3530_Aat_Orchestration_Id().setValue(pdaScia9000.getScia9000_Pnd_Orchestration_Id());                                                          //Natural: ASSIGN SCIA3530.AAT-ORCHESTRATION-ID := SCIA9000.#ORCHESTRATION-ID
        pdaScia3530.getScia3530_Aat_Mail_Addr_Country_Cd().setValue(pdaScia9000.getScia9000_Pnd_Mail_Addr_Country_Cd());                                                  //Natural: ASSIGN SCIA3530.AAT-MAIL-ADDR-COUNTRY-CD := SCIA9000.#MAIL-ADDR-COUNTRY-CD
        pdaScia3530.getScia3530_Aat_Substitution_Contract_Ind().setValue(pdaScia9000.getScia9000_Pnd_Substitution_Contract_Ind());                                        //Natural: ASSIGN SCIA3530.AAT-SUBSTITUTION-CONTRACT-IND := SCIA9000.#SUBSTITUTION-CONTRACT-IND
        pdaScia3530.getScia3530_Aat_Conv_Issue_State().setValue(pdaScia9000.getScia9000_Pnd_Conversion_Issue_State());                                                    //Natural: ASSIGN SCIA3530.AAT-CONV-ISSUE-STATE := SCIA9000.#CONVERSION-ISSUE-STATE
        pdaScia3530.getScia3530_Aat_Deceased_Ind().setValue(pdaScia9000.getScia9000_Pnd_Deceased_Ind());                                                                  //Natural: ASSIGN SCIA3530.AAT-DECEASED-IND := SCIA9000.#DECEASED-IND
        pdaScia3530.getScia3530_Aat_Non_Proprietary_Pkg_Ind().setValue(pdaScia9000.getScia9000_Pnd_Non_Proprietary_Pkg_Ind());                                            //Natural: ASSIGN SCIA3530.AAT-NON-PROPRIETARY-PKG-IND := SCIA9000.#NON-PROPRIETARY-PKG-IND
        if (condition(pdaScia9000.getScia9000_Pnd_Annuity_Start_Date().greater(" ")))                                                                                     //Natural: IF SCIA9000.#ANNUITY-START-DATE GT ' '
        {
            pdaScia3530.getScia3530_Aat_Annuity_Start_Date().compute(new ComputeParameters(false, pdaScia3530.getScia3530_Aat_Annuity_Start_Date()), pdaScia9000.getScia9000_Pnd_Annuity_Start_Date().val()); //Natural: ASSIGN SCIA3530.AAT-ANNUITY-START-DATE := VAL ( SCIA9000.#ANNUITY-START-DATE )
            //*  LPAO
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3530.getScia3530_Aat_Legal_Ann_Option().setValue(pdaScia9000.getScia9000_Pnd_Legal_Ann_Option());                                                          //Natural: ASSIGN SCIA3530.AAT-LEGAL-ANN-OPTION := SCIA9000.#LEGAL-ANN-OPTION
        //*  KEOGH
        //*  GA  CRS
        //*  TNT CRS
        //*  TNT CRS
        //*  TNT CRS
        //*  ICAP CRS
        //*  ICAP CRS
        //*  DCA  CRS
        if (condition((pdaScia9000.getScia9000_Pnd_Lob().equals("I") && pdaScia9000.getScia9000_Pnd_Lob_Type().equals("5")) || (pdaScia9000.getScia9000_Pnd_Lob().equals("S")  //Natural: IF ( SCIA9000.#LOB = 'I' AND SCIA9000.#LOB-TYPE = '5' ) OR ( SCIA9000.#LOB = 'S' AND SCIA9000.#LOB-TYPE = '3' ) OR ( SCIA9000.#LOB = 'S' AND SCIA9000.#LOB-TYPE = '4' ) OR ( SCIA9000.#LOB = 'S' AND SCIA9000.#LOB-TYPE = '5' ) OR ( SCIA9000.#LOB = 'S' AND SCIA9000.#LOB-TYPE = '6' ) OR ( SCIA9000.#LOB = 'D' AND SCIA9000.#LOB-TYPE = '5' ) OR ( SCIA9000.#LOB = 'D' AND SCIA9000.#LOB-TYPE = '6' ) OR ( SCIA9000.#LOB = 'D' AND SCIA9000.#LOB-TYPE = '7' ) OR ( SCIA9000.#LOB = 'D' AND SCIA9000.#LOB-TYPE = 'A' ) OR ( SCIA9000.#LOB = 'S' AND SCIA9000.#LOB-TYPE = '7' ) OR ( SCIA9000.#LOB = 'S' AND SCIA9000.#LOB-TYPE = '9' )
            && pdaScia9000.getScia9000_Pnd_Lob_Type().equals("3")) || (pdaScia9000.getScia9000_Pnd_Lob().equals("S") && pdaScia9000.getScia9000_Pnd_Lob_Type().equals("4")) 
            || (pdaScia9000.getScia9000_Pnd_Lob().equals("S") && pdaScia9000.getScia9000_Pnd_Lob_Type().equals("5")) || (pdaScia9000.getScia9000_Pnd_Lob().equals("S") 
            && pdaScia9000.getScia9000_Pnd_Lob_Type().equals("6")) || (pdaScia9000.getScia9000_Pnd_Lob().equals("D") && pdaScia9000.getScia9000_Pnd_Lob_Type().equals("5")) 
            || (pdaScia9000.getScia9000_Pnd_Lob().equals("D") && pdaScia9000.getScia9000_Pnd_Lob_Type().equals("6")) || (pdaScia9000.getScia9000_Pnd_Lob().equals("D") 
            && pdaScia9000.getScia9000_Pnd_Lob_Type().equals("7")) || (pdaScia9000.getScia9000_Pnd_Lob().equals("D") && pdaScia9000.getScia9000_Pnd_Lob_Type().equals("A")) 
            || (pdaScia9000.getScia9000_Pnd_Lob().equals("S") && pdaScia9000.getScia9000_Pnd_Lob_Type().equals("7")) || (pdaScia9000.getScia9000_Pnd_Lob().equals("S") 
            && pdaScia9000.getScia9000_Pnd_Lob_Type().equals("9"))))
        {
            pdaScia3530.getScia3530_Aat_Coll_St_Cd().setValue(pdaScia9000.getScia9000_Pnd_Issue_State());                                                                 //Natural: ASSIGN SCIA3530.AAT-COLL-ST-CD := SCIA9000.#ISSUE-STATE
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3530.getScia3530_Aat_Decedent_Contract().setValue(pdaScia9000.getScia9000_Pnd_Decedent_Contract());                                                        //Natural: ASSIGN SCIA3530.AAT-DECEDENT-CONTRACT := SCIA9000.#DECEDENT-CONTRACT
        pdaScia3530.getScia3530_Aat_Relation_To_Decedent().setValue(pdaScia9000.getScia9000_Pnd_Relation_To_Decedent());                                                  //Natural: ASSIGN SCIA3530.AAT-RELATION-TO-DECEDENT := SCIA9000.#RELATION-TO-DECEDENT
        pdaScia3530.getScia3530_Aat_Ssn_Tin_Ind().setValue(pdaScia9000.getScia9000_Pnd_Ssn_Tin_Ind());                                                                    //Natural: ASSIGN SCIA3530.AAT-SSN-TIN-IND := SCIA9000.#SSN-TIN-IND
        //*  MOVE SCIA1100 TO SCIA3000
        //*  OIA VR
        DbsUtil.callnat(Scin1122.class , getCurrentProcessState(), pdaScia3000.getScia3000(), pdaScia1100.getScia1100(), pdaScia3530.getScia3530(), pdaScia3530.getScia3530_Id(),  //Natural: CALLNAT 'SCIN1122' SCIA3000 SCIA1100 SCIA3530 SCIA3530-ID SCIA3531 SCIA3531-ID SCIA1203 DIALOG-INFO-SUB MSG-INFO-SUB #K SCIA9000.#SYSTEM-REQUESTOR SCIA9000.#RACF-ID
            pdaScia3531.getScia3531(), pdaScia3531.getScia3531_Id(), pdaScia1203.getScia1203(), pdaAcipda_D.getDialog_Info_Sub(), pdaAcipda_M.getMsg_Info_Sub(), 
            pnd_K, pdaScia9000.getScia9000_Pnd_System_Requestor(), pdaScia9000.getScia9000_Pnd_Racf_Id());
        if (condition(Global.isEscape())) return;
        if (condition(pdaScia9000.getScia9000_Pnd_System_Requestor().notEquals("ACIBATNE")))                                                                              //Natural: IF SCIA9000.#SYSTEM-REQUESTOR NE 'ACIBATNE'
        {
            //*  OVERRIDE DELAYED VESTING
            if (condition(pdaScia9000.getScia9000_Pnd_Vesting_Code_Override().equals("Y")))                                                                               //Natural: IF SCIA9000.#VESTING-CODE-OVERRIDE = 'Y'
            {
                //*  TO IMMEDIATE VESTING
                pdaScia3000.getScia3000_Pnd_Pnd_Pnd_Vesting().setValue(1);                                                                                                //Natural: MOVE 1 TO SCIA3000.###VESTING
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  OVERRIDE DELAYED VESTING
            if (condition(pdaScia9000.getScia9000_Pnd_Vesting_Code_Override().equals("Y")))                                                                               //Natural: IF SCIA9000.#VESTING-CODE-OVERRIDE = 'Y'
            {
                short decideConditionsMet5420 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SCIA9000.#FIELD2 = '1'
                if (condition(pdaScia9000.getScia9000_Pnd_Field2().equals("1")))
                {
                    decideConditionsMet5420++;
                    pdaScia3000.getScia3000_Pnd_Pnd_Pnd_Vesting().setValue(1);                                                                                            //Natural: ASSIGN SCIA3000.###VESTING := 1
                }                                                                                                                                                         //Natural: WHEN SCIA9000.#FIELD2 = '2'
                else if (condition(pdaScia9000.getScia9000_Pnd_Field2().equals("2")))
                {
                    decideConditionsMet5420++;
                    pdaScia3000.getScia3000_Pnd_Pnd_Pnd_Vesting().setValue(2);                                                                                            //Natural: ASSIGN SCIA3000.###VESTING := 2
                }                                                                                                                                                         //Natural: WHEN SCIA9000.#FIELD2 = '3'
                else if (condition(pdaScia9000.getScia9000_Pnd_Field2().equals("3")))
                {
                    decideConditionsMet5420++;
                    pdaScia3000.getScia3000_Pnd_Pnd_Pnd_Vesting().setValue(3);                                                                                            //Natural: ASSIGN SCIA3000.###VESTING := 3
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1203.getScia1203_Pnd_Return_Msg_Fld2().setValue(pdaScia9000.getScia9000_Pnd_Field3());                                                                 //Natural: ASSIGN SCIA1203.#RETURN-MSG-FLD2 := SCIA9000.#FIELD3
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3530.getScia3530_Aat_Conv_Issue_State().setValue(pdaScia9000.getScia9000_Pnd_Conversion_Issue_State());                                                    //Natural: ASSIGN SCIA3530.AAT-CONV-ISSUE-STATE := SCIA9000.#CONVERSION-ISSUE-STATE
        //*  RTN-6A-PRE-PROCESS-ACIN3000
    }
    private void sub_Rtn_6a_Process_Acin3000() throws Exception                                                                                                           //Natural: RTN-6A-PROCESS-ACIN3000
    {
        if (BLNatReinput.isReinput()) return;

        pdaScia3530.getScia3530_Aat_Addr_Usage_Code().setValue(pnd_Cip);                                                                                                  //Natural: ASSIGN SCIA3530.AAT-ADDR-USAGE-CODE := #CIP
        //*  OIA VR
        //*  TNGSUB LS
        //*  TNGSUB LS
        DbsUtil.callnat(Scin3000.class , getCurrentProcessState(), pdaAcipda_D.getDialog_Info_Sub(), pdaAcipda_M.getMsg_Info_Sub(), pdaScia3000.getScia3000(),            //Natural: CALLNAT 'SCIN3000' DIALOG-INFO-SUB MSG-INFO-SUB SCIA3000 SCIA3530 SCIA3530-ID SCIA3531 SCIA3531-ID SCIA3535 SCIA1203 #NEW-ALLOC-FORMAT SCIA1100.SUPERVISOR #DISPLAY-FLAG SCIA9000.#SYSTEM-REQUESTOR SCIA9000.#RACF-ID SCIA9000.#ENROLLMENT-TYPE SCIA9000.#LOAN-IND SCIA9000.#SUBSTITUTION-CONTRACT-IND SCIA9000.#CREF-DATE-OF-ISSUE #DEBUG
            pdaScia3530.getScia3530(), pdaScia3530.getScia3530_Id(), pdaScia3531.getScia3531(), pdaScia3531.getScia3531_Id(), pdaScia3535.getScia3535(), 
            pdaScia1203.getScia1203(), pnd_New_Alloc_Format, pdaScia1100.getScia1100_Supervisor(), pnd_Display_Flag, pdaScia9000.getScia9000_Pnd_System_Requestor(), 
            pdaScia9000.getScia9000_Pnd_Racf_Id(), pdaScia9000.getScia9000_Pnd_Enrollment_Type(), pdaScia9000.getScia9000_Pnd_Loan_Ind(), pdaScia9000.getScia9000_Pnd_Substitution_Contract_Ind(), 
            pdaScia9000.getScia9000_Pnd_Cref_Date_Of_Issue(), pnd_Debug);
        if (condition(Global.isEscape())) return;
        //*  ERROR
        if (condition(pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E")))                                                                                     //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E'
        {
            pdaScia9000.getScia9000_Pnd_Return_Code().setValue("3000");                                                                                                   //Natural: MOVE '3000' TO SCIA9000.#RETURN-CODE
            pdaScia9000.getScia9000_Pnd_Return_Msg().setValue(pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                 //Natural: MOVE MSG-INFO-SUB.##MSG TO SCIA9000.#RETURN-MSG
            pdaScia9000.getScia9000_Pnd_Process_Ind().setValue("E");                                                                                                      //Natural: MOVE 'E' TO SCIA9000.#PROCESS-IND
        }                                                                                                                                                                 //Natural: END-IF
        //*  RTN-6A-PROCESS-ACIN3000
    }
    private void sub_Rtn_6a_Process_Scin3001() throws Exception                                                                                                           //Natural: RTN-6A-PROCESS-SCIN3001
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        //*  OIA VR
        DbsUtil.callnat(Scin3001.class , getCurrentProcessState(), pdaAcipda_D.getDialog_Info_Sub(), pdaAcipda_M.getMsg_Info_Sub(), pdaScia3000.getScia3000(),            //Natural: CALLNAT 'SCIN3001' DIALOG-INFO-SUB MSG-INFO-SUB SCIA3000 SCIA3530 SCIA3530-ID SCIA3531 SCIA3531-ID SCIA3535 SCIA1203 SCIA9000.#SYSTEM-REQUESTOR SCIA9000.#RACF-ID SCIA9000.#ENROLLMENT-TYPE SCIA9000.#LOAN-IND #DEBUG
            pdaScia3530.getScia3530(), pdaScia3530.getScia3530_Id(), pdaScia3531.getScia3531(), pdaScia3531.getScia3531_Id(), pdaScia3535.getScia3535(), 
            pdaScia1203.getScia1203(), pdaScia9000.getScia9000_Pnd_System_Requestor(), pdaScia9000.getScia9000_Pnd_Racf_Id(), pdaScia9000.getScia9000_Pnd_Enrollment_Type(), 
            pdaScia9000.getScia9000_Pnd_Loan_Ind(), pnd_Debug);
        if (condition(Global.isEscape())) return;
        //*  ERROR
        if (condition(pdaScia3000.getScia3000_Pnd_Pnd_Pnd_Process_Error_Ind().equals("E")))                                                                               //Natural: IF SCIA3000.###PROCESS-ERROR-IND = 'E'
        {
            pdaScia9000.getScia9000_Pnd_Return_Code().setValue("3001");                                                                                                   //Natural: MOVE '3001' TO SCIA9000.#RETURN-CODE
            pdaScia9000.getScia9000_Pnd_Return_Msg().setValue(pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                 //Natural: MOVE MSG-INFO-SUB.##MSG TO SCIA9000.#RETURN-MSG
            pdaScia9000.getScia9000_Pnd_Process_Ind().setValue("E");                                                                                                      //Natural: MOVE 'E' TO SCIA9000.#PROCESS-IND
        }                                                                                                                                                                 //Natural: END-IF
        //*  RTN-6A-PROCESS-SCIN3001
    }
    private void sub_Rtn_6a_Post_Process_Acin3000() throws Exception                                                                                                      //Natural: RTN-6A-POST-PROCESS-ACIN3000
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        //*                                                          /* BULK VR \/
        pdaScia9000.getScia9000_Pnd_Address_Line().getValue(1,":",5).setValue(pdaScia3530.getScia3530_Aat_Addrss_Line().getValue(1,":",5));                               //Natural: MOVE SCIA3530.AAT-ADDRSS-LINE ( 1:5 ) TO SCIA9000.#ADDRESS-LINE ( 1:5 )
        pdaScia9000.getScia9000_Pnd_City().setValue(pdaScia3530.getScia3530_Aat_City());                                                                                  //Natural: MOVE SCIA3530.AAT-CITY TO SCIA9000.#CITY
        pdaScia9000.getScia9000_Pnd_State().setValue(pdaScia3530.getScia3530_Aat_State());                                                                                //Natural: MOVE SCIA3530.AAT-STATE TO SCIA9000.#STATE
        pdaScia9000.getScia9000_Pnd_Zip_Code().setValue(pdaScia3530.getScia3530_Aat_Zip());                                                                               //Natural: MOVE SCIA3530.AAT-ZIP TO SCIA9000.#ZIP-CODE
        //*                                                          /* BULK VR /\
        pdaScia9000.getScia9000_Pnd_Allocation_Model_Type().setValue(pdaScia3530.getScia3530_Aat_Allocation_Model_Type());                                                //Natural: MOVE SCIA3530.AAT-ALLOCATION-MODEL-TYPE TO SCIA9000.#ALLOCATION-MODEL-TYPE
        pnd_A100_Pnd_N1.setValue(pdaScia3530.getScia3530_Aat_Alloc_Discr());                                                                                              //Natural: MOVE SCIA3530.AAT-ALLOC-DISCR TO #N1
        pdaScia9000.getScia9000_Pnd_Alloc_Discrepancy_Ind().setValue(pnd_A100_Pnd_A1);                                                                                    //Natural: MOVE #A1 TO SCIA9000.#ALLOC-DISCREPANCY-IND
        //*  OIA
        //*  OIA (CRS) \/
        pdaScia9000.getScia9000_Pnd_Allocation_Fmt().setValue(pdaScia3530.getScia3530_Aat_Allocation_Fmt());                                                              //Natural: MOVE SCIA3530.AAT-ALLOCATION-FMT TO SCIA9000.#ALLOCATION-FMT
        FOR06:                                                                                                                                                            //Natural: FOR #I = 1 100
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(100)); pnd_I.nadd(1))
        {
            pnd_A100_Pnd_N3.setValue(pdaScia3530.getScia3530_Aat_Allocation_Pct().getValue(pnd_I));                                                                       //Natural: MOVE SCIA3530.AAT-ALLOCATION-PCT ( #I ) TO #N3
            pdaScia9000.getScia9000_Pnd_Allocation().getValue(pnd_I).setValue(pnd_A100_Pnd_A3);                                                                           //Natural: MOVE #A3 TO SCIA9000.#ALLOCATION ( #I )
            pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker().getValue(pnd_I).setValue(pdaScia3530.getScia3530_Aat_Fund_Cde().getValue(pnd_I));                        //Natural: MOVE SCIA3530.AAT-FUND-CDE ( #I ) TO SCIA9000.#ALLOCATION-FUND-TICKER ( #I )
            //*  OIA (CRS) /\
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*                                                  /* IISG START >>>>
        pdaScia9000.getScia9000_Pnd_Fund_Source_1().setValue(pdaScia3530.getScia3530_Aat_Fund_Source_Cde_1());                                                            //Natural: MOVE SCIA3530.AAT-FUND-SOURCE-CDE-1 TO SCIA9000.#FUND-SOURCE-1
        pdaScia9000.getScia9000_Pnd_Fund_Source_2().setValue(pdaScia3530.getScia3530_Aat_Fund_Source_Cde_2());                                                            //Natural: MOVE SCIA3530.AAT-FUND-SOURCE-CDE-2 TO SCIA9000.#FUND-SOURCE-2
        FOR07:                                                                                                                                                            //Natural: FOR #I = 1 100
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(100)); pnd_I.nadd(1))
        {
            pnd_A100_Pnd_N3.setValue(pdaScia3530.getScia3530_Aat_Allocation_Pct_2().getValue(pnd_I));                                                                     //Natural: MOVE SCIA3530.AAT-ALLOCATION-PCT-2 ( #I ) TO #N3
            pdaScia9000.getScia9000_Pnd_Allocation_2().getValue(pnd_I).setValue(pnd_A100_Pnd_A3);                                                                         //Natural: MOVE #A3 TO SCIA9000.#ALLOCATION-2 ( #I )
            pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker_2().getValue(pnd_I).setValue(pdaScia3530.getScia3530_Aat_Fund_Cde_2().getValue(pnd_I));                    //Natural: MOVE SCIA3530.AAT-FUND-CDE-2 ( #I ) TO SCIA9000.#ALLOCATION-FUND-TICKER-2 ( #I )
            //*  IISG END <<<
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_A100_Pnd_N8.setValue(pdaScia3530.getScia3530_Aat_Annuity_Start_Date());                                                                                       //Natural: MOVE SCIA3530.AAT-ANNUITY-START-DATE TO #N8
        pdaScia9000.getScia9000_Pnd_Annuity_Start_Date().setValue(pnd_A100_Pnd_A8);                                                                                       //Natural: MOVE #A8 TO SCIA9000.#ANNUITY-START-DATE
        pnd_A100_Pnd_N4.setValue(pdaScia3530.getScia3530_Aat_T_1st_Pymt());                                                                                               //Natural: MOVE SCIA3530.AAT-T-1ST-PYMT TO #N4
        pdaScia9000.getScia9000_Pnd_Tiaa_Age_1st_Pymnt().setValue(pnd_A100_Pnd_A4);                                                                                       //Natural: MOVE #A4 TO SCIA9000.#TIAA-AGE-1ST-PYMNT
        pnd_A100_Pnd_N4.setValue(pdaScia3530.getScia3530_Aat_C_1st_Pymt());                                                                                               //Natural: MOVE SCIA3530.AAT-C-1ST-PYMT TO #N4
        pdaScia9000.getScia9000_Pnd_Cref_Age_1st_Pymnt().setValue(pnd_A100_Pnd_A4);                                                                                       //Natural: MOVE #A4 TO SCIA9000.#CREF-AGE-1ST-PYMNT
        if (condition(DbsUtil.maskMatches(pdaScia3530.getScia3530_Aat_T_Doi(),"MMDDYY")))                                                                                 //Natural: IF SCIA3530.AAT-T-DOI = MASK ( MMDDYY )
        {
            pnd_A100_Pnd_N6.setValue(pdaScia3530.getScia3530_Aat_T_Doi());                                                                                                //Natural: MOVE SCIA3530.AAT-T-DOI TO #N6
            pnd_D.setValueEdited(new ReportEditMask("MMDDYY"),pnd_A100_Pnd_A6);                                                                                           //Natural: MOVE EDITED #A6 TO #D ( EM = MMDDYY )
            pdaScia9000.getScia9000_Pnd_Tiaa_Date_Of_Issue().setValueEdited(pnd_D,new ReportEditMask("YYYYMMDD"));                                                        //Natural: MOVE EDITED #D ( EM = YYYYMMDD ) TO SCIA9000.#TIAA-DATE-OF-ISSUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(pdaScia3530.getScia3530_Aat_C_Doi(),"MMDDYY")))                                                                                 //Natural: IF SCIA3530.AAT-C-DOI = MASK ( MMDDYY )
        {
            pnd_A100_Pnd_N6.setValue(pdaScia3530.getScia3530_Aat_C_Doi());                                                                                                //Natural: MOVE SCIA3530.AAT-C-DOI TO #N6
            pnd_D.setValueEdited(new ReportEditMask("MMDDYY"),pnd_A100_Pnd_A6);                                                                                           //Natural: MOVE EDITED #A6 TO #D ( EM = MMDDYY )
            pdaScia9000.getScia9000_Pnd_Cref_Date_Of_Issue().setValueEdited(pnd_D,new ReportEditMask("YYYYMMDD"));                                                        //Natural: MOVE EDITED #D ( EM = YYYYMMDD ) TO SCIA9000.#CREF-DATE-OF-ISSUE
        }                                                                                                                                                                 //Natural: END-IF
        //*  RTN-6A-POST-PROCESS-ACIN3000
    }
    private void sub_Rtn_6b_Process_Beneficiary() throws Exception                                                                                                        //Natural: RTN-6B-PROCESS-BENEFICIARY
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        if (condition(pdaScia9000.getScia9000_Pnd_Substitution_Contract_Ind().equals("W") || pdaScia9000.getScia9000_Pnd_Substitution_Contract_Ind().equals("O")          //Natural: IF SCIA9000.#SUBSTITUTION-CONTRACT-IND = 'W' OR = 'O' OR SCIA9000.#DECEDENT-CONTRACT GT ' '
            || pdaScia9000.getScia9000_Pnd_Decedent_Contract().greater(" ")))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia1200.getScia1200().setValuesByName(pdaScia9000.getScia9000());                                                                                         //Natural: MOVE BY NAME SCIA9000 TO SCIA1200
            DbsUtil.callnat(Scin3017.class , getCurrentProcessState(), pdaScia1203.getScia1203(), pdaScia1200.getScia1200(), pnd_Debug);                                  //Natural: CALLNAT 'SCIN3017' SCIA1203 SCIA1200 #DEBUG
            if (condition(Global.isEscape())) return;
            pdaScia9000.getScia9000().setValuesByName(pdaScia1200.getScia1200());                                                                                         //Natural: MOVE BY NAME SCIA1200 TO SCIA9000
        }                                                                                                                                                                 //Natural: END-IF
        //*  RTN-6B-PROCESS-BENEFICIARY
    }
    private void sub_Rtn_6d_Process_Acin3500() throws Exception                                                                                                           //Natural: RTN-6D-PROCESS-ACIN3500
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        if (condition(pdaScia9000.getScia9000_Pnd_System_Requestor().equals("ACIBATIO") || pdaScia9000.getScia9000_Pnd_System_Requestor().equals("ACIBATSG")))            //Natural: IF SCIA9000.#SYSTEM-REQUESTOR EQ 'ACIBATIO' OR = 'ACIBATSG'
        {
            if (condition(pdaScia9000.getScia9000_Pnd_Ica_Ind().equals("Y")))                                                                                             //Natural: IF SCIA9000.#ICA-IND = 'Y'
            {
                pnd_Cor_Msg_Pnd_Callnat_Cai_Ind.setValue("Y");                                                                                                            //Natural: ASSIGN #CALLNAT-CAI-IND := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cor_Msg_Pnd_Callnat_Cai_Ind.setValue(" ");                                                                                                            //Natural: ASSIGN #CALLNAT-CAI-IND := ' '
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1203.getScia1203_Pnd_Return_Msg_Fld3().setValue(pnd_Cor_Msg);                                                                                          //Natural: ASSIGN SCIA1203.#RETURN-MSG-FLD3 := #COR-MSG
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Region().setValue("2");                                                                                                       //Natural: MOVE '2' TO SCIA3500.###REGION
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Need().setValue("R");                                                                                                         //Natural: MOVE 'R' TO SCIA3500.###NEED
        //*  COMPANION
        if (condition(pdaScia9000.getScia9000_Pnd_Companion_Ind().equals("Y")))                                                                                           //Natural: IF SCIA9000.#COMPANION-IND = 'Y'
        {
            if (condition(pdaScia9000.getScia9000_Pnd_Tiaa_Date_Of_Issue().greater(" ")))                                                                                 //Natural: IF SCIA9000.#TIAA-DATE-OF-ISSUE GT ' '
            {
                pnd_Issue_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pdaScia9000.getScia9000_Pnd_Tiaa_Date_Of_Issue());                                           //Natural: MOVE EDITED SCIA9000.#TIAA-DATE-OF-ISSUE TO #ISSUE-DATE ( EM = YYYYMMDD )
                pnd_Issue_Date_A.setValueEdited(pnd_Issue_Date,new ReportEditMask("MMYY"));                                                                               //Natural: MOVE EDITED #ISSUE-DATE ( EM = MMYY ) TO #ISSUE-DATE-A
                pdaScia3500.getScia3500_Pnd_Pnd_Pnd_T_Doi().setValue(pnd_Issue_Date_A_Pnd_Issue_Date_N);                                                                  //Natural: MOVE #ISSUE-DATE-N TO SCIA3500.###T-DOI
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Cref_Date_Of_Issue().greater(" ")))                                                                                 //Natural: IF SCIA9000.#CREF-DATE-OF-ISSUE GT ' '
            {
                pnd_Issue_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pdaScia9000.getScia9000_Pnd_Cref_Date_Of_Issue());                                           //Natural: MOVE EDITED SCIA9000.#CREF-DATE-OF-ISSUE TO #ISSUE-DATE ( EM = YYYYMMDD )
                pnd_Issue_Date_A.setValueEdited(pnd_Issue_Date,new ReportEditMask("MMYY"));                                                                               //Natural: MOVE EDITED #ISSUE-DATE ( EM = MMYY ) TO #ISSUE-DATE-A
                pdaScia3500.getScia3500_Pnd_Pnd_Pnd_C_Doi().setValue(pnd_Issue_Date_A_Pnd_Issue_Date_N);                                                                  //Natural: MOVE #ISSUE-DATE-N TO SCIA3500.###C-DOI
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cor_Msg.setValue(pdaScia1203.getScia1203_Pnd_Return_Msg_Fld3());                                                                                              //Natural: ASSIGN #COR-MSG := SCIA1203.#RETURN-MSG-FLD3
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Pin().compute(new ComputeParameters(false, pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Pin()), pdaScia9000.getScia9000_Pnd_Pin_Nbr().val()); //Natural: ASSIGN SCIA3500.###PIN := VAL ( SCIA9000.#PIN-NBR )
        pnd_System_Date.setValue(Global.getDATN());                                                                                                                       //Natural: ASSIGN #SYSTEM-DATE := *DATN
        pnd_Current_Date_Pnd_Cur_Yy.setValue(pnd_System_Date_Pnd_Sys_Yy);                                                                                                 //Natural: ASSIGN #CUR-YY := #SYS-YY
        pnd_Current_Date_Pnd_Cur_Mmdd.setValue(pnd_System_Date_Pnd_Sys_Mmdd);                                                                                             //Natural: ASSIGN #CUR-MMDD := #SYS-MMDD
        pdaScia3560.getScia3560_Pnd_Start_Date().setValue(Global.getDATN());                                                                                              //Natural: ASSIGN #START-DATE := *DATN
        pnd_System_Temp_Date.setValue(pnd_System_Date_Pnd_Sys_Year);                                                                                                      //Natural: MOVE #SYS-YEAR TO #SYSTEM-TEMP-DATE
        pnd_System_Date_Pnd_Sys_Year.setValue(pnd_System_Date_Pnd_Sys_Mmdd);                                                                                              //Natural: MOVE #SYS-MMDD TO #SYS-YEAR
        pnd_System_Date_Pnd_Sys_Mmdd.setValue(pnd_System_Temp_Date);                                                                                                      //Natural: MOVE #SYSTEM-TEMP-DATE TO #SYS-MMDD
        pdaScia3530.getScia3530_Aat_Coll_Code().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Srce_Cd());                                                                  //Natural: ASSIGN SCIA3530.AAT-COLL-CODE := SCIA3500.###SRCE-CD
        pdaScia3530.getScia3530_Aat_Pin_Nbr().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Pin());                                                                        //Natural: ASSIGN SCIA3530.AAT-PIN-NBR := SCIA3500.###PIN
        pdaScia3530.getScia3530_Aat_Lob().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Lob());                                                                            //Natural: ASSIGN SCIA3530.AAT-LOB := SCIA3500.###LOB
        pdaScia3530.getScia3530_Aat_Lob_Type().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Lob_Type());                                                                  //Natural: ASSIGN SCIA3530.AAT-LOB-TYPE := SCIA3500.###LOB-TYPE
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("GET");                                                                                                               //Natural: MOVE 'GET' TO CDAOBJ.#FUNCTION
        if (condition(pdaScia1203.getScia1203_Pnd_Func_Accptd_Applctn().equals("N")))                                                                                     //Natural: IF SCIA1203.#FUNC-ACCPTD-APPLCTN = 'N'
        {
            pdaAcipda_M.getMsg_Info_Sub().reset();                                                                                                                        //Natural: RESET MSG-INFO-SUB
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.callnat(Scin3530.class , getCurrentProcessState(), pdaScia3530.getScia3530(), pdaScia3530.getScia3530_Id(), pdaScia3535.getScia3535(),                //Natural: CALLNAT 'SCIN3530' SCIA3530 SCIA3530-ID SCIA3535 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB
                pdaCdaobj.getCdaobj(), pdaAcipda_D.getDialog_Info_Sub(), pdaAcipda_M.getMsg_Info_Sub());
            if (condition(Global.isEscape())) return;
            if (condition(pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().notEquals(" ")))                                                                              //Natural: IF MSG-INFO-SUB.##ERROR-FIELD NE ' ' THEN
            {
                pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("* PROBLEM WITH ANNTY-ACTVTY *", pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg(),           //Natural: COMPRESS '* PROBLEM WITH ANNTY-ACTVTY *' MSG-INFO-SUB.##MSG MSG-INFO-SUB.##ERROR-FIELD INTO MSG-INFO-SUB.##MSG
                    pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field()));
                getReports().write(0, pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                         //Natural: WRITE MSG-INFO-SUB.##MSG
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaScia3530.getScia3530_Aat_Sgrd_Subplan_No().getSubstring(1,3).equals("RS1") || pdaScia3530.getScia3530_Aat_Sgrd_Subplan_No().getSubstring(1,3).equals("RS2")  //Natural: IF SUBSTRING ( SCIA3530.AAT-SGRD-SUBPLAN-NO,1,3 ) = 'RS1' OR = 'RS2' OR = 'RS3' OR = 'RP1' OR = 'RP2' OR = 'RP3'
            || pdaScia3530.getScia3530_Aat_Sgrd_Subplan_No().getSubstring(1,3).equals("RS3") || pdaScia3530.getScia3530_Aat_Sgrd_Subplan_No().getSubstring(1,3).equals("RP1") 
            || pdaScia3530.getScia3530_Aat_Sgrd_Subplan_No().getSubstring(1,3).equals("RP2") || pdaScia3530.getScia3530_Aat_Sgrd_Subplan_No().getSubstring(1,
            3).equals("RP3")))
        {
            pnd_Cor_Msg_Pnd_Cor_Icap_Ind.setValue("Y");                                                                                                                   //Natural: ASSIGN #COR-ICAP-IND = 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cor_Msg_Pnd_Cor_Oia_Ind.setValue(pdaScia3530.getScia3530_Aat_Oia_Ind());                                                                                      //Natural: ASSIGN #COR-OIA-IND = SCIA3530.AAT-OIA-IND
        if (condition(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mult_App_Lob().equals(" ") && pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mult_App_Lob_Type().equals(" ")               //Natural: IF SCIA3500.###MULT-APP-LOB = ' ' AND SCIA3500.###MULT-APP-LOB-TYPE = ' ' AND SCIA3500.###MULT-APP-PPG = ' '
            && pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mult_App_Ppg().equals(" ")))
        {
            pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mult_App_Status().setValue(pdaScia3530.getScia3530_Aat_Mult_App_Status());                                                //Natural: ASSIGN SCIA3500.###MULT-APP-STATUS := SCIA3530.AAT-MULT-APP-STATUS
            pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mult_App_Lob().setValue(pdaScia3530.getScia3530_Aat_Mult_App_Lob());                                                      //Natural: ASSIGN SCIA3500.###MULT-APP-LOB := SCIA3530.AAT-MULT-APP-LOB
            pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mult_App_Lob_Type().setValue(pdaScia3530.getScia3530_Aat_Mult_App_Lob_Type());                                            //Natural: ASSIGN SCIA3500.###MULT-APP-LOB-TYPE := SCIA3530.AAT-MULT-APP-LOB-TYPE
            pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mult_App_Ppg().setValue(pdaScia3530.getScia3530_Aat_Mult_App_Ppg());                                                      //Natural: ASSIGN SCIA3500.###MULT-APP-PPG := SCIA3530.AAT-MULT-APP-PPG
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet5609 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF SCIA3530.AAT-LOB;//Natural: VALUE 'D'
        if (condition((pdaScia3530.getScia3530_Aat_Lob().equals("D"))))
        {
            decideConditionsMet5609++;
            pnd_Lob_Type_For_Prap.setValue(pdaScia3530.getScia3530_Aat_Lob_Type());                                                                                       //Natural: MOVE SCIA3530.AAT-LOB-TYPE TO #LOB-TYPE-FOR-PRAP
            pnd_Compare_Index_Pnd_Compare_Index_A.setValue(pdaScia3530.getScia3530_Aat_Lob_Type(), MoveOption.RightJustified);                                            //Natural: MOVE RIGHT JUSTIFIED SCIA3530.AAT-LOB-TYPE TO #COMPARE-INDEX-A
            short decideConditionsMet5614 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF SCIA3530.AAT-LOB-TYPE;//Natural: VALUE '5'
            if (condition((pdaScia3530.getScia3530_Aat_Lob_Type().equals("5"))))
            {
                decideConditionsMet5614++;
                pnd_Compare_Index.setValue(20);                                                                                                                           //Natural: MOVE 20 TO #COMPARE-INDEX
            }                                                                                                                                                             //Natural: VALUE 'A'
            else if (condition((pdaScia3530.getScia3530_Aat_Lob_Type().equals("A"))))
            {
                decideConditionsMet5614++;
                pnd_Compare_Index.setValue(24);                                                                                                                           //Natural: MOVE 24 TO #COMPARE-INDEX
            }                                                                                                                                                             //Natural: VALUE 'B'
            else if (condition((pdaScia3530.getScia3530_Aat_Lob_Type().equals("B"))))
            {
                decideConditionsMet5614++;
                pnd_Compare_Index.setValue(27);                                                                                                                           //Natural: MOVE 27 TO #COMPARE-INDEX
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((pdaScia3530.getScia3530_Aat_Lob().equals("I"))))
        {
            decideConditionsMet5609++;
            pnd_Lob_Type_For_Prap.setValue(pdaScia3530.getScia3530_Aat_Lob_Type());                                                                                       //Natural: MOVE SCIA3530.AAT-LOB-TYPE TO #LOB-TYPE-FOR-PRAP
            short decideConditionsMet5627 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF SCIA3530.AAT-LOB-TYPE;//Natural: VALUE '1','2'
            if (condition((pdaScia3530.getScia3530_Aat_Lob_Type().equals("1") || pdaScia3530.getScia3530_Aat_Lob_Type().equals("2"))))
            {
                decideConditionsMet5627++;
                pnd_Compare_Index.setValue(14);                                                                                                                           //Natural: MOVE 14 TO #COMPARE-INDEX
            }                                                                                                                                                             //Natural: VALUE '3'
            else if (condition((pdaScia3530.getScia3530_Aat_Lob_Type().equals("3"))))
            {
                decideConditionsMet5627++;
                pnd_Compare_Index.setValue(15);                                                                                                                           //Natural: MOVE 15 TO #COMPARE-INDEX
            }                                                                                                                                                             //Natural: VALUE '4'
            else if (condition((pdaScia3530.getScia3530_Aat_Lob_Type().equals("4"))))
            {
                decideConditionsMet5627++;
                pnd_Compare_Index.setValue(16);                                                                                                                           //Natural: MOVE 16 TO #COMPARE-INDEX
            }                                                                                                                                                             //Natural: VALUE '5'
            else if (condition((pdaScia3530.getScia3530_Aat_Lob_Type().equals("5"))))
            {
                decideConditionsMet5627++;
                pnd_Compare_Index.setValue(17);                                                                                                                           //Natural: MOVE 17 TO #COMPARE-INDEX
            }                                                                                                                                                             //Natural: VALUE '6'
            else if (condition((pdaScia3530.getScia3530_Aat_Lob_Type().equals("6"))))
            {
                decideConditionsMet5627++;
                pnd_Compare_Index.setValue(23);                                                                                                                           //Natural: MOVE 23 TO #COMPARE-INDEX
            }                                                                                                                                                             //Natural: VALUE '7'
            else if (condition((pdaScia3530.getScia3530_Aat_Lob_Type().equals("7"))))
            {
                decideConditionsMet5627++;
                pnd_Compare_Index.setValue(28);                                                                                                                           //Natural: MOVE 28 TO #COMPARE-INDEX
            }                                                                                                                                                             //Natural: VALUE '8'
            else if (condition((pdaScia3530.getScia3530_Aat_Lob_Type().equals("8"))))
            {
                decideConditionsMet5627++;
                pnd_Compare_Index.setValue(29);                                                                                                                           //Natural: MOVE 29 TO #COMPARE-INDEX
            }                                                                                                                                                             //Natural: VALUE '9'
            else if (condition((pdaScia3530.getScia3530_Aat_Lob_Type().equals("9"))))
            {
                decideConditionsMet5627++;
                pnd_Compare_Index.setValue(30);                                                                                                                           //Natural: MOVE 30 TO #COMPARE-INDEX
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pdaScia3530.getScia3530_Aat_Lob().equals("S"))))
        {
            decideConditionsMet5609++;
            if (condition(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Srce_Cd().equals("9005")))                                                                                  //Natural: IF SCIA3500.###SRCE-CD EQ '9005'
            {
                pnd_Compare_Index.setValue(11);                                                                                                                           //Natural: MOVE 11 TO #COMPARE-INDEX
                pnd_Lob_Type_For_Prap.setValue("2");                                                                                                                      //Natural: MOVE '2' TO #LOB-TYPE-FOR-PRAP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                short decideConditionsMet5652 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF SCIA3530.AAT-LOB-TYPE;//Natural: VALUE '2'
                if (condition((pdaScia3530.getScia3530_Aat_Lob_Type().equals("2"))))
                {
                    decideConditionsMet5652++;
                    pnd_Compare_Index.setValue(12);                                                                                                                       //Natural: MOVE 12 TO #COMPARE-INDEX
                    pnd_Lob_Type_For_Prap.setValue("2");                                                                                                                  //Natural: MOVE '2' TO #LOB-TYPE-FOR-PRAP
                }                                                                                                                                                         //Natural: VALUE '3'
                else if (condition((pdaScia3530.getScia3530_Aat_Lob_Type().equals("3"))))
                {
                    decideConditionsMet5652++;
                    pnd_Lob_Type_For_Prap.setValue("3");                                                                                                                  //Natural: MOVE '3' TO #LOB-TYPE-FOR-PRAP
                    pnd_Compare_Index.setValue(13);                                                                                                                       //Natural: MOVE 13 TO #COMPARE-INDEX
                }                                                                                                                                                         //Natural: VALUE '4'
                else if (condition((pdaScia3530.getScia3530_Aat_Lob_Type().equals("4"))))
                {
                    decideConditionsMet5652++;
                    pnd_Lob_Type_For_Prap.setValue("4");                                                                                                                  //Natural: MOVE '4' TO #LOB-TYPE-FOR-PRAP
                    pnd_Compare_Index.setValue(19);                                                                                                                       //Natural: MOVE 19 TO #COMPARE-INDEX
                }                                                                                                                                                         //Natural: VALUE '8'
                else if (condition((pdaScia3530.getScia3530_Aat_Lob_Type().equals("8"))))
                {
                    decideConditionsMet5652++;
                    pnd_Lob_Type_For_Prap.setValue("8");                                                                                                                  //Natural: MOVE '8' TO #LOB-TYPE-FOR-PRAP
                    pnd_Compare_Index.setValue(18);                                                                                                                       //Natural: MOVE 18 TO #COMPARE-INDEX
                }                                                                                                                                                         //Natural: VALUE '5'
                else if (condition((pdaScia3530.getScia3530_Aat_Lob_Type().equals("5"))))
                {
                    decideConditionsMet5652++;
                    pnd_Lob_Type_For_Prap.setValue("5");                                                                                                                  //Natural: MOVE '5' TO #LOB-TYPE-FOR-PRAP
                    pnd_Compare_Index.setValue(21);                                                                                                                       //Natural: MOVE 21 TO #COMPARE-INDEX
                }                                                                                                                                                         //Natural: VALUE '6'
                else if (condition((pdaScia3530.getScia3530_Aat_Lob_Type().equals("6"))))
                {
                    decideConditionsMet5652++;
                    pnd_Lob_Type_For_Prap.setValue("6");                                                                                                                  //Natural: MOVE '6' TO #LOB-TYPE-FOR-PRAP
                    pnd_Compare_Index.setValue(22);                                                                                                                       //Natural: MOVE 22 TO #COMPARE-INDEX
                }                                                                                                                                                         //Natural: VALUE '7'
                else if (condition((pdaScia3530.getScia3530_Aat_Lob_Type().equals("7"))))
                {
                    decideConditionsMet5652++;
                    pnd_Compare_Index.setValue(25);                                                                                                                       //Natural: MOVE 25 TO #COMPARE-INDEX
                }                                                                                                                                                         //Natural: VALUE '9'
                else if (condition((pdaScia3530.getScia3530_Aat_Lob_Type().equals("9"))))
                {
                    decideConditionsMet5652++;
                    pnd_Compare_Index.setValue(26);                                                                                                                       //Natural: MOVE 26 TO #COMPARE-INDEX
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet5683 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #COMPARE-INDEX;//Natural: VALUE 2
        if (condition((pnd_Compare_Index.equals(2))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("RA");                                                                                                         //Natural: MOVE 'RA' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_Compare_Index.equals(3))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("CANP");                                                                                                       //Natural: MOVE 'CANP' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_Compare_Index.equals(4))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("CANS");                                                                                                       //Natural: MOVE 'CANS' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 6
        else if (condition((pnd_Compare_Index.equals(6))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("LGA");                                                                                                        //Natural: MOVE 'LGA' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 7
        else if (condition((pnd_Compare_Index.equals(7))))
        {
            decideConditionsMet5683++;
            if (condition(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Srce_Cd().equals("AJ21")))                                                                                  //Natural: IF SCIA3500.###SRCE-CD = 'AJ21'
            {
                pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("NGRA");                                                                                                   //Natural: MOVE 'NGRA' TO #PROD-ID
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("GRA");                                                                                                    //Natural: MOVE 'GRA' TO #PROD-ID
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 8
        else if (condition((pnd_Compare_Index.equals(8))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("RAQV");                                                                                                       //Natural: MOVE 'RAQV' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 9
        else if (condition((pnd_Compare_Index.equals(9))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("GRA2");                                                                                                       //Natural: MOVE 'GRA2' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 11
        else if (condition((pnd_Compare_Index.equals(11))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("SSRA");                                                                                                       //Natural: MOVE 'SSRA' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 12
        else if (condition((pnd_Compare_Index.equals(12))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("SRA");                                                                                                        //Natural: MOVE 'SRA' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 13
        else if (condition((pnd_Compare_Index.equals(13))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("GSRA");                                                                                                       //Natural: MOVE 'GSRA' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 14
        else if (condition((pnd_Compare_Index.equals(14))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("IRA");                                                                                                        //Natural: MOVE 'IRA' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 15
        else if (condition((pnd_Compare_Index.equals(15))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("IRAR");                                                                                                       //Natural: MOVE 'IRAR' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 16
        else if (condition((pnd_Compare_Index.equals(16))))
        {
            decideConditionsMet5683++;
            if (condition(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_State_Approval_Indicator().equals("Y")))                                                                    //Natural: IF SCIA3500.###STATE-APPROVAL-INDICATOR = 'Y'
            {
                pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("IRAC");                                                                                                   //Natural: MOVE 'IRAC' TO #PROD-ID
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("IRA");                                                                                                    //Natural: MOVE 'IRA' TO #PROD-ID
                pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Lob().setValue("I");                                                                                                  //Natural: ASSIGN SCIA3500.###LOB = 'I'
                pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Lob_Type().setValue("1");                                                                                             //Natural: ASSIGN SCIA3500.###LOB-TYPE = '1'
                pnd_Lob_Type_For_Prap.setValue("1");                                                                                                                      //Natural: MOVE '1' TO #LOB-TYPE-FOR-PRAP
                //*  BJD RHSP
                //*  RS0 GUAR INDX RATE
                //*  RS0 GUAR INDX RATE
                //*  RS0 GUAR INDX RATE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 17
        else if (condition((pnd_Compare_Index.equals(17))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("KEOG");                                                                                                       //Natural: MOVE 'KEOG' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 18
        else if (condition((pnd_Compare_Index.equals(18))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("SRAQ");                                                                                                       //Natural: MOVE 'SRAQ' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 19
        else if (condition((pnd_Compare_Index.equals(19))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("GA");                                                                                                         //Natural: MOVE 'GA' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 20
        else if (condition((pnd_Compare_Index.equals(20))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("SA");                                                                                                         //Natural: MOVE 'SA' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 21
        else if (condition((pnd_Compare_Index.equals(21))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("SP");                                                                                                         //Natural: MOVE 'SP' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 22
        else if (condition((pnd_Compare_Index.equals(22))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("SP2");                                                                                                        //Natural: MOVE 'SP2' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 23
        else if (condition((pnd_Compare_Index.equals(23))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("IRAS");                                                                                                       //Natural: MOVE 'IRAS' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 24
        else if (condition((pnd_Compare_Index.equals(24))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("RS");                                                                                                         //Natural: MOVE 'RS' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 25
        else if (condition((pnd_Compare_Index.equals(25))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("RSP");                                                                                                        //Natural: MOVE 'RSP' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 26
        else if (condition((pnd_Compare_Index.equals(26))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("TGA");                                                                                                        //Natural: MOVE 'TGA' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 27
        else if (condition((pnd_Compare_Index.equals(27))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("RHSP");                                                                                                       //Natural: MOVE 'RHSP' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 28
        else if (condition((pnd_Compare_Index.equals(28))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("IRIR");                                                                                                       //Natural: MOVE 'IRIR' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 29
        else if (condition((pnd_Compare_Index.equals(29))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("IRIC");                                                                                                       //Natural: MOVE 'IRIC' TO #PROD-ID
        }                                                                                                                                                                 //Natural: VALUE 30
        else if (condition((pnd_Compare_Index.equals(30))))
        {
            decideConditionsMet5683++;
            pdaScia3560.getScia3560_Pnd_Prod_Id().setValue("IRIS");                                                                                                       //Natural: MOVE 'IRIS' TO #PROD-ID
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(" INVALID LOB TYPE");                                                                                      //Natural: MOVE ' INVALID LOB TYPE' TO MSG-INFO-SUB.##MSG
            getReports().write(0, pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                             //Natural: WRITE MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Hold_Premium_Team_Cde.reset();                                                                                                                                //Natural: RESET #HOLD-PREMIUM-TEAM-CDE
        if (condition(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Premium_Team_Cde().notEquals(" ")))                                                                             //Natural: IF SCIA3500.###PREMIUM-TEAM-CDE NE ' '
        {
            pdaScia3570.getScia3570_Parameter().reset();                                                                                                                  //Natural: RESET SCIA3570-PARAMETER #ENTRY-DSCRPTN-TEXT
            pnd_Entry_Dscrptn_Text.reset();
            pdaScia3570.getScia3570_Parameter_Pnd_P_Iis_Premium_Team_Cde().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Premium_Team_Cde());                              //Natural: ASSIGN #P-IIS-PREMIUM-TEAM-CDE = SCIA3500.###PREMIUM-TEAM-CDE
            DbsUtil.callnat(Scin3570.class , getCurrentProcessState(), pdaScia3570.getScia3570_Parameter());                                                              //Natural: CALLNAT 'SCIN3570' SCIA3570-PARAMETER
            if (condition(Global.isEscape())) return;
            pnd_Entry_Dscrptn_Text.setValue(pdaScia3570.getScia3570_Parameter_Pnd_P_Entry_Dscrptn_Txt());                                                                 //Natural: ASSIGN #ENTRY-DSCRPTN-TEXT = #P-ENTRY-DSCRPTN-TXT
            pnd_Hold_Premium_Team_Cde.setValue(pnd_Entry_Dscrptn_Text_Pnd_Entry_Prap_Team);                                                                               //Natural: ASSIGN #HOLD-PREMIUM-TEAM-CDE = #ENTRY-PRAP-TEAM
            pnd_Hold_Bucket_Cde.setValue(pnd_Entry_Dscrptn_Text_Pnd_Entry_Bucket);                                                                                        //Natural: ASSIGN #HOLD-BUCKET-CDE = #ENTRY-BUCKET
            pnd_Hold_Accum_Cde.setValue(pnd_Entry_Dscrptn_Text_Pnd_Entry_Accum);                                                                                          //Natural: ASSIGN #HOLD-ACCUM-CDE = #ENTRY-ACCUM
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind().equals(" ") || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind().equals("I")))                    //Natural: IF SCIA3500.###PROCESS-IND = ' ' OR = 'I'
        {
            pdaScia3560.getScia3560_Pnd_Contr_Gotten_T().setValue(pdaScia9000.getScia9000_Pnd_Tiaa_Contract_No());                                                        //Natural: ASSIGN #CONTR-GOTTEN-T := SCIA9000.#TIAA-CONTRACT-NO
            hold_T_Cont.setValue(pdaScia9000.getScia9000_Pnd_Tiaa_Contract_No());                                                                                         //Natural: ASSIGN HOLD-T-CONT := SCIA9000.#TIAA-CONTRACT-NO
            pdaScia3560.getScia3560_Pnd_Contr_Gotten_C().setValue(pdaScia9000.getScia9000_Pnd_Cref_Certificate_No());                                                     //Natural: ASSIGN #CONTR-GOTTEN-C := SCIA9000.#CREF-CERTIFICATE-NO
            hold_C_Cont.setValue(pdaScia9000.getScia9000_Pnd_Cref_Certificate_No());                                                                                      //Natural: ASSIGN HOLD-C-CONT := SCIA9000.#CREF-CERTIFICATE-NO
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind().equals("P") || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind().equals("C")                  //Natural: IF SCIA3500.###PROCESS-IND = 'P' OR = 'C' OR = 'T'
                || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind().equals("T")))
            {
                hold_T_Cont.setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_T_Contract());                                                                                   //Natural: MOVE SCIA3500.###T-CONTRACT TO HOLD-T-CONT
                pdaScia3560.getScia3560_Pnd_Contr_Gotten_T().setValue(hold_T_Cont);                                                                                       //Natural: ASSIGN #CONTR-GOTTEN-T := HOLD-T-CONT
                if (condition(! (DbsUtil.maskMatches(hold_T_Cont_Hold_App_Cont,"NNN...N")) || hold_T_Cont_Hold_App_Cont.equals("0000000")))                               //Natural: IF HOLD-APP-CONT NE MASK ( NNN...N ) OR HOLD-APP-CONT = '0000000'
                {
                    pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("ERROR !!! CONTRACT NUMBER =", pdaScia3500.getScia3500_Pnd_Pnd_Pnd_T_Contract(),  //Natural: COMPRESS 'ERROR !!! CONTRACT NUMBER =' SCIA3500.###T-CONTRACT 'OR PROCESS INDEX =' SCIA3500.###PROCESS-IND 'IS WRONG' INTO MSG-INFO-SUB.##MSG
                        "OR PROCESS INDEX =", pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind(), "IS WRONG"));
                    getReports().write(0, pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                     //Natural: WRITE MSG-INFO-SUB.##MSG
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                hold_C_Cont.setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_C_Contract());                                                                                   //Natural: MOVE SCIA3500.###C-CONTRACT TO HOLD-C-CONT
                pdaScia3560.getScia3560_Pnd_Contr_Gotten_C().setValue(hold_C_Cont);                                                                                       //Natural: ASSIGN #CONTR-GOTTEN-C := HOLD-C-CONT
                //*  EAC (SINGLETON EM WAS 9999999)
                if (condition(! (DbsUtil.maskMatches(hold_C_Cont_Hold_App_C_Cont,"NNN...N")) || hold_C_Cont_Hold_App_C_Cont.equals("0000000")))                           //Natural: IF HOLD-APP-C-CONT NE MASK ( NNN...N ) OR HOLD-APP-C-CONT = '0000000'
                {
                    pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("ERROR !!! CONTRACT NUMBER =", pdaScia3500.getScia3500_Pnd_Pnd_Pnd_C_Contract(),  //Natural: COMPRESS 'ERROR !!! CONTRACT NUMBER =' SCIA3500.###C-CONTRACT 'OR PROCESS INDEX =' SCIA3500.###PROCESS-IND 'IS WRONG' INTO MSG-INFO-SUB.##MSG
                        "OR PROCESS INDEX =", pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind(), "IS WRONG"));
                    getReports().write(0, pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                     //Natural: WRITE MSG-INFO-SUB.##MSG
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                hold_Mmyy.setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_T_Doi());                                                                                          //Natural: MOVE SCIA3500.###T-DOI TO HOLD-MMYY
                hold_Date_Mmddyy_Hold_Mm.setValue(hold_Mmyy_Pnd_Hold_Mm);                                                                                                 //Natural: MOVE #HOLD-MM TO HOLD-MM
                hold_Date_Mmddyy_Hold_Yy.setValue(hold_Mmyy_Pnd_Hold_Yy);                                                                                                 //Natural: MOVE #HOLD-YY TO HOLD-YY
                hold_Date_Mmddyy_Hold_Dd.setValue(1);                                                                                                                     //Natural: MOVE 01 TO HOLD-DD
                pdaScia3530.getScia3530_Aat_T_Doi().setValue(hold_Date_Mmddyy);                                                                                           //Natural: MOVE HOLD-DATE-MMDDYY TO SCIA3530.AAT-T-DOI
                hold_Mmyy.setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_C_Doi());                                                                                          //Natural: MOVE SCIA3500.###C-DOI TO HOLD-MMYY
                hold_Date_Mmddyy_Hold_Mm.setValue(hold_Mmyy_Pnd_Hold_Mm);                                                                                                 //Natural: MOVE #HOLD-MM TO HOLD-MM
                hold_Date_Mmddyy_Hold_Yy.setValue(hold_Mmyy_Pnd_Hold_Yy);                                                                                                 //Natural: MOVE #HOLD-YY TO HOLD-YY
                hold_Date_Mmddyy_Hold_Dd.setValue(1);                                                                                                                     //Natural: MOVE 01 TO HOLD-DD
                pdaScia3530.getScia3530_Aat_C_Doi().setValue(hold_Date_Mmddyy);                                                                                           //Natural: MOVE HOLD-DATE-MMDDYY TO SCIA3530.AAT-C-DOI
                pnd_Hold_Ap_Status.setValue("C");                                                                                                                         //Natural: MOVE 'C' TO #HOLD-AP-STATUS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_T_Contract().setValue(pdaScia3560.getScia3560_Pnd_Contr_Gotten_T());                                                          //Natural: MOVE #CONTR-GOTTEN-T TO SCIA3500.###T-CONTRACT
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_C_Contract().setValue(pdaScia3560.getScia3560_Pnd_Contr_Gotten_C());                                                          //Natural: MOVE #CONTR-GOTTEN-C TO SCIA3500.###C-CONTRACT
        pdaScia3540.getScia3540_Ap_Coll_Code().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Srce_Cd());                                                                   //Natural: ASSIGN SCIA3540.AP-COLL-CODE := SCIA3500.###SRCE-CD
        pdaScia3540.getScia3540_Ap_Soc_Sec().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Ssn());                                                                         //Natural: ASSIGN SCIA3540.AP-SOC-SEC := SCIA3500.###SSN
        pdaScia3540.getScia3540_Ap_Cor_Last_Nme().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Last_Name());                                                              //Natural: ASSIGN SCIA3540.AP-COR-LAST-NME := SCIA3500.###LAST-NAME
        pdaScia3540.getScia3540_Ap_Cor_First_Nme().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_First_Name());                                                            //Natural: ASSIGN SCIA3540.AP-COR-FIRST-NME := SCIA3500.###FIRST-NAME
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("FINDNUM");                                                                                                           //Natural: MOVE 'FINDNUM' TO CDAOBJ.#FUNCTION
                                                                                                                                                                          //Natural: PERFORM CALL-OBJECT
        sub_Call_Object();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaScia3540.getScia3540_Var_Pnd_G_Key_Max().greater(getZero())))                                                                                    //Natural: IF SCIA3540-VAR.#G-KEY-MAX > 0
        {
            pdaScia3540.getScia3540_Ap_Soc_Sec().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Ssn());                                                                     //Natural: ASSIGN SCIA3540.AP-SOC-SEC := SCIA3500.###SSN
            pdaScia3540.getScia3540_Ap_G_Key().setValue(pdaScia3540.getScia3540_Var_Pnd_G_Key_Max());                                                                     //Natural: ASSIGN SCIA3540.AP-G-KEY := SCIA3540-VAR.#G-KEY-MAX
            pdaScia3540.getScia3540_Ap_Coll_Code().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Srce_Cd());                                                               //Natural: ASSIGN SCIA3540.AP-COLL-CODE := SCIA3500.###SRCE-CD
            pdaScia3540.getScia3540_Ap_Cor_Last_Nme().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Last_Name());                                                          //Natural: ASSIGN SCIA3540.AP-COR-LAST-NME := SCIA3500.###LAST-NAME
            pdaScia3540.getScia3540_Ap_Cor_First_Nme().setValue(pdaScia3540.getScia3540_Var_Pnd_Prap_Save_First_Name());                                                  //Natural: ASSIGN SCIA3540.AP-COR-FIRST-NME := SCIA3540-VAR.#PRAP-SAVE-FIRST-NAME
            pdaCdaobj.getCdaobj_Pnd_Function().setValue("GET");                                                                                                           //Natural: MOVE 'GET' TO CDAOBJ.#FUNCTION
                                                                                                                                                                          //Natural: PERFORM CALL-OBJECT
            sub_Call_Object();
            if (condition(Global.isEscape())) {return;}
            pdaScia3540.getScia3540_Ap_G_Ind().setValue(0);                                                                                                               //Natural: ASSIGN SCIA3540.AP-G-IND = 0
            if (condition(pdaScia3540.getScia3540_Ap_G_Key().equals(pdaScia3540.getScia3540_Var_Pnd_G_Key_Max())))                                                        //Natural: IF SCIA3540.AP-G-KEY = SCIA3540-VAR.#G-KEY-MAX
            {
                pdaCdaobj.getCdaobj_Pnd_Function().setValue("UPDATE");                                                                                                    //Natural: MOVE 'UPDATE' TO CDAOBJ.#FUNCTION
                                                                                                                                                                          //Natural: PERFORM CALL-OBJECT
                sub_Call_Object();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind().equals("P")))                                                                                     //Natural: IF SCIA3500.###PROCESS-IND = 'P'
        {
            pdaScia3540.getScia3540_Ap_Coll_Code().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Srce_Cd());                                                               //Natural: ASSIGN SCIA3540.AP-COLL-CODE := SCIA3500.###SRCE-CD
            pdaScia3540.getScia3540_Ap_G_Key().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_G_Key());                                                                     //Natural: ASSIGN SCIA3540.AP-G-KEY := SCIA3500.###G-KEY
            pdaScia3540.getScia3540_Ap_Cor_Last_Nme().setValue(pdaScia3500.getScia3500_Pnd_Last_Name());                                                                  //Natural: ASSIGN SCIA3540.AP-COR-LAST-NME := SCIA3500.#LAST-NAME
            pdaScia3540.getScia3540_Ap_Cor_First_Nme().setValue(pdaScia3500.getScia3500_Pnd_First_Name());                                                                //Natural: ASSIGN SCIA3540.AP-COR-FIRST-NME := SCIA3500.#FIRST-NAME
            pdaScia3540.getScia3540_Ap_Lob().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Lob());                                                                         //Natural: ASSIGN SCIA3540.AP-LOB := SCIA3500.###LOB
            pdaScia3540.getScia3540_Ap_Lob_Type().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Lob_Type());                                                               //Natural: ASSIGN SCIA3540.AP-LOB-TYPE := SCIA3500.###LOB-TYPE
            pdaCdaobj.getCdaobj_Pnd_Function().setValue("GET");                                                                                                           //Natural: MOVE 'GET' TO CDAOBJ.#FUNCTION
                                                                                                                                                                          //Natural: PERFORM CALL-OBJECT
            sub_Call_Object();
            if (condition(Global.isEscape())) {return;}
            pdaScia3540.getScia3540_Ap_G_Ind().setValue(0);                                                                                                               //Natural: ASSIGN SCIA3540.AP-G-IND = 0
            //*  CHANGE THE STATUS TO M & R
            if (condition(pdaScia3540.getScia3540_Ap_G_Key().equals(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_G_Key())))                                                        //Natural: IF SCIA3540.AP-G-KEY = SCIA3500.###G-KEY
            {
                pdaScia3540.getScia3540_Ap_Dt_Matched().setValue(pnd_System_Date);                                                                                        //Natural: ASSIGN SCIA3540.AP-DT-MATCHED := #SYSTEM-DATE
                pdaScia3540.getScia3540_Ap_Status().setValue("C");                                                                                                        //Natural: ASSIGN SCIA3540.AP-STATUS := 'C'
                pdaScia3540.getScia3540_Ap_T_Doi().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_T_Doi());                                                                 //Natural: ASSIGN SCIA3540.AP-T-DOI := SCIA3500.###T-DOI
                pdaScia3540.getScia3540_Ap_C_Doi().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_C_Doi());                                                                 //Natural: ASSIGN SCIA3540.AP-C-DOI := SCIA3500.###C-DOI
                pdaCdaobj.getCdaobj_Pnd_Function().setValue("UPDATE");                                                                                                    //Natural: MOVE 'UPDATE' TO CDAOBJ.#FUNCTION
                                                                                                                                                                          //Natural: PERFORM CALL-OBJECT
                sub_Call_Object();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Mit_Update.reset();                                                                                                                                           //Natural: RESET #MIT-UPDATE
        pdaScia3540.getScia3540().reset();                                                                                                                                //Natural: RESET SCIA3540
        pnd_Ap_Cor_Ind.setValue("N");                                                                                                                                     //Natural: ASSIGN #AP-COR-IND := 'N'
        pdaScia8510.getScia8510_Pnd_Ap_Alloc_Ind().setValue("N");                                                                                                         //Natural: ASSIGN #AP-ALLOC-IND := 'N'
        pdaScia3540.getScia3540_Ap_Sync_Ind().setValue("N");                                                                                                              //Natural: ASSIGN SCIA3540.AP-SYNC-IND := 'N'
        pdaScia3540.getScia3540_Ap_Cor_Ind().setValue("N");                                                                                                               //Natural: ASSIGN SCIA3540.AP-COR-IND := 'N'
        pdaScia3540.getScia3540_Ap_Alloc_Ind().setValue("N");                                                                                                             //Natural: ASSIGN SCIA3540.AP-ALLOC-IND := 'N'
        pdaScia3540.getScia3540_Ap_Tiaa_Cntrct().setValue(hold_T_Cont);                                                                                                   //Natural: ASSIGN SCIA3540.AP-TIAA-CNTRCT := HOLD-T-CONT
        pdaScia3530.getScia3530_Aat_Tiaa_Cntrct().setValue(hold_T_Cont);                                                                                                  //Natural: ASSIGN SCIA3530.AAT-TIAA-CNTRCT := HOLD-T-CONT
        pdaScia3540.getScia3540_Ap_Cref_Cert().setValue(hold_C_Cont);                                                                                                     //Natural: ASSIGN SCIA3540.AP-CREF-CERT := HOLD-C-CONT
        pdaScia3530.getScia3530_Aat_Cref_Cert().setValue(hold_C_Cont);                                                                                                    //Natural: ASSIGN SCIA3530.AAT-CREF-CERT := HOLD-C-CONT
        pdaScia3540.getScia3540_Ap_Coll_Code().setValue(pdaScia3530.getScia3530_Aat_Coll_Code());                                                                         //Natural: ASSIGN SCIA3540.AP-COLL-CODE := SCIA3530.AAT-COLL-CODE
        pdaScia3540.getScia3540_Ap_G_Key().compute(new ComputeParameters(false, pdaScia3540.getScia3540_Ap_G_Key()), DbsField.add(1,pdaScia3540.getScia3540_Var_Pnd_G_Key_Max())); //Natural: ASSIGN SCIA3540.AP-G-KEY := 1 + SCIA3540-VAR.#G-KEY-MAX
        pdaScia3540.getScia3540_Ap_G_Ind().setValue(1);                                                                                                                   //Natural: ASSIGN SCIA3540.AP-G-IND := 1
        pdaScia3540.getScia3540_Ap_Soc_Sec().setValue(pdaScia3530.getScia3530_Aat_Soc_Sec());                                                                             //Natural: ASSIGN SCIA3540.AP-SOC-SEC := SCIA3530.AAT-SOC-SEC
        pdaScia3540.getScia3540_Ap_Dob().setValue(pdaScia3530.getScia3530_Aat_Dob());                                                                                     //Natural: ASSIGN SCIA3540.AP-DOB := SCIA3530.AAT-DOB
        pdaScia3540.getScia3540_Ap_Lob().setValue(pdaScia3530.getScia3530_Aat_Lob());                                                                                     //Natural: ASSIGN SCIA3540.AP-LOB := SCIA3530.AAT-LOB
        pdaScia3540.getScia3540_Ap_Bill_Code().setValue(pdaScia3530.getScia3530_Aat_Bill_Code());                                                                         //Natural: ASSIGN SCIA3540.AP-BILL-CODE := SCIA3530.AAT-BILL-CODE
        hold_Date_Mmddyy.setValue(pdaScia3530.getScia3530_Aat_T_Doi());                                                                                                   //Natural: ASSIGN HOLD-DATE-MMDDYY := SCIA3530.AAT-T-DOI
        if (condition(hold_Date_Mmddyy.equals(999999)))                                                                                                                   //Natural: IF HOLD-DATE-MMDDYY = 999999
        {
            hold_Date_Mmddyy.setValue(0);                                                                                                                                 //Natural: ASSIGN HOLD-DATE-MMDDYY := 0
        }                                                                                                                                                                 //Natural: END-IF
        hold_Mmyy_Pnd_Hold_Mm.setValue(hold_Date_Mmddyy_Hold_Mm);                                                                                                         //Natural: ASSIGN #HOLD-MM := HOLD-MM
        hold_Mmyy_Pnd_Hold_Yy.setValue(hold_Date_Mmddyy_Hold_Yy);                                                                                                         //Natural: ASSIGN #HOLD-YY := HOLD-YY
        if (condition(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind().equals("P") || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind().equals("C") ||                   //Natural: IF SCIA3500.###PROCESS-IND = 'P' OR = 'C' OR = 'T'
            pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind().equals("T")))
        {
            hold_Mmyy.setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_T_Doi());                                                                                              //Natural: ASSIGN HOLD-MMYY := SCIA3500.###T-DOI
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3540.getScia3540_Ap_T_Doi().setValue(hold_Mmyy);                                                                                                           //Natural: ASSIGN SCIA3540.AP-T-DOI := HOLD-MMYY
        hold_Date_Mmddyy.setValue(pdaScia3530.getScia3530_Aat_C_Doi());                                                                                                   //Natural: ASSIGN HOLD-DATE-MMDDYY := SCIA3530.AAT-C-DOI
        if (condition(hold_Date_Mmddyy.equals(999999)))                                                                                                                   //Natural: IF HOLD-DATE-MMDDYY = 999999
        {
            hold_Date_Mmddyy.setValue(0);                                                                                                                                 //Natural: ASSIGN HOLD-DATE-MMDDYY := 0
        }                                                                                                                                                                 //Natural: END-IF
        hold_Mmyy_Pnd_Hold_Mm.setValue(hold_Date_Mmddyy_Hold_Mm);                                                                                                         //Natural: ASSIGN #HOLD-MM := HOLD-MM
        hold_Mmyy_Pnd_Hold_Yy.setValue(hold_Date_Mmddyy_Hold_Yy);                                                                                                         //Natural: ASSIGN #HOLD-YY := HOLD-YY
        if (condition(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind().equals("P") || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind().equals("C") ||                   //Natural: IF SCIA3500.###PROCESS-IND = 'P' OR = 'C' OR = 'T'
            pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind().equals("T")))
        {
            hold_Mmyy.setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_C_Doi());                                                                                              //Natural: ASSIGN HOLD-MMYY := SCIA3500.###C-DOI
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3540.getScia3540_Ap_C_Doi().setValue(hold_Mmyy);                                                                                                           //Natural: ASSIGN SCIA3540.AP-C-DOI := HOLD-MMYY
        pdaScia3540.getScia3540_Ap_Dt_Released().setValue(pnd_System_Date);                                                                                               //Natural: ASSIGN SCIA3540.AP-DT-RELEASED := #SYSTEM-DATE
        hold_Curr.setValue(pdaScia3530.getScia3530_Aat_Currency());                                                                                                       //Natural: ASSIGN HOLD-CURR := SCIA3530.AAT-CURRENCY
        pdaScia3540.getScia3540_Ap_Curr().setValue(hold_Curr_Hold_Curr_N);                                                                                                //Natural: ASSIGN SCIA3540.AP-CURR := HOLD-CURR-N
        pdaScia3540.getScia3540_Ap_T_Age_1st().setValue(pdaScia3530.getScia3530_Aat_T_1st_Pymt());                                                                        //Natural: ASSIGN SCIA3540.AP-T-AGE-1ST := SCIA3530.AAT-T-1ST-PYMT
        pdaScia3540.getScia3540_Ap_C_Age_1st().setValue(pdaScia3530.getScia3530_Aat_C_1st_Pymt());                                                                        //Natural: ASSIGN SCIA3540.AP-C-AGE-1ST := SCIA3530.AAT-C-1ST-PYMT
        pdaScia3540.getScia3540_Ap_Ownership().compute(new ComputeParameters(false, pdaScia3540.getScia3540_Ap_Ownership()), pdaScia9000.getScia9000_Pnd_Vesting_Code().val()); //Natural: ASSIGN SCIA3540.AP-OWNERSHIP := VAL ( SCIA9000.#VESTING-CODE )
        getReports().write(0, "=",pdaScia3540.getScia3540_Ap_Ownership());                                                                                                //Natural: WRITE '=' SCIA3540.AP-OWNERSHIP
        if (Global.isEscape()) return;
        pdaScia3540.getScia3540_Ap_Sex().setValue(pdaScia3530.getScia3530_Aat_Sex());                                                                                     //Natural: ASSIGN SCIA3540.AP-SEX := SCIA3530.AAT-SEX
        pdaScia3540.getScia3540_Ap_Address_Line().getValue("*").setValue(pdaScia3530.getScia3530_Aat_Addrss_Line().getValue("*"));                                        //Natural: ASSIGN SCIA3540.AP-ADDRESS-LINE ( * ) := SCIA3530.AAT-ADDRSS-LINE ( * )
        pdaScia3540.getScia3540_Ap_City().setValue(pdaScia3530.getScia3530_Aat_City());                                                                                   //Natural: ASSIGN SCIA3540.AP-CITY := SCIA3530.AAT-CITY
        pdaScia3540.getScia3540_Ap_Mail_Zip().setValue(pdaScia3530.getScia3530_Aat_Zip());                                                                                //Natural: ASSIGN SCIA3540.AP-MAIL-ZIP := SCIA3530.AAT-ZIP
        pdaScia3540.getScia3540_Ap_Name_Addr_Cd().setValue(pdaScia3530.getScia3530_Aat_Na_Code());                                                                        //Natural: ASSIGN SCIA3540.AP-NAME-ADDR-CD := SCIA3530.AAT-NA-CODE
        pdaScia3540.getScia3540_Ap_Rcrd_Updt_Tm_Stamp().setValue(Global.getTIME());                                                                                       //Natural: ASSIGN SCIA3540.AP-RCRD-UPDT-TM-STAMP := *TIME
        pdaScia3540.getScia3540_Ap_Dt_Ent_Sys().setValue(pdaScia3530.getScia3530_Aat_System_Date_Entered());                                                              //Natural: ASSIGN SCIA3540.AP-DT-ENT-SYS := SCIA3530.AAT-SYSTEM-DATE-ENTERED
        pdaScia3540.getScia3540_Ap_Alloc_Discr().setValue(pdaScia3530.getScia3530_Aat_Alloc_Discr());                                                                     //Natural: ASSIGN SCIA3540.AP-ALLOC-DISCR := SCIA3530.AAT-ALLOC-DISCR
        pdaScia3540.getScia3540_Ap_Release_Ind().setValue(1);                                                                                                             //Natural: ASSIGN SCIA3540.AP-RELEASE-IND := 1
        pdaScia3540.getScia3540_Ap_Type().setValue(2);                                                                                                                    //Natural: ASSIGN SCIA3540.AP-TYPE := 2
        pdaScia3540.getScia3540_Ap_Other_Pols().setValue(pdaScia3530.getScia3530_Aat_Other_Policy());                                                                     //Natural: ASSIGN SCIA3540.AP-OTHER-POLS := SCIA3530.AAT-OTHER-POLICY
        pdaScia3540.getScia3540_Ap_Record_Type().setValue(1);                                                                                                             //Natural: ASSIGN SCIA3540.AP-RECORD-TYPE := 1
        pdaScia3540.getScia3540_Ap_Numb_Dailys().setValue(0);                                                                                                             //Natural: ASSIGN SCIA3540.AP-NUMB-DAILYS := 0
        pdaScia3540.getScia3540_Ap_Dt_App_Recvd().setValue(pdaScia3530.getScia3530_Aat_Dt_App_Recvd());                                                                   //Natural: ASSIGN SCIA3540.AP-DT-APP-RECVD := SCIA3530.AAT-DT-APP-RECVD
        pdaScia3540.getScia3540_Ap_App_Source().setValue(pdaScia3530.getScia3530_Aat_Appl_Source());                                                                      //Natural: ASSIGN SCIA3540.AP-APP-SOURCE := SCIA3530.AAT-APPL-SOURCE
        pdaScia3540.getScia3540_Ap_Region_Code().setValue(pdaScia3530.getScia3530_Aat_Region_Code());                                                                     //Natural: ASSIGN SCIA3540.AP-REGION-CODE := SCIA3530.AAT-REGION-CODE
        pdaScia3540.getScia3540_Ap_Orig_Issue_State().setValue(pdaScia3530.getScia3530_Aat_Orig_Issue_State());                                                           //Natural: ASSIGN SCIA3540.AP-ORIG-ISSUE-STATE := SCIA3530.AAT-ORIG-ISSUE-STATE
        pdaScia3540.getScia3540_Ap_Ownership_Type().setValue(pdaScia3530.getScia3530_Aat_Instruct());                                                                     //Natural: ASSIGN SCIA3540.AP-OWNERSHIP-TYPE := SCIA3530.AAT-INSTRUCT
        pdaScia3540.getScia3540_Ap_Lob_Type().setValue(pdaScia3530.getScia3530_Aat_Lob_Type());                                                                           //Natural: ASSIGN SCIA3540.AP-LOB-TYPE := SCIA3530.AAT-LOB-TYPE
        pdaScia3540.getScia3540_Ap_Split_Code().setValue(pdaScia3530.getScia3530_Aat_Split_Code());                                                                       //Natural: ASSIGN SCIA3540.AP-SPLIT-CODE := SCIA3530.AAT-SPLIT-CODE
        pnd_Hold_Lob_Type_Full_Pnd_Hold_Lob.reset();                                                                                                                      //Natural: RESET #HOLD-LOB #HOLD-LOB-TYPE #HOLD-LOB-TYPE-FULL
        pnd_Hold_Lob_Type_Full_Pnd_Hold_Lob_Type.reset();
        pnd_Hold_Lob_Type_Full.reset();
        pnd_Hold_Lob_Type_Full_Pnd_Hold_Lob.setValue(pdaScia3530.getScia3530_Aat_Lob());                                                                                  //Natural: ASSIGN #HOLD-LOB := SCIA3530.AAT-LOB
        pnd_Hold_Lob_Type_Full_Pnd_Hold_Lob_Type.setValue(pdaScia3530.getScia3530_Aat_Lob_Type());                                                                        //Natural: ASSIGN #HOLD-LOB-TYPE := SCIA3530.AAT-LOB-TYPE
        //*  RA AND SRA
        //*  IRA
        //*  INDEXED IRA
        if (condition(pnd_Hold_Lob_Type_Full.equals("D2") || pnd_Hold_Lob_Type_Full.equals("S2") || pnd_Hold_Lob_Type_Full.equals("I3") || pnd_Hold_Lob_Type_Full.equals("I4")  //Natural: IF #HOLD-LOB-TYPE-FULL EQ 'D2' OR EQ 'S2' OR EQ 'I3' OR EQ 'I4' OR EQ 'I6' OR EQ 'I7' OR EQ 'I8' OR EQ 'I9'
            || pnd_Hold_Lob_Type_Full.equals("I6") || pnd_Hold_Lob_Type_Full.equals("I7") || pnd_Hold_Lob_Type_Full.equals("I8") || pnd_Hold_Lob_Type_Full.equals("I9")))
        {
            pnd_K.reset();                                                                                                                                                //Natural: RESET #K #STATE-2
            pnd_State_2.reset();
            DbsUtil.examine(new ExamineSource(ldaScil1080.getPnd_State_Zipcode_Array_Pnd_State_Zip_Idx().getValue("*"),1,2), new ExamineSearch(pdaScia9000.getScia9000_Pnd_Issue_State()),  //Natural: EXAMINE SUBSTR ( #STATE-ZIP-IDX ( * ) ,1,2 ) FOR SCIA9000.#ISSUE-STATE GIVING INDEX #K
                new ExamineGivingIndex(pnd_K));
            if (condition(pnd_K.notEquals(getZero())))                                                                                                                    //Natural: IF #K NE 0
            {
                pnd_State_2.setValue(ldaScil1080.getPnd_State_Zipcode_Array_Pnd_State_Zip_Idx().getValue(pnd_K).getSubstring(3,2));                                       //Natural: MOVE SUBSTR ( #STATE-ZIP-IDX ( #K ) ,3,2 ) TO #STATE-2
                pdaScia3540.getScia3540_Ap_Current_State_Code().setValue(pnd_State_2);                                                                                    //Natural: ASSIGN SCIA3540.AP-CURRENT-STATE-CODE := #STATE-2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia3540.getScia3540_Ap_Current_State_Code().setValue(pdaScia3530.getScia3530_Aat_Current_State_Code());                                               //Natural: ASSIGN SCIA3540.AP-CURRENT-STATE-CODE := SCIA3530.AAT-CURRENT-STATE-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia3540.getScia3540_Ap_Current_State_Code().setValue(pdaScia3530.getScia3530_Aat_Current_State_Code());                                                   //Natural: ASSIGN SCIA3540.AP-CURRENT-STATE-CODE := SCIA3530.AAT-CURRENT-STATE-CODE
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3540.getScia3540_Ap_Dana_Transaction_Cd().setValue(pdaScia3530.getScia3530_Aat_Dana_Transaction_Cd());                                                     //Natural: ASSIGN SCIA3540.AP-DANA-TRANSACTION-CD := SCIA3530.AAT-DANA-TRANSACTION-CD
        pdaScia3540.getScia3540_Ap_Dana_Stndrd_Rtn_Cd().setValue(pdaScia3530.getScia3530_Aat_Dana_Stndrd_Rnt_Cd());                                                       //Natural: ASSIGN SCIA3540.AP-DANA-STNDRD-RTN-CD := SCIA3530.AAT-DANA-STNDRD-RNT-CD
        pdaScia3540.getScia3540_Ap_Dana_Finalist_Reason_Cd().setValue(pdaScia3530.getScia3530_Aat_Dana_Finalist_Reason_Cd());                                             //Natural: ASSIGN SCIA3540.AP-DANA-FINALIST-REASON-CD := SCIA3530.AAT-DANA-FINALIST-REASON-CD
        pdaScia3540.getScia3540_Ap_Dana_Addr_Stndrd_Code().setValue(pdaScia3530.getScia3530_Aat_Dana_Addr_Stndrd_Code());                                                 //Natural: ASSIGN SCIA3540.AP-DANA-ADDR-STNDRD-CODE := SCIA3530.AAT-DANA-ADDR-STNDRD-CODE
        pdaScia3540.getScia3540_Ap_Dana_Stndrd_Overide().setValue(pdaScia3530.getScia3530_Aat_Dana_Stndrd_Overide());                                                     //Natural: ASSIGN SCIA3540.AP-DANA-STNDRD-OVERIDE := SCIA3530.AAT-DANA-STNDRD-OVERIDE
        pdaScia3540.getScia3540_Ap_Dana_Postal_Data_Fields().setValue(pdaScia3530.getScia3530_Aat_Dana_Postal_Data_Fields());                                             //Natural: ASSIGN SCIA3540.AP-DANA-POSTAL-DATA-FIELDS := SCIA3530.AAT-DANA-POSTAL-DATA-FIELDS
        pdaScia3540.getScia3540_Ap_Dana_Addr_Geographic_Code().setValue(pdaScia3530.getScia3530_Aat_Dana_Addr_Geographic_Code());                                         //Natural: ASSIGN SCIA3540.AP-DANA-ADDR-GEOGRAPHIC-CODE := SCIA3530.AAT-DANA-ADDR-GEOGRAPHIC-CODE
        pdaScia3540.getScia3540_Ap_Annuity_Start_Date().setValue(pdaScia3530.getScia3530_Aat_Annuity_Start_Date());                                                       //Natural: ASSIGN SCIA3540.AP-ANNUITY-START-DATE := SCIA3530.AAT-ANNUITY-START-DATE
        pdaScia3540.getScia3540_Ap_Max_Alloc_Pct().getValue("*").setValue(pdaScia3530.getScia3530_Aat_Max_Alloc_Pct().getValue("*"));                                     //Natural: ASSIGN SCIA3540.AP-MAX-ALLOC-PCT ( * ) := SCIA3530.AAT-MAX-ALLOC-PCT ( * )
        pdaScia3540.getScia3540_Ap_Max_Alloc_Pct_Sign().getValue("*").setValue(pdaScia3530.getScia3530_Aat_Max_Alloc_Pct_Sign().getValue("*"));                           //Natural: ASSIGN SCIA3540.AP-MAX-ALLOC-PCT-SIGN ( * ) := SCIA3530.AAT-MAX-ALLOC-PCT-SIGN ( * )
        pdaScia3540.getScia3540_Ap_Bene_Info_Type().setValue(pdaScia3530.getScia3530_Aat_Bene_Info_Type());                                                               //Natural: ASSIGN SCIA3540.AP-BENE-INFO-TYPE := SCIA3530.AAT-BENE-INFO-TYPE
        pdaScia3540.getScia3540_Ap_Primary_Std_Ent().setValue(pdaScia3530.getScia3530_Aat_Primary_Std_Ent());                                                             //Natural: ASSIGN SCIA3540.AP-PRIMARY-STD-ENT := SCIA3530.AAT-PRIMARY-STD-ENT
        FOR08:                                                                                                                                                            //Natural: FOR #I = 1 TO 5
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
        {
            pnd_Switch_Line.setValue(pdaScia3530.getScia3530_Aat_Primary_Bene_Info().getValue(pnd_I));                                                                    //Natural: ASSIGN #SWITCH-LINE := SCIA3530.AAT-PRIMARY-BENE-INFO ( #I )
            if (condition(pnd_Switch_Line_Pnd_P_Ssn_A.equals("000000000")))                                                                                               //Natural: IF #P-SSN-A = '000000000'
            {
                pnd_Switch_Line_Pnd_P_Ssn_A.setValue(" ");                                                                                                                //Natural: ASSIGN #P-SSN-A := ' '
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Switch_Line_Pnd_P_Dob_A.equals("00000000")))                                                                                                //Natural: IF #P-DOB-A = '00000000'
            {
                pnd_Switch_Line_Pnd_P_Dob_A.setValue(" ");                                                                                                                //Natural: ASSIGN #P-DOB-A := ' '
            }                                                                                                                                                             //Natural: END-IF
            pdaScia3530.getScia3530_Aat_Primary_Bene_Info().getValue(pnd_I).setValue(pnd_Switch_Line);                                                                    //Natural: ASSIGN SCIA3530.AAT-PRIMARY-BENE-INFO ( #I ) := #SWITCH-LINE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaScia3540.getScia3540_Ap_Primary_Bene_Info().getValue("*").setValue(pdaScia3530.getScia3530_Aat_Primary_Bene_Info().getValue("*"));                             //Natural: ASSIGN SCIA3540.AP-PRIMARY-BENE-INFO ( * ) := SCIA3530.AAT-PRIMARY-BENE-INFO ( * )
        pdaScia3540.getScia3540_Ap_Contingent_Std_Ent().setValue(pdaScia3530.getScia3530_Aat_Bene_Contingent_Std_Ent());                                                  //Natural: ASSIGN SCIA3540.AP-CONTINGENT-STD-ENT := SCIA3530.AAT-BENE-CONTINGENT-STD-ENT
        FOR09:                                                                                                                                                            //Natural: FOR #I = 1 TO 5
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
        {
            pnd_Switch_Line.setValue(pdaScia3530.getScia3530_Aat_Contingent_Bene_Info().getValue(pnd_I));                                                                 //Natural: ASSIGN #SWITCH-LINE := SCIA3530.AAT-CONTINGENT-BENE-INFO ( #I )
            if (condition(pnd_Switch_Line_Pnd_P_Ssn_A.equals("000000000")))                                                                                               //Natural: IF #P-SSN-A = '000000000'
            {
                pnd_Switch_Line_Pnd_P_Ssn_A.setValue(" ");                                                                                                                //Natural: ASSIGN #P-SSN-A := ' '
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Switch_Line_Pnd_P_Dob_A.equals("00000000")))                                                                                                //Natural: IF #P-DOB-A = '00000000'
            {
                pnd_Switch_Line_Pnd_P_Dob_A.setValue(" ");                                                                                                                //Natural: ASSIGN #P-DOB-A := ' '
            }                                                                                                                                                             //Natural: END-IF
            pdaScia3530.getScia3530_Aat_Contingent_Bene_Info().getValue(pnd_I).setValue(pnd_Switch_Line);                                                                 //Natural: ASSIGN SCIA3530.AAT-CONTINGENT-BENE-INFO ( #I ) := #SWITCH-LINE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaScia3540.getScia3540_Ap_Contingent_Bene_Info().getValue("*").setValue(pdaScia3530.getScia3530_Aat_Contingent_Bene_Info().getValue("*"));                       //Natural: ASSIGN SCIA3540.AP-CONTINGENT-BENE-INFO ( * ) := SCIA3530.AAT-CONTINGENT-BENE-INFO ( * )
        pdaScia3540.getScia3540_Ap_Bene_Estate().setValue(pdaScia3530.getScia3530_Aat_Bene_Estate());                                                                     //Natural: ASSIGN SCIA3540.AP-BENE-ESTATE := SCIA3530.AAT-BENE-ESTATE
        pdaScia3540.getScia3540_Ap_Bene_Trust().setValue(pdaScia3530.getScia3530_Aat_Bene_Trust());                                                                       //Natural: ASSIGN SCIA3540.AP-BENE-TRUST := SCIA3530.AAT-BENE-TRUST
        pdaScia3540.getScia3540_Ap_Bene_Category().setValue(pdaScia3530.getScia3530_Aat_Bene_Category());                                                                 //Natural: ASSIGN SCIA3540.AP-BENE-CATEGORY := SCIA3530.AAT-BENE-CATEGORY
        pdaScia3540.getScia3540_Ap_Mail_Instructions().setValue(pdaScia3530.getScia3530_Aat_Mail_Instructions());                                                         //Natural: ASSIGN SCIA3540.AP-MAIL-INSTRUCTIONS := SCIA3530.AAT-MAIL-INSTRUCTIONS
        pdaScia3540.getScia3540_Ap_Eop_Addl_Cref_Request().setValue(pdaScia3530.getScia3530_Aat_Eop_Addl_Cref_Request());                                                 //Natural: ASSIGN SCIA3540.AP-EOP-ADDL-CREF-REQUEST := SCIA3530.AAT-EOP-ADDL-CREF-REQUEST
        //*  ILLINOIS SMP
        //*  NEEDED FOR OASIS
        if (condition(pdaScia3530.getScia3530_Aat_Sgrd_Plan_No().equals("100825")))                                                                                       //Natural: IF SCIA3530.AAT-SGRD-PLAN-NO = '100825'
        {
            pdaScia3540.getScia3540_Ap_Csm_Sec_Seg().setValue("ORP");                                                                                                     //Natural: ASSIGN SCIA3540.AP-CSM-SEC-SEG := 'ORP'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia3540.getScia3540_Ap_Csm_Sec_Seg().setValue(pdaScia3530.getScia3530_Aat_Csm_Sec_Seg());                                                                 //Natural: ASSIGN SCIA3540.AP-CSM-SEC-SEG := SCIA3530.AAT-CSM-SEC-SEG
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3540.getScia3540_Ap_Coll_St_Cd().setValue(pdaScia3530.getScia3530_Aat_Coll_St_Cd());                                                                       //Natural: ASSIGN SCIA3540.AP-COLL-ST-CD := SCIA3530.AAT-COLL-ST-CD
        pdaScia3540.getScia3540_Ap_Cor_Prfx_Nme().setValue(pdaScia3530.getScia3530_Aat_Cor_Prfx_Nme());                                                                   //Natural: ASSIGN SCIA3540.AP-COR-PRFX-NME := SCIA3530.AAT-COR-PRFX-NME
        pdaScia3540.getScia3540_Ap_Cor_Last_Nme().setValue(pdaScia3530.getScia3530_Aat_Cor_Last_Nme());                                                                   //Natural: ASSIGN SCIA3540.AP-COR-LAST-NME := SCIA3530.AAT-COR-LAST-NME
        pdaScia3540.getScia3540_Ap_Cor_First_Nme().setValue(pdaScia3530.getScia3530_Aat_Cor_First_Nme());                                                                 //Natural: ASSIGN SCIA3540.AP-COR-FIRST-NME := SCIA3530.AAT-COR-FIRST-NME
        pdaScia3540.getScia3540_Ap_Cor_Mddle_Nme().setValue(pdaScia3530.getScia3530_Aat_Cor_Mddle_Nme());                                                                 //Natural: ASSIGN SCIA3540.AP-COR-MDDLE-NME := SCIA3530.AAT-COR-MDDLE-NME
        pdaScia3540.getScia3540_Ap_Cor_Sffx_Nme().setValue(pdaScia3530.getScia3530_Aat_Cor_Sffx_Nme());                                                                   //Natural: ASSIGN SCIA3540.AP-COR-SFFX-NME := SCIA3530.AAT-COR-SFFX-NME
        pdaScia3540.getScia3540_Ap_Ph_Hist_Ind().setValue(pdaScia3530.getScia3530_Aat_Ph_Hist_Ind());                                                                     //Natural: ASSIGN SCIA3540.AP-PH-HIST-IND := SCIA3530.AAT-PH-HIST-IND
        pdaScia3540.getScia3540_Ap_Racf_Id().setValue(pdaScia3530.getScia3530_Aat_Racf_Id());                                                                             //Natural: ASSIGN SCIA3540.AP-RACF-ID := SCIA3530.AAT-RACF-ID
        pdaScia3540.getScia3540_Ap_Pin_Nbr().setValue(pdaScia3530.getScia3530_Aat_Pin_Nbr());                                                                             //Natural: ASSIGN SCIA3540.AP-PIN-NBR := SCIA3530.AAT-PIN-NBR
        if (condition(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mit_Log_Dte_Tme_Sub().equals(" ")))                                                                             //Natural: IF SCIA3500.###MIT-LOG-DTE-TME-SUB EQ ' '
        {
            pdaScia3540.getScia3540_Ap_Rqst_Log_Dte_Time().getValue(1).setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mit_Log_Dte_Tme());                                   //Natural: ASSIGN SCIA3540.AP-RQST-LOG-DTE-TIME ( 1 ) := SCIA3500.###MIT-LOG-DTE-TME
            pdaScia3540.getScia3540_Ap_Rqst_Log_Dte_Time().getValue(3).setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mit_Log_Dte_Tme());                                   //Natural: ASSIGN SCIA3540.AP-RQST-LOG-DTE-TIME ( 3 ) := SCIA3500.###MIT-LOG-DTE-TME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia3540.getScia3540_Ap_Rqst_Log_Dte_Time().getValue(1).setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mit_Log_Dte_Tme_Sub());                               //Natural: ASSIGN SCIA3540.AP-RQST-LOG-DTE-TIME ( 1 ) := SCIA3500.###MIT-LOG-DTE-TME-SUB
            pdaScia3540.getScia3540_Ap_Rqst_Log_Dte_Time().getValue(3).setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mit_Log_Dte_Tme());                                   //Natural: ASSIGN SCIA3540.AP-RQST-LOG-DTE-TIME ( 3 ) := SCIA3500.###MIT-LOG-DTE-TME
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Premium_Team_Cde().equals(" ")))                                                                                //Natural: IF SCIA3500.###PREMIUM-TEAM-CDE = ' '
        {
            pdaScia3540.getScia3540_Ap_Mit_Unit().getValue(1).setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Existing_Rqst_Log_Unit());                                     //Natural: ASSIGN SCIA3540.AP-MIT-UNIT ( 1 ) := SCIA3500.###EXISTING-RQST-LOG-UNIT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia3540.getScia3540_Ap_Mit_Unit().getValue(1).setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Premium_Team_Cde());                                           //Natural: ASSIGN SCIA3540.AP-MIT-UNIT ( 1 ) := SCIA3500.###PREMIUM-TEAM-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mit_Wpid().notEquals(" ")))                                                                                     //Natural: IF SCIA3500.###MIT-WPID NE ' '
        {
            pdaScia3540.getScia3540_Ap_Mit_Wpid().getValue(1).setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mit_Wpid());                                                   //Natural: ASSIGN SCIA3540.AP-MIT-WPID ( 1 ) := SCIA3500.###MIT-WPID
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia3540.getScia3540_Ap_Mit_Wpid().getValue(1).setValue(pnd_Hold_Mit_Wpid.getValue(1));                                                                    //Natural: ASSIGN SCIA3540.AP-MIT-WPID ( 1 ) := #HOLD-MIT-WPID ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3540.getScia3540_Ap_Mult_App_Status().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mult_App_Status());                                                     //Natural: ASSIGN SCIA3540.AP-MULT-APP-STATUS := SCIA3500.###MULT-APP-STATUS
        pdaScia3540.getScia3540_Ap_Mult_App_Lob().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mult_App_Lob());                                                           //Natural: ASSIGN SCIA3540.AP-MULT-APP-LOB := SCIA3500.###MULT-APP-LOB
        pdaScia3540.getScia3540_Ap_Mult_App_Lob_Type().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mult_App_Lob_Type());                                                 //Natural: ASSIGN SCIA3540.AP-MULT-APP-LOB-TYPE := SCIA3500.###MULT-APP-LOB-TYPE
        pdaScia3540.getScia3540_Ap_Mult_App_Ppg().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mult_App_Ppg());                                                           //Natural: ASSIGN SCIA3540.AP-MULT-APP-PPG := SCIA3500.###MULT-APP-PPG
        pdaScia3540.getScia3540_Ap_Tiaa_Doi().setValue(pdaScia3530.getScia3530_Aat_Tiaa_Doi());                                                                           //Natural: ASSIGN SCIA3540.AP-TIAA-DOI := SCIA3530.AAT-TIAA-DOI
        //*  COMPANION
        if (condition(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind().equals("C") || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind().equals("T")))                    //Natural: IF SCIA3500.###PROCESS-IND = 'C' OR = 'T'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Cor_Msg_Pnd_Cor_Oia_Ind.equals("10")))                                                                                                      //Natural: IF #COR-OIA-IND = '10'
            {
                pdaScia3540.getScia3540_Ap_Cref_Doi().reset();                                                                                                            //Natural: RESET SCIA3540.AP-CREF-DOI
                pdaScia3540.getScia3540_Ap_C_Doi().reset();                                                                                                               //Natural: RESET SCIA3540.AP-C-DOI
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia3540.getScia3540_Ap_Cref_Doi().setValue(pdaScia3530.getScia3530_Aat_Cref_Doi());                                                                   //Natural: ASSIGN SCIA3540.AP-CREF-DOI := SCIA3530.AAT-CREF-DOI
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cor_Msg_Pnd_Cor_Oia_Ind.equals("11")))                                                                                                      //Natural: IF #COR-OIA-IND = '11'
            {
                pdaScia3540.getScia3540_Ap_Tiaa_Doi().reset();                                                                                                            //Natural: RESET SCIA3540.AP-TIAA-DOI
                pdaScia3540.getScia3540_Ap_T_Doi().reset();                                                                                                               //Natural: RESET SCIA3540.AP-T-DOI
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia3540.getScia3540_Ap_Tiaa_Doi().setValue(pdaScia3530.getScia3530_Aat_Tiaa_Doi());                                                                   //Natural: ASSIGN SCIA3540.AP-TIAA-DOI := SCIA3530.AAT-TIAA-DOI
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3540.getScia3540_Ap_Ira_Rollover_Type().setValue(pdaScia3530.getScia3530_Aat_Ira_Rollover_Type());                                                         //Natural: ASSIGN SCIA3540.AP-IRA-ROLLOVER-TYPE := SCIA3530.AAT-IRA-ROLLOVER-TYPE
        pdaScia3540.getScia3540_Ap_Ira_Record_Type().setValue(pdaScia3530.getScia3530_Aat_Ira_Record_Type());                                                             //Natural: ASSIGN SCIA3540.AP-IRA-RECORD-TYPE := SCIA3530.AAT-IRA-RECORD-TYPE
        pdaScia3540.getScia3540_Ap_Cntrct_Type().getValue("*").setValue(pdaScia3530.getScia3530_Aat_Cntrct_Type().getValue("*"));                                         //Natural: ASSIGN SCIA3540.AP-CNTRCT-TYPE ( * ) := SCIA3530.AAT-CNTRCT-TYPE ( * )
        pdaScia3540.getScia3540_Ap_Cntrct_Nbr().getValue("*").setValue(pdaScia3530.getScia3530_Aat_Cntrct_Nbr().getValue("*"));                                           //Natural: ASSIGN SCIA3540.AP-CNTRCT-NBR ( * ) := SCIA3530.AAT-CNTRCT-NBR ( * )
        pdaScia3540.getScia3540_Ap_Cntrct_Proceeds_Amt().getValue("*").setValue(pdaScia3530.getScia3530_Aat_Cntrct_Proceeds_Amt().getValue("*"));                         //Natural: ASSIGN SCIA3540.AP-CNTRCT-PROCEEDS-AMT ( * ) := SCIA3530.AAT-CNTRCT-PROCEEDS-AMT ( * )
        pdaScia3540.getScia3540_Ap_Address_Txt().getValue("*").setValue(pdaScia3530.getScia3530_Aat_Address_Txt().getValue("*"));                                         //Natural: ASSIGN SCIA3540.AP-ADDRESS-TXT ( * ) := SCIA3530.AAT-ADDRESS-TXT ( * )
        pdaScia3540.getScia3540_Ap_Address_Dest_Name().setValue(pdaScia3530.getScia3530_Aat_Address_Dest_Name());                                                         //Natural: ASSIGN SCIA3540.AP-ADDRESS-DEST-NAME := SCIA3530.AAT-ADDRESS-DEST-NAME
        pdaScia3540.getScia3540_Ap_Zip_Code().setValue(pdaScia3530.getScia3530_Aat_Zip_Code());                                                                           //Natural: ASSIGN SCIA3540.AP-ZIP-CODE := SCIA3530.AAT-ZIP-CODE
        pdaScia3540.getScia3540_Ap_Bank_Pymnt_Acct_Nmbr().setValue(pdaScia3530.getScia3530_Aat_Bank_Pymnt_Acct_Nmbr());                                                   //Natural: ASSIGN SCIA3540.AP-BANK-PYMNT-ACCT-NMBR := SCIA3530.AAT-BANK-PYMNT-ACCT-NMBR
        pdaScia3540.getScia3540_Ap_Bank_Aba_Acct_Nmbr().setValue(pdaScia3530.getScia3530_Aat_Bank_Aba_Acct_Nmbr());                                                       //Natural: ASSIGN SCIA3540.AP-BANK-ABA-ACCT-NMBR := SCIA3530.AAT-BANK-ABA-ACCT-NMBR
        pdaScia3540.getScia3540_Ap_Addr_Usage_Code().setValue(pdaScia3530.getScia3530_Aat_Addr_Usage_Code());                                                             //Natural: ASSIGN SCIA3540.AP-ADDR-USAGE-CODE := SCIA3530.AAT-ADDR-USAGE-CODE
        pdaScia3540.getScia3540_Ap_Init_Paymt_Amt().setValue(pdaScia3530.getScia3530_Aat_Init_Paymt_Amt());                                                               //Natural: ASSIGN SCIA3540.AP-INIT-PAYMT-AMT := SCIA3530.AAT-INIT-PAYMT-AMT
        pdaScia3540.getScia3540_Ap_Init_Paymt_Pct().setValue(pdaScia3530.getScia3530_Aat_Init_Paymt_Pct());                                                               //Natural: ASSIGN SCIA3540.AP-INIT-PAYMT-PCT := SCIA3530.AAT-INIT-PAYMT-PCT
        pdaScia3540.getScia3540_Ap_Financial_1().setValue(pdaScia3530.getScia3530_Aat_Financial_1());                                                                     //Natural: ASSIGN SCIA3540.AP-FINANCIAL-1 := SCIA3530.AAT-FINANCIAL-1
        pdaScia3540.getScia3540_Ap_Financial_2().setValue(pdaScia3530.getScia3530_Aat_Financial_2());                                                                     //Natural: ASSIGN SCIA3540.AP-FINANCIAL-2 := SCIA3530.AAT-FINANCIAL-2
        pdaScia3540.getScia3540_Ap_Financial_3().setValue(pdaScia3530.getScia3530_Aat_Financial_3());                                                                     //Natural: ASSIGN SCIA3540.AP-FINANCIAL-3 := SCIA3530.AAT-FINANCIAL-3
        pdaScia3540.getScia3540_Ap_Financial_4().setValue(pdaScia3530.getScia3530_Aat_Financial_4());                                                                     //Natural: ASSIGN SCIA3540.AP-FINANCIAL-4 := SCIA3530.AAT-FINANCIAL-4
        pdaScia3540.getScia3540_Ap_Financial_5().setValue(pdaScia3530.getScia3530_Aat_Financial_5());                                                                     //Natural: ASSIGN SCIA3540.AP-FINANCIAL-5 := SCIA3530.AAT-FINANCIAL-5
        if (condition((pdaScia3540.getScia3540_Ap_Lob().equals("I") && pdaScia3540.getScia3540_Ap_Lob_Type().equals("5"))))                                               //Natural: IF ( SCIA3540.AP-LOB = 'I' AND SCIA3540.AP-LOB-TYPE = '5' )
        {
            pdaScia3590.getScia3590_Pnd_College_State_Code().setValue(pdaScia3530.getScia3530_Aat_Coll_St_Cd());                                                          //Natural: ASSIGN SCIA3590.#COLLEGE-STATE-CODE = SCIA3530.AAT-COLL-ST-CD
            DbsUtil.callnat(Scin3590.class , getCurrentProcessState(), pdaScia3590.getScia3590());                                                                        //Natural: CALLNAT 'SCIN3590' SCIA3590
            if (condition(Global.isEscape())) return;
            pdaScia3540.getScia3540_Ap_Coll_St_Cd().setValue(pdaScia3590.getScia3590_Pnd_College_State_Code());                                                           //Natural: ASSIGN SCIA3540.AP-COLL-ST-CD = SCIA3590.#COLLEGE-STATE-CODE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((pdaScia3540.getScia3540_Ap_Lob().equals("S") && pdaScia3540.getScia3540_Ap_Lob_Type().equals("3")) || (pdaScia3540.getScia3540_Ap_Lob().equals("S")  //Natural: IF ( SCIA3540.AP-LOB = 'S' AND SCIA3540.AP-LOB-TYPE = '3' ) OR ( SCIA3540.AP-LOB = 'S' AND SCIA3540.AP-LOB-TYPE = '4' ) OR ( SCIA3540.AP-LOB = 'S' AND SCIA3540.AP-LOB-TYPE = '5' ) OR ( SCIA3540.AP-LOB = 'S' AND SCIA3540.AP-LOB-TYPE = '6' ) OR ( SCIA3540.AP-LOB = 'S' AND SCIA3540.AP-LOB-TYPE = '7' ) OR ( SCIA3540.AP-LOB = 'D' AND SCIA3540.AP-LOB-TYPE = '5' ) OR ( SCIA3540.AP-LOB = 'D' AND SCIA3540.AP-LOB-TYPE = '6' ) OR ( SCIA3540.AP-LOB = 'D' AND SCIA3540.AP-LOB-TYPE = '7' ) OR ( SCIA3540.AP-LOB = 'D' AND SCIA3540.AP-LOB-TYPE = 'A' )
            && pdaScia3540.getScia3540_Ap_Lob_Type().equals("4")) || (pdaScia3540.getScia3540_Ap_Lob().equals("S") && pdaScia3540.getScia3540_Ap_Lob_Type().equals("5")) 
            || (pdaScia3540.getScia3540_Ap_Lob().equals("S") && pdaScia3540.getScia3540_Ap_Lob_Type().equals("6")) || (pdaScia3540.getScia3540_Ap_Lob().equals("S") 
            && pdaScia3540.getScia3540_Ap_Lob_Type().equals("7")) || (pdaScia3540.getScia3540_Ap_Lob().equals("D") && pdaScia3540.getScia3540_Ap_Lob_Type().equals("5")) 
            || (pdaScia3540.getScia3540_Ap_Lob().equals("D") && pdaScia3540.getScia3540_Ap_Lob_Type().equals("6")) || (pdaScia3540.getScia3540_Ap_Lob().equals("D") 
            && pdaScia3540.getScia3540_Ap_Lob_Type().equals("7")) || (pdaScia3540.getScia3540_Ap_Lob().equals("D") && pdaScia3540.getScia3540_Ap_Lob_Type().equals("A"))))
        {
            pdaScia3590.getScia3590_Pnd_College_State_Code().setValue(pdaScia3530.getScia3530_Aat_Coll_St_Cd());                                                          //Natural: ASSIGN SCIA3590.#COLLEGE-STATE-CODE = SCIA3530.AAT-COLL-ST-CD
            DbsUtil.callnat(Scin3590.class , getCurrentProcessState(), pdaScia3590.getScia3590());                                                                        //Natural: CALLNAT 'SCIN3590' SCIA3590
            if (condition(Global.isEscape())) return;
            pdaScia3540.getScia3540_Ap_Coll_St_Cd().setValue(pdaScia3590.getScia3590_Pnd_College_State_Code());                                                           //Natural: ASSIGN SCIA3540.AP-COLL-ST-CD = SCIA3590.#COLLEGE-STATE-CODE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaScia3530.getScia3530_Aat_Lob().equals("D")))                                                                                                     //Natural: IF SCIA3530.AAT-LOB = 'D'
        {
            if (condition(pdaScia3530.getScia3530_Aat_Lob_Type().equals("6")))                                                                                            //Natural: IF SCIA3530.AAT-LOB-TYPE = '6'
            {
                pdaScia3540.getScia3540_Ap_Rlc_College().setValue(pdaScia3530.getScia3530_Aat_Rlc_College());                                                             //Natural: ASSIGN SCIA3540.AP-RLC-COLLEGE := SCIA3530.AAT-RLC-COLLEGE
                pdaScia3540.getScia3540_Ap_Rlc_Cref_Cert().setValue(pdaScia3530.getScia3530_Aat_Rlc_Cref_Cert());                                                         //Natural: ASSIGN SCIA3540.AP-RLC-CREF-CERT := SCIA3530.AAT-RLC-CREF-CERT
                pdaScia3540.getScia3540_Ap_Status().setValue("C");                                                                                                        //Natural: ASSIGN SCIA3540.AP-STATUS := 'C'
            }                                                                                                                                                             //Natural: END-IF
            //*  IISG <<<
            //*  DTM 12/08
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3540.getScia3540_Ap_Allocation_Fmt().setValue(pdaScia3530.getScia3530_Aat_Allocation_Fmt());                                                               //Natural: ASSIGN SCIA3540.AP-ALLOCATION-FMT := SCIA3530.AAT-ALLOCATION-FMT
        pdaScia3540.getScia3540_Ap_Allocation_Pct().getValue("*").setValue(pdaScia3530.getScia3530_Aat_Allocation_Pct().getValue("*"));                                   //Natural: ASSIGN SCIA3540.AP-ALLOCATION-PCT ( * ) := SCIA3530.AAT-ALLOCATION-PCT ( * )
        pdaScia3540.getScia3540_Ap_Fund_Cde().getValue("*").setValue(pdaScia3530.getScia3530_Aat_Fund_Cde().getValue("*"));                                               //Natural: ASSIGN SCIA3540.AP-FUND-CDE ( * ) := SCIA3530.AAT-FUND-CDE ( * )
        pdaScia3540.getScia3540_Ap_Fund_Source_Cde_1().setValue(pdaScia3530.getScia3530_Aat_Fund_Source_Cde_1());                                                         //Natural: ASSIGN SCIA3540.AP-FUND-SOURCE-CDE-1 := SCIA3530.AAT-FUND-SOURCE-CDE-1
        pdaScia3540.getScia3540_Ap_Fund_Source_Cde_2().setValue(pdaScia3530.getScia3530_Aat_Fund_Source_Cde_2());                                                         //Natural: ASSIGN SCIA3540.AP-FUND-SOURCE-CDE-2 := SCIA3530.AAT-FUND-SOURCE-CDE-2
        pdaScia3540.getScia3540_Ap_Allocation_Pct_2().getValue("*").setValue(pdaScia3530.getScia3530_Aat_Allocation_Pct_2().getValue("*"));                               //Natural: ASSIGN SCIA3540.AP-ALLOCATION-PCT-2 ( * ) := SCIA3530.AAT-ALLOCATION-PCT-2 ( * )
        pdaScia3540.getScia3540_Ap_Fund_Cde_2().getValue("*").setValue(pdaScia3530.getScia3530_Aat_Fund_Cde_2().getValue("*"));                                           //Natural: ASSIGN SCIA3540.AP-FUND-CDE-2 ( * ) := SCIA3530.AAT-FUND-CDE-2 ( * )
        pdaScia3540.getScia3540_Ap_Sgrd_Plan_No().setValue(pdaScia3530.getScia3530_Aat_Sgrd_Plan_No());                                                                   //Natural: ASSIGN SCIA3540.AP-SGRD-PLAN-NO := SCIA3530.AAT-SGRD-PLAN-NO
        pdaScia3540.getScia3540_Ap_Sgrd_Subplan_No().setValue(pdaScia3530.getScia3530_Aat_Sgrd_Subplan_No());                                                             //Natural: ASSIGN SCIA3540.AP-SGRD-SUBPLAN-NO := SCIA3530.AAT-SGRD-SUBPLAN-NO
        pdaScia3540.getScia3540_Ap_Sgrd_Part_Ext().setValue(pdaScia3530.getScia3530_Aat_Sgrd_Part_Ext());                                                                 //Natural: ASSIGN SCIA3540.AP-SGRD-PART-EXT := SCIA3530.AAT-SGRD-PART-EXT
        pdaScia3540.getScia3540_Ap_Sgrd_Divsub().setValue(pdaScia3530.getScia3530_Aat_Sgrd_Divsub());                                                                     //Natural: ASSIGN SCIA3540.AP-SGRD-DIVSUB := SCIA3530.AAT-SGRD-DIVSUB
        pdaScia3540.getScia3540_Ap_Text_Udf_1().setValue(pdaScia3530.getScia3530_Aat_Text_Udf_1());                                                                       //Natural: ASSIGN SCIA3540.AP-TEXT-UDF-1 := SCIA3530.AAT-TEXT-UDF-1
        pdaScia3540.getScia3540_Ap_Text_Udf_2().setValue(pdaScia3530.getScia3530_Aat_Text_Udf_2());                                                                       //Natural: ASSIGN SCIA3540.AP-TEXT-UDF-2 := SCIA3530.AAT-TEXT-UDF-2
        pdaScia3540.getScia3540_Ap_Text_Udf_3().setValue(pdaScia3530.getScia3530_Aat_Text_Udf_3());                                                                       //Natural: ASSIGN SCIA3540.AP-TEXT-UDF-3 := SCIA3530.AAT-TEXT-UDF-3
        pdaScia3540.getScia3540_Ap_Replacement_Ind().setValue(pdaScia3530.getScia3530_Aat_Sgrd_Replacement_Ind());                                                        //Natural: ASSIGN SCIA3540.AP-REPLACEMENT-IND := SCIA3530.AAT-SGRD-REPLACEMENT-IND
        pdaScia3540.getScia3540_Ap_Agent_Or_Racf_Id().setValue(pdaScia3530.getScia3530_Aat_Sgrd_Agent_Racf_Id());                                                         //Natural: ASSIGN SCIA3540.AP-AGENT-OR-RACF-ID := SCIA3530.AAT-SGRD-AGENT-RACF-ID
        pdaScia3540.getScia3540_Ap_Exempt_Ind().setValue(pdaScia3530.getScia3530_Aat_Sgrd_Exempt_Ind());                                                                  //Natural: ASSIGN SCIA3540.AP-EXEMPT-IND := SCIA3530.AAT-SGRD-EXEMPT-IND
        if (condition(pnd_Non_T_C_Administered_Plan.getBoolean() || pdaScia3530.getScia3530_Aat_Substitution_Contract_Ind().equals("W") || pdaScia3530.getScia3530_Aat_Substitution_Contract_Ind().equals("O")  //Natural: IF #NON-T-C-ADMINISTERED-PLAN OR ( SCIA3530.AAT-SUBSTITUTION-CONTRACT-IND = 'W' OR = 'O' ) OR SCIA3530.AAT-DECEDENT-CONTRACT GT ' '
            || pdaScia3530.getScia3530_Aat_Decedent_Contract().greater(" ")))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaScia3530.getScia3530_Aat_Sgrd_Register_Id().greater(" ")))                                                                                   //Natural: IF SCIA3530.AAT-SGRD-REGISTER-ID GT ' '
            {
                pdaScia3540.getScia3540_Ap_Register_Id().setValue(pdaScia3530.getScia3530_Aat_Sgrd_Register_Id());                                                        //Natural: ASSIGN SCIA3540.AP-REGISTER-ID := SCIA3530.AAT-SGRD-REGISTER-ID
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                scia8580.reset();                                                                                                                                         //Natural: RESET SCIA8580
                //*  'G'ENERATE
                scia8580_Pnd_Action_Code.setValue("G");                                                                                                                   //Natural: MOVE 'G' TO #ACTION-CODE
                DbsUtil.callnat(Scin8580.class , getCurrentProcessState(), scia8580);                                                                                     //Natural: CALLNAT 'SCIN8580' SCIA8580
                if (condition(Global.isEscape())) return;
                if (condition(scia8580_Pnd_Return_Code.greater(getZero())))                                                                                               //Natural: IF SCIA8580.#RETURN-CODE GT 0
                {
                    pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("SCIN8580 - ERROR GENERATING REGISTER ID");                                                        //Natural: ASSIGN MSG-INFO-SUB.##MSG := 'SCIN8580 - ERROR GENERATING REGISTER ID'
                    getReports().write(0, pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                     //Natural: WRITE MSG-INFO-SUB.##MSG
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                pdaScia3540.getScia3540_Ap_Register_Id().setValue(scia8580_Pnd_Register_Id);                                                                              //Natural: ASSIGN SCIA3540.AP-REGISTER-ID := SCIA8580.#REGISTER-ID
            }                                                                                                                                                             //Natural: END-IF
            //*  AB 5-7-07
            //*  AB 5-7-07
            //*  AB 5-7-07
            //*  AB 5-7-07
            //*  AB 5-7-07
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3530.getScia3530_Aat_Sgrd_Register_Id().setValue(pdaScia3540.getScia3540_Ap_Register_Id());                                                                //Natural: ASSIGN SCIA3530.AAT-SGRD-REGISTER-ID := SCIA3540.AP-REGISTER-ID
        pdaScia3540.getScia3540_Ap_Arr_Insurer_1().setValue(pdaScia3530.getScia3530_Aat_Arr_Insurer_1());                                                                 //Natural: ASSIGN SCIA3540.AP-ARR-INSURER-1 := SCIA3530.AAT-ARR-INSURER-1
        pdaScia3540.getScia3540_Ap_Arr_Insurer_2().setValue(pdaScia3530.getScia3530_Aat_Arr_Insurer_2());                                                                 //Natural: ASSIGN SCIA3540.AP-ARR-INSURER-2 := SCIA3530.AAT-ARR-INSURER-2
        pdaScia3540.getScia3540_Ap_Arr_Insurer_3().setValue(pdaScia3530.getScia3530_Aat_Arr_Insurer_3());                                                                 //Natural: ASSIGN SCIA3540.AP-ARR-INSURER-3 := SCIA3530.AAT-ARR-INSURER-3
        pdaScia3540.getScia3540_Ap_Arr_1035_Exch_Ind_1().setValue(pdaScia3530.getScia3530_Aat_Arr_1035_Exch_Ind_1());                                                     //Natural: ASSIGN SCIA3540.AP-ARR-1035-EXCH-IND-1 := SCIA3530.AAT-ARR-1035-EXCH-IND-1
        pdaScia3540.getScia3540_Ap_Arr_1035_Exch_Ind_2().setValue(pdaScia3530.getScia3530_Aat_Arr_1035_Exch_Ind_2());                                                     //Natural: ASSIGN SCIA3540.AP-ARR-1035-EXCH-IND-2 := SCIA3530.AAT-ARR-1035-EXCH-IND-2
        pdaScia3540.getScia3540_Ap_Arr_1035_Exch_Ind_3().setValue(pdaScia3530.getScia3530_Aat_Arr_1035_Exch_Ind_3());                                                     //Natural: ASSIGN SCIA3540.AP-ARR-1035-EXCH-IND-3 := SCIA3530.AAT-ARR-1035-EXCH-IND-3
        pdaScia3540.getScia3540_Ap_Irc_Sectn_Cde().setValue(pdaScia3530.getScia3530_Aat_Irc_Sectn_Cde());                                                                 //Natural: ASSIGN SCIA3540.AP-IRC-SECTN-CDE := SCIA3530.AAT-IRC-SECTN-CDE
        pdaScia3540.getScia3540_Ap_Irc_Sectn_Grp_Cde().setValue(pdaScia3530.getScia3530_Aat_Irc_Sectn_Grp_Cde());                                                         //Natural: ASSIGN SCIA3540.AP-IRC-SECTN-GRP-CDE := SCIA3530.AAT-IRC-SECTN-GRP-CDE
        pdaScia3540.getScia3540_Ap_Inst_Link_Cde().setValue(pdaScia3530.getScia3530_Aat_Inst_Link_Cde());                                                                 //Natural: ASSIGN SCIA3540.AP-INST-LINK-CDE := SCIA3530.AAT-INST-LINK-CDE
        pdaScia1102.getScia1102_Pnd_Entry_Actve_Ind().setValue("Y");                                                                                                      //Natural: ASSIGN SCIA1102.#ENTRY-ACTVE-IND := 'Y'
        pdaScia1102.getScia1102_Pnd_Entry_Table_Id_Nbr().setValue(100);                                                                                                   //Natural: ASSIGN SCIA1102.#ENTRY-TABLE-ID-NBR := 000100
        pdaScia1102.getScia1102_Pnd_Entry_Table_Sub_Id().setValue("LB");                                                                                                  //Natural: ASSIGN SCIA1102.#ENTRY-TABLE-SUB-ID := 'LB'
        pdaScia1102.getScia1102_Pnd_Contract_Type().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Contract_Type());                                                        //Natural: ASSIGN SCIA1102.#CONTRACT-TYPE := SCIA3500.###CONTRACT-TYPE
        DbsUtil.callnat(Scin1102.class , getCurrentProcessState(), pdaScia1102.getScia1102());                                                                            //Natural: CALLNAT 'SCIN1102' SCIA1102
        if (condition(Global.isEscape())) return;
        pnd_Addtnl_Txt.setValue(pdaScia1102.getScia1102_Pnd_Addtnl_Dscrptn_Txt());                                                                                        //Natural: ASSIGN #ADDTNL-TXT := SCIA1102.#ADDTNL-DSCRPTN-TXT
        pnd_Dscrptn_Txt.setValue(pdaScia1102.getScia1102_Pnd_Entry_Dscrptn_Txt());                                                                                        //Natural: ASSIGN #DSCRPTN-TXT := SCIA1102.#ENTRY-DSCRPTN-TXT
        pdaScia1105.getScia1105_Vesting().setValue(pnd_Addtnl_Txt_Pnd_Vesting_Local);                                                                                     //Natural: ASSIGN SCIA1105.VESTING := #VESTING-LOCAL
        pdaScia1105.getScia1105_Prdct_Cde().setValue(pnd_Addtnl_Txt_Pnd_Prdct_Cd);                                                                                        //Natural: ASSIGN SCIA1105.PRDCT-CDE := #PRDCT-CD
        if (condition(pdaScia3530.getScia3530_Aat_Appl_Status().equals("R")))                                                                                             //Natural: IF SCIA3530.AAT-APPL-STATUS EQ 'R'
        {
            pdaScia3540.getScia3540_Ap_Applcnt_Req_Type().setValue("I");                                                                                                  //Natural: ASSIGN SCIA3540.AP-APPLCNT-REQ-TYPE := 'I'
            if (condition(pdaScia3540.getScia3540_Ap_Mult_App_Lob().notEquals(" ") && pdaScia3540.getScia3540_Ap_Mult_App_Lob_Type().notEquals(" ")))                     //Natural: IF SCIA3540.AP-MULT-APP-LOB NE ' ' AND SCIA3540.AP-MULT-APP-LOB-TYPE NE ' '
            {
                if (condition(pdaScia3540.getScia3540_Ap_Lob().equals("D")))                                                                                              //Natural: IF SCIA3540.AP-LOB EQ 'D'
                {
                    pdaScia9004.getScia9004_Aat_Soc_Sec().setValue(pdaScia3530.getScia3530_Aat_Soc_Sec());                                                                //Natural: ASSIGN SCIA9004.AAT-SOC-SEC := SCIA3530.AAT-SOC-SEC
                    pdaScia9004.getScia9004_Aat_Lob().setValue(pdaScia3540.getScia3540_Ap_Mult_App_Lob());                                                                //Natural: ASSIGN SCIA9004.AAT-LOB := SCIA3540.AP-MULT-APP-LOB
                    pdaScia9004.getScia9004_Aat_Lob_Type().setValue(pdaScia3540.getScia3540_Ap_Mult_App_Lob_Type());                                                      //Natural: ASSIGN SCIA9004.AAT-LOB-TYPE := SCIA3540.AP-MULT-APP-LOB-TYPE
                    pdaCdaobj.getCdaobj_Pnd_Function().setValue("GET");                                                                                                   //Natural: MOVE 'GET' TO CDAOBJ.#FUNCTION
                                                                                                                                                                          //Natural: PERFORM CALL-ACIN9004
                    sub_Call_Acin9004();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().equals("FOUND")))                                                                             //Natural: IF ##MSG EQ 'FOUND'
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                    //*  IISG
                    //*  IISG
                    //*  IISG
                    //*  IISG
                    pnd_Tmp_Alloc.getValue("*").reset();                                                                                                                  //Natural: RESET #TMP-ALLOC ( * ) #TMP-FUND-CDE ( * )
                    pnd_Tmp_Fund_Cde.getValue("*").reset();
                    pnd_Tmp_Alloc.getValue("*").setValue(pdaScia9004.getScia9004_Aat_Allocation_Pct().getValue("*"));                                                     //Natural: ASSIGN #TMP-ALLOC ( * ) := SCIA9004.AAT-ALLOCATION-PCT ( * )
                    pnd_Tmp_Fund_Cde.getValue("*").setValue(pdaScia9004.getScia9004_Aat_Fund_Cde().getValue("*"));                                                        //Natural: ASSIGN #TMP-FUND-CDE ( * ) := SCIA9004.AAT-FUND-CDE ( * )
                    pnd_Tmp_Fund_Source_Cde_1.setValue(pdaScia9004.getScia9004_Aat_Fund_Source_Cde_1());                                                                  //Natural: ASSIGN #TMP-FUND-SOURCE-CDE-1 := SCIA9004.AAT-FUND-SOURCE-CDE-1
                    pnd_Tmp_Fund_Source_Cde_2.setValue(pdaScia9004.getScia9004_Aat_Fund_Source_Cde_2());                                                                  //Natural: ASSIGN #TMP-FUND-SOURCE-CDE-2 := SCIA9004.AAT-FUND-SOURCE-CDE-2
                    pnd_Tmp_Alloc_2.getValue("*").setValue(pdaScia9004.getScia9004_Aat_Allocation_Pct_2().getValue("*"));                                                 //Natural: ASSIGN #TMP-ALLOC-2 ( * ) := SCIA9004.AAT-ALLOCATION-PCT-2 ( * )
                    pnd_Tmp_Fund_Cde_2.getValue("*").setValue(pdaScia9004.getScia9004_Aat_Fund_Cde_2().getValue("*"));                                                    //Natural: ASSIGN #TMP-FUND-CDE-2 ( * ) := SCIA9004.AAT-FUND-CDE-2 ( * )
                    //*  IISG
                    //*  IISG
                    //*  IISG
                    //*  IISG
                    pdaScia9004.getScia9004().setValuesByName(pdaScia3530.getScia3530());                                                                                 //Natural: MOVE BY NAME SCIA3530 TO SCIA9004
                    pdaScia9004.getScia9004_Aat_Allocation_Pct().getValue("*").setValue(pnd_Tmp_Alloc.getValue("*"));                                                     //Natural: ASSIGN SCIA9004.AAT-ALLOCATION-PCT ( * ) := #TMP-ALLOC ( * )
                    pdaScia9004.getScia9004_Aat_Fund_Cde().getValue("*").setValue(pnd_Tmp_Fund_Cde.getValue("*"));                                                        //Natural: ASSIGN SCIA9004.AAT-FUND-CDE ( * ) := #TMP-FUND-CDE ( * )
                    pdaScia9004.getScia9004_Aat_Fund_Source_Cde_1().setValue(pnd_Tmp_Fund_Source_Cde_1);                                                                  //Natural: ASSIGN SCIA9004.AAT-FUND-SOURCE-CDE-1 := #TMP-FUND-SOURCE-CDE-1
                    pdaScia9004.getScia9004_Aat_Fund_Source_Cde_2().setValue(pnd_Tmp_Fund_Source_Cde_2);                                                                  //Natural: ASSIGN SCIA9004.AAT-FUND-SOURCE-CDE-2 := #TMP-FUND-SOURCE-CDE-2
                    pdaScia9004.getScia9004_Aat_Allocation_Pct_2().getValue("*").setValue(pnd_Tmp_Alloc_2.getValue("*"));                                                 //Natural: ASSIGN SCIA9004.AAT-ALLOCATION-PCT-2 ( * ) := #TMP-ALLOC-2 ( * )
                    pdaScia9004.getScia9004_Aat_Fund_Cde_2().getValue("*").setValue(pnd_Tmp_Fund_Cde_2.getValue("*"));                                                    //Natural: ASSIGN SCIA9004.AAT-FUND-CDE-2 ( * ) := #TMP-FUND-CDE-2 ( * )
                    pdaScia9004.getScia9004_Aat_Mult_App_Status().reset();                                                                                                //Natural: RESET SCIA9004.AAT-MULT-APP-STATUS SCIA9004.AAT-MULT-APP-LOB SCIA9004.AAT-MULT-APP-LOB-TYPE SCIA9004.AAT-MULT-APP-PPG
                    pdaScia9004.getScia9004_Aat_Mult_App_Lob().reset();
                    pdaScia9004.getScia9004_Aat_Mult_App_Lob_Type().reset();
                    pdaScia9004.getScia9004_Aat_Mult_App_Ppg().reset();
                    pnd_Temp_Doi.setValue(pdaScia3530.getScia3530_Aat_Dt_App_Recvd());                                                                                    //Natural: ASSIGN #TEMP-DOI := SCIA3530.AAT-DT-APP-RECVD
                    pnd_Temp_Doi1_Pnd_Temp_T_Doi1_Mm.setValue(pnd_Temp_Doi_Pnd_Temp_T_Doi_Mm);                                                                            //Natural: ASSIGN #TEMP-T-DOI1-MM := #TEMP-T-DOI-MM
                    pnd_Temp_Doi1_Pnd_Temp_T_Doi1_Dd.setValue(pnd_Temp_Doi_Pnd_Temp_T_Doi_Dd);                                                                            //Natural: ASSIGN #TEMP-T-DOI1-DD := #TEMP-T-DOI-DD
                    pnd_Temp_Doi1_Pnd_Temp_T_Doi1_Yy.setValue(pnd_Temp_Doi_Pnd_Temp_T_Doi_Yy);                                                                            //Natural: ASSIGN #TEMP-T-DOI1-YY := #TEMP-T-DOI-YY
                    pdaScia9004.getScia9004_Aat_T_Doi().setValue(pnd_Temp_Doi1);                                                                                          //Natural: ASSIGN SCIA9004.AAT-T-DOI := #TEMP-DOI1
                    pdaScia9004.getScia9004_Aat_C_Doi().setValue(pnd_Temp_Doi1);                                                                                          //Natural: ASSIGN SCIA9004.AAT-C-DOI := #TEMP-DOI1
                    pdaScia9004.getScia9004_Aat_Lob().setValue(pdaScia3540.getScia3540_Ap_Mult_App_Lob());                                                                //Natural: ASSIGN SCIA9004.AAT-LOB := SCIA3540.AP-MULT-APP-LOB
                    pdaScia9004.getScia9004_Aat_Lob_Type().setValue(pdaScia3540.getScia3540_Ap_Mult_App_Lob_Type());                                                      //Natural: ASSIGN SCIA9004.AAT-LOB-TYPE := SCIA3540.AP-MULT-APP-LOB-TYPE
                    pdaCdaobj.getCdaobj_Pnd_Function().setValue("UPDATE");                                                                                                //Natural: MOVE 'UPDATE' TO CDAOBJ.#FUNCTION
                                                                                                                                                                          //Natural: PERFORM CALL-ACIN9004
                    sub_Call_Acin9004();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia3540.getScia3540_Ap_Applcnt_Req_Type().setValue(pdaScia9000.getScia9000_Pnd_Enrollment_Type());                                                        //Natural: MOVE SCIA9000.#ENROLLMENT-TYPE TO SCIA3540.AP-APPLCNT-REQ-TYPE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Premium_Team_Cde().equals(" ")))                                                                                //Natural: IF SCIA3500.###PREMIUM-TEAM-CDE = ' '
        {
            pdaScia3540.getScia3540_Ap_Mit_Unit().getValue(1).setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Existing_Rqst_Log_Unit());                                     //Natural: ASSIGN SCIA3540.AP-MIT-UNIT ( 1 ) := SCIA3500.###EXISTING-RQST-LOG-UNIT
            pdaScia3540.getScia3540_Ap_Ppg_Team_Cde().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Existing_Rqst_Log_Unit());                                             //Natural: ASSIGN SCIA3540.AP-PPG-TEAM-CDE := SCIA3500.###EXISTING-RQST-LOG-UNIT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia3540.getScia3540_Ap_Ppg_Team_Cde().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Premium_Team_Cde());                                                   //Natural: ASSIGN SCIA3540.AP-PPG-TEAM-CDE := SCIA3500.###PREMIUM-TEAM-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaScia3540.getScia3540_Ap_Ppg_Team_Cde().equals(" ")))                                                                                             //Natural: IF SCIA3540.AP-PPG-TEAM-CDE = ' '
        {
            pdaScia3540.getScia3540_Ap_Ppg_Team_Cde().setValue(pdaScia3530.getScia3530_Aat_Ppg_Team_Cde());                                                               //Natural: ASSIGN SCIA3540.AP-PPG-TEAM-CDE := SCIA3530.AAT-PPG-TEAM-CDE
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3540.getScia3540_Ap_Email_Address().setValue(pdaScia3530.getScia3530_Aat_Email_Address());                                                                 //Natural: ASSIGN SCIA3540.AP-EMAIL-ADDRESS := SCIA3530.AAT-EMAIL-ADDRESS
        pdaScia3540.getScia3540_Ap_Allocation_Model_Type().setValue(pdaScia3530.getScia3530_Aat_Allocation_Model_Type());                                                 //Natural: ASSIGN SCIA3540.AP-ALLOCATION-MODEL-TYPE := SCIA3530.AAT-ALLOCATION-MODEL-TYPE
        pdaScia3540.getScia3540_Ap_Divorce_Ind().setValue(pdaScia3530.getScia3530_Aat_Divorce_Ind());                                                                     //Natural: ASSIGN SCIA3540.AP-DIVORCE-IND := SCIA3530.AAT-DIVORCE-IND
        pdaScia3540.getScia3540_Ap_E_Signed_Appl_Ind().setValue(pdaScia3530.getScia3530_Aat_E_Signed_Appl_Ind());                                                         //Natural: ASSIGN SCIA3540.AP-E-SIGNED-APPL-IND := SCIA3530.AAT-E-SIGNED-APPL-IND
        pdaScia3540.getScia3540_Ap_Eft_Requested_Ind().setValue(pdaScia3530.getScia3530_Aat_Eft_Request_Ind());                                                           //Natural: ASSIGN SCIA3540.AP-EFT-REQUESTED-IND := SCIA3530.AAT-EFT-REQUEST-IND
        pdaScia3540.getScia3540_Ap_Phone_No().setValue(pdaScia3530.getScia3530_Aat_Phone_No());                                                                           //Natural: ASSIGN SCIA3540.AP-PHONE-NO := SCIA3530.AAT-PHONE-NO
        pdaScia3540.getScia3540_Ap_Incmpl_Acct_Ind().setValue(pdaScia3530.getScia3530_Aat_Incmpl_Acct_Ind());                                                             //Natural: ASSIGN SCIA3540.AP-INCMPL-ACCT-IND := SCIA3530.AAT-INCMPL-ACCT-IND
        pdaScia3540.getScia3540_Ap_Autosave_Ind().setValue(pdaScia3530.getScia3530_Aat_Autosave_Ind());                                                                   //Natural: ASSIGN SCIA3540.AP-AUTOSAVE-IND := SCIA3530.AAT-AUTOSAVE-IND
        pdaScia3540.getScia3540_Ap_As_Cur_Dflt_Opt().setValue(pdaScia3530.getScia3530_Aat_As_Cur_Dflt_Opt());                                                             //Natural: ASSIGN SCIA3540.AP-AS-CUR-DFLT-OPT := SCIA3530.AAT-AS-CUR-DFLT-OPT
        pdaScia3540.getScia3540_Ap_As_Cur_Dflt_Amt().setValue(pdaScia3530.getScia3530_Aat_As_Cur_Dflt_Amt());                                                             //Natural: ASSIGN SCIA3540.AP-AS-CUR-DFLT-AMT := SCIA3530.AAT-AS-CUR-DFLT-AMT
        pdaScia3540.getScia3540_Ap_As_Incr_Opt().setValue(pdaScia3530.getScia3530_Aat_As_Incr_Opt());                                                                     //Natural: ASSIGN SCIA3540.AP-AS-INCR-OPT := SCIA3530.AAT-AS-INCR-OPT
        pdaScia3540.getScia3540_Ap_As_Incr_Amt().setValue(pdaScia3530.getScia3530_Aat_As_Incr_Amt());                                                                     //Natural: ASSIGN SCIA3540.AP-AS-INCR-AMT := SCIA3530.AAT-AS-INCR-AMT
        pdaScia3540.getScia3540_Ap_As_Max_Pct().setValue(pdaScia3530.getScia3530_Aat_As_Max_Pct());                                                                       //Natural: ASSIGN SCIA3540.AP-AS-MAX-PCT := SCIA3530.AAT-AS-MAX-PCT
        pdaScia3540.getScia3540_Ap_Ae_Opt_Out_Days().setValue(pdaScia3530.getScia3530_Aat_Ae_Opt_Out_Days());                                                             //Natural: ASSIGN SCIA3540.AP-AE-OPT-OUT-DAYS := SCIA3530.AAT-AE-OPT-OUT-DAYS
        pdaScia3540.getScia3540_Ap_Orchestration_Id().setValue(pdaScia3530.getScia3530_Aat_Orchestration_Id());                                                           //Natural: ASSIGN SCIA3540.AP-ORCHESTRATION-ID := SCIA3530.AAT-ORCHESTRATION-ID
        pdaScia3540.getScia3540_Ap_Mail_Addr_Country_Cd().setValue(pdaScia3530.getScia3530_Aat_Mail_Addr_Country_Cd());                                                   //Natural: ASSIGN SCIA3540.AP-MAIL-ADDR-COUNTRY-CD := SCIA3530.AAT-MAIL-ADDR-COUNTRY-CD
        pdaScia3540.getScia3540_Ap_Legal_Ann_Option().setValue(pdaScia3530.getScia3530_Aat_Legal_Ann_Option());                                                           //Natural: ASSIGN SCIA3540.AP-LEGAL-ANN-OPTION := SCIA3530.AAT-LEGAL-ANN-OPTION
        pdaScia3540.getScia3540_Ap_Substitution_Contract_Ind().setValue(pdaScia3530.getScia3530_Aat_Substitution_Contract_Ind());                                         //Natural: ASSIGN SCIA3540.AP-SUBSTITUTION-CONTRACT-IND := SCIA3530.AAT-SUBSTITUTION-CONTRACT-IND
        pdaScia3540.getScia3540_Ap_Conv_Issue_State().setValue(pdaScia3530.getScia3530_Aat_Conv_Issue_State());                                                           //Natural: ASSIGN SCIA3540.AP-CONV-ISSUE-STATE := SCIA3530.AAT-CONV-ISSUE-STATE
        pdaScia3540.getScia3540_Ap_Deceased_Ind().setValue(pdaScia3530.getScia3530_Aat_Deceased_Ind());                                                                   //Natural: ASSIGN SCIA3540.AP-DECEASED-IND := SCIA3530.AAT-DECEASED-IND
        pdaScia3540.getScia3540_Ap_Non_Proprietary_Pkg_Ind().setValue(pdaScia3530.getScia3530_Aat_Non_Proprietary_Pkg_Ind());                                             //Natural: ASSIGN SCIA3540.AP-NON-PROPRIETARY-PKG-IND := SCIA3530.AAT-NON-PROPRIETARY-PKG-IND
        pdaScia3540.getScia3540_Ap_Decedent_Contract().setValue(pdaScia3530.getScia3530_Aat_Decedent_Contract());                                                         //Natural: ASSIGN SCIA3540.AP-DECEDENT-CONTRACT := SCIA3530.AAT-DECEDENT-CONTRACT
        pdaScia3540.getScia3540_Ap_Relation_To_Decedent().setValue(pdaScia3530.getScia3530_Aat_Relation_To_Decedent());                                                   //Natural: ASSIGN SCIA3540.AP-RELATION-TO-DECEDENT := SCIA3530.AAT-RELATION-TO-DECEDENT
        pdaScia3540.getScia3540_Ap_Ssn_Tin_Ind().setValue(pdaScia3530.getScia3530_Aat_Ssn_Tin_Ind());                                                                     //Natural: ASSIGN SCIA3540.AP-SSN-TIN-IND := SCIA3530.AAT-SSN-TIN-IND
        pdaScia3540.getScia3540_Ap_Oneira_Acct_No().setValue(pdaScia9000.getScia9000_Pnd_Oneira_Acct_No());                                                               //Natural: ASSIGN SCIA3540.AP-ONEIRA-ACCT-NO := SCIA9000.#ONEIRA-ACCT-NO
        short decideConditionsMet6272 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF SCIA3500.###PROCESS-IND;//Natural: VALUE 'P'
        if (condition((pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind().equals("P"))))
        {
            decideConditionsMet6272++;
            pnd_Hold_Ap_Status.setValue("C");                                                                                                                             //Natural: MOVE 'C' TO #HOLD-AP-STATUS
            if (condition(pnd_Hold_Premium_Team_Cde.equals(" ")))                                                                                                         //Natural: IF #HOLD-PREMIUM-TEAM-CDE = ' '
            {
                pnd_Hold_Ap_Process.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Region(), pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Need(),  //Natural: COMPRESS SCIA3500.###REGION SCIA3500.###NEED '01' INTO #HOLD-AP-PROCESS LEAVING NO SPACE
                    "01"));
                pdaScia3560.getScia3560_Pnd_Each_Bucket_Name().getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Region(),  //Natural: COMPRESS SCIA3500.###REGION SCIA3500.###NEED '-PREMAPP/MANU' INTO #EACH-BUCKET-NAME ( 1 ) LEAVING NO SPACE
                    pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Need(), "-PREMAPP/MANU"));
                pdaScia3560.getScia3560_Pnd_P_Bucket_Code().setValue("BU");                                                                                               //Natural: MOVE 'BU' TO #P-BUCKET-CODE
                pdaScia3560.getScia3560_Pnd_P_Accum_Code().setValue("AC");                                                                                                //Natural: MOVE 'AC' TO #P-ACCUM-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Hold_Ap_Process.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Hold_Premium_Team_Cde, "01"));                                           //Natural: COMPRESS #HOLD-PREMIUM-TEAM-CDE '01' INTO #HOLD-AP-PROCESS LEAVING NO SPACE
                pdaScia3560.getScia3560_Pnd_Each_Bucket_Name().getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Hold_Premium_Team_Cde,            //Natural: COMPRESS #HOLD-PREMIUM-TEAM-CDE '-PREMAPP/MANU' INTO #EACH-BUCKET-NAME ( 1 ) LEAVING NO SPACE
                    "-PREMAPP/MANU"));
                pdaScia3560.getScia3560_Pnd_P_Bucket_Code().setValue(pnd_Hold_Bucket_Cde);                                                                                //Natural: MOVE #HOLD-BUCKET-CDE TO #P-BUCKET-CODE
                pdaScia3560.getScia3560_Pnd_P_Accum_Code().setValue(pnd_Hold_Accum_Cde);                                                                                  //Natural: MOVE #HOLD-ACCUM-CDE TO #P-ACCUM-CODE
            }                                                                                                                                                             //Natural: END-IF
            //*  COMPANION
            pdaScia3540.getScia3540_Ap_Dt_Matched().setValue(pnd_System_Date);                                                                                            //Natural: MOVE #SYSTEM-DATE TO SCIA3540.AP-DT-MATCHED
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind().equals("C"))))
        {
            decideConditionsMet6272++;
            //*  COMPANION PROCESS
            pnd_Hold_Ap_Status.setValue("D");                                                                                                                             //Natural: MOVE 'D' TO #HOLD-AP-STATUS
            if (condition(pnd_Hold_Premium_Team_Cde.equals(" ")))                                                                                                         //Natural: IF #HOLD-PREMIUM-TEAM-CDE = ' '
            {
                pnd_Hold_Ap_Process.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Region(), pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Need(),  //Natural: COMPRESS SCIA3500.###REGION SCIA3500.###NEED '06' INTO #HOLD-AP-PROCESS LEAVING NO SPACE
                    "06"));
                pdaScia3560.getScia3560_Pnd_Each_Bucket_Name().getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Region(),  //Natural: COMPRESS SCIA3500.###REGION SCIA3500.###NEED '-COMPANION/C' INTO #EACH-BUCKET-NAME ( 1 ) LEAVING NO SPACE
                    pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Need(), "-COMPANION/C"));
                pdaScia3560.getScia3560_Pnd_P_Bucket_Code().setValue("BU");                                                                                               //Natural: MOVE 'BU' TO #P-BUCKET-CODE
                pdaScia3560.getScia3560_Pnd_P_Accum_Code().setValue("AC");                                                                                                //Natural: MOVE 'AC' TO #P-ACCUM-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Hold_Ap_Process.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Hold_Premium_Team_Cde, "06"));                                           //Natural: COMPRESS #HOLD-PREMIUM-TEAM-CDE '06' INTO #HOLD-AP-PROCESS LEAVING NO SPACE
                pdaScia3560.getScia3560_Pnd_Each_Bucket_Name().getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Hold_Premium_Team_Cde,            //Natural: COMPRESS #HOLD-PREMIUM-TEAM-CDE '-COMPANION/C' INTO #EACH-BUCKET-NAME ( 1 ) LEAVING NO SPACE
                    "-COMPANION/C"));
                pdaScia3560.getScia3560_Pnd_P_Bucket_Code().setValue(pnd_Hold_Bucket_Cde);                                                                                //Natural: MOVE #HOLD-BUCKET-CDE TO #P-BUCKET-CODE
                pdaScia3560.getScia3560_Pnd_P_Accum_Code().setValue(pnd_Hold_Accum_Cde);                                                                                  //Natural: MOVE #HOLD-ACCUM-CDE TO #P-ACCUM-CODE
            }                                                                                                                                                             //Natural: END-IF
            //*  COMPANION
            pdaScia3540.getScia3540_Ap_C_Doi().reset();                                                                                                                   //Natural: RESET SCIA3540.AP-C-DOI
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind().equals("T"))))
        {
            decideConditionsMet6272++;
            //*  COMPANION PROCESS
            pnd_Hold_Ap_Status.setValue("D");                                                                                                                             //Natural: MOVE 'D' TO #HOLD-AP-STATUS
            if (condition(pnd_Hold_Premium_Team_Cde.equals(" ")))                                                                                                         //Natural: IF #HOLD-PREMIUM-TEAM-CDE = ' '
            {
                pnd_Hold_Ap_Process.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Region(), pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Need(),  //Natural: COMPRESS SCIA3500.###REGION SCIA3500.###NEED '02' INTO #HOLD-AP-PROCESS LEAVING NO SPACE
                    "02"));
                pdaScia3560.getScia3560_Pnd_Each_Bucket_Name().getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Region(),  //Natural: COMPRESS SCIA3500.###REGION SCIA3500.###NEED '-COMPANION/T' INTO #EACH-BUCKET-NAME ( 1 ) LEAVING NO SPACE
                    pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Need(), "-COMPANION/T"));
                pdaScia3560.getScia3560_Pnd_P_Bucket_Code().setValue("BU");                                                                                               //Natural: MOVE 'BU' TO #P-BUCKET-CODE
                pdaScia3560.getScia3560_Pnd_P_Accum_Code().setValue("AC");                                                                                                //Natural: MOVE 'AC' TO #P-ACCUM-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Hold_Ap_Process.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Hold_Premium_Team_Cde, "02"));                                           //Natural: COMPRESS #HOLD-PREMIUM-TEAM-CDE '02' INTO #HOLD-AP-PROCESS LEAVING NO SPACE
                pdaScia3560.getScia3560_Pnd_Each_Bucket_Name().getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Hold_Premium_Team_Cde,            //Natural: COMPRESS #HOLD-PREMIUM-TEAM-CDE '-COMPANION/T' INTO #EACH-BUCKET-NAME ( 1 ) LEAVING NO SPACE
                    "-COMPANION/T"));
                pdaScia3560.getScia3560_Pnd_P_Bucket_Code().setValue(pnd_Hold_Bucket_Cde);                                                                                //Natural: MOVE #HOLD-BUCKET-CDE TO #P-BUCKET-CODE
                pdaScia3560.getScia3560_Pnd_P_Accum_Code().setValue(pnd_Hold_Accum_Cde);                                                                                  //Natural: MOVE #HOLD-ACCUM-CDE TO #P-ACCUM-CODE
            }                                                                                                                                                             //Natural: END-IF
            pdaScia3540.getScia3540_Ap_T_Doi().reset();                                                                                                                   //Natural: RESET SCIA3540.AP-T-DOI
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Contract_Type().equals("RL") || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Contract_Type().equals("IIRA")              //Natural: IF SCIA3500.###CONTRACT-TYPE = 'RL' OR = 'IIRA' OR = 'IRA' OR = 'IRACLAS C' OR = 'IRACLAS R' OR = 'IRAROTH C' OR = 'IRAROTH R' OR = 'KEOGH' OR = 'KEOGH DV' OR = 'IRA SEP' OR = 'RHSP' OR = 'RHSP CO' OR = 'RHSP DV' OR = 'IRACIDX' OR = 'IRARIDX' OR = 'IRASIDX' OR SCIA3500.###PROCESS-IND = 'C' OR = 'T' OR SCIA3500.###LOAN = 'Y' OR SCIA9000.#DECEDENT-CONTRACT GT ' '
            || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Contract_Type().equals("IRA") || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Contract_Type().equals("IRACLAS C") 
            || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Contract_Type().equals("IRACLAS R") || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Contract_Type().equals("IRAROTH C") 
            || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Contract_Type().equals("IRAROTH R") || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Contract_Type().equals("KEOGH") 
            || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Contract_Type().equals("KEOGH DV") || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Contract_Type().equals("IRA SEP") 
            || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Contract_Type().equals("RHSP") || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Contract_Type().equals("RHSP CO") 
            || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Contract_Type().equals("RHSP DV") || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Contract_Type().equals("IRACIDX") 
            || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Contract_Type().equals("IRARIDX") || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Contract_Type().equals("IRASIDX") 
            || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind().equals("C") || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind().equals("T") || pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Loan().equals("Y") 
            || pdaScia9000.getScia9000_Pnd_Decedent_Contract().greater(" ")))
        {
            pnd_Hold_Ap_Status.setValue("C");                                                                                                                             //Natural: MOVE 'C' TO #HOLD-AP-STATUS
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3540.getScia3540_Ap_Status().setValue(pnd_Hold_Ap_Status);                                                                                                 //Natural: ASSIGN SCIA3540.AP-STATUS := #HOLD-AP-STATUS
        pdaScia3540.getScia3540_Ap_Process_Status().setValue(pnd_Hold_Ap_Process);                                                                                        //Natural: ASSIGN SCIA3540.AP-PROCESS-STATUS := #HOLD-AP-PROCESS
        if (condition(pnd_Hold_Ap_Status.equals("C")))                                                                                                                    //Natural: IF #HOLD-AP-STATUS = 'C'
        {
            pdaScia3540.getScia3540_Ap_Dt_Matched().setValue(pnd_System_Date);                                                                                            //Natural: ASSIGN SCIA3540.AP-DT-MATCHED := #SYSTEM-DATE
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3540.getScia3540_Id_Structure().setValuesByName(pdaScia3540.getScia3540());                                                                                //Natural: MOVE BY NAME SCIA3540 TO SCIA3540-ID.STRUCTURE
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION = 'STORE'
                                                                                                                                                                          //Natural: PERFORM CALL-OBJECT
        sub_Call_Object();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Display_Flag.equals(" ")))                                                                                                                      //Natural: IF #DISPLAY-FLAG = ' '
        {
            pdaCdaobj.getCdaobj_Pnd_Function().setValue("DELETE");                                                                                                        //Natural: MOVE 'DELETE' TO CDAOBJ.#FUNCTION
            if (condition(pdaScia1203.getScia1203_Pnd_Func_Accptd_Applctn().equals("N")))                                                                                 //Natural: IF SCIA1203.#FUNC-ACCPTD-APPLCTN = 'N'
            {
                pdaAcipda_M.getMsg_Info_Sub().reset();                                                                                                                    //Natural: RESET MSG-INFO-SUB
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Action.setValue("D");                                                                                                                                 //Natural: ASSIGN #ACTION := 'D'
                //*  OIA BE
                DbsUtil.callnat(Scin1140.class , getCurrentProcessState(), pdaScia3530.getScia3530_Aat_Soc_Sec(), pdaScia3530.getScia3530_Aat_Pin_Nbr(),                  //Natural: CALLNAT 'SCIN1140' SCIA3530.AAT-SOC-SEC SCIA3530.AAT-PIN-NBR SCIA3530.AAT-COLL-CODE SCIA3530.AAT-COR-FIRST-NME SCIA3530.AAT-COR-LAST-NME SCIA3530.AAT-COR-MDDLE-NME SCIA3530.AAT-COR-SFFX-NME SCIA3530.AAT-COR-PRFX-NME SCIA3530.AAT-DT-APP-RECVD SCIA3530.AAT-DOB SCIA3530.AAT-T-DOI SCIA3530.AAT-C-DOI #ACTION SCIA3530.AAT-LOB SCIA3530.AAT-LOB-TYPE #ISN-1 SCIA3530.AAT-ALLOCATION-FMT
                    pdaScia3530.getScia3530_Aat_Coll_Code(), pdaScia3530.getScia3530_Aat_Cor_First_Nme(), pdaScia3530.getScia3530_Aat_Cor_Last_Nme(), pdaScia3530.getScia3530_Aat_Cor_Mddle_Nme(), 
                    pdaScia3530.getScia3530_Aat_Cor_Sffx_Nme(), pdaScia3530.getScia3530_Aat_Cor_Prfx_Nme(), pdaScia3530.getScia3530_Aat_Dt_App_Recvd(), 
                    pdaScia3530.getScia3530_Aat_Dob(), pdaScia3530.getScia3530_Aat_T_Doi(), pdaScia3530.getScia3530_Aat_C_Doi(), pnd_Action, pdaScia3530.getScia3530_Aat_Lob(), 
                    pdaScia3530.getScia3530_Aat_Lob_Type(), pnd_Isn_1, pdaScia3530.getScia3530_Aat_Allocation_Fmt());
                if (condition(Global.isEscape())) return;
                if (condition(pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().notEquals(" ")))                                                                          //Natural: IF MSG-INFO-SUB.##ERROR-FIELD NE ' '
                {
                    getReports().write(0, pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                     //Natural: WRITE MSG-INFO-SUB.##MSG
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Plan.setValue(pdaScia3530.getScia3530_Aat_Sgrd_Plan_No());                                                                                                    //Natural: ASSIGN #PLAN := SCIA3530.AAT-SGRD-PLAN-NO
        DbsUtil.callnat(Scin8540.class , getCurrentProcessState(), pnd_Plan, pnd_Non_T_C_Administered_Plan);                                                              //Natural: CALLNAT 'SCIN8540' #PLAN #NON-T-C-ADMINISTERED-PLAN
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Non_T_C_Administered_Plan.getBoolean() && !pnd_Plan.getSubstring(1,1).equals("M")))                                                             //Natural: IF #NON-T-C-ADMINISTERED-PLAN AND SUBSTR ( #PLAN,1,1 ) NE 'M'
        {
            pnd_Ssn_Mayo_A_Pnd_Ssn_Mayo_N.setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Ssn());                                                                            //Natural: ASSIGN #SSN-MAYO-N := SCIA3500.###SSN
            pnd_Fidelity_Contract_Found.reset();                                                                                                                          //Natural: RESET #FIDELITY-CONTRACT-FOUND
            pnd_Pnum_Ssn_Pnd_Plan_Num.setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Srce_Cd());                                                                            //Natural: ASSIGN #PLAN-NUM := ###SRCE-CD
            pnd_Pnum_Ssn_Pnd_Ph_Ssn.setValue(pnd_Ssn_Mayo_A);                                                                                                             //Natural: ASSIGN #PH-SSN := #SSN-MAYO-A
            vw_fidelity_Ppg.startDatabaseFind                                                                                                                             //Natural: FIND FIDELITY-PPG WITH PPG EQUAL #PLAN-NUM
            (
            "FIND01",
            new Wc[] { new Wc("PPG", "=", pnd_Pnum_Ssn_Pnd_Plan_Num, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_fidelity_Ppg.readNextRow("FIND01", true)))
            {
                vw_fidelity_Ppg.setIfNotFoundControlFlag(false);
                if (condition(vw_fidelity_Ppg.getAstCOUNTER().equals(0)))                                                                                                 //Natural: IF NO RECORDS FOUND
                {
                    getReports().write(0, "FIDELITY PPG NOT FOUND FOR",pnd_Pnum_Ssn_Pnd_Plan_Num);                                                                        //Natural: WRITE 'FIDELITY PPG NOT FOUND FOR' #PLAN-NUM
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-NOREC
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
            vw_fidelity_Contract.startDatabaseRead                                                                                                                        //Natural: READ ( 1 ) FIDELITY-CONTRACT BY PNUM-SSN STARTING FROM #PNUM-SSN
            (
            "READ03",
            new Wc[] { new Wc("PNUM_SSN", ">=", pnd_Pnum_Ssn, WcType.BY) },
            new Oc[] { new Oc("PNUM_SSN", "ASC") },
            1
            );
            READ03:
            while (condition(vw_fidelity_Contract.readNextRow("READ03")))
            {
                if (condition(fidelity_Contract_Plan_Num.equals(pnd_Pnum_Ssn_Pnd_Plan_Num) && fidelity_Contract_Ph_Ssn.equals(pnd_Pnum_Ssn_Pnd_Ph_Ssn)))                  //Natural: IF FIDELITY-CONTRACT.PLAN-NUM EQ #PLAN-NUM AND FIDELITY-CONTRACT.PH-SSN EQ #PH-SSN
                {
                    pnd_Fidelity_Contract_Found.setValue(true);                                                                                                           //Natural: ASSIGN #FIDELITY-CONTRACT-FOUND := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            if (condition(! (pnd_Fidelity_Contract_Found.getBoolean())))                                                                                                  //Natural: IF NOT #FIDELITY-CONTRACT-FOUND
            {
                fidelity_Contract_Plan_Num.setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Srce_Cd());                                                                       //Natural: ASSIGN FIDELITY-CONTRACT.PLAN-NUM := ###SRCE-CD
                fidelity_Contract_Ph_Ssn.setValue(pnd_Ssn_Mayo_A);                                                                                                        //Natural: ASSIGN FIDELITY-CONTRACT.PH-SSN := #SSN-MAYO-A
                fidelity_Contract_Ph_Tiaa_Contract.setValue(pdaScia3530.getScia3530_Aat_Tiaa_Cntrct());                                                                   //Natural: ASSIGN FIDELITY-CONTRACT.PH-TIAA-CONTRACT := SCIA3530.AAT-TIAA-CNTRCT
                fidelity_Contract_Ph_Cref_Contract.setValue(pdaScia3530.getScia3530_Aat_Cref_Cert());                                                                     //Natural: ASSIGN FIDELITY-CONTRACT.PH-CREF-CONTRACT := SCIA3530.AAT-CREF-CERT
                fidelity_Contract_Issue_Date.setValue(Global.getDATN());                                                                                                  //Natural: ASSIGN FIDELITY-CONTRACT.ISSUE-DATE := *DATN
                fidelity_Contract_Sg_Plan_No.setValue(pdaScia3530.getScia3530_Aat_Sgrd_Plan_No());                                                                        //Natural: ASSIGN FIDELITY-CONTRACT.SG-PLAN-NO := SCIA3530.AAT-SGRD-PLAN-NO
                fidelity_Contract_Sg_Subplan_No.setValue(pdaScia3530.getScia3530_Aat_Sgrd_Subplan_No());                                                                  //Natural: ASSIGN FIDELITY-CONTRACT.SG-SUBPLAN-NO := SCIA3530.AAT-SGRD-SUBPLAN-NO
                fidelity_Contract_Sg_Div_Sub.setValue(pdaScia3530.getScia3530_Aat_Sgrd_Divsub());                                                                         //Natural: ASSIGN FIDELITY-CONTRACT.SG-DIV-SUB := SCIA3530.AAT-SGRD-DIVSUB
                vw_fidelity_Contract.insertDBRow();                                                                                                                       //Natural: STORE FIDELITY-CONTRACT
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().reset();                                                                                                        //Natural: RESET ##RETURN-CODE
        pnd_New_Contract_Pnd_New_Pref_Ed.setValue(hold_T_Cont_Hold_App_Pref);                                                                                             //Natural: MOVE HOLD-APP-PREF TO #NEW-PREF-ED
        pnd_New_Contract_Pnd_Dash.setValue("-");                                                                                                                          //Natural: ASSIGN #DASH = '-'
        pnd_New_Contract_Pnd_New_Cont_Ed.setValue(hold_T_Cont_Hold_App_Cont);                                                                                             //Natural: MOVE HOLD-APP-CONT TO #NEW-CONT-ED
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("GET");                                                                                                               //Natural: ASSIGN CDAOBJ.#FUNCTION = 'GET'
                                                                                                                                                                          //Natural: PERFORM CALL-OBJECT
        sub_Call_Object();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##ERROR-FIELD NE ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Non_T_C_Administered_Plan.getBoolean()))                                                                                                        //Natural: IF #NON-T-C-ADMINISTERED-PLAN
        {
            pdaScia8510.getScia8510_Pnd_Ap_Alloc_Ind().reset();                                                                                                           //Natural: RESET #AP-ALLOC-IND
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ap_Cor_Ind.equals(" ") && pdaScia8510.getScia8510_Pnd_Ap_Alloc_Ind().equals(" ") && pdaScia3540.getScia3540_Ap_Addr_Sync_Ind().equals("O")))    //Natural: IF #AP-COR-IND = ' ' AND #AP-ALLOC-IND = ' ' AND AP-ADDR-SYNC-IND EQ 'O'
        {
            pdaScia3540.getScia3540_Ap_Sync_Ind().reset();                                                                                                                //Natural: RESET AP-SYNC-IND
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3540.getScia3540_Ap_Cor_Ind().setValue(pnd_Ap_Cor_Ind);                                                                                                    //Natural: MOVE #AP-COR-IND TO SCIA3540.AP-COR-IND
        pdaScia3540.getScia3540_Ap_Alloc_Ind().setValue(pdaScia8510.getScia8510_Pnd_Ap_Alloc_Ind());                                                                      //Natural: MOVE #AP-ALLOC-IND TO SCIA3540.AP-ALLOC-IND
        if (condition(pnd_Prem_Mit_Log_Dte_Tme.notEquals(" ")))                                                                                                           //Natural: IF #PREM-MIT-LOG-DTE-TME NE ' '
        {
            pdaScia3540.getScia3540_Ap_Rqst_Log_Dte_Time().getValue(2).setValue(pnd_Prem_Mit_Log_Dte_Tme);                                                                //Natural: ASSIGN SCIA3540.AP-RQST-LOG-DTE-TIME ( 2 ) := #PREM-MIT-LOG-DTE-TME
        }                                                                                                                                                                 //Natural: END-IF
        //*  JR-SR
        //*  JR-SR
        if (condition(pdaScia3540.getScia3540_Ap_Coll_Code().equals("9004") || pdaScia3540.getScia3540_Ap_Coll_Code().equals("9006") || pdaScia3540.getScia3540_Ap_Coll_Code().equals("9007")  //Natural: IF SCIA3540.AP-COLL-CODE = '9004' OR = '9006' OR = '9007' OR = '9008' OR = '9010' OR = '9011'
            || pdaScia3540.getScia3540_Ap_Coll_Code().equals("9008") || pdaScia3540.getScia3540_Ap_Coll_Code().equals("9010") || pdaScia3540.getScia3540_Ap_Coll_Code().equals("9011")))
        {
            if (condition(Global.getINIT_USER().equals("P9998NIR") || Global.getINIT_USER().equals("T9998NIR")))                                                          //Natural: IF *INIT-USER = 'P9998NIR' OR = 'T9998NIR'
            {
                pdaScia3540.getScia3540_Ap_Current_State_Code().setValue(pdaScia1203.getScia1203_Pnd_Return_Msg_Fld1());                                                  //Natural: ASSIGN SCIA3540.AP-CURRENT-STATE-CODE := SCIA1203.#RETURN-MSG-FLD1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("UPDATE");                                                                                                            //Natural: ASSIGN CDAOBJ.#FUNCTION = 'UPDATE'
                                                                                                                                                                          //Natural: PERFORM CALL-OBJECT
        sub_Call_Object();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##ERROR-FIELD NE ' '
        {
            getReports().write(0, "ERROR 1");                                                                                                                             //Natural: WRITE 'ERROR 1'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaScia9000.getScia9000_Pnd_Orchestration_Id().greater(" ") && pnd_Use_Bpel_Data.equals("Y")))                                                      //Natural: IF SCIA9000.#ORCHESTRATION-ID GT ' ' AND #USE-BPEL-DATA = 'Y'
        {
            pdaScia1281.getPnd_Scia1281_Pnd_Ssn().setValue(pdaScia3530.getScia3530_Aat_Soc_Sec());                                                                        //Natural: ASSIGN #SCIA1281.#SSN := SCIA3530.AAT-SOC-SEC
            pdaScia1281.getPnd_Scia1281_Pnd_Orchestration_Id().setValue(pdaScia3530.getScia3530_Aat_Orchestration_Id());                                                  //Natural: ASSIGN #SCIA1281.#ORCHESTRATION-ID := SCIA3530.AAT-ORCHESTRATION-ID
            DbsUtil.callnat(Scin1281.class , getCurrentProcessState(), pdaScia1281.getPnd_Scia1281());                                                                    //Natural: CALLNAT 'SCIN1281' #SCIA1281
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind().equals("P")))                                                                                     //Natural: IF SCIA3500.###PROCESS-IND = 'P'
        {
            pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("CONTRACT PREM/APP FOR", pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Last_Name(),                 //Natural: COMPRESS 'CONTRACT PREM/APP FOR' SCIA3500.###LAST-NAME '- CONTRACT IS:' #CONTR-GOTTEN-T INTO MSG-INFO-SUB.##MSG
                "- CONTRACT IS:", pdaScia3560.getScia3560_Pnd_Contr_Gotten_T()));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("CONTRACT ISSUED FOR", pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Last_Name(),                   //Natural: COMPRESS 'CONTRACT ISSUED FOR' SCIA3500.###LAST-NAME '- CONTRACT IS:' #CONTR-GOTTEN-T INTO MSG-INFO-SUB.##MSG
                "- CONTRACT IS:", pdaScia3560.getScia3560_Pnd_Contr_Gotten_T()));
            if (condition(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Entered_Ppg().notEquals(" ") && pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Entered_Ppg().notEquals(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Srce_Cd()))) //Natural: IF SCIA3500.###ENTERED-PPG NE ' ' AND SCIA3500.###ENTERED-PPG NE ###SRCE-CD
            {
                pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress(pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg(), "UNDER", pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Srce_Cd())); //Natural: COMPRESS MSG-INFO-SUB.##MSG 'UNDER' SCIA3500.###SRCE-CD INTO MSG-INFO-SUB.##MSG
                getReports().write(0, pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                         //Natural: WRITE MSG-INFO-SUB.##MSG
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  NO OTHER ERROR EXCEPT MIT ERROR
        //*  MIT ERROR ENCOUNTERED
        if (condition(pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().equals(" ") && pnd_Tmp_Msg.greater(" ")))                                                                 //Natural: IF ##MSG = ' ' AND #TMP-MSG GT ' '
        {
            pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(pnd_Tmp_Msg);                                                                                              //Natural: ASSIGN ##MSG := #TMP-MSG
            pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(pnd_Tmp_Msg_Nr);                                                                                        //Natural: ASSIGN ##MSG-NR := #TMP-MSG-NR
            pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Error_Ind().setValue("X");                                                                                        //Natural: ASSIGN SCIA3500.###PROCESS-ERROR-IND := 'X'
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Tiaa_Rcvd_Dte().reset();                                                                                                      //Natural: RESET SCIA3500.###TIAA-RCVD-DTE SCIA3500.###PREFIX SCIA3500.###LAST-NAME SCIA3500.###FIRST-NAME SCIA3500.###MIDDLE-NAME SCIA3500.###SUFFIX SCIA3500.###DOB SCIA3500.###SSN SCIA3500.###PIN SCIA3500.###DA-INDICATOR SCIA3500.###PROCESS-IND SCIA3500.###MIT-LOG-DTE-TME SCIA3500.###T-DOI SCIA3500.###C-DOI SCIA3500.###MIT-LOG-DTE-TME-SUB SCIA3500.###MIT-UNIT SCIA3500.###MIT-WPID SCIA3500.###MULT-APP-STATUS SCIA3500.###MULT-APP-LOB SCIA3500.###MULT-APP-LOB-TYPE SCIA3500.###MULT-APP-PPG SCIA3500.###MULT-PREMIUM-TEAM-CDE SCIA3500.###MAIL-INSTRCT SCIA3500.###LOAN
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Prefix().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Last_Name().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_First_Name().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Middle_Name().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Suffix().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Dob().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Ssn().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Pin().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Da_Indicator().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Ind().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mit_Log_Dte_Tme().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_T_Doi().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_C_Doi().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mit_Log_Dte_Tme_Sub().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mit_Unit().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mit_Wpid().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mult_App_Status().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mult_App_Lob().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mult_App_Lob_Type().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mult_App_Ppg().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mult_Premium_Team_Cde().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mail_Instrct().reset();
        pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Loan().reset();
        if (condition(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Entered_Ppg().notEquals(" ")))                                                                                  //Natural: IF SCIA3500.###ENTERED-PPG NE ' '
        {
            pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Srce_Cd().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Entered_Ppg());                                                    //Natural: ASSIGN SCIA3500.###SRCE-CD := SCIA3500.###ENTERED-PPG
        }                                                                                                                                                                 //Natural: END-IF
        //* *************************** END SCIN3500 ****************************
        //*  RTN-6D-PROCESS-ACIN3500
    }
    private void sub_Rtn_6d_Post_Process_Acin3500() throws Exception                                                                                                      //Natural: RTN-6D-POST-PROCESS-ACIN3500
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        pdaScia9000.getScia9000_Pnd_Tiaa_Contract_No().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_T_Contract());                                                        //Natural: MOVE SCIA3500.###T-CONTRACT TO SCIA9000.#TIAA-CONTRACT-NO
        pdaScia9000.getScia9000_Pnd_Cref_Certificate_No().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_C_Contract());                                                     //Natural: MOVE SCIA3500.###C-CONTRACT TO SCIA9000.#CREF-CERTIFICATE-NO
        pdaScia9000.getScia9000_Pnd_Mit_Unit().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Existing_Rqst_Log_Unit());                                                    //Natural: MOVE SCIA3500.###EXISTING-RQST-LOG-UNIT TO SCIA9000.#MIT-UNIT
        pdaScia9000.getScia9000_Pnd_Mit_Wpid().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mit_Wpid());                                                                  //Natural: MOVE SCIA3500.###MIT-WPID TO SCIA9000.#MIT-WPID
        pdaScia9000.getScia9000_Pnd_Mit_Log_Date_Time_Parent().setValue(pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Mit_Log_Dte_Tme());                                           //Natural: MOVE SCIA3500.###MIT-LOG-DTE-TME TO SCIA9000.#MIT-LOG-DATE-TIME-PARENT
        pnd_A100_Pnd_N8.setValue(pdaScia3530.getScia3530_Aat_Annuity_Start_Date());                                                                                       //Natural: MOVE SCIA3530.AAT-ANNUITY-START-DATE TO #N8
        pdaScia9000.getScia9000_Pnd_Annuity_Start_Date().setValue(pnd_A100_Pnd_A8);                                                                                       //Natural: MOVE #A8 TO SCIA9000.#ANNUITY-START-DATE
        pnd_A100_Pnd_N4.setValue(pdaScia3530.getScia3530_Aat_T_1st_Pymt());                                                                                               //Natural: MOVE SCIA3530.AAT-T-1ST-PYMT TO #N4
        pdaScia9000.getScia9000_Pnd_Tiaa_Age_1st_Pymnt().setValue(pnd_A100_Pnd_A4);                                                                                       //Natural: MOVE #A4 TO SCIA9000.#TIAA-AGE-1ST-PYMNT
        pnd_A100_Pnd_N4.setValue(pdaScia3530.getScia3530_Aat_C_1st_Pymt());                                                                                               //Natural: MOVE SCIA3530.AAT-C-1ST-PYMT TO #N4
        pdaScia9000.getScia9000_Pnd_Cref_Age_1st_Pymnt().setValue(pnd_A100_Pnd_A4);                                                                                       //Natural: MOVE #A4 TO SCIA9000.#CREF-AGE-1ST-PYMNT
        if (condition(DbsUtil.maskMatches(pdaScia3530.getScia3530_Aat_T_Doi(),"MMDDYY")))                                                                                 //Natural: IF SCIA3530.AAT-T-DOI = MASK ( MMDDYY )
        {
            pnd_A100_Pnd_N6.setValue(pdaScia3530.getScia3530_Aat_T_Doi());                                                                                                //Natural: MOVE SCIA3530.AAT-T-DOI TO #N6
            pnd_D.setValueEdited(new ReportEditMask("MMDDYY"),pnd_A100_Pnd_A6);                                                                                           //Natural: MOVE EDITED #A6 TO #D ( EM = MMDDYY )
            pdaScia9000.getScia9000_Pnd_Tiaa_Date_Of_Issue().setValueEdited(pnd_D,new ReportEditMask("YYYYMMDD"));                                                        //Natural: MOVE EDITED #D ( EM = YYYYMMDD ) TO SCIA9000.#TIAA-DATE-OF-ISSUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(pdaScia3530.getScia3530_Aat_C_Doi(),"MMDDYY")))                                                                                 //Natural: IF SCIA3530.AAT-C-DOI = MASK ( MMDDYY )
        {
            pnd_A100_Pnd_N6.setValue(pdaScia3530.getScia3530_Aat_C_Doi());                                                                                                //Natural: MOVE SCIA3530.AAT-C-DOI TO #N6
            pnd_D.setValueEdited(new ReportEditMask("MMDDYY"),pnd_A100_Pnd_A6);                                                                                           //Natural: MOVE EDITED #A6 TO #D ( EM = MMDDYY )
            pdaScia9000.getScia9000_Pnd_Cref_Date_Of_Issue().setValueEdited(pnd_D,new ReportEditMask("YYYYMMDD"));                                                        //Natural: MOVE EDITED #D ( EM = YYYYMMDD ) TO SCIA9000.#CREF-DATE-OF-ISSUE
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia9000.getScia9000_Pnd_Sg_Register_Id().setValue(pdaScia3530.getScia3530_Aat_Sgrd_Register_Id());                                                            //Natural: MOVE SCIA3530.AAT-SGRD-REGISTER-ID TO SCIA9000.#SG-REGISTER-ID
        //* ********************* AB 05-04-2007 START *****************************
        pdaScia9000.getScia9000_Pnd_Sg_Insurer_1().setValue(pdaScia3530.getScia3530_Aat_Arr_Insurer_1());                                                                 //Natural: MOVE SCIA3530.AAT-ARR-INSURER-1 TO SCIA9000.#SG-INSURER-1
        pdaScia9000.getScia9000_Pnd_Sg_Insurer_2().setValue(pdaScia3530.getScia3530_Aat_Arr_Insurer_2());                                                                 //Natural: MOVE SCIA3530.AAT-ARR-INSURER-2 TO SCIA9000.#SG-INSURER-2
        pdaScia9000.getScia9000_Pnd_Sg_Insurer_3().setValue(pdaScia3530.getScia3530_Aat_Arr_Insurer_3());                                                                 //Natural: MOVE SCIA3530.AAT-ARR-INSURER-3 TO SCIA9000.#SG-INSURER-3
        pdaScia9000.getScia9000_Pnd_Sg_1035_Exch_Ind_1().setValue(pdaScia3530.getScia3530_Aat_Arr_1035_Exch_Ind_1());                                                     //Natural: MOVE SCIA3530.AAT-ARR-1035-EXCH-IND-1 TO SCIA9000.#SG-1035-EXCH-IND-1
        pdaScia9000.getScia9000_Pnd_Sg_1035_Exch_Ind_2().setValue(pdaScia3530.getScia3530_Aat_Arr_1035_Exch_Ind_2());                                                     //Natural: MOVE SCIA3530.AAT-ARR-1035-EXCH-IND-2 TO SCIA9000.#SG-1035-EXCH-IND-2
        pdaScia9000.getScia9000_Pnd_Sg_1035_Exch_Ind_3().setValue(pdaScia3530.getScia3530_Aat_Arr_1035_Exch_Ind_3());                                                     //Natural: MOVE SCIA3530.AAT-ARR-1035-EXCH-IND-3 TO SCIA9000.#SG-1035-EXCH-IND-3
        //* ********************* AB 05-04-2007 END *******************************
        //*  RTN-6D-POST-PROCESS-ACIN3500
    }
    private void sub_Rtn_9_Processing_Complete() throws Exception                                                                                                         //Natural: RTN-9-PROCESSING-COMPLETE
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        //*  YOU're OK, I'M OK !
        pdaScia9000.getScia9000_Pnd_Return_Code().setValue("0000");                                                                                                       //Natural: MOVE '0000' TO SCIA9000.#RETURN-CODE
        short decideConditionsMet6545 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SCIA9000.#PROCESSING-TYPE = 'AP' AND SCIA9000.#PROCESS-IND = ' '
        if (condition(pdaScia9000.getScia9000_Pnd_Processing_Type().equals("AP") && pdaScia9000.getScia9000_Pnd_Process_Ind().equals(" ")))
        {
            decideConditionsMet6545++;
            //*  I'S'SUED
            pdaScia9000.getScia9000_Pnd_Process_Ind().setValue("S");                                                                                                      //Natural: MOVE 'S' TO SCIA9000.#PROCESS-IND
        }                                                                                                                                                                 //Natural: WHEN SCIA9000.#PROCESSING-TYPE = 'PP' AND SCIA9000.#PROCESS-IND = ' '
        else if (condition(pdaScia9000.getScia9000_Pnd_Processing_Type().equals("PP") && pdaScia9000.getScia9000_Pnd_Process_Ind().equals(" ")))
        {
            decideConditionsMet6545++;
            //*  ASSI'G'NED
            pdaScia9000.getScia9000_Pnd_Process_Ind().setValue("G");                                                                                                      //Natural: MOVE 'G' TO SCIA9000.#PROCESS-IND
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM RTN-13-COMPRESS-ALLOC-ARRAY
        sub_Rtn_13_Compress_Alloc_Array();
        if (condition(Global.isEscape())) {return;}
        //*  RTN-9-PROCESSING-COMPLETE
    }
    private void sub_Rtn_11b_Post_Completion_Acibat() throws Exception                                                                                                    //Natural: RTN-11B-POST-COMPLETION-ACIBAT
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        //*  ISSUE
        short decideConditionsMet6565 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE SCIA9000.#PROCESS-IND;//Natural: VALUE 'S','P'
        if (condition((pdaScia9000.getScia9000_Pnd_Process_Ind().equals("S") || pdaScia9000.getScia9000_Pnd_Process_Ind().equals("P"))))
        {
            decideConditionsMet6565++;
            pnd_A100_Pnd_N8.setValue(Global.getDATN());                                                                                                                   //Natural: MOVE *DATN TO #N8
            if (condition(pdaScia9000.getScia9000_Pnd_Tiaa_Date_Of_Issue().equals(" ")))                                                                                  //Natural: IF SCIA9000.#TIAA-DATE-OF-ISSUE = ' '
            {
                pdaScia9000.getScia9000_Pnd_Tiaa_Date_Of_Issue().setValue(pnd_A100_Pnd_A8);                                                                               //Natural: MOVE #A8 TO SCIA9000.#TIAA-DATE-OF-ISSUE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia9000.getScia9000_Pnd_Cref_Date_Of_Issue().equals(" ")))                                                                                  //Natural: IF SCIA9000.#CREF-DATE-OF-ISSUE = ' '
            {
                pdaScia9000.getScia9000_Pnd_Cref_Date_Of_Issue().setValue(pnd_A100_Pnd_A8);                                                                               //Natural: MOVE #A8 TO SCIA9000.#CREF-DATE-OF-ISSUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  RTN-11B-POST-COMPLETION-ACIBAT
    }
    private void sub_Rtn_13_Compress_Alloc_Array() throws Exception                                                                                                       //Natural: RTN-13-COMPRESS-ALLOC-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        pnd_J.reset();                                                                                                                                                    //Natural: RESET #J
        FOR10:                                                                                                                                                            //Natural: FOR #I = 1 100
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(100)); pnd_I.nadd(1))
        {
            if (condition(pdaScia9000.getScia9000_Pnd_Allocation().getValue(pnd_I).equals("000") || pdaScia9000.getScia9000_Pnd_Allocation().getValue(pnd_I).equals(" "))) //Natural: IF SCIA9000.#ALLOCATION ( #I ) = '000' OR = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_J.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #J
            pdaScia9000.getScia9000_Pnd_Allocation().getValue(pnd_J).setValue(pdaScia9000.getScia9000_Pnd_Allocation().getValue(pnd_I));                                  //Natural: MOVE SCIA9000.#ALLOCATION ( #I ) TO SCIA9000.#ALLOCATION ( #J )
            pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker().getValue(pnd_J).setValue(pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker().getValue(pnd_I));          //Natural: MOVE SCIA9000.#ALLOCATION-FUND-TICKER ( #I ) TO SCIA9000.#ALLOCATION-FUND-TICKER ( #J )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_J.less(100)))                                                                                                                                   //Natural: IF #J LT 100
        {
            pdaScia9000.getScia9000_Pnd_Allocation().getValue(pnd_J.getDec().add(1),":",100).reset();                                                                     //Natural: RESET SCIA9000.#ALLOCATION ( #J+1:100 ) SCIA9000.#ALLOCATION-FUND-TICKER ( #J+1:100 )
            pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker().getValue(pnd_J.getDec().add(1),":",100).reset();
        }                                                                                                                                                                 //Natural: END-IF
        //*  THIS IS FOR 2ND ALLOCATION                     /* IISG STARTS >>>
        pnd_J.reset();                                                                                                                                                    //Natural: RESET #J
        FOR11:                                                                                                                                                            //Natural: FOR #I = 1 100
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(100)); pnd_I.nadd(1))
        {
            if (condition(pdaScia9000.getScia9000_Pnd_Allocation_2().getValue(pnd_I).equals("000") || pdaScia9000.getScia9000_Pnd_Allocation_2().getValue(pnd_I).equals(" "))) //Natural: IF SCIA9000.#ALLOCATION-2 ( #I ) = '000' OR = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_J.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #J
            pdaScia9000.getScia9000_Pnd_Allocation_2().getValue(pnd_J).setValue(pdaScia9000.getScia9000_Pnd_Allocation_2().getValue(pnd_I));                              //Natural: MOVE SCIA9000.#ALLOCATION-2 ( #I ) TO SCIA9000.#ALLOCATION-2 ( #J )
            pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker_2().getValue(pnd_J).setValue(pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker_2().getValue(pnd_I));      //Natural: MOVE SCIA9000.#ALLOCATION-FUND-TICKER-2 ( #I ) TO SCIA9000.#ALLOCATION-FUND-TICKER-2 ( #J )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_J.less(100)))                                                                                                                                   //Natural: IF #J LT 100
        {
            pdaScia9000.getScia9000_Pnd_Allocation_2().getValue(pnd_J.getDec().add(1),":",100).reset();                                                                   //Natural: RESET SCIA9000.#ALLOCATION-2 ( #J+1:100 ) SCIA9000.#ALLOCATION-FUND-TICKER-2 ( #J+1:100 )
            pdaScia9000.getScia9000_Pnd_Allocation_Fund_Ticker_2().getValue(pnd_J.getDec().add(1),":",100).reset();
            //*  IISG END  <<<
        }                                                                                                                                                                 //Natural: END-IF
        //*  RTN-13-COMPRESS-ALLOC-ARRAY
    }
    private void sub_Verify_If_Contract_Needs_Cip() throws Exception                                                                                                      //Natural: VERIFY-IF-CONTRACT-NEEDS-CIP
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        pnd_Cip.reset();                                                                                                                                                  //Natural: RESET #CIP
        if (condition(pdaScia9000.getScia9000_Pnd_Ppg_Code().equals("SGRD")))                                                                                             //Natural: IF SCIA9000.#PPG-CODE = 'SGRD'
        {
            if (condition(pdaScia9000.getScia9000_Pnd_Cip_Ind().equals("Y")))                                                                                             //Natural: IF SCIA9000.#CIP-IND = 'Y'
            {
                pnd_Cip.setValue("1");                                                                                                                                    //Natural: ASSIGN #CIP := '1'
            }                                                                                                                                                             //Natural: END-IF
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia3600.getScia3600_Ppg().setValue(pdaScia9000.getScia9000_Pnd_Ppg_Code());                                                                                   //Natural: ASSIGN SCIA3600.PPG := SCIA9000.#PPG-CODE
        pdaScia3600.getScia3600_Lob().setValue(pdaScia9000.getScia9000_Pnd_Lob());                                                                                        //Natural: ASSIGN SCIA3600.LOB := SCIA9000.#LOB
        pdaScia3600.getScia3600_Lob_Type().setValue(pdaScia9000.getScia9000_Pnd_Lob_Type());                                                                              //Natural: ASSIGN SCIA3600.LOB-TYPE := SCIA9000.#LOB-TYPE
        pnd_Prdct_Cde.setValue(pdaScia9000.getScia9000_Pnd_Product_Code());                                                                                               //Natural: ASSIGN #PRDCT-CDE := SCIA9000.#PRODUCT-CODE
        if (condition(pnd_Prdct_Cde_Pnd_Prdct_3.greater(" ")))                                                                                                            //Natural: IF #PRDCT-3 GT ' '
        {
            pdaScia3600.getScia3600_Prdct_Cde().setValueEdited(new ReportEditMask("999"),pnd_Prdct_Cde);                                                                  //Natural: MOVE EDITED #PRDCT-CDE TO SCIA3600.PRDCT-CDE ( EM = 999 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Prdct_Cde_Pnd_Prdct_2.greater(" ")))                                                                                                        //Natural: IF #PRDCT-2 GT ' '
            {
                pdaScia3600.getScia3600_Prdct_Cde().setValueEdited(new ReportEditMask("99"),pnd_Prdct_Cde);                                                               //Natural: MOVE EDITED #PRDCT-CDE TO SCIA3600.PRDCT-CDE ( EM = 99 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia3600.getScia3600_Prdct_Cde().setValueEdited(new ReportEditMask("9"),pnd_Prdct_Cde);                                                                //Natural: MOVE EDITED #PRDCT-CDE TO SCIA3600.PRDCT-CDE ( EM = 9 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Scin3600.class , getCurrentProcessState(), pdaScia3600.getScia3600());                                                                            //Natural: CALLNAT 'SCIN3600' SCIA3600
        if (condition(Global.isEscape())) return;
        if (condition(pdaScia3600.getScia3600_Cip_Required().equals("Y")))                                                                                                //Natural: IF SCIA3600.CIP-REQUIRED EQ 'Y'
        {
            pnd_Cip.setValue("1");                                                                                                                                        //Natural: ASSIGN #CIP := '1'
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Rtn_102_Determine_Lob_Lobtype() throws Exception                                                                                                     //Natural: RTN-102-DETERMINE-LOB-LOBTYPE
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        pnd_W_Lob.reset();                                                                                                                                                //Natural: RESET #W-LOB #W-LOB-TYPE #HOLD-SG-PRODUCT
        pnd_W_Lob_Type.reset();
        pnd_Hold_Sg_Product.reset();
        //*  BD1
        //*  BD1
        if (condition(pdaScia9000.getScia9000_Pnd_Sg_Subplan_No().getSubstring(1,3).equals("GA1")))                                                                       //Natural: IF SUBSTR ( SCIA9000.#SG-SUBPLAN-NO,1,3 ) = 'GA1'
        {
            pdaScia9000.getScia9000_Pnd_Irc_Section_Code().setValue("17");                                                                                                //Natural: ASSIGN SCIA9000.#IRC-SECTION-CODE := '17'
            //*  BD1
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Hold_Sg_Product.setValue(pdaScia9000.getScia9000_Pnd_Sg_Subplan_No().getSubstring(1,3));                                                                      //Natural: MOVE SUBSTRING ( SCIA9000.#SG-SUBPLAN-NO,1,3 ) TO #HOLD-SG-PRODUCT
        short decideConditionsMet6669 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF SCIA9000.#PRODUCT-CODE;//Natural: VALUE '001'
        if (condition((pdaScia9000.getScia9000_Pnd_Product_Code().equals("001"))))
        {
            decideConditionsMet6669++;
            //*  RETIREMENT LOAN (CRS) 8/1/05
            if (condition(pnd_Hold_Sg_Product.equals("RL1")))                                                                                                             //Natural: IF #HOLD-SG-PRODUCT = 'RL1'
            {
                pnd_W_Lob.setValue("D");                                                                                                                                  //Natural: MOVE 'D' TO #W-LOB
                pnd_W_Lob_Type.setValue("6");                                                                                                                             //Natural: MOVE '6' TO #W-LOB-TYPE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_W_Lob.setValue("D");                                                                                                                                  //Natural: MOVE 'D' TO #W-LOB
                pnd_W_Lob_Type.setValue("2");                                                                                                                             //Natural: MOVE '2' TO #W-LOB-TYPE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE '002'
        else if (condition((pdaScia9000.getScia9000_Pnd_Product_Code().equals("002"))))
        {
            decideConditionsMet6669++;
            pnd_W_Lob.setValue("D");                                                                                                                                      //Natural: MOVE 'D' TO #W-LOB
            pnd_W_Lob_Type.setValue("7");                                                                                                                                 //Natural: MOVE '7' TO #W-LOB-TYPE
        }                                                                                                                                                                 //Natural: VALUE '003'
        else if (condition((pdaScia9000.getScia9000_Pnd_Product_Code().equals("003"))))
        {
            decideConditionsMet6669++;
            //*  CRS 8/1/05
            //*  IRA TRADITIONAL
            short decideConditionsMet6685 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #HOLD-SG-PRODUCT;//Natural: VALUE 'IR1'
            if (condition((pnd_Hold_Sg_Product.equals("IR1"))))
            {
                decideConditionsMet6685++;
                if (condition(pdaScia9000.getScia9000_Pnd_Sg_Text_Udf_1().getSubstring(4,1).equals("Y")))                                                                 //Natural: IF SUBSTR ( SCIA9000.#SG-TEXT-UDF-1,4,1 ) = 'Y'
                {
                    pnd_W_Lob.setValue("I");                                                                                                                              //Natural: MOVE 'I' TO #W-LOB
                    pnd_W_Lob_Type.setValue("8");                                                                                                                         //Natural: MOVE '8' TO #W-LOB-TYPE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IRA DM - RL6
                    pnd_W_Lob.setValue("I");                                                                                                                              //Natural: MOVE 'I' TO #W-LOB
                    //*  IRA DM - RL6
                    pnd_W_Lob_Type.setValue("4");                                                                                                                         //Natural: MOVE '4' TO #W-LOB-TYPE
                    //*  IRA ROTH
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'IR2'
            else if (condition((pnd_Hold_Sg_Product.equals("IR2"))))
            {
                decideConditionsMet6685++;
                if (condition(pdaScia9000.getScia9000_Pnd_Sg_Text_Udf_1().getSubstring(4,1).equals("Y")))                                                                 //Natural: IF SUBSTR ( SCIA9000.#SG-TEXT-UDF-1,4,1 ) = 'Y'
                {
                    pnd_W_Lob.setValue("I");                                                                                                                              //Natural: MOVE 'I' TO #W-LOB
                    pnd_W_Lob_Type.setValue("7");                                                                                                                         //Natural: MOVE '7' TO #W-LOB-TYPE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IRA DM - RL6
                    pnd_W_Lob.setValue("I");                                                                                                                              //Natural: MOVE 'I' TO #W-LOB
                    //*  IRA DM - RL6
                    pnd_W_Lob_Type.setValue("3");                                                                                                                         //Natural: MOVE '3' TO #W-LOB-TYPE
                    //*  IRA ROLLOVER
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'IR3'
            else if (condition((pnd_Hold_Sg_Product.equals("IR3"))))
            {
                decideConditionsMet6685++;
                //*  IRA DM - RL6
                pnd_W_Lob.setValue("I");                                                                                                                                  //Natural: MOVE 'I' TO #W-LOB
                //*  IRA DM - RL6
                //*  IRA SEP
                pnd_W_Lob_Type.setValue("1");                                                                                                                             //Natural: MOVE '1' TO #W-LOB-TYPE
            }                                                                                                                                                             //Natural: VALUE 'IR8'
            else if (condition((pnd_Hold_Sg_Product.equals("IR8"))))
            {
                decideConditionsMet6685++;
                if (condition(pdaScia9000.getScia9000_Pnd_Sg_Text_Udf_1().getSubstring(4,1).equals("Y")))                                                                 //Natural: IF SUBSTR ( SCIA9000.#SG-TEXT-UDF-1,4,1 ) = 'Y'
                {
                    pnd_W_Lob.setValue("I");                                                                                                                              //Natural: MOVE 'I' TO #W-LOB
                    pnd_W_Lob_Type.setValue("9");                                                                                                                         //Natural: MOVE '9' TO #W-LOB-TYPE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IRA DM - RL6
                    pnd_W_Lob.setValue("I");                                                                                                                              //Natural: MOVE 'I' TO #W-LOB
                    //*  IRA DM - RL6
                    pnd_W_Lob_Type.setValue("6");                                                                                                                         //Natural: MOVE '6' TO #W-LOB-TYPE
                    //*  IRA DM - RL6
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_W_Lob.setValue("S");                                                                                                                                  //Natural: MOVE 'S' TO #W-LOB
                pnd_W_Lob_Type.setValue("2");                                                                                                                             //Natural: MOVE '2' TO #W-LOB-TYPE
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE '004'
        else if (condition((pdaScia9000.getScia9000_Pnd_Product_Code().equals("004"))))
        {
            decideConditionsMet6669++;
            if (condition(pdaScia9000.getScia9000_Pnd_Irc_Section_Code().equals("17")))                                                                                   //Natural: IF SCIA9000.#IRC-SECTION-CODE = '17'
            {
                pnd_W_Lob.setValue("S");                                                                                                                                  //Natural: MOVE 'S' TO #W-LOB
                pnd_W_Lob_Type.setValue("4");                                                                                                                             //Natural: MOVE '4' TO #W-LOB-TYPE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  GSRA
                pnd_W_Lob.setValue("S");                                                                                                                                  //Natural: MOVE 'S' TO #W-LOB
                pnd_W_Lob_Type.setValue("3");                                                                                                                             //Natural: MOVE '3' TO #W-LOB-TYPE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE '018'
        else if (condition((pdaScia9000.getScia9000_Pnd_Product_Code().equals("018"))))
        {
            decideConditionsMet6669++;
            pnd_W_Lob.setValue("I");                                                                                                                                      //Natural: MOVE 'I' TO #W-LOB
            //*  RS PRODUCT
            pnd_W_Lob_Type.setValue("5");                                                                                                                                 //Natural: MOVE '5' TO #W-LOB-TYPE
        }                                                                                                                                                                 //Natural: VALUE '020'
        else if (condition((pdaScia9000.getScia9000_Pnd_Product_Code().equals("020"))))
        {
            decideConditionsMet6669++;
            pnd_W_Lob.setValue("D");                                                                                                                                      //Natural: MOVE 'D' TO #W-LOB
            //*  RSP PRODUCT
            pnd_W_Lob_Type.setValue("5");                                                                                                                                 //Natural: MOVE '5' TO #W-LOB-TYPE
        }                                                                                                                                                                 //Natural: VALUE '021'
        else if (condition((pdaScia9000.getScia9000_Pnd_Product_Code().equals("021"))))
        {
            decideConditionsMet6669++;
            pnd_W_Lob.setValue("S");                                                                                                                                      //Natural: MOVE 'S' TO #W-LOB
            //*  RSP PRODUCT
            pnd_W_Lob_Type.setValue("5");                                                                                                                                 //Natural: MOVE '5' TO #W-LOB-TYPE
        }                                                                                                                                                                 //Natural: VALUE '022'
        else if (condition((pdaScia9000.getScia9000_Pnd_Product_Code().equals("022"))))
        {
            decideConditionsMet6669++;
            pnd_W_Lob.setValue("S");                                                                                                                                      //Natural: MOVE 'S' TO #W-LOB
            //* * ICAP RETIREMENT CHOICE
            pnd_W_Lob_Type.setValue("6");                                                                                                                                 //Natural: MOVE '6' TO #W-LOB-TYPE
        }                                                                                                                                                                 //Natural: VALUE '023'
        else if (condition((pdaScia9000.getScia9000_Pnd_Product_Code().equals("023"))))
        {
            decideConditionsMet6669++;
            pnd_W_Lob.setValue("D");                                                                                                                                      //Natural: MOVE 'D' TO #W-LOB
            //* * ICAP RETIREMENT CHOICE PLUS
            pnd_W_Lob_Type.setValue("A");                                                                                                                                 //Natural: MOVE 'A' TO #W-LOB-TYPE
        }                                                                                                                                                                 //Natural: VALUE '024'
        else if (condition((pdaScia9000.getScia9000_Pnd_Product_Code().equals("024"))))
        {
            decideConditionsMet6669++;
            pnd_W_Lob.setValue("S");                                                                                                                                      //Natural: MOVE 'S' TO #W-LOB
            //* * ICAP RETIREMENT CHOICE PLUS
            pnd_W_Lob_Type.setValue("7");                                                                                                                                 //Natural: MOVE '7' TO #W-LOB-TYPE
        }                                                                                                                                                                 //Natural: VALUE '025'
        else if (condition((pdaScia9000.getScia9000_Pnd_Product_Code().equals("025"))))
        {
            decideConditionsMet6669++;
            pnd_W_Lob.setValue("S");                                                                                                                                      //Natural: MOVE 'S' TO #W-LOB
            //* * DCA CHANGE  - CRS 9/8/06
            //* * ICAP RETIREMENT CHOICE PLUS
            pnd_W_Lob_Type.setValue("9");                                                                                                                                 //Natural: MOVE '9' TO #W-LOB-TYPE
        }                                                                                                                                                                 //Natural: VALUE '026'
        else if (condition((pdaScia9000.getScia9000_Pnd_Product_Code().equals("026"))))
        {
            decideConditionsMet6669++;
            pnd_W_Lob.setValue("D");                                                                                                                                      //Natural: MOVE 'D' TO #W-LOB
            //*  BJD RHSP
            pnd_W_Lob_Type.setValue("B");                                                                                                                                 //Natural: MOVE 'B' TO #W-LOB-TYPE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                                   //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
            pdaScia9000.getScia9000_Pnd_Return_Msg().setValue(DbsUtil.compress("(8350) UNKNOWN PRODUCT", pdaScia9000.getScia9000_Pnd_Product_Code()));                    //Natural: COMPRESS '(8350) UNKNOWN PRODUCT'SCIA9000.#PRODUCT-CODE INTO SCIA9000.#RETURN-MSG
            pdaScia9000.getScia9000_Pnd_Process_Ind().setValue("E");                                                                                                      //Natural: MOVE 'E' TO SCIA9000.#PROCESS-IND
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  RTN-102-DETERMINE-LOB-LOBTYPE
    }
    private void sub_Rtn_1b_Defaults_Acibat() throws Exception                                                                                                            //Natural: RTN-1B-DEFAULTS-ACIBAT
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        pdaScia1203.getScia1203_Pnd_Func_Mit().setValue("Y");                                                                                                             //Natural: MOVE 'Y' TO SCIA1203.#FUNC-MIT SCIA1203.#FUNC-COR SCIA1203.#FUNC-COR-GET-CONTRACT SCIA1203.#FUNC-COR-CREATE-PIN SCIA1203.#FUNC-COR-ADD-CONTRACT SCIA1203.#FUNC-NAS SCIA1203.#FUNC-NAS-FINALIST SCIA1203.#FUNC-BENE SCIA1203.#FUNC-PRAP SCIA1203.#FUNC-NON-PREM SCIA1203.#FUNC-STATE
        pdaScia1203.getScia1203_Pnd_Func_Cor().setValue("Y");
        pdaScia1203.getScia1203_Pnd_Func_Cor_Get_Contract().setValue("Y");
        pdaScia1203.getScia1203_Pnd_Func_Cor_Create_Pin().setValue("Y");
        pdaScia1203.getScia1203_Pnd_Func_Cor_Add_Contract().setValue("Y");
        pdaScia1203.getScia1203_Pnd_Func_Nas().setValue("Y");
        pdaScia1203.getScia1203_Pnd_Func_Nas_Finalist().setValue("Y");
        pdaScia1203.getScia1203_Pnd_Func_Bene().setValue("Y");
        pdaScia1203.getScia1203_Pnd_Func_Prap().setValue("Y");
        pdaScia1203.getScia1203_Pnd_Func_Non_Prem().setValue("Y");
        pdaScia1203.getScia1203_Pnd_Func_State().setValue("Y");
        pdaScia1203.getScia1203_Pnd_Func_Nas_Get_Current_Address().setValue("N");                                                                                         //Natural: MOVE 'N' TO SCIA1203.#FUNC-NAS-GET-CURRENT-ADDRESS SCIA1203.#FUNC-ACCPTD-APPLCTN SCIA1203.#FUNC-ACCPTD-APPLCTN-INET SCIA1203.#FUNC-FLD1 SCIA1203.#FUNC-FLD2 SCIA1203.#FUNC-FLD3
        pdaScia1203.getScia1203_Pnd_Func_Accptd_Applctn().setValue("N");
        pdaScia1203.getScia1203_Pnd_Func_Accptd_Applctn_Inet().setValue("N");
        pdaScia1203.getScia1203_Pnd_Func_Fld1().setValue("N");
        pdaScia1203.getScia1203_Pnd_Func_Fld2().setValue("N");
        pdaScia1203.getScia1203_Pnd_Func_Fld3().setValue("N");
        if (condition(pdaScia9000.getScia9000_Pnd_Processing_Type().equals("AP")))                                                                                        //Natural: IF SCIA9000.#PROCESSING-TYPE = 'AP'
        {
            pdaScia1203.getScia1203_Pnd_Func_Alloc().setValue("Y");                                                                                                       //Natural: MOVE 'Y' TO SCIA1203.#FUNC-ALLOC
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia1203.getScia1203_Pnd_Func_Alloc().setValue("N");                                                                                                       //Natural: MOVE 'N' TO SCIA1203.#FUNC-ALLOC
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Edit_Flags_Pnd_Edit_Communication_Data.setValue(true);                                                                                                        //Natural: MOVE TRUE TO #EDIT-COMMUNICATION-DATA #EDIT-REQUESTOR-DATA #EDIT-PROCESSING-DATA #EDIT-CONTRACT-DATA #EDIT-MIT-DATA #EDIT-PARTICIPANT-DATA #EDIT-ALLOCATION-DATA #EDIT-FINANCIAL-DATA
        pnd_Edit_Flags_Pnd_Edit_Requestor_Data.setValue(true);
        pnd_Edit_Flags_Pnd_Edit_Processing_Data.setValue(true);
        pnd_Edit_Flags_Pnd_Edit_Contract_Data.setValue(true);
        pnd_Edit_Flags_Pnd_Edit_Mit_Data.setValue(true);
        pnd_Edit_Flags_Pnd_Edit_Participant_Data.setValue(true);
        pnd_Edit_Flags_Pnd_Edit_Allocation_Data.setValue(true);
        pnd_Edit_Flags_Pnd_Edit_Financial_Data.setValue(true);
        if (condition(pdaScia9000.getScia9000_Pnd_Processing_Type().equals("AP")))                                                                                        //Natural: IF SCIA9000.#PROCESSING-TYPE = 'AP'
        {
            pnd_Edit_Flags_Pnd_Edit_Address_Data.setValue(true);                                                                                                          //Natural: MOVE TRUE TO #EDIT-ADDRESS-DATA #EDIT-BENEFICIARY-DATA
            pnd_Edit_Flags_Pnd_Edit_Beneficiary_Data.setValue(true);
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaScia9000.getScia9000_Pnd_Product_Code().equals(" ") || pdaScia9000.getScia9000_Pnd_Product_Code().equals(pnd_Null)))                             //Natural: IF SCIA9000.#PRODUCT-CODE = ' ' OR = #NULL
        {
            pdaScia9000.getScia9000_Pnd_Product_Code().moveAll("0");                                                                                                      //Natural: MOVE ALL '0' TO SCIA9000.#PRODUCT-CODE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia9000.getScia9000_Pnd_Product_Code().setValue(pdaScia9000.getScia9000_Pnd_Product_Code(), MoveOption.RightJustified);                                   //Natural: MOVE RIGHT JUSTIFIED SCIA9000.#PRODUCT-CODE TO SCIA9000.#PRODUCT-CODE
            DbsUtil.examine(new ExamineSource(pdaScia9000.getScia9000_Pnd_Product_Code(),true), new ExamineSearch(" "), new ExamineReplace("0"));                         //Natural: EXAMINE FULL SCIA9000.#PRODUCT-CODE FOR ' ' REPLACE WITH '0'
            if (condition(! (DbsUtil.maskMatches(pdaScia9000.getScia9000_Pnd_Product_Code(),"NNN"))))                                                                     //Natural: IF SCIA9000.#PRODUCT-CODE NE MASK ( NNN )
            {
                pdaScia9000.getScia9000_Pnd_Return_Code().setValue("1201");                                                                                               //Natural: MOVE '1201' TO SCIA9000.#RETURN-CODE
                pdaScia9000.getScia9000_Pnd_Return_Msg().setValue("(1880) PRDCT CODE MUST BE NUMERIC");                                                                   //Natural: MOVE '(1880) PRDCT CODE MUST BE NUMERIC' TO SCIA9000.#RETURN-MSG
                pdaScia9000.getScia9000_Pnd_Process_Ind().setValue("E");                                                                                                  //Natural: MOVE 'E' TO SCIA9000.#PROCESS-IND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet6813 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SCIA9000.#CONTRACT-TYPE NE ' '
        if (condition(pdaScia9000.getScia9000_Pnd_Contract_Type().notEquals(" ")))
        {
            decideConditionsMet6813++;
            ignore();
        }                                                                                                                                                                 //Natural: WHEN SCIA9000.#LOB = ' ' AND SCIA9000.#LOB-TYPE = ' '
        else if (condition(pdaScia9000.getScia9000_Pnd_Lob().equals(" ") && pdaScia9000.getScia9000_Pnd_Lob_Type().equals(" ")))
        {
            decideConditionsMet6813++;
                                                                                                                                                                          //Natural: PERFORM RTN-102-DETERMINE-LOB-LOBTYPE
            sub_Rtn_102_Determine_Lob_Lobtype();
            if (condition(Global.isEscape())) {return;}
            if (condition(pdaScia9000.getScia9000_Pnd_Process_Ind().equals("E")))                                                                                         //Natural: IF SCIA9000.#PROCESS-IND = 'E'
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia9000.getScia9000_Pnd_Lob().setValue(pnd_W_Lob);                                                                                                    //Natural: MOVE #W-LOB TO SCIA9000.#LOB
                pdaScia9000.getScia9000_Pnd_Lob_Type().setValue(pnd_W_Lob_Type);                                                                                          //Natural: MOVE #W-LOB-TYPE TO SCIA9000.#LOB-TYPE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN SCIA9000.#PRODUCT-CODE = '004' OR ( SCIA9000.#LOB = 'S' AND SCIA9000.#LOB-TYPE = '3' )
        else if (condition((pdaScia9000.getScia9000_Pnd_Product_Code().equals("004") || (pdaScia9000.getScia9000_Pnd_Lob().equals("S") && pdaScia9000.getScia9000_Pnd_Lob_Type().equals("3")))))
        {
            decideConditionsMet6813++;
            if (condition(pdaScia9000.getScia9000_Pnd_Irc_Section_Code().equals("17")))                                                                                   //Natural: IF SCIA9000.#IRC-SECTION-CODE = '17'
            {
                pdaScia9000.getScia9000_Pnd_Lob().setValue("S");                                                                                                          //Natural: MOVE 'S' TO SCIA9000.#LOB
                pdaScia9000.getScia9000_Pnd_Lob_Type().setValue("4");                                                                                                     //Natural: MOVE '4' TO SCIA9000.#LOB-TYPE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  GSRA
                pdaScia9000.getScia9000_Pnd_Lob().setValue("S");                                                                                                          //Natural: MOVE 'S' TO SCIA9000.#LOB
                pdaScia9000.getScia9000_Pnd_Lob_Type().setValue("3");                                                                                                     //Natural: MOVE '3' TO SCIA9000.#LOB-TYPE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pdaScia9000.getScia9000_Pnd_Allocation_Fmt().setValue("O");                                                                                                       //Natural: MOVE 'O' TO SCIA9000.#ALLOCATION-FMT
        //*  RTN-1B-DEFAULTS-ACIBAT
    }
    private void sub_Call_Object() throws Exception                                                                                                                       //Natural: CALL-OBJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("STORE")))                                                                                                //Natural: IF CDAOBJ.#FUNCTION = 'STORE'
        {
            if (condition(pdaScia9000.getScia9000_Pnd_System_Requestor().equals("ACIBATNE") || pdaScia9000.getScia9000_Pnd_System_Requestor().equals("ACIBATSG")))        //Natural: IF SCIA9000.#SYSTEM-REQUESTOR EQ 'ACIBATNE' OR = 'ACIBATSG'
            {
                pnd_Tiaa_Cref_Contract.setValue(pdaScia1203.getScia1203_Pnd_Return_Msg_Fld2());                                                                           //Natural: ASSIGN #TIAA-CREF-CONTRACT := SCIA1203.#RETURN-MSG-FLD2
                pdaScia3540.getScia3540_Ap_Contingent_Bene_Info().getValue(4).setValue(pnd_Tiaa_Cref_Contract_Pnd_Tiaa_Contract);                                         //Natural: ASSIGN SCIA3540.AP-CONTINGENT-BENE-INFO ( 4 ) := #TIAA-CONTRACT
                pdaScia3540.getScia3540_Ap_Contingent_Bene_Info().getValue(5).setValue(pnd_Tiaa_Cref_Contract_Pnd_Cref_Contract);                                         //Natural: ASSIGN SCIA3540.AP-CONTINGENT-BENE-INFO ( 5 ) := #CREF-CONTRACT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Scin3540.class , getCurrentProcessState(), pdaScia3540.getScia3540(), pdaScia3540.getScia3540_Id(), pdaScia3540.getScia3540_Var(),                //Natural: CALLNAT 'SCIN3540' SCIA3540 SCIA3540-ID SCIA3540-VAR SCIA3545 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB
            pdaScia3545.getScia3545(), pdaCdaobj.getCdaobj(), pdaAcipda_D.getDialog_Info_Sub(), pdaAcipda_M.getMsg_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##ERROR-FIELD NE ' ' THEN
        {
            pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("ACIN3500(2310)-", pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg(), "SCIA3540-ID =",            //Natural: COMPRESS 'ACIN3500(2310)-' MSG-INFO-SUB.##MSG 'SCIA3540-ID =' SCIA3540-ID SCIA3540.AP-G-KEY INTO MSG-INFO-SUB.##MSG
                pdaScia3540.getScia3540_Id(), pdaScia3540.getScia3540_Ap_G_Key()));
            getReports().write(0, pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                             //Natural: WRITE MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet6864 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CDAOBJ.#FUNCTION = 'GET' OR = 'NEXT' AND CDAOBJ.#EXISTS
        if (condition(((pdaCdaobj.getCdaobj_Pnd_Function().equals("GET") || pdaCdaobj.getCdaobj_Pnd_Function().equals("NEXT")) && pdaCdaobj.getCdaobj_Pnd_Exists().getBoolean())))
        {
            decideConditionsMet6864++;
            pnd_New_Object.setValue(true);                                                                                                                                //Natural: ASSIGN #NEW-OBJECT = TRUE
            if (condition(pdaCdmntdfa.getCdmntdfa_Pnd_Reposition_Display().getBoolean()))                                                                                 //Natural: IF CDMNTDFA.#REPOSITION-DISPLAY THEN
            {
                pnd_Scroll_Vars.resetInitial();                                                                                                                           //Natural: RESET INITIAL #SCROLL-VARS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN CDAOBJ.#FUNCTION = 'UPDATE' OR = 'STORE' OR = 'DELETE'
        else if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE") || pdaCdaobj.getCdaobj_Pnd_Function().equals("STORE") || pdaCdaobj.getCdaobj_Pnd_Function().equals("DELETE")))
        {
            decideConditionsMet6864++;
            if (condition(! (pdaCdmntdfa.getCdmntdfa_Pnd_Clear_After_Update().getBoolean())))                                                                             //Natural: IF NOT CDMNTDFA.#CLEAR-AFTER-UPDATE THEN
            {
                pnd_New_Object.setValue(true);                                                                                                                            //Natural: ASSIGN #NEW-OBJECT = TRUE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaCdmntdfa.getCdmntdfa_Pnd_Reposition_Update().getBoolean()))                                                                                  //Natural: IF CDMNTDFA.#REPOSITION-UPDATE THEN
            {
                pnd_Scroll_Vars.resetInitial();                                                                                                                           //Natural: RESET INITIAL #SCROLL-VARS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN CDAOBJ.#FUNCTION = 'FINDNUM'
        else if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("FINDNUM")))
        {
            decideConditionsMet6864++;
            pnd_Scroll_Vars.resetInitial();                                                                                                                               //Natural: RESET INITIAL #SCROLL-VARS
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Scroll_Vars.resetInitial();                                                                                                                               //Natural: RESET INITIAL #SCROLL-VARS
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_New_Object.getBoolean()))                                                                                                                       //Natural: IF #NEW-OBJECT THEN
        {
            pnd_Displayed_Key.setValue(pdaScia3540.getScia3540_Id());                                                                                                     //Natural: ASSIGN #DISPLAYED-KEY = SCIA3540-ID
            pnd_New_Object.reset();                                                                                                                                       //Natural: RESET #NEW-OBJECT
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALL-OBJECT
    }
    private void sub_Call_Acin9004() throws Exception                                                                                                                     //Natural: CALL-ACIN9004
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------
        if (condition(pdaScia1203.getScia1203_Pnd_Func_Accptd_Applctn().equals("N")))                                                                                     //Natural: IF SCIA1203.#FUNC-ACCPTD-APPLCTN = 'N'
        {
            pdaAcipda_M.getMsg_Info_Sub().reset();                                                                                                                        //Natural: RESET MSG-INFO-SUB
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.callnat(Scin9004.class , getCurrentProcessState(), pdaScia9004.getScia9004(), pdaScia9004.getScia9004_Id(), pdaScia904r.getScia904r(),                //Natural: CALLNAT 'SCIN9004' SCIA9004 SCIA9004-ID SCIA904R CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB
                pdaCdaobj.getCdaobj(), pdaAcipda_D.getDialog_Info_Sub(), pdaAcipda_M.getMsg_Info_Sub());
            if (condition(Global.isEscape())) return;
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  HK <<<<
    }
    private void sub_Process_Error() throws Exception                                                                                                                     //Natural: PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  SET DEFAULT FOR :1:.
        if (condition(pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).equals(" ")))                                                                            //Natural: IF MSG-INFO-SUB.##MSG-DATA ( 1 ) = ' ' THEN
        {
                                                                                                                                                                          //Natural: PERFORM SET-OBJECT-ID
            sub_Set_Object_Id();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  SET DEFAULT ERROR FIELD.
        if (condition(pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().equals(" ")))                                                                                     //Natural: IF MSG-INFO-SUB.##ERROR-FIELD = ' ' THEN
        {
            if (condition(pnd_Current_Field.notEquals(" ")))                                                                                                              //Natural: IF #CURRENT-FIELD NE ' '
            {
                pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue(pnd_Current_Field);                                                                            //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = #CURRENT-FIELD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("BENE-STS-LOB-SSN");                                                                           //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'BENE-STS-LOB-SSN'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ERROR.
        pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE = 'E'
        //*  PROCESS-ERROR
    }
    private void sub_Create_Object() throws Exception                                                                                                                     //Natural: CREATE-OBJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  DON't allow the key to be null.
        //*  COMPARE WITH NULL
        if (condition(pdaScia3012.getScia3012_Id().equals(next_View_Bene_Sts_Lob_Ssn)))                                                                                   //Natural: IF SCIA3012-ID = NEXT-VIEW.BENE-STS-LOB-SSN
        {
            pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue(pnd_Object);                                                                              //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = #OBJECT
            pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("EI");                                                                                                     //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'EI'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF OBJECT ALREADY EXIST, RETURN WITH ERROR.
                                                                                                                                                                          //Natural: PERFORM CHECK-EXISTENCE
        sub_Check_Existence();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaCdaobj.getCdaobj_Pnd_Exists().getBoolean()))                                                                                                     //Natural: IF CDAOBJ.#EXISTS THEN
        {
            pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("EXISTS");                                                                                                 //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'EXISTS'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  PRE-EDIT OBJECT HEADER
                                                                                                                                                                          //Natural: PERFORM EDIT-OBJECT
        sub_Edit_Object();
        if (condition(Global.isEscape())) {return;}
        ldaScil3010.getAcis_Bene_File_Bene_Hold_Cde().setValue(Global.getTIMX());                                                                                         //Natural: ASSIGN ACIS-BENE-FILE.BENE-HOLD-CDE = *TIMX
        ldaScil3010.getVw_acis_Bene_File().insertDBRow();                                                                                                                 //Natural: STORE ACIS-BENE-FILE
        //*  SINCE OBJECT ID DOES NOT HAVE UNIQUE KEY ON DATABASE, MAKE SURE
        //*  THAT TWO USERS DID NOT STORE OBJECT AT THE SAME TIME.
                                                                                                                                                                          //Natural: PERFORM CHECK-EXISTENCE
        sub_Check_Existence();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-OBJECT
    }
    private void sub_Get_Object() throws Exception                                                                                                                        //Natural: GET-OBJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  GET THE PRIMARY OBJECT RECORD.
        ldaScil3010.getVw_acis_Bene_File().startDatabaseFind                                                                                                              //Natural: FIND ( 1 ) ACIS-BENE-FILE WITH BENE-STS-LOB-SSN = SCIA3012-ID
        (
        "FIND02",
        new Wc[] { new Wc("BENE_STS_LOB_SSN", "=", pdaScia3012.getScia3012_Id(), WcType.WITH) },
        1
        );
        FIND02:
        while (condition(ldaScil3010.getVw_acis_Bene_File().readNextRow("FIND02", true)))
        {
            ldaScil3010.getVw_acis_Bene_File().setIfNotFoundControlFlag(false);
            if (condition(ldaScil3010.getVw_acis_Bene_File().getAstCOUNTER().equals(0)))                                                                                  //Natural: IF NO RECORDS FOUND
            {
                //*  RETURN THE FACT THAT THE OBJECT DOES NOT EXIST.
                pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("EE");                                                                                                 //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'EE'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-NOREC
                                                                                                                                                                          //Natural: PERFORM CLEAR
            sub_Clear();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaCdaobj.getCdaobj_Pnd_Exists().setValue(true);                                                                                                              //Natural: ASSIGN CDAOBJ.#EXISTS = TRUE
            //*  "Hold" THE RECORD BY CAPTURING THE VALUE OF THE UPDATE FLAG.
            pdaScia301r.getScia301r_Intervening_Upd_Fld().setValue(ldaScil3010.getAcis_Bene_File_Bene_Hold_Cde());                                                        //Natural: ASSIGN SCIA301R.INTERVENING-UPD-FLD = ACIS-BENE-FILE.BENE-HOLD-CDE
            //*  SHIFT PRIMARY ENTITY INFORMATION TO OBJECT
            pdaScia3012.getScia3012().setValuesByName(ldaScil3010.getVw_acis_Bene_File());                                                                                //Natural: MOVE BY NAME ACIS-BENE-FILE TO SCIA3012
            pdaScia301r.getScia301r_Held_Id().setValue(pdaScia3012.getScia3012_Id());                                                                                     //Natural: ASSIGN SCIA301R.HELD-ID = SCIA3012-ID
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  GET-OBJECT
    }
    private void sub_Hold_Object() throws Exception                                                                                                                       //Natural: HOLD-OBJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  GET PRIMARY ENTITY AND PROCESS IT.
        ldaScil3010.getVw_acis_Bene_File().startDatabaseFind                                                                                                              //Natural: FIND ( 1 ) ACIS-BENE-FILE WITH BENE-STS-LOB-SSN = SCIA3012-ID
        (
        "HOLD_PRIME",
        new Wc[] { new Wc("BENE_STS_LOB_SSN", "=", pdaScia3012.getScia3012_Id(), WcType.WITH) },
        1
        );
        HOLD_PRIME:
        while (condition(ldaScil3010.getVw_acis_Bene_File().readNextRow("HOLD_PRIME", true)))
        {
            ldaScil3010.getVw_acis_Bene_File().setIfNotFoundControlFlag(false);
            if (condition(ldaScil3010.getVw_acis_Bene_File().getAstCOUNTER().equals(0)))                                                                                  //Natural: IF NO RECORDS FOUND
            {
                //*  TRYING TO PROCESS AN OBJECT THAT DOESN't exist.
                pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("E");                                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'E'
                pdaScia301r.getScia301r().reset();                                                                                                                        //Natural: RESET SCIA301R
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("HOLD_PRIME"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("HOLD_PRIME"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(pdaScia301r.getScia301r_Intervening_Upd_Fld().notEquals(ldaScil3010.getAcis_Bene_File_Bene_Hold_Cde())))                                        //Natural: IF SCIA301R.INTERVENING-UPD-FLD NE ACIS-BENE-FILE.BENE-HOLD-CDE THEN
            {
                pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("E");                                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'E'
                pdaScia301r.getScia301r().reset();                                                                                                                        //Natural: RESET SCIA301R
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("HOLD_PRIME"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("HOLD_PRIME"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pdaCdaobj.getCdaobj_Pnd_Exists().setValue(true);                                                                                                              //Natural: ASSIGN CDAOBJ.#EXISTS = TRUE
            //*  PRE-EDIT OBJECT HEADER
                                                                                                                                                                          //Natural: PERFORM EDIT-OBJECT
            sub_Edit_Object();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("HOLD_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("HOLD_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            short decideConditionsMet7017 = 0;                                                                                                                            //Natural: DECIDE ON EVERY VALUE CDAOBJ.#FUNCTION;//Natural: VALUE 'UPDATE'
            if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE")))
            {
                decideConditionsMet7017++;
                ldaScil3010.getAcis_Bene_File_Bene_Hold_Cde().setValue(Global.getTIMX());                                                                                 //Natural: ASSIGN ACIS-BENE-FILE.BENE-HOLD-CDE = *TIMX
                ldaScil3010.getVw_acis_Bene_File().updateDBRow("HOLD_PRIME");                                                                                             //Natural: UPDATE ( HOLD-PRIME. )
            }                                                                                                                                                             //Natural: VALUE 'DELETE'
            if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("DELETE")))
            {
                decideConditionsMet7017++;
                ldaScil3010.getVw_acis_Bene_File().deleteDBRow("HOLD_PRIME");                                                                                             //Natural: DELETE ( HOLD-PRIME. )
                pdaScia301r.getScia301r_Held_Id().reset();                                                                                                                //Natural: RESET SCIA301R.HELD-ID
            }                                                                                                                                                             //Natural: NONE
            if (condition(decideConditionsMet7017 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  HOLD-OBJECT
    }
    //*  PRE-EDITING OBJECT HEADER
    private void sub_Edit_Object() throws Exception                                                                                                                       //Natural: EDIT-OBJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE") || pdaCdaobj.getCdaobj_Pnd_Function().equals("STORE")))                                         //Natural: IF CDAOBJ.#FUNCTION = 'UPDATE' OR = 'STORE'
        {
            //*  BUILD UP THE HEADER RECORD FOR EDITING OR UPDATING
            ldaScil3010.getVw_acis_Bene_File().setValuesByName(pdaScia3012.getScia3012());                                                                                //Natural: MOVE BY NAME SCIA3012 TO ACIS-BENE-FILE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT-OBJECT
    }
    private void sub_Check_Existence() throws Exception                                                                                                                   //Natural: CHECK-EXISTENCE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  CHECK WHETHER OBJECT CURRENTLY EXISTS.
        vw_next_View.createHistogram                                                                                                                                      //Natural: HISTOGRAM ( 1 ) NEXT-VIEW FOR BENE-STS-LOB-SSN FROM SCIA3012-ID THRU SCIA3012-ID
        (
        "EXISTENCE_CHECK",
        "BENE_STS_LOB_SSN",
        new Wc[] { new Wc("BENE_STS_LOB_SSN", ">=", pdaScia3012.getScia3012_Id(), "And", WcType.WITH) ,
        new Wc("BENE_STS_LOB_SSN", "<=", pdaScia3012.getScia3012_Id(), WcType.WITH) },
        1
        );
        EXISTENCE_CHECK:
        while (condition(vw_next_View.readNextRow("EXISTENCE_CHECK")))
        {
            //*  OBJECT ID NOT UNIQUE.
            if (condition(vw_next_View.getAstNUMBER().greater(1)))                                                                                                        //Natural: IF *NUMBER ( EXISTENCE-CHECK. ) GT 1 THEN
            {
                pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("ED");                                                                                                 //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'ED'
                pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(2).setValue(vw_next_View.getAstNUMBER());                                                         //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 2 ) = *NUMBER ( EXISTENCE-CHECK. )
                pdaScia301r.getScia301r().reset();                                                                                                                        //Natural: RESET SCIA301R
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("EXISTENCE_CHECK"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("EXISTENCE_CHECK"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pdaCdaobj.getCdaobj_Pnd_Exists().setValue(true);                                                                                                              //Natural: ASSIGN CDAOBJ.#EXISTS = TRUE
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        //*  CHECK-EXISTENCE
    }
    private void sub_Clear() throws Exception                                                                                                                             //Natural: CLEAR
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        //*  CAPTURE KEY.
        pdaScia3012.getScia3012_Id_Structure().setValuesByName(pdaScia3012.getScia3012());                                                                                //Natural: MOVE BY NAME SCIA3012 TO SCIA3012-ID.STRUCTURE
        pdaScia3012.getScia3012().reset();                                                                                                                                //Natural: RESET SCIA3012 SCIA301R
        pdaScia301r.getScia301r().reset();
        //*  RESTORE KEY.
        pdaScia3012.getScia3012().setValuesByName(pdaScia3012.getScia3012_Id_Structure());                                                                                //Natural: MOVE BY NAME SCIA3012-ID.STRUCTURE TO SCIA3012
        //*  CLEAR
    }
    private void sub_Get_Db_Call() throws Exception                                                                                                                       //Natural: GET-DB-CALL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet7079 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #OLD-REC AND ( CDAOBJ.#FUNCTION = 'DELETE' OR ( CDAOBJ.#FUNCTION = 'UPDATE' AND NOT #SAVE-REC ) )
        if (condition((pnd_Old_Rec.getBoolean() && (pdaCdaobj.getCdaobj_Pnd_Function().equals("DELETE") || (pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE") 
            && ! (pnd_Save_Rec.getBoolean()))))))
        {
            decideConditionsMet7079++;
            //*  DELETE
            pnd_Db_Call.setValue("D");                                                                                                                                    //Natural: ASSIGN #DB-CALL = 'D'
        }                                                                                                                                                                 //Natural: WHEN CDAOBJ.#FUNCTION = 'UPDATE' AND #SAVE-REC AND #OLD-REC
        else if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE") && pnd_Save_Rec.getBoolean() && pnd_Old_Rec.getBoolean()))
        {
            decideConditionsMet7079++;
            //*  UPDATE
            pnd_Db_Call.setValue("U");                                                                                                                                    //Natural: ASSIGN #DB-CALL = 'U'
        }                                                                                                                                                                 //Natural: WHEN CDAOBJ.#FUNCTION = 'UPDATE' AND #SAVE-REC AND NOT #OLD-REC
        else if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE") && pnd_Save_Rec.getBoolean() && ! (pnd_Old_Rec.getBoolean())))
        {
            decideConditionsMet7079++;
            pnd_Db_Call.setValue("S");                                                                                                                                    //Natural: ASSIGN #DB-CALL = 'S'
        }                                                                                                                                                                 //Natural: WHEN CDAOBJ.#FUNCTION = 'STORE' AND #SAVE-REC
        else if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("STORE") && pnd_Save_Rec.getBoolean()))
        {
            decideConditionsMet7079++;
            pnd_Db_Call.setValue("S");                                                                                                                                    //Natural: ASSIGN #DB-CALL = 'S'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Db_Call.reset();                                                                                                                                          //Natural: RESET #DB-CALL
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  GET-DB-CALL
    }
    private void sub_Set_Object_Id() throws Exception                                                                                                                     //Natural: SET-OBJECT-ID
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaScia3012.getScia3012_Bene_Record_Status(), //Natural: COMPRESS SCIA3012.BENE-RECORD-STATUS '-' SCIA3012.BENE-LOB '-' SCIA3012.BENE-LOB-TYPE '-' SCIA3012.BENE-PART-SSN TO MSG-INFO-SUB.##MSG-DATA ( 1 ) LEAVING NO
            "-", pdaScia3012.getScia3012_Bene_Lob(), "-", pdaScia3012.getScia3012_Bene_Lob_Type(), "-", pdaScia3012.getScia3012_Bene_Part_Ssn()));
        pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue(DbsUtil.compress(pnd_Object, pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1)));    //Natural: COMPRESS #OBJECT MSG-INFO-SUB.##MSG-DATA ( 1 ) TO MSG-INFO-SUB.##MSG-DATA ( 1 )
        //*  SET-OBJECT-ID
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        if (condition(Global.getERROR_NR().equals(3145)))                                                                                                                 //Natural: IF *ERROR-NR = 3145
        {
            if (condition(pnd_Retry_Count.greater(200)))                                                                                                                  //Natural: IF #RETRY-COUNT > 200
            {
                pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress(Global.getPROGRAM(), "TRIED TO ACCESS FILE 213 -  200 TIMES, FAILED"));               //Natural: COMPRESS *PROGRAM 'TRIED TO ACCESS FILE 213 -  200 TIMES, FAILED' INTO MSG-INFO-SUB.##MSG
                pdaScia3500.getScia3500_Pnd_Pnd_Pnd_Process_Error_Ind().setValue("E");                                                                                    //Natural: ASSIGN SCIA3500.###PROCESS-ERROR-IND = 'E'
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Retry_Count.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #RETRY-COUNT
                OnErrorManager.popRetry();                                                                                                                                //Natural: RETRY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132");
    }
}
