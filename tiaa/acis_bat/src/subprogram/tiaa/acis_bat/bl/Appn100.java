/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:58:51 PM
**        * FROM NATURAL SUBPROGRAM : Appn100
************************************************************
**        * FILE NAME            : Appn100.java
**        * CLASS NAME           : Appn100
**        * INSTANCE NAME        : Appn100
************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appn100 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAppa100 pdaAppa100;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Name;

    private DbsGroup pnd_Name__R_Field_1;
    private DbsField pnd_Name_Pnd_Name_C;
    private DbsField pnd_U;
    private DbsField pnd_U2;

    private DbsGroup pnd_U2__R_Field_2;
    private DbsField pnd_U2_Pnd_U2il;
    private DbsField pnd_U2_Pnd_U2ir;

    private DbsGroup pnd_U2__R_Field_3;
    private DbsField pnd_U2_Pnd_Ui;
    private DbsField pnd_L;

    private DbsGroup pnd_Tt;
    private DbsField pnd_Tt_Pnd_Tt_0;
    private DbsField pnd_Tt_Pnd_Tt_1;
    private DbsField pnd_Tt_Pnd_Tt_2;
    private DbsField pnd_Tt_Pnd_Tt_3;
    private DbsField pnd_Tt_Pnd_Tt_4;
    private DbsField pnd_Tt_Pnd_Tt_5;
    private DbsField pnd_Tt_Pnd_Tt_6;
    private DbsField pnd_Tt_Pnd_Tt_7;
    private DbsField pnd_Tt_Pnd_Tt_8;
    private DbsField pnd_Tt_Pnd_Tt_9;
    private DbsField pnd_Tt_Pnd_Tt_A;
    private DbsField pnd_Tt_Pnd_Tt_B;
    private DbsField pnd_Tt_Pnd_Tt_C;
    private DbsField pnd_Tt_Pnd_Tt_D;
    private DbsField pnd_Tt_Pnd_Tt_E;
    private DbsField pnd_Tt_Pnd_Tt_F;

    private DbsGroup pnd_Tt__R_Field_4;
    private DbsField pnd_Tt_Pnd_Ttc;

    private DbsGroup pnd_Tt__R_Field_5;
    private DbsField pnd_Tt_Pnd_Tti;
    private DbsField pnd_A;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Del_Cnt;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaAppa100 = new PdaAppa100(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 35);

        pnd_Name__R_Field_1 = localVariables.newGroupInRecord("pnd_Name__R_Field_1", "REDEFINE", pnd_Name);
        pnd_Name_Pnd_Name_C = pnd_Name__R_Field_1.newFieldArrayInGroup("pnd_Name_Pnd_Name_C", "#NAME-C", FieldType.STRING, 1, new DbsArrayController(1, 
            35));
        pnd_U = localVariables.newFieldInRecord("pnd_U", "#U", FieldType.STRING, 1);
        pnd_U2 = localVariables.newFieldInRecord("pnd_U2", "#U2", FieldType.STRING, 2);

        pnd_U2__R_Field_2 = localVariables.newGroupInRecord("pnd_U2__R_Field_2", "REDEFINE", pnd_U2);
        pnd_U2_Pnd_U2il = pnd_U2__R_Field_2.newFieldInGroup("pnd_U2_Pnd_U2il", "#U2IL", FieldType.STRING, 1);
        pnd_U2_Pnd_U2ir = pnd_U2__R_Field_2.newFieldInGroup("pnd_U2_Pnd_U2ir", "#U2IR", FieldType.STRING, 1);

        pnd_U2__R_Field_3 = localVariables.newGroupInRecord("pnd_U2__R_Field_3", "REDEFINE", pnd_U2);
        pnd_U2_Pnd_Ui = pnd_U2__R_Field_3.newFieldInGroup("pnd_U2_Pnd_Ui", "#UI", FieldType.INTEGER, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.STRING, 1);

        pnd_Tt = localVariables.newGroupInRecord("pnd_Tt", "#TT");
        pnd_Tt_Pnd_Tt_0 = pnd_Tt.newFieldInGroup("pnd_Tt_Pnd_Tt_0", "#TT-0", FieldType.STRING, 16);
        pnd_Tt_Pnd_Tt_1 = pnd_Tt.newFieldInGroup("pnd_Tt_Pnd_Tt_1", "#TT-1", FieldType.STRING, 16);
        pnd_Tt_Pnd_Tt_2 = pnd_Tt.newFieldInGroup("pnd_Tt_Pnd_Tt_2", "#TT-2", FieldType.STRING, 16);
        pnd_Tt_Pnd_Tt_3 = pnd_Tt.newFieldInGroup("pnd_Tt_Pnd_Tt_3", "#TT-3", FieldType.STRING, 16);
        pnd_Tt_Pnd_Tt_4 = pnd_Tt.newFieldInGroup("pnd_Tt_Pnd_Tt_4", "#TT-4", FieldType.STRING, 16);
        pnd_Tt_Pnd_Tt_5 = pnd_Tt.newFieldInGroup("pnd_Tt_Pnd_Tt_5", "#TT-5", FieldType.STRING, 16);
        pnd_Tt_Pnd_Tt_6 = pnd_Tt.newFieldInGroup("pnd_Tt_Pnd_Tt_6", "#TT-6", FieldType.STRING, 16);
        pnd_Tt_Pnd_Tt_7 = pnd_Tt.newFieldInGroup("pnd_Tt_Pnd_Tt_7", "#TT-7", FieldType.STRING, 16);
        pnd_Tt_Pnd_Tt_8 = pnd_Tt.newFieldInGroup("pnd_Tt_Pnd_Tt_8", "#TT-8", FieldType.STRING, 16);
        pnd_Tt_Pnd_Tt_9 = pnd_Tt.newFieldInGroup("pnd_Tt_Pnd_Tt_9", "#TT-9", FieldType.STRING, 16);
        pnd_Tt_Pnd_Tt_A = pnd_Tt.newFieldInGroup("pnd_Tt_Pnd_Tt_A", "#TT-A", FieldType.STRING, 16);
        pnd_Tt_Pnd_Tt_B = pnd_Tt.newFieldInGroup("pnd_Tt_Pnd_Tt_B", "#TT-B", FieldType.STRING, 16);
        pnd_Tt_Pnd_Tt_C = pnd_Tt.newFieldInGroup("pnd_Tt_Pnd_Tt_C", "#TT-C", FieldType.STRING, 16);
        pnd_Tt_Pnd_Tt_D = pnd_Tt.newFieldInGroup("pnd_Tt_Pnd_Tt_D", "#TT-D", FieldType.STRING, 16);
        pnd_Tt_Pnd_Tt_E = pnd_Tt.newFieldInGroup("pnd_Tt_Pnd_Tt_E", "#TT-E", FieldType.STRING, 16);
        pnd_Tt_Pnd_Tt_F = pnd_Tt.newFieldInGroup("pnd_Tt_Pnd_Tt_F", "#TT-F", FieldType.STRING, 16);

        pnd_Tt__R_Field_4 = localVariables.newGroupInRecord("pnd_Tt__R_Field_4", "REDEFINE", pnd_Tt);
        pnd_Tt_Pnd_Ttc = pnd_Tt__R_Field_4.newFieldArrayInGroup("pnd_Tt_Pnd_Ttc", "#TTC", FieldType.STRING, 1, new DbsArrayController(1, 256));

        pnd_Tt__R_Field_5 = localVariables.newGroupInRecord("pnd_Tt__R_Field_5", "REDEFINE", pnd_Tt);
        pnd_Tt_Pnd_Tti = pnd_Tt__R_Field_5.newFieldArrayInGroup("pnd_Tt_Pnd_Tti", "#TTI", FieldType.STRING, 16, new DbsArrayController(1, 16));
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Del_Cnt = localVariables.newFieldInRecord("pnd_Del_Cnt", "#DEL-CNT", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Tt_Pnd_Tt_0.setInitialValue("H'0102030405060708090A0B0C0D0E0F'");
        pnd_Tt_Pnd_Tt_1.setInitialValue("H'1112131415161718191A1B1C1D1E1F'");
        pnd_Tt_Pnd_Tt_2.setInitialValue("H'2122232425262728292A2B2C2D2E2F'");
        pnd_Tt_Pnd_Tt_3.setInitialValue("H'3132333435363738393A3B3C3D3E3F'");
        pnd_Tt_Pnd_Tt_4.setInitialValue("H'4142434445464748494A4B4C4D4E4F'");
        pnd_Tt_Pnd_Tt_5.setInitialValue("H'5152535455565758595A5B5C5D5E5F'");
        pnd_Tt_Pnd_Tt_6.setInitialValue("H'6162636465666768696A6B6C6D6E6F'");
        pnd_Tt_Pnd_Tt_7.setInitialValue("H'7172737475767778797A7B7C7D7E7F'");
        pnd_Tt_Pnd_Tt_8.setInitialValue("H'8182838485868788898A8B8C8D8E8F'");
        pnd_Tt_Pnd_Tt_9.setInitialValue("H'9192939495969798999A9B9C9D9E9F'");
        pnd_Tt_Pnd_Tt_A.setInitialValue("H'A1A2A3A4A5A6A7A8A9AAABACADAEAF'");
        pnd_Tt_Pnd_Tt_B.setInitialValue("H'B1B2B3B4B5B6B7B8B9BABBBCBDBEBF'");
        pnd_Tt_Pnd_Tt_C.setInitialValue("H'818283848586878889CACBCCCDCECF'");
        pnd_Tt_Pnd_Tt_D.setInitialValue("H'919293949596979899DADBDCDDDEDF'");
        pnd_Tt_Pnd_Tt_E.setInitialValue("H'A1A2A3A4A5A6A7A8A9EAEBECEDEEEF'");
        pnd_Tt_Pnd_Tt_F.setInitialValue("H'F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF'");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Appn100() throws Exception
    {
        super("Appn100");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_U2_Pnd_Ui.reset();                                                                                                                                            //Natural: RESET #UI
        FOR01:                                                                                                                                                            //Natural: FOR #A 1 #PAR-PARAMETER-COUNT
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Parameter_Count())); pnd_A.nadd(1))
        {
            pnd_Name.setValue(pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Parameter_Data().getValue(pnd_A));                                                             //Natural: MOVE #PAR-PARAMETER-DATA ( #A ) TO #NAME
            FOR02:                                                                                                                                                        //Natural: FOR #J 2 TO 35
            for (pnd_J.setValue(2); condition(pnd_J.lessOrEqual(35)); pnd_J.nadd(1))
            {
                //*  IDENTIFIES THE CITY/STATE FIELD
                if (condition(pnd_A.equals(pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Parameter_Count())))                                                              //Natural: IF #A EQ #PAR-PARAMETER-COUNT
                {
                    //*  BEGINING POSITION OF STATE
                    if (condition(pnd_J.equals(pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_State_Field())))                                                              //Natural: IF #J EQ #PAR-STATE-FIELD
                    {
                        pnd_J.nadd(3);                                                                                                                                    //Natural: ADD 3 TO #J
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Name_Pnd_Name_C.getValue(pnd_J).equals(" ") || pnd_Name_Pnd_Name_C.getValue(pnd_J).equals("-") || pnd_Name_Pnd_Name_C.getValue(pnd_J).equals("/")  //Natural: IF #NAME-C ( #J ) = ' ' OR = '-' OR = '/' OR = '.'
                    || pnd_Name_Pnd_Name_C.getValue(pnd_J).equals(".")))
                {
                    pnd_J.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #J
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Name_Pnd_Name_C.getValue(pnd_J).equals("0")))                                                                                           //Natural: IF #NAME-C ( #J ) = '0'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_U2_Pnd_U2ir.setValue(pnd_Name_Pnd_Name_C.getValue(pnd_J));                                                                                            //Natural: MOVE #NAME-C ( #J ) TO #U2IR
                pnd_Name_Pnd_Name_C.getValue(pnd_J).setValue(pnd_Tt_Pnd_Ttc.getValue(pnd_U2_Pnd_Ui));                                                                     //Natural: MOVE #TTC ( #UI ) TO #NAME-C ( #J )
                pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Parameter_Data().getValue(pnd_A).setValue(pnd_Name);                                                         //Natural: MOVE #NAME TO #PAR-PARAMETER-DATA ( #A )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //
}
