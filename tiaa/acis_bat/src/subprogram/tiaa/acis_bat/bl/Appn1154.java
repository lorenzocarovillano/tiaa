/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:58:54 PM
**        * FROM NATURAL SUBPROGRAM : Appn1154
************************************************************
**        * FILE NAME            : Appn1154.java
**        * CLASS NAME           : Appn1154
**        * INSTANCE NAME        : Appn1154
************************************************************
************************************************************************
*  PROGRAM-ID  :  APPN1154 (COPY OF APPB105)                           *
*  DESCRIPTION :  FORMAT BENEFICIARY INFORMATION FOR DIALOGUE          *
*  CALLED BY   :  APPB1152                                             *
*  UPDATED     :  07/28/08 DEVELBISS - PROGRAM CREATED                 *
*                                                                      *
* MOD DATE   MOD BY   DESCRIPTION OF CHANGES                           *
**-------- ---------- ------------------------------------------------**
* 07/23/10 DEVELBISS  MAKE CORRECTIONS TO BENE CATEGORY - BJD1         *
* 12/27/11 BERGHEISER PASS SPECIAL BENE TEXT INDICATORS - JRB1         *
* 01/31/11 BERGHEISER COMMENT OUT DISPLAYS                             *
* 07/19/12 ELLO       CHANGE DESCRIPTION IN THE WELCOME PACKAGE FOR    *
*                     TO 'DEFAULT' WHEN THE BENEFICARY IS DEFAULTED    *
*                     TO PLAN/PRODUCT PROVISIONS.  SEE  CHG256421      *
* 06/14/14 L SHU      USING NEW PDA APPA1154 FOR IRA SUBSTITUTION      *
*                     INCREASE THE OCCURENCES OF SPCL-DSGN-TEXT TO 60  *
*                     TNGSUB                                           *
* 08/23/13 RJF        TNG CHANGES TO CALLING PROGRAM EXPECT THE PRIMARY*
*                     AND CONTINGENT BENEFICIARIES TO BE RETURNED IN   *
*                     SEPARATE GROUPS. ONE ARRAY FOR PRIMARY ONE ARRAY *
*                     FOR CONTINGENT. THIS CHANGE MAKES THAT HAPPEN.   *
*                     SEE CHANGE TAG C291829                           *
* 02/01/16 ELLO       MOVE BENE-EXTENDED-NAME AS PART OF THE BENE-NAME *
*                     INFO TO SEND TO DIALOGUE  (BNL2)                 *
* 03/19/16 SHU        MOVE CONTIGENT BENE NAME LINE 2 TO BENE-NAME     *
*                     INFO TO SEND TO DIALOGUE  (BNL2V2)               *
* 06/22/17 BABRE      PIN EXPANSION CHANGES. CHG425939         PINE    *
**--------------------------------------------------------------------**
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appn1154 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAppa1154 pdaAppa1154;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_acis_Bene_File_View;
    private DbsField acis_Bene_File_View_Bene_Sync_Ind;
    private DbsField acis_Bene_File_View_Bene_Tiaa_Nbr;
    private DbsField acis_Bene_File_View_Bene_Cref_Nbr;
    private DbsField acis_Bene_File_View_Bene_Orign_System;
    private DbsField acis_Bene_File_View_Bene_Pin_Nbr;
    private DbsField acis_Bene_File_View_Bene_Lob;
    private DbsField acis_Bene_File_View_Bene_Lob_Type;
    private DbsField acis_Bene_File_View_Bene_Part_Prfx;
    private DbsField acis_Bene_File_View_Bene_Part_Sffx;
    private DbsField acis_Bene_File_View_Bene_Part_First_Nme;
    private DbsField acis_Bene_File_View_Bene_Part_Middle_Nme;
    private DbsField acis_Bene_File_View_Bene_Part_Last_Nme;
    private DbsField acis_Bene_File_View_Bene_Part_Ssn;
    private DbsField acis_Bene_File_View_Bene_Part_Dob;

    private DbsGroup acis_Bene_File_View__R_Field_1;
    private DbsField acis_Bene_File_View_Bene_Part_Dob_A;
    private DbsField acis_Bene_File_View_Bene_Estate;
    private DbsField acis_Bene_File_View_Bene_Trust;
    private DbsField acis_Bene_File_View_Bene_Category;
    private DbsField acis_Bene_File_View_Bene_Effective_Dt;
    private DbsField acis_Bene_File_View_Bene_Mos_Ind;
    private DbsField acis_Bene_File_View_Count_Castbene_Data;
    private DbsGroup acis_Bene_File_View_Count_Castbene_Spcl_TxtMuGroup;
    private DbsField acis_Bene_File_View_Count_Castbene_Spcl_Txt;

    private DbsGroup acis_Bene_File_View_Bene_Data;
    private DbsField acis_Bene_File_View_Bene_Type;
    private DbsField acis_Bene_File_View_Bene_Name;
    private DbsField acis_Bene_File_View_Bene_Extended_Name;
    private DbsField acis_Bene_File_View_Bene_Ssn_Nbr;
    private DbsField acis_Bene_File_View_Bene_Dob;
    private DbsField acis_Bene_File_View_Bene_Relationship;
    private DbsField acis_Bene_File_View_Bene_Relationship_Cde;
    private DbsField acis_Bene_File_View_Bene_Alloc_Pct;
    private DbsField acis_Bene_File_View_Bene_Nmrtr_Nbr;
    private DbsField acis_Bene_File_View_Bene_Dnmntr_Nbr;
    private DbsField acis_Bene_File_View_Bene_Std_Txt_Ind;
    private DbsGroup acis_Bene_File_View_Bene_Spcl_TxtMuGroup;
    private DbsField acis_Bene_File_View_Bene_Spcl_Txt;
    private DbsGroup acis_Bene_File_View_Count_Castbene_Spcl_Dsgn_TxtMuGroup;
    private DbsField acis_Bene_File_View_Count_Castbene_Spcl_Dsgn_Txt;
    private DbsGroup acis_Bene_File_View_Bene_Spcl_Dsgn_TxtMuGroup;
    private DbsField acis_Bene_File_View_Bene_Spcl_Dsgn_Txt;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_P;
    private DbsField pnd_C;
    private DbsField pnd_Isn;
    private DbsField pnd_Found;
    private DbsField pnd_Debug;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaAppa1154 = new PdaAppa1154(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_acis_Bene_File_View = new DataAccessProgramView(new NameInfo("vw_acis_Bene_File_View", "ACIS-BENE-FILE-VIEW"), "ACIS_BENE_FILE_12", "ACIS_BENE_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("ACIS_BENE_FILE_12"));
        acis_Bene_File_View_Bene_Sync_Ind = vw_acis_Bene_File_View.getRecord().newFieldInGroup("acis_Bene_File_View_Bene_Sync_Ind", "BENE-SYNC-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BENE_SYNC_IND");
        acis_Bene_File_View_Bene_Tiaa_Nbr = vw_acis_Bene_File_View.getRecord().newFieldInGroup("acis_Bene_File_View_Bene_Tiaa_Nbr", "BENE-TIAA-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "BENE_TIAA_NBR");
        acis_Bene_File_View_Bene_Cref_Nbr = vw_acis_Bene_File_View.getRecord().newFieldInGroup("acis_Bene_File_View_Bene_Cref_Nbr", "BENE-CREF-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "BENE_CREF_NBR");
        acis_Bene_File_View_Bene_Orign_System = vw_acis_Bene_File_View.getRecord().newFieldInGroup("acis_Bene_File_View_Bene_Orign_System", "BENE-ORIGN-SYSTEM", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "BENE_ORIGN_SYSTEM");
        acis_Bene_File_View_Bene_Pin_Nbr = vw_acis_Bene_File_View.getRecord().newFieldInGroup("acis_Bene_File_View_Bene_Pin_Nbr", "BENE-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "BENE_PIN_NBR");
        acis_Bene_File_View_Bene_Lob = vw_acis_Bene_File_View.getRecord().newFieldInGroup("acis_Bene_File_View_Bene_Lob", "BENE-LOB", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BENE_LOB");
        acis_Bene_File_View_Bene_Lob_Type = vw_acis_Bene_File_View.getRecord().newFieldInGroup("acis_Bene_File_View_Bene_Lob_Type", "BENE-LOB-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BENE_LOB_TYPE");
        acis_Bene_File_View_Bene_Part_Prfx = vw_acis_Bene_File_View.getRecord().newFieldInGroup("acis_Bene_File_View_Bene_Part_Prfx", "BENE-PART-PRFX", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "BENE_PART_PRFX");
        acis_Bene_File_View_Bene_Part_Sffx = vw_acis_Bene_File_View.getRecord().newFieldInGroup("acis_Bene_File_View_Bene_Part_Sffx", "BENE-PART-SFFX", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "BENE_PART_SFFX");
        acis_Bene_File_View_Bene_Part_First_Nme = vw_acis_Bene_File_View.getRecord().newFieldInGroup("acis_Bene_File_View_Bene_Part_First_Nme", "BENE-PART-FIRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "BENE_PART_FIRST_NME");
        acis_Bene_File_View_Bene_Part_Middle_Nme = vw_acis_Bene_File_View.getRecord().newFieldInGroup("acis_Bene_File_View_Bene_Part_Middle_Nme", "BENE-PART-MIDDLE-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "BENE_PART_MIDDLE_NME");
        acis_Bene_File_View_Bene_Part_Last_Nme = vw_acis_Bene_File_View.getRecord().newFieldInGroup("acis_Bene_File_View_Bene_Part_Last_Nme", "BENE-PART-LAST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "BENE_PART_LAST_NME");
        acis_Bene_File_View_Bene_Part_Ssn = vw_acis_Bene_File_View.getRecord().newFieldInGroup("acis_Bene_File_View_Bene_Part_Ssn", "BENE-PART-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "BENE_PART_SSN");
        acis_Bene_File_View_Bene_Part_Dob = vw_acis_Bene_File_View.getRecord().newFieldInGroup("acis_Bene_File_View_Bene_Part_Dob", "BENE-PART-DOB", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "BENE_PART_DOB");

        acis_Bene_File_View__R_Field_1 = vw_acis_Bene_File_View.getRecord().newGroupInGroup("acis_Bene_File_View__R_Field_1", "REDEFINE", acis_Bene_File_View_Bene_Part_Dob);
        acis_Bene_File_View_Bene_Part_Dob_A = acis_Bene_File_View__R_Field_1.newFieldInGroup("acis_Bene_File_View_Bene_Part_Dob_A", "BENE-PART-DOB-A", 
            FieldType.STRING, 8);
        acis_Bene_File_View_Bene_Estate = vw_acis_Bene_File_View.getRecord().newFieldInGroup("acis_Bene_File_View_Bene_Estate", "BENE-ESTATE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BENE_ESTATE");
        acis_Bene_File_View_Bene_Trust = vw_acis_Bene_File_View.getRecord().newFieldInGroup("acis_Bene_File_View_Bene_Trust", "BENE-TRUST", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BENE_TRUST");
        acis_Bene_File_View_Bene_Category = vw_acis_Bene_File_View.getRecord().newFieldInGroup("acis_Bene_File_View_Bene_Category", "BENE-CATEGORY", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BENE_CATEGORY");
        acis_Bene_File_View_Bene_Effective_Dt = vw_acis_Bene_File_View.getRecord().newFieldInGroup("acis_Bene_File_View_Bene_Effective_Dt", "BENE-EFFECTIVE-DT", 
            FieldType.DATE, RepeatingFieldStrategy.None, "BENE_EFFECTIVE_DT");
        acis_Bene_File_View_Bene_Mos_Ind = vw_acis_Bene_File_View.getRecord().newFieldInGroup("acis_Bene_File_View_Bene_Mos_Ind", "BENE-MOS-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BENE_MOS_IND");
        acis_Bene_File_View_Count_Castbene_Data = vw_acis_Bene_File_View.getRecord().newFieldInGroup("acis_Bene_File_View_Count_Castbene_Data", "C*BENE-DATA", 
            RepeatingFieldStrategy.CAsteriskVariable, "ACIS_BENE_FILE_BENE_DATA");
        acis_Bene_File_View_Count_Castbene_Spcl_Txt = vw_acis_Bene_File_View.getRecord().newFieldArrayInGroup("acis_Bene_File_View_Count_Castbene_Spcl_Txt", 
            "C*BENE-SPCL-TXT", new DbsArrayController(1, 20), RepeatingFieldStrategy.PeriodicGroupSubTableCAsteriskVariable, "ACIS_BENE_FILE_BENE_SPCL_TXT", 
            "ACIS_BENE_FILE_BENE_DATA");

        acis_Bene_File_View_Bene_Data = vw_acis_Bene_File_View.getRecord().newGroupInGroup("acis_Bene_File_View_Bene_Data", "BENE-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_BENE_FILE_BENE_DATA");
        acis_Bene_File_View_Bene_Type = acis_Bene_File_View_Bene_Data.newFieldArrayInGroup("acis_Bene_File_View_Bene_Type", "BENE-TYPE", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BENE_TYPE", "ACIS_BENE_FILE_BENE_DATA");
        acis_Bene_File_View_Bene_Name = acis_Bene_File_View_Bene_Data.newFieldArrayInGroup("acis_Bene_File_View_Bene_Name", "BENE-NAME", FieldType.STRING, 
            35, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BENE_NAME", "ACIS_BENE_FILE_BENE_DATA");
        acis_Bene_File_View_Bene_Extended_Name = acis_Bene_File_View_Bene_Data.newFieldArrayInGroup("acis_Bene_File_View_Bene_Extended_Name", "BENE-EXTENDED-NAME", 
            FieldType.STRING, 35, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BENE_EXTENDED_NAME", "ACIS_BENE_FILE_BENE_DATA");
        acis_Bene_File_View_Bene_Ssn_Nbr = acis_Bene_File_View_Bene_Data.newFieldArrayInGroup("acis_Bene_File_View_Bene_Ssn_Nbr", "BENE-SSN-NBR", FieldType.NUMERIC, 
            9, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BENE_SSN_NBR", "ACIS_BENE_FILE_BENE_DATA");
        acis_Bene_File_View_Bene_Dob = acis_Bene_File_View_Bene_Data.newFieldArrayInGroup("acis_Bene_File_View_Bene_Dob", "BENE-DOB", FieldType.NUMERIC, 
            8, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BENE_DOB", "ACIS_BENE_FILE_BENE_DATA");
        acis_Bene_File_View_Bene_Relationship = acis_Bene_File_View_Bene_Data.newFieldArrayInGroup("acis_Bene_File_View_Bene_Relationship", "BENE-RELATIONSHIP", 
            FieldType.STRING, 15, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BENE_RELATIONSHIP", "ACIS_BENE_FILE_BENE_DATA");
        acis_Bene_File_View_Bene_Relationship_Cde = acis_Bene_File_View_Bene_Data.newFieldArrayInGroup("acis_Bene_File_View_Bene_Relationship_Cde", "BENE-RELATIONSHIP-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BENE_RELATIONSHIP_CDE", "ACIS_BENE_FILE_BENE_DATA");
        acis_Bene_File_View_Bene_Alloc_Pct = acis_Bene_File_View_Bene_Data.newFieldArrayInGroup("acis_Bene_File_View_Bene_Alloc_Pct", "BENE-ALLOC-PCT", 
            FieldType.NUMERIC, 5, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BENE_ALLOC_PCT", "ACIS_BENE_FILE_BENE_DATA");
        acis_Bene_File_View_Bene_Nmrtr_Nbr = acis_Bene_File_View_Bene_Data.newFieldArrayInGroup("acis_Bene_File_View_Bene_Nmrtr_Nbr", "BENE-NMRTR-NBR", 
            FieldType.NUMERIC, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BENE_NMRTR_NBR", "ACIS_BENE_FILE_BENE_DATA");
        acis_Bene_File_View_Bene_Dnmntr_Nbr = acis_Bene_File_View_Bene_Data.newFieldArrayInGroup("acis_Bene_File_View_Bene_Dnmntr_Nbr", "BENE-DNMNTR-NBR", 
            FieldType.NUMERIC, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BENE_DNMNTR_NBR", "ACIS_BENE_FILE_BENE_DATA");
        acis_Bene_File_View_Bene_Std_Txt_Ind = acis_Bene_File_View_Bene_Data.newFieldArrayInGroup("acis_Bene_File_View_Bene_Std_Txt_Ind", "BENE-STD-TXT-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BENE_STD_TXT_IND", "ACIS_BENE_FILE_BENE_DATA");
        acis_Bene_File_View_Bene_Spcl_TxtMuGroup = acis_Bene_File_View_Bene_Data.newGroupInGroup("ACIS_BENE_FILE_VIEW_BENE_SPCL_TXTMuGroup", "BENE_SPCL_TXTMuGroup", 
            RepeatingFieldStrategy.PeriodicGroupSubTableFieldArray, "ACIS_BENE_FILE_BENE_SPCL_TXT", "ACIS_BENE_FILE_BENE_DATA");
        acis_Bene_File_View_Bene_Spcl_Txt = acis_Bene_File_View_Bene_Spcl_TxtMuGroup.newFieldArrayInGroup("acis_Bene_File_View_Bene_Spcl_Txt", "BENE-SPCL-TXT", 
            FieldType.STRING, 72, new DbsArrayController(1, 20, 1, 3) , RepeatingFieldStrategy.SubTableFieldArray, "BENE_SPCL_TXT", "ACIS_BENE_FILE_BENE_DATA");
        acis_Bene_File_View_Count_Castbene_Spcl_Dsgn_Txt = vw_acis_Bene_File_View.getRecord().newFieldInGroup("acis_Bene_File_View_Count_Castbene_Spcl_Dsgn_Txt", 
            "C*BENE-SPCL-DSGN-TXT", RepeatingFieldStrategy.CAsteriskVariable, "ACIS_BENE_FILE_BENE_SPCL_DSGN_TXT");
        acis_Bene_File_View_Bene_Spcl_Dsgn_TxtMuGroup = vw_acis_Bene_File_View.getRecord().newGroupInGroup("ACIS_BENE_FILE_VIEW_BENE_SPCL_DSGN_TXTMuGroup", 
            "BENE_SPCL_DSGN_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "ACIS_BENE_FILE_BENE_SPCL_DSGN_TXT");
        acis_Bene_File_View_Bene_Spcl_Dsgn_Txt = acis_Bene_File_View_Bene_Spcl_Dsgn_TxtMuGroup.newFieldArrayInGroup("acis_Bene_File_View_Bene_Spcl_Dsgn_Txt", 
            "BENE-SPCL-DSGN-TXT", FieldType.STRING, 72, new DbsArrayController(1, 60), RepeatingFieldStrategy.SubTableFieldArray, "BENE_SPCL_DSGN_TXT");
        registerRecord(vw_acis_Bene_File_View);

        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 2);
        pnd_P = localVariables.newFieldInRecord("pnd_P", "#P", FieldType.PACKED_DECIMAL, 2);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.PACKED_DECIMAL, 2);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.BOOLEAN, 1);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_acis_Bene_File_View.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Appn1154() throws Exception
    {
        super("Appn1154");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*    MOVE TRUE TO #DEBUG
        //* ***********************************************************************
        //*  MAIN LOGIC SECTION                                                   *
        //* ***********************************************************************
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM(),"(0890) START OF MAINLINE ...");                                                                                    //Natural: WRITE *PROGRAM '(0890) START OF MAINLINE ...'
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Tiaa_Nbr());                                                                                   //Natural: WRITE '=' #BENE-TIAA-NBR
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cref_Nbr());                                                                                   //Natural: WRITE '=' #BENE-CREF-NBR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  C291829
        pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Data().getValue("*").reset();                                                                                          //Natural: RESET #BENE-PRMRY-DATA ( * ) #BENE-CNTGNT-DATA ( * ) #ISN #FOUND
        pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Data().getValue("*").reset();
        pnd_Isn.reset();
        pnd_Found.reset();
        if (condition(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Tiaa_Nbr().notEquals(" ")))                                                                                    //Natural: IF #BENE-TIAA-NBR NE ' '
        {
            vw_acis_Bene_File_View.startDatabaseRead                                                                                                                      //Natural: READ ( 1 ) ACIS-BENE-FILE-VIEW WITH BENE-TIAA-NBR = #BENE-TIAA-NBR
            (
            "PND_PND_L1010",
            new Wc[] { new Wc("BENE_TIAA_NBR", ">=", pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Tiaa_Nbr(), WcType.BY) },
            new Oc[] { new Oc("BENE_TIAA_NBR", "ASC") },
            1
            );
            PND_PND_L1010:
            while (condition(vw_acis_Bene_File_View.readNextRow("PND_PND_L1010")))
            {
                if (condition(acis_Bene_File_View_Bene_Tiaa_Nbr.equals(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Tiaa_Nbr())))                                                 //Natural: IF BENE-TIAA-NBR = #BENE-TIAA-NBR
                {
                    pnd_Found.setValue(true);                                                                                                                             //Natural: MOVE TRUE TO #FOUND
                    pnd_Isn.setValue(vw_acis_Bene_File_View.getAstISN("PND_PND_L1010"));                                                                                  //Natural: MOVE *ISN ( ##L1010. ) TO #ISN
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Found.getBoolean()) && pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cref_Nbr().notEquals(" ")))                                                      //Natural: IF NOT #FOUND AND #BENE-CREF-NBR NE ' '
        {
            vw_acis_Bene_File_View.startDatabaseRead                                                                                                                      //Natural: READ ( 1 ) ACIS-BENE-FILE-VIEW WITH BENE-CREF-NBR = #BENE-CREF-NBR
            (
            "PND_PND_L1110",
            new Wc[] { new Wc("BENE_CREF_NBR", ">=", pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cref_Nbr(), WcType.BY) },
            new Oc[] { new Oc("BENE_CREF_NBR", "ASC") },
            1
            );
            PND_PND_L1110:
            while (condition(vw_acis_Bene_File_View.readNextRow("PND_PND_L1110")))
            {
                if (condition(acis_Bene_File_View_Bene_Cref_Nbr.equals(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cref_Nbr())))                                                 //Natural: IF BENE-CREF-NBR = #BENE-CREF-NBR
                {
                    pnd_Found.setValue(true);                                                                                                                             //Natural: MOVE TRUE TO #FOUND
                    pnd_Isn.setValue(vw_acis_Bene_File_View.getAstISN("PND_PND_L1110"));                                                                                  //Natural: MOVE *ISN ( ##L1110. ) TO #ISN
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Found.getBoolean())))                                                                                                                        //Natural: IF NOT #FOUND
        {
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Msg().setValue(DbsUtil.compress(Global.getPROGRAM(), "(1160)", "- RECORD NOT FOUND", pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Tiaa_Nbr(),  //Natural: COMPRESS *PROGRAM '(1160)' '- RECORD NOT FOUND' #BENE-TIAA-NBR #BENE-CREF-NBR INTO #BENE-MSG
                pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cref_Nbr()));
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Return_Code().setValue(99);                                                                                              //Natural: ASSIGN #BENE-RETURN-CODE := 99
            getReports().write(0, "=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Msg());                                                                                        //Natural: WRITE '=' #BENE-MSG
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Return_Code());                                                                                //Natural: WRITE '=' #BENE-RETURN-CODE
            if (Global.isEscape()) return;
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        GET01:                                                                                                                                                            //Natural: GET ACIS-BENE-FILE-VIEW #ISN
        vw_acis_Bene_File_View.readByID(pnd_Isn.getLong(), "GET01");
                                                                                                                                                                          //Natural: PERFORM RTN-2-FORMAT-BENE-INFO
        sub_Rtn_2_Format_Bene_Info();
        if (condition(Global.isEscape())) {return;}
        if (condition(acis_Bene_File_View_Bene_Category.equals("1") && acis_Bene_File_View_Bene_Estate.equals("N") && acis_Bene_File_View_Bene_Trust.equals("N")))        //Natural: IF BENE-CATEGORY = '1' AND BENE-ESTATE = 'N' AND BENE-TRUST = 'N'
        {
            //*  TNGSUB
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Cnt().nadd(1);                                                                                                     //Natural: ADD 1 TO #BENE-PRMRY-CNT
            //*  C291829 /* TNGSUB
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Type().getValue(1).setValue("P");                                                                                  //Natural: MOVE 'P' TO #BENE-PRMRY-TYPE ( 1 )
            //*  C291829 /* TNGSUB
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Name().getValue(1).setValue("NONE");                                                                               //Natural: MOVE 'NONE' TO #BENE-PRMRY-NAME ( 1 )
            //*  TNGSUB
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Cnt().nadd(1);                                                                                                    //Natural: ADD 1 TO #BENE-CNTGNT-CNT
            //*  C291829 /* TNGSUB
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Type().getValue(1).setValue("C");                                                                                 //Natural: MOVE 'C' TO #BENE-CNTGNT-TYPE ( 1 )
            //*  C291829 /* TNGSUB
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Name().getValue(1).setValue("NONE");                                                                              //Natural: MOVE 'NONE' TO #BENE-CNTGNT-NAME ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(acis_Bene_File_View_Bene_Category.equals("1") && acis_Bene_File_View_Bene_Estate.equals("Y")))                                                      //Natural: IF BENE-CATEGORY = '1' AND BENE-ESTATE = 'Y'
        {
            //*  TNGSUB
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Cnt().nadd(1);                                                                                                     //Natural: ADD 1 TO #BENE-PRMRY-CNT
            //*  C291829 /* TNGSUB
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Name().getValue(1).setValue("ESTATE");                                                                             //Natural: MOVE 'ESTATE' TO #BENE-PRMRY-NAME ( 1 )
            //*  C291829
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Type().getValue(1).setValue("P");                                                                                  //Natural: MOVE 'P' TO #BENE-PRMRY-TYPE ( 1 )
            //*  BJD1
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Category().setValue(" ");                                                                                                //Natural: MOVE ' ' TO #BENE-CATEGORY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(acis_Bene_File_View_Bene_Category.equals("1") && acis_Bene_File_View_Bene_Trust.equals("Y")))                                                       //Natural: IF BENE-CATEGORY = '1' AND BENE-TRUST = 'Y'
        {
            //*  TNGSUB
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Cnt().nadd(1);                                                                                                     //Natural: ADD 1 TO #BENE-PRMRY-CNT
            //*  C291829 /* TNGSUB
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Name().getValue(1).setValue("TRUST");                                                                              //Natural: MOVE 'TRUST' TO #BENE-PRMRY-NAME ( 1 )
            //*  C291829
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Type().getValue(1).setValue("P");                                                                                  //Natural: MOVE 'P' TO #BENE-PRMRY-TYPE ( 1 )
            //*  BJD1
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Category().setValue(" ");                                                                                                //Natural: MOVE ' ' TO #BENE-CATEGORY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
                                                                                                                                                                          //Natural: PERFORM RTN-99-DEBUG
            sub_Rtn_99_Debug();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-2-FORMAT-BENE-INFO
        //*  #BENE-CATEGORY        :=  BENE-MOS-IND
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-3A-PRMRY-STANDARD
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-3B-PRMRY-TEXT
        //* *--------------------------------------------------------------------**
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-3C-FORMAT-STANDARD
        //*   CHG256421 ENDS HERE.
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-4A-CNTGNT-STANDARD
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-4B-CNTGNT-TEXT
        //* *--------------------------------------------------------------------**
        //*   START OF C291829 - PARAGRAPH TO LOAD CONTINGENT BENE ARRAY
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-4C-FORMAT-STANDARD
        //*   END   OF C291829 - PARAGRAPH TO LOAD CONTINGENT BENE ARRAY
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-99-DEBUG
    }
    private void sub_Rtn_2_Format_Bene_Info() throws Exception                                                                                                            //Natural: RTN-2-FORMAT-BENE-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        //*  BJD1
        if (condition(acis_Bene_File_View_Bene_Category.equals("4")))                                                                                                     //Natural: IF BENE-CATEGORY EQ '4'
        {
            //*  BJD1
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Category().setValue(" ");                                                                                                //Natural: MOVE ' ' TO #BENE-CATEGORY
            //*  BJD1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  BJD1
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Category().setValue(acis_Bene_File_View_Bene_Category);                                                                  //Natural: MOVE BENE-CATEGORY TO #BENE-CATEGORY
            //*  TNGSUB2
        }                                                                                                                                                                 //Natural: END-IF
        pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Estate_As_Bene().setValue(acis_Bene_File_View_Bene_Estate);                                                                  //Natural: ASSIGN #BENE-ESTATE-AS-BENE := BENE-ESTATE
        pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Trust_As_Bene().setValue(acis_Bene_File_View_Bene_Trust);                                                                    //Natural: ASSIGN #BENE-TRUST-AS-BENE := BENE-TRUST
        pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Mos_Ind().setValue(acis_Bene_File_View_Bene_Mos_Ind);                                                                        //Natural: ASSIGN #BENE-MOS-IND := BENE-MOS-IND
        short decideConditionsMet281 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF BENE-MOS-IND;//Natural: VALUE 'Y'
        if (condition((acis_Bene_File_View_Bene_Mos_Ind.equals("Y"))))
        {
            decideConditionsMet281++;
            //*  TNGSUB
            //*  TNGSUB
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Spcl_Dsgn_Txt().getValue("*").setValue(acis_Bene_File_View_Bene_Spcl_Dsgn_Txt.getValue("*"));                            //Natural: MOVE BENE-SPCL-DSGN-TXT ( * ) TO #BENE-SPCL-DSGN-TXT ( * )
            FOR01:                                                                                                                                                        //Natural: FOR #I = 60 1 -1
            for (pnd_I.setValue(60); condition(pnd_I.greaterOrEqual(1)); pnd_I.nsubtract(1))
            {
                if (condition(acis_Bene_File_View_Bene_Spcl_Dsgn_Txt.getValue(pnd_I).notEquals(" ")))                                                                     //Natural: IF BENE-SPCL-DSGN-TXT ( #I ) NE ' '
                {
                    //*  TNGSUB
                    pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Cnt().setValue(pnd_I);                                                                                     //Natural: MOVE #I TO #BENE-PRMRY-CNT
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            //*  C291829
            pnd_P.reset();                                                                                                                                                //Natural: RESET #P #C
            pnd_C.reset();
            FOR02:                                                                                                                                                        //Natural: FOR #I = 1 C*BENE-DATA
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(acis_Bene_File_View_Count_Castbene_Data)); pnd_I.nadd(1))
            {
                //*  PRIMARY
                short decideConditionsMet298 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF BENE-TYPE ( #I );//Natural: VALUE 'P'
                if (condition((acis_Bene_File_View_Bene_Type.getValue(pnd_I).equals("P"))))
                {
                    decideConditionsMet298++;
                    short decideConditionsMet300 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF BENE-STD-TXT-IND ( #I );//Natural: VALUE 'S'
                    if (condition((acis_Bene_File_View_Bene_Std_Txt_Ind.getValue(pnd_I).equals("S"))))
                    {
                        decideConditionsMet300++;
                                                                                                                                                                          //Natural: PERFORM RTN-3A-PRMRY-STANDARD
                        sub_Rtn_3a_Prmry_Standard();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: VALUE 'T'
                    else if (condition((acis_Bene_File_View_Bene_Std_Txt_Ind.getValue(pnd_I).equals("T"))))
                    {
                        decideConditionsMet300++;
                                                                                                                                                                          //Natural: PERFORM RTN-3B-PRMRY-TEXT
                        sub_Rtn_3b_Prmry_Text();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: VALUE 'B'
                    else if (condition((acis_Bene_File_View_Bene_Std_Txt_Ind.getValue(pnd_I).equals("B"))))
                    {
                        decideConditionsMet300++;
                                                                                                                                                                          //Natural: PERFORM RTN-3A-PRMRY-STANDARD
                        sub_Rtn_3a_Prmry_Standard();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM RTN-3B-PRMRY-TEXT
                        sub_Rtn_3b_Prmry_Text();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                        //*  CONTINGENT
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: VALUE 'C'
                else if (condition((acis_Bene_File_View_Bene_Type.getValue(pnd_I).equals("C"))))
                {
                    decideConditionsMet298++;
                    short decideConditionsMet313 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF BENE-STD-TXT-IND ( #I );//Natural: VALUE 'S'
                    if (condition((acis_Bene_File_View_Bene_Std_Txt_Ind.getValue(pnd_I).equals("S"))))
                    {
                        decideConditionsMet313++;
                                                                                                                                                                          //Natural: PERFORM RTN-4A-CNTGNT-STANDARD
                        sub_Rtn_4a_Cntgnt_Standard();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: VALUE 'T'
                    else if (condition((acis_Bene_File_View_Bene_Std_Txt_Ind.getValue(pnd_I).equals("T"))))
                    {
                        decideConditionsMet313++;
                                                                                                                                                                          //Natural: PERFORM RTN-4B-CNTGNT-TEXT
                        sub_Rtn_4b_Cntgnt_Text();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: VALUE 'B'
                    else if (condition((acis_Bene_File_View_Bene_Std_Txt_Ind.getValue(pnd_I).equals("B"))))
                    {
                        decideConditionsMet313++;
                                                                                                                                                                          //Natural: PERFORM RTN-4A-CNTGNT-STANDARD
                        sub_Rtn_4a_Cntgnt_Standard();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM RTN-4B-CNTGNT-TEXT
                        sub_Rtn_4b_Cntgnt_Text();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  RTN-2-FORMAT-BENE-INFO
    }
    private void sub_Rtn_3a_Prmry_Standard() throws Exception                                                                                                             //Natural: RTN-3A-PRMRY-STANDARD
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        if (condition(acis_Bene_File_View_Bene_Name.getValue(pnd_I).equals(" ") && acis_Bene_File_View_Bene_Relationship.getValue(pnd_I).equals(" ") &&                   //Natural: IF BENE-NAME ( #I ) = ' ' AND BENE-RELATIONSHIP ( #I ) = ' ' AND BENE-SSN-NBR ( #I ) = 0 AND BENE-DOB ( #I ) = 0
            acis_Bene_File_View_Bene_Ssn_Nbr.getValue(pnd_I).equals(getZero()) && acis_Bene_File_View_Bene_Dob.getValue(pnd_I).equals(getZero())))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM RTN-3C-FORMAT-STANDARD
        sub_Rtn_3c_Format_Standard();
        if (condition(Global.isEscape())) {return;}
        //*  TNGSUB
        //*  C291829
        pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Cnt().nadd(1);                                                                                                         //Natural: ADD 1 TO #BENE-PRMRY-CNT
        pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Std_Txt_Ind().getValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Cnt()).setValue("N");                                //Natural: ASSIGN #BENE-PRMRY-STD-TXT-IND ( #BENE-PRMRY-CNT ) := 'N'
        //*                                                              /* C291829
        //*  RTN-3A-PRMRY-STANDARD
    }
    private void sub_Rtn_3b_Prmry_Text() throws Exception                                                                                                                 //Natural: RTN-3B-PRMRY-TEXT
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #J = 1 3
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(3)); pnd_J.nadd(1))
        {
            if (condition(acis_Bene_File_View_Bene_Spcl_Txt.getValue(pnd_I,pnd_J).notEquals(" ")))                                                                        //Natural: IF BENE-SPCL-TXT ( #I,#J ) NE ' '
            {
                //*  TNGSUB
                pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Cnt().nadd(1);                                                                                                 //Natural: ADD 1 TO #BENE-PRMRY-CNT
                //*  TNGSUB
                //*  C291829
                pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Spcl_Txt().getValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Cnt(),pnd_J).setValue(acis_Bene_File_View_Bene_Spcl_Txt.getValue(pnd_I, //Natural: MOVE BENE-SPCL-TXT ( #I,#J ) TO #BENE-PRMRY-SPCL-TXT ( #BENE-PRMRY-CNT,#J )
                    pnd_J));
                //*  C291829
                pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Std_Txt_Ind().getValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Cnt()).setValue("Y");                        //Natural: MOVE 'Y' TO #BENE-PRMRY-STD-TXT-IND ( #BENE-PRMRY-CNT )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  RTN-3B-PRMRY-TEXT
    }
    private void sub_Rtn_3c_Format_Standard() throws Exception                                                                                                            //Natural: RTN-3C-FORMAT-STANDARD
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        //*  C291829
        pnd_P.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #P
        //*  BNL2
        //*  BNL2V2
        if (condition(acis_Bene_File_View_Bene_Name.getValue(pnd_I).getSubstring(35,1).compareTo(" ") > 0 && acis_Bene_File_View_Bene_Name.getValue(pnd_I).notEquals("DEFAULT TO PRODUCT / PLAN PROVISION"))) //Natural: IF SUBSTR ( BENE-NAME ( #I ) ,35,1 ) GT ' ' AND BENE-NAME ( #I ) NOT = 'DEFAULT TO PRODUCT / PLAN PROVISION'
        {
            //*  TNGSUB
            //*  C291829       /* TNGSUB
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Name().getValue(pnd_P).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, acis_Bene_File_View_Bene_Name.getValue(pnd_I),  //Natural: COMPRESS BENE-NAME ( #I ) BENE-EXTENDED-NAME ( #I ) INTO #BENE-PRMRY-NAME ( #P ) LEAVING NO SPACE
                acis_Bene_File_View_Bene_Extended_Name.getValue(pnd_I)));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  BNL2
            //*  BNL2
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Name().getValue(pnd_P).setValue(DbsUtil.compress(acis_Bene_File_View_Bene_Name.getValue(pnd_I),                    //Natural: COMPRESS BENE-NAME ( #I ) BENE-EXTENDED-NAME ( #I ) INTO #BENE-PRMRY-NAME ( #P )
                acis_Bene_File_View_Bene_Extended_Name.getValue(pnd_I)));
        }                                                                                                                                                                 //Natural: END-IF
        //*  C291829
        pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Type().getValue(pnd_P).setValue(acis_Bene_File_View_Bene_Type.getValue(pnd_I));                                        //Natural: MOVE BENE-TYPE ( #I ) TO #BENE-PRMRY-TYPE ( #P )
        //*  C291829
        pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Relationship().getValue(pnd_P).setValue(acis_Bene_File_View_Bene_Relationship.getValue(pnd_I));                        //Natural: MOVE BENE-RELATIONSHIP ( #I ) TO #BENE-PRMRY-RELATIONSHIP ( #P )
        //*    CHG256421  STARTS HERE...
        //*  FOR DISPLAY IN THE WELCOME PKG - CHANGE BENEFICIARY RELATIONSHIP
        //*  DESCRIPTION FROM 'OTHER' TO 'DEFAULT'
        if (condition(acis_Bene_File_View_Bene_Spcl_Txt.getValue(pnd_I,1).equals("You currently have not designated a beneficiary; we will default") &&                   //Natural: IF BENE-SPCL-TXT ( #I,1 ) = 'You currently have not designated a beneficiary; we will default' AND BENE-SPCL-TXT ( #I,2 ) = 'to the provisions of the plan and/or product for this account.' AND BENE-SPCL-TXT ( #I,3 ) = 'Please add a beneficiary.'
            acis_Bene_File_View_Bene_Spcl_Txt.getValue(pnd_I,2).equals("to the provisions of the plan and/or product for this account.") && acis_Bene_File_View_Bene_Spcl_Txt.getValue(pnd_I,
            3).equals("Please add a beneficiary.")))
        {
            //*  C291829 /* TNGSUB
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Relationship().getValue(pnd_P).setValue("DEFAULT");                                                                //Natural: MOVE 'DEFAULT' TO #BENE-PRMRY-RELATIONSHIP ( #P )
            //*  C291829/* TNGSUB
        }                                                                                                                                                                 //Natural: END-IF
        pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Ssn().getValue(pnd_P).setValue(0);                                                                                     //Natural: ASSIGN #BENE-PRMRY-SSN ( #P ) := 0
        //*  TNGSUB
        if (condition(acis_Bene_File_View_Bene_Dob.getValue(pnd_I).notEquals(getZero())))                                                                                 //Natural: IF BENE-DOB ( #I ) NE 0
        {
            //*  C291829/* TNGSUB
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Dob().getValue(pnd_P).setValue(acis_Bene_File_View_Bene_Dob.getValue(pnd_I));                                      //Natural: MOVE BENE-DOB ( #I ) TO #BENE-PRMRY-DOB ( #P )
        }                                                                                                                                                                 //Natural: END-IF
        //*  TNGSUB
        if (condition(acis_Bene_File_View_Bene_Alloc_Pct.getValue(pnd_I).notEquals(getZero())))                                                                           //Natural: IF BENE-ALLOC-PCT ( #I ) NE 0
        {
            //*  C291829
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Alloc_Pct().getValue(pnd_P).setValue(acis_Bene_File_View_Bene_Alloc_Pct.getValue(pnd_I));                          //Natural: MOVE BENE-ALLOC-PCT ( #I ) TO #BENE-PRMRY-ALLOC-PCT ( #P )
            //*  TNGSUB
        }                                                                                                                                                                 //Natural: END-IF
        //*  TNGSUB
        if (condition(acis_Bene_File_View_Bene_Nmrtr_Nbr.getValue(pnd_I).notEquals(getZero())))                                                                           //Natural: IF BENE-NMRTR-NBR ( #I ) NE 0
        {
            //*  C291829
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Nmrtr_Nbr().getValue(pnd_P).setValue(acis_Bene_File_View_Bene_Nmrtr_Nbr.getValue(pnd_I));                          //Natural: MOVE BENE-NMRTR-NBR ( #I ) TO #BENE-PRMRY-NMRTR-NBR ( #P )
            //*  TNGSUB
        }                                                                                                                                                                 //Natural: END-IF
        //*  TNGSUB
        if (condition(acis_Bene_File_View_Bene_Dnmntr_Nbr.getValue(pnd_I).notEquals(getZero())))                                                                          //Natural: IF BENE-DNMNTR-NBR ( #I ) NE 0
        {
            //*  C291829
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Dnmntr_Nbr().getValue(pnd_P).setValue(acis_Bene_File_View_Bene_Dnmntr_Nbr.getValue(pnd_I));                        //Natural: MOVE BENE-DNMNTR-NBR ( #I ) TO #BENE-PRMRY-DNMNTR-NBR ( #P )
        }                                                                                                                                                                 //Natural: END-IF
        //*  RTN-3C-FORMAT-STANDARD
    }
    private void sub_Rtn_4a_Cntgnt_Standard() throws Exception                                                                                                            //Natural: RTN-4A-CNTGNT-STANDARD
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        if (condition(acis_Bene_File_View_Bene_Name.getValue(pnd_I).equals(" ") && acis_Bene_File_View_Bene_Relationship.getValue(pnd_I).equals(" ") &&                   //Natural: IF BENE-NAME ( #I ) = ' ' AND BENE-RELATIONSHIP ( #I ) = ' ' AND BENE-SSN-NBR ( #I ) = 0 AND BENE-DOB ( #I ) = 0
            acis_Bene_File_View_Bene_Ssn_Nbr.getValue(pnd_I).equals(getZero()) && acis_Bene_File_View_Bene_Dob.getValue(pnd_I).equals(getZero())))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  C291829
                                                                                                                                                                          //Natural: PERFORM RTN-4C-FORMAT-STANDARD
        sub_Rtn_4c_Format_Standard();
        if (condition(Global.isEscape())) {return;}
        //*  TNGSUB
        //*  C291829
        pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Cnt().nadd(1);                                                                                                        //Natural: ADD 1 TO #BENE-CNTGNT-CNT
        pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Std_Txt_Ind().getValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Cnt()).setValue("N");                              //Natural: ASSIGN #BENE-CNTGNT-STD-TXT-IND ( #BENE-CNTGNT-CNT ) := 'N'
        //*  RTN-4A-CNTGNT-STANDARD
    }
    private void sub_Rtn_4b_Cntgnt_Text() throws Exception                                                                                                                //Natural: RTN-4B-CNTGNT-TEXT
    {
        if (BLNatReinput.isReinput()) return;

        FOR04:                                                                                                                                                            //Natural: FOR #J = 1 3
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(3)); pnd_J.nadd(1))
        {
            if (condition(acis_Bene_File_View_Bene_Spcl_Txt.getValue(pnd_I,pnd_J).notEquals(" ")))                                                                        //Natural: IF BENE-SPCL-TXT ( #I,#J ) NE ' '
            {
                pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Cnt().nadd(1);                                                                                                //Natural: ADD 1 TO #BENE-CNTGNT-CNT
                //*  TNGSUB
                //*  C291829
                pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Spcl_Txt().getValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Cnt(),pnd_J).setValue(acis_Bene_File_View_Bene_Spcl_Txt.getValue(pnd_I, //Natural: MOVE BENE-SPCL-TXT ( #I,#J ) TO #BENE-CNTGNT-SPCL-TXT ( #BENE-CNTGNT-CNT,#J )
                    pnd_J));
                //*  C291829
                pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Std_Txt_Ind().getValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Cnt()).setValue("Y");                      //Natural: MOVE 'Y' TO #BENE-CNTGNT-STD-TXT-IND ( #BENE-CNTGNT-CNT )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  RTN-4B-CNTGNT-TEXT
    }
    private void sub_Rtn_4c_Format_Standard() throws Exception                                                                                                            //Natural: RTN-4C-FORMAT-STANDARD
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        pnd_C.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #C
        //*  BNL2V2
        //*  BNL2V2
        if (condition(acis_Bene_File_View_Bene_Name.getValue(pnd_I).getSubstring(35,1).compareTo(" ") > 0 && acis_Bene_File_View_Bene_Name.getValue(pnd_I).notEquals("DEFAULT TO PRODUCT / PLAN PROVISION"))) //Natural: IF SUBSTR ( BENE-NAME ( #I ) ,35,1 ) GT ' ' AND BENE-NAME ( #I ) NOT = 'DEFAULT TO PRODUCT / PLAN PROVISION'
        {
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Name().getValue(pnd_C).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, acis_Bene_File_View_Bene_Name.getValue(pnd_I),  //Natural: COMPRESS BENE-NAME ( #I ) BENE-EXTENDED-NAME ( #I ) INTO #BENE-CNTGNT-NAME ( #C ) LEAVING NO SPACE
                acis_Bene_File_View_Bene_Extended_Name.getValue(pnd_I)));
            //*  BNL2V2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  BNL2V2
            //*  BNL2V2
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Name().getValue(pnd_C).setValue(DbsUtil.compress(acis_Bene_File_View_Bene_Name.getValue(pnd_I),                   //Natural: COMPRESS BENE-NAME ( #I ) BENE-EXTENDED-NAME ( #I ) INTO #BENE-CNTGNT-NAME ( #C )
                acis_Bene_File_View_Bene_Extended_Name.getValue(pnd_I)));
        }                                                                                                                                                                 //Natural: END-IF
        //* * COMPRESS BENE-NAME (#I) BENE-EXTENDED-NAME (#I) INTO       /* BNL2V2
        //*    #BENE-CNTGNT-NAME (#C)  LEAVING NO SPACE                  /* BNL2V2
        pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Type().getValue(pnd_C).setValue(acis_Bene_File_View_Bene_Type.getValue(pnd_I));                                       //Natural: MOVE BENE-TYPE ( #I ) TO #BENE-CNTGNT-TYPE ( #C )
        pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Relationship().getValue(pnd_C).setValue(acis_Bene_File_View_Bene_Relationship.getValue(pnd_I));                       //Natural: MOVE BENE-RELATIONSHIP ( #I ) TO #BENE-CNTGNT-RELATIONSHIP ( #C )
        if (condition(acis_Bene_File_View_Bene_Spcl_Txt.getValue(pnd_I,1).equals("You currently have not designated a beneficiary; we will default") &&                   //Natural: IF BENE-SPCL-TXT ( #I,1 ) = 'You currently have not designated a beneficiary; we will default' AND BENE-SPCL-TXT ( #I,2 ) = 'to the provisions of the plan and/or product for this account.' AND BENE-SPCL-TXT ( #I,3 ) = 'Please add a beneficiary.'
            acis_Bene_File_View_Bene_Spcl_Txt.getValue(pnd_I,2).equals("to the provisions of the plan and/or product for this account.") && acis_Bene_File_View_Bene_Spcl_Txt.getValue(pnd_I,
            3).equals("Please add a beneficiary.")))
        {
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Relationship().getValue(pnd_C).setValue("DEFAULT");                                                               //Natural: MOVE 'DEFAULT' TO #BENE-CNTGNT-RELATIONSHIP ( #C )
        }                                                                                                                                                                 //Natural: END-IF
        pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Ssn().getValue(pnd_C).setValue(0);                                                                                    //Natural: ASSIGN #BENE-CNTGNT-SSN ( #C ) := 0
        if (condition(acis_Bene_File_View_Bene_Dob.getValue(pnd_I).notEquals(getZero())))                                                                                 //Natural: IF BENE-DOB ( #I ) NE 0
        {
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Dob().getValue(pnd_C).setValue(acis_Bene_File_View_Bene_Dob.getValue(pnd_I));                                     //Natural: MOVE BENE-DOB ( #I ) TO #BENE-CNTGNT-DOB ( #C )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(acis_Bene_File_View_Bene_Alloc_Pct.getValue(pnd_I).notEquals(getZero())))                                                                           //Natural: IF BENE-ALLOC-PCT ( #I ) NE 0
        {
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Alloc_Pct().getValue(pnd_C).setValue(acis_Bene_File_View_Bene_Alloc_Pct.getValue(pnd_I));                         //Natural: MOVE BENE-ALLOC-PCT ( #I ) TO #BENE-CNTGNT-ALLOC-PCT ( #C )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(acis_Bene_File_View_Bene_Nmrtr_Nbr.getValue(pnd_I).notEquals(getZero())))                                                                           //Natural: IF BENE-NMRTR-NBR ( #I ) NE 0
        {
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Nmrtr_Nbr().getValue(pnd_C).setValue(acis_Bene_File_View_Bene_Nmrtr_Nbr.getValue(pnd_I));                         //Natural: MOVE BENE-NMRTR-NBR ( #I ) TO #BENE-CNTGNT-NMRTR-NBR ( #C )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(acis_Bene_File_View_Bene_Dnmntr_Nbr.getValue(pnd_I).notEquals(getZero())))                                                                          //Natural: IF BENE-DNMNTR-NBR ( #I ) NE 0
        {
            pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Dnmntr_Nbr().getValue(pnd_C).setValue(acis_Bene_File_View_Bene_Dnmntr_Nbr.getValue(pnd_I));                       //Natural: MOVE BENE-DNMNTR-NBR ( #I ) TO #BENE-CNTGNT-DNMNTR-NBR ( #C )
        }                                                                                                                                                                 //Natural: END-IF
        //*  RTN-4C-FORMAT-STANDARD
    }
    private void sub_Rtn_99_Debug() throws Exception                                                                                                                      //Natural: RTN-99-DEBUG
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        getReports().write(0, "-",new RepeatItem(20),Global.getPROGRAM(),"(3520)","-",new RepeatItem(20));                                                                //Natural: WRITE '-' ( 20 ) *PROGRAM '(3520)' '-' ( 20 )
        if (Global.isEscape()) return;
        getReports().write(0, "APPA105-INPUT");                                                                                                                           //Natural: WRITE 'APPA105-INPUT'
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Tiaa_Nbr());                                                                                       //Natural: WRITE '=' #BENE-TIAA-NBR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cref_Nbr());                                                                                       //Natural: WRITE '=' #BENE-CREF-NBR
        if (Global.isEscape()) return;
        getReports().write(0, "APPA105-OUTPUT");                                                                                                                          //Natural: WRITE 'APPA105-OUTPUT'
        if (Global.isEscape()) return;
        //*  TNGSUB
        getReports().write(0, "-",new RepeatItem(10),"PRMRY BENE DATA","-",new RepeatItem(10));                                                                           //Natural: WRITE '-' ( 10 ) 'PRMRY BENE DATA' '-' ( 10 )
        if (Global.isEscape()) return;
        FOR05:                                                                                                                                                            //Natural: FOR #I = 1 #BENE-PRMRY-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Cnt())); pnd_I.nadd(1))
        {
            //*  C291829 /* TNGSUB
            getReports().write(0, pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Data().getValue(pnd_I));                                                                     //Natural: WRITE #BENE-PRMRY-DATA ( #I )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *--------------------------------------------------------------------**
        //*  TNGSUB
        getReports().write(0, "-",new RepeatItem(10),"CNTGNT BENE DATA","-",new RepeatItem(10));                                                                          //Natural: WRITE '-' ( 10 ) 'CNTGNT BENE DATA' '-' ( 10 )
        if (Global.isEscape()) return;
        FOR06:                                                                                                                                                            //Natural: FOR #I = 1 #BENE-CNTGNT-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Cnt())); pnd_I.nadd(1))
        {
            //*  C291829 /* TNGSUB
            getReports().write(0, pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Data().getValue(pnd_I));                                                                    //Natural: WRITE #BENE-CNTGNT-DATA ( #I )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  TNGSUB
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *--------------------------------------------------------------------**
        getReports().write(0, "=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Return_Code());                                                                                    //Natural: WRITE '=' #BENE-RETURN-CODE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Msg(), new AlphanumericLength (78));                                                               //Natural: WRITE '=' #BENE-MSG ( AL = 78 )
        if (Global.isEscape()) return;
        //*  RTN-99-DEBUG
    }

    //
}
