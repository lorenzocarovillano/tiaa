/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:10:36 AM
**        * FROM NATURAL SUBPROGRAM : Scin9005
************************************************************
**        * FILE NAME            : Scin9005.java
**        * CLASS NAME           : Scin9005
**        * INSTANCE NAME        : Scin9005
************************************************************
************************************************************************
* PROGRAM  : SCIN9005
* SYSTEM   : SUNGARD ENROLLMENT - CONTRACT PICK-UP PROCESSING
* GENERATED: SEPT 6, 2005
* FUNCTION : THIS MODULE READS THE ACIS PRAP FILE TO CHECK IF A CONTRACT
*          : WAS ALREADY ISSUED FOR THE PERSON
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* 05-07-2007 BILLIODEAU ADDED NEW FIELDS TO PRAP-VIEW: INSURER-1,
*                       INSURER-2, INSURER-3, 1035-EXCH-IND-1,
*                       1035-EXCH-IND-2, 1035-EXCH-IND-3
* 07/18/13   ELLO   ADDED LOGIC FOR TNG IRA SUBSTITUTION TO ALLOW
*                   1 DUPLICATE CONTRACT WITH THE SAME PLAN AND SUBPLAN
*          TNGSUB   AS LONG AS THE AP-SUBSTITUTION-CONTRACT-IND = ' '.
* 05/23/17 B.ELLO   PIN EXPANSION CHANGES
*                   CHANGED TO POINT TO NEW VIEW OF THE PRAP FILE =
*                   ANNTY-ACTVTY-PRAP_12 (PINE)
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Scin9005 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Plan_No;
    private DbsField pnd_Sub_Plan_No;
    private DbsField pnd_Sg_Part_Ext;
    private DbsField pnd_Ssn;
    private DbsField pnd_Substitution_Contract_Ind;
    private DbsField pnd_Contract_Exists;

    private DataAccessProgramView vw_prap_View;
    private DbsField prap_View_Ap_Coll_Code;
    private DbsField prap_View_Ap_Soc_Sec;
    private DbsField prap_View_Ap_Status;
    private DbsField prap_View_Ap_Record_Type;
    private DbsField prap_View_Ap_Sgrd_Plan_No;
    private DbsField prap_View_Ap_Sgrd_Subplan_No;
    private DbsField prap_View_Ap_Sgrd_Part_Ext;
    private DbsField prap_View_Ap_Sgrd_Divsub;
    private DbsField prap_View_Ap_Text_Udf_1;
    private DbsField prap_View_Ap_Text_Udf_2;
    private DbsField prap_View_Ap_Text_Udf_3;
    private DbsField prap_View_Ap_Pin_Nbr;
    private DbsField prap_View_Ap_Substitution_Contract_Ind;
    private DbsField pnd_Prap_Ssn;

    private DbsGroup pnd_Prap_Ssn__R_Field_1;
    private DbsField pnd_Prap_Ssn_Pnd_Prap_Ssnn;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Plan_No = parameters.newFieldInRecord("pnd_Plan_No", "#PLAN-NO", FieldType.STRING, 6);
        pnd_Plan_No.setParameterOption(ParameterOption.ByReference);
        pnd_Sub_Plan_No = parameters.newFieldInRecord("pnd_Sub_Plan_No", "#SUB-PLAN-NO", FieldType.STRING, 6);
        pnd_Sub_Plan_No.setParameterOption(ParameterOption.ByReference);
        pnd_Sg_Part_Ext = parameters.newFieldInRecord("pnd_Sg_Part_Ext", "#SG-PART-EXT", FieldType.STRING, 2);
        pnd_Sg_Part_Ext.setParameterOption(ParameterOption.ByReference);
        pnd_Ssn = parameters.newFieldInRecord("pnd_Ssn", "#SSN", FieldType.STRING, 9);
        pnd_Ssn.setParameterOption(ParameterOption.ByReference);
        pnd_Substitution_Contract_Ind = parameters.newFieldInRecord("pnd_Substitution_Contract_Ind", "#SUBSTITUTION-CONTRACT-IND", FieldType.STRING, 1);
        pnd_Substitution_Contract_Ind.setParameterOption(ParameterOption.ByReference);
        pnd_Contract_Exists = parameters.newFieldInRecord("pnd_Contract_Exists", "#CONTRACT-EXISTS", FieldType.STRING, 1);
        pnd_Contract_Exists.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_prap_View = new DataAccessProgramView(new NameInfo("vw_prap_View", "PRAP-VIEW"), "ANNTY_ACTVTY_PRAP_12", "ACIS_ANNTY_PRAP");
        prap_View_Ap_Coll_Code = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Coll_Code", "AP-COLL-CODE", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_COLL_CODE");
        prap_View_Ap_Soc_Sec = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, 
            "AP_SOC_SEC");
        prap_View_Ap_Status = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Status", "AP-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_STATUS");
        prap_View_Ap_Record_Type = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Record_Type", "AP-RECORD-TYPE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_RECORD_TYPE");
        prap_View_Ap_Sgrd_Plan_No = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_SGRD_PLAN_NO");
        prap_View_Ap_Sgrd_Subplan_No = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Sgrd_Subplan_No", "AP-SGRD-SUBPLAN-NO", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "AP_SGRD_SUBPLAN_NO");
        prap_View_Ap_Sgrd_Part_Ext = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Sgrd_Part_Ext", "AP-SGRD-PART-EXT", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "AP_SGRD_PART_EXT");
        prap_View_Ap_Sgrd_Divsub = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Sgrd_Divsub", "AP-SGRD-DIVSUB", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "AP_SGRD_DIVSUB");
        prap_View_Ap_Text_Udf_1 = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Text_Udf_1", "AP-TEXT-UDF-1", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TEXT_UDF_1");
        prap_View_Ap_Text_Udf_2 = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Text_Udf_2", "AP-TEXT-UDF-2", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TEXT_UDF_2");
        prap_View_Ap_Text_Udf_3 = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Text_Udf_3", "AP-TEXT-UDF-3", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TEXT_UDF_3");
        prap_View_Ap_Pin_Nbr = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "AP_PIN_NBR");
        prap_View_Ap_Substitution_Contract_Ind = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Substitution_Contract_Ind", "AP-SUBSTITUTION-CONTRACT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_SUBSTITUTION_CONTRACT_IND");
        registerRecord(vw_prap_View);

        pnd_Prap_Ssn = localVariables.newFieldInRecord("pnd_Prap_Ssn", "#PRAP-SSN", FieldType.STRING, 9);

        pnd_Prap_Ssn__R_Field_1 = localVariables.newGroupInRecord("pnd_Prap_Ssn__R_Field_1", "REDEFINE", pnd_Prap_Ssn);
        pnd_Prap_Ssn_Pnd_Prap_Ssnn = pnd_Prap_Ssn__R_Field_1.newFieldInGroup("pnd_Prap_Ssn_Pnd_Prap_Ssnn", "#PRAP-SSNN", FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_prap_View.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Scin9005() throws Exception
    {
        super("Scin9005");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* *======================================================================
        //* *    M A I N    P R O C E S S
        //* *======================================================================
        pnd_Contract_Exists.setValue("N");                                                                                                                                //Natural: ASSIGN #CONTRACT-EXISTS := 'N'
        pnd_Prap_Ssn.setValue(pnd_Ssn);                                                                                                                                   //Natural: ASSIGN #PRAP-SSN := #SSN
        vw_prap_View.startDatabaseRead                                                                                                                                    //Natural: READ PRAP-VIEW BY AP-SOC-SEC = #PRAP-SSNN
        (
        "READ01",
        new Wc[] { new Wc("AP_SOC_SEC", ">=", pnd_Prap_Ssn_Pnd_Prap_Ssnn, WcType.BY) },
        new Oc[] { new Oc("AP_SOC_SEC", "ASC") }
        );
        READ01:
        while (condition(vw_prap_View.readNextRow("READ01")))
        {
            if (condition(prap_View_Ap_Soc_Sec.greater(pnd_Prap_Ssn_Pnd_Prap_Ssnn)))                                                                                      //Natural: IF AP-SOC-SEC GT #PRAP-SSNN
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  DELETED
            if (condition(prap_View_Ap_Sgrd_Plan_No.equals(pnd_Plan_No) && prap_View_Ap_Sgrd_Subplan_No.equals(pnd_Sub_Plan_No) && prap_View_Ap_Sgrd_Part_Ext.equals(pnd_Sg_Part_Ext)  //Natural: IF AP-SGRD-PLAN-NO = #PLAN-NO AND AP-SGRD-SUBPLAN-NO = #SUB-PLAN-NO AND AP-SGRD-PART-EXT = #SG-PART-EXT AND AP-STATUS NE 'A'
                && prap_View_Ap_Status.notEquals("A")))
            {
                //*  INCOMING CRT  TNGSUB
                if (condition(pnd_Substitution_Contract_Ind.equals("W") || pnd_Substitution_Contract_Ind.equals("O")))                                                    //Natural: IF #SUBSTITUTION-CONTRACT-IND = 'W' OR = 'O'
                {
                    //*  ON PRAP TNGSUB
                    if (condition(prap_View_Ap_Substitution_Contract_Ind.equals("W") || prap_View_Ap_Substitution_Contract_Ind.equals("O")))                              //Natural: IF AP-SUBSTITUTION-CONTRACT-IND = 'W' OR = 'O'
                    {
                        pnd_Contract_Exists.setValue("Y");                                                                                                                //Natural: ASSIGN #CONTRACT-EXISTS := 'Y'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Contract_Exists.setValue("Y");                                                                                                                    //Natural: ASSIGN #CONTRACT-EXISTS := 'Y'
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
