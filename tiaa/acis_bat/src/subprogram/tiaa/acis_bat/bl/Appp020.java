/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:59:15 PM
**        * FROM NATURAL SUBPROGRAM : Appp020
************************************************************
**        * FILE NAME            : Appp020.java
**        * CLASS NAME           : Appp020
**        * INSTANCE NAME        : Appp020
************************************************************
************************************************************************
* PROGRAM ID : APPP020                                                 *
* FUNCTION   : TO KEEP TRACK OF CONTRACT # AND DAILY BALANCE           *
*                                                                      *
* --------  ------------- -------------------------------------------- *
*   DATE   DEVELOPER      PURPOSE                                      *
* -------- -------------- -------------------------------------------- *
* 02/05/99  L. SHU        UPDATE ALL ACCUMULATORS DYNAMICALLY LS1      *
* 08/24/05  J. BERGHEISER CORRECT PRODUCTION PROBLEM WHERE CONTRACTS   *
* IM186845                NOT BEING ISSUED                             *
* 08/31/05  B. DEVELBISS  REMOVED 'TEST VERSION' DISPLAY STATEMENT     *
* 01/29/07  J. BERGHEISER ALPHA CONTRACT RANGE EXPANSION PROJECT (JRB1)*
*                         ADDED WORK-CONTRACT-N3 LOGIC                 *
*                         CALL APPN2075 FOR NEXT ALPHA RANGE VALUE     *
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appp020 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAppa010 pdaAppa010;
    private LdaAppl020 ldaAppl020;
    private PdaAcia3570 pdaAcia3570;
    private LdaAcil7070 ldaAcil7070;
    private PdaAppa2075 pdaAppa2075;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Current_Rcrd_Dte;
    private DbsField pnd_Previous_Rcrd_Dte;
    private DbsField pnd_Dummy_Parm;
    private DbsField nat2_Parm_Ppcn;

    private DbsGroup nat2_Parm_Ppcn__R_Field_1;
    private DbsField nat2_Parm_Ppcn_Parm_List_Ppcn;
    private DbsField nat2_Parm_Ppcn_Parm_List_Chkdig;
    private DbsField pnd_B_Isn;
    private DbsField pnd_Retry_Count;
    private DbsField pnd_Wk_Bucket_Cde;
    private DbsField pnd_J;
    private DbsField pnd_I;
    private DbsField pnd_Work_Contract;

    private DbsGroup pnd_Work_Contract__R_Field_2;
    private DbsField pnd_Work_Contract_Pnd_Work_Char;

    private DbsGroup pnd_Work_Contract__R_Field_3;
    private DbsField pnd_Work_Contract_Pnd_Work_Contract_N6;

    private DbsGroup pnd_Work_Contract__R_Field_4;
    private DbsField pnd_Work_Contract_Pnd_Work_Contract_N5;

    private DbsGroup pnd_Work_Contract__R_Field_5;
    private DbsField pnd_Work_Contract_Pnd_Work_Contract_N4;

    private DbsGroup pnd_Work_Contract__R_Field_6;
    private DbsField pnd_Work_Contract_Pnd_Work_Contract_N3;
    private DbsField pnd_Work_Contract_Pnd_Work_Alpha_Pos_5_7;

    private DbsGroup pnd_Work_Contract__R_Field_7;
    private DbsField pnd_Work_Contract_Pnd_Work_Filler_5;
    private DbsField pnd_Work_Contract_Pnd_Work_Alpha_Pos_6_7;

    private DbsGroup pnd_Work_Contract__R_Field_8;
    private DbsField pnd_Work_Contract_Pnd_Work_Filler_6;
    private DbsField pnd_Work_Contract_Pnd_Work_Alpha_Pos_7;
    private DbsField pnd_Contract_End;

    private DbsGroup pnd_Contract_End__R_Field_9;
    private DbsField pnd_Contract_End_Pnd_End_Prefix;
    private DbsField pnd_Contract_End_Pnd_End_Char;
    private DbsField pnd_Alpha_Sub;
    private DbsField pnd_Last_Curr_Alpha_Contrct;
    private int chekdigReturnCode;
    private int cda0670ReturnCode;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl020 = new LdaAppl020();
        registerRecord(ldaAppl020);
        registerRecord(ldaAppl020.getVw_app_Ilog_B_Rcrd_View());
        registerRecord(ldaAppl020.getVw_app_Ilog_J_Rcrd_View());
        registerRecord(ldaAppl020.getVw_app_Ilog_A_Rcrd_View());
        localVariables = new DbsRecord();
        pdaAcia3570 = new PdaAcia3570(localVariables);
        ldaAcil7070 = new LdaAcil7070();
        registerRecord(ldaAcil7070);
        registerRecord(ldaAcil7070.getVw_app_Table_Entry_View());
        pdaAppa2075 = new PdaAppa2075(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaAppa010 = new PdaAppa010(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Current_Rcrd_Dte = localVariables.newFieldInRecord("pnd_Current_Rcrd_Dte", "#CURRENT-RCRD-DTE", FieldType.STRING, 8);
        pnd_Previous_Rcrd_Dte = localVariables.newFieldInRecord("pnd_Previous_Rcrd_Dte", "#PREVIOUS-RCRD-DTE", FieldType.STRING, 8);
        pnd_Dummy_Parm = localVariables.newFieldInRecord("pnd_Dummy_Parm", "#DUMMY-PARM", FieldType.STRING, 1);
        nat2_Parm_Ppcn = localVariables.newFieldInRecord("nat2_Parm_Ppcn", "NAT2-PARM-PPCN", FieldType.STRING, 8);

        nat2_Parm_Ppcn__R_Field_1 = localVariables.newGroupInRecord("nat2_Parm_Ppcn__R_Field_1", "REDEFINE", nat2_Parm_Ppcn);
        nat2_Parm_Ppcn_Parm_List_Ppcn = nat2_Parm_Ppcn__R_Field_1.newFieldInGroup("nat2_Parm_Ppcn_Parm_List_Ppcn", "PARM-LIST-PPCN", FieldType.STRING, 
            7);
        nat2_Parm_Ppcn_Parm_List_Chkdig = nat2_Parm_Ppcn__R_Field_1.newFieldInGroup("nat2_Parm_Ppcn_Parm_List_Chkdig", "PARM-LIST-CHKDIG", FieldType.STRING, 
            1);
        pnd_B_Isn = localVariables.newFieldInRecord("pnd_B_Isn", "#B-ISN", FieldType.BINARY, 4);
        pnd_Retry_Count = localVariables.newFieldInRecord("pnd_Retry_Count", "#RETRY-COUNT", FieldType.NUMERIC, 7);
        pnd_Wk_Bucket_Cde = localVariables.newFieldInRecord("pnd_Wk_Bucket_Cde", "#WK-BUCKET-CDE", FieldType.STRING, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Work_Contract = localVariables.newFieldInRecord("pnd_Work_Contract", "#WORK-CONTRACT", FieldType.STRING, 6);

        pnd_Work_Contract__R_Field_2 = localVariables.newGroupInRecord("pnd_Work_Contract__R_Field_2", "REDEFINE", pnd_Work_Contract);
        pnd_Work_Contract_Pnd_Work_Char = pnd_Work_Contract__R_Field_2.newFieldArrayInGroup("pnd_Work_Contract_Pnd_Work_Char", "#WORK-CHAR", FieldType.STRING, 
            1, new DbsArrayController(1, 6));

        pnd_Work_Contract__R_Field_3 = localVariables.newGroupInRecord("pnd_Work_Contract__R_Field_3", "REDEFINE", pnd_Work_Contract);
        pnd_Work_Contract_Pnd_Work_Contract_N6 = pnd_Work_Contract__R_Field_3.newFieldInGroup("pnd_Work_Contract_Pnd_Work_Contract_N6", "#WORK-CONTRACT-N6", 
            FieldType.NUMERIC, 6);

        pnd_Work_Contract__R_Field_4 = localVariables.newGroupInRecord("pnd_Work_Contract__R_Field_4", "REDEFINE", pnd_Work_Contract);
        pnd_Work_Contract_Pnd_Work_Contract_N5 = pnd_Work_Contract__R_Field_4.newFieldInGroup("pnd_Work_Contract_Pnd_Work_Contract_N5", "#WORK-CONTRACT-N5", 
            FieldType.NUMERIC, 5);

        pnd_Work_Contract__R_Field_5 = localVariables.newGroupInRecord("pnd_Work_Contract__R_Field_5", "REDEFINE", pnd_Work_Contract);
        pnd_Work_Contract_Pnd_Work_Contract_N4 = pnd_Work_Contract__R_Field_5.newFieldInGroup("pnd_Work_Contract_Pnd_Work_Contract_N4", "#WORK-CONTRACT-N4", 
            FieldType.NUMERIC, 4);

        pnd_Work_Contract__R_Field_6 = localVariables.newGroupInRecord("pnd_Work_Contract__R_Field_6", "REDEFINE", pnd_Work_Contract);
        pnd_Work_Contract_Pnd_Work_Contract_N3 = pnd_Work_Contract__R_Field_6.newFieldInGroup("pnd_Work_Contract_Pnd_Work_Contract_N3", "#WORK-CONTRACT-N3", 
            FieldType.NUMERIC, 3);
        pnd_Work_Contract_Pnd_Work_Alpha_Pos_5_7 = pnd_Work_Contract__R_Field_6.newFieldInGroup("pnd_Work_Contract_Pnd_Work_Alpha_Pos_5_7", "#WORK-ALPHA-POS-5-7", 
            FieldType.STRING, 3);

        pnd_Work_Contract__R_Field_7 = pnd_Work_Contract__R_Field_6.newGroupInGroup("pnd_Work_Contract__R_Field_7", "REDEFINE", pnd_Work_Contract_Pnd_Work_Alpha_Pos_5_7);
        pnd_Work_Contract_Pnd_Work_Filler_5 = pnd_Work_Contract__R_Field_7.newFieldInGroup("pnd_Work_Contract_Pnd_Work_Filler_5", "#WORK-FILLER-5", FieldType.STRING, 
            1);
        pnd_Work_Contract_Pnd_Work_Alpha_Pos_6_7 = pnd_Work_Contract__R_Field_7.newFieldInGroup("pnd_Work_Contract_Pnd_Work_Alpha_Pos_6_7", "#WORK-ALPHA-POS-6-7", 
            FieldType.STRING, 2);

        pnd_Work_Contract__R_Field_8 = pnd_Work_Contract__R_Field_7.newGroupInGroup("pnd_Work_Contract__R_Field_8", "REDEFINE", pnd_Work_Contract_Pnd_Work_Alpha_Pos_6_7);
        pnd_Work_Contract_Pnd_Work_Filler_6 = pnd_Work_Contract__R_Field_8.newFieldInGroup("pnd_Work_Contract_Pnd_Work_Filler_6", "#WORK-FILLER-6", FieldType.STRING, 
            1);
        pnd_Work_Contract_Pnd_Work_Alpha_Pos_7 = pnd_Work_Contract__R_Field_8.newFieldInGroup("pnd_Work_Contract_Pnd_Work_Alpha_Pos_7", "#WORK-ALPHA-POS-7", 
            FieldType.STRING, 1);
        pnd_Contract_End = localVariables.newFieldInRecord("pnd_Contract_End", "#CONTRACT-END", FieldType.STRING, 7);

        pnd_Contract_End__R_Field_9 = localVariables.newGroupInRecord("pnd_Contract_End__R_Field_9", "REDEFINE", pnd_Contract_End);
        pnd_Contract_End_Pnd_End_Prefix = pnd_Contract_End__R_Field_9.newFieldInGroup("pnd_Contract_End_Pnd_End_Prefix", "#END-PREFIX", FieldType.STRING, 
            1);
        pnd_Contract_End_Pnd_End_Char = pnd_Contract_End__R_Field_9.newFieldArrayInGroup("pnd_Contract_End_Pnd_End_Char", "#END-CHAR", FieldType.STRING, 
            1, new DbsArrayController(1, 6));
        pnd_Alpha_Sub = localVariables.newFieldInRecord("pnd_Alpha_Sub", "#ALPHA-SUB", FieldType.NUMERIC, 3);
        pnd_Last_Curr_Alpha_Contrct = localVariables.newFieldInRecord("pnd_Last_Curr_Alpha_Contrct", "#LAST-CURR-ALPHA-CONTRCT", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl020.initializeValues();
        ldaAcil7070.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Appp020() throws Exception
    {
        super("Appp020");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPP020", onError);
        //* **********************************************************************
        //*  MAIN LOGIC SECTION                                                  *
        //* **********************************************************************
        //*  ---------------USAGE NOTES--------------------------------------
        //*   CALLER MUST CONTROL END-TRANSACTIONS -- OTHERWISE UNPREDICTABLE
        //*   UPDATES MIGHT BE MADE TOO SOON BEFORE A LOGICAL UNIT OF WORK
        //*  ----------END  USAGE NOTES--------------------------------------
        pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message().reset();                                                                                                 //Natural: RESET #OTHER-ACTIVITY-MESSAGE #GET-CONTRACT-MESSAGE #ADD-TO-BUCKETS-MESSAGE
        pdaAppa010.getPnd_Parameter_Pnd_Get_Contract_Message().reset();
        pdaAppa010.getPnd_Parameter_Pnd_Add_To_Buckets_Message().reset();
        if (condition(pdaAppa010.getPnd_Parameter_Pnd_Get_Contract_Flag().equals("PUT NEW RUN DT")))                                                                      //Natural: IF #GET-CONTRACT-FLAG = 'PUT NEW RUN DT'
        {
                                                                                                                                                                          //Natural: PERFORM NEWRUNDATE
            sub_Newrundate();
            if (condition(Global.isEscape())) {return;}
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  -------FIRST NEW DAY RTN DETERMINES IF LAST DAY SHOULD BE CLOSED
        //*  -------OUT AND A NEW SET OF TIMESLICES SHOULD BE CREATED--------
        //*  REPEAT
        //*    PERFORM FIRST-NEW-DAY
        //*    ESCAPE ROUTINE
        //*  END-REPEAT
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
                                                                                                                                                                          //Natural: PERFORM GET-NEW-CONTRACT
            sub_Get_New_Contract();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  REPEAT
        //*    PERFORM ADD-TO-BUCKETS
        //*    ESCAPE ROUTINE
        //*  END-REPEAT
        //*  --------------------------------------------------------------------
        //*                                                                                                                                                               //Natural: ON ERROR
        //*  -------------------------------------------------------------
        //*  ----------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NEWRUNDATE
        //*  -----------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIRST-NEW-DAY
        //*  WRITE ' FIRST-NEW-DAY '
        //*  -------FINDING THE CURRENT JUMPER RECORDS DETERMINES POINTER DATE
        //*  ------ AND IT CYCLES THROUGH ALL OF THE PRODUCTS TO BE CREATED
        //*  -------THIS GETS CREATIVE (NEW TIMESLICES)-----------------------
        //*  TO LOOP UNTIL ALL THE ACCUMMULATOR IS CREATED
        //*  -------FIND THE RELEVENT BUCKET RECORD TO POINT FROM ACCUMULATORS
        //* *          WITH RCRD-CDE = 'BU'
        //*  -------FIND THE CURRENT ACCUM RECORD TO CLOSE OUT (UPDATE NEEDED)
        //* **         AND  RCRD-CDE   =  'AC'
        //*  --------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NEW-CONTRACT
        //*  WRITE ' GET-NEW-CONTRACT'
        //*       'JU3. FIND APP-ILOG-J-RCRD-VIEW -- CURRENT -- JU --'
        //*       #PROD-ID
        //*  -------FINDING THE NEXT JUMPER RANGE TO MAKE CURRENT (UPDATE NEEDED)
        //*  WRITE 'JU4. FIND (1) APP-ILOG-J-RCRD-VIEW'
        //*  --------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-TO-BUCKETS
        //*  WRITE ' ADD-TO-BUCKETS'
        //*  -------MATCH THE PARM BUCKET NAMES TO THE BUCKET RECORD FOR POSITION
        //*  -------------------------------------------------------------
    }
    private void sub_Newrundate() throws Exception                                                                                                                        //Natural: NEWRUNDATE
    {
        if (BLNatReinput.isReinput()) return;

        REPEAT02:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            ldaAppl020.getVw_app_Ilog_J_Rcrd_View().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) APP-ILOG-J-RCRD-VIEW WITH RCRD-NME = #PARAMETER.#REQUESTOR AND RCRD-CDE = 'CP'
            (
            "CP1",
            new Wc[] { new Wc("RCRD_NME", "=", pdaAppa010.getPnd_Parameter_Pnd_Requestor(), "And", WcType.WITH) ,
            new Wc("RCRD_CDE", "=", "CP", WcType.WITH) },
            1
            );
            CP1:
            while (condition(ldaAppl020.getVw_app_Ilog_J_Rcrd_View().readNextRow("CP1", true)))
            {
                ldaAppl020.getVw_app_Ilog_J_Rcrd_View().setIfNotFoundControlFlag(false);
                //*  -------IF NO CURRENT JUMPER RECORD---CRITICAL APPLICATION ERROR
                if (condition(ldaAppl020.getVw_app_Ilog_J_Rcrd_View().getAstCOUNTER().equals(0)))                                                                         //Natural: IF NO RECORDS FOUND
                {
                    pdaAppa010.getPnd_Parameter_Pnd_Get_Contract_Message().setValue("ILOG400 NO CHECKPOINT");                                                             //Natural: MOVE 'ILOG400 NO CHECKPOINT' TO #GET-CONTRACT-MESSAGE
                    pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message().setValue(DbsUtil.compress(pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message(),          //Natural: COMPRESS #OTHER-ACTIVITY-MESSAGE 'CRITICAL ERROR' INTO #OTHER-ACTIVITY-MESSAGE
                        "CRITICAL ERROR"));
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-NOREC
                ldaAppl020.getApp_Ilog_J_Rcrd_View_Previous_Rcrd_Dte().setValue(ldaAppl020.getApp_Ilog_J_Rcrd_View_Current_Rcrd_Dte());                                   //Natural: MOVE CURRENT-RCRD-DTE TO PREVIOUS-RCRD-DTE
                ldaAppl020.getApp_Ilog_J_Rcrd_View_Current_Rcrd_Dte().setValue(pdaAppa010.getPnd_Parameter_Pnd_Start_Date());                                             //Natural: MOVE #PARAMETER.#START-DATE TO CURRENT-RCRD-DTE
                pdaAppa010.getPnd_Parameter_Pnd_Get_Contract_Message().setValue("PUT LAST RUN DT");                                                                       //Natural: MOVE 'PUT LAST RUN DT' TO #GET-CONTRACT-MESSAGE
                ldaAppl020.getVw_app_Ilog_J_Rcrd_View().updateDBRow("CP1");                                                                                               //Natural: UPDATE ( CP1. )
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_First_New_Day() throws Exception                                                                                                                     //Natural: FIRST-NEW-DAY
    {
        if (BLNatReinput.isReinput()) return;

        ldaAppl020.getVw_app_Ilog_J_Rcrd_View().startDatabaseFind                                                                                                         //Natural: FIND APP-ILOG-J-RCRD-VIEW WITH RCRD-NME = 'CURRENT' AND RCRD-CDE = 'JU' AND PRDCT-CDE = #PROD-ID
        (
        "JU0",
        new Wc[] { new Wc("RCRD_NME", "=", "CURRENT", "And", WcType.WITH) ,
        new Wc("RCRD_CDE", "=", "JU", "And", WcType.WITH) ,
        new Wc("PRDCT_CDE", "=", pdaAppa010.getPnd_Parameter_Pnd_Prod_Id(), WcType.WITH) }
        );
        JU0:
        while (condition(ldaAppl020.getVw_app_Ilog_J_Rcrd_View().readNextRow("JU0", true)))
        {
            ldaAppl020.getVw_app_Ilog_J_Rcrd_View().setIfNotFoundControlFlag(false);
            //*  -------IF NO CURRENT JUMPER RECORD---CRITICAL APPLICATION ERROR
            if (condition(ldaAppl020.getVw_app_Ilog_J_Rcrd_View().getAstCOUNTER().equals(0)))                                                                             //Natural: IF NO RECORDS FOUND
            {
                pdaAppa010.getPnd_Parameter_Pnd_Get_Contract_Message().setValue("ILOG445 NO CURRENT JUMPER");                                                             //Natural: MOVE 'ILOG445 NO CURRENT JUMPER' TO #GET-CONTRACT-MESSAGE
                pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message().setValue(DbsUtil.compress(pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message(),              //Natural: COMPRESS #OTHER-ACTIVITY-MESSAGE 'CRITICAL ERROR' INTO #OTHER-ACTIVITY-MESSAGE
                    "CRITICAL ERROR"));
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            //*  -------NO NEED TO MAKE NEW TIMESLICES IF ITS POINTING TO THE DATE
            //*  -------NEXT DATE MUST BE GREATER THAN LAST DATE
            if (condition(pdaAppa010.getPnd_Parameter_Pnd_Start_Date().less(ldaAppl020.getApp_Ilog_J_Rcrd_View_Current_Rcrd_Dte())))                                      //Natural: IF #START-DATE < APP-ILOG-J-RCRD-VIEW.CURRENT-RCRD-DTE
            {
                pdaAppa010.getPnd_Parameter_Pnd_Get_Contract_Message().setValue("ILOG447 PARM INVALID DATE");                                                             //Natural: MOVE 'ILOG447 PARM INVALID DATE' TO #GET-CONTRACT-MESSAGE
                pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message().setValue(DbsUtil.compress(pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message(),              //Natural: COMPRESS #OTHER-ACTIVITY-MESSAGE 'CRITICAL ERROR' INTO #OTHER-ACTIVITY-MESSAGE
                    "CRITICAL ERROR"));
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  JU0.
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(pdaAppa010.getPnd_Parameter_Pnd_Start_Date().equals(ldaAppl020.getApp_Ilog_J_Rcrd_View_Current_Rcrd_Dte())))                                        //Natural: IF #START-DATE = APP-ILOG-J-RCRD-VIEW.CURRENT-RCRD-DTE
        {
            Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                              //Natural: ESCAPE BOTTOM
            if (true) return;
            //*  LS1
        }                                                                                                                                                                 //Natural: END-IF
        //* * ELSE                                                         /* LS1
        //*  TO RETRIEVE ALL BUCKETS AND ACCUMS                            /* LS1
        //*  LS1
        pdaAcia3570.getAcia3570_Parameter().reset();                                                                                                                      //Natural: RESET ACIA3570-PARAMETER
        //*  LS1
        pdaAcia3570.getAcia3570_Parameter_Pnd_P_Get_All_Buckets_Sw().setValue("Y");                                                                                       //Natural: ASSIGN #P-GET-ALL-BUCKETS-SW = 'Y'
        //*  LS1
        DbsUtil.callnat(Acin3570.class , getCurrentProcessState(), pdaAcia3570.getAcia3570_Parameter());                                                                  //Natural: CALLNAT 'ACIN3570' ACIA3570-PARAMETER
        if (condition(Global.isEscape())) return;
        ldaAppl020.getVw_app_Ilog_J_Rcrd_View().startDatabaseFind                                                                                                         //Natural: FIND APP-ILOG-J-RCRD-VIEW WITH RCRD-NME = 'CURRENT' AND RCRD-CDE = 'JU'
        (
        "JU1",
        new Wc[] { new Wc("RCRD_NME", "=", "CURRENT", "And", WcType.WITH) ,
        new Wc("RCRD_CDE", "=", "JU", WcType.WITH) }
        );
        JU1:
        while (condition(ldaAppl020.getVw_app_Ilog_J_Rcrd_View().readNextRow("JU1", true)))
        {
            ldaAppl020.getVw_app_Ilog_J_Rcrd_View().setIfNotFoundControlFlag(false);
            //*  -------IF NO CURRENT JUMPER RECORD---CRITICAL APPLICATION ERROR
            if (condition(ldaAppl020.getVw_app_Ilog_J_Rcrd_View().getAstCOUNTER().equals(0)))                                                                             //Natural: IF NO RECORDS FOUND
            {
                pdaAppa010.getPnd_Parameter_Pnd_Get_Contract_Message().setValue("ILOG405 NO CURRENT JUMPER");                                                             //Natural: MOVE 'ILOG405 NO CURRENT JUMPER' TO #GET-CONTRACT-MESSAGE
                pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message().setValue(DbsUtil.compress(pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message(),              //Natural: COMPRESS #OTHER-ACTIVITY-MESSAGE 'CRITICAL ERROR' INTO #OTHER-ACTIVITY-MESSAGE
                    "CRITICAL ERROR"));
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            //*  ----WE KNOW ITS NOT POINTING TO THE DATE, SO WHY WOULD IT BE TRUE?
            if (condition(pdaAppa010.getPnd_Parameter_Pnd_Start_Date().equals(ldaAppl020.getApp_Ilog_J_Rcrd_View_Current_Rcrd_Dte())))                                    //Natural: IF #START-DATE = CURRENT-RCRD-DTE
            {
                pdaAppa010.getPnd_Parameter_Pnd_Get_Contract_Message().setValue("ILOG444 JU RECS NOT SYNC");                                                              //Natural: MOVE 'ILOG444 JU RECS NOT SYNC' TO #GET-CONTRACT-MESSAGE
                pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message().setValue(DbsUtil.compress(pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message(),              //Natural: COMPRESS #OTHER-ACTIVITY-MESSAGE 'CRITICAL ERROR' INTO #OTHER-ACTIVITY-MESSAGE
                    "CRITICAL ERROR"));
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  -------NEXT DATE MUST BE GREATER THAN LAST DATE
                if (condition(pdaAppa010.getPnd_Parameter_Pnd_Start_Date().less(ldaAppl020.getApp_Ilog_J_Rcrd_View_Current_Rcrd_Dte())))                                  //Natural: IF #START-DATE < CURRENT-RCRD-DTE
                {
                    pdaAppa010.getPnd_Parameter_Pnd_Get_Contract_Message().setValue("ILOG407 PARM INVALID DATE");                                                         //Natural: MOVE 'ILOG407 PARM INVALID DATE' TO #GET-CONTRACT-MESSAGE
                    pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message().setValue(DbsUtil.compress(pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message(),          //Natural: COMPRESS #OTHER-ACTIVITY-MESSAGE 'CRITICAL ERROR' INTO #OTHER-ACTIVITY-MESSAGE
                        "CRITICAL ERROR"));
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                    //*  LS1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    FOR01:                                                                                                                                                //Natural: FOR #J = 1 TO 99
                    for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(99)); pnd_J.nadd(1))
                    {
                        //*  LS1
                        if (condition(pdaAcia3570.getAcia3570_Parameter_Pnd_P_Bucket().getValue(pnd_J).equals(" ")))                                                      //Natural: IF #P-BUCKET ( #J ) = ' '
                        {
                            //*  LS1
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                            //*  LS1
                            //*  LS1
                        }                                                                                                                                                 //Natural: END-IF
                        ldaAppl020.getVw_app_Ilog_B_Rcrd_View().startDatabaseFind                                                                                         //Natural: FIND ( 1 ) APP-ILOG-B-RCRD-VIEW WITH RCRD-CDE = #P-BUCKET ( #J ) AND START-DTE < #START-DATE SORTED BY START-DTE DESCENDING
                        (
                        "BU1",
                        new Wc[] { new Wc("RCRD_CDE", "=", pdaAcia3570.getAcia3570_Parameter_Pnd_P_Bucket().getValue(pnd_J), "And", WcType.WITH) ,
                        new Wc("START_DTE", "<", pdaAppa010.getPnd_Parameter_Pnd_Start_Date(), WcType.WITH) },
                        new Oc[] { new Oc("START_DTE", "DESC") },
                        1
                        );
                        BU1:
                        while (condition(ldaAppl020.getVw_app_Ilog_B_Rcrd_View().readNextRow("BU1", true)))
                        {
                            ldaAppl020.getVw_app_Ilog_B_Rcrd_View().setIfNotFoundControlFlag(false);
                            //*  -------IF NO CURRENT BUCKET RECORD---CRITICAL APPLICATION ERROR
                            if (condition(ldaAppl020.getVw_app_Ilog_B_Rcrd_View().getAstCOUNTER().equals(0)))                                                             //Natural: IF NO RECORDS FOUND
                            {
                                pdaAppa010.getPnd_Parameter_Pnd_Get_Contract_Message().setValue("ILOG410 NO CURRENT BUCKET");                                             //Natural: MOVE 'ILOG410 NO CURRENT BUCKET' TO #GET-CONTRACT-MESSAGE
                                pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message().setValue(DbsUtil.compress(pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message(),  //Natural: COMPRESS #OTHER-ACTIVITY-MESSAGE 'CRITICAL ERROR' INTO #OTHER-ACTIVITY-MESSAGE
                                    "CRITICAL ERROR"));
                                if (true) return;                                                                                                                         //Natural: ESCAPE ROUTINE
                            }                                                                                                                                             //Natural: END-NOREC
                            pnd_B_Isn.setValue(ldaAppl020.getVw_app_Ilog_B_Rcrd_View().getAstISN("BU1"));                                                                 //Natural: MOVE *ISN TO #B-ISN
                            //*  LS1
                        }                                                                                                                                                 //Natural: END-FIND
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        GETBU1:                                                                                                                                           //Natural: GET APP-ILOG-B-RCRD-VIEW #B-ISN
                        ldaAppl020.getVw_app_Ilog_B_Rcrd_View().readByID(pnd_B_Isn.getLong(), "GETBU1");
                        ldaAppl020.getVw_app_Ilog_A_Rcrd_View().startDatabaseFind                                                                                         //Natural: FIND APP-ILOG-A-RCRD-VIEW WITH START-DTE = APP-ILOG-J-RCRD-VIEW.CURRENT-RCRD-DTE AND RCRD-CDE = #P-ACCUM ( #J ) AND PRDCT-CDE = APP-ILOG-J-RCRD-VIEW.PRDCT-CDE
                        (
                        "AC1",
                        new Wc[] { new Wc("START_DTE", "=", ldaAppl020.getApp_Ilog_J_Rcrd_View_Current_Rcrd_Dte(), "And", WcType.WITH) ,
                        new Wc("RCRD_CDE", "=", pdaAcia3570.getAcia3570_Parameter_Pnd_P_Accum().getValue(pnd_J), "And", WcType.WITH) ,
                        new Wc("PRDCT_CDE", "=", ldaAppl020.getApp_Ilog_J_Rcrd_View_Prdct_Cde(), WcType.WITH) }
                        );
                        AC1:
                        while (condition(ldaAppl020.getVw_app_Ilog_A_Rcrd_View().readNextRow("AC1", true)))
                        {
                            ldaAppl020.getVw_app_Ilog_A_Rcrd_View().setIfNotFoundControlFlag(false);
                            //*  -------IF NO CURRENT ACCUM  RECORD---CRITICAL APPLICATION ERROR
                            if (condition(ldaAppl020.getVw_app_Ilog_A_Rcrd_View().getAstCOUNTER().equals(0)))                                                             //Natural: IF NO RECORDS FOUND
                            {
                                pdaAppa010.getPnd_Parameter_Pnd_Get_Contract_Message().setValue("ILOG420 NO CURRENT ACCUM");                                              //Natural: MOVE 'ILOG420 NO CURRENT ACCUM' TO #GET-CONTRACT-MESSAGE
                                pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message().setValue(DbsUtil.compress(pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message(),  //Natural: COMPRESS #OTHER-ACTIVITY-MESSAGE 'CRITICAL ERROR' INTO #OTHER-ACTIVITY-MESSAGE
                                    "CRITICAL ERROR"));
                                if (true) return;                                                                                                                         //Natural: ESCAPE ROUTINE
                            }                                                                                                                                             //Natural: END-NOREC
                            //*  ******* WRITE   APP-ILOG-A-RCRD-VIEW
                            ldaAppl020.getApp_Ilog_A_Rcrd_View_End_Dte().setValue(pdaAppa010.getPnd_Parameter_Pnd_Start_Date());                                          //Natural: MOVE #START-DATE TO END-DTE
                            //*          MOVE TRANSACTION TIME  TO END-TIME
                            ldaAppl020.getApp_Ilog_A_Rcrd_View_Cntrct_Last_Used().setValue(ldaAppl020.getApp_Ilog_J_Rcrd_View_Cntrct_Last_Used());                        //Natural: MOVE APP-ILOG-J-RCRD-VIEW.CNTRCT-LAST-USED TO CNTRCT-LAST-USED
                            ldaAppl020.getVw_app_Ilog_A_Rcrd_View().updateDBRow("AC1");                                                                                   //Natural: UPDATE ( AC1. )
                            //*  -------FORMAT NEXT ACCUM RECORD FOR PRODUCT (STORE NEEDED)--------
                            ldaAppl020.getApp_Ilog_A_Rcrd_View_Buckets().getValue("*").reset();                                                                           //Natural: RESET BUCKETS ( * )
                            ldaAppl020.getApp_Ilog_A_Rcrd_View_Start_Dte().setValue(pdaAppa010.getPnd_Parameter_Pnd_Start_Date());                                        //Natural: MOVE #START-DATE TO START-DTE
                            ldaAppl020.getApp_Ilog_A_Rcrd_View_End_Dte().setValue(" ");                                                                                   //Natural: MOVE ' ' TO END-DTE
                            //*          MOVE TRANSACTION TIME     TO START-TME
                            ldaAppl020.getApp_Ilog_A_Rcrd_View_Rcrd_Revision_Nbr().setValue(ldaAppl020.getApp_Ilog_B_Rcrd_View_Start_Dte());                              //Natural: MOVE APP-ILOG-B-RCRD-VIEW.START-DTE TO RCRD-REVISION-NBR
                            ldaAppl020.getApp_Ilog_A_Rcrd_View_Cntrct_Previous_Rcrd().setValue(ldaAppl020.getApp_Ilog_J_Rcrd_View_Cntrct_Last_Used());                    //Natural: MOVE APP-ILOG-J-RCRD-VIEW.CNTRCT-LAST-USED TO CNTRCT-PREVIOUS-RCRD
                            ldaAppl020.getApp_Ilog_A_Rcrd_View_Cntrct_Last_Used().setValue(" ");                                                                          //Natural: MOVE ' ' TO CNTRCT-LAST-USED
                            ldaAppl020.getVw_app_Ilog_A_Rcrd_View().insertDBRow();                                                                                        //Natural: STORE APP-ILOG-A-RCRD-VIEW
                            //*  (AC1.)
                        }                                                                                                                                                 //Natural: END-FIND
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  (2210)                                        /* LS1
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("JU1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("JU1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  -------FORMAT CURRENT JUMPER TO POINT TO NEW ACCUM (UPDATE NEEDED)
                    ldaAppl020.getApp_Ilog_J_Rcrd_View_Previous_Rcrd_Dte().setValue(ldaAppl020.getApp_Ilog_J_Rcrd_View_Current_Rcrd_Dte());                               //Natural: MOVE CURRENT-RCRD-DTE TO PREVIOUS-RCRD-DTE
                    ldaAppl020.getApp_Ilog_J_Rcrd_View_Current_Rcrd_Dte().setValue(pdaAppa010.getPnd_Parameter_Pnd_Start_Date());                                         //Natural: MOVE #START-DATE TO CURRENT-RCRD-DTE
                    ldaAppl020.getVw_app_Ilog_J_Rcrd_View().updateDBRow("JU1");                                                                                           //Natural: UPDATE ( JU1. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message().setValue("1ST NEW DAY /");                                                                           //Natural: MOVE '1ST NEW DAY /' TO #OTHER-ACTIVITY-MESSAGE
            //*  JU1.
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //* ** END-IF                                                       /* LS1
        Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                                  //Natural: ESCAPE BOTTOM
        if (true) return;
    }
    private void sub_Get_New_Contract() throws Exception                                                                                                                  //Natural: GET-NEW-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        ldaAppl020.getVw_app_Ilog_J_Rcrd_View().startDatabaseFind                                                                                                         //Natural: FIND APP-ILOG-J-RCRD-VIEW WITH RCRD-NME = 'CURRENT' AND RCRD-CDE = 'JU' AND PRDCT-CDE = #PROD-ID
        (
        "JU3",
        new Wc[] { new Wc("RCRD_NME", "=", "CURRENT", "And", WcType.WITH) ,
        new Wc("RCRD_CDE", "=", "JU", "And", WcType.WITH) ,
        new Wc("PRDCT_CDE", "=", pdaAppa010.getPnd_Parameter_Pnd_Prod_Id(), WcType.WITH) }
        );
        JU3:
        while (condition(ldaAppl020.getVw_app_Ilog_J_Rcrd_View().readNextRow("JU3", true)))
        {
            ldaAppl020.getVw_app_Ilog_J_Rcrd_View().setIfNotFoundControlFlag(false);
            //*  -------IF NO CURRENT JUMPER RECORD---CRITICAL APPLICATION ERROR
            if (condition(ldaAppl020.getVw_app_Ilog_J_Rcrd_View().getAstCOUNTER().equals(0)))                                                                             //Natural: IF NO RECORDS FOUND
            {
                pdaAppa010.getPnd_Parameter_Pnd_Get_Contract_Message().setValue("ILOG430 NO CURRENT JUMPER");                                                             //Natural: MOVE 'ILOG430 NO CURRENT JUMPER' TO #GET-CONTRACT-MESSAGE
                pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message().setValue(DbsUtil.compress(pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message(),              //Natural: COMPRESS #OTHER-ACTIVITY-MESSAGE '/CRITICAL ERROR' INTO #OTHER-ACTIVITY-MESSAGE
                    "/CRITICAL ERROR"));
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(pdaAppa010.getPnd_Parameter_Pnd_Get_Contract_Flag().equals("GET CONTRACT")))                                                                    //Natural: IF #GET-CONTRACT-FLAG = 'GET CONTRACT'
            {
                //*  -------GET NEW RANGE RTN DETERMINES IF CURRENT JUMPER RECORD IS
                //*  -------EXHAUSTED AND NEXT RECORD BECOMES CURRENT (UPDATE NEEDED)
                if (condition(ldaAppl020.getApp_Ilog_J_Rcrd_View_Cntrct_Last_Used().equals(ldaAppl020.getApp_Ilog_J_Rcrd_View_Cntrct_Range_End())))                       //Natural: IF CNTRCT-LAST-USED = CNTRCT-RANGE-END
                {
                    ldaAppl020.getApp_Ilog_J_Rcrd_View_Rcrd_Nme().setValue("PREVIOUS");                                                                                   //Natural: MOVE 'PREVIOUS' TO RCRD-NME
                    pnd_Previous_Rcrd_Dte.setValue(ldaAppl020.getApp_Ilog_J_Rcrd_View_Previous_Rcrd_Dte());                                                               //Natural: MOVE PREVIOUS-RCRD-DTE TO #PREVIOUS-RCRD-DTE
                    pnd_Current_Rcrd_Dte.setValue(ldaAppl020.getApp_Ilog_J_Rcrd_View_Current_Rcrd_Dte());                                                                 //Natural: MOVE CURRENT-RCRD-DTE TO #CURRENT-RCRD-DTE
                    //*  WRITE ' XX     UPDATE (JU3.) '
                    ldaAppl020.getVw_app_Ilog_J_Rcrd_View().updateDBRow("JU3");                                                                                           //Natural: UPDATE ( JU3. )
                    ldaAppl020.getVw_app_Ilog_J_Rcrd_View().startDatabaseFind                                                                                             //Natural: FIND ( 1 ) APP-ILOG-J-RCRD-VIEW WITH RCRD-NME = 'NEXT' AND RCRD-CDE = 'JU' AND PRDCT-CDE = #PROD-ID
                    (
                    "JU4",
                    new Wc[] { new Wc("RCRD_NME", "=", "NEXT", "And", WcType.WITH) ,
                    new Wc("RCRD_CDE", "=", "JU", "And", WcType.WITH) ,
                    new Wc("PRDCT_CDE", "=", pdaAppa010.getPnd_Parameter_Pnd_Prod_Id(), WcType.WITH) },
                    1
                    );
                    JU4:
                    while (condition(ldaAppl020.getVw_app_Ilog_J_Rcrd_View().readNextRow("JU4", true)))
                    {
                        ldaAppl020.getVw_app_Ilog_J_Rcrd_View().setIfNotFoundControlFlag(false);
                        //*  -------IF NO NEXT JUMPER    RECORD---CRITICAL APPLICATION ERROR
                        if (condition(ldaAppl020.getVw_app_Ilog_J_Rcrd_View().getAstCOUNTER().equals(0)))                                                                 //Natural: IF NO RECORDS FOUND
                        {
                            pdaAppa010.getPnd_Parameter_Pnd_Get_Contract_Message().setValue("ILOG440 NO NEXT JUMPER");                                                    //Natural: MOVE 'ILOG440 NO NEXT JUMPER' TO #GET-CONTRACT-MESSAGE
                            pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message().setValue(DbsUtil.compress(pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message(),  //Natural: COMPRESS #OTHER-ACTIVITY-MESSAGE '/CRITICAL ERROR' INTO #OTHER-ACTIVITY-MESSAGE
                                "/CRITICAL ERROR"));
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-NOREC
                        ldaAppl020.getApp_Ilog_J_Rcrd_View_Rcrd_Nme().setValue("CURRENT");                                                                                //Natural: MOVE 'CURRENT' TO APP-ILOG-J-RCRD-VIEW.RCRD-NME
                        ldaAppl020.getApp_Ilog_J_Rcrd_View_Cntrct_Last_Used().setValue(ldaAppl020.getApp_Ilog_J_Rcrd_View_Cntrct_Range_Start());                          //Natural: MOVE CNTRCT-RANGE-START TO APP-ILOG-J-RCRD-VIEW.CNTRCT-LAST-USED
                        ldaAppl020.getApp_Ilog_J_Rcrd_View_Previous_Rcrd_Dte().setValue(pnd_Previous_Rcrd_Dte);                                                           //Natural: MOVE #PREVIOUS-RCRD-DTE TO APP-ILOG-J-RCRD-VIEW.PREVIOUS-RCRD-DTE
                        ldaAppl020.getApp_Ilog_J_Rcrd_View_Current_Rcrd_Dte().setValue(pnd_Current_Rcrd_Dte);                                                             //Natural: MOVE #CURRENT-RCRD-DTE TO APP-ILOG-J-RCRD-VIEW.CURRENT-RCRD-DTE
                        //*  WRITE ' UPDATE (JU4.) '
                        ldaAppl020.getVw_app_Ilog_J_Rcrd_View().updateDBRow("JU4");                                                                                       //Natural: UPDATE ( JU4. )
                        pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message().setValue(DbsUtil.compress(pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message(),      //Natural: COMPRESS #OTHER-ACTIVITY-MESSAGE '/GOT NEW RANGE ' INTO #OTHER-ACTIVITY-MESSAGE
                            "/GOT NEW RANGE "));
                    }                                                                                                                                                     //Natural: END-FIND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("JU3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("JU3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  3190
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  ----------INCREMENT RANGE NUMBER ON JUMPER RECORD (UPDATE NEEDED)
                    //*       ADD 1 TO  #CNTRCT-LAST-NUMB
                    //*  ----------INCREMENT RANGE NUMBER ON JUMPER RECORD (UPDATE NEEDED)
                    //*       ADD 1 TO  #CNTRCT-LAST-NUMB  /* EAC (RODGER)
                    //*  EAC (RODGER)
                    pnd_Work_Contract.setValue(ldaAppl020.getApp_Ilog_J_Rcrd_View_Pnd_Cntrct_Last_Numb());                                                                //Natural: MOVE #CNTRCT-LAST-NUMB TO #WORK-CONTRACT
                    //*  JRB1
                    //*  JRB1
                    //*  EAC (RODGER)
                    pnd_Contract_End.setValue(ldaAppl020.getApp_Ilog_J_Rcrd_View_Cntrct_Range_End());                                                                     //Natural: MOVE CNTRCT-RANGE-END TO #CONTRACT-END
                    pnd_Last_Curr_Alpha_Contrct.setValue(true);                                                                                                           //Natural: ASSIGN #LAST-CURR-ALPHA-CONTRCT := TRUE
                    FOR02:                                                                                                                                                //Natural: FOR #I = 1 TO 6
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(6)); pnd_I.nadd(1))
                    {
                        //* ***IF SUBSTR(#CNTRCT-LAST-NUMB,#I,1) NE MASK (N)     /* EAC (RODGER)
                        //* ***  ESCAPE BOTTOM                                   /* EAC (RODGER)
                        //* ***END-IF                                            /* EAC (RODGER)
                        //*  JRB1
                        //*  JRB1
                        short decideConditionsMet634 = 0;                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #WORK-CHAR ( #I ) = MASK ( A )
                        if (condition(DbsUtil.maskMatches(pnd_Work_Contract_Pnd_Work_Char.getValue(pnd_I),"A")))
                        {
                            decideConditionsMet634++;
                            //*  JRB1
                            //*  JRB1
                            if (condition(pnd_I.greaterOrEqual(4)))                                                                                                       //Natural: IF #I GE 4
                            {
                                pnd_Alpha_Sub.setValue(pnd_I);                                                                                                            //Natural: ASSIGN #ALPHA-SUB := #I
                                //*  JRB1
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                                //*  JRB1
                                //*  JRB1
                                //*  JRB1
                                //*  JRB1
                                //*  JRB1
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: WHEN #WORK-CHAR ( #I ) NE #END-CHAR ( #I )
                        else if (condition(pnd_Work_Contract_Pnd_Work_Char.getValue(pnd_I).notEquals(pnd_Contract_End_Pnd_End_Char.getValue(pnd_I))))
                        {
                            decideConditionsMet634++;
                            pnd_Last_Curr_Alpha_Contrct.setValue(false);                                                                                                  //Natural: ASSIGN #LAST-CURR-ALPHA-CONTRCT := FALSE
                        }                                                                                                                                                 //Natural: WHEN NONE
                        else if (condition())
                        {
                            ignore();
                            //*  JRB1
                        }                                                                                                                                                 //Natural: END-DECIDE
                        //*  3640                                     /* EAC (RODGER)
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("JU3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("JU3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  JRB1
                    //*  JRB1
                    //*  JRB1
                    if (condition(pnd_Last_Curr_Alpha_Contrct.getBoolean()))                                                                                              //Natural: IF #LAST-CURR-ALPHA-CONTRCT
                    {
                        pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Contract().setValue(ldaAppl020.getApp_Ilog_J_Rcrd_View_Cntrct_Range_Start());                               //Natural: ASSIGN #APPA2075.#P-BEG-CONTRACT := CNTRCT-RANGE-START
                        pdaAppa2075.getPnd_Appa2075_Pnd_P_End_Contract().setValue(ldaAppl020.getApp_Ilog_J_Rcrd_View_Cntrct_Range_End());                                 //Natural: ASSIGN #APPA2075.#P-END-CONTRACT := CNTRCT-RANGE-END
                        //*  JRB1
                        //*  JRB1
                        //*  JRB1
                        //*  JRB1
                        //*  JRB1
                        //*  JRB1
                        //*  JRB1
                        //*  JRB1
                        //*  ALL NUMERIC                        /* JRB1
                        short decideConditionsMet671 = 0;                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #ALPHA-SUB;//Natural: VALUE 4
                        if (condition((pnd_Alpha_Sub.equals(4))))
                        {
                            decideConditionsMet671++;
                            pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Pos_5_7().setValue(pnd_Work_Contract_Pnd_Work_Alpha_Pos_5_7);                                           //Natural: ASSIGN #P-BEG-POS-5-7 := #WORK-ALPHA-POS-5-7
                        }                                                                                                                                                 //Natural: VALUE 5
                        else if (condition((pnd_Alpha_Sub.equals(5))))
                        {
                            decideConditionsMet671++;
                            pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Pos_6_7().setValue(pnd_Work_Contract_Pnd_Work_Alpha_Pos_6_7);                                           //Natural: ASSIGN #P-BEG-POS-6-7 := #WORK-ALPHA-POS-6-7
                        }                                                                                                                                                 //Natural: VALUE 6
                        else if (condition((pnd_Alpha_Sub.equals(6))))
                        {
                            decideConditionsMet671++;
                            pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Pos_7().setValue(pnd_Work_Contract_Pnd_Work_Alpha_Pos_7);                                               //Natural: ASSIGN #P-BEG-POS-7 := #WORK-ALPHA-POS-7
                        }                                                                                                                                                 //Natural: NONE
                        else if (condition())
                        {
                            ignore();
                            //*  JRB1
                        }                                                                                                                                                 //Natural: END-DECIDE
                        //*  JRB1
                        DbsUtil.callnat(Appn2075.class , getCurrentProcessState(), pdaAppa2075.getPnd_Appa2075());                                                        //Natural: CALLNAT 'APPN2075' #APPA2075
                        if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                        //*  JRB1
                        //*  JRB1
                        if (condition(pdaAppa2075.getPnd_Appa2075_Pnd_P_Return_Code().equals(getZero())))                                                                 //Natural: IF #APPA2075.#P-RETURN-CODE = 0
                        {
                            ldaAppl020.getApp_Ilog_J_Rcrd_View_Cntrct_Last_Used().setValue(pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Contract());                             //Natural: ASSIGN CNTRCT-LAST-USED := #APPA2075.#P-BEG-CONTRACT
                            //*  3940                                     /* JRB1
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  JRB1
                            //*  JRB1
                            pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message().setValue(DbsUtil.compress(pdaAppa2075.getPnd_Appa2075_Pnd_P_Return_Message(),        //Natural: COMPRESS #APPA2075.#P-RETURN-MESSAGE 'CRITICAL ERROR' INTO #OTHER-ACTIVITY-MESSAGE
                                "CRITICAL ERROR"));
                            //*  JRB1
                            DbsUtil.terminateApplication("External Subroutine pnd_ERROR is missing from the collection!");                                                //Natural: PERFORM #ERROR
                            //*  JRB1
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                            //*  3940                                   /* JRB1
                        }                                                                                                                                                 //Natural: END-IF
                        //*  3800                                        /* JRB1
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  JRB1
                        //*  EAC (RODGER)
                        short decideConditionsMet703 = 0;                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #ALPHA-SUB;//Natural: VALUE 4
                        if (condition((pnd_Alpha_Sub.equals(4))))
                        {
                            decideConditionsMet703++;
                            //*  EAC (RODGER VR)
                            //*  EAC (RODGER)
                            pnd_Work_Contract_Pnd_Work_Contract_N3.nadd(1);                                                                                               //Natural: ADD 1 TO #WORK-CONTRACT-N3
                        }                                                                                                                                                 //Natural: VALUE 5
                        else if (condition((pnd_Alpha_Sub.equals(5))))
                        {
                            decideConditionsMet703++;
                            //*  EAC (RODGER)
                            //*  EAC (RODGER)
                            pnd_Work_Contract_Pnd_Work_Contract_N4.nadd(1);                                                                                               //Natural: ADD 1 TO #WORK-CONTRACT-N4
                        }                                                                                                                                                 //Natural: VALUE 6
                        else if (condition((pnd_Alpha_Sub.equals(6))))
                        {
                            decideConditionsMet703++;
                            //*  EAC (RODGER)
                            //*  EAC (RODGER)
                            pnd_Work_Contract_Pnd_Work_Contract_N5.nadd(1);                                                                                               //Natural: ADD 1 TO #WORK-CONTRACT-N5
                        }                                                                                                                                                 //Natural: NONE
                        else if (condition())
                        {
                            //*  EAC (RODGER)
                            pnd_Work_Contract_Pnd_Work_Contract_N6.nadd(1);                                                                                               //Natural: ADD 1 TO #WORK-CONTRACT-N6
                            //*  EAC (RODGER)
                        }                                                                                                                                                 //Natural: END-DECIDE
                        //*  EAC (RODGER)
                        ldaAppl020.getApp_Ilog_J_Rcrd_View_Pnd_Cntrct_Last_Numb().setValue(pnd_Work_Contract);                                                            //Natural: MOVE #WORK-CONTRACT TO #CNTRCT-LAST-NUMB
                        //*  3800                                      /* JRB1
                    }                                                                                                                                                     //Natural: END-IF
                    //* *
                    //*  WRITE 'UPDATE (JU3.)'
                    ldaAppl020.getVw_app_Ilog_J_Rcrd_View().updateDBRow("JU3");                                                                                           //Natural: UPDATE ( JU3. )
                    //*  3190
                }                                                                                                                                                         //Natural: END-IF
                //*  ----------CALL CHECK DIGIT RTN DEPENDING ON ONLINE/BATCH PROCESSING
                nat2_Parm_Ppcn_Parm_List_Ppcn.setValue(ldaAppl020.getApp_Ilog_J_Rcrd_View_Cntrct_Last_Used());                                                            //Natural: MOVE CNTRCT-LAST-USED TO PARM-LIST-PPCN
                if (condition(Global.getTPSYS().equals(" ")))                                                                                                             //Natural: IF *TPSYS = ' '
                {
                    chekdigReturnCode = DbsUtil.callExternalProgram("CHEKDIG",nat2_Parm_Ppcn_Parm_List_Ppcn,nat2_Parm_Ppcn_Parm_List_Chkdig);                             //Natural: CALL 'CHEKDIG' PARM-LIST-PPCN PARM-LIST-CHKDIG
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    cda0670ReturnCode = DbsUtil.callExternalProgram("CDA0670",nat2_Parm_Ppcn_Parm_List_Ppcn,pnd_Dummy_Parm);                                              //Natural: CALL 'CDA0670' PARM-LIST-PPCN #DUMMY-PARM
                }                                                                                                                                                         //Natural: END-IF
                pdaAppa010.getPnd_Parameter_Pnd_Contr_Gotten_T().setValue(nat2_Parm_Ppcn);                                                                                //Natural: MOVE NAT2-PARM-PPCN TO #CONTR-GOTTEN-T
                pdaAppa010.getPnd_Parameter_Pnd_Contr_Gotten_C().setValue(nat2_Parm_Ppcn);                                                                                //Natural: MOVE NAT2-PARM-PPCN TO #CONTR-GOTTEN-C
                pdaAppa010.getPnd_Parameter_Pnd_Contr_Gotten_C_Pref().setValue(ldaAppl020.getApp_Ilog_J_Rcrd_View_Cntrct_Cref_Pref());                                    //Natural: MOVE CNTRCT-CREF-PREF TO #CONTR-GOTTEN-C-PREF
                nat2_Parm_Ppcn_Parm_List_Ppcn.setValue(pdaAppa010.getPnd_Parameter_Pnd_Contr_Gotten_C());                                                                 //Natural: MOVE #CONTR-GOTTEN-C TO PARM-LIST-PPCN
                if (condition(Global.getTPSYS().equals(" ")))                                                                                                             //Natural: IF *TPSYS = ' '
                {
                    chekdigReturnCode = DbsUtil.callExternalProgram("CHEKDIG",nat2_Parm_Ppcn_Parm_List_Ppcn,nat2_Parm_Ppcn_Parm_List_Chkdig);                             //Natural: CALL 'CHEKDIG' PARM-LIST-PPCN PARM-LIST-CHKDIG
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    cda0670ReturnCode = DbsUtil.callExternalProgram("CDA0670",nat2_Parm_Ppcn_Parm_List_Ppcn,pnd_Dummy_Parm);                                              //Natural: CALL 'CDA0670' PARM-LIST-PPCN #DUMMY-PARM
                }                                                                                                                                                         //Natural: END-IF
                pdaAppa010.getPnd_Parameter_Pnd_Contr_Gotten_C().setValue(nat2_Parm_Ppcn);                                                                                //Natural: MOVE NAT2-PARM-PPCN TO #CONTR-GOTTEN-C
                pdaAppa010.getPnd_Parameter_Pnd_Get_Contract_Message().setValue("CONTRACT GOTTEN");                                                                       //Natural: MOVE 'CONTRACT GOTTEN' TO #GET-CONTRACT-MESSAGE
                //*  3140
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAppa010.getPnd_Parameter_Pnd_Get_Contract_Message().setValue("NO CONTRACT REQUESTED");                                                                 //Natural: MOVE 'NO CONTRACT REQUESTED' TO #GET-CONTRACT-MESSAGE
                //*  3140
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                                  //Natural: ESCAPE BOTTOM
        if (true) return;
    }
    private void sub_Add_To_Buckets() throws Exception                                                                                                                    //Natural: ADD-TO-BUCKETS
    {
        if (BLNatReinput.isReinput()) return;

        ldaAppl020.getVw_app_Ilog_J_Rcrd_View().startDatabaseFind                                                                                                         //Natural: FIND APP-ILOG-J-RCRD-VIEW WITH RCRD-NME = 'CURRENT' AND RCRD-CDE = 'JU' AND PRDCT-CDE = #PROD-ID
        (
        "JU5",
        new Wc[] { new Wc("RCRD_NME", "=", "CURRENT", "And", WcType.WITH) ,
        new Wc("RCRD_CDE", "=", "JU", "And", WcType.WITH) ,
        new Wc("PRDCT_CDE", "=", pdaAppa010.getPnd_Parameter_Pnd_Prod_Id(), WcType.WITH) }
        );
        JU5:
        while (condition(ldaAppl020.getVw_app_Ilog_J_Rcrd_View().readNextRow("JU5", true)))
        {
            ldaAppl020.getVw_app_Ilog_J_Rcrd_View().setIfNotFoundControlFlag(false);
            //*  -------IF NO CURRENT JUMPER RECORD---CRITICAL APPLICATION ERROR
            if (condition(ldaAppl020.getVw_app_Ilog_J_Rcrd_View().getAstCOUNTER().equals(0)))                                                                             //Natural: IF NO RECORDS FOUND
            {
                pdaAppa010.getPnd_Parameter_Pnd_Add_To_Buckets_Message().setValue("ILOG450 NO CURRENT JUMPER");                                                           //Natural: MOVE 'ILOG450 NO CURRENT JUMPER' TO #ADD-TO-BUCKETS-MESSAGE
                pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message().setValue(DbsUtil.compress(pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message(),              //Natural: COMPRESS #OTHER-ACTIVITY-MESSAGE '/CRITICAL ERROR' INTO #OTHER-ACTIVITY-MESSAGE
                    "/CRITICAL ERROR"));
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(pdaAppa010.getPnd_Parameter_Pnd_Add_To_Buckets_Flag().equals("ADD TO BUCKETS")))                                                                //Natural: IF #ADD-TO-BUCKETS-FLAG = 'ADD TO BUCKETS'
            {
                //*  -------FORMAT THE ACCUM KEY  TO FIND CURRENT RELEVENT FOR PRODUCT
                ldaAppl020.getAapnd_Rcrd_Key_Aapnd_Start_Dte().setValue(ldaAppl020.getApp_Ilog_J_Rcrd_View_Current_Rcrd_Dte());                                           //Natural: MOVE APP-ILOG-J-RCRD-VIEW.CURRENT-RCRD-DTE TO AA#START-DTE
                //*  LS1
                pnd_Wk_Bucket_Cde.setValue(pdaAppa010.getPnd_Parameter_Pnd_P_Bucket_Code());                                                                              //Natural: MOVE #P-BUCKET-CODE TO #WK-BUCKET-CDE
                //*  LS1
                ldaAppl020.getAapnd_Rcrd_Key_Aapnd_Rcrd_Cde().setValue(pdaAppa010.getPnd_Parameter_Pnd_P_Accum_Code());                                                   //Natural: MOVE #P-ACCUM-CODE TO AA#RCRD-CDE
                ldaAppl020.getAapnd_Rcrd_Key_Aapnd_Rcrd_Nme().setValue(pdaAppa010.getPnd_Parameter_Pnd_Requestor());                                                      //Natural: MOVE #REQUESTOR TO AA#RCRD-NME
                ldaAppl020.getAapnd_Rcrd_Key_Aapnd_Prdct_Cde().setValue(pdaAppa010.getPnd_Parameter_Pnd_Prod_Id());                                                       //Natural: MOVE #PROD-ID TO AA#PRDCT-CDE
                ldaAppl020.getVw_app_Ilog_A_Rcrd_View().startDatabaseFind                                                                                                 //Natural: FIND ( 1 ) APP-ILOG-A-RCRD-VIEW WITH RCRD-KEY = AA#RCRD-KEY
                (
                "AC2",
                new Wc[] { new Wc("RCRD_KEY", "=", ldaAppl020.getAapnd_Rcrd_Key(), WcType.WITH) },
                1
                );
                AC2:
                while (condition(ldaAppl020.getVw_app_Ilog_A_Rcrd_View().readNextRow("AC2", true)))
                {
                    ldaAppl020.getVw_app_Ilog_A_Rcrd_View().setIfNotFoundControlFlag(false);
                    //*  -------IF NO CURRENT ACCUM  RECORD---CRITICAL APPLICATION ERROR
                    if (condition(ldaAppl020.getVw_app_Ilog_A_Rcrd_View().getAstCOUNTER().equals(0)))                                                                     //Natural: IF NO RECORDS FOUND
                    {
                        pdaAppa010.getPnd_Parameter_Pnd_Add_To_Buckets_Message().setValue("ILOG470 NO CURRENT ACCUM");                                                    //Natural: MOVE 'ILOG470 NO CURRENT ACCUM' TO #ADD-TO-BUCKETS-MESSAGE
                        pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message().setValue(DbsUtil.compress(pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message(),      //Natural: COMPRESS #OTHER-ACTIVITY-MESSAGE '/CRITICAL ERROR' INTO #OTHER-ACTIVITY-MESSAGE
                            "/CRITICAL ERROR"));
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-NOREC
                    //*  -------GET THE RELEVENT BUCKET DESCRIPTIONS--------------------
                    //*  LS1
                    ldaAppl020.getVw_app_Ilog_B_Rcrd_View().startDatabaseFind                                                                                             //Natural: FIND APP-ILOG-B-RCRD-VIEW WITH START-DTE = APP-ILOG-A-RCRD-VIEW.RCRD-REVISION-NBR AND RCRD-CDE = #WK-BUCKET-CDE
                    (
                    "FIND01",
                    new Wc[] { new Wc("START_DTE", "=", ldaAppl020.getApp_Ilog_A_Rcrd_View_Rcrd_Revision_Nbr(), "And", WcType.WITH) ,
                    new Wc("RCRD_CDE", "=", pnd_Wk_Bucket_Cde, WcType.WITH) }
                    );
                    FIND01:
                    while (condition(ldaAppl020.getVw_app_Ilog_B_Rcrd_View().readNextRow("FIND01", true)))
                    {
                        ldaAppl020.getVw_app_Ilog_B_Rcrd_View().setIfNotFoundControlFlag(false);
                        //* *        AND RCRD-CDE = 'BU'
                        //*  -------IF NO CURRENT BUCKET RECORD---CRITICAL APPLICATION ERROR
                        if (condition(ldaAppl020.getVw_app_Ilog_B_Rcrd_View().getAstCOUNTER().equals(0)))                                                                 //Natural: IF NO RECORDS FOUND
                        {
                            pdaAppa010.getPnd_Parameter_Pnd_Add_To_Buckets_Message().setValue("ILOG480 NO CURRENT BUCKET");                                               //Natural: MOVE 'ILOG480 NO CURRENT BUCKET' TO #ADD-TO-BUCKETS-MESSAGE
                            pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message().setValue(DbsUtil.compress(pdaAppa010.getPnd_Parameter_Pnd_Other_Activity_Message(),  //Natural: COMPRESS #OTHER-ACTIVITY-MESSAGE '/CRITICAL ERROR' INTO #OTHER-ACTIVITY-MESSAGE
                                "/CRITICAL ERROR"));
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-NOREC
                        pnd_B_Isn.setValue(ldaAppl020.getVw_app_Ilog_B_Rcrd_View().getAstISN("Find01"));                                                                  //Natural: MOVE *ISN TO #B-ISN
                    }                                                                                                                                                     //Natural: END-FIND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("AC2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("AC2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    GETBU2:                                                                                                                                               //Natural: GET APP-ILOG-B-RCRD-VIEW #B-ISN
                    ldaAppl020.getVw_app_Ilog_B_Rcrd_View().readByID(pnd_B_Isn.getLong(), "GETBU2");
                    FOR03:                                                                                                                                                //Natural: FOR #ADDBUCK-VAL 1 APP-ILOG-B-RCRD-VIEW.C*BUCKETS-GRP
                    for (ldaAppl020.getPnd_Addbuck_Val().setValue(1); condition(ldaAppl020.getPnd_Addbuck_Val().lessOrEqual(ldaAppl020.getApp_Ilog_B_Rcrd_View_Count_Castbuckets_Grp())); 
                        ldaAppl020.getPnd_Addbuck_Val().nadd(1))
                    {
                        DbsUtil.examine(new ExamineSource(ldaAppl020.getApp_Ilog_B_Rcrd_View_Buckets_Nme().getValue("*")), new ExamineSearch(pdaAppa010.getPnd_Parameter_Pnd_Each_Bucket_Name().getValue(ldaAppl020.getPnd_Addbuck_Val())),  //Natural: EXAMINE APP-ILOG-B-RCRD-VIEW.BUCKETS-NME ( * ) FOR #EACH-BUCKET-NAME ( #ADDBUCK-VAL ) GIVING INDEX #BUCK-VAL
                            new ExamineGivingIndex(ldaAppl020.getPnd_Buck_Val()));
                        if (condition(ldaAppl020.getPnd_Buck_Val().notEquals(getZero())))                                                                                 //Natural: IF #BUCK-VAL NOT = 0
                        {
                            ldaAppl020.getApp_Ilog_A_Rcrd_View_Buckets().getValue(ldaAppl020.getPnd_Buck_Val()).nadd(pdaAppa010.getPnd_Parameter_Pnd_Each_Tally_Value().getValue(ldaAppl020.getPnd_Addbuck_Val())); //Natural: ADD #EACH-TALLY-VALUE ( #ADDBUCK-VAL ) TO BUCKETS ( #BUCK-VAL )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("AC2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("AC2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  -------DUMP TOKENS INTO RELEVENT BUCKETS IN ACCUM RECORD-(UPDATE)---
                    //*        MOVE JU5.CNTRCT-LAST-USED TO AC2.CNTRCT-LAST-USED
                    //*          MOVE TRANSACTION TIME     TO END-TIME
                    ldaAppl020.getVw_app_Ilog_A_Rcrd_View().updateDBRow("AC2");                                                                                           //Natural: UPDATE ( AC2. )
                    pdaAppa010.getPnd_Parameter_Pnd_Add_To_Buckets_Message().setValue("BUCKETS ADDED");                                                                   //Natural: MOVE 'BUCKETS ADDED' TO #ADD-TO-BUCKETS-MESSAGE
                    //*      END-FIND
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("JU5"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("JU5"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAppa010.getPnd_Parameter_Pnd_Add_To_Buckets_Message().setValue("NO BUCKETS REQUESTED");                                                                //Natural: MOVE 'NO BUCKETS REQUESTED' TO #ADD-TO-BUCKETS-MESSAGE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                                  //Natural: ESCAPE BOTTOM
        if (true) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //*  WRITE *ERROR-NR #RETRY-COUNT
        if (condition(Global.getERROR_NR().equals(3145)))                                                                                                                 //Natural: IF *ERROR-NR = 3145
        {
            if (condition(pnd_Retry_Count.greater(200)))                                                                                                                  //Natural: IF #RETRY-COUNT > 200
            {
                getReports().write(0, "TRIED TO ACCESS RECORD 200 TIMES, FAILED");                                                                                        //Natural: WRITE 'TRIED TO ACCESS RECORD 200 TIMES, FAILED'
                DbsUtil.terminate();  if (true) return;                                                                                                                   //Natural: TERMINATE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Retry_Count.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #RETRY-COUNT
                //*  STOP
                OnErrorManager.popRetry();                                                                                                                                //Natural: RETRY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    };                                                                                                                                                                    //Natural: END-ERROR
}
