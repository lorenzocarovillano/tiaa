/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:10:53 AM
**        * FROM NATURAL SUBPROGRAM : Scin9913
************************************************************
**        * FILE NAME            : Scin9913.java
**        * CLASS NAME           : Scin9913
**        * INSTANCE NAME        : Scin9913
************************************************************
************************************************************************
* PROGRAM ID:     SCIN9913
* AUTHOR:         CHRISTOPHER LAEVEY
* DATE CREATED:   7/18/2013
* BUSINESS GROUP: ACIS
* FUNCTION:       CALLED BY SCIB9000 TO ADD BENEFICIARY DATA FOR NEW
*                 CONTRACTS IF AND ONLY IF A IRA SUB CONTRACT TO BE
*                 CREATED, TOO.  CALLS BENN970 TO ACCESS BENE API
**----------------------------------------------------------------------
* CHANGE HISTORY:   (MOST RECENT AT BOTTOM)
* IMPL
* DATE    CHANGED                                                 CHANGE
* YY/MM      BY      CHANGE DESCRIPTION                             TAG
**-----  ----------  -------------------------------------------  ------
* 13/07  LAEVEY      INITIAL
* 13/07  ELLO        RESTOW FOR SCIA1200 FOR NEW BENE FIELDS
*                    FOR NY200.
* 13/10  ELLO        RESTOW FOR MT. SINAI
* 15/09  SHU         RESTOW FOR SCIA1200 FOR BENE IN THE PLAN
* 16/02  SHU         POPULATE BENE NAME 2                          BNL2
* 16/07  NEWSOM      REMOVED POPULATING THE BENE RELATIONSHIP CODE RPRC
* 05/23/17 MUKHR     PIN EXPANSION CHANGES (STOW ONLY) CHG425939
* 11/16/19 SHUL      RESTOW FOR SCIA1200                        ISSG
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Scin9913 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaScia1200 pdaScia1200;
    private PdaBena970 pdaBena970;
    private PdaBenpda_M pdaBenpda_M;
    private PdaBenpda_E pdaBenpda_E;
    private LdaAcil3010 ldaAcil3010;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Scin9913_Return_Code;
    private DbsField pnd_Scin9913_Return_Msg;
    private DbsField pnd_Today;

    private DbsGroup pnd_Today__R_Field_1;
    private DbsField pnd_Today_Pnd_Today_N;
    private DbsField i;
    private DbsField pnd_Pct_A;

    private DbsGroup pnd_Pct_A__R_Field_2;
    private DbsField pnd_Pct_A_Pnd_Pct;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaBena970 = new PdaBena970(localVariables);
        pdaBenpda_M = new PdaBenpda_M(localVariables);
        pdaBenpda_E = new PdaBenpda_E(localVariables);
        ldaAcil3010 = new LdaAcil3010();
        registerRecord(ldaAcil3010);
        registerRecord(ldaAcil3010.getVw_acis_Bene_File());

        // parameters
        parameters = new DbsRecord();
        pdaScia1200 = new PdaScia1200(parameters);
        pnd_Scin9913_Return_Code = parameters.newFieldInRecord("pnd_Scin9913_Return_Code", "#SCIN9913-RETURN-CODE", FieldType.STRING, 4);
        pnd_Scin9913_Return_Code.setParameterOption(ParameterOption.ByReference);
        pnd_Scin9913_Return_Msg = parameters.newFieldInRecord("pnd_Scin9913_Return_Msg", "#SCIN9913-RETURN-MSG", FieldType.STRING, 80);
        pnd_Scin9913_Return_Msg.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Today = localVariables.newFieldInRecord("pnd_Today", "#TODAY", FieldType.STRING, 8);

        pnd_Today__R_Field_1 = localVariables.newGroupInRecord("pnd_Today__R_Field_1", "REDEFINE", pnd_Today);
        pnd_Today_Pnd_Today_N = pnd_Today__R_Field_1.newFieldInGroup("pnd_Today_Pnd_Today_N", "#TODAY-N", FieldType.NUMERIC, 8);
        i = localVariables.newFieldInRecord("i", "I", FieldType.PACKED_DECIMAL, 3);
        pnd_Pct_A = localVariables.newFieldInRecord("pnd_Pct_A", "#PCT-A", FieldType.STRING, 5);

        pnd_Pct_A__R_Field_2 = localVariables.newGroupInRecord("pnd_Pct_A__R_Field_2", "REDEFINE", pnd_Pct_A);
        pnd_Pct_A_Pnd_Pct = pnd_Pct_A__R_Field_2.newFieldInGroup("pnd_Pct_A_Pnd_Pct", "#PCT", FieldType.NUMERIC, 5, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAcil3010.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Scin9913() throws Exception
    {
        super("Scin9913");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        ldaAcil3010.getVw_acis_Bene_File().startDatabaseFind                                                                                                              //Natural: FIND ACIS-BENE-FILE WITH BENE-TIAA-NBR = #TIAA-CONTRACT-NO
        (
        "FIND01",
        new Wc[] { new Wc("BENE_TIAA_NBR", "=", pdaScia1200.getScia1200_Pnd_Tiaa_Contract_No(), WcType.WITH) }
        );
        FIND01:
        while (condition(ldaAcil3010.getVw_acis_Bene_File().readNextRow("FIND01", true)))
        {
            ldaAcil3010.getVw_acis_Bene_File().setIfNotFoundControlFlag(false);
            if (condition(ldaAcil3010.getVw_acis_Bene_File().getAstCOUNTER().equals(0)))                                                                                  //Natural: IF NO RECORD FOUND
            {
                pnd_Scin9913_Return_Code.setValue("9913");                                                                                                                //Natural: ASSIGN #SCIN9913-RETURN-CODE = '9913'
                pnd_Scin9913_Return_Msg.setValue(DbsUtil.compress("No record on ACIS BENE file for", pdaScia1200.getScia1200_Pnd_Tiaa_Contract_No()));                    //Natural: COMPRESS 'No record on ACIS BENE file for' #TIAA-CONTRACT-NO INTO #SCIN9913-RETURN-MSG
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        DbsUtil.callnat(Benn9831.class , getCurrentProcessState(), pnd_Today, pdaBenpda_M.getMsg_Info_Sub(), pdaBenpda_E.getError_Info_Sub());                            //Natural: CALLNAT 'BENN9831' #TODAY MSG-INFO-SUB ERROR-INFO-SUB
        if (condition(Global.isEscape())) return;
        if (condition(pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().notEquals(getZero())))                                                                                 //Natural: IF ##MSG-NR NE 0
        {
            pnd_Scin9913_Return_Code.setValue(pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr());                                                                              //Natural: ASSIGN #SCIN9913-RETURN-CODE = ##MSG-NR
            pnd_Scin9913_Return_Msg.setValue(pdaBenpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                  //Natural: ASSIGN #SCIN9913-RETURN-MSG = ##MSG
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaBena970.getBena970_Pnd_Intrfce_Bsnss_Dte().setValue(pnd_Today_Pnd_Today_N);                                                                                    //Natural: ASSIGN #INTRFCE-BSNSS-DTE := #TODAY-N
        pdaBena970.getBena970_Pnd_Intrfcng_Systm().setValue("ACIS");                                                                                                      //Natural: ASSIGN #INTRFCNG-SYSTM := 'ACIS'
        pdaBena970.getBena970_Pnd_Rqstng_System().setValue("ACIS");                                                                                                       //Natural: ASSIGN #RQSTNG-SYSTEM := 'ACIS'
        pdaBena970.getBena970_Pnd_New_Issuefslash_Chng_Ind().setValue("N");                                                                                               //Natural: ASSIGN #NEW-ISSUE/CHNG-IND := 'N'
        pdaBena970.getBena970_Pnd_Intrfce_Mgrtn_Ind().setValue("I");                                                                                                      //Natural: ASSIGN #INTRFCE-MGRTN-IND := 'I'
        pdaBena970.getBena970_Pnd_Pin().compute(new ComputeParameters(false, pdaBena970.getBena970_Pnd_Pin()), pdaScia1200.getScia1200_Pnd_Pin_Nbr().val());              //Natural: ASSIGN #PIN := VAL ( #PIN-NBR )
        pdaBena970.getBena970_Pnd_Tiaa_Cntrct().setValue(pdaScia1200.getScia1200_Pnd_Tiaa_Contract_No());                                                                 //Natural: ASSIGN #TIAA-CNTRCT := #TIAA-CONTRACT-NO
        pdaBena970.getBena970_Pnd_Cref_Cntrct().setValue(pdaScia1200.getScia1200_Pnd_Cref_Certificate_No());                                                              //Natural: ASSIGN #CREF-CNTRCT := #CREF-CERTIFICATE-NO
        pdaBena970.getBena970_Pnd_Cntrct_Type().setValue("D");                                                                                                            //Natural: ASSIGN #CNTRCT-TYPE := 'D'
        if (condition(ldaAcil3010.getAcis_Bene_File_Bene_Tiaa_Cref_Ind().equals("B")))                                                                                    //Natural: IF ACIS-BENE-FILE.BENE-TIAA-CREF-IND = 'B'
        {
            pdaBena970.getBena970_Pnd_Tiaa_Cref_Ind().setValue(" ");                                                                                                      //Natural: ASSIGN #TIAA-CREF-IND := ' '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaBena970.getBena970_Pnd_Tiaa_Cref_Ind().setValue(ldaAcil3010.getAcis_Bene_File_Bene_Tiaa_Cref_Ind());                                                       //Natural: ASSIGN #TIAA-CREF-IND := ACIS-BENE-FILE.BENE-TIAA-CREF-IND
        }                                                                                                                                                                 //Natural: END-IF
        pdaBena970.getBena970_Pnd_Mos_Ind().setValue(ldaAcil3010.getAcis_Bene_File_Bene_Mos_Ind());                                                                       //Natural: ASSIGN #MOS-IND := ACIS-BENE-FILE.BENE-MOS-IND
        pdaBena970.getBena970_Pnd_Effctve_Dte().setValueEdited(ldaAcil3010.getAcis_Bene_File_Bene_Effective_Dt(),new ReportEditMask("YYYYMMDD"));                         //Natural: MOVE EDITED ACIS-BENE-FILE.BENE-EFFECTIVE-DT ( EM = YYYYMMDD ) TO #EFFCTVE-DTE
        if (condition(ldaAcil3010.getAcis_Bene_File_Bene_Mos_Ind().equals("Y")))                                                                                          //Natural: IF ACIS-BENE-FILE.BENE-MOS-IND EQ 'Y'
        {
            pdaBena970.getBena970_Pnd_Mos_Ind().setValue("Y");                                                                                                            //Natural: ASSIGN #MOS-IND := 'Y'
            pdaBena970.getBena970_Pnd_Mos_Txt().getValue("*").setValue(ldaAcil3010.getAcis_Bene_File_Bene_Spcl_Dsgn_Txt().getValue("*"));                                 //Natural: ASSIGN #MOS-TXT ( * ) := ACIS-BENE-FILE.BENE-SPCL-DSGN-TXT ( * )
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAcil3010.getAcis_Bene_File_Bene_Category().equals("2")))                                                                                         //Natural: IF ACIS-BENE-FILE.BENE-CATEGORY EQ '2'
        {
            pdaBena970.getBena970_Pnd_More_Than_Five_Benes_Ind().setValue("Y");                                                                                           //Natural: ASSIGN #MORE-THAN-FIVE-BENES-IND := 'Y'
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAcil3010.getAcis_Bene_File_Bene_Category().equals("3")))                                                                                         //Natural: IF ACIS-BENE-FILE.BENE-CATEGORY EQ '3'
        {
            pdaBena970.getBena970_Pnd_Illgble_Ind().setValue("Y");                                                                                                        //Natural: ASSIGN #ILLGBLE-IND := 'Y'
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR I = 1 TO 20
        for (i.setValue(1); condition(i.lessOrEqual(20)); i.nadd(1))
        {
            if (condition(pdaScia1200.getScia1200_Pnd_Bene_Type().getValue(i).equals(" ")))                                                                               //Natural: IF SCIA1200.#BENE-TYPE ( I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  BNL2
            //*  BNL2
            pdaBena970.getBena970_Pnd_Nmbr_Of_Benes().nadd(1);                                                                                                            //Natural: ADD 1 TO #NMBR-OF-BENES
            pdaBena970.getBena970_Pnd_Bene_Type().getValue(i).setValue(pdaScia1200.getScia1200_Pnd_Bene_Type().getValue(i));                                              //Natural: ASSIGN BENA970.#BENE-TYPE ( I ) := SCIA1200.#BENE-TYPE ( I )
            pdaBena970.getBena970_Pnd_Bene_Name1().getValue(i).setValue(pdaScia1200.getScia1200_Pnd_Bene_Name().getValue(i));                                             //Natural: ASSIGN BENA970.#BENE-NAME1 ( I ) := SCIA1200.#BENE-NAME ( I )
            pdaBena970.getBena970_Pnd_Bene_Name2().getValue(i).setValue(pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue(i));                                    //Natural: ASSIGN BENA970.#BENE-NAME2 ( I ) := SCIA1200.#BENE-EXTENDED-NAME ( I )
            //*  BENA970.#RTLN-CDE(I) := SCIA1200.#BENE-RELATIONSHIP-CODE(I)
            pdaBena970.getBena970_Pnd_Rltn_Free_Txt().getValue(i).setValue(pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(i));                                  //Natural: ASSIGN BENA970.#RLTN-FREE-TXT ( I ) := SCIA1200.#BENE-RELATIONSHIP ( I )
            pdaBena970.getBena970_Pnd_Dte_Birth_Trust().getValue(i).setValue(pdaScia1200.getScia1200_Pnd_Bene_Date_Of_Birth().getValue(i));                               //Natural: ASSIGN BENA970.#DTE-BIRTH-TRUST ( I ) := SCIA1200.#BENE-DATE-OF-BIRTH ( I )
            pdaBena970.getBena970_Pnd_Ss_Cd().getValue(i).setValue("S");                                                                                                  //Natural: ASSIGN BENA970.#SS-CD ( I ) := 'S'
            pdaBena970.getBena970_Pnd_Ss_Nbr().getValue(i).setValue(pdaScia1200.getScia1200_Pnd_Bene_Ssn().getValue(i));                                                  //Natural: ASSIGN BENA970.#SS-NBR ( I ) := SCIA1200.#BENE-SSN ( I )
            pdaBena970.getBena970_Pnd_Prctge_Frctn_Ind().getValue(i).setValue("P");                                                                                       //Natural: ASSIGN BENA970.#PRCTGE-FRCTN-IND ( I ) := 'P'
            pnd_Pct_A.setValue(pdaScia1200.getScia1200_Pnd_Bene_Alloc_Pct().getValue(i));                                                                                 //Natural: ASSIGN #PCT-A := SCIA1200.#BENE-ALLOC-PCT ( I )
            pdaBena970.getBena970_Pnd_Prctge().getValue(i).setValue(pnd_Pct_A_Pnd_Pct);                                                                                   //Natural: ASSIGN BENA970.#PRCTGE ( I ) := #PCT
            pdaBena970.getBena970_Pnd_Nmrtr().getValue(i).compute(new ComputeParameters(false, pdaBena970.getBena970_Pnd_Nmrtr().getValue(i)), pdaScia1200.getScia1200_Pnd_Bene_Numerator().getValue(i).val()); //Natural: ASSIGN BENA970.#NMRTR ( I ) := VAL ( SCIA1200.#BENE-NUMERATOR ( I ) )
            pdaBena970.getBena970_Pnd_Dnmntr().getValue(i).compute(new ComputeParameters(false, pdaBena970.getBena970_Pnd_Dnmntr().getValue(i)), pdaScia1200.getScia1200_Pnd_Bene_Denominator().getValue(i).val()); //Natural: ASSIGN BENA970.#DNMNTR ( I ) := VAL ( SCIA1200.#BENE-DENOMINATOR ( I ) )
            pdaBena970.getBena970_Pnd_Spcl_Txt().getValue(i,"*").setValue(pdaScia1200.getScia1200_Pnd_Bene_Special_Text().getValue(i,"*"));                               //Natural: ASSIGN BENA970.#SPCL-TXT ( I,* ) := SCIA1200.#BENE-SPECIAL-TEXT ( I,* )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        DbsUtil.callnat(Benn970.class , getCurrentProcessState(), pdaBena970.getBena970());                                                                               //Natural: CALLNAT 'BENN970' BENA970
        if (condition(Global.isEscape())) return;
        if (condition(pdaBena970.getBena970_Pnd_Return_Code().notEquals(" ")))                                                                                            //Natural: IF BENA970.#RETURN-CODE NE ' '
        {
            pnd_Scin9913_Return_Code.setValue("9914");                                                                                                                    //Natural: ASSIGN #SCIN9913-RETURN-CODE = '9914'
            pnd_Scin9913_Return_Msg.setValue(pdaBena970.getBena970_Pnd_Error_Msg());                                                                                      //Natural: ASSIGN #SCIN9913-RETURN-MSG = #ERROR-MSG
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
