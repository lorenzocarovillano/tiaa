/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:10:40 AM
**        * FROM NATURAL SUBPROGRAM : Scin9600
************************************************************
**        * FILE NAME            : Scin9600.java
**        * CLASS NAME           : Scin9600
**        * INSTANCE NAME        : Scin9600
************************************************************
************************************************************************
* SUBPROGRAM:SCIN9600 - OMNI/ACIS T051, T850, T851, T813 (PH665) AND
*            T801 CHG (PH665) SUBPROGRAM
* FUNCTION   NEW NATURAL SUBPROGRAM WILL UPDATE THE ACIS FILES FOR THE
*            T051, T850, T851, T813 (PH665) AND T801 CHG (PH665)
*            TRANSACTIONS DONE IN OMNIPLUS.
*            THIS PROGRAM WILL UPDATE THE ACIS FILES:
*             1. ANNTY-ACTVTY-PRAP
*             2. ACIS-REPRINT-FL
*
* -------------------------------------------------------------------- *
*   DATE   DEVELOPER CHANGE DESCRIPTION                                *
* -------- --------- ------------------------------------------------- *
* 12/07/09 B.ELLO    COMMENT OUT CALL TO SUBROUTINE UPDATE-DEFAULT-
*                    ENROLLMENT INDICATOR.  THIS UPDATE IS NOT NEEDED
* 05/23/17 B.ELLO     PIN EXPANSION CHANGES
*                     RESTOWED FOR UPDATED PDAS AND LDAS (PINE)
*                     POINT TO THE NEW VIEWS OF THE ACIS FILES WITH THE
*                     EXPANDED PIN   (PINE)
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Scin9600 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaScia9600 pdaScia9600;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_prap_View;
    private DbsField prap_View_Ap_Coll_Code;
    private DbsField prap_View_Ap_Soc_Sec;
    private DbsField prap_View_Ap_Lob;
    private DbsField prap_View_Ap_T_Doi;
    private DbsField prap_View_Ap_C_Doi;
    private DbsField prap_View_Ap_Release_Ind;
    private DbsField prap_View_Ap_Record_Type;
    private DbsField prap_View_Ap_Lob_Type;
    private DbsField prap_View_Ap_Cor_Last_Nme;
    private DbsField prap_View_Ap_Cor_First_Nme;
    private DbsField prap_View_Ap_Cor_Mddle_Nme;
    private DbsField prap_View_Ap_Pin_Nbr;
    private DbsField prap_View_Ap_Bank_Pymnt_Acct_Nmbr;

    private DbsGroup prap_View__R_Field_1;
    private DbsField prap_View_Ap_Bank_Home_Ac;
    private DbsField prap_View_Ap_Bank_Home_A3;
    private DbsField prap_View_Ap_Bank_Home_A4;
    private DbsField prap_View_Ap_Bank_Filler_4;
    private DbsField prap_View_Ap_Oia_Ind;
    private DbsField prap_View_Ap_Erisa_Ind;
    private DbsField prap_View_Ap_Cai_Ind;
    private DbsField prap_View_Ap_Cip;
    private DbsField prap_View_Ap_Same_Addr;
    private DbsField prap_View_Ap_Omni_Account_Issuance;
    private DbsField prap_View_Ap_Tiaa_Cntrct;
    private DbsField prap_View_Ap_Cref_Cert;
    private DbsField prap_View_Ap_Sgrd_Plan_No;
    private DbsField prap_View_Ap_Sgrd_Subplan_No;
    private DbsField prap_View_Ap_Sgrd_Part_Ext;
    private DbsField prap_View_Ap_Status;
    private DbsField prap_View_Ap_Dt_Deleted;
    private DbsField prap_View_Ap_Delete_User_Id;
    private DbsField prap_View_Ap_Delete_Reason_Cd;

    private DataAccessProgramView vw_prap_Update;
    private DbsField prap_Update_Ap_Coll_Code;
    private DbsField prap_Update_Ap_Soc_Sec;
    private DbsField prap_Update_Ap_Lob;
    private DbsField prap_Update_Ap_T_Doi;
    private DbsField prap_Update_Ap_C_Doi;
    private DbsField prap_Update_Ap_Release_Ind;
    private DbsField prap_Update_Ap_Record_Type;
    private DbsField prap_Update_Ap_Lob_Type;
    private DbsField prap_Update_Ap_Cor_Last_Nme;
    private DbsField prap_Update_Ap_Cor_First_Nme;
    private DbsField prap_Update_Ap_Cor_Mddle_Nme;
    private DbsField prap_Update_Ap_Pin_Nbr;
    private DbsField prap_Update_Ap_Bank_Pymnt_Acct_Nmbr;

    private DbsGroup prap_Update__R_Field_2;
    private DbsField prap_Update_Ap_Bank_Home_Ac;
    private DbsField prap_Update_Ap_Bank_Home_A3;
    private DbsField prap_Update_Ap_Bank_Home_A4;
    private DbsField prap_Update_Ap_Bank_Filler_4;
    private DbsField prap_Update_Ap_Oia_Ind;
    private DbsField prap_Update_Ap_Erisa_Ind;
    private DbsField prap_Update_Ap_Cai_Ind;
    private DbsField prap_Update_Ap_Cip;
    private DbsField prap_Update_Ap_Same_Addr;
    private DbsField prap_Update_Ap_Omni_Account_Issuance;
    private DbsField prap_Update_Ap_Tiaa_Cntrct;
    private DbsField prap_Update_Ap_Cref_Cert;
    private DbsField prap_Update_Ap_Sgrd_Plan_No;
    private DbsField prap_Update_Ap_Sgrd_Subplan_No;
    private DbsField prap_Update_Ap_Sgrd_Part_Ext;
    private DbsField prap_Update_Ap_Status;
    private DbsField prap_Update_Ap_Dt_Deleted;
    private DbsField prap_Update_Ap_Delete_User_Id;
    private DbsField prap_Update_Ap_Delete_Reason_Cd;

    private DataAccessProgramView vw_reprint_View;
    private DbsField reprint_View_Rp_Soc_Sec;
    private DbsField reprint_View_Rp_Pin_Nbr;
    private DbsField reprint_View_Rp_Cor_Last_Nme;
    private DbsField reprint_View_Rp_Cor_First_Nme;
    private DbsField reprint_View_Rp_Cor_Mddle_Nme;
    private DbsField reprint_View_Rp_Dob;
    private DbsField reprint_View_Rp_Email_Address;
    private DbsField reprint_View_Rp_Divorce_Ind;
    private DbsField reprint_View_Rp_Tiaa_Contr;
    private DbsField reprint_View_Rp_Ownership;

    private DbsGroup reprint_View_Rp_Address_Info;
    private DbsField reprint_View_Rp_Address_Line;
    private DbsField reprint_View_Rp_Mail_Zip;
    private DbsField reprint_View_Rp_Current_State_Code;
    private DbsField reprint_View_Rp_Sgrd_Plan_No;
    private DbsField reprint_View_Rp_Sgrd_Subplan_No;
    private DbsField reprint_View_Rp_Text_Udf_1;
    private DbsField reprint_View_Rp_Delete_User_Id;
    private DbsField reprint_View_Rp_Delete_Reason_Cd;
    private DbsField reprint_View_Rp_Dt_Deleted;
    private DbsField reprint_View_Rp_Update_Date;
    private DbsField reprint_View_Rp_Update_Time;
    private DbsField reprint_View_Rp_Racf_Id;
    private DbsField reprint_View_Rp_Tiaa_Service_Agent;

    private DataAccessProgramView vw_reprint_Update;
    private DbsField reprint_Update_Rp_Soc_Sec;
    private DbsField reprint_Update_Rp_Pin_Nbr;
    private DbsField reprint_Update_Rp_Cor_Last_Nme;
    private DbsField reprint_Update_Rp_Cor_First_Nme;
    private DbsField reprint_Update_Rp_Cor_Mddle_Nme;
    private DbsField reprint_Update_Rp_Dob;
    private DbsField reprint_Update_Rp_Email_Address;
    private DbsField reprint_Update_Rp_Divorce_Ind;
    private DbsField reprint_Update_Rp_Tiaa_Contr;
    private DbsField reprint_Update_Rp_Ownership;

    private DbsGroup reprint_Update_Rp_Address_Info;
    private DbsField reprint_Update_Rp_Address_Line;
    private DbsField reprint_Update_Rp_Mail_Zip;
    private DbsField reprint_Update_Rp_Current_State_Code;
    private DbsField reprint_Update_Rp_Sgrd_Plan_No;
    private DbsField reprint_Update_Rp_Sgrd_Subplan_No;
    private DbsField reprint_Update_Rp_Text_Udf_1;
    private DbsField reprint_Update_Rp_Delete_User_Id;
    private DbsField reprint_Update_Rp_Delete_Reason_Cd;
    private DbsField reprint_Update_Rp_Dt_Deleted;
    private DbsField reprint_Update_Rp_Update_Date;
    private DbsField reprint_Update_Rp_Update_Time;
    private DbsField reprint_Update_Rp_Racf_Id;
    private DbsField reprint_Update_Rp_Tiaa_Service_Agent;
    private DbsField pnd_Record_Count;
    private DbsField pnd_Report_Count;
    private DbsField pnd_Ssn_N;

    private DbsGroup pnd_Ssn_N__R_Field_3;
    private DbsField pnd_Ssn_N_Pnd_Ssn_A;
    private DbsField pnd_New_Pin;
    private DbsField pnd_Date_Mmddyyyy_A;

    private DbsGroup pnd_Date_Mmddyyyy_A__R_Field_4;
    private DbsField pnd_Date_Mmddyyyy_A_Pnd_Date_Mmddyyyy_N;
    private DbsField pnd_Update_Done;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaScia9600 = new PdaScia9600(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_prap_View = new DataAccessProgramView(new NameInfo("vw_prap_View", "PRAP-VIEW"), "ANNTY_ACTVTY_PRAP_12", "ACIS_ANNTY_PRAP");
        prap_View_Ap_Coll_Code = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Coll_Code", "AP-COLL-CODE", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_COLL_CODE");
        prap_View_Ap_Soc_Sec = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, 
            "AP_SOC_SEC");
        prap_View_Ap_Lob = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Lob", "AP-LOB", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LOB");
        prap_View_Ap_Lob.setDdmHeader("LOB");
        prap_View_Ap_T_Doi = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_T_Doi", "AP-T-DOI", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, 
            "AP_T_DOI");
        prap_View_Ap_C_Doi = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_C_Doi", "AP-C-DOI", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, 
            "AP_C_DOI");
        prap_View_Ap_Release_Ind = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Release_Ind", "AP-RELEASE-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_RELEASE_IND");
        prap_View_Ap_Record_Type = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Record_Type", "AP-RECORD-TYPE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_RECORD_TYPE");
        prap_View_Ap_Lob_Type = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Lob_Type", "AP-LOB-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_LOB_TYPE");
        prap_View_Ap_Lob_Type.setDdmHeader("LOB/TYPE");
        prap_View_Ap_Cor_Last_Nme = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "AP_COR_LAST_NME");
        prap_View_Ap_Cor_First_Nme = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "AP_COR_FIRST_NME");
        prap_View_Ap_Cor_Mddle_Nme = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Cor_Mddle_Nme", "AP-COR-MDDLE-NME", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "AP_COR_MDDLE_NME");
        prap_View_Ap_Pin_Nbr = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "AP_PIN_NBR");
        prap_View_Ap_Bank_Pymnt_Acct_Nmbr = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Bank_Pymnt_Acct_Nmbr", "AP-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 
            21, RepeatingFieldStrategy.None, "AP_BANK_PYMNT_ACCT_NMBR");

        prap_View__R_Field_1 = vw_prap_View.getRecord().newGroupInGroup("prap_View__R_Field_1", "REDEFINE", prap_View_Ap_Bank_Pymnt_Acct_Nmbr);
        prap_View_Ap_Bank_Home_Ac = prap_View__R_Field_1.newFieldInGroup("prap_View_Ap_Bank_Home_Ac", "AP-BANK-HOME-AC", FieldType.STRING, 3);
        prap_View_Ap_Bank_Home_A3 = prap_View__R_Field_1.newFieldInGroup("prap_View_Ap_Bank_Home_A3", "AP-BANK-HOME-A3", FieldType.STRING, 3);
        prap_View_Ap_Bank_Home_A4 = prap_View__R_Field_1.newFieldInGroup("prap_View_Ap_Bank_Home_A4", "AP-BANK-HOME-A4", FieldType.STRING, 4);
        prap_View_Ap_Bank_Filler_4 = prap_View__R_Field_1.newFieldInGroup("prap_View_Ap_Bank_Filler_4", "AP-BANK-FILLER-4", FieldType.STRING, 4);
        prap_View_Ap_Oia_Ind = prap_View__R_Field_1.newFieldInGroup("prap_View_Ap_Oia_Ind", "AP-OIA-IND", FieldType.STRING, 2);
        prap_View_Ap_Erisa_Ind = prap_View__R_Field_1.newFieldInGroup("prap_View_Ap_Erisa_Ind", "AP-ERISA-IND", FieldType.STRING, 1);
        prap_View_Ap_Cai_Ind = prap_View__R_Field_1.newFieldInGroup("prap_View_Ap_Cai_Ind", "AP-CAI-IND", FieldType.STRING, 1);
        prap_View_Ap_Cip = prap_View__R_Field_1.newFieldInGroup("prap_View_Ap_Cip", "AP-CIP", FieldType.STRING, 1);
        prap_View_Ap_Same_Addr = prap_View__R_Field_1.newFieldInGroup("prap_View_Ap_Same_Addr", "AP-SAME-ADDR", FieldType.STRING, 1);
        prap_View_Ap_Omni_Account_Issuance = prap_View__R_Field_1.newFieldInGroup("prap_View_Ap_Omni_Account_Issuance", "AP-OMNI-ACCOUNT-ISSUANCE", FieldType.STRING, 
            1);
        prap_View_Ap_Tiaa_Cntrct = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TIAA_CNTRCT");
        prap_View_Ap_Cref_Cert = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Cref_Cert", "AP-CREF-CERT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_CREF_CERT");
        prap_View_Ap_Sgrd_Plan_No = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_SGRD_PLAN_NO");
        prap_View_Ap_Sgrd_Subplan_No = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Sgrd_Subplan_No", "AP-SGRD-SUBPLAN-NO", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "AP_SGRD_SUBPLAN_NO");
        prap_View_Ap_Sgrd_Part_Ext = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Sgrd_Part_Ext", "AP-SGRD-PART-EXT", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "AP_SGRD_PART_EXT");
        prap_View_Ap_Status = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Status", "AP-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_STATUS");
        prap_View_Ap_Dt_Deleted = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Dt_Deleted", "AP-DT-DELETED", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, 
            "AP_DT_DELETED");
        prap_View_Ap_Delete_User_Id = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Delete_User_Id", "AP-DELETE-USER-ID", FieldType.STRING, 16, 
            RepeatingFieldStrategy.None, "AP_DELETE_USER_ID");
        prap_View_Ap_Delete_Reason_Cd = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Delete_Reason_Cd", "AP-DELETE-REASON-CD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_DELETE_REASON_CD");
        registerRecord(vw_prap_View);

        vw_prap_Update = new DataAccessProgramView(new NameInfo("vw_prap_Update", "PRAP-UPDATE"), "ANNTY_ACTVTY_PRAP_12", "ACIS_ANNTY_PRAP");
        prap_Update_Ap_Coll_Code = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_Coll_Code", "AP-COLL-CODE", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_COLL_CODE");
        prap_Update_Ap_Soc_Sec = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, 
            "AP_SOC_SEC");
        prap_Update_Ap_Lob = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_Lob", "AP-LOB", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_LOB");
        prap_Update_Ap_Lob.setDdmHeader("LOB");
        prap_Update_Ap_T_Doi = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_T_Doi", "AP-T-DOI", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, 
            "AP_T_DOI");
        prap_Update_Ap_C_Doi = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_C_Doi", "AP-C-DOI", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, 
            "AP_C_DOI");
        prap_Update_Ap_Release_Ind = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_Release_Ind", "AP-RELEASE-IND", FieldType.NUMERIC, 1, 
            RepeatingFieldStrategy.None, "AP_RELEASE_IND");
        prap_Update_Ap_Record_Type = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_Record_Type", "AP-RECORD-TYPE", FieldType.NUMERIC, 1, 
            RepeatingFieldStrategy.None, "AP_RECORD_TYPE");
        prap_Update_Ap_Lob_Type = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_Lob_Type", "AP-LOB-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_LOB_TYPE");
        prap_Update_Ap_Lob_Type.setDdmHeader("LOB/TYPE");
        prap_Update_Ap_Cor_Last_Nme = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "AP_COR_LAST_NME");
        prap_Update_Ap_Cor_First_Nme = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "AP_COR_FIRST_NME");
        prap_Update_Ap_Cor_Mddle_Nme = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_Cor_Mddle_Nme", "AP-COR-MDDLE-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "AP_COR_MDDLE_NME");
        prap_Update_Ap_Pin_Nbr = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "AP_PIN_NBR");
        prap_Update_Ap_Bank_Pymnt_Acct_Nmbr = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_Bank_Pymnt_Acct_Nmbr", "AP-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 21, RepeatingFieldStrategy.None, "AP_BANK_PYMNT_ACCT_NMBR");

        prap_Update__R_Field_2 = vw_prap_Update.getRecord().newGroupInGroup("prap_Update__R_Field_2", "REDEFINE", prap_Update_Ap_Bank_Pymnt_Acct_Nmbr);
        prap_Update_Ap_Bank_Home_Ac = prap_Update__R_Field_2.newFieldInGroup("prap_Update_Ap_Bank_Home_Ac", "AP-BANK-HOME-AC", FieldType.STRING, 3);
        prap_Update_Ap_Bank_Home_A3 = prap_Update__R_Field_2.newFieldInGroup("prap_Update_Ap_Bank_Home_A3", "AP-BANK-HOME-A3", FieldType.STRING, 3);
        prap_Update_Ap_Bank_Home_A4 = prap_Update__R_Field_2.newFieldInGroup("prap_Update_Ap_Bank_Home_A4", "AP-BANK-HOME-A4", FieldType.STRING, 4);
        prap_Update_Ap_Bank_Filler_4 = prap_Update__R_Field_2.newFieldInGroup("prap_Update_Ap_Bank_Filler_4", "AP-BANK-FILLER-4", FieldType.STRING, 4);
        prap_Update_Ap_Oia_Ind = prap_Update__R_Field_2.newFieldInGroup("prap_Update_Ap_Oia_Ind", "AP-OIA-IND", FieldType.STRING, 2);
        prap_Update_Ap_Erisa_Ind = prap_Update__R_Field_2.newFieldInGroup("prap_Update_Ap_Erisa_Ind", "AP-ERISA-IND", FieldType.STRING, 1);
        prap_Update_Ap_Cai_Ind = prap_Update__R_Field_2.newFieldInGroup("prap_Update_Ap_Cai_Ind", "AP-CAI-IND", FieldType.STRING, 1);
        prap_Update_Ap_Cip = prap_Update__R_Field_2.newFieldInGroup("prap_Update_Ap_Cip", "AP-CIP", FieldType.STRING, 1);
        prap_Update_Ap_Same_Addr = prap_Update__R_Field_2.newFieldInGroup("prap_Update_Ap_Same_Addr", "AP-SAME-ADDR", FieldType.STRING, 1);
        prap_Update_Ap_Omni_Account_Issuance = prap_Update__R_Field_2.newFieldInGroup("prap_Update_Ap_Omni_Account_Issuance", "AP-OMNI-ACCOUNT-ISSUANCE", 
            FieldType.STRING, 1);
        prap_Update_Ap_Tiaa_Cntrct = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "AP_TIAA_CNTRCT");
        prap_Update_Ap_Cref_Cert = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_Cref_Cert", "AP-CREF-CERT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_CREF_CERT");
        prap_Update_Ap_Sgrd_Plan_No = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", FieldType.STRING, 6, 
            RepeatingFieldStrategy.None, "AP_SGRD_PLAN_NO");
        prap_Update_Ap_Sgrd_Subplan_No = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_Sgrd_Subplan_No", "AP-SGRD-SUBPLAN-NO", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "AP_SGRD_SUBPLAN_NO");
        prap_Update_Ap_Sgrd_Part_Ext = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_Sgrd_Part_Ext", "AP-SGRD-PART-EXT", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "AP_SGRD_PART_EXT");
        prap_Update_Ap_Status = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_Status", "AP-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_STATUS");
        prap_Update_Ap_Dt_Deleted = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_Dt_Deleted", "AP-DT-DELETED", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "AP_DT_DELETED");
        prap_Update_Ap_Delete_User_Id = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_Delete_User_Id", "AP-DELETE-USER-ID", FieldType.STRING, 
            16, RepeatingFieldStrategy.None, "AP_DELETE_USER_ID");
        prap_Update_Ap_Delete_Reason_Cd = vw_prap_Update.getRecord().newFieldInGroup("prap_Update_Ap_Delete_Reason_Cd", "AP-DELETE-REASON-CD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_DELETE_REASON_CD");
        registerRecord(vw_prap_Update);

        vw_reprint_View = new DataAccessProgramView(new NameInfo("vw_reprint_View", "REPRINT-VIEW"), "ACIS_REPRINT_FL_12", "ACIS_RPRNT_FILE", DdmPeriodicGroups.getInstance().getGroups("ACIS_REPRINT_FL_12"));
        reprint_View_Rp_Soc_Sec = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Soc_Sec", "RP-SOC-SEC", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "RP_SOC_SEC");
        reprint_View_Rp_Soc_Sec.setDdmHeader("SOC/SEC");
        reprint_View_Rp_Pin_Nbr = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Pin_Nbr", "RP-PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "RP_PIN_NBR");
        reprint_View_Rp_Pin_Nbr.setDdmHeader("PIN/NBR");
        reprint_View_Rp_Cor_Last_Nme = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Cor_Last_Nme", "RP-COR-LAST-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "RP_COR_LAST_NME");
        reprint_View_Rp_Cor_Last_Nme.setDdmHeader("PARTICIPANT/LAST NAME");
        reprint_View_Rp_Cor_First_Nme = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Cor_First_Nme", "RP-COR-FIRST-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "RP_COR_FIRST_NME");
        reprint_View_Rp_Cor_First_Nme.setDdmHeader("PARTICIPANT/FIRST NAME");
        reprint_View_Rp_Cor_Mddle_Nme = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Cor_Mddle_Nme", "RP-COR-MDDLE-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "RP_COR_MDDLE_NME");
        reprint_View_Rp_Cor_Mddle_Nme.setDdmHeader("PARTICIPANT/MIDDLE NAME");
        reprint_View_Rp_Dob = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Dob", "RP-DOB", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "RP_DOB");
        reprint_View_Rp_Dob.setDdmHeader("DOB");
        reprint_View_Rp_Email_Address = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Email_Address", "RP-EMAIL-ADDRESS", FieldType.STRING, 
            50, RepeatingFieldStrategy.None, "RP_EMAIL_ADDRESS");
        reprint_View_Rp_Divorce_Ind = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Divorce_Ind", "RP-DIVORCE-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RP_DIVORCE_IND");
        reprint_View_Rp_Tiaa_Contr = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Tiaa_Contr", "RP-TIAA-CONTR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "RP_TIAA_CONTR");
        reprint_View_Rp_Tiaa_Contr.setDdmHeader("TIAA/CONTR");
        reprint_View_Rp_Ownership = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Ownership", "RP-OWNERSHIP", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "RP_OWNERSHIP");
        reprint_View_Rp_Ownership.setDdmHeader("OWNER");

        reprint_View_Rp_Address_Info = vw_reprint_View.getRecord().newGroupInGroup("reprint_View_Rp_Address_Info", "RP-ADDRESS-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_RPRNT_FILE_RP_ADDRESS_INFO");
        reprint_View_Rp_Address_Info.setDdmHeader("ADDRESS/INFO");
        reprint_View_Rp_Address_Line = reprint_View_Rp_Address_Info.newFieldArrayInGroup("reprint_View_Rp_Address_Line", "RP-ADDRESS-LINE", FieldType.STRING, 
            35, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_ADDRESS_LINE", "ACIS_RPRNT_FILE_RP_ADDRESS_INFO");
        reprint_View_Rp_Address_Line.setDdmHeader("ADDRESS");
        reprint_View_Rp_Mail_Zip = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Mail_Zip", "RP-MAIL-ZIP", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "RP_MAIL_ZIP");
        reprint_View_Rp_Mail_Zip.setDdmHeader("ZIP/CODE");
        reprint_View_Rp_Current_State_Code = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Current_State_Code", "RP-CURRENT-STATE-CODE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "RP_CURRENT_STATE_CODE");
        reprint_View_Rp_Current_State_Code.setDdmHeader("CURR STATE/CODE");
        reprint_View_Rp_Sgrd_Plan_No = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Sgrd_Plan_No", "RP-SGRD-PLAN-NO", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "RP_SGRD_PLAN_NO");
        reprint_View_Rp_Sgrd_Subplan_No = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Sgrd_Subplan_No", "RP-SGRD-SUBPLAN-NO", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "RP_SGRD_SUBPLAN_NO");
        reprint_View_Rp_Text_Udf_1 = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Text_Udf_1", "RP-TEXT-UDF-1", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "RP_TEXT_UDF_1");
        reprint_View_Rp_Delete_User_Id = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Delete_User_Id", "RP-DELETE-USER-ID", FieldType.STRING, 
            16, RepeatingFieldStrategy.None, "RP_DELETE_USER_ID");
        reprint_View_Rp_Delete_Reason_Cd = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Delete_Reason_Cd", "RP-DELETE-REASON-CD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RP_DELETE_REASON_CD");
        reprint_View_Rp_Dt_Deleted = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Dt_Deleted", "RP-DT-DELETED", FieldType.NUMERIC, 8, 
            RepeatingFieldStrategy.None, "RP_DT_DELETED");
        reprint_View_Rp_Update_Date = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Update_Date", "RP-UPDATE-DATE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "RP_UPDATE_DATE");
        reprint_View_Rp_Update_Date.setDdmHeader("UPDATE/ DATE");
        reprint_View_Rp_Update_Time = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Update_Time", "RP-UPDATE-TIME", FieldType.STRING, 7, 
            RepeatingFieldStrategy.None, "RP_UPDATE_TIME");
        reprint_View_Rp_Update_Time.setDdmHeader("UPDATE/ TIME");
        reprint_View_Rp_Racf_Id = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Racf_Id", "RP-RACF-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RP_RACF_ID");
        reprint_View_Rp_Racf_Id.setDdmHeader("REQUESTOR/ID");
        reprint_View_Rp_Tiaa_Service_Agent = vw_reprint_View.getRecord().newFieldInGroup("reprint_View_Rp_Tiaa_Service_Agent", "RP-TIAA-SERVICE-AGENT", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_TIAA_SERVICE_AGENT");
        reprint_View_Rp_Tiaa_Service_Agent.setDdmHeader("TIAA/SERV AGT");
        registerRecord(vw_reprint_View);

        vw_reprint_Update = new DataAccessProgramView(new NameInfo("vw_reprint_Update", "REPRINT-UPDATE"), "ACIS_REPRINT_FL_12", "ACIS_RPRNT_FILE", DdmPeriodicGroups.getInstance().getGroups("ACIS_REPRINT_FL_12"));
        reprint_Update_Rp_Soc_Sec = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Soc_Sec", "RP-SOC-SEC", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "RP_SOC_SEC");
        reprint_Update_Rp_Soc_Sec.setDdmHeader("SOC/SEC");
        reprint_Update_Rp_Pin_Nbr = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Pin_Nbr", "RP-PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "RP_PIN_NBR");
        reprint_Update_Rp_Pin_Nbr.setDdmHeader("PIN/NBR");
        reprint_Update_Rp_Cor_Last_Nme = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Cor_Last_Nme", "RP-COR-LAST-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "RP_COR_LAST_NME");
        reprint_Update_Rp_Cor_Last_Nme.setDdmHeader("PARTICIPANT/LAST NAME");
        reprint_Update_Rp_Cor_First_Nme = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Cor_First_Nme", "RP-COR-FIRST-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "RP_COR_FIRST_NME");
        reprint_Update_Rp_Cor_First_Nme.setDdmHeader("PARTICIPANT/FIRST NAME");
        reprint_Update_Rp_Cor_Mddle_Nme = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Cor_Mddle_Nme", "RP-COR-MDDLE-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "RP_COR_MDDLE_NME");
        reprint_Update_Rp_Cor_Mddle_Nme.setDdmHeader("PARTICIPANT/MIDDLE NAME");
        reprint_Update_Rp_Dob = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Dob", "RP-DOB", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "RP_DOB");
        reprint_Update_Rp_Dob.setDdmHeader("DOB");
        reprint_Update_Rp_Email_Address = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Email_Address", "RP-EMAIL-ADDRESS", FieldType.STRING, 
            50, RepeatingFieldStrategy.None, "RP_EMAIL_ADDRESS");
        reprint_Update_Rp_Divorce_Ind = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Divorce_Ind", "RP-DIVORCE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RP_DIVORCE_IND");
        reprint_Update_Rp_Tiaa_Contr = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Tiaa_Contr", "RP-TIAA-CONTR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "RP_TIAA_CONTR");
        reprint_Update_Rp_Tiaa_Contr.setDdmHeader("TIAA/CONTR");
        reprint_Update_Rp_Ownership = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Ownership", "RP-OWNERSHIP", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "RP_OWNERSHIP");
        reprint_Update_Rp_Ownership.setDdmHeader("OWNER");

        reprint_Update_Rp_Address_Info = vw_reprint_Update.getRecord().newGroupInGroup("reprint_Update_Rp_Address_Info", "RP-ADDRESS-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_RPRNT_FILE_RP_ADDRESS_INFO");
        reprint_Update_Rp_Address_Info.setDdmHeader("ADDRESS/INFO");
        reprint_Update_Rp_Address_Line = reprint_Update_Rp_Address_Info.newFieldArrayInGroup("reprint_Update_Rp_Address_Line", "RP-ADDRESS-LINE", FieldType.STRING, 
            35, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_ADDRESS_LINE", "ACIS_RPRNT_FILE_RP_ADDRESS_INFO");
        reprint_Update_Rp_Address_Line.setDdmHeader("ADDRESS");
        reprint_Update_Rp_Mail_Zip = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Mail_Zip", "RP-MAIL-ZIP", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "RP_MAIL_ZIP");
        reprint_Update_Rp_Mail_Zip.setDdmHeader("ZIP/CODE");
        reprint_Update_Rp_Current_State_Code = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Current_State_Code", "RP-CURRENT-STATE-CODE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "RP_CURRENT_STATE_CODE");
        reprint_Update_Rp_Current_State_Code.setDdmHeader("CURR STATE/CODE");
        reprint_Update_Rp_Sgrd_Plan_No = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Sgrd_Plan_No", "RP-SGRD-PLAN-NO", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "RP_SGRD_PLAN_NO");
        reprint_Update_Rp_Sgrd_Subplan_No = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Sgrd_Subplan_No", "RP-SGRD-SUBPLAN-NO", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "RP_SGRD_SUBPLAN_NO");
        reprint_Update_Rp_Text_Udf_1 = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Text_Udf_1", "RP-TEXT-UDF-1", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "RP_TEXT_UDF_1");
        reprint_Update_Rp_Delete_User_Id = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Delete_User_Id", "RP-DELETE-USER-ID", FieldType.STRING, 
            16, RepeatingFieldStrategy.None, "RP_DELETE_USER_ID");
        reprint_Update_Rp_Delete_Reason_Cd = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Delete_Reason_Cd", "RP-DELETE-REASON-CD", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_DELETE_REASON_CD");
        reprint_Update_Rp_Dt_Deleted = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Dt_Deleted", "RP-DT-DELETED", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "RP_DT_DELETED");
        reprint_Update_Rp_Update_Date = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Update_Date", "RP-UPDATE-DATE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "RP_UPDATE_DATE");
        reprint_Update_Rp_Update_Date.setDdmHeader("UPDATE/ DATE");
        reprint_Update_Rp_Update_Time = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Update_Time", "RP-UPDATE-TIME", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "RP_UPDATE_TIME");
        reprint_Update_Rp_Update_Time.setDdmHeader("UPDATE/ TIME");
        reprint_Update_Rp_Racf_Id = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Racf_Id", "RP-RACF-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RP_RACF_ID");
        reprint_Update_Rp_Racf_Id.setDdmHeader("REQUESTOR/ID");
        reprint_Update_Rp_Tiaa_Service_Agent = vw_reprint_Update.getRecord().newFieldInGroup("reprint_Update_Rp_Tiaa_Service_Agent", "RP-TIAA-SERVICE-AGENT", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_TIAA_SERVICE_AGENT");
        reprint_Update_Rp_Tiaa_Service_Agent.setDdmHeader("TIAA/SERV AGT");
        registerRecord(vw_reprint_Update);

        pnd_Record_Count = localVariables.newFieldInRecord("pnd_Record_Count", "#RECORD-COUNT", FieldType.NUMERIC, 7);
        pnd_Report_Count = localVariables.newFieldInRecord("pnd_Report_Count", "#REPORT-COUNT", FieldType.NUMERIC, 7);
        pnd_Ssn_N = localVariables.newFieldInRecord("pnd_Ssn_N", "#SSN-N", FieldType.NUMERIC, 9);

        pnd_Ssn_N__R_Field_3 = localVariables.newGroupInRecord("pnd_Ssn_N__R_Field_3", "REDEFINE", pnd_Ssn_N);
        pnd_Ssn_N_Pnd_Ssn_A = pnd_Ssn_N__R_Field_3.newFieldInGroup("pnd_Ssn_N_Pnd_Ssn_A", "#SSN-A", FieldType.STRING, 9);
        pnd_New_Pin = localVariables.newFieldInRecord("pnd_New_Pin", "#NEW-PIN", FieldType.NUMERIC, 12);
        pnd_Date_Mmddyyyy_A = localVariables.newFieldInRecord("pnd_Date_Mmddyyyy_A", "#DATE-MMDDYYYY-A", FieldType.STRING, 8);

        pnd_Date_Mmddyyyy_A__R_Field_4 = localVariables.newGroupInRecord("pnd_Date_Mmddyyyy_A__R_Field_4", "REDEFINE", pnd_Date_Mmddyyyy_A);
        pnd_Date_Mmddyyyy_A_Pnd_Date_Mmddyyyy_N = pnd_Date_Mmddyyyy_A__R_Field_4.newFieldInGroup("pnd_Date_Mmddyyyy_A_Pnd_Date_Mmddyyyy_N", "#DATE-MMDDYYYY-N", 
            FieldType.NUMERIC, 8);
        pnd_Update_Done = localVariables.newFieldInRecord("pnd_Update_Done", "#UPDATE-DONE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_prap_View.reset();
        vw_prap_Update.reset();
        vw_reprint_View.reset();
        vw_reprint_Update.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Scin9600() throws Exception
    {
        super("Scin9600");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("SCIN9600", onError);
        pdaScia9600.getScia9600_Pnd_P_Return_Code().setValue("0000");                                                                                                     //Natural: ASSIGN #P-RETURN-CODE := '0000'
        //*  DELETE CONTRACT
        short decideConditionsMet187 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #P-REC-TYPE;//Natural: VALUE 'A051'
        if (condition((pdaScia9600.getScia9600_Pnd_P_Rec_Type().equals("A051"))))
        {
            decideConditionsMet187++;
            //*  SSN CHANGE
                                                                                                                                                                          //Natural: PERFORM DELETE-CONTRACT
            sub_Delete_Contract();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'A850'
        else if (condition((pdaScia9600.getScia9600_Pnd_P_Rec_Type().equals("A850"))))
        {
            decideConditionsMet187++;
            //*  PIN MERGE
                                                                                                                                                                          //Natural: PERFORM SSN-CHANGE
            sub_Ssn_Change();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'A851'
        else if (condition((pdaScia9600.getScia9600_Pnd_P_Rec_Type().equals("A851"))))
        {
            decideConditionsMet187++;
                                                                                                                                                                          //Natural: PERFORM PIN-MERGE
            sub_Pin_Merge();
            if (condition(Global.isEscape())) {return;}
            //* *B.E. 12/7/2009
            //* *VALUE 'A801', 'A813'
            //* *  PERFORM UPDATE-DEFAULT-ENROLLMENT
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  NO RECORDS FOUND TO UPDATE
        if (condition(! (pnd_Update_Done.getBoolean())))                                                                                                                  //Natural: IF NOT #UPDATE-DONE
        {
            pdaScia9600.getScia9600_Pnd_P_Return_Code().setValue("1111");                                                                                                 //Natural: ASSIGN #P-RETURN-CODE := '1111'
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DELETE-CONTRACT
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SSN-CHANGE
        //* ***********************************************************************
        //*  SET UPDATE FIELDS FOR PRAP FILE.
        //*  SET UPDATE FIELDS FOR REPRINT FILE.
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PIN-MERGE
        //* ***********************************************************************
        //*  SET UPDATE FIELDS FOR PRAP FILE.
        //*  SET UPDATE FIELDS FOR REPRINT FILE.
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-DEFAULT-ENROLLMENT
        //*  RESET DEFAULT ENROLLMENT INDICATOR
        //*  SET UPDATE FIELDS FOR REPRINT FILE.
        //* ***********************************************************************
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
    }
    private void sub_Delete_Contract() throws Exception                                                                                                                   //Natural: DELETE-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Update_Done.setValue(false);                                                                                                                                  //Natural: ASSIGN #UPDATE-DONE := FALSE
        vw_prap_View.startDatabaseFind                                                                                                                                    //Natural: FIND PRAP-VIEW WITH AP-TIAA-CNTRCT = #P-TIAA-CONTRACT
        (
        "L_FIND",
        new Wc[] { new Wc("AP_TIAA_CNTRCT", "=", pdaScia9600.getScia9600_Pnd_P_Tiaa_Contract(), WcType.WITH) }
        );
        L_FIND:
        while (condition(vw_prap_View.readNextRow("L_FIND")))
        {
            vw_prap_View.setIfNotFoundControlFlag(false);
            L_GET:                                                                                                                                                        //Natural: GET PRAP-UPDATE *ISN ( L-FIND. )
            vw_prap_Update.readByID(vw_prap_View.getAstISN("L_FIND"), "L_GET");
            //*  SET UPDATE FIELDS FOR PRAP FILE.
            //*  OMNI DELETE
            pnd_Date_Mmddyyyy_A.setValueEdited(Global.getDATX(),new ReportEditMask("MMDDYYYY"));                                                                          //Natural: MOVE EDITED *DATX ( EM = MMDDYYYY ) TO #DATE-MMDDYYYY-A
            prap_Update_Ap_Status.setValue("A");                                                                                                                          //Natural: ASSIGN PRAP-UPDATE.AP-STATUS := 'A'
            prap_Update_Ap_Dt_Deleted.setValue(pnd_Date_Mmddyyyy_A_Pnd_Date_Mmddyyyy_N);                                                                                  //Natural: ASSIGN PRAP-UPDATE.AP-DT-DELETED := #DATE-MMDDYYYY-N
            prap_Update_Ap_Delete_User_Id.setValue(Global.getINIT_USER());                                                                                                //Natural: ASSIGN PRAP-UPDATE.AP-DELETE-USER-ID := *INIT-USER
            prap_Update_Ap_Delete_Reason_Cd.setValue("5");                                                                                                                //Natural: ASSIGN PRAP-UPDATE.AP-DELETE-REASON-CD := '5'
            vw_prap_Update.updateDBRow("L_GET");                                                                                                                          //Natural: UPDATE ( L-GET. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Update_Done.setValue(true);                                                                                                                               //Natural: ASSIGN #UPDATE-DONE := TRUE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        vw_reprint_View.startDatabaseFind                                                                                                                                 //Natural: FIND REPRINT-VIEW WITH RP-TIAA-CONTR = #P-TIAA-CONTRACT
        (
        "L_FIND2",
        new Wc[] { new Wc("RP_TIAA_CONTR", "=", pdaScia9600.getScia9600_Pnd_P_Tiaa_Contract(), WcType.WITH) }
        );
        L_FIND2:
        while (condition(vw_reprint_View.readNextRow("L_FIND2")))
        {
            vw_reprint_View.setIfNotFoundControlFlag(false);
            L_GET2:                                                                                                                                                       //Natural: GET REPRINT-UPDATE *ISN ( L-FIND2. )
            vw_reprint_Update.readByID(vw_reprint_View.getAstISN("L_FIND2"), "L_GET2");
            //*  SET UPDATE FIELDS FOR REPRINT FILE.
            //*  OMNI DELETE
            pnd_Date_Mmddyyyy_A.setValueEdited(Global.getDATX(),new ReportEditMask("MMDDYYYY"));                                                                          //Natural: MOVE EDITED *DATX ( EM = MMDDYYYY ) TO #DATE-MMDDYYYY-A
            reprint_Update_Rp_Dt_Deleted.setValue(pnd_Date_Mmddyyyy_A_Pnd_Date_Mmddyyyy_N);                                                                               //Natural: ASSIGN REPRINT-UPDATE.RP-DT-DELETED := #DATE-MMDDYYYY-N
            reprint_Update_Rp_Delete_User_Id.setValue(Global.getINIT_USER());                                                                                             //Natural: ASSIGN REPRINT-UPDATE.RP-DELETE-USER-ID := *INIT-USER
            reprint_Update_Rp_Delete_Reason_Cd.setValue("5");                                                                                                             //Natural: ASSIGN REPRINT-UPDATE.RP-DELETE-REASON-CD := '5'
            vw_reprint_Update.updateDBRow("L_GET2");                                                                                                                      //Natural: UPDATE ( L-GET2. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Update_Done.setValue(true);                                                                                                                               //Natural: ASSIGN #UPDATE-DONE := TRUE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Ssn_Change() throws Exception                                                                                                                        //Natural: SSN-CHANGE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Update_Done.setValue(false);                                                                                                                                  //Natural: ASSIGN #UPDATE-DONE := FALSE
        vw_prap_View.startDatabaseFind                                                                                                                                    //Natural: FIND PRAP-VIEW WITH AP-TIAA-CNTRCT = #P-TIAA-CONTRACT
        (
        "L_FIND3",
        new Wc[] { new Wc("AP_TIAA_CNTRCT", "=", pdaScia9600.getScia9600_Pnd_P_Tiaa_Contract(), WcType.WITH) }
        );
        L_FIND3:
        while (condition(vw_prap_View.readNextRow("L_FIND3")))
        {
            vw_prap_View.setIfNotFoundControlFlag(false);
            L_GET3:                                                                                                                                                       //Natural: GET PRAP-UPDATE *ISN ( L-FIND3. )
            vw_prap_Update.readByID(vw_prap_View.getAstISN("L_FIND3"), "L_GET3");
            pnd_Ssn_N_Pnd_Ssn_A.setValue(pdaScia9600.getScia9600_Pnd_P_New_Ssn());                                                                                        //Natural: ASSIGN #SSN-A := #P-NEW-SSN
            prap_Update_Ap_Soc_Sec.setValue(pnd_Ssn_N);                                                                                                                   //Natural: ASSIGN PRAP-UPDATE.AP-SOC-SEC := #SSN-N
            vw_prap_Update.updateDBRow("L_GET3");                                                                                                                         //Natural: UPDATE ( L-GET3. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Update_Done.setValue(true);                                                                                                                               //Natural: ASSIGN #UPDATE-DONE := TRUE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        vw_reprint_View.startDatabaseFind                                                                                                                                 //Natural: FIND REPRINT-VIEW WITH RP-TIAA-CONTR = #P-TIAA-CONTRACT
        (
        "L_FIND4",
        new Wc[] { new Wc("RP_TIAA_CONTR", "=", pdaScia9600.getScia9600_Pnd_P_Tiaa_Contract(), WcType.WITH) }
        );
        L_FIND4:
        while (condition(vw_reprint_View.readNextRow("L_FIND4")))
        {
            vw_reprint_View.setIfNotFoundControlFlag(false);
            L_GET4:                                                                                                                                                       //Natural: GET REPRINT-UPDATE *ISN ( L-FIND4. )
            vw_reprint_Update.readByID(vw_reprint_View.getAstISN("L_FIND4"), "L_GET4");
            pnd_Ssn_N_Pnd_Ssn_A.setValue(pdaScia9600.getScia9600_Pnd_P_New_Ssn());                                                                                        //Natural: ASSIGN #SSN-A := #P-NEW-SSN
            reprint_Update_Rp_Soc_Sec.setValue(pnd_Ssn_N);                                                                                                                //Natural: ASSIGN REPRINT-UPDATE.RP-SOC-SEC := #SSN-N
            reprint_Update_Rp_Update_Date.setValue(Global.getDATN());                                                                                                     //Natural: ASSIGN REPRINT-UPDATE.RP-UPDATE-DATE := *DATN
            reprint_Update_Rp_Update_Time.setValue(Global.getTIMN());                                                                                                     //Natural: ASSIGN REPRINT-UPDATE.RP-UPDATE-TIME := *TIMN
            reprint_Update_Rp_Racf_Id.setValue(Global.getINIT_USER());                                                                                                    //Natural: ASSIGN REPRINT-UPDATE.RP-RACF-ID := *INIT-USER
            vw_reprint_Update.updateDBRow("L_GET4");                                                                                                                      //Natural: UPDATE ( L-GET4. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Update_Done.setValue(true);                                                                                                                               //Natural: ASSIGN #UPDATE-DONE := TRUE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Pin_Merge() throws Exception                                                                                                                         //Natural: PIN-MERGE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Update_Done.setValue(false);                                                                                                                                  //Natural: ASSIGN #UPDATE-DONE := FALSE
        pnd_New_Pin.reset();                                                                                                                                              //Natural: RESET #NEW-PIN
        pnd_Ssn_N_Pnd_Ssn_A.setValue(pdaScia9600.getScia9600_Pnd_P_New_Ssn());                                                                                            //Natural: ASSIGN #SSN-A := #P-NEW-SSN
        vw_prap_View.startDatabaseFind                                                                                                                                    //Natural: FIND PRAP-VIEW WITH AP-SOC-SEC = #SSN-N
        (
        "L_FINDP",
        new Wc[] { new Wc("AP_SOC_SEC", "=", pnd_Ssn_N, WcType.WITH) }
        );
        L_FINDP:
        while (condition(vw_prap_View.readNextRow("L_FINDP", true)))
        {
            vw_prap_View.setIfNotFoundControlFlag(false);
            if (condition(vw_prap_View.getAstCOUNTER().equals(0)))                                                                                                        //Natural: IF NO RECORD FOUND
            {
                vw_reprint_View.startDatabaseRead                                                                                                                         //Natural: READ ( 1 ) REPRINT-VIEW BY RP-SSN-TIAA-NO STARTING FROM #SSN-N
                (
                "READ01",
                new Wc[] { new Wc("RP_SSN_TIAA_NO", ">=", pnd_Ssn_N, WcType.BY) },
                new Oc[] { new Oc("RP_SSN_TIAA_NO", "ASC") },
                1
                );
                READ01:
                while (condition(vw_reprint_View.readNextRow("READ01")))
                {
                    if (condition(reprint_View_Rp_Soc_Sec.equals(pnd_Ssn_N)))                                                                                             //Natural: IF RP-SOC-SEC = #SSN-N
                    {
                        pnd_New_Pin.setValue(reprint_View_Rp_Pin_Nbr);                                                                                                    //Natural: ASSIGN #NEW-PIN := REPRINT-VIEW.RP-PIN-NBR
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("L_FINDP"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("L_FINDP"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) break L_FINDP;                                                                                                                                  //Natural: ESCAPE BOTTOM ( L-FINDP. )
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_New_Pin.setValue(prap_View_Ap_Pin_Nbr);                                                                                                                   //Natural: ASSIGN #NEW-PIN := PRAP-VIEW.AP-PIN-NBR
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        vw_prap_View.startDatabaseFind                                                                                                                                    //Natural: FIND PRAP-VIEW WITH AP-TIAA-CNTRCT = #P-TIAA-CONTRACT
        (
        "L_FIND5",
        new Wc[] { new Wc("AP_TIAA_CNTRCT", "=", pdaScia9600.getScia9600_Pnd_P_Tiaa_Contract(), WcType.WITH) }
        );
        L_FIND5:
        while (condition(vw_prap_View.readNextRow("L_FIND5")))
        {
            vw_prap_View.setIfNotFoundControlFlag(false);
            L_GET5:                                                                                                                                                       //Natural: GET PRAP-UPDATE *ISN ( L-FIND5. )
            vw_prap_Update.readByID(vw_prap_View.getAstISN("L_FIND5"), "L_GET5");
            pnd_Ssn_N_Pnd_Ssn_A.setValue(pdaScia9600.getScia9600_Pnd_P_New_Ssn());                                                                                        //Natural: ASSIGN #SSN-A := #P-NEW-SSN
            prap_Update_Ap_Soc_Sec.setValue(pnd_Ssn_N);                                                                                                                   //Natural: ASSIGN PRAP-UPDATE.AP-SOC-SEC := #SSN-N
            prap_Update_Ap_Pin_Nbr.setValue(pnd_New_Pin);                                                                                                                 //Natural: ASSIGN PRAP-UPDATE.AP-PIN-NBR := #NEW-PIN
            vw_prap_Update.updateDBRow("L_GET5");                                                                                                                         //Natural: UPDATE ( L-GET5. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Update_Done.setValue(true);                                                                                                                               //Natural: ASSIGN #UPDATE-DONE := TRUE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        vw_reprint_View.startDatabaseFind                                                                                                                                 //Natural: FIND REPRINT-VIEW WITH RP-TIAA-CONTR = #P-TIAA-CONTRACT
        (
        "L_FIND6",
        new Wc[] { new Wc("RP_TIAA_CONTR", "=", pdaScia9600.getScia9600_Pnd_P_Tiaa_Contract(), WcType.WITH) }
        );
        L_FIND6:
        while (condition(vw_reprint_View.readNextRow("L_FIND6")))
        {
            vw_reprint_View.setIfNotFoundControlFlag(false);
            L_GET6:                                                                                                                                                       //Natural: GET REPRINT-UPDATE *ISN ( L-FIND6. )
            vw_reprint_Update.readByID(vw_reprint_View.getAstISN("L_FIND6"), "L_GET6");
            pnd_Ssn_N_Pnd_Ssn_A.setValue(pdaScia9600.getScia9600_Pnd_P_New_Ssn());                                                                                        //Natural: ASSIGN #SSN-A := #P-NEW-SSN
            reprint_Update_Rp_Soc_Sec.setValue(pnd_Ssn_N);                                                                                                                //Natural: ASSIGN REPRINT-UPDATE.RP-SOC-SEC := #SSN-N
            reprint_Update_Rp_Pin_Nbr.setValue(pnd_New_Pin);                                                                                                              //Natural: ASSIGN REPRINT-UPDATE.RP-PIN-NBR := #NEW-PIN
            reprint_Update_Rp_Update_Date.setValue(Global.getDATN());                                                                                                     //Natural: ASSIGN REPRINT-UPDATE.RP-UPDATE-DATE := *DATN
            reprint_Update_Rp_Update_Time.setValue(Global.getTIMN());                                                                                                     //Natural: ASSIGN REPRINT-UPDATE.RP-UPDATE-TIME := *TIMN
            reprint_Update_Rp_Racf_Id.setValue(Global.getINIT_USER());                                                                                                    //Natural: ASSIGN REPRINT-UPDATE.RP-RACF-ID := *INIT-USER
            vw_reprint_Update.updateDBRow("L_GET6");                                                                                                                      //Natural: UPDATE ( L-GET6. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Update_Done.setValue(true);                                                                                                                               //Natural: ASSIGN #UPDATE-DONE := TRUE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Update_Default_Enrollment() throws Exception                                                                                                         //Natural: UPDATE-DEFAULT-ENROLLMENT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  B.E. 12/7/09 - ADDED SO THAT ROUTINE IS NOT EXECUTED.
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        pnd_Update_Done.setValue(false);                                                                                                                                  //Natural: ASSIGN #UPDATE-DONE := FALSE
        vw_prap_View.startDatabaseFind                                                                                                                                    //Natural: FIND PRAP-VIEW WITH AP-TIAA-CNTRCT = #P-TIAA-CONTRACT
        (
        "L_FIND7",
        new Wc[] { new Wc("AP_TIAA_CNTRCT", "=", pdaScia9600.getScia9600_Pnd_P_Tiaa_Contract(), WcType.WITH) }
        );
        L_FIND7:
        while (condition(vw_prap_View.readNextRow("L_FIND7")))
        {
            vw_prap_View.setIfNotFoundControlFlag(false);
            L_GET7:                                                                                                                                                       //Natural: GET PRAP-UPDATE *ISN ( L-FIND7. )
            vw_prap_Update.readByID(vw_prap_View.getAstISN("L_FIND7"), "L_GET7");
            prap_Update_Ap_Omni_Account_Issuance.setValue(pdaScia9600.getScia9600_Pnd_P_New_Def_Enrl_Ind());                                                              //Natural: ASSIGN PRAP-UPDATE.AP-OMNI-ACCOUNT-ISSUANCE := #P-NEW-DEF-ENRL-IND
            vw_prap_Update.updateDBRow("L_GET7");                                                                                                                         //Natural: UPDATE ( L-GET7. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Update_Done.setValue(true);                                                                                                                               //Natural: ASSIGN #UPDATE-DONE := TRUE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        vw_reprint_View.startDatabaseFind                                                                                                                                 //Natural: FIND REPRINT-VIEW WITH RP-TIAA-CONTR = #P-TIAA-CONTRACT
        (
        "L_FIND8",
        new Wc[] { new Wc("RP_TIAA_CONTR", "=", pdaScia9600.getScia9600_Pnd_P_Tiaa_Contract(), WcType.WITH) }
        );
        L_FIND8:
        while (condition(vw_reprint_View.readNextRow("L_FIND8")))
        {
            vw_reprint_View.setIfNotFoundControlFlag(false);
            L_GET8:                                                                                                                                                       //Natural: GET REPRINT-UPDATE *ISN ( L-FIND8. )
            vw_reprint_Update.readByID(vw_reprint_View.getAstISN("L_FIND8"), "L_GET8");
            reprint_Update_Rp_Tiaa_Service_Agent.setValue(pdaScia9600.getScia9600_Pnd_P_New_Def_Enrl_Ind());                                                              //Natural: ASSIGN REPRINT-UPDATE.RP-TIAA-SERVICE-AGENT := #P-NEW-DEF-ENRL-IND
            reprint_Update_Rp_Update_Date.setValue(Global.getDATN());                                                                                                     //Natural: ASSIGN REPRINT-UPDATE.RP-UPDATE-DATE := *DATN
            reprint_Update_Rp_Update_Time.setValue(Global.getTIMN());                                                                                                     //Natural: ASSIGN REPRINT-UPDATE.RP-UPDATE-TIME := *TIMN
            reprint_Update_Rp_Racf_Id.setValue(Global.getINIT_USER());                                                                                                    //Natural: ASSIGN REPRINT-UPDATE.RP-RACF-ID := *INIT-USER
            vw_reprint_Update.updateDBRow("L_GET8");                                                                                                                      //Natural: UPDATE ( L-GET8. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Update_Done.setValue(true);                                                                                                                               //Natural: ASSIGN #UPDATE-DONE := TRUE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"PROGRAM:",new TabSetting(25),Global.getPROGRAM(),NEWLINE,"ERROR:",new TabSetting(25),Global.getERROR_NR(),NEWLINE,"LINE:",new  //Natural: WRITE // 'PROGRAM:' 25T *PROGRAM / 'ERROR:' 25T *ERROR-NR / 'LINE:' 25T *ERROR-LINE
            TabSetting(25),Global.getERROR_LINE());
        pdaScia9600.getScia9600_Pnd_P_Return_Code().setValue(Global.getERROR_NR());                                                                                       //Natural: ASSIGN #P-RETURN-CODE := *ERROR-NR
    };                                                                                                                                                                    //Natural: END-ERROR
}
