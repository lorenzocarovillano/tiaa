/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:51:16 PM
**        * FROM NATURAL LDA     : APPL500
************************************************************
**        * FILE NAME            : LdaAppl500.java
**        * CLASS NAME           : LdaAppl500
**        * INSTANCE NAME        : LdaAppl500
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl500 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_aap_View;
    private DbsField aap_View_Ap_Coll_Code;
    private DbsField aap_View_Ap_G_Key;
    private DbsField aap_View_Ap_G_Ind;
    private DbsField aap_View_Ap_Soc_Sec;
    private DbsField aap_View_Ap_Dob;
    private DbsField aap_View_Ap_Lob;
    private DbsField aap_View_Ap_Bill_Code;
    private DbsField aap_View_Ap_T_Doi;
    private DbsField aap_View_Ap_C_Doi;
    private DbsField aap_View_Ap_Curr;
    private DbsField aap_View_Ap_T_Age_1st;
    private DbsField aap_View_Ap_C_Age_1st;
    private DbsField aap_View_Ap_Ownership;
    private DbsField aap_View_Ap_Sex;
    private DbsField aap_View_Ap_Mail_Zip;
    private DbsField aap_View_Ap_Name_Addr_Cd;
    private DbsField aap_View_Ap_Dt_Ent_Sys;
    private DbsField aap_View_Ap_Dt_Released;
    private DbsField aap_View_Ap_Dt_Matched;
    private DbsField aap_View_Ap_Dt_Deleted;
    private DbsField aap_View_Ap_Dt_Withdrawn;
    private DbsField aap_View_Ap_Alloc_Discr;
    private DbsField aap_View_Ap_Release_Ind;
    private DbsField aap_View_Ap_Type;
    private DbsField aap_View_Ap_Other_Pols;
    private DbsField aap_View_Ap_Record_Type;
    private DbsField aap_View_Ap_Status;
    private DbsField aap_View_Ap_Numb_Dailys;
    private DbsField aap_View_Ap_Dt_App_Recvd;
    private DbsField aap_View_Ap_App_Source;
    private DbsField aap_View_Ap_Region_Code;
    private DbsField aap_View_Ap_Orig_Issue_State;
    private DbsField aap_View_Ap_Ownership_Type;
    private DbsField aap_View_Ap_Lob_Type;
    private DbsField aap_View_Ap_Split_Code;
    private DbsGroup aap_View_Ap_Address_Info;
    private DbsField aap_View_Ap_Address_Line;
    private DbsField aap_View_Ap_City;
    private DbsField aap_View_Ap_Current_State_Code;
    private DbsField aap_View_Ap_Dana_Transaction_Cd;
    private DbsField aap_View_Ap_Dana_Stndrd_Rtn_Cd;
    private DbsField aap_View_Ap_Dana_Finalist_Reason_Cd;
    private DbsField aap_View_Ap_Dana_Addr_Stndrd_Code;
    private DbsField aap_View_Ap_Dana_Stndrd_Overide;
    private DbsField aap_View_Ap_Dana_Postal_Data_Fields;
    private DbsField aap_View_Ap_Dana_Addr_Geographic_Code;
    private DbsField aap_View_Ap_Annuity_Start_Date;
    private DbsGroup aap_View_Ap_Maximum_Alloc_Pct;
    private DbsField aap_View_Ap_Max_Alloc_Pct;
    private DbsField aap_View_Ap_Max_Alloc_Pct_Sign;
    private DbsField aap_View_Ap_Bene_Info_Type;
    private DbsField aap_View_Ap_Primary_Std_Ent;
    private DbsGroup aap_View_Ap_Bene_Primary_Info;
    private DbsField aap_View_Ap_Primary_Bene_Info;
    private DbsField aap_View_Ap_Contingent_Std_Ent;
    private DbsGroup aap_View_Ap_Bene_Contingent_Info;
    private DbsField aap_View_Ap_Contingent_Bene_Info;
    private DbsField aap_View_Ap_Bene_Estate;
    private DbsField aap_View_Ap_Bene_Trust;
    private DbsField aap_View_Ap_Bene_Category;
    private DbsField aap_View_Ap_Mail_Instructions;
    private DbsField aap_View_Ap_Eop_Addl_Cref_Request;
    private DbsField aap_View_Ap_Process_Status;
    private DbsField aap_View_Ap_Coll_St_Cd;
    private DbsField aap_View_Ap_Csm_Sec_Seg;
    private DbsField aap_View_Ap_Rlc_College;
    private DbsField aap_View_Ap_Cor_Prfx_Nme;
    private DbsField aap_View_Ap_Cor_Last_Nme;
    private DbsField aap_View_Ap_Cor_First_Nme;
    private DbsField aap_View_Ap_Cor_Mddle_Nme;
    private DbsField aap_View_Ap_Cor_Sffx_Nme;
    private DbsField aap_View_Ap_Ph_Hist_Ind;
    private DbsField aap_View_Ap_Rcrd_Updt_Tm_Stamp;
    private DbsField aap_View_Ap_Contact_Mode;
    private DbsField aap_View_Ap_Racf_Id;
    private DbsField aap_View_Ap_Pin_Nbr;
    private DbsGroup aap_View_Ap_Rqst_Log_Dte_Tm;
    private DbsField aap_View_Ap_Rqst_Log_Dte_Time;
    private DbsField aap_View_Ap_Sync_Ind;
    private DbsField aap_View_Ap_Cor_Ind;
    private DbsField aap_View_Ap_Alloc_Ind;
    private DbsField aap_View_Ap_Tiaa_Doi;
    private DbsField aap_View_Ap_Cref_Doi;
    private DbsGroup aap_View_Ap_Mit_Request;
    private DbsField aap_View_Ap_Mit_Unit;
    private DbsField aap_View_Ap_Mit_Wpid;
    private DbsField aap_View_Ap_Ira_Rollover_Type;
    private DbsField aap_View_Ap_Ira_Record_Type;
    private DbsField aap_View_Ap_Mult_App_Status;
    private DbsField aap_View_Ap_Mult_App_Lob;
    private DbsField aap_View_Ap_Mult_App_Lob_Type;
    private DbsField aap_View_Ap_Mult_App_Ppg;
    private DbsField aap_View_Ap_Print_Date;
    private DbsGroup aap_View_Ap_Cntrct_Info;
    private DbsField aap_View_Ap_Cntrct_Type;
    private DbsField aap_View_Ap_Cntrct_Nbr;
    private DbsField aap_View_Ap_Cntrct_Proceeds_Amt;
    private DbsGroup aap_View_Ap_Addr_Info;
    private DbsField aap_View_Ap_Address_Txt;
    private DbsField aap_View_Ap_Address_Dest_Name;
    private DbsField aap_View_Ap_Zip_Code;
    private DbsField aap_View_Ap_Bank_Pymnt_Acct_Nmbr;
    private DbsField aap_View_Ap_Bank_Aba_Acct_Nmbr;
    private DbsField aap_View_Ap_Addr_Usage_Code;
    private DbsField aap_View_Ap_Init_Paymt_Amt;
    private DbsField aap_View_Ap_Init_Paymt_Pct;
    private DbsField aap_View_Ap_Financial_1;
    private DbsField aap_View_Ap_Financial_2;
    private DbsField aap_View_Ap_Financial_3;
    private DbsField aap_View_Ap_Financial_4;
    private DbsField aap_View_Ap_Financial_5;
    private DbsField aap_View_Ap_Irc_Sectn_Grp_Cde;
    private DbsField aap_View_Ap_Irc_Sectn_Cde;
    private DbsField aap_View_Ap_Inst_Link_Cde;
    private DbsField aap_View_Ap_Applcnt_Req_Type;
    private DbsField aap_View_Ap_Addr_Sync_Ind;
    private DbsField aap_View_Ap_Tiaa_Service_Agent;
    private DbsField aap_View_Ap_Prap_Rsch_Mit_Ind;
    private DbsField aap_View_Ap_Ppg_Team_Cde;
    private DbsField aap_View_Ap_Tiaa_Cntrct;
    private DbsField aap_View_Ap_Cref_Cert;
    private DbsField aap_View_Ap_Rlc_Cref_Cert;
    private DbsField aap_View_Ap_Allocation_Model_Type;
    private DbsField aap_View_Ap_Divorce_Ind;
    private DbsField aap_View_Ap_Email_Address;
    private DbsField aap_View_Ap_E_Signed_Appl_Ind;
    private DbsField aap_View_Ap_Eft_Requested_Ind;
    private DbsField aap_View_Ap_Phone_No;
    private DbsField aap_View_Ap_Allocation_Fmt;
    private DbsGroup aap_View_Ap_Fund_Identifier;
    private DbsField aap_View_Ap_Fund_Cde;
    private DbsField aap_View_Ap_Allocation_Pct;
    private DbsField aap_View_Ap_Sgrd_Plan_No;
    private DbsField aap_View_Ap_Sgrd_Subplan_No;
    private DbsField aap_View_Ap_Sgrd_Part_Ext;

    public DataAccessProgramView getVw_aap_View() { return vw_aap_View; }

    public DbsField getAap_View_Ap_Coll_Code() { return aap_View_Ap_Coll_Code; }

    public DbsField getAap_View_Ap_G_Key() { return aap_View_Ap_G_Key; }

    public DbsField getAap_View_Ap_G_Ind() { return aap_View_Ap_G_Ind; }

    public DbsField getAap_View_Ap_Soc_Sec() { return aap_View_Ap_Soc_Sec; }

    public DbsField getAap_View_Ap_Dob() { return aap_View_Ap_Dob; }

    public DbsField getAap_View_Ap_Lob() { return aap_View_Ap_Lob; }

    public DbsField getAap_View_Ap_Bill_Code() { return aap_View_Ap_Bill_Code; }

    public DbsField getAap_View_Ap_T_Doi() { return aap_View_Ap_T_Doi; }

    public DbsField getAap_View_Ap_C_Doi() { return aap_View_Ap_C_Doi; }

    public DbsField getAap_View_Ap_Curr() { return aap_View_Ap_Curr; }

    public DbsField getAap_View_Ap_T_Age_1st() { return aap_View_Ap_T_Age_1st; }

    public DbsField getAap_View_Ap_C_Age_1st() { return aap_View_Ap_C_Age_1st; }

    public DbsField getAap_View_Ap_Ownership() { return aap_View_Ap_Ownership; }

    public DbsField getAap_View_Ap_Sex() { return aap_View_Ap_Sex; }

    public DbsField getAap_View_Ap_Mail_Zip() { return aap_View_Ap_Mail_Zip; }

    public DbsField getAap_View_Ap_Name_Addr_Cd() { return aap_View_Ap_Name_Addr_Cd; }

    public DbsField getAap_View_Ap_Dt_Ent_Sys() { return aap_View_Ap_Dt_Ent_Sys; }

    public DbsField getAap_View_Ap_Dt_Released() { return aap_View_Ap_Dt_Released; }

    public DbsField getAap_View_Ap_Dt_Matched() { return aap_View_Ap_Dt_Matched; }

    public DbsField getAap_View_Ap_Dt_Deleted() { return aap_View_Ap_Dt_Deleted; }

    public DbsField getAap_View_Ap_Dt_Withdrawn() { return aap_View_Ap_Dt_Withdrawn; }

    public DbsField getAap_View_Ap_Alloc_Discr() { return aap_View_Ap_Alloc_Discr; }

    public DbsField getAap_View_Ap_Release_Ind() { return aap_View_Ap_Release_Ind; }

    public DbsField getAap_View_Ap_Type() { return aap_View_Ap_Type; }

    public DbsField getAap_View_Ap_Other_Pols() { return aap_View_Ap_Other_Pols; }

    public DbsField getAap_View_Ap_Record_Type() { return aap_View_Ap_Record_Type; }

    public DbsField getAap_View_Ap_Status() { return aap_View_Ap_Status; }

    public DbsField getAap_View_Ap_Numb_Dailys() { return aap_View_Ap_Numb_Dailys; }

    public DbsField getAap_View_Ap_Dt_App_Recvd() { return aap_View_Ap_Dt_App_Recvd; }

    public DbsField getAap_View_Ap_App_Source() { return aap_View_Ap_App_Source; }

    public DbsField getAap_View_Ap_Region_Code() { return aap_View_Ap_Region_Code; }

    public DbsField getAap_View_Ap_Orig_Issue_State() { return aap_View_Ap_Orig_Issue_State; }

    public DbsField getAap_View_Ap_Ownership_Type() { return aap_View_Ap_Ownership_Type; }

    public DbsField getAap_View_Ap_Lob_Type() { return aap_View_Ap_Lob_Type; }

    public DbsField getAap_View_Ap_Split_Code() { return aap_View_Ap_Split_Code; }

    public DbsGroup getAap_View_Ap_Address_Info() { return aap_View_Ap_Address_Info; }

    public DbsField getAap_View_Ap_Address_Line() { return aap_View_Ap_Address_Line; }

    public DbsField getAap_View_Ap_City() { return aap_View_Ap_City; }

    public DbsField getAap_View_Ap_Current_State_Code() { return aap_View_Ap_Current_State_Code; }

    public DbsField getAap_View_Ap_Dana_Transaction_Cd() { return aap_View_Ap_Dana_Transaction_Cd; }

    public DbsField getAap_View_Ap_Dana_Stndrd_Rtn_Cd() { return aap_View_Ap_Dana_Stndrd_Rtn_Cd; }

    public DbsField getAap_View_Ap_Dana_Finalist_Reason_Cd() { return aap_View_Ap_Dana_Finalist_Reason_Cd; }

    public DbsField getAap_View_Ap_Dana_Addr_Stndrd_Code() { return aap_View_Ap_Dana_Addr_Stndrd_Code; }

    public DbsField getAap_View_Ap_Dana_Stndrd_Overide() { return aap_View_Ap_Dana_Stndrd_Overide; }

    public DbsField getAap_View_Ap_Dana_Postal_Data_Fields() { return aap_View_Ap_Dana_Postal_Data_Fields; }

    public DbsField getAap_View_Ap_Dana_Addr_Geographic_Code() { return aap_View_Ap_Dana_Addr_Geographic_Code; }

    public DbsField getAap_View_Ap_Annuity_Start_Date() { return aap_View_Ap_Annuity_Start_Date; }

    public DbsGroup getAap_View_Ap_Maximum_Alloc_Pct() { return aap_View_Ap_Maximum_Alloc_Pct; }

    public DbsField getAap_View_Ap_Max_Alloc_Pct() { return aap_View_Ap_Max_Alloc_Pct; }

    public DbsField getAap_View_Ap_Max_Alloc_Pct_Sign() { return aap_View_Ap_Max_Alloc_Pct_Sign; }

    public DbsField getAap_View_Ap_Bene_Info_Type() { return aap_View_Ap_Bene_Info_Type; }

    public DbsField getAap_View_Ap_Primary_Std_Ent() { return aap_View_Ap_Primary_Std_Ent; }

    public DbsGroup getAap_View_Ap_Bene_Primary_Info() { return aap_View_Ap_Bene_Primary_Info; }

    public DbsField getAap_View_Ap_Primary_Bene_Info() { return aap_View_Ap_Primary_Bene_Info; }

    public DbsField getAap_View_Ap_Contingent_Std_Ent() { return aap_View_Ap_Contingent_Std_Ent; }

    public DbsGroup getAap_View_Ap_Bene_Contingent_Info() { return aap_View_Ap_Bene_Contingent_Info; }

    public DbsField getAap_View_Ap_Contingent_Bene_Info() { return aap_View_Ap_Contingent_Bene_Info; }

    public DbsField getAap_View_Ap_Bene_Estate() { return aap_View_Ap_Bene_Estate; }

    public DbsField getAap_View_Ap_Bene_Trust() { return aap_View_Ap_Bene_Trust; }

    public DbsField getAap_View_Ap_Bene_Category() { return aap_View_Ap_Bene_Category; }

    public DbsField getAap_View_Ap_Mail_Instructions() { return aap_View_Ap_Mail_Instructions; }

    public DbsField getAap_View_Ap_Eop_Addl_Cref_Request() { return aap_View_Ap_Eop_Addl_Cref_Request; }

    public DbsField getAap_View_Ap_Process_Status() { return aap_View_Ap_Process_Status; }

    public DbsField getAap_View_Ap_Coll_St_Cd() { return aap_View_Ap_Coll_St_Cd; }

    public DbsField getAap_View_Ap_Csm_Sec_Seg() { return aap_View_Ap_Csm_Sec_Seg; }

    public DbsField getAap_View_Ap_Rlc_College() { return aap_View_Ap_Rlc_College; }

    public DbsField getAap_View_Ap_Cor_Prfx_Nme() { return aap_View_Ap_Cor_Prfx_Nme; }

    public DbsField getAap_View_Ap_Cor_Last_Nme() { return aap_View_Ap_Cor_Last_Nme; }

    public DbsField getAap_View_Ap_Cor_First_Nme() { return aap_View_Ap_Cor_First_Nme; }

    public DbsField getAap_View_Ap_Cor_Mddle_Nme() { return aap_View_Ap_Cor_Mddle_Nme; }

    public DbsField getAap_View_Ap_Cor_Sffx_Nme() { return aap_View_Ap_Cor_Sffx_Nme; }

    public DbsField getAap_View_Ap_Ph_Hist_Ind() { return aap_View_Ap_Ph_Hist_Ind; }

    public DbsField getAap_View_Ap_Rcrd_Updt_Tm_Stamp() { return aap_View_Ap_Rcrd_Updt_Tm_Stamp; }

    public DbsField getAap_View_Ap_Contact_Mode() { return aap_View_Ap_Contact_Mode; }

    public DbsField getAap_View_Ap_Racf_Id() { return aap_View_Ap_Racf_Id; }

    public DbsField getAap_View_Ap_Pin_Nbr() { return aap_View_Ap_Pin_Nbr; }

    public DbsGroup getAap_View_Ap_Rqst_Log_Dte_Tm() { return aap_View_Ap_Rqst_Log_Dte_Tm; }

    public DbsField getAap_View_Ap_Rqst_Log_Dte_Time() { return aap_View_Ap_Rqst_Log_Dte_Time; }

    public DbsField getAap_View_Ap_Sync_Ind() { return aap_View_Ap_Sync_Ind; }

    public DbsField getAap_View_Ap_Cor_Ind() { return aap_View_Ap_Cor_Ind; }

    public DbsField getAap_View_Ap_Alloc_Ind() { return aap_View_Ap_Alloc_Ind; }

    public DbsField getAap_View_Ap_Tiaa_Doi() { return aap_View_Ap_Tiaa_Doi; }

    public DbsField getAap_View_Ap_Cref_Doi() { return aap_View_Ap_Cref_Doi; }

    public DbsGroup getAap_View_Ap_Mit_Request() { return aap_View_Ap_Mit_Request; }

    public DbsField getAap_View_Ap_Mit_Unit() { return aap_View_Ap_Mit_Unit; }

    public DbsField getAap_View_Ap_Mit_Wpid() { return aap_View_Ap_Mit_Wpid; }

    public DbsField getAap_View_Ap_Ira_Rollover_Type() { return aap_View_Ap_Ira_Rollover_Type; }

    public DbsField getAap_View_Ap_Ira_Record_Type() { return aap_View_Ap_Ira_Record_Type; }

    public DbsField getAap_View_Ap_Mult_App_Status() { return aap_View_Ap_Mult_App_Status; }

    public DbsField getAap_View_Ap_Mult_App_Lob() { return aap_View_Ap_Mult_App_Lob; }

    public DbsField getAap_View_Ap_Mult_App_Lob_Type() { return aap_View_Ap_Mult_App_Lob_Type; }

    public DbsField getAap_View_Ap_Mult_App_Ppg() { return aap_View_Ap_Mult_App_Ppg; }

    public DbsField getAap_View_Ap_Print_Date() { return aap_View_Ap_Print_Date; }

    public DbsGroup getAap_View_Ap_Cntrct_Info() { return aap_View_Ap_Cntrct_Info; }

    public DbsField getAap_View_Ap_Cntrct_Type() { return aap_View_Ap_Cntrct_Type; }

    public DbsField getAap_View_Ap_Cntrct_Nbr() { return aap_View_Ap_Cntrct_Nbr; }

    public DbsField getAap_View_Ap_Cntrct_Proceeds_Amt() { return aap_View_Ap_Cntrct_Proceeds_Amt; }

    public DbsGroup getAap_View_Ap_Addr_Info() { return aap_View_Ap_Addr_Info; }

    public DbsField getAap_View_Ap_Address_Txt() { return aap_View_Ap_Address_Txt; }

    public DbsField getAap_View_Ap_Address_Dest_Name() { return aap_View_Ap_Address_Dest_Name; }

    public DbsField getAap_View_Ap_Zip_Code() { return aap_View_Ap_Zip_Code; }

    public DbsField getAap_View_Ap_Bank_Pymnt_Acct_Nmbr() { return aap_View_Ap_Bank_Pymnt_Acct_Nmbr; }

    public DbsField getAap_View_Ap_Bank_Aba_Acct_Nmbr() { return aap_View_Ap_Bank_Aba_Acct_Nmbr; }

    public DbsField getAap_View_Ap_Addr_Usage_Code() { return aap_View_Ap_Addr_Usage_Code; }

    public DbsField getAap_View_Ap_Init_Paymt_Amt() { return aap_View_Ap_Init_Paymt_Amt; }

    public DbsField getAap_View_Ap_Init_Paymt_Pct() { return aap_View_Ap_Init_Paymt_Pct; }

    public DbsField getAap_View_Ap_Financial_1() { return aap_View_Ap_Financial_1; }

    public DbsField getAap_View_Ap_Financial_2() { return aap_View_Ap_Financial_2; }

    public DbsField getAap_View_Ap_Financial_3() { return aap_View_Ap_Financial_3; }

    public DbsField getAap_View_Ap_Financial_4() { return aap_View_Ap_Financial_4; }

    public DbsField getAap_View_Ap_Financial_5() { return aap_View_Ap_Financial_5; }

    public DbsField getAap_View_Ap_Irc_Sectn_Grp_Cde() { return aap_View_Ap_Irc_Sectn_Grp_Cde; }

    public DbsField getAap_View_Ap_Irc_Sectn_Cde() { return aap_View_Ap_Irc_Sectn_Cde; }

    public DbsField getAap_View_Ap_Inst_Link_Cde() { return aap_View_Ap_Inst_Link_Cde; }

    public DbsField getAap_View_Ap_Applcnt_Req_Type() { return aap_View_Ap_Applcnt_Req_Type; }

    public DbsField getAap_View_Ap_Addr_Sync_Ind() { return aap_View_Ap_Addr_Sync_Ind; }

    public DbsField getAap_View_Ap_Tiaa_Service_Agent() { return aap_View_Ap_Tiaa_Service_Agent; }

    public DbsField getAap_View_Ap_Prap_Rsch_Mit_Ind() { return aap_View_Ap_Prap_Rsch_Mit_Ind; }

    public DbsField getAap_View_Ap_Ppg_Team_Cde() { return aap_View_Ap_Ppg_Team_Cde; }

    public DbsField getAap_View_Ap_Tiaa_Cntrct() { return aap_View_Ap_Tiaa_Cntrct; }

    public DbsField getAap_View_Ap_Cref_Cert() { return aap_View_Ap_Cref_Cert; }

    public DbsField getAap_View_Ap_Rlc_Cref_Cert() { return aap_View_Ap_Rlc_Cref_Cert; }

    public DbsField getAap_View_Ap_Allocation_Model_Type() { return aap_View_Ap_Allocation_Model_Type; }

    public DbsField getAap_View_Ap_Divorce_Ind() { return aap_View_Ap_Divorce_Ind; }

    public DbsField getAap_View_Ap_Email_Address() { return aap_View_Ap_Email_Address; }

    public DbsField getAap_View_Ap_E_Signed_Appl_Ind() { return aap_View_Ap_E_Signed_Appl_Ind; }

    public DbsField getAap_View_Ap_Eft_Requested_Ind() { return aap_View_Ap_Eft_Requested_Ind; }

    public DbsField getAap_View_Ap_Phone_No() { return aap_View_Ap_Phone_No; }

    public DbsField getAap_View_Ap_Allocation_Fmt() { return aap_View_Ap_Allocation_Fmt; }

    public DbsGroup getAap_View_Ap_Fund_Identifier() { return aap_View_Ap_Fund_Identifier; }

    public DbsField getAap_View_Ap_Fund_Cde() { return aap_View_Ap_Fund_Cde; }

    public DbsField getAap_View_Ap_Allocation_Pct() { return aap_View_Ap_Allocation_Pct; }

    public DbsField getAap_View_Ap_Sgrd_Plan_No() { return aap_View_Ap_Sgrd_Plan_No; }

    public DbsField getAap_View_Ap_Sgrd_Subplan_No() { return aap_View_Ap_Sgrd_Subplan_No; }

    public DbsField getAap_View_Ap_Sgrd_Part_Ext() { return aap_View_Ap_Sgrd_Part_Ext; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_aap_View = new DataAccessProgramView(new NameInfo("vw_aap_View", "AAP-VIEW"), "ANNTY_ACTVTY_PRAP_12", "ACIS_ANNTY_PRAP", DdmPeriodicGroups.getInstance().getGroups("ANNTY_ACTVTY_PRAP_12"));
        aap_View_Ap_Coll_Code = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Coll_Code", "AP-COLL-CODE", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_COLL_CODE");
        aap_View_Ap_G_Key = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_G_Key", "AP-G-KEY", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, 
            "AP_G_KEY");
        aap_View_Ap_G_Ind = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_G_Ind", "AP-G-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_G_IND");
        aap_View_Ap_Soc_Sec = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, 
            "AP_SOC_SEC");
        aap_View_Ap_Dob = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Dob", "AP-DOB", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, 
            "AP_DOB");
        aap_View_Ap_Lob = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Lob", "AP-LOB", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LOB");
        aap_View_Ap_Lob.setDdmHeader("LOB");
        aap_View_Ap_Bill_Code = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Bill_Code", "AP-BILL-CODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_BILL_CODE");
        aap_View_Ap_T_Doi = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_T_Doi", "AP-T-DOI", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, 
            "AP_T_DOI");
        aap_View_Ap_C_Doi = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_C_Doi", "AP-C-DOI", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, 
            "AP_C_DOI");
        aap_View_Ap_Curr = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Curr", "AP-CURR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_CURR");
        aap_View_Ap_T_Age_1st = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_T_Age_1st", "AP-T-AGE-1ST", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, 
            "AP_T_AGE_1ST");
        aap_View_Ap_C_Age_1st = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_C_Age_1st", "AP-C-AGE-1ST", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, 
            "AP_C_AGE_1ST");
        aap_View_Ap_Ownership = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Ownership", "AP-OWNERSHIP", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_OWNERSHIP");
        aap_View_Ap_Sex = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Sex", "AP-SEX", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_SEX");
        aap_View_Ap_Sex.setDdmHeader("SEX CDE");
        aap_View_Ap_Mail_Zip = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Mail_Zip", "AP-MAIL-ZIP", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "AP_MAIL_ZIP");
        aap_View_Ap_Name_Addr_Cd = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Name_Addr_Cd", "AP-NAME-ADDR-CD", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "AP_NAME_ADDR_CD");
        aap_View_Ap_Dt_Ent_Sys = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Dt_Ent_Sys", "AP-DT-ENT-SYS", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, 
            "AP_DT_ENT_SYS");
        aap_View_Ap_Dt_Released = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Dt_Released", "AP-DT-RELEASED", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, 
            "AP_DT_RELEASED");
        aap_View_Ap_Dt_Matched = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Dt_Matched", "AP-DT-MATCHED", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, 
            "AP_DT_MATCHED");
        aap_View_Ap_Dt_Deleted = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Dt_Deleted", "AP-DT-DELETED", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, 
            "AP_DT_DELETED");
        aap_View_Ap_Dt_Withdrawn = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Dt_Withdrawn", "AP-DT-WITHDRAWN", FieldType.PACKED_DECIMAL, 8, 
            RepeatingFieldStrategy.None, "AP_DT_WITHDRAWN");
        aap_View_Ap_Alloc_Discr = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Alloc_Discr", "AP-ALLOC-DISCR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_ALLOC_DISCR");
        aap_View_Ap_Release_Ind = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Release_Ind", "AP-RELEASE-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_RELEASE_IND");
        aap_View_Ap_Type = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Type", "AP-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_TYPE");
        aap_View_Ap_Other_Pols = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Other_Pols", "AP-OTHER-POLS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_OTHER_POLS");
        aap_View_Ap_Record_Type = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Record_Type", "AP-RECORD-TYPE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_RECORD_TYPE");
        aap_View_Ap_Status = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Status", "AP-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_STATUS");
        aap_View_Ap_Numb_Dailys = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Numb_Dailys", "AP-NUMB-DAILYS", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_NUMB_DAILYS");
        aap_View_Ap_Dt_App_Recvd = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Dt_App_Recvd", "AP-DT-APP-RECVD", FieldType.PACKED_DECIMAL, 8, 
            RepeatingFieldStrategy.None, "AP_DT_APP_RECVD");
        aap_View_Ap_App_Source = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_App_Source", "AP-APP-SOURCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_APP_SOURCE");
        aap_View_Ap_App_Source.setDdmHeader("SOURCE");
        aap_View_Ap_Region_Code = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Region_Code", "AP-REGION-CODE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "AP_REGION_CODE");
        aap_View_Ap_Orig_Issue_State = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Orig_Issue_State", "AP-ORIG-ISSUE-STATE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "AP_ORIG_ISSUE_STATE");
        aap_View_Ap_Ownership_Type = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Ownership_Type", "AP-OWNERSHIP-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_OWNERSHIP_TYPE");
        aap_View_Ap_Ownership_Type.setDdmHeader("CNTRCT OWNERSHIP");
        aap_View_Ap_Lob_Type = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Lob_Type", "AP-LOB-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_LOB_TYPE");
        aap_View_Ap_Lob_Type.setDdmHeader("LOB/TYPE");
        aap_View_Ap_Split_Code = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Split_Code", "AP-SPLIT-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "AP_SPLIT_CODE");
        aap_View_Ap_Address_Info = vw_aap_View.getRecord().newGroupArrayInGroup("aap_View_Ap_Address_Info", "AP-ADDRESS-INFO", new DbsArrayController(1,5), 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        aap_View_Ap_Address_Line = aap_View_Ap_Address_Info.newFieldInGroup("aap_View_Ap_Address_Line", "AP-ADDRESS-LINE", FieldType.STRING, 35, null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ADDRESS_LINE", "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        aap_View_Ap_City = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_City", "AP-CITY", FieldType.STRING, 27, RepeatingFieldStrategy.None, "AP_CITY");
        aap_View_Ap_Current_State_Code = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Current_State_Code", "AP-CURRENT-STATE-CODE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "AP_CURRENT_STATE_CODE");
        aap_View_Ap_Dana_Transaction_Cd = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Dana_Transaction_Cd", "AP-DANA-TRANSACTION-CD", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "AP_DANA_TRANSACTION_CD");
        aap_View_Ap_Dana_Stndrd_Rtn_Cd = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Dana_Stndrd_Rtn_Cd", "AP-DANA-STNDRD-RTN-CD", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "AP_DANA_STNDRD_RTN_CD");
        aap_View_Ap_Dana_Finalist_Reason_Cd = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Dana_Finalist_Reason_Cd", "AP-DANA-FINALIST-REASON-CD", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_DANA_FINALIST_REASON_CD");
        aap_View_Ap_Dana_Addr_Stndrd_Code = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Dana_Addr_Stndrd_Code", "AP-DANA-ADDR-STNDRD-CODE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_DANA_ADDR_STNDRD_CODE");
        aap_View_Ap_Dana_Stndrd_Overide = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Dana_Stndrd_Overide", "AP-DANA-STNDRD-OVERIDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_DANA_STNDRD_OVERIDE");
        aap_View_Ap_Dana_Postal_Data_Fields = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Dana_Postal_Data_Fields", "AP-DANA-POSTAL-DATA-FIELDS", 
            FieldType.STRING, 44, RepeatingFieldStrategy.None, "AP_DANA_POSTAL_DATA_FIELDS");
        aap_View_Ap_Dana_Addr_Geographic_Code = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Dana_Addr_Geographic_Code", "AP-DANA-ADDR-GEOGRAPHIC-CODE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_DANA_ADDR_GEOGRAPHIC_CODE");
        aap_View_Ap_Annuity_Start_Date = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Annuity_Start_Date", "AP-ANNUITY-START-DATE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AP_ANNUITY_START_DATE");
        aap_View_Ap_Maximum_Alloc_Pct = vw_aap_View.getRecord().newGroupArrayInGroup("aap_View_Ap_Maximum_Alloc_Pct", "AP-MAXIMUM-ALLOC-PCT", new DbsArrayController(1,100), 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        aap_View_Ap_Max_Alloc_Pct = aap_View_Ap_Maximum_Alloc_Pct.newFieldInGroup("aap_View_Ap_Max_Alloc_Pct", "AP-MAX-ALLOC-PCT", FieldType.STRING, 3, 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MAX_ALLOC_PCT", "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        aap_View_Ap_Max_Alloc_Pct_Sign = aap_View_Ap_Maximum_Alloc_Pct.newFieldInGroup("aap_View_Ap_Max_Alloc_Pct_Sign", "AP-MAX-ALLOC-PCT-SIGN", FieldType.STRING, 
            1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MAX_ALLOC_PCT_SIGN", "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        aap_View_Ap_Bene_Info_Type = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Bene_Info_Type", "AP-BENE-INFO-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_BENE_INFO_TYPE");
        aap_View_Ap_Primary_Std_Ent = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Primary_Std_Ent", "AP-PRIMARY-STD-ENT", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_PRIMARY_STD_ENT");
        aap_View_Ap_Bene_Primary_Info = vw_aap_View.getRecord().newGroupArrayInGroup("aap_View_Ap_Bene_Primary_Info", "AP-BENE-PRIMARY-INFO", new DbsArrayController(1,5), 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_BENE_PRIMARY_INFO");
        aap_View_Ap_Primary_Bene_Info = aap_View_Ap_Bene_Primary_Info.newFieldInGroup("aap_View_Ap_Primary_Bene_Info", "AP-PRIMARY-BENE-INFO", FieldType.STRING, 
            72, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_PRIMARY_BENE_INFO", "ACIS_ANNTY_PRAP_AP_BENE_PRIMARY_INFO");
        aap_View_Ap_Contingent_Std_Ent = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Contingent_Std_Ent", "AP-CONTINGENT-STD-ENT", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_CONTINGENT_STD_ENT");
        aap_View_Ap_Bene_Contingent_Info = vw_aap_View.getRecord().newGroupArrayInGroup("aap_View_Ap_Bene_Contingent_Info", "AP-BENE-CONTINGENT-INFO", 
            new DbsArrayController(1,5), RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_BENE_CONTINGENT_INFO");
        aap_View_Ap_Contingent_Bene_Info = aap_View_Ap_Bene_Contingent_Info.newFieldInGroup("aap_View_Ap_Contingent_Bene_Info", "AP-CONTINGENT-BENE-INFO", 
            FieldType.STRING, 72, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CONTINGENT_BENE_INFO", "ACIS_ANNTY_PRAP_AP_BENE_CONTINGENT_INFO");
        aap_View_Ap_Bene_Estate = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Bene_Estate", "AP-BENE-ESTATE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_BENE_ESTATE");
        aap_View_Ap_Bene_Trust = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Bene_Trust", "AP-BENE-TRUST", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_BENE_TRUST");
        aap_View_Ap_Bene_Category = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Bene_Category", "AP-BENE-CATEGORY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_BENE_CATEGORY");
        aap_View_Ap_Mail_Instructions = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Mail_Instructions", "AP-MAIL-INSTRUCTIONS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_MAIL_INSTRUCTIONS");
        aap_View_Ap_Eop_Addl_Cref_Request = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Eop_Addl_Cref_Request", "AP-EOP-ADDL-CREF-REQUEST", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_EOP_ADDL_CREF_REQUEST");
        aap_View_Ap_Process_Status = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Process_Status", "AP-PROCESS-STATUS", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "AP_PROCESS_STATUS");
        aap_View_Ap_Coll_St_Cd = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Coll_St_Cd", "AP-COLL-ST-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "AP_COLL_ST_CD");
        aap_View_Ap_Csm_Sec_Seg = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Csm_Sec_Seg", "AP-CSM-SEC-SEG", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "AP_CSM_SEC_SEG");
        aap_View_Ap_Rlc_College = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Rlc_College", "AP-RLC-COLLEGE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "AP_RLC_COLLEGE");
        aap_View_Ap_Cor_Prfx_Nme = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Cor_Prfx_Nme", "AP-COR-PRFX-NME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "AP_COR_PRFX_NME");
        aap_View_Ap_Cor_Last_Nme = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "AP_COR_LAST_NME");
        aap_View_Ap_Cor_First_Nme = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "AP_COR_FIRST_NME");
        aap_View_Ap_Cor_Mddle_Nme = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Cor_Mddle_Nme", "AP-COR-MDDLE-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "AP_COR_MDDLE_NME");
        aap_View_Ap_Cor_Sffx_Nme = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Cor_Sffx_Nme", "AP-COR-SFFX-NME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "AP_COR_SFFX_NME");
        aap_View_Ap_Ph_Hist_Ind = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Ph_Hist_Ind", "AP-PH-HIST-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_PH_HIST_IND");
        aap_View_Ap_Rcrd_Updt_Tm_Stamp = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Rcrd_Updt_Tm_Stamp", "AP-RCRD-UPDT-TM-STAMP", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AP_RCRD_UPDT_TM_STAMP");
        aap_View_Ap_Contact_Mode = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Contact_Mode", "AP-CONTACT-MODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_CONTACT_MODE");
        aap_View_Ap_Racf_Id = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Racf_Id", "AP-RACF-ID", FieldType.STRING, 16, RepeatingFieldStrategy.None, 
            "AP_RACF_ID");
        aap_View_Ap_Pin_Nbr = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "AP_PIN_NBR");
        aap_View_Ap_Rqst_Log_Dte_Tm = vw_aap_View.getRecord().newGroupArrayInGroup("aap_View_Ap_Rqst_Log_Dte_Tm", "AP-RQST-LOG-DTE-TM", new DbsArrayController(1,5), 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        aap_View_Ap_Rqst_Log_Dte_Time = aap_View_Ap_Rqst_Log_Dte_Tm.newFieldInGroup("aap_View_Ap_Rqst_Log_Dte_Time", "AP-RQST-LOG-DTE-TIME", FieldType.STRING, 
            15, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_RQST_LOG_DTE_TIME", "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        aap_View_Ap_Sync_Ind = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Sync_Ind", "AP-SYNC-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_SYNC_IND");
        aap_View_Ap_Cor_Ind = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Cor_Ind", "AP-COR-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_COR_IND");
        aap_View_Ap_Alloc_Ind = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Alloc_Ind", "AP-ALLOC-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_ALLOC_IND");
        aap_View_Ap_Tiaa_Doi = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Tiaa_Doi", "AP-TIAA-DOI", FieldType.DATE, RepeatingFieldStrategy.None, 
            "AP_TIAA_DOI");
        aap_View_Ap_Cref_Doi = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Cref_Doi", "AP-CREF-DOI", FieldType.DATE, RepeatingFieldStrategy.None, 
            "AP_CREF_DOI");
        aap_View_Ap_Mit_Request = vw_aap_View.getRecord().newGroupArrayInGroup("aap_View_Ap_Mit_Request", "AP-MIT-REQUEST", new DbsArrayController(1,5), 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        aap_View_Ap_Mit_Unit = aap_View_Ap_Mit_Request.newFieldInGroup("aap_View_Ap_Mit_Unit", "AP-MIT-UNIT", FieldType.STRING, 8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "AP_MIT_UNIT", "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        aap_View_Ap_Mit_Wpid = aap_View_Ap_Mit_Request.newFieldInGroup("aap_View_Ap_Mit_Wpid", "AP-MIT-WPID", FieldType.STRING, 6, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "AP_MIT_WPID", "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        aap_View_Ap_Ira_Rollover_Type = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Ira_Rollover_Type", "AP-IRA-ROLLOVER-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_IRA_ROLLOVER_TYPE");
        aap_View_Ap_Ira_Record_Type = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Ira_Record_Type", "AP-IRA-RECORD-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "AP_IRA_RECORD_TYPE");
        aap_View_Ap_Mult_App_Status = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Mult_App_Status", "AP-MULT-APP-STATUS", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "AP_MULT_APP_STATUS");
        aap_View_Ap_Mult_App_Lob = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Mult_App_Lob", "AP-MULT-APP-LOB", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_MULT_APP_LOB");
        aap_View_Ap_Mult_App_Lob_Type = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Mult_App_Lob_Type", "AP-MULT-APP-LOB-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_MULT_APP_LOB_TYPE");
        aap_View_Ap_Mult_App_Ppg = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Mult_App_Ppg", "AP-MULT-APP-PPG", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_MULT_APP_PPG");
        aap_View_Ap_Print_Date = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Print_Date", "AP-PRINT-DATE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "AP_PRINT_DATE");
        aap_View_Ap_Cntrct_Info = vw_aap_View.getRecord().newGroupArrayInGroup("aap_View_Ap_Cntrct_Info", "AP-CNTRCT-INFO", new DbsArrayController(1,15), 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");
        aap_View_Ap_Cntrct_Type = aap_View_Ap_Cntrct_Info.newFieldInGroup("aap_View_Ap_Cntrct_Type", "AP-CNTRCT-TYPE", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "AP_CNTRCT_TYPE", "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");
        aap_View_Ap_Cntrct_Nbr = aap_View_Ap_Cntrct_Info.newFieldInGroup("aap_View_Ap_Cntrct_Nbr", "AP-CNTRCT-NBR", FieldType.STRING, 10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "AP_CNTRCT_NBR", "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");
        aap_View_Ap_Cntrct_Proceeds_Amt = aap_View_Ap_Cntrct_Info.newFieldInGroup("aap_View_Ap_Cntrct_Proceeds_Amt", "AP-CNTRCT-PROCEEDS-AMT", FieldType.NUMERIC, 
            10, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CNTRCT_PROCEEDS_AMT", "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");
        aap_View_Ap_Addr_Info = vw_aap_View.getRecord().newGroupArrayInGroup("aap_View_Ap_Addr_Info", "AP-ADDR-INFO", new DbsArrayController(1,5), RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_ANNTY_PRAP_AP_ADDR_INFO");
        aap_View_Ap_Address_Txt = aap_View_Ap_Addr_Info.newFieldInGroup("aap_View_Ap_Address_Txt", "AP-ADDRESS-TXT", FieldType.STRING, 35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "AP_ADDRESS_TXT", "ACIS_ANNTY_PRAP_AP_ADDR_INFO");
        aap_View_Ap_Address_Dest_Name = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Address_Dest_Name", "AP-ADDRESS-DEST-NAME", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "AP_ADDRESS_DEST_NAME");
        aap_View_Ap_Zip_Code = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Zip_Code", "AP-ZIP-CODE", FieldType.STRING, 9, RepeatingFieldStrategy.None, 
            "AP_ZIP_CODE");
        aap_View_Ap_Bank_Pymnt_Acct_Nmbr = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Bank_Pymnt_Acct_Nmbr", "AP-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 
            21, RepeatingFieldStrategy.None, "AP_BANK_PYMNT_ACCT_NMBR");
        aap_View_Ap_Bank_Aba_Acct_Nmbr = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Bank_Aba_Acct_Nmbr", "AP-BANK-ABA-ACCT-NMBR", FieldType.STRING, 
            9, RepeatingFieldStrategy.None, "AP_BANK_ABA_ACCT_NMBR");
        aap_View_Ap_Addr_Usage_Code = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Addr_Usage_Code", "AP-ADDR-USAGE-CODE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "AP_ADDR_USAGE_CODE");
        aap_View_Ap_Init_Paymt_Amt = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Init_Paymt_Amt", "AP-INIT-PAYMT-AMT", FieldType.NUMERIC, 10, 
            2, RepeatingFieldStrategy.None, "AP_INIT_PAYMT_AMT");
        aap_View_Ap_Init_Paymt_Pct = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Init_Paymt_Pct", "AP-INIT-PAYMT-PCT", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "AP_INIT_PAYMT_PCT");
        aap_View_Ap_Financial_1 = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Financial_1", "AP-FINANCIAL-1", FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, 
            "AP_FINANCIAL_1");
        aap_View_Ap_Financial_2 = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Financial_2", "AP-FINANCIAL-2", FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, 
            "AP_FINANCIAL_2");
        aap_View_Ap_Financial_3 = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Financial_3", "AP-FINANCIAL-3", FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, 
            "AP_FINANCIAL_3");
        aap_View_Ap_Financial_4 = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Financial_4", "AP-FINANCIAL-4", FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, 
            "AP_FINANCIAL_4");
        aap_View_Ap_Financial_5 = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Financial_5", "AP-FINANCIAL-5", FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, 
            "AP_FINANCIAL_5");
        aap_View_Ap_Irc_Sectn_Grp_Cde = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Irc_Sectn_Grp_Cde", "AP-IRC-SECTN-GRP-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "AP_IRC_SECTN_GRP_CDE");
        aap_View_Ap_Irc_Sectn_Cde = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Irc_Sectn_Cde", "AP-IRC-SECTN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "AP_IRC_SECTN_CDE");
        aap_View_Ap_Inst_Link_Cde = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Inst_Link_Cde", "AP-INST-LINK-CDE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "AP_INST_LINK_CDE");
        aap_View_Ap_Applcnt_Req_Type = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Applcnt_Req_Type", "AP-APPLCNT-REQ-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_APPLCNT_REQ_TYPE");
        aap_View_Ap_Applcnt_Req_Type.setDdmHeader("APP REQ TYPE");
        aap_View_Ap_Addr_Sync_Ind = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Addr_Sync_Ind", "AP-ADDR-SYNC-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_ADDR_SYNC_IND");
        aap_View_Ap_Tiaa_Service_Agent = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Tiaa_Service_Agent", "AP-TIAA-SERVICE-AGENT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_TIAA_SERVICE_AGENT");
        aap_View_Ap_Prap_Rsch_Mit_Ind = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Prap_Rsch_Mit_Ind", "AP-PRAP-RSCH-MIT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_PRAP_RSCH_MIT_IND");
        aap_View_Ap_Ppg_Team_Cde = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Ppg_Team_Cde", "AP-PPG-TEAM-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "AP_PPG_TEAM_CDE");
        aap_View_Ap_Tiaa_Cntrct = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TIAA_CNTRCT");
        aap_View_Ap_Cref_Cert = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Cref_Cert", "AP-CREF-CERT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_CREF_CERT");
        aap_View_Ap_Rlc_Cref_Cert = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Rlc_Cref_Cert", "AP-RLC-CREF-CERT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_RLC_CREF_CERT");
        aap_View_Ap_Allocation_Model_Type = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Allocation_Model_Type", "AP-ALLOCATION-MODEL-TYPE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "AP_ALLOCATION_MODEL_TYPE");
        aap_View_Ap_Divorce_Ind = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Divorce_Ind", "AP-DIVORCE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_DIVORCE_IND");
        aap_View_Ap_Email_Address = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Email_Address", "AP-EMAIL-ADDRESS", FieldType.STRING, 50, RepeatingFieldStrategy.None, 
            "AP_EMAIL_ADDRESS");
        aap_View_Ap_E_Signed_Appl_Ind = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_E_Signed_Appl_Ind", "AP-E-SIGNED-APPL-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_E_SIGNED_APPL_IND");
        aap_View_Ap_Eft_Requested_Ind = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Eft_Requested_Ind", "AP-EFT-REQUESTED-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_EFT_REQUESTED_IND");
        aap_View_Ap_Phone_No = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Phone_No", "AP-PHONE-NO", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "AP_PHONE_NO");
        aap_View_Ap_Allocation_Fmt = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Allocation_Fmt", "AP-ALLOCATION-FMT", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_ALLOCATION_FMT");
        aap_View_Ap_Fund_Identifier = vw_aap_View.getRecord().newGroupInGroup("aap_View_Ap_Fund_Identifier", "AP-FUND-IDENTIFIER", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        aap_View_Ap_Fund_Cde = aap_View_Ap_Fund_Identifier.newFieldArrayInGroup("aap_View_Ap_Fund_Cde", "AP-FUND-CDE", FieldType.STRING, 10, new DbsArrayController(1,100) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_FUND_CDE", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        aap_View_Ap_Allocation_Pct = aap_View_Ap_Fund_Identifier.newFieldArrayInGroup("aap_View_Ap_Allocation_Pct", "AP-ALLOCATION-PCT", FieldType.NUMERIC, 
            3, new DbsArrayController(1,100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ALLOCATION_PCT", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        aap_View_Ap_Sgrd_Plan_No = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_SGRD_PLAN_NO");
        aap_View_Ap_Sgrd_Subplan_No = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Sgrd_Subplan_No", "AP-SGRD-SUBPLAN-NO", FieldType.STRING, 6, 
            RepeatingFieldStrategy.None, "AP_SGRD_SUBPLAN_NO");
        aap_View_Ap_Sgrd_Part_Ext = vw_aap_View.getRecord().newFieldInGroup("aap_View_Ap_Sgrd_Part_Ext", "AP-SGRD-PART-EXT", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "AP_SGRD_PART_EXT");
        vw_aap_View.setUniquePeList();

        this.setRecordName("LdaAppl500");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_aap_View.reset();
    }

    // Constructor
    public LdaAppl500() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
