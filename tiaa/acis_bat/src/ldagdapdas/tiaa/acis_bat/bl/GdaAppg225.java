/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:48:19 PM
**        * FROM NATURAL GDA     : APPG225
************************************************************
**        * FILE NAME            : GdaAppg225.java
**        * CLASS NAME           : GdaAppg225
**        * INSTANCE NAME        : GdaAppg225
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import java.util.ArrayList;
import java.util.List;

import tiaa.tiaacommon.bl.*;

public final class GdaAppg225 extends DbsRecord
{
    private static ThreadLocal<List<GdaAppg225>> _instance = new ThreadLocal<List<GdaAppg225>>();

    // Properties
    private DbsGroup pnd_Balancing_Report_Fields;
    private DbsField pnd_Balancing_Report_Fields_Hold_Proc_Stat;
    private DbsGroup pnd_Balancing_Report_Fields_Hold_Proc_StatRedef1;
    private DbsField pnd_Balancing_Report_Fields_Hold_Proc_Stat_Reg;
    private DbsField pnd_Balancing_Report_Fields_Hold_Proc_Stat_Need;
    private DbsField pnd_Balancing_Report_Fields_Hold_Proc_Stat_Num;
    private DbsField pnd_Balancing_Report_Fields_Process_Stat_Reject;
    private DbsField pnd_Balancing_Report_Fields_Total_Assigned_Premiums;
    private DbsField pnd_Balancing_Report_Fields_Total_Issued_Applications;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Prap_Cnt;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Prap_Appl_Cnt;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Prap_New_Appl_Cnt;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Prap_New_Appl_Del_Cnt;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Prap_Old_Appl_Cnt;
    private DbsField pnd_Balancing_Report_Fields_Region_Wrong_Count;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Prap_Prem_Cnt;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Prap_Othr_Cnt;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Prap_New_Prem_Cnt;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Prap_Old_Prem_Cnt;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Prap_Updt_Cnt;
    private DbsField pnd_Balancing_Report_Fields_Test_Contract_Min;
    private DbsField pnd_Balancing_Report_Fields_Test_Contract_Max;
    private DbsField pnd_Balancing_Report_Fields_Comp_Contract_Min;
    private DbsField pnd_Balancing_Report_Fields_Comp_Contract_Max;
    private DbsField pnd_Balancing_Report_Fields_Prev_Contract;
    private DbsGroup pnd_Balancing_Report_Fields_Prev_ContractRedef2;
    private DbsField pnd_Balancing_Report_Fields_Prev_Contract_Pfx;
    private DbsField pnd_Balancing_Report_Fields_Prev_Contract_Num7;
    private DbsGroup pnd_Balancing_Report_Fields_Prev_Contract_Num7Redef3;
    private DbsField pnd_Balancing_Report_Fields_Prev_Contract_Num;
    private DbsField pnd_Balancing_Report_Fields_Prev_Rest;
    private DbsField pnd_Balancing_Report_Fields_Curr_Contract;
    private DbsGroup pnd_Balancing_Report_Fields_Curr_ContractRedef4;
    private DbsField pnd_Balancing_Report_Fields_Curr_Contract_Pfx;
    private DbsField pnd_Balancing_Report_Fields_Curr_Contract_Num7;
    private DbsGroup pnd_Balancing_Report_Fields_Curr_Contract_Num7Redef5;
    private DbsField pnd_Balancing_Report_Fields_Curr_Contract_Num;
    private DbsField pnd_Balancing_Report_Fields_Curr_Rest;
    private DbsField pnd_Balancing_Report_Fields_H_Contract;
    private DbsGroup pnd_Balancing_Report_Fields_H_ContractRedef6;
    private DbsField pnd_Balancing_Report_Fields_H_Contract_Pfx;
    private DbsField pnd_Balancing_Report_Fields_H_Contract_Num7;
    private DbsGroup pnd_Balancing_Report_Fields_H_Contract_Num7Redef7;
    private DbsField pnd_Balancing_Report_Fields_H_Contract_Num;
    private DbsField pnd_Balancing_Report_Fields_H_Rest;
    private DbsField pnd_Balancing_Report_Fields_Contract7;
    private DbsGroup pnd_Balancing_Report_Fields_Contract7Redef8;
    private DbsField pnd_Balancing_Report_Fields_Contract_Pfx;
    private DbsField pnd_Balancing_Report_Fields_Contract_Num;
    private DbsField pnd_Balancing_Report_Fields_Contract_Info;
    private DbsGroup pnd_Balancing_Report_Fields_Contract_InfoRedef9;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Contract_Type;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Contract_Plus;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Contract_Lob_Type;
    private DbsGroup pnd_Balancing_Report_Fields_Pnd_Contract_Lob_TypeRedef10;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Product_Lob;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Product_Lob_Type;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Contract_Prod_Code;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Filler;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Contract_Start;
    private DbsGroup pnd_Balancing_Report_Fields_Pnd_Contract_StartRedef11;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Contract_Start_Pfx;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Contract_Start_Num;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Contract_Start_Pfx_C;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Contract_End;
    private DbsGroup pnd_Balancing_Report_Fields_Pnd_Contract_EndRedef12;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Contract_End_Pfx;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Contract_End_Num;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Contract_End_Pfx_C;
    private DbsField pnd_Balancing_Report_Fields_D_Start_Date;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Prt_Start;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Process_Status_To_Find;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Product_Code_To_Find;
    private DbsField pnd_Balancing_Report_Fields_Tens_Bucket;
    private DbsField pnd_Balancing_Report_Fields_Num_Tens;
    private DbsField pnd_Balancing_Report_Fields_Ten_Sub;
    private DbsField pnd_Balancing_Report_Fields_One_Sub;
    private DbsField pnd_Balancing_Report_Fields_Where_Sub;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Multi_Prod_Sub;
    private DbsField pnd_Balancing_Report_Fields_Display_Sub;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Prod_Sub;
    private DbsField pnd_Balancing_Report_Fields_Total_D_Sub;
    private DbsField pnd_Balancing_Report_Fields_Total_A_Sub;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Group_Val;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Prod_Val;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Proc_Val;
    private DbsField pnd_Balancing_Report_Fields_Pnd_That_Tally;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Acctall_Val;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Prap_Soc_Sec;
    private DbsField pnd_Balancing_Report_Fields_Pnd_Ap_Prap_Key;
    private DataAccessProgramView vw_app_Prap_Rule_View;
    private DbsField app_Prap_Rule_View_Ap_Coll_Code;
    private DbsGroup app_Prap_Rule_View_Ap_Coll_CodeRedef13;
    private DbsField app_Prap_Rule_View_Ap_Coll_Cd;
    private DbsField app_Prap_Rule_View_Ap_Coll_Fill;
    private DbsField app_Prap_Rule_View_Ap_G_Key;
    private DbsField app_Prap_Rule_View_Ap_G_Ind;
    private DbsField app_Prap_Rule_View_Ap_Soc_Sec;
    private DbsField app_Prap_Rule_View_Ap_Dob;
    private DbsField app_Prap_Rule_View_Ap_Lob;
    private DbsField app_Prap_Rule_View_Ap_Bill_Code;
    private DbsField app_Prap_Rule_View_Ap_T_Doi;
    private DbsField app_Prap_Rule_View_Ap_C_Doi;
    private DbsField app_Prap_Rule_View_Ap_Curr;
    private DbsField app_Prap_Rule_View_Ap_T_Age_1st;
    private DbsField app_Prap_Rule_View_Ap_C_Age_1st;
    private DbsField app_Prap_Rule_View_Ap_Ownership;
    private DbsField app_Prap_Rule_View_Ap_Sex;
    private DbsField app_Prap_Rule_View_Ap_Mail_Zip;
    private DbsField app_Prap_Rule_View_Ap_Name_Addr_Cd;
    private DbsField app_Prap_Rule_View_Ap_Dt_Ent_Sys;
    private DbsField app_Prap_Rule_View_Ap_Dt_Released;
    private DbsField app_Prap_Rule_View_Ap_Dt_Matched;
    private DbsField app_Prap_Rule_View_Ap_Dt_Deleted;
    private DbsField app_Prap_Rule_View_Ap_Dt_Withdrawn;
    private DbsField app_Prap_Rule_View_Ap_Alloc_Discr;
    private DbsField app_Prap_Rule_View_Ap_Release_Ind;
    private DbsField app_Prap_Rule_View_Ap_Type;
    private DbsField app_Prap_Rule_View_Ap_Other_Pols;
    private DbsField app_Prap_Rule_View_Ap_Record_Type;
    private DbsField app_Prap_Rule_View_Ap_Status;
    private DbsField app_Prap_Rule_View_Ap_Numb_Dailys;
    private DbsField app_Prap_Rule_View_Ap_Dt_App_Recvd;
    private DbsGroup app_Prap_Rule_View_Ap_Allocation_Info;
    private DbsField app_Prap_Rule_View_Ap_Allocation;
    private DbsField app_Prap_Rule_View_Ap_App_Source;
    private DbsField app_Prap_Rule_View_Ap_Region_Code;
    private DbsField app_Prap_Rule_View_Ap_Orig_Issue_State;
    private DbsField app_Prap_Rule_View_Ap_Ownership_Type;
    private DbsField app_Prap_Rule_View_Ap_Lob_Type;
    private DbsField app_Prap_Rule_View_Ap_Split_Code;
    private DbsField app_Prap_Rule_View_Count_Castap_Address_Info;
    private DbsGroup app_Prap_Rule_View_Ap_Address_Info;
    private DbsField app_Prap_Rule_View_Ap_Address_Line;
    private DbsField app_Prap_Rule_View_Ap_City;
    private DbsField app_Prap_Rule_View_Ap_Current_State_Code;
    private DbsField app_Prap_Rule_View_Ap_Dana_Transaction_Cd;
    private DbsField app_Prap_Rule_View_Ap_Dana_Stndrd_Rtn_Cd;
    private DbsField app_Prap_Rule_View_Ap_Dana_Finalist_Reason_Cd;
    private DbsField app_Prap_Rule_View_Ap_Dana_Addr_Stndrd_Code;
    private DbsField app_Prap_Rule_View_Ap_Dana_Stndrd_Overide;
    private DbsField app_Prap_Rule_View_Ap_Dana_Postal_Data_Fields;
    private DbsField app_Prap_Rule_View_Ap_Dana_Addr_Geographic_Code;
    private DbsField app_Prap_Rule_View_Ap_Annuity_Start_Date;
    private DbsGroup app_Prap_Rule_View_Ap_Maximum_Alloc_Pct;
    private DbsField app_Prap_Rule_View_Ap_Max_Alloc_Pct;
    private DbsField app_Prap_Rule_View_Ap_Max_Alloc_Pct_Sign;
    private DbsField app_Prap_Rule_View_Ap_Bene_Info_Type;
    private DbsField app_Prap_Rule_View_Ap_Primary_Std_Ent;
    private DbsGroup app_Prap_Rule_View_Ap_Bene_Primary_Info;
    private DbsField app_Prap_Rule_View_Ap_Primary_Bene_Info;
    private DbsField app_Prap_Rule_View_Ap_Contingent_Std_Ent;
    private DbsGroup app_Prap_Rule_View_Ap_Bene_Contingent_Info;
    private DbsField app_Prap_Rule_View_Ap_Contingent_Bene_Info;
    private DbsField app_Prap_Rule_View_Ap_Bene_Estate;
    private DbsField app_Prap_Rule_View_Ap_Bene_Trust;
    private DbsField app_Prap_Rule_View_Ap_Bene_Category;
    private DbsField app_Prap_Rule_View_Ap_Mail_Instructions;
    private DbsField app_Prap_Rule_View_Ap_Eop_Addl_Cref_Request;
    private DbsField app_Prap_Rule_View_Ap_Process_Status;
    private DbsGroup app_Prap_Rule_View_Ap_Process_StatusRedef14;
    private DbsField app_Prap_Rule_View_Pnd_Ap_Proc_Stat_Reg;
    private DbsField app_Prap_Rule_View_Pnd_Ap_Proc_Stat_Need;
    private DbsField app_Prap_Rule_View_Pnd_Ap_Proc_Stat_Num;
    private DbsField app_Prap_Rule_View_Ap_Coll_St_Cd;
    private DbsField app_Prap_Rule_View_Ap_Csm_Sec_Seg;
    private DbsField app_Prap_Rule_View_Ap_Rlc_College;
    private DbsField app_Prap_Rule_View_Ap_Cor_Prfx_Nme;
    private DbsField app_Prap_Rule_View_Ap_Cor_Last_Nme;
    private DbsField app_Prap_Rule_View_Ap_Cor_First_Nme;
    private DbsField app_Prap_Rule_View_Ap_Cor_Mddle_Nme;
    private DbsField app_Prap_Rule_View_Ap_Cor_Sffx_Nme;
    private DbsField app_Prap_Rule_View_Ap_Ph_Hist_Ind;
    private DbsField app_Prap_Rule_View_Ap_Rcrd_Updt_Tm_Stamp;
    private DbsField app_Prap_Rule_View_Ap_Contact_Mode;
    private DbsField app_Prap_Rule_View_Ap_Racf_Id;
    private DbsField app_Prap_Rule_View_Ap_Pin_Nbr;
    private DbsGroup app_Prap_Rule_View_Ap_Rqst_Log_Dte_Tm;
    private DbsField app_Prap_Rule_View_Ap_Rqst_Log_Dte_Time;
    private DbsField app_Prap_Rule_View_Ap_Sync_Ind;
    private DbsField app_Prap_Rule_View_Ap_Cor_Ind;
    private DbsField app_Prap_Rule_View_Ap_Tiaa_Doi;
    private DbsField app_Prap_Rule_View_Ap_Cref_Doi;
    private DbsField app_Prap_Rule_View_Ap_Alloc_Ind;
    private DbsField app_Prap_Rule_View_Ap_Irc_Sectn_Grp_Cde;
    private DbsField app_Prap_Rule_View_Ap_Irc_Sectn_Cde;
    private DbsField app_Prap_Rule_View_Ap_Inst_Link_Cde;
    private DbsField app_Prap_Rule_View_Ap_Applcnt_Req_Type;
    private DbsField app_Prap_Rule_View_Ap_Addr_Sync_Ind;
    private DbsField app_Prap_Rule_View_Ap_Tiaa_Service_Agent;
    private DbsField app_Prap_Rule_View_Ap_Prap_Rsch_Mit_Ind;
    private DbsField app_Prap_Rule_View_Ap_Ppg_Team_Cde;
    private DbsField app_Prap_Rule_View_Ap_Tiaa_Cntrct;
    private DbsGroup app_Prap_Rule_View_Ap_Tiaa_CntrctRedef15;
    private DbsField app_Prap_Rule_View_Ap_Pref_New;
    private DbsField app_Prap_Rule_View_Ap_Cont_New;
    private DbsField app_Prap_Rule_View_Ap_Cref_Cert;
    private DbsGroup app_Prap_Rule_View_Ap_Cref_CertRedef16;
    private DbsField app_Prap_Rule_View_Ap_C_Pref_New;
    private DbsField app_Prap_Rule_View_Ap_C_Cont_New;
    private DbsField app_Prap_Rule_View_Ap_Rlc_Cref_Cert;
    private DbsGroup app_Prap_Rule_View_Ap_Rlc_Cref_CertRedef17;
    private DbsField app_Prap_Rule_View_Ap_Rlc_Pref_New;
    private DbsField app_Prap_Rule_View_Ap_Rlc_Cont_New;
    private DbsField app_Prap_Rule_View_Ap_Allocation_Model_Type;
    private DbsField app_Prap_Rule_View_Ap_Divorce_Ind;
    private DbsField app_Prap_Rule_View_Ap_Email_Address;
    private DbsField app_Prap_Rule_View_Ap_E_Signed_Appl_Ind;
    private DbsField app_Prap_Rule_View_Ap_Eft_Requested_Ind;
    private DbsField app_Prap_Rule_View_Ap_Allocation_Fmt;
    private DbsGroup app_Prap_Rule_View_Ap_Fund_Identifier;
    private DbsField app_Prap_Rule_View_Ap_Fund_Cde;
    private DbsField app_Prap_Rule_View_Ap_Allocation_Pct;
    private DbsField app_Prap_Rule_View_Ap_Sgrd_Plan_No;
    private DbsField app_Prap_Rule_View_Ap_Sgrd_Subplan_No;
    private DbsField app_Prap_Rule_View_Ap_Sgrd_Part_Ext;
    private DbsGroup roman_Global;
    private DbsField roman_Global_Display_Region;
    private DbsField roman_Global_Display_Need;
    private DbsField roman_Global_Display_Contracts_Tot;
    private DbsField roman_Global_Display_Out_Balance;
    private DbsGroup roman_Global_Display_Table;
    private DbsField roman_Global_Display_Product;
    private DbsField roman_Global_Display_Count;
    private DbsField roman_Global_Display_Contra_Count;
    private DbsField roman_Global_Display_Error_Flag;
    private DbsGroup roman_Global_Pnd_Cnt_Data;
    private DbsField roman_Global_Pnd_Cnt_Product_Code;
    private DbsField roman_Global_Pnd_Cnt_Product_Group;
    private DbsField roman_Global_Display_Prev_Contract;
    private DbsField roman_Global_Display_Curr_Contract;
    private DbsField roman_Global_Display_Nums_Contract;
    private DbsField roman_Global_Pnd_Cnt_Accum_Tally;
    private DbsField roman_Global_Pnd_Cnt_Compare_Tally;

    public DbsGroup getPnd_Balancing_Report_Fields() { return pnd_Balancing_Report_Fields; }

    public DbsField getPnd_Balancing_Report_Fields_Hold_Proc_Stat() { return pnd_Balancing_Report_Fields_Hold_Proc_Stat; }

    public DbsGroup getPnd_Balancing_Report_Fields_Hold_Proc_StatRedef1() { return pnd_Balancing_Report_Fields_Hold_Proc_StatRedef1; }

    public DbsField getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Reg() { return pnd_Balancing_Report_Fields_Hold_Proc_Stat_Reg; }

    public DbsField getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Need() { return pnd_Balancing_Report_Fields_Hold_Proc_Stat_Need; }

    public DbsField getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Num() { return pnd_Balancing_Report_Fields_Hold_Proc_Stat_Num; }

    public DbsField getPnd_Balancing_Report_Fields_Process_Stat_Reject() { return pnd_Balancing_Report_Fields_Process_Stat_Reject; }

    public DbsField getPnd_Balancing_Report_Fields_Total_Assigned_Premiums() { return pnd_Balancing_Report_Fields_Total_Assigned_Premiums; }

    public DbsField getPnd_Balancing_Report_Fields_Total_Issued_Applications() { return pnd_Balancing_Report_Fields_Total_Issued_Applications; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Prap_Cnt() { return pnd_Balancing_Report_Fields_Pnd_Prap_Cnt; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Prap_Appl_Cnt() { return pnd_Balancing_Report_Fields_Pnd_Prap_Appl_Cnt; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Prap_New_Appl_Cnt() { return pnd_Balancing_Report_Fields_Pnd_Prap_New_Appl_Cnt; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Prap_New_Appl_Del_Cnt() { return pnd_Balancing_Report_Fields_Pnd_Prap_New_Appl_Del_Cnt; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Prap_Old_Appl_Cnt() { return pnd_Balancing_Report_Fields_Pnd_Prap_Old_Appl_Cnt; }

    public DbsField getPnd_Balancing_Report_Fields_Region_Wrong_Count() { return pnd_Balancing_Report_Fields_Region_Wrong_Count; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Prap_Prem_Cnt() { return pnd_Balancing_Report_Fields_Pnd_Prap_Prem_Cnt; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Prap_Othr_Cnt() { return pnd_Balancing_Report_Fields_Pnd_Prap_Othr_Cnt; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Prap_New_Prem_Cnt() { return pnd_Balancing_Report_Fields_Pnd_Prap_New_Prem_Cnt; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Prap_Old_Prem_Cnt() { return pnd_Balancing_Report_Fields_Pnd_Prap_Old_Prem_Cnt; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Prap_Updt_Cnt() { return pnd_Balancing_Report_Fields_Pnd_Prap_Updt_Cnt; }

    public DbsField getPnd_Balancing_Report_Fields_Test_Contract_Min() { return pnd_Balancing_Report_Fields_Test_Contract_Min; }

    public DbsField getPnd_Balancing_Report_Fields_Test_Contract_Max() { return pnd_Balancing_Report_Fields_Test_Contract_Max; }

    public DbsField getPnd_Balancing_Report_Fields_Comp_Contract_Min() { return pnd_Balancing_Report_Fields_Comp_Contract_Min; }

    public DbsField getPnd_Balancing_Report_Fields_Comp_Contract_Max() { return pnd_Balancing_Report_Fields_Comp_Contract_Max; }

    public DbsField getPnd_Balancing_Report_Fields_Prev_Contract() { return pnd_Balancing_Report_Fields_Prev_Contract; }

    public DbsGroup getPnd_Balancing_Report_Fields_Prev_ContractRedef2() { return pnd_Balancing_Report_Fields_Prev_ContractRedef2; }

    public DbsField getPnd_Balancing_Report_Fields_Prev_Contract_Pfx() { return pnd_Balancing_Report_Fields_Prev_Contract_Pfx; }

    public DbsField getPnd_Balancing_Report_Fields_Prev_Contract_Num7() { return pnd_Balancing_Report_Fields_Prev_Contract_Num7; }

    public DbsGroup getPnd_Balancing_Report_Fields_Prev_Contract_Num7Redef3() { return pnd_Balancing_Report_Fields_Prev_Contract_Num7Redef3; }

    public DbsField getPnd_Balancing_Report_Fields_Prev_Contract_Num() { return pnd_Balancing_Report_Fields_Prev_Contract_Num; }

    public DbsField getPnd_Balancing_Report_Fields_Prev_Rest() { return pnd_Balancing_Report_Fields_Prev_Rest; }

    public DbsField getPnd_Balancing_Report_Fields_Curr_Contract() { return pnd_Balancing_Report_Fields_Curr_Contract; }

    public DbsGroup getPnd_Balancing_Report_Fields_Curr_ContractRedef4() { return pnd_Balancing_Report_Fields_Curr_ContractRedef4; }

    public DbsField getPnd_Balancing_Report_Fields_Curr_Contract_Pfx() { return pnd_Balancing_Report_Fields_Curr_Contract_Pfx; }

    public DbsField getPnd_Balancing_Report_Fields_Curr_Contract_Num7() { return pnd_Balancing_Report_Fields_Curr_Contract_Num7; }

    public DbsGroup getPnd_Balancing_Report_Fields_Curr_Contract_Num7Redef5() { return pnd_Balancing_Report_Fields_Curr_Contract_Num7Redef5; }

    public DbsField getPnd_Balancing_Report_Fields_Curr_Contract_Num() { return pnd_Balancing_Report_Fields_Curr_Contract_Num; }

    public DbsField getPnd_Balancing_Report_Fields_Curr_Rest() { return pnd_Balancing_Report_Fields_Curr_Rest; }

    public DbsField getPnd_Balancing_Report_Fields_H_Contract() { return pnd_Balancing_Report_Fields_H_Contract; }

    public DbsGroup getPnd_Balancing_Report_Fields_H_ContractRedef6() { return pnd_Balancing_Report_Fields_H_ContractRedef6; }

    public DbsField getPnd_Balancing_Report_Fields_H_Contract_Pfx() { return pnd_Balancing_Report_Fields_H_Contract_Pfx; }

    public DbsField getPnd_Balancing_Report_Fields_H_Contract_Num7() { return pnd_Balancing_Report_Fields_H_Contract_Num7; }

    public DbsGroup getPnd_Balancing_Report_Fields_H_Contract_Num7Redef7() { return pnd_Balancing_Report_Fields_H_Contract_Num7Redef7; }

    public DbsField getPnd_Balancing_Report_Fields_H_Contract_Num() { return pnd_Balancing_Report_Fields_H_Contract_Num; }

    public DbsField getPnd_Balancing_Report_Fields_H_Rest() { return pnd_Balancing_Report_Fields_H_Rest; }

    public DbsField getPnd_Balancing_Report_Fields_Contract7() { return pnd_Balancing_Report_Fields_Contract7; }

    public DbsGroup getPnd_Balancing_Report_Fields_Contract7Redef8() { return pnd_Balancing_Report_Fields_Contract7Redef8; }

    public DbsField getPnd_Balancing_Report_Fields_Contract_Pfx() { return pnd_Balancing_Report_Fields_Contract_Pfx; }

    public DbsField getPnd_Balancing_Report_Fields_Contract_Num() { return pnd_Balancing_Report_Fields_Contract_Num; }

    public DbsField getPnd_Balancing_Report_Fields_Contract_Info() { return pnd_Balancing_Report_Fields_Contract_Info; }

    public DbsGroup getPnd_Balancing_Report_Fields_Contract_InfoRedef9() { return pnd_Balancing_Report_Fields_Contract_InfoRedef9; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Contract_Type() { return pnd_Balancing_Report_Fields_Pnd_Contract_Type; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Contract_Plus() { return pnd_Balancing_Report_Fields_Pnd_Contract_Plus; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Contract_Lob_Type() { return pnd_Balancing_Report_Fields_Pnd_Contract_Lob_Type; }

    public DbsGroup getPnd_Balancing_Report_Fields_Pnd_Contract_Lob_TypeRedef10() { return pnd_Balancing_Report_Fields_Pnd_Contract_Lob_TypeRedef10; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Product_Lob() { return pnd_Balancing_Report_Fields_Pnd_Product_Lob; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Product_Lob_Type() { return pnd_Balancing_Report_Fields_Pnd_Product_Lob_Type; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Contract_Prod_Code() { return pnd_Balancing_Report_Fields_Pnd_Contract_Prod_Code; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Filler() { return pnd_Balancing_Report_Fields_Pnd_Filler; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Contract_Start() { return pnd_Balancing_Report_Fields_Pnd_Contract_Start; }

    public DbsGroup getPnd_Balancing_Report_Fields_Pnd_Contract_StartRedef11() { return pnd_Balancing_Report_Fields_Pnd_Contract_StartRedef11; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Contract_Start_Pfx() { return pnd_Balancing_Report_Fields_Pnd_Contract_Start_Pfx; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Contract_Start_Num() { return pnd_Balancing_Report_Fields_Pnd_Contract_Start_Num; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Contract_Start_Pfx_C() { return pnd_Balancing_Report_Fields_Pnd_Contract_Start_Pfx_C; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Contract_End() { return pnd_Balancing_Report_Fields_Pnd_Contract_End; }

    public DbsGroup getPnd_Balancing_Report_Fields_Pnd_Contract_EndRedef12() { return pnd_Balancing_Report_Fields_Pnd_Contract_EndRedef12; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Contract_End_Pfx() { return pnd_Balancing_Report_Fields_Pnd_Contract_End_Pfx; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Contract_End_Num() { return pnd_Balancing_Report_Fields_Pnd_Contract_End_Num; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Contract_End_Pfx_C() { return pnd_Balancing_Report_Fields_Pnd_Contract_End_Pfx_C; }

    public DbsField getPnd_Balancing_Report_Fields_D_Start_Date() { return pnd_Balancing_Report_Fields_D_Start_Date; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Prt_Start() { return pnd_Balancing_Report_Fields_Pnd_Prt_Start; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Process_Status_To_Find() { return pnd_Balancing_Report_Fields_Pnd_Process_Status_To_Find; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Product_Code_To_Find() { return pnd_Balancing_Report_Fields_Pnd_Product_Code_To_Find; }

    public DbsField getPnd_Balancing_Report_Fields_Tens_Bucket() { return pnd_Balancing_Report_Fields_Tens_Bucket; }

    public DbsField getPnd_Balancing_Report_Fields_Num_Tens() { return pnd_Balancing_Report_Fields_Num_Tens; }

    public DbsField getPnd_Balancing_Report_Fields_Ten_Sub() { return pnd_Balancing_Report_Fields_Ten_Sub; }

    public DbsField getPnd_Balancing_Report_Fields_One_Sub() { return pnd_Balancing_Report_Fields_One_Sub; }

    public DbsField getPnd_Balancing_Report_Fields_Where_Sub() { return pnd_Balancing_Report_Fields_Where_Sub; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Multi_Prod_Sub() { return pnd_Balancing_Report_Fields_Pnd_Multi_Prod_Sub; }

    public DbsField getPnd_Balancing_Report_Fields_Display_Sub() { return pnd_Balancing_Report_Fields_Display_Sub; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Prod_Sub() { return pnd_Balancing_Report_Fields_Pnd_Prod_Sub; }

    public DbsField getPnd_Balancing_Report_Fields_Total_D_Sub() { return pnd_Balancing_Report_Fields_Total_D_Sub; }

    public DbsField getPnd_Balancing_Report_Fields_Total_A_Sub() { return pnd_Balancing_Report_Fields_Total_A_Sub; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Group_Val() { return pnd_Balancing_Report_Fields_Pnd_Group_Val; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Prod_Val() { return pnd_Balancing_Report_Fields_Pnd_Prod_Val; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Proc_Val() { return pnd_Balancing_Report_Fields_Pnd_Proc_Val; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_That_Tally() { return pnd_Balancing_Report_Fields_Pnd_That_Tally; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Acctall_Val() { return pnd_Balancing_Report_Fields_Pnd_Acctall_Val; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Prap_Soc_Sec() { return pnd_Balancing_Report_Fields_Pnd_Prap_Soc_Sec; }

    public DbsField getPnd_Balancing_Report_Fields_Pnd_Ap_Prap_Key() { return pnd_Balancing_Report_Fields_Pnd_Ap_Prap_Key; }

    public DataAccessProgramView getVw_app_Prap_Rule_View() { return vw_app_Prap_Rule_View; }

    public DbsField getApp_Prap_Rule_View_Ap_Coll_Code() { return app_Prap_Rule_View_Ap_Coll_Code; }

    public DbsGroup getApp_Prap_Rule_View_Ap_Coll_CodeRedef13() { return app_Prap_Rule_View_Ap_Coll_CodeRedef13; }

    public DbsField getApp_Prap_Rule_View_Ap_Coll_Cd() { return app_Prap_Rule_View_Ap_Coll_Cd; }

    public DbsField getApp_Prap_Rule_View_Ap_Coll_Fill() { return app_Prap_Rule_View_Ap_Coll_Fill; }

    public DbsField getApp_Prap_Rule_View_Ap_G_Key() { return app_Prap_Rule_View_Ap_G_Key; }

    public DbsField getApp_Prap_Rule_View_Ap_G_Ind() { return app_Prap_Rule_View_Ap_G_Ind; }

    public DbsField getApp_Prap_Rule_View_Ap_Soc_Sec() { return app_Prap_Rule_View_Ap_Soc_Sec; }

    public DbsField getApp_Prap_Rule_View_Ap_Dob() { return app_Prap_Rule_View_Ap_Dob; }

    public DbsField getApp_Prap_Rule_View_Ap_Lob() { return app_Prap_Rule_View_Ap_Lob; }

    public DbsField getApp_Prap_Rule_View_Ap_Bill_Code() { return app_Prap_Rule_View_Ap_Bill_Code; }

    public DbsField getApp_Prap_Rule_View_Ap_T_Doi() { return app_Prap_Rule_View_Ap_T_Doi; }

    public DbsField getApp_Prap_Rule_View_Ap_C_Doi() { return app_Prap_Rule_View_Ap_C_Doi; }

    public DbsField getApp_Prap_Rule_View_Ap_Curr() { return app_Prap_Rule_View_Ap_Curr; }

    public DbsField getApp_Prap_Rule_View_Ap_T_Age_1st() { return app_Prap_Rule_View_Ap_T_Age_1st; }

    public DbsField getApp_Prap_Rule_View_Ap_C_Age_1st() { return app_Prap_Rule_View_Ap_C_Age_1st; }

    public DbsField getApp_Prap_Rule_View_Ap_Ownership() { return app_Prap_Rule_View_Ap_Ownership; }

    public DbsField getApp_Prap_Rule_View_Ap_Sex() { return app_Prap_Rule_View_Ap_Sex; }

    public DbsField getApp_Prap_Rule_View_Ap_Mail_Zip() { return app_Prap_Rule_View_Ap_Mail_Zip; }

    public DbsField getApp_Prap_Rule_View_Ap_Name_Addr_Cd() { return app_Prap_Rule_View_Ap_Name_Addr_Cd; }

    public DbsField getApp_Prap_Rule_View_Ap_Dt_Ent_Sys() { return app_Prap_Rule_View_Ap_Dt_Ent_Sys; }

    public DbsField getApp_Prap_Rule_View_Ap_Dt_Released() { return app_Prap_Rule_View_Ap_Dt_Released; }

    public DbsField getApp_Prap_Rule_View_Ap_Dt_Matched() { return app_Prap_Rule_View_Ap_Dt_Matched; }

    public DbsField getApp_Prap_Rule_View_Ap_Dt_Deleted() { return app_Prap_Rule_View_Ap_Dt_Deleted; }

    public DbsField getApp_Prap_Rule_View_Ap_Dt_Withdrawn() { return app_Prap_Rule_View_Ap_Dt_Withdrawn; }

    public DbsField getApp_Prap_Rule_View_Ap_Alloc_Discr() { return app_Prap_Rule_View_Ap_Alloc_Discr; }

    public DbsField getApp_Prap_Rule_View_Ap_Release_Ind() { return app_Prap_Rule_View_Ap_Release_Ind; }

    public DbsField getApp_Prap_Rule_View_Ap_Type() { return app_Prap_Rule_View_Ap_Type; }

    public DbsField getApp_Prap_Rule_View_Ap_Other_Pols() { return app_Prap_Rule_View_Ap_Other_Pols; }

    public DbsField getApp_Prap_Rule_View_Ap_Record_Type() { return app_Prap_Rule_View_Ap_Record_Type; }

    public DbsField getApp_Prap_Rule_View_Ap_Status() { return app_Prap_Rule_View_Ap_Status; }

    public DbsField getApp_Prap_Rule_View_Ap_Numb_Dailys() { return app_Prap_Rule_View_Ap_Numb_Dailys; }

    public DbsField getApp_Prap_Rule_View_Ap_Dt_App_Recvd() { return app_Prap_Rule_View_Ap_Dt_App_Recvd; }

    public DbsGroup getApp_Prap_Rule_View_Ap_Allocation_Info() { return app_Prap_Rule_View_Ap_Allocation_Info; }

    public DbsField getApp_Prap_Rule_View_Ap_Allocation() { return app_Prap_Rule_View_Ap_Allocation; }

    public DbsField getApp_Prap_Rule_View_Ap_App_Source() { return app_Prap_Rule_View_Ap_App_Source; }

    public DbsField getApp_Prap_Rule_View_Ap_Region_Code() { return app_Prap_Rule_View_Ap_Region_Code; }

    public DbsField getApp_Prap_Rule_View_Ap_Orig_Issue_State() { return app_Prap_Rule_View_Ap_Orig_Issue_State; }

    public DbsField getApp_Prap_Rule_View_Ap_Ownership_Type() { return app_Prap_Rule_View_Ap_Ownership_Type; }

    public DbsField getApp_Prap_Rule_View_Ap_Lob_Type() { return app_Prap_Rule_View_Ap_Lob_Type; }

    public DbsField getApp_Prap_Rule_View_Ap_Split_Code() { return app_Prap_Rule_View_Ap_Split_Code; }

    public DbsField getApp_Prap_Rule_View_Count_Castap_Address_Info() { return app_Prap_Rule_View_Count_Castap_Address_Info; }

    public DbsGroup getApp_Prap_Rule_View_Ap_Address_Info() { return app_Prap_Rule_View_Ap_Address_Info; }

    public DbsField getApp_Prap_Rule_View_Ap_Address_Line() { return app_Prap_Rule_View_Ap_Address_Line; }

    public DbsField getApp_Prap_Rule_View_Ap_City() { return app_Prap_Rule_View_Ap_City; }

    public DbsField getApp_Prap_Rule_View_Ap_Current_State_Code() { return app_Prap_Rule_View_Ap_Current_State_Code; }

    public DbsField getApp_Prap_Rule_View_Ap_Dana_Transaction_Cd() { return app_Prap_Rule_View_Ap_Dana_Transaction_Cd; }

    public DbsField getApp_Prap_Rule_View_Ap_Dana_Stndrd_Rtn_Cd() { return app_Prap_Rule_View_Ap_Dana_Stndrd_Rtn_Cd; }

    public DbsField getApp_Prap_Rule_View_Ap_Dana_Finalist_Reason_Cd() { return app_Prap_Rule_View_Ap_Dana_Finalist_Reason_Cd; }

    public DbsField getApp_Prap_Rule_View_Ap_Dana_Addr_Stndrd_Code() { return app_Prap_Rule_View_Ap_Dana_Addr_Stndrd_Code; }

    public DbsField getApp_Prap_Rule_View_Ap_Dana_Stndrd_Overide() { return app_Prap_Rule_View_Ap_Dana_Stndrd_Overide; }

    public DbsField getApp_Prap_Rule_View_Ap_Dana_Postal_Data_Fields() { return app_Prap_Rule_View_Ap_Dana_Postal_Data_Fields; }

    public DbsField getApp_Prap_Rule_View_Ap_Dana_Addr_Geographic_Code() { return app_Prap_Rule_View_Ap_Dana_Addr_Geographic_Code; }

    public DbsField getApp_Prap_Rule_View_Ap_Annuity_Start_Date() { return app_Prap_Rule_View_Ap_Annuity_Start_Date; }

    public DbsGroup getApp_Prap_Rule_View_Ap_Maximum_Alloc_Pct() { return app_Prap_Rule_View_Ap_Maximum_Alloc_Pct; }

    public DbsField getApp_Prap_Rule_View_Ap_Max_Alloc_Pct() { return app_Prap_Rule_View_Ap_Max_Alloc_Pct; }

    public DbsField getApp_Prap_Rule_View_Ap_Max_Alloc_Pct_Sign() { return app_Prap_Rule_View_Ap_Max_Alloc_Pct_Sign; }

    public DbsField getApp_Prap_Rule_View_Ap_Bene_Info_Type() { return app_Prap_Rule_View_Ap_Bene_Info_Type; }

    public DbsField getApp_Prap_Rule_View_Ap_Primary_Std_Ent() { return app_Prap_Rule_View_Ap_Primary_Std_Ent; }

    public DbsGroup getApp_Prap_Rule_View_Ap_Bene_Primary_Info() { return app_Prap_Rule_View_Ap_Bene_Primary_Info; }

    public DbsField getApp_Prap_Rule_View_Ap_Primary_Bene_Info() { return app_Prap_Rule_View_Ap_Primary_Bene_Info; }

    public DbsField getApp_Prap_Rule_View_Ap_Contingent_Std_Ent() { return app_Prap_Rule_View_Ap_Contingent_Std_Ent; }

    public DbsGroup getApp_Prap_Rule_View_Ap_Bene_Contingent_Info() { return app_Prap_Rule_View_Ap_Bene_Contingent_Info; }

    public DbsField getApp_Prap_Rule_View_Ap_Contingent_Bene_Info() { return app_Prap_Rule_View_Ap_Contingent_Bene_Info; }

    public DbsField getApp_Prap_Rule_View_Ap_Bene_Estate() { return app_Prap_Rule_View_Ap_Bene_Estate; }

    public DbsField getApp_Prap_Rule_View_Ap_Bene_Trust() { return app_Prap_Rule_View_Ap_Bene_Trust; }

    public DbsField getApp_Prap_Rule_View_Ap_Bene_Category() { return app_Prap_Rule_View_Ap_Bene_Category; }

    public DbsField getApp_Prap_Rule_View_Ap_Mail_Instructions() { return app_Prap_Rule_View_Ap_Mail_Instructions; }

    public DbsField getApp_Prap_Rule_View_Ap_Eop_Addl_Cref_Request() { return app_Prap_Rule_View_Ap_Eop_Addl_Cref_Request; }

    public DbsField getApp_Prap_Rule_View_Ap_Process_Status() { return app_Prap_Rule_View_Ap_Process_Status; }

    public DbsGroup getApp_Prap_Rule_View_Ap_Process_StatusRedef14() { return app_Prap_Rule_View_Ap_Process_StatusRedef14; }

    public DbsField getApp_Prap_Rule_View_Pnd_Ap_Proc_Stat_Reg() { return app_Prap_Rule_View_Pnd_Ap_Proc_Stat_Reg; }

    public DbsField getApp_Prap_Rule_View_Pnd_Ap_Proc_Stat_Need() { return app_Prap_Rule_View_Pnd_Ap_Proc_Stat_Need; }

    public DbsField getApp_Prap_Rule_View_Pnd_Ap_Proc_Stat_Num() { return app_Prap_Rule_View_Pnd_Ap_Proc_Stat_Num; }

    public DbsField getApp_Prap_Rule_View_Ap_Coll_St_Cd() { return app_Prap_Rule_View_Ap_Coll_St_Cd; }

    public DbsField getApp_Prap_Rule_View_Ap_Csm_Sec_Seg() { return app_Prap_Rule_View_Ap_Csm_Sec_Seg; }

    public DbsField getApp_Prap_Rule_View_Ap_Rlc_College() { return app_Prap_Rule_View_Ap_Rlc_College; }

    public DbsField getApp_Prap_Rule_View_Ap_Cor_Prfx_Nme() { return app_Prap_Rule_View_Ap_Cor_Prfx_Nme; }

    public DbsField getApp_Prap_Rule_View_Ap_Cor_Last_Nme() { return app_Prap_Rule_View_Ap_Cor_Last_Nme; }

    public DbsField getApp_Prap_Rule_View_Ap_Cor_First_Nme() { return app_Prap_Rule_View_Ap_Cor_First_Nme; }

    public DbsField getApp_Prap_Rule_View_Ap_Cor_Mddle_Nme() { return app_Prap_Rule_View_Ap_Cor_Mddle_Nme; }

    public DbsField getApp_Prap_Rule_View_Ap_Cor_Sffx_Nme() { return app_Prap_Rule_View_Ap_Cor_Sffx_Nme; }

    public DbsField getApp_Prap_Rule_View_Ap_Ph_Hist_Ind() { return app_Prap_Rule_View_Ap_Ph_Hist_Ind; }

    public DbsField getApp_Prap_Rule_View_Ap_Rcrd_Updt_Tm_Stamp() { return app_Prap_Rule_View_Ap_Rcrd_Updt_Tm_Stamp; }

    public DbsField getApp_Prap_Rule_View_Ap_Contact_Mode() { return app_Prap_Rule_View_Ap_Contact_Mode; }

    public DbsField getApp_Prap_Rule_View_Ap_Racf_Id() { return app_Prap_Rule_View_Ap_Racf_Id; }

    public DbsField getApp_Prap_Rule_View_Ap_Pin_Nbr() { return app_Prap_Rule_View_Ap_Pin_Nbr; }

    public DbsGroup getApp_Prap_Rule_View_Ap_Rqst_Log_Dte_Tm() { return app_Prap_Rule_View_Ap_Rqst_Log_Dte_Tm; }

    public DbsField getApp_Prap_Rule_View_Ap_Rqst_Log_Dte_Time() { return app_Prap_Rule_View_Ap_Rqst_Log_Dte_Time; }

    public DbsField getApp_Prap_Rule_View_Ap_Sync_Ind() { return app_Prap_Rule_View_Ap_Sync_Ind; }

    public DbsField getApp_Prap_Rule_View_Ap_Cor_Ind() { return app_Prap_Rule_View_Ap_Cor_Ind; }

    public DbsField getApp_Prap_Rule_View_Ap_Tiaa_Doi() { return app_Prap_Rule_View_Ap_Tiaa_Doi; }

    public DbsField getApp_Prap_Rule_View_Ap_Cref_Doi() { return app_Prap_Rule_View_Ap_Cref_Doi; }

    public DbsField getApp_Prap_Rule_View_Ap_Alloc_Ind() { return app_Prap_Rule_View_Ap_Alloc_Ind; }

    public DbsField getApp_Prap_Rule_View_Ap_Irc_Sectn_Grp_Cde() { return app_Prap_Rule_View_Ap_Irc_Sectn_Grp_Cde; }

    public DbsField getApp_Prap_Rule_View_Ap_Irc_Sectn_Cde() { return app_Prap_Rule_View_Ap_Irc_Sectn_Cde; }

    public DbsField getApp_Prap_Rule_View_Ap_Inst_Link_Cde() { return app_Prap_Rule_View_Ap_Inst_Link_Cde; }

    public DbsField getApp_Prap_Rule_View_Ap_Applcnt_Req_Type() { return app_Prap_Rule_View_Ap_Applcnt_Req_Type; }

    public DbsField getApp_Prap_Rule_View_Ap_Addr_Sync_Ind() { return app_Prap_Rule_View_Ap_Addr_Sync_Ind; }

    public DbsField getApp_Prap_Rule_View_Ap_Tiaa_Service_Agent() { return app_Prap_Rule_View_Ap_Tiaa_Service_Agent; }

    public DbsField getApp_Prap_Rule_View_Ap_Prap_Rsch_Mit_Ind() { return app_Prap_Rule_View_Ap_Prap_Rsch_Mit_Ind; }

    public DbsField getApp_Prap_Rule_View_Ap_Ppg_Team_Cde() { return app_Prap_Rule_View_Ap_Ppg_Team_Cde; }

    public DbsField getApp_Prap_Rule_View_Ap_Tiaa_Cntrct() { return app_Prap_Rule_View_Ap_Tiaa_Cntrct; }

    public DbsGroup getApp_Prap_Rule_View_Ap_Tiaa_CntrctRedef15() { return app_Prap_Rule_View_Ap_Tiaa_CntrctRedef15; }

    public DbsField getApp_Prap_Rule_View_Ap_Pref_New() { return app_Prap_Rule_View_Ap_Pref_New; }

    public DbsField getApp_Prap_Rule_View_Ap_Cont_New() { return app_Prap_Rule_View_Ap_Cont_New; }

    public DbsField getApp_Prap_Rule_View_Ap_Cref_Cert() { return app_Prap_Rule_View_Ap_Cref_Cert; }

    public DbsGroup getApp_Prap_Rule_View_Ap_Cref_CertRedef16() { return app_Prap_Rule_View_Ap_Cref_CertRedef16; }

    public DbsField getApp_Prap_Rule_View_Ap_C_Pref_New() { return app_Prap_Rule_View_Ap_C_Pref_New; }

    public DbsField getApp_Prap_Rule_View_Ap_C_Cont_New() { return app_Prap_Rule_View_Ap_C_Cont_New; }

    public DbsField getApp_Prap_Rule_View_Ap_Rlc_Cref_Cert() { return app_Prap_Rule_View_Ap_Rlc_Cref_Cert; }

    public DbsGroup getApp_Prap_Rule_View_Ap_Rlc_Cref_CertRedef17() { return app_Prap_Rule_View_Ap_Rlc_Cref_CertRedef17; }

    public DbsField getApp_Prap_Rule_View_Ap_Rlc_Pref_New() { return app_Prap_Rule_View_Ap_Rlc_Pref_New; }

    public DbsField getApp_Prap_Rule_View_Ap_Rlc_Cont_New() { return app_Prap_Rule_View_Ap_Rlc_Cont_New; }

    public DbsField getApp_Prap_Rule_View_Ap_Allocation_Model_Type() { return app_Prap_Rule_View_Ap_Allocation_Model_Type; }

    public DbsField getApp_Prap_Rule_View_Ap_Divorce_Ind() { return app_Prap_Rule_View_Ap_Divorce_Ind; }

    public DbsField getApp_Prap_Rule_View_Ap_Email_Address() { return app_Prap_Rule_View_Ap_Email_Address; }

    public DbsField getApp_Prap_Rule_View_Ap_E_Signed_Appl_Ind() { return app_Prap_Rule_View_Ap_E_Signed_Appl_Ind; }

    public DbsField getApp_Prap_Rule_View_Ap_Eft_Requested_Ind() { return app_Prap_Rule_View_Ap_Eft_Requested_Ind; }

    public DbsField getApp_Prap_Rule_View_Ap_Allocation_Fmt() { return app_Prap_Rule_View_Ap_Allocation_Fmt; }

    public DbsGroup getApp_Prap_Rule_View_Ap_Fund_Identifier() { return app_Prap_Rule_View_Ap_Fund_Identifier; }

    public DbsField getApp_Prap_Rule_View_Ap_Fund_Cde() { return app_Prap_Rule_View_Ap_Fund_Cde; }

    public DbsField getApp_Prap_Rule_View_Ap_Allocation_Pct() { return app_Prap_Rule_View_Ap_Allocation_Pct; }

    public DbsField getApp_Prap_Rule_View_Ap_Sgrd_Plan_No() { return app_Prap_Rule_View_Ap_Sgrd_Plan_No; }

    public DbsField getApp_Prap_Rule_View_Ap_Sgrd_Subplan_No() { return app_Prap_Rule_View_Ap_Sgrd_Subplan_No; }

    public DbsField getApp_Prap_Rule_View_Ap_Sgrd_Part_Ext() { return app_Prap_Rule_View_Ap_Sgrd_Part_Ext; }

    public DbsGroup getRoman_Global() { return roman_Global; }

    public DbsField getRoman_Global_Display_Region() { return roman_Global_Display_Region; }

    public DbsField getRoman_Global_Display_Need() { return roman_Global_Display_Need; }

    public DbsField getRoman_Global_Display_Contracts_Tot() { return roman_Global_Display_Contracts_Tot; }

    public DbsField getRoman_Global_Display_Out_Balance() { return roman_Global_Display_Out_Balance; }

    public DbsGroup getRoman_Global_Display_Table() { return roman_Global_Display_Table; }

    public DbsField getRoman_Global_Display_Product() { return roman_Global_Display_Product; }

    public DbsField getRoman_Global_Display_Count() { return roman_Global_Display_Count; }

    public DbsField getRoman_Global_Display_Contra_Count() { return roman_Global_Display_Contra_Count; }

    public DbsField getRoman_Global_Display_Error_Flag() { return roman_Global_Display_Error_Flag; }

    public DbsGroup getRoman_Global_Pnd_Cnt_Data() { return roman_Global_Pnd_Cnt_Data; }

    public DbsField getRoman_Global_Pnd_Cnt_Product_Code() { return roman_Global_Pnd_Cnt_Product_Code; }

    public DbsField getRoman_Global_Pnd_Cnt_Product_Group() { return roman_Global_Pnd_Cnt_Product_Group; }

    public DbsField getRoman_Global_Display_Prev_Contract() { return roman_Global_Display_Prev_Contract; }

    public DbsField getRoman_Global_Display_Curr_Contract() { return roman_Global_Display_Curr_Contract; }

    public DbsField getRoman_Global_Display_Nums_Contract() { return roman_Global_Display_Nums_Contract; }

    public DbsField getRoman_Global_Pnd_Cnt_Accum_Tally() { return roman_Global_Pnd_Cnt_Accum_Tally; }

    public DbsField getRoman_Global_Pnd_Cnt_Compare_Tally() { return roman_Global_Pnd_Cnt_Compare_Tally; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Balancing_Report_Fields = newGroupInRecord("pnd_Balancing_Report_Fields", "#BALANCING-REPORT-FIELDS");
        pnd_Balancing_Report_Fields_Hold_Proc_Stat = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Hold_Proc_Stat", "HOLD-PROC-STAT", 
            FieldType.STRING, 4);
        pnd_Balancing_Report_Fields_Hold_Proc_StatRedef1 = pnd_Balancing_Report_Fields.newGroupInGroup("pnd_Balancing_Report_Fields_Hold_Proc_StatRedef1", 
            "Redefines", pnd_Balancing_Report_Fields_Hold_Proc_Stat);
        pnd_Balancing_Report_Fields_Hold_Proc_Stat_Reg = pnd_Balancing_Report_Fields_Hold_Proc_StatRedef1.newFieldInGroup("pnd_Balancing_Report_Fields_Hold_Proc_Stat_Reg", 
            "HOLD-PROC-STAT-REG", FieldType.STRING, 1);
        pnd_Balancing_Report_Fields_Hold_Proc_Stat_Need = pnd_Balancing_Report_Fields_Hold_Proc_StatRedef1.newFieldInGroup("pnd_Balancing_Report_Fields_Hold_Proc_Stat_Need", 
            "HOLD-PROC-STAT-NEED", FieldType.STRING, 1);
        pnd_Balancing_Report_Fields_Hold_Proc_Stat_Num = pnd_Balancing_Report_Fields_Hold_Proc_StatRedef1.newFieldInGroup("pnd_Balancing_Report_Fields_Hold_Proc_Stat_Num", 
            "HOLD-PROC-STAT-NUM", FieldType.STRING, 2);
        pnd_Balancing_Report_Fields_Process_Stat_Reject = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Process_Stat_Reject", 
            "PROCESS-STAT-REJECT", FieldType.NUMERIC, 10);
        pnd_Balancing_Report_Fields_Total_Assigned_Premiums = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Total_Assigned_Premiums", 
            "TOTAL-ASSIGNED-PREMIUMS", FieldType.NUMERIC, 10);
        pnd_Balancing_Report_Fields_Total_Issued_Applications = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Total_Issued_Applications", 
            "TOTAL-ISSUED-APPLICATIONS", FieldType.NUMERIC, 10);
        pnd_Balancing_Report_Fields_Pnd_Prap_Cnt = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Prap_Cnt", "#PRAP-CNT", 
            FieldType.NUMERIC, 10);
        pnd_Balancing_Report_Fields_Pnd_Prap_Appl_Cnt = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Prap_Appl_Cnt", "#PRAP-APPL-CNT", 
            FieldType.NUMERIC, 10);
        pnd_Balancing_Report_Fields_Pnd_Prap_New_Appl_Cnt = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Prap_New_Appl_Cnt", 
            "#PRAP-NEW-APPL-CNT", FieldType.NUMERIC, 10);
        pnd_Balancing_Report_Fields_Pnd_Prap_New_Appl_Del_Cnt = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Prap_New_Appl_Del_Cnt", 
            "#PRAP-NEW-APPL-DEL-CNT", FieldType.NUMERIC, 10);
        pnd_Balancing_Report_Fields_Pnd_Prap_Old_Appl_Cnt = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Prap_Old_Appl_Cnt", 
            "#PRAP-OLD-APPL-CNT", FieldType.NUMERIC, 10);
        pnd_Balancing_Report_Fields_Region_Wrong_Count = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Region_Wrong_Count", 
            "REGION-WRONG-COUNT", FieldType.NUMERIC, 10);
        pnd_Balancing_Report_Fields_Pnd_Prap_Prem_Cnt = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Prap_Prem_Cnt", "#PRAP-PREM-CNT", 
            FieldType.NUMERIC, 10);
        pnd_Balancing_Report_Fields_Pnd_Prap_Othr_Cnt = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Prap_Othr_Cnt", "#PRAP-OTHR-CNT", 
            FieldType.NUMERIC, 10);
        pnd_Balancing_Report_Fields_Pnd_Prap_New_Prem_Cnt = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Prap_New_Prem_Cnt", 
            "#PRAP-NEW-PREM-CNT", FieldType.NUMERIC, 10);
        pnd_Balancing_Report_Fields_Pnd_Prap_Old_Prem_Cnt = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Prap_Old_Prem_Cnt", 
            "#PRAP-OLD-PREM-CNT", FieldType.NUMERIC, 10);
        pnd_Balancing_Report_Fields_Pnd_Prap_Updt_Cnt = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Prap_Updt_Cnt", "#PRAP-UPDT-CNT", 
            FieldType.NUMERIC, 10);
        pnd_Balancing_Report_Fields_Test_Contract_Min = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Test_Contract_Min", "TEST-CONTRACT-MIN", 
            FieldType.STRING, 8);
        pnd_Balancing_Report_Fields_Test_Contract_Max = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Test_Contract_Max", "TEST-CONTRACT-MAX", 
            FieldType.STRING, 8);
        pnd_Balancing_Report_Fields_Comp_Contract_Min = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Comp_Contract_Min", "COMP-CONTRACT-MIN", 
            FieldType.STRING, 8);
        pnd_Balancing_Report_Fields_Comp_Contract_Max = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Comp_Contract_Max", "COMP-CONTRACT-MAX", 
            FieldType.STRING, 8);
        pnd_Balancing_Report_Fields_Prev_Contract = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Prev_Contract", "PREV-CONTRACT", 
            FieldType.STRING, 8);
        pnd_Balancing_Report_Fields_Prev_ContractRedef2 = pnd_Balancing_Report_Fields.newGroupInGroup("pnd_Balancing_Report_Fields_Prev_ContractRedef2", 
            "Redefines", pnd_Balancing_Report_Fields_Prev_Contract);
        pnd_Balancing_Report_Fields_Prev_Contract_Pfx = pnd_Balancing_Report_Fields_Prev_ContractRedef2.newFieldInGroup("pnd_Balancing_Report_Fields_Prev_Contract_Pfx", 
            "PREV-CONTRACT-PFX", FieldType.STRING, 1);
        pnd_Balancing_Report_Fields_Prev_Contract_Num7 = pnd_Balancing_Report_Fields_Prev_ContractRedef2.newFieldInGroup("pnd_Balancing_Report_Fields_Prev_Contract_Num7", 
            "PREV-CONTRACT-NUM7", FieldType.STRING, 7);
        pnd_Balancing_Report_Fields_Prev_Contract_Num7Redef3 = pnd_Balancing_Report_Fields_Prev_ContractRedef2.newGroupInGroup("pnd_Balancing_Report_Fields_Prev_Contract_Num7Redef3", 
            "Redefines", pnd_Balancing_Report_Fields_Prev_Contract_Num7);
        pnd_Balancing_Report_Fields_Prev_Contract_Num = pnd_Balancing_Report_Fields_Prev_Contract_Num7Redef3.newFieldInGroup("pnd_Balancing_Report_Fields_Prev_Contract_Num", 
            "PREV-CONTRACT-NUM", FieldType.STRING, 6);
        pnd_Balancing_Report_Fields_Prev_Rest = pnd_Balancing_Report_Fields_Prev_Contract_Num7Redef3.newFieldInGroup("pnd_Balancing_Report_Fields_Prev_Rest", 
            "PREV-REST", FieldType.NUMERIC, 1);
        pnd_Balancing_Report_Fields_Curr_Contract = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Curr_Contract", "CURR-CONTRACT", 
            FieldType.STRING, 8);
        pnd_Balancing_Report_Fields_Curr_ContractRedef4 = pnd_Balancing_Report_Fields.newGroupInGroup("pnd_Balancing_Report_Fields_Curr_ContractRedef4", 
            "Redefines", pnd_Balancing_Report_Fields_Curr_Contract);
        pnd_Balancing_Report_Fields_Curr_Contract_Pfx = pnd_Balancing_Report_Fields_Curr_ContractRedef4.newFieldInGroup("pnd_Balancing_Report_Fields_Curr_Contract_Pfx", 
            "CURR-CONTRACT-PFX", FieldType.STRING, 1);
        pnd_Balancing_Report_Fields_Curr_Contract_Num7 = pnd_Balancing_Report_Fields_Curr_ContractRedef4.newFieldInGroup("pnd_Balancing_Report_Fields_Curr_Contract_Num7", 
            "CURR-CONTRACT-NUM7", FieldType.STRING, 7);
        pnd_Balancing_Report_Fields_Curr_Contract_Num7Redef5 = pnd_Balancing_Report_Fields_Curr_ContractRedef4.newGroupInGroup("pnd_Balancing_Report_Fields_Curr_Contract_Num7Redef5", 
            "Redefines", pnd_Balancing_Report_Fields_Curr_Contract_Num7);
        pnd_Balancing_Report_Fields_Curr_Contract_Num = pnd_Balancing_Report_Fields_Curr_Contract_Num7Redef5.newFieldInGroup("pnd_Balancing_Report_Fields_Curr_Contract_Num", 
            "CURR-CONTRACT-NUM", FieldType.STRING, 6);
        pnd_Balancing_Report_Fields_Curr_Rest = pnd_Balancing_Report_Fields_Curr_Contract_Num7Redef5.newFieldInGroup("pnd_Balancing_Report_Fields_Curr_Rest", 
            "CURR-REST", FieldType.NUMERIC, 1);
        pnd_Balancing_Report_Fields_H_Contract = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_H_Contract", "H-CONTRACT", FieldType.STRING, 
            8);
        pnd_Balancing_Report_Fields_H_ContractRedef6 = pnd_Balancing_Report_Fields.newGroupInGroup("pnd_Balancing_Report_Fields_H_ContractRedef6", "Redefines", 
            pnd_Balancing_Report_Fields_H_Contract);
        pnd_Balancing_Report_Fields_H_Contract_Pfx = pnd_Balancing_Report_Fields_H_ContractRedef6.newFieldInGroup("pnd_Balancing_Report_Fields_H_Contract_Pfx", 
            "H-CONTRACT-PFX", FieldType.STRING, 1);
        pnd_Balancing_Report_Fields_H_Contract_Num7 = pnd_Balancing_Report_Fields_H_ContractRedef6.newFieldInGroup("pnd_Balancing_Report_Fields_H_Contract_Num7", 
            "H-CONTRACT-NUM7", FieldType.STRING, 7);
        pnd_Balancing_Report_Fields_H_Contract_Num7Redef7 = pnd_Balancing_Report_Fields_H_ContractRedef6.newGroupInGroup("pnd_Balancing_Report_Fields_H_Contract_Num7Redef7", 
            "Redefines", pnd_Balancing_Report_Fields_H_Contract_Num7);
        pnd_Balancing_Report_Fields_H_Contract_Num = pnd_Balancing_Report_Fields_H_Contract_Num7Redef7.newFieldInGroup("pnd_Balancing_Report_Fields_H_Contract_Num", 
            "H-CONTRACT-NUM", FieldType.STRING, 6);
        pnd_Balancing_Report_Fields_H_Rest = pnd_Balancing_Report_Fields_H_Contract_Num7Redef7.newFieldInGroup("pnd_Balancing_Report_Fields_H_Rest", "H-REST", 
            FieldType.NUMERIC, 1);
        pnd_Balancing_Report_Fields_Contract7 = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Contract7", "CONTRACT7", FieldType.STRING, 
            7);
        pnd_Balancing_Report_Fields_Contract7Redef8 = pnd_Balancing_Report_Fields.newGroupInGroup("pnd_Balancing_Report_Fields_Contract7Redef8", "Redefines", 
            pnd_Balancing_Report_Fields_Contract7);
        pnd_Balancing_Report_Fields_Contract_Pfx = pnd_Balancing_Report_Fields_Contract7Redef8.newFieldInGroup("pnd_Balancing_Report_Fields_Contract_Pfx", 
            "CONTRACT-PFX", FieldType.STRING, 1);
        pnd_Balancing_Report_Fields_Contract_Num = pnd_Balancing_Report_Fields_Contract7Redef8.newFieldInGroup("pnd_Balancing_Report_Fields_Contract_Num", 
            "CONTRACT-NUM", FieldType.STRING, 6);
        pnd_Balancing_Report_Fields_Contract_Info = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Contract_Info", "CONTRACT-INFO", 
            FieldType.STRING, 50);
        pnd_Balancing_Report_Fields_Contract_InfoRedef9 = pnd_Balancing_Report_Fields.newGroupInGroup("pnd_Balancing_Report_Fields_Contract_InfoRedef9", 
            "Redefines", pnd_Balancing_Report_Fields_Contract_Info);
        pnd_Balancing_Report_Fields_Pnd_Contract_Type = pnd_Balancing_Report_Fields_Contract_InfoRedef9.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Contract_Type", 
            "#CONTRACT-TYPE", FieldType.STRING, 10);
        pnd_Balancing_Report_Fields_Pnd_Contract_Plus = pnd_Balancing_Report_Fields_Contract_InfoRedef9.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Contract_Plus", 
            "#CONTRACT-PLUS", FieldType.STRING, 10);
        pnd_Balancing_Report_Fields_Pnd_Contract_Lob_Type = pnd_Balancing_Report_Fields_Contract_InfoRedef9.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Contract_Lob_Type", 
            "#CONTRACT-LOB-TYPE", FieldType.STRING, 3);
        pnd_Balancing_Report_Fields_Pnd_Contract_Lob_TypeRedef10 = pnd_Balancing_Report_Fields_Contract_InfoRedef9.newGroupInGroup("pnd_Balancing_Report_Fields_Pnd_Contract_Lob_TypeRedef10", 
            "Redefines", pnd_Balancing_Report_Fields_Pnd_Contract_Lob_Type);
        pnd_Balancing_Report_Fields_Pnd_Product_Lob = pnd_Balancing_Report_Fields_Pnd_Contract_Lob_TypeRedef10.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Product_Lob", 
            "#PRODUCT-LOB", FieldType.STRING, 1);
        pnd_Balancing_Report_Fields_Pnd_Product_Lob_Type = pnd_Balancing_Report_Fields_Pnd_Contract_Lob_TypeRedef10.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Product_Lob_Type", 
            "#PRODUCT-LOB-TYPE", FieldType.STRING, 2);
        pnd_Balancing_Report_Fields_Pnd_Contract_Prod_Code = pnd_Balancing_Report_Fields_Contract_InfoRedef9.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Contract_Prod_Code", 
            "#CONTRACT-PROD-CODE", FieldType.STRING, 4);
        pnd_Balancing_Report_Fields_Pnd_Filler = pnd_Balancing_Report_Fields_Contract_InfoRedef9.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Filler", 
            "#FILLER", FieldType.STRING, 1);
        pnd_Balancing_Report_Fields_Pnd_Contract_Start = pnd_Balancing_Report_Fields_Contract_InfoRedef9.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Contract_Start", 
            "#CONTRACT-START", FieldType.STRING, 10);
        pnd_Balancing_Report_Fields_Pnd_Contract_StartRedef11 = pnd_Balancing_Report_Fields_Contract_InfoRedef9.newGroupInGroup("pnd_Balancing_Report_Fields_Pnd_Contract_StartRedef11", 
            "Redefines", pnd_Balancing_Report_Fields_Pnd_Contract_Start);
        pnd_Balancing_Report_Fields_Pnd_Contract_Start_Pfx = pnd_Balancing_Report_Fields_Pnd_Contract_StartRedef11.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Contract_Start_Pfx", 
            "#CONTRACT-START-PFX", FieldType.STRING, 1);
        pnd_Balancing_Report_Fields_Pnd_Contract_Start_Num = pnd_Balancing_Report_Fields_Pnd_Contract_StartRedef11.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Contract_Start_Num", 
            "#CONTRACT-START-NUM", FieldType.STRING, 6);
        pnd_Balancing_Report_Fields_Pnd_Contract_Start_Pfx_C = pnd_Balancing_Report_Fields_Contract_InfoRedef9.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Contract_Start_Pfx_C", 
            "#CONTRACT-START-PFX-C", FieldType.STRING, 1);
        pnd_Balancing_Report_Fields_Pnd_Contract_End = pnd_Balancing_Report_Fields_Contract_InfoRedef9.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Contract_End", 
            "#CONTRACT-END", FieldType.STRING, 10);
        pnd_Balancing_Report_Fields_Pnd_Contract_EndRedef12 = pnd_Balancing_Report_Fields_Contract_InfoRedef9.newGroupInGroup("pnd_Balancing_Report_Fields_Pnd_Contract_EndRedef12", 
            "Redefines", pnd_Balancing_Report_Fields_Pnd_Contract_End);
        pnd_Balancing_Report_Fields_Pnd_Contract_End_Pfx = pnd_Balancing_Report_Fields_Pnd_Contract_EndRedef12.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Contract_End_Pfx", 
            "#CONTRACT-END-PFX", FieldType.STRING, 1);
        pnd_Balancing_Report_Fields_Pnd_Contract_End_Num = pnd_Balancing_Report_Fields_Pnd_Contract_EndRedef12.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Contract_End_Num", 
            "#CONTRACT-END-NUM", FieldType.STRING, 6);
        pnd_Balancing_Report_Fields_Pnd_Contract_End_Pfx_C = pnd_Balancing_Report_Fields_Contract_InfoRedef9.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Contract_End_Pfx_C", 
            "#CONTRACT-END-PFX-C", FieldType.STRING, 1);
        pnd_Balancing_Report_Fields_D_Start_Date = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_D_Start_Date", "D-START-DATE", 
            FieldType.DATE);
        pnd_Balancing_Report_Fields_Pnd_Prt_Start = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Prt_Start", "#PRT-START", 
            FieldType.STRING, 8);
        pnd_Balancing_Report_Fields_Pnd_Process_Status_To_Find = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Process_Status_To_Find", 
            "#PROCESS-STATUS-TO-FIND", FieldType.STRING, 4);
        pnd_Balancing_Report_Fields_Pnd_Product_Code_To_Find = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Product_Code_To_Find", 
            "#PRODUCT-CODE-TO-FIND", FieldType.STRING, 4);
        pnd_Balancing_Report_Fields_Tens_Bucket = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Tens_Bucket", "TENS-BUCKET", 
            FieldType.INTEGER, 4);
        pnd_Balancing_Report_Fields_Num_Tens = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Num_Tens", "NUM-TENS", FieldType.NUMERIC, 
            4);
        pnd_Balancing_Report_Fields_Ten_Sub = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Ten_Sub", "TEN-SUB", FieldType.INTEGER, 
            4);
        pnd_Balancing_Report_Fields_One_Sub = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_One_Sub", "ONE-SUB", FieldType.INTEGER, 
            4);
        pnd_Balancing_Report_Fields_Where_Sub = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Where_Sub", "WHERE-SUB", FieldType.INTEGER, 
            4);
        pnd_Balancing_Report_Fields_Pnd_Multi_Prod_Sub = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Multi_Prod_Sub", 
            "#MULTI-PROD-SUB", FieldType.INTEGER, 4);
        pnd_Balancing_Report_Fields_Display_Sub = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Display_Sub", "DISPLAY-SUB", 
            FieldType.INTEGER, 4);
        pnd_Balancing_Report_Fields_Pnd_Prod_Sub = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Prod_Sub", "#PROD-SUB", 
            FieldType.INTEGER, 4);
        pnd_Balancing_Report_Fields_Total_D_Sub = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Total_D_Sub", "TOTAL-D-SUB", 
            FieldType.INTEGER, 4);
        pnd_Balancing_Report_Fields_Total_A_Sub = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Total_A_Sub", "TOTAL-A-SUB", 
            FieldType.INTEGER, 4);
        pnd_Balancing_Report_Fields_Pnd_Group_Val = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Group_Val", "#GROUP-VAL", 
            FieldType.INTEGER, 4);
        pnd_Balancing_Report_Fields_Pnd_Prod_Val = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Prod_Val", "#PROD-VAL", 
            FieldType.INTEGER, 4);
        pnd_Balancing_Report_Fields_Pnd_Proc_Val = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Proc_Val", "#PROC-VAL", 
            FieldType.INTEGER, 4);
        pnd_Balancing_Report_Fields_Pnd_That_Tally = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_That_Tally", "#THAT-TALLY", 
            FieldType.INTEGER, 4);
        pnd_Balancing_Report_Fields_Pnd_Acctall_Val = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Acctall_Val", "#ACCTALL-VAL", 
            FieldType.INTEGER, 4);
        pnd_Balancing_Report_Fields_Pnd_Prap_Soc_Sec = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Prap_Soc_Sec", "#PRAP-SOC-SEC", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Balancing_Report_Fields_Pnd_Ap_Prap_Key = pnd_Balancing_Report_Fields.newFieldInGroup("pnd_Balancing_Report_Fields_Pnd_Ap_Prap_Key", "#AP-PRAP-KEY", 
            FieldType.STRING, 15);

        vw_app_Prap_Rule_View = new DataAccessProgramView(new NameInfo("vw_app_Prap_Rule_View", "APP-PRAP-RULE-VIEW"), "ANNTY_ACTVTY_PRAP_12", "ACIS_ANNTY_PRAP", 
            DdmPeriodicGroups.getInstance().getGroups("ANNTY_ACTVTY_PRAP_12"));
        app_Prap_Rule_View_Ap_Coll_Code = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Coll_Code", "AP-COLL-CODE", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "AP_COLL_CODE");
        app_Prap_Rule_View_Ap_Coll_CodeRedef13 = vw_app_Prap_Rule_View.getRecord().newGroupInGroup("app_Prap_Rule_View_Ap_Coll_CodeRedef13", "Redefines", 
            app_Prap_Rule_View_Ap_Coll_Code);
        app_Prap_Rule_View_Ap_Coll_Cd = app_Prap_Rule_View_Ap_Coll_CodeRedef13.newFieldInGroup("app_Prap_Rule_View_Ap_Coll_Cd", "AP-COLL-CD", FieldType.STRING, 
            4);
        app_Prap_Rule_View_Ap_Coll_Fill = app_Prap_Rule_View_Ap_Coll_CodeRedef13.newFieldInGroup("app_Prap_Rule_View_Ap_Coll_Fill", "AP-COLL-FILL", FieldType.STRING, 
            2);
        app_Prap_Rule_View_Ap_G_Key = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_G_Key", "AP-G-KEY", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_G_KEY");
        app_Prap_Rule_View_Ap_G_Ind = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_G_Ind", "AP-G-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_G_IND");
        app_Prap_Rule_View_Ap_Soc_Sec = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "AP_SOC_SEC");
        app_Prap_Rule_View_Ap_Dob = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Dob", "AP-DOB", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "AP_DOB");
        app_Prap_Rule_View_Ap_Lob = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Lob", "AP-LOB", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_LOB");
        app_Prap_Rule_View_Ap_Lob.setDdmHeader("LOB");
        app_Prap_Rule_View_Ap_Bill_Code = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Bill_Code", "AP-BILL-CODE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_BILL_CODE");
        app_Prap_Rule_View_Ap_T_Doi = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_T_Doi", "AP-T-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_T_DOI");
        app_Prap_Rule_View_Ap_C_Doi = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_C_Doi", "AP-C-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_C_DOI");
        app_Prap_Rule_View_Ap_Curr = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Curr", "AP-CURR", FieldType.NUMERIC, 1, 
            RepeatingFieldStrategy.None, "AP_CURR");
        app_Prap_Rule_View_Ap_T_Age_1st = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_T_Age_1st", "AP-T-AGE-1ST", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_T_AGE_1ST");
        app_Prap_Rule_View_Ap_C_Age_1st = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_C_Age_1st", "AP-C-AGE-1ST", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_C_AGE_1ST");
        app_Prap_Rule_View_Ap_Ownership = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Ownership", "AP-OWNERSHIP", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_OWNERSHIP");
        app_Prap_Rule_View_Ap_Sex = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Sex", "AP-SEX", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_SEX");
        app_Prap_Rule_View_Ap_Sex.setDdmHeader("SEX CDE");
        app_Prap_Rule_View_Ap_Mail_Zip = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Mail_Zip", "AP-MAIL-ZIP", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "AP_MAIL_ZIP");
        app_Prap_Rule_View_Ap_Name_Addr_Cd = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Name_Addr_Cd", "AP-NAME-ADDR-CD", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "AP_NAME_ADDR_CD");
        app_Prap_Rule_View_Ap_Dt_Ent_Sys = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Dt_Ent_Sys", "AP-DT-ENT-SYS", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "AP_DT_ENT_SYS");
        app_Prap_Rule_View_Ap_Dt_Released = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Dt_Released", "AP-DT-RELEASED", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "AP_DT_RELEASED");
        app_Prap_Rule_View_Ap_Dt_Matched = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Dt_Matched", "AP-DT-MATCHED", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "AP_DT_MATCHED");
        app_Prap_Rule_View_Ap_Dt_Deleted = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Dt_Deleted", "AP-DT-DELETED", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "AP_DT_DELETED");
        app_Prap_Rule_View_Ap_Dt_Withdrawn = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Dt_Withdrawn", "AP-DT-WITHDRAWN", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_WITHDRAWN");
        app_Prap_Rule_View_Ap_Alloc_Discr = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Alloc_Discr", "AP-ALLOC-DISCR", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_ALLOC_DISCR");
        app_Prap_Rule_View_Ap_Release_Ind = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Release_Ind", "AP-RELEASE-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_RELEASE_IND");
        app_Prap_Rule_View_Ap_Type = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Type", "AP-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_TYPE");
        app_Prap_Rule_View_Ap_Other_Pols = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Other_Pols", "AP-OTHER-POLS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_OTHER_POLS");
        app_Prap_Rule_View_Ap_Record_Type = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Record_Type", "AP-RECORD-TYPE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_RECORD_TYPE");
        app_Prap_Rule_View_Ap_Status = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Status", "AP-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_STATUS");
        app_Prap_Rule_View_Ap_Numb_Dailys = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Numb_Dailys", "AP-NUMB-DAILYS", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_NUMB_DAILYS");
        app_Prap_Rule_View_Ap_Dt_App_Recvd = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Dt_App_Recvd", "AP-DT-APP-RECVD", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_APP_RECVD");
        app_Prap_Rule_View_Ap_Allocation_Info = vw_app_Prap_Rule_View.getRecord().newGroupArrayInGroup("app_Prap_Rule_View_Ap_Allocation_Info", "AP-ALLOCATION-INFO", 
            new DbsArrayController(1,20), RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_ALLOCATION_INFO");
        app_Prap_Rule_View_Ap_Allocation = app_Prap_Rule_View_Ap_Allocation_Info.newFieldInGroup("app_Prap_Rule_View_Ap_Allocation", "AP-ALLOCATION", 
            FieldType.PACKED_DECIMAL, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ALLOCATION", "ACIS_ANNTY_PRAP_AP_ALLOCATION_INFO");
        app_Prap_Rule_View_Ap_App_Source = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_App_Source", "AP-APP-SOURCE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_APP_SOURCE");
        app_Prap_Rule_View_Ap_App_Source.setDdmHeader("SOURCE");
        app_Prap_Rule_View_Ap_Region_Code = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Region_Code", "AP-REGION-CODE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "AP_REGION_CODE");
        app_Prap_Rule_View_Ap_Orig_Issue_State = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Orig_Issue_State", "AP-ORIG-ISSUE-STATE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_ORIG_ISSUE_STATE");
        app_Prap_Rule_View_Ap_Ownership_Type = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Ownership_Type", "AP-OWNERSHIP-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_OWNERSHIP_TYPE");
        app_Prap_Rule_View_Ap_Ownership_Type.setDdmHeader("CNTRCT OWNERSHIP");
        app_Prap_Rule_View_Ap_Lob_Type = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Lob_Type", "AP-LOB-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_LOB_TYPE");
        app_Prap_Rule_View_Ap_Lob_Type.setDdmHeader("LOB/TYPE");
        app_Prap_Rule_View_Ap_Split_Code = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Split_Code", "AP-SPLIT-CODE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "AP_SPLIT_CODE");
        app_Prap_Rule_View_Count_Castap_Address_Info = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Count_Castap_Address_Info", 
            "C*AP-ADDRESS-INFO", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        app_Prap_Rule_View_Ap_Address_Info = vw_app_Prap_Rule_View.getRecord().newGroupInGroup("app_Prap_Rule_View_Ap_Address_Info", "AP-ADDRESS-INFO", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        app_Prap_Rule_View_Ap_Address_Line = app_Prap_Rule_View_Ap_Address_Info.newFieldArrayInGroup("app_Prap_Rule_View_Ap_Address_Line", "AP-ADDRESS-LINE", 
            FieldType.STRING, 35, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ADDRESS_LINE", "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        app_Prap_Rule_View_Ap_City = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_City", "AP-CITY", FieldType.STRING, 27, 
            RepeatingFieldStrategy.None, "AP_CITY");
        app_Prap_Rule_View_Ap_Current_State_Code = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Current_State_Code", "AP-CURRENT-STATE-CODE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_CURRENT_STATE_CODE");
        app_Prap_Rule_View_Ap_Dana_Transaction_Cd = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Dana_Transaction_Cd", "AP-DANA-TRANSACTION-CD", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_DANA_TRANSACTION_CD");
        app_Prap_Rule_View_Ap_Dana_Stndrd_Rtn_Cd = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Dana_Stndrd_Rtn_Cd", "AP-DANA-STNDRD-RTN-CD", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_DANA_STNDRD_RTN_CD");
        app_Prap_Rule_View_Ap_Dana_Finalist_Reason_Cd = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Dana_Finalist_Reason_Cd", 
            "AP-DANA-FINALIST-REASON-CD", FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_DANA_FINALIST_REASON_CD");
        app_Prap_Rule_View_Ap_Dana_Addr_Stndrd_Code = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Dana_Addr_Stndrd_Code", 
            "AP-DANA-ADDR-STNDRD-CODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_DANA_ADDR_STNDRD_CODE");
        app_Prap_Rule_View_Ap_Dana_Stndrd_Overide = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Dana_Stndrd_Overide", "AP-DANA-STNDRD-OVERIDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_DANA_STNDRD_OVERIDE");
        app_Prap_Rule_View_Ap_Dana_Postal_Data_Fields = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Dana_Postal_Data_Fields", 
            "AP-DANA-POSTAL-DATA-FIELDS", FieldType.STRING, 44, RepeatingFieldStrategy.None, "AP_DANA_POSTAL_DATA_FIELDS");
        app_Prap_Rule_View_Ap_Dana_Addr_Geographic_Code = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Dana_Addr_Geographic_Code", 
            "AP-DANA-ADDR-GEOGRAPHIC-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_DANA_ADDR_GEOGRAPHIC_CODE");
        app_Prap_Rule_View_Ap_Annuity_Start_Date = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Annuity_Start_Date", "AP-ANNUITY-START-DATE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_ANNUITY_START_DATE");
        app_Prap_Rule_View_Ap_Maximum_Alloc_Pct = vw_app_Prap_Rule_View.getRecord().newGroupArrayInGroup("app_Prap_Rule_View_Ap_Maximum_Alloc_Pct", "AP-MAXIMUM-ALLOC-PCT", 
            new DbsArrayController(1,100), RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        app_Prap_Rule_View_Ap_Max_Alloc_Pct = app_Prap_Rule_View_Ap_Maximum_Alloc_Pct.newFieldInGroup("app_Prap_Rule_View_Ap_Max_Alloc_Pct", "AP-MAX-ALLOC-PCT", 
            FieldType.STRING, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MAX_ALLOC_PCT", "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        app_Prap_Rule_View_Ap_Max_Alloc_Pct_Sign = app_Prap_Rule_View_Ap_Maximum_Alloc_Pct.newFieldInGroup("app_Prap_Rule_View_Ap_Max_Alloc_Pct_Sign", 
            "AP-MAX-ALLOC-PCT-SIGN", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MAX_ALLOC_PCT_SIGN", "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        app_Prap_Rule_View_Ap_Bene_Info_Type = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Bene_Info_Type", "AP-BENE-INFO-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_INFO_TYPE");
        app_Prap_Rule_View_Ap_Primary_Std_Ent = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Primary_Std_Ent", "AP-PRIMARY-STD-ENT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_PRIMARY_STD_ENT");
        app_Prap_Rule_View_Ap_Bene_Primary_Info = vw_app_Prap_Rule_View.getRecord().newGroupInGroup("app_Prap_Rule_View_Ap_Bene_Primary_Info", "AP-BENE-PRIMARY-INFO", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_BENE_PRIMARY_INFO");
        app_Prap_Rule_View_Ap_Primary_Bene_Info = app_Prap_Rule_View_Ap_Bene_Primary_Info.newFieldArrayInGroup("app_Prap_Rule_View_Ap_Primary_Bene_Info", 
            "AP-PRIMARY-BENE-INFO", FieldType.STRING, 72, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_PRIMARY_BENE_INFO", 
            "ACIS_ANNTY_PRAP_AP_BENE_PRIMARY_INFO");
        app_Prap_Rule_View_Ap_Contingent_Std_Ent = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Contingent_Std_Ent", "AP-CONTINGENT-STD-ENT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_CONTINGENT_STD_ENT");
        app_Prap_Rule_View_Ap_Bene_Contingent_Info = vw_app_Prap_Rule_View.getRecord().newGroupArrayInGroup("app_Prap_Rule_View_Ap_Bene_Contingent_Info", 
            "AP-BENE-CONTINGENT-INFO", new DbsArrayController(1,5), RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_BENE_CONTINGENT_INFO");
        app_Prap_Rule_View_Ap_Contingent_Bene_Info = app_Prap_Rule_View_Ap_Bene_Contingent_Info.newFieldInGroup("app_Prap_Rule_View_Ap_Contingent_Bene_Info", 
            "AP-CONTINGENT-BENE-INFO", FieldType.STRING, 72, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CONTINGENT_BENE_INFO", "ACIS_ANNTY_PRAP_AP_BENE_CONTINGENT_INFO");
        app_Prap_Rule_View_Ap_Bene_Estate = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Bene_Estate", "AP-BENE-ESTATE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_BENE_ESTATE");
        app_Prap_Rule_View_Ap_Bene_Trust = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Bene_Trust", "AP-BENE-TRUST", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_BENE_TRUST");
        app_Prap_Rule_View_Ap_Bene_Category = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Bene_Category", "AP-BENE-CATEGORY", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_CATEGORY");
        app_Prap_Rule_View_Ap_Mail_Instructions = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Mail_Instructions", "AP-MAIL-INSTRUCTIONS", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MAIL_INSTRUCTIONS");
        app_Prap_Rule_View_Ap_Eop_Addl_Cref_Request = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Eop_Addl_Cref_Request", 
            "AP-EOP-ADDL-CREF-REQUEST", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_EOP_ADDL_CREF_REQUEST");
        app_Prap_Rule_View_Ap_Process_Status = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Process_Status", "AP-PROCESS-STATUS", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "AP_PROCESS_STATUS");
        app_Prap_Rule_View_Ap_Process_StatusRedef14 = vw_app_Prap_Rule_View.getRecord().newGroupInGroup("app_Prap_Rule_View_Ap_Process_StatusRedef14", 
            "Redefines", app_Prap_Rule_View_Ap_Process_Status);
        app_Prap_Rule_View_Pnd_Ap_Proc_Stat_Reg = app_Prap_Rule_View_Ap_Process_StatusRedef14.newFieldInGroup("app_Prap_Rule_View_Pnd_Ap_Proc_Stat_Reg", 
            "#AP-PROC-STAT-REG", FieldType.STRING, 1);
        app_Prap_Rule_View_Pnd_Ap_Proc_Stat_Need = app_Prap_Rule_View_Ap_Process_StatusRedef14.newFieldInGroup("app_Prap_Rule_View_Pnd_Ap_Proc_Stat_Need", 
            "#AP-PROC-STAT-NEED", FieldType.STRING, 1);
        app_Prap_Rule_View_Pnd_Ap_Proc_Stat_Num = app_Prap_Rule_View_Ap_Process_StatusRedef14.newFieldInGroup("app_Prap_Rule_View_Pnd_Ap_Proc_Stat_Num", 
            "#AP-PROC-STAT-NUM", FieldType.STRING, 1);
        app_Prap_Rule_View_Ap_Coll_St_Cd = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Coll_St_Cd", "AP-COLL-ST-CD", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "AP_COLL_ST_CD");
        app_Prap_Rule_View_Ap_Csm_Sec_Seg = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Csm_Sec_Seg", "AP-CSM-SEC-SEG", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "AP_CSM_SEC_SEG");
        app_Prap_Rule_View_Ap_Rlc_College = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Rlc_College", "AP-RLC-COLLEGE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "AP_RLC_COLLEGE");
        app_Prap_Rule_View_Ap_Cor_Prfx_Nme = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Cor_Prfx_Nme", "AP-COR-PRFX-NME", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_COR_PRFX_NME");
        app_Prap_Rule_View_Ap_Cor_Last_Nme = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_LAST_NME");
        app_Prap_Rule_View_Ap_Cor_First_Nme = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_FIRST_NME");
        app_Prap_Rule_View_Ap_Cor_Mddle_Nme = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Cor_Mddle_Nme", "AP-COR-MDDLE-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_MDDLE_NME");
        app_Prap_Rule_View_Ap_Cor_Sffx_Nme = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Cor_Sffx_Nme", "AP-COR-SFFX-NME", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_COR_SFFX_NME");
        app_Prap_Rule_View_Ap_Ph_Hist_Ind = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Ph_Hist_Ind", "AP-PH-HIST-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_PH_HIST_IND");
        app_Prap_Rule_View_Ap_Rcrd_Updt_Tm_Stamp = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Rcrd_Updt_Tm_Stamp", "AP-RCRD-UPDT-TM-STAMP", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_RCRD_UPDT_TM_STAMP");
        app_Prap_Rule_View_Ap_Contact_Mode = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Contact_Mode", "AP-CONTACT-MODE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_CONTACT_MODE");
        app_Prap_Rule_View_Ap_Racf_Id = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Racf_Id", "AP-RACF-ID", FieldType.STRING, 
            16, RepeatingFieldStrategy.None, "AP_RACF_ID");
        app_Prap_Rule_View_Ap_Pin_Nbr = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "AP_PIN_NBR");
        app_Prap_Rule_View_Ap_Rqst_Log_Dte_Tm = vw_app_Prap_Rule_View.getRecord().newGroupInGroup("app_Prap_Rule_View_Ap_Rqst_Log_Dte_Tm", "AP-RQST-LOG-DTE-TM", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        app_Prap_Rule_View_Ap_Rqst_Log_Dte_Time = app_Prap_Rule_View_Ap_Rqst_Log_Dte_Tm.newFieldArrayInGroup("app_Prap_Rule_View_Ap_Rqst_Log_Dte_Time", 
            "AP-RQST-LOG-DTE-TIME", FieldType.STRING, 15, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_RQST_LOG_DTE_TIME", 
            "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        app_Prap_Rule_View_Ap_Sync_Ind = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Sync_Ind", "AP-SYNC-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_SYNC_IND");
        app_Prap_Rule_View_Ap_Cor_Ind = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Cor_Ind", "AP-COR-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_COR_IND");
        app_Prap_Rule_View_Ap_Tiaa_Doi = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Tiaa_Doi", "AP-TIAA-DOI", FieldType.DATE, 
            RepeatingFieldStrategy.None, "AP_TIAA_DOI");
        app_Prap_Rule_View_Ap_Cref_Doi = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Cref_Doi", "AP-CREF-DOI", FieldType.DATE, 
            RepeatingFieldStrategy.None, "AP_CREF_DOI");
        app_Prap_Rule_View_Ap_Alloc_Ind = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Alloc_Ind", "AP-ALLOC-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_ALLOC_IND");
        app_Prap_Rule_View_Ap_Irc_Sectn_Grp_Cde = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Irc_Sectn_Grp_Cde", "AP-IRC-SECTN-GRP-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_IRC_SECTN_GRP_CDE");
        app_Prap_Rule_View_Ap_Irc_Sectn_Cde = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Irc_Sectn_Cde", "AP-IRC-SECTN-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_IRC_SECTN_CDE");
        app_Prap_Rule_View_Ap_Inst_Link_Cde = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Inst_Link_Cde", "AP-INST-LINK-CDE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "AP_INST_LINK_CDE");
        app_Prap_Rule_View_Ap_Applcnt_Req_Type = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Applcnt_Req_Type", "AP-APPLCNT-REQ-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_APPLCNT_REQ_TYPE");
        app_Prap_Rule_View_Ap_Applcnt_Req_Type.setDdmHeader("APP REQ TYPE");
        app_Prap_Rule_View_Ap_Addr_Sync_Ind = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Addr_Sync_Ind", "AP-ADDR-SYNC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ADDR_SYNC_IND");
        app_Prap_Rule_View_Ap_Tiaa_Service_Agent = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Tiaa_Service_Agent", "AP-TIAA-SERVICE-AGENT", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_TIAA_SERVICE_AGENT");
        app_Prap_Rule_View_Ap_Prap_Rsch_Mit_Ind = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Prap_Rsch_Mit_Ind", "AP-PRAP-RSCH-MIT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_PRAP_RSCH_MIT_IND");
        app_Prap_Rule_View_Ap_Ppg_Team_Cde = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Ppg_Team_Cde", "AP-PPG-TEAM-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_PPG_TEAM_CDE");
        app_Prap_Rule_View_Ap_Tiaa_Cntrct = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "AP_TIAA_CNTRCT");
        app_Prap_Rule_View_Ap_Tiaa_CntrctRedef15 = vw_app_Prap_Rule_View.getRecord().newGroupInGroup("app_Prap_Rule_View_Ap_Tiaa_CntrctRedef15", "Redefines", 
            app_Prap_Rule_View_Ap_Tiaa_Cntrct);
        app_Prap_Rule_View_Ap_Pref_New = app_Prap_Rule_View_Ap_Tiaa_CntrctRedef15.newFieldInGroup("app_Prap_Rule_View_Ap_Pref_New", "AP-PREF-NEW", FieldType.STRING, 
            1);
        app_Prap_Rule_View_Ap_Cont_New = app_Prap_Rule_View_Ap_Tiaa_CntrctRedef15.newFieldInGroup("app_Prap_Rule_View_Ap_Cont_New", "AP-CONT-NEW", FieldType.STRING, 
            7);
        app_Prap_Rule_View_Ap_Cref_Cert = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Cref_Cert", "AP-CREF-CERT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "AP_CREF_CERT");
        app_Prap_Rule_View_Ap_Cref_CertRedef16 = vw_app_Prap_Rule_View.getRecord().newGroupInGroup("app_Prap_Rule_View_Ap_Cref_CertRedef16", "Redefines", 
            app_Prap_Rule_View_Ap_Cref_Cert);
        app_Prap_Rule_View_Ap_C_Pref_New = app_Prap_Rule_View_Ap_Cref_CertRedef16.newFieldInGroup("app_Prap_Rule_View_Ap_C_Pref_New", "AP-C-PREF-NEW", 
            FieldType.STRING, 1);
        app_Prap_Rule_View_Ap_C_Cont_New = app_Prap_Rule_View_Ap_Cref_CertRedef16.newFieldInGroup("app_Prap_Rule_View_Ap_C_Cont_New", "AP-C-CONT-NEW", 
            FieldType.STRING, 7);
        app_Prap_Rule_View_Ap_Rlc_Cref_Cert = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Rlc_Cref_Cert", "AP-RLC-CREF-CERT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_RLC_CREF_CERT");
        app_Prap_Rule_View_Ap_Rlc_Cref_CertRedef17 = vw_app_Prap_Rule_View.getRecord().newGroupInGroup("app_Prap_Rule_View_Ap_Rlc_Cref_CertRedef17", "Redefines", 
            app_Prap_Rule_View_Ap_Rlc_Cref_Cert);
        app_Prap_Rule_View_Ap_Rlc_Pref_New = app_Prap_Rule_View_Ap_Rlc_Cref_CertRedef17.newFieldInGroup("app_Prap_Rule_View_Ap_Rlc_Pref_New", "AP-RLC-PREF-NEW", 
            FieldType.STRING, 1);
        app_Prap_Rule_View_Ap_Rlc_Cont_New = app_Prap_Rule_View_Ap_Rlc_Cref_CertRedef17.newFieldInGroup("app_Prap_Rule_View_Ap_Rlc_Cont_New", "AP-RLC-CONT-NEW", 
            FieldType.STRING, 7);
        app_Prap_Rule_View_Ap_Allocation_Model_Type = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Allocation_Model_Type", 
            "AP-ALLOCATION-MODEL-TYPE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_ALLOCATION_MODEL_TYPE");
        app_Prap_Rule_View_Ap_Divorce_Ind = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Divorce_Ind", "AP-DIVORCE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_DIVORCE_IND");
        app_Prap_Rule_View_Ap_Email_Address = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Email_Address", "AP-EMAIL-ADDRESS", 
            FieldType.STRING, 50, RepeatingFieldStrategy.None, "AP_EMAIL_ADDRESS");
        app_Prap_Rule_View_Ap_E_Signed_Appl_Ind = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_E_Signed_Appl_Ind", "AP-E-SIGNED-APPL-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_E_SIGNED_APPL_IND");
        app_Prap_Rule_View_Ap_Eft_Requested_Ind = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Eft_Requested_Ind", "AP-EFT-REQUESTED-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_EFT_REQUESTED_IND");
        app_Prap_Rule_View_Ap_Allocation_Fmt = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Allocation_Fmt", "AP-ALLOCATION-FMT", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ALLOCATION_FMT");
        app_Prap_Rule_View_Ap_Fund_Identifier = vw_app_Prap_Rule_View.getRecord().newGroupArrayInGroup("app_Prap_Rule_View_Ap_Fund_Identifier", "AP-FUND-IDENTIFIER", 
            new DbsArrayController(1,100), RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        app_Prap_Rule_View_Ap_Fund_Cde = app_Prap_Rule_View_Ap_Fund_Identifier.newFieldInGroup("app_Prap_Rule_View_Ap_Fund_Cde", "AP-FUND-CDE", FieldType.STRING, 
            10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_FUND_CDE", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        app_Prap_Rule_View_Ap_Allocation_Pct = app_Prap_Rule_View_Ap_Fund_Identifier.newFieldInGroup("app_Prap_Rule_View_Ap_Allocation_Pct", "AP-ALLOCATION-PCT", 
            FieldType.NUMERIC, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ALLOCATION_PCT", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        app_Prap_Rule_View_Ap_Sgrd_Plan_No = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_PLAN_NO");
        app_Prap_Rule_View_Ap_Sgrd_Subplan_No = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Sgrd_Subplan_No", "AP-SGRD-SUBPLAN-NO", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_SUBPLAN_NO");
        app_Prap_Rule_View_Ap_Sgrd_Part_Ext = vw_app_Prap_Rule_View.getRecord().newFieldInGroup("app_Prap_Rule_View_Ap_Sgrd_Part_Ext", "AP-SGRD-PART-EXT", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_SGRD_PART_EXT");

        roman_Global = newGroupInRecord("roman_Global", "ROMAN-GLOBAL");
        roman_Global_Display_Region = roman_Global.newFieldInGroup("roman_Global_Display_Region", "DISPLAY-REGION", FieldType.STRING, 3);
        roman_Global_Display_Need = roman_Global.newFieldInGroup("roman_Global_Display_Need", "DISPLAY-NEED", FieldType.STRING, 10);
        roman_Global_Display_Contracts_Tot = roman_Global.newFieldInGroup("roman_Global_Display_Contracts_Tot", "DISPLAY-CONTRACTS-TOT", FieldType.NUMERIC, 
            10);
        roman_Global_Display_Out_Balance = roman_Global.newFieldInGroup("roman_Global_Display_Out_Balance", "DISPLAY-OUT-BALANCE", FieldType.NUMERIC, 
            10);
        roman_Global_Display_Table = roman_Global.newGroupArrayInGroup("roman_Global_Display_Table", "DISPLAY-TABLE", new DbsArrayController(1,9));
        roman_Global_Display_Product = roman_Global_Display_Table.newFieldInGroup("roman_Global_Display_Product", "DISPLAY-PRODUCT", FieldType.STRING, 
            5);
        roman_Global_Display_Count = roman_Global_Display_Table.newFieldArrayInGroup("roman_Global_Display_Count", "DISPLAY-COUNT", FieldType.PACKED_DECIMAL, 
            7, new DbsArrayController(1,9));
        roman_Global_Display_Contra_Count = roman_Global_Display_Table.newFieldArrayInGroup("roman_Global_Display_Contra_Count", "DISPLAY-CONTRA-COUNT", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1,9));
        roman_Global_Display_Error_Flag = roman_Global_Display_Table.newFieldArrayInGroup("roman_Global_Display_Error_Flag", "DISPLAY-ERROR-FLAG", FieldType.STRING, 
            1, new DbsArrayController(1,9));
        roman_Global_Pnd_Cnt_Data = roman_Global.newGroupArrayInGroup("roman_Global_Pnd_Cnt_Data", "#CNT-DATA", new DbsArrayController(1,30));
        roman_Global_Pnd_Cnt_Product_Code = roman_Global_Pnd_Cnt_Data.newFieldInGroup("roman_Global_Pnd_Cnt_Product_Code", "#CNT-PRODUCT-CODE", FieldType.STRING, 
            4);
        roman_Global_Pnd_Cnt_Product_Group = roman_Global_Pnd_Cnt_Data.newFieldInGroup("roman_Global_Pnd_Cnt_Product_Group", "#CNT-PRODUCT-GROUP", FieldType.STRING, 
            4);
        roman_Global_Display_Prev_Contract = roman_Global_Pnd_Cnt_Data.newFieldInGroup("roman_Global_Display_Prev_Contract", "DISPLAY-PREV-CONTRACT", 
            FieldType.STRING, 8);
        roman_Global_Display_Curr_Contract = roman_Global_Pnd_Cnt_Data.newFieldInGroup("roman_Global_Display_Curr_Contract", "DISPLAY-CURR-CONTRACT", 
            FieldType.STRING, 8);
        roman_Global_Display_Nums_Contract = roman_Global_Pnd_Cnt_Data.newFieldInGroup("roman_Global_Display_Nums_Contract", "DISPLAY-NUMS-CONTRACT", 
            FieldType.NUMERIC, 7);
        roman_Global_Pnd_Cnt_Accum_Tally = roman_Global_Pnd_Cnt_Data.newFieldArrayInGroup("roman_Global_Pnd_Cnt_Accum_Tally", "#CNT-ACCUM-TALLY", FieldType.PACKED_DECIMAL, 
            7, new DbsArrayController(1,99));
        roman_Global_Pnd_Cnt_Compare_Tally = roman_Global_Pnd_Cnt_Data.newFieldArrayInGroup("roman_Global_Pnd_Cnt_Compare_Tally", "#CNT-COMPARE-TALLY", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1,99));
        vw_app_Prap_Rule_View.setUniquePeList();

        this.setRecordName("GdaAppg225");
    }
    public void initializeValues() throws Exception
    {
        reset();

        vw_app_Prap_Rule_View.reset();
        pnd_Balancing_Report_Fields_Test_Contract_Min.setInitialValue("99999999");
        pnd_Balancing_Report_Fields_Test_Contract_Max.setInitialValue("A0000000");
        pnd_Balancing_Report_Fields_Num_Tens.setInitialValue(80);
        pnd_Balancing_Report_Fields_Total_D_Sub.setInitialValue(9);
        pnd_Balancing_Report_Fields_Total_A_Sub.setInitialValue(9);
        roman_Global_Pnd_Cnt_Product_Code.getValue(1).setInitialValue("CANP");
        roman_Global_Pnd_Cnt_Product_Code.getValue(2).setInitialValue("CANS");
        roman_Global_Pnd_Cnt_Product_Code.getValue(3).setInitialValue("RAQV");
        roman_Global_Pnd_Cnt_Product_Code.getValue(4).setInitialValue("RA");
        roman_Global_Pnd_Cnt_Product_Code.getValue(5).setInitialValue("GSRA");
        roman_Global_Pnd_Cnt_Product_Code.getValue(6).setInitialValue("IRA");
        roman_Global_Pnd_Cnt_Product_Code.getValue(7).setInitialValue("IRAC");
        roman_Global_Pnd_Cnt_Product_Code.getValue(8).setInitialValue("IRAR");
        roman_Global_Pnd_Cnt_Product_Code.getValue(9).setInitialValue("GRA");
        roman_Global_Pnd_Cnt_Product_Code.getValue(10).setInitialValue("GRA2");
        roman_Global_Pnd_Cnt_Product_Code.getValue(11).setInitialValue("LGA");
        roman_Global_Pnd_Cnt_Product_Code.getValue(12).setInitialValue("SSRA");
        roman_Global_Pnd_Cnt_Product_Code.getValue(13).setInitialValue("SRA");
        roman_Global_Pnd_Cnt_Product_Code.getValue(14).setInitialValue("SRAQ");
        roman_Global_Pnd_Cnt_Product_Code.getValue(15).setInitialValue("KEOG");
        roman_Global_Pnd_Cnt_Product_Code.getValue(16).setInitialValue("GA");
        roman_Global_Pnd_Cnt_Product_Code.getValue(17).setInitialValue("RC");
        roman_Global_Pnd_Cnt_Product_Code.getValue(18).setInitialValue("RCP");
        roman_Global_Pnd_Cnt_Product_Code.getValue(19).setInitialValue("IRAS");
        roman_Global_Pnd_Cnt_Product_Code.getValue(20).setInitialValue("TGA");
        roman_Global_Pnd_Cnt_Product_Code.getValue(21).setInitialValue("RHSP");
        roman_Global_Pnd_Cnt_Product_Code.getValue(22).setInitialValue("IRIC");
        roman_Global_Pnd_Cnt_Product_Code.getValue(23).setInitialValue("IRIR");
        roman_Global_Pnd_Cnt_Product_Code.getValue(24).setInitialValue("IRIS");
        roman_Global_Pnd_Cnt_Product_Group.getValue(1).setInitialValue("RA");
        roman_Global_Pnd_Cnt_Product_Group.getValue(2).setInitialValue("RA");
        roman_Global_Pnd_Cnt_Product_Group.getValue(3).setInitialValue("RA");
        roman_Global_Pnd_Cnt_Product_Group.getValue(4).setInitialValue("RA");
        roman_Global_Pnd_Cnt_Product_Group.getValue(5).setInitialValue("GSRA");
        roman_Global_Pnd_Cnt_Product_Group.getValue(6).setInitialValue("IRA");
        roman_Global_Pnd_Cnt_Product_Group.getValue(7).setInitialValue("IRAC");
        roman_Global_Pnd_Cnt_Product_Group.getValue(8).setInitialValue("IRAR");
        roman_Global_Pnd_Cnt_Product_Group.getValue(9).setInitialValue("GRA");
        roman_Global_Pnd_Cnt_Product_Group.getValue(10).setInitialValue("GRA");
        roman_Global_Pnd_Cnt_Product_Group.getValue(11).setInitialValue("LGA");
        roman_Global_Pnd_Cnt_Product_Group.getValue(12).setInitialValue("SRA");
        roman_Global_Pnd_Cnt_Product_Group.getValue(13).setInitialValue("SRA");
        roman_Global_Pnd_Cnt_Product_Group.getValue(14).setInitialValue("SRA");
        roman_Global_Pnd_Cnt_Product_Group.getValue(15).setInitialValue("KEOG");
        roman_Global_Pnd_Cnt_Product_Group.getValue(16).setInitialValue("GA");
        roman_Global_Pnd_Cnt_Product_Group.getValue(17).setInitialValue("RC");
        roman_Global_Pnd_Cnt_Product_Group.getValue(18).setInitialValue("RCP");
        roman_Global_Pnd_Cnt_Product_Group.getValue(19).setInitialValue("IRAS");
        roman_Global_Pnd_Cnt_Product_Group.getValue(20).setInitialValue("TGA");
        roman_Global_Pnd_Cnt_Product_Group.getValue(21).setInitialValue("RHSP");
        roman_Global_Pnd_Cnt_Product_Group.getValue(22).setInitialValue("IRIC");
        roman_Global_Pnd_Cnt_Product_Group.getValue(23).setInitialValue("IRIR");
        roman_Global_Pnd_Cnt_Product_Group.getValue(24).setInitialValue("IRIS");
    }

    // Constructor
    private GdaAppg225() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }

    // Instance Property
    public static GdaAppg225 getInstance(int callnatLevel) throws Exception
    {
        if (_instance.get() == null)
            _instance.set(new ArrayList<GdaAppg225>());

        if (_instance.get().size() < callnatLevel)
        {
            while (_instance.get().size() < callnatLevel)
            {
                _instance.get().add(new GdaAppg225());
            }
        }
        else if (_instance.get().size() > callnatLevel)
        {
            while(_instance.get().size() > callnatLevel)
            _instance.get().remove(_instance.get().size() - 1);
        }

        return _instance.get().get(callnatLevel - 1);
    }
}

