/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:53 PM
**        * FROM NATURAL LDA     : APPL160
************************************************************
**        * FILE NAME            : LdaAppl160.java
**        * CLASS NAME           : LdaAppl160
**        * INSTANCE NAME        : LdaAppl160
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl160 extends DbsRecord
{
    // Properties
    private DbsField doc_Ext_File;
    private DbsGroup doc_Ext_FileRedef1;
    private DbsField doc_Ext_File_Dx_Type_Cd;
    private DbsField doc_Ext_File_Dx_Contract_Table;
    private DbsGroup doc_Ext_File_Dx_Contract_TableRedef2;
    private DbsGroup doc_Ext_File_Dx_Contract_Numbers;
    private DbsField doc_Ext_File_Dx_Tiaa_Prfx;
    private DbsField doc_Ext_File_Dx_Tiaa_No;
    private DbsField doc_Ext_File_Dx_Cref_Prfx;
    private DbsField doc_Ext_File_Dx_Cref_No;
    private DbsField doc_Ext_File_Dx_Date_Table;
    private DbsField doc_Ext_File_Dx_Age_First_Pymt;
    private DbsField doc_Ext_File_Dx_Ownership;
    private DbsField doc_Ext_File_Dx_Alloc_Disc;
    private DbsField doc_Ext_File_Dx_Currency;
    private DbsField doc_Ext_File_Dx_Tiaa_Form_No;
    private DbsField doc_Ext_File_Dx_Cref_Form_No;
    private DbsField doc_Ext_File_Dx_Tiaa_Ed_No;
    private DbsField doc_Ext_File_Dx_Cref_Ed_No;
    private DbsField doc_Ext_File_Dx_Annuitant_Name;
    private DbsGroup doc_Ext_File_Dx_Eop_Name;
    private DbsField doc_Ext_File_Dx_Eop_Last_Name;
    private DbsField doc_Ext_File_Dx_Eop_First_Name;
    private DbsField doc_Ext_File_Dx_Eop_Middle_Name;
    private DbsGroup doc_Ext_File_Dx_Eop_Middle_NameRedef3;
    private DbsField doc_Ext_File_Dx_Eop_Mid_Init;
    private DbsField doc_Ext_File_Dx_Eop_Mid_Filler;
    private DbsField doc_Ext_File_Dx_Eop_Title;
    private DbsField doc_Ext_File_Dx_Eop_Prefix;
    private DbsField doc_Ext_File_Dx_Soc_Sec;
    private DbsField doc_Ext_File_Dx_Pin_No;
    private DbsField doc_Ext_File_Dx_Address_Table;
    private DbsField doc_Ext_File_Dx_Eop_Zipcode;
    private DbsField doc_Ext_File_Dx_Participant_Status;
    private DbsField doc_Ext_File_Dx_Email_Address;
    private DbsField doc_Ext_File_Dx_Request_Package_Type;
    private DbsField doc_Ext_File_Dx_Portfolio_Selected;
    private DbsField doc_Ext_File_Dx_Divorce_Contract;
    private DbsField doc_Ext_File_Dx_Access_Cd_Ind;
    private DbsField doc_Ext_File_Dx_Address_Change;
    private DbsField doc_Ext_File_Dx_Package_Version;
    private DbsField doc_Ext_File_Dx_Unit_Name;
    private DbsField doc_Ext_File_Dx_Print_Package_Type;
    private DbsField doc_Ext_File_Dx_Future_Filler;
    private DbsField doc_Ext_File_Dx_Bene_Estate;
    private DbsField doc_Ext_File_Dx_Bene_Trust;
    private DbsField doc_Ext_File_Dx_Bene_Category;
    private DbsField doc_Ext_File_Dx_Sex;
    private DbsField doc_Ext_File_Dx_Inst_Name_Table;
    private DbsField doc_Ext_File_Dx_Inst_Name_76;
    private DbsField doc_Ext_File_Dx_Inst_Address_Table;
    private DbsField doc_Ext_File_Dx_Inst_Zip_Cd;
    private DbsField doc_Ext_File_Dx_Mail_Instructions;
    private DbsField doc_Ext_File_Dx_Orig_Iss_State;
    private DbsField doc_Ext_File_Dx_Current_Iss_State;
    private DbsField doc_Ext_File_Dx_Eop_State_Name;
    private DbsField doc_Ext_File_Dx_Lob;
    private DbsField doc_Ext_File_Dx_Lob_Type;
    private DbsField doc_Ext_File_Dx_Inst_Code;
    private DbsField doc_Ext_File_Dx_Inst_Bill_Cd;
    private DbsField doc_Ext_File_Dx_Inst_Permit_Trans;
    private DbsField doc_Ext_File_Dx_Perm_Gra_Trans;
    private DbsField doc_Ext_File_Dx_Inst_Full_Immed_Vest;
    private DbsField doc_Ext_File_Dx_Inst_Cashable_Ra;
    private DbsField doc_Ext_File_Dx_Inst_Cashable_Gra;
    private DbsField doc_Ext_File_Dx_Inst_Fixed_Per_Opt;
    private DbsField doc_Ext_File_Dx_Inst_Cntrl_Consent;
    private DbsField doc_Ext_File_Dx_Eop;
    private DbsField doc_Ext_File_Dx_Gsra_Inst_St_Code;
    private DbsField doc_Ext_File_Dx_Region_Cd;
    private DbsField doc_Ext_File_Dx_Staff_Id;
    private DbsField doc_Ext_File_Dx_Appl_Source;
    private DbsField doc_Ext_File_Dx_Eop_Appl_Status;
    private DbsField doc_Ext_File_Dx_Eop_Addl_Cref_Req;
    private DbsField doc_Ext_File_Dx_Extract_Date;
    private DbsField doc_Ext_File_Dx_Gsra_Loan_Ind;
    private DbsField doc_Ext_File_Dx_Spec_Ppg_Ind;
    private DbsField doc_Ext_File_Dx_Mail_Pull_Cd;
    private DbsField doc_Ext_File_Dx_Mit_Log_Dt_Tm;
    private DbsField doc_Ext_File_Dx_Addr_Page_Name;
    private DbsGroup doc_Ext_File_Dx_Addr_Page_NameRedef4;
    private DbsField doc_Ext_File_Dx_Addr_Pref;
    private DbsField doc_Ext_File_Dx_Addr_Filler_1;
    private DbsField doc_Ext_File_Dx_Addr_Frst;
    private DbsField doc_Ext_File_Dx_Addr_Filler_2;
    private DbsField doc_Ext_File_Dx_Addr_Mddle;
    private DbsField doc_Ext_File_Dx_Addr_Filler_3;
    private DbsField doc_Ext_File_Dx_Addr_Last;
    private DbsField doc_Ext_File_Dx_Addr_Filler_4;
    private DbsField doc_Ext_File_Dx_Addr_Sffx;
    private DbsField doc_Ext_File_Dx_Welcome_Name;
    private DbsGroup doc_Ext_File_Dx_Welcome_NameRedef5;
    private DbsField doc_Ext_File_Dx_Welcome_Pref;
    private DbsField doc_Ext_File_Dx_Welcome_Filler_1;
    private DbsField doc_Ext_File_Dx_Welcome_Last;
    private DbsField doc_Ext_File_Dx_Pin_Name;
    private DbsGroup doc_Ext_File_Dx_Pin_NameRedef6;
    private DbsField doc_Ext_File_Dx_Pin_First;
    private DbsField doc_Ext_File_Dx_Pin_Filler_1;
    private DbsField doc_Ext_File_Dx_Pin_Mddle;
    private DbsField doc_Ext_File_Dx_Pin_Filler_2;
    private DbsField doc_Ext_File_Dx_Pin_Last;
    private DbsField doc_Ext_File_Dx_Zip;
    private DbsGroup doc_Ext_File_Dx_ZipRedef7;
    private DbsField doc_Ext_File_Dx_Zip_Plus4;
    private DbsField doc_Ext_File_Dx_Zip_Plus1;
    private DbsField doc_Ext_File_Dx_Suspension;
    private DbsField doc_Ext_File_Dx_Mult_App_Version;
    private DbsField doc_Ext_File_Dx_Mult_App_Status;
    private DbsField doc_Ext_File_Dx_Mult_App_Lob;
    private DbsField doc_Ext_File_Dx_Mult_App_Lob_Type;
    private DbsField doc_Ext_File_Dx_Mult_App_Ppg;
    private DbsField doc_Ext_File_Dx_K12_Ppg;
    private DbsField doc_Ext_File_Dx_Ira_Rollover_Type;
    private DbsField doc_Ext_File_Dx_Ira_Record_Type;
    private DbsField doc_Ext_File_Dx_Rollover_Amt;
    private DbsField doc_Ext_File_Dx_Mit_Unit;
    private DbsField doc_Ext_File_Dx_Mit_Wpid;
    private DbsField doc_Ext_File_Dx_New_Bene_Ind;
    private DbsField doc_Ext_File_Dx_Irc_Sectn_Cde;
    private DbsField doc_Ext_File_Dx_Released_Dt;
    private DbsField doc_Ext_File_Dx_Irc_Sectn_Grp;
    private DbsField doc_Ext_File_Dx_Enroll_Request_Type;
    private DbsField doc_Ext_File_Dx_Erisa_Inst;
    private DbsField doc_Ext_File_Dx_Reprint_Request_Type;
    private DbsField doc_Ext_File_Dx_Print_Destination;
    private DbsField doc_Ext_File_Dx_Correction_Type;
    private DbsField doc_Ext_File_Dx_Processor_Name;
    private DbsField doc_Ext_File_Dx_Mail_Date;
    private DbsField doc_Ext_File_Dx_Alloc_Fmt;
    private DbsField doc_Ext_File_Dx_Phone_No;
    private DbsField doc_Ext_File_Dx_Fund_Cde;
    private DbsField doc_Ext_File_Dx_Alloc_Pct;
    private DbsField doc_Ext_File_Dx_Oia_Ind;
    private DbsField doc_Ext_File_Dx_Product_Cde;
    private DbsField doc_Ext_File_Dx_Acct_Sum_Sheet_Type;
    private DbsField doc_Ext_File_Dx_Sg_Plan_No;
    private DbsField doc_Ext_File_Dx_Sg_Subplan_No;
    private DbsField doc_Ext_File_Dx_Sg_Subplan_Type;
    private DbsField doc_Ext_File_Dx_Omni_Issue_Ind;
    private DbsField doc_Ext_File_Dx_Effective_Date;
    private DbsField doc_Ext_File_Dx_Employer_Name;
    private DbsField doc_Ext_File_Dx_Tiaa_Rate;
    private DbsField doc_Ext_File_Dx_Sg_Plan_Id;
    private DbsField doc_Ext_File_Dx_Tiaa_Pg4_Numb;
    private DbsField doc_Ext_File_Dx_Cref_Pg4_Numb;
    private DbsField doc_Ext_File_Dx_Dflt_Access_Code;
    private DbsField doc_Ext_File_Dx_Single_Issue_Ind;
    private DbsField doc_Ext_File_Dx_Spec_Fund_Ind;
    private DbsField doc_Ext_File_Dx_Product_Price_Level;
    private DbsField doc_Ext_File_Dx_Roth_Ind;
    private DbsField doc_Ext_File_Dx_Plan_Issue_State;
    private DbsField doc_Ext_File_Dx_Sgrd_Client_Id;
    private DbsField doc_Ext_File_Dx_Portfolio_Type;
    private DbsField doc_Ext_File_Dx_Auto_Enroll_Ind;
    private DbsField doc_Ext_File_Dx_Auto_Save_Ind;
    private DbsField doc_Ext_File_Dx_As_Cur_Dflt_Amt;
    private DbsField doc_Ext_File_Dx_As_Incr_Amt;
    private DbsField doc_Ext_File_Dx_As_Max_Pct;
    private DbsField doc_Ext_File_Dx_Ae_Opt_Out_Days;
    private DbsField doc_Ext_File_Dx_Tsv_Ind;
    private DbsField doc_Ext_File_Dx_Tiaa_Indx_Guarntd_Rte;
    private DbsField doc_Ext_File_Dx_Agent_Crd_No;
    private DbsField doc_Ext_File_Dx_Tic_Ind;
    private DbsField doc_Ext_File_Dx_Tic_Startdate;
    private DbsField doc_Ext_File_Dx_Tic_Enddate;
    private DbsField doc_Ext_File_Dx_Tic_Percentage;
    private DbsField doc_Ext_File_Dx_Tic_Postdays;
    private DbsField doc_Ext_File_Dx_Tic_Limit;
    private DbsField doc_Ext_File_Dx_Tic_Postfreq;
    private DbsField doc_Ext_File_Dx_Tic_Pl_Level;
    private DbsField doc_Ext_File_Dx_Tic_Windowdays;
    private DbsField doc_Ext_File_Dx_Tic_Reqdlywindow;
    private DbsField doc_Ext_File_Dx_Tic_Recap_Prov;
    private DbsField doc_Ext_File_Dx_Ecs_Dcs_E_Dlvry_Ind;
    private DbsField doc_Ext_File_Dx_Ecs_Dcs_Annty_Optn_Ind;
    private DbsField doc_Ext_File_Dx_Full_Middle_Name;
    private DbsField doc_Ext_File_Dx_Substitution_Contract_Ind;
    private DbsField doc_Ext_File_Dx_Non_Proprietary_Pkg_Ind;
    private DbsField doc_Ext_File_Dx_Multi_Plan_Table;
    private DbsGroup doc_Ext_File_Dx_Multi_Plan_TableRedef8;
    private DbsGroup doc_Ext_File_Dx_Multi_Plan_Info;
    private DbsField doc_Ext_File_Dx_Multi_Plan_No;
    private DbsField doc_Ext_File_Dx_Multi_Sub_Plan;
    private DbsField doc_Ext_File_Dx_Tiaa_Init_Prem;
    private DbsField doc_Ext_File_Dx_Cref_Init_Prem;
    private DbsField doc_Ext_File_Dx_Filler;
    private DbsField doc_Ext_File_Dx_Related_Contract_Info;
    private DbsGroup doc_Ext_File_Dx_Related_Contract_InfoRedef9;
    private DbsGroup doc_Ext_File_Dx_Related_Contract;
    private DbsField doc_Ext_File_Dx_Related_Contract_Type;
    private DbsField doc_Ext_File_Dx_Related_Tiaa_No;
    private DbsField doc_Ext_File_Dx_Related_Tiaa_Issue_Date;
    private DbsField doc_Ext_File_Dx_Related_First_Payment_Date;
    private DbsField doc_Ext_File_Dx_Related_Tiaa_Total_Amt;
    private DbsField doc_Ext_File_Dx_Related_Last_Payment_Date;
    private DbsField doc_Ext_File_Dx_Related_Payment_Frequency;
    private DbsField doc_Ext_File_Dx_Related_Tiaa_Issue_State;
    private DbsField doc_Ext_File_Dx_First_Tpa_Ipro_Ind;
    private DbsField doc_Ext_File_Dx_Fund_Source_Cde_1;
    private DbsField doc_Ext_File_Dx_Fund_Source_Cde_2;
    private DbsField doc_Ext_File_Dx_Fund_Cde_2;
    private DbsField doc_Ext_File_Dx_Alloc_Pct_2;
    private DbsGroup doc_Ext_FileRedef10;
    private DbsField doc_Ext_File_Doc_Ext_Data_1;
    private DbsField doc_Ext_File_Doc_Ext_Data_2;
    private DbsField doc_Ext_File_Doc_Ext_Data_3;
    private DbsField doc_Ext_File_Doc_Ext_Data_4;
    private DbsField doc_Ext_File_Doc_Ext_Data_5;
    private DbsField doc_Ext_File_Doc_Ext_Data_6;
    private DbsField doc_Ext_File_Doc_Ext_Data_7;
    private DbsField doc_Ext_File_Doc_Ext_Data_8;
    private DbsField doc_Ext_File_Doc_Ext_Data_9;
    private DbsField doc_Ext_File_Doc_Ext_Data_10;
    private DbsField doc_Ext_File_Doc_Ext_Data_11;
    private DbsField doc_Ext_File_Doc_Ext_Data_12;
    private DbsField doc_Ext_File_Doc_Ext_Data_13;
    private DbsField doc_Ext_File_Doc_Ext_Data_14;
    private DbsField doc_Ext_File_Doc_Ext_Data_15;
    private DbsField doc_Ext_File_Doc_Ext_Data_16;
    private DbsField doc_Ext_File_Doc_Ext_Data_17;
    private DbsField doc_Ext_File_Doc_Ext_Data_18;
    private DbsField doc_Ext_File_Doc_Ext_Data_19;
    private DbsField doc_Ext_File_Doc_Ext_Data_20;
    private DbsField doc_Ext_File_Doc_Ext_Data_21;
    private DbsField doc_Ext_File_Doc_Ext_Data_22;
    private DbsField doc_Ext_File_Doc_Ext_Data_23;
    private DbsField doc_Ext_File_Doc_Ext_Data_24;
    private DbsField doc_Ext_File_Doc_Ext_Data_25;
    private DbsField doc_Ext_File_Doc_Ext_Data_26;

    public DbsField getDoc_Ext_File() { return doc_Ext_File; }

    public DbsGroup getDoc_Ext_FileRedef1() { return doc_Ext_FileRedef1; }

    public DbsField getDoc_Ext_File_Dx_Type_Cd() { return doc_Ext_File_Dx_Type_Cd; }

    public DbsField getDoc_Ext_File_Dx_Contract_Table() { return doc_Ext_File_Dx_Contract_Table; }

    public DbsGroup getDoc_Ext_File_Dx_Contract_TableRedef2() { return doc_Ext_File_Dx_Contract_TableRedef2; }

    public DbsGroup getDoc_Ext_File_Dx_Contract_Numbers() { return doc_Ext_File_Dx_Contract_Numbers; }

    public DbsField getDoc_Ext_File_Dx_Tiaa_Prfx() { return doc_Ext_File_Dx_Tiaa_Prfx; }

    public DbsField getDoc_Ext_File_Dx_Tiaa_No() { return doc_Ext_File_Dx_Tiaa_No; }

    public DbsField getDoc_Ext_File_Dx_Cref_Prfx() { return doc_Ext_File_Dx_Cref_Prfx; }

    public DbsField getDoc_Ext_File_Dx_Cref_No() { return doc_Ext_File_Dx_Cref_No; }

    public DbsField getDoc_Ext_File_Dx_Date_Table() { return doc_Ext_File_Dx_Date_Table; }

    public DbsField getDoc_Ext_File_Dx_Age_First_Pymt() { return doc_Ext_File_Dx_Age_First_Pymt; }

    public DbsField getDoc_Ext_File_Dx_Ownership() { return doc_Ext_File_Dx_Ownership; }

    public DbsField getDoc_Ext_File_Dx_Alloc_Disc() { return doc_Ext_File_Dx_Alloc_Disc; }

    public DbsField getDoc_Ext_File_Dx_Currency() { return doc_Ext_File_Dx_Currency; }

    public DbsField getDoc_Ext_File_Dx_Tiaa_Form_No() { return doc_Ext_File_Dx_Tiaa_Form_No; }

    public DbsField getDoc_Ext_File_Dx_Cref_Form_No() { return doc_Ext_File_Dx_Cref_Form_No; }

    public DbsField getDoc_Ext_File_Dx_Tiaa_Ed_No() { return doc_Ext_File_Dx_Tiaa_Ed_No; }

    public DbsField getDoc_Ext_File_Dx_Cref_Ed_No() { return doc_Ext_File_Dx_Cref_Ed_No; }

    public DbsField getDoc_Ext_File_Dx_Annuitant_Name() { return doc_Ext_File_Dx_Annuitant_Name; }

    public DbsGroup getDoc_Ext_File_Dx_Eop_Name() { return doc_Ext_File_Dx_Eop_Name; }

    public DbsField getDoc_Ext_File_Dx_Eop_Last_Name() { return doc_Ext_File_Dx_Eop_Last_Name; }

    public DbsField getDoc_Ext_File_Dx_Eop_First_Name() { return doc_Ext_File_Dx_Eop_First_Name; }

    public DbsField getDoc_Ext_File_Dx_Eop_Middle_Name() { return doc_Ext_File_Dx_Eop_Middle_Name; }

    public DbsGroup getDoc_Ext_File_Dx_Eop_Middle_NameRedef3() { return doc_Ext_File_Dx_Eop_Middle_NameRedef3; }

    public DbsField getDoc_Ext_File_Dx_Eop_Mid_Init() { return doc_Ext_File_Dx_Eop_Mid_Init; }

    public DbsField getDoc_Ext_File_Dx_Eop_Mid_Filler() { return doc_Ext_File_Dx_Eop_Mid_Filler; }

    public DbsField getDoc_Ext_File_Dx_Eop_Title() { return doc_Ext_File_Dx_Eop_Title; }

    public DbsField getDoc_Ext_File_Dx_Eop_Prefix() { return doc_Ext_File_Dx_Eop_Prefix; }

    public DbsField getDoc_Ext_File_Dx_Soc_Sec() { return doc_Ext_File_Dx_Soc_Sec; }

    public DbsField getDoc_Ext_File_Dx_Pin_No() { return doc_Ext_File_Dx_Pin_No; }

    public DbsField getDoc_Ext_File_Dx_Address_Table() { return doc_Ext_File_Dx_Address_Table; }

    public DbsField getDoc_Ext_File_Dx_Eop_Zipcode() { return doc_Ext_File_Dx_Eop_Zipcode; }

    public DbsField getDoc_Ext_File_Dx_Participant_Status() { return doc_Ext_File_Dx_Participant_Status; }

    public DbsField getDoc_Ext_File_Dx_Email_Address() { return doc_Ext_File_Dx_Email_Address; }

    public DbsField getDoc_Ext_File_Dx_Request_Package_Type() { return doc_Ext_File_Dx_Request_Package_Type; }

    public DbsField getDoc_Ext_File_Dx_Portfolio_Selected() { return doc_Ext_File_Dx_Portfolio_Selected; }

    public DbsField getDoc_Ext_File_Dx_Divorce_Contract() { return doc_Ext_File_Dx_Divorce_Contract; }

    public DbsField getDoc_Ext_File_Dx_Access_Cd_Ind() { return doc_Ext_File_Dx_Access_Cd_Ind; }

    public DbsField getDoc_Ext_File_Dx_Address_Change() { return doc_Ext_File_Dx_Address_Change; }

    public DbsField getDoc_Ext_File_Dx_Package_Version() { return doc_Ext_File_Dx_Package_Version; }

    public DbsField getDoc_Ext_File_Dx_Unit_Name() { return doc_Ext_File_Dx_Unit_Name; }

    public DbsField getDoc_Ext_File_Dx_Print_Package_Type() { return doc_Ext_File_Dx_Print_Package_Type; }

    public DbsField getDoc_Ext_File_Dx_Future_Filler() { return doc_Ext_File_Dx_Future_Filler; }

    public DbsField getDoc_Ext_File_Dx_Bene_Estate() { return doc_Ext_File_Dx_Bene_Estate; }

    public DbsField getDoc_Ext_File_Dx_Bene_Trust() { return doc_Ext_File_Dx_Bene_Trust; }

    public DbsField getDoc_Ext_File_Dx_Bene_Category() { return doc_Ext_File_Dx_Bene_Category; }

    public DbsField getDoc_Ext_File_Dx_Sex() { return doc_Ext_File_Dx_Sex; }

    public DbsField getDoc_Ext_File_Dx_Inst_Name_Table() { return doc_Ext_File_Dx_Inst_Name_Table; }

    public DbsField getDoc_Ext_File_Dx_Inst_Name_76() { return doc_Ext_File_Dx_Inst_Name_76; }

    public DbsField getDoc_Ext_File_Dx_Inst_Address_Table() { return doc_Ext_File_Dx_Inst_Address_Table; }

    public DbsField getDoc_Ext_File_Dx_Inst_Zip_Cd() { return doc_Ext_File_Dx_Inst_Zip_Cd; }

    public DbsField getDoc_Ext_File_Dx_Mail_Instructions() { return doc_Ext_File_Dx_Mail_Instructions; }

    public DbsField getDoc_Ext_File_Dx_Orig_Iss_State() { return doc_Ext_File_Dx_Orig_Iss_State; }

    public DbsField getDoc_Ext_File_Dx_Current_Iss_State() { return doc_Ext_File_Dx_Current_Iss_State; }

    public DbsField getDoc_Ext_File_Dx_Eop_State_Name() { return doc_Ext_File_Dx_Eop_State_Name; }

    public DbsField getDoc_Ext_File_Dx_Lob() { return doc_Ext_File_Dx_Lob; }

    public DbsField getDoc_Ext_File_Dx_Lob_Type() { return doc_Ext_File_Dx_Lob_Type; }

    public DbsField getDoc_Ext_File_Dx_Inst_Code() { return doc_Ext_File_Dx_Inst_Code; }

    public DbsField getDoc_Ext_File_Dx_Inst_Bill_Cd() { return doc_Ext_File_Dx_Inst_Bill_Cd; }

    public DbsField getDoc_Ext_File_Dx_Inst_Permit_Trans() { return doc_Ext_File_Dx_Inst_Permit_Trans; }

    public DbsField getDoc_Ext_File_Dx_Perm_Gra_Trans() { return doc_Ext_File_Dx_Perm_Gra_Trans; }

    public DbsField getDoc_Ext_File_Dx_Inst_Full_Immed_Vest() { return doc_Ext_File_Dx_Inst_Full_Immed_Vest; }

    public DbsField getDoc_Ext_File_Dx_Inst_Cashable_Ra() { return doc_Ext_File_Dx_Inst_Cashable_Ra; }

    public DbsField getDoc_Ext_File_Dx_Inst_Cashable_Gra() { return doc_Ext_File_Dx_Inst_Cashable_Gra; }

    public DbsField getDoc_Ext_File_Dx_Inst_Fixed_Per_Opt() { return doc_Ext_File_Dx_Inst_Fixed_Per_Opt; }

    public DbsField getDoc_Ext_File_Dx_Inst_Cntrl_Consent() { return doc_Ext_File_Dx_Inst_Cntrl_Consent; }

    public DbsField getDoc_Ext_File_Dx_Eop() { return doc_Ext_File_Dx_Eop; }

    public DbsField getDoc_Ext_File_Dx_Gsra_Inst_St_Code() { return doc_Ext_File_Dx_Gsra_Inst_St_Code; }

    public DbsField getDoc_Ext_File_Dx_Region_Cd() { return doc_Ext_File_Dx_Region_Cd; }

    public DbsField getDoc_Ext_File_Dx_Staff_Id() { return doc_Ext_File_Dx_Staff_Id; }

    public DbsField getDoc_Ext_File_Dx_Appl_Source() { return doc_Ext_File_Dx_Appl_Source; }

    public DbsField getDoc_Ext_File_Dx_Eop_Appl_Status() { return doc_Ext_File_Dx_Eop_Appl_Status; }

    public DbsField getDoc_Ext_File_Dx_Eop_Addl_Cref_Req() { return doc_Ext_File_Dx_Eop_Addl_Cref_Req; }

    public DbsField getDoc_Ext_File_Dx_Extract_Date() { return doc_Ext_File_Dx_Extract_Date; }

    public DbsField getDoc_Ext_File_Dx_Gsra_Loan_Ind() { return doc_Ext_File_Dx_Gsra_Loan_Ind; }

    public DbsField getDoc_Ext_File_Dx_Spec_Ppg_Ind() { return doc_Ext_File_Dx_Spec_Ppg_Ind; }

    public DbsField getDoc_Ext_File_Dx_Mail_Pull_Cd() { return doc_Ext_File_Dx_Mail_Pull_Cd; }

    public DbsField getDoc_Ext_File_Dx_Mit_Log_Dt_Tm() { return doc_Ext_File_Dx_Mit_Log_Dt_Tm; }

    public DbsField getDoc_Ext_File_Dx_Addr_Page_Name() { return doc_Ext_File_Dx_Addr_Page_Name; }

    public DbsGroup getDoc_Ext_File_Dx_Addr_Page_NameRedef4() { return doc_Ext_File_Dx_Addr_Page_NameRedef4; }

    public DbsField getDoc_Ext_File_Dx_Addr_Pref() { return doc_Ext_File_Dx_Addr_Pref; }

    public DbsField getDoc_Ext_File_Dx_Addr_Filler_1() { return doc_Ext_File_Dx_Addr_Filler_1; }

    public DbsField getDoc_Ext_File_Dx_Addr_Frst() { return doc_Ext_File_Dx_Addr_Frst; }

    public DbsField getDoc_Ext_File_Dx_Addr_Filler_2() { return doc_Ext_File_Dx_Addr_Filler_2; }

    public DbsField getDoc_Ext_File_Dx_Addr_Mddle() { return doc_Ext_File_Dx_Addr_Mddle; }

    public DbsField getDoc_Ext_File_Dx_Addr_Filler_3() { return doc_Ext_File_Dx_Addr_Filler_3; }

    public DbsField getDoc_Ext_File_Dx_Addr_Last() { return doc_Ext_File_Dx_Addr_Last; }

    public DbsField getDoc_Ext_File_Dx_Addr_Filler_4() { return doc_Ext_File_Dx_Addr_Filler_4; }

    public DbsField getDoc_Ext_File_Dx_Addr_Sffx() { return doc_Ext_File_Dx_Addr_Sffx; }

    public DbsField getDoc_Ext_File_Dx_Welcome_Name() { return doc_Ext_File_Dx_Welcome_Name; }

    public DbsGroup getDoc_Ext_File_Dx_Welcome_NameRedef5() { return doc_Ext_File_Dx_Welcome_NameRedef5; }

    public DbsField getDoc_Ext_File_Dx_Welcome_Pref() { return doc_Ext_File_Dx_Welcome_Pref; }

    public DbsField getDoc_Ext_File_Dx_Welcome_Filler_1() { return doc_Ext_File_Dx_Welcome_Filler_1; }

    public DbsField getDoc_Ext_File_Dx_Welcome_Last() { return doc_Ext_File_Dx_Welcome_Last; }

    public DbsField getDoc_Ext_File_Dx_Pin_Name() { return doc_Ext_File_Dx_Pin_Name; }

    public DbsGroup getDoc_Ext_File_Dx_Pin_NameRedef6() { return doc_Ext_File_Dx_Pin_NameRedef6; }

    public DbsField getDoc_Ext_File_Dx_Pin_First() { return doc_Ext_File_Dx_Pin_First; }

    public DbsField getDoc_Ext_File_Dx_Pin_Filler_1() { return doc_Ext_File_Dx_Pin_Filler_1; }

    public DbsField getDoc_Ext_File_Dx_Pin_Mddle() { return doc_Ext_File_Dx_Pin_Mddle; }

    public DbsField getDoc_Ext_File_Dx_Pin_Filler_2() { return doc_Ext_File_Dx_Pin_Filler_2; }

    public DbsField getDoc_Ext_File_Dx_Pin_Last() { return doc_Ext_File_Dx_Pin_Last; }

    public DbsField getDoc_Ext_File_Dx_Zip() { return doc_Ext_File_Dx_Zip; }

    public DbsGroup getDoc_Ext_File_Dx_ZipRedef7() { return doc_Ext_File_Dx_ZipRedef7; }

    public DbsField getDoc_Ext_File_Dx_Zip_Plus4() { return doc_Ext_File_Dx_Zip_Plus4; }

    public DbsField getDoc_Ext_File_Dx_Zip_Plus1() { return doc_Ext_File_Dx_Zip_Plus1; }

    public DbsField getDoc_Ext_File_Dx_Suspension() { return doc_Ext_File_Dx_Suspension; }

    public DbsField getDoc_Ext_File_Dx_Mult_App_Version() { return doc_Ext_File_Dx_Mult_App_Version; }

    public DbsField getDoc_Ext_File_Dx_Mult_App_Status() { return doc_Ext_File_Dx_Mult_App_Status; }

    public DbsField getDoc_Ext_File_Dx_Mult_App_Lob() { return doc_Ext_File_Dx_Mult_App_Lob; }

    public DbsField getDoc_Ext_File_Dx_Mult_App_Lob_Type() { return doc_Ext_File_Dx_Mult_App_Lob_Type; }

    public DbsField getDoc_Ext_File_Dx_Mult_App_Ppg() { return doc_Ext_File_Dx_Mult_App_Ppg; }

    public DbsField getDoc_Ext_File_Dx_K12_Ppg() { return doc_Ext_File_Dx_K12_Ppg; }

    public DbsField getDoc_Ext_File_Dx_Ira_Rollover_Type() { return doc_Ext_File_Dx_Ira_Rollover_Type; }

    public DbsField getDoc_Ext_File_Dx_Ira_Record_Type() { return doc_Ext_File_Dx_Ira_Record_Type; }

    public DbsField getDoc_Ext_File_Dx_Rollover_Amt() { return doc_Ext_File_Dx_Rollover_Amt; }

    public DbsField getDoc_Ext_File_Dx_Mit_Unit() { return doc_Ext_File_Dx_Mit_Unit; }

    public DbsField getDoc_Ext_File_Dx_Mit_Wpid() { return doc_Ext_File_Dx_Mit_Wpid; }

    public DbsField getDoc_Ext_File_Dx_New_Bene_Ind() { return doc_Ext_File_Dx_New_Bene_Ind; }

    public DbsField getDoc_Ext_File_Dx_Irc_Sectn_Cde() { return doc_Ext_File_Dx_Irc_Sectn_Cde; }

    public DbsField getDoc_Ext_File_Dx_Released_Dt() { return doc_Ext_File_Dx_Released_Dt; }

    public DbsField getDoc_Ext_File_Dx_Irc_Sectn_Grp() { return doc_Ext_File_Dx_Irc_Sectn_Grp; }

    public DbsField getDoc_Ext_File_Dx_Enroll_Request_Type() { return doc_Ext_File_Dx_Enroll_Request_Type; }

    public DbsField getDoc_Ext_File_Dx_Erisa_Inst() { return doc_Ext_File_Dx_Erisa_Inst; }

    public DbsField getDoc_Ext_File_Dx_Reprint_Request_Type() { return doc_Ext_File_Dx_Reprint_Request_Type; }

    public DbsField getDoc_Ext_File_Dx_Print_Destination() { return doc_Ext_File_Dx_Print_Destination; }

    public DbsField getDoc_Ext_File_Dx_Correction_Type() { return doc_Ext_File_Dx_Correction_Type; }

    public DbsField getDoc_Ext_File_Dx_Processor_Name() { return doc_Ext_File_Dx_Processor_Name; }

    public DbsField getDoc_Ext_File_Dx_Mail_Date() { return doc_Ext_File_Dx_Mail_Date; }

    public DbsField getDoc_Ext_File_Dx_Alloc_Fmt() { return doc_Ext_File_Dx_Alloc_Fmt; }

    public DbsField getDoc_Ext_File_Dx_Phone_No() { return doc_Ext_File_Dx_Phone_No; }

    public DbsField getDoc_Ext_File_Dx_Fund_Cde() { return doc_Ext_File_Dx_Fund_Cde; }

    public DbsField getDoc_Ext_File_Dx_Alloc_Pct() { return doc_Ext_File_Dx_Alloc_Pct; }

    public DbsField getDoc_Ext_File_Dx_Oia_Ind() { return doc_Ext_File_Dx_Oia_Ind; }

    public DbsField getDoc_Ext_File_Dx_Product_Cde() { return doc_Ext_File_Dx_Product_Cde; }

    public DbsField getDoc_Ext_File_Dx_Acct_Sum_Sheet_Type() { return doc_Ext_File_Dx_Acct_Sum_Sheet_Type; }

    public DbsField getDoc_Ext_File_Dx_Sg_Plan_No() { return doc_Ext_File_Dx_Sg_Plan_No; }

    public DbsField getDoc_Ext_File_Dx_Sg_Subplan_No() { return doc_Ext_File_Dx_Sg_Subplan_No; }

    public DbsField getDoc_Ext_File_Dx_Sg_Subplan_Type() { return doc_Ext_File_Dx_Sg_Subplan_Type; }

    public DbsField getDoc_Ext_File_Dx_Omni_Issue_Ind() { return doc_Ext_File_Dx_Omni_Issue_Ind; }

    public DbsField getDoc_Ext_File_Dx_Effective_Date() { return doc_Ext_File_Dx_Effective_Date; }

    public DbsField getDoc_Ext_File_Dx_Employer_Name() { return doc_Ext_File_Dx_Employer_Name; }

    public DbsField getDoc_Ext_File_Dx_Tiaa_Rate() { return doc_Ext_File_Dx_Tiaa_Rate; }

    public DbsField getDoc_Ext_File_Dx_Sg_Plan_Id() { return doc_Ext_File_Dx_Sg_Plan_Id; }

    public DbsField getDoc_Ext_File_Dx_Tiaa_Pg4_Numb() { return doc_Ext_File_Dx_Tiaa_Pg4_Numb; }

    public DbsField getDoc_Ext_File_Dx_Cref_Pg4_Numb() { return doc_Ext_File_Dx_Cref_Pg4_Numb; }

    public DbsField getDoc_Ext_File_Dx_Dflt_Access_Code() { return doc_Ext_File_Dx_Dflt_Access_Code; }

    public DbsField getDoc_Ext_File_Dx_Single_Issue_Ind() { return doc_Ext_File_Dx_Single_Issue_Ind; }

    public DbsField getDoc_Ext_File_Dx_Spec_Fund_Ind() { return doc_Ext_File_Dx_Spec_Fund_Ind; }

    public DbsField getDoc_Ext_File_Dx_Product_Price_Level() { return doc_Ext_File_Dx_Product_Price_Level; }

    public DbsField getDoc_Ext_File_Dx_Roth_Ind() { return doc_Ext_File_Dx_Roth_Ind; }

    public DbsField getDoc_Ext_File_Dx_Plan_Issue_State() { return doc_Ext_File_Dx_Plan_Issue_State; }

    public DbsField getDoc_Ext_File_Dx_Sgrd_Client_Id() { return doc_Ext_File_Dx_Sgrd_Client_Id; }

    public DbsField getDoc_Ext_File_Dx_Portfolio_Type() { return doc_Ext_File_Dx_Portfolio_Type; }

    public DbsField getDoc_Ext_File_Dx_Auto_Enroll_Ind() { return doc_Ext_File_Dx_Auto_Enroll_Ind; }

    public DbsField getDoc_Ext_File_Dx_Auto_Save_Ind() { return doc_Ext_File_Dx_Auto_Save_Ind; }

    public DbsField getDoc_Ext_File_Dx_As_Cur_Dflt_Amt() { return doc_Ext_File_Dx_As_Cur_Dflt_Amt; }

    public DbsField getDoc_Ext_File_Dx_As_Incr_Amt() { return doc_Ext_File_Dx_As_Incr_Amt; }

    public DbsField getDoc_Ext_File_Dx_As_Max_Pct() { return doc_Ext_File_Dx_As_Max_Pct; }

    public DbsField getDoc_Ext_File_Dx_Ae_Opt_Out_Days() { return doc_Ext_File_Dx_Ae_Opt_Out_Days; }

    public DbsField getDoc_Ext_File_Dx_Tsv_Ind() { return doc_Ext_File_Dx_Tsv_Ind; }

    public DbsField getDoc_Ext_File_Dx_Tiaa_Indx_Guarntd_Rte() { return doc_Ext_File_Dx_Tiaa_Indx_Guarntd_Rte; }

    public DbsField getDoc_Ext_File_Dx_Agent_Crd_No() { return doc_Ext_File_Dx_Agent_Crd_No; }

    public DbsField getDoc_Ext_File_Dx_Tic_Ind() { return doc_Ext_File_Dx_Tic_Ind; }

    public DbsField getDoc_Ext_File_Dx_Tic_Startdate() { return doc_Ext_File_Dx_Tic_Startdate; }

    public DbsField getDoc_Ext_File_Dx_Tic_Enddate() { return doc_Ext_File_Dx_Tic_Enddate; }

    public DbsField getDoc_Ext_File_Dx_Tic_Percentage() { return doc_Ext_File_Dx_Tic_Percentage; }

    public DbsField getDoc_Ext_File_Dx_Tic_Postdays() { return doc_Ext_File_Dx_Tic_Postdays; }

    public DbsField getDoc_Ext_File_Dx_Tic_Limit() { return doc_Ext_File_Dx_Tic_Limit; }

    public DbsField getDoc_Ext_File_Dx_Tic_Postfreq() { return doc_Ext_File_Dx_Tic_Postfreq; }

    public DbsField getDoc_Ext_File_Dx_Tic_Pl_Level() { return doc_Ext_File_Dx_Tic_Pl_Level; }

    public DbsField getDoc_Ext_File_Dx_Tic_Windowdays() { return doc_Ext_File_Dx_Tic_Windowdays; }

    public DbsField getDoc_Ext_File_Dx_Tic_Reqdlywindow() { return doc_Ext_File_Dx_Tic_Reqdlywindow; }

    public DbsField getDoc_Ext_File_Dx_Tic_Recap_Prov() { return doc_Ext_File_Dx_Tic_Recap_Prov; }

    public DbsField getDoc_Ext_File_Dx_Ecs_Dcs_E_Dlvry_Ind() { return doc_Ext_File_Dx_Ecs_Dcs_E_Dlvry_Ind; }

    public DbsField getDoc_Ext_File_Dx_Ecs_Dcs_Annty_Optn_Ind() { return doc_Ext_File_Dx_Ecs_Dcs_Annty_Optn_Ind; }

    public DbsField getDoc_Ext_File_Dx_Full_Middle_Name() { return doc_Ext_File_Dx_Full_Middle_Name; }

    public DbsField getDoc_Ext_File_Dx_Substitution_Contract_Ind() { return doc_Ext_File_Dx_Substitution_Contract_Ind; }

    public DbsField getDoc_Ext_File_Dx_Non_Proprietary_Pkg_Ind() { return doc_Ext_File_Dx_Non_Proprietary_Pkg_Ind; }

    public DbsField getDoc_Ext_File_Dx_Multi_Plan_Table() { return doc_Ext_File_Dx_Multi_Plan_Table; }

    public DbsGroup getDoc_Ext_File_Dx_Multi_Plan_TableRedef8() { return doc_Ext_File_Dx_Multi_Plan_TableRedef8; }

    public DbsGroup getDoc_Ext_File_Dx_Multi_Plan_Info() { return doc_Ext_File_Dx_Multi_Plan_Info; }

    public DbsField getDoc_Ext_File_Dx_Multi_Plan_No() { return doc_Ext_File_Dx_Multi_Plan_No; }

    public DbsField getDoc_Ext_File_Dx_Multi_Sub_Plan() { return doc_Ext_File_Dx_Multi_Sub_Plan; }

    public DbsField getDoc_Ext_File_Dx_Tiaa_Init_Prem() { return doc_Ext_File_Dx_Tiaa_Init_Prem; }

    public DbsField getDoc_Ext_File_Dx_Cref_Init_Prem() { return doc_Ext_File_Dx_Cref_Init_Prem; }

    public DbsField getDoc_Ext_File_Dx_Filler() { return doc_Ext_File_Dx_Filler; }

    public DbsField getDoc_Ext_File_Dx_Related_Contract_Info() { return doc_Ext_File_Dx_Related_Contract_Info; }

    public DbsGroup getDoc_Ext_File_Dx_Related_Contract_InfoRedef9() { return doc_Ext_File_Dx_Related_Contract_InfoRedef9; }

    public DbsGroup getDoc_Ext_File_Dx_Related_Contract() { return doc_Ext_File_Dx_Related_Contract; }

    public DbsField getDoc_Ext_File_Dx_Related_Contract_Type() { return doc_Ext_File_Dx_Related_Contract_Type; }

    public DbsField getDoc_Ext_File_Dx_Related_Tiaa_No() { return doc_Ext_File_Dx_Related_Tiaa_No; }

    public DbsField getDoc_Ext_File_Dx_Related_Tiaa_Issue_Date() { return doc_Ext_File_Dx_Related_Tiaa_Issue_Date; }

    public DbsField getDoc_Ext_File_Dx_Related_First_Payment_Date() { return doc_Ext_File_Dx_Related_First_Payment_Date; }

    public DbsField getDoc_Ext_File_Dx_Related_Tiaa_Total_Amt() { return doc_Ext_File_Dx_Related_Tiaa_Total_Amt; }

    public DbsField getDoc_Ext_File_Dx_Related_Last_Payment_Date() { return doc_Ext_File_Dx_Related_Last_Payment_Date; }

    public DbsField getDoc_Ext_File_Dx_Related_Payment_Frequency() { return doc_Ext_File_Dx_Related_Payment_Frequency; }

    public DbsField getDoc_Ext_File_Dx_Related_Tiaa_Issue_State() { return doc_Ext_File_Dx_Related_Tiaa_Issue_State; }

    public DbsField getDoc_Ext_File_Dx_First_Tpa_Ipro_Ind() { return doc_Ext_File_Dx_First_Tpa_Ipro_Ind; }

    public DbsField getDoc_Ext_File_Dx_Fund_Source_Cde_1() { return doc_Ext_File_Dx_Fund_Source_Cde_1; }

    public DbsField getDoc_Ext_File_Dx_Fund_Source_Cde_2() { return doc_Ext_File_Dx_Fund_Source_Cde_2; }

    public DbsField getDoc_Ext_File_Dx_Fund_Cde_2() { return doc_Ext_File_Dx_Fund_Cde_2; }

    public DbsField getDoc_Ext_File_Dx_Alloc_Pct_2() { return doc_Ext_File_Dx_Alloc_Pct_2; }

    public DbsGroup getDoc_Ext_FileRedef10() { return doc_Ext_FileRedef10; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_1() { return doc_Ext_File_Doc_Ext_Data_1; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_2() { return doc_Ext_File_Doc_Ext_Data_2; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_3() { return doc_Ext_File_Doc_Ext_Data_3; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_4() { return doc_Ext_File_Doc_Ext_Data_4; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_5() { return doc_Ext_File_Doc_Ext_Data_5; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_6() { return doc_Ext_File_Doc_Ext_Data_6; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_7() { return doc_Ext_File_Doc_Ext_Data_7; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_8() { return doc_Ext_File_Doc_Ext_Data_8; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_9() { return doc_Ext_File_Doc_Ext_Data_9; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_10() { return doc_Ext_File_Doc_Ext_Data_10; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_11() { return doc_Ext_File_Doc_Ext_Data_11; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_12() { return doc_Ext_File_Doc_Ext_Data_12; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_13() { return doc_Ext_File_Doc_Ext_Data_13; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_14() { return doc_Ext_File_Doc_Ext_Data_14; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_15() { return doc_Ext_File_Doc_Ext_Data_15; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_16() { return doc_Ext_File_Doc_Ext_Data_16; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_17() { return doc_Ext_File_Doc_Ext_Data_17; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_18() { return doc_Ext_File_Doc_Ext_Data_18; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_19() { return doc_Ext_File_Doc_Ext_Data_19; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_20() { return doc_Ext_File_Doc_Ext_Data_20; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_21() { return doc_Ext_File_Doc_Ext_Data_21; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_22() { return doc_Ext_File_Doc_Ext_Data_22; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_23() { return doc_Ext_File_Doc_Ext_Data_23; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_24() { return doc_Ext_File_Doc_Ext_Data_24; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_25() { return doc_Ext_File_Doc_Ext_Data_25; }

    public DbsField getDoc_Ext_File_Doc_Ext_Data_26() { return doc_Ext_File_Doc_Ext_Data_26; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        doc_Ext_File = newFieldInRecord("doc_Ext_File", "DOC-EXT-FILE", FieldType.STRING, 6372);
        doc_Ext_FileRedef1 = newGroupInRecord("doc_Ext_FileRedef1", "Redefines", doc_Ext_File);
        doc_Ext_File_Dx_Type_Cd = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Type_Cd", "DX-TYPE-CD", FieldType.STRING, 1);
        doc_Ext_File_Dx_Contract_Table = doc_Ext_FileRedef1.newFieldArrayInGroup("doc_Ext_File_Dx_Contract_Table", "DX-CONTRACT-TABLE", FieldType.STRING, 
            16, new DbsArrayController(1,4));
        doc_Ext_File_Dx_Contract_TableRedef2 = doc_Ext_FileRedef1.newGroupInGroup("doc_Ext_File_Dx_Contract_TableRedef2", "Redefines", doc_Ext_File_Dx_Contract_Table);
        doc_Ext_File_Dx_Contract_Numbers = doc_Ext_File_Dx_Contract_TableRedef2.newGroupArrayInGroup("doc_Ext_File_Dx_Contract_Numbers", "DX-CONTRACT-NUMBERS", 
            new DbsArrayController(1,4));
        doc_Ext_File_Dx_Tiaa_Prfx = doc_Ext_File_Dx_Contract_Numbers.newFieldInGroup("doc_Ext_File_Dx_Tiaa_Prfx", "DX-TIAA-PRFX", FieldType.STRING, 1);
        doc_Ext_File_Dx_Tiaa_No = doc_Ext_File_Dx_Contract_Numbers.newFieldInGroup("doc_Ext_File_Dx_Tiaa_No", "DX-TIAA-NO", FieldType.STRING, 7);
        doc_Ext_File_Dx_Cref_Prfx = doc_Ext_File_Dx_Contract_Numbers.newFieldInGroup("doc_Ext_File_Dx_Cref_Prfx", "DX-CREF-PRFX", FieldType.STRING, 1);
        doc_Ext_File_Dx_Cref_No = doc_Ext_File_Dx_Contract_Numbers.newFieldInGroup("doc_Ext_File_Dx_Cref_No", "DX-CREF-NO", FieldType.STRING, 7);
        doc_Ext_File_Dx_Date_Table = doc_Ext_FileRedef1.newFieldArrayInGroup("doc_Ext_File_Dx_Date_Table", "DX-DATE-TABLE", FieldType.STRING, 8, new DbsArrayController(1,
            6));
        doc_Ext_File_Dx_Age_First_Pymt = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Age_First_Pymt", "DX-AGE-FIRST-PYMT", FieldType.STRING, 4);
        doc_Ext_File_Dx_Ownership = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Ownership", "DX-OWNERSHIP", FieldType.STRING, 1);
        doc_Ext_File_Dx_Alloc_Disc = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Alloc_Disc", "DX-ALLOC-DISC", FieldType.STRING, 1);
        doc_Ext_File_Dx_Currency = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Currency", "DX-CURRENCY", FieldType.STRING, 1);
        doc_Ext_File_Dx_Tiaa_Form_No = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Tiaa_Form_No", "DX-TIAA-FORM-NO", FieldType.STRING, 9);
        doc_Ext_File_Dx_Cref_Form_No = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Cref_Form_No", "DX-CREF-FORM-NO", FieldType.STRING, 9);
        doc_Ext_File_Dx_Tiaa_Ed_No = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Tiaa_Ed_No", "DX-TIAA-ED-NO", FieldType.STRING, 9);
        doc_Ext_File_Dx_Cref_Ed_No = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Cref_Ed_No", "DX-CREF-ED-NO", FieldType.STRING, 9);
        doc_Ext_File_Dx_Annuitant_Name = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Annuitant_Name", "DX-ANNUITANT-NAME", FieldType.STRING, 38);
        doc_Ext_File_Dx_Eop_Name = doc_Ext_FileRedef1.newGroupInGroup("doc_Ext_File_Dx_Eop_Name", "DX-EOP-NAME");
        doc_Ext_File_Dx_Eop_Last_Name = doc_Ext_File_Dx_Eop_Name.newFieldInGroup("doc_Ext_File_Dx_Eop_Last_Name", "DX-EOP-LAST-NAME", FieldType.STRING, 
            16);
        doc_Ext_File_Dx_Eop_First_Name = doc_Ext_File_Dx_Eop_Name.newFieldInGroup("doc_Ext_File_Dx_Eop_First_Name", "DX-EOP-FIRST-NAME", FieldType.STRING, 
            10);
        doc_Ext_File_Dx_Eop_Middle_Name = doc_Ext_File_Dx_Eop_Name.newFieldInGroup("doc_Ext_File_Dx_Eop_Middle_Name", "DX-EOP-MIDDLE-NAME", FieldType.STRING, 
            10);
        doc_Ext_File_Dx_Eop_Middle_NameRedef3 = doc_Ext_File_Dx_Eop_Name.newGroupInGroup("doc_Ext_File_Dx_Eop_Middle_NameRedef3", "Redefines", doc_Ext_File_Dx_Eop_Middle_Name);
        doc_Ext_File_Dx_Eop_Mid_Init = doc_Ext_File_Dx_Eop_Middle_NameRedef3.newFieldInGroup("doc_Ext_File_Dx_Eop_Mid_Init", "DX-EOP-MID-INIT", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Eop_Mid_Filler = doc_Ext_File_Dx_Eop_Middle_NameRedef3.newFieldInGroup("doc_Ext_File_Dx_Eop_Mid_Filler", "DX-EOP-MID-FILLER", 
            FieldType.STRING, 9);
        doc_Ext_File_Dx_Eop_Title = doc_Ext_File_Dx_Eop_Name.newFieldInGroup("doc_Ext_File_Dx_Eop_Title", "DX-EOP-TITLE", FieldType.STRING, 4);
        doc_Ext_File_Dx_Eop_Prefix = doc_Ext_File_Dx_Eop_Name.newFieldInGroup("doc_Ext_File_Dx_Eop_Prefix", "DX-EOP-PREFIX", FieldType.STRING, 4);
        doc_Ext_File_Dx_Soc_Sec = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Soc_Sec", "DX-SOC-SEC", FieldType.STRING, 11);
        doc_Ext_File_Dx_Pin_No = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Pin_No", "DX-PIN-NO", FieldType.STRING, 12);
        doc_Ext_File_Dx_Address_Table = doc_Ext_FileRedef1.newFieldArrayInGroup("doc_Ext_File_Dx_Address_Table", "DX-ADDRESS-TABLE", FieldType.STRING, 
            35, new DbsArrayController(1,6));
        doc_Ext_File_Dx_Eop_Zipcode = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Eop_Zipcode", "DX-EOP-ZIPCODE", FieldType.STRING, 5);
        doc_Ext_File_Dx_Participant_Status = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Participant_Status", "DX-PARTICIPANT-STATUS", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Email_Address = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Email_Address", "DX-EMAIL-ADDRESS", FieldType.STRING, 50);
        doc_Ext_File_Dx_Request_Package_Type = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Request_Package_Type", "DX-REQUEST-PACKAGE-TYPE", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Portfolio_Selected = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Portfolio_Selected", "DX-PORTFOLIO-SELECTED", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Divorce_Contract = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Divorce_Contract", "DX-DIVORCE-CONTRACT", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Access_Cd_Ind = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Access_Cd_Ind", "DX-ACCESS-CD-IND", FieldType.STRING, 1);
        doc_Ext_File_Dx_Address_Change = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Address_Change", "DX-ADDRESS-CHANGE", FieldType.STRING, 1);
        doc_Ext_File_Dx_Package_Version = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Package_Version", "DX-PACKAGE-VERSION", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Unit_Name = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Unit_Name", "DX-UNIT-NAME", FieldType.STRING, 10);
        doc_Ext_File_Dx_Print_Package_Type = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Print_Package_Type", "DX-PRINT-PACKAGE-TYPE", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Future_Filler = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Future_Filler", "DX-FUTURE-FILLER", FieldType.STRING, 5);
        doc_Ext_File_Dx_Bene_Estate = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Bene_Estate", "DX-BENE-ESTATE", FieldType.STRING, 1);
        doc_Ext_File_Dx_Bene_Trust = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Bene_Trust", "DX-BENE-TRUST", FieldType.STRING, 1);
        doc_Ext_File_Dx_Bene_Category = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Bene_Category", "DX-BENE-CATEGORY", FieldType.STRING, 1);
        doc_Ext_File_Dx_Sex = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Sex", "DX-SEX", FieldType.STRING, 1);
        doc_Ext_File_Dx_Inst_Name_Table = doc_Ext_FileRedef1.newFieldArrayInGroup("doc_Ext_File_Dx_Inst_Name_Table", "DX-INST-NAME-TABLE", FieldType.STRING, 
            30, new DbsArrayController(1,10));
        doc_Ext_File_Dx_Inst_Name_76 = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Inst_Name_76", "DX-INST-NAME-76", FieldType.STRING, 76);
        doc_Ext_File_Dx_Inst_Address_Table = doc_Ext_FileRedef1.newFieldArrayInGroup("doc_Ext_File_Dx_Inst_Address_Table", "DX-INST-ADDRESS-TABLE", FieldType.STRING, 
            35, new DbsArrayController(1,6));
        doc_Ext_File_Dx_Inst_Zip_Cd = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Inst_Zip_Cd", "DX-INST-ZIP-CD", FieldType.STRING, 5);
        doc_Ext_File_Dx_Mail_Instructions = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Mail_Instructions", "DX-MAIL-INSTRUCTIONS", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Orig_Iss_State = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Orig_Iss_State", "DX-ORIG-ISS-STATE", FieldType.STRING, 2);
        doc_Ext_File_Dx_Current_Iss_State = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Current_Iss_State", "DX-CURRENT-ISS-STATE", FieldType.STRING, 
            2);
        doc_Ext_File_Dx_Eop_State_Name = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Eop_State_Name", "DX-EOP-STATE-NAME", FieldType.STRING, 15);
        doc_Ext_File_Dx_Lob = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Lob", "DX-LOB", FieldType.STRING, 1);
        doc_Ext_File_Dx_Lob_Type = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Lob_Type", "DX-LOB-TYPE", FieldType.STRING, 1);
        doc_Ext_File_Dx_Inst_Code = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Inst_Code", "DX-INST-CODE", FieldType.STRING, 4);
        doc_Ext_File_Dx_Inst_Bill_Cd = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Inst_Bill_Cd", "DX-INST-BILL-CD", FieldType.STRING, 1);
        doc_Ext_File_Dx_Inst_Permit_Trans = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Inst_Permit_Trans", "DX-INST-PERMIT-TRANS", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Perm_Gra_Trans = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Perm_Gra_Trans", "DX-PERM-GRA-TRANS", FieldType.STRING, 1);
        doc_Ext_File_Dx_Inst_Full_Immed_Vest = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Inst_Full_Immed_Vest", "DX-INST-FULL-IMMED-VEST", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Inst_Cashable_Ra = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Inst_Cashable_Ra", "DX-INST-CASHABLE-RA", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Inst_Cashable_Gra = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Inst_Cashable_Gra", "DX-INST-CASHABLE-GRA", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Inst_Fixed_Per_Opt = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Inst_Fixed_Per_Opt", "DX-INST-FIXED-PER-OPT", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Inst_Cntrl_Consent = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Inst_Cntrl_Consent", "DX-INST-CNTRL-CONSENT", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Eop = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Eop", "DX-EOP", FieldType.STRING, 1);
        doc_Ext_File_Dx_Gsra_Inst_St_Code = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Gsra_Inst_St_Code", "DX-GSRA-INST-ST-CODE", FieldType.STRING, 
            2);
        doc_Ext_File_Dx_Region_Cd = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Region_Cd", "DX-REGION-CD", FieldType.STRING, 3);
        doc_Ext_File_Dx_Staff_Id = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Staff_Id", "DX-STAFF-ID", FieldType.STRING, 3);
        doc_Ext_File_Dx_Appl_Source = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Appl_Source", "DX-APPL-SOURCE", FieldType.STRING, 1);
        doc_Ext_File_Dx_Eop_Appl_Status = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Eop_Appl_Status", "DX-EOP-APPL-STATUS", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Eop_Addl_Cref_Req = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Eop_Addl_Cref_Req", "DX-EOP-ADDL-CREF-REQ", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Extract_Date = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Extract_Date", "DX-EXTRACT-DATE", FieldType.STRING, 6);
        doc_Ext_File_Dx_Gsra_Loan_Ind = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Gsra_Loan_Ind", "DX-GSRA-LOAN-IND", FieldType.STRING, 1);
        doc_Ext_File_Dx_Spec_Ppg_Ind = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Spec_Ppg_Ind", "DX-SPEC-PPG-IND", FieldType.STRING, 2);
        doc_Ext_File_Dx_Mail_Pull_Cd = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Mail_Pull_Cd", "DX-MAIL-PULL-CD", FieldType.STRING, 1);
        doc_Ext_File_Dx_Mit_Log_Dt_Tm = doc_Ext_FileRedef1.newFieldArrayInGroup("doc_Ext_File_Dx_Mit_Log_Dt_Tm", "DX-MIT-LOG-DT-TM", FieldType.STRING, 
            15, new DbsArrayController(1,5));
        doc_Ext_File_Dx_Addr_Page_Name = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Addr_Page_Name", "DX-ADDR-PAGE-NAME", FieldType.STRING, 81);
        doc_Ext_File_Dx_Addr_Page_NameRedef4 = doc_Ext_FileRedef1.newGroupInGroup("doc_Ext_File_Dx_Addr_Page_NameRedef4", "Redefines", doc_Ext_File_Dx_Addr_Page_Name);
        doc_Ext_File_Dx_Addr_Pref = doc_Ext_File_Dx_Addr_Page_NameRedef4.newFieldInGroup("doc_Ext_File_Dx_Addr_Pref", "DX-ADDR-PREF", FieldType.STRING, 
            8);
        doc_Ext_File_Dx_Addr_Filler_1 = doc_Ext_File_Dx_Addr_Page_NameRedef4.newFieldInGroup("doc_Ext_File_Dx_Addr_Filler_1", "DX-ADDR-FILLER-1", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Addr_Frst = doc_Ext_File_Dx_Addr_Page_NameRedef4.newFieldInGroup("doc_Ext_File_Dx_Addr_Frst", "DX-ADDR-FRST", FieldType.STRING, 
            30);
        doc_Ext_File_Dx_Addr_Filler_2 = doc_Ext_File_Dx_Addr_Page_NameRedef4.newFieldInGroup("doc_Ext_File_Dx_Addr_Filler_2", "DX-ADDR-FILLER-2", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Addr_Mddle = doc_Ext_File_Dx_Addr_Page_NameRedef4.newFieldInGroup("doc_Ext_File_Dx_Addr_Mddle", "DX-ADDR-MDDLE", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Addr_Filler_3 = doc_Ext_File_Dx_Addr_Page_NameRedef4.newFieldInGroup("doc_Ext_File_Dx_Addr_Filler_3", "DX-ADDR-FILLER-3", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Addr_Last = doc_Ext_File_Dx_Addr_Page_NameRedef4.newFieldInGroup("doc_Ext_File_Dx_Addr_Last", "DX-ADDR-LAST", FieldType.STRING, 
            30);
        doc_Ext_File_Dx_Addr_Filler_4 = doc_Ext_File_Dx_Addr_Page_NameRedef4.newFieldInGroup("doc_Ext_File_Dx_Addr_Filler_4", "DX-ADDR-FILLER-4", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Addr_Sffx = doc_Ext_File_Dx_Addr_Page_NameRedef4.newFieldInGroup("doc_Ext_File_Dx_Addr_Sffx", "DX-ADDR-SFFX", FieldType.STRING, 
            8);
        doc_Ext_File_Dx_Welcome_Name = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Welcome_Name", "DX-WELCOME-NAME", FieldType.STRING, 39);
        doc_Ext_File_Dx_Welcome_NameRedef5 = doc_Ext_FileRedef1.newGroupInGroup("doc_Ext_File_Dx_Welcome_NameRedef5", "Redefines", doc_Ext_File_Dx_Welcome_Name);
        doc_Ext_File_Dx_Welcome_Pref = doc_Ext_File_Dx_Welcome_NameRedef5.newFieldInGroup("doc_Ext_File_Dx_Welcome_Pref", "DX-WELCOME-PREF", FieldType.STRING, 
            8);
        doc_Ext_File_Dx_Welcome_Filler_1 = doc_Ext_File_Dx_Welcome_NameRedef5.newFieldInGroup("doc_Ext_File_Dx_Welcome_Filler_1", "DX-WELCOME-FILLER-1", 
            FieldType.STRING, 1);
        doc_Ext_File_Dx_Welcome_Last = doc_Ext_File_Dx_Welcome_NameRedef5.newFieldInGroup("doc_Ext_File_Dx_Welcome_Last", "DX-WELCOME-LAST", FieldType.STRING, 
            30);
        doc_Ext_File_Dx_Pin_Name = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Pin_Name", "DX-PIN-NAME", FieldType.STRING, 63);
        doc_Ext_File_Dx_Pin_NameRedef6 = doc_Ext_FileRedef1.newGroupInGroup("doc_Ext_File_Dx_Pin_NameRedef6", "Redefines", doc_Ext_File_Dx_Pin_Name);
        doc_Ext_File_Dx_Pin_First = doc_Ext_File_Dx_Pin_NameRedef6.newFieldInGroup("doc_Ext_File_Dx_Pin_First", "DX-PIN-FIRST", FieldType.STRING, 30);
        doc_Ext_File_Dx_Pin_Filler_1 = doc_Ext_File_Dx_Pin_NameRedef6.newFieldInGroup("doc_Ext_File_Dx_Pin_Filler_1", "DX-PIN-FILLER-1", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Pin_Mddle = doc_Ext_File_Dx_Pin_NameRedef6.newFieldInGroup("doc_Ext_File_Dx_Pin_Mddle", "DX-PIN-MDDLE", FieldType.STRING, 1);
        doc_Ext_File_Dx_Pin_Filler_2 = doc_Ext_File_Dx_Pin_NameRedef6.newFieldInGroup("doc_Ext_File_Dx_Pin_Filler_2", "DX-PIN-FILLER-2", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Pin_Last = doc_Ext_File_Dx_Pin_NameRedef6.newFieldInGroup("doc_Ext_File_Dx_Pin_Last", "DX-PIN-LAST", FieldType.STRING, 30);
        doc_Ext_File_Dx_Zip = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Zip", "DX-ZIP", FieldType.STRING, 5);
        doc_Ext_File_Dx_ZipRedef7 = doc_Ext_FileRedef1.newGroupInGroup("doc_Ext_File_Dx_ZipRedef7", "Redefines", doc_Ext_File_Dx_Zip);
        doc_Ext_File_Dx_Zip_Plus4 = doc_Ext_File_Dx_ZipRedef7.newFieldInGroup("doc_Ext_File_Dx_Zip_Plus4", "DX-ZIP-PLUS4", FieldType.STRING, 4);
        doc_Ext_File_Dx_Zip_Plus1 = doc_Ext_File_Dx_ZipRedef7.newFieldInGroup("doc_Ext_File_Dx_Zip_Plus1", "DX-ZIP-PLUS1", FieldType.STRING, 1);
        doc_Ext_File_Dx_Suspension = doc_Ext_FileRedef1.newFieldArrayInGroup("doc_Ext_File_Dx_Suspension", "DX-SUSPENSION", FieldType.STRING, 1, new DbsArrayController(1,
            20));
        doc_Ext_File_Dx_Mult_App_Version = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Mult_App_Version", "DX-MULT-APP-VERSION", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Mult_App_Status = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Mult_App_Status", "DX-MULT-APP-STATUS", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Mult_App_Lob = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Mult_App_Lob", "DX-MULT-APP-LOB", FieldType.STRING, 1);
        doc_Ext_File_Dx_Mult_App_Lob_Type = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Mult_App_Lob_Type", "DX-MULT-APP-LOB-TYPE", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Mult_App_Ppg = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Mult_App_Ppg", "DX-MULT-APP-PPG", FieldType.STRING, 6);
        doc_Ext_File_Dx_K12_Ppg = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_K12_Ppg", "DX-K12-PPG", FieldType.STRING, 1);
        doc_Ext_File_Dx_Ira_Rollover_Type = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Ira_Rollover_Type", "DX-IRA-ROLLOVER-TYPE", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Ira_Record_Type = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Ira_Record_Type", "DX-IRA-RECORD-TYPE", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Rollover_Amt = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Rollover_Amt", "DX-ROLLOVER-AMT", FieldType.STRING, 14);
        doc_Ext_File_Dx_Mit_Unit = doc_Ext_FileRedef1.newFieldArrayInGroup("doc_Ext_File_Dx_Mit_Unit", "DX-MIT-UNIT", FieldType.STRING, 8, new DbsArrayController(1,
            5));
        doc_Ext_File_Dx_Mit_Wpid = doc_Ext_FileRedef1.newFieldArrayInGroup("doc_Ext_File_Dx_Mit_Wpid", "DX-MIT-WPID", FieldType.STRING, 6, new DbsArrayController(1,
            5));
        doc_Ext_File_Dx_New_Bene_Ind = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_New_Bene_Ind", "DX-NEW-BENE-IND", FieldType.STRING, 1);
        doc_Ext_File_Dx_Irc_Sectn_Cde = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Irc_Sectn_Cde", "DX-IRC-SECTN-CDE", FieldType.NUMERIC, 2);
        doc_Ext_File_Dx_Released_Dt = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Released_Dt", "DX-RELEASED-DT", FieldType.NUMERIC, 8);
        doc_Ext_File_Dx_Irc_Sectn_Grp = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Irc_Sectn_Grp", "DX-IRC-SECTN-GRP", FieldType.NUMERIC, 2);
        doc_Ext_File_Dx_Enroll_Request_Type = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Enroll_Request_Type", "DX-ENROLL-REQUEST-TYPE", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Erisa_Inst = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Erisa_Inst", "DX-ERISA-INST", FieldType.STRING, 1);
        doc_Ext_File_Dx_Reprint_Request_Type = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Reprint_Request_Type", "DX-REPRINT-REQUEST-TYPE", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Print_Destination = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Print_Destination", "DX-PRINT-DESTINATION", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Correction_Type = doc_Ext_FileRedef1.newFieldArrayInGroup("doc_Ext_File_Dx_Correction_Type", "DX-CORRECTION-TYPE", FieldType.STRING, 
            1, new DbsArrayController(1,20));
        doc_Ext_File_Dx_Processor_Name = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Processor_Name", "DX-PROCESSOR-NAME", FieldType.STRING, 40);
        doc_Ext_File_Dx_Mail_Date = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Mail_Date", "DX-MAIL-DATE", FieldType.STRING, 17);
        doc_Ext_File_Dx_Alloc_Fmt = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Alloc_Fmt", "DX-ALLOC-FMT", FieldType.STRING, 1);
        doc_Ext_File_Dx_Phone_No = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Phone_No", "DX-PHONE-NO", FieldType.STRING, 20);
        doc_Ext_File_Dx_Fund_Cde = doc_Ext_FileRedef1.newFieldArrayInGroup("doc_Ext_File_Dx_Fund_Cde", "DX-FUND-CDE", FieldType.STRING, 10, new DbsArrayController(1,
            100));
        doc_Ext_File_Dx_Alloc_Pct = doc_Ext_FileRedef1.newFieldArrayInGroup("doc_Ext_File_Dx_Alloc_Pct", "DX-ALLOC-PCT", FieldType.STRING, 3, new DbsArrayController(1,
            100));
        doc_Ext_File_Dx_Oia_Ind = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Oia_Ind", "DX-OIA-IND", FieldType.STRING, 1);
        doc_Ext_File_Dx_Product_Cde = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Product_Cde", "DX-PRODUCT-CDE", FieldType.STRING, 3);
        doc_Ext_File_Dx_Acct_Sum_Sheet_Type = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Acct_Sum_Sheet_Type", "DX-ACCT-SUM-SHEET-TYPE", FieldType.STRING, 
            56);
        doc_Ext_File_Dx_Sg_Plan_No = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Sg_Plan_No", "DX-SG-PLAN-NO", FieldType.STRING, 6);
        doc_Ext_File_Dx_Sg_Subplan_No = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Sg_Subplan_No", "DX-SG-SUBPLAN-NO", FieldType.STRING, 6);
        doc_Ext_File_Dx_Sg_Subplan_Type = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Sg_Subplan_Type", "DX-SG-SUBPLAN-TYPE", FieldType.STRING, 
            3);
        doc_Ext_File_Dx_Omni_Issue_Ind = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Omni_Issue_Ind", "DX-OMNI-ISSUE-IND", FieldType.STRING, 1);
        doc_Ext_File_Dx_Effective_Date = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Effective_Date", "DX-EFFECTIVE-DATE", FieldType.STRING, 8);
        doc_Ext_File_Dx_Employer_Name = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Employer_Name", "DX-EMPLOYER-NAME", FieldType.STRING, 40);
        doc_Ext_File_Dx_Tiaa_Rate = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Tiaa_Rate", "DX-TIAA-RATE", FieldType.STRING, 8);
        doc_Ext_File_Dx_Sg_Plan_Id = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Sg_Plan_Id", "DX-SG-PLAN-ID", FieldType.STRING, 6);
        doc_Ext_File_Dx_Tiaa_Pg4_Numb = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Tiaa_Pg4_Numb", "DX-TIAA-PG4-NUMB", FieldType.STRING, 15);
        doc_Ext_File_Dx_Cref_Pg4_Numb = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Cref_Pg4_Numb", "DX-CREF-PG4-NUMB", FieldType.STRING, 15);
        doc_Ext_File_Dx_Dflt_Access_Code = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Dflt_Access_Code", "DX-DFLT-ACCESS-CODE", FieldType.STRING, 
            9);
        doc_Ext_File_Dx_Single_Issue_Ind = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Single_Issue_Ind", "DX-SINGLE-ISSUE-IND", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Spec_Fund_Ind = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Spec_Fund_Ind", "DX-SPEC-FUND-IND", FieldType.STRING, 3);
        doc_Ext_File_Dx_Product_Price_Level = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Product_Price_Level", "DX-PRODUCT-PRICE-LEVEL", FieldType.STRING, 
            5);
        doc_Ext_File_Dx_Roth_Ind = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Roth_Ind", "DX-ROTH-IND", FieldType.STRING, 1);
        doc_Ext_File_Dx_Plan_Issue_State = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Plan_Issue_State", "DX-PLAN-ISSUE-STATE", FieldType.STRING, 
            2);
        doc_Ext_File_Dx_Sgrd_Client_Id = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Sgrd_Client_Id", "DX-SGRD-CLIENT-ID", FieldType.STRING, 6);
        doc_Ext_File_Dx_Portfolio_Type = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Portfolio_Type", "DX-PORTFOLIO-TYPE", FieldType.STRING, 4);
        doc_Ext_File_Dx_Auto_Enroll_Ind = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Auto_Enroll_Ind", "DX-AUTO-ENROLL-IND", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Auto_Save_Ind = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Auto_Save_Ind", "DX-AUTO-SAVE-IND", FieldType.STRING, 1);
        doc_Ext_File_Dx_As_Cur_Dflt_Amt = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_As_Cur_Dflt_Amt", "DX-AS-CUR-DFLT-AMT", FieldType.STRING, 
            10);
        doc_Ext_File_Dx_As_Incr_Amt = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_As_Incr_Amt", "DX-AS-INCR-AMT", FieldType.STRING, 7);
        doc_Ext_File_Dx_As_Max_Pct = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_As_Max_Pct", "DX-AS-MAX-PCT", FieldType.STRING, 7);
        doc_Ext_File_Dx_Ae_Opt_Out_Days = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Ae_Opt_Out_Days", "DX-AE-OPT-OUT-DAYS", FieldType.STRING, 
            2);
        doc_Ext_File_Dx_Tsv_Ind = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Tsv_Ind", "DX-TSV-IND", FieldType.STRING, 1);
        doc_Ext_File_Dx_Tiaa_Indx_Guarntd_Rte = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Tiaa_Indx_Guarntd_Rte", "DX-TIAA-INDX-GUARNTD-RTE", 
            FieldType.STRING, 6);
        doc_Ext_File_Dx_Agent_Crd_No = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Agent_Crd_No", "DX-AGENT-CRD-NO", FieldType.STRING, 15);
        doc_Ext_File_Dx_Tic_Ind = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Tic_Ind", "DX-TIC-IND", FieldType.STRING, 1);
        doc_Ext_File_Dx_Tic_Startdate = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Tic_Startdate", "DX-TIC-STARTDATE", FieldType.STRING, 8);
        doc_Ext_File_Dx_Tic_Enddate = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Tic_Enddate", "DX-TIC-ENDDATE", FieldType.STRING, 8);
        doc_Ext_File_Dx_Tic_Percentage = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Tic_Percentage", "DX-TIC-PERCENTAGE", FieldType.STRING, 9);
        doc_Ext_File_Dx_Tic_Postdays = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Tic_Postdays", "DX-TIC-POSTDAYS", FieldType.STRING, 2);
        doc_Ext_File_Dx_Tic_Limit = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Tic_Limit", "DX-TIC-LIMIT", FieldType.STRING, 11);
        doc_Ext_File_Dx_Tic_Postfreq = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Tic_Postfreq", "DX-TIC-POSTFREQ", FieldType.STRING, 1);
        doc_Ext_File_Dx_Tic_Pl_Level = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Tic_Pl_Level", "DX-TIC-PL-LEVEL", FieldType.STRING, 1);
        doc_Ext_File_Dx_Tic_Windowdays = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Tic_Windowdays", "DX-TIC-WINDOWDAYS", FieldType.STRING, 3);
        doc_Ext_File_Dx_Tic_Reqdlywindow = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Tic_Reqdlywindow", "DX-TIC-REQDLYWINDOW", FieldType.STRING, 
            3);
        doc_Ext_File_Dx_Tic_Recap_Prov = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Tic_Recap_Prov", "DX-TIC-RECAP-PROV", FieldType.STRING, 3);
        doc_Ext_File_Dx_Ecs_Dcs_E_Dlvry_Ind = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Ecs_Dcs_E_Dlvry_Ind", "DX-ECS-DCS-E-DLVRY-IND", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Ecs_Dcs_Annty_Optn_Ind = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Ecs_Dcs_Annty_Optn_Ind", "DX-ECS-DCS-ANNTY-OPTN-IND", 
            FieldType.STRING, 1);
        doc_Ext_File_Dx_Full_Middle_Name = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Full_Middle_Name", "DX-FULL-MIDDLE-NAME", FieldType.STRING, 
            30);
        doc_Ext_File_Dx_Substitution_Contract_Ind = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Substitution_Contract_Ind", "DX-SUBSTITUTION-CONTRACT-IND", 
            FieldType.STRING, 1);
        doc_Ext_File_Dx_Non_Proprietary_Pkg_Ind = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Non_Proprietary_Pkg_Ind", "DX-NON-PROPRIETARY-PKG-IND", 
            FieldType.STRING, 5);
        doc_Ext_File_Dx_Multi_Plan_Table = doc_Ext_FileRedef1.newFieldArrayInGroup("doc_Ext_File_Dx_Multi_Plan_Table", "DX-MULTI-PLAN-TABLE", FieldType.STRING, 
            12, new DbsArrayController(1,15));
        doc_Ext_File_Dx_Multi_Plan_TableRedef8 = doc_Ext_FileRedef1.newGroupInGroup("doc_Ext_File_Dx_Multi_Plan_TableRedef8", "Redefines", doc_Ext_File_Dx_Multi_Plan_Table);
        doc_Ext_File_Dx_Multi_Plan_Info = doc_Ext_File_Dx_Multi_Plan_TableRedef8.newGroupArrayInGroup("doc_Ext_File_Dx_Multi_Plan_Info", "DX-MULTI-PLAN-INFO", 
            new DbsArrayController(1,15));
        doc_Ext_File_Dx_Multi_Plan_No = doc_Ext_File_Dx_Multi_Plan_Info.newFieldInGroup("doc_Ext_File_Dx_Multi_Plan_No", "DX-MULTI-PLAN-NO", FieldType.STRING, 
            6);
        doc_Ext_File_Dx_Multi_Sub_Plan = doc_Ext_File_Dx_Multi_Plan_Info.newFieldInGroup("doc_Ext_File_Dx_Multi_Sub_Plan", "DX-MULTI-SUB-PLAN", FieldType.STRING, 
            6);
        doc_Ext_File_Dx_Tiaa_Init_Prem = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Tiaa_Init_Prem", "DX-TIAA-INIT-PREM", FieldType.DECIMAL, 
            10,2);
        doc_Ext_File_Dx_Cref_Init_Prem = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Cref_Init_Prem", "DX-CREF-INIT-PREM", FieldType.DECIMAL, 
            10,2);
        doc_Ext_File_Dx_Filler = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Filler", "DX-FILLER", FieldType.STRING, 68);
        doc_Ext_File_Dx_Related_Contract_Info = doc_Ext_FileRedef1.newFieldArrayInGroup("doc_Ext_File_Dx_Related_Contract_Info", "DX-RELATED-CONTRACT-INFO", 
            FieldType.STRING, 49, new DbsArrayController(1,30));
        doc_Ext_File_Dx_Related_Contract_InfoRedef9 = doc_Ext_FileRedef1.newGroupInGroup("doc_Ext_File_Dx_Related_Contract_InfoRedef9", "Redefines", doc_Ext_File_Dx_Related_Contract_Info);
        doc_Ext_File_Dx_Related_Contract = doc_Ext_File_Dx_Related_Contract_InfoRedef9.newGroupArrayInGroup("doc_Ext_File_Dx_Related_Contract", "DX-RELATED-CONTRACT", 
            new DbsArrayController(1,30));
        doc_Ext_File_Dx_Related_Contract_Type = doc_Ext_File_Dx_Related_Contract.newFieldInGroup("doc_Ext_File_Dx_Related_Contract_Type", "DX-RELATED-CONTRACT-TYPE", 
            FieldType.STRING, 1);
        doc_Ext_File_Dx_Related_Tiaa_No = doc_Ext_File_Dx_Related_Contract.newFieldInGroup("doc_Ext_File_Dx_Related_Tiaa_No", "DX-RELATED-TIAA-NO", FieldType.STRING, 
            10);
        doc_Ext_File_Dx_Related_Tiaa_Issue_Date = doc_Ext_File_Dx_Related_Contract.newFieldInGroup("doc_Ext_File_Dx_Related_Tiaa_Issue_Date", "DX-RELATED-TIAA-ISSUE-DATE", 
            FieldType.NUMERIC, 8);
        doc_Ext_File_Dx_Related_First_Payment_Date = doc_Ext_File_Dx_Related_Contract.newFieldInGroup("doc_Ext_File_Dx_Related_First_Payment_Date", "DX-RELATED-FIRST-PAYMENT-DATE", 
            FieldType.NUMERIC, 8);
        doc_Ext_File_Dx_Related_Tiaa_Total_Amt = doc_Ext_File_Dx_Related_Contract.newFieldInGroup("doc_Ext_File_Dx_Related_Tiaa_Total_Amt", "DX-RELATED-TIAA-TOTAL-AMT", 
            FieldType.DECIMAL, 10,2);
        doc_Ext_File_Dx_Related_Last_Payment_Date = doc_Ext_File_Dx_Related_Contract.newFieldInGroup("doc_Ext_File_Dx_Related_Last_Payment_Date", "DX-RELATED-LAST-PAYMENT-DATE", 
            FieldType.NUMERIC, 8);
        doc_Ext_File_Dx_Related_Payment_Frequency = doc_Ext_File_Dx_Related_Contract.newFieldInGroup("doc_Ext_File_Dx_Related_Payment_Frequency", "DX-RELATED-PAYMENT-FREQUENCY", 
            FieldType.STRING, 1);
        doc_Ext_File_Dx_Related_Tiaa_Issue_State = doc_Ext_File_Dx_Related_Contract.newFieldInGroup("doc_Ext_File_Dx_Related_Tiaa_Issue_State", "DX-RELATED-TIAA-ISSUE-STATE", 
            FieldType.STRING, 2);
        doc_Ext_File_Dx_First_Tpa_Ipro_Ind = doc_Ext_File_Dx_Related_Contract.newFieldInGroup("doc_Ext_File_Dx_First_Tpa_Ipro_Ind", "DX-FIRST-TPA-IPRO-IND", 
            FieldType.STRING, 1);
        doc_Ext_File_Dx_Fund_Source_Cde_1 = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Fund_Source_Cde_1", "DX-FUND-SOURCE-CDE-1", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Fund_Source_Cde_2 = doc_Ext_FileRedef1.newFieldInGroup("doc_Ext_File_Dx_Fund_Source_Cde_2", "DX-FUND-SOURCE-CDE-2", FieldType.STRING, 
            1);
        doc_Ext_File_Dx_Fund_Cde_2 = doc_Ext_FileRedef1.newFieldArrayInGroup("doc_Ext_File_Dx_Fund_Cde_2", "DX-FUND-CDE-2", FieldType.STRING, 10, new 
            DbsArrayController(1,100));
        doc_Ext_File_Dx_Alloc_Pct_2 = doc_Ext_FileRedef1.newFieldArrayInGroup("doc_Ext_File_Dx_Alloc_Pct_2", "DX-ALLOC-PCT-2", FieldType.STRING, 3, new 
            DbsArrayController(1,100));
        doc_Ext_FileRedef10 = newGroupInRecord("doc_Ext_FileRedef10", "Redefines", doc_Ext_File);
        doc_Ext_File_Doc_Ext_Data_1 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_1", "DOC-EXT-DATA-1", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_2 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_2", "DOC-EXT-DATA-2", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_3 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_3", "DOC-EXT-DATA-3", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_4 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_4", "DOC-EXT-DATA-4", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_5 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_5", "DOC-EXT-DATA-5", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_6 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_6", "DOC-EXT-DATA-6", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_7 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_7", "DOC-EXT-DATA-7", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_8 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_8", "DOC-EXT-DATA-8", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_9 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_9", "DOC-EXT-DATA-9", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_10 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_10", "DOC-EXT-DATA-10", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_11 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_11", "DOC-EXT-DATA-11", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_12 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_12", "DOC-EXT-DATA-12", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_13 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_13", "DOC-EXT-DATA-13", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_14 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_14", "DOC-EXT-DATA-14", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_15 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_15", "DOC-EXT-DATA-15", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_16 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_16", "DOC-EXT-DATA-16", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_17 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_17", "DOC-EXT-DATA-17", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_18 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_18", "DOC-EXT-DATA-18", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_19 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_19", "DOC-EXT-DATA-19", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_20 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_20", "DOC-EXT-DATA-20", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_21 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_21", "DOC-EXT-DATA-21", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_22 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_22", "DOC-EXT-DATA-22", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_23 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_23", "DOC-EXT-DATA-23", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_24 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_24", "DOC-EXT-DATA-24", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_25 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_25", "DOC-EXT-DATA-25", FieldType.STRING, 250);
        doc_Ext_File_Doc_Ext_Data_26 = doc_Ext_FileRedef10.newFieldInGroup("doc_Ext_File_Doc_Ext_Data_26", "DOC-EXT-DATA-26", FieldType.STRING, 122);

        this.setRecordName("LdaAppl160");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAppl160() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
