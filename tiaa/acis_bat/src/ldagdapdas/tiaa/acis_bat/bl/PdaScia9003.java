/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:23:23 PM
**        * FROM NATURAL PDA     : SCIA9003
************************************************************
**        * FILE NAME            : PdaScia9003.java
**        * CLASS NAME           : PdaScia9003
**        * INSTANCE NAME        : PdaScia9003
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaScia9003 extends PdaBase
{
    // Properties
    private DbsGroup scia9003;
    private DbsField scia9003_Pnd_Plan_No;
    private DbsField scia9003_Pnd_Sub_Plan_No;
    private DbsField scia9003_Pnd_Ssn;
    private DbsGroup scia9003_Pnd_SsnRedef1;
    private DbsField scia9003_Pnd_Ssn_N;
    private DbsField scia9003_Pnd_Tiaa_Cntrct;
    private DbsField scia9003_Pnd_Return_Code;
    private DbsField scia9003_Pnd_Return_Msg;

    public DbsGroup getScia9003() { return scia9003; }

    public DbsField getScia9003_Pnd_Plan_No() { return scia9003_Pnd_Plan_No; }

    public DbsField getScia9003_Pnd_Sub_Plan_No() { return scia9003_Pnd_Sub_Plan_No; }

    public DbsField getScia9003_Pnd_Ssn() { return scia9003_Pnd_Ssn; }

    public DbsGroup getScia9003_Pnd_SsnRedef1() { return scia9003_Pnd_SsnRedef1; }

    public DbsField getScia9003_Pnd_Ssn_N() { return scia9003_Pnd_Ssn_N; }

    public DbsField getScia9003_Pnd_Tiaa_Cntrct() { return scia9003_Pnd_Tiaa_Cntrct; }

    public DbsField getScia9003_Pnd_Return_Code() { return scia9003_Pnd_Return_Code; }

    public DbsField getScia9003_Pnd_Return_Msg() { return scia9003_Pnd_Return_Msg; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        scia9003 = dbsRecord.newGroupInRecord("scia9003", "SCIA9003");
        scia9003.setParameterOption(ParameterOption.ByReference);
        scia9003_Pnd_Plan_No = scia9003.newFieldInGroup("scia9003_Pnd_Plan_No", "#PLAN-NO", FieldType.STRING, 6);
        scia9003_Pnd_Sub_Plan_No = scia9003.newFieldInGroup("scia9003_Pnd_Sub_Plan_No", "#SUB-PLAN-NO", FieldType.STRING, 6);
        scia9003_Pnd_Ssn = scia9003.newFieldInGroup("scia9003_Pnd_Ssn", "#SSN", FieldType.STRING, 9);
        scia9003_Pnd_SsnRedef1 = scia9003.newGroupInGroup("scia9003_Pnd_SsnRedef1", "Redefines", scia9003_Pnd_Ssn);
        scia9003_Pnd_Ssn_N = scia9003_Pnd_SsnRedef1.newFieldInGroup("scia9003_Pnd_Ssn_N", "#SSN-N", FieldType.NUMERIC, 9);
        scia9003_Pnd_Tiaa_Cntrct = scia9003.newFieldInGroup("scia9003_Pnd_Tiaa_Cntrct", "#TIAA-CNTRCT", FieldType.STRING, 10);
        scia9003_Pnd_Return_Code = scia9003.newFieldInGroup("scia9003_Pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 4);
        scia9003_Pnd_Return_Msg = scia9003.newFieldInGroup("scia9003_Pnd_Return_Msg", "#RETURN-MSG", FieldType.STRING, 50);

        dbsRecord.reset();
    }

    // Constructors
    public PdaScia9003(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

