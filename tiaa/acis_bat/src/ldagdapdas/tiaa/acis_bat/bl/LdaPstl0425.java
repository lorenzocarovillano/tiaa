/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:10:11 PM
**        * FROM NATURAL LDA     : PSTL0425
************************************************************
**        * FILE NAME            : LdaPstl0425.java
**        * CLASS NAME           : LdaPstl0425
**        * INSTANCE NAME        : LdaPstl0425
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaPstl0425 extends DbsRecord
{
    // Properties
    private DbsGroup sort_Key;
    private DbsField sort_Key_Sort_Key_Lgth;
    private DbsField sort_Key_Sort_Key_Array;
    private DbsGroup pstl0425_Data;
    private DbsField pstl0425_Data_Pstl0425_Rec_Id;
    private DbsField pstl0425_Data_Pstl0425_Data_Lgth;
    private DbsField pstl0425_Data_Pstl0425_Data_Occurs;
    private DbsField pstl0425_Data_Pstl0425_Data_Array;
    private DbsGroup pstl0425_Data_Pstl0425_Data_ArrayRedef1;
    private DbsField pstl0425_Data_Rh_Employer;
    private DbsField pstl0425_Data_Rh_Date_Of_Birth;
    private DbsField pstl0425_Data_Rh_Account_Number;
    private DbsGroup pstl0425_Data_Pstl0425_Data_ArrayRedef2;
    private DbsGroup pstl0425_Data_Rh_Fund_List;
    private DbsField pstl0425_Data_Rh_Aa_Fund_Name;
    private DbsField pstl0425_Data_Rh_Aa_Percent_Amount;
    private DbsGroup pstl0425_Data_Pstl0425_Data_ArrayRedef3;
    private DbsGroup pstl0425_Data_Power_Image_Task_Information;
    private DbsField pstl0425_Data_Pi_Begin_Literal;
    private DbsField pstl0425_Data_Pi_Export_Ind;
    private DbsField pstl0425_Data_Pi_Task_Id;
    private DbsField pstl0425_Data_Pi_Task_Type;
    private DbsField pstl0425_Data_Pi_Task_Guid;
    private DbsField pstl0425_Data_Pi_Action_Step;
    private DbsField pstl0425_Data_Pi_Tiaa_Full_Date;
    private DbsGroup pstl0425_Data_Pi_Tiaa_Full_DateRedef4;
    private DbsField pstl0425_Data_Pi_Tiaa_Date_Century;
    private DbsField pstl0425_Data_Pi_Tiaa_Date_Year;
    private DbsField pstl0425_Data_Pi_Tiaa_Date_Month;
    private DbsField pstl0425_Data_Pi_Tiaa_Date_Day;
    private DbsField pstl0425_Data_Pi_Tiaa_Time;
    private DbsField pstl0425_Data_Pi_Task_Status;
    private DbsField pstl0425_Data_Pi_Ssn;
    private DbsField pstl0425_Data_Pi_Pin_Npin_Ppg;
    private DbsField pstl0425_Data_Pi_Pin_Type;
    private DbsField pstl0425_Data_Pi_Contract;
    private DbsGroup pstl0425_Data_Pi_ContractRedef5;
    private DbsField pstl0425_Data_Pi_Contract_A;
    private DbsField pstl0425_Data_Pi_Plan_Id;
    private DbsField pstl0425_Data_Pi_Doc_Content;
    private DbsField pstl0425_Data_Pi_Filler_1;
    private DbsField pstl0425_Data_Pi_Filler_2;
    private DbsField pstl0425_Data_Pi_Filler_3;
    private DbsField pstl0425_Data_Pi_Filler_4;
    private DbsField pstl0425_Data_Pi_Filler_5;
    private DbsField pstl0425_Data_Pi_End_Literal;

    public DbsGroup getSort_Key() { return sort_Key; }

    public DbsField getSort_Key_Sort_Key_Lgth() { return sort_Key_Sort_Key_Lgth; }

    public DbsField getSort_Key_Sort_Key_Array() { return sort_Key_Sort_Key_Array; }

    public DbsGroup getPstl0425_Data() { return pstl0425_Data; }

    public DbsField getPstl0425_Data_Pstl0425_Rec_Id() { return pstl0425_Data_Pstl0425_Rec_Id; }

    public DbsField getPstl0425_Data_Pstl0425_Data_Lgth() { return pstl0425_Data_Pstl0425_Data_Lgth; }

    public DbsField getPstl0425_Data_Pstl0425_Data_Occurs() { return pstl0425_Data_Pstl0425_Data_Occurs; }

    public DbsField getPstl0425_Data_Pstl0425_Data_Array() { return pstl0425_Data_Pstl0425_Data_Array; }

    public DbsGroup getPstl0425_Data_Pstl0425_Data_ArrayRedef1() { return pstl0425_Data_Pstl0425_Data_ArrayRedef1; }

    public DbsField getPstl0425_Data_Rh_Employer() { return pstl0425_Data_Rh_Employer; }

    public DbsField getPstl0425_Data_Rh_Date_Of_Birth() { return pstl0425_Data_Rh_Date_Of_Birth; }

    public DbsField getPstl0425_Data_Rh_Account_Number() { return pstl0425_Data_Rh_Account_Number; }

    public DbsGroup getPstl0425_Data_Pstl0425_Data_ArrayRedef2() { return pstl0425_Data_Pstl0425_Data_ArrayRedef2; }

    public DbsGroup getPstl0425_Data_Rh_Fund_List() { return pstl0425_Data_Rh_Fund_List; }

    public DbsField getPstl0425_Data_Rh_Aa_Fund_Name() { return pstl0425_Data_Rh_Aa_Fund_Name; }

    public DbsField getPstl0425_Data_Rh_Aa_Percent_Amount() { return pstl0425_Data_Rh_Aa_Percent_Amount; }

    public DbsGroup getPstl0425_Data_Pstl0425_Data_ArrayRedef3() { return pstl0425_Data_Pstl0425_Data_ArrayRedef3; }

    public DbsGroup getPstl0425_Data_Power_Image_Task_Information() { return pstl0425_Data_Power_Image_Task_Information; }

    public DbsField getPstl0425_Data_Pi_Begin_Literal() { return pstl0425_Data_Pi_Begin_Literal; }

    public DbsField getPstl0425_Data_Pi_Export_Ind() { return pstl0425_Data_Pi_Export_Ind; }

    public DbsField getPstl0425_Data_Pi_Task_Id() { return pstl0425_Data_Pi_Task_Id; }

    public DbsField getPstl0425_Data_Pi_Task_Type() { return pstl0425_Data_Pi_Task_Type; }

    public DbsField getPstl0425_Data_Pi_Task_Guid() { return pstl0425_Data_Pi_Task_Guid; }

    public DbsField getPstl0425_Data_Pi_Action_Step() { return pstl0425_Data_Pi_Action_Step; }

    public DbsField getPstl0425_Data_Pi_Tiaa_Full_Date() { return pstl0425_Data_Pi_Tiaa_Full_Date; }

    public DbsGroup getPstl0425_Data_Pi_Tiaa_Full_DateRedef4() { return pstl0425_Data_Pi_Tiaa_Full_DateRedef4; }

    public DbsField getPstl0425_Data_Pi_Tiaa_Date_Century() { return pstl0425_Data_Pi_Tiaa_Date_Century; }

    public DbsField getPstl0425_Data_Pi_Tiaa_Date_Year() { return pstl0425_Data_Pi_Tiaa_Date_Year; }

    public DbsField getPstl0425_Data_Pi_Tiaa_Date_Month() { return pstl0425_Data_Pi_Tiaa_Date_Month; }

    public DbsField getPstl0425_Data_Pi_Tiaa_Date_Day() { return pstl0425_Data_Pi_Tiaa_Date_Day; }

    public DbsField getPstl0425_Data_Pi_Tiaa_Time() { return pstl0425_Data_Pi_Tiaa_Time; }

    public DbsField getPstl0425_Data_Pi_Task_Status() { return pstl0425_Data_Pi_Task_Status; }

    public DbsField getPstl0425_Data_Pi_Ssn() { return pstl0425_Data_Pi_Ssn; }

    public DbsField getPstl0425_Data_Pi_Pin_Npin_Ppg() { return pstl0425_Data_Pi_Pin_Npin_Ppg; }

    public DbsField getPstl0425_Data_Pi_Pin_Type() { return pstl0425_Data_Pi_Pin_Type; }

    public DbsField getPstl0425_Data_Pi_Contract() { return pstl0425_Data_Pi_Contract; }

    public DbsGroup getPstl0425_Data_Pi_ContractRedef5() { return pstl0425_Data_Pi_ContractRedef5; }

    public DbsField getPstl0425_Data_Pi_Contract_A() { return pstl0425_Data_Pi_Contract_A; }

    public DbsField getPstl0425_Data_Pi_Plan_Id() { return pstl0425_Data_Pi_Plan_Id; }

    public DbsField getPstl0425_Data_Pi_Doc_Content() { return pstl0425_Data_Pi_Doc_Content; }

    public DbsField getPstl0425_Data_Pi_Filler_1() { return pstl0425_Data_Pi_Filler_1; }

    public DbsField getPstl0425_Data_Pi_Filler_2() { return pstl0425_Data_Pi_Filler_2; }

    public DbsField getPstl0425_Data_Pi_Filler_3() { return pstl0425_Data_Pi_Filler_3; }

    public DbsField getPstl0425_Data_Pi_Filler_4() { return pstl0425_Data_Pi_Filler_4; }

    public DbsField getPstl0425_Data_Pi_Filler_5() { return pstl0425_Data_Pi_Filler_5; }

    public DbsField getPstl0425_Data_Pi_End_Literal() { return pstl0425_Data_Pi_End_Literal; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        sort_Key = newGroupInRecord("sort_Key", "SORT-KEY");
        sort_Key_Sort_Key_Lgth = sort_Key.newFieldInGroup("sort_Key_Sort_Key_Lgth", "SORT-KEY-LGTH", FieldType.NUMERIC, 3);
        sort_Key_Sort_Key_Array = sort_Key.newFieldArrayInGroup("sort_Key_Sort_Key_Array", "SORT-KEY-ARRAY", FieldType.STRING, 1, new DbsArrayController(1,
            1));

        pstl0425_Data = newGroupInRecord("pstl0425_Data", "PSTL0425-DATA");
        pstl0425_Data_Pstl0425_Rec_Id = pstl0425_Data.newFieldInGroup("pstl0425_Data_Pstl0425_Rec_Id", "PSTL0425-REC-ID", FieldType.STRING, 2);
        pstl0425_Data_Pstl0425_Data_Lgth = pstl0425_Data.newFieldInGroup("pstl0425_Data_Pstl0425_Data_Lgth", "PSTL0425-DATA-LGTH", FieldType.NUMERIC, 
            5);
        pstl0425_Data_Pstl0425_Data_Occurs = pstl0425_Data.newFieldInGroup("pstl0425_Data_Pstl0425_Data_Occurs", "PSTL0425-DATA-OCCURS", FieldType.NUMERIC, 
            5);
        pstl0425_Data_Pstl0425_Data_Array = pstl0425_Data.newFieldArrayInGroup("pstl0425_Data_Pstl0425_Data_Array", "PSTL0425-DATA-ARRAY", FieldType.STRING, 
            1, new DbsArrayController(1,4449));
        pstl0425_Data_Pstl0425_Data_ArrayRedef1 = pstl0425_Data.newGroupInGroup("pstl0425_Data_Pstl0425_Data_ArrayRedef1", "Redefines", pstl0425_Data_Pstl0425_Data_Array);
        pstl0425_Data_Rh_Employer = pstl0425_Data_Pstl0425_Data_ArrayRedef1.newFieldInGroup("pstl0425_Data_Rh_Employer", "RH-EMPLOYER", FieldType.STRING, 
            76);
        pstl0425_Data_Rh_Date_Of_Birth = pstl0425_Data_Pstl0425_Data_ArrayRedef1.newFieldInGroup("pstl0425_Data_Rh_Date_Of_Birth", "RH-DATE-OF-BIRTH", 
            FieldType.STRING, 10);
        pstl0425_Data_Rh_Account_Number = pstl0425_Data_Pstl0425_Data_ArrayRedef1.newFieldInGroup("pstl0425_Data_Rh_Account_Number", "RH-ACCOUNT-NUMBER", 
            FieldType.STRING, 10);
        pstl0425_Data_Pstl0425_Data_ArrayRedef2 = pstl0425_Data.newGroupInGroup("pstl0425_Data_Pstl0425_Data_ArrayRedef2", "Redefines", pstl0425_Data_Pstl0425_Data_Array);
        pstl0425_Data_Rh_Fund_List = pstl0425_Data_Pstl0425_Data_ArrayRedef2.newGroupArrayInGroup("pstl0425_Data_Rh_Fund_List", "RH-FUND-LIST", new DbsArrayController(1,
            100));
        pstl0425_Data_Rh_Aa_Fund_Name = pstl0425_Data_Rh_Fund_List.newFieldInGroup("pstl0425_Data_Rh_Aa_Fund_Name", "RH-AA-FUND-NAME", FieldType.STRING, 
            40);
        pstl0425_Data_Rh_Aa_Percent_Amount = pstl0425_Data_Rh_Fund_List.newFieldInGroup("pstl0425_Data_Rh_Aa_Percent_Amount", "RH-AA-PERCENT-AMOUNT", 
            FieldType.NUMERIC, 3);
        pstl0425_Data_Pstl0425_Data_ArrayRedef3 = pstl0425_Data.newGroupInGroup("pstl0425_Data_Pstl0425_Data_ArrayRedef3", "Redefines", pstl0425_Data_Pstl0425_Data_Array);
        pstl0425_Data_Power_Image_Task_Information = pstl0425_Data_Pstl0425_Data_ArrayRedef3.newGroupInGroup("pstl0425_Data_Power_Image_Task_Information", 
            "POWER-IMAGE-TASK-INFORMATION");
        pstl0425_Data_Pi_Begin_Literal = pstl0425_Data_Power_Image_Task_Information.newFieldInGroup("pstl0425_Data_Pi_Begin_Literal", "PI-BEGIN-LITERAL", 
            FieldType.STRING, 4);
        pstl0425_Data_Pi_Export_Ind = pstl0425_Data_Power_Image_Task_Information.newFieldInGroup("pstl0425_Data_Pi_Export_Ind", "PI-EXPORT-IND", FieldType.STRING, 
            1);
        pstl0425_Data_Pi_Task_Id = pstl0425_Data_Power_Image_Task_Information.newFieldInGroup("pstl0425_Data_Pi_Task_Id", "PI-TASK-ID", FieldType.STRING, 
            11);
        pstl0425_Data_Pi_Task_Type = pstl0425_Data_Power_Image_Task_Information.newFieldInGroup("pstl0425_Data_Pi_Task_Type", "PI-TASK-TYPE", FieldType.STRING, 
            10);
        pstl0425_Data_Pi_Task_Guid = pstl0425_Data_Power_Image_Task_Information.newFieldInGroup("pstl0425_Data_Pi_Task_Guid", "PI-TASK-GUID", FieldType.STRING, 
            47);
        pstl0425_Data_Pi_Action_Step = pstl0425_Data_Power_Image_Task_Information.newFieldInGroup("pstl0425_Data_Pi_Action_Step", "PI-ACTION-STEP", FieldType.STRING, 
            8);
        pstl0425_Data_Pi_Tiaa_Full_Date = pstl0425_Data_Power_Image_Task_Information.newFieldInGroup("pstl0425_Data_Pi_Tiaa_Full_Date", "PI-TIAA-FULL-DATE", 
            FieldType.STRING, 8);
        pstl0425_Data_Pi_Tiaa_Full_DateRedef4 = pstl0425_Data_Power_Image_Task_Information.newGroupInGroup("pstl0425_Data_Pi_Tiaa_Full_DateRedef4", "Redefines", 
            pstl0425_Data_Pi_Tiaa_Full_Date);
        pstl0425_Data_Pi_Tiaa_Date_Century = pstl0425_Data_Pi_Tiaa_Full_DateRedef4.newFieldInGroup("pstl0425_Data_Pi_Tiaa_Date_Century", "PI-TIAA-DATE-CENTURY", 
            FieldType.NUMERIC, 2);
        pstl0425_Data_Pi_Tiaa_Date_Year = pstl0425_Data_Pi_Tiaa_Full_DateRedef4.newFieldInGroup("pstl0425_Data_Pi_Tiaa_Date_Year", "PI-TIAA-DATE-YEAR", 
            FieldType.NUMERIC, 2);
        pstl0425_Data_Pi_Tiaa_Date_Month = pstl0425_Data_Pi_Tiaa_Full_DateRedef4.newFieldInGroup("pstl0425_Data_Pi_Tiaa_Date_Month", "PI-TIAA-DATE-MONTH", 
            FieldType.NUMERIC, 2);
        pstl0425_Data_Pi_Tiaa_Date_Day = pstl0425_Data_Pi_Tiaa_Full_DateRedef4.newFieldInGroup("pstl0425_Data_Pi_Tiaa_Date_Day", "PI-TIAA-DATE-DAY", FieldType.NUMERIC, 
            2);
        pstl0425_Data_Pi_Tiaa_Time = pstl0425_Data_Power_Image_Task_Information.newFieldInGroup("pstl0425_Data_Pi_Tiaa_Time", "PI-TIAA-TIME", FieldType.STRING, 
            8);
        pstl0425_Data_Pi_Task_Status = pstl0425_Data_Power_Image_Task_Information.newFieldInGroup("pstl0425_Data_Pi_Task_Status", "PI-TASK-STATUS", FieldType.STRING, 
            1);
        pstl0425_Data_Pi_Ssn = pstl0425_Data_Power_Image_Task_Information.newFieldInGroup("pstl0425_Data_Pi_Ssn", "PI-SSN", FieldType.STRING, 9);
        pstl0425_Data_Pi_Pin_Npin_Ppg = pstl0425_Data_Power_Image_Task_Information.newFieldInGroup("pstl0425_Data_Pi_Pin_Npin_Ppg", "PI-PIN-NPIN-PPG", 
            FieldType.STRING, 7);
        pstl0425_Data_Pi_Pin_Type = pstl0425_Data_Power_Image_Task_Information.newFieldInGroup("pstl0425_Data_Pi_Pin_Type", "PI-PIN-TYPE", FieldType.STRING, 
            1);
        pstl0425_Data_Pi_Contract = pstl0425_Data_Power_Image_Task_Information.newFieldInGroup("pstl0425_Data_Pi_Contract", "PI-CONTRACT", FieldType.STRING, 
            100);
        pstl0425_Data_Pi_ContractRedef5 = pstl0425_Data_Power_Image_Task_Information.newGroupInGroup("pstl0425_Data_Pi_ContractRedef5", "Redefines", pstl0425_Data_Pi_Contract);
        pstl0425_Data_Pi_Contract_A = pstl0425_Data_Pi_ContractRedef5.newFieldArrayInGroup("pstl0425_Data_Pi_Contract_A", "PI-CONTRACT-A", FieldType.STRING, 
            10, new DbsArrayController(1,10));
        pstl0425_Data_Pi_Plan_Id = pstl0425_Data_Power_Image_Task_Information.newFieldInGroup("pstl0425_Data_Pi_Plan_Id", "PI-PLAN-ID", FieldType.STRING, 
            6);
        pstl0425_Data_Pi_Doc_Content = pstl0425_Data_Power_Image_Task_Information.newFieldInGroup("pstl0425_Data_Pi_Doc_Content", "PI-DOC-CONTENT", FieldType.STRING, 
            30);
        pstl0425_Data_Pi_Filler_1 = pstl0425_Data_Power_Image_Task_Information.newFieldInGroup("pstl0425_Data_Pi_Filler_1", "PI-FILLER-1", FieldType.STRING, 
            20);
        pstl0425_Data_Pi_Filler_2 = pstl0425_Data_Power_Image_Task_Information.newFieldInGroup("pstl0425_Data_Pi_Filler_2", "PI-FILLER-2", FieldType.STRING, 
            20);
        pstl0425_Data_Pi_Filler_3 = pstl0425_Data_Power_Image_Task_Information.newFieldInGroup("pstl0425_Data_Pi_Filler_3", "PI-FILLER-3", FieldType.STRING, 
            20);
        pstl0425_Data_Pi_Filler_4 = pstl0425_Data_Power_Image_Task_Information.newFieldInGroup("pstl0425_Data_Pi_Filler_4", "PI-FILLER-4", FieldType.STRING, 
            20);
        pstl0425_Data_Pi_Filler_5 = pstl0425_Data_Power_Image_Task_Information.newFieldInGroup("pstl0425_Data_Pi_Filler_5", "PI-FILLER-5", FieldType.STRING, 
            20);
        pstl0425_Data_Pi_End_Literal = pstl0425_Data_Power_Image_Task_Information.newFieldInGroup("pstl0425_Data_Pi_End_Literal", "PI-END-LITERAL", FieldType.STRING, 
            4);

        this.setRecordName("LdaPstl0425");
    }

    public void initializeValues() throws Exception
    {
        reset();
        sort_Key_Sort_Key_Lgth.setInitialValue(1);
        pstl0425_Data_Pstl0425_Data_Lgth.setInitialValue(4449);
        pstl0425_Data_Pstl0425_Data_Occurs.setInitialValue(1);
    }

    // Constructor
    public LdaPstl0425() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
