/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:51:02 PM
**        * FROM NATURAL LDA     : APPL220
************************************************************
**        * FILE NAME            : LdaAppl220.java
**        * CLASS NAME           : LdaAppl220
**        * INSTANCE NAME        : LdaAppl220
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl220 extends DbsRecord
{
    // Properties
    private DbsGroup prap_Extract_File;
    private DbsField prap_Extract_File_Ex_Reg_Code;
    private DbsGroup prap_Extract_File_Ex_Reg_CodeRedef1;
    private DbsField prap_Extract_File_Ex_Reg_Num;
    private DbsField prap_Extract_File_Ex_Reg_Needs;
    private DbsField prap_Extract_File_Ex_Name_Key;
    private DbsGroup prap_Extract_File_Ex_Name_KeyRedef2;
    private DbsField prap_Extract_File_Ex_N_Last;
    private DbsField prap_Extract_File_Ex_N_First;
    private DbsField prap_Extract_File_Ex_N_Middle;
    private DbsField prap_Extract_File_Ex_Coll_Code;
    private DbsGroup prap_Extract_File_Ex_Coll_CodeRedef3;
    private DbsField prap_Extract_File_Ex_Coll_Cd;
    private DbsField prap_Extract_File_Ex_Bill_Cd;
    private DbsField prap_Extract_File_Ex_Type;
    private DbsField prap_Extract_File_Ex_Name;
    private DbsField prap_Extract_File_Ex_Soc_Sec;
    private DbsField prap_Extract_File_Ex_Date_Birth;
    private DbsField prap_Extract_File_Ex_Sex;
    private DbsField prap_Extract_File_Ex_Currency;
    private DbsField prap_Extract_File_Ex_Ownership;
    private DbsField prap_Extract_File_Ex_Tiaa_Age_1st_Pay;
    private DbsField prap_Extract_File_Ex_Cref_Age_1st_Pay;
    private DbsField prap_Extract_File_Ex_Zip_Code;
    private DbsField prap_Extract_File_Ex_T_Contract;
    private DbsGroup prap_Extract_File_Ex_T_ContractRedef4;
    private DbsField prap_Extract_File_Ex_T_Pref;
    private DbsField prap_Extract_File_Ex_T_Contr;
    private DbsField prap_Extract_File_Ex_C_Contract;
    private DbsGroup prap_Extract_File_Ex_C_ContractRedef5;
    private DbsField prap_Extract_File_Ex_C_Pref;
    private DbsField prap_Extract_File_Ex_C_Contr;
    private DbsField prap_Extract_File_Ex_T_Date_Issue;
    private DbsGroup prap_Extract_File_Ex_T_Date_IssueRedef6;
    private DbsField prap_Extract_File_Ex_T_Mm_Issue;
    private DbsField prap_Extract_File_Ex_T_Dd_Issue;
    private DbsField prap_Extract_File_Ex_T_Cc_Issue;
    private DbsField prap_Extract_File_Ex_T_Yy_Issue;
    private DbsField prap_Extract_File_Ex_C_Date_Issue;
    private DbsGroup prap_Extract_File_Ex_C_Date_IssueRedef7;
    private DbsField prap_Extract_File_Ex_C_Mm_Issue;
    private DbsField prap_Extract_File_Ex_C_Dd_Issue;
    private DbsField prap_Extract_File_Ex_C_Cc_Issue;
    private DbsField prap_Extract_File_Ex_C_Yy_Issue;
    private DbsField prap_Extract_File_Ex_Dt_Appl_Rec;
    private DbsField prap_Extract_File_Ex_App_Source;
    private DbsField prap_Extract_File_Ex_Orig_Iss_State;
    private DbsField prap_Extract_File_Ex_Ownership_Type;
    private DbsField prap_Extract_File_Ex_Coll_State_Cd;
    private DbsField prap_Extract_File_Ex_Split_Cd;
    private DbsField prap_Extract_File_Ex_Trans_Code;
    private DbsField prap_Extract_File_Ex_Address_Field;
    private DbsField prap_Extract_File_Ex_Sthd_Rtn_Cd;
    private DbsField prap_Extract_File_Ex_Final_List_Rd_Cd;
    private DbsField prap_Extract_File_Ex_Addr_Stnd_Code;
    private DbsField prap_Extract_File_Ex_Stndr_Overide;
    private DbsField prap_Extract_File_Ex_Post_Data_Fileds;
    private DbsField prap_Extract_File_Ex_Addrss_Geogr_Cd;
    private DbsField prap_Extract_File_Ex_Lob;
    private DbsField prap_Extract_File_Ex_Lob_Type;
    private DbsField prap_Extract_File_Ex_Cor_Prefix;
    private DbsField prap_Extract_File_Ex_Cor_Last_Name;
    private DbsField prap_Extract_File_Ex_Cor_First_Name;
    private DbsField prap_Extract_File_Ex_Cor_Mddle_Name;
    private DbsField prap_Extract_File_Ex_Cor_Suffix;
    private DbsField prap_Extract_File_Ex_Cor_His_Ind;
    private DbsField prap_Extract_File_Ex_Cor_Unique_Id_Nbr;
    private DbsField prap_Extract_File_Ex_Net_Ind;
    private DbsField prap_Extract_File_Ex_Ppg_Team;
    private DbsField prap_Extract_File_Ex_Addr_Sync_Ind;
    private DbsField prap_Extract_File_Ex_G_Key;
    private DbsField prap_Extract_File_Ex_G_Ind;
    private DbsField prap_Extract_File_Ex_Name_Addr_Cd;
    private DbsField prap_Extract_File_Ex_Dt_Ent_Sys;
    private DbsField prap_Extract_File_Ex_Dt_Released;
    private DbsField prap_Extract_File_Ex_Dt_Matched;
    private DbsField prap_Extract_File_Ex_Dt_Deleted;
    private DbsField prap_Extract_File_Ex_Dt_Withdrawn;
    private DbsField prap_Extract_File_Ex_Alloc_Discr;
    private DbsField prap_Extract_File_Ex_Release_Ind;
    private DbsField prap_Extract_File_Ex_Other_Pols;
    private DbsField prap_Extract_File_Ex_Record_Type;
    private DbsField prap_Extract_File_Ex_Status;
    private DbsField prap_Extract_File_Ex_Numb_Dailys;
    private DbsField prap_Extract_File_Ex_City;
    private DbsField prap_Extract_File_Ex_Current_State_Code;
    private DbsField prap_Extract_File_Ex_Dana_Transaction_Cd;
    private DbsField prap_Extract_File_Ex_Annuity_Start_Date;
    private DbsGroup prap_Extract_File_Ex_Maximum_Alloc_Pct;
    private DbsField prap_Extract_File_Ex_Max_Alloc_Pct;
    private DbsField prap_Extract_File_Ex_Max_Alloc_Pct_Sign;
    private DbsField prap_Extract_File_Ex_Bene_Info_Type;
    private DbsField prap_Extract_File_Ex_Primary_Std_Ent;
    private DbsGroup prap_Extract_File_Ex_Bene_Primary_Info;
    private DbsField prap_Extract_File_Ex_Primary_Bene_Info;
    private DbsField prap_Extract_File_Ex_Contingent_Std_Ent;
    private DbsGroup prap_Extract_File_Ex_Bene_Contingent_Info;
    private DbsField prap_Extract_File_Ex_Contingent_Bene_Info;
    private DbsField prap_Extract_File_Ex_Bene_Estate;
    private DbsField prap_Extract_File_Ex_Bene_Trust;
    private DbsField prap_Extract_File_Ex_Bene_Category;
    private DbsField prap_Extract_File_Ex_Mail_Instructions;
    private DbsField prap_Extract_File_Ex_Eop_Addl_Cref_Request;
    private DbsField prap_Extract_File_Ex_Process_Status;
    private DbsField prap_Extract_File_Ex_Csm_Sec_Seg;
    private DbsField prap_Extract_File_Ex_Rlc_College;
    private DbsField prap_Extract_File_Ex_Rcrd_Updt_Tm_Stamp;
    private DbsField prap_Extract_File_Ex_Contact_Mode;
    private DbsField prap_Extract_File_Ex_Racf_Id;
    private DbsGroup prap_Extract_File_Ex_Rqst_Log_Dte_Tm;
    private DbsField prap_Extract_File_Ex_Rqst_Log_Dte_Time;
    private DbsField prap_Extract_File_Ex_Sync_Ind;
    private DbsField prap_Extract_File_Ex_Cor_Ind;
    private DbsField prap_Extract_File_Ex_Alloc_Ind;
    private DbsField prap_Extract_File_Ex_Tiaa_Doi;
    private DbsField prap_Extract_File_Ex_Cref_Doi;
    private DbsGroup prap_Extract_File_Ex_Mit_Request;
    private DbsField prap_Extract_File_Ex_Mit_Unit;
    private DbsField prap_Extract_File_Ex_Mit_Wpid;
    private DbsField prap_Extract_File_Ex_Ira_Rollover_Type;
    private DbsField prap_Extract_File_Ex_Ira_Record_Type;
    private DbsField prap_Extract_File_Ex_Mult_App_Status;
    private DbsField prap_Extract_File_Ex_Mult_App_Lob;
    private DbsField prap_Extract_File_Ex_Mult_App_Lob_Type;
    private DbsField prap_Extract_File_Ex_Mult_App_Ppg;
    private DbsField prap_Extract_File_Ex_Print_Date;
    private DbsGroup prap_Extract_File_Ex_Cntrct_Info;
    private DbsField prap_Extract_File_Ex_Cntrct_Type;
    private DbsField prap_Extract_File_Ex_Cntrct_Nbr;
    private DbsField prap_Extract_File_Ex_Cntrct_Proceeds_Amt;
    private DbsGroup prap_Extract_File_Ex_Addr_Info;
    private DbsField prap_Extract_File_Ex_Address_Txt;
    private DbsField prap_Extract_File_Ex_Address_Dest_Name;
    private DbsField prap_Extract_File_Ex_Bank_Pymnt_Acct_Nmbr;
    private DbsField prap_Extract_File_Ex_Bank_Aba_Acct_Nmbr;
    private DbsField prap_Extract_File_Ex_Addr_Usage_Code;
    private DbsField prap_Extract_File_Ex_Init_Paymt_Amt;
    private DbsField prap_Extract_File_Ex_Init_Paymt_Pct;
    private DbsField prap_Extract_File_Ex_Financial_1;
    private DbsField prap_Extract_File_Ex_Financial_2;
    private DbsField prap_Extract_File_Ex_Financial_3;
    private DbsField prap_Extract_File_Ex_Financial_4;
    private DbsField prap_Extract_File_Ex_Financial_5;
    private DbsField prap_Extract_File_Ex_Irc_Sectn_Grp_Cde;
    private DbsField prap_Extract_File_Ex_Irc_Sectn_Cde;
    private DbsField prap_Extract_File_Ex_Inst_Link_Cde;
    private DbsField prap_Extract_File_Ex_Applcnt_Req_Type;
    private DbsField prap_Extract_File_Ex_Tiaa_Service_Agent;
    private DbsField prap_Extract_File_Ex_Prap_Rsch_Mit_Ind;
    private DbsField prap_Extract_File_Ex_Rlc_Cref_Cert;
    private DbsGroup prap_Extract_File_Ex_Rlc_Cref_CertRedef8;
    private DbsField prap_Extract_File_Ex_Rlc_Cref_Pref;
    private DbsField prap_Extract_File_Ex_Rlc_Cref_Cont;
    private DbsField prap_Extract_File_Ex_Allocation_Model_Type;
    private DbsField prap_Extract_File_Ex_Divorce_Ind;
    private DbsField prap_Extract_File_Ex_Email_Address;
    private DbsField prap_Extract_File_Ex_E_Signed_Appl_Ind;
    private DbsField prap_Extract_File_Ex_Eft_Requested_Ind;
    private DbsField prap_Extract_File_Ex_Prap_Prem_Rsch_Ind;
    private DbsField prap_Extract_File_Ex_Address_Change_Ind;
    private DbsField prap_Extract_File_Ex_Allocation_Fmt;
    private DbsField prap_Extract_File_Ex_Phone_No;
    private DbsField prap_Extract_File_Ex_Fund_Cde;
    private DbsField prap_Extract_File_Ex_Allocation_Pct;
    private DbsField prap_Extract_File_Ex_Sg_Plan_No;
    private DbsField prap_Extract_File_Ex_Sg_Subplan_No;
    private DbsField prap_Extract_File_Ex_Sg_Part_Ext;
    private DbsField prap_Extract_File_Ex_Filler;
    private DbsGroup prap_Extract_FileRedef9;
    private DbsField prap_Extract_File_Ex_Data_1;
    private DbsField prap_Extract_File_Ex_Data_2;
    private DbsField prap_Extract_File_Ex_Data_3;
    private DbsField prap_Extract_File_Ex_Data_4;
    private DbsField prap_Extract_File_Ex_Data_5;
    private DbsField prap_Extract_File_Ex_Data_6;
    private DbsField prap_Extract_File_Ex_Data_7;
    private DbsField prap_Extract_File_Ex_Data_8;
    private DbsField prap_Extract_File_Ex_Data_9;
    private DbsField prap_Extract_File_Ex_Data_10;
    private DbsField prap_Extract_File_Ex_Data_11;
    private DbsField prap_Extract_File_Ex_Data_12;
    private DbsField prap_Extract_File_Ex_Data_13;
    private DbsField prap_Extract_File_Ex_Data_14;
    private DbsField prap_Extract_File_Ex_Data_15;
    private DbsField prap_Extract_File_Ex_Data_16;

    public DbsGroup getPrap_Extract_File() { return prap_Extract_File; }

    public DbsField getPrap_Extract_File_Ex_Reg_Code() { return prap_Extract_File_Ex_Reg_Code; }

    public DbsGroup getPrap_Extract_File_Ex_Reg_CodeRedef1() { return prap_Extract_File_Ex_Reg_CodeRedef1; }

    public DbsField getPrap_Extract_File_Ex_Reg_Num() { return prap_Extract_File_Ex_Reg_Num; }

    public DbsField getPrap_Extract_File_Ex_Reg_Needs() { return prap_Extract_File_Ex_Reg_Needs; }

    public DbsField getPrap_Extract_File_Ex_Name_Key() { return prap_Extract_File_Ex_Name_Key; }

    public DbsGroup getPrap_Extract_File_Ex_Name_KeyRedef2() { return prap_Extract_File_Ex_Name_KeyRedef2; }

    public DbsField getPrap_Extract_File_Ex_N_Last() { return prap_Extract_File_Ex_N_Last; }

    public DbsField getPrap_Extract_File_Ex_N_First() { return prap_Extract_File_Ex_N_First; }

    public DbsField getPrap_Extract_File_Ex_N_Middle() { return prap_Extract_File_Ex_N_Middle; }

    public DbsField getPrap_Extract_File_Ex_Coll_Code() { return prap_Extract_File_Ex_Coll_Code; }

    public DbsGroup getPrap_Extract_File_Ex_Coll_CodeRedef3() { return prap_Extract_File_Ex_Coll_CodeRedef3; }

    public DbsField getPrap_Extract_File_Ex_Coll_Cd() { return prap_Extract_File_Ex_Coll_Cd; }

    public DbsField getPrap_Extract_File_Ex_Bill_Cd() { return prap_Extract_File_Ex_Bill_Cd; }

    public DbsField getPrap_Extract_File_Ex_Type() { return prap_Extract_File_Ex_Type; }

    public DbsField getPrap_Extract_File_Ex_Name() { return prap_Extract_File_Ex_Name; }

    public DbsField getPrap_Extract_File_Ex_Soc_Sec() { return prap_Extract_File_Ex_Soc_Sec; }

    public DbsField getPrap_Extract_File_Ex_Date_Birth() { return prap_Extract_File_Ex_Date_Birth; }

    public DbsField getPrap_Extract_File_Ex_Sex() { return prap_Extract_File_Ex_Sex; }

    public DbsField getPrap_Extract_File_Ex_Currency() { return prap_Extract_File_Ex_Currency; }

    public DbsField getPrap_Extract_File_Ex_Ownership() { return prap_Extract_File_Ex_Ownership; }

    public DbsField getPrap_Extract_File_Ex_Tiaa_Age_1st_Pay() { return prap_Extract_File_Ex_Tiaa_Age_1st_Pay; }

    public DbsField getPrap_Extract_File_Ex_Cref_Age_1st_Pay() { return prap_Extract_File_Ex_Cref_Age_1st_Pay; }

    public DbsField getPrap_Extract_File_Ex_Zip_Code() { return prap_Extract_File_Ex_Zip_Code; }

    public DbsField getPrap_Extract_File_Ex_T_Contract() { return prap_Extract_File_Ex_T_Contract; }

    public DbsGroup getPrap_Extract_File_Ex_T_ContractRedef4() { return prap_Extract_File_Ex_T_ContractRedef4; }

    public DbsField getPrap_Extract_File_Ex_T_Pref() { return prap_Extract_File_Ex_T_Pref; }

    public DbsField getPrap_Extract_File_Ex_T_Contr() { return prap_Extract_File_Ex_T_Contr; }

    public DbsField getPrap_Extract_File_Ex_C_Contract() { return prap_Extract_File_Ex_C_Contract; }

    public DbsGroup getPrap_Extract_File_Ex_C_ContractRedef5() { return prap_Extract_File_Ex_C_ContractRedef5; }

    public DbsField getPrap_Extract_File_Ex_C_Pref() { return prap_Extract_File_Ex_C_Pref; }

    public DbsField getPrap_Extract_File_Ex_C_Contr() { return prap_Extract_File_Ex_C_Contr; }

    public DbsField getPrap_Extract_File_Ex_T_Date_Issue() { return prap_Extract_File_Ex_T_Date_Issue; }

    public DbsGroup getPrap_Extract_File_Ex_T_Date_IssueRedef6() { return prap_Extract_File_Ex_T_Date_IssueRedef6; }

    public DbsField getPrap_Extract_File_Ex_T_Mm_Issue() { return prap_Extract_File_Ex_T_Mm_Issue; }

    public DbsField getPrap_Extract_File_Ex_T_Dd_Issue() { return prap_Extract_File_Ex_T_Dd_Issue; }

    public DbsField getPrap_Extract_File_Ex_T_Cc_Issue() { return prap_Extract_File_Ex_T_Cc_Issue; }

    public DbsField getPrap_Extract_File_Ex_T_Yy_Issue() { return prap_Extract_File_Ex_T_Yy_Issue; }

    public DbsField getPrap_Extract_File_Ex_C_Date_Issue() { return prap_Extract_File_Ex_C_Date_Issue; }

    public DbsGroup getPrap_Extract_File_Ex_C_Date_IssueRedef7() { return prap_Extract_File_Ex_C_Date_IssueRedef7; }

    public DbsField getPrap_Extract_File_Ex_C_Mm_Issue() { return prap_Extract_File_Ex_C_Mm_Issue; }

    public DbsField getPrap_Extract_File_Ex_C_Dd_Issue() { return prap_Extract_File_Ex_C_Dd_Issue; }

    public DbsField getPrap_Extract_File_Ex_C_Cc_Issue() { return prap_Extract_File_Ex_C_Cc_Issue; }

    public DbsField getPrap_Extract_File_Ex_C_Yy_Issue() { return prap_Extract_File_Ex_C_Yy_Issue; }

    public DbsField getPrap_Extract_File_Ex_Dt_Appl_Rec() { return prap_Extract_File_Ex_Dt_Appl_Rec; }

    public DbsField getPrap_Extract_File_Ex_App_Source() { return prap_Extract_File_Ex_App_Source; }

    public DbsField getPrap_Extract_File_Ex_Orig_Iss_State() { return prap_Extract_File_Ex_Orig_Iss_State; }

    public DbsField getPrap_Extract_File_Ex_Ownership_Type() { return prap_Extract_File_Ex_Ownership_Type; }

    public DbsField getPrap_Extract_File_Ex_Coll_State_Cd() { return prap_Extract_File_Ex_Coll_State_Cd; }

    public DbsField getPrap_Extract_File_Ex_Split_Cd() { return prap_Extract_File_Ex_Split_Cd; }

    public DbsField getPrap_Extract_File_Ex_Trans_Code() { return prap_Extract_File_Ex_Trans_Code; }

    public DbsField getPrap_Extract_File_Ex_Address_Field() { return prap_Extract_File_Ex_Address_Field; }

    public DbsField getPrap_Extract_File_Ex_Sthd_Rtn_Cd() { return prap_Extract_File_Ex_Sthd_Rtn_Cd; }

    public DbsField getPrap_Extract_File_Ex_Final_List_Rd_Cd() { return prap_Extract_File_Ex_Final_List_Rd_Cd; }

    public DbsField getPrap_Extract_File_Ex_Addr_Stnd_Code() { return prap_Extract_File_Ex_Addr_Stnd_Code; }

    public DbsField getPrap_Extract_File_Ex_Stndr_Overide() { return prap_Extract_File_Ex_Stndr_Overide; }

    public DbsField getPrap_Extract_File_Ex_Post_Data_Fileds() { return prap_Extract_File_Ex_Post_Data_Fileds; }

    public DbsField getPrap_Extract_File_Ex_Addrss_Geogr_Cd() { return prap_Extract_File_Ex_Addrss_Geogr_Cd; }

    public DbsField getPrap_Extract_File_Ex_Lob() { return prap_Extract_File_Ex_Lob; }

    public DbsField getPrap_Extract_File_Ex_Lob_Type() { return prap_Extract_File_Ex_Lob_Type; }

    public DbsField getPrap_Extract_File_Ex_Cor_Prefix() { return prap_Extract_File_Ex_Cor_Prefix; }

    public DbsField getPrap_Extract_File_Ex_Cor_Last_Name() { return prap_Extract_File_Ex_Cor_Last_Name; }

    public DbsField getPrap_Extract_File_Ex_Cor_First_Name() { return prap_Extract_File_Ex_Cor_First_Name; }

    public DbsField getPrap_Extract_File_Ex_Cor_Mddle_Name() { return prap_Extract_File_Ex_Cor_Mddle_Name; }

    public DbsField getPrap_Extract_File_Ex_Cor_Suffix() { return prap_Extract_File_Ex_Cor_Suffix; }

    public DbsField getPrap_Extract_File_Ex_Cor_His_Ind() { return prap_Extract_File_Ex_Cor_His_Ind; }

    public DbsField getPrap_Extract_File_Ex_Cor_Unique_Id_Nbr() { return prap_Extract_File_Ex_Cor_Unique_Id_Nbr; }

    public DbsField getPrap_Extract_File_Ex_Net_Ind() { return prap_Extract_File_Ex_Net_Ind; }

    public DbsField getPrap_Extract_File_Ex_Ppg_Team() { return prap_Extract_File_Ex_Ppg_Team; }

    public DbsField getPrap_Extract_File_Ex_Addr_Sync_Ind() { return prap_Extract_File_Ex_Addr_Sync_Ind; }

    public DbsField getPrap_Extract_File_Ex_G_Key() { return prap_Extract_File_Ex_G_Key; }

    public DbsField getPrap_Extract_File_Ex_G_Ind() { return prap_Extract_File_Ex_G_Ind; }

    public DbsField getPrap_Extract_File_Ex_Name_Addr_Cd() { return prap_Extract_File_Ex_Name_Addr_Cd; }

    public DbsField getPrap_Extract_File_Ex_Dt_Ent_Sys() { return prap_Extract_File_Ex_Dt_Ent_Sys; }

    public DbsField getPrap_Extract_File_Ex_Dt_Released() { return prap_Extract_File_Ex_Dt_Released; }

    public DbsField getPrap_Extract_File_Ex_Dt_Matched() { return prap_Extract_File_Ex_Dt_Matched; }

    public DbsField getPrap_Extract_File_Ex_Dt_Deleted() { return prap_Extract_File_Ex_Dt_Deleted; }

    public DbsField getPrap_Extract_File_Ex_Dt_Withdrawn() { return prap_Extract_File_Ex_Dt_Withdrawn; }

    public DbsField getPrap_Extract_File_Ex_Alloc_Discr() { return prap_Extract_File_Ex_Alloc_Discr; }

    public DbsField getPrap_Extract_File_Ex_Release_Ind() { return prap_Extract_File_Ex_Release_Ind; }

    public DbsField getPrap_Extract_File_Ex_Other_Pols() { return prap_Extract_File_Ex_Other_Pols; }

    public DbsField getPrap_Extract_File_Ex_Record_Type() { return prap_Extract_File_Ex_Record_Type; }

    public DbsField getPrap_Extract_File_Ex_Status() { return prap_Extract_File_Ex_Status; }

    public DbsField getPrap_Extract_File_Ex_Numb_Dailys() { return prap_Extract_File_Ex_Numb_Dailys; }

    public DbsField getPrap_Extract_File_Ex_City() { return prap_Extract_File_Ex_City; }

    public DbsField getPrap_Extract_File_Ex_Current_State_Code() { return prap_Extract_File_Ex_Current_State_Code; }

    public DbsField getPrap_Extract_File_Ex_Dana_Transaction_Cd() { return prap_Extract_File_Ex_Dana_Transaction_Cd; }

    public DbsField getPrap_Extract_File_Ex_Annuity_Start_Date() { return prap_Extract_File_Ex_Annuity_Start_Date; }

    public DbsGroup getPrap_Extract_File_Ex_Maximum_Alloc_Pct() { return prap_Extract_File_Ex_Maximum_Alloc_Pct; }

    public DbsField getPrap_Extract_File_Ex_Max_Alloc_Pct() { return prap_Extract_File_Ex_Max_Alloc_Pct; }

    public DbsField getPrap_Extract_File_Ex_Max_Alloc_Pct_Sign() { return prap_Extract_File_Ex_Max_Alloc_Pct_Sign; }

    public DbsField getPrap_Extract_File_Ex_Bene_Info_Type() { return prap_Extract_File_Ex_Bene_Info_Type; }

    public DbsField getPrap_Extract_File_Ex_Primary_Std_Ent() { return prap_Extract_File_Ex_Primary_Std_Ent; }

    public DbsGroup getPrap_Extract_File_Ex_Bene_Primary_Info() { return prap_Extract_File_Ex_Bene_Primary_Info; }

    public DbsField getPrap_Extract_File_Ex_Primary_Bene_Info() { return prap_Extract_File_Ex_Primary_Bene_Info; }

    public DbsField getPrap_Extract_File_Ex_Contingent_Std_Ent() { return prap_Extract_File_Ex_Contingent_Std_Ent; }

    public DbsGroup getPrap_Extract_File_Ex_Bene_Contingent_Info() { return prap_Extract_File_Ex_Bene_Contingent_Info; }

    public DbsField getPrap_Extract_File_Ex_Contingent_Bene_Info() { return prap_Extract_File_Ex_Contingent_Bene_Info; }

    public DbsField getPrap_Extract_File_Ex_Bene_Estate() { return prap_Extract_File_Ex_Bene_Estate; }

    public DbsField getPrap_Extract_File_Ex_Bene_Trust() { return prap_Extract_File_Ex_Bene_Trust; }

    public DbsField getPrap_Extract_File_Ex_Bene_Category() { return prap_Extract_File_Ex_Bene_Category; }

    public DbsField getPrap_Extract_File_Ex_Mail_Instructions() { return prap_Extract_File_Ex_Mail_Instructions; }

    public DbsField getPrap_Extract_File_Ex_Eop_Addl_Cref_Request() { return prap_Extract_File_Ex_Eop_Addl_Cref_Request; }

    public DbsField getPrap_Extract_File_Ex_Process_Status() { return prap_Extract_File_Ex_Process_Status; }

    public DbsField getPrap_Extract_File_Ex_Csm_Sec_Seg() { return prap_Extract_File_Ex_Csm_Sec_Seg; }

    public DbsField getPrap_Extract_File_Ex_Rlc_College() { return prap_Extract_File_Ex_Rlc_College; }

    public DbsField getPrap_Extract_File_Ex_Rcrd_Updt_Tm_Stamp() { return prap_Extract_File_Ex_Rcrd_Updt_Tm_Stamp; }

    public DbsField getPrap_Extract_File_Ex_Contact_Mode() { return prap_Extract_File_Ex_Contact_Mode; }

    public DbsField getPrap_Extract_File_Ex_Racf_Id() { return prap_Extract_File_Ex_Racf_Id; }

    public DbsGroup getPrap_Extract_File_Ex_Rqst_Log_Dte_Tm() { return prap_Extract_File_Ex_Rqst_Log_Dte_Tm; }

    public DbsField getPrap_Extract_File_Ex_Rqst_Log_Dte_Time() { return prap_Extract_File_Ex_Rqst_Log_Dte_Time; }

    public DbsField getPrap_Extract_File_Ex_Sync_Ind() { return prap_Extract_File_Ex_Sync_Ind; }

    public DbsField getPrap_Extract_File_Ex_Cor_Ind() { return prap_Extract_File_Ex_Cor_Ind; }

    public DbsField getPrap_Extract_File_Ex_Alloc_Ind() { return prap_Extract_File_Ex_Alloc_Ind; }

    public DbsField getPrap_Extract_File_Ex_Tiaa_Doi() { return prap_Extract_File_Ex_Tiaa_Doi; }

    public DbsField getPrap_Extract_File_Ex_Cref_Doi() { return prap_Extract_File_Ex_Cref_Doi; }

    public DbsGroup getPrap_Extract_File_Ex_Mit_Request() { return prap_Extract_File_Ex_Mit_Request; }

    public DbsField getPrap_Extract_File_Ex_Mit_Unit() { return prap_Extract_File_Ex_Mit_Unit; }

    public DbsField getPrap_Extract_File_Ex_Mit_Wpid() { return prap_Extract_File_Ex_Mit_Wpid; }

    public DbsField getPrap_Extract_File_Ex_Ira_Rollover_Type() { return prap_Extract_File_Ex_Ira_Rollover_Type; }

    public DbsField getPrap_Extract_File_Ex_Ira_Record_Type() { return prap_Extract_File_Ex_Ira_Record_Type; }

    public DbsField getPrap_Extract_File_Ex_Mult_App_Status() { return prap_Extract_File_Ex_Mult_App_Status; }

    public DbsField getPrap_Extract_File_Ex_Mult_App_Lob() { return prap_Extract_File_Ex_Mult_App_Lob; }

    public DbsField getPrap_Extract_File_Ex_Mult_App_Lob_Type() { return prap_Extract_File_Ex_Mult_App_Lob_Type; }

    public DbsField getPrap_Extract_File_Ex_Mult_App_Ppg() { return prap_Extract_File_Ex_Mult_App_Ppg; }

    public DbsField getPrap_Extract_File_Ex_Print_Date() { return prap_Extract_File_Ex_Print_Date; }

    public DbsGroup getPrap_Extract_File_Ex_Cntrct_Info() { return prap_Extract_File_Ex_Cntrct_Info; }

    public DbsField getPrap_Extract_File_Ex_Cntrct_Type() { return prap_Extract_File_Ex_Cntrct_Type; }

    public DbsField getPrap_Extract_File_Ex_Cntrct_Nbr() { return prap_Extract_File_Ex_Cntrct_Nbr; }

    public DbsField getPrap_Extract_File_Ex_Cntrct_Proceeds_Amt() { return prap_Extract_File_Ex_Cntrct_Proceeds_Amt; }

    public DbsGroup getPrap_Extract_File_Ex_Addr_Info() { return prap_Extract_File_Ex_Addr_Info; }

    public DbsField getPrap_Extract_File_Ex_Address_Txt() { return prap_Extract_File_Ex_Address_Txt; }

    public DbsField getPrap_Extract_File_Ex_Address_Dest_Name() { return prap_Extract_File_Ex_Address_Dest_Name; }

    public DbsField getPrap_Extract_File_Ex_Bank_Pymnt_Acct_Nmbr() { return prap_Extract_File_Ex_Bank_Pymnt_Acct_Nmbr; }

    public DbsField getPrap_Extract_File_Ex_Bank_Aba_Acct_Nmbr() { return prap_Extract_File_Ex_Bank_Aba_Acct_Nmbr; }

    public DbsField getPrap_Extract_File_Ex_Addr_Usage_Code() { return prap_Extract_File_Ex_Addr_Usage_Code; }

    public DbsField getPrap_Extract_File_Ex_Init_Paymt_Amt() { return prap_Extract_File_Ex_Init_Paymt_Amt; }

    public DbsField getPrap_Extract_File_Ex_Init_Paymt_Pct() { return prap_Extract_File_Ex_Init_Paymt_Pct; }

    public DbsField getPrap_Extract_File_Ex_Financial_1() { return prap_Extract_File_Ex_Financial_1; }

    public DbsField getPrap_Extract_File_Ex_Financial_2() { return prap_Extract_File_Ex_Financial_2; }

    public DbsField getPrap_Extract_File_Ex_Financial_3() { return prap_Extract_File_Ex_Financial_3; }

    public DbsField getPrap_Extract_File_Ex_Financial_4() { return prap_Extract_File_Ex_Financial_4; }

    public DbsField getPrap_Extract_File_Ex_Financial_5() { return prap_Extract_File_Ex_Financial_5; }

    public DbsField getPrap_Extract_File_Ex_Irc_Sectn_Grp_Cde() { return prap_Extract_File_Ex_Irc_Sectn_Grp_Cde; }

    public DbsField getPrap_Extract_File_Ex_Irc_Sectn_Cde() { return prap_Extract_File_Ex_Irc_Sectn_Cde; }

    public DbsField getPrap_Extract_File_Ex_Inst_Link_Cde() { return prap_Extract_File_Ex_Inst_Link_Cde; }

    public DbsField getPrap_Extract_File_Ex_Applcnt_Req_Type() { return prap_Extract_File_Ex_Applcnt_Req_Type; }

    public DbsField getPrap_Extract_File_Ex_Tiaa_Service_Agent() { return prap_Extract_File_Ex_Tiaa_Service_Agent; }

    public DbsField getPrap_Extract_File_Ex_Prap_Rsch_Mit_Ind() { return prap_Extract_File_Ex_Prap_Rsch_Mit_Ind; }

    public DbsField getPrap_Extract_File_Ex_Rlc_Cref_Cert() { return prap_Extract_File_Ex_Rlc_Cref_Cert; }

    public DbsGroup getPrap_Extract_File_Ex_Rlc_Cref_CertRedef8() { return prap_Extract_File_Ex_Rlc_Cref_CertRedef8; }

    public DbsField getPrap_Extract_File_Ex_Rlc_Cref_Pref() { return prap_Extract_File_Ex_Rlc_Cref_Pref; }

    public DbsField getPrap_Extract_File_Ex_Rlc_Cref_Cont() { return prap_Extract_File_Ex_Rlc_Cref_Cont; }

    public DbsField getPrap_Extract_File_Ex_Allocation_Model_Type() { return prap_Extract_File_Ex_Allocation_Model_Type; }

    public DbsField getPrap_Extract_File_Ex_Divorce_Ind() { return prap_Extract_File_Ex_Divorce_Ind; }

    public DbsField getPrap_Extract_File_Ex_Email_Address() { return prap_Extract_File_Ex_Email_Address; }

    public DbsField getPrap_Extract_File_Ex_E_Signed_Appl_Ind() { return prap_Extract_File_Ex_E_Signed_Appl_Ind; }

    public DbsField getPrap_Extract_File_Ex_Eft_Requested_Ind() { return prap_Extract_File_Ex_Eft_Requested_Ind; }

    public DbsField getPrap_Extract_File_Ex_Prap_Prem_Rsch_Ind() { return prap_Extract_File_Ex_Prap_Prem_Rsch_Ind; }

    public DbsField getPrap_Extract_File_Ex_Address_Change_Ind() { return prap_Extract_File_Ex_Address_Change_Ind; }

    public DbsField getPrap_Extract_File_Ex_Allocation_Fmt() { return prap_Extract_File_Ex_Allocation_Fmt; }

    public DbsField getPrap_Extract_File_Ex_Phone_No() { return prap_Extract_File_Ex_Phone_No; }

    public DbsField getPrap_Extract_File_Ex_Fund_Cde() { return prap_Extract_File_Ex_Fund_Cde; }

    public DbsField getPrap_Extract_File_Ex_Allocation_Pct() { return prap_Extract_File_Ex_Allocation_Pct; }

    public DbsField getPrap_Extract_File_Ex_Sg_Plan_No() { return prap_Extract_File_Ex_Sg_Plan_No; }

    public DbsField getPrap_Extract_File_Ex_Sg_Subplan_No() { return prap_Extract_File_Ex_Sg_Subplan_No; }

    public DbsField getPrap_Extract_File_Ex_Sg_Part_Ext() { return prap_Extract_File_Ex_Sg_Part_Ext; }

    public DbsField getPrap_Extract_File_Ex_Filler() { return prap_Extract_File_Ex_Filler; }

    public DbsGroup getPrap_Extract_FileRedef9() { return prap_Extract_FileRedef9; }

    public DbsField getPrap_Extract_File_Ex_Data_1() { return prap_Extract_File_Ex_Data_1; }

    public DbsField getPrap_Extract_File_Ex_Data_2() { return prap_Extract_File_Ex_Data_2; }

    public DbsField getPrap_Extract_File_Ex_Data_3() { return prap_Extract_File_Ex_Data_3; }

    public DbsField getPrap_Extract_File_Ex_Data_4() { return prap_Extract_File_Ex_Data_4; }

    public DbsField getPrap_Extract_File_Ex_Data_5() { return prap_Extract_File_Ex_Data_5; }

    public DbsField getPrap_Extract_File_Ex_Data_6() { return prap_Extract_File_Ex_Data_6; }

    public DbsField getPrap_Extract_File_Ex_Data_7() { return prap_Extract_File_Ex_Data_7; }

    public DbsField getPrap_Extract_File_Ex_Data_8() { return prap_Extract_File_Ex_Data_8; }

    public DbsField getPrap_Extract_File_Ex_Data_9() { return prap_Extract_File_Ex_Data_9; }

    public DbsField getPrap_Extract_File_Ex_Data_10() { return prap_Extract_File_Ex_Data_10; }

    public DbsField getPrap_Extract_File_Ex_Data_11() { return prap_Extract_File_Ex_Data_11; }

    public DbsField getPrap_Extract_File_Ex_Data_12() { return prap_Extract_File_Ex_Data_12; }

    public DbsField getPrap_Extract_File_Ex_Data_13() { return prap_Extract_File_Ex_Data_13; }

    public DbsField getPrap_Extract_File_Ex_Data_14() { return prap_Extract_File_Ex_Data_14; }

    public DbsField getPrap_Extract_File_Ex_Data_15() { return prap_Extract_File_Ex_Data_15; }

    public DbsField getPrap_Extract_File_Ex_Data_16() { return prap_Extract_File_Ex_Data_16; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        prap_Extract_File = newGroupInRecord("prap_Extract_File", "PRAP-EXTRACT-FILE");
        prap_Extract_File_Ex_Reg_Code = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Reg_Code", "EX-REG-CODE", FieldType.STRING, 3);
        prap_Extract_File_Ex_Reg_CodeRedef1 = prap_Extract_File.newGroupInGroup("prap_Extract_File_Ex_Reg_CodeRedef1", "Redefines", prap_Extract_File_Ex_Reg_Code);
        prap_Extract_File_Ex_Reg_Num = prap_Extract_File_Ex_Reg_CodeRedef1.newFieldInGroup("prap_Extract_File_Ex_Reg_Num", "EX-REG-NUM", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Reg_Needs = prap_Extract_File_Ex_Reg_CodeRedef1.newFieldInGroup("prap_Extract_File_Ex_Reg_Needs", "EX-REG-NEEDS", FieldType.STRING, 
            2);
        prap_Extract_File_Ex_Name_Key = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Name_Key", "EX-NAME-KEY", FieldType.STRING, 9);
        prap_Extract_File_Ex_Name_KeyRedef2 = prap_Extract_File.newGroupInGroup("prap_Extract_File_Ex_Name_KeyRedef2", "Redefines", prap_Extract_File_Ex_Name_Key);
        prap_Extract_File_Ex_N_Last = prap_Extract_File_Ex_Name_KeyRedef2.newFieldInGroup("prap_Extract_File_Ex_N_Last", "EX-N-LAST", FieldType.STRING, 
            7);
        prap_Extract_File_Ex_N_First = prap_Extract_File_Ex_Name_KeyRedef2.newFieldInGroup("prap_Extract_File_Ex_N_First", "EX-N-FIRST", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_N_Middle = prap_Extract_File_Ex_Name_KeyRedef2.newFieldInGroup("prap_Extract_File_Ex_N_Middle", "EX-N-MIDDLE", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Coll_Code = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Coll_Code", "EX-COLL-CODE", FieldType.STRING, 5);
        prap_Extract_File_Ex_Coll_CodeRedef3 = prap_Extract_File.newGroupInGroup("prap_Extract_File_Ex_Coll_CodeRedef3", "Redefines", prap_Extract_File_Ex_Coll_Code);
        prap_Extract_File_Ex_Coll_Cd = prap_Extract_File_Ex_Coll_CodeRedef3.newFieldInGroup("prap_Extract_File_Ex_Coll_Cd", "EX-COLL-CD", FieldType.STRING, 
            4);
        prap_Extract_File_Ex_Bill_Cd = prap_Extract_File_Ex_Coll_CodeRedef3.newFieldInGroup("prap_Extract_File_Ex_Bill_Cd", "EX-BILL-CD", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Type = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Type", "EX-TYPE", FieldType.STRING, 1);
        prap_Extract_File_Ex_Name = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Name", "EX-NAME", FieldType.STRING, 38);
        prap_Extract_File_Ex_Soc_Sec = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Soc_Sec", "EX-SOC-SEC", FieldType.NUMERIC, 9);
        prap_Extract_File_Ex_Date_Birth = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Date_Birth", "EX-DATE-BIRTH", FieldType.NUMERIC, 8);
        prap_Extract_File_Ex_Sex = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Sex", "EX-SEX", FieldType.STRING, 1);
        prap_Extract_File_Ex_Currency = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Currency", "EX-CURRENCY", FieldType.NUMERIC, 1);
        prap_Extract_File_Ex_Ownership = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Ownership", "EX-OWNERSHIP", FieldType.STRING, 1);
        prap_Extract_File_Ex_Tiaa_Age_1st_Pay = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Tiaa_Age_1st_Pay", "EX-TIAA-AGE-1ST-PAY", FieldType.NUMERIC, 
            4);
        prap_Extract_File_Ex_Cref_Age_1st_Pay = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Cref_Age_1st_Pay", "EX-CREF-AGE-1ST-PAY", FieldType.NUMERIC, 
            4);
        prap_Extract_File_Ex_Zip_Code = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Zip_Code", "EX-ZIP-CODE", FieldType.STRING, 5);
        prap_Extract_File_Ex_T_Contract = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_T_Contract", "EX-T-CONTRACT", FieldType.STRING, 10);
        prap_Extract_File_Ex_T_ContractRedef4 = prap_Extract_File.newGroupInGroup("prap_Extract_File_Ex_T_ContractRedef4", "Redefines", prap_Extract_File_Ex_T_Contract);
        prap_Extract_File_Ex_T_Pref = prap_Extract_File_Ex_T_ContractRedef4.newFieldInGroup("prap_Extract_File_Ex_T_Pref", "EX-T-PREF", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_T_Contr = prap_Extract_File_Ex_T_ContractRedef4.newFieldInGroup("prap_Extract_File_Ex_T_Contr", "EX-T-CONTR", FieldType.STRING, 
            7);
        prap_Extract_File_Ex_C_Contract = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_C_Contract", "EX-C-CONTRACT", FieldType.STRING, 10);
        prap_Extract_File_Ex_C_ContractRedef5 = prap_Extract_File.newGroupInGroup("prap_Extract_File_Ex_C_ContractRedef5", "Redefines", prap_Extract_File_Ex_C_Contract);
        prap_Extract_File_Ex_C_Pref = prap_Extract_File_Ex_C_ContractRedef5.newFieldInGroup("prap_Extract_File_Ex_C_Pref", "EX-C-PREF", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_C_Contr = prap_Extract_File_Ex_C_ContractRedef5.newFieldInGroup("prap_Extract_File_Ex_C_Contr", "EX-C-CONTR", FieldType.STRING, 
            7);
        prap_Extract_File_Ex_T_Date_Issue = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_T_Date_Issue", "EX-T-DATE-ISSUE", FieldType.STRING, 
            8);
        prap_Extract_File_Ex_T_Date_IssueRedef6 = prap_Extract_File.newGroupInGroup("prap_Extract_File_Ex_T_Date_IssueRedef6", "Redefines", prap_Extract_File_Ex_T_Date_Issue);
        prap_Extract_File_Ex_T_Mm_Issue = prap_Extract_File_Ex_T_Date_IssueRedef6.newFieldInGroup("prap_Extract_File_Ex_T_Mm_Issue", "EX-T-MM-ISSUE", 
            FieldType.NUMERIC, 2);
        prap_Extract_File_Ex_T_Dd_Issue = prap_Extract_File_Ex_T_Date_IssueRedef6.newFieldInGroup("prap_Extract_File_Ex_T_Dd_Issue", "EX-T-DD-ISSUE", 
            FieldType.NUMERIC, 2);
        prap_Extract_File_Ex_T_Cc_Issue = prap_Extract_File_Ex_T_Date_IssueRedef6.newFieldInGroup("prap_Extract_File_Ex_T_Cc_Issue", "EX-T-CC-ISSUE", 
            FieldType.NUMERIC, 2);
        prap_Extract_File_Ex_T_Yy_Issue = prap_Extract_File_Ex_T_Date_IssueRedef6.newFieldInGroup("prap_Extract_File_Ex_T_Yy_Issue", "EX-T-YY-ISSUE", 
            FieldType.NUMERIC, 2);
        prap_Extract_File_Ex_C_Date_Issue = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_C_Date_Issue", "EX-C-DATE-ISSUE", FieldType.STRING, 
            8);
        prap_Extract_File_Ex_C_Date_IssueRedef7 = prap_Extract_File.newGroupInGroup("prap_Extract_File_Ex_C_Date_IssueRedef7", "Redefines", prap_Extract_File_Ex_C_Date_Issue);
        prap_Extract_File_Ex_C_Mm_Issue = prap_Extract_File_Ex_C_Date_IssueRedef7.newFieldInGroup("prap_Extract_File_Ex_C_Mm_Issue", "EX-C-MM-ISSUE", 
            FieldType.NUMERIC, 2);
        prap_Extract_File_Ex_C_Dd_Issue = prap_Extract_File_Ex_C_Date_IssueRedef7.newFieldInGroup("prap_Extract_File_Ex_C_Dd_Issue", "EX-C-DD-ISSUE", 
            FieldType.NUMERIC, 2);
        prap_Extract_File_Ex_C_Cc_Issue = prap_Extract_File_Ex_C_Date_IssueRedef7.newFieldInGroup("prap_Extract_File_Ex_C_Cc_Issue", "EX-C-CC-ISSUE", 
            FieldType.NUMERIC, 2);
        prap_Extract_File_Ex_C_Yy_Issue = prap_Extract_File_Ex_C_Date_IssueRedef7.newFieldInGroup("prap_Extract_File_Ex_C_Yy_Issue", "EX-C-YY-ISSUE", 
            FieldType.NUMERIC, 2);
        prap_Extract_File_Ex_Dt_Appl_Rec = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Dt_Appl_Rec", "EX-DT-APPL-REC", FieldType.NUMERIC, 
            8);
        prap_Extract_File_Ex_App_Source = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_App_Source", "EX-APP-SOURCE", FieldType.STRING, 1);
        prap_Extract_File_Ex_Orig_Iss_State = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Orig_Iss_State", "EX-ORIG-ISS-STATE", FieldType.STRING, 
            2);
        prap_Extract_File_Ex_Ownership_Type = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Ownership_Type", "EX-OWNERSHIP-TYPE", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Coll_State_Cd = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Coll_State_Cd", "EX-COLL-STATE-CD", FieldType.STRING, 
            2);
        prap_Extract_File_Ex_Split_Cd = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Split_Cd", "EX-SPLIT-CD", FieldType.STRING, 2);
        prap_Extract_File_Ex_Trans_Code = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Trans_Code", "EX-TRANS-CODE", FieldType.NUMERIC, 2);
        prap_Extract_File_Ex_Address_Field = prap_Extract_File.newFieldArrayInGroup("prap_Extract_File_Ex_Address_Field", "EX-ADDRESS-FIELD", FieldType.STRING, 
            35, new DbsArrayController(1,5));
        prap_Extract_File_Ex_Sthd_Rtn_Cd = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Sthd_Rtn_Cd", "EX-STHD-RTN-CD", FieldType.STRING, 2);
        prap_Extract_File_Ex_Final_List_Rd_Cd = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Final_List_Rd_Cd", "EX-FINAL-LIST-RD-CD", FieldType.STRING, 
            10);
        prap_Extract_File_Ex_Addr_Stnd_Code = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Addr_Stnd_Code", "EX-ADDR-STND-CODE", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Stndr_Overide = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Stndr_Overide", "EX-STNDR-OVERIDE", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Post_Data_Fileds = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Post_Data_Fileds", "EX-POST-DATA-FILEDS", FieldType.STRING, 
            44);
        prap_Extract_File_Ex_Addrss_Geogr_Cd = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Addrss_Geogr_Cd", "EX-ADDRSS-GEOGR-CD", FieldType.STRING, 
            2);
        prap_Extract_File_Ex_Lob = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Lob", "EX-LOB", FieldType.STRING, 1);
        prap_Extract_File_Ex_Lob_Type = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Lob_Type", "EX-LOB-TYPE", FieldType.STRING, 1);
        prap_Extract_File_Ex_Cor_Prefix = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Cor_Prefix", "EX-COR-PREFIX", FieldType.STRING, 8);
        prap_Extract_File_Ex_Cor_Last_Name = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Cor_Last_Name", "EX-COR-LAST-NAME", FieldType.STRING, 
            30);
        prap_Extract_File_Ex_Cor_First_Name = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Cor_First_Name", "EX-COR-FIRST-NAME", FieldType.STRING, 
            30);
        prap_Extract_File_Ex_Cor_Mddle_Name = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Cor_Mddle_Name", "EX-COR-MDDLE-NAME", FieldType.STRING, 
            30);
        prap_Extract_File_Ex_Cor_Suffix = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Cor_Suffix", "EX-COR-SUFFIX", FieldType.STRING, 8);
        prap_Extract_File_Ex_Cor_His_Ind = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Cor_His_Ind", "EX-COR-HIS-IND", FieldType.STRING, 1);
        prap_Extract_File_Ex_Cor_Unique_Id_Nbr = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Cor_Unique_Id_Nbr", "EX-COR-UNIQUE-ID-NBR", FieldType.NUMERIC, 
            12);
        prap_Extract_File_Ex_Net_Ind = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Net_Ind", "EX-NET-IND", FieldType.STRING, 1);
        prap_Extract_File_Ex_Ppg_Team = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Ppg_Team", "EX-PPG-TEAM", FieldType.STRING, 8);
        prap_Extract_File_Ex_Addr_Sync_Ind = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Addr_Sync_Ind", "EX-ADDR-SYNC-IND", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_G_Key = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_G_Key", "EX-G-KEY", FieldType.NUMERIC, 4);
        prap_Extract_File_Ex_G_Ind = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_G_Ind", "EX-G-IND", FieldType.NUMERIC, 1);
        prap_Extract_File_Ex_Name_Addr_Cd = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Name_Addr_Cd", "EX-NAME-ADDR-CD", FieldType.STRING, 
            5);
        prap_Extract_File_Ex_Dt_Ent_Sys = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Dt_Ent_Sys", "EX-DT-ENT-SYS", FieldType.NUMERIC, 8);
        prap_Extract_File_Ex_Dt_Released = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Dt_Released", "EX-DT-RELEASED", FieldType.NUMERIC, 
            8);
        prap_Extract_File_Ex_Dt_Matched = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Dt_Matched", "EX-DT-MATCHED", FieldType.NUMERIC, 8);
        prap_Extract_File_Ex_Dt_Deleted = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Dt_Deleted", "EX-DT-DELETED", FieldType.NUMERIC, 8);
        prap_Extract_File_Ex_Dt_Withdrawn = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Dt_Withdrawn", "EX-DT-WITHDRAWN", FieldType.NUMERIC, 
            8);
        prap_Extract_File_Ex_Alloc_Discr = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Alloc_Discr", "EX-ALLOC-DISCR", FieldType.NUMERIC, 
            1);
        prap_Extract_File_Ex_Release_Ind = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Release_Ind", "EX-RELEASE-IND", FieldType.NUMERIC, 
            1);
        prap_Extract_File_Ex_Other_Pols = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Other_Pols", "EX-OTHER-POLS", FieldType.STRING, 1);
        prap_Extract_File_Ex_Record_Type = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Record_Type", "EX-RECORD-TYPE", FieldType.NUMERIC, 
            1);
        prap_Extract_File_Ex_Status = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Status", "EX-STATUS", FieldType.STRING, 1);
        prap_Extract_File_Ex_Numb_Dailys = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Numb_Dailys", "EX-NUMB-DAILYS", FieldType.NUMERIC, 
            1);
        prap_Extract_File_Ex_City = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_City", "EX-CITY", FieldType.STRING, 27);
        prap_Extract_File_Ex_Current_State_Code = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Current_State_Code", "EX-CURRENT-STATE-CODE", 
            FieldType.STRING, 2);
        prap_Extract_File_Ex_Dana_Transaction_Cd = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Dana_Transaction_Cd", "EX-DANA-TRANSACTION-CD", 
            FieldType.STRING, 2);
        prap_Extract_File_Ex_Annuity_Start_Date = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Annuity_Start_Date", "EX-ANNUITY-START-DATE", 
            FieldType.STRING, 8);
        prap_Extract_File_Ex_Maximum_Alloc_Pct = prap_Extract_File.newGroupArrayInGroup("prap_Extract_File_Ex_Maximum_Alloc_Pct", "EX-MAXIMUM-ALLOC-PCT", 
            new DbsArrayController(1,100));
        prap_Extract_File_Ex_Max_Alloc_Pct = prap_Extract_File_Ex_Maximum_Alloc_Pct.newFieldInGroup("prap_Extract_File_Ex_Max_Alloc_Pct", "EX-MAX-ALLOC-PCT", 
            FieldType.STRING, 3);
        prap_Extract_File_Ex_Max_Alloc_Pct_Sign = prap_Extract_File_Ex_Maximum_Alloc_Pct.newFieldInGroup("prap_Extract_File_Ex_Max_Alloc_Pct_Sign", "EX-MAX-ALLOC-PCT-SIGN", 
            FieldType.STRING, 1);
        prap_Extract_File_Ex_Bene_Info_Type = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Bene_Info_Type", "EX-BENE-INFO-TYPE", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Primary_Std_Ent = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Primary_Std_Ent", "EX-PRIMARY-STD-ENT", FieldType.NUMERIC, 
            1);
        prap_Extract_File_Ex_Bene_Primary_Info = prap_Extract_File.newGroupArrayInGroup("prap_Extract_File_Ex_Bene_Primary_Info", "EX-BENE-PRIMARY-INFO", 
            new DbsArrayController(1,5));
        prap_Extract_File_Ex_Primary_Bene_Info = prap_Extract_File_Ex_Bene_Primary_Info.newFieldInGroup("prap_Extract_File_Ex_Primary_Bene_Info", "EX-PRIMARY-BENE-INFO", 
            FieldType.STRING, 72);
        prap_Extract_File_Ex_Contingent_Std_Ent = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Contingent_Std_Ent", "EX-CONTINGENT-STD-ENT", 
            FieldType.NUMERIC, 1);
        prap_Extract_File_Ex_Bene_Contingent_Info = prap_Extract_File.newGroupArrayInGroup("prap_Extract_File_Ex_Bene_Contingent_Info", "EX-BENE-CONTINGENT-INFO", 
            new DbsArrayController(1,5));
        prap_Extract_File_Ex_Contingent_Bene_Info = prap_Extract_File_Ex_Bene_Contingent_Info.newFieldInGroup("prap_Extract_File_Ex_Contingent_Bene_Info", 
            "EX-CONTINGENT-BENE-INFO", FieldType.STRING, 72);
        prap_Extract_File_Ex_Bene_Estate = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Bene_Estate", "EX-BENE-ESTATE", FieldType.STRING, 1);
        prap_Extract_File_Ex_Bene_Trust = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Bene_Trust", "EX-BENE-TRUST", FieldType.STRING, 1);
        prap_Extract_File_Ex_Bene_Category = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Bene_Category", "EX-BENE-CATEGORY", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Mail_Instructions = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Mail_Instructions", "EX-MAIL-INSTRUCTIONS", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Eop_Addl_Cref_Request = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Eop_Addl_Cref_Request", "EX-EOP-ADDL-CREF-REQUEST", 
            FieldType.STRING, 1);
        prap_Extract_File_Ex_Process_Status = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Process_Status", "EX-PROCESS-STATUS", FieldType.STRING, 
            4);
        prap_Extract_File_Ex_Csm_Sec_Seg = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Csm_Sec_Seg", "EX-CSM-SEC-SEG", FieldType.STRING, 4);
        prap_Extract_File_Ex_Rlc_College = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Rlc_College", "EX-RLC-COLLEGE", FieldType.STRING, 4);
        prap_Extract_File_Ex_Rcrd_Updt_Tm_Stamp = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Rcrd_Updt_Tm_Stamp", "EX-RCRD-UPDT-TM-STAMP", 
            FieldType.STRING, 8);
        prap_Extract_File_Ex_Contact_Mode = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Contact_Mode", "EX-CONTACT-MODE", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Racf_Id = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Racf_Id", "EX-RACF-ID", FieldType.STRING, 16);
        prap_Extract_File_Ex_Rqst_Log_Dte_Tm = prap_Extract_File.newGroupArrayInGroup("prap_Extract_File_Ex_Rqst_Log_Dte_Tm", "EX-RQST-LOG-DTE-TM", new 
            DbsArrayController(1,5));
        prap_Extract_File_Ex_Rqst_Log_Dte_Time = prap_Extract_File_Ex_Rqst_Log_Dte_Tm.newFieldInGroup("prap_Extract_File_Ex_Rqst_Log_Dte_Time", "EX-RQST-LOG-DTE-TIME", 
            FieldType.STRING, 15);
        prap_Extract_File_Ex_Sync_Ind = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Sync_Ind", "EX-SYNC-IND", FieldType.STRING, 1);
        prap_Extract_File_Ex_Cor_Ind = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Cor_Ind", "EX-COR-IND", FieldType.STRING, 1);
        prap_Extract_File_Ex_Alloc_Ind = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Alloc_Ind", "EX-ALLOC-IND", FieldType.STRING, 1);
        prap_Extract_File_Ex_Tiaa_Doi = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Tiaa_Doi", "EX-TIAA-DOI", FieldType.PACKED_DECIMAL, 4);
        prap_Extract_File_Ex_Cref_Doi = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Cref_Doi", "EX-CREF-DOI", FieldType.PACKED_DECIMAL, 4);
        prap_Extract_File_Ex_Mit_Request = prap_Extract_File.newGroupArrayInGroup("prap_Extract_File_Ex_Mit_Request", "EX-MIT-REQUEST", new DbsArrayController(1,
            5));
        prap_Extract_File_Ex_Mit_Unit = prap_Extract_File_Ex_Mit_Request.newFieldInGroup("prap_Extract_File_Ex_Mit_Unit", "EX-MIT-UNIT", FieldType.STRING, 
            8);
        prap_Extract_File_Ex_Mit_Wpid = prap_Extract_File_Ex_Mit_Request.newFieldInGroup("prap_Extract_File_Ex_Mit_Wpid", "EX-MIT-WPID", FieldType.STRING, 
            6);
        prap_Extract_File_Ex_Ira_Rollover_Type = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Ira_Rollover_Type", "EX-IRA-ROLLOVER-TYPE", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Ira_Record_Type = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Ira_Record_Type", "EX-IRA-RECORD-TYPE", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Mult_App_Status = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Mult_App_Status", "EX-MULT-APP-STATUS", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Mult_App_Lob = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Mult_App_Lob", "EX-MULT-APP-LOB", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Mult_App_Lob_Type = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Mult_App_Lob_Type", "EX-MULT-APP-LOB-TYPE", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Mult_App_Ppg = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Mult_App_Ppg", "EX-MULT-APP-PPG", FieldType.STRING, 
            6);
        prap_Extract_File_Ex_Print_Date = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Print_Date", "EX-PRINT-DATE", FieldType.NUMERIC, 8);
        prap_Extract_File_Ex_Cntrct_Info = prap_Extract_File.newGroupArrayInGroup("prap_Extract_File_Ex_Cntrct_Info", "EX-CNTRCT-INFO", new DbsArrayController(1,
            15));
        prap_Extract_File_Ex_Cntrct_Type = prap_Extract_File_Ex_Cntrct_Info.newFieldInGroup("prap_Extract_File_Ex_Cntrct_Type", "EX-CNTRCT-TYPE", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Cntrct_Nbr = prap_Extract_File_Ex_Cntrct_Info.newFieldInGroup("prap_Extract_File_Ex_Cntrct_Nbr", "EX-CNTRCT-NBR", FieldType.STRING, 
            10);
        prap_Extract_File_Ex_Cntrct_Proceeds_Amt = prap_Extract_File_Ex_Cntrct_Info.newFieldInGroup("prap_Extract_File_Ex_Cntrct_Proceeds_Amt", "EX-CNTRCT-PROCEEDS-AMT", 
            FieldType.DECIMAL, 10,2);
        prap_Extract_File_Ex_Addr_Info = prap_Extract_File.newGroupArrayInGroup("prap_Extract_File_Ex_Addr_Info", "EX-ADDR-INFO", new DbsArrayController(1,
            5));
        prap_Extract_File_Ex_Address_Txt = prap_Extract_File_Ex_Addr_Info.newFieldInGroup("prap_Extract_File_Ex_Address_Txt", "EX-ADDRESS-TXT", FieldType.STRING, 
            35);
        prap_Extract_File_Ex_Address_Dest_Name = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Address_Dest_Name", "EX-ADDRESS-DEST-NAME", FieldType.STRING, 
            35);
        prap_Extract_File_Ex_Bank_Pymnt_Acct_Nmbr = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Bank_Pymnt_Acct_Nmbr", "EX-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 21);
        prap_Extract_File_Ex_Bank_Aba_Acct_Nmbr = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Bank_Aba_Acct_Nmbr", "EX-BANK-ABA-ACCT-NMBR", 
            FieldType.STRING, 9);
        prap_Extract_File_Ex_Addr_Usage_Code = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Addr_Usage_Code", "EX-ADDR-USAGE-CODE", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Init_Paymt_Amt = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Init_Paymt_Amt", "EX-INIT-PAYMT-AMT", FieldType.DECIMAL, 
            10,2);
        prap_Extract_File_Ex_Init_Paymt_Pct = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Init_Paymt_Pct", "EX-INIT-PAYMT-PCT", FieldType.NUMERIC, 
            2);
        prap_Extract_File_Ex_Financial_1 = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Financial_1", "EX-FINANCIAL-1", FieldType.DECIMAL, 
            10,2);
        prap_Extract_File_Ex_Financial_2 = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Financial_2", "EX-FINANCIAL-2", FieldType.DECIMAL, 
            10,2);
        prap_Extract_File_Ex_Financial_3 = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Financial_3", "EX-FINANCIAL-3", FieldType.DECIMAL, 
            10,2);
        prap_Extract_File_Ex_Financial_4 = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Financial_4", "EX-FINANCIAL-4", FieldType.DECIMAL, 
            10,2);
        prap_Extract_File_Ex_Financial_5 = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Financial_5", "EX-FINANCIAL-5", FieldType.DECIMAL, 
            10,2);
        prap_Extract_File_Ex_Irc_Sectn_Grp_Cde = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Irc_Sectn_Grp_Cde", "EX-IRC-SECTN-GRP-CDE", FieldType.NUMERIC, 
            2);
        prap_Extract_File_Ex_Irc_Sectn_Cde = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Irc_Sectn_Cde", "EX-IRC-SECTN-CDE", FieldType.NUMERIC, 
            2);
        prap_Extract_File_Ex_Inst_Link_Cde = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Inst_Link_Cde", "EX-INST-LINK-CDE", FieldType.NUMERIC, 
            6);
        prap_Extract_File_Ex_Applcnt_Req_Type = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Applcnt_Req_Type", "EX-APPLCNT-REQ-TYPE", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Tiaa_Service_Agent = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Tiaa_Service_Agent", "EX-TIAA-SERVICE-AGENT", 
            FieldType.STRING, 1);
        prap_Extract_File_Ex_Prap_Rsch_Mit_Ind = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Prap_Rsch_Mit_Ind", "EX-PRAP-RSCH-MIT-IND", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Rlc_Cref_Cert = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Rlc_Cref_Cert", "EX-RLC-CREF-CERT", FieldType.STRING, 
            10);
        prap_Extract_File_Ex_Rlc_Cref_CertRedef8 = prap_Extract_File.newGroupInGroup("prap_Extract_File_Ex_Rlc_Cref_CertRedef8", "Redefines", prap_Extract_File_Ex_Rlc_Cref_Cert);
        prap_Extract_File_Ex_Rlc_Cref_Pref = prap_Extract_File_Ex_Rlc_Cref_CertRedef8.newFieldInGroup("prap_Extract_File_Ex_Rlc_Cref_Pref", "EX-RLC-CREF-PREF", 
            FieldType.STRING, 1);
        prap_Extract_File_Ex_Rlc_Cref_Cont = prap_Extract_File_Ex_Rlc_Cref_CertRedef8.newFieldInGroup("prap_Extract_File_Ex_Rlc_Cref_Cont", "EX-RLC-CREF-CONT", 
            FieldType.STRING, 7);
        prap_Extract_File_Ex_Allocation_Model_Type = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Allocation_Model_Type", "EX-ALLOCATION-MODEL-TYPE", 
            FieldType.STRING, 2);
        prap_Extract_File_Ex_Divorce_Ind = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Divorce_Ind", "EX-DIVORCE-IND", FieldType.STRING, 1);
        prap_Extract_File_Ex_Email_Address = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Email_Address", "EX-EMAIL-ADDRESS", FieldType.STRING, 
            50);
        prap_Extract_File_Ex_E_Signed_Appl_Ind = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_E_Signed_Appl_Ind", "EX-E-SIGNED-APPL-IND", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Eft_Requested_Ind = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Eft_Requested_Ind", "EX-EFT-REQUESTED-IND", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Prap_Prem_Rsch_Ind = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Prap_Prem_Rsch_Ind", "EX-PRAP-PREM-RSCH-IND", 
            FieldType.STRING, 1);
        prap_Extract_File_Ex_Address_Change_Ind = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Address_Change_Ind", "EX-ADDRESS-CHANGE-IND", 
            FieldType.STRING, 1);
        prap_Extract_File_Ex_Allocation_Fmt = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Allocation_Fmt", "EX-ALLOCATION-FMT", FieldType.STRING, 
            1);
        prap_Extract_File_Ex_Phone_No = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Phone_No", "EX-PHONE-NO", FieldType.STRING, 20);
        prap_Extract_File_Ex_Fund_Cde = prap_Extract_File.newFieldArrayInGroup("prap_Extract_File_Ex_Fund_Cde", "EX-FUND-CDE", FieldType.STRING, 10, new 
            DbsArrayController(1,100));
        prap_Extract_File_Ex_Allocation_Pct = prap_Extract_File.newFieldArrayInGroup("prap_Extract_File_Ex_Allocation_Pct", "EX-ALLOCATION-PCT", FieldType.NUMERIC, 
            3, new DbsArrayController(1,100));
        prap_Extract_File_Ex_Sg_Plan_No = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Sg_Plan_No", "EX-SG-PLAN-NO", FieldType.STRING, 6);
        prap_Extract_File_Ex_Sg_Subplan_No = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Sg_Subplan_No", "EX-SG-SUBPLAN-NO", FieldType.STRING, 
            6);
        prap_Extract_File_Ex_Sg_Part_Ext = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Sg_Part_Ext", "EX-SG-PART-EXT", FieldType.STRING, 2);
        prap_Extract_File_Ex_Filler = prap_Extract_File.newFieldInGroup("prap_Extract_File_Ex_Filler", "EX-FILLER", FieldType.STRING, 25);
        prap_Extract_FileRedef9 = newGroupInRecord("prap_Extract_FileRedef9", "Redefines", prap_Extract_File);
        prap_Extract_File_Ex_Data_1 = prap_Extract_FileRedef9.newFieldInGroup("prap_Extract_File_Ex_Data_1", "EX-DATA-1", FieldType.STRING, 250);
        prap_Extract_File_Ex_Data_2 = prap_Extract_FileRedef9.newFieldInGroup("prap_Extract_File_Ex_Data_2", "EX-DATA-2", FieldType.STRING, 250);
        prap_Extract_File_Ex_Data_3 = prap_Extract_FileRedef9.newFieldInGroup("prap_Extract_File_Ex_Data_3", "EX-DATA-3", FieldType.STRING, 250);
        prap_Extract_File_Ex_Data_4 = prap_Extract_FileRedef9.newFieldInGroup("prap_Extract_File_Ex_Data_4", "EX-DATA-4", FieldType.STRING, 250);
        prap_Extract_File_Ex_Data_5 = prap_Extract_FileRedef9.newFieldInGroup("prap_Extract_File_Ex_Data_5", "EX-DATA-5", FieldType.STRING, 250);
        prap_Extract_File_Ex_Data_6 = prap_Extract_FileRedef9.newFieldInGroup("prap_Extract_File_Ex_Data_6", "EX-DATA-6", FieldType.STRING, 250);
        prap_Extract_File_Ex_Data_7 = prap_Extract_FileRedef9.newFieldInGroup("prap_Extract_File_Ex_Data_7", "EX-DATA-7", FieldType.STRING, 250);
        prap_Extract_File_Ex_Data_8 = prap_Extract_FileRedef9.newFieldInGroup("prap_Extract_File_Ex_Data_8", "EX-DATA-8", FieldType.STRING, 250);
        prap_Extract_File_Ex_Data_9 = prap_Extract_FileRedef9.newFieldInGroup("prap_Extract_File_Ex_Data_9", "EX-DATA-9", FieldType.STRING, 250);
        prap_Extract_File_Ex_Data_10 = prap_Extract_FileRedef9.newFieldInGroup("prap_Extract_File_Ex_Data_10", "EX-DATA-10", FieldType.STRING, 250);
        prap_Extract_File_Ex_Data_11 = prap_Extract_FileRedef9.newFieldInGroup("prap_Extract_File_Ex_Data_11", "EX-DATA-11", FieldType.STRING, 250);
        prap_Extract_File_Ex_Data_12 = prap_Extract_FileRedef9.newFieldInGroup("prap_Extract_File_Ex_Data_12", "EX-DATA-12", FieldType.STRING, 250);
        prap_Extract_File_Ex_Data_13 = prap_Extract_FileRedef9.newFieldInGroup("prap_Extract_File_Ex_Data_13", "EX-DATA-13", FieldType.STRING, 250);
        prap_Extract_File_Ex_Data_14 = prap_Extract_FileRedef9.newFieldInGroup("prap_Extract_File_Ex_Data_14", "EX-DATA-14", FieldType.STRING, 250);
        prap_Extract_File_Ex_Data_15 = prap_Extract_FileRedef9.newFieldInGroup("prap_Extract_File_Ex_Data_15", "EX-DATA-15", FieldType.STRING, 250);
        prap_Extract_File_Ex_Data_16 = prap_Extract_FileRedef9.newFieldInGroup("prap_Extract_File_Ex_Data_16", "EX-DATA-16", FieldType.STRING, 250);

        this.setRecordName("LdaAppl220");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAppl220() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
