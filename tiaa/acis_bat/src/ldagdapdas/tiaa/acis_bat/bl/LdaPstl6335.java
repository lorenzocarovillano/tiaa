/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:10:39 PM
**        * FROM NATURAL LDA     : PSTL6335
************************************************************
**        * FILE NAME            : LdaPstl6335.java
**        * CLASS NAME           : LdaPstl6335
**        * INSTANCE NAME        : LdaPstl6335
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaPstl6335 extends DbsRecord
{
    // Properties
    private DbsGroup sort_Key;
    private DbsField sort_Key_Sort_Key_Lgth;
    private DbsField sort_Key_Sort_Key_Array;
    private DbsGroup pstl6335_Data;
    private DbsField pstl6335_Data_Pstl6335_Rec_Id;
    private DbsField pstl6335_Data_Pstl6335_Data_Lgth;
    private DbsField pstl6335_Data_Pstl6335_Data_Occurs;
    private DbsField pstl6335_Data_Pstl6335_Data_Array;
    private DbsGroup pstl6335_Data_Pstl6335_Data_ArrayRedef1;
    private DbsGroup pstl6335_Data_Prap_Letter_Data;
    private DbsField pstl6335_Data_Prap_Letter_Type_Ind;
    private DbsField pstl6335_Data_Prap_Letter_End_Date;
    private DbsField pstl6335_Data_Prap_Letter_Cutoff_Date;
    private DbsField pstl6335_Data_Prap_Letter_Inst_Name;
    private DbsField pstl6335_Data_Prap_Letter_Report_Count;
    private DbsGroup pstl6335_Data_Prap_Letter_Report_Detail;
    private DbsField pstl6335_Data_Prap_Letter_Detail_Newpage_Ind;
    private DbsField pstl6335_Data_Prap_Letter_Detail_Daysafter;
    private DbsField pstl6335_Data_Prap_Letter_Detail_Ppg;
    private DbsField pstl6335_Data_Prap_Letter_Detail_Div_Sub;
    private DbsField pstl6335_Data_Prap_Letter_Detail_Cntrtype_Nbr;
    private DbsGroup pstl6335_Data_Prap_Letter_Detail_Cntrtype_NbrRedef2;
    private DbsField pstl6335_Data_Prap_Letter_Detail_Cntrtype;
    private DbsField pstl6335_Data_Prap_Letter_Detail_Cntrnbr;
    private DbsField pstl6335_Data_Prap_Letter_Detail_Empl_Name;
    private DbsField pstl6335_Data_Prap_Letter_Detail_Socsec_Nbr;

    public DbsGroup getSort_Key() { return sort_Key; }

    public DbsField getSort_Key_Sort_Key_Lgth() { return sort_Key_Sort_Key_Lgth; }

    public DbsField getSort_Key_Sort_Key_Array() { return sort_Key_Sort_Key_Array; }

    public DbsGroup getPstl6335_Data() { return pstl6335_Data; }

    public DbsField getPstl6335_Data_Pstl6335_Rec_Id() { return pstl6335_Data_Pstl6335_Rec_Id; }

    public DbsField getPstl6335_Data_Pstl6335_Data_Lgth() { return pstl6335_Data_Pstl6335_Data_Lgth; }

    public DbsField getPstl6335_Data_Pstl6335_Data_Occurs() { return pstl6335_Data_Pstl6335_Data_Occurs; }

    public DbsField getPstl6335_Data_Pstl6335_Data_Array() { return pstl6335_Data_Pstl6335_Data_Array; }

    public DbsGroup getPstl6335_Data_Pstl6335_Data_ArrayRedef1() { return pstl6335_Data_Pstl6335_Data_ArrayRedef1; }

    public DbsGroup getPstl6335_Data_Prap_Letter_Data() { return pstl6335_Data_Prap_Letter_Data; }

    public DbsField getPstl6335_Data_Prap_Letter_Type_Ind() { return pstl6335_Data_Prap_Letter_Type_Ind; }

    public DbsField getPstl6335_Data_Prap_Letter_End_Date() { return pstl6335_Data_Prap_Letter_End_Date; }

    public DbsField getPstl6335_Data_Prap_Letter_Cutoff_Date() { return pstl6335_Data_Prap_Letter_Cutoff_Date; }

    public DbsField getPstl6335_Data_Prap_Letter_Inst_Name() { return pstl6335_Data_Prap_Letter_Inst_Name; }

    public DbsField getPstl6335_Data_Prap_Letter_Report_Count() { return pstl6335_Data_Prap_Letter_Report_Count; }

    public DbsGroup getPstl6335_Data_Prap_Letter_Report_Detail() { return pstl6335_Data_Prap_Letter_Report_Detail; }

    public DbsField getPstl6335_Data_Prap_Letter_Detail_Newpage_Ind() { return pstl6335_Data_Prap_Letter_Detail_Newpage_Ind; }

    public DbsField getPstl6335_Data_Prap_Letter_Detail_Daysafter() { return pstl6335_Data_Prap_Letter_Detail_Daysafter; }

    public DbsField getPstl6335_Data_Prap_Letter_Detail_Ppg() { return pstl6335_Data_Prap_Letter_Detail_Ppg; }

    public DbsField getPstl6335_Data_Prap_Letter_Detail_Div_Sub() { return pstl6335_Data_Prap_Letter_Detail_Div_Sub; }

    public DbsField getPstl6335_Data_Prap_Letter_Detail_Cntrtype_Nbr() { return pstl6335_Data_Prap_Letter_Detail_Cntrtype_Nbr; }

    public DbsGroup getPstl6335_Data_Prap_Letter_Detail_Cntrtype_NbrRedef2() { return pstl6335_Data_Prap_Letter_Detail_Cntrtype_NbrRedef2; }

    public DbsField getPstl6335_Data_Prap_Letter_Detail_Cntrtype() { return pstl6335_Data_Prap_Letter_Detail_Cntrtype; }

    public DbsField getPstl6335_Data_Prap_Letter_Detail_Cntrnbr() { return pstl6335_Data_Prap_Letter_Detail_Cntrnbr; }

    public DbsField getPstl6335_Data_Prap_Letter_Detail_Empl_Name() { return pstl6335_Data_Prap_Letter_Detail_Empl_Name; }

    public DbsField getPstl6335_Data_Prap_Letter_Detail_Socsec_Nbr() { return pstl6335_Data_Prap_Letter_Detail_Socsec_Nbr; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        sort_Key = newGroupInRecord("sort_Key", "SORT-KEY");
        sort_Key_Sort_Key_Lgth = sort_Key.newFieldInGroup("sort_Key_Sort_Key_Lgth", "SORT-KEY-LGTH", FieldType.NUMERIC, 3);
        sort_Key_Sort_Key_Array = sort_Key.newFieldArrayInGroup("sort_Key_Sort_Key_Array", "SORT-KEY-ARRAY", FieldType.STRING, 1, new DbsArrayController(1,
            1));

        pstl6335_Data = newGroupInRecord("pstl6335_Data", "PSTL6335-DATA");
        pstl6335_Data_Pstl6335_Rec_Id = pstl6335_Data.newFieldInGroup("pstl6335_Data_Pstl6335_Rec_Id", "PSTL6335-REC-ID", FieldType.STRING, 2);
        pstl6335_Data_Pstl6335_Data_Lgth = pstl6335_Data.newFieldInGroup("pstl6335_Data_Pstl6335_Data_Lgth", "PSTL6335-DATA-LGTH", FieldType.NUMERIC, 
            5);
        pstl6335_Data_Pstl6335_Data_Occurs = pstl6335_Data.newFieldInGroup("pstl6335_Data_Pstl6335_Data_Occurs", "PSTL6335-DATA-OCCURS", FieldType.NUMERIC, 
            5);
        pstl6335_Data_Pstl6335_Data_Array = pstl6335_Data.newFieldArrayInGroup("pstl6335_Data_Pstl6335_Data_Array", "PSTL6335-DATA-ARRAY", FieldType.STRING, 
            1, new DbsArrayController(1,4475));
        pstl6335_Data_Pstl6335_Data_ArrayRedef1 = pstl6335_Data.newGroupInGroup("pstl6335_Data_Pstl6335_Data_ArrayRedef1", "Redefines", pstl6335_Data_Pstl6335_Data_Array);
        pstl6335_Data_Prap_Letter_Data = pstl6335_Data_Pstl6335_Data_ArrayRedef1.newGroupInGroup("pstl6335_Data_Prap_Letter_Data", "PRAP-LETTER-DATA");
        pstl6335_Data_Prap_Letter_Type_Ind = pstl6335_Data_Prap_Letter_Data.newFieldInGroup("pstl6335_Data_Prap_Letter_Type_Ind", "PRAP-LETTER-TYPE-IND", 
            FieldType.STRING, 1);
        pstl6335_Data_Prap_Letter_End_Date = pstl6335_Data_Prap_Letter_Data.newFieldInGroup("pstl6335_Data_Prap_Letter_End_Date", "PRAP-LETTER-END-DATE", 
            FieldType.STRING, 10);
        pstl6335_Data_Prap_Letter_Cutoff_Date = pstl6335_Data_Prap_Letter_Data.newFieldInGroup("pstl6335_Data_Prap_Letter_Cutoff_Date", "PRAP-LETTER-CUTOFF-DATE", 
            FieldType.STRING, 10);
        pstl6335_Data_Prap_Letter_Inst_Name = pstl6335_Data_Prap_Letter_Data.newFieldInGroup("pstl6335_Data_Prap_Letter_Inst_Name", "PRAP-LETTER-INST-NAME", 
            FieldType.STRING, 50);
        pstl6335_Data_Prap_Letter_Report_Count = pstl6335_Data_Prap_Letter_Data.newFieldInGroup("pstl6335_Data_Prap_Letter_Report_Count", "PRAP-LETTER-REPORT-COUNT", 
            FieldType.NUMERIC, 2);
        pstl6335_Data_Prap_Letter_Report_Detail = pstl6335_Data_Prap_Letter_Data.newGroupArrayInGroup("pstl6335_Data_Prap_Letter_Report_Detail", "PRAP-LETTER-REPORT-DETAIL", 
            new DbsArrayController(1,58));
        pstl6335_Data_Prap_Letter_Detail_Newpage_Ind = pstl6335_Data_Prap_Letter_Report_Detail.newFieldInGroup("pstl6335_Data_Prap_Letter_Detail_Newpage_Ind", 
            "PRAP-LETTER-DETAIL-NEWPAGE-IND", FieldType.STRING, 1);
        pstl6335_Data_Prap_Letter_Detail_Daysafter = pstl6335_Data_Prap_Letter_Report_Detail.newFieldInGroup("pstl6335_Data_Prap_Letter_Detail_Daysafter", 
            "PRAP-LETTER-DETAIL-DAYSAFTER", FieldType.STRING, 3);
        pstl6335_Data_Prap_Letter_Detail_Ppg = pstl6335_Data_Prap_Letter_Report_Detail.newFieldInGroup("pstl6335_Data_Prap_Letter_Detail_Ppg", "PRAP-LETTER-DETAIL-PPG", 
            FieldType.STRING, 6);
        pstl6335_Data_Prap_Letter_Detail_Div_Sub = pstl6335_Data_Prap_Letter_Report_Detail.newFieldInGroup("pstl6335_Data_Prap_Letter_Detail_Div_Sub", 
            "PRAP-LETTER-DETAIL-DIV-SUB", FieldType.STRING, 4);
        pstl6335_Data_Prap_Letter_Detail_Cntrtype_Nbr = pstl6335_Data_Prap_Letter_Report_Detail.newFieldInGroup("pstl6335_Data_Prap_Letter_Detail_Cntrtype_Nbr", 
            "PRAP-LETTER-DETAIL-CNTRTYPE-NBR", FieldType.STRING, 15);
        pstl6335_Data_Prap_Letter_Detail_Cntrtype_NbrRedef2 = pstl6335_Data_Prap_Letter_Report_Detail.newGroupInGroup("pstl6335_Data_Prap_Letter_Detail_Cntrtype_NbrRedef2", 
            "Redefines", pstl6335_Data_Prap_Letter_Detail_Cntrtype_Nbr);
        pstl6335_Data_Prap_Letter_Detail_Cntrtype = pstl6335_Data_Prap_Letter_Detail_Cntrtype_NbrRedef2.newFieldInGroup("pstl6335_Data_Prap_Letter_Detail_Cntrtype", 
            "PRAP-LETTER-DETAIL-CNTRTYPE", FieldType.STRING, 5);
        pstl6335_Data_Prap_Letter_Detail_Cntrnbr = pstl6335_Data_Prap_Letter_Detail_Cntrtype_NbrRedef2.newFieldInGroup("pstl6335_Data_Prap_Letter_Detail_Cntrnbr", 
            "PRAP-LETTER-DETAIL-CNTRNBR", FieldType.STRING, 10);
        pstl6335_Data_Prap_Letter_Detail_Empl_Name = pstl6335_Data_Prap_Letter_Report_Detail.newFieldInGroup("pstl6335_Data_Prap_Letter_Detail_Empl_Name", 
            "PRAP-LETTER-DETAIL-EMPL-NAME", FieldType.STRING, 35);
        pstl6335_Data_Prap_Letter_Detail_Socsec_Nbr = pstl6335_Data_Prap_Letter_Report_Detail.newFieldInGroup("pstl6335_Data_Prap_Letter_Detail_Socsec_Nbr", 
            "PRAP-LETTER-DETAIL-SOCSEC-NBR", FieldType.STRING, 11);

        this.setRecordName("LdaPstl6335");
    }

    public void initializeValues() throws Exception
    {
        reset();
        sort_Key_Sort_Key_Lgth.setInitialValue(1);
        pstl6335_Data_Pstl6335_Rec_Id.setInitialValue("a");
        pstl6335_Data_Pstl6335_Data_Lgth.setInitialValue(4475);
        pstl6335_Data_Pstl6335_Data_Occurs.setInitialValue(1);
    }

    // Constructor
    public LdaPstl6335() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
