/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:53 PM
**        * FROM NATURAL LDA     : APPL165
************************************************************
**        * FILE NAME            : LdaAppl165.java
**        * CLASS NAME           : LdaAppl165
**        * INSTANCE NAME        : LdaAppl165
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl165 extends DbsRecord
{
    // Properties
    private DbsGroup appl165;
    private DbsField appl165_Plan;
    private DbsGroup appl165_PlanRedef1;
    private DbsField appl165_Plan_Num;
    private DbsField appl165_Divsub;
    private DbsField appl165_Tiaa_Contract;
    private DbsField appl165_Pin;
    private DbsField appl165_Ssn;
    private DbsGroup appl165_SsnRedef2;
    private DbsField appl165_Ssn_Alpha;
    private DbsField appl165_Full_Name;
    private DbsField appl165_Birthdate_Mmddyyyy;
    private DbsGroup appl165_Birthdate_MmddyyyyRedef3;
    private DbsField appl165_Birthdate_Mmddyyyy_Num;
    private DbsField appl165_Alloc_Fund_Cde;
    private DbsField appl165_Alloc_Pct;
    private DbsField appl165_Email_Address;
    private DbsField appl165_Address;
    private DbsField appl165_Zip;
    private DbsField appl165_Restart_Date;
    private DbsField appl165_Restart_Time;
    private DbsField appl165_Restart_Record_Number;

    public DbsGroup getAppl165() { return appl165; }

    public DbsField getAppl165_Plan() { return appl165_Plan; }

    public DbsGroup getAppl165_PlanRedef1() { return appl165_PlanRedef1; }

    public DbsField getAppl165_Plan_Num() { return appl165_Plan_Num; }

    public DbsField getAppl165_Divsub() { return appl165_Divsub; }

    public DbsField getAppl165_Tiaa_Contract() { return appl165_Tiaa_Contract; }

    public DbsField getAppl165_Pin() { return appl165_Pin; }

    public DbsField getAppl165_Ssn() { return appl165_Ssn; }

    public DbsGroup getAppl165_SsnRedef2() { return appl165_SsnRedef2; }

    public DbsField getAppl165_Ssn_Alpha() { return appl165_Ssn_Alpha; }

    public DbsField getAppl165_Full_Name() { return appl165_Full_Name; }

    public DbsField getAppl165_Birthdate_Mmddyyyy() { return appl165_Birthdate_Mmddyyyy; }

    public DbsGroup getAppl165_Birthdate_MmddyyyyRedef3() { return appl165_Birthdate_MmddyyyyRedef3; }

    public DbsField getAppl165_Birthdate_Mmddyyyy_Num() { return appl165_Birthdate_Mmddyyyy_Num; }

    public DbsField getAppl165_Alloc_Fund_Cde() { return appl165_Alloc_Fund_Cde; }

    public DbsField getAppl165_Alloc_Pct() { return appl165_Alloc_Pct; }

    public DbsField getAppl165_Email_Address() { return appl165_Email_Address; }

    public DbsField getAppl165_Address() { return appl165_Address; }

    public DbsField getAppl165_Zip() { return appl165_Zip; }

    public DbsField getAppl165_Restart_Date() { return appl165_Restart_Date; }

    public DbsField getAppl165_Restart_Time() { return appl165_Restart_Time; }

    public DbsField getAppl165_Restart_Record_Number() { return appl165_Restart_Record_Number; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        appl165 = newGroupInRecord("appl165", "APPL165");
        appl165_Plan = appl165.newFieldInGroup("appl165_Plan", "PLAN", FieldType.STRING, 6);
        appl165_PlanRedef1 = appl165.newGroupInGroup("appl165_PlanRedef1", "Redefines", appl165_Plan);
        appl165_Plan_Num = appl165_PlanRedef1.newFieldInGroup("appl165_Plan_Num", "PLAN-NUM", FieldType.NUMERIC, 6);
        appl165_Divsub = appl165.newFieldInGroup("appl165_Divsub", "DIVSUB", FieldType.STRING, 4);
        appl165_Tiaa_Contract = appl165.newFieldInGroup("appl165_Tiaa_Contract", "TIAA-CONTRACT", FieldType.STRING, 8);
        appl165_Pin = appl165.newFieldInGroup("appl165_Pin", "PIN", FieldType.NUMERIC, 12);
        appl165_Ssn = appl165.newFieldInGroup("appl165_Ssn", "SSN", FieldType.NUMERIC, 9);
        appl165_SsnRedef2 = appl165.newGroupInGroup("appl165_SsnRedef2", "Redefines", appl165_Ssn);
        appl165_Ssn_Alpha = appl165_SsnRedef2.newFieldInGroup("appl165_Ssn_Alpha", "SSN-ALPHA", FieldType.STRING, 9);
        appl165_Full_Name = appl165.newFieldInGroup("appl165_Full_Name", "FULL-NAME", FieldType.STRING, 40);
        appl165_Birthdate_Mmddyyyy = appl165.newFieldInGroup("appl165_Birthdate_Mmddyyyy", "BIRTHDATE-MMDDYYYY", FieldType.STRING, 8);
        appl165_Birthdate_MmddyyyyRedef3 = appl165.newGroupInGroup("appl165_Birthdate_MmddyyyyRedef3", "Redefines", appl165_Birthdate_Mmddyyyy);
        appl165_Birthdate_Mmddyyyy_Num = appl165_Birthdate_MmddyyyyRedef3.newFieldInGroup("appl165_Birthdate_Mmddyyyy_Num", "BIRTHDATE-MMDDYYYY-NUM", 
            FieldType.NUMERIC, 8);
        appl165_Alloc_Fund_Cde = appl165.newFieldArrayInGroup("appl165_Alloc_Fund_Cde", "ALLOC-FUND-CDE", FieldType.STRING, 10, new DbsArrayController(1,
            100));
        appl165_Alloc_Pct = appl165.newFieldArrayInGroup("appl165_Alloc_Pct", "ALLOC-PCT", FieldType.NUMERIC, 3, new DbsArrayController(1,100));
        appl165_Email_Address = appl165.newFieldInGroup("appl165_Email_Address", "EMAIL-ADDRESS", FieldType.STRING, 50);
        appl165_Address = appl165.newFieldArrayInGroup("appl165_Address", "ADDRESS", FieldType.STRING, 35, new DbsArrayController(1,5));
        appl165_Zip = appl165.newFieldInGroup("appl165_Zip", "ZIP", FieldType.STRING, 5);
        appl165_Restart_Date = appl165.newFieldInGroup("appl165_Restart_Date", "RESTART-DATE", FieldType.STRING, 10);
        appl165_Restart_Time = appl165.newFieldInGroup("appl165_Restart_Time", "RESTART-TIME", FieldType.STRING, 5);
        appl165_Restart_Record_Number = appl165.newFieldInGroup("appl165_Restart_Record_Number", "RESTART-RECORD-NUMBER", FieldType.NUMERIC, 9);

        this.setRecordName("LdaAppl165");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAppl165() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
