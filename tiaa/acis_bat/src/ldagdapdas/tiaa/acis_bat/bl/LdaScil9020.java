/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:11:57 PM
**        * FROM NATURAL LDA     : SCIL9020
************************************************************
**        * FILE NAME            : LdaScil9020.java
**        * CLASS NAME           : LdaScil9020
**        * INSTANCE NAME        : LdaScil9020
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaScil9020 extends DbsRecord
{
    // Properties
    private DbsField pnd_Powerimage_File;
    private DbsGroup pnd_Powerimage_FileRedef1;
    private DbsField pnd_Powerimage_File_Pnd_Pi_Log_Task_Type;
    private DbsField pnd_Powerimage_File_Pnd_Pi_Sgrd_Plan_No;
    private DbsField pnd_Powerimage_File_Pnd_Pi_Ssn;
    private DbsField pnd_Powerimage_File_Pnd_Pi_Sgrd_Subplan_No;
    private DbsField pnd_Powerimage_File_Pnd_Pi_Acis_Error_No;
    private DbsField pnd_Powerimage_File_Pnd_Pi_Acis_Error_Msg;
    private DbsField pnd_Powerimage_File_Pnd_Pi_Orchestration_Id;
    private DbsField pnd_Powerimage_File_Pnd_Pi_Filler;
    private DbsGroup pnd_Powerimage_FileRedef2;
    private DbsField pnd_Powerimage_File_Pnd_Pi_Log_Task_Type_2;
    private DbsField pnd_Powerimage_File_Pnd_Pi_Sgrd_Plan_No_2;
    private DbsField pnd_Powerimage_File_Pnd_Pi_Ssn_2;
    private DbsField pnd_Powerimage_File_Pnd_Pi_Sgrd_Subplan_No_2;
    private DbsField pnd_Powerimage_File_Pnd_Pi_Omni_Folder_2;
    private DbsField pnd_Powerimage_File_Pnd_Pi_Divorce_Ind_2;
    private DbsField pnd_Powerimage_File_Pnd_Pi_Beneinplan_Ind_2;
    private DbsField pnd_Powerimage_File_Pnd_Pi_Enroll_Method_2;
    private DbsField pnd_Powerimage_File_Pnd_Pi_Acis_Error_No_2;
    private DbsField pnd_Powerimage_File_Pnd_Pi_Acis_Error_Msg_2;
    private DbsField pnd_Powerimage_File_Pnd_Pi_Orchestration_Id_2;
    private DbsField pnd_Powerimage_File_Pnd_Pi_Filler_2;

    public DbsField getPnd_Powerimage_File() { return pnd_Powerimage_File; }

    public DbsGroup getPnd_Powerimage_FileRedef1() { return pnd_Powerimage_FileRedef1; }

    public DbsField getPnd_Powerimage_File_Pnd_Pi_Log_Task_Type() { return pnd_Powerimage_File_Pnd_Pi_Log_Task_Type; }

    public DbsField getPnd_Powerimage_File_Pnd_Pi_Sgrd_Plan_No() { return pnd_Powerimage_File_Pnd_Pi_Sgrd_Plan_No; }

    public DbsField getPnd_Powerimage_File_Pnd_Pi_Ssn() { return pnd_Powerimage_File_Pnd_Pi_Ssn; }

    public DbsField getPnd_Powerimage_File_Pnd_Pi_Sgrd_Subplan_No() { return pnd_Powerimage_File_Pnd_Pi_Sgrd_Subplan_No; }

    public DbsField getPnd_Powerimage_File_Pnd_Pi_Acis_Error_No() { return pnd_Powerimage_File_Pnd_Pi_Acis_Error_No; }

    public DbsField getPnd_Powerimage_File_Pnd_Pi_Acis_Error_Msg() { return pnd_Powerimage_File_Pnd_Pi_Acis_Error_Msg; }

    public DbsField getPnd_Powerimage_File_Pnd_Pi_Orchestration_Id() { return pnd_Powerimage_File_Pnd_Pi_Orchestration_Id; }

    public DbsField getPnd_Powerimage_File_Pnd_Pi_Filler() { return pnd_Powerimage_File_Pnd_Pi_Filler; }

    public DbsGroup getPnd_Powerimage_FileRedef2() { return pnd_Powerimage_FileRedef2; }

    public DbsField getPnd_Powerimage_File_Pnd_Pi_Log_Task_Type_2() { return pnd_Powerimage_File_Pnd_Pi_Log_Task_Type_2; }

    public DbsField getPnd_Powerimage_File_Pnd_Pi_Sgrd_Plan_No_2() { return pnd_Powerimage_File_Pnd_Pi_Sgrd_Plan_No_2; }

    public DbsField getPnd_Powerimage_File_Pnd_Pi_Ssn_2() { return pnd_Powerimage_File_Pnd_Pi_Ssn_2; }

    public DbsField getPnd_Powerimage_File_Pnd_Pi_Sgrd_Subplan_No_2() { return pnd_Powerimage_File_Pnd_Pi_Sgrd_Subplan_No_2; }

    public DbsField getPnd_Powerimage_File_Pnd_Pi_Omni_Folder_2() { return pnd_Powerimage_File_Pnd_Pi_Omni_Folder_2; }

    public DbsField getPnd_Powerimage_File_Pnd_Pi_Divorce_Ind_2() { return pnd_Powerimage_File_Pnd_Pi_Divorce_Ind_2; }

    public DbsField getPnd_Powerimage_File_Pnd_Pi_Beneinplan_Ind_2() { return pnd_Powerimage_File_Pnd_Pi_Beneinplan_Ind_2; }

    public DbsField getPnd_Powerimage_File_Pnd_Pi_Enroll_Method_2() { return pnd_Powerimage_File_Pnd_Pi_Enroll_Method_2; }

    public DbsField getPnd_Powerimage_File_Pnd_Pi_Acis_Error_No_2() { return pnd_Powerimage_File_Pnd_Pi_Acis_Error_No_2; }

    public DbsField getPnd_Powerimage_File_Pnd_Pi_Acis_Error_Msg_2() { return pnd_Powerimage_File_Pnd_Pi_Acis_Error_Msg_2; }

    public DbsField getPnd_Powerimage_File_Pnd_Pi_Orchestration_Id_2() { return pnd_Powerimage_File_Pnd_Pi_Orchestration_Id_2; }

    public DbsField getPnd_Powerimage_File_Pnd_Pi_Filler_2() { return pnd_Powerimage_File_Pnd_Pi_Filler_2; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Powerimage_File = newFieldInRecord("pnd_Powerimage_File", "#POWERIMAGE-FILE", FieldType.STRING, 175);
        pnd_Powerimage_FileRedef1 = newGroupInRecord("pnd_Powerimage_FileRedef1", "Redefines", pnd_Powerimage_File);
        pnd_Powerimage_File_Pnd_Pi_Log_Task_Type = pnd_Powerimage_FileRedef1.newFieldInGroup("pnd_Powerimage_File_Pnd_Pi_Log_Task_Type", "#PI-LOG-TASK-TYPE", 
            FieldType.STRING, 15);
        pnd_Powerimage_File_Pnd_Pi_Sgrd_Plan_No = pnd_Powerimage_FileRedef1.newFieldInGroup("pnd_Powerimage_File_Pnd_Pi_Sgrd_Plan_No", "#PI-SGRD-PLAN-NO", 
            FieldType.STRING, 6);
        pnd_Powerimage_File_Pnd_Pi_Ssn = pnd_Powerimage_FileRedef1.newFieldInGroup("pnd_Powerimage_File_Pnd_Pi_Ssn", "#PI-SSN", FieldType.STRING, 9);
        pnd_Powerimage_File_Pnd_Pi_Sgrd_Subplan_No = pnd_Powerimage_FileRedef1.newFieldInGroup("pnd_Powerimage_File_Pnd_Pi_Sgrd_Subplan_No", "#PI-SGRD-SUBPLAN-NO", 
            FieldType.STRING, 6);
        pnd_Powerimage_File_Pnd_Pi_Acis_Error_No = pnd_Powerimage_FileRedef1.newFieldInGroup("pnd_Powerimage_File_Pnd_Pi_Acis_Error_No", "#PI-ACIS-ERROR-NO", 
            FieldType.STRING, 4);
        pnd_Powerimage_File_Pnd_Pi_Acis_Error_Msg = pnd_Powerimage_FileRedef1.newFieldInGroup("pnd_Powerimage_File_Pnd_Pi_Acis_Error_Msg", "#PI-ACIS-ERROR-MSG", 
            FieldType.STRING, 80);
        pnd_Powerimage_File_Pnd_Pi_Orchestration_Id = pnd_Powerimage_FileRedef1.newFieldInGroup("pnd_Powerimage_File_Pnd_Pi_Orchestration_Id", "#PI-ORCHESTRATION-ID", 
            FieldType.STRING, 15);
        pnd_Powerimage_File_Pnd_Pi_Filler = pnd_Powerimage_FileRedef1.newFieldInGroup("pnd_Powerimage_File_Pnd_Pi_Filler", "#PI-FILLER", FieldType.STRING, 
            15);
        pnd_Powerimage_FileRedef2 = newGroupInRecord("pnd_Powerimage_FileRedef2", "Redefines", pnd_Powerimage_File);
        pnd_Powerimage_File_Pnd_Pi_Log_Task_Type_2 = pnd_Powerimage_FileRedef2.newFieldInGroup("pnd_Powerimage_File_Pnd_Pi_Log_Task_Type_2", "#PI-LOG-TASK-TYPE-2", 
            FieldType.STRING, 15);
        pnd_Powerimage_File_Pnd_Pi_Sgrd_Plan_No_2 = pnd_Powerimage_FileRedef2.newFieldInGroup("pnd_Powerimage_File_Pnd_Pi_Sgrd_Plan_No_2", "#PI-SGRD-PLAN-NO-2", 
            FieldType.STRING, 6);
        pnd_Powerimage_File_Pnd_Pi_Ssn_2 = pnd_Powerimage_FileRedef2.newFieldInGroup("pnd_Powerimage_File_Pnd_Pi_Ssn_2", "#PI-SSN-2", FieldType.STRING, 
            9);
        pnd_Powerimage_File_Pnd_Pi_Sgrd_Subplan_No_2 = pnd_Powerimage_FileRedef2.newFieldInGroup("pnd_Powerimage_File_Pnd_Pi_Sgrd_Subplan_No_2", "#PI-SGRD-SUBPLAN-NO-2", 
            FieldType.STRING, 6);
        pnd_Powerimage_File_Pnd_Pi_Omni_Folder_2 = pnd_Powerimage_FileRedef2.newFieldInGroup("pnd_Powerimage_File_Pnd_Pi_Omni_Folder_2", "#PI-OMNI-FOLDER-2", 
            FieldType.STRING, 20);
        pnd_Powerimage_File_Pnd_Pi_Divorce_Ind_2 = pnd_Powerimage_FileRedef2.newFieldInGroup("pnd_Powerimage_File_Pnd_Pi_Divorce_Ind_2", "#PI-DIVORCE-IND-2", 
            FieldType.STRING, 1);
        pnd_Powerimage_File_Pnd_Pi_Beneinplan_Ind_2 = pnd_Powerimage_FileRedef2.newFieldInGroup("pnd_Powerimage_File_Pnd_Pi_Beneinplan_Ind_2", "#PI-BENEINPLAN-IND-2", 
            FieldType.STRING, 1);
        pnd_Powerimage_File_Pnd_Pi_Enroll_Method_2 = pnd_Powerimage_FileRedef2.newFieldInGroup("pnd_Powerimage_File_Pnd_Pi_Enroll_Method_2", "#PI-ENROLL-METHOD-2", 
            FieldType.STRING, 1);
        pnd_Powerimage_File_Pnd_Pi_Acis_Error_No_2 = pnd_Powerimage_FileRedef2.newFieldInGroup("pnd_Powerimage_File_Pnd_Pi_Acis_Error_No_2", "#PI-ACIS-ERROR-NO-2", 
            FieldType.STRING, 4);
        pnd_Powerimage_File_Pnd_Pi_Acis_Error_Msg_2 = pnd_Powerimage_FileRedef2.newFieldInGroup("pnd_Powerimage_File_Pnd_Pi_Acis_Error_Msg_2", "#PI-ACIS-ERROR-MSG-2", 
            FieldType.STRING, 80);
        pnd_Powerimage_File_Pnd_Pi_Orchestration_Id_2 = pnd_Powerimage_FileRedef2.newFieldInGroup("pnd_Powerimage_File_Pnd_Pi_Orchestration_Id_2", "#PI-ORCHESTRATION-ID-2", 
            FieldType.STRING, 15);
        pnd_Powerimage_File_Pnd_Pi_Filler_2 = pnd_Powerimage_FileRedef2.newFieldInGroup("pnd_Powerimage_File_Pnd_Pi_Filler_2", "#PI-FILLER-2", FieldType.STRING, 
            17);

        this.setRecordName("LdaScil9020");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaScil9020() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
