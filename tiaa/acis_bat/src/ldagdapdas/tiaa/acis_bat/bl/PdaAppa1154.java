/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:17:47 PM
**        * FROM NATURAL PDA     : APPA1154
************************************************************
**        * FILE NAME            : PdaAppa1154.java
**        * CLASS NAME           : PdaAppa1154
**        * INSTANCE NAME        : PdaAppa1154
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaAppa1154 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Appa1154;
    private DbsField pnd_Appa1154_Pnd_Bene_Tiaa_Nbr;
    private DbsField pnd_Appa1154_Pnd_Bene_Cref_Nbr;
    private DbsField pnd_Appa1154_Pnd_Bene_Resid_State;
    private DbsField pnd_Appa1154_Pnd_Bene_Estate_As_Bene;
    private DbsField pnd_Appa1154_Pnd_Bene_Trust_As_Bene;
    private DbsField pnd_Appa1154_Pnd_Bene_Category;
    private DbsField pnd_Appa1154_Pnd_Bene_Prmry_Cnt;
    private DbsField pnd_Appa1154_Pnd_Bene_Cntgnt_Cnt;
    private DbsGroup pnd_Appa1154_Pnd_Bene_Prmry_Data;
    private DbsField pnd_Appa1154_Pnd_Bene_Prmry_Type;
    private DbsField pnd_Appa1154_Pnd_Bene_Prmry_Name;
    private DbsField pnd_Appa1154_Pnd_Bene_Prmry_Ssn;
    private DbsField pnd_Appa1154_Pnd_Bene_Prmry_Dob;
    private DbsField pnd_Appa1154_Pnd_Bene_Prmry_Relationship;
    private DbsField pnd_Appa1154_Pnd_Bene_Prmry_Alloc_Pct;
    private DbsField pnd_Appa1154_Pnd_Bene_Prmry_Nmrtr_Nbr;
    private DbsField pnd_Appa1154_Pnd_Bene_Prmry_Dnmntr_Nbr;
    private DbsField pnd_Appa1154_Pnd_Bene_Prmry_Std_Txt_Ind;
    private DbsField pnd_Appa1154_Pnd_Bene_Prmry_Spcl_Txt;
    private DbsGroup pnd_Appa1154_Pnd_Bene_Cntgnt_Data;
    private DbsField pnd_Appa1154_Pnd_Bene_Cntgnt_Type;
    private DbsField pnd_Appa1154_Pnd_Bene_Cntgnt_Name;
    private DbsField pnd_Appa1154_Pnd_Bene_Cntgnt_Ssn;
    private DbsField pnd_Appa1154_Pnd_Bene_Cntgnt_Dob;
    private DbsField pnd_Appa1154_Pnd_Bene_Cntgnt_Relationship;
    private DbsField pnd_Appa1154_Pnd_Bene_Cntgnt_Alloc_Pct;
    private DbsField pnd_Appa1154_Pnd_Bene_Cntgnt_Nmrtr_Nbr;
    private DbsField pnd_Appa1154_Pnd_Bene_Cntgnt_Dnmntr_Nbr;
    private DbsField pnd_Appa1154_Pnd_Bene_Cntgnt_Std_Txt_Ind;
    private DbsField pnd_Appa1154_Pnd_Bene_Cntgnt_Spcl_Txt;
    private DbsField pnd_Appa1154_Pnd_Bene_Mos_Ind;
    private DbsField pnd_Appa1154_Pnd_Bene_Spcl_Dsgn_Txt;
    private DbsField pnd_Appa1154_Pnd_Bene_Return_Code;
    private DbsField pnd_Appa1154_Pnd_Bene_Msg;

    public DbsGroup getPnd_Appa1154() { return pnd_Appa1154; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Tiaa_Nbr() { return pnd_Appa1154_Pnd_Bene_Tiaa_Nbr; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Cref_Nbr() { return pnd_Appa1154_Pnd_Bene_Cref_Nbr; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Resid_State() { return pnd_Appa1154_Pnd_Bene_Resid_State; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Estate_As_Bene() { return pnd_Appa1154_Pnd_Bene_Estate_As_Bene; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Trust_As_Bene() { return pnd_Appa1154_Pnd_Bene_Trust_As_Bene; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Category() { return pnd_Appa1154_Pnd_Bene_Category; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Prmry_Cnt() { return pnd_Appa1154_Pnd_Bene_Prmry_Cnt; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Cntgnt_Cnt() { return pnd_Appa1154_Pnd_Bene_Cntgnt_Cnt; }

    public DbsGroup getPnd_Appa1154_Pnd_Bene_Prmry_Data() { return pnd_Appa1154_Pnd_Bene_Prmry_Data; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Prmry_Type() { return pnd_Appa1154_Pnd_Bene_Prmry_Type; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Prmry_Name() { return pnd_Appa1154_Pnd_Bene_Prmry_Name; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Prmry_Ssn() { return pnd_Appa1154_Pnd_Bene_Prmry_Ssn; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Prmry_Dob() { return pnd_Appa1154_Pnd_Bene_Prmry_Dob; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Prmry_Relationship() { return pnd_Appa1154_Pnd_Bene_Prmry_Relationship; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Prmry_Alloc_Pct() { return pnd_Appa1154_Pnd_Bene_Prmry_Alloc_Pct; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Prmry_Nmrtr_Nbr() { return pnd_Appa1154_Pnd_Bene_Prmry_Nmrtr_Nbr; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Prmry_Dnmntr_Nbr() { return pnd_Appa1154_Pnd_Bene_Prmry_Dnmntr_Nbr; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Prmry_Std_Txt_Ind() { return pnd_Appa1154_Pnd_Bene_Prmry_Std_Txt_Ind; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Prmry_Spcl_Txt() { return pnd_Appa1154_Pnd_Bene_Prmry_Spcl_Txt; }

    public DbsGroup getPnd_Appa1154_Pnd_Bene_Cntgnt_Data() { return pnd_Appa1154_Pnd_Bene_Cntgnt_Data; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Cntgnt_Type() { return pnd_Appa1154_Pnd_Bene_Cntgnt_Type; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Cntgnt_Name() { return pnd_Appa1154_Pnd_Bene_Cntgnt_Name; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Cntgnt_Ssn() { return pnd_Appa1154_Pnd_Bene_Cntgnt_Ssn; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Cntgnt_Dob() { return pnd_Appa1154_Pnd_Bene_Cntgnt_Dob; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Cntgnt_Relationship() { return pnd_Appa1154_Pnd_Bene_Cntgnt_Relationship; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Cntgnt_Alloc_Pct() { return pnd_Appa1154_Pnd_Bene_Cntgnt_Alloc_Pct; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Cntgnt_Nmrtr_Nbr() { return pnd_Appa1154_Pnd_Bene_Cntgnt_Nmrtr_Nbr; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Cntgnt_Dnmntr_Nbr() { return pnd_Appa1154_Pnd_Bene_Cntgnt_Dnmntr_Nbr; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Cntgnt_Std_Txt_Ind() { return pnd_Appa1154_Pnd_Bene_Cntgnt_Std_Txt_Ind; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Cntgnt_Spcl_Txt() { return pnd_Appa1154_Pnd_Bene_Cntgnt_Spcl_Txt; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Mos_Ind() { return pnd_Appa1154_Pnd_Bene_Mos_Ind; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Spcl_Dsgn_Txt() { return pnd_Appa1154_Pnd_Bene_Spcl_Dsgn_Txt; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Return_Code() { return pnd_Appa1154_Pnd_Bene_Return_Code; }

    public DbsField getPnd_Appa1154_Pnd_Bene_Msg() { return pnd_Appa1154_Pnd_Bene_Msg; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Appa1154 = dbsRecord.newGroupInRecord("pnd_Appa1154", "#APPA1154");
        pnd_Appa1154.setParameterOption(ParameterOption.ByReference);
        pnd_Appa1154_Pnd_Bene_Tiaa_Nbr = pnd_Appa1154.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Tiaa_Nbr", "#BENE-TIAA-NBR", FieldType.STRING, 8);
        pnd_Appa1154_Pnd_Bene_Cref_Nbr = pnd_Appa1154.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Cref_Nbr", "#BENE-CREF-NBR", FieldType.STRING, 8);
        pnd_Appa1154_Pnd_Bene_Resid_State = pnd_Appa1154.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Resid_State", "#BENE-RESID-STATE", FieldType.STRING, 2);
        pnd_Appa1154_Pnd_Bene_Estate_As_Bene = pnd_Appa1154.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Estate_As_Bene", "#BENE-ESTATE-AS-BENE", FieldType.STRING, 
            1);
        pnd_Appa1154_Pnd_Bene_Trust_As_Bene = pnd_Appa1154.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Trust_As_Bene", "#BENE-TRUST-AS-BENE", FieldType.STRING, 
            1);
        pnd_Appa1154_Pnd_Bene_Category = pnd_Appa1154.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Category", "#BENE-CATEGORY", FieldType.STRING, 1);
        pnd_Appa1154_Pnd_Bene_Prmry_Cnt = pnd_Appa1154.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Prmry_Cnt", "#BENE-PRMRY-CNT", FieldType.NUMERIC, 2);
        pnd_Appa1154_Pnd_Bene_Cntgnt_Cnt = pnd_Appa1154.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Cntgnt_Cnt", "#BENE-CNTGNT-CNT", FieldType.NUMERIC, 2);
        pnd_Appa1154_Pnd_Bene_Prmry_Data = pnd_Appa1154.newGroupArrayInGroup("pnd_Appa1154_Pnd_Bene_Prmry_Data", "#BENE-PRMRY-DATA", new DbsArrayController(1,
            20));
        pnd_Appa1154_Pnd_Bene_Prmry_Type = pnd_Appa1154_Pnd_Bene_Prmry_Data.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Prmry_Type", "#BENE-PRMRY-TYPE", FieldType.STRING, 
            1);
        pnd_Appa1154_Pnd_Bene_Prmry_Name = pnd_Appa1154_Pnd_Bene_Prmry_Data.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Prmry_Name", "#BENE-PRMRY-NAME", FieldType.STRING, 
            70);
        pnd_Appa1154_Pnd_Bene_Prmry_Ssn = pnd_Appa1154_Pnd_Bene_Prmry_Data.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Prmry_Ssn", "#BENE-PRMRY-SSN", FieldType.NUMERIC, 
            9);
        pnd_Appa1154_Pnd_Bene_Prmry_Dob = pnd_Appa1154_Pnd_Bene_Prmry_Data.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Prmry_Dob", "#BENE-PRMRY-DOB", FieldType.NUMERIC, 
            8);
        pnd_Appa1154_Pnd_Bene_Prmry_Relationship = pnd_Appa1154_Pnd_Bene_Prmry_Data.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Prmry_Relationship", "#BENE-PRMRY-RELATIONSHIP", 
            FieldType.STRING, 15);
        pnd_Appa1154_Pnd_Bene_Prmry_Alloc_Pct = pnd_Appa1154_Pnd_Bene_Prmry_Data.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Prmry_Alloc_Pct", "#BENE-PRMRY-ALLOC-PCT", 
            FieldType.DECIMAL, 5,2);
        pnd_Appa1154_Pnd_Bene_Prmry_Nmrtr_Nbr = pnd_Appa1154_Pnd_Bene_Prmry_Data.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Prmry_Nmrtr_Nbr", "#BENE-PRMRY-NMRTR-NBR", 
            FieldType.NUMERIC, 3);
        pnd_Appa1154_Pnd_Bene_Prmry_Dnmntr_Nbr = pnd_Appa1154_Pnd_Bene_Prmry_Data.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Prmry_Dnmntr_Nbr", "#BENE-PRMRY-DNMNTR-NBR", 
            FieldType.NUMERIC, 3);
        pnd_Appa1154_Pnd_Bene_Prmry_Std_Txt_Ind = pnd_Appa1154_Pnd_Bene_Prmry_Data.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Prmry_Std_Txt_Ind", "#BENE-PRMRY-STD-TXT-IND", 
            FieldType.STRING, 1);
        pnd_Appa1154_Pnd_Bene_Prmry_Spcl_Txt = pnd_Appa1154_Pnd_Bene_Prmry_Data.newFieldArrayInGroup("pnd_Appa1154_Pnd_Bene_Prmry_Spcl_Txt", "#BENE-PRMRY-SPCL-TXT", 
            FieldType.STRING, 72, new DbsArrayController(1,3));
        pnd_Appa1154_Pnd_Bene_Cntgnt_Data = pnd_Appa1154.newGroupArrayInGroup("pnd_Appa1154_Pnd_Bene_Cntgnt_Data", "#BENE-CNTGNT-DATA", new DbsArrayController(1,
            20));
        pnd_Appa1154_Pnd_Bene_Cntgnt_Type = pnd_Appa1154_Pnd_Bene_Cntgnt_Data.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Cntgnt_Type", "#BENE-CNTGNT-TYPE", 
            FieldType.STRING, 1);
        pnd_Appa1154_Pnd_Bene_Cntgnt_Name = pnd_Appa1154_Pnd_Bene_Cntgnt_Data.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Cntgnt_Name", "#BENE-CNTGNT-NAME", 
            FieldType.STRING, 70);
        pnd_Appa1154_Pnd_Bene_Cntgnt_Ssn = pnd_Appa1154_Pnd_Bene_Cntgnt_Data.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Cntgnt_Ssn", "#BENE-CNTGNT-SSN", FieldType.NUMERIC, 
            9);
        pnd_Appa1154_Pnd_Bene_Cntgnt_Dob = pnd_Appa1154_Pnd_Bene_Cntgnt_Data.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Cntgnt_Dob", "#BENE-CNTGNT-DOB", FieldType.NUMERIC, 
            8);
        pnd_Appa1154_Pnd_Bene_Cntgnt_Relationship = pnd_Appa1154_Pnd_Bene_Cntgnt_Data.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Cntgnt_Relationship", "#BENE-CNTGNT-RELATIONSHIP", 
            FieldType.STRING, 15);
        pnd_Appa1154_Pnd_Bene_Cntgnt_Alloc_Pct = pnd_Appa1154_Pnd_Bene_Cntgnt_Data.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Cntgnt_Alloc_Pct", "#BENE-CNTGNT-ALLOC-PCT", 
            FieldType.DECIMAL, 5,2);
        pnd_Appa1154_Pnd_Bene_Cntgnt_Nmrtr_Nbr = pnd_Appa1154_Pnd_Bene_Cntgnt_Data.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Cntgnt_Nmrtr_Nbr", "#BENE-CNTGNT-NMRTR-NBR", 
            FieldType.NUMERIC, 3);
        pnd_Appa1154_Pnd_Bene_Cntgnt_Dnmntr_Nbr = pnd_Appa1154_Pnd_Bene_Cntgnt_Data.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Cntgnt_Dnmntr_Nbr", "#BENE-CNTGNT-DNMNTR-NBR", 
            FieldType.NUMERIC, 3);
        pnd_Appa1154_Pnd_Bene_Cntgnt_Std_Txt_Ind = pnd_Appa1154_Pnd_Bene_Cntgnt_Data.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Cntgnt_Std_Txt_Ind", "#BENE-CNTGNT-STD-TXT-IND", 
            FieldType.STRING, 1);
        pnd_Appa1154_Pnd_Bene_Cntgnt_Spcl_Txt = pnd_Appa1154_Pnd_Bene_Cntgnt_Data.newFieldArrayInGroup("pnd_Appa1154_Pnd_Bene_Cntgnt_Spcl_Txt", "#BENE-CNTGNT-SPCL-TXT", 
            FieldType.STRING, 72, new DbsArrayController(1,3));
        pnd_Appa1154_Pnd_Bene_Mos_Ind = pnd_Appa1154.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Mos_Ind", "#BENE-MOS-IND", FieldType.STRING, 1);
        pnd_Appa1154_Pnd_Bene_Spcl_Dsgn_Txt = pnd_Appa1154.newFieldArrayInGroup("pnd_Appa1154_Pnd_Bene_Spcl_Dsgn_Txt", "#BENE-SPCL-DSGN-TXT", FieldType.STRING, 
            72, new DbsArrayController(1,60));
        pnd_Appa1154_Pnd_Bene_Return_Code = pnd_Appa1154.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Return_Code", "#BENE-RETURN-CODE", FieldType.NUMERIC, 
            2);
        pnd_Appa1154_Pnd_Bene_Msg = pnd_Appa1154.newFieldInGroup("pnd_Appa1154_Pnd_Bene_Msg", "#BENE-MSG", FieldType.STRING, 79);

        dbsRecord.reset();
    }

    // Constructors
    public PdaAppa1154(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

