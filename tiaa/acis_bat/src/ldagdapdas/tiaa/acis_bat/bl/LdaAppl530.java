/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:51:23 PM
**        * FROM NATURAL LDA     : APPL530
************************************************************
**        * FILE NAME            : LdaAppl530.java
**        * CLASS NAME           : LdaAppl530
**        * INSTANCE NAME        : LdaAppl530
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl530 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_acis_Reconcile_Fl_View;
    private DbsField acis_Reconcile_Fl_View_Rc_Ssn;
    private DbsField acis_Reconcile_Fl_View_Rc_Plan;
    private DbsField acis_Reconcile_Fl_View_Rc_Sub_Plan;
    private DbsField acis_Reconcile_Fl_View_Rc_Tiaa_Cntrct;
    private DbsField acis_Reconcile_Fl_View_Rc_Cref_Cntrct;
    private DbsField acis_Reconcile_Fl_View_Rc_Dflt_Enroll_Ind;
    private DbsField acis_Reconcile_Fl_View_Rc_Funded_Ind;
    private DbsField acis_Reconcile_Fl_View_Rc_Initial_Funding_Dt;
    private DbsField acis_Reconcile_Fl_View_Rc_Reconcile_Type;
    private DbsField acis_Reconcile_Fl_View_Rc_Reconcile_Dt;
    private DbsField acis_Reconcile_Fl_View_Rc_Reconcile_Category;
    private DbsField acis_Reconcile_Fl_View_Rc_Exception_Ind;
    private DbsField acis_Reconcile_Fl_View_Rc_Exception_Dt;
    private DbsField acis_Reconcile_Fl_View_Rc_Exception_Rslvd_Dt;
    private DbsField acis_Reconcile_Fl_View_Rc_Control_Dt;

    public DataAccessProgramView getVw_acis_Reconcile_Fl_View() { return vw_acis_Reconcile_Fl_View; }

    public DbsField getAcis_Reconcile_Fl_View_Rc_Ssn() { return acis_Reconcile_Fl_View_Rc_Ssn; }

    public DbsField getAcis_Reconcile_Fl_View_Rc_Plan() { return acis_Reconcile_Fl_View_Rc_Plan; }

    public DbsField getAcis_Reconcile_Fl_View_Rc_Sub_Plan() { return acis_Reconcile_Fl_View_Rc_Sub_Plan; }

    public DbsField getAcis_Reconcile_Fl_View_Rc_Tiaa_Cntrct() { return acis_Reconcile_Fl_View_Rc_Tiaa_Cntrct; }

    public DbsField getAcis_Reconcile_Fl_View_Rc_Cref_Cntrct() { return acis_Reconcile_Fl_View_Rc_Cref_Cntrct; }

    public DbsField getAcis_Reconcile_Fl_View_Rc_Dflt_Enroll_Ind() { return acis_Reconcile_Fl_View_Rc_Dflt_Enroll_Ind; }

    public DbsField getAcis_Reconcile_Fl_View_Rc_Funded_Ind() { return acis_Reconcile_Fl_View_Rc_Funded_Ind; }

    public DbsField getAcis_Reconcile_Fl_View_Rc_Initial_Funding_Dt() { return acis_Reconcile_Fl_View_Rc_Initial_Funding_Dt; }

    public DbsField getAcis_Reconcile_Fl_View_Rc_Reconcile_Type() { return acis_Reconcile_Fl_View_Rc_Reconcile_Type; }

    public DbsField getAcis_Reconcile_Fl_View_Rc_Reconcile_Dt() { return acis_Reconcile_Fl_View_Rc_Reconcile_Dt; }

    public DbsField getAcis_Reconcile_Fl_View_Rc_Reconcile_Category() { return acis_Reconcile_Fl_View_Rc_Reconcile_Category; }

    public DbsField getAcis_Reconcile_Fl_View_Rc_Exception_Ind() { return acis_Reconcile_Fl_View_Rc_Exception_Ind; }

    public DbsField getAcis_Reconcile_Fl_View_Rc_Exception_Dt() { return acis_Reconcile_Fl_View_Rc_Exception_Dt; }

    public DbsField getAcis_Reconcile_Fl_View_Rc_Exception_Rslvd_Dt() { return acis_Reconcile_Fl_View_Rc_Exception_Rslvd_Dt; }

    public DbsField getAcis_Reconcile_Fl_View_Rc_Control_Dt() { return acis_Reconcile_Fl_View_Rc_Control_Dt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_acis_Reconcile_Fl_View = new DataAccessProgramView(new NameInfo("vw_acis_Reconcile_Fl_View", "ACIS-RECONCILE-FL-VIEW"), "ACIS_RECONCILE_FL", 
            "ACIS_RECONCILE");
        acis_Reconcile_Fl_View_Rc_Ssn = vw_acis_Reconcile_Fl_View.getRecord().newFieldInGroup("acis_Reconcile_Fl_View_Rc_Ssn", "RC-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "RC_SSN");
        acis_Reconcile_Fl_View_Rc_Plan = vw_acis_Reconcile_Fl_View.getRecord().newFieldInGroup("acis_Reconcile_Fl_View_Rc_Plan", "RC-PLAN", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "RC_PLAN");
        acis_Reconcile_Fl_View_Rc_Sub_Plan = vw_acis_Reconcile_Fl_View.getRecord().newFieldInGroup("acis_Reconcile_Fl_View_Rc_Sub_Plan", "RC-SUB-PLAN", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "RC_SUB_PLAN");
        acis_Reconcile_Fl_View_Rc_Tiaa_Cntrct = vw_acis_Reconcile_Fl_View.getRecord().newFieldInGroup("acis_Reconcile_Fl_View_Rc_Tiaa_Cntrct", "RC-TIAA-CNTRCT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "RC_TIAA_CNTRCT");
        acis_Reconcile_Fl_View_Rc_Cref_Cntrct = vw_acis_Reconcile_Fl_View.getRecord().newFieldInGroup("acis_Reconcile_Fl_View_Rc_Cref_Cntrct", "RC-CREF-CNTRCT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "RC_CREF_CNTRCT");
        acis_Reconcile_Fl_View_Rc_Dflt_Enroll_Ind = vw_acis_Reconcile_Fl_View.getRecord().newFieldInGroup("acis_Reconcile_Fl_View_Rc_Dflt_Enroll_Ind", 
            "RC-DFLT-ENROLL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RC_DFLT_ENROLL_IND");
        acis_Reconcile_Fl_View_Rc_Funded_Ind = vw_acis_Reconcile_Fl_View.getRecord().newFieldInGroup("acis_Reconcile_Fl_View_Rc_Funded_Ind", "RC-FUNDED-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RC_FUNDED_IND");
        acis_Reconcile_Fl_View_Rc_Initial_Funding_Dt = vw_acis_Reconcile_Fl_View.getRecord().newFieldInGroup("acis_Reconcile_Fl_View_Rc_Initial_Funding_Dt", 
            "RC-INITIAL-FUNDING-DT", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RC_INITIAL_FUNDING_DT");
        acis_Reconcile_Fl_View_Rc_Reconcile_Type = vw_acis_Reconcile_Fl_View.getRecord().newFieldInGroup("acis_Reconcile_Fl_View_Rc_Reconcile_Type", "RC-RECONCILE-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RC_RECONCILE_TYPE");
        acis_Reconcile_Fl_View_Rc_Reconcile_Dt = vw_acis_Reconcile_Fl_View.getRecord().newFieldInGroup("acis_Reconcile_Fl_View_Rc_Reconcile_Dt", "RC-RECONCILE-DT", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RC_RECONCILE_DT");
        acis_Reconcile_Fl_View_Rc_Reconcile_Category = vw_acis_Reconcile_Fl_View.getRecord().newFieldInGroup("acis_Reconcile_Fl_View_Rc_Reconcile_Category", 
            "RC-RECONCILE-CATEGORY", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RC_RECONCILE_CATEGORY");
        acis_Reconcile_Fl_View_Rc_Exception_Ind = vw_acis_Reconcile_Fl_View.getRecord().newFieldInGroup("acis_Reconcile_Fl_View_Rc_Exception_Ind", "RC-EXCEPTION-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RC_EXCEPTION_IND");
        acis_Reconcile_Fl_View_Rc_Exception_Dt = vw_acis_Reconcile_Fl_View.getRecord().newFieldInGroup("acis_Reconcile_Fl_View_Rc_Exception_Dt", "RC-EXCEPTION-DT", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RC_EXCEPTION_DT");
        acis_Reconcile_Fl_View_Rc_Exception_Rslvd_Dt = vw_acis_Reconcile_Fl_View.getRecord().newFieldInGroup("acis_Reconcile_Fl_View_Rc_Exception_Rslvd_Dt", 
            "RC-EXCEPTION-RSLVD-DT", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RC_EXCEPTION_RSLVD_DT");
        acis_Reconcile_Fl_View_Rc_Control_Dt = vw_acis_Reconcile_Fl_View.getRecord().newFieldInGroup("acis_Reconcile_Fl_View_Rc_Control_Dt", "RC-CONTROL-DT", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RC_CONTROL_DT");

        this.setRecordName("LdaAppl530");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_acis_Reconcile_Fl_View.reset();
    }

    // Constructor
    public LdaAppl530() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
