/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:49 PM
**        * FROM NATURAL LDA     : APPL1151
************************************************************
**        * FILE NAME            : LdaAppl1151.java
**        * CLASS NAME           : LdaAppl1151
**        * INSTANCE NAME        : LdaAppl1151
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl1151 extends DbsRecord
{
    // Properties
    private DbsField router_Ex_File;
    private DbsGroup router_Ex_FileRedef1;
    private DbsField router_Ex_File_Rt_Type_Cd;
    private DbsField router_Ex_File_Rt_Tiaa_No_1;
    private DbsField router_Ex_File_Rt_Cref_No_1;
    private DbsField router_Ex_File_Rt_Tiaa_No_2;
    private DbsField router_Ex_File_Rt_Cref_No_2;
    private DbsField router_Ex_File_Rt_Tiaa_No_3;
    private DbsField router_Ex_File_Rt_Cref_No_3;
    private DbsField router_Ex_File_Rt_Tiaa_No_4;
    private DbsField router_Ex_File_Rt_Cref_No_4;
    private DbsField router_Ex_File_Rt_Date_Table;
    private DbsField router_Ex_File_Rt_Age_First_Pymt;
    private DbsField router_Ex_File_Rt_Ownership;
    private DbsField router_Ex_File_Rt_Alloc_Disc;
    private DbsField router_Ex_File_Rt_Currency;
    private DbsField router_Ex_File_Rt_Tiaa_Extr_Form;
    private DbsField router_Ex_File_Rt_Cref_Extr_Form;
    private DbsField router_Ex_File_Rt_Tiaa_Extr_Ed;
    private DbsField router_Ex_File_Rt_Cref_Extr_Ed;
    private DbsField router_Ex_File_Rt_Annuitant_Name;
    private DbsGroup router_Ex_File_Rt_Eop_Name;
    private DbsField router_Ex_File_Rt_Eop_Last_Name;
    private DbsField router_Ex_File_Rt_Eop_First_Name;
    private DbsField router_Ex_File_Rt_Eop_Middle_Name;
    private DbsGroup router_Ex_File_Rt_Eop_Middle_NameRedef2;
    private DbsField router_Ex_File_Rt_Eop_Mid_Init;
    private DbsField router_Ex_File_Rt_Eop_Mid_Filler;
    private DbsField router_Ex_File_Rt_Eop_Title;
    private DbsField router_Ex_File_Rt_Eop_Prefix;
    private DbsField router_Ex_File_Rt_Soc_Sec;
    private DbsField router_Ex_File_Rt_Pin_No;
    private DbsField router_Ex_File_Rt_Address_Table;
    private DbsField router_Ex_File_Rt_Eop_Zipcode;
    private DbsField router_Ex_File_Rt_Participant_Status;
    private DbsField router_Ex_File_Rt_Email_Address;
    private DbsField router_Ex_File_Rt_Request_Package_Type;
    private DbsField router_Ex_File_Rt_Portfolio_Selected;
    private DbsField router_Ex_File_Rt_Divorce_Contract;
    private DbsField router_Ex_File_Rt_Access_Cd_Ind;
    private DbsField router_Ex_File_Rt_Address_Change;
    private DbsField router_Ex_File_Rt_Package_Version;
    private DbsField router_Ex_File_Rt_Unit_Name;
    private DbsField router_Ex_File_Rt_Print_Package_Type;
    private DbsField router_Ex_File_Rt_Future_Filler;
    private DbsField router_Ex_File_Rt_Bene_Estate;
    private DbsField router_Ex_File_Rt_Bene_Trust;
    private DbsField router_Ex_File_Rt_Bene_Category;
    private DbsField router_Ex_File_Rt_Sex;
    private DbsField router_Ex_File_Rt_Inst_Name_Table;
    private DbsField router_Ex_File_Rt_Inst_Name;
    private DbsField router_Ex_File_Rt_Inst_Address_Table;
    private DbsField router_Ex_File_Rt_Inst_Zip_Cd;
    private DbsField router_Ex_File_Rt_Mail_Instructions;
    private DbsField router_Ex_File_Rt_Orig_Iss_State;
    private DbsField router_Ex_File_Rt_Current_Iss_State;
    private DbsField router_Ex_File_Rt_Eop_State_Name;
    private DbsField router_Ex_File_Rt_Lob;
    private DbsField router_Ex_File_Rt_Lob_Type;
    private DbsField router_Ex_File_Rt_Inst_Code;
    private DbsField router_Ex_File_Rt_Inst_Bill_Cd;
    private DbsField router_Ex_File_Rt_Inst_Permit_Trans;
    private DbsField router_Ex_File_Rt_Perm_Gra_Trans;
    private DbsField router_Ex_File_Rt_Inst_Full_Immed_Vest;
    private DbsField router_Ex_File_Rt_Inst_Cashable_Ra;
    private DbsField router_Ex_File_Rt_Inst_Cashable_Gra;
    private DbsField router_Ex_File_Rt_Inst_Fixed_Per_Opt;
    private DbsField router_Ex_File_Rt_Inst_Cntrl_Consent;
    private DbsField router_Ex_File_Rt_Eop;
    private DbsField router_Ex_File_Rt_Gsra_Inst_St_Code;
    private DbsField router_Ex_File_Rt_Region_Cd;
    private DbsField router_Ex_File_Rt_Staff_Id;
    private DbsField router_Ex_File_Rt_Appl_Source;
    private DbsField router_Ex_File_Rt_Eop_Appl_Status;
    private DbsField router_Ex_File_Rt_Eop_Addl_Cref_Req;
    private DbsField router_Ex_File_Rt_Extract_Date;
    private DbsField router_Ex_File_Rt_Gsra_Loan_Ind;
    private DbsField router_Ex_File_Rt_Spec_Ppg_Ind;
    private DbsField router_Ex_File_Rt_Mail_Pull_Cd;
    private DbsField router_Ex_File_Rt_Mit_Log_Dt_Tm;
    private DbsField router_Ex_File_Rt_Addr_Page_Name;
    private DbsGroup router_Ex_File_Rt_Addr_Page_NameRedef3;
    private DbsField router_Ex_File_Rt_Addr_Pref;
    private DbsField router_Ex_File_Rt_Addr_Filler_1;
    private DbsField router_Ex_File_Rt_Addr_Frst;
    private DbsField router_Ex_File_Rt_Addr_Filler_2;
    private DbsField router_Ex_File_Rt_Addr_Mddle;
    private DbsField router_Ex_File_Rt_Addr_Filler_3;
    private DbsField router_Ex_File_Rt_Addr_Last;
    private DbsField router_Ex_File_Rt_Addr_Filler_4;
    private DbsField router_Ex_File_Rt_Addr_Sffx;
    private DbsField router_Ex_File_Rt_Welcome_Name;
    private DbsGroup router_Ex_File_Rt_Welcome_NameRedef4;
    private DbsField router_Ex_File_Rt_Welcome_Pref;
    private DbsField router_Ex_File_Rt_Welcome_Filler_1;
    private DbsField router_Ex_File_Rt_Welcome_Last;
    private DbsField router_Ex_File_Rt_Pin_Name;
    private DbsGroup router_Ex_File_Rt_Pin_NameRedef5;
    private DbsField router_Ex_File_Rt_Pin_First;
    private DbsField router_Ex_File_Rt_Pin_Filler_1;
    private DbsField router_Ex_File_Rt_Pin_Mddle;
    private DbsField router_Ex_File_Rt_Pin_Filler_2;
    private DbsField router_Ex_File_Rt_Pin_Last;
    private DbsField router_Ex_File_Rt_Zip;
    private DbsGroup router_Ex_File_Rt_ZipRedef6;
    private DbsField router_Ex_File_Rt_Zip_Plus4;
    private DbsField router_Ex_File_Rt_Zip_Plus1;
    private DbsField router_Ex_File_Rt_Suspension;
    private DbsField router_Ex_File_Rt_Mult_App_Version;
    private DbsField router_Ex_File_Rt_Mult_App_Status;
    private DbsField router_Ex_File_Rt_Mult_App_Lob;
    private DbsField router_Ex_File_Rt_Mult_App_Lob_Type;
    private DbsField router_Ex_File_Rt_Mult_App_Ppg;
    private DbsField router_Ex_File_Rt_K12_Ppg;
    private DbsField router_Ex_File_Rt_Ira_Rollover_Type;
    private DbsField router_Ex_File_Rt_Ira_Record_Type;
    private DbsField router_Ex_File_Rt_Rollover_Amt;
    private DbsField router_Ex_File_Rt_Mit_Unit;
    private DbsField router_Ex_File_Rt_Mit_Wpid;
    private DbsField router_Ex_File_Rt_New_Bene_Ind;
    private DbsField router_Ex_File_Rt_Irc_Sectn_Cde;
    private DbsField router_Ex_File_Rt_Released_Dt;
    private DbsField router_Ex_File_Rt_Irc_Sectn_Grp;
    private DbsField router_Ex_File_Rt_Enroll_Request_Type;
    private DbsField router_Ex_File_Rt_Erisa_Inst;
    private DbsField router_Ex_File_Rt_Reprint_Request_Type;
    private DbsField router_Ex_File_Rt_Print_Destination;
    private DbsField router_Ex_File_Rt_Correction_Type;
    private DbsField router_Ex_File_Rt_Processor_Name;
    private DbsField router_Ex_File_Rt_Mail_Date;
    private DbsField router_Ex_File_Rt_Alloc_Fmt;
    private DbsField router_Ex_File_Rt_Phone_No;
    private DbsField router_Ex_File_Rt_Fund_Cde;
    private DbsField router_Ex_File_Rt_Alloc_Pct;
    private DbsField router_Ex_File_Rt_Oia_Ind;
    private DbsField router_Ex_File_Rt_Product_Cde;
    private DbsField router_Ex_File_Rt_Acct_Sum_Sheet_Type;
    private DbsField router_Ex_File_Rt_Sg_Plan_No;
    private DbsField router_Ex_File_Rt_Sg_Subplan_No;
    private DbsField router_Ex_File_Rt_Sg_Subplan_Type;
    private DbsField router_Ex_File_Rt_Omni_Issue_Ind;
    private DbsField router_Ex_File_Rt_Effective_Dt;
    private DbsField router_Ex_File_Rt_Employer_Name;
    private DbsField router_Ex_File_Rt_Tiaa_Rate;
    private DbsField router_Ex_File_Rt_Sg_Plan_Id;
    private DbsField router_Ex_File_Rt_Tiaa_Pg4_Numb;
    private DbsField router_Ex_File_Rt_Cref_Pg4_Numb;
    private DbsField router_Ex_File_Rt_Dflt_Access_Code;
    private DbsField router_Ex_File_Rt_Single_Issue_Ind;
    private DbsField router_Ex_File_Rt_Spec_Fund_Ind;
    private DbsField router_Ex_File_Rt_Product_Price_Level;
    private DbsField router_Ex_File_Rt_Roth_Ind;
    private DbsField router_Ex_File_Rt_Plan_Issue_State;
    private DbsField router_Ex_File_Rt_Client_Id;
    private DbsField router_Ex_File_Rt_Portfolio_Model;
    private DbsField router_Ex_File_Rt_Auto_Enroll_Ind;
    private DbsField router_Ex_File_Rt_Auto_Save_Ind;
    private DbsField router_Ex_File_Rt_As_Cur_Dflt_Amt;
    private DbsField router_Ex_File_Rt_As_Incr_Amt;
    private DbsField router_Ex_File_Rt_As_Max_Pct;
    private DbsField router_Ex_File_Rt_Ae_Opt_Out_Days;
    private DbsField router_Ex_File_Rt_Tsv_Ind;
    private DbsField router_Ex_File_Rt_Tiaa_Indx_Guarntd_Rte;
    private DbsField router_Ex_File_Rt_Agent_Crd_No;
    private DbsField router_Ex_File_Rt_Crd_Pull_Ind;
    private DbsField router_Ex_File_Rt_Agent_Name;
    private DbsField router_Ex_File_Rt_Agent_Work_Addr1;
    private DbsField router_Ex_File_Rt_Agent_Work_Addr2;
    private DbsField router_Ex_File_Rt_Agent_Work_City;
    private DbsField router_Ex_File_Rt_Agent_Work_State;
    private DbsField router_Ex_File_Rt_Agent_Work_Zip;
    private DbsField router_Ex_File_Rt_Agent_Work_Phone;
    private DbsField router_Ex_File_Rt_Tic_Ind;
    private DbsField router_Ex_File_Rt_Tic_Startdate;
    private DbsField router_Ex_File_Rt_Tic_Enddate;
    private DbsField router_Ex_File_Rt_Tic_Percentage;
    private DbsField router_Ex_File_Rt_Tic_Postdays;
    private DbsField router_Ex_File_Rt_Tic_Limit;
    private DbsField router_Ex_File_Rt_Tic_Postfreq;
    private DbsField router_Ex_File_Rt_Tic_Pl_Level;
    private DbsField router_Ex_File_Rt_Tic_Windowdays;
    private DbsField router_Ex_File_Rt_Tic_Reqdlywindow;
    private DbsField router_Ex_File_Rt_Tic_Recap_Prov;
    private DbsField router_Ex_File_Rt_Ecs_Dcs_E_Dlvry_Ind;
    private DbsField router_Ex_File_Rt_Ecs_Dcs_Annty_Optn_Ind;
    private DbsField router_Ex_File_Rt_Full_Middle_Name;
    private DbsField router_Ex_File_Rt_Substitution_Contract_Ind;
    private DbsField router_Ex_File_Rt_Non_Proprietary_Pkg_Ind;
    private DbsField router_Ex_File_Rt_Multi_Plan_Table;
    private DbsGroup router_Ex_File_Rt_Multi_Plan_TableRedef7;
    private DbsGroup router_Ex_File_Rt_Multi_Plan_Info;
    private DbsField router_Ex_File_Rt_Multi_Plan_No;
    private DbsField router_Ex_File_Rt_Multi_Sub_Plan;
    private DbsField router_Ex_File_Rt_Filler;
    private DbsField router_Ex_File_Rt_Tiaa_Init_Prem;
    private DbsField router_Ex_File_Rt_Cref_Init_Prem;
    private DbsField router_Ex_File_Rt_Related_Contract_Info;
    private DbsGroup router_Ex_File_Rt_Related_Contract_InfoRedef8;
    private DbsGroup router_Ex_File_Rt_Related_Contract;
    private DbsField router_Ex_File_Rt_Related_Contract_Type;
    private DbsField router_Ex_File_Rt_Related_Tiaa_No;
    private DbsField router_Ex_File_Rt_Related_Tiaa_Issue_Date;
    private DbsField router_Ex_File_Rt_Related_First_Payment_Date;
    private DbsField router_Ex_File_Rt_Related_Tiaa_Total_Amt;
    private DbsField router_Ex_File_Rt_Related_Last_Payment_Date;
    private DbsField router_Ex_File_Rt_Related_Payment_Frequency;
    private DbsField router_Ex_File_Rt_Related_Tiaa_Issue_State;
    private DbsField router_Ex_File_Rt_First_Tpa_Ipro_Ind;
    private DbsField router_Ex_File_Rt_Fund_Source_1;
    private DbsField router_Ex_File_Rt_Fund_Source_2;
    private DbsField router_Ex_File_Rt_Fund_Cde_2;
    private DbsField router_Ex_File_Rt_Alloc_Pct_2;
    private DbsField router_Ex_File_Rt_Job_Name;
    private DbsField router_Ex_File_Rt_Short_Name;
    private DbsGroup router_Ex_FileRedef9;
    private DbsField router_Ex_File_Router_Ext_Data_1;
    private DbsField router_Ex_File_Router_Ext_Data_2;
    private DbsField router_Ex_File_Router_Ext_Data_3;
    private DbsField router_Ex_File_Router_Ext_Data_4;
    private DbsField router_Ex_File_Router_Ext_Data_5;
    private DbsField router_Ex_File_Router_Ext_Data_6;
    private DbsField router_Ex_File_Router_Ext_Data_7;
    private DbsField router_Ex_File_Router_Ext_Data_8;
    private DbsField router_Ex_File_Router_Ext_Data_9;
    private DbsField router_Ex_File_Router_Ext_Data_10;
    private DbsField router_Ex_File_Router_Ext_Data_11;
    private DbsField router_Ex_File_Router_Ext_Data_12;
    private DbsField router_Ex_File_Router_Ext_Data_13;
    private DbsField router_Ex_File_Router_Ext_Data_14;
    private DbsField router_Ex_File_Router_Ext_Data_15;
    private DbsField router_Ex_File_Router_Ext_Data_16;
    private DbsField router_Ex_File_Router_Ext_Data_17;
    private DbsField router_Ex_File_Router_Ext_Data_18;
    private DbsField router_Ex_File_Router_Ext_Data_19;
    private DbsField router_Ex_File_Router_Ext_Data_20;
    private DbsField router_Ex_File_Router_Ext_Data_21;
    private DbsField router_Ex_File_Router_Ext_Data_22;
    private DbsField router_Ex_File_Router_Ext_Data_23;
    private DbsField router_Ex_File_Router_Ext_Data_24;
    private DbsField router_Ex_File_Router_Ext_Data_25;
    private DbsField router_Ex_File_Router_Ext_Data_26;
    private DbsField router_Ex_File_Router_Ext_Data_27;

    public DbsField getRouter_Ex_File() { return router_Ex_File; }

    public DbsGroup getRouter_Ex_FileRedef1() { return router_Ex_FileRedef1; }

    public DbsField getRouter_Ex_File_Rt_Type_Cd() { return router_Ex_File_Rt_Type_Cd; }

    public DbsField getRouter_Ex_File_Rt_Tiaa_No_1() { return router_Ex_File_Rt_Tiaa_No_1; }

    public DbsField getRouter_Ex_File_Rt_Cref_No_1() { return router_Ex_File_Rt_Cref_No_1; }

    public DbsField getRouter_Ex_File_Rt_Tiaa_No_2() { return router_Ex_File_Rt_Tiaa_No_2; }

    public DbsField getRouter_Ex_File_Rt_Cref_No_2() { return router_Ex_File_Rt_Cref_No_2; }

    public DbsField getRouter_Ex_File_Rt_Tiaa_No_3() { return router_Ex_File_Rt_Tiaa_No_3; }

    public DbsField getRouter_Ex_File_Rt_Cref_No_3() { return router_Ex_File_Rt_Cref_No_3; }

    public DbsField getRouter_Ex_File_Rt_Tiaa_No_4() { return router_Ex_File_Rt_Tiaa_No_4; }

    public DbsField getRouter_Ex_File_Rt_Cref_No_4() { return router_Ex_File_Rt_Cref_No_4; }

    public DbsField getRouter_Ex_File_Rt_Date_Table() { return router_Ex_File_Rt_Date_Table; }

    public DbsField getRouter_Ex_File_Rt_Age_First_Pymt() { return router_Ex_File_Rt_Age_First_Pymt; }

    public DbsField getRouter_Ex_File_Rt_Ownership() { return router_Ex_File_Rt_Ownership; }

    public DbsField getRouter_Ex_File_Rt_Alloc_Disc() { return router_Ex_File_Rt_Alloc_Disc; }

    public DbsField getRouter_Ex_File_Rt_Currency() { return router_Ex_File_Rt_Currency; }

    public DbsField getRouter_Ex_File_Rt_Tiaa_Extr_Form() { return router_Ex_File_Rt_Tiaa_Extr_Form; }

    public DbsField getRouter_Ex_File_Rt_Cref_Extr_Form() { return router_Ex_File_Rt_Cref_Extr_Form; }

    public DbsField getRouter_Ex_File_Rt_Tiaa_Extr_Ed() { return router_Ex_File_Rt_Tiaa_Extr_Ed; }

    public DbsField getRouter_Ex_File_Rt_Cref_Extr_Ed() { return router_Ex_File_Rt_Cref_Extr_Ed; }

    public DbsField getRouter_Ex_File_Rt_Annuitant_Name() { return router_Ex_File_Rt_Annuitant_Name; }

    public DbsGroup getRouter_Ex_File_Rt_Eop_Name() { return router_Ex_File_Rt_Eop_Name; }

    public DbsField getRouter_Ex_File_Rt_Eop_Last_Name() { return router_Ex_File_Rt_Eop_Last_Name; }

    public DbsField getRouter_Ex_File_Rt_Eop_First_Name() { return router_Ex_File_Rt_Eop_First_Name; }

    public DbsField getRouter_Ex_File_Rt_Eop_Middle_Name() { return router_Ex_File_Rt_Eop_Middle_Name; }

    public DbsGroup getRouter_Ex_File_Rt_Eop_Middle_NameRedef2() { return router_Ex_File_Rt_Eop_Middle_NameRedef2; }

    public DbsField getRouter_Ex_File_Rt_Eop_Mid_Init() { return router_Ex_File_Rt_Eop_Mid_Init; }

    public DbsField getRouter_Ex_File_Rt_Eop_Mid_Filler() { return router_Ex_File_Rt_Eop_Mid_Filler; }

    public DbsField getRouter_Ex_File_Rt_Eop_Title() { return router_Ex_File_Rt_Eop_Title; }

    public DbsField getRouter_Ex_File_Rt_Eop_Prefix() { return router_Ex_File_Rt_Eop_Prefix; }

    public DbsField getRouter_Ex_File_Rt_Soc_Sec() { return router_Ex_File_Rt_Soc_Sec; }

    public DbsField getRouter_Ex_File_Rt_Pin_No() { return router_Ex_File_Rt_Pin_No; }

    public DbsField getRouter_Ex_File_Rt_Address_Table() { return router_Ex_File_Rt_Address_Table; }

    public DbsField getRouter_Ex_File_Rt_Eop_Zipcode() { return router_Ex_File_Rt_Eop_Zipcode; }

    public DbsField getRouter_Ex_File_Rt_Participant_Status() { return router_Ex_File_Rt_Participant_Status; }

    public DbsField getRouter_Ex_File_Rt_Email_Address() { return router_Ex_File_Rt_Email_Address; }

    public DbsField getRouter_Ex_File_Rt_Request_Package_Type() { return router_Ex_File_Rt_Request_Package_Type; }

    public DbsField getRouter_Ex_File_Rt_Portfolio_Selected() { return router_Ex_File_Rt_Portfolio_Selected; }

    public DbsField getRouter_Ex_File_Rt_Divorce_Contract() { return router_Ex_File_Rt_Divorce_Contract; }

    public DbsField getRouter_Ex_File_Rt_Access_Cd_Ind() { return router_Ex_File_Rt_Access_Cd_Ind; }

    public DbsField getRouter_Ex_File_Rt_Address_Change() { return router_Ex_File_Rt_Address_Change; }

    public DbsField getRouter_Ex_File_Rt_Package_Version() { return router_Ex_File_Rt_Package_Version; }

    public DbsField getRouter_Ex_File_Rt_Unit_Name() { return router_Ex_File_Rt_Unit_Name; }

    public DbsField getRouter_Ex_File_Rt_Print_Package_Type() { return router_Ex_File_Rt_Print_Package_Type; }

    public DbsField getRouter_Ex_File_Rt_Future_Filler() { return router_Ex_File_Rt_Future_Filler; }

    public DbsField getRouter_Ex_File_Rt_Bene_Estate() { return router_Ex_File_Rt_Bene_Estate; }

    public DbsField getRouter_Ex_File_Rt_Bene_Trust() { return router_Ex_File_Rt_Bene_Trust; }

    public DbsField getRouter_Ex_File_Rt_Bene_Category() { return router_Ex_File_Rt_Bene_Category; }

    public DbsField getRouter_Ex_File_Rt_Sex() { return router_Ex_File_Rt_Sex; }

    public DbsField getRouter_Ex_File_Rt_Inst_Name_Table() { return router_Ex_File_Rt_Inst_Name_Table; }

    public DbsField getRouter_Ex_File_Rt_Inst_Name() { return router_Ex_File_Rt_Inst_Name; }

    public DbsField getRouter_Ex_File_Rt_Inst_Address_Table() { return router_Ex_File_Rt_Inst_Address_Table; }

    public DbsField getRouter_Ex_File_Rt_Inst_Zip_Cd() { return router_Ex_File_Rt_Inst_Zip_Cd; }

    public DbsField getRouter_Ex_File_Rt_Mail_Instructions() { return router_Ex_File_Rt_Mail_Instructions; }

    public DbsField getRouter_Ex_File_Rt_Orig_Iss_State() { return router_Ex_File_Rt_Orig_Iss_State; }

    public DbsField getRouter_Ex_File_Rt_Current_Iss_State() { return router_Ex_File_Rt_Current_Iss_State; }

    public DbsField getRouter_Ex_File_Rt_Eop_State_Name() { return router_Ex_File_Rt_Eop_State_Name; }

    public DbsField getRouter_Ex_File_Rt_Lob() { return router_Ex_File_Rt_Lob; }

    public DbsField getRouter_Ex_File_Rt_Lob_Type() { return router_Ex_File_Rt_Lob_Type; }

    public DbsField getRouter_Ex_File_Rt_Inst_Code() { return router_Ex_File_Rt_Inst_Code; }

    public DbsField getRouter_Ex_File_Rt_Inst_Bill_Cd() { return router_Ex_File_Rt_Inst_Bill_Cd; }

    public DbsField getRouter_Ex_File_Rt_Inst_Permit_Trans() { return router_Ex_File_Rt_Inst_Permit_Trans; }

    public DbsField getRouter_Ex_File_Rt_Perm_Gra_Trans() { return router_Ex_File_Rt_Perm_Gra_Trans; }

    public DbsField getRouter_Ex_File_Rt_Inst_Full_Immed_Vest() { return router_Ex_File_Rt_Inst_Full_Immed_Vest; }

    public DbsField getRouter_Ex_File_Rt_Inst_Cashable_Ra() { return router_Ex_File_Rt_Inst_Cashable_Ra; }

    public DbsField getRouter_Ex_File_Rt_Inst_Cashable_Gra() { return router_Ex_File_Rt_Inst_Cashable_Gra; }

    public DbsField getRouter_Ex_File_Rt_Inst_Fixed_Per_Opt() { return router_Ex_File_Rt_Inst_Fixed_Per_Opt; }

    public DbsField getRouter_Ex_File_Rt_Inst_Cntrl_Consent() { return router_Ex_File_Rt_Inst_Cntrl_Consent; }

    public DbsField getRouter_Ex_File_Rt_Eop() { return router_Ex_File_Rt_Eop; }

    public DbsField getRouter_Ex_File_Rt_Gsra_Inst_St_Code() { return router_Ex_File_Rt_Gsra_Inst_St_Code; }

    public DbsField getRouter_Ex_File_Rt_Region_Cd() { return router_Ex_File_Rt_Region_Cd; }

    public DbsField getRouter_Ex_File_Rt_Staff_Id() { return router_Ex_File_Rt_Staff_Id; }

    public DbsField getRouter_Ex_File_Rt_Appl_Source() { return router_Ex_File_Rt_Appl_Source; }

    public DbsField getRouter_Ex_File_Rt_Eop_Appl_Status() { return router_Ex_File_Rt_Eop_Appl_Status; }

    public DbsField getRouter_Ex_File_Rt_Eop_Addl_Cref_Req() { return router_Ex_File_Rt_Eop_Addl_Cref_Req; }

    public DbsField getRouter_Ex_File_Rt_Extract_Date() { return router_Ex_File_Rt_Extract_Date; }

    public DbsField getRouter_Ex_File_Rt_Gsra_Loan_Ind() { return router_Ex_File_Rt_Gsra_Loan_Ind; }

    public DbsField getRouter_Ex_File_Rt_Spec_Ppg_Ind() { return router_Ex_File_Rt_Spec_Ppg_Ind; }

    public DbsField getRouter_Ex_File_Rt_Mail_Pull_Cd() { return router_Ex_File_Rt_Mail_Pull_Cd; }

    public DbsField getRouter_Ex_File_Rt_Mit_Log_Dt_Tm() { return router_Ex_File_Rt_Mit_Log_Dt_Tm; }

    public DbsField getRouter_Ex_File_Rt_Addr_Page_Name() { return router_Ex_File_Rt_Addr_Page_Name; }

    public DbsGroup getRouter_Ex_File_Rt_Addr_Page_NameRedef3() { return router_Ex_File_Rt_Addr_Page_NameRedef3; }

    public DbsField getRouter_Ex_File_Rt_Addr_Pref() { return router_Ex_File_Rt_Addr_Pref; }

    public DbsField getRouter_Ex_File_Rt_Addr_Filler_1() { return router_Ex_File_Rt_Addr_Filler_1; }

    public DbsField getRouter_Ex_File_Rt_Addr_Frst() { return router_Ex_File_Rt_Addr_Frst; }

    public DbsField getRouter_Ex_File_Rt_Addr_Filler_2() { return router_Ex_File_Rt_Addr_Filler_2; }

    public DbsField getRouter_Ex_File_Rt_Addr_Mddle() { return router_Ex_File_Rt_Addr_Mddle; }

    public DbsField getRouter_Ex_File_Rt_Addr_Filler_3() { return router_Ex_File_Rt_Addr_Filler_3; }

    public DbsField getRouter_Ex_File_Rt_Addr_Last() { return router_Ex_File_Rt_Addr_Last; }

    public DbsField getRouter_Ex_File_Rt_Addr_Filler_4() { return router_Ex_File_Rt_Addr_Filler_4; }

    public DbsField getRouter_Ex_File_Rt_Addr_Sffx() { return router_Ex_File_Rt_Addr_Sffx; }

    public DbsField getRouter_Ex_File_Rt_Welcome_Name() { return router_Ex_File_Rt_Welcome_Name; }

    public DbsGroup getRouter_Ex_File_Rt_Welcome_NameRedef4() { return router_Ex_File_Rt_Welcome_NameRedef4; }

    public DbsField getRouter_Ex_File_Rt_Welcome_Pref() { return router_Ex_File_Rt_Welcome_Pref; }

    public DbsField getRouter_Ex_File_Rt_Welcome_Filler_1() { return router_Ex_File_Rt_Welcome_Filler_1; }

    public DbsField getRouter_Ex_File_Rt_Welcome_Last() { return router_Ex_File_Rt_Welcome_Last; }

    public DbsField getRouter_Ex_File_Rt_Pin_Name() { return router_Ex_File_Rt_Pin_Name; }

    public DbsGroup getRouter_Ex_File_Rt_Pin_NameRedef5() { return router_Ex_File_Rt_Pin_NameRedef5; }

    public DbsField getRouter_Ex_File_Rt_Pin_First() { return router_Ex_File_Rt_Pin_First; }

    public DbsField getRouter_Ex_File_Rt_Pin_Filler_1() { return router_Ex_File_Rt_Pin_Filler_1; }

    public DbsField getRouter_Ex_File_Rt_Pin_Mddle() { return router_Ex_File_Rt_Pin_Mddle; }

    public DbsField getRouter_Ex_File_Rt_Pin_Filler_2() { return router_Ex_File_Rt_Pin_Filler_2; }

    public DbsField getRouter_Ex_File_Rt_Pin_Last() { return router_Ex_File_Rt_Pin_Last; }

    public DbsField getRouter_Ex_File_Rt_Zip() { return router_Ex_File_Rt_Zip; }

    public DbsGroup getRouter_Ex_File_Rt_ZipRedef6() { return router_Ex_File_Rt_ZipRedef6; }

    public DbsField getRouter_Ex_File_Rt_Zip_Plus4() { return router_Ex_File_Rt_Zip_Plus4; }

    public DbsField getRouter_Ex_File_Rt_Zip_Plus1() { return router_Ex_File_Rt_Zip_Plus1; }

    public DbsField getRouter_Ex_File_Rt_Suspension() { return router_Ex_File_Rt_Suspension; }

    public DbsField getRouter_Ex_File_Rt_Mult_App_Version() { return router_Ex_File_Rt_Mult_App_Version; }

    public DbsField getRouter_Ex_File_Rt_Mult_App_Status() { return router_Ex_File_Rt_Mult_App_Status; }

    public DbsField getRouter_Ex_File_Rt_Mult_App_Lob() { return router_Ex_File_Rt_Mult_App_Lob; }

    public DbsField getRouter_Ex_File_Rt_Mult_App_Lob_Type() { return router_Ex_File_Rt_Mult_App_Lob_Type; }

    public DbsField getRouter_Ex_File_Rt_Mult_App_Ppg() { return router_Ex_File_Rt_Mult_App_Ppg; }

    public DbsField getRouter_Ex_File_Rt_K12_Ppg() { return router_Ex_File_Rt_K12_Ppg; }

    public DbsField getRouter_Ex_File_Rt_Ira_Rollover_Type() { return router_Ex_File_Rt_Ira_Rollover_Type; }

    public DbsField getRouter_Ex_File_Rt_Ira_Record_Type() { return router_Ex_File_Rt_Ira_Record_Type; }

    public DbsField getRouter_Ex_File_Rt_Rollover_Amt() { return router_Ex_File_Rt_Rollover_Amt; }

    public DbsField getRouter_Ex_File_Rt_Mit_Unit() { return router_Ex_File_Rt_Mit_Unit; }

    public DbsField getRouter_Ex_File_Rt_Mit_Wpid() { return router_Ex_File_Rt_Mit_Wpid; }

    public DbsField getRouter_Ex_File_Rt_New_Bene_Ind() { return router_Ex_File_Rt_New_Bene_Ind; }

    public DbsField getRouter_Ex_File_Rt_Irc_Sectn_Cde() { return router_Ex_File_Rt_Irc_Sectn_Cde; }

    public DbsField getRouter_Ex_File_Rt_Released_Dt() { return router_Ex_File_Rt_Released_Dt; }

    public DbsField getRouter_Ex_File_Rt_Irc_Sectn_Grp() { return router_Ex_File_Rt_Irc_Sectn_Grp; }

    public DbsField getRouter_Ex_File_Rt_Enroll_Request_Type() { return router_Ex_File_Rt_Enroll_Request_Type; }

    public DbsField getRouter_Ex_File_Rt_Erisa_Inst() { return router_Ex_File_Rt_Erisa_Inst; }

    public DbsField getRouter_Ex_File_Rt_Reprint_Request_Type() { return router_Ex_File_Rt_Reprint_Request_Type; }

    public DbsField getRouter_Ex_File_Rt_Print_Destination() { return router_Ex_File_Rt_Print_Destination; }

    public DbsField getRouter_Ex_File_Rt_Correction_Type() { return router_Ex_File_Rt_Correction_Type; }

    public DbsField getRouter_Ex_File_Rt_Processor_Name() { return router_Ex_File_Rt_Processor_Name; }

    public DbsField getRouter_Ex_File_Rt_Mail_Date() { return router_Ex_File_Rt_Mail_Date; }

    public DbsField getRouter_Ex_File_Rt_Alloc_Fmt() { return router_Ex_File_Rt_Alloc_Fmt; }

    public DbsField getRouter_Ex_File_Rt_Phone_No() { return router_Ex_File_Rt_Phone_No; }

    public DbsField getRouter_Ex_File_Rt_Fund_Cde() { return router_Ex_File_Rt_Fund_Cde; }

    public DbsField getRouter_Ex_File_Rt_Alloc_Pct() { return router_Ex_File_Rt_Alloc_Pct; }

    public DbsField getRouter_Ex_File_Rt_Oia_Ind() { return router_Ex_File_Rt_Oia_Ind; }

    public DbsField getRouter_Ex_File_Rt_Product_Cde() { return router_Ex_File_Rt_Product_Cde; }

    public DbsField getRouter_Ex_File_Rt_Acct_Sum_Sheet_Type() { return router_Ex_File_Rt_Acct_Sum_Sheet_Type; }

    public DbsField getRouter_Ex_File_Rt_Sg_Plan_No() { return router_Ex_File_Rt_Sg_Plan_No; }

    public DbsField getRouter_Ex_File_Rt_Sg_Subplan_No() { return router_Ex_File_Rt_Sg_Subplan_No; }

    public DbsField getRouter_Ex_File_Rt_Sg_Subplan_Type() { return router_Ex_File_Rt_Sg_Subplan_Type; }

    public DbsField getRouter_Ex_File_Rt_Omni_Issue_Ind() { return router_Ex_File_Rt_Omni_Issue_Ind; }

    public DbsField getRouter_Ex_File_Rt_Effective_Dt() { return router_Ex_File_Rt_Effective_Dt; }

    public DbsField getRouter_Ex_File_Rt_Employer_Name() { return router_Ex_File_Rt_Employer_Name; }

    public DbsField getRouter_Ex_File_Rt_Tiaa_Rate() { return router_Ex_File_Rt_Tiaa_Rate; }

    public DbsField getRouter_Ex_File_Rt_Sg_Plan_Id() { return router_Ex_File_Rt_Sg_Plan_Id; }

    public DbsField getRouter_Ex_File_Rt_Tiaa_Pg4_Numb() { return router_Ex_File_Rt_Tiaa_Pg4_Numb; }

    public DbsField getRouter_Ex_File_Rt_Cref_Pg4_Numb() { return router_Ex_File_Rt_Cref_Pg4_Numb; }

    public DbsField getRouter_Ex_File_Rt_Dflt_Access_Code() { return router_Ex_File_Rt_Dflt_Access_Code; }

    public DbsField getRouter_Ex_File_Rt_Single_Issue_Ind() { return router_Ex_File_Rt_Single_Issue_Ind; }

    public DbsField getRouter_Ex_File_Rt_Spec_Fund_Ind() { return router_Ex_File_Rt_Spec_Fund_Ind; }

    public DbsField getRouter_Ex_File_Rt_Product_Price_Level() { return router_Ex_File_Rt_Product_Price_Level; }

    public DbsField getRouter_Ex_File_Rt_Roth_Ind() { return router_Ex_File_Rt_Roth_Ind; }

    public DbsField getRouter_Ex_File_Rt_Plan_Issue_State() { return router_Ex_File_Rt_Plan_Issue_State; }

    public DbsField getRouter_Ex_File_Rt_Client_Id() { return router_Ex_File_Rt_Client_Id; }

    public DbsField getRouter_Ex_File_Rt_Portfolio_Model() { return router_Ex_File_Rt_Portfolio_Model; }

    public DbsField getRouter_Ex_File_Rt_Auto_Enroll_Ind() { return router_Ex_File_Rt_Auto_Enroll_Ind; }

    public DbsField getRouter_Ex_File_Rt_Auto_Save_Ind() { return router_Ex_File_Rt_Auto_Save_Ind; }

    public DbsField getRouter_Ex_File_Rt_As_Cur_Dflt_Amt() { return router_Ex_File_Rt_As_Cur_Dflt_Amt; }

    public DbsField getRouter_Ex_File_Rt_As_Incr_Amt() { return router_Ex_File_Rt_As_Incr_Amt; }

    public DbsField getRouter_Ex_File_Rt_As_Max_Pct() { return router_Ex_File_Rt_As_Max_Pct; }

    public DbsField getRouter_Ex_File_Rt_Ae_Opt_Out_Days() { return router_Ex_File_Rt_Ae_Opt_Out_Days; }

    public DbsField getRouter_Ex_File_Rt_Tsv_Ind() { return router_Ex_File_Rt_Tsv_Ind; }

    public DbsField getRouter_Ex_File_Rt_Tiaa_Indx_Guarntd_Rte() { return router_Ex_File_Rt_Tiaa_Indx_Guarntd_Rte; }

    public DbsField getRouter_Ex_File_Rt_Agent_Crd_No() { return router_Ex_File_Rt_Agent_Crd_No; }

    public DbsField getRouter_Ex_File_Rt_Crd_Pull_Ind() { return router_Ex_File_Rt_Crd_Pull_Ind; }

    public DbsField getRouter_Ex_File_Rt_Agent_Name() { return router_Ex_File_Rt_Agent_Name; }

    public DbsField getRouter_Ex_File_Rt_Agent_Work_Addr1() { return router_Ex_File_Rt_Agent_Work_Addr1; }

    public DbsField getRouter_Ex_File_Rt_Agent_Work_Addr2() { return router_Ex_File_Rt_Agent_Work_Addr2; }

    public DbsField getRouter_Ex_File_Rt_Agent_Work_City() { return router_Ex_File_Rt_Agent_Work_City; }

    public DbsField getRouter_Ex_File_Rt_Agent_Work_State() { return router_Ex_File_Rt_Agent_Work_State; }

    public DbsField getRouter_Ex_File_Rt_Agent_Work_Zip() { return router_Ex_File_Rt_Agent_Work_Zip; }

    public DbsField getRouter_Ex_File_Rt_Agent_Work_Phone() { return router_Ex_File_Rt_Agent_Work_Phone; }

    public DbsField getRouter_Ex_File_Rt_Tic_Ind() { return router_Ex_File_Rt_Tic_Ind; }

    public DbsField getRouter_Ex_File_Rt_Tic_Startdate() { return router_Ex_File_Rt_Tic_Startdate; }

    public DbsField getRouter_Ex_File_Rt_Tic_Enddate() { return router_Ex_File_Rt_Tic_Enddate; }

    public DbsField getRouter_Ex_File_Rt_Tic_Percentage() { return router_Ex_File_Rt_Tic_Percentage; }

    public DbsField getRouter_Ex_File_Rt_Tic_Postdays() { return router_Ex_File_Rt_Tic_Postdays; }

    public DbsField getRouter_Ex_File_Rt_Tic_Limit() { return router_Ex_File_Rt_Tic_Limit; }

    public DbsField getRouter_Ex_File_Rt_Tic_Postfreq() { return router_Ex_File_Rt_Tic_Postfreq; }

    public DbsField getRouter_Ex_File_Rt_Tic_Pl_Level() { return router_Ex_File_Rt_Tic_Pl_Level; }

    public DbsField getRouter_Ex_File_Rt_Tic_Windowdays() { return router_Ex_File_Rt_Tic_Windowdays; }

    public DbsField getRouter_Ex_File_Rt_Tic_Reqdlywindow() { return router_Ex_File_Rt_Tic_Reqdlywindow; }

    public DbsField getRouter_Ex_File_Rt_Tic_Recap_Prov() { return router_Ex_File_Rt_Tic_Recap_Prov; }

    public DbsField getRouter_Ex_File_Rt_Ecs_Dcs_E_Dlvry_Ind() { return router_Ex_File_Rt_Ecs_Dcs_E_Dlvry_Ind; }

    public DbsField getRouter_Ex_File_Rt_Ecs_Dcs_Annty_Optn_Ind() { return router_Ex_File_Rt_Ecs_Dcs_Annty_Optn_Ind; }

    public DbsField getRouter_Ex_File_Rt_Full_Middle_Name() { return router_Ex_File_Rt_Full_Middle_Name; }

    public DbsField getRouter_Ex_File_Rt_Substitution_Contract_Ind() { return router_Ex_File_Rt_Substitution_Contract_Ind; }

    public DbsField getRouter_Ex_File_Rt_Non_Proprietary_Pkg_Ind() { return router_Ex_File_Rt_Non_Proprietary_Pkg_Ind; }

    public DbsField getRouter_Ex_File_Rt_Multi_Plan_Table() { return router_Ex_File_Rt_Multi_Plan_Table; }

    public DbsGroup getRouter_Ex_File_Rt_Multi_Plan_TableRedef7() { return router_Ex_File_Rt_Multi_Plan_TableRedef7; }

    public DbsGroup getRouter_Ex_File_Rt_Multi_Plan_Info() { return router_Ex_File_Rt_Multi_Plan_Info; }

    public DbsField getRouter_Ex_File_Rt_Multi_Plan_No() { return router_Ex_File_Rt_Multi_Plan_No; }

    public DbsField getRouter_Ex_File_Rt_Multi_Sub_Plan() { return router_Ex_File_Rt_Multi_Sub_Plan; }

    public DbsField getRouter_Ex_File_Rt_Filler() { return router_Ex_File_Rt_Filler; }

    public DbsField getRouter_Ex_File_Rt_Tiaa_Init_Prem() { return router_Ex_File_Rt_Tiaa_Init_Prem; }

    public DbsField getRouter_Ex_File_Rt_Cref_Init_Prem() { return router_Ex_File_Rt_Cref_Init_Prem; }

    public DbsField getRouter_Ex_File_Rt_Related_Contract_Info() { return router_Ex_File_Rt_Related_Contract_Info; }

    public DbsGroup getRouter_Ex_File_Rt_Related_Contract_InfoRedef8() { return router_Ex_File_Rt_Related_Contract_InfoRedef8; }

    public DbsGroup getRouter_Ex_File_Rt_Related_Contract() { return router_Ex_File_Rt_Related_Contract; }

    public DbsField getRouter_Ex_File_Rt_Related_Contract_Type() { return router_Ex_File_Rt_Related_Contract_Type; }

    public DbsField getRouter_Ex_File_Rt_Related_Tiaa_No() { return router_Ex_File_Rt_Related_Tiaa_No; }

    public DbsField getRouter_Ex_File_Rt_Related_Tiaa_Issue_Date() { return router_Ex_File_Rt_Related_Tiaa_Issue_Date; }

    public DbsField getRouter_Ex_File_Rt_Related_First_Payment_Date() { return router_Ex_File_Rt_Related_First_Payment_Date; }

    public DbsField getRouter_Ex_File_Rt_Related_Tiaa_Total_Amt() { return router_Ex_File_Rt_Related_Tiaa_Total_Amt; }

    public DbsField getRouter_Ex_File_Rt_Related_Last_Payment_Date() { return router_Ex_File_Rt_Related_Last_Payment_Date; }

    public DbsField getRouter_Ex_File_Rt_Related_Payment_Frequency() { return router_Ex_File_Rt_Related_Payment_Frequency; }

    public DbsField getRouter_Ex_File_Rt_Related_Tiaa_Issue_State() { return router_Ex_File_Rt_Related_Tiaa_Issue_State; }

    public DbsField getRouter_Ex_File_Rt_First_Tpa_Ipro_Ind() { return router_Ex_File_Rt_First_Tpa_Ipro_Ind; }

    public DbsField getRouter_Ex_File_Rt_Fund_Source_1() { return router_Ex_File_Rt_Fund_Source_1; }

    public DbsField getRouter_Ex_File_Rt_Fund_Source_2() { return router_Ex_File_Rt_Fund_Source_2; }

    public DbsField getRouter_Ex_File_Rt_Fund_Cde_2() { return router_Ex_File_Rt_Fund_Cde_2; }

    public DbsField getRouter_Ex_File_Rt_Alloc_Pct_2() { return router_Ex_File_Rt_Alloc_Pct_2; }

    public DbsField getRouter_Ex_File_Rt_Job_Name() { return router_Ex_File_Rt_Job_Name; }

    public DbsField getRouter_Ex_File_Rt_Short_Name() { return router_Ex_File_Rt_Short_Name; }

    public DbsGroup getRouter_Ex_FileRedef9() { return router_Ex_FileRedef9; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_1() { return router_Ex_File_Router_Ext_Data_1; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_2() { return router_Ex_File_Router_Ext_Data_2; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_3() { return router_Ex_File_Router_Ext_Data_3; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_4() { return router_Ex_File_Router_Ext_Data_4; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_5() { return router_Ex_File_Router_Ext_Data_5; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_6() { return router_Ex_File_Router_Ext_Data_6; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_7() { return router_Ex_File_Router_Ext_Data_7; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_8() { return router_Ex_File_Router_Ext_Data_8; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_9() { return router_Ex_File_Router_Ext_Data_9; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_10() { return router_Ex_File_Router_Ext_Data_10; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_11() { return router_Ex_File_Router_Ext_Data_11; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_12() { return router_Ex_File_Router_Ext_Data_12; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_13() { return router_Ex_File_Router_Ext_Data_13; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_14() { return router_Ex_File_Router_Ext_Data_14; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_15() { return router_Ex_File_Router_Ext_Data_15; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_16() { return router_Ex_File_Router_Ext_Data_16; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_17() { return router_Ex_File_Router_Ext_Data_17; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_18() { return router_Ex_File_Router_Ext_Data_18; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_19() { return router_Ex_File_Router_Ext_Data_19; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_20() { return router_Ex_File_Router_Ext_Data_20; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_21() { return router_Ex_File_Router_Ext_Data_21; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_22() { return router_Ex_File_Router_Ext_Data_22; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_23() { return router_Ex_File_Router_Ext_Data_23; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_24() { return router_Ex_File_Router_Ext_Data_24; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_25() { return router_Ex_File_Router_Ext_Data_25; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_26() { return router_Ex_File_Router_Ext_Data_26; }

    public DbsField getRouter_Ex_File_Router_Ext_Data_27() { return router_Ex_File_Router_Ext_Data_27; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        router_Ex_File = newFieldInRecord("router_Ex_File", "ROUTER-EX-FILE", FieldType.STRING, 6620);
        router_Ex_FileRedef1 = newGroupInRecord("router_Ex_FileRedef1", "Redefines", router_Ex_File);
        router_Ex_File_Rt_Type_Cd = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Type_Cd", "RT-TYPE-CD", FieldType.STRING, 1);
        router_Ex_File_Rt_Tiaa_No_1 = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tiaa_No_1", "RT-TIAA-NO-1", FieldType.STRING, 8);
        router_Ex_File_Rt_Cref_No_1 = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Cref_No_1", "RT-CREF-NO-1", FieldType.STRING, 8);
        router_Ex_File_Rt_Tiaa_No_2 = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tiaa_No_2", "RT-TIAA-NO-2", FieldType.STRING, 8);
        router_Ex_File_Rt_Cref_No_2 = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Cref_No_2", "RT-CREF-NO-2", FieldType.STRING, 8);
        router_Ex_File_Rt_Tiaa_No_3 = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tiaa_No_3", "RT-TIAA-NO-3", FieldType.STRING, 8);
        router_Ex_File_Rt_Cref_No_3 = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Cref_No_3", "RT-CREF-NO-3", FieldType.STRING, 8);
        router_Ex_File_Rt_Tiaa_No_4 = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tiaa_No_4", "RT-TIAA-NO-4", FieldType.STRING, 8);
        router_Ex_File_Rt_Cref_No_4 = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Cref_No_4", "RT-CREF-NO-4", FieldType.STRING, 8);
        router_Ex_File_Rt_Date_Table = router_Ex_FileRedef1.newFieldArrayInGroup("router_Ex_File_Rt_Date_Table", "RT-DATE-TABLE", FieldType.STRING, 8, 
            new DbsArrayController(1,6));
        router_Ex_File_Rt_Age_First_Pymt = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Age_First_Pymt", "RT-AGE-FIRST-PYMT", FieldType.STRING, 
            4);
        router_Ex_File_Rt_Ownership = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Ownership", "RT-OWNERSHIP", FieldType.STRING, 1);
        router_Ex_File_Rt_Alloc_Disc = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Alloc_Disc", "RT-ALLOC-DISC", FieldType.STRING, 1);
        router_Ex_File_Rt_Currency = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Currency", "RT-CURRENCY", FieldType.STRING, 1);
        router_Ex_File_Rt_Tiaa_Extr_Form = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tiaa_Extr_Form", "RT-TIAA-EXTR-FORM", FieldType.STRING, 
            9);
        router_Ex_File_Rt_Cref_Extr_Form = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Cref_Extr_Form", "RT-CREF-EXTR-FORM", FieldType.STRING, 
            9);
        router_Ex_File_Rt_Tiaa_Extr_Ed = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tiaa_Extr_Ed", "RT-TIAA-EXTR-ED", FieldType.STRING, 9);
        router_Ex_File_Rt_Cref_Extr_Ed = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Cref_Extr_Ed", "RT-CREF-EXTR-ED", FieldType.STRING, 9);
        router_Ex_File_Rt_Annuitant_Name = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Annuitant_Name", "RT-ANNUITANT-NAME", FieldType.STRING, 
            38);
        router_Ex_File_Rt_Eop_Name = router_Ex_FileRedef1.newGroupInGroup("router_Ex_File_Rt_Eop_Name", "RT-EOP-NAME");
        router_Ex_File_Rt_Eop_Last_Name = router_Ex_File_Rt_Eop_Name.newFieldInGroup("router_Ex_File_Rt_Eop_Last_Name", "RT-EOP-LAST-NAME", FieldType.STRING, 
            16);
        router_Ex_File_Rt_Eop_First_Name = router_Ex_File_Rt_Eop_Name.newFieldInGroup("router_Ex_File_Rt_Eop_First_Name", "RT-EOP-FIRST-NAME", FieldType.STRING, 
            10);
        router_Ex_File_Rt_Eop_Middle_Name = router_Ex_File_Rt_Eop_Name.newFieldInGroup("router_Ex_File_Rt_Eop_Middle_Name", "RT-EOP-MIDDLE-NAME", FieldType.STRING, 
            10);
        router_Ex_File_Rt_Eop_Middle_NameRedef2 = router_Ex_File_Rt_Eop_Name.newGroupInGroup("router_Ex_File_Rt_Eop_Middle_NameRedef2", "Redefines", router_Ex_File_Rt_Eop_Middle_Name);
        router_Ex_File_Rt_Eop_Mid_Init = router_Ex_File_Rt_Eop_Middle_NameRedef2.newFieldInGroup("router_Ex_File_Rt_Eop_Mid_Init", "RT-EOP-MID-INIT", 
            FieldType.STRING, 1);
        router_Ex_File_Rt_Eop_Mid_Filler = router_Ex_File_Rt_Eop_Middle_NameRedef2.newFieldInGroup("router_Ex_File_Rt_Eop_Mid_Filler", "RT-EOP-MID-FILLER", 
            FieldType.STRING, 9);
        router_Ex_File_Rt_Eop_Title = router_Ex_File_Rt_Eop_Name.newFieldInGroup("router_Ex_File_Rt_Eop_Title", "RT-EOP-TITLE", FieldType.STRING, 4);
        router_Ex_File_Rt_Eop_Prefix = router_Ex_File_Rt_Eop_Name.newFieldInGroup("router_Ex_File_Rt_Eop_Prefix", "RT-EOP-PREFIX", FieldType.STRING, 4);
        router_Ex_File_Rt_Soc_Sec = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Soc_Sec", "RT-SOC-SEC", FieldType.STRING, 11);
        router_Ex_File_Rt_Pin_No = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Pin_No", "RT-PIN-NO", FieldType.STRING, 12);
        router_Ex_File_Rt_Address_Table = router_Ex_FileRedef1.newFieldArrayInGroup("router_Ex_File_Rt_Address_Table", "RT-ADDRESS-TABLE", FieldType.STRING, 
            35, new DbsArrayController(1,6));
        router_Ex_File_Rt_Eop_Zipcode = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Eop_Zipcode", "RT-EOP-ZIPCODE", FieldType.STRING, 5);
        router_Ex_File_Rt_Participant_Status = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Participant_Status", "RT-PARTICIPANT-STATUS", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Email_Address = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Email_Address", "RT-EMAIL-ADDRESS", FieldType.STRING, 
            50);
        router_Ex_File_Rt_Request_Package_Type = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Request_Package_Type", "RT-REQUEST-PACKAGE-TYPE", 
            FieldType.STRING, 1);
        router_Ex_File_Rt_Portfolio_Selected = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Portfolio_Selected", "RT-PORTFOLIO-SELECTED", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Divorce_Contract = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Divorce_Contract", "RT-DIVORCE-CONTRACT", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Access_Cd_Ind = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Access_Cd_Ind", "RT-ACCESS-CD-IND", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Address_Change = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Address_Change", "RT-ADDRESS-CHANGE", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Package_Version = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Package_Version", "RT-PACKAGE-VERSION", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Unit_Name = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Unit_Name", "RT-UNIT-NAME", FieldType.STRING, 10);
        router_Ex_File_Rt_Print_Package_Type = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Print_Package_Type", "RT-PRINT-PACKAGE-TYPE", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Future_Filler = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Future_Filler", "RT-FUTURE-FILLER", FieldType.STRING, 
            5);
        router_Ex_File_Rt_Bene_Estate = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Bene_Estate", "RT-BENE-ESTATE", FieldType.STRING, 1);
        router_Ex_File_Rt_Bene_Trust = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Bene_Trust", "RT-BENE-TRUST", FieldType.STRING, 1);
        router_Ex_File_Rt_Bene_Category = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Bene_Category", "RT-BENE-CATEGORY", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Sex = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Sex", "RT-SEX", FieldType.STRING, 1);
        router_Ex_File_Rt_Inst_Name_Table = router_Ex_FileRedef1.newFieldArrayInGroup("router_Ex_File_Rt_Inst_Name_Table", "RT-INST-NAME-TABLE", FieldType.STRING, 
            30, new DbsArrayController(1,10));
        router_Ex_File_Rt_Inst_Name = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Inst_Name", "RT-INST-NAME", FieldType.STRING, 76);
        router_Ex_File_Rt_Inst_Address_Table = router_Ex_FileRedef1.newFieldArrayInGroup("router_Ex_File_Rt_Inst_Address_Table", "RT-INST-ADDRESS-TABLE", 
            FieldType.STRING, 35, new DbsArrayController(1,6));
        router_Ex_File_Rt_Inst_Zip_Cd = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Inst_Zip_Cd", "RT-INST-ZIP-CD", FieldType.STRING, 5);
        router_Ex_File_Rt_Mail_Instructions = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Mail_Instructions", "RT-MAIL-INSTRUCTIONS", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Orig_Iss_State = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Orig_Iss_State", "RT-ORIG-ISS-STATE", FieldType.STRING, 
            2);
        router_Ex_File_Rt_Current_Iss_State = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Current_Iss_State", "RT-CURRENT-ISS-STATE", FieldType.STRING, 
            2);
        router_Ex_File_Rt_Eop_State_Name = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Eop_State_Name", "RT-EOP-STATE-NAME", FieldType.STRING, 
            15);
        router_Ex_File_Rt_Lob = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Lob", "RT-LOB", FieldType.STRING, 1);
        router_Ex_File_Rt_Lob_Type = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Lob_Type", "RT-LOB-TYPE", FieldType.STRING, 1);
        router_Ex_File_Rt_Inst_Code = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Inst_Code", "RT-INST-CODE", FieldType.STRING, 4);
        router_Ex_File_Rt_Inst_Bill_Cd = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Inst_Bill_Cd", "RT-INST-BILL-CD", FieldType.STRING, 1);
        router_Ex_File_Rt_Inst_Permit_Trans = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Inst_Permit_Trans", "RT-INST-PERMIT-TRANS", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Perm_Gra_Trans = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Perm_Gra_Trans", "RT-PERM-GRA-TRANS", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Inst_Full_Immed_Vest = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Inst_Full_Immed_Vest", "RT-INST-FULL-IMMED-VEST", 
            FieldType.STRING, 1);
        router_Ex_File_Rt_Inst_Cashable_Ra = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Inst_Cashable_Ra", "RT-INST-CASHABLE-RA", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Inst_Cashable_Gra = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Inst_Cashable_Gra", "RT-INST-CASHABLE-GRA", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Inst_Fixed_Per_Opt = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Inst_Fixed_Per_Opt", "RT-INST-FIXED-PER-OPT", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Inst_Cntrl_Consent = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Inst_Cntrl_Consent", "RT-INST-CNTRL-CONSENT", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Eop = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Eop", "RT-EOP", FieldType.STRING, 1);
        router_Ex_File_Rt_Gsra_Inst_St_Code = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Gsra_Inst_St_Code", "RT-GSRA-INST-ST-CODE", FieldType.STRING, 
            2);
        router_Ex_File_Rt_Region_Cd = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Region_Cd", "RT-REGION-CD", FieldType.STRING, 3);
        router_Ex_File_Rt_Staff_Id = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Staff_Id", "RT-STAFF-ID", FieldType.STRING, 3);
        router_Ex_File_Rt_Appl_Source = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Appl_Source", "RT-APPL-SOURCE", FieldType.STRING, 1);
        router_Ex_File_Rt_Eop_Appl_Status = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Eop_Appl_Status", "RT-EOP-APPL-STATUS", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Eop_Addl_Cref_Req = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Eop_Addl_Cref_Req", "RT-EOP-ADDL-CREF-REQ", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Extract_Date = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Extract_Date", "RT-EXTRACT-DATE", FieldType.STRING, 6);
        router_Ex_File_Rt_Gsra_Loan_Ind = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Gsra_Loan_Ind", "RT-GSRA-LOAN-IND", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Spec_Ppg_Ind = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Spec_Ppg_Ind", "RT-SPEC-PPG-IND", FieldType.STRING, 2);
        router_Ex_File_Rt_Mail_Pull_Cd = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Mail_Pull_Cd", "RT-MAIL-PULL-CD", FieldType.STRING, 1);
        router_Ex_File_Rt_Mit_Log_Dt_Tm = router_Ex_FileRedef1.newFieldArrayInGroup("router_Ex_File_Rt_Mit_Log_Dt_Tm", "RT-MIT-LOG-DT-TM", FieldType.STRING, 
            15, new DbsArrayController(1,5));
        router_Ex_File_Rt_Addr_Page_Name = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Addr_Page_Name", "RT-ADDR-PAGE-NAME", FieldType.STRING, 
            81);
        router_Ex_File_Rt_Addr_Page_NameRedef3 = router_Ex_FileRedef1.newGroupInGroup("router_Ex_File_Rt_Addr_Page_NameRedef3", "Redefines", router_Ex_File_Rt_Addr_Page_Name);
        router_Ex_File_Rt_Addr_Pref = router_Ex_File_Rt_Addr_Page_NameRedef3.newFieldInGroup("router_Ex_File_Rt_Addr_Pref", "RT-ADDR-PREF", FieldType.STRING, 
            8);
        router_Ex_File_Rt_Addr_Filler_1 = router_Ex_File_Rt_Addr_Page_NameRedef3.newFieldInGroup("router_Ex_File_Rt_Addr_Filler_1", "RT-ADDR-FILLER-1", 
            FieldType.STRING, 1);
        router_Ex_File_Rt_Addr_Frst = router_Ex_File_Rt_Addr_Page_NameRedef3.newFieldInGroup("router_Ex_File_Rt_Addr_Frst", "RT-ADDR-FRST", FieldType.STRING, 
            30);
        router_Ex_File_Rt_Addr_Filler_2 = router_Ex_File_Rt_Addr_Page_NameRedef3.newFieldInGroup("router_Ex_File_Rt_Addr_Filler_2", "RT-ADDR-FILLER-2", 
            FieldType.STRING, 1);
        router_Ex_File_Rt_Addr_Mddle = router_Ex_File_Rt_Addr_Page_NameRedef3.newFieldInGroup("router_Ex_File_Rt_Addr_Mddle", "RT-ADDR-MDDLE", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Addr_Filler_3 = router_Ex_File_Rt_Addr_Page_NameRedef3.newFieldInGroup("router_Ex_File_Rt_Addr_Filler_3", "RT-ADDR-FILLER-3", 
            FieldType.STRING, 1);
        router_Ex_File_Rt_Addr_Last = router_Ex_File_Rt_Addr_Page_NameRedef3.newFieldInGroup("router_Ex_File_Rt_Addr_Last", "RT-ADDR-LAST", FieldType.STRING, 
            30);
        router_Ex_File_Rt_Addr_Filler_4 = router_Ex_File_Rt_Addr_Page_NameRedef3.newFieldInGroup("router_Ex_File_Rt_Addr_Filler_4", "RT-ADDR-FILLER-4", 
            FieldType.STRING, 1);
        router_Ex_File_Rt_Addr_Sffx = router_Ex_File_Rt_Addr_Page_NameRedef3.newFieldInGroup("router_Ex_File_Rt_Addr_Sffx", "RT-ADDR-SFFX", FieldType.STRING, 
            8);
        router_Ex_File_Rt_Welcome_Name = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Welcome_Name", "RT-WELCOME-NAME", FieldType.STRING, 39);
        router_Ex_File_Rt_Welcome_NameRedef4 = router_Ex_FileRedef1.newGroupInGroup("router_Ex_File_Rt_Welcome_NameRedef4", "Redefines", router_Ex_File_Rt_Welcome_Name);
        router_Ex_File_Rt_Welcome_Pref = router_Ex_File_Rt_Welcome_NameRedef4.newFieldInGroup("router_Ex_File_Rt_Welcome_Pref", "RT-WELCOME-PREF", FieldType.STRING, 
            8);
        router_Ex_File_Rt_Welcome_Filler_1 = router_Ex_File_Rt_Welcome_NameRedef4.newFieldInGroup("router_Ex_File_Rt_Welcome_Filler_1", "RT-WELCOME-FILLER-1", 
            FieldType.STRING, 1);
        router_Ex_File_Rt_Welcome_Last = router_Ex_File_Rt_Welcome_NameRedef4.newFieldInGroup("router_Ex_File_Rt_Welcome_Last", "RT-WELCOME-LAST", FieldType.STRING, 
            30);
        router_Ex_File_Rt_Pin_Name = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Pin_Name", "RT-PIN-NAME", FieldType.STRING, 63);
        router_Ex_File_Rt_Pin_NameRedef5 = router_Ex_FileRedef1.newGroupInGroup("router_Ex_File_Rt_Pin_NameRedef5", "Redefines", router_Ex_File_Rt_Pin_Name);
        router_Ex_File_Rt_Pin_First = router_Ex_File_Rt_Pin_NameRedef5.newFieldInGroup("router_Ex_File_Rt_Pin_First", "RT-PIN-FIRST", FieldType.STRING, 
            30);
        router_Ex_File_Rt_Pin_Filler_1 = router_Ex_File_Rt_Pin_NameRedef5.newFieldInGroup("router_Ex_File_Rt_Pin_Filler_1", "RT-PIN-FILLER-1", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Pin_Mddle = router_Ex_File_Rt_Pin_NameRedef5.newFieldInGroup("router_Ex_File_Rt_Pin_Mddle", "RT-PIN-MDDLE", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Pin_Filler_2 = router_Ex_File_Rt_Pin_NameRedef5.newFieldInGroup("router_Ex_File_Rt_Pin_Filler_2", "RT-PIN-FILLER-2", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Pin_Last = router_Ex_File_Rt_Pin_NameRedef5.newFieldInGroup("router_Ex_File_Rt_Pin_Last", "RT-PIN-LAST", FieldType.STRING, 30);
        router_Ex_File_Rt_Zip = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Zip", "RT-ZIP", FieldType.STRING, 5);
        router_Ex_File_Rt_ZipRedef6 = router_Ex_FileRedef1.newGroupInGroup("router_Ex_File_Rt_ZipRedef6", "Redefines", router_Ex_File_Rt_Zip);
        router_Ex_File_Rt_Zip_Plus4 = router_Ex_File_Rt_ZipRedef6.newFieldInGroup("router_Ex_File_Rt_Zip_Plus4", "RT-ZIP-PLUS4", FieldType.STRING, 4);
        router_Ex_File_Rt_Zip_Plus1 = router_Ex_File_Rt_ZipRedef6.newFieldInGroup("router_Ex_File_Rt_Zip_Plus1", "RT-ZIP-PLUS1", FieldType.STRING, 1);
        router_Ex_File_Rt_Suspension = router_Ex_FileRedef1.newFieldArrayInGroup("router_Ex_File_Rt_Suspension", "RT-SUSPENSION", FieldType.STRING, 1, 
            new DbsArrayController(1,20));
        router_Ex_File_Rt_Mult_App_Version = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Mult_App_Version", "RT-MULT-APP-VERSION", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Mult_App_Status = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Mult_App_Status", "RT-MULT-APP-STATUS", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Mult_App_Lob = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Mult_App_Lob", "RT-MULT-APP-LOB", FieldType.STRING, 1);
        router_Ex_File_Rt_Mult_App_Lob_Type = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Mult_App_Lob_Type", "RT-MULT-APP-LOB-TYPE", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Mult_App_Ppg = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Mult_App_Ppg", "RT-MULT-APP-PPG", FieldType.STRING, 6);
        router_Ex_File_Rt_K12_Ppg = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_K12_Ppg", "RT-K12-PPG", FieldType.STRING, 1);
        router_Ex_File_Rt_Ira_Rollover_Type = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Ira_Rollover_Type", "RT-IRA-ROLLOVER-TYPE", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Ira_Record_Type = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Ira_Record_Type", "RT-IRA-RECORD-TYPE", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Rollover_Amt = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Rollover_Amt", "RT-ROLLOVER-AMT", FieldType.STRING, 14);
        router_Ex_File_Rt_Mit_Unit = router_Ex_FileRedef1.newFieldArrayInGroup("router_Ex_File_Rt_Mit_Unit", "RT-MIT-UNIT", FieldType.STRING, 8, new DbsArrayController(1,
            5));
        router_Ex_File_Rt_Mit_Wpid = router_Ex_FileRedef1.newFieldArrayInGroup("router_Ex_File_Rt_Mit_Wpid", "RT-MIT-WPID", FieldType.STRING, 6, new DbsArrayController(1,
            5));
        router_Ex_File_Rt_New_Bene_Ind = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_New_Bene_Ind", "RT-NEW-BENE-IND", FieldType.STRING, 1);
        router_Ex_File_Rt_Irc_Sectn_Cde = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Irc_Sectn_Cde", "RT-IRC-SECTN-CDE", FieldType.NUMERIC, 
            2);
        router_Ex_File_Rt_Released_Dt = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Released_Dt", "RT-RELEASED-DT", FieldType.NUMERIC, 8);
        router_Ex_File_Rt_Irc_Sectn_Grp = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Irc_Sectn_Grp", "RT-IRC-SECTN-GRP", FieldType.NUMERIC, 
            2);
        router_Ex_File_Rt_Enroll_Request_Type = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Enroll_Request_Type", "RT-ENROLL-REQUEST-TYPE", 
            FieldType.STRING, 1);
        router_Ex_File_Rt_Erisa_Inst = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Erisa_Inst", "RT-ERISA-INST", FieldType.STRING, 1);
        router_Ex_File_Rt_Reprint_Request_Type = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Reprint_Request_Type", "RT-REPRINT-REQUEST-TYPE", 
            FieldType.STRING, 1);
        router_Ex_File_Rt_Print_Destination = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Print_Destination", "RT-PRINT-DESTINATION", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Correction_Type = router_Ex_FileRedef1.newFieldArrayInGroup("router_Ex_File_Rt_Correction_Type", "RT-CORRECTION-TYPE", FieldType.STRING, 
            1, new DbsArrayController(1,20));
        router_Ex_File_Rt_Processor_Name = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Processor_Name", "RT-PROCESSOR-NAME", FieldType.STRING, 
            40);
        router_Ex_File_Rt_Mail_Date = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Mail_Date", "RT-MAIL-DATE", FieldType.STRING, 17);
        router_Ex_File_Rt_Alloc_Fmt = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Alloc_Fmt", "RT-ALLOC-FMT", FieldType.STRING, 1);
        router_Ex_File_Rt_Phone_No = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Phone_No", "RT-PHONE-NO", FieldType.STRING, 20);
        router_Ex_File_Rt_Fund_Cde = router_Ex_FileRedef1.newFieldArrayInGroup("router_Ex_File_Rt_Fund_Cde", "RT-FUND-CDE", FieldType.STRING, 10, new 
            DbsArrayController(1,100));
        router_Ex_File_Rt_Alloc_Pct = router_Ex_FileRedef1.newFieldArrayInGroup("router_Ex_File_Rt_Alloc_Pct", "RT-ALLOC-PCT", FieldType.STRING, 3, new 
            DbsArrayController(1,100));
        router_Ex_File_Rt_Oia_Ind = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Oia_Ind", "RT-OIA-IND", FieldType.STRING, 1);
        router_Ex_File_Rt_Product_Cde = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Product_Cde", "RT-PRODUCT-CDE", FieldType.STRING, 3);
        router_Ex_File_Rt_Acct_Sum_Sheet_Type = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Acct_Sum_Sheet_Type", "RT-ACCT-SUM-SHEET-TYPE", 
            FieldType.STRING, 56);
        router_Ex_File_Rt_Sg_Plan_No = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Sg_Plan_No", "RT-SG-PLAN-NO", FieldType.STRING, 6);
        router_Ex_File_Rt_Sg_Subplan_No = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Sg_Subplan_No", "RT-SG-SUBPLAN-NO", FieldType.STRING, 
            6);
        router_Ex_File_Rt_Sg_Subplan_Type = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Sg_Subplan_Type", "RT-SG-SUBPLAN-TYPE", FieldType.STRING, 
            3);
        router_Ex_File_Rt_Omni_Issue_Ind = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Omni_Issue_Ind", "RT-OMNI-ISSUE-IND", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Effective_Dt = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Effective_Dt", "RT-EFFECTIVE-DT", FieldType.STRING, 8);
        router_Ex_File_Rt_Employer_Name = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Employer_Name", "RT-EMPLOYER-NAME", FieldType.STRING, 
            40);
        router_Ex_File_Rt_Tiaa_Rate = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tiaa_Rate", "RT-TIAA-RATE", FieldType.STRING, 8);
        router_Ex_File_Rt_Sg_Plan_Id = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Sg_Plan_Id", "RT-SG-PLAN-ID", FieldType.STRING, 6);
        router_Ex_File_Rt_Tiaa_Pg4_Numb = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tiaa_Pg4_Numb", "RT-TIAA-PG4-NUMB", FieldType.STRING, 
            15);
        router_Ex_File_Rt_Cref_Pg4_Numb = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Cref_Pg4_Numb", "RT-CREF-PG4-NUMB", FieldType.STRING, 
            15);
        router_Ex_File_Rt_Dflt_Access_Code = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Dflt_Access_Code", "RT-DFLT-ACCESS-CODE", FieldType.STRING, 
            9);
        router_Ex_File_Rt_Single_Issue_Ind = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Single_Issue_Ind", "RT-SINGLE-ISSUE-IND", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Spec_Fund_Ind = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Spec_Fund_Ind", "RT-SPEC-FUND-IND", FieldType.STRING, 
            3);
        router_Ex_File_Rt_Product_Price_Level = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Product_Price_Level", "RT-PRODUCT-PRICE-LEVEL", 
            FieldType.STRING, 5);
        router_Ex_File_Rt_Roth_Ind = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Roth_Ind", "RT-ROTH-IND", FieldType.STRING, 1);
        router_Ex_File_Rt_Plan_Issue_State = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Plan_Issue_State", "RT-PLAN-ISSUE-STATE", FieldType.STRING, 
            2);
        router_Ex_File_Rt_Client_Id = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Client_Id", "RT-CLIENT-ID", FieldType.STRING, 6);
        router_Ex_File_Rt_Portfolio_Model = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Portfolio_Model", "RT-PORTFOLIO-MODEL", FieldType.STRING, 
            4);
        router_Ex_File_Rt_Auto_Enroll_Ind = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Auto_Enroll_Ind", "RT-AUTO-ENROLL-IND", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Auto_Save_Ind = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Auto_Save_Ind", "RT-AUTO-SAVE-IND", FieldType.STRING, 
            1);
        router_Ex_File_Rt_As_Cur_Dflt_Amt = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_As_Cur_Dflt_Amt", "RT-AS-CUR-DFLT-AMT", FieldType.STRING, 
            10);
        router_Ex_File_Rt_As_Incr_Amt = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_As_Incr_Amt", "RT-AS-INCR-AMT", FieldType.STRING, 7);
        router_Ex_File_Rt_As_Max_Pct = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_As_Max_Pct", "RT-AS-MAX-PCT", FieldType.STRING, 7);
        router_Ex_File_Rt_Ae_Opt_Out_Days = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Ae_Opt_Out_Days", "RT-AE-OPT-OUT-DAYS", FieldType.STRING, 
            2);
        router_Ex_File_Rt_Tsv_Ind = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tsv_Ind", "RT-TSV-IND", FieldType.STRING, 1);
        router_Ex_File_Rt_Tiaa_Indx_Guarntd_Rte = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tiaa_Indx_Guarntd_Rte", "RT-TIAA-INDX-GUARNTD-RTE", 
            FieldType.STRING, 6);
        router_Ex_File_Rt_Agent_Crd_No = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Agent_Crd_No", "RT-AGENT-CRD-NO", FieldType.STRING, 15);
        router_Ex_File_Rt_Crd_Pull_Ind = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Crd_Pull_Ind", "RT-CRD-PULL-IND", FieldType.STRING, 1);
        router_Ex_File_Rt_Agent_Name = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Agent_Name", "RT-AGENT-NAME", FieldType.STRING, 90);
        router_Ex_File_Rt_Agent_Work_Addr1 = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Agent_Work_Addr1", "RT-AGENT-WORK-ADDR1", FieldType.STRING, 
            35);
        router_Ex_File_Rt_Agent_Work_Addr2 = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Agent_Work_Addr2", "RT-AGENT-WORK-ADDR2", FieldType.STRING, 
            35);
        router_Ex_File_Rt_Agent_Work_City = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Agent_Work_City", "RT-AGENT-WORK-CITY", FieldType.STRING, 
            35);
        router_Ex_File_Rt_Agent_Work_State = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Agent_Work_State", "RT-AGENT-WORK-STATE", FieldType.STRING, 
            2);
        router_Ex_File_Rt_Agent_Work_Zip = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Agent_Work_Zip", "RT-AGENT-WORK-ZIP", FieldType.STRING, 
            10);
        router_Ex_File_Rt_Agent_Work_Phone = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Agent_Work_Phone", "RT-AGENT-WORK-PHONE", FieldType.STRING, 
            25);
        router_Ex_File_Rt_Tic_Ind = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tic_Ind", "RT-TIC-IND", FieldType.STRING, 1);
        router_Ex_File_Rt_Tic_Startdate = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tic_Startdate", "RT-TIC-STARTDATE", FieldType.STRING, 
            8);
        router_Ex_File_Rt_Tic_Enddate = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tic_Enddate", "RT-TIC-ENDDATE", FieldType.STRING, 8);
        router_Ex_File_Rt_Tic_Percentage = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tic_Percentage", "RT-TIC-PERCENTAGE", FieldType.STRING, 
            9);
        router_Ex_File_Rt_Tic_Postdays = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tic_Postdays", "RT-TIC-POSTDAYS", FieldType.STRING, 2);
        router_Ex_File_Rt_Tic_Limit = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tic_Limit", "RT-TIC-LIMIT", FieldType.STRING, 11);
        router_Ex_File_Rt_Tic_Postfreq = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tic_Postfreq", "RT-TIC-POSTFREQ", FieldType.STRING, 1);
        router_Ex_File_Rt_Tic_Pl_Level = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tic_Pl_Level", "RT-TIC-PL-LEVEL", FieldType.STRING, 1);
        router_Ex_File_Rt_Tic_Windowdays = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tic_Windowdays", "RT-TIC-WINDOWDAYS", FieldType.STRING, 
            3);
        router_Ex_File_Rt_Tic_Reqdlywindow = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tic_Reqdlywindow", "RT-TIC-REQDLYWINDOW", FieldType.STRING, 
            3);
        router_Ex_File_Rt_Tic_Recap_Prov = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tic_Recap_Prov", "RT-TIC-RECAP-PROV", FieldType.STRING, 
            3);
        router_Ex_File_Rt_Ecs_Dcs_E_Dlvry_Ind = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Ecs_Dcs_E_Dlvry_Ind", "RT-ECS-DCS-E-DLVRY-IND", 
            FieldType.STRING, 1);
        router_Ex_File_Rt_Ecs_Dcs_Annty_Optn_Ind = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Ecs_Dcs_Annty_Optn_Ind", "RT-ECS-DCS-ANNTY-OPTN-IND", 
            FieldType.STRING, 1);
        router_Ex_File_Rt_Full_Middle_Name = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Full_Middle_Name", "RT-FULL-MIDDLE-NAME", FieldType.STRING, 
            30);
        router_Ex_File_Rt_Substitution_Contract_Ind = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Substitution_Contract_Ind", "RT-SUBSTITUTION-CONTRACT-IND", 
            FieldType.STRING, 1);
        router_Ex_File_Rt_Non_Proprietary_Pkg_Ind = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Non_Proprietary_Pkg_Ind", "RT-NON-PROPRIETARY-PKG-IND", 
            FieldType.STRING, 5);
        router_Ex_File_Rt_Multi_Plan_Table = router_Ex_FileRedef1.newFieldArrayInGroup("router_Ex_File_Rt_Multi_Plan_Table", "RT-MULTI-PLAN-TABLE", FieldType.STRING, 
            12, new DbsArrayController(1,15));
        router_Ex_File_Rt_Multi_Plan_TableRedef7 = router_Ex_FileRedef1.newGroupInGroup("router_Ex_File_Rt_Multi_Plan_TableRedef7", "Redefines", router_Ex_File_Rt_Multi_Plan_Table);
        router_Ex_File_Rt_Multi_Plan_Info = router_Ex_File_Rt_Multi_Plan_TableRedef7.newGroupArrayInGroup("router_Ex_File_Rt_Multi_Plan_Info", "RT-MULTI-PLAN-INFO", 
            new DbsArrayController(1,15));
        router_Ex_File_Rt_Multi_Plan_No = router_Ex_File_Rt_Multi_Plan_Info.newFieldInGroup("router_Ex_File_Rt_Multi_Plan_No", "RT-MULTI-PLAN-NO", FieldType.STRING, 
            6);
        router_Ex_File_Rt_Multi_Sub_Plan = router_Ex_File_Rt_Multi_Plan_Info.newFieldInGroup("router_Ex_File_Rt_Multi_Sub_Plan", "RT-MULTI-SUB-PLAN", 
            FieldType.STRING, 6);
        router_Ex_File_Rt_Filler = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Filler", "RT-FILLER", FieldType.STRING, 35);
        router_Ex_File_Rt_Tiaa_Init_Prem = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Tiaa_Init_Prem", "RT-TIAA-INIT-PREM", FieldType.DECIMAL, 
            10,2);
        router_Ex_File_Rt_Cref_Init_Prem = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Cref_Init_Prem", "RT-CREF-INIT-PREM", FieldType.DECIMAL, 
            10,2);
        router_Ex_File_Rt_Related_Contract_Info = router_Ex_FileRedef1.newFieldArrayInGroup("router_Ex_File_Rt_Related_Contract_Info", "RT-RELATED-CONTRACT-INFO", 
            FieldType.STRING, 49, new DbsArrayController(1,30));
        router_Ex_File_Rt_Related_Contract_InfoRedef8 = router_Ex_FileRedef1.newGroupInGroup("router_Ex_File_Rt_Related_Contract_InfoRedef8", "Redefines", 
            router_Ex_File_Rt_Related_Contract_Info);
        router_Ex_File_Rt_Related_Contract = router_Ex_File_Rt_Related_Contract_InfoRedef8.newGroupArrayInGroup("router_Ex_File_Rt_Related_Contract", 
            "RT-RELATED-CONTRACT", new DbsArrayController(1,30));
        router_Ex_File_Rt_Related_Contract_Type = router_Ex_File_Rt_Related_Contract.newFieldInGroup("router_Ex_File_Rt_Related_Contract_Type", "RT-RELATED-CONTRACT-TYPE", 
            FieldType.STRING, 1);
        router_Ex_File_Rt_Related_Tiaa_No = router_Ex_File_Rt_Related_Contract.newFieldInGroup("router_Ex_File_Rt_Related_Tiaa_No", "RT-RELATED-TIAA-NO", 
            FieldType.STRING, 10);
        router_Ex_File_Rt_Related_Tiaa_Issue_Date = router_Ex_File_Rt_Related_Contract.newFieldInGroup("router_Ex_File_Rt_Related_Tiaa_Issue_Date", "RT-RELATED-TIAA-ISSUE-DATE", 
            FieldType.NUMERIC, 8);
        router_Ex_File_Rt_Related_First_Payment_Date = router_Ex_File_Rt_Related_Contract.newFieldInGroup("router_Ex_File_Rt_Related_First_Payment_Date", 
            "RT-RELATED-FIRST-PAYMENT-DATE", FieldType.NUMERIC, 8);
        router_Ex_File_Rt_Related_Tiaa_Total_Amt = router_Ex_File_Rt_Related_Contract.newFieldInGroup("router_Ex_File_Rt_Related_Tiaa_Total_Amt", "RT-RELATED-TIAA-TOTAL-AMT", 
            FieldType.DECIMAL, 10,2);
        router_Ex_File_Rt_Related_Last_Payment_Date = router_Ex_File_Rt_Related_Contract.newFieldInGroup("router_Ex_File_Rt_Related_Last_Payment_Date", 
            "RT-RELATED-LAST-PAYMENT-DATE", FieldType.NUMERIC, 8);
        router_Ex_File_Rt_Related_Payment_Frequency = router_Ex_File_Rt_Related_Contract.newFieldInGroup("router_Ex_File_Rt_Related_Payment_Frequency", 
            "RT-RELATED-PAYMENT-FREQUENCY", FieldType.STRING, 1);
        router_Ex_File_Rt_Related_Tiaa_Issue_State = router_Ex_File_Rt_Related_Contract.newFieldInGroup("router_Ex_File_Rt_Related_Tiaa_Issue_State", 
            "RT-RELATED-TIAA-ISSUE-STATE", FieldType.STRING, 2);
        router_Ex_File_Rt_First_Tpa_Ipro_Ind = router_Ex_File_Rt_Related_Contract.newFieldInGroup("router_Ex_File_Rt_First_Tpa_Ipro_Ind", "RT-FIRST-TPA-IPRO-IND", 
            FieldType.STRING, 1);
        router_Ex_File_Rt_Fund_Source_1 = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Fund_Source_1", "RT-FUND-SOURCE-1", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Fund_Source_2 = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Fund_Source_2", "RT-FUND-SOURCE-2", FieldType.STRING, 
            1);
        router_Ex_File_Rt_Fund_Cde_2 = router_Ex_FileRedef1.newFieldArrayInGroup("router_Ex_File_Rt_Fund_Cde_2", "RT-FUND-CDE-2", FieldType.STRING, 10, 
            new DbsArrayController(1,100));
        router_Ex_File_Rt_Alloc_Pct_2 = router_Ex_FileRedef1.newFieldArrayInGroup("router_Ex_File_Rt_Alloc_Pct_2", "RT-ALLOC-PCT-2", FieldType.STRING, 
            3, new DbsArrayController(1,100));
        router_Ex_File_Rt_Job_Name = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Job_Name", "RT-JOB-NAME", FieldType.STRING, 8);
        router_Ex_File_Rt_Short_Name = router_Ex_FileRedef1.newFieldInGroup("router_Ex_File_Rt_Short_Name", "RT-SHORT-NAME", FieldType.STRING, 40);
        router_Ex_FileRedef9 = newGroupInRecord("router_Ex_FileRedef9", "Redefines", router_Ex_File);
        router_Ex_File_Router_Ext_Data_1 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_1", "ROUTER-EXT-DATA-1", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_2 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_2", "ROUTER-EXT-DATA-2", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_3 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_3", "ROUTER-EXT-DATA-3", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_4 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_4", "ROUTER-EXT-DATA-4", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_5 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_5", "ROUTER-EXT-DATA-5", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_6 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_6", "ROUTER-EXT-DATA-6", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_7 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_7", "ROUTER-EXT-DATA-7", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_8 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_8", "ROUTER-EXT-DATA-8", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_9 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_9", "ROUTER-EXT-DATA-9", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_10 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_10", "ROUTER-EXT-DATA-10", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_11 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_11", "ROUTER-EXT-DATA-11", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_12 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_12", "ROUTER-EXT-DATA-12", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_13 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_13", "ROUTER-EXT-DATA-13", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_14 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_14", "ROUTER-EXT-DATA-14", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_15 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_15", "ROUTER-EXT-DATA-15", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_16 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_16", "ROUTER-EXT-DATA-16", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_17 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_17", "ROUTER-EXT-DATA-17", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_18 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_18", "ROUTER-EXT-DATA-18", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_19 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_19", "ROUTER-EXT-DATA-19", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_20 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_20", "ROUTER-EXT-DATA-20", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_21 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_21", "ROUTER-EXT-DATA-21", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_22 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_22", "ROUTER-EXT-DATA-22", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_23 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_23", "ROUTER-EXT-DATA-23", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_24 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_24", "ROUTER-EXT-DATA-24", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_25 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_25", "ROUTER-EXT-DATA-25", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_26 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_26", "ROUTER-EXT-DATA-26", FieldType.STRING, 
            250);
        router_Ex_File_Router_Ext_Data_27 = router_Ex_FileRedef9.newFieldInGroup("router_Ex_File_Router_Ext_Data_27", "ROUTER-EXT-DATA-27", FieldType.STRING, 
            120);

        this.setRecordName("LdaAppl1151");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAppl1151() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
