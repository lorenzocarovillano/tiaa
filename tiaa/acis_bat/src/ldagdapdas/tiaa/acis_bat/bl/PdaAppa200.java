/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:17:48 PM
**        * FROM NATURAL PDA     : APPA200
************************************************************
**        * FILE NAME            : PdaAppa200.java
**        * CLASS NAME           : PdaAppa200
**        * INSTANCE NAME        : PdaAppa200
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaAppa200 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Par_Parameter_Area_2;
    private DbsField pnd_Par_Parameter_Area_2_Pnd_Par_Hold_Length;
    private DbsField pnd_Par_Parameter_Area_2_Pnd_Par_Hold_Data;
    private DbsGroup pnd_Par_Parameter_Area_2_Pnd_Par_Hold_DataRedef1;
    private DbsField pnd_Par_Parameter_Area_2_Pnd_Par_Hold_1st_Part;
    private DbsField pnd_Par_Parameter_Area_2_Pnd_Par_Trailer_Data;

    public DbsGroup getPnd_Par_Parameter_Area_2() { return pnd_Par_Parameter_Area_2; }

    public DbsField getPnd_Par_Parameter_Area_2_Pnd_Par_Hold_Length() { return pnd_Par_Parameter_Area_2_Pnd_Par_Hold_Length; }

    public DbsField getPnd_Par_Parameter_Area_2_Pnd_Par_Hold_Data() { return pnd_Par_Parameter_Area_2_Pnd_Par_Hold_Data; }

    public DbsGroup getPnd_Par_Parameter_Area_2_Pnd_Par_Hold_DataRedef1() { return pnd_Par_Parameter_Area_2_Pnd_Par_Hold_DataRedef1; }

    public DbsField getPnd_Par_Parameter_Area_2_Pnd_Par_Hold_1st_Part() { return pnd_Par_Parameter_Area_2_Pnd_Par_Hold_1st_Part; }

    public DbsField getPnd_Par_Parameter_Area_2_Pnd_Par_Trailer_Data() { return pnd_Par_Parameter_Area_2_Pnd_Par_Trailer_Data; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Par_Parameter_Area_2 = dbsRecord.newGroupInRecord("pnd_Par_Parameter_Area_2", "#PAR-PARAMETER-AREA-2");
        pnd_Par_Parameter_Area_2.setParameterOption(ParameterOption.ByReference);
        pnd_Par_Parameter_Area_2_Pnd_Par_Hold_Length = pnd_Par_Parameter_Area_2.newFieldInGroup("pnd_Par_Parameter_Area_2_Pnd_Par_Hold_Length", "#PAR-HOLD-LENGTH", 
            FieldType.BINARY, 2);
        pnd_Par_Parameter_Area_2_Pnd_Par_Hold_Data = pnd_Par_Parameter_Area_2.newFieldInGroup("pnd_Par_Parameter_Area_2_Pnd_Par_Hold_Data", "#PAR-HOLD-DATA", 
            FieldType.STRING, 150);
        pnd_Par_Parameter_Area_2_Pnd_Par_Hold_DataRedef1 = pnd_Par_Parameter_Area_2.newGroupInGroup("pnd_Par_Parameter_Area_2_Pnd_Par_Hold_DataRedef1", 
            "Redefines", pnd_Par_Parameter_Area_2_Pnd_Par_Hold_Data);
        pnd_Par_Parameter_Area_2_Pnd_Par_Hold_1st_Part = pnd_Par_Parameter_Area_2_Pnd_Par_Hold_DataRedef1.newFieldInGroup("pnd_Par_Parameter_Area_2_Pnd_Par_Hold_1st_Part", 
            "#PAR-HOLD-1ST-PART", FieldType.STRING, 75);
        pnd_Par_Parameter_Area_2_Pnd_Par_Trailer_Data = pnd_Par_Parameter_Area_2_Pnd_Par_Hold_DataRedef1.newFieldInGroup("pnd_Par_Parameter_Area_2_Pnd_Par_Trailer_Data", 
            "#PAR-TRAILER-DATA", FieldType.STRING, 75);

        dbsRecord.reset();
    }

    // Constructors
    public PdaAppa200(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

