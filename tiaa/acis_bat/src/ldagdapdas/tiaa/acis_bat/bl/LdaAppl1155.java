/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:51 PM
**        * FROM NATURAL LDA     : APPL1155
************************************************************
**        * FILE NAME            : LdaAppl1155.java
**        * CLASS NAME           : LdaAppl1155
**        * INSTANCE NAME        : LdaAppl1155
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl1155 extends DbsRecord
{
    // Properties
    private DbsGroup acis_Alloc_Record;
    private DbsField acis_Alloc_Record_Acis_Alloc_Record_Id;
    private DbsGroup acis_Alloc_Record_Acis_Alloc_Record_IdRedef1;
    private DbsField acis_Alloc_Record_Acis_Eop_Record_Type;
    private DbsField acis_Alloc_Record_Acis_Eop_Output_Profile;
    private DbsField acis_Alloc_Record_Acis_Eop_Bundle_Type;
    private DbsField acis_Alloc_Record_Acis_Eop_Us_Soc_Sec;
    private DbsField acis_Alloc_Record_Acis_Eop_Tiaa_Cntr_Numb_1;
    private DbsField acis_Alloc_Record_Acis_Eop_Cref_Cert_Numb_1;
    private DbsField acis_Alloc_Record_Acis_Eop_Print_Package_Type;
    private DbsField acis_Alloc_Record_Acis_Eop_Product_Type;
    private DbsField acis_Alloc_Record_Acis_Eop_Asset_Cl_Name;
    private DbsField acis_Alloc_Record_Acis_Eop_Fund_Name;
    private DbsField acis_Alloc_Record_Acis_Eop_Alloc_Pct;
    private DbsField acis_Alloc_Record_Acis_Dialog_Sequence;

    public DbsGroup getAcis_Alloc_Record() { return acis_Alloc_Record; }

    public DbsField getAcis_Alloc_Record_Acis_Alloc_Record_Id() { return acis_Alloc_Record_Acis_Alloc_Record_Id; }

    public DbsGroup getAcis_Alloc_Record_Acis_Alloc_Record_IdRedef1() { return acis_Alloc_Record_Acis_Alloc_Record_IdRedef1; }

    public DbsField getAcis_Alloc_Record_Acis_Eop_Record_Type() { return acis_Alloc_Record_Acis_Eop_Record_Type; }

    public DbsField getAcis_Alloc_Record_Acis_Eop_Output_Profile() { return acis_Alloc_Record_Acis_Eop_Output_Profile; }

    public DbsField getAcis_Alloc_Record_Acis_Eop_Bundle_Type() { return acis_Alloc_Record_Acis_Eop_Bundle_Type; }

    public DbsField getAcis_Alloc_Record_Acis_Eop_Us_Soc_Sec() { return acis_Alloc_Record_Acis_Eop_Us_Soc_Sec; }

    public DbsField getAcis_Alloc_Record_Acis_Eop_Tiaa_Cntr_Numb_1() { return acis_Alloc_Record_Acis_Eop_Tiaa_Cntr_Numb_1; }

    public DbsField getAcis_Alloc_Record_Acis_Eop_Cref_Cert_Numb_1() { return acis_Alloc_Record_Acis_Eop_Cref_Cert_Numb_1; }

    public DbsField getAcis_Alloc_Record_Acis_Eop_Print_Package_Type() { return acis_Alloc_Record_Acis_Eop_Print_Package_Type; }

    public DbsField getAcis_Alloc_Record_Acis_Eop_Product_Type() { return acis_Alloc_Record_Acis_Eop_Product_Type; }

    public DbsField getAcis_Alloc_Record_Acis_Eop_Asset_Cl_Name() { return acis_Alloc_Record_Acis_Eop_Asset_Cl_Name; }

    public DbsField getAcis_Alloc_Record_Acis_Eop_Fund_Name() { return acis_Alloc_Record_Acis_Eop_Fund_Name; }

    public DbsField getAcis_Alloc_Record_Acis_Eop_Alloc_Pct() { return acis_Alloc_Record_Acis_Eop_Alloc_Pct; }

    public DbsField getAcis_Alloc_Record_Acis_Dialog_Sequence() { return acis_Alloc_Record_Acis_Dialog_Sequence; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        acis_Alloc_Record = newGroupInRecord("acis_Alloc_Record", "ACIS-ALLOC-RECORD");
        acis_Alloc_Record_Acis_Alloc_Record_Id = acis_Alloc_Record.newFieldInGroup("acis_Alloc_Record_Acis_Alloc_Record_Id", "ACIS-ALLOC-RECORD-ID", FieldType.STRING, 
            43);
        acis_Alloc_Record_Acis_Alloc_Record_IdRedef1 = acis_Alloc_Record.newGroupInGroup("acis_Alloc_Record_Acis_Alloc_Record_IdRedef1", "Redefines", 
            acis_Alloc_Record_Acis_Alloc_Record_Id);
        acis_Alloc_Record_Acis_Eop_Record_Type = acis_Alloc_Record_Acis_Alloc_Record_IdRedef1.newFieldInGroup("acis_Alloc_Record_Acis_Eop_Record_Type", 
            "ACIS-EOP-RECORD-TYPE", FieldType.STRING, 2);
        acis_Alloc_Record_Acis_Eop_Output_Profile = acis_Alloc_Record_Acis_Alloc_Record_IdRedef1.newFieldInGroup("acis_Alloc_Record_Acis_Eop_Output_Profile", 
            "ACIS-EOP-OUTPUT-PROFILE", FieldType.STRING, 2);
        acis_Alloc_Record_Acis_Eop_Bundle_Type = acis_Alloc_Record_Acis_Alloc_Record_IdRedef1.newFieldInGroup("acis_Alloc_Record_Acis_Eop_Bundle_Type", 
            "ACIS-EOP-BUNDLE-TYPE", FieldType.STRING, 1);
        acis_Alloc_Record_Acis_Eop_Us_Soc_Sec = acis_Alloc_Record_Acis_Alloc_Record_IdRedef1.newFieldInGroup("acis_Alloc_Record_Acis_Eop_Us_Soc_Sec", 
            "ACIS-EOP-US-SOC-SEC", FieldType.STRING, 11);
        acis_Alloc_Record_Acis_Eop_Tiaa_Cntr_Numb_1 = acis_Alloc_Record_Acis_Alloc_Record_IdRedef1.newFieldInGroup("acis_Alloc_Record_Acis_Eop_Tiaa_Cntr_Numb_1", 
            "ACIS-EOP-TIAA-CNTR-NUMB-1", FieldType.STRING, 10);
        acis_Alloc_Record_Acis_Eop_Cref_Cert_Numb_1 = acis_Alloc_Record_Acis_Alloc_Record_IdRedef1.newFieldInGroup("acis_Alloc_Record_Acis_Eop_Cref_Cert_Numb_1", 
            "ACIS-EOP-CREF-CERT-NUMB-1", FieldType.STRING, 10);
        acis_Alloc_Record_Acis_Eop_Print_Package_Type = acis_Alloc_Record_Acis_Alloc_Record_IdRedef1.newFieldInGroup("acis_Alloc_Record_Acis_Eop_Print_Package_Type", 
            "ACIS-EOP-PRINT-PACKAGE-TYPE", FieldType.STRING, 1);
        acis_Alloc_Record_Acis_Eop_Product_Type = acis_Alloc_Record_Acis_Alloc_Record_IdRedef1.newFieldInGroup("acis_Alloc_Record_Acis_Eop_Product_Type", 
            "ACIS-EOP-PRODUCT-TYPE", FieldType.STRING, 6);
        acis_Alloc_Record_Acis_Eop_Asset_Cl_Name = acis_Alloc_Record.newFieldInGroup("acis_Alloc_Record_Acis_Eop_Asset_Cl_Name", "ACIS-EOP-ASSET-CL-NAME", 
            FieldType.STRING, 60);
        acis_Alloc_Record_Acis_Eop_Fund_Name = acis_Alloc_Record.newFieldInGroup("acis_Alloc_Record_Acis_Eop_Fund_Name", "ACIS-EOP-FUND-NAME", FieldType.STRING, 
            60);
        acis_Alloc_Record_Acis_Eop_Alloc_Pct = acis_Alloc_Record.newFieldInGroup("acis_Alloc_Record_Acis_Eop_Alloc_Pct", "ACIS-EOP-ALLOC-PCT", FieldType.STRING, 
            4);
        acis_Alloc_Record_Acis_Dialog_Sequence = acis_Alloc_Record.newFieldInGroup("acis_Alloc_Record_Acis_Dialog_Sequence", "ACIS-DIALOG-SEQUENCE", FieldType.NUMERIC, 
            15);

        this.setRecordName("LdaAppl1155");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAppl1155() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
