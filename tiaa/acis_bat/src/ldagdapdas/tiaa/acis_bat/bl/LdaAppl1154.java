/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:51 PM
**        * FROM NATURAL LDA     : APPL1154
************************************************************
**        * FILE NAME            : LdaAppl1154.java
**        * CLASS NAME           : LdaAppl1154
**        * INSTANCE NAME        : LdaAppl1154
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl1154 extends DbsRecord
{
    // Properties
    private DbsGroup acis_Bene_Record;
    private DbsField acis_Bene_Record_Acis_Bene_Record_Id;
    private DbsGroup acis_Bene_Record_Acis_Bene_Record_IdRedef1;
    private DbsField acis_Bene_Record_Acis_Eop_Record_Type;
    private DbsField acis_Bene_Record_Acis_Eop_Output_Profile;
    private DbsField acis_Bene_Record_Acis_Eop_Bundle_Type;
    private DbsField acis_Bene_Record_Acis_Eop_Us_Soc_Sec;
    private DbsField acis_Bene_Record_Acis_Eop_Tiaa_Cntr_Numb_1;
    private DbsField acis_Bene_Record_Acis_Eop_Cref_Cert_Numb_1;
    private DbsField acis_Bene_Record_Acis_Eop_Print_Package_Type;
    private DbsField acis_Bene_Record_Acis_Eop_Product_Type;
    private DbsField acis_Bene_Record_Acis_Bene_Record_Detail;
    private DbsGroup acis_Bene_Record_Acis_Bene_Record_DetailRedef2;
    private DbsField acis_Bene_Record_Acis_Eop_Bene_Name;
    private DbsField acis_Bene_Record_Acis_Eop_Bene_Ssn;
    private DbsField acis_Bene_Record_Acis_Eop_Bene_Dob;
    private DbsField acis_Bene_Record_Acis_Eop_Bene_Relationship;
    private DbsGroup acis_Bene_Record_Acis_Bene_Record_DetailRedef3;
    private DbsField acis_Bene_Record_Acis_Eop_Bene_Spcl_Txt;
    private DbsGroup acis_Bene_Record_Acis_Bene_Record_DetailRedef4;
    private DbsField acis_Bene_Record_Acis_Eop_Bene_Spcl_Dsgn_Txt;
    private DbsField acis_Bene_Record_Acis_Dialog_Sequence;

    public DbsGroup getAcis_Bene_Record() { return acis_Bene_Record; }

    public DbsField getAcis_Bene_Record_Acis_Bene_Record_Id() { return acis_Bene_Record_Acis_Bene_Record_Id; }

    public DbsGroup getAcis_Bene_Record_Acis_Bene_Record_IdRedef1() { return acis_Bene_Record_Acis_Bene_Record_IdRedef1; }

    public DbsField getAcis_Bene_Record_Acis_Eop_Record_Type() { return acis_Bene_Record_Acis_Eop_Record_Type; }

    public DbsField getAcis_Bene_Record_Acis_Eop_Output_Profile() { return acis_Bene_Record_Acis_Eop_Output_Profile; }

    public DbsField getAcis_Bene_Record_Acis_Eop_Bundle_Type() { return acis_Bene_Record_Acis_Eop_Bundle_Type; }

    public DbsField getAcis_Bene_Record_Acis_Eop_Us_Soc_Sec() { return acis_Bene_Record_Acis_Eop_Us_Soc_Sec; }

    public DbsField getAcis_Bene_Record_Acis_Eop_Tiaa_Cntr_Numb_1() { return acis_Bene_Record_Acis_Eop_Tiaa_Cntr_Numb_1; }

    public DbsField getAcis_Bene_Record_Acis_Eop_Cref_Cert_Numb_1() { return acis_Bene_Record_Acis_Eop_Cref_Cert_Numb_1; }

    public DbsField getAcis_Bene_Record_Acis_Eop_Print_Package_Type() { return acis_Bene_Record_Acis_Eop_Print_Package_Type; }

    public DbsField getAcis_Bene_Record_Acis_Eop_Product_Type() { return acis_Bene_Record_Acis_Eop_Product_Type; }

    public DbsField getAcis_Bene_Record_Acis_Bene_Record_Detail() { return acis_Bene_Record_Acis_Bene_Record_Detail; }

    public DbsGroup getAcis_Bene_Record_Acis_Bene_Record_DetailRedef2() { return acis_Bene_Record_Acis_Bene_Record_DetailRedef2; }

    public DbsField getAcis_Bene_Record_Acis_Eop_Bene_Name() { return acis_Bene_Record_Acis_Eop_Bene_Name; }

    public DbsField getAcis_Bene_Record_Acis_Eop_Bene_Ssn() { return acis_Bene_Record_Acis_Eop_Bene_Ssn; }

    public DbsField getAcis_Bene_Record_Acis_Eop_Bene_Dob() { return acis_Bene_Record_Acis_Eop_Bene_Dob; }

    public DbsField getAcis_Bene_Record_Acis_Eop_Bene_Relationship() { return acis_Bene_Record_Acis_Eop_Bene_Relationship; }

    public DbsGroup getAcis_Bene_Record_Acis_Bene_Record_DetailRedef3() { return acis_Bene_Record_Acis_Bene_Record_DetailRedef3; }

    public DbsField getAcis_Bene_Record_Acis_Eop_Bene_Spcl_Txt() { return acis_Bene_Record_Acis_Eop_Bene_Spcl_Txt; }

    public DbsGroup getAcis_Bene_Record_Acis_Bene_Record_DetailRedef4() { return acis_Bene_Record_Acis_Bene_Record_DetailRedef4; }

    public DbsField getAcis_Bene_Record_Acis_Eop_Bene_Spcl_Dsgn_Txt() { return acis_Bene_Record_Acis_Eop_Bene_Spcl_Dsgn_Txt; }

    public DbsField getAcis_Bene_Record_Acis_Dialog_Sequence() { return acis_Bene_Record_Acis_Dialog_Sequence; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        acis_Bene_Record = newGroupInRecord("acis_Bene_Record", "ACIS-BENE-RECORD");
        acis_Bene_Record_Acis_Bene_Record_Id = acis_Bene_Record.newFieldInGroup("acis_Bene_Record_Acis_Bene_Record_Id", "ACIS-BENE-RECORD-ID", FieldType.STRING, 
            43);
        acis_Bene_Record_Acis_Bene_Record_IdRedef1 = acis_Bene_Record.newGroupInGroup("acis_Bene_Record_Acis_Bene_Record_IdRedef1", "Redefines", acis_Bene_Record_Acis_Bene_Record_Id);
        acis_Bene_Record_Acis_Eop_Record_Type = acis_Bene_Record_Acis_Bene_Record_IdRedef1.newFieldInGroup("acis_Bene_Record_Acis_Eop_Record_Type", "ACIS-EOP-RECORD-TYPE", 
            FieldType.STRING, 2);
        acis_Bene_Record_Acis_Eop_Output_Profile = acis_Bene_Record_Acis_Bene_Record_IdRedef1.newFieldInGroup("acis_Bene_Record_Acis_Eop_Output_Profile", 
            "ACIS-EOP-OUTPUT-PROFILE", FieldType.STRING, 2);
        acis_Bene_Record_Acis_Eop_Bundle_Type = acis_Bene_Record_Acis_Bene_Record_IdRedef1.newFieldInGroup("acis_Bene_Record_Acis_Eop_Bundle_Type", "ACIS-EOP-BUNDLE-TYPE", 
            FieldType.STRING, 1);
        acis_Bene_Record_Acis_Eop_Us_Soc_Sec = acis_Bene_Record_Acis_Bene_Record_IdRedef1.newFieldInGroup("acis_Bene_Record_Acis_Eop_Us_Soc_Sec", "ACIS-EOP-US-SOC-SEC", 
            FieldType.STRING, 11);
        acis_Bene_Record_Acis_Eop_Tiaa_Cntr_Numb_1 = acis_Bene_Record_Acis_Bene_Record_IdRedef1.newFieldInGroup("acis_Bene_Record_Acis_Eop_Tiaa_Cntr_Numb_1", 
            "ACIS-EOP-TIAA-CNTR-NUMB-1", FieldType.STRING, 10);
        acis_Bene_Record_Acis_Eop_Cref_Cert_Numb_1 = acis_Bene_Record_Acis_Bene_Record_IdRedef1.newFieldInGroup("acis_Bene_Record_Acis_Eop_Cref_Cert_Numb_1", 
            "ACIS-EOP-CREF-CERT-NUMB-1", FieldType.STRING, 10);
        acis_Bene_Record_Acis_Eop_Print_Package_Type = acis_Bene_Record_Acis_Bene_Record_IdRedef1.newFieldInGroup("acis_Bene_Record_Acis_Eop_Print_Package_Type", 
            "ACIS-EOP-PRINT-PACKAGE-TYPE", FieldType.STRING, 1);
        acis_Bene_Record_Acis_Eop_Product_Type = acis_Bene_Record_Acis_Bene_Record_IdRedef1.newFieldInGroup("acis_Bene_Record_Acis_Eop_Product_Type", 
            "ACIS-EOP-PRODUCT-TYPE", FieldType.STRING, 6);
        acis_Bene_Record_Acis_Bene_Record_Detail = acis_Bene_Record.newFieldInGroup("acis_Bene_Record_Acis_Bene_Record_Detail", "ACIS-BENE-RECORD-DETAIL", 
            FieldType.STRING, 102);
        acis_Bene_Record_Acis_Bene_Record_DetailRedef2 = acis_Bene_Record.newGroupInGroup("acis_Bene_Record_Acis_Bene_Record_DetailRedef2", "Redefines", 
            acis_Bene_Record_Acis_Bene_Record_Detail);
        acis_Bene_Record_Acis_Eop_Bene_Name = acis_Bene_Record_Acis_Bene_Record_DetailRedef2.newFieldInGroup("acis_Bene_Record_Acis_Eop_Bene_Name", "ACIS-EOP-BENE-NAME", 
            FieldType.STRING, 70);
        acis_Bene_Record_Acis_Eop_Bene_Ssn = acis_Bene_Record_Acis_Bene_Record_DetailRedef2.newFieldInGroup("acis_Bene_Record_Acis_Eop_Bene_Ssn", "ACIS-EOP-BENE-SSN", 
            FieldType.NUMERIC, 9);
        acis_Bene_Record_Acis_Eop_Bene_Dob = acis_Bene_Record_Acis_Bene_Record_DetailRedef2.newFieldInGroup("acis_Bene_Record_Acis_Eop_Bene_Dob", "ACIS-EOP-BENE-DOB", 
            FieldType.NUMERIC, 8);
        acis_Bene_Record_Acis_Eop_Bene_Relationship = acis_Bene_Record_Acis_Bene_Record_DetailRedef2.newFieldInGroup("acis_Bene_Record_Acis_Eop_Bene_Relationship", 
            "ACIS-EOP-BENE-RELATIONSHIP", FieldType.STRING, 15);
        acis_Bene_Record_Acis_Bene_Record_DetailRedef3 = acis_Bene_Record.newGroupInGroup("acis_Bene_Record_Acis_Bene_Record_DetailRedef3", "Redefines", 
            acis_Bene_Record_Acis_Bene_Record_Detail);
        acis_Bene_Record_Acis_Eop_Bene_Spcl_Txt = acis_Bene_Record_Acis_Bene_Record_DetailRedef3.newFieldInGroup("acis_Bene_Record_Acis_Eop_Bene_Spcl_Txt", 
            "ACIS-EOP-BENE-SPCL-TXT", FieldType.STRING, 72);
        acis_Bene_Record_Acis_Bene_Record_DetailRedef4 = acis_Bene_Record.newGroupInGroup("acis_Bene_Record_Acis_Bene_Record_DetailRedef4", "Redefines", 
            acis_Bene_Record_Acis_Bene_Record_Detail);
        acis_Bene_Record_Acis_Eop_Bene_Spcl_Dsgn_Txt = acis_Bene_Record_Acis_Bene_Record_DetailRedef4.newFieldInGroup("acis_Bene_Record_Acis_Eop_Bene_Spcl_Dsgn_Txt", 
            "ACIS-EOP-BENE-SPCL-DSGN-TXT", FieldType.STRING, 72);
        acis_Bene_Record_Acis_Dialog_Sequence = acis_Bene_Record.newFieldInGroup("acis_Bene_Record_Acis_Dialog_Sequence", "ACIS-DIALOG-SEQUENCE", FieldType.NUMERIC, 
            15);

        this.setRecordName("LdaAppl1154");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAppl1154() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
