/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:51:03 PM
**        * FROM NATURAL LDA     : APPL231
************************************************************
**        * FILE NAME            : LdaAppl231.java
**        * CLASS NAME           : LdaAppl231
**        * INSTANCE NAME        : LdaAppl231
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl231 extends DbsRecord
{
    // Properties
    private DbsField pnd_Report_Ctl_Card;
    private DbsGroup pnd_Report_Ctl_CardRedef1;
    private DbsField pnd_Report_Ctl_Card_Cc_Prgm_Id;
    private DbsField pnd_Report_Ctl_Card_Crpt_Proc_Date;
    private DbsGroup pnd_Report_Ctl_Card_Crpt_Proc_DateRedef2;
    private DbsField pnd_Report_Ctl_Card_Crpt_Proc_Dt_Mm;
    private DbsField pnd_Report_Ctl_Card_Pnd_Filler1;
    private DbsField pnd_Report_Ctl_Card_Pnd_Rpt_Proc_Dt_Dd;
    private DbsField pnd_Report_Ctl_Card_Pnd_Filler2;
    private DbsField pnd_Report_Ctl_Card_Pnd_Rpt_Proc_Dt_Yy;
    private DbsField pnd_Ctr;
    private DbsField pnd_Ctr_D;
    private DbsField pnd_Ctr_S;
    private DbsField pnd_Ctl_Reg;
    private DbsGroup pnd_Ctl_RegRedef3;
    private DbsField pnd_Ctl_Reg_Pnd_Clob;
    private DbsField pnd_Ctl_Reg_Pnd_Creg;
    private DbsField pnd_Ctl_Reg_Pnd_Cregn;
    private DbsField pnd_Prev_Ctlr;
    private DbsGroup pnd_Prev_CtlrRedef4;
    private DbsField pnd_Prev_Ctlr_Pnd_Prv_Reg;
    private DbsField pnd_Prev_Ctlr_Pnd_Prv_Regnd;
    private DbsField pnd_Prv_Lob;
    private DbsField pnd_Reg_Total_1r;
    private DbsField pnd_Reg_Total_1s;
    private DbsField pnd_Reg_Total_2r;
    private DbsField pnd_Reg_Total_2s;
    private DbsField pnd_Reg_Total_T1;
    private DbsField pnd_Reg_Total_T2;
    private DbsField pnd_Ed_Cont_T;
    private DbsField pnd_Ed_Cont_C;
    private DbsField pnd_Ed_Name;
    private DbsField pnd_Ed_Ssn;
    private DbsField pnd_Ed_Dob;
    private DbsField pnd_Ed_Tap;
    private DbsField pnd_Ed_Cap;
    private DbsField pnd_Ed_Doi;
    private DbsField pnd_Ed_Dap;
    private DbsField pnd_Ed_Ppgpn;
    private DbsField pnd_Lname;
    private DbsGroup pnd_LnameRedef5;
    private DbsField pnd_Lname_Pnd_Namel;
    private DbsField pnd_Fname;
    private DbsGroup pnd_FnameRedef6;
    private DbsField pnd_Fname_Pnd_Namef;
    private DbsField pnd_Mname;
    private DbsGroup pnd_MnameRedef7;
    private DbsField pnd_Mname_Pnd_Namem;
    private DbsField pnd_Date;
    private DbsField pnd_One_Day;
    private DbsField pnd_Datea;
    private DbsGroup pnd_DateaRedef8;
    private DbsField pnd_Datea_Pnd_Mm;
    private DbsField pnd_Month_Table;
    private DbsField pnd_Mth_Idx;
    private DbsField pnd_Hd_Need;
    private DbsField pnd_Hd_Lit;

    public DbsField getPnd_Report_Ctl_Card() { return pnd_Report_Ctl_Card; }

    public DbsGroup getPnd_Report_Ctl_CardRedef1() { return pnd_Report_Ctl_CardRedef1; }

    public DbsField getPnd_Report_Ctl_Card_Cc_Prgm_Id() { return pnd_Report_Ctl_Card_Cc_Prgm_Id; }

    public DbsField getPnd_Report_Ctl_Card_Crpt_Proc_Date() { return pnd_Report_Ctl_Card_Crpt_Proc_Date; }

    public DbsGroup getPnd_Report_Ctl_Card_Crpt_Proc_DateRedef2() { return pnd_Report_Ctl_Card_Crpt_Proc_DateRedef2; }

    public DbsField getPnd_Report_Ctl_Card_Crpt_Proc_Dt_Mm() { return pnd_Report_Ctl_Card_Crpt_Proc_Dt_Mm; }

    public DbsField getPnd_Report_Ctl_Card_Pnd_Filler1() { return pnd_Report_Ctl_Card_Pnd_Filler1; }

    public DbsField getPnd_Report_Ctl_Card_Pnd_Rpt_Proc_Dt_Dd() { return pnd_Report_Ctl_Card_Pnd_Rpt_Proc_Dt_Dd; }

    public DbsField getPnd_Report_Ctl_Card_Pnd_Filler2() { return pnd_Report_Ctl_Card_Pnd_Filler2; }

    public DbsField getPnd_Report_Ctl_Card_Pnd_Rpt_Proc_Dt_Yy() { return pnd_Report_Ctl_Card_Pnd_Rpt_Proc_Dt_Yy; }

    public DbsField getPnd_Ctr() { return pnd_Ctr; }

    public DbsField getPnd_Ctr_D() { return pnd_Ctr_D; }

    public DbsField getPnd_Ctr_S() { return pnd_Ctr_S; }

    public DbsField getPnd_Ctl_Reg() { return pnd_Ctl_Reg; }

    public DbsGroup getPnd_Ctl_RegRedef3() { return pnd_Ctl_RegRedef3; }

    public DbsField getPnd_Ctl_Reg_Pnd_Clob() { return pnd_Ctl_Reg_Pnd_Clob; }

    public DbsField getPnd_Ctl_Reg_Pnd_Creg() { return pnd_Ctl_Reg_Pnd_Creg; }

    public DbsField getPnd_Ctl_Reg_Pnd_Cregn() { return pnd_Ctl_Reg_Pnd_Cregn; }

    public DbsField getPnd_Prev_Ctlr() { return pnd_Prev_Ctlr; }

    public DbsGroup getPnd_Prev_CtlrRedef4() { return pnd_Prev_CtlrRedef4; }

    public DbsField getPnd_Prev_Ctlr_Pnd_Prv_Reg() { return pnd_Prev_Ctlr_Pnd_Prv_Reg; }

    public DbsField getPnd_Prev_Ctlr_Pnd_Prv_Regnd() { return pnd_Prev_Ctlr_Pnd_Prv_Regnd; }

    public DbsField getPnd_Prv_Lob() { return pnd_Prv_Lob; }

    public DbsField getPnd_Reg_Total_1r() { return pnd_Reg_Total_1r; }

    public DbsField getPnd_Reg_Total_1s() { return pnd_Reg_Total_1s; }

    public DbsField getPnd_Reg_Total_2r() { return pnd_Reg_Total_2r; }

    public DbsField getPnd_Reg_Total_2s() { return pnd_Reg_Total_2s; }

    public DbsField getPnd_Reg_Total_T1() { return pnd_Reg_Total_T1; }

    public DbsField getPnd_Reg_Total_T2() { return pnd_Reg_Total_T2; }

    public DbsField getPnd_Ed_Cont_T() { return pnd_Ed_Cont_T; }

    public DbsField getPnd_Ed_Cont_C() { return pnd_Ed_Cont_C; }

    public DbsField getPnd_Ed_Name() { return pnd_Ed_Name; }

    public DbsField getPnd_Ed_Ssn() { return pnd_Ed_Ssn; }

    public DbsField getPnd_Ed_Dob() { return pnd_Ed_Dob; }

    public DbsField getPnd_Ed_Tap() { return pnd_Ed_Tap; }

    public DbsField getPnd_Ed_Cap() { return pnd_Ed_Cap; }

    public DbsField getPnd_Ed_Doi() { return pnd_Ed_Doi; }

    public DbsField getPnd_Ed_Dap() { return pnd_Ed_Dap; }

    public DbsField getPnd_Ed_Ppgpn() { return pnd_Ed_Ppgpn; }

    public DbsField getPnd_Lname() { return pnd_Lname; }

    public DbsGroup getPnd_LnameRedef5() { return pnd_LnameRedef5; }

    public DbsField getPnd_Lname_Pnd_Namel() { return pnd_Lname_Pnd_Namel; }

    public DbsField getPnd_Fname() { return pnd_Fname; }

    public DbsGroup getPnd_FnameRedef6() { return pnd_FnameRedef6; }

    public DbsField getPnd_Fname_Pnd_Namef() { return pnd_Fname_Pnd_Namef; }

    public DbsField getPnd_Mname() { return pnd_Mname; }

    public DbsGroup getPnd_MnameRedef7() { return pnd_MnameRedef7; }

    public DbsField getPnd_Mname_Pnd_Namem() { return pnd_Mname_Pnd_Namem; }

    public DbsField getPnd_Date() { return pnd_Date; }

    public DbsField getPnd_One_Day() { return pnd_One_Day; }

    public DbsField getPnd_Datea() { return pnd_Datea; }

    public DbsGroup getPnd_DateaRedef8() { return pnd_DateaRedef8; }

    public DbsField getPnd_Datea_Pnd_Mm() { return pnd_Datea_Pnd_Mm; }

    public DbsField getPnd_Month_Table() { return pnd_Month_Table; }

    public DbsField getPnd_Mth_Idx() { return pnd_Mth_Idx; }

    public DbsField getPnd_Hd_Need() { return pnd_Hd_Need; }

    public DbsField getPnd_Hd_Lit() { return pnd_Hd_Lit; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Report_Ctl_Card = newFieldInRecord("pnd_Report_Ctl_Card", "#REPORT-CTL-CARD", FieldType.STRING, 18);
        pnd_Report_Ctl_CardRedef1 = newGroupInRecord("pnd_Report_Ctl_CardRedef1", "Redefines", pnd_Report_Ctl_Card);
        pnd_Report_Ctl_Card_Cc_Prgm_Id = pnd_Report_Ctl_CardRedef1.newFieldInGroup("pnd_Report_Ctl_Card_Cc_Prgm_Id", "CC-PRGM-ID", FieldType.STRING, 10);
        pnd_Report_Ctl_Card_Crpt_Proc_Date = pnd_Report_Ctl_CardRedef1.newFieldInGroup("pnd_Report_Ctl_Card_Crpt_Proc_Date", "CRPT-PROC-DATE", FieldType.STRING, 
            8);
        pnd_Report_Ctl_Card_Crpt_Proc_DateRedef2 = pnd_Report_Ctl_CardRedef1.newGroupInGroup("pnd_Report_Ctl_Card_Crpt_Proc_DateRedef2", "Redefines", 
            pnd_Report_Ctl_Card_Crpt_Proc_Date);
        pnd_Report_Ctl_Card_Crpt_Proc_Dt_Mm = pnd_Report_Ctl_Card_Crpt_Proc_DateRedef2.newFieldInGroup("pnd_Report_Ctl_Card_Crpt_Proc_Dt_Mm", "CRPT-PROC-DT-MM", 
            FieldType.NUMERIC, 2);
        pnd_Report_Ctl_Card_Pnd_Filler1 = pnd_Report_Ctl_Card_Crpt_Proc_DateRedef2.newFieldInGroup("pnd_Report_Ctl_Card_Pnd_Filler1", "#FILLER1", FieldType.STRING, 
            1);
        pnd_Report_Ctl_Card_Pnd_Rpt_Proc_Dt_Dd = pnd_Report_Ctl_Card_Crpt_Proc_DateRedef2.newFieldInGroup("pnd_Report_Ctl_Card_Pnd_Rpt_Proc_Dt_Dd", "#RPT-PROC-DT-DD", 
            FieldType.NUMERIC, 2);
        pnd_Report_Ctl_Card_Pnd_Filler2 = pnd_Report_Ctl_Card_Crpt_Proc_DateRedef2.newFieldInGroup("pnd_Report_Ctl_Card_Pnd_Filler2", "#FILLER2", FieldType.STRING, 
            1);
        pnd_Report_Ctl_Card_Pnd_Rpt_Proc_Dt_Yy = pnd_Report_Ctl_Card_Crpt_Proc_DateRedef2.newFieldInGroup("pnd_Report_Ctl_Card_Pnd_Rpt_Proc_Dt_Yy", "#RPT-PROC-DT-YY", 
            FieldType.NUMERIC, 2);

        pnd_Ctr = newFieldInRecord("pnd_Ctr", "#CTR", FieldType.NUMERIC, 2);

        pnd_Ctr_D = newFieldInRecord("pnd_Ctr_D", "#CTR-D", FieldType.NUMERIC, 7);

        pnd_Ctr_S = newFieldInRecord("pnd_Ctr_S", "#CTR-S", FieldType.NUMERIC, 7);

        pnd_Ctl_Reg = newFieldInRecord("pnd_Ctl_Reg", "#CTL-REG", FieldType.STRING, 3);
        pnd_Ctl_RegRedef3 = newGroupInRecord("pnd_Ctl_RegRedef3", "Redefines", pnd_Ctl_Reg);
        pnd_Ctl_Reg_Pnd_Clob = pnd_Ctl_RegRedef3.newFieldInGroup("pnd_Ctl_Reg_Pnd_Clob", "#CLOB", FieldType.STRING, 1);
        pnd_Ctl_Reg_Pnd_Creg = pnd_Ctl_RegRedef3.newFieldInGroup("pnd_Ctl_Reg_Pnd_Creg", "#CREG", FieldType.STRING, 1);
        pnd_Ctl_Reg_Pnd_Cregn = pnd_Ctl_RegRedef3.newFieldInGroup("pnd_Ctl_Reg_Pnd_Cregn", "#CREGN", FieldType.STRING, 1);

        pnd_Prev_Ctlr = newFieldInRecord("pnd_Prev_Ctlr", "#PREV-CTLR", FieldType.STRING, 2);
        pnd_Prev_CtlrRedef4 = newGroupInRecord("pnd_Prev_CtlrRedef4", "Redefines", pnd_Prev_Ctlr);
        pnd_Prev_Ctlr_Pnd_Prv_Reg = pnd_Prev_CtlrRedef4.newFieldInGroup("pnd_Prev_Ctlr_Pnd_Prv_Reg", "#PRV-REG", FieldType.STRING, 1);
        pnd_Prev_Ctlr_Pnd_Prv_Regnd = pnd_Prev_CtlrRedef4.newFieldInGroup("pnd_Prev_Ctlr_Pnd_Prv_Regnd", "#PRV-REGND", FieldType.STRING, 1);

        pnd_Prv_Lob = newFieldInRecord("pnd_Prv_Lob", "#PRV-LOB", FieldType.STRING, 1);

        pnd_Reg_Total_1r = newFieldInRecord("pnd_Reg_Total_1r", "#REG-TOTAL-1R", FieldType.NUMERIC, 7);

        pnd_Reg_Total_1s = newFieldInRecord("pnd_Reg_Total_1s", "#REG-TOTAL-1S", FieldType.NUMERIC, 7);

        pnd_Reg_Total_2r = newFieldInRecord("pnd_Reg_Total_2r", "#REG-TOTAL-2R", FieldType.NUMERIC, 7);

        pnd_Reg_Total_2s = newFieldInRecord("pnd_Reg_Total_2s", "#REG-TOTAL-2S", FieldType.NUMERIC, 7);

        pnd_Reg_Total_T1 = newFieldInRecord("pnd_Reg_Total_T1", "#REG-TOTAL-T1", FieldType.NUMERIC, 7);

        pnd_Reg_Total_T2 = newFieldInRecord("pnd_Reg_Total_T2", "#REG-TOTAL-T2", FieldType.NUMERIC, 7);

        pnd_Ed_Cont_T = newFieldInRecord("pnd_Ed_Cont_T", "#ED-CONT-T", FieldType.STRING, 10);

        pnd_Ed_Cont_C = newFieldInRecord("pnd_Ed_Cont_C", "#ED-CONT-C", FieldType.STRING, 10);

        pnd_Ed_Name = newFieldInRecord("pnd_Ed_Name", "#ED-NAME", FieldType.STRING, 11);

        pnd_Ed_Ssn = newFieldInRecord("pnd_Ed_Ssn", "#ED-SSN", FieldType.STRING, 11);

        pnd_Ed_Dob = newFieldInRecord("pnd_Ed_Dob", "#ED-DOB", FieldType.STRING, 8);

        pnd_Ed_Tap = newFieldInRecord("pnd_Ed_Tap", "#ED-TAP", FieldType.STRING, 5);

        pnd_Ed_Cap = newFieldInRecord("pnd_Ed_Cap", "#ED-CAP", FieldType.STRING, 5);

        pnd_Ed_Doi = newFieldInRecord("pnd_Ed_Doi", "#ED-DOI", FieldType.STRING, 5);

        pnd_Ed_Dap = newFieldInRecord("pnd_Ed_Dap", "#ED-DAP", FieldType.STRING, 8);

        pnd_Ed_Ppgpn = newFieldInRecord("pnd_Ed_Ppgpn", "#ED-PPGPN", FieldType.STRING, 6);

        pnd_Lname = newFieldInRecord("pnd_Lname", "#LNAME", FieldType.STRING, 38);
        pnd_LnameRedef5 = newGroupInRecord("pnd_LnameRedef5", "Redefines", pnd_Lname);
        pnd_Lname_Pnd_Namel = pnd_LnameRedef5.newFieldInGroup("pnd_Lname_Pnd_Namel", "#NAMEL", FieldType.STRING, 7);

        pnd_Fname = newFieldInRecord("pnd_Fname", "#FNAME", FieldType.STRING, 38);
        pnd_FnameRedef6 = newGroupInRecord("pnd_FnameRedef6", "Redefines", pnd_Fname);
        pnd_Fname_Pnd_Namef = pnd_FnameRedef6.newFieldInGroup("pnd_Fname_Pnd_Namef", "#NAMEF", FieldType.STRING, 1);

        pnd_Mname = newFieldInRecord("pnd_Mname", "#MNAME", FieldType.STRING, 38);
        pnd_MnameRedef7 = newGroupInRecord("pnd_MnameRedef7", "Redefines", pnd_Mname);
        pnd_Mname_Pnd_Namem = pnd_MnameRedef7.newFieldInGroup("pnd_Mname_Pnd_Namem", "#NAMEM", FieldType.STRING, 1);

        pnd_Date = newFieldInRecord("pnd_Date", "#DATE", FieldType.DATE);

        pnd_One_Day = newFieldInRecord("pnd_One_Day", "#ONE-DAY", FieldType.DATE);

        pnd_Datea = newFieldInRecord("pnd_Datea", "#DATEA", FieldType.STRING, 9);
        pnd_DateaRedef8 = newGroupInRecord("pnd_DateaRedef8", "Redefines", pnd_Datea);
        pnd_Datea_Pnd_Mm = pnd_DateaRedef8.newFieldInGroup("pnd_Datea_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);

        pnd_Month_Table = newFieldArrayInRecord("pnd_Month_Table", "#MONTH-TABLE", FieldType.STRING, 9, new DbsArrayController(1,12));

        pnd_Mth_Idx = newFieldInRecord("pnd_Mth_Idx", "#MTH-IDX", FieldType.PACKED_DECIMAL, 2);

        pnd_Hd_Need = newFieldInRecord("pnd_Hd_Need", "#HD-NEED", FieldType.STRING, 7);

        pnd_Hd_Lit = newFieldInRecord("pnd_Hd_Lit", "#HD-LIT", FieldType.STRING, 3);

        this.setRecordName("LdaAppl231");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Month_Table.getValue(1).setInitialValue("JANUARY");
        pnd_Month_Table.getValue(2).setInitialValue("FEBRUARY");
        pnd_Month_Table.getValue(3).setInitialValue("MARCH");
        pnd_Month_Table.getValue(4).setInitialValue("APRIL");
        pnd_Month_Table.getValue(5).setInitialValue("MAY");
        pnd_Month_Table.getValue(6).setInitialValue("JUNE");
        pnd_Month_Table.getValue(7).setInitialValue("JULY");
        pnd_Month_Table.getValue(8).setInitialValue("AUGUST");
        pnd_Month_Table.getValue(9).setInitialValue("SEPTEMBER");
        pnd_Month_Table.getValue(10).setInitialValue("OCTOBER");
        pnd_Month_Table.getValue(11).setInitialValue("NOVEMBER");
        pnd_Month_Table.getValue(12).setInitialValue("DECEMBER");
    }

    // Constructor
    public LdaAppl231() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
