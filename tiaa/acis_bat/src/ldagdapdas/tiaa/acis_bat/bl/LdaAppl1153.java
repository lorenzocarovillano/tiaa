/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:51 PM
**        * FROM NATURAL LDA     : APPL1153
************************************************************
**        * FILE NAME            : LdaAppl1153.java
**        * CLASS NAME           : LdaAppl1153
**        * INSTANCE NAME        : LdaAppl1153
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl1153 extends DbsRecord
{
    // Properties
    private DbsGroup acis_Driver_Record;
    private DbsField acis_Driver_Record_Acis_Eop_Record_Id;
    private DbsGroup acis_Driver_Record_Acis_Eop_Record_IdRedef1;
    private DbsField acis_Driver_Record_Acis_Eop_Record_Type;
    private DbsField acis_Driver_Record_Acis_Eop_Output_Profile;
    private DbsField acis_Driver_Record_Acis_Eop_Bundle_Type;
    private DbsField acis_Driver_Record_Acis_Eop_Us_Soc_Sec;
    private DbsField acis_Driver_Record_Acis_Eop_Tiaa_Cntr_Numb_1;
    private DbsField acis_Driver_Record_Acis_Eop_Cref_Cert_Numb_1;
    private DbsField acis_Driver_Record_Acis_Eop_Print_Package_Type;
    private DbsField acis_Driver_Record_Acis_Eop_Product_Type;
    private DbsField acis_Driver_Record_Acis_Eop_Reprint_Type;
    private DbsField acis_Driver_Record_Acis_Eop_Tiaa_Cref_Both;
    private DbsField acis_Driver_Record_Acis_Eop_Sg_Plan_No;
    private DbsField acis_Driver_Record_Acis_Eop_Tiaa_Issue_Date;
    private DbsField acis_Driver_Record_Acis_Eop_Cref_Issue_Date;
    private DbsField acis_Driver_Record_Acis_Eop_Original_Issue_State;
    private DbsField acis_Driver_Record_Acis_Eop_Current_Issue_State;
    private DbsField acis_Driver_Record_Acis_Eop_Tiaa_Cntr_Numb_2;
    private DbsField acis_Driver_Record_Acis_Eop_Cref_Cert_Numb_2;
    private DbsField acis_Driver_Record_Acis_Eop_Cref_Cert_Numb_4;
    private DbsField acis_Driver_Record_Acis_Eop_Tiaa_Annuity_Start_Date;
    private DbsField acis_Driver_Record_Acis_Eop_Cref_Annuity_Start_Date;
    private DbsField acis_Driver_Record_Acis_Eop_Dob;
    private DbsField acis_Driver_Record_Acis_Eop_Dt_App_Recvd;
    private DbsField acis_Driver_Record_Acis_Eop_Estate_As_Bene;
    private DbsField acis_Driver_Record_Acis_Eop_Trust_As_Bene;
    private DbsField acis_Driver_Record_Acis_Eop_Beneficiary_Category;
    private DbsField acis_Driver_Record_Acis_Eop_Cref_Request;
    private DbsField acis_Driver_Record_Acis_Eop_Name_Prefix_Lg;
    private DbsField acis_Driver_Record_Acis_Eop_Name_Last_Lg;
    private DbsField acis_Driver_Record_Acis_Eop_Name_First_Lg;
    private DbsField acis_Driver_Record_Acis_Eop_Name_Middle_Lg;
    private DbsField acis_Driver_Record_Acis_Eop_Name_Suffix_Lg;
    private DbsField acis_Driver_Record_Acis_Eop_Pin_Numb;
    private DbsField acis_Driver_Record_Acis_Eop_Each_Name_Line;
    private DbsField acis_Driver_Record_Acis_Eop_Address_Type_Code;
    private DbsField acis_Driver_Record_Acis_Eop_Post_Zip;
    private DbsField acis_Driver_Record_Acis_Eop_Inst_Name;
    private DbsField acis_Driver_Record_Acis_Eop_Each_Ia_Line;
    private DbsField acis_Driver_Record_Acis_Eop_Inst_Zip;
    private DbsField acis_Driver_Record_Acis_Eop_Lob;
    private DbsField acis_Driver_Record_Acis_Eop_Lob_Type;
    private DbsField acis_Driver_Record_Acis_Eop_Name_State;
    private DbsField acis_Driver_Record_Acis_Eop_Institution_Code;
    private DbsField acis_Driver_Record_Acis_Eop_Rollover_Amt;
    private DbsField acis_Driver_Record_Acis_Eop_Mail_Date;
    private DbsField acis_Driver_Record_Acis_Eop_Unit_Name;
    private DbsField acis_Driver_Record_Acis_Eop_Email_Address;
    private DbsField acis_Driver_Record_Acis_Eop_Acct_Sum_Type;
    private DbsField acis_Driver_Record_Acis_Eop_Effective_Date;
    private DbsField acis_Driver_Record_Acis_Eop_Group_Contract_Name;
    private DbsField acis_Driver_Record_Acis_Eop_Employer_Name;
    private DbsField acis_Driver_Record_Acis_Eop_Access_Code;
    private DbsField acis_Driver_Record_Acis_Eop_Ownership_Code;
    private DbsField acis_Driver_Record_Acis_Eop_Alloc_Descrepancy;
    private DbsField acis_Driver_Record_Acis_Eop_Participant_Status;
    private DbsField acis_Driver_Record_Acis_Eop_Portfolio_Selected;
    private DbsField acis_Driver_Record_Acis_Eop_Divorce_Contract;
    private DbsField acis_Driver_Record_Acis_Eop_Access_Cd_Ind;
    private DbsField acis_Driver_Record_Acis_Eop_Mailing_Instructions;
    private DbsField acis_Driver_Record_Acis_Eop_Application_Source;
    private DbsField acis_Driver_Record_Acis_Eop_Gsra_Loan_Ind;
    private DbsField acis_Driver_Record_Acis_Eop_Spec_Ppg_Ind;
    private DbsField acis_Driver_Record_Acis_Eop_Mult_App_Version;
    private DbsField acis_Driver_Record_Acis_Eop_Mult_App_Status;
    private DbsField acis_Driver_Record_Acis_Eop_Mult_App_Lob;
    private DbsField acis_Driver_Record_Acis_Eop_Mult_App_Lob_Type;
    private DbsField acis_Driver_Record_Acis_Eop_Ira_Record_Type;
    private DbsField acis_Driver_Record_Acis_Eop_Enroll_Req_Type;
    private DbsField acis_Driver_Record_Acis_Eop_Erisa_Inst;
    private DbsField acis_Driver_Record_Acis_Eop_Each_Correction_Type;
    private DbsField acis_Driver_Record_Acis_Eop_Oia_Ind;
    private DbsField acis_Driver_Record_Acis_Eop_Sg_Subplan_No;
    private DbsField acis_Driver_Record_Acis_Eop_Sg_Subplan_Type;
    private DbsField acis_Driver_Record_Acis_Eop_Omni_Issue;
    private DbsField acis_Driver_Record_Acis_Eop_Single_Issue_Ind;
    private DbsField acis_Driver_Record_Acis_Eop_Spec_Fund_Ind;
    private DbsField acis_Driver_Record_Acis_Eop_Product_Price_Level;
    private DbsField acis_Driver_Record_Acis_Eop_Roth_Ind;
    private DbsField acis_Driver_Record_Acis_Eop_Plan_Issue_State;
    private DbsField acis_Driver_Record_Acis_Eop_Irc_Sectn_Cde;
    private DbsField acis_Driver_Record_Acis_Eop_Client_Id;
    private DbsField acis_Driver_Record_Acis_Eop_Portfolio_Model;
    private DbsField acis_Driver_Record_Acis_Eop_As_Cur_Dflt_Amt;
    private DbsField acis_Driver_Record_Acis_Eop_As_Incr_Amt;
    private DbsField acis_Driver_Record_Acis_Eop_As_Max_Pct;
    private DbsField acis_Driver_Record_Acis_Eop_Ae_Opt_Out_Days;
    private DbsField acis_Driver_Record_Acis_Eop_Plan_Name;
    private DbsField acis_Driver_Record_Acis_Eop_Auto_Enroll_Ind;
    private DbsField acis_Driver_Record_Acis_Eop_Auto_Save_Ind;
    private DbsField acis_Driver_Record_Acis_Eop_Guid;
    private DbsField acis_Driver_Record_Acis_Eop_Tiaa_Issue_Date_Ls;
    private DbsField acis_Driver_Record_Acis_Eop_Cref_Issue_Date_Ls;
    private DbsField acis_Driver_Record_Acis_Tsv_Ind;
    private DbsField acis_Driver_Record_Acis_Eop_Tiaa_Indx_Guarntd_Rte;
    private DbsField acis_Driver_Record_Acis_Eop_Crd_Pull_Ind;
    private DbsField acis_Driver_Record_Acis_Eop_Agent_Name;
    private DbsField acis_Driver_Record_Acis_Eop_Agent_Work_Addr1;
    private DbsField acis_Driver_Record_Acis_Eop_Agent_Work_Addr2;
    private DbsField acis_Driver_Record_Acis_Eop_Agent_Work_City;
    private DbsField acis_Driver_Record_Acis_Eop_Agent_Work_State;
    private DbsField acis_Driver_Record_Acis_Eop_Agent_Work_Zip;
    private DbsField acis_Driver_Record_Acis_Eop_Agent_Work_Phone;
    private DbsField acis_Driver_Record_Acis_Eop_Tic_Ind;
    private DbsField acis_Driver_Record_Acis_Eop_Tic_Startdate;
    private DbsField acis_Driver_Record_Acis_Eop_Tic_Enddate;
    private DbsField acis_Driver_Record_Acis_Eop_Tic_Percentage;
    private DbsField acis_Driver_Record_Acis_Eop_Tic_Postdays;
    private DbsField acis_Driver_Record_Acis_Eop_Tic_Limit;
    private DbsField acis_Driver_Record_Acis_Eop_Tic_Postfreq;
    private DbsField acis_Driver_Record_Acis_Eop_Tic_Pl_Level;
    private DbsField acis_Driver_Record_Acis_Eop_Tic_Windowdays;
    private DbsField acis_Driver_Record_Acis_Eop_Tic_Reqdlywindow;
    private DbsField acis_Driver_Record_Acis_Eop_Tic_Recap_Prov;
    private DbsField acis_Driver_Record_Acis_Ecs_Dcs_Annty_Optn_Ind;
    private DbsField acis_Driver_Record_Acis_Eop_Substitution_Ind;
    private DbsField acis_Driver_Record_Acis_Eop_Bundle_Record_Type;
    private DbsField acis_Driver_Record_Acis_Eop_Bundle_Number;
    private DbsField acis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_1;
    private DbsField acis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_2;
    private DbsField acis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_3;
    private DbsField acis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_4;
    private DbsField acis_Driver_Record_Acis_Eop_Cntr_Form_Numb;
    private DbsField acis_Driver_Record_Acis_Eop_Cert_Form_Numb;
    private DbsField acis_Driver_Record_Acis_Eop_Ed_Numb_T;
    private DbsField acis_Driver_Record_Acis_Eop_Ed_Numb_C;
    private DbsField acis_Driver_Record_Acis_Eop_Ira_Insert_Ind;
    private DbsField acis_Driver_Record_Acis_Eop_Non_Proprietary_Pkg_Ind;
    private DbsField acis_Driver_Record_Acis_Dialog_Sequence;
    private DbsField acis_Driver_Record_Acis_Multi_Plan_Ind;
    private DbsField acis_Driver_Record_Acis_Multi_Plan_Table;
    private DbsGroup acis_Driver_Record_Acis_Multi_Plan_TableRedef2;
    private DbsGroup acis_Driver_Record_Acis_Multi_Plan_Info;
    private DbsField acis_Driver_Record_Acis_Multi_Plan_No;
    private DbsField acis_Driver_Record_Acis_Multi_Sub_Plan;
    private DbsField acis_Driver_Record_Acis_Eop_Tiaa_Init_Prem;
    private DbsField acis_Driver_Record_Acis_Eop_Cref_Init_Prem;
    private DbsField acis_Driver_Record_Cis_Eop_Related_Contract_Info;
    private DbsGroup acis_Driver_Record_Cis_Eop_Related_Contract_InfoRedef3;
    private DbsGroup acis_Driver_Record_Cis_Eop_Related_Contract;
    private DbsField acis_Driver_Record_Cis_Eop_Related_Contract_Type;
    private DbsField acis_Driver_Record_Cis_Eop_Related_Tiaa_No;
    private DbsField acis_Driver_Record_Cis_Eop_Related_Tiaa_Issue_Date;
    private DbsField acis_Driver_Record_Cis_Eop_Related_First_Paym_Date;
    private DbsField acis_Driver_Record_Cis_Eop_Related_Tiaa_Total_Amt;
    private DbsField acis_Driver_Record_Cis_Eop_Related_Last_Paym_Date;
    private DbsField acis_Driver_Record_Cis_Eop_Related_Payment_Freq;
    private DbsField acis_Driver_Record_Cis_Eop_Related_Tiaa_Issue_State;
    private DbsField acis_Driver_Record_Cis_Eop_First_Tpa_Ipro_Ind;

    public DbsGroup getAcis_Driver_Record() { return acis_Driver_Record; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Record_Id() { return acis_Driver_Record_Acis_Eop_Record_Id; }

    public DbsGroup getAcis_Driver_Record_Acis_Eop_Record_IdRedef1() { return acis_Driver_Record_Acis_Eop_Record_IdRedef1; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Record_Type() { return acis_Driver_Record_Acis_Eop_Record_Type; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Output_Profile() { return acis_Driver_Record_Acis_Eop_Output_Profile; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Bundle_Type() { return acis_Driver_Record_Acis_Eop_Bundle_Type; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Us_Soc_Sec() { return acis_Driver_Record_Acis_Eop_Us_Soc_Sec; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Tiaa_Cntr_Numb_1() { return acis_Driver_Record_Acis_Eop_Tiaa_Cntr_Numb_1; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Cref_Cert_Numb_1() { return acis_Driver_Record_Acis_Eop_Cref_Cert_Numb_1; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Print_Package_Type() { return acis_Driver_Record_Acis_Eop_Print_Package_Type; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Product_Type() { return acis_Driver_Record_Acis_Eop_Product_Type; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Reprint_Type() { return acis_Driver_Record_Acis_Eop_Reprint_Type; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Tiaa_Cref_Both() { return acis_Driver_Record_Acis_Eop_Tiaa_Cref_Both; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Sg_Plan_No() { return acis_Driver_Record_Acis_Eop_Sg_Plan_No; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Tiaa_Issue_Date() { return acis_Driver_Record_Acis_Eop_Tiaa_Issue_Date; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Cref_Issue_Date() { return acis_Driver_Record_Acis_Eop_Cref_Issue_Date; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Original_Issue_State() { return acis_Driver_Record_Acis_Eop_Original_Issue_State; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Current_Issue_State() { return acis_Driver_Record_Acis_Eop_Current_Issue_State; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Tiaa_Cntr_Numb_2() { return acis_Driver_Record_Acis_Eop_Tiaa_Cntr_Numb_2; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Cref_Cert_Numb_2() { return acis_Driver_Record_Acis_Eop_Cref_Cert_Numb_2; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Cref_Cert_Numb_4() { return acis_Driver_Record_Acis_Eop_Cref_Cert_Numb_4; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Tiaa_Annuity_Start_Date() { return acis_Driver_Record_Acis_Eop_Tiaa_Annuity_Start_Date; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Cref_Annuity_Start_Date() { return acis_Driver_Record_Acis_Eop_Cref_Annuity_Start_Date; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Dob() { return acis_Driver_Record_Acis_Eop_Dob; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Dt_App_Recvd() { return acis_Driver_Record_Acis_Eop_Dt_App_Recvd; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Estate_As_Bene() { return acis_Driver_Record_Acis_Eop_Estate_As_Bene; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Trust_As_Bene() { return acis_Driver_Record_Acis_Eop_Trust_As_Bene; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Beneficiary_Category() { return acis_Driver_Record_Acis_Eop_Beneficiary_Category; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Cref_Request() { return acis_Driver_Record_Acis_Eop_Cref_Request; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Name_Prefix_Lg() { return acis_Driver_Record_Acis_Eop_Name_Prefix_Lg; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Name_Last_Lg() { return acis_Driver_Record_Acis_Eop_Name_Last_Lg; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Name_First_Lg() { return acis_Driver_Record_Acis_Eop_Name_First_Lg; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Name_Middle_Lg() { return acis_Driver_Record_Acis_Eop_Name_Middle_Lg; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Name_Suffix_Lg() { return acis_Driver_Record_Acis_Eop_Name_Suffix_Lg; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Pin_Numb() { return acis_Driver_Record_Acis_Eop_Pin_Numb; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Each_Name_Line() { return acis_Driver_Record_Acis_Eop_Each_Name_Line; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Address_Type_Code() { return acis_Driver_Record_Acis_Eop_Address_Type_Code; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Post_Zip() { return acis_Driver_Record_Acis_Eop_Post_Zip; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Inst_Name() { return acis_Driver_Record_Acis_Eop_Inst_Name; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Each_Ia_Line() { return acis_Driver_Record_Acis_Eop_Each_Ia_Line; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Inst_Zip() { return acis_Driver_Record_Acis_Eop_Inst_Zip; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Lob() { return acis_Driver_Record_Acis_Eop_Lob; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Lob_Type() { return acis_Driver_Record_Acis_Eop_Lob_Type; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Name_State() { return acis_Driver_Record_Acis_Eop_Name_State; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Institution_Code() { return acis_Driver_Record_Acis_Eop_Institution_Code; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Rollover_Amt() { return acis_Driver_Record_Acis_Eop_Rollover_Amt; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Mail_Date() { return acis_Driver_Record_Acis_Eop_Mail_Date; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Unit_Name() { return acis_Driver_Record_Acis_Eop_Unit_Name; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Email_Address() { return acis_Driver_Record_Acis_Eop_Email_Address; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Acct_Sum_Type() { return acis_Driver_Record_Acis_Eop_Acct_Sum_Type; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Effective_Date() { return acis_Driver_Record_Acis_Eop_Effective_Date; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Group_Contract_Name() { return acis_Driver_Record_Acis_Eop_Group_Contract_Name; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Employer_Name() { return acis_Driver_Record_Acis_Eop_Employer_Name; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Access_Code() { return acis_Driver_Record_Acis_Eop_Access_Code; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Ownership_Code() { return acis_Driver_Record_Acis_Eop_Ownership_Code; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Alloc_Descrepancy() { return acis_Driver_Record_Acis_Eop_Alloc_Descrepancy; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Participant_Status() { return acis_Driver_Record_Acis_Eop_Participant_Status; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Portfolio_Selected() { return acis_Driver_Record_Acis_Eop_Portfolio_Selected; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Divorce_Contract() { return acis_Driver_Record_Acis_Eop_Divorce_Contract; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Access_Cd_Ind() { return acis_Driver_Record_Acis_Eop_Access_Cd_Ind; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Mailing_Instructions() { return acis_Driver_Record_Acis_Eop_Mailing_Instructions; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Application_Source() { return acis_Driver_Record_Acis_Eop_Application_Source; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Gsra_Loan_Ind() { return acis_Driver_Record_Acis_Eop_Gsra_Loan_Ind; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Spec_Ppg_Ind() { return acis_Driver_Record_Acis_Eop_Spec_Ppg_Ind; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Mult_App_Version() { return acis_Driver_Record_Acis_Eop_Mult_App_Version; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Mult_App_Status() { return acis_Driver_Record_Acis_Eop_Mult_App_Status; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Mult_App_Lob() { return acis_Driver_Record_Acis_Eop_Mult_App_Lob; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Mult_App_Lob_Type() { return acis_Driver_Record_Acis_Eop_Mult_App_Lob_Type; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Ira_Record_Type() { return acis_Driver_Record_Acis_Eop_Ira_Record_Type; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Enroll_Req_Type() { return acis_Driver_Record_Acis_Eop_Enroll_Req_Type; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Erisa_Inst() { return acis_Driver_Record_Acis_Eop_Erisa_Inst; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Each_Correction_Type() { return acis_Driver_Record_Acis_Eop_Each_Correction_Type; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Oia_Ind() { return acis_Driver_Record_Acis_Eop_Oia_Ind; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Sg_Subplan_No() { return acis_Driver_Record_Acis_Eop_Sg_Subplan_No; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Sg_Subplan_Type() { return acis_Driver_Record_Acis_Eop_Sg_Subplan_Type; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Omni_Issue() { return acis_Driver_Record_Acis_Eop_Omni_Issue; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Single_Issue_Ind() { return acis_Driver_Record_Acis_Eop_Single_Issue_Ind; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Spec_Fund_Ind() { return acis_Driver_Record_Acis_Eop_Spec_Fund_Ind; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Product_Price_Level() { return acis_Driver_Record_Acis_Eop_Product_Price_Level; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Roth_Ind() { return acis_Driver_Record_Acis_Eop_Roth_Ind; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Plan_Issue_State() { return acis_Driver_Record_Acis_Eop_Plan_Issue_State; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Irc_Sectn_Cde() { return acis_Driver_Record_Acis_Eop_Irc_Sectn_Cde; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Client_Id() { return acis_Driver_Record_Acis_Eop_Client_Id; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Portfolio_Model() { return acis_Driver_Record_Acis_Eop_Portfolio_Model; }

    public DbsField getAcis_Driver_Record_Acis_Eop_As_Cur_Dflt_Amt() { return acis_Driver_Record_Acis_Eop_As_Cur_Dflt_Amt; }

    public DbsField getAcis_Driver_Record_Acis_Eop_As_Incr_Amt() { return acis_Driver_Record_Acis_Eop_As_Incr_Amt; }

    public DbsField getAcis_Driver_Record_Acis_Eop_As_Max_Pct() { return acis_Driver_Record_Acis_Eop_As_Max_Pct; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Ae_Opt_Out_Days() { return acis_Driver_Record_Acis_Eop_Ae_Opt_Out_Days; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Plan_Name() { return acis_Driver_Record_Acis_Eop_Plan_Name; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Auto_Enroll_Ind() { return acis_Driver_Record_Acis_Eop_Auto_Enroll_Ind; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Auto_Save_Ind() { return acis_Driver_Record_Acis_Eop_Auto_Save_Ind; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Guid() { return acis_Driver_Record_Acis_Eop_Guid; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Tiaa_Issue_Date_Ls() { return acis_Driver_Record_Acis_Eop_Tiaa_Issue_Date_Ls; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Cref_Issue_Date_Ls() { return acis_Driver_Record_Acis_Eop_Cref_Issue_Date_Ls; }

    public DbsField getAcis_Driver_Record_Acis_Tsv_Ind() { return acis_Driver_Record_Acis_Tsv_Ind; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Tiaa_Indx_Guarntd_Rte() { return acis_Driver_Record_Acis_Eop_Tiaa_Indx_Guarntd_Rte; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Crd_Pull_Ind() { return acis_Driver_Record_Acis_Eop_Crd_Pull_Ind; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Agent_Name() { return acis_Driver_Record_Acis_Eop_Agent_Name; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Agent_Work_Addr1() { return acis_Driver_Record_Acis_Eop_Agent_Work_Addr1; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Agent_Work_Addr2() { return acis_Driver_Record_Acis_Eop_Agent_Work_Addr2; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Agent_Work_City() { return acis_Driver_Record_Acis_Eop_Agent_Work_City; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Agent_Work_State() { return acis_Driver_Record_Acis_Eop_Agent_Work_State; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Agent_Work_Zip() { return acis_Driver_Record_Acis_Eop_Agent_Work_Zip; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Agent_Work_Phone() { return acis_Driver_Record_Acis_Eop_Agent_Work_Phone; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Tic_Ind() { return acis_Driver_Record_Acis_Eop_Tic_Ind; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Tic_Startdate() { return acis_Driver_Record_Acis_Eop_Tic_Startdate; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Tic_Enddate() { return acis_Driver_Record_Acis_Eop_Tic_Enddate; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Tic_Percentage() { return acis_Driver_Record_Acis_Eop_Tic_Percentage; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Tic_Postdays() { return acis_Driver_Record_Acis_Eop_Tic_Postdays; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Tic_Limit() { return acis_Driver_Record_Acis_Eop_Tic_Limit; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Tic_Postfreq() { return acis_Driver_Record_Acis_Eop_Tic_Postfreq; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Tic_Pl_Level() { return acis_Driver_Record_Acis_Eop_Tic_Pl_Level; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Tic_Windowdays() { return acis_Driver_Record_Acis_Eop_Tic_Windowdays; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Tic_Reqdlywindow() { return acis_Driver_Record_Acis_Eop_Tic_Reqdlywindow; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Tic_Recap_Prov() { return acis_Driver_Record_Acis_Eop_Tic_Recap_Prov; }

    public DbsField getAcis_Driver_Record_Acis_Ecs_Dcs_Annty_Optn_Ind() { return acis_Driver_Record_Acis_Ecs_Dcs_Annty_Optn_Ind; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Substitution_Ind() { return acis_Driver_Record_Acis_Eop_Substitution_Ind; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Bundle_Record_Type() { return acis_Driver_Record_Acis_Eop_Bundle_Record_Type; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Bundle_Number() { return acis_Driver_Record_Acis_Eop_Bundle_Number; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_1() { return acis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_1; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_2() { return acis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_2; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_3() { return acis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_3; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_4() { return acis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_4; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Cntr_Form_Numb() { return acis_Driver_Record_Acis_Eop_Cntr_Form_Numb; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Cert_Form_Numb() { return acis_Driver_Record_Acis_Eop_Cert_Form_Numb; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Ed_Numb_T() { return acis_Driver_Record_Acis_Eop_Ed_Numb_T; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Ed_Numb_C() { return acis_Driver_Record_Acis_Eop_Ed_Numb_C; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Ira_Insert_Ind() { return acis_Driver_Record_Acis_Eop_Ira_Insert_Ind; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Non_Proprietary_Pkg_Ind() { return acis_Driver_Record_Acis_Eop_Non_Proprietary_Pkg_Ind; }

    public DbsField getAcis_Driver_Record_Acis_Dialog_Sequence() { return acis_Driver_Record_Acis_Dialog_Sequence; }

    public DbsField getAcis_Driver_Record_Acis_Multi_Plan_Ind() { return acis_Driver_Record_Acis_Multi_Plan_Ind; }

    public DbsField getAcis_Driver_Record_Acis_Multi_Plan_Table() { return acis_Driver_Record_Acis_Multi_Plan_Table; }

    public DbsGroup getAcis_Driver_Record_Acis_Multi_Plan_TableRedef2() { return acis_Driver_Record_Acis_Multi_Plan_TableRedef2; }

    public DbsGroup getAcis_Driver_Record_Acis_Multi_Plan_Info() { return acis_Driver_Record_Acis_Multi_Plan_Info; }

    public DbsField getAcis_Driver_Record_Acis_Multi_Plan_No() { return acis_Driver_Record_Acis_Multi_Plan_No; }

    public DbsField getAcis_Driver_Record_Acis_Multi_Sub_Plan() { return acis_Driver_Record_Acis_Multi_Sub_Plan; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Tiaa_Init_Prem() { return acis_Driver_Record_Acis_Eop_Tiaa_Init_Prem; }

    public DbsField getAcis_Driver_Record_Acis_Eop_Cref_Init_Prem() { return acis_Driver_Record_Acis_Eop_Cref_Init_Prem; }

    public DbsField getAcis_Driver_Record_Cis_Eop_Related_Contract_Info() { return acis_Driver_Record_Cis_Eop_Related_Contract_Info; }

    public DbsGroup getAcis_Driver_Record_Cis_Eop_Related_Contract_InfoRedef3() { return acis_Driver_Record_Cis_Eop_Related_Contract_InfoRedef3; }

    public DbsGroup getAcis_Driver_Record_Cis_Eop_Related_Contract() { return acis_Driver_Record_Cis_Eop_Related_Contract; }

    public DbsField getAcis_Driver_Record_Cis_Eop_Related_Contract_Type() { return acis_Driver_Record_Cis_Eop_Related_Contract_Type; }

    public DbsField getAcis_Driver_Record_Cis_Eop_Related_Tiaa_No() { return acis_Driver_Record_Cis_Eop_Related_Tiaa_No; }

    public DbsField getAcis_Driver_Record_Cis_Eop_Related_Tiaa_Issue_Date() { return acis_Driver_Record_Cis_Eop_Related_Tiaa_Issue_Date; }

    public DbsField getAcis_Driver_Record_Cis_Eop_Related_First_Paym_Date() { return acis_Driver_Record_Cis_Eop_Related_First_Paym_Date; }

    public DbsField getAcis_Driver_Record_Cis_Eop_Related_Tiaa_Total_Amt() { return acis_Driver_Record_Cis_Eop_Related_Tiaa_Total_Amt; }

    public DbsField getAcis_Driver_Record_Cis_Eop_Related_Last_Paym_Date() { return acis_Driver_Record_Cis_Eop_Related_Last_Paym_Date; }

    public DbsField getAcis_Driver_Record_Cis_Eop_Related_Payment_Freq() { return acis_Driver_Record_Cis_Eop_Related_Payment_Freq; }

    public DbsField getAcis_Driver_Record_Cis_Eop_Related_Tiaa_Issue_State() { return acis_Driver_Record_Cis_Eop_Related_Tiaa_Issue_State; }

    public DbsField getAcis_Driver_Record_Cis_Eop_First_Tpa_Ipro_Ind() { return acis_Driver_Record_Cis_Eop_First_Tpa_Ipro_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        acis_Driver_Record = newGroupInRecord("acis_Driver_Record", "ACIS-DRIVER-RECORD");
        acis_Driver_Record_Acis_Eop_Record_Id = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Record_Id", "ACIS-EOP-RECORD-ID", FieldType.STRING, 
            43);
        acis_Driver_Record_Acis_Eop_Record_IdRedef1 = acis_Driver_Record.newGroupInGroup("acis_Driver_Record_Acis_Eop_Record_IdRedef1", "Redefines", acis_Driver_Record_Acis_Eop_Record_Id);
        acis_Driver_Record_Acis_Eop_Record_Type = acis_Driver_Record_Acis_Eop_Record_IdRedef1.newFieldInGroup("acis_Driver_Record_Acis_Eop_Record_Type", 
            "ACIS-EOP-RECORD-TYPE", FieldType.STRING, 2);
        acis_Driver_Record_Acis_Eop_Output_Profile = acis_Driver_Record_Acis_Eop_Record_IdRedef1.newFieldInGroup("acis_Driver_Record_Acis_Eop_Output_Profile", 
            "ACIS-EOP-OUTPUT-PROFILE", FieldType.STRING, 2);
        acis_Driver_Record_Acis_Eop_Bundle_Type = acis_Driver_Record_Acis_Eop_Record_IdRedef1.newFieldInGroup("acis_Driver_Record_Acis_Eop_Bundle_Type", 
            "ACIS-EOP-BUNDLE-TYPE", FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Us_Soc_Sec = acis_Driver_Record_Acis_Eop_Record_IdRedef1.newFieldInGroup("acis_Driver_Record_Acis_Eop_Us_Soc_Sec", 
            "ACIS-EOP-US-SOC-SEC", FieldType.STRING, 11);
        acis_Driver_Record_Acis_Eop_Tiaa_Cntr_Numb_1 = acis_Driver_Record_Acis_Eop_Record_IdRedef1.newFieldInGroup("acis_Driver_Record_Acis_Eop_Tiaa_Cntr_Numb_1", 
            "ACIS-EOP-TIAA-CNTR-NUMB-1", FieldType.STRING, 10);
        acis_Driver_Record_Acis_Eop_Cref_Cert_Numb_1 = acis_Driver_Record_Acis_Eop_Record_IdRedef1.newFieldInGroup("acis_Driver_Record_Acis_Eop_Cref_Cert_Numb_1", 
            "ACIS-EOP-CREF-CERT-NUMB-1", FieldType.STRING, 10);
        acis_Driver_Record_Acis_Eop_Print_Package_Type = acis_Driver_Record_Acis_Eop_Record_IdRedef1.newFieldInGroup("acis_Driver_Record_Acis_Eop_Print_Package_Type", 
            "ACIS-EOP-PRINT-PACKAGE-TYPE", FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Product_Type = acis_Driver_Record_Acis_Eop_Record_IdRedef1.newFieldInGroup("acis_Driver_Record_Acis_Eop_Product_Type", 
            "ACIS-EOP-PRODUCT-TYPE", FieldType.STRING, 6);
        acis_Driver_Record_Acis_Eop_Reprint_Type = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Reprint_Type", "ACIS-EOP-REPRINT-TYPE", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Tiaa_Cref_Both = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Tiaa_Cref_Both", "ACIS-EOP-TIAA-CREF-BOTH", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Sg_Plan_No = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Sg_Plan_No", "ACIS-EOP-SG-PLAN-NO", FieldType.STRING, 
            6);
        acis_Driver_Record_Acis_Eop_Tiaa_Issue_Date = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Tiaa_Issue_Date", "ACIS-EOP-TIAA-ISSUE-DATE", 
            FieldType.STRING, 8);
        acis_Driver_Record_Acis_Eop_Cref_Issue_Date = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Cref_Issue_Date", "ACIS-EOP-CREF-ISSUE-DATE", 
            FieldType.STRING, 8);
        acis_Driver_Record_Acis_Eop_Original_Issue_State = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Original_Issue_State", "ACIS-EOP-ORIGINAL-ISSUE-STATE", 
            FieldType.STRING, 2);
        acis_Driver_Record_Acis_Eop_Current_Issue_State = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Current_Issue_State", "ACIS-EOP-CURRENT-ISSUE-STATE", 
            FieldType.STRING, 2);
        acis_Driver_Record_Acis_Eop_Tiaa_Cntr_Numb_2 = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Tiaa_Cntr_Numb_2", "ACIS-EOP-TIAA-CNTR-NUMB-2", 
            FieldType.STRING, 10);
        acis_Driver_Record_Acis_Eop_Cref_Cert_Numb_2 = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Cref_Cert_Numb_2", "ACIS-EOP-CREF-CERT-NUMB-2", 
            FieldType.STRING, 10);
        acis_Driver_Record_Acis_Eop_Cref_Cert_Numb_4 = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Cref_Cert_Numb_4", "ACIS-EOP-CREF-CERT-NUMB-4", 
            FieldType.STRING, 10);
        acis_Driver_Record_Acis_Eop_Tiaa_Annuity_Start_Date = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Tiaa_Annuity_Start_Date", 
            "ACIS-EOP-TIAA-ANNUITY-START-DATE", FieldType.STRING, 8);
        acis_Driver_Record_Acis_Eop_Cref_Annuity_Start_Date = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Cref_Annuity_Start_Date", 
            "ACIS-EOP-CREF-ANNUITY-START-DATE", FieldType.STRING, 8);
        acis_Driver_Record_Acis_Eop_Dob = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Dob", "ACIS-EOP-DOB", FieldType.STRING, 8);
        acis_Driver_Record_Acis_Eop_Dt_App_Recvd = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Dt_App_Recvd", "ACIS-EOP-DT-APP-RECVD", 
            FieldType.STRING, 8);
        acis_Driver_Record_Acis_Eop_Estate_As_Bene = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Estate_As_Bene", "ACIS-EOP-ESTATE-AS-BENE", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Trust_As_Bene = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Trust_As_Bene", "ACIS-EOP-TRUST-AS-BENE", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Beneficiary_Category = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Beneficiary_Category", "ACIS-EOP-BENEFICIARY-CATEGORY", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Cref_Request = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Cref_Request", "ACIS-EOP-CREF-REQUEST", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Name_Prefix_Lg = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Name_Prefix_Lg", "ACIS-EOP-NAME-PREFIX-LG", 
            FieldType.STRING, 8);
        acis_Driver_Record_Acis_Eop_Name_Last_Lg = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Name_Last_Lg", "ACIS-EOP-NAME-LAST-LG", 
            FieldType.STRING, 30);
        acis_Driver_Record_Acis_Eop_Name_First_Lg = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Name_First_Lg", "ACIS-EOP-NAME-FIRST-LG", 
            FieldType.STRING, 30);
        acis_Driver_Record_Acis_Eop_Name_Middle_Lg = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Name_Middle_Lg", "ACIS-EOP-NAME-MIDDLE-LG", 
            FieldType.STRING, 30);
        acis_Driver_Record_Acis_Eop_Name_Suffix_Lg = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Name_Suffix_Lg", "ACIS-EOP-NAME-SUFFIX-LG", 
            FieldType.STRING, 8);
        acis_Driver_Record_Acis_Eop_Pin_Numb = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Pin_Numb", "ACIS-EOP-PIN-NUMB", FieldType.STRING, 
            13);
        acis_Driver_Record_Acis_Eop_Each_Name_Line = acis_Driver_Record.newFieldArrayInGroup("acis_Driver_Record_Acis_Eop_Each_Name_Line", "ACIS-EOP-EACH-NAME-LINE", 
            FieldType.STRING, 35, new DbsArrayController(1,6));
        acis_Driver_Record_Acis_Eop_Address_Type_Code = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Address_Type_Code", "ACIS-EOP-ADDRESS-TYPE-CODE", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Post_Zip = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Post_Zip", "ACIS-EOP-POST-ZIP", FieldType.STRING, 
            12);
        acis_Driver_Record_Acis_Eop_Inst_Name = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Inst_Name", "ACIS-EOP-INST-NAME", FieldType.STRING, 
            76);
        acis_Driver_Record_Acis_Eop_Each_Ia_Line = acis_Driver_Record.newFieldArrayInGroup("acis_Driver_Record_Acis_Eop_Each_Ia_Line", "ACIS-EOP-EACH-IA-LINE", 
            FieldType.STRING, 35, new DbsArrayController(1,6));
        acis_Driver_Record_Acis_Eop_Inst_Zip = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Inst_Zip", "ACIS-EOP-INST-ZIP", FieldType.STRING, 
            5);
        acis_Driver_Record_Acis_Eop_Lob = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Lob", "ACIS-EOP-LOB", FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Lob_Type = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Lob_Type", "ACIS-EOP-LOB-TYPE", FieldType.STRING, 
            1);
        acis_Driver_Record_Acis_Eop_Name_State = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Name_State", "ACIS-EOP-NAME-STATE", FieldType.STRING, 
            15);
        acis_Driver_Record_Acis_Eop_Institution_Code = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Institution_Code", "ACIS-EOP-INSTITUTION-CODE", 
            FieldType.STRING, 4);
        acis_Driver_Record_Acis_Eop_Rollover_Amt = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Rollover_Amt", "ACIS-EOP-ROLLOVER-AMT", 
            FieldType.STRING, 14);
        acis_Driver_Record_Acis_Eop_Mail_Date = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Mail_Date", "ACIS-EOP-MAIL-DATE", FieldType.STRING, 
            8);
        acis_Driver_Record_Acis_Eop_Unit_Name = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Unit_Name", "ACIS-EOP-UNIT-NAME", FieldType.STRING, 
            10);
        acis_Driver_Record_Acis_Eop_Email_Address = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Email_Address", "ACIS-EOP-EMAIL-ADDRESS", 
            FieldType.STRING, 50);
        acis_Driver_Record_Acis_Eop_Acct_Sum_Type = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Acct_Sum_Type", "ACIS-EOP-ACCT-SUM-TYPE", 
            FieldType.STRING, 56);
        acis_Driver_Record_Acis_Eop_Effective_Date = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Effective_Date", "ACIS-EOP-EFFECTIVE-DATE", 
            FieldType.STRING, 8);
        acis_Driver_Record_Acis_Eop_Group_Contract_Name = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Group_Contract_Name", "ACIS-EOP-GROUP-CONTRACT-NAME", 
            FieldType.STRING, 12);
        acis_Driver_Record_Acis_Eop_Employer_Name = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Employer_Name", "ACIS-EOP-EMPLOYER-NAME", 
            FieldType.STRING, 40);
        acis_Driver_Record_Acis_Eop_Access_Code = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Access_Code", "ACIS-EOP-ACCESS-CODE", 
            FieldType.STRING, 9);
        acis_Driver_Record_Acis_Eop_Ownership_Code = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Ownership_Code", "ACIS-EOP-OWNERSHIP-CODE", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Alloc_Descrepancy = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Alloc_Descrepancy", "ACIS-EOP-ALLOC-DESCREPANCY", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Participant_Status = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Participant_Status", "ACIS-EOP-PARTICIPANT-STATUS", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Portfolio_Selected = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Portfolio_Selected", "ACIS-EOP-PORTFOLIO-SELECTED", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Divorce_Contract = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Divorce_Contract", "ACIS-EOP-DIVORCE-CONTRACT", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Access_Cd_Ind = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Access_Cd_Ind", "ACIS-EOP-ACCESS-CD-IND", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Mailing_Instructions = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Mailing_Instructions", "ACIS-EOP-MAILING-INSTRUCTIONS", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Application_Source = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Application_Source", "ACIS-EOP-APPLICATION-SOURCE", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Gsra_Loan_Ind = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Gsra_Loan_Ind", "ACIS-EOP-GSRA-LOAN-IND", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Spec_Ppg_Ind = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Spec_Ppg_Ind", "ACIS-EOP-SPEC-PPG-IND", 
            FieldType.STRING, 2);
        acis_Driver_Record_Acis_Eop_Mult_App_Version = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Mult_App_Version", "ACIS-EOP-MULT-APP-VERSION", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Mult_App_Status = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Mult_App_Status", "ACIS-EOP-MULT-APP-STATUS", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Mult_App_Lob = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Mult_App_Lob", "ACIS-EOP-MULT-APP-LOB", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Mult_App_Lob_Type = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Mult_App_Lob_Type", "ACIS-EOP-MULT-APP-LOB-TYPE", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Ira_Record_Type = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Ira_Record_Type", "ACIS-EOP-IRA-RECORD-TYPE", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Enroll_Req_Type = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Enroll_Req_Type", "ACIS-EOP-ENROLL-REQ-TYPE", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Erisa_Inst = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Erisa_Inst", "ACIS-EOP-ERISA-INST", FieldType.STRING, 
            1);
        acis_Driver_Record_Acis_Eop_Each_Correction_Type = acis_Driver_Record.newFieldArrayInGroup("acis_Driver_Record_Acis_Eop_Each_Correction_Type", 
            "ACIS-EOP-EACH-CORRECTION-TYPE", FieldType.STRING, 1, new DbsArrayController(1,20));
        acis_Driver_Record_Acis_Eop_Oia_Ind = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Oia_Ind", "ACIS-EOP-OIA-IND", FieldType.STRING, 
            1);
        acis_Driver_Record_Acis_Eop_Sg_Subplan_No = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Sg_Subplan_No", "ACIS-EOP-SG-SUBPLAN-NO", 
            FieldType.STRING, 6);
        acis_Driver_Record_Acis_Eop_Sg_Subplan_Type = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Sg_Subplan_Type", "ACIS-EOP-SG-SUBPLAN-TYPE", 
            FieldType.STRING, 3);
        acis_Driver_Record_Acis_Eop_Omni_Issue = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Omni_Issue", "ACIS-EOP-OMNI-ISSUE", FieldType.STRING, 
            1);
        acis_Driver_Record_Acis_Eop_Single_Issue_Ind = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Single_Issue_Ind", "ACIS-EOP-SINGLE-ISSUE-IND", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Spec_Fund_Ind = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Spec_Fund_Ind", "ACIS-EOP-SPEC-FUND-IND", 
            FieldType.STRING, 3);
        acis_Driver_Record_Acis_Eop_Product_Price_Level = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Product_Price_Level", "ACIS-EOP-PRODUCT-PRICE-LEVEL", 
            FieldType.STRING, 5);
        acis_Driver_Record_Acis_Eop_Roth_Ind = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Roth_Ind", "ACIS-EOP-ROTH-IND", FieldType.STRING, 
            1);
        acis_Driver_Record_Acis_Eop_Plan_Issue_State = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Plan_Issue_State", "ACIS-EOP-PLAN-ISSUE-STATE", 
            FieldType.STRING, 2);
        acis_Driver_Record_Acis_Eop_Irc_Sectn_Cde = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Irc_Sectn_Cde", "ACIS-EOP-IRC-SECTN-CDE", 
            FieldType.NUMERIC, 2);
        acis_Driver_Record_Acis_Eop_Client_Id = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Client_Id", "ACIS-EOP-CLIENT-ID", FieldType.STRING, 
            6);
        acis_Driver_Record_Acis_Eop_Portfolio_Model = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Portfolio_Model", "ACIS-EOP-PORTFOLIO-MODEL", 
            FieldType.STRING, 4);
        acis_Driver_Record_Acis_Eop_As_Cur_Dflt_Amt = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_As_Cur_Dflt_Amt", "ACIS-EOP-AS-CUR-DFLT-AMT", 
            FieldType.STRING, 10);
        acis_Driver_Record_Acis_Eop_As_Incr_Amt = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_As_Incr_Amt", "ACIS-EOP-AS-INCR-AMT", 
            FieldType.STRING, 7);
        acis_Driver_Record_Acis_Eop_As_Max_Pct = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_As_Max_Pct", "ACIS-EOP-AS-MAX-PCT", FieldType.STRING, 
            7);
        acis_Driver_Record_Acis_Eop_Ae_Opt_Out_Days = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Ae_Opt_Out_Days", "ACIS-EOP-AE-OPT-OUT-DAYS", 
            FieldType.STRING, 2);
        acis_Driver_Record_Acis_Eop_Plan_Name = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Plan_Name", "ACIS-EOP-PLAN-NAME", FieldType.STRING, 
            64);
        acis_Driver_Record_Acis_Eop_Auto_Enroll_Ind = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Auto_Enroll_Ind", "ACIS-EOP-AUTO-ENROLL-IND", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Auto_Save_Ind = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Auto_Save_Ind", "ACIS-EOP-AUTO-SAVE-IND", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Guid = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Guid", "ACIS_EOP_GUID", FieldType.NUMERIC, 
            9);
        acis_Driver_Record_Acis_Eop_Tiaa_Issue_Date_Ls = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Tiaa_Issue_Date_Ls", "ACIS-EOP-TIAA-ISSUE-DATE-LS", 
            FieldType.STRING, 8);
        acis_Driver_Record_Acis_Eop_Cref_Issue_Date_Ls = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Cref_Issue_Date_Ls", "ACIS-EOP-CREF-ISSUE-DATE-LS", 
            FieldType.STRING, 8);
        acis_Driver_Record_Acis_Tsv_Ind = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Tsv_Ind", "ACIS-TSV-IND", FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Tiaa_Indx_Guarntd_Rte = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Tiaa_Indx_Guarntd_Rte", "ACIS-EOP-TIAA-INDX-GUARNTD-RTE", 
            FieldType.STRING, 6);
        acis_Driver_Record_Acis_Eop_Crd_Pull_Ind = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Crd_Pull_Ind", "ACIS-EOP-CRD-PULL-IND", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Agent_Name = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Agent_Name", "ACIS-EOP-AGENT-NAME", FieldType.STRING, 
            90);
        acis_Driver_Record_Acis_Eop_Agent_Work_Addr1 = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Agent_Work_Addr1", "ACIS-EOP-AGENT-WORK-ADDR1", 
            FieldType.STRING, 35);
        acis_Driver_Record_Acis_Eop_Agent_Work_Addr2 = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Agent_Work_Addr2", "ACIS-EOP-AGENT-WORK-ADDR2", 
            FieldType.STRING, 35);
        acis_Driver_Record_Acis_Eop_Agent_Work_City = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Agent_Work_City", "ACIS-EOP-AGENT-WORK-CITY", 
            FieldType.STRING, 35);
        acis_Driver_Record_Acis_Eop_Agent_Work_State = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Agent_Work_State", "ACIS-EOP-AGENT-WORK-STATE", 
            FieldType.STRING, 2);
        acis_Driver_Record_Acis_Eop_Agent_Work_Zip = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Agent_Work_Zip", "ACIS-EOP-AGENT-WORK-ZIP", 
            FieldType.STRING, 10);
        acis_Driver_Record_Acis_Eop_Agent_Work_Phone = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Agent_Work_Phone", "ACIS-EOP-AGENT-WORK-PHONE", 
            FieldType.STRING, 25);
        acis_Driver_Record_Acis_Eop_Tic_Ind = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Tic_Ind", "ACIS-EOP-TIC-IND", FieldType.STRING, 
            1);
        acis_Driver_Record_Acis_Eop_Tic_Startdate = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Tic_Startdate", "ACIS-EOP-TIC-STARTDATE", 
            FieldType.STRING, 8);
        acis_Driver_Record_Acis_Eop_Tic_Enddate = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Tic_Enddate", "ACIS-EOP-TIC-ENDDATE", 
            FieldType.STRING, 8);
        acis_Driver_Record_Acis_Eop_Tic_Percentage = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Tic_Percentage", "ACIS-EOP-TIC-PERCENTAGE", 
            FieldType.STRING, 6);
        acis_Driver_Record_Acis_Eop_Tic_Postdays = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Tic_Postdays", "ACIS-EOP-TIC-POSTDAYS", 
            FieldType.STRING, 2);
        acis_Driver_Record_Acis_Eop_Tic_Limit = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Tic_Limit", "ACIS-EOP-TIC-LIMIT", FieldType.STRING, 
            14);
        acis_Driver_Record_Acis_Eop_Tic_Postfreq = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Tic_Postfreq", "ACIS-EOP-TIC-POSTFREQ", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Tic_Pl_Level = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Tic_Pl_Level", "ACIS-EOP-TIC-PL-LEVEL", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Tic_Windowdays = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Tic_Windowdays", "ACIS-EOP-TIC-WINDOWDAYS", 
            FieldType.STRING, 9);
        acis_Driver_Record_Acis_Eop_Tic_Reqdlywindow = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Tic_Reqdlywindow", "ACIS-EOP-TIC-REQDLYWINDOW", 
            FieldType.STRING, 9);
        acis_Driver_Record_Acis_Eop_Tic_Recap_Prov = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Tic_Recap_Prov", "ACIS-EOP-TIC-RECAP-PROV", 
            FieldType.STRING, 14);
        acis_Driver_Record_Acis_Ecs_Dcs_Annty_Optn_Ind = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Ecs_Dcs_Annty_Optn_Ind", "ACIS-ECS-DCS-ANNTY-OPTN-IND", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Substitution_Ind = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Substitution_Ind", "ACIS-EOP-SUBSTITUTION-IND", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Bundle_Record_Type = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Bundle_Record_Type", "ACIS-EOP-BUNDLE-RECORD-TYPE", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Bundle_Number = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Bundle_Number", "ACIS-EOP-BUNDLE-NUMBER", 
            FieldType.NUMERIC, 9);
        acis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_1 = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_1", "ACIS-EOP-CL-TIAA-CONTRACT-1", 
            FieldType.STRING, 10);
        acis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_2 = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_2", "ACIS-EOP-CL-TIAA-CONTRACT-2", 
            FieldType.STRING, 10);
        acis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_3 = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_3", "ACIS-EOP-CL-TIAA-CONTRACT-3", 
            FieldType.STRING, 10);
        acis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_4 = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_4", "ACIS-EOP-CL-TIAA-CONTRACT-4", 
            FieldType.STRING, 10);
        acis_Driver_Record_Acis_Eop_Cntr_Form_Numb = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Cntr_Form_Numb", "ACIS-EOP-CNTR-FORM-NUMB", 
            FieldType.STRING, 9);
        acis_Driver_Record_Acis_Eop_Cert_Form_Numb = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Cert_Form_Numb", "ACIS-EOP-CERT-FORM-NUMB", 
            FieldType.STRING, 9);
        acis_Driver_Record_Acis_Eop_Ed_Numb_T = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Ed_Numb_T", "ACIS-EOP-ED-NUMB-T", FieldType.STRING, 
            9);
        acis_Driver_Record_Acis_Eop_Ed_Numb_C = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Ed_Numb_C", "ACIS-EOP-ED-NUMB-C", FieldType.STRING, 
            9);
        acis_Driver_Record_Acis_Eop_Ira_Insert_Ind = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Ira_Insert_Ind", "ACIS-EOP-IRA-INSERT-IND", 
            FieldType.STRING, 1);
        acis_Driver_Record_Acis_Eop_Non_Proprietary_Pkg_Ind = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Non_Proprietary_Pkg_Ind", 
            "ACIS-EOP-NON-PROPRIETARY-PKG-IND", FieldType.STRING, 5);
        acis_Driver_Record_Acis_Dialog_Sequence = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Dialog_Sequence", "ACIS-DIALOG-SEQUENCE", 
            FieldType.NUMERIC, 15);
        acis_Driver_Record_Acis_Multi_Plan_Ind = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Multi_Plan_Ind", "ACIS-MULTI-PLAN-IND", FieldType.STRING, 
            1);
        acis_Driver_Record_Acis_Multi_Plan_Table = acis_Driver_Record.newFieldArrayInGroup("acis_Driver_Record_Acis_Multi_Plan_Table", "ACIS-MULTI-PLAN-TABLE", 
            FieldType.STRING, 12, new DbsArrayController(1,15));
        acis_Driver_Record_Acis_Multi_Plan_TableRedef2 = acis_Driver_Record.newGroupInGroup("acis_Driver_Record_Acis_Multi_Plan_TableRedef2", "Redefines", 
            acis_Driver_Record_Acis_Multi_Plan_Table);
        acis_Driver_Record_Acis_Multi_Plan_Info = acis_Driver_Record_Acis_Multi_Plan_TableRedef2.newGroupArrayInGroup("acis_Driver_Record_Acis_Multi_Plan_Info", 
            "ACIS-MULTI-PLAN-INFO", new DbsArrayController(1,15));
        acis_Driver_Record_Acis_Multi_Plan_No = acis_Driver_Record_Acis_Multi_Plan_Info.newFieldInGroup("acis_Driver_Record_Acis_Multi_Plan_No", "ACIS-MULTI-PLAN-NO", 
            FieldType.STRING, 6);
        acis_Driver_Record_Acis_Multi_Sub_Plan = acis_Driver_Record_Acis_Multi_Plan_Info.newFieldInGroup("acis_Driver_Record_Acis_Multi_Sub_Plan", "ACIS-MULTI-SUB-PLAN", 
            FieldType.STRING, 6);
        acis_Driver_Record_Acis_Eop_Tiaa_Init_Prem = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Tiaa_Init_Prem", "ACIS-EOP-TIAA-INIT-PREM", 
            FieldType.STRING, 11);
        acis_Driver_Record_Acis_Eop_Cref_Init_Prem = acis_Driver_Record.newFieldInGroup("acis_Driver_Record_Acis_Eop_Cref_Init_Prem", "ACIS-EOP-CREF-INIT-PREM", 
            FieldType.STRING, 11);
        acis_Driver_Record_Cis_Eop_Related_Contract_Info = acis_Driver_Record.newFieldArrayInGroup("acis_Driver_Record_Cis_Eop_Related_Contract_Info", 
            "CIS-EOP-RELATED-CONTRACT-INFO", FieldType.STRING, 49, new DbsArrayController(1,30));
        acis_Driver_Record_Cis_Eop_Related_Contract_InfoRedef3 = acis_Driver_Record.newGroupInGroup("acis_Driver_Record_Cis_Eop_Related_Contract_InfoRedef3", 
            "Redefines", acis_Driver_Record_Cis_Eop_Related_Contract_Info);
        acis_Driver_Record_Cis_Eop_Related_Contract = acis_Driver_Record_Cis_Eop_Related_Contract_InfoRedef3.newGroupArrayInGroup("acis_Driver_Record_Cis_Eop_Related_Contract", 
            "CIS-EOP-RELATED-CONTRACT", new DbsArrayController(1,30));
        acis_Driver_Record_Cis_Eop_Related_Contract_Type = acis_Driver_Record_Cis_Eop_Related_Contract.newFieldInGroup("acis_Driver_Record_Cis_Eop_Related_Contract_Type", 
            "CIS-EOP-RELATED-CONTRACT-TYPE", FieldType.STRING, 1);
        acis_Driver_Record_Cis_Eop_Related_Tiaa_No = acis_Driver_Record_Cis_Eop_Related_Contract.newFieldInGroup("acis_Driver_Record_Cis_Eop_Related_Tiaa_No", 
            "CIS-EOP-RELATED-TIAA-NO", FieldType.STRING, 10);
        acis_Driver_Record_Cis_Eop_Related_Tiaa_Issue_Date = acis_Driver_Record_Cis_Eop_Related_Contract.newFieldInGroup("acis_Driver_Record_Cis_Eop_Related_Tiaa_Issue_Date", 
            "CIS-EOP-RELATED-TIAA-ISSUE-DATE", FieldType.NUMERIC, 8);
        acis_Driver_Record_Cis_Eop_Related_First_Paym_Date = acis_Driver_Record_Cis_Eop_Related_Contract.newFieldInGroup("acis_Driver_Record_Cis_Eop_Related_First_Paym_Date", 
            "CIS-EOP-RELATED-FIRST-PAYM-DATE", FieldType.NUMERIC, 8);
        acis_Driver_Record_Cis_Eop_Related_Tiaa_Total_Amt = acis_Driver_Record_Cis_Eop_Related_Contract.newFieldInGroup("acis_Driver_Record_Cis_Eop_Related_Tiaa_Total_Amt", 
            "CIS-EOP-RELATED-TIAA-TOTAL-AMT", FieldType.DECIMAL, 10,2);
        acis_Driver_Record_Cis_Eop_Related_Last_Paym_Date = acis_Driver_Record_Cis_Eop_Related_Contract.newFieldInGroup("acis_Driver_Record_Cis_Eop_Related_Last_Paym_Date", 
            "CIS-EOP-RELATED-LAST-PAYM-DATE", FieldType.NUMERIC, 8);
        acis_Driver_Record_Cis_Eop_Related_Payment_Freq = acis_Driver_Record_Cis_Eop_Related_Contract.newFieldInGroup("acis_Driver_Record_Cis_Eop_Related_Payment_Freq", 
            "CIS-EOP-RELATED-PAYMENT-FREQ", FieldType.STRING, 1);
        acis_Driver_Record_Cis_Eop_Related_Tiaa_Issue_State = acis_Driver_Record_Cis_Eop_Related_Contract.newFieldInGroup("acis_Driver_Record_Cis_Eop_Related_Tiaa_Issue_State", 
            "CIS-EOP-RELATED-TIAA-ISSUE-STATE", FieldType.STRING, 2);
        acis_Driver_Record_Cis_Eop_First_Tpa_Ipro_Ind = acis_Driver_Record_Cis_Eop_Related_Contract.newFieldInGroup("acis_Driver_Record_Cis_Eop_First_Tpa_Ipro_Ind", 
            "CIS-EOP-FIRST-TPA-IPRO-IND", FieldType.STRING, 1);

        this.setRecordName("LdaAppl1153");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAppl1153() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
