/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:51:02 PM
**        * FROM NATURAL LDA     : APPL185
************************************************************
**        * FILE NAME            : LdaAppl185.java
**        * CLASS NAME           : LdaAppl185
**        * INSTANCE NAME        : LdaAppl185
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl185 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Grm_Table;
    private DbsField pnd_Grm_Table_Pnd_Grm_Data;
    private DbsGroup pnd_Grm_Table_Pnd_Grm_DataRedef1;
    private DbsField pnd_Grm_Table_Pnd_Grm_State_Eff_Date;
    private DbsField pnd_Grm_Table_Pnd_Filler_1;
    private DbsField pnd_Grm_Table_Pnd_Grm_T_C_Ra_Form_Num;
    private DbsField pnd_Grm_Table_Pnd_Filler_2;
    private DbsField pnd_Grm_Table_Pnd_Grm_T_C_Sra_Form_Num;
    private DbsField pnd_Grm_Table_Pnd_Filler_3;

    public DbsGroup getPnd_Grm_Table() { return pnd_Grm_Table; }

    public DbsField getPnd_Grm_Table_Pnd_Grm_Data() { return pnd_Grm_Table_Pnd_Grm_Data; }

    public DbsGroup getPnd_Grm_Table_Pnd_Grm_DataRedef1() { return pnd_Grm_Table_Pnd_Grm_DataRedef1; }

    public DbsField getPnd_Grm_Table_Pnd_Grm_State_Eff_Date() { return pnd_Grm_Table_Pnd_Grm_State_Eff_Date; }

    public DbsField getPnd_Grm_Table_Pnd_Filler_1() { return pnd_Grm_Table_Pnd_Filler_1; }

    public DbsField getPnd_Grm_Table_Pnd_Grm_T_C_Ra_Form_Num() { return pnd_Grm_Table_Pnd_Grm_T_C_Ra_Form_Num; }

    public DbsField getPnd_Grm_Table_Pnd_Filler_2() { return pnd_Grm_Table_Pnd_Filler_2; }

    public DbsField getPnd_Grm_Table_Pnd_Grm_T_C_Sra_Form_Num() { return pnd_Grm_Table_Pnd_Grm_T_C_Sra_Form_Num; }

    public DbsField getPnd_Grm_Table_Pnd_Filler_3() { return pnd_Grm_Table_Pnd_Filler_3; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Grm_Table = newGroupArrayInRecord("pnd_Grm_Table", "#GRM-TABLE", new DbsArrayController(1,20));
        pnd_Grm_Table_Pnd_Grm_Data = pnd_Grm_Table.newFieldInGroup("pnd_Grm_Table_Pnd_Grm_Data", "#GRM-DATA", FieldType.STRING, 27);
        pnd_Grm_Table_Pnd_Grm_DataRedef1 = pnd_Grm_Table.newGroupInGroup("pnd_Grm_Table_Pnd_Grm_DataRedef1", "Redefines", pnd_Grm_Table_Pnd_Grm_Data);
        pnd_Grm_Table_Pnd_Grm_State_Eff_Date = pnd_Grm_Table_Pnd_Grm_DataRedef1.newFieldInGroup("pnd_Grm_Table_Pnd_Grm_State_Eff_Date", "#GRM-STATE-EFF-DATE", 
            FieldType.NUMERIC, 6);
        pnd_Grm_Table_Pnd_Filler_1 = pnd_Grm_Table_Pnd_Grm_DataRedef1.newFieldInGroup("pnd_Grm_Table_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 1);
        pnd_Grm_Table_Pnd_Grm_T_C_Ra_Form_Num = pnd_Grm_Table_Pnd_Grm_DataRedef1.newFieldInGroup("pnd_Grm_Table_Pnd_Grm_T_C_Ra_Form_Num", "#GRM-T-C-RA-FORM-NUM", 
            FieldType.STRING, 9);
        pnd_Grm_Table_Pnd_Filler_2 = pnd_Grm_Table_Pnd_Grm_DataRedef1.newFieldInGroup("pnd_Grm_Table_Pnd_Filler_2", "#FILLER-2", FieldType.STRING, 1);
        pnd_Grm_Table_Pnd_Grm_T_C_Sra_Form_Num = pnd_Grm_Table_Pnd_Grm_DataRedef1.newFieldInGroup("pnd_Grm_Table_Pnd_Grm_T_C_Sra_Form_Num", "#GRM-T-C-SRA-FORM-NUM", 
            FieldType.STRING, 9);
        pnd_Grm_Table_Pnd_Filler_3 = pnd_Grm_Table_Pnd_Grm_DataRedef1.newFieldInGroup("pnd_Grm_Table_Pnd_Filler_3", "#FILLER-3", FieldType.STRING, 1);

        this.setRecordName("LdaAppl185");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Grm_Table_Pnd_Grm_Data.getValue(1).setInitialValue("190001,1000.17  ,1200     ,");
        pnd_Grm_Table_Pnd_Grm_Data.getValue(2).setInitialValue("197702,1000.18  ,1200.1   ,");
        pnd_Grm_Table_Pnd_Grm_Data.getValue(3).setInitialValue("197812,1000.19  ,1200.2   ,");
        pnd_Grm_Table_Pnd_Grm_Data.getValue(4).setInitialValue("198112,1000.20  ,1200.3   ,");
        pnd_Grm_Table_Pnd_Grm_Data.getValue(5).setInitialValue("198312,1000.21  ,1200.4   ,");
        pnd_Grm_Table_Pnd_Grm_Data.getValue(6).setInitialValue("198412,1000.22  ,1200.5   ,");
        pnd_Grm_Table_Pnd_Grm_Data.getValue(7).setInitialValue("198512,1000.23  ,1200.6   ,");
        pnd_Grm_Table_Pnd_Grm_Data.getValue(8).setInitialValue("198712,1000.23  ,1200.7   ,");
        pnd_Grm_Table_Pnd_Grm_Data.getValue(11).setInitialValue("190001,C1000.6  ,C1200    ,");
        pnd_Grm_Table_Pnd_Grm_Data.getValue(12).setInitialValue("197309,C1000.6  ,C1200    ,");
        pnd_Grm_Table_Pnd_Grm_Data.getValue(13).setInitialValue("197702,C1000.7  ,C1200.1  ,");
        pnd_Grm_Table_Pnd_Grm_Data.getValue(14).setInitialValue("197812,C1000.7  ,C1200.1  ,");
        pnd_Grm_Table_Pnd_Grm_Data.getValue(15).setInitialValue("198112,C1000.8  ,C1200.2  ,");
        pnd_Grm_Table_Pnd_Grm_Data.getValue(16).setInitialValue("198307,C1000.9  ,C1200.3  ,");
        pnd_Grm_Table_Pnd_Grm_Data.getValue(17).setInitialValue("198412,C1000.10 ,C1200.4  ,");
        pnd_Grm_Table_Pnd_Grm_Data.getValue(18).setInitialValue("198702,C1000.10 ,C1200.4  ,");
    }

    // Constructor
    public LdaAppl185() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
