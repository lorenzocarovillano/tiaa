/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:17:24 PM
**        * FROM NATURAL PDA     : ACIA3570
************************************************************
**        * FILE NAME            : PdaAcia3570.java
**        * CLASS NAME           : PdaAcia3570
**        * INSTANCE NAME        : PdaAcia3570
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaAcia3570 extends PdaBase
{
    // Properties
    private DbsGroup acia3570_Parameter;
    private DbsField acia3570_Parameter_Pnd_P_Get_All_Buckets_Sw;
    private DbsField acia3570_Parameter_Pnd_P_Iis_Premium_Team_Cde;
    private DbsField acia3570_Parameter_Pnd_P_Entry_Dscrptn_Txt;
    private DbsGroup acia3570_Parameter_Pnd_P_Entry_Dscrptn_TxtRedef1;
    private DbsField acia3570_Parameter_Pnd_P_Bucket_Dsc;
    private DbsField acia3570_Parameter_Pnd_P_Accum_Dsc;
    private DbsField acia3570_Parameter_Pnd_P_Team_Cde;
    private DbsField acia3570_Parameter_Pnd_P_Filler;
    private DbsGroup acia3570_Parameter_Pnd_P_Bucket_Accum_Array;
    private DbsField acia3570_Parameter_Pnd_P_Bucket_Accum;
    private DbsGroup acia3570_Parameter_Pnd_P_Bucket_AccumRedef2;
    private DbsField acia3570_Parameter_Pnd_P_Bucket;
    private DbsField acia3570_Parameter_Pnd_P_Accum;
    private DbsField acia3570_Parameter_Pnd_P_Addtnl_Dscrptn_Txt;
    private DbsGroup acia3570_Parameter_Pnd_P_Addtnl_Dscrptn_TxtRedef3;
    private DbsField acia3570_Parameter_Pnd_P_Addtnl_Short_Txt;
    private DbsField acia3570_Parameter_Pnd_P_Addtnl_Filler;
    private DbsField acia3570_Parameter_Pnd_P_Addtnl_Long_Txt;

    public DbsGroup getAcia3570_Parameter() { return acia3570_Parameter; }

    public DbsField getAcia3570_Parameter_Pnd_P_Get_All_Buckets_Sw() { return acia3570_Parameter_Pnd_P_Get_All_Buckets_Sw; }

    public DbsField getAcia3570_Parameter_Pnd_P_Iis_Premium_Team_Cde() { return acia3570_Parameter_Pnd_P_Iis_Premium_Team_Cde; }

    public DbsField getAcia3570_Parameter_Pnd_P_Entry_Dscrptn_Txt() { return acia3570_Parameter_Pnd_P_Entry_Dscrptn_Txt; }

    public DbsGroup getAcia3570_Parameter_Pnd_P_Entry_Dscrptn_TxtRedef1() { return acia3570_Parameter_Pnd_P_Entry_Dscrptn_TxtRedef1; }

    public DbsField getAcia3570_Parameter_Pnd_P_Bucket_Dsc() { return acia3570_Parameter_Pnd_P_Bucket_Dsc; }

    public DbsField getAcia3570_Parameter_Pnd_P_Accum_Dsc() { return acia3570_Parameter_Pnd_P_Accum_Dsc; }

    public DbsField getAcia3570_Parameter_Pnd_P_Team_Cde() { return acia3570_Parameter_Pnd_P_Team_Cde; }

    public DbsField getAcia3570_Parameter_Pnd_P_Filler() { return acia3570_Parameter_Pnd_P_Filler; }

    public DbsGroup getAcia3570_Parameter_Pnd_P_Bucket_Accum_Array() { return acia3570_Parameter_Pnd_P_Bucket_Accum_Array; }

    public DbsField getAcia3570_Parameter_Pnd_P_Bucket_Accum() { return acia3570_Parameter_Pnd_P_Bucket_Accum; }

    public DbsGroup getAcia3570_Parameter_Pnd_P_Bucket_AccumRedef2() { return acia3570_Parameter_Pnd_P_Bucket_AccumRedef2; }

    public DbsField getAcia3570_Parameter_Pnd_P_Bucket() { return acia3570_Parameter_Pnd_P_Bucket; }

    public DbsField getAcia3570_Parameter_Pnd_P_Accum() { return acia3570_Parameter_Pnd_P_Accum; }

    public DbsField getAcia3570_Parameter_Pnd_P_Addtnl_Dscrptn_Txt() { return acia3570_Parameter_Pnd_P_Addtnl_Dscrptn_Txt; }

    public DbsGroup getAcia3570_Parameter_Pnd_P_Addtnl_Dscrptn_TxtRedef3() { return acia3570_Parameter_Pnd_P_Addtnl_Dscrptn_TxtRedef3; }

    public DbsField getAcia3570_Parameter_Pnd_P_Addtnl_Short_Txt() { return acia3570_Parameter_Pnd_P_Addtnl_Short_Txt; }

    public DbsField getAcia3570_Parameter_Pnd_P_Addtnl_Filler() { return acia3570_Parameter_Pnd_P_Addtnl_Filler; }

    public DbsField getAcia3570_Parameter_Pnd_P_Addtnl_Long_Txt() { return acia3570_Parameter_Pnd_P_Addtnl_Long_Txt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        acia3570_Parameter = dbsRecord.newGroupInRecord("acia3570_Parameter", "ACIA3570-PARAMETER");
        acia3570_Parameter.setParameterOption(ParameterOption.ByReference);
        acia3570_Parameter_Pnd_P_Get_All_Buckets_Sw = acia3570_Parameter.newFieldInGroup("acia3570_Parameter_Pnd_P_Get_All_Buckets_Sw", "#P-GET-ALL-BUCKETS-SW", 
            FieldType.STRING, 1);
        acia3570_Parameter_Pnd_P_Iis_Premium_Team_Cde = acia3570_Parameter.newFieldInGroup("acia3570_Parameter_Pnd_P_Iis_Premium_Team_Cde", "#P-IIS-PREMIUM-TEAM-CDE", 
            FieldType.STRING, 8);
        acia3570_Parameter_Pnd_P_Entry_Dscrptn_Txt = acia3570_Parameter.newFieldInGroup("acia3570_Parameter_Pnd_P_Entry_Dscrptn_Txt", "#P-ENTRY-DSCRPTN-TXT", 
            FieldType.STRING, 60);
        acia3570_Parameter_Pnd_P_Entry_Dscrptn_TxtRedef1 = acia3570_Parameter.newGroupInGroup("acia3570_Parameter_Pnd_P_Entry_Dscrptn_TxtRedef1", "Redefines", 
            acia3570_Parameter_Pnd_P_Entry_Dscrptn_Txt);
        acia3570_Parameter_Pnd_P_Bucket_Dsc = acia3570_Parameter_Pnd_P_Entry_Dscrptn_TxtRedef1.newFieldInGroup("acia3570_Parameter_Pnd_P_Bucket_Dsc", 
            "#P-BUCKET-DSC", FieldType.STRING, 2);
        acia3570_Parameter_Pnd_P_Accum_Dsc = acia3570_Parameter_Pnd_P_Entry_Dscrptn_TxtRedef1.newFieldInGroup("acia3570_Parameter_Pnd_P_Accum_Dsc", "#P-ACCUM-DSC", 
            FieldType.STRING, 2);
        acia3570_Parameter_Pnd_P_Team_Cde = acia3570_Parameter_Pnd_P_Entry_Dscrptn_TxtRedef1.newFieldInGroup("acia3570_Parameter_Pnd_P_Team_Cde", "#P-TEAM-CDE", 
            FieldType.STRING, 2);
        acia3570_Parameter_Pnd_P_Filler = acia3570_Parameter_Pnd_P_Entry_Dscrptn_TxtRedef1.newFieldInGroup("acia3570_Parameter_Pnd_P_Filler", "#P-FILLER", 
            FieldType.STRING, 54);
        acia3570_Parameter_Pnd_P_Bucket_Accum_Array = acia3570_Parameter.newGroupArrayInGroup("acia3570_Parameter_Pnd_P_Bucket_Accum_Array", "#P-BUCKET-ACCUM-ARRAY", 
            new DbsArrayController(1,99));
        acia3570_Parameter_Pnd_P_Bucket_Accum = acia3570_Parameter_Pnd_P_Bucket_Accum_Array.newFieldInGroup("acia3570_Parameter_Pnd_P_Bucket_Accum", "#P-BUCKET-ACCUM", 
            FieldType.STRING, 4);
        acia3570_Parameter_Pnd_P_Bucket_AccumRedef2 = acia3570_Parameter_Pnd_P_Bucket_Accum_Array.newGroupInGroup("acia3570_Parameter_Pnd_P_Bucket_AccumRedef2", 
            "Redefines", acia3570_Parameter_Pnd_P_Bucket_Accum);
        acia3570_Parameter_Pnd_P_Bucket = acia3570_Parameter_Pnd_P_Bucket_AccumRedef2.newFieldInGroup("acia3570_Parameter_Pnd_P_Bucket", "#P-BUCKET", 
            FieldType.STRING, 2);
        acia3570_Parameter_Pnd_P_Accum = acia3570_Parameter_Pnd_P_Bucket_AccumRedef2.newFieldInGroup("acia3570_Parameter_Pnd_P_Accum", "#P-ACCUM", FieldType.STRING, 
            2);
        acia3570_Parameter_Pnd_P_Addtnl_Dscrptn_Txt = acia3570_Parameter.newFieldInGroup("acia3570_Parameter_Pnd_P_Addtnl_Dscrptn_Txt", "#P-ADDTNL-DSCRPTN-TXT", 
            FieldType.STRING, 60);
        acia3570_Parameter_Pnd_P_Addtnl_Dscrptn_TxtRedef3 = acia3570_Parameter.newGroupInGroup("acia3570_Parameter_Pnd_P_Addtnl_Dscrptn_TxtRedef3", "Redefines", 
            acia3570_Parameter_Pnd_P_Addtnl_Dscrptn_Txt);
        acia3570_Parameter_Pnd_P_Addtnl_Short_Txt = acia3570_Parameter_Pnd_P_Addtnl_Dscrptn_TxtRedef3.newFieldInGroup("acia3570_Parameter_Pnd_P_Addtnl_Short_Txt", 
            "#P-ADDTNL-SHORT-TXT", FieldType.STRING, 7);
        acia3570_Parameter_Pnd_P_Addtnl_Filler = acia3570_Parameter_Pnd_P_Addtnl_Dscrptn_TxtRedef3.newFieldInGroup("acia3570_Parameter_Pnd_P_Addtnl_Filler", 
            "#P-ADDTNL-FILLER", FieldType.STRING, 1);
        acia3570_Parameter_Pnd_P_Addtnl_Long_Txt = acia3570_Parameter_Pnd_P_Addtnl_Dscrptn_TxtRedef3.newFieldInGroup("acia3570_Parameter_Pnd_P_Addtnl_Long_Txt", 
            "#P-ADDTNL-LONG-TXT", FieldType.STRING, 52);

        dbsRecord.reset();
    }

    // Constructors
    public PdaAcia3570(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

