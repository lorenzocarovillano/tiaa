/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:17:47 PM
**        * FROM NATURAL PDA     : APPA010
************************************************************
**        * FILE NAME            : PdaAppa010.java
**        * CLASS NAME           : PdaAppa010
**        * INSTANCE NAME        : PdaAppa010
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaAppa010 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Parameter;
    private DbsField pnd_Parameter_Pnd_Start_Date;
    private DbsField pnd_Parameter_Pnd_Requestor;
    private DbsField pnd_Parameter_Pnd_Accum_Type;
    private DbsField pnd_Parameter_Pnd_Get_Contract_Flag;
    private DbsField pnd_Parameter_Pnd_Add_To_Buckets_Flag;
    private DbsField pnd_Parameter_Pnd_Add_To_Buckets_Message;
    private DbsField pnd_Parameter_Pnd_Other_Activity_Message;
    private DbsField pnd_Parameter_Pnd_Get_Contract_Message;
    private DbsField pnd_Parameter_Pnd_Prod_Id;
    private DbsField pnd_Parameter_Pnd_Contr_Gotten_T;
    private DbsGroup pnd_Parameter_Pnd_Contr_Gotten_TRedef1;
    private DbsField pnd_Parameter_Pnd_Contr_Gotten_T_Pref;
    private DbsField pnd_Parameter_Pnd_Contr_Gotten_T_Numb;
    private DbsField pnd_Parameter_Pnd_Contr_Gotten_T_Chek;
    private DbsField pnd_Parameter_Pnd_Contr_Gotten_C;
    private DbsGroup pnd_Parameter_Pnd_Contr_Gotten_CRedef2;
    private DbsField pnd_Parameter_Pnd_Contr_Gotten_C_Pref;
    private DbsField pnd_Parameter_Pnd_Contr_Gotten_C_Numb;
    private DbsField pnd_Parameter_Pnd_Contr_Gotten_C_Chek;
    private DbsGroup pnd_Parameter_Pnd_Buckets_To_Tally;
    private DbsField pnd_Parameter_Pnd_Each_Bucket_Name;
    private DbsField pnd_Parameter_Pnd_Each_Tally_Value;
    private DbsField pnd_Parameter_Pnd_P_Bucket_Code;
    private DbsField pnd_Parameter_Pnd_P_Accum_Code;

    public DbsGroup getPnd_Parameter() { return pnd_Parameter; }

    public DbsField getPnd_Parameter_Pnd_Start_Date() { return pnd_Parameter_Pnd_Start_Date; }

    public DbsField getPnd_Parameter_Pnd_Requestor() { return pnd_Parameter_Pnd_Requestor; }

    public DbsField getPnd_Parameter_Pnd_Accum_Type() { return pnd_Parameter_Pnd_Accum_Type; }

    public DbsField getPnd_Parameter_Pnd_Get_Contract_Flag() { return pnd_Parameter_Pnd_Get_Contract_Flag; }

    public DbsField getPnd_Parameter_Pnd_Add_To_Buckets_Flag() { return pnd_Parameter_Pnd_Add_To_Buckets_Flag; }

    public DbsField getPnd_Parameter_Pnd_Add_To_Buckets_Message() { return pnd_Parameter_Pnd_Add_To_Buckets_Message; }

    public DbsField getPnd_Parameter_Pnd_Other_Activity_Message() { return pnd_Parameter_Pnd_Other_Activity_Message; }

    public DbsField getPnd_Parameter_Pnd_Get_Contract_Message() { return pnd_Parameter_Pnd_Get_Contract_Message; }

    public DbsField getPnd_Parameter_Pnd_Prod_Id() { return pnd_Parameter_Pnd_Prod_Id; }

    public DbsField getPnd_Parameter_Pnd_Contr_Gotten_T() { return pnd_Parameter_Pnd_Contr_Gotten_T; }

    public DbsGroup getPnd_Parameter_Pnd_Contr_Gotten_TRedef1() { return pnd_Parameter_Pnd_Contr_Gotten_TRedef1; }

    public DbsField getPnd_Parameter_Pnd_Contr_Gotten_T_Pref() { return pnd_Parameter_Pnd_Contr_Gotten_T_Pref; }

    public DbsField getPnd_Parameter_Pnd_Contr_Gotten_T_Numb() { return pnd_Parameter_Pnd_Contr_Gotten_T_Numb; }

    public DbsField getPnd_Parameter_Pnd_Contr_Gotten_T_Chek() { return pnd_Parameter_Pnd_Contr_Gotten_T_Chek; }

    public DbsField getPnd_Parameter_Pnd_Contr_Gotten_C() { return pnd_Parameter_Pnd_Contr_Gotten_C; }

    public DbsGroup getPnd_Parameter_Pnd_Contr_Gotten_CRedef2() { return pnd_Parameter_Pnd_Contr_Gotten_CRedef2; }

    public DbsField getPnd_Parameter_Pnd_Contr_Gotten_C_Pref() { return pnd_Parameter_Pnd_Contr_Gotten_C_Pref; }

    public DbsField getPnd_Parameter_Pnd_Contr_Gotten_C_Numb() { return pnd_Parameter_Pnd_Contr_Gotten_C_Numb; }

    public DbsField getPnd_Parameter_Pnd_Contr_Gotten_C_Chek() { return pnd_Parameter_Pnd_Contr_Gotten_C_Chek; }

    public DbsGroup getPnd_Parameter_Pnd_Buckets_To_Tally() { return pnd_Parameter_Pnd_Buckets_To_Tally; }

    public DbsField getPnd_Parameter_Pnd_Each_Bucket_Name() { return pnd_Parameter_Pnd_Each_Bucket_Name; }

    public DbsField getPnd_Parameter_Pnd_Each_Tally_Value() { return pnd_Parameter_Pnd_Each_Tally_Value; }

    public DbsField getPnd_Parameter_Pnd_P_Bucket_Code() { return pnd_Parameter_Pnd_P_Bucket_Code; }

    public DbsField getPnd_Parameter_Pnd_P_Accum_Code() { return pnd_Parameter_Pnd_P_Accum_Code; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Parameter = dbsRecord.newGroupInRecord("pnd_Parameter", "#PARAMETER");
        pnd_Parameter.setParameterOption(ParameterOption.ByReference);
        pnd_Parameter_Pnd_Start_Date = pnd_Parameter.newFieldInGroup("pnd_Parameter_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);
        pnd_Parameter_Pnd_Requestor = pnd_Parameter.newFieldInGroup("pnd_Parameter_Pnd_Requestor", "#REQUESTOR", FieldType.STRING, 12);
        pnd_Parameter_Pnd_Accum_Type = pnd_Parameter.newFieldInGroup("pnd_Parameter_Pnd_Accum_Type", "#ACCUM-TYPE", FieldType.STRING, 2);
        pnd_Parameter_Pnd_Get_Contract_Flag = pnd_Parameter.newFieldInGroup("pnd_Parameter_Pnd_Get_Contract_Flag", "#GET-CONTRACT-FLAG", FieldType.STRING, 
            15);
        pnd_Parameter_Pnd_Add_To_Buckets_Flag = pnd_Parameter.newFieldInGroup("pnd_Parameter_Pnd_Add_To_Buckets_Flag", "#ADD-TO-BUCKETS-FLAG", FieldType.STRING, 
            15);
        pnd_Parameter_Pnd_Add_To_Buckets_Message = pnd_Parameter.newFieldInGroup("pnd_Parameter_Pnd_Add_To_Buckets_Message", "#ADD-TO-BUCKETS-MESSAGE", 
            FieldType.STRING, 25);
        pnd_Parameter_Pnd_Other_Activity_Message = pnd_Parameter.newFieldInGroup("pnd_Parameter_Pnd_Other_Activity_Message", "#OTHER-ACTIVITY-MESSAGE", 
            FieldType.STRING, 25);
        pnd_Parameter_Pnd_Get_Contract_Message = pnd_Parameter.newFieldInGroup("pnd_Parameter_Pnd_Get_Contract_Message", "#GET-CONTRACT-MESSAGE", FieldType.STRING, 
            25);
        pnd_Parameter_Pnd_Prod_Id = pnd_Parameter.newFieldInGroup("pnd_Parameter_Pnd_Prod_Id", "#PROD-ID", FieldType.STRING, 4);
        pnd_Parameter_Pnd_Contr_Gotten_T = pnd_Parameter.newFieldInGroup("pnd_Parameter_Pnd_Contr_Gotten_T", "#CONTR-GOTTEN-T", FieldType.STRING, 10);
        pnd_Parameter_Pnd_Contr_Gotten_TRedef1 = pnd_Parameter.newGroupInGroup("pnd_Parameter_Pnd_Contr_Gotten_TRedef1", "Redefines", pnd_Parameter_Pnd_Contr_Gotten_T);
        pnd_Parameter_Pnd_Contr_Gotten_T_Pref = pnd_Parameter_Pnd_Contr_Gotten_TRedef1.newFieldInGroup("pnd_Parameter_Pnd_Contr_Gotten_T_Pref", "#CONTR-GOTTEN-T-PREF", 
            FieldType.STRING, 1);
        pnd_Parameter_Pnd_Contr_Gotten_T_Numb = pnd_Parameter_Pnd_Contr_Gotten_TRedef1.newFieldInGroup("pnd_Parameter_Pnd_Contr_Gotten_T_Numb", "#CONTR-GOTTEN-T-NUMB", 
            FieldType.STRING, 6);
        pnd_Parameter_Pnd_Contr_Gotten_T_Chek = pnd_Parameter_Pnd_Contr_Gotten_TRedef1.newFieldInGroup("pnd_Parameter_Pnd_Contr_Gotten_T_Chek", "#CONTR-GOTTEN-T-CHEK", 
            FieldType.NUMERIC, 1);
        pnd_Parameter_Pnd_Contr_Gotten_C = pnd_Parameter.newFieldInGroup("pnd_Parameter_Pnd_Contr_Gotten_C", "#CONTR-GOTTEN-C", FieldType.STRING, 10);
        pnd_Parameter_Pnd_Contr_Gotten_CRedef2 = pnd_Parameter.newGroupInGroup("pnd_Parameter_Pnd_Contr_Gotten_CRedef2", "Redefines", pnd_Parameter_Pnd_Contr_Gotten_C);
        pnd_Parameter_Pnd_Contr_Gotten_C_Pref = pnd_Parameter_Pnd_Contr_Gotten_CRedef2.newFieldInGroup("pnd_Parameter_Pnd_Contr_Gotten_C_Pref", "#CONTR-GOTTEN-C-PREF", 
            FieldType.STRING, 1);
        pnd_Parameter_Pnd_Contr_Gotten_C_Numb = pnd_Parameter_Pnd_Contr_Gotten_CRedef2.newFieldInGroup("pnd_Parameter_Pnd_Contr_Gotten_C_Numb", "#CONTR-GOTTEN-C-NUMB", 
            FieldType.STRING, 6);
        pnd_Parameter_Pnd_Contr_Gotten_C_Chek = pnd_Parameter_Pnd_Contr_Gotten_CRedef2.newFieldInGroup("pnd_Parameter_Pnd_Contr_Gotten_C_Chek", "#CONTR-GOTTEN-C-CHEK", 
            FieldType.NUMERIC, 1);
        pnd_Parameter_Pnd_Buckets_To_Tally = pnd_Parameter.newGroupArrayInGroup("pnd_Parameter_Pnd_Buckets_To_Tally", "#BUCKETS-TO-TALLY", new DbsArrayController(1,
            99));
        pnd_Parameter_Pnd_Each_Bucket_Name = pnd_Parameter_Pnd_Buckets_To_Tally.newFieldInGroup("pnd_Parameter_Pnd_Each_Bucket_Name", "#EACH-BUCKET-NAME", 
            FieldType.STRING, 15);
        pnd_Parameter_Pnd_Each_Tally_Value = pnd_Parameter_Pnd_Buckets_To_Tally.newFieldInGroup("pnd_Parameter_Pnd_Each_Tally_Value", "#EACH-TALLY-VALUE", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Parameter_Pnd_P_Bucket_Code = pnd_Parameter.newFieldInGroup("pnd_Parameter_Pnd_P_Bucket_Code", "#P-BUCKET-CODE", FieldType.STRING, 2);
        pnd_Parameter_Pnd_P_Accum_Code = pnd_Parameter.newFieldInGroup("pnd_Parameter_Pnd_P_Accum_Code", "#P-ACCUM-CODE", FieldType.STRING, 2);

        dbsRecord.reset();
    }

    // Constructors
    public PdaAppa010(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

