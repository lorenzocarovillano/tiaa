/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:52 PM
**        * FROM NATURAL LDA     : APPL1158
************************************************************
**        * FILE NAME            : LdaAppl1158.java
**        * CLASS NAME           : LdaAppl1158
**        * INSTANCE NAME        : LdaAppl1158
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl1158 extends DbsRecord
{
    // Properties
    private DbsGroup record_Id_Ex_File_3;
    private DbsField record_Id_Ex_File_3_Record_Id;
    private DbsGroup record_Id_Ex_File_3_Record_IdRedef1;
    private DbsField record_Id_Ex_File_3_Hd_Record_Type;
    private DbsField record_Id_Ex_File_3_Hd_Output_Profile;
    private DbsField record_Id_Ex_File_3_Rt_Print_Package_Type;
    private DbsField record_Id_Ex_File_3_Rt_Reprint_Request_Type;
    private DbsField record_Id_Ex_File_3_Hd_Product_Type;
    private DbsField record_Id_Ex_File_3_Rt_Type_Cd;
    private DbsField record_Id_Ex_File_3_Rt_Tiaa_No_1;
    private DbsField record_Id_Ex_File_3_Rt_Tiaa_Fill_1;
    private DbsField record_Id_Ex_File_3_Rt_Cref_No_1;
    private DbsField record_Id_Ex_File_3_Rt_Cref_Fill_1;
    private DbsField record_Id_Ex_File_3_Rt_Tiaa_Cntr_2;
    private DbsGroup record_Id_Ex_File_3_Rt_Tiaa_Cntr_2Redef2;
    private DbsField record_Id_Ex_File_3_Rt_Tiaa_No_2;
    private DbsField record_Id_Ex_File_3_Rt_Tiaa_Fill_2;
    private DbsField record_Id_Ex_File_3_Rt_Cref_Cert_2;
    private DbsGroup record_Id_Ex_File_3_Rt_Cref_Cert_2Redef3;
    private DbsField record_Id_Ex_File_3_Rt_Cref_No_2;
    private DbsField record_Id_Ex_File_3_Rt_Cref_Fill_2;
    private DbsField record_Id_Ex_File_3_Rt_Tiaa_Cntr_3;
    private DbsGroup record_Id_Ex_File_3_Rt_Tiaa_Cntr_3Redef4;
    private DbsField record_Id_Ex_File_3_Rt_Tiaa_No_3;
    private DbsField record_Id_Ex_File_3_Rt_Tiaa_Fill_3;
    private DbsField record_Id_Ex_File_3_Rt_Cref_Cert_3;
    private DbsGroup record_Id_Ex_File_3_Rt_Cref_Cert_3Redef5;
    private DbsField record_Id_Ex_File_3_Rt_Cref_No_3;
    private DbsField record_Id_Ex_File_3_Rt_Cref_Fill_3;
    private DbsField record_Id_Ex_File_3_Rt_Tiaa_Cntr_4;
    private DbsGroup record_Id_Ex_File_3_Rt_Tiaa_Cntr_4Redef6;
    private DbsField record_Id_Ex_File_3_Rt_Tiaa_No_4;
    private DbsField record_Id_Ex_File_3_Rt_Tiaa_Fill_4;
    private DbsField record_Id_Ex_File_3_Rt_Cref_Cert_4;
    private DbsGroup record_Id_Ex_File_3_Rt_Cref_Cert_4Redef7;
    private DbsField record_Id_Ex_File_3_Rt_Cref_No_4;
    private DbsField record_Id_Ex_File_3_Rt_Cref_Fill_4;
    private DbsField record_Id_Ex_File_3_Rt_Date_Table;
    private DbsField record_Id_Ex_File_3_Rt_Age_First_Pymt;
    private DbsField record_Id_Ex_File_3_Rt_Ownership;
    private DbsField record_Id_Ex_File_3_Rt_Alloc_Disc;
    private DbsField record_Id_Ex_File_3_Rt_Currency;
    private DbsField record_Id_Ex_File_3_Rt_Tiaa_Extr_Form;
    private DbsField record_Id_Ex_File_3_Rt_Cref_Extr_Form;
    private DbsField record_Id_Ex_File_3_Rt_Tiaa_Extr_Ed;
    private DbsField record_Id_Ex_File_3_Rt_Cref_Extr_Ed;
    private DbsField record_Id_Ex_File_3_Rt_Annuitant_Name;
    private DbsGroup record_Id_Ex_File_3_Rt_Eop_Name;
    private DbsField record_Id_Ex_File_3_Rt_Eop_Last_Name;
    private DbsField record_Id_Ex_File_3_Rt_Eop_First_Name;
    private DbsField record_Id_Ex_File_3_Rt_Eop_Middle_Name;
    private DbsGroup record_Id_Ex_File_3_Rt_Eop_Middle_NameRedef8;
    private DbsField record_Id_Ex_File_3_Rt_Eop_Mid_Init;
    private DbsField record_Id_Ex_File_3_Rt_Eop_Mid_Filler;
    private DbsField record_Id_Ex_File_3_Rt_Eop_Title;
    private DbsField record_Id_Ex_File_3_Rt_Eop_Prefix;
    private DbsField record_Id_Ex_File_3_Rt_Soc_Sec;
    private DbsField record_Id_Ex_File_3_Rt_Pin_No;
    private DbsField record_Id_Ex_File_3_Rt_Address_Table;
    private DbsField record_Id_Ex_File_3_Rt_Eop_Zipcode;
    private DbsField record_Id_Ex_File_3_Rt_Participant_Status;
    private DbsField record_Id_Ex_File_3_Rt_Email_Address;
    private DbsField record_Id_Ex_File_3_Rt_Request_Package_Type;
    private DbsField record_Id_Ex_File_3_Rt_Portfolio_Selected;
    private DbsField record_Id_Ex_File_3_Rt_Divorce_Contract;
    private DbsField record_Id_Ex_File_3_Rt_Access_Cd_Ind;
    private DbsField record_Id_Ex_File_3_Rt_Address_Change;
    private DbsField record_Id_Ex_File_3_Rt_Package_Version;
    private DbsField record_Id_Ex_File_3_Rt_Unit_Name;
    private DbsField record_Id_Ex_File_3_Rt_Future_Filler;
    private DbsField record_Id_Ex_File_3_Rt_Bene_Estate;
    private DbsField record_Id_Ex_File_3_Rt_Bene_Trust;
    private DbsField record_Id_Ex_File_3_Rt_Bene_Category;
    private DbsField record_Id_Ex_File_3_Rt_Sex;
    private DbsField record_Id_Ex_File_3_Rt_Inst_Name_Table;
    private DbsField record_Id_Ex_File_3_Rt_Inst_Name;
    private DbsField record_Id_Ex_File_3_Rt_Inst_Address_Table;
    private DbsField record_Id_Ex_File_3_Rt_Inst_Zip_Cd;
    private DbsField record_Id_Ex_File_3_Rt_Mail_Instructions;
    private DbsField record_Id_Ex_File_3_Rt_Orig_Iss_State;
    private DbsField record_Id_Ex_File_3_Rt_Current_Iss_State;
    private DbsField record_Id_Ex_File_3_Rt_Eop_State_Name;
    private DbsField record_Id_Ex_File_3_Rt_Lob;
    private DbsField record_Id_Ex_File_3_Rt_Lob_Type;
    private DbsField record_Id_Ex_File_3_Rt_Inst_Code;
    private DbsField record_Id_Ex_File_3_Rt_Inst_Bill_Cd;
    private DbsField record_Id_Ex_File_3_Rt_Inst_Permit_Trans;
    private DbsField record_Id_Ex_File_3_Rt_Perm_Gra_Trans;
    private DbsField record_Id_Ex_File_3_Rt_Inst_Full_Immed_Vest;
    private DbsField record_Id_Ex_File_3_Rt_Inst_Cashable_Ra;
    private DbsField record_Id_Ex_File_3_Rt_Inst_Cashable_Gra;
    private DbsField record_Id_Ex_File_3_Rt_Inst_Fixed_Per_Opt;
    private DbsField record_Id_Ex_File_3_Rt_Inst_Cntrl_Consent;
    private DbsField record_Id_Ex_File_3_Rt_Eop;
    private DbsField record_Id_Ex_File_3_Rt_Gsra_Inst_St_Code;
    private DbsField record_Id_Ex_File_3_Rt_Region_Cd;
    private DbsGroup record_Id_Ex_File_3_Rt_Region_CdRedef9;
    private DbsField record_Id_Ex_File_3_Rt_Region;
    private DbsField record_Id_Ex_File_3_Rt_Branch;
    private DbsField record_Id_Ex_File_3_Rt_Need;
    private DbsField record_Id_Ex_File_3_Rt_Staff_Id;
    private DbsField record_Id_Ex_File_3_Rt_Appl_Source;
    private DbsField record_Id_Ex_File_3_Rt_Eop_Appl_Status;
    private DbsField record_Id_Ex_File_3_Rt_Eop_Addl_Cref_Req;
    private DbsField record_Id_Ex_File_3_Rt_Extract_Date;
    private DbsField record_Id_Ex_File_3_Rt_Gsra_Loan_Ind;
    private DbsField record_Id_Ex_File_3_Rt_Spec_Ppg_Ind;
    private DbsField record_Id_Ex_File_3_Rt_Mail_Pull_Cd;
    private DbsField record_Id_Ex_File_3_Rt_Mit_Log_Dt_Tm;
    private DbsField record_Id_Ex_File_3_Rt_Addr_Page_Name;
    private DbsGroup record_Id_Ex_File_3_Rt_Addr_Page_NameRedef10;
    private DbsField record_Id_Ex_File_3_Rt_Addr_Pref;
    private DbsField record_Id_Ex_File_3_Rt_Addr_Filler_1;
    private DbsField record_Id_Ex_File_3_Rt_Addr_Frst;
    private DbsField record_Id_Ex_File_3_Rt_Addr_Filler_2;
    private DbsField record_Id_Ex_File_3_Rt_Addr_Mddle;
    private DbsField record_Id_Ex_File_3_Rt_Addr_Filler_3;
    private DbsField record_Id_Ex_File_3_Rt_Addr_Last;
    private DbsField record_Id_Ex_File_3_Rt_Addr_Filler_4;
    private DbsField record_Id_Ex_File_3_Rt_Addr_Sffx;
    private DbsField record_Id_Ex_File_3_Rt_Welcome_Name;
    private DbsGroup record_Id_Ex_File_3_Rt_Welcome_NameRedef11;
    private DbsField record_Id_Ex_File_3_Rt_Welcome_Pref;
    private DbsField record_Id_Ex_File_3_Rt_Welcome_Filler_1;
    private DbsField record_Id_Ex_File_3_Rt_Welcome_Last;
    private DbsField record_Id_Ex_File_3_Rt_Pin_Name;
    private DbsGroup record_Id_Ex_File_3_Rt_Pin_NameRedef12;
    private DbsField record_Id_Ex_File_3_Rt_Pin_First;
    private DbsField record_Id_Ex_File_3_Rt_Pin_Filler_1;
    private DbsField record_Id_Ex_File_3_Rt_Pin_Mddle;
    private DbsField record_Id_Ex_File_3_Rt_Pin_Filler_2;
    private DbsField record_Id_Ex_File_3_Rt_Pin_Last;
    private DbsField record_Id_Ex_File_3_Rt_Zip;
    private DbsGroup record_Id_Ex_File_3_Rt_ZipRedef13;
    private DbsField record_Id_Ex_File_3_Rt_Zip_Plus4;
    private DbsField record_Id_Ex_File_3_Rt_Zip_Plus1;
    private DbsField record_Id_Ex_File_3_Rt_Suspension;
    private DbsField record_Id_Ex_File_3_Rt_Mult_App_Version;
    private DbsField record_Id_Ex_File_3_Rt_Mult_App_Status;
    private DbsField record_Id_Ex_File_3_Rt_Mult_App_Lob;
    private DbsField record_Id_Ex_File_3_Rt_Mult_App_Lob_Type;
    private DbsField record_Id_Ex_File_3_Rt_Mult_App_Ppg;
    private DbsField record_Id_Ex_File_3_Rt_K12_Ppg;
    private DbsField record_Id_Ex_File_3_Rt_Ira_Rollover_Type;
    private DbsField record_Id_Ex_File_3_Rt_Ira_Record_Type;
    private DbsField record_Id_Ex_File_3_Rt_Rollover_Amt;
    private DbsField record_Id_Ex_File_3_Rt_Mit_Unit;
    private DbsField record_Id_Ex_File_3_Rt_Mit_Wpid;
    private DbsField record_Id_Ex_File_3_Rt_New_Bene_Ind;
    private DbsField record_Id_Ex_File_3_Rt_Irc_Sectn_Cde;
    private DbsField record_Id_Ex_File_3_Rt_Released_Dt;
    private DbsField record_Id_Ex_File_3_Rt_Irc_Sectn_Grp;
    private DbsField record_Id_Ex_File_3_Rt_Enroll_Request_Type;
    private DbsField record_Id_Ex_File_3_Rt_Erisa_Inst;
    private DbsField record_Id_Ex_File_3_Rt_Print_Destination;
    private DbsField record_Id_Ex_File_3_Rt_Correction_Type;
    private DbsField record_Id_Ex_File_3_Rt_Processor_Name;
    private DbsField record_Id_Ex_File_3_Rt_Mail_Date;
    private DbsField record_Id_Ex_File_3_Rt_Alloc_Fmt;
    private DbsField record_Id_Ex_File_3_Rt_Phone_No;
    private DbsField record_Id_Ex_File_3_Rt_Fund_Cde;
    private DbsField record_Id_Ex_File_3_Rt_Alloc_Pct;
    private DbsField record_Id_Ex_File_3_Rt_Oia_Ind;
    private DbsField record_Id_Ex_File_3_Rt_Product_Cde;
    private DbsField record_Id_Ex_File_3_Rt_Acct_Sum_Sheet_Type;
    private DbsField record_Id_Ex_File_3_Rt_Sg_Plan_No;
    private DbsField record_Id_Ex_File_3_Rt_Sg_Subplan_No;
    private DbsField record_Id_Ex_File_3_Rt_Sg_Subplan_Type;
    private DbsField record_Id_Ex_File_3_Rt_Omni_Issue_Ind;
    private DbsField record_Id_Ex_File_3_Rt_Effective_Dt;
    private DbsField record_Id_Ex_File_3_Rt_Employer_Name;
    private DbsField record_Id_Ex_File_3_Rt_Tiaa_Rate;
    private DbsField record_Id_Ex_File_3_Rt_Sg_Plan_Id;
    private DbsField record_Id_Ex_File_3_Rt_Tiaa_Pg4_Numb;
    private DbsField record_Id_Ex_File_3_Rt_Cref_Pg4_Numb;
    private DbsField record_Id_Ex_File_3_Rt_Dflt_Access_Code;
    private DbsField record_Id_Ex_File_3_Rt_Single_Issue_Ind;
    private DbsField record_Id_Ex_File_3_Rt_Spec_Fund_Ind;
    private DbsField record_Id_Ex_File_3_Rt_Product_Price_Level;
    private DbsField record_Id_Ex_File_3_Rt_Roth_Ind;
    private DbsField record_Id_Ex_File_3_Rt_Plan_Issue_State;
    private DbsField record_Id_Ex_File_3_Rt_Client_Id;
    private DbsField record_Id_Ex_File_3_Rt_Portfolio_Model;
    private DbsField record_Id_Ex_File_3_Rt_Auto_Enroll_Ind;
    private DbsField record_Id_Ex_File_3_Rt_Auto_Save_Ind;
    private DbsField record_Id_Ex_File_3_Rt_As_Cur_Dflt_Amt;
    private DbsField record_Id_Ex_File_3_Rt_As_Incr_Amt;
    private DbsField record_Id_Ex_File_3_Rt_As_Max_Pct;
    private DbsField record_Id_Ex_File_3_Rt_Ae_Opt_Out_Days;
    private DbsField record_Id_Ex_File_3_Rt_Tsv_Ind;
    private DbsField record_Id_Ex_File_3_Rt_Tiaa_Indx_Guarntd_Rte;
    private DbsField record_Id_Ex_File_3_Rt_Agent_Crd_No;
    private DbsField record_Id_Ex_File_3_Rt_Crd_Pull_Ind;
    private DbsField record_Id_Ex_File_3_Rt_Agent_Name;
    private DbsField record_Id_Ex_File_3_Rt_Agent_Work_Addr1;
    private DbsField record_Id_Ex_File_3_Rt_Agent_Work_Addr2;
    private DbsField record_Id_Ex_File_3_Rt_Agent_Work_City;
    private DbsField record_Id_Ex_File_3_Rt_Agent_Work_State;
    private DbsField record_Id_Ex_File_3_Rt_Agent_Work_Zip;
    private DbsField record_Id_Ex_File_3_Rt_Agent_Work_Phone;
    private DbsField record_Id_Ex_File_3_Rt_Tic_Ind;
    private DbsField record_Id_Ex_File_3_Rt_Tic_Startdate;
    private DbsField record_Id_Ex_File_3_Rt_Tic_Enddate;
    private DbsField record_Id_Ex_File_3_Rt_Tic_Percentage;
    private DbsField record_Id_Ex_File_3_Rt_Tic_Postdays;
    private DbsField record_Id_Ex_File_3_Rt_Tic_Limit;
    private DbsField record_Id_Ex_File_3_Rt_Tic_Postfreq;
    private DbsField record_Id_Ex_File_3_Rt_Tic_Pl_Level;
    private DbsField record_Id_Ex_File_3_Rt_Tic_Windowdays;
    private DbsField record_Id_Ex_File_3_Rt_Tic_Reqdlywindow;
    private DbsField record_Id_Ex_File_3_Rt_Tic_Recap_Prov;
    private DbsField record_Id_Ex_File_3_Rt_Ecs_Dcs_E_Dlvry_Ind;
    private DbsField record_Id_Ex_File_3_Rt_Ecs_Dcs_Annty_Optn_Ind;
    private DbsField record_Id_Ex_File_3_Rt_Full_Middle_Name;
    private DbsField record_Id_Ex_File_3_Rt_Substitution_Contract_Ind;
    private DbsField record_Id_Ex_File_3_Rt_Bundle_Type;
    private DbsField record_Id_Ex_File_3_Rt_Non_Proprietary_Pkg_Ind;
    private DbsField record_Id_Ex_File_3_Rt_Multi_Plan_Table;
    private DbsGroup record_Id_Ex_File_3_Rt_Multi_Plan_TableRedef14;
    private DbsGroup record_Id_Ex_File_3_Rt_Multi_Plan_Info;
    private DbsField record_Id_Ex_File_3_Rt_Multi_Plan_No;
    private DbsField record_Id_Ex_File_3_Rt_Multi_Sub_Plan;
    private DbsField record_Id_Ex_File_3_Rt_Filler;
    private DbsField record_Id_Ex_File_3_Rt_Tiaa_Init_Prem;
    private DbsField record_Id_Ex_File_3_Rt_Cref_Init_Prem;
    private DbsField record_Id_Ex_File_3_Rt_Related_Contract_Info;
    private DbsGroup record_Id_Ex_File_3_Rt_Related_Contract_InfoRedef15;
    private DbsGroup record_Id_Ex_File_3_Rt_Related_Contract;
    private DbsField record_Id_Ex_File_3_Rt_Related_Contract_Type;
    private DbsField record_Id_Ex_File_3_Rt_Related_Tiaa_No;
    private DbsField record_Id_Ex_File_3_Rt_Related_Tiaa_Issue_Date;
    private DbsField record_Id_Ex_File_3_Rt_Related_First_Payment_Date;
    private DbsField record_Id_Ex_File_3_Rt_Related_Tiaa_Total_Amt;
    private DbsField record_Id_Ex_File_3_Rt_Related_Last_Payment_Date;
    private DbsField record_Id_Ex_File_3_Rt_Related_Payment_Frequency;
    private DbsField record_Id_Ex_File_3_Rt_Related_Tiaa_Issue_State;
    private DbsField record_Id_Ex_File_3_Rt_First_Tpa_Ipro_Ind;
    private DbsField record_Id_Ex_File_3_Rt_Fund_Source_1;
    private DbsField record_Id_Ex_File_3_Rt_Fund_Source_2;
    private DbsField record_Id_Ex_File_3_Rt_Fund_Cde_2;
    private DbsField record_Id_Ex_File_3_Rt_Alloc_Pct_2;

    public DbsGroup getRecord_Id_Ex_File_3() { return record_Id_Ex_File_3; }

    public DbsField getRecord_Id_Ex_File_3_Record_Id() { return record_Id_Ex_File_3_Record_Id; }

    public DbsGroup getRecord_Id_Ex_File_3_Record_IdRedef1() { return record_Id_Ex_File_3_Record_IdRedef1; }

    public DbsField getRecord_Id_Ex_File_3_Hd_Record_Type() { return record_Id_Ex_File_3_Hd_Record_Type; }

    public DbsField getRecord_Id_Ex_File_3_Hd_Output_Profile() { return record_Id_Ex_File_3_Hd_Output_Profile; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Print_Package_Type() { return record_Id_Ex_File_3_Rt_Print_Package_Type; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Reprint_Request_Type() { return record_Id_Ex_File_3_Rt_Reprint_Request_Type; }

    public DbsField getRecord_Id_Ex_File_3_Hd_Product_Type() { return record_Id_Ex_File_3_Hd_Product_Type; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Type_Cd() { return record_Id_Ex_File_3_Rt_Type_Cd; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tiaa_No_1() { return record_Id_Ex_File_3_Rt_Tiaa_No_1; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tiaa_Fill_1() { return record_Id_Ex_File_3_Rt_Tiaa_Fill_1; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Cref_No_1() { return record_Id_Ex_File_3_Rt_Cref_No_1; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Cref_Fill_1() { return record_Id_Ex_File_3_Rt_Cref_Fill_1; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tiaa_Cntr_2() { return record_Id_Ex_File_3_Rt_Tiaa_Cntr_2; }

    public DbsGroup getRecord_Id_Ex_File_3_Rt_Tiaa_Cntr_2Redef2() { return record_Id_Ex_File_3_Rt_Tiaa_Cntr_2Redef2; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tiaa_No_2() { return record_Id_Ex_File_3_Rt_Tiaa_No_2; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tiaa_Fill_2() { return record_Id_Ex_File_3_Rt_Tiaa_Fill_2; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Cref_Cert_2() { return record_Id_Ex_File_3_Rt_Cref_Cert_2; }

    public DbsGroup getRecord_Id_Ex_File_3_Rt_Cref_Cert_2Redef3() { return record_Id_Ex_File_3_Rt_Cref_Cert_2Redef3; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Cref_No_2() { return record_Id_Ex_File_3_Rt_Cref_No_2; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Cref_Fill_2() { return record_Id_Ex_File_3_Rt_Cref_Fill_2; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tiaa_Cntr_3() { return record_Id_Ex_File_3_Rt_Tiaa_Cntr_3; }

    public DbsGroup getRecord_Id_Ex_File_3_Rt_Tiaa_Cntr_3Redef4() { return record_Id_Ex_File_3_Rt_Tiaa_Cntr_3Redef4; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tiaa_No_3() { return record_Id_Ex_File_3_Rt_Tiaa_No_3; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tiaa_Fill_3() { return record_Id_Ex_File_3_Rt_Tiaa_Fill_3; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Cref_Cert_3() { return record_Id_Ex_File_3_Rt_Cref_Cert_3; }

    public DbsGroup getRecord_Id_Ex_File_3_Rt_Cref_Cert_3Redef5() { return record_Id_Ex_File_3_Rt_Cref_Cert_3Redef5; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Cref_No_3() { return record_Id_Ex_File_3_Rt_Cref_No_3; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Cref_Fill_3() { return record_Id_Ex_File_3_Rt_Cref_Fill_3; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tiaa_Cntr_4() { return record_Id_Ex_File_3_Rt_Tiaa_Cntr_4; }

    public DbsGroup getRecord_Id_Ex_File_3_Rt_Tiaa_Cntr_4Redef6() { return record_Id_Ex_File_3_Rt_Tiaa_Cntr_4Redef6; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tiaa_No_4() { return record_Id_Ex_File_3_Rt_Tiaa_No_4; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tiaa_Fill_4() { return record_Id_Ex_File_3_Rt_Tiaa_Fill_4; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Cref_Cert_4() { return record_Id_Ex_File_3_Rt_Cref_Cert_4; }

    public DbsGroup getRecord_Id_Ex_File_3_Rt_Cref_Cert_4Redef7() { return record_Id_Ex_File_3_Rt_Cref_Cert_4Redef7; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Cref_No_4() { return record_Id_Ex_File_3_Rt_Cref_No_4; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Cref_Fill_4() { return record_Id_Ex_File_3_Rt_Cref_Fill_4; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Date_Table() { return record_Id_Ex_File_3_Rt_Date_Table; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Age_First_Pymt() { return record_Id_Ex_File_3_Rt_Age_First_Pymt; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Ownership() { return record_Id_Ex_File_3_Rt_Ownership; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Alloc_Disc() { return record_Id_Ex_File_3_Rt_Alloc_Disc; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Currency() { return record_Id_Ex_File_3_Rt_Currency; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tiaa_Extr_Form() { return record_Id_Ex_File_3_Rt_Tiaa_Extr_Form; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Cref_Extr_Form() { return record_Id_Ex_File_3_Rt_Cref_Extr_Form; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tiaa_Extr_Ed() { return record_Id_Ex_File_3_Rt_Tiaa_Extr_Ed; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Cref_Extr_Ed() { return record_Id_Ex_File_3_Rt_Cref_Extr_Ed; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Annuitant_Name() { return record_Id_Ex_File_3_Rt_Annuitant_Name; }

    public DbsGroup getRecord_Id_Ex_File_3_Rt_Eop_Name() { return record_Id_Ex_File_3_Rt_Eop_Name; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Eop_Last_Name() { return record_Id_Ex_File_3_Rt_Eop_Last_Name; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Eop_First_Name() { return record_Id_Ex_File_3_Rt_Eop_First_Name; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Eop_Middle_Name() { return record_Id_Ex_File_3_Rt_Eop_Middle_Name; }

    public DbsGroup getRecord_Id_Ex_File_3_Rt_Eop_Middle_NameRedef8() { return record_Id_Ex_File_3_Rt_Eop_Middle_NameRedef8; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Eop_Mid_Init() { return record_Id_Ex_File_3_Rt_Eop_Mid_Init; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Eop_Mid_Filler() { return record_Id_Ex_File_3_Rt_Eop_Mid_Filler; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Eop_Title() { return record_Id_Ex_File_3_Rt_Eop_Title; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Eop_Prefix() { return record_Id_Ex_File_3_Rt_Eop_Prefix; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Soc_Sec() { return record_Id_Ex_File_3_Rt_Soc_Sec; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Pin_No() { return record_Id_Ex_File_3_Rt_Pin_No; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Address_Table() { return record_Id_Ex_File_3_Rt_Address_Table; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Eop_Zipcode() { return record_Id_Ex_File_3_Rt_Eop_Zipcode; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Participant_Status() { return record_Id_Ex_File_3_Rt_Participant_Status; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Email_Address() { return record_Id_Ex_File_3_Rt_Email_Address; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Request_Package_Type() { return record_Id_Ex_File_3_Rt_Request_Package_Type; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Portfolio_Selected() { return record_Id_Ex_File_3_Rt_Portfolio_Selected; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Divorce_Contract() { return record_Id_Ex_File_3_Rt_Divorce_Contract; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Access_Cd_Ind() { return record_Id_Ex_File_3_Rt_Access_Cd_Ind; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Address_Change() { return record_Id_Ex_File_3_Rt_Address_Change; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Package_Version() { return record_Id_Ex_File_3_Rt_Package_Version; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Unit_Name() { return record_Id_Ex_File_3_Rt_Unit_Name; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Future_Filler() { return record_Id_Ex_File_3_Rt_Future_Filler; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Bene_Estate() { return record_Id_Ex_File_3_Rt_Bene_Estate; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Bene_Trust() { return record_Id_Ex_File_3_Rt_Bene_Trust; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Bene_Category() { return record_Id_Ex_File_3_Rt_Bene_Category; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Sex() { return record_Id_Ex_File_3_Rt_Sex; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Inst_Name_Table() { return record_Id_Ex_File_3_Rt_Inst_Name_Table; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Inst_Name() { return record_Id_Ex_File_3_Rt_Inst_Name; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Inst_Address_Table() { return record_Id_Ex_File_3_Rt_Inst_Address_Table; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Inst_Zip_Cd() { return record_Id_Ex_File_3_Rt_Inst_Zip_Cd; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Mail_Instructions() { return record_Id_Ex_File_3_Rt_Mail_Instructions; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Orig_Iss_State() { return record_Id_Ex_File_3_Rt_Orig_Iss_State; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Current_Iss_State() { return record_Id_Ex_File_3_Rt_Current_Iss_State; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Eop_State_Name() { return record_Id_Ex_File_3_Rt_Eop_State_Name; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Lob() { return record_Id_Ex_File_3_Rt_Lob; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Lob_Type() { return record_Id_Ex_File_3_Rt_Lob_Type; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Inst_Code() { return record_Id_Ex_File_3_Rt_Inst_Code; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Inst_Bill_Cd() { return record_Id_Ex_File_3_Rt_Inst_Bill_Cd; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Inst_Permit_Trans() { return record_Id_Ex_File_3_Rt_Inst_Permit_Trans; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Perm_Gra_Trans() { return record_Id_Ex_File_3_Rt_Perm_Gra_Trans; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Inst_Full_Immed_Vest() { return record_Id_Ex_File_3_Rt_Inst_Full_Immed_Vest; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Inst_Cashable_Ra() { return record_Id_Ex_File_3_Rt_Inst_Cashable_Ra; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Inst_Cashable_Gra() { return record_Id_Ex_File_3_Rt_Inst_Cashable_Gra; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Inst_Fixed_Per_Opt() { return record_Id_Ex_File_3_Rt_Inst_Fixed_Per_Opt; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Inst_Cntrl_Consent() { return record_Id_Ex_File_3_Rt_Inst_Cntrl_Consent; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Eop() { return record_Id_Ex_File_3_Rt_Eop; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Gsra_Inst_St_Code() { return record_Id_Ex_File_3_Rt_Gsra_Inst_St_Code; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Region_Cd() { return record_Id_Ex_File_3_Rt_Region_Cd; }

    public DbsGroup getRecord_Id_Ex_File_3_Rt_Region_CdRedef9() { return record_Id_Ex_File_3_Rt_Region_CdRedef9; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Region() { return record_Id_Ex_File_3_Rt_Region; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Branch() { return record_Id_Ex_File_3_Rt_Branch; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Need() { return record_Id_Ex_File_3_Rt_Need; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Staff_Id() { return record_Id_Ex_File_3_Rt_Staff_Id; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Appl_Source() { return record_Id_Ex_File_3_Rt_Appl_Source; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Eop_Appl_Status() { return record_Id_Ex_File_3_Rt_Eop_Appl_Status; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Eop_Addl_Cref_Req() { return record_Id_Ex_File_3_Rt_Eop_Addl_Cref_Req; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Extract_Date() { return record_Id_Ex_File_3_Rt_Extract_Date; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Gsra_Loan_Ind() { return record_Id_Ex_File_3_Rt_Gsra_Loan_Ind; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Spec_Ppg_Ind() { return record_Id_Ex_File_3_Rt_Spec_Ppg_Ind; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Mail_Pull_Cd() { return record_Id_Ex_File_3_Rt_Mail_Pull_Cd; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Mit_Log_Dt_Tm() { return record_Id_Ex_File_3_Rt_Mit_Log_Dt_Tm; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Addr_Page_Name() { return record_Id_Ex_File_3_Rt_Addr_Page_Name; }

    public DbsGroup getRecord_Id_Ex_File_3_Rt_Addr_Page_NameRedef10() { return record_Id_Ex_File_3_Rt_Addr_Page_NameRedef10; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Addr_Pref() { return record_Id_Ex_File_3_Rt_Addr_Pref; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Addr_Filler_1() { return record_Id_Ex_File_3_Rt_Addr_Filler_1; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Addr_Frst() { return record_Id_Ex_File_3_Rt_Addr_Frst; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Addr_Filler_2() { return record_Id_Ex_File_3_Rt_Addr_Filler_2; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Addr_Mddle() { return record_Id_Ex_File_3_Rt_Addr_Mddle; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Addr_Filler_3() { return record_Id_Ex_File_3_Rt_Addr_Filler_3; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Addr_Last() { return record_Id_Ex_File_3_Rt_Addr_Last; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Addr_Filler_4() { return record_Id_Ex_File_3_Rt_Addr_Filler_4; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Addr_Sffx() { return record_Id_Ex_File_3_Rt_Addr_Sffx; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Welcome_Name() { return record_Id_Ex_File_3_Rt_Welcome_Name; }

    public DbsGroup getRecord_Id_Ex_File_3_Rt_Welcome_NameRedef11() { return record_Id_Ex_File_3_Rt_Welcome_NameRedef11; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Welcome_Pref() { return record_Id_Ex_File_3_Rt_Welcome_Pref; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Welcome_Filler_1() { return record_Id_Ex_File_3_Rt_Welcome_Filler_1; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Welcome_Last() { return record_Id_Ex_File_3_Rt_Welcome_Last; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Pin_Name() { return record_Id_Ex_File_3_Rt_Pin_Name; }

    public DbsGroup getRecord_Id_Ex_File_3_Rt_Pin_NameRedef12() { return record_Id_Ex_File_3_Rt_Pin_NameRedef12; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Pin_First() { return record_Id_Ex_File_3_Rt_Pin_First; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Pin_Filler_1() { return record_Id_Ex_File_3_Rt_Pin_Filler_1; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Pin_Mddle() { return record_Id_Ex_File_3_Rt_Pin_Mddle; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Pin_Filler_2() { return record_Id_Ex_File_3_Rt_Pin_Filler_2; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Pin_Last() { return record_Id_Ex_File_3_Rt_Pin_Last; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Zip() { return record_Id_Ex_File_3_Rt_Zip; }

    public DbsGroup getRecord_Id_Ex_File_3_Rt_ZipRedef13() { return record_Id_Ex_File_3_Rt_ZipRedef13; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Zip_Plus4() { return record_Id_Ex_File_3_Rt_Zip_Plus4; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Zip_Plus1() { return record_Id_Ex_File_3_Rt_Zip_Plus1; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Suspension() { return record_Id_Ex_File_3_Rt_Suspension; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Mult_App_Version() { return record_Id_Ex_File_3_Rt_Mult_App_Version; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Mult_App_Status() { return record_Id_Ex_File_3_Rt_Mult_App_Status; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Mult_App_Lob() { return record_Id_Ex_File_3_Rt_Mult_App_Lob; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Mult_App_Lob_Type() { return record_Id_Ex_File_3_Rt_Mult_App_Lob_Type; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Mult_App_Ppg() { return record_Id_Ex_File_3_Rt_Mult_App_Ppg; }

    public DbsField getRecord_Id_Ex_File_3_Rt_K12_Ppg() { return record_Id_Ex_File_3_Rt_K12_Ppg; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Ira_Rollover_Type() { return record_Id_Ex_File_3_Rt_Ira_Rollover_Type; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Ira_Record_Type() { return record_Id_Ex_File_3_Rt_Ira_Record_Type; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Rollover_Amt() { return record_Id_Ex_File_3_Rt_Rollover_Amt; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Mit_Unit() { return record_Id_Ex_File_3_Rt_Mit_Unit; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Mit_Wpid() { return record_Id_Ex_File_3_Rt_Mit_Wpid; }

    public DbsField getRecord_Id_Ex_File_3_Rt_New_Bene_Ind() { return record_Id_Ex_File_3_Rt_New_Bene_Ind; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Irc_Sectn_Cde() { return record_Id_Ex_File_3_Rt_Irc_Sectn_Cde; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Released_Dt() { return record_Id_Ex_File_3_Rt_Released_Dt; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Irc_Sectn_Grp() { return record_Id_Ex_File_3_Rt_Irc_Sectn_Grp; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Enroll_Request_Type() { return record_Id_Ex_File_3_Rt_Enroll_Request_Type; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Erisa_Inst() { return record_Id_Ex_File_3_Rt_Erisa_Inst; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Print_Destination() { return record_Id_Ex_File_3_Rt_Print_Destination; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Correction_Type() { return record_Id_Ex_File_3_Rt_Correction_Type; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Processor_Name() { return record_Id_Ex_File_3_Rt_Processor_Name; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Mail_Date() { return record_Id_Ex_File_3_Rt_Mail_Date; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Alloc_Fmt() { return record_Id_Ex_File_3_Rt_Alloc_Fmt; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Phone_No() { return record_Id_Ex_File_3_Rt_Phone_No; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Fund_Cde() { return record_Id_Ex_File_3_Rt_Fund_Cde; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Alloc_Pct() { return record_Id_Ex_File_3_Rt_Alloc_Pct; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Oia_Ind() { return record_Id_Ex_File_3_Rt_Oia_Ind; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Product_Cde() { return record_Id_Ex_File_3_Rt_Product_Cde; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Acct_Sum_Sheet_Type() { return record_Id_Ex_File_3_Rt_Acct_Sum_Sheet_Type; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Sg_Plan_No() { return record_Id_Ex_File_3_Rt_Sg_Plan_No; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Sg_Subplan_No() { return record_Id_Ex_File_3_Rt_Sg_Subplan_No; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Sg_Subplan_Type() { return record_Id_Ex_File_3_Rt_Sg_Subplan_Type; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Omni_Issue_Ind() { return record_Id_Ex_File_3_Rt_Omni_Issue_Ind; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Effective_Dt() { return record_Id_Ex_File_3_Rt_Effective_Dt; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Employer_Name() { return record_Id_Ex_File_3_Rt_Employer_Name; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tiaa_Rate() { return record_Id_Ex_File_3_Rt_Tiaa_Rate; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Sg_Plan_Id() { return record_Id_Ex_File_3_Rt_Sg_Plan_Id; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tiaa_Pg4_Numb() { return record_Id_Ex_File_3_Rt_Tiaa_Pg4_Numb; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Cref_Pg4_Numb() { return record_Id_Ex_File_3_Rt_Cref_Pg4_Numb; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Dflt_Access_Code() { return record_Id_Ex_File_3_Rt_Dflt_Access_Code; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Single_Issue_Ind() { return record_Id_Ex_File_3_Rt_Single_Issue_Ind; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Spec_Fund_Ind() { return record_Id_Ex_File_3_Rt_Spec_Fund_Ind; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Product_Price_Level() { return record_Id_Ex_File_3_Rt_Product_Price_Level; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Roth_Ind() { return record_Id_Ex_File_3_Rt_Roth_Ind; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Plan_Issue_State() { return record_Id_Ex_File_3_Rt_Plan_Issue_State; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Client_Id() { return record_Id_Ex_File_3_Rt_Client_Id; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Portfolio_Model() { return record_Id_Ex_File_3_Rt_Portfolio_Model; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Auto_Enroll_Ind() { return record_Id_Ex_File_3_Rt_Auto_Enroll_Ind; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Auto_Save_Ind() { return record_Id_Ex_File_3_Rt_Auto_Save_Ind; }

    public DbsField getRecord_Id_Ex_File_3_Rt_As_Cur_Dflt_Amt() { return record_Id_Ex_File_3_Rt_As_Cur_Dflt_Amt; }

    public DbsField getRecord_Id_Ex_File_3_Rt_As_Incr_Amt() { return record_Id_Ex_File_3_Rt_As_Incr_Amt; }

    public DbsField getRecord_Id_Ex_File_3_Rt_As_Max_Pct() { return record_Id_Ex_File_3_Rt_As_Max_Pct; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Ae_Opt_Out_Days() { return record_Id_Ex_File_3_Rt_Ae_Opt_Out_Days; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tsv_Ind() { return record_Id_Ex_File_3_Rt_Tsv_Ind; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tiaa_Indx_Guarntd_Rte() { return record_Id_Ex_File_3_Rt_Tiaa_Indx_Guarntd_Rte; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Agent_Crd_No() { return record_Id_Ex_File_3_Rt_Agent_Crd_No; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Crd_Pull_Ind() { return record_Id_Ex_File_3_Rt_Crd_Pull_Ind; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Agent_Name() { return record_Id_Ex_File_3_Rt_Agent_Name; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Agent_Work_Addr1() { return record_Id_Ex_File_3_Rt_Agent_Work_Addr1; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Agent_Work_Addr2() { return record_Id_Ex_File_3_Rt_Agent_Work_Addr2; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Agent_Work_City() { return record_Id_Ex_File_3_Rt_Agent_Work_City; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Agent_Work_State() { return record_Id_Ex_File_3_Rt_Agent_Work_State; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Agent_Work_Zip() { return record_Id_Ex_File_3_Rt_Agent_Work_Zip; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Agent_Work_Phone() { return record_Id_Ex_File_3_Rt_Agent_Work_Phone; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tic_Ind() { return record_Id_Ex_File_3_Rt_Tic_Ind; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tic_Startdate() { return record_Id_Ex_File_3_Rt_Tic_Startdate; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tic_Enddate() { return record_Id_Ex_File_3_Rt_Tic_Enddate; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tic_Percentage() { return record_Id_Ex_File_3_Rt_Tic_Percentage; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tic_Postdays() { return record_Id_Ex_File_3_Rt_Tic_Postdays; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tic_Limit() { return record_Id_Ex_File_3_Rt_Tic_Limit; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tic_Postfreq() { return record_Id_Ex_File_3_Rt_Tic_Postfreq; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tic_Pl_Level() { return record_Id_Ex_File_3_Rt_Tic_Pl_Level; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tic_Windowdays() { return record_Id_Ex_File_3_Rt_Tic_Windowdays; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tic_Reqdlywindow() { return record_Id_Ex_File_3_Rt_Tic_Reqdlywindow; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tic_Recap_Prov() { return record_Id_Ex_File_3_Rt_Tic_Recap_Prov; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Ecs_Dcs_E_Dlvry_Ind() { return record_Id_Ex_File_3_Rt_Ecs_Dcs_E_Dlvry_Ind; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Ecs_Dcs_Annty_Optn_Ind() { return record_Id_Ex_File_3_Rt_Ecs_Dcs_Annty_Optn_Ind; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Full_Middle_Name() { return record_Id_Ex_File_3_Rt_Full_Middle_Name; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Substitution_Contract_Ind() { return record_Id_Ex_File_3_Rt_Substitution_Contract_Ind; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Bundle_Type() { return record_Id_Ex_File_3_Rt_Bundle_Type; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Non_Proprietary_Pkg_Ind() { return record_Id_Ex_File_3_Rt_Non_Proprietary_Pkg_Ind; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Multi_Plan_Table() { return record_Id_Ex_File_3_Rt_Multi_Plan_Table; }

    public DbsGroup getRecord_Id_Ex_File_3_Rt_Multi_Plan_TableRedef14() { return record_Id_Ex_File_3_Rt_Multi_Plan_TableRedef14; }

    public DbsGroup getRecord_Id_Ex_File_3_Rt_Multi_Plan_Info() { return record_Id_Ex_File_3_Rt_Multi_Plan_Info; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Multi_Plan_No() { return record_Id_Ex_File_3_Rt_Multi_Plan_No; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Multi_Sub_Plan() { return record_Id_Ex_File_3_Rt_Multi_Sub_Plan; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Filler() { return record_Id_Ex_File_3_Rt_Filler; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Tiaa_Init_Prem() { return record_Id_Ex_File_3_Rt_Tiaa_Init_Prem; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Cref_Init_Prem() { return record_Id_Ex_File_3_Rt_Cref_Init_Prem; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Related_Contract_Info() { return record_Id_Ex_File_3_Rt_Related_Contract_Info; }

    public DbsGroup getRecord_Id_Ex_File_3_Rt_Related_Contract_InfoRedef15() { return record_Id_Ex_File_3_Rt_Related_Contract_InfoRedef15; }

    public DbsGroup getRecord_Id_Ex_File_3_Rt_Related_Contract() { return record_Id_Ex_File_3_Rt_Related_Contract; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Related_Contract_Type() { return record_Id_Ex_File_3_Rt_Related_Contract_Type; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Related_Tiaa_No() { return record_Id_Ex_File_3_Rt_Related_Tiaa_No; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Related_Tiaa_Issue_Date() { return record_Id_Ex_File_3_Rt_Related_Tiaa_Issue_Date; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Related_First_Payment_Date() { return record_Id_Ex_File_3_Rt_Related_First_Payment_Date; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Related_Tiaa_Total_Amt() { return record_Id_Ex_File_3_Rt_Related_Tiaa_Total_Amt; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Related_Last_Payment_Date() { return record_Id_Ex_File_3_Rt_Related_Last_Payment_Date; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Related_Payment_Frequency() { return record_Id_Ex_File_3_Rt_Related_Payment_Frequency; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Related_Tiaa_Issue_State() { return record_Id_Ex_File_3_Rt_Related_Tiaa_Issue_State; }

    public DbsField getRecord_Id_Ex_File_3_Rt_First_Tpa_Ipro_Ind() { return record_Id_Ex_File_3_Rt_First_Tpa_Ipro_Ind; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Fund_Source_1() { return record_Id_Ex_File_3_Rt_Fund_Source_1; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Fund_Source_2() { return record_Id_Ex_File_3_Rt_Fund_Source_2; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Fund_Cde_2() { return record_Id_Ex_File_3_Rt_Fund_Cde_2; }

    public DbsField getRecord_Id_Ex_File_3_Rt_Alloc_Pct_2() { return record_Id_Ex_File_3_Rt_Alloc_Pct_2; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        record_Id_Ex_File_3 = newGroupInRecord("record_Id_Ex_File_3", "RECORD-ID-EX-FILE-3");
        record_Id_Ex_File_3_Record_Id = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Record_Id", "RECORD-ID", FieldType.STRING, 33);
        record_Id_Ex_File_3_Record_IdRedef1 = record_Id_Ex_File_3.newGroupInGroup("record_Id_Ex_File_3_Record_IdRedef1", "Redefines", record_Id_Ex_File_3_Record_Id);
        record_Id_Ex_File_3_Hd_Record_Type = record_Id_Ex_File_3_Record_IdRedef1.newFieldInGroup("record_Id_Ex_File_3_Hd_Record_Type", "HD-RECORD-TYPE", 
            FieldType.STRING, 2);
        record_Id_Ex_File_3_Hd_Output_Profile = record_Id_Ex_File_3_Record_IdRedef1.newFieldInGroup("record_Id_Ex_File_3_Hd_Output_Profile", "HD-OUTPUT-PROFILE", 
            FieldType.STRING, 2);
        record_Id_Ex_File_3_Rt_Print_Package_Type = record_Id_Ex_File_3_Record_IdRedef1.newFieldInGroup("record_Id_Ex_File_3_Rt_Print_Package_Type", "RT-PRINT-PACKAGE-TYPE", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Reprint_Request_Type = record_Id_Ex_File_3_Record_IdRedef1.newFieldInGroup("record_Id_Ex_File_3_Rt_Reprint_Request_Type", 
            "RT-REPRINT-REQUEST-TYPE", FieldType.STRING, 1);
        record_Id_Ex_File_3_Hd_Product_Type = record_Id_Ex_File_3_Record_IdRedef1.newFieldInGroup("record_Id_Ex_File_3_Hd_Product_Type", "HD-PRODUCT-TYPE", 
            FieldType.STRING, 6);
        record_Id_Ex_File_3_Rt_Type_Cd = record_Id_Ex_File_3_Record_IdRedef1.newFieldInGroup("record_Id_Ex_File_3_Rt_Type_Cd", "RT-TYPE-CD", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Tiaa_No_1 = record_Id_Ex_File_3_Record_IdRedef1.newFieldInGroup("record_Id_Ex_File_3_Rt_Tiaa_No_1", "RT-TIAA-NO-1", FieldType.STRING, 
            8);
        record_Id_Ex_File_3_Rt_Tiaa_Fill_1 = record_Id_Ex_File_3_Record_IdRedef1.newFieldInGroup("record_Id_Ex_File_3_Rt_Tiaa_Fill_1", "RT-TIAA-FILL-1", 
            FieldType.STRING, 2);
        record_Id_Ex_File_3_Rt_Cref_No_1 = record_Id_Ex_File_3_Record_IdRedef1.newFieldInGroup("record_Id_Ex_File_3_Rt_Cref_No_1", "RT-CREF-NO-1", FieldType.STRING, 
            8);
        record_Id_Ex_File_3_Rt_Cref_Fill_1 = record_Id_Ex_File_3_Record_IdRedef1.newFieldInGroup("record_Id_Ex_File_3_Rt_Cref_Fill_1", "RT-CREF-FILL-1", 
            FieldType.STRING, 2);
        record_Id_Ex_File_3_Rt_Tiaa_Cntr_2 = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Tiaa_Cntr_2", "RT-TIAA-CNTR-2", FieldType.STRING, 
            10);
        record_Id_Ex_File_3_Rt_Tiaa_Cntr_2Redef2 = record_Id_Ex_File_3.newGroupInGroup("record_Id_Ex_File_3_Rt_Tiaa_Cntr_2Redef2", "Redefines", record_Id_Ex_File_3_Rt_Tiaa_Cntr_2);
        record_Id_Ex_File_3_Rt_Tiaa_No_2 = record_Id_Ex_File_3_Rt_Tiaa_Cntr_2Redef2.newFieldInGroup("record_Id_Ex_File_3_Rt_Tiaa_No_2", "RT-TIAA-NO-2", 
            FieldType.STRING, 8);
        record_Id_Ex_File_3_Rt_Tiaa_Fill_2 = record_Id_Ex_File_3_Rt_Tiaa_Cntr_2Redef2.newFieldInGroup("record_Id_Ex_File_3_Rt_Tiaa_Fill_2", "RT-TIAA-FILL-2", 
            FieldType.STRING, 2);
        record_Id_Ex_File_3_Rt_Cref_Cert_2 = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Cref_Cert_2", "RT-CREF-CERT-2", FieldType.STRING, 
            10);
        record_Id_Ex_File_3_Rt_Cref_Cert_2Redef3 = record_Id_Ex_File_3.newGroupInGroup("record_Id_Ex_File_3_Rt_Cref_Cert_2Redef3", "Redefines", record_Id_Ex_File_3_Rt_Cref_Cert_2);
        record_Id_Ex_File_3_Rt_Cref_No_2 = record_Id_Ex_File_3_Rt_Cref_Cert_2Redef3.newFieldInGroup("record_Id_Ex_File_3_Rt_Cref_No_2", "RT-CREF-NO-2", 
            FieldType.STRING, 8);
        record_Id_Ex_File_3_Rt_Cref_Fill_2 = record_Id_Ex_File_3_Rt_Cref_Cert_2Redef3.newFieldInGroup("record_Id_Ex_File_3_Rt_Cref_Fill_2", "RT-CREF-FILL-2", 
            FieldType.STRING, 2);
        record_Id_Ex_File_3_Rt_Tiaa_Cntr_3 = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Tiaa_Cntr_3", "RT-TIAA-CNTR-3", FieldType.STRING, 
            10);
        record_Id_Ex_File_3_Rt_Tiaa_Cntr_3Redef4 = record_Id_Ex_File_3.newGroupInGroup("record_Id_Ex_File_3_Rt_Tiaa_Cntr_3Redef4", "Redefines", record_Id_Ex_File_3_Rt_Tiaa_Cntr_3);
        record_Id_Ex_File_3_Rt_Tiaa_No_3 = record_Id_Ex_File_3_Rt_Tiaa_Cntr_3Redef4.newFieldInGroup("record_Id_Ex_File_3_Rt_Tiaa_No_3", "RT-TIAA-NO-3", 
            FieldType.STRING, 8);
        record_Id_Ex_File_3_Rt_Tiaa_Fill_3 = record_Id_Ex_File_3_Rt_Tiaa_Cntr_3Redef4.newFieldInGroup("record_Id_Ex_File_3_Rt_Tiaa_Fill_3", "RT-TIAA-FILL-3", 
            FieldType.STRING, 2);
        record_Id_Ex_File_3_Rt_Cref_Cert_3 = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Cref_Cert_3", "RT-CREF-CERT-3", FieldType.STRING, 
            10);
        record_Id_Ex_File_3_Rt_Cref_Cert_3Redef5 = record_Id_Ex_File_3.newGroupInGroup("record_Id_Ex_File_3_Rt_Cref_Cert_3Redef5", "Redefines", record_Id_Ex_File_3_Rt_Cref_Cert_3);
        record_Id_Ex_File_3_Rt_Cref_No_3 = record_Id_Ex_File_3_Rt_Cref_Cert_3Redef5.newFieldInGroup("record_Id_Ex_File_3_Rt_Cref_No_3", "RT-CREF-NO-3", 
            FieldType.STRING, 8);
        record_Id_Ex_File_3_Rt_Cref_Fill_3 = record_Id_Ex_File_3_Rt_Cref_Cert_3Redef5.newFieldInGroup("record_Id_Ex_File_3_Rt_Cref_Fill_3", "RT-CREF-FILL-3", 
            FieldType.STRING, 2);
        record_Id_Ex_File_3_Rt_Tiaa_Cntr_4 = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Tiaa_Cntr_4", "RT-TIAA-CNTR-4", FieldType.STRING, 
            10);
        record_Id_Ex_File_3_Rt_Tiaa_Cntr_4Redef6 = record_Id_Ex_File_3.newGroupInGroup("record_Id_Ex_File_3_Rt_Tiaa_Cntr_4Redef6", "Redefines", record_Id_Ex_File_3_Rt_Tiaa_Cntr_4);
        record_Id_Ex_File_3_Rt_Tiaa_No_4 = record_Id_Ex_File_3_Rt_Tiaa_Cntr_4Redef6.newFieldInGroup("record_Id_Ex_File_3_Rt_Tiaa_No_4", "RT-TIAA-NO-4", 
            FieldType.STRING, 8);
        record_Id_Ex_File_3_Rt_Tiaa_Fill_4 = record_Id_Ex_File_3_Rt_Tiaa_Cntr_4Redef6.newFieldInGroup("record_Id_Ex_File_3_Rt_Tiaa_Fill_4", "RT-TIAA-FILL-4", 
            FieldType.STRING, 2);
        record_Id_Ex_File_3_Rt_Cref_Cert_4 = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Cref_Cert_4", "RT-CREF-CERT-4", FieldType.STRING, 
            10);
        record_Id_Ex_File_3_Rt_Cref_Cert_4Redef7 = record_Id_Ex_File_3.newGroupInGroup("record_Id_Ex_File_3_Rt_Cref_Cert_4Redef7", "Redefines", record_Id_Ex_File_3_Rt_Cref_Cert_4);
        record_Id_Ex_File_3_Rt_Cref_No_4 = record_Id_Ex_File_3_Rt_Cref_Cert_4Redef7.newFieldInGroup("record_Id_Ex_File_3_Rt_Cref_No_4", "RT-CREF-NO-4", 
            FieldType.STRING, 8);
        record_Id_Ex_File_3_Rt_Cref_Fill_4 = record_Id_Ex_File_3_Rt_Cref_Cert_4Redef7.newFieldInGroup("record_Id_Ex_File_3_Rt_Cref_Fill_4", "RT-CREF-FILL-4", 
            FieldType.STRING, 2);
        record_Id_Ex_File_3_Rt_Date_Table = record_Id_Ex_File_3.newFieldArrayInGroup("record_Id_Ex_File_3_Rt_Date_Table", "RT-DATE-TABLE", FieldType.STRING, 
            8, new DbsArrayController(1,6));
        record_Id_Ex_File_3_Rt_Age_First_Pymt = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Age_First_Pymt", "RT-AGE-FIRST-PYMT", FieldType.STRING, 
            4);
        record_Id_Ex_File_3_Rt_Ownership = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Ownership", "RT-OWNERSHIP", FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Alloc_Disc = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Alloc_Disc", "RT-ALLOC-DISC", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Currency = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Currency", "RT-CURRENCY", FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Tiaa_Extr_Form = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Tiaa_Extr_Form", "RT-TIAA-EXTR-FORM", FieldType.STRING, 
            9);
        record_Id_Ex_File_3_Rt_Cref_Extr_Form = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Cref_Extr_Form", "RT-CREF-EXTR-FORM", FieldType.STRING, 
            9);
        record_Id_Ex_File_3_Rt_Tiaa_Extr_Ed = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Tiaa_Extr_Ed", "RT-TIAA-EXTR-ED", FieldType.STRING, 
            9);
        record_Id_Ex_File_3_Rt_Cref_Extr_Ed = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Cref_Extr_Ed", "RT-CREF-EXTR-ED", FieldType.STRING, 
            9);
        record_Id_Ex_File_3_Rt_Annuitant_Name = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Annuitant_Name", "RT-ANNUITANT-NAME", FieldType.STRING, 
            38);
        record_Id_Ex_File_3_Rt_Eop_Name = record_Id_Ex_File_3.newGroupInGroup("record_Id_Ex_File_3_Rt_Eop_Name", "RT-EOP-NAME");
        record_Id_Ex_File_3_Rt_Eop_Last_Name = record_Id_Ex_File_3_Rt_Eop_Name.newFieldInGroup("record_Id_Ex_File_3_Rt_Eop_Last_Name", "RT-EOP-LAST-NAME", 
            FieldType.STRING, 16);
        record_Id_Ex_File_3_Rt_Eop_First_Name = record_Id_Ex_File_3_Rt_Eop_Name.newFieldInGroup("record_Id_Ex_File_3_Rt_Eop_First_Name", "RT-EOP-FIRST-NAME", 
            FieldType.STRING, 10);
        record_Id_Ex_File_3_Rt_Eop_Middle_Name = record_Id_Ex_File_3_Rt_Eop_Name.newFieldInGroup("record_Id_Ex_File_3_Rt_Eop_Middle_Name", "RT-EOP-MIDDLE-NAME", 
            FieldType.STRING, 10);
        record_Id_Ex_File_3_Rt_Eop_Middle_NameRedef8 = record_Id_Ex_File_3_Rt_Eop_Name.newGroupInGroup("record_Id_Ex_File_3_Rt_Eop_Middle_NameRedef8", 
            "Redefines", record_Id_Ex_File_3_Rt_Eop_Middle_Name);
        record_Id_Ex_File_3_Rt_Eop_Mid_Init = record_Id_Ex_File_3_Rt_Eop_Middle_NameRedef8.newFieldInGroup("record_Id_Ex_File_3_Rt_Eop_Mid_Init", "RT-EOP-MID-INIT", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Eop_Mid_Filler = record_Id_Ex_File_3_Rt_Eop_Middle_NameRedef8.newFieldInGroup("record_Id_Ex_File_3_Rt_Eop_Mid_Filler", 
            "RT-EOP-MID-FILLER", FieldType.STRING, 9);
        record_Id_Ex_File_3_Rt_Eop_Title = record_Id_Ex_File_3_Rt_Eop_Name.newFieldInGroup("record_Id_Ex_File_3_Rt_Eop_Title", "RT-EOP-TITLE", FieldType.STRING, 
            4);
        record_Id_Ex_File_3_Rt_Eop_Prefix = record_Id_Ex_File_3_Rt_Eop_Name.newFieldInGroup("record_Id_Ex_File_3_Rt_Eop_Prefix", "RT-EOP-PREFIX", FieldType.STRING, 
            4);
        record_Id_Ex_File_3_Rt_Soc_Sec = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Soc_Sec", "RT-SOC-SEC", FieldType.STRING, 11);
        record_Id_Ex_File_3_Rt_Pin_No = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Pin_No", "RT-PIN-NO", FieldType.STRING, 12);
        record_Id_Ex_File_3_Rt_Address_Table = record_Id_Ex_File_3.newFieldArrayInGroup("record_Id_Ex_File_3_Rt_Address_Table", "RT-ADDRESS-TABLE", FieldType.STRING, 
            35, new DbsArrayController(1,6));
        record_Id_Ex_File_3_Rt_Eop_Zipcode = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Eop_Zipcode", "RT-EOP-ZIPCODE", FieldType.STRING, 
            5);
        record_Id_Ex_File_3_Rt_Participant_Status = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Participant_Status", "RT-PARTICIPANT-STATUS", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Email_Address = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Email_Address", "RT-EMAIL-ADDRESS", FieldType.STRING, 
            50);
        record_Id_Ex_File_3_Rt_Request_Package_Type = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Request_Package_Type", "RT-REQUEST-PACKAGE-TYPE", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Portfolio_Selected = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Portfolio_Selected", "RT-PORTFOLIO-SELECTED", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Divorce_Contract = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Divorce_Contract", "RT-DIVORCE-CONTRACT", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Access_Cd_Ind = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Access_Cd_Ind", "RT-ACCESS-CD-IND", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Address_Change = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Address_Change", "RT-ADDRESS-CHANGE", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Package_Version = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Package_Version", "RT-PACKAGE-VERSION", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Unit_Name = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Unit_Name", "RT-UNIT-NAME", FieldType.STRING, 10);
        record_Id_Ex_File_3_Rt_Future_Filler = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Future_Filler", "RT-FUTURE-FILLER", FieldType.STRING, 
            5);
        record_Id_Ex_File_3_Rt_Bene_Estate = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Bene_Estate", "RT-BENE-ESTATE", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Bene_Trust = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Bene_Trust", "RT-BENE-TRUST", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Bene_Category = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Bene_Category", "RT-BENE-CATEGORY", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Sex = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Sex", "RT-SEX", FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Inst_Name_Table = record_Id_Ex_File_3.newFieldArrayInGroup("record_Id_Ex_File_3_Rt_Inst_Name_Table", "RT-INST-NAME-TABLE", 
            FieldType.STRING, 30, new DbsArrayController(1,10));
        record_Id_Ex_File_3_Rt_Inst_Name = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Inst_Name", "RT-INST-NAME", FieldType.STRING, 76);
        record_Id_Ex_File_3_Rt_Inst_Address_Table = record_Id_Ex_File_3.newFieldArrayInGroup("record_Id_Ex_File_3_Rt_Inst_Address_Table", "RT-INST-ADDRESS-TABLE", 
            FieldType.STRING, 35, new DbsArrayController(1,6));
        record_Id_Ex_File_3_Rt_Inst_Zip_Cd = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Inst_Zip_Cd", "RT-INST-ZIP-CD", FieldType.STRING, 
            5);
        record_Id_Ex_File_3_Rt_Mail_Instructions = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Mail_Instructions", "RT-MAIL-INSTRUCTIONS", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Orig_Iss_State = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Orig_Iss_State", "RT-ORIG-ISS-STATE", FieldType.STRING, 
            2);
        record_Id_Ex_File_3_Rt_Current_Iss_State = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Current_Iss_State", "RT-CURRENT-ISS-STATE", 
            FieldType.STRING, 2);
        record_Id_Ex_File_3_Rt_Eop_State_Name = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Eop_State_Name", "RT-EOP-STATE-NAME", FieldType.STRING, 
            15);
        record_Id_Ex_File_3_Rt_Lob = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Lob", "RT-LOB", FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Lob_Type = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Lob_Type", "RT-LOB-TYPE", FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Inst_Code = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Inst_Code", "RT-INST-CODE", FieldType.STRING, 4);
        record_Id_Ex_File_3_Rt_Inst_Bill_Cd = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Inst_Bill_Cd", "RT-INST-BILL-CD", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Inst_Permit_Trans = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Inst_Permit_Trans", "RT-INST-PERMIT-TRANS", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Perm_Gra_Trans = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Perm_Gra_Trans", "RT-PERM-GRA-TRANS", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Inst_Full_Immed_Vest = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Inst_Full_Immed_Vest", "RT-INST-FULL-IMMED-VEST", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Inst_Cashable_Ra = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Inst_Cashable_Ra", "RT-INST-CASHABLE-RA", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Inst_Cashable_Gra = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Inst_Cashable_Gra", "RT-INST-CASHABLE-GRA", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Inst_Fixed_Per_Opt = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Inst_Fixed_Per_Opt", "RT-INST-FIXED-PER-OPT", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Inst_Cntrl_Consent = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Inst_Cntrl_Consent", "RT-INST-CNTRL-CONSENT", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Eop = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Eop", "RT-EOP", FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Gsra_Inst_St_Code = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Gsra_Inst_St_Code", "RT-GSRA-INST-ST-CODE", 
            FieldType.STRING, 2);
        record_Id_Ex_File_3_Rt_Region_Cd = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Region_Cd", "RT-REGION-CD", FieldType.STRING, 3);
        record_Id_Ex_File_3_Rt_Region_CdRedef9 = record_Id_Ex_File_3.newGroupInGroup("record_Id_Ex_File_3_Rt_Region_CdRedef9", "Redefines", record_Id_Ex_File_3_Rt_Region_Cd);
        record_Id_Ex_File_3_Rt_Region = record_Id_Ex_File_3_Rt_Region_CdRedef9.newFieldInGroup("record_Id_Ex_File_3_Rt_Region", "RT-REGION", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Branch = record_Id_Ex_File_3_Rt_Region_CdRedef9.newFieldInGroup("record_Id_Ex_File_3_Rt_Branch", "RT-BRANCH", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Need = record_Id_Ex_File_3_Rt_Region_CdRedef9.newFieldInGroup("record_Id_Ex_File_3_Rt_Need", "RT-NEED", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Staff_Id = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Staff_Id", "RT-STAFF-ID", FieldType.STRING, 3);
        record_Id_Ex_File_3_Rt_Appl_Source = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Appl_Source", "RT-APPL-SOURCE", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Eop_Appl_Status = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Eop_Appl_Status", "RT-EOP-APPL-STATUS", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Eop_Addl_Cref_Req = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Eop_Addl_Cref_Req", "RT-EOP-ADDL-CREF-REQ", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Extract_Date = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Extract_Date", "RT-EXTRACT-DATE", FieldType.STRING, 
            6);
        record_Id_Ex_File_3_Rt_Gsra_Loan_Ind = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Gsra_Loan_Ind", "RT-GSRA-LOAN-IND", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Spec_Ppg_Ind = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Spec_Ppg_Ind", "RT-SPEC-PPG-IND", FieldType.STRING, 
            2);
        record_Id_Ex_File_3_Rt_Mail_Pull_Cd = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Mail_Pull_Cd", "RT-MAIL-PULL-CD", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Mit_Log_Dt_Tm = record_Id_Ex_File_3.newFieldArrayInGroup("record_Id_Ex_File_3_Rt_Mit_Log_Dt_Tm", "RT-MIT-LOG-DT-TM", FieldType.STRING, 
            15, new DbsArrayController(1,5));
        record_Id_Ex_File_3_Rt_Addr_Page_Name = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Addr_Page_Name", "RT-ADDR-PAGE-NAME", FieldType.STRING, 
            81);
        record_Id_Ex_File_3_Rt_Addr_Page_NameRedef10 = record_Id_Ex_File_3.newGroupInGroup("record_Id_Ex_File_3_Rt_Addr_Page_NameRedef10", "Redefines", 
            record_Id_Ex_File_3_Rt_Addr_Page_Name);
        record_Id_Ex_File_3_Rt_Addr_Pref = record_Id_Ex_File_3_Rt_Addr_Page_NameRedef10.newFieldInGroup("record_Id_Ex_File_3_Rt_Addr_Pref", "RT-ADDR-PREF", 
            FieldType.STRING, 8);
        record_Id_Ex_File_3_Rt_Addr_Filler_1 = record_Id_Ex_File_3_Rt_Addr_Page_NameRedef10.newFieldInGroup("record_Id_Ex_File_3_Rt_Addr_Filler_1", "RT-ADDR-FILLER-1", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Addr_Frst = record_Id_Ex_File_3_Rt_Addr_Page_NameRedef10.newFieldInGroup("record_Id_Ex_File_3_Rt_Addr_Frst", "RT-ADDR-FRST", 
            FieldType.STRING, 30);
        record_Id_Ex_File_3_Rt_Addr_Filler_2 = record_Id_Ex_File_3_Rt_Addr_Page_NameRedef10.newFieldInGroup("record_Id_Ex_File_3_Rt_Addr_Filler_2", "RT-ADDR-FILLER-2", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Addr_Mddle = record_Id_Ex_File_3_Rt_Addr_Page_NameRedef10.newFieldInGroup("record_Id_Ex_File_3_Rt_Addr_Mddle", "RT-ADDR-MDDLE", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Addr_Filler_3 = record_Id_Ex_File_3_Rt_Addr_Page_NameRedef10.newFieldInGroup("record_Id_Ex_File_3_Rt_Addr_Filler_3", "RT-ADDR-FILLER-3", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Addr_Last = record_Id_Ex_File_3_Rt_Addr_Page_NameRedef10.newFieldInGroup("record_Id_Ex_File_3_Rt_Addr_Last", "RT-ADDR-LAST", 
            FieldType.STRING, 30);
        record_Id_Ex_File_3_Rt_Addr_Filler_4 = record_Id_Ex_File_3_Rt_Addr_Page_NameRedef10.newFieldInGroup("record_Id_Ex_File_3_Rt_Addr_Filler_4", "RT-ADDR-FILLER-4", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Addr_Sffx = record_Id_Ex_File_3_Rt_Addr_Page_NameRedef10.newFieldInGroup("record_Id_Ex_File_3_Rt_Addr_Sffx", "RT-ADDR-SFFX", 
            FieldType.STRING, 8);
        record_Id_Ex_File_3_Rt_Welcome_Name = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Welcome_Name", "RT-WELCOME-NAME", FieldType.STRING, 
            39);
        record_Id_Ex_File_3_Rt_Welcome_NameRedef11 = record_Id_Ex_File_3.newGroupInGroup("record_Id_Ex_File_3_Rt_Welcome_NameRedef11", "Redefines", record_Id_Ex_File_3_Rt_Welcome_Name);
        record_Id_Ex_File_3_Rt_Welcome_Pref = record_Id_Ex_File_3_Rt_Welcome_NameRedef11.newFieldInGroup("record_Id_Ex_File_3_Rt_Welcome_Pref", "RT-WELCOME-PREF", 
            FieldType.STRING, 8);
        record_Id_Ex_File_3_Rt_Welcome_Filler_1 = record_Id_Ex_File_3_Rt_Welcome_NameRedef11.newFieldInGroup("record_Id_Ex_File_3_Rt_Welcome_Filler_1", 
            "RT-WELCOME-FILLER-1", FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Welcome_Last = record_Id_Ex_File_3_Rt_Welcome_NameRedef11.newFieldInGroup("record_Id_Ex_File_3_Rt_Welcome_Last", "RT-WELCOME-LAST", 
            FieldType.STRING, 30);
        record_Id_Ex_File_3_Rt_Pin_Name = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Pin_Name", "RT-PIN-NAME", FieldType.STRING, 63);
        record_Id_Ex_File_3_Rt_Pin_NameRedef12 = record_Id_Ex_File_3.newGroupInGroup("record_Id_Ex_File_3_Rt_Pin_NameRedef12", "Redefines", record_Id_Ex_File_3_Rt_Pin_Name);
        record_Id_Ex_File_3_Rt_Pin_First = record_Id_Ex_File_3_Rt_Pin_NameRedef12.newFieldInGroup("record_Id_Ex_File_3_Rt_Pin_First", "RT-PIN-FIRST", 
            FieldType.STRING, 30);
        record_Id_Ex_File_3_Rt_Pin_Filler_1 = record_Id_Ex_File_3_Rt_Pin_NameRedef12.newFieldInGroup("record_Id_Ex_File_3_Rt_Pin_Filler_1", "RT-PIN-FILLER-1", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Pin_Mddle = record_Id_Ex_File_3_Rt_Pin_NameRedef12.newFieldInGroup("record_Id_Ex_File_3_Rt_Pin_Mddle", "RT-PIN-MDDLE", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Pin_Filler_2 = record_Id_Ex_File_3_Rt_Pin_NameRedef12.newFieldInGroup("record_Id_Ex_File_3_Rt_Pin_Filler_2", "RT-PIN-FILLER-2", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Pin_Last = record_Id_Ex_File_3_Rt_Pin_NameRedef12.newFieldInGroup("record_Id_Ex_File_3_Rt_Pin_Last", "RT-PIN-LAST", FieldType.STRING, 
            30);
        record_Id_Ex_File_3_Rt_Zip = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Zip", "RT-ZIP", FieldType.STRING, 5);
        record_Id_Ex_File_3_Rt_ZipRedef13 = record_Id_Ex_File_3.newGroupInGroup("record_Id_Ex_File_3_Rt_ZipRedef13", "Redefines", record_Id_Ex_File_3_Rt_Zip);
        record_Id_Ex_File_3_Rt_Zip_Plus4 = record_Id_Ex_File_3_Rt_ZipRedef13.newFieldInGroup("record_Id_Ex_File_3_Rt_Zip_Plus4", "RT-ZIP-PLUS4", FieldType.STRING, 
            4);
        record_Id_Ex_File_3_Rt_Zip_Plus1 = record_Id_Ex_File_3_Rt_ZipRedef13.newFieldInGroup("record_Id_Ex_File_3_Rt_Zip_Plus1", "RT-ZIP-PLUS1", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Suspension = record_Id_Ex_File_3.newFieldArrayInGroup("record_Id_Ex_File_3_Rt_Suspension", "RT-SUSPENSION", FieldType.STRING, 
            1, new DbsArrayController(1,20));
        record_Id_Ex_File_3_Rt_Mult_App_Version = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Mult_App_Version", "RT-MULT-APP-VERSION", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Mult_App_Status = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Mult_App_Status", "RT-MULT-APP-STATUS", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Mult_App_Lob = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Mult_App_Lob", "RT-MULT-APP-LOB", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Mult_App_Lob_Type = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Mult_App_Lob_Type", "RT-MULT-APP-LOB-TYPE", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Mult_App_Ppg = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Mult_App_Ppg", "RT-MULT-APP-PPG", FieldType.STRING, 
            6);
        record_Id_Ex_File_3_Rt_K12_Ppg = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_K12_Ppg", "RT-K12-PPG", FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Ira_Rollover_Type = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Ira_Rollover_Type", "RT-IRA-ROLLOVER-TYPE", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Ira_Record_Type = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Ira_Record_Type", "RT-IRA-RECORD-TYPE", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Rollover_Amt = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Rollover_Amt", "RT-ROLLOVER-AMT", FieldType.STRING, 
            14);
        record_Id_Ex_File_3_Rt_Mit_Unit = record_Id_Ex_File_3.newFieldArrayInGroup("record_Id_Ex_File_3_Rt_Mit_Unit", "RT-MIT-UNIT", FieldType.STRING, 
            8, new DbsArrayController(1,5));
        record_Id_Ex_File_3_Rt_Mit_Wpid = record_Id_Ex_File_3.newFieldArrayInGroup("record_Id_Ex_File_3_Rt_Mit_Wpid", "RT-MIT-WPID", FieldType.STRING, 
            6, new DbsArrayController(1,5));
        record_Id_Ex_File_3_Rt_New_Bene_Ind = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_New_Bene_Ind", "RT-NEW-BENE-IND", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Irc_Sectn_Cde = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Irc_Sectn_Cde", "RT-IRC-SECTN-CDE", FieldType.NUMERIC, 
            2);
        record_Id_Ex_File_3_Rt_Released_Dt = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Released_Dt", "RT-RELEASED-DT", FieldType.NUMERIC, 
            8);
        record_Id_Ex_File_3_Rt_Irc_Sectn_Grp = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Irc_Sectn_Grp", "RT-IRC-SECTN-GRP", FieldType.NUMERIC, 
            2);
        record_Id_Ex_File_3_Rt_Enroll_Request_Type = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Enroll_Request_Type", "RT-ENROLL-REQUEST-TYPE", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Erisa_Inst = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Erisa_Inst", "RT-ERISA-INST", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Print_Destination = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Print_Destination", "RT-PRINT-DESTINATION", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Correction_Type = record_Id_Ex_File_3.newFieldArrayInGroup("record_Id_Ex_File_3_Rt_Correction_Type", "RT-CORRECTION-TYPE", 
            FieldType.STRING, 1, new DbsArrayController(1,20));
        record_Id_Ex_File_3_Rt_Processor_Name = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Processor_Name", "RT-PROCESSOR-NAME", FieldType.STRING, 
            40);
        record_Id_Ex_File_3_Rt_Mail_Date = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Mail_Date", "RT-MAIL-DATE", FieldType.STRING, 17);
        record_Id_Ex_File_3_Rt_Alloc_Fmt = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Alloc_Fmt", "RT-ALLOC-FMT", FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Phone_No = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Phone_No", "RT-PHONE-NO", FieldType.STRING, 20);
        record_Id_Ex_File_3_Rt_Fund_Cde = record_Id_Ex_File_3.newFieldArrayInGroup("record_Id_Ex_File_3_Rt_Fund_Cde", "RT-FUND-CDE", FieldType.STRING, 
            10, new DbsArrayController(1,100));
        record_Id_Ex_File_3_Rt_Alloc_Pct = record_Id_Ex_File_3.newFieldArrayInGroup("record_Id_Ex_File_3_Rt_Alloc_Pct", "RT-ALLOC-PCT", FieldType.STRING, 
            3, new DbsArrayController(1,100));
        record_Id_Ex_File_3_Rt_Oia_Ind = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Oia_Ind", "RT-OIA-IND", FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Product_Cde = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Product_Cde", "RT-PRODUCT-CDE", FieldType.STRING, 
            3);
        record_Id_Ex_File_3_Rt_Acct_Sum_Sheet_Type = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Acct_Sum_Sheet_Type", "RT-ACCT-SUM-SHEET-TYPE", 
            FieldType.STRING, 56);
        record_Id_Ex_File_3_Rt_Sg_Plan_No = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Sg_Plan_No", "RT-SG-PLAN-NO", FieldType.STRING, 
            6);
        record_Id_Ex_File_3_Rt_Sg_Subplan_No = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Sg_Subplan_No", "RT-SG-SUBPLAN-NO", FieldType.STRING, 
            6);
        record_Id_Ex_File_3_Rt_Sg_Subplan_Type = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Sg_Subplan_Type", "RT-SG-SUBPLAN-TYPE", FieldType.STRING, 
            3);
        record_Id_Ex_File_3_Rt_Omni_Issue_Ind = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Omni_Issue_Ind", "RT-OMNI-ISSUE-IND", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Effective_Dt = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Effective_Dt", "RT-EFFECTIVE-DT", FieldType.STRING, 
            8);
        record_Id_Ex_File_3_Rt_Employer_Name = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Employer_Name", "RT-EMPLOYER-NAME", FieldType.STRING, 
            40);
        record_Id_Ex_File_3_Rt_Tiaa_Rate = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Tiaa_Rate", "RT-TIAA-RATE", FieldType.STRING, 8);
        record_Id_Ex_File_3_Rt_Sg_Plan_Id = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Sg_Plan_Id", "RT-SG-PLAN-ID", FieldType.STRING, 
            6);
        record_Id_Ex_File_3_Rt_Tiaa_Pg4_Numb = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Tiaa_Pg4_Numb", "RT-TIAA-PG4-NUMB", FieldType.STRING, 
            15);
        record_Id_Ex_File_3_Rt_Cref_Pg4_Numb = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Cref_Pg4_Numb", "RT-CREF-PG4-NUMB", FieldType.STRING, 
            15);
        record_Id_Ex_File_3_Rt_Dflt_Access_Code = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Dflt_Access_Code", "RT-DFLT-ACCESS-CODE", 
            FieldType.STRING, 9);
        record_Id_Ex_File_3_Rt_Single_Issue_Ind = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Single_Issue_Ind", "RT-SINGLE-ISSUE-IND", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Spec_Fund_Ind = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Spec_Fund_Ind", "RT-SPEC-FUND-IND", FieldType.STRING, 
            3);
        record_Id_Ex_File_3_Rt_Product_Price_Level = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Product_Price_Level", "RT-PRODUCT-PRICE-LEVEL", 
            FieldType.STRING, 5);
        record_Id_Ex_File_3_Rt_Roth_Ind = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Roth_Ind", "RT-ROTH-IND", FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Plan_Issue_State = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Plan_Issue_State", "RT-PLAN-ISSUE-STATE", 
            FieldType.STRING, 2);
        record_Id_Ex_File_3_Rt_Client_Id = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Client_Id", "RT-CLIENT-ID", FieldType.STRING, 6);
        record_Id_Ex_File_3_Rt_Portfolio_Model = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Portfolio_Model", "RT-PORTFOLIO-MODEL", FieldType.STRING, 
            4);
        record_Id_Ex_File_3_Rt_Auto_Enroll_Ind = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Auto_Enroll_Ind", "RT-AUTO-ENROLL-IND", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Auto_Save_Ind = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Auto_Save_Ind", "RT-AUTO-SAVE-IND", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_As_Cur_Dflt_Amt = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_As_Cur_Dflt_Amt", "RT-AS-CUR-DFLT-AMT", FieldType.STRING, 
            10);
        record_Id_Ex_File_3_Rt_As_Incr_Amt = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_As_Incr_Amt", "RT-AS-INCR-AMT", FieldType.STRING, 
            7);
        record_Id_Ex_File_3_Rt_As_Max_Pct = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_As_Max_Pct", "RT-AS-MAX-PCT", FieldType.STRING, 
            7);
        record_Id_Ex_File_3_Rt_Ae_Opt_Out_Days = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Ae_Opt_Out_Days", "RT-AE-OPT-OUT-DAYS", FieldType.STRING, 
            2);
        record_Id_Ex_File_3_Rt_Tsv_Ind = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Tsv_Ind", "RT-TSV-IND", FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Tiaa_Indx_Guarntd_Rte = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Tiaa_Indx_Guarntd_Rte", "RT-TIAA-INDX-GUARNTD-RTE", 
            FieldType.STRING, 6);
        record_Id_Ex_File_3_Rt_Agent_Crd_No = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Agent_Crd_No", "RT-AGENT-CRD-NO", FieldType.STRING, 
            15);
        record_Id_Ex_File_3_Rt_Crd_Pull_Ind = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Crd_Pull_Ind", "RT-CRD-PULL-IND", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Agent_Name = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Agent_Name", "RT-AGENT-NAME", FieldType.STRING, 
            90);
        record_Id_Ex_File_3_Rt_Agent_Work_Addr1 = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Agent_Work_Addr1", "RT-AGENT-WORK-ADDR1", 
            FieldType.STRING, 35);
        record_Id_Ex_File_3_Rt_Agent_Work_Addr2 = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Agent_Work_Addr2", "RT-AGENT-WORK-ADDR2", 
            FieldType.STRING, 35);
        record_Id_Ex_File_3_Rt_Agent_Work_City = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Agent_Work_City", "RT-AGENT-WORK-CITY", FieldType.STRING, 
            35);
        record_Id_Ex_File_3_Rt_Agent_Work_State = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Agent_Work_State", "RT-AGENT-WORK-STATE", 
            FieldType.STRING, 2);
        record_Id_Ex_File_3_Rt_Agent_Work_Zip = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Agent_Work_Zip", "RT-AGENT-WORK-ZIP", FieldType.STRING, 
            10);
        record_Id_Ex_File_3_Rt_Agent_Work_Phone = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Agent_Work_Phone", "RT-AGENT-WORK-PHONE", 
            FieldType.STRING, 25);
        record_Id_Ex_File_3_Rt_Tic_Ind = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Tic_Ind", "RT-TIC-IND", FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Tic_Startdate = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Tic_Startdate", "RT-TIC-STARTDATE", FieldType.STRING, 
            8);
        record_Id_Ex_File_3_Rt_Tic_Enddate = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Tic_Enddate", "RT-TIC-ENDDATE", FieldType.STRING, 
            8);
        record_Id_Ex_File_3_Rt_Tic_Percentage = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Tic_Percentage", "RT-TIC-PERCENTAGE", FieldType.STRING, 
            9);
        record_Id_Ex_File_3_Rt_Tic_Postdays = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Tic_Postdays", "RT-TIC-POSTDAYS", FieldType.STRING, 
            2);
        record_Id_Ex_File_3_Rt_Tic_Limit = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Tic_Limit", "RT-TIC-LIMIT", FieldType.STRING, 11);
        record_Id_Ex_File_3_Rt_Tic_Postfreq = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Tic_Postfreq", "RT-TIC-POSTFREQ", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Tic_Pl_Level = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Tic_Pl_Level", "RT-TIC-PL-LEVEL", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Tic_Windowdays = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Tic_Windowdays", "RT-TIC-WINDOWDAYS", FieldType.STRING, 
            3);
        record_Id_Ex_File_3_Rt_Tic_Reqdlywindow = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Tic_Reqdlywindow", "RT-TIC-REQDLYWINDOW", 
            FieldType.STRING, 3);
        record_Id_Ex_File_3_Rt_Tic_Recap_Prov = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Tic_Recap_Prov", "RT-TIC-RECAP-PROV", FieldType.STRING, 
            3);
        record_Id_Ex_File_3_Rt_Ecs_Dcs_E_Dlvry_Ind = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Ecs_Dcs_E_Dlvry_Ind", "RT-ECS-DCS-E-DLVRY-IND", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Ecs_Dcs_Annty_Optn_Ind = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Ecs_Dcs_Annty_Optn_Ind", "RT-ECS-DCS-ANNTY-OPTN-IND", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Full_Middle_Name = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Full_Middle_Name", "RT-FULL-MIDDLE-NAME", 
            FieldType.STRING, 30);
        record_Id_Ex_File_3_Rt_Substitution_Contract_Ind = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Substitution_Contract_Ind", "RT-SUBSTITUTION-CONTRACT-IND", 
            FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Bundle_Type = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Bundle_Type", "RT-BUNDLE-TYPE", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Non_Proprietary_Pkg_Ind = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Non_Proprietary_Pkg_Ind", "RT-NON-PROPRIETARY-PKG-IND", 
            FieldType.STRING, 5);
        record_Id_Ex_File_3_Rt_Multi_Plan_Table = record_Id_Ex_File_3.newFieldArrayInGroup("record_Id_Ex_File_3_Rt_Multi_Plan_Table", "RT-MULTI-PLAN-TABLE", 
            FieldType.STRING, 12, new DbsArrayController(1,15));
        record_Id_Ex_File_3_Rt_Multi_Plan_TableRedef14 = record_Id_Ex_File_3.newGroupInGroup("record_Id_Ex_File_3_Rt_Multi_Plan_TableRedef14", "Redefines", 
            record_Id_Ex_File_3_Rt_Multi_Plan_Table);
        record_Id_Ex_File_3_Rt_Multi_Plan_Info = record_Id_Ex_File_3_Rt_Multi_Plan_TableRedef14.newGroupArrayInGroup("record_Id_Ex_File_3_Rt_Multi_Plan_Info", 
            "RT-MULTI-PLAN-INFO", new DbsArrayController(1,15));
        record_Id_Ex_File_3_Rt_Multi_Plan_No = record_Id_Ex_File_3_Rt_Multi_Plan_Info.newFieldInGroup("record_Id_Ex_File_3_Rt_Multi_Plan_No", "RT-MULTI-PLAN-NO", 
            FieldType.STRING, 6);
        record_Id_Ex_File_3_Rt_Multi_Sub_Plan = record_Id_Ex_File_3_Rt_Multi_Plan_Info.newFieldInGroup("record_Id_Ex_File_3_Rt_Multi_Sub_Plan", "RT-MULTI-SUB-PLAN", 
            FieldType.STRING, 6);
        record_Id_Ex_File_3_Rt_Filler = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Filler", "RT-FILLER", FieldType.STRING, 5);
        record_Id_Ex_File_3_Rt_Tiaa_Init_Prem = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Tiaa_Init_Prem", "RT-TIAA-INIT-PREM", FieldType.DECIMAL, 
            10,2);
        record_Id_Ex_File_3_Rt_Cref_Init_Prem = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Cref_Init_Prem", "RT-CREF-INIT-PREM", FieldType.DECIMAL, 
            10,2);
        record_Id_Ex_File_3_Rt_Related_Contract_Info = record_Id_Ex_File_3.newFieldArrayInGroup("record_Id_Ex_File_3_Rt_Related_Contract_Info", "RT-RELATED-CONTRACT-INFO", 
            FieldType.STRING, 49, new DbsArrayController(1,30));
        record_Id_Ex_File_3_Rt_Related_Contract_InfoRedef15 = record_Id_Ex_File_3.newGroupInGroup("record_Id_Ex_File_3_Rt_Related_Contract_InfoRedef15", 
            "Redefines", record_Id_Ex_File_3_Rt_Related_Contract_Info);
        record_Id_Ex_File_3_Rt_Related_Contract = record_Id_Ex_File_3_Rt_Related_Contract_InfoRedef15.newGroupArrayInGroup("record_Id_Ex_File_3_Rt_Related_Contract", 
            "RT-RELATED-CONTRACT", new DbsArrayController(1,30));
        record_Id_Ex_File_3_Rt_Related_Contract_Type = record_Id_Ex_File_3_Rt_Related_Contract.newFieldInGroup("record_Id_Ex_File_3_Rt_Related_Contract_Type", 
            "RT-RELATED-CONTRACT-TYPE", FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Related_Tiaa_No = record_Id_Ex_File_3_Rt_Related_Contract.newFieldInGroup("record_Id_Ex_File_3_Rt_Related_Tiaa_No", "RT-RELATED-TIAA-NO", 
            FieldType.STRING, 10);
        record_Id_Ex_File_3_Rt_Related_Tiaa_Issue_Date = record_Id_Ex_File_3_Rt_Related_Contract.newFieldInGroup("record_Id_Ex_File_3_Rt_Related_Tiaa_Issue_Date", 
            "RT-RELATED-TIAA-ISSUE-DATE", FieldType.NUMERIC, 8);
        record_Id_Ex_File_3_Rt_Related_First_Payment_Date = record_Id_Ex_File_3_Rt_Related_Contract.newFieldInGroup("record_Id_Ex_File_3_Rt_Related_First_Payment_Date", 
            "RT-RELATED-FIRST-PAYMENT-DATE", FieldType.NUMERIC, 8);
        record_Id_Ex_File_3_Rt_Related_Tiaa_Total_Amt = record_Id_Ex_File_3_Rt_Related_Contract.newFieldInGroup("record_Id_Ex_File_3_Rt_Related_Tiaa_Total_Amt", 
            "RT-RELATED-TIAA-TOTAL-AMT", FieldType.DECIMAL, 10,2);
        record_Id_Ex_File_3_Rt_Related_Last_Payment_Date = record_Id_Ex_File_3_Rt_Related_Contract.newFieldInGroup("record_Id_Ex_File_3_Rt_Related_Last_Payment_Date", 
            "RT-RELATED-LAST-PAYMENT-DATE", FieldType.NUMERIC, 8);
        record_Id_Ex_File_3_Rt_Related_Payment_Frequency = record_Id_Ex_File_3_Rt_Related_Contract.newFieldInGroup("record_Id_Ex_File_3_Rt_Related_Payment_Frequency", 
            "RT-RELATED-PAYMENT-FREQUENCY", FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Related_Tiaa_Issue_State = record_Id_Ex_File_3_Rt_Related_Contract.newFieldInGroup("record_Id_Ex_File_3_Rt_Related_Tiaa_Issue_State", 
            "RT-RELATED-TIAA-ISSUE-STATE", FieldType.STRING, 2);
        record_Id_Ex_File_3_Rt_First_Tpa_Ipro_Ind = record_Id_Ex_File_3_Rt_Related_Contract.newFieldInGroup("record_Id_Ex_File_3_Rt_First_Tpa_Ipro_Ind", 
            "RT-FIRST-TPA-IPRO-IND", FieldType.STRING, 1);
        record_Id_Ex_File_3_Rt_Fund_Source_1 = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Fund_Source_1", "RT-FUND-SOURCE-1", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Fund_Source_2 = record_Id_Ex_File_3.newFieldInGroup("record_Id_Ex_File_3_Rt_Fund_Source_2", "RT-FUND-SOURCE-2", FieldType.STRING, 
            1);
        record_Id_Ex_File_3_Rt_Fund_Cde_2 = record_Id_Ex_File_3.newFieldArrayInGroup("record_Id_Ex_File_3_Rt_Fund_Cde_2", "RT-FUND-CDE-2", FieldType.STRING, 
            10, new DbsArrayController(1,100));
        record_Id_Ex_File_3_Rt_Alloc_Pct_2 = record_Id_Ex_File_3.newFieldArrayInGroup("record_Id_Ex_File_3_Rt_Alloc_Pct_2", "RT-ALLOC-PCT-2", FieldType.STRING, 
            3, new DbsArrayController(1,100));

        this.setRecordName("LdaAppl1158");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAppl1158() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
