/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:54 PM
**        * FROM NATURAL LDA     : APPL180
************************************************************
**        * FILE NAME            : LdaAppl180.java
**        * CLASS NAME           : LdaAppl180
**        * INSTANCE NAME        : LdaAppl180
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl180 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_acis_Reprint_Fl_View;
    private DbsField acis_Reprint_Fl_View_Rp_Record_Type;
    private DbsField acis_Reprint_Fl_View_Rp_Maintenance_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_Maintenance_Date;
    private DbsField acis_Reprint_Fl_View_Rp_Correct_Maint_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_Reprint_Pull_Code;
    private DbsField acis_Reprint_Fl_View_Rp_Reprint_Request_Type;
    private DbsField acis_Reprint_Fl_View_Rp_Print_Destination;
    private DbsField acis_Reprint_Fl_View_Rp_Tiaa_Contr;
    private DbsField acis_Reprint_Fl_View_Rp_Cref_Contr;
    private DbsField acis_Reprint_Fl_View_Rp_Tiaa_Negr_Contract;
    private DbsField acis_Reprint_Fl_View_Rp_Cref_Negr_Contract;
    private DbsField acis_Reprint_Fl_View_Rp_Lob;
    private DbsField acis_Reprint_Fl_View_Rp_Lob_Type;
    private DbsField acis_Reprint_Fl_View_Rp_Bill_Code;
    private DbsField acis_Reprint_Fl_View_Rp_Cor_Prfx_Nme;
    private DbsField acis_Reprint_Fl_View_Rp_Cor_Last_Nme;
    private DbsField acis_Reprint_Fl_View_Rp_Cor_First_Nme;
    private DbsField acis_Reprint_Fl_View_Rp_Cor_Mddle_Nme;
    private DbsField acis_Reprint_Fl_View_Rp_Cor_Sffx_Nme;
    private DbsGroup acis_Reprint_Fl_View_Rp_Address_Info;
    private DbsField acis_Reprint_Fl_View_Rp_Address_Line;
    private DbsField acis_Reprint_Fl_View_Rp_City;
    private DbsField acis_Reprint_Fl_View_Rp_Orig_Issue_State;
    private DbsField acis_Reprint_Fl_View_Rp_Current_State_Code;
    private DbsField acis_Reprint_Fl_View_Rp_Mail_Zip;
    private DbsField acis_Reprint_Fl_View_Rp_Mail_Instructions;
    private DbsField acis_Reprint_Fl_View_Rp_Soc_Sec;
    private DbsField acis_Reprint_Fl_View_Rp_Dob;
    private DbsField acis_Reprint_Fl_View_Rp_Sex;
    private DbsField acis_Reprint_Fl_View_Rp_Pin_Nbr;
    private DbsField acis_Reprint_Fl_View_Rp_Coll_Code;
    private DbsField acis_Reprint_Fl_View_Rp_Coll_St_Cd;
    private DbsField acis_Reprint_Fl_View_Rp_Irc_Sectn_Grp_Cde;
    private DbsField acis_Reprint_Fl_View_Rp_Irc_Sectn_Cde;
    private DbsField acis_Reprint_Fl_View_Rp_Ppg_Team_Cde;
    private DbsField acis_Reprint_Fl_View_Rp_Inst_Link_Cde;
    private DbsField acis_Reprint_Fl_View_Rp_T_Doi;
    private DbsField acis_Reprint_Fl_View_Rp_C_Doi;
    private DbsField acis_Reprint_Fl_View_Rp_Dt_Released;
    private DbsField acis_Reprint_Fl_View_Rp_Dt_App_Recvd;
    private DbsField acis_Reprint_Fl_View_Rp_Annuity_Start_Date;
    private DbsField acis_Reprint_Fl_View_Rp_Extract_Date;
    private DbsField acis_Reprint_Fl_View_Rp_Curr;
    private DbsField acis_Reprint_Fl_View_Rp_T_Age_1st;
    private DbsField acis_Reprint_Fl_View_Rp_Ownership;
    private DbsField acis_Reprint_Fl_View_Rp_Alloc_Discr;
    private DbsGroup acis_Reprint_Fl_View_Rp_Allocation_Info;
    private DbsField acis_Reprint_Fl_View_Rp_Allocation;
    private DbsField acis_Reprint_Fl_View_Rp_App_Source;
    private DbsField acis_Reprint_Fl_View_Rp_Region_Code;
    private DbsField acis_Reprint_Fl_View_Rp_Eop_Addl_Cref_Request;
    private DbsField acis_Reprint_Fl_View_Rp_Rlc_Cref_Pref;
    private DbsField acis_Reprint_Fl_View_Rp_Rlc_Cref_Cont;
    private DbsField acis_Reprint_Fl_View_Rp_Ira_Rollover_Type;
    private DbsField acis_Reprint_Fl_View_Rp_Ira_Record_Type;
    private DbsField acis_Reprint_Fl_View_Rp_Mult_App_Status;
    private DbsField acis_Reprint_Fl_View_Rp_Mult_App_Lob;
    private DbsField acis_Reprint_Fl_View_Rp_Mult_App_Lob_Type;
    private DbsField acis_Reprint_Fl_View_Rp_Mult_App_Ppg;
    private DbsGroup acis_Reprint_Fl_View_Rp_Correction_Data_Type;
    private DbsField acis_Reprint_Fl_View_Rp_Correction_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_Correction_Sync;
    private DbsField acis_Reprint_Fl_View_Rp_Financial_1;
    private DbsField acis_Reprint_Fl_View_Rp_Applcnt_Req_Type;
    private DbsField acis_Reprint_Fl_View_Rp_Tiaa_Service_Agent;
    private DbsGroup acis_Reprint_Fl_View_Rp_Mit_Request;
    private DbsField acis_Reprint_Fl_View_Rp_Rqst_Log_Dte_Time;
    private DbsField acis_Reprint_Fl_View_Rp_Mit_Unit;
    private DbsField acis_Reprint_Fl_View_Rp_Mit_Wpid;
    private DbsField acis_Reprint_Fl_View_Rp_Update_Date;
    private DbsField acis_Reprint_Fl_View_Rp_Update_Time;
    private DbsField acis_Reprint_Fl_View_Rp_Racf_Id;
    private DbsField acis_Reprint_Fl_View_Rp_Rlc_Cref_Cert;
    private DbsField acis_Reprint_Fl_View_Rp_Allocation_Model;
    private DbsField acis_Reprint_Fl_View_Rp_Divorce_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_Email_Address;
    private DbsField acis_Reprint_Fl_View_Rp_Participant_Status_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_Request_Pkg_Type;
    private DbsField acis_Reprint_Fl_View_Rp_E_Signed_Appl_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_Eft_Request_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_Contract_Package_Version;
    private DbsField acis_Reprint_Fl_View_Rp_Package_Mail_Type;
    private DbsField acis_Reprint_Fl_View_Rp_Duplicate_Copies_Requested;
    private DbsField acis_Reprint_Fl_View_Rp_Address_Change_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_Allocation_Fmt;
    private DbsField acis_Reprint_Fl_View_Rp_Phone_No;
    private DbsGroup acis_Reprint_Fl_View_Rp_Fund_Identifier;
    private DbsField acis_Reprint_Fl_View_Rp_Fund_Cde;
    private DbsField acis_Reprint_Fl_View_Rp_Allocation_Pct;
    private DbsField acis_Reprint_Fl_View_Rp_Sgrd_Plan_No;
    private DbsField acis_Reprint_Fl_View_Rp_Sgrd_Subplan_No;
    private DbsField acis_Reprint_Fl_View_Rp_Sgrd_Part_Ext;
    private DbsField acis_Reprint_Fl_View_Rp_Ls_Effective_Date;
    private DbsField acis_Reprint_Fl_View_Rp_Interest_Rate;
    private DbsField acis_Reprint_Fl_View_Rp_Text_Udf_1;
    private DbsGroup acis_Reprint_Fl_View_Rp_Text_Udf_1Redef1;
    private DbsField acis_Reprint_Fl_View_Rp_Single_Issue_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_Sgrd_Divsub;
    private DbsField acis_Reprint_Fl_View_Rp_Spec_Fund_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_Plan_Issue_State;
    private DbsField acis_Reprint_Fl_View_Rp_Text_Udf_2;
    private DbsGroup acis_Reprint_Fl_View_Rp_Text_Udf_2Redef2;
    private DbsField acis_Reprint_Fl_View_Rp_Orig_Resid_Issue_St;
    private DbsField acis_Reprint_Fl_View_Rp_Tsv_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_Spcl_Lgl_Pkg_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_Tsr_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_Text_Udf_3;
    private DbsGroup acis_Reprint_Fl_View_Rp_Text_Udf_3Redef3;
    private DbsField acis_Reprint_Fl_View_Rp_Sgrd_Client_Id;
    private DbsField acis_Reprint_Fl_View_Rp_Portfolio_Type;
    private DbsField acis_Reprint_Fl_View_Rp_Replacement_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_Register_Id;
    private DbsField acis_Reprint_Fl_View_Rp_Exempt_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_Incmpl_Acct_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_Autosave_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_As_Cur_Dflt_Opt;
    private DbsField acis_Reprint_Fl_View_Rp_As_Cur_Dflt_Amt;
    private DbsField acis_Reprint_Fl_View_Rp_As_Incr_Opt;
    private DbsField acis_Reprint_Fl_View_Rp_As_Incr_Amt;
    private DbsField acis_Reprint_Fl_View_Rp_As_Max_Pct;
    private DbsField acis_Reprint_Fl_View_Rp_Ae_Opt_Out_Days;
    private DbsField acis_Reprint_Fl_View_Rp_Welcome_Mail_Dt;
    private DbsField acis_Reprint_Fl_View_Rp_Legal_Mail_Dt;
    private DbsField acis_Reprint_Fl_View_Rp_Delete_User_Id;
    private DbsField acis_Reprint_Fl_View_Rp_Delete_Reason_Cd;
    private DbsField acis_Reprint_Fl_View_Rp_Dt_Deleted;
    private DbsField acis_Reprint_Fl_View_Rp_Agent_Crd_No;
    private DbsField acis_Reprint_Fl_View_Rp_Welc_E_Delivery_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_Legal_E_Delivery_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_Legal_Ann_Option;
    private DbsField acis_Reprint_Fl_View_Rp_Orchestration_Id;
    private DbsField acis_Reprint_Fl_View_Rp_Mail_Addr_Country_Cd;
    private DbsGroup acis_Reprint_Fl_View_Rp_Res_Addr_Info;
    private DbsField acis_Reprint_Fl_View_Rp_Res_Addr_Line;
    private DbsField acis_Reprint_Fl_View_Rp_Res_Addr_City;
    private DbsField acis_Reprint_Fl_View_Rp_Res_Addr_State_Code;
    private DbsField acis_Reprint_Fl_View_Rp_Res_Addr_Mail_Zip;
    private DbsField acis_Reprint_Fl_View_Rp_Res_Addr_Country_Cd;
    private DbsField acis_Reprint_Fl_View_Rp_Tic_Startdate;
    private DbsField acis_Reprint_Fl_View_Rp_Tic_Enddate;
    private DbsField acis_Reprint_Fl_View_Rp_Tic_Percentage;
    private DbsField acis_Reprint_Fl_View_Rp_Tic_Postdays;
    private DbsField acis_Reprint_Fl_View_Rp_Tic_Limit;
    private DbsField acis_Reprint_Fl_View_Rp_Tic_Postfreq;
    private DbsField acis_Reprint_Fl_View_Rp_Tic_Pl_Level;
    private DbsField acis_Reprint_Fl_View_Rp_Tic_Windowdays;
    private DbsField acis_Reprint_Fl_View_Rp_Tic_Reqdlywindow;
    private DbsField acis_Reprint_Fl_View_Rp_Tic_Recap_Prov;
    private DbsField acis_Reprint_Fl_View_Rp_Ann_Funding_Dt;
    private DbsField acis_Reprint_Fl_View_Rp_Tiaa_Ann_Issue_Dt;
    private DbsField acis_Reprint_Fl_View_Rp_Cref_Ann_Issue_Dt;
    private DbsField acis_Reprint_Fl_View_Rp_Substitution_Contract_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_Conv_Issue_State;
    private DbsField acis_Reprint_Fl_View_Rp_Deceased_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_Non_Proprietary_Pkg_Ind;
    private DbsGroup acis_Reprint_Fl_View_Rp_Multi_Plan_Info;
    private DbsField acis_Reprint_Fl_View_Rp_Multi_Plan_No;
    private DbsField acis_Reprint_Fl_View_Rp_Multi_Sub_Plan;
    private DbsField acis_Reprint_Fl_View_Rp_Decedent_Contract;
    private DbsField acis_Reprint_Fl_View_Rp_Relation_To_Decedent;
    private DbsField acis_Reprint_Fl_View_Rp_Ssn_Tin_Ind;
    private DbsField acis_Reprint_Fl_View_Rp_Oneira_Acct_No;
    private DbsField acis_Reprint_Fl_View_Rp_Financial_2;
    private DbsField acis_Reprint_Fl_View_Rp_Financial_3;
    private DbsField acis_Reprint_Fl_View_Rp_Financial_4;
    private DbsField acis_Reprint_Fl_View_Rp_Financial_5;
    private DbsField acis_Reprint_Fl_View_Rp_Source_Cde_1;
    private DbsField acis_Reprint_Fl_View_Rp_Source_Cde_2;
    private DbsGroup acis_Reprint_Fl_View_Rp_Fund_Identifier_2;
    private DbsField acis_Reprint_Fl_View_Rp_Fund_Cde_2;
    private DbsField acis_Reprint_Fl_View_Rp_Allocation_Pct_2;
    private DbsField acis_Reprint_Fl_View_Count_Castrp_Related_Contract_Info;
    private DbsGroup acis_Reprint_Fl_View_Rp_Related_Contract_Info;
    private DbsField acis_Reprint_Fl_View_Rp_Related_Contract_Type;
    private DbsField acis_Reprint_Fl_View_Rp_Related_Tiaa_No;
    private DbsField acis_Reprint_Fl_View_Rp_Related_Tiaa_Issue_Date;
    private DbsField acis_Reprint_Fl_View_Rp_Related_First_Payment_Date;
    private DbsField acis_Reprint_Fl_View_Rp_Related_Tiaa_Total_Amt;
    private DbsField acis_Reprint_Fl_View_Rp_Related_Last_Payment_Date;
    private DbsField acis_Reprint_Fl_View_Rp_Related_Payment_Frequency;
    private DbsField acis_Reprint_Fl_View_Rp_Related_Tiaa_Issue_State;

    public DataAccessProgramView getVw_acis_Reprint_Fl_View() { return vw_acis_Reprint_Fl_View; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Record_Type() { return acis_Reprint_Fl_View_Rp_Record_Type; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Maintenance_Ind() { return acis_Reprint_Fl_View_Rp_Maintenance_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Maintenance_Date() { return acis_Reprint_Fl_View_Rp_Maintenance_Date; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Correct_Maint_Ind() { return acis_Reprint_Fl_View_Rp_Correct_Maint_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Reprint_Pull_Code() { return acis_Reprint_Fl_View_Rp_Reprint_Pull_Code; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Reprint_Request_Type() { return acis_Reprint_Fl_View_Rp_Reprint_Request_Type; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Print_Destination() { return acis_Reprint_Fl_View_Rp_Print_Destination; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Tiaa_Contr() { return acis_Reprint_Fl_View_Rp_Tiaa_Contr; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Cref_Contr() { return acis_Reprint_Fl_View_Rp_Cref_Contr; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Tiaa_Negr_Contract() { return acis_Reprint_Fl_View_Rp_Tiaa_Negr_Contract; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Cref_Negr_Contract() { return acis_Reprint_Fl_View_Rp_Cref_Negr_Contract; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Lob() { return acis_Reprint_Fl_View_Rp_Lob; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Lob_Type() { return acis_Reprint_Fl_View_Rp_Lob_Type; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Bill_Code() { return acis_Reprint_Fl_View_Rp_Bill_Code; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Cor_Prfx_Nme() { return acis_Reprint_Fl_View_Rp_Cor_Prfx_Nme; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Cor_Last_Nme() { return acis_Reprint_Fl_View_Rp_Cor_Last_Nme; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Cor_First_Nme() { return acis_Reprint_Fl_View_Rp_Cor_First_Nme; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Cor_Mddle_Nme() { return acis_Reprint_Fl_View_Rp_Cor_Mddle_Nme; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Cor_Sffx_Nme() { return acis_Reprint_Fl_View_Rp_Cor_Sffx_Nme; }

    public DbsGroup getAcis_Reprint_Fl_View_Rp_Address_Info() { return acis_Reprint_Fl_View_Rp_Address_Info; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Address_Line() { return acis_Reprint_Fl_View_Rp_Address_Line; }

    public DbsField getAcis_Reprint_Fl_View_Rp_City() { return acis_Reprint_Fl_View_Rp_City; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Orig_Issue_State() { return acis_Reprint_Fl_View_Rp_Orig_Issue_State; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Current_State_Code() { return acis_Reprint_Fl_View_Rp_Current_State_Code; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Mail_Zip() { return acis_Reprint_Fl_View_Rp_Mail_Zip; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Mail_Instructions() { return acis_Reprint_Fl_View_Rp_Mail_Instructions; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Soc_Sec() { return acis_Reprint_Fl_View_Rp_Soc_Sec; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Dob() { return acis_Reprint_Fl_View_Rp_Dob; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Sex() { return acis_Reprint_Fl_View_Rp_Sex; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Pin_Nbr() { return acis_Reprint_Fl_View_Rp_Pin_Nbr; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Coll_Code() { return acis_Reprint_Fl_View_Rp_Coll_Code; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Coll_St_Cd() { return acis_Reprint_Fl_View_Rp_Coll_St_Cd; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Irc_Sectn_Grp_Cde() { return acis_Reprint_Fl_View_Rp_Irc_Sectn_Grp_Cde; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Irc_Sectn_Cde() { return acis_Reprint_Fl_View_Rp_Irc_Sectn_Cde; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Ppg_Team_Cde() { return acis_Reprint_Fl_View_Rp_Ppg_Team_Cde; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Inst_Link_Cde() { return acis_Reprint_Fl_View_Rp_Inst_Link_Cde; }

    public DbsField getAcis_Reprint_Fl_View_Rp_T_Doi() { return acis_Reprint_Fl_View_Rp_T_Doi; }

    public DbsField getAcis_Reprint_Fl_View_Rp_C_Doi() { return acis_Reprint_Fl_View_Rp_C_Doi; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Dt_Released() { return acis_Reprint_Fl_View_Rp_Dt_Released; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Dt_App_Recvd() { return acis_Reprint_Fl_View_Rp_Dt_App_Recvd; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Annuity_Start_Date() { return acis_Reprint_Fl_View_Rp_Annuity_Start_Date; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Extract_Date() { return acis_Reprint_Fl_View_Rp_Extract_Date; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Curr() { return acis_Reprint_Fl_View_Rp_Curr; }

    public DbsField getAcis_Reprint_Fl_View_Rp_T_Age_1st() { return acis_Reprint_Fl_View_Rp_T_Age_1st; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Ownership() { return acis_Reprint_Fl_View_Rp_Ownership; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Alloc_Discr() { return acis_Reprint_Fl_View_Rp_Alloc_Discr; }

    public DbsGroup getAcis_Reprint_Fl_View_Rp_Allocation_Info() { return acis_Reprint_Fl_View_Rp_Allocation_Info; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Allocation() { return acis_Reprint_Fl_View_Rp_Allocation; }

    public DbsField getAcis_Reprint_Fl_View_Rp_App_Source() { return acis_Reprint_Fl_View_Rp_App_Source; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Region_Code() { return acis_Reprint_Fl_View_Rp_Region_Code; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Eop_Addl_Cref_Request() { return acis_Reprint_Fl_View_Rp_Eop_Addl_Cref_Request; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Rlc_Cref_Pref() { return acis_Reprint_Fl_View_Rp_Rlc_Cref_Pref; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Rlc_Cref_Cont() { return acis_Reprint_Fl_View_Rp_Rlc_Cref_Cont; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Ira_Rollover_Type() { return acis_Reprint_Fl_View_Rp_Ira_Rollover_Type; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Ira_Record_Type() { return acis_Reprint_Fl_View_Rp_Ira_Record_Type; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Mult_App_Status() { return acis_Reprint_Fl_View_Rp_Mult_App_Status; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Mult_App_Lob() { return acis_Reprint_Fl_View_Rp_Mult_App_Lob; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Mult_App_Lob_Type() { return acis_Reprint_Fl_View_Rp_Mult_App_Lob_Type; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Mult_App_Ppg() { return acis_Reprint_Fl_View_Rp_Mult_App_Ppg; }

    public DbsGroup getAcis_Reprint_Fl_View_Rp_Correction_Data_Type() { return acis_Reprint_Fl_View_Rp_Correction_Data_Type; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Correction_Ind() { return acis_Reprint_Fl_View_Rp_Correction_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Correction_Sync() { return acis_Reprint_Fl_View_Rp_Correction_Sync; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Financial_1() { return acis_Reprint_Fl_View_Rp_Financial_1; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Applcnt_Req_Type() { return acis_Reprint_Fl_View_Rp_Applcnt_Req_Type; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Tiaa_Service_Agent() { return acis_Reprint_Fl_View_Rp_Tiaa_Service_Agent; }

    public DbsGroup getAcis_Reprint_Fl_View_Rp_Mit_Request() { return acis_Reprint_Fl_View_Rp_Mit_Request; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Rqst_Log_Dte_Time() { return acis_Reprint_Fl_View_Rp_Rqst_Log_Dte_Time; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Mit_Unit() { return acis_Reprint_Fl_View_Rp_Mit_Unit; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Mit_Wpid() { return acis_Reprint_Fl_View_Rp_Mit_Wpid; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Update_Date() { return acis_Reprint_Fl_View_Rp_Update_Date; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Update_Time() { return acis_Reprint_Fl_View_Rp_Update_Time; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Racf_Id() { return acis_Reprint_Fl_View_Rp_Racf_Id; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Rlc_Cref_Cert() { return acis_Reprint_Fl_View_Rp_Rlc_Cref_Cert; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Allocation_Model() { return acis_Reprint_Fl_View_Rp_Allocation_Model; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Divorce_Ind() { return acis_Reprint_Fl_View_Rp_Divorce_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Email_Address() { return acis_Reprint_Fl_View_Rp_Email_Address; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Participant_Status_Ind() { return acis_Reprint_Fl_View_Rp_Participant_Status_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Request_Pkg_Type() { return acis_Reprint_Fl_View_Rp_Request_Pkg_Type; }

    public DbsField getAcis_Reprint_Fl_View_Rp_E_Signed_Appl_Ind() { return acis_Reprint_Fl_View_Rp_E_Signed_Appl_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Eft_Request_Ind() { return acis_Reprint_Fl_View_Rp_Eft_Request_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Contract_Package_Version() { return acis_Reprint_Fl_View_Rp_Contract_Package_Version; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Package_Mail_Type() { return acis_Reprint_Fl_View_Rp_Package_Mail_Type; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Duplicate_Copies_Requested() { return acis_Reprint_Fl_View_Rp_Duplicate_Copies_Requested; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Address_Change_Ind() { return acis_Reprint_Fl_View_Rp_Address_Change_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Allocation_Fmt() { return acis_Reprint_Fl_View_Rp_Allocation_Fmt; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Phone_No() { return acis_Reprint_Fl_View_Rp_Phone_No; }

    public DbsGroup getAcis_Reprint_Fl_View_Rp_Fund_Identifier() { return acis_Reprint_Fl_View_Rp_Fund_Identifier; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Fund_Cde() { return acis_Reprint_Fl_View_Rp_Fund_Cde; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Allocation_Pct() { return acis_Reprint_Fl_View_Rp_Allocation_Pct; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Sgrd_Plan_No() { return acis_Reprint_Fl_View_Rp_Sgrd_Plan_No; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Sgrd_Subplan_No() { return acis_Reprint_Fl_View_Rp_Sgrd_Subplan_No; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Sgrd_Part_Ext() { return acis_Reprint_Fl_View_Rp_Sgrd_Part_Ext; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Ls_Effective_Date() { return acis_Reprint_Fl_View_Rp_Ls_Effective_Date; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Interest_Rate() { return acis_Reprint_Fl_View_Rp_Interest_Rate; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Text_Udf_1() { return acis_Reprint_Fl_View_Rp_Text_Udf_1; }

    public DbsGroup getAcis_Reprint_Fl_View_Rp_Text_Udf_1Redef1() { return acis_Reprint_Fl_View_Rp_Text_Udf_1Redef1; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Single_Issue_Ind() { return acis_Reprint_Fl_View_Rp_Single_Issue_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Sgrd_Divsub() { return acis_Reprint_Fl_View_Rp_Sgrd_Divsub; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Spec_Fund_Ind() { return acis_Reprint_Fl_View_Rp_Spec_Fund_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Plan_Issue_State() { return acis_Reprint_Fl_View_Rp_Plan_Issue_State; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Text_Udf_2() { return acis_Reprint_Fl_View_Rp_Text_Udf_2; }

    public DbsGroup getAcis_Reprint_Fl_View_Rp_Text_Udf_2Redef2() { return acis_Reprint_Fl_View_Rp_Text_Udf_2Redef2; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Orig_Resid_Issue_St() { return acis_Reprint_Fl_View_Rp_Orig_Resid_Issue_St; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Tsv_Ind() { return acis_Reprint_Fl_View_Rp_Tsv_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Spcl_Lgl_Pkg_Ind() { return acis_Reprint_Fl_View_Rp_Spcl_Lgl_Pkg_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Tsr_Ind() { return acis_Reprint_Fl_View_Rp_Tsr_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Text_Udf_3() { return acis_Reprint_Fl_View_Rp_Text_Udf_3; }

    public DbsGroup getAcis_Reprint_Fl_View_Rp_Text_Udf_3Redef3() { return acis_Reprint_Fl_View_Rp_Text_Udf_3Redef3; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Sgrd_Client_Id() { return acis_Reprint_Fl_View_Rp_Sgrd_Client_Id; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Portfolio_Type() { return acis_Reprint_Fl_View_Rp_Portfolio_Type; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Replacement_Ind() { return acis_Reprint_Fl_View_Rp_Replacement_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Register_Id() { return acis_Reprint_Fl_View_Rp_Register_Id; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Exempt_Ind() { return acis_Reprint_Fl_View_Rp_Exempt_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Incmpl_Acct_Ind() { return acis_Reprint_Fl_View_Rp_Incmpl_Acct_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Autosave_Ind() { return acis_Reprint_Fl_View_Rp_Autosave_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_As_Cur_Dflt_Opt() { return acis_Reprint_Fl_View_Rp_As_Cur_Dflt_Opt; }

    public DbsField getAcis_Reprint_Fl_View_Rp_As_Cur_Dflt_Amt() { return acis_Reprint_Fl_View_Rp_As_Cur_Dflt_Amt; }

    public DbsField getAcis_Reprint_Fl_View_Rp_As_Incr_Opt() { return acis_Reprint_Fl_View_Rp_As_Incr_Opt; }

    public DbsField getAcis_Reprint_Fl_View_Rp_As_Incr_Amt() { return acis_Reprint_Fl_View_Rp_As_Incr_Amt; }

    public DbsField getAcis_Reprint_Fl_View_Rp_As_Max_Pct() { return acis_Reprint_Fl_View_Rp_As_Max_Pct; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Ae_Opt_Out_Days() { return acis_Reprint_Fl_View_Rp_Ae_Opt_Out_Days; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Welcome_Mail_Dt() { return acis_Reprint_Fl_View_Rp_Welcome_Mail_Dt; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Legal_Mail_Dt() { return acis_Reprint_Fl_View_Rp_Legal_Mail_Dt; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Delete_User_Id() { return acis_Reprint_Fl_View_Rp_Delete_User_Id; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Delete_Reason_Cd() { return acis_Reprint_Fl_View_Rp_Delete_Reason_Cd; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Dt_Deleted() { return acis_Reprint_Fl_View_Rp_Dt_Deleted; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Agent_Crd_No() { return acis_Reprint_Fl_View_Rp_Agent_Crd_No; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Welc_E_Delivery_Ind() { return acis_Reprint_Fl_View_Rp_Welc_E_Delivery_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Legal_E_Delivery_Ind() { return acis_Reprint_Fl_View_Rp_Legal_E_Delivery_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Legal_Ann_Option() { return acis_Reprint_Fl_View_Rp_Legal_Ann_Option; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Orchestration_Id() { return acis_Reprint_Fl_View_Rp_Orchestration_Id; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Mail_Addr_Country_Cd() { return acis_Reprint_Fl_View_Rp_Mail_Addr_Country_Cd; }

    public DbsGroup getAcis_Reprint_Fl_View_Rp_Res_Addr_Info() { return acis_Reprint_Fl_View_Rp_Res_Addr_Info; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Res_Addr_Line() { return acis_Reprint_Fl_View_Rp_Res_Addr_Line; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Res_Addr_City() { return acis_Reprint_Fl_View_Rp_Res_Addr_City; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Res_Addr_State_Code() { return acis_Reprint_Fl_View_Rp_Res_Addr_State_Code; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Res_Addr_Mail_Zip() { return acis_Reprint_Fl_View_Rp_Res_Addr_Mail_Zip; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Res_Addr_Country_Cd() { return acis_Reprint_Fl_View_Rp_Res_Addr_Country_Cd; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Tic_Startdate() { return acis_Reprint_Fl_View_Rp_Tic_Startdate; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Tic_Enddate() { return acis_Reprint_Fl_View_Rp_Tic_Enddate; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Tic_Percentage() { return acis_Reprint_Fl_View_Rp_Tic_Percentage; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Tic_Postdays() { return acis_Reprint_Fl_View_Rp_Tic_Postdays; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Tic_Limit() { return acis_Reprint_Fl_View_Rp_Tic_Limit; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Tic_Postfreq() { return acis_Reprint_Fl_View_Rp_Tic_Postfreq; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Tic_Pl_Level() { return acis_Reprint_Fl_View_Rp_Tic_Pl_Level; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Tic_Windowdays() { return acis_Reprint_Fl_View_Rp_Tic_Windowdays; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Tic_Reqdlywindow() { return acis_Reprint_Fl_View_Rp_Tic_Reqdlywindow; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Tic_Recap_Prov() { return acis_Reprint_Fl_View_Rp_Tic_Recap_Prov; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Ann_Funding_Dt() { return acis_Reprint_Fl_View_Rp_Ann_Funding_Dt; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Tiaa_Ann_Issue_Dt() { return acis_Reprint_Fl_View_Rp_Tiaa_Ann_Issue_Dt; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Cref_Ann_Issue_Dt() { return acis_Reprint_Fl_View_Rp_Cref_Ann_Issue_Dt; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Substitution_Contract_Ind() { return acis_Reprint_Fl_View_Rp_Substitution_Contract_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Conv_Issue_State() { return acis_Reprint_Fl_View_Rp_Conv_Issue_State; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Deceased_Ind() { return acis_Reprint_Fl_View_Rp_Deceased_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Non_Proprietary_Pkg_Ind() { return acis_Reprint_Fl_View_Rp_Non_Proprietary_Pkg_Ind; }

    public DbsGroup getAcis_Reprint_Fl_View_Rp_Multi_Plan_Info() { return acis_Reprint_Fl_View_Rp_Multi_Plan_Info; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Multi_Plan_No() { return acis_Reprint_Fl_View_Rp_Multi_Plan_No; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Multi_Sub_Plan() { return acis_Reprint_Fl_View_Rp_Multi_Sub_Plan; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Decedent_Contract() { return acis_Reprint_Fl_View_Rp_Decedent_Contract; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Relation_To_Decedent() { return acis_Reprint_Fl_View_Rp_Relation_To_Decedent; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Ssn_Tin_Ind() { return acis_Reprint_Fl_View_Rp_Ssn_Tin_Ind; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Oneira_Acct_No() { return acis_Reprint_Fl_View_Rp_Oneira_Acct_No; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Financial_2() { return acis_Reprint_Fl_View_Rp_Financial_2; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Financial_3() { return acis_Reprint_Fl_View_Rp_Financial_3; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Financial_4() { return acis_Reprint_Fl_View_Rp_Financial_4; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Financial_5() { return acis_Reprint_Fl_View_Rp_Financial_5; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Source_Cde_1() { return acis_Reprint_Fl_View_Rp_Source_Cde_1; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Source_Cde_2() { return acis_Reprint_Fl_View_Rp_Source_Cde_2; }

    public DbsGroup getAcis_Reprint_Fl_View_Rp_Fund_Identifier_2() { return acis_Reprint_Fl_View_Rp_Fund_Identifier_2; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Fund_Cde_2() { return acis_Reprint_Fl_View_Rp_Fund_Cde_2; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Allocation_Pct_2() { return acis_Reprint_Fl_View_Rp_Allocation_Pct_2; }

    public DbsField getAcis_Reprint_Fl_View_Count_Castrp_Related_Contract_Info() { return acis_Reprint_Fl_View_Count_Castrp_Related_Contract_Info; }

    public DbsGroup getAcis_Reprint_Fl_View_Rp_Related_Contract_Info() { return acis_Reprint_Fl_View_Rp_Related_Contract_Info; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Related_Contract_Type() { return acis_Reprint_Fl_View_Rp_Related_Contract_Type; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Related_Tiaa_No() { return acis_Reprint_Fl_View_Rp_Related_Tiaa_No; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Related_Tiaa_Issue_Date() { return acis_Reprint_Fl_View_Rp_Related_Tiaa_Issue_Date; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Related_First_Payment_Date() { return acis_Reprint_Fl_View_Rp_Related_First_Payment_Date; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Related_Tiaa_Total_Amt() { return acis_Reprint_Fl_View_Rp_Related_Tiaa_Total_Amt; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Related_Last_Payment_Date() { return acis_Reprint_Fl_View_Rp_Related_Last_Payment_Date; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Related_Payment_Frequency() { return acis_Reprint_Fl_View_Rp_Related_Payment_Frequency; }

    public DbsField getAcis_Reprint_Fl_View_Rp_Related_Tiaa_Issue_State() { return acis_Reprint_Fl_View_Rp_Related_Tiaa_Issue_State; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_acis_Reprint_Fl_View = new DataAccessProgramView(new NameInfo("vw_acis_Reprint_Fl_View", "ACIS-REPRINT-FL-VIEW"), "ACIS_REPRINT_FL_12", "ACIS_RPRNT_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("ACIS_REPRINT_FL_12"));
        acis_Reprint_Fl_View_Rp_Record_Type = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Record_Type", "RP-RECORD-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_RECORD_TYPE");
        acis_Reprint_Fl_View_Rp_Record_Type.setDdmHeader("REC/TYPE");
        acis_Reprint_Fl_View_Rp_Maintenance_Ind = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Maintenance_Ind", "RP-MAINTENANCE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_MAINTENANCE_IND");
        acis_Reprint_Fl_View_Rp_Maintenance_Ind.setDdmHeader("REPRINT/IND");
        acis_Reprint_Fl_View_Rp_Maintenance_Date = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Maintenance_Date", "RP-MAINTENANCE-DATE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RP_MAINTENANCE_DATE");
        acis_Reprint_Fl_View_Rp_Correct_Maint_Ind = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Correct_Maint_Ind", "RP-CORRECT-MAINT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_CORRECT_MAINT_IND");
        acis_Reprint_Fl_View_Rp_Reprint_Pull_Code = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Reprint_Pull_Code", "RP-REPRINT-PULL-CODE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_REPRINT_PULL_CODE");
        acis_Reprint_Fl_View_Rp_Reprint_Pull_Code.setDdmHeader("REPRINT/PULL CDE");
        acis_Reprint_Fl_View_Rp_Reprint_Request_Type = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Reprint_Request_Type", 
            "RP-REPRINT-REQUEST-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_REPRINT_REQUEST_TYPE");
        acis_Reprint_Fl_View_Rp_Reprint_Request_Type.setDdmHeader("REPRINT/ TYPE");
        acis_Reprint_Fl_View_Rp_Print_Destination = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Print_Destination", "RP-PRINT-DESTINATION", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_PRINT_DESTINATION");
        acis_Reprint_Fl_View_Rp_Print_Destination.setDdmHeader("PRT/DEST");
        acis_Reprint_Fl_View_Rp_Tiaa_Contr = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Tiaa_Contr", "RP-TIAA-CONTR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "RP_TIAA_CONTR");
        acis_Reprint_Fl_View_Rp_Tiaa_Contr.setDdmHeader("TIAA/CONTR");
        acis_Reprint_Fl_View_Rp_Cref_Contr = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Cref_Contr", "RP-CREF-CONTR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "RP_CREF_CONTR");
        acis_Reprint_Fl_View_Rp_Cref_Contr.setDdmHeader("CREF/CONTR");
        acis_Reprint_Fl_View_Rp_Tiaa_Negr_Contract = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Tiaa_Negr_Contract", 
            "RP-TIAA-NEGR-CONTRACT", FieldType.STRING, 10, RepeatingFieldStrategy.None, "RP_TIAA_NEGR_CONTRACT");
        acis_Reprint_Fl_View_Rp_Tiaa_Negr_Contract.setDdmHeader("TIAANEGR/ CONTRACT");
        acis_Reprint_Fl_View_Rp_Cref_Negr_Contract = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Cref_Negr_Contract", 
            "RP-CREF-NEGR-CONTRACT", FieldType.STRING, 10, RepeatingFieldStrategy.None, "RP_CREF_NEGR_CONTRACT");
        acis_Reprint_Fl_View_Rp_Cref_Negr_Contract.setDdmHeader("CREF NEGR/ CONTRACT");
        acis_Reprint_Fl_View_Rp_Lob = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Lob", "RP-LOB", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RP_LOB");
        acis_Reprint_Fl_View_Rp_Lob.setDdmHeader("LOB");
        acis_Reprint_Fl_View_Rp_Lob_Type = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Lob_Type", "RP-LOB-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RP_LOB_TYPE");
        acis_Reprint_Fl_View_Rp_Lob_Type.setDdmHeader("LOB/TYPE");
        acis_Reprint_Fl_View_Rp_Bill_Code = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Bill_Code", "RP-BILL-CODE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RP_BILL_CODE");
        acis_Reprint_Fl_View_Rp_Bill_Code.setDdmHeader("BILL/CODE");
        acis_Reprint_Fl_View_Rp_Cor_Prfx_Nme = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Cor_Prfx_Nme", "RP-COR-PRFX-NME", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RP_COR_PRFX_NME");
        acis_Reprint_Fl_View_Rp_Cor_Prfx_Nme.setDdmHeader("PARTICIPANT/PREFIX");
        acis_Reprint_Fl_View_Rp_Cor_Last_Nme = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Cor_Last_Nme", "RP-COR-LAST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "RP_COR_LAST_NME");
        acis_Reprint_Fl_View_Rp_Cor_Last_Nme.setDdmHeader("PARTICIPANT/LAST NAME");
        acis_Reprint_Fl_View_Rp_Cor_First_Nme = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Cor_First_Nme", "RP-COR-FIRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "RP_COR_FIRST_NME");
        acis_Reprint_Fl_View_Rp_Cor_First_Nme.setDdmHeader("PARTICIPANT/FIRST NAME");
        acis_Reprint_Fl_View_Rp_Cor_Mddle_Nme = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Cor_Mddle_Nme", "RP-COR-MDDLE-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "RP_COR_MDDLE_NME");
        acis_Reprint_Fl_View_Rp_Cor_Mddle_Nme.setDdmHeader("PARTICIPANT/MIDDLE NAME");
        acis_Reprint_Fl_View_Rp_Cor_Sffx_Nme = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Cor_Sffx_Nme", "RP-COR-SFFX-NME", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RP_COR_SFFX_NME");
        acis_Reprint_Fl_View_Rp_Cor_Sffx_Nme.setDdmHeader("PARTICIPANT/SUFFIX NAME");
        acis_Reprint_Fl_View_Rp_Address_Info = vw_acis_Reprint_Fl_View.getRecord().newGroupInGroup("acis_Reprint_Fl_View_Rp_Address_Info", "RP-ADDRESS-INFO", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_RPRNT_FILE_RP_ADDRESS_INFO");
        acis_Reprint_Fl_View_Rp_Address_Info.setDdmHeader("ADDRESS/INFO");
        acis_Reprint_Fl_View_Rp_Address_Line = acis_Reprint_Fl_View_Rp_Address_Info.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Address_Line", "RP-ADDRESS-LINE", 
            FieldType.STRING, 35, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_ADDRESS_LINE", "ACIS_RPRNT_FILE_RP_ADDRESS_INFO");
        acis_Reprint_Fl_View_Rp_Address_Line.setDdmHeader("ADDRESS");
        acis_Reprint_Fl_View_Rp_City = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_City", "RP-CITY", FieldType.STRING, 
            27, RepeatingFieldStrategy.None, "RP_CITY");
        acis_Reprint_Fl_View_Rp_City.setDdmHeader("CITY");
        acis_Reprint_Fl_View_Rp_Orig_Issue_State = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Orig_Issue_State", "RP-ORIG-ISSUE-STATE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "RP_ORIG_ISSUE_STATE");
        acis_Reprint_Fl_View_Rp_Orig_Issue_State.setDdmHeader("ORIG ISS/STATE");
        acis_Reprint_Fl_View_Rp_Current_State_Code = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Current_State_Code", 
            "RP-CURRENT-STATE-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "RP_CURRENT_STATE_CODE");
        acis_Reprint_Fl_View_Rp_Current_State_Code.setDdmHeader("CURR STATE/CODE");
        acis_Reprint_Fl_View_Rp_Mail_Zip = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Mail_Zip", "RP-MAIL-ZIP", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "RP_MAIL_ZIP");
        acis_Reprint_Fl_View_Rp_Mail_Zip.setDdmHeader("ZIP/CODE");
        acis_Reprint_Fl_View_Rp_Mail_Instructions = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Mail_Instructions", "RP-MAIL-INSTRUCTIONS", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_MAIL_INSTRUCTIONS");
        acis_Reprint_Fl_View_Rp_Mail_Instructions.setDdmHeader("MAIL/INSTR");
        acis_Reprint_Fl_View_Rp_Soc_Sec = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Soc_Sec", "RP-SOC-SEC", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "RP_SOC_SEC");
        acis_Reprint_Fl_View_Rp_Soc_Sec.setDdmHeader("SOC/SEC");
        acis_Reprint_Fl_View_Rp_Dob = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Dob", "RP-DOB", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "RP_DOB");
        acis_Reprint_Fl_View_Rp_Dob.setDdmHeader("DOB");
        acis_Reprint_Fl_View_Rp_Sex = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Sex", "RP-SEX", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RP_SEX");
        acis_Reprint_Fl_View_Rp_Sex.setDdmHeader("SEX");
        acis_Reprint_Fl_View_Rp_Pin_Nbr = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Pin_Nbr", "RP-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "RP_PIN_NBR");
        acis_Reprint_Fl_View_Rp_Pin_Nbr.setDdmHeader("PIN/NBR");
        acis_Reprint_Fl_View_Rp_Coll_Code = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Coll_Code", "RP-COLL-CODE", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "RP_COLL_CODE");
        acis_Reprint_Fl_View_Rp_Coll_Code.setDdmHeader("PPG");
        acis_Reprint_Fl_View_Rp_Coll_St_Cd = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Coll_St_Cd", "RP-COLL-ST-CD", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "RP_COLL_ST_CD");
        acis_Reprint_Fl_View_Rp_Coll_St_Cd.setDdmHeader("COLL/STATE");
        acis_Reprint_Fl_View_Rp_Irc_Sectn_Grp_Cde = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Irc_Sectn_Grp_Cde", "RP-IRC-SECTN-GRP-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "RP_IRC_SECTN_GRP_CDE");
        acis_Reprint_Fl_View_Rp_Irc_Sectn_Grp_Cde.setDdmHeader("IRC SECTN/GRP CODE");
        acis_Reprint_Fl_View_Rp_Irc_Sectn_Cde = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Irc_Sectn_Cde", "RP-IRC-SECTN-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "RP_IRC_SECTN_CDE");
        acis_Reprint_Fl_View_Rp_Irc_Sectn_Cde.setDdmHeader("IRC SECTN/CODE");
        acis_Reprint_Fl_View_Rp_Ppg_Team_Cde = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Ppg_Team_Cde", "RP-PPG-TEAM-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RP_PPG_TEAM_CDE");
        acis_Reprint_Fl_View_Rp_Ppg_Team_Cde.setDdmHeader("TEAM/CODE");
        acis_Reprint_Fl_View_Rp_Inst_Link_Cde = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Inst_Link_Cde", "RP-INST-LINK-CDE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "RP_INST_LINK_CDE");
        acis_Reprint_Fl_View_Rp_Inst_Link_Cde.setDdmHeader("INSTN/CODE");
        acis_Reprint_Fl_View_Rp_T_Doi = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_T_Doi", "RP-T-DOI", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "RP_T_DOI");
        acis_Reprint_Fl_View_Rp_T_Doi.setDdmHeader("TIAA/DOI");
        acis_Reprint_Fl_View_Rp_C_Doi = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_C_Doi", "RP-C-DOI", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "RP_C_DOI");
        acis_Reprint_Fl_View_Rp_C_Doi.setDdmHeader("CREF/DOI");
        acis_Reprint_Fl_View_Rp_Dt_Released = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Dt_Released", "RP-DT-RELEASED", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RP_DT_RELEASED");
        acis_Reprint_Fl_View_Rp_Dt_Released.setDdmHeader("DATE/RELEASED");
        acis_Reprint_Fl_View_Rp_Dt_App_Recvd = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Dt_App_Recvd", "RP-DT-APP-RECVD", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RP_DT_APP_RECVD");
        acis_Reprint_Fl_View_Rp_Dt_App_Recvd.setDdmHeader("APPL RECV/DATE");
        acis_Reprint_Fl_View_Rp_Annuity_Start_Date = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Annuity_Start_Date", 
            "RP-ANNUITY-START-DATE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RP_ANNUITY_START_DATE");
        acis_Reprint_Fl_View_Rp_Annuity_Start_Date.setDdmHeader("ANNUITY/START DTE");
        acis_Reprint_Fl_View_Rp_Extract_Date = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Extract_Date", "RP-EXTRACT-DATE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RP_EXTRACT_DATE");
        acis_Reprint_Fl_View_Rp_Extract_Date.setDdmHeader("EXTRACT/ DATE");
        acis_Reprint_Fl_View_Rp_Curr = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Curr", "RP-CURR", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "RP_CURR");
        acis_Reprint_Fl_View_Rp_Curr.setDdmHeader("CURR");
        acis_Reprint_Fl_View_Rp_T_Age_1st = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_T_Age_1st", "RP-T-AGE-1ST", FieldType.NUMERIC, 
            4, RepeatingFieldStrategy.None, "RP_T_AGE_1ST");
        acis_Reprint_Fl_View_Rp_T_Age_1st.setDdmHeader("AGE FIRST/ANNUITY");
        acis_Reprint_Fl_View_Rp_Ownership = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Ownership", "RP-OWNERSHIP", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "RP_OWNERSHIP");
        acis_Reprint_Fl_View_Rp_Ownership.setDdmHeader("OWNER");
        acis_Reprint_Fl_View_Rp_Alloc_Discr = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Alloc_Discr", "RP-ALLOC-DISCR", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "RP_ALLOC_DISCR");
        acis_Reprint_Fl_View_Rp_Alloc_Discr.setDdmHeader("ALLOC/DISCR");
        acis_Reprint_Fl_View_Rp_Allocation_Info = vw_acis_Reprint_Fl_View.getRecord().newGroupInGroup("acis_Reprint_Fl_View_Rp_Allocation_Info", "RP-ALLOCATION-INFO", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_RPRNT_FILE_RP_ALLOCATION_INFO");
        acis_Reprint_Fl_View_Rp_Allocation_Info.setDdmHeader("ALLOCATION/INFO");
        acis_Reprint_Fl_View_Rp_Allocation = acis_Reprint_Fl_View_Rp_Allocation_Info.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Allocation", "RP-ALLOCATION", 
            FieldType.NUMERIC, 3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_ALLOCATION", "ACIS_RPRNT_FILE_RP_ALLOCATION_INFO");
        acis_Reprint_Fl_View_Rp_Allocation.setDdmHeader("ALLOCATION");
        acis_Reprint_Fl_View_Rp_App_Source = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_App_Source", "RP-APP-SOURCE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_APP_SOURCE");
        acis_Reprint_Fl_View_Rp_App_Source.setDdmHeader("APPL/SOURCE");
        acis_Reprint_Fl_View_Rp_Region_Code = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Region_Code", "RP-REGION-CODE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "RP_REGION_CODE");
        acis_Reprint_Fl_View_Rp_Region_Code.setDdmHeader("REG/CDE");
        acis_Reprint_Fl_View_Rp_Eop_Addl_Cref_Request = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Eop_Addl_Cref_Request", 
            "RP-EOP-ADDL-CREF-REQUEST", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_EOP_ADDL_CREF_REQUEST");
        acis_Reprint_Fl_View_Rp_Eop_Addl_Cref_Request.setDdmHeader("EOP ADDL/CREF RQST");
        acis_Reprint_Fl_View_Rp_Rlc_Cref_Pref = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Rlc_Cref_Pref", "RP-RLC-CREF-PREF", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_RLC_CREF_PREF");
        acis_Reprint_Fl_View_Rp_Rlc_Cref_Pref.setDdmHeader("RLC CREF/  PREF");
        acis_Reprint_Fl_View_Rp_Rlc_Cref_Cont = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Rlc_Cref_Cont", "RP-RLC-CREF-CONT", 
            FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "RP_RLC_CREF_CONT");
        acis_Reprint_Fl_View_Rp_Rlc_Cref_Cont.setDdmHeader("RLC CREF/ CONTR");
        acis_Reprint_Fl_View_Rp_Ira_Rollover_Type = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Ira_Rollover_Type", "RP-IRA-ROLLOVER-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_IRA_ROLLOVER_TYPE");
        acis_Reprint_Fl_View_Rp_Ira_Rollover_Type.setDdmHeader("IRA ROLLOVER/TYPE");
        acis_Reprint_Fl_View_Rp_Ira_Record_Type = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Ira_Record_Type", "RP-IRA-RECORD-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_IRA_RECORD_TYPE");
        acis_Reprint_Fl_View_Rp_Ira_Record_Type.setDdmHeader("IRA REC/TYPE");
        acis_Reprint_Fl_View_Rp_Mult_App_Status = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Mult_App_Status", "RP-MULT-APP-STATUS", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_MULT_APP_STATUS");
        acis_Reprint_Fl_View_Rp_Mult_App_Status.setDdmHeader("MULT APPL/STATUS");
        acis_Reprint_Fl_View_Rp_Mult_App_Lob = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Mult_App_Lob", "RP-MULT-APP-LOB", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_MULT_APP_LOB");
        acis_Reprint_Fl_View_Rp_Mult_App_Lob.setDdmHeader("MULT APPL/LOB");
        acis_Reprint_Fl_View_Rp_Mult_App_Lob_Type = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Mult_App_Lob_Type", "RP-MULT-APP-LOB-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_MULT_APP_LOB_TYPE");
        acis_Reprint_Fl_View_Rp_Mult_App_Lob_Type.setDdmHeader("MULT APPL/LOB TYPE");
        acis_Reprint_Fl_View_Rp_Mult_App_Ppg = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Mult_App_Ppg", "RP-MULT-APP-PPG", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "RP_MULT_APP_PPG");
        acis_Reprint_Fl_View_Rp_Mult_App_Ppg.setDdmHeader("MULT APPL/PPG");
        acis_Reprint_Fl_View_Rp_Correction_Data_Type = vw_acis_Reprint_Fl_View.getRecord().newGroupInGroup("acis_Reprint_Fl_View_Rp_Correction_Data_Type", 
            "RP-CORRECTION-DATA-TYPE", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_RPRNT_FILE_RP_CORRECTION_DATA_TYPE");
        acis_Reprint_Fl_View_Rp_Correction_Data_Type.setDdmHeader("CORRECTION/   IND");
        acis_Reprint_Fl_View_Rp_Correction_Ind = acis_Reprint_Fl_View_Rp_Correction_Data_Type.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Correction_Ind", 
            "RP-CORRECTION-IND", FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_CORRECTION_IND", 
            "ACIS_RPRNT_FILE_RP_CORRECTION_DATA_TYPE");
        acis_Reprint_Fl_View_Rp_Correction_Ind.setDdmHeader("CORRECTION IND");
        acis_Reprint_Fl_View_Rp_Correction_Sync = acis_Reprint_Fl_View_Rp_Correction_Data_Type.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Correction_Sync", 
            "RP-CORRECTION-SYNC", FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_CORRECTION_SYNC", 
            "ACIS_RPRNT_FILE_RP_CORRECTION_DATA_TYPE");
        acis_Reprint_Fl_View_Rp_Financial_1 = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Financial_1", "RP-FINANCIAL-1", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "RP_FINANCIAL_1");
        acis_Reprint_Fl_View_Rp_Financial_1.setDdmHeader("FINANCIAL/1");
        acis_Reprint_Fl_View_Rp_Applcnt_Req_Type = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Applcnt_Req_Type", "RP-APPLCNT-REQ-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_APPLCNT_REQ_TYPE");
        acis_Reprint_Fl_View_Rp_Applcnt_Req_Type.setDdmHeader("APP REQ/TYPE");
        acis_Reprint_Fl_View_Rp_Tiaa_Service_Agent = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Tiaa_Service_Agent", 
            "RP-TIAA-SERVICE-AGENT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_TIAA_SERVICE_AGENT");
        acis_Reprint_Fl_View_Rp_Tiaa_Service_Agent.setDdmHeader("TIAA/SERV AGT");
        acis_Reprint_Fl_View_Rp_Mit_Request = vw_acis_Reprint_Fl_View.getRecord().newGroupInGroup("acis_Reprint_Fl_View_Rp_Mit_Request", "RP-MIT-REQUEST", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_RPRNT_FILE_RP_MIT_REQUEST");
        acis_Reprint_Fl_View_Rp_Mit_Request.setDdmHeader("MIT/REQUEST");
        acis_Reprint_Fl_View_Rp_Rqst_Log_Dte_Time = acis_Reprint_Fl_View_Rp_Mit_Request.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Rqst_Log_Dte_Time", 
            "RP-RQST-LOG-DTE-TIME", FieldType.STRING, 15, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RQST_LOG_DTE_TIME", 
            "ACIS_RPRNT_FILE_RP_MIT_REQUEST");
        acis_Reprint_Fl_View_Rp_Rqst_Log_Dte_Time.setDdmHeader("MIT LOG/DATE-TIME");
        acis_Reprint_Fl_View_Rp_Mit_Unit = acis_Reprint_Fl_View_Rp_Mit_Request.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Mit_Unit", "RP-MIT-UNIT", 
            FieldType.STRING, 8, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_MIT_UNIT", "ACIS_RPRNT_FILE_RP_MIT_REQUEST");
        acis_Reprint_Fl_View_Rp_Mit_Unit.setDdmHeader("MIT/UNIT");
        acis_Reprint_Fl_View_Rp_Mit_Wpid = acis_Reprint_Fl_View_Rp_Mit_Request.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Mit_Wpid", "RP-MIT-WPID", 
            FieldType.STRING, 6, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_MIT_WPID", "ACIS_RPRNT_FILE_RP_MIT_REQUEST");
        acis_Reprint_Fl_View_Rp_Mit_Wpid.setDdmHeader("MIT/WPID");
        acis_Reprint_Fl_View_Rp_Update_Date = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Update_Date", "RP-UPDATE-DATE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RP_UPDATE_DATE");
        acis_Reprint_Fl_View_Rp_Update_Date.setDdmHeader("UPDATE/ DATE");
        acis_Reprint_Fl_View_Rp_Update_Time = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Update_Time", "RP-UPDATE-TIME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "RP_UPDATE_TIME");
        acis_Reprint_Fl_View_Rp_Update_Time.setDdmHeader("UPDATE/ TIME");
        acis_Reprint_Fl_View_Rp_Racf_Id = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Racf_Id", "RP-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RP_RACF_ID");
        acis_Reprint_Fl_View_Rp_Racf_Id.setDdmHeader("REQUESTOR/ID");
        acis_Reprint_Fl_View_Rp_Rlc_Cref_Cert = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Rlc_Cref_Cert", "RP-RLC-CREF-CERT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "RP_RLC_CREF_CERT");
        acis_Reprint_Fl_View_Rp_Allocation_Model = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Allocation_Model", "RP-ALLOCATION-MODEL", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "RP_ALLOCATION_MODEL");
        acis_Reprint_Fl_View_Rp_Divorce_Ind = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Divorce_Ind", "RP-DIVORCE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_DIVORCE_IND");
        acis_Reprint_Fl_View_Rp_Email_Address = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Email_Address", "RP-EMAIL-ADDRESS", 
            FieldType.STRING, 50, RepeatingFieldStrategy.None, "RP_EMAIL_ADDRESS");
        acis_Reprint_Fl_View_Rp_Participant_Status_Ind = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Participant_Status_Ind", 
            "RP-PARTICIPANT-STATUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_PARTICIPANT_STATUS_IND");
        acis_Reprint_Fl_View_Rp_Request_Pkg_Type = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Request_Pkg_Type", "RP-REQUEST-PKG-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_REQUEST_PKG_TYPE");
        acis_Reprint_Fl_View_Rp_E_Signed_Appl_Ind = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_E_Signed_Appl_Ind", "RP-E-SIGNED-APPL-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_E_SIGNED_APPL_IND");
        acis_Reprint_Fl_View_Rp_Eft_Request_Ind = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Eft_Request_Ind", "RP-EFT-REQUEST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_EFT_REQUEST_IND");
        acis_Reprint_Fl_View_Rp_Contract_Package_Version = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Contract_Package_Version", 
            "RP-CONTRACT-PACKAGE-VERSION", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_CONTRACT_PACKAGE_VERSION");
        acis_Reprint_Fl_View_Rp_Package_Mail_Type = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Package_Mail_Type", "RP-PACKAGE-MAIL-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_PACKAGE_MAIL_TYPE");
        acis_Reprint_Fl_View_Rp_Duplicate_Copies_Requested = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Duplicate_Copies_Requested", 
            "RP-DUPLICATE-COPIES-REQUESTED", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "RP_DUPLICATE_COPIES_REQUESTED");
        acis_Reprint_Fl_View_Rp_Address_Change_Ind = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Address_Change_Ind", 
            "RP-ADDRESS-CHANGE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_ADDRESS_CHANGE_IND");
        acis_Reprint_Fl_View_Rp_Allocation_Fmt = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Allocation_Fmt", "RP-ALLOCATION-FMT", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_ALLOCATION_FMT");
        acis_Reprint_Fl_View_Rp_Phone_No = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Phone_No", "RP-PHONE-NO", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "RP_PHONE_NO");
        acis_Reprint_Fl_View_Rp_Fund_Identifier = vw_acis_Reprint_Fl_View.getRecord().newGroupInGroup("acis_Reprint_Fl_View_Rp_Fund_Identifier", "RP-FUND-IDENTIFIER", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_RPRNT_FILE_RP_FUND_IDENTIFIER");
        acis_Reprint_Fl_View_Rp_Fund_Cde = acis_Reprint_Fl_View_Rp_Fund_Identifier.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Fund_Cde", "RP-FUND-CDE", 
            FieldType.STRING, 10, new DbsArrayController(1,100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_FUND_CDE", "ACIS_RPRNT_FILE_RP_FUND_IDENTIFIER");
        acis_Reprint_Fl_View_Rp_Allocation_Pct = acis_Reprint_Fl_View_Rp_Fund_Identifier.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Allocation_Pct", 
            "RP-ALLOCATION-PCT", FieldType.NUMERIC, 3, new DbsArrayController(1,100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_ALLOCATION_PCT", 
            "ACIS_RPRNT_FILE_RP_FUND_IDENTIFIER");
        acis_Reprint_Fl_View_Rp_Sgrd_Plan_No = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Sgrd_Plan_No", "RP-SGRD-PLAN-NO", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "RP_SGRD_PLAN_NO");
        acis_Reprint_Fl_View_Rp_Sgrd_Subplan_No = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Sgrd_Subplan_No", "RP-SGRD-SUBPLAN-NO", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "RP_SGRD_SUBPLAN_NO");
        acis_Reprint_Fl_View_Rp_Sgrd_Part_Ext = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Sgrd_Part_Ext", "RP-SGRD-PART-EXT", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "RP_SGRD_PART_EXT");
        acis_Reprint_Fl_View_Rp_Ls_Effective_Date = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Ls_Effective_Date", "RP-LS-EFFECTIVE-DATE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RP_LS_EFFECTIVE_DATE");
        acis_Reprint_Fl_View_Rp_Interest_Rate = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Interest_Rate", "RP-INTEREST-RATE", 
            FieldType.NUMERIC, 6, 3, RepeatingFieldStrategy.None, "RP_INTEREST_RATE");
        acis_Reprint_Fl_View_Rp_Text_Udf_1 = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Text_Udf_1", "RP-TEXT-UDF-1", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "RP_TEXT_UDF_1");
        acis_Reprint_Fl_View_Rp_Text_Udf_1Redef1 = vw_acis_Reprint_Fl_View.getRecord().newGroupInGroup("acis_Reprint_Fl_View_Rp_Text_Udf_1Redef1", "Redefines", 
            acis_Reprint_Fl_View_Rp_Text_Udf_1);
        acis_Reprint_Fl_View_Rp_Single_Issue_Ind = acis_Reprint_Fl_View_Rp_Text_Udf_1Redef1.newFieldInGroup("acis_Reprint_Fl_View_Rp_Single_Issue_Ind", 
            "RP-SINGLE-ISSUE-IND", FieldType.STRING, 1);
        acis_Reprint_Fl_View_Rp_Sgrd_Divsub = acis_Reprint_Fl_View_Rp_Text_Udf_1Redef1.newFieldInGroup("acis_Reprint_Fl_View_Rp_Sgrd_Divsub", "RP-SGRD-DIVSUB", 
            FieldType.STRING, 4);
        acis_Reprint_Fl_View_Rp_Spec_Fund_Ind = acis_Reprint_Fl_View_Rp_Text_Udf_1Redef1.newFieldInGroup("acis_Reprint_Fl_View_Rp_Spec_Fund_Ind", "RP-SPEC-FUND-IND", 
            FieldType.STRING, 3);
        acis_Reprint_Fl_View_Rp_Plan_Issue_State = acis_Reprint_Fl_View_Rp_Text_Udf_1Redef1.newFieldInGroup("acis_Reprint_Fl_View_Rp_Plan_Issue_State", 
            "RP-PLAN-ISSUE-STATE", FieldType.STRING, 2);
        acis_Reprint_Fl_View_Rp_Text_Udf_2 = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Text_Udf_2", "RP-TEXT-UDF-2", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "RP_TEXT_UDF_2");
        acis_Reprint_Fl_View_Rp_Text_Udf_2Redef2 = vw_acis_Reprint_Fl_View.getRecord().newGroupInGroup("acis_Reprint_Fl_View_Rp_Text_Udf_2Redef2", "Redefines", 
            acis_Reprint_Fl_View_Rp_Text_Udf_2);
        acis_Reprint_Fl_View_Rp_Orig_Resid_Issue_St = acis_Reprint_Fl_View_Rp_Text_Udf_2Redef2.newFieldInGroup("acis_Reprint_Fl_View_Rp_Orig_Resid_Issue_St", 
            "RP-ORIG-RESID-ISSUE-ST", FieldType.STRING, 2);
        acis_Reprint_Fl_View_Rp_Tsv_Ind = acis_Reprint_Fl_View_Rp_Text_Udf_2Redef2.newFieldInGroup("acis_Reprint_Fl_View_Rp_Tsv_Ind", "RP-TSV-IND", FieldType.STRING, 
            1);
        acis_Reprint_Fl_View_Rp_Spcl_Lgl_Pkg_Ind = acis_Reprint_Fl_View_Rp_Text_Udf_2Redef2.newFieldInGroup("acis_Reprint_Fl_View_Rp_Spcl_Lgl_Pkg_Ind", 
            "RP-SPCL-LGL-PKG-IND", FieldType.STRING, 1);
        acis_Reprint_Fl_View_Rp_Tsr_Ind = acis_Reprint_Fl_View_Rp_Text_Udf_2Redef2.newFieldInGroup("acis_Reprint_Fl_View_Rp_Tsr_Ind", "RP-TSR-IND", FieldType.STRING, 
            3);
        acis_Reprint_Fl_View_Rp_Text_Udf_3 = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Text_Udf_3", "RP-TEXT-UDF-3", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "RP_TEXT_UDF_3");
        acis_Reprint_Fl_View_Rp_Text_Udf_3Redef3 = vw_acis_Reprint_Fl_View.getRecord().newGroupInGroup("acis_Reprint_Fl_View_Rp_Text_Udf_3Redef3", "Redefines", 
            acis_Reprint_Fl_View_Rp_Text_Udf_3);
        acis_Reprint_Fl_View_Rp_Sgrd_Client_Id = acis_Reprint_Fl_View_Rp_Text_Udf_3Redef3.newFieldInGroup("acis_Reprint_Fl_View_Rp_Sgrd_Client_Id", "RP-SGRD-CLIENT-ID", 
            FieldType.STRING, 6);
        acis_Reprint_Fl_View_Rp_Portfolio_Type = acis_Reprint_Fl_View_Rp_Text_Udf_3Redef3.newFieldInGroup("acis_Reprint_Fl_View_Rp_Portfolio_Type", "RP-PORTFOLIO-TYPE", 
            FieldType.STRING, 4);
        acis_Reprint_Fl_View_Rp_Replacement_Ind = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Replacement_Ind", "RP-REPLACEMENT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_REPLACEMENT_IND");
        acis_Reprint_Fl_View_Rp_Register_Id = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Register_Id", "RP-REGISTER-ID", 
            FieldType.STRING, 11, RepeatingFieldStrategy.None, "RP_REGISTER_ID");
        acis_Reprint_Fl_View_Rp_Exempt_Ind = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Exempt_Ind", "RP-EXEMPT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_EXEMPT_IND");
        acis_Reprint_Fl_View_Rp_Incmpl_Acct_Ind = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Incmpl_Acct_Ind", "RP-INCMPL-ACCT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_INCMPL_ACCT_IND");
        acis_Reprint_Fl_View_Rp_Autosave_Ind = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Autosave_Ind", "RP-AUTOSAVE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_AUTOSAVE_IND");
        acis_Reprint_Fl_View_Rp_As_Cur_Dflt_Opt = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_As_Cur_Dflt_Opt", "RP-AS-CUR-DFLT-OPT", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_AS_CUR_DFLT_OPT");
        acis_Reprint_Fl_View_Rp_As_Cur_Dflt_Amt = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_As_Cur_Dflt_Amt", "RP-AS-CUR-DFLT-AMT", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "RP_AS_CUR_DFLT_AMT");
        acis_Reprint_Fl_View_Rp_As_Incr_Opt = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_As_Incr_Opt", "RP-AS-INCR-OPT", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_AS_INCR_OPT");
        acis_Reprint_Fl_View_Rp_As_Incr_Amt = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_As_Incr_Amt", "RP-AS-INCR-AMT", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "RP_AS_INCR_AMT");
        acis_Reprint_Fl_View_Rp_As_Max_Pct = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_As_Max_Pct", "RP-AS-MAX-PCT", 
            FieldType.NUMERIC, 7, 2, RepeatingFieldStrategy.None, "RP_AS_MAX_PCT");
        acis_Reprint_Fl_View_Rp_Ae_Opt_Out_Days = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Ae_Opt_Out_Days", "RP-AE-OPT-OUT-DAYS", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "RP_AE_OPT_OUT_DAYS");
        acis_Reprint_Fl_View_Rp_Welcome_Mail_Dt = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Welcome_Mail_Dt", "RP-WELCOME-MAIL-DT", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RP_WELCOME_MAIL_DT");
        acis_Reprint_Fl_View_Rp_Legal_Mail_Dt = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Legal_Mail_Dt", "RP-LEGAL-MAIL-DT", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RP_LEGAL_MAIL_DT");
        acis_Reprint_Fl_View_Rp_Delete_User_Id = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Delete_User_Id", "RP-DELETE-USER-ID", 
            FieldType.STRING, 16, RepeatingFieldStrategy.None, "RP_DELETE_USER_ID");
        acis_Reprint_Fl_View_Rp_Delete_Reason_Cd = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Delete_Reason_Cd", "RP-DELETE-REASON-CD", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_DELETE_REASON_CD");
        acis_Reprint_Fl_View_Rp_Dt_Deleted = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Dt_Deleted", "RP-DT-DELETED", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RP_DT_DELETED");
        acis_Reprint_Fl_View_Rp_Agent_Crd_No = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Agent_Crd_No", "RP-AGENT-CRD-NO", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RP_AGENT_CRD_NO");
        acis_Reprint_Fl_View_Rp_Welc_E_Delivery_Ind = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Welc_E_Delivery_Ind", 
            "RP-WELC-E-DELIVERY-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_WELC_E_DELIVERY_IND");
        acis_Reprint_Fl_View_Rp_Welc_E_Delivery_Ind.setDdmHeader("WELC EDLVRY IND");
        acis_Reprint_Fl_View_Rp_Legal_E_Delivery_Ind = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Legal_E_Delivery_Ind", 
            "RP-LEGAL-E-DELIVERY-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_LEGAL_E_DELIVERY_IND");
        acis_Reprint_Fl_View_Rp_Legal_E_Delivery_Ind.setDdmHeader("LEGAL EDLVRY IND");
        acis_Reprint_Fl_View_Rp_Legal_Ann_Option = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Legal_Ann_Option", "RP-LEGAL-ANN-OPTION", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_LEGAL_ANN_OPTION");
        acis_Reprint_Fl_View_Rp_Orchestration_Id = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Orchestration_Id", "RP-ORCHESTRATION-ID", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "RP_ORCHESTRATION_ID");
        acis_Reprint_Fl_View_Rp_Mail_Addr_Country_Cd = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Mail_Addr_Country_Cd", 
            "RP-MAIL-ADDR-COUNTRY-CD", FieldType.STRING, 3, RepeatingFieldStrategy.None, "RP_MAIL_ADDR_COUNTRY_CD");
        acis_Reprint_Fl_View_Rp_Res_Addr_Info = vw_acis_Reprint_Fl_View.getRecord().newGroupInGroup("acis_Reprint_Fl_View_Rp_Res_Addr_Info", "RP-RES-ADDR-INFO", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_RPRNT_FILE_RP_RES_ADDR_INFO");
        acis_Reprint_Fl_View_Rp_Res_Addr_Line = acis_Reprint_Fl_View_Rp_Res_Addr_Info.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Res_Addr_Line", "RP-RES-ADDR-LINE", 
            FieldType.STRING, 35, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RES_ADDR_LINE", "ACIS_RPRNT_FILE_RP_RES_ADDR_INFO");
        acis_Reprint_Fl_View_Rp_Res_Addr_City = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Res_Addr_City", "RP-RES-ADDR-CITY", 
            FieldType.STRING, 27, RepeatingFieldStrategy.None, "RP_RES_ADDR_CITY");
        acis_Reprint_Fl_View_Rp_Res_Addr_State_Code = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Res_Addr_State_Code", 
            "RP-RES-ADDR-STATE-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "RP_RES_ADDR_STATE_CODE");
        acis_Reprint_Fl_View_Rp_Res_Addr_Mail_Zip = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Res_Addr_Mail_Zip", "RP-RES-ADDR-MAIL-ZIP", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "RP_RES_ADDR_MAIL_ZIP");
        acis_Reprint_Fl_View_Rp_Res_Addr_Country_Cd = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Res_Addr_Country_Cd", 
            "RP-RES-ADDR-COUNTRY-CD", FieldType.STRING, 3, RepeatingFieldStrategy.None, "RP_RES_ADDR_COUNTRY_CD");
        acis_Reprint_Fl_View_Rp_Tic_Startdate = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Tic_Startdate", "RP-TIC-STARTDATE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RP_TIC_STARTDATE");
        acis_Reprint_Fl_View_Rp_Tic_Enddate = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Tic_Enddate", "RP-TIC-ENDDATE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RP_TIC_ENDDATE");
        acis_Reprint_Fl_View_Rp_Tic_Percentage = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Tic_Percentage", "RP-TIC-PERCENTAGE", 
            FieldType.NUMERIC, 15, 6, RepeatingFieldStrategy.None, "RP_TIC_PERCENTAGE");
        acis_Reprint_Fl_View_Rp_Tic_Postdays = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Tic_Postdays", "RP-TIC-POSTDAYS", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "RP_TIC_POSTDAYS");
        acis_Reprint_Fl_View_Rp_Tic_Limit = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Tic_Limit", "RP-TIC-LIMIT", FieldType.NUMERIC, 
            13, 2, RepeatingFieldStrategy.None, "RP_TIC_LIMIT");
        acis_Reprint_Fl_View_Rp_Tic_Postfreq = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Tic_Postfreq", "RP-TIC-POSTFREQ", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_TIC_POSTFREQ");
        acis_Reprint_Fl_View_Rp_Tic_Pl_Level = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Tic_Pl_Level", "RP-TIC-PL-LEVEL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_TIC_PL_LEVEL");
        acis_Reprint_Fl_View_Rp_Tic_Windowdays = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Tic_Windowdays", "RP-TIC-WINDOWDAYS", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "RP_TIC_WINDOWDAYS");
        acis_Reprint_Fl_View_Rp_Tic_Reqdlywindow = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Tic_Reqdlywindow", "RP-TIC-REQDLYWINDOW", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "RP_TIC_REQDLYWINDOW");
        acis_Reprint_Fl_View_Rp_Tic_Recap_Prov = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Tic_Recap_Prov", "RP-TIC-RECAP-PROV", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "RP_TIC_RECAP_PROV");
        acis_Reprint_Fl_View_Rp_Ann_Funding_Dt = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Ann_Funding_Dt", "RP-ANN-FUNDING-DT", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RP_ANN_FUNDING_DT");
        acis_Reprint_Fl_View_Rp_Tiaa_Ann_Issue_Dt = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Tiaa_Ann_Issue_Dt", "RP-TIAA-ANN-ISSUE-DT", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RP_TIAA_ANN_ISSUE_DT");
        acis_Reprint_Fl_View_Rp_Cref_Ann_Issue_Dt = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Cref_Ann_Issue_Dt", "RP-CREF-ANN-ISSUE-DT", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RP_CREF_ANN_ISSUE_DT");
        acis_Reprint_Fl_View_Rp_Substitution_Contract_Ind = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Substitution_Contract_Ind", 
            "RP-SUBSTITUTION-CONTRACT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_SUBSTITUTION_CONTRACT_IND");
        acis_Reprint_Fl_View_Rp_Conv_Issue_State = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Conv_Issue_State", "RP-CONV-ISSUE-STATE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "RP_CONV_ISSUE_STATE");
        acis_Reprint_Fl_View_Rp_Deceased_Ind = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Deceased_Ind", "RP-DECEASED-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_DECEASED_IND");
        acis_Reprint_Fl_View_Rp_Non_Proprietary_Pkg_Ind = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Non_Proprietary_Pkg_Ind", 
            "RP-NON-PROPRIETARY-PKG-IND", FieldType.STRING, 5, RepeatingFieldStrategy.None, "RP_NON_PROPRIETARY_PKG_IND");
        acis_Reprint_Fl_View_Rp_Multi_Plan_Info = vw_acis_Reprint_Fl_View.getRecord().newGroupInGroup("acis_Reprint_Fl_View_Rp_Multi_Plan_Info", "RP-MULTI-PLAN-INFO", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_RPRNT_FILE_RP_MULTI_PLAN_INFO");
        acis_Reprint_Fl_View_Rp_Multi_Plan_No = acis_Reprint_Fl_View_Rp_Multi_Plan_Info.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Multi_Plan_No", 
            "RP-MULTI-PLAN-NO", FieldType.STRING, 6, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_MULTI_PLAN_NO", 
            "ACIS_RPRNT_FILE_RP_MULTI_PLAN_INFO");
        acis_Reprint_Fl_View_Rp_Multi_Sub_Plan = acis_Reprint_Fl_View_Rp_Multi_Plan_Info.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Multi_Sub_Plan", 
            "RP-MULTI-SUB-PLAN", FieldType.STRING, 6, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_MULTI_SUB_PLAN", 
            "ACIS_RPRNT_FILE_RP_MULTI_PLAN_INFO");
        acis_Reprint_Fl_View_Rp_Decedent_Contract = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Decedent_Contract", "RP-DECEDENT-CONTRACT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "RP_DECEDENT_CONTRACT");
        acis_Reprint_Fl_View_Rp_Relation_To_Decedent = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Relation_To_Decedent", 
            "RP-RELATION-TO-DECEDENT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_RELATION_TO_DECEDENT");
        acis_Reprint_Fl_View_Rp_Ssn_Tin_Ind = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Ssn_Tin_Ind", "RP-SSN-TIN-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_SSN_TIN_IND");
        acis_Reprint_Fl_View_Rp_Oneira_Acct_No = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Oneira_Acct_No", "RP-ONEIRA-ACCT-NO", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "RP_ONEIRA_ACCT_NO");
        acis_Reprint_Fl_View_Rp_Financial_2 = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Financial_2", "RP-FINANCIAL-2", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "RP_FINANCIAL_2");
        acis_Reprint_Fl_View_Rp_Financial_3 = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Financial_3", "RP-FINANCIAL-3", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "RP_FINANCIAL_3");
        acis_Reprint_Fl_View_Rp_Financial_4 = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Financial_4", "RP-FINANCIAL-4", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "RP_FINANCIAL_4");
        acis_Reprint_Fl_View_Rp_Financial_5 = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Financial_5", "RP-FINANCIAL-5", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "RP_FINANCIAL_5");
        acis_Reprint_Fl_View_Rp_Source_Cde_1 = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Source_Cde_1", "RP-SOURCE-CDE-1", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_SOURCE_CDE_1");
        acis_Reprint_Fl_View_Rp_Source_Cde_2 = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Rp_Source_Cde_2", "RP-SOURCE-CDE-2", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_SOURCE_CDE_2");
        acis_Reprint_Fl_View_Rp_Fund_Identifier_2 = vw_acis_Reprint_Fl_View.getRecord().newGroupInGroup("acis_Reprint_Fl_View_Rp_Fund_Identifier_2", "RP-FUND-IDENTIFIER-2", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_RPRNT_FILE_RP_FUND_IDENTIFIER_2");
        acis_Reprint_Fl_View_Rp_Fund_Cde_2 = acis_Reprint_Fl_View_Rp_Fund_Identifier_2.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Fund_Cde_2", "RP-FUND-CDE-2", 
            FieldType.STRING, 10, new DbsArrayController(1,100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_FUND_CDE_2", "ACIS_RPRNT_FILE_RP_FUND_IDENTIFIER_2");
        acis_Reprint_Fl_View_Rp_Allocation_Pct_2 = acis_Reprint_Fl_View_Rp_Fund_Identifier_2.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Allocation_Pct_2", 
            "RP-ALLOCATION-PCT-2", FieldType.NUMERIC, 3, new DbsArrayController(1,100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_ALLOCATION_PCT_2", 
            "ACIS_RPRNT_FILE_RP_FUND_IDENTIFIER_2");
        acis_Reprint_Fl_View_Count_Castrp_Related_Contract_Info = vw_acis_Reprint_Fl_View.getRecord().newFieldInGroup("acis_Reprint_Fl_View_Count_Castrp_Related_Contract_Info", 
            "C*RP-RELATED-CONTRACT-INFO", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_View_Rp_Related_Contract_Info = vw_acis_Reprint_Fl_View.getRecord().newGroupInGroup("acis_Reprint_Fl_View_Rp_Related_Contract_Info", 
            "RP-RELATED-CONTRACT-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_View_Rp_Related_Contract_Type = acis_Reprint_Fl_View_Rp_Related_Contract_Info.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Related_Contract_Type", 
            "RP-RELATED-CONTRACT-TYPE", FieldType.STRING, 1, new DbsArrayController(1,30) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_CONTRACT_TYPE", 
            "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_View_Rp_Related_Tiaa_No = acis_Reprint_Fl_View_Rp_Related_Contract_Info.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Related_Tiaa_No", 
            "RP-RELATED-TIAA-NO", FieldType.STRING, 10, new DbsArrayController(1,30) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_TIAA_NO", 
            "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_View_Rp_Related_Tiaa_Issue_Date = acis_Reprint_Fl_View_Rp_Related_Contract_Info.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Related_Tiaa_Issue_Date", 
            "RP-RELATED-TIAA-ISSUE-DATE", FieldType.NUMERIC, 8, new DbsArrayController(1,30) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_TIAA_ISSUE_DATE", 
            "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_View_Rp_Related_First_Payment_Date = acis_Reprint_Fl_View_Rp_Related_Contract_Info.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Related_First_Payment_Date", 
            "RP-RELATED-FIRST-PAYMENT-DATE", FieldType.NUMERIC, 8, new DbsArrayController(1,30) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_FIRST_PAYMENT_DATE", 
            "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_View_Rp_Related_Tiaa_Total_Amt = acis_Reprint_Fl_View_Rp_Related_Contract_Info.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Related_Tiaa_Total_Amt", 
            "RP-RELATED-TIAA-TOTAL-AMT", FieldType.NUMERIC, 10, 2, new DbsArrayController(1,30) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_TIAA_TOTAL_AMT", 
            "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_View_Rp_Related_Last_Payment_Date = acis_Reprint_Fl_View_Rp_Related_Contract_Info.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Related_Last_Payment_Date", 
            "RP-RELATED-LAST-PAYMENT-DATE", FieldType.NUMERIC, 8, new DbsArrayController(1,30) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_LAST_PAYMENT_DATE", 
            "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_View_Rp_Related_Payment_Frequency = acis_Reprint_Fl_View_Rp_Related_Contract_Info.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Related_Payment_Frequency", 
            "RP-RELATED-PAYMENT-FREQUENCY", FieldType.STRING, 1, new DbsArrayController(1,30) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_PAYMENT_FREQUENCY", 
            "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_View_Rp_Related_Tiaa_Issue_State = acis_Reprint_Fl_View_Rp_Related_Contract_Info.newFieldArrayInGroup("acis_Reprint_Fl_View_Rp_Related_Tiaa_Issue_State", 
            "RP-RELATED-TIAA-ISSUE-STATE", FieldType.STRING, 2, new DbsArrayController(1,30) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_TIAA_ISSUE_STATE", 
            "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        vw_acis_Reprint_Fl_View.setUniquePeList();

        this.setRecordName("LdaAppl180");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_acis_Reprint_Fl_View.reset();
    }

    // Constructor
    public LdaAppl180() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
