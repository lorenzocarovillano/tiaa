/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:17:48 PM
**        * FROM NATURAL PDA     : APPA211
************************************************************
**        * FILE NAME            : PdaAppa211.java
**        * CLASS NAME           : PdaAppa211
**        * INSTANCE NAME        : PdaAppa211
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaAppa211 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Pda_Cor_Batch;
    private DbsField pnd_Pda_Cor_Batch_Pnd_Par_To_Rcd_Type_Cde;
    private DbsField pnd_Pda_Cor_Batch_Pnd_Par_To_Social_Security_No;
    private DbsField pnd_Pda_Cor_Batch_Pnd_Par_Fm_Unique_Id_Number;
    private DbsField pnd_Pda_Cor_Batch_Pnd_Par_Fm_Prfx_Nme;
    private DbsField pnd_Pda_Cor_Batch_Pnd_Par_Fm_Last_Nme;
    private DbsField pnd_Pda_Cor_Batch_Pnd_Par_Fm_First_Nme;
    private DbsField pnd_Pda_Cor_Batch_Pnd_Par_Fm_Mddle_Nme;
    private DbsField pnd_Pda_Cor_Batch_Pnd_Par_Fm_Sffx_Nme;
    private DbsField pnd_Pda_Cor_Batch_Pnd_Par_Fm_His_Ind;

    public DbsGroup getPnd_Pda_Cor_Batch() { return pnd_Pda_Cor_Batch; }

    public DbsField getPnd_Pda_Cor_Batch_Pnd_Par_To_Rcd_Type_Cde() { return pnd_Pda_Cor_Batch_Pnd_Par_To_Rcd_Type_Cde; }

    public DbsField getPnd_Pda_Cor_Batch_Pnd_Par_To_Social_Security_No() { return pnd_Pda_Cor_Batch_Pnd_Par_To_Social_Security_No; }

    public DbsField getPnd_Pda_Cor_Batch_Pnd_Par_Fm_Unique_Id_Number() { return pnd_Pda_Cor_Batch_Pnd_Par_Fm_Unique_Id_Number; }

    public DbsField getPnd_Pda_Cor_Batch_Pnd_Par_Fm_Prfx_Nme() { return pnd_Pda_Cor_Batch_Pnd_Par_Fm_Prfx_Nme; }

    public DbsField getPnd_Pda_Cor_Batch_Pnd_Par_Fm_Last_Nme() { return pnd_Pda_Cor_Batch_Pnd_Par_Fm_Last_Nme; }

    public DbsField getPnd_Pda_Cor_Batch_Pnd_Par_Fm_First_Nme() { return pnd_Pda_Cor_Batch_Pnd_Par_Fm_First_Nme; }

    public DbsField getPnd_Pda_Cor_Batch_Pnd_Par_Fm_Mddle_Nme() { return pnd_Pda_Cor_Batch_Pnd_Par_Fm_Mddle_Nme; }

    public DbsField getPnd_Pda_Cor_Batch_Pnd_Par_Fm_Sffx_Nme() { return pnd_Pda_Cor_Batch_Pnd_Par_Fm_Sffx_Nme; }

    public DbsField getPnd_Pda_Cor_Batch_Pnd_Par_Fm_His_Ind() { return pnd_Pda_Cor_Batch_Pnd_Par_Fm_His_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Pda_Cor_Batch = dbsRecord.newGroupInRecord("pnd_Pda_Cor_Batch", "#PDA-COR-BATCH");
        pnd_Pda_Cor_Batch.setParameterOption(ParameterOption.ByReference);
        pnd_Pda_Cor_Batch_Pnd_Par_To_Rcd_Type_Cde = pnd_Pda_Cor_Batch.newFieldInGroup("pnd_Pda_Cor_Batch_Pnd_Par_To_Rcd_Type_Cde", "#PAR-TO-RCD-TYPE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Pda_Cor_Batch_Pnd_Par_To_Social_Security_No = pnd_Pda_Cor_Batch.newFieldInGroup("pnd_Pda_Cor_Batch_Pnd_Par_To_Social_Security_No", "#PAR-TO-SOCIAL-SECURITY-NO", 
            FieldType.NUMERIC, 9);
        pnd_Pda_Cor_Batch_Pnd_Par_Fm_Unique_Id_Number = pnd_Pda_Cor_Batch.newFieldInGroup("pnd_Pda_Cor_Batch_Pnd_Par_Fm_Unique_Id_Number", "#PAR-FM-UNIQUE-ID-NUMBER", 
            FieldType.NUMERIC, 12);
        pnd_Pda_Cor_Batch_Pnd_Par_Fm_Prfx_Nme = pnd_Pda_Cor_Batch.newFieldInGroup("pnd_Pda_Cor_Batch_Pnd_Par_Fm_Prfx_Nme", "#PAR-FM-PRFX-NME", FieldType.STRING, 
            8);
        pnd_Pda_Cor_Batch_Pnd_Par_Fm_Last_Nme = pnd_Pda_Cor_Batch.newFieldInGroup("pnd_Pda_Cor_Batch_Pnd_Par_Fm_Last_Nme", "#PAR-FM-LAST-NME", FieldType.STRING, 
            30);
        pnd_Pda_Cor_Batch_Pnd_Par_Fm_First_Nme = pnd_Pda_Cor_Batch.newFieldInGroup("pnd_Pda_Cor_Batch_Pnd_Par_Fm_First_Nme", "#PAR-FM-FIRST-NME", FieldType.STRING, 
            30);
        pnd_Pda_Cor_Batch_Pnd_Par_Fm_Mddle_Nme = pnd_Pda_Cor_Batch.newFieldInGroup("pnd_Pda_Cor_Batch_Pnd_Par_Fm_Mddle_Nme", "#PAR-FM-MDDLE-NME", FieldType.STRING, 
            30);
        pnd_Pda_Cor_Batch_Pnd_Par_Fm_Sffx_Nme = pnd_Pda_Cor_Batch.newFieldInGroup("pnd_Pda_Cor_Batch_Pnd_Par_Fm_Sffx_Nme", "#PAR-FM-SFFX-NME", FieldType.STRING, 
            8);
        pnd_Pda_Cor_Batch_Pnd_Par_Fm_His_Ind = pnd_Pda_Cor_Batch.newFieldInGroup("pnd_Pda_Cor_Batch_Pnd_Par_Fm_His_Ind", "#PAR-FM-HIS-IND", FieldType.STRING, 
            1);

        dbsRecord.reset();
    }

    // Constructors
    public PdaAppa211(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

