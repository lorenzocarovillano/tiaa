/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:51:03 PM
**        * FROM NATURAL LDA     : APPL230
************************************************************
**        * FILE NAME            : LdaAppl230.java
**        * CLASS NAME           : LdaAppl230
**        * INSTANCE NAME        : LdaAppl230
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl230 extends DbsRecord
{
    // Properties
    private DbsGroup da_Sra_Summary_Trans;
    private DbsField da_Sra_Summary_Trans_Code;
    private DbsField da_Sra_Summary_Trans_Region_Code;
    private DbsGroup da_Sra_Summary_Trans_Region_CodeRedef1;
    private DbsField da_Sra_Summary_Trans_Region_Num;
    private DbsField da_Sra_Summary_Trans_Region_Branch;
    private DbsField da_Sra_Summary_Trans_Region_Need;
    private DbsField da_Sra_Summary_Trans_College_Code;
    private DbsGroup da_Sra_Summary_Trans_College_CodeRedef2;
    private DbsField da_Sra_Summary_Trans_Coll_Code;
    private DbsField da_Sra_Summary_Trans_Coll_Code_Filler;
    private DbsField da_Sra_Summary_Trans_Tiaa_Contract;
    private DbsGroup da_Sra_Summary_Trans_Tiaa_ContractRedef3;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_Tcont1;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_Tcont2;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_Tcont3;
    private DbsField da_Sra_Summary_Trans_Trans_Type;
    private DbsField da_Sra_Summary_Trans_Trans_Month;
    private DbsField da_Sra_Summary_Trans_Trans_Day;
    private DbsField da_Sra_Summary_Trans_Trans_Year;
    private DbsField da_Sra_Summary_Trans_Cref_Contract;
    private DbsGroup da_Sra_Summary_Trans_Cref_ContractRedef4;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_Ccont1;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_Ccont2;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_Ccont3;
    private DbsField da_Sra_Summary_Trans_Name_Key;
    private DbsField da_Sra_Summary_Trans_Soc_Sec_Num;
    private DbsGroup da_Sra_Summary_Trans_Soc_Sec_NumRedef5;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_Ssn1;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_Ssn2;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_Ssn3;
    private DbsField da_Sra_Summary_Trans_Dob;
    private DbsGroup da_Sra_Summary_Trans_DobRedef6;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_Dob_Mm;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_Dob_Dd;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_Dob_Yy;
    private DbsField da_Sra_Summary_Trans_Sex;
    private DbsField da_Sra_Summary_Trans_Currency;
    private DbsField da_Sra_Summary_Trans_Ownership;
    private DbsField da_Sra_Summary_Trans_Vesting_Date;
    private DbsField da_Sra_Summary_Trans_Status;
    private DbsField da_Sra_Summary_Trans_State_Code;
    private DbsField da_Sra_Summary_Trans_Tiaa_Age_1st_Paymnt;
    private DbsGroup da_Sra_Summary_Trans_Tiaa_Age_1st_PaymntRedef7;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_T_A_Yy;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_T_A_Mm;
    private DbsField da_Sra_Summary_Trans_Cref_Age_1st_Paymnt;
    private DbsGroup da_Sra_Summary_Trans_Cref_Age_1st_PaymntRedef8;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_C_A_Yy;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_C_A_Mm;
    private DbsField da_Sra_Summary_Trans_Tiaa_Doi;
    private DbsGroup da_Sra_Summary_Trans_Tiaa_DoiRedef9;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_T_Doi_Mm;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_T_Doi_Yy;
    private DbsField da_Sra_Summary_Trans_Cref_Doi;
    private DbsGroup da_Sra_Summary_Trans_Cref_DoiRedef10;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_C_Doi_Mm;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_C_Doi_Yy;
    private DbsField da_Sra_Summary_Trans_Name;
    private DbsField da_Sra_Summary_Trans_Date_App_Recvd;
    private DbsGroup da_Sra_Summary_Trans_Date_App_RecvdRedef11;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_App_Mm;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_App_Dd;
    private DbsField da_Sra_Summary_Trans_Pnd_Wk_App_Yy;
    private DbsField da_Sra_Summary_Trans_Alloc_Fmt;
    private DbsField da_Sra_Summary_Trans_Alloc_Fund_Cde;
    private DbsField da_Sra_Summary_Trans_Alloc;
    private DbsField da_Sra_Summary_Trans_Appl_Source;
    private DbsField da_Sra_Summary_Trans_Split_Code;
    private DbsField da_Sra_Summary_Trans_Lob;
    private DbsField da_Sra_Summary_Trans_Lob_Type;
    private DbsField da_Sra_Summary_Trans_Cor_Hist_Ind;
    private DbsField da_Sra_Summary_Trans_Pnd_Iis_Prem_Team_Code;
    private DbsField da_Sra_Summary_Trans_Sg_Plan_No;
    private DbsField da_Sra_Summary_Trans_Sg_Subplan_No;
    private DbsField da_Sra_Summary_Trans_Sg_Part_Ext;
    private DbsField filler01;
    private DbsGroup da_Sra_Summary_TransRedef12;
    private DbsField da_Sra_Summary_Trans_Da_Sra_1;
    private DbsField da_Sra_Summary_Trans_Da_Sra_2;
    private DbsField da_Sra_Summary_Trans_Da_Sra_3;
    private DbsField da_Sra_Summary_Trans_Da_Sra_4;
    private DbsField da_Sra_Summary_Trans_Da_Sra_5;
    private DbsField da_Sra_Summary_Trans_Da_Sra_6;

    public DbsGroup getDa_Sra_Summary_Trans() { return da_Sra_Summary_Trans; }

    public DbsField getDa_Sra_Summary_Trans_Code() { return da_Sra_Summary_Trans_Code; }

    public DbsField getDa_Sra_Summary_Trans_Region_Code() { return da_Sra_Summary_Trans_Region_Code; }

    public DbsGroup getDa_Sra_Summary_Trans_Region_CodeRedef1() { return da_Sra_Summary_Trans_Region_CodeRedef1; }

    public DbsField getDa_Sra_Summary_Trans_Region_Num() { return da_Sra_Summary_Trans_Region_Num; }

    public DbsField getDa_Sra_Summary_Trans_Region_Branch() { return da_Sra_Summary_Trans_Region_Branch; }

    public DbsField getDa_Sra_Summary_Trans_Region_Need() { return da_Sra_Summary_Trans_Region_Need; }

    public DbsField getDa_Sra_Summary_Trans_College_Code() { return da_Sra_Summary_Trans_College_Code; }

    public DbsGroup getDa_Sra_Summary_Trans_College_CodeRedef2() { return da_Sra_Summary_Trans_College_CodeRedef2; }

    public DbsField getDa_Sra_Summary_Trans_Coll_Code() { return da_Sra_Summary_Trans_Coll_Code; }

    public DbsField getDa_Sra_Summary_Trans_Coll_Code_Filler() { return da_Sra_Summary_Trans_Coll_Code_Filler; }

    public DbsField getDa_Sra_Summary_Trans_Tiaa_Contract() { return da_Sra_Summary_Trans_Tiaa_Contract; }

    public DbsGroup getDa_Sra_Summary_Trans_Tiaa_ContractRedef3() { return da_Sra_Summary_Trans_Tiaa_ContractRedef3; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_Tcont1() { return da_Sra_Summary_Trans_Pnd_Wk_Tcont1; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_Tcont2() { return da_Sra_Summary_Trans_Pnd_Wk_Tcont2; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_Tcont3() { return da_Sra_Summary_Trans_Pnd_Wk_Tcont3; }

    public DbsField getDa_Sra_Summary_Trans_Trans_Type() { return da_Sra_Summary_Trans_Trans_Type; }

    public DbsField getDa_Sra_Summary_Trans_Trans_Month() { return da_Sra_Summary_Trans_Trans_Month; }

    public DbsField getDa_Sra_Summary_Trans_Trans_Day() { return da_Sra_Summary_Trans_Trans_Day; }

    public DbsField getDa_Sra_Summary_Trans_Trans_Year() { return da_Sra_Summary_Trans_Trans_Year; }

    public DbsField getDa_Sra_Summary_Trans_Cref_Contract() { return da_Sra_Summary_Trans_Cref_Contract; }

    public DbsGroup getDa_Sra_Summary_Trans_Cref_ContractRedef4() { return da_Sra_Summary_Trans_Cref_ContractRedef4; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_Ccont1() { return da_Sra_Summary_Trans_Pnd_Wk_Ccont1; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_Ccont2() { return da_Sra_Summary_Trans_Pnd_Wk_Ccont2; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_Ccont3() { return da_Sra_Summary_Trans_Pnd_Wk_Ccont3; }

    public DbsField getDa_Sra_Summary_Trans_Name_Key() { return da_Sra_Summary_Trans_Name_Key; }

    public DbsField getDa_Sra_Summary_Trans_Soc_Sec_Num() { return da_Sra_Summary_Trans_Soc_Sec_Num; }

    public DbsGroup getDa_Sra_Summary_Trans_Soc_Sec_NumRedef5() { return da_Sra_Summary_Trans_Soc_Sec_NumRedef5; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_Ssn1() { return da_Sra_Summary_Trans_Pnd_Wk_Ssn1; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_Ssn2() { return da_Sra_Summary_Trans_Pnd_Wk_Ssn2; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_Ssn3() { return da_Sra_Summary_Trans_Pnd_Wk_Ssn3; }

    public DbsField getDa_Sra_Summary_Trans_Dob() { return da_Sra_Summary_Trans_Dob; }

    public DbsGroup getDa_Sra_Summary_Trans_DobRedef6() { return da_Sra_Summary_Trans_DobRedef6; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_Dob_Mm() { return da_Sra_Summary_Trans_Pnd_Wk_Dob_Mm; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_Dob_Dd() { return da_Sra_Summary_Trans_Pnd_Wk_Dob_Dd; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_Dob_Yy() { return da_Sra_Summary_Trans_Pnd_Wk_Dob_Yy; }

    public DbsField getDa_Sra_Summary_Trans_Sex() { return da_Sra_Summary_Trans_Sex; }

    public DbsField getDa_Sra_Summary_Trans_Currency() { return da_Sra_Summary_Trans_Currency; }

    public DbsField getDa_Sra_Summary_Trans_Ownership() { return da_Sra_Summary_Trans_Ownership; }

    public DbsField getDa_Sra_Summary_Trans_Vesting_Date() { return da_Sra_Summary_Trans_Vesting_Date; }

    public DbsField getDa_Sra_Summary_Trans_Status() { return da_Sra_Summary_Trans_Status; }

    public DbsField getDa_Sra_Summary_Trans_State_Code() { return da_Sra_Summary_Trans_State_Code; }

    public DbsField getDa_Sra_Summary_Trans_Tiaa_Age_1st_Paymnt() { return da_Sra_Summary_Trans_Tiaa_Age_1st_Paymnt; }

    public DbsGroup getDa_Sra_Summary_Trans_Tiaa_Age_1st_PaymntRedef7() { return da_Sra_Summary_Trans_Tiaa_Age_1st_PaymntRedef7; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_T_A_Yy() { return da_Sra_Summary_Trans_Pnd_Wk_T_A_Yy; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_T_A_Mm() { return da_Sra_Summary_Trans_Pnd_Wk_T_A_Mm; }

    public DbsField getDa_Sra_Summary_Trans_Cref_Age_1st_Paymnt() { return da_Sra_Summary_Trans_Cref_Age_1st_Paymnt; }

    public DbsGroup getDa_Sra_Summary_Trans_Cref_Age_1st_PaymntRedef8() { return da_Sra_Summary_Trans_Cref_Age_1st_PaymntRedef8; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_C_A_Yy() { return da_Sra_Summary_Trans_Pnd_Wk_C_A_Yy; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_C_A_Mm() { return da_Sra_Summary_Trans_Pnd_Wk_C_A_Mm; }

    public DbsField getDa_Sra_Summary_Trans_Tiaa_Doi() { return da_Sra_Summary_Trans_Tiaa_Doi; }

    public DbsGroup getDa_Sra_Summary_Trans_Tiaa_DoiRedef9() { return da_Sra_Summary_Trans_Tiaa_DoiRedef9; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_T_Doi_Mm() { return da_Sra_Summary_Trans_Pnd_Wk_T_Doi_Mm; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_T_Doi_Yy() { return da_Sra_Summary_Trans_Pnd_Wk_T_Doi_Yy; }

    public DbsField getDa_Sra_Summary_Trans_Cref_Doi() { return da_Sra_Summary_Trans_Cref_Doi; }

    public DbsGroup getDa_Sra_Summary_Trans_Cref_DoiRedef10() { return da_Sra_Summary_Trans_Cref_DoiRedef10; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_C_Doi_Mm() { return da_Sra_Summary_Trans_Pnd_Wk_C_Doi_Mm; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_C_Doi_Yy() { return da_Sra_Summary_Trans_Pnd_Wk_C_Doi_Yy; }

    public DbsField getDa_Sra_Summary_Trans_Name() { return da_Sra_Summary_Trans_Name; }

    public DbsField getDa_Sra_Summary_Trans_Date_App_Recvd() { return da_Sra_Summary_Trans_Date_App_Recvd; }

    public DbsGroup getDa_Sra_Summary_Trans_Date_App_RecvdRedef11() { return da_Sra_Summary_Trans_Date_App_RecvdRedef11; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_App_Mm() { return da_Sra_Summary_Trans_Pnd_Wk_App_Mm; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_App_Dd() { return da_Sra_Summary_Trans_Pnd_Wk_App_Dd; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Wk_App_Yy() { return da_Sra_Summary_Trans_Pnd_Wk_App_Yy; }

    public DbsField getDa_Sra_Summary_Trans_Alloc_Fmt() { return da_Sra_Summary_Trans_Alloc_Fmt; }

    public DbsField getDa_Sra_Summary_Trans_Alloc_Fund_Cde() { return da_Sra_Summary_Trans_Alloc_Fund_Cde; }

    public DbsField getDa_Sra_Summary_Trans_Alloc() { return da_Sra_Summary_Trans_Alloc; }

    public DbsField getDa_Sra_Summary_Trans_Appl_Source() { return da_Sra_Summary_Trans_Appl_Source; }

    public DbsField getDa_Sra_Summary_Trans_Split_Code() { return da_Sra_Summary_Trans_Split_Code; }

    public DbsField getDa_Sra_Summary_Trans_Lob() { return da_Sra_Summary_Trans_Lob; }

    public DbsField getDa_Sra_Summary_Trans_Lob_Type() { return da_Sra_Summary_Trans_Lob_Type; }

    public DbsField getDa_Sra_Summary_Trans_Cor_Hist_Ind() { return da_Sra_Summary_Trans_Cor_Hist_Ind; }

    public DbsField getDa_Sra_Summary_Trans_Pnd_Iis_Prem_Team_Code() { return da_Sra_Summary_Trans_Pnd_Iis_Prem_Team_Code; }

    public DbsField getDa_Sra_Summary_Trans_Sg_Plan_No() { return da_Sra_Summary_Trans_Sg_Plan_No; }

    public DbsField getDa_Sra_Summary_Trans_Sg_Subplan_No() { return da_Sra_Summary_Trans_Sg_Subplan_No; }

    public DbsField getDa_Sra_Summary_Trans_Sg_Part_Ext() { return da_Sra_Summary_Trans_Sg_Part_Ext; }

    public DbsField getFiller01() { return filler01; }

    public DbsGroup getDa_Sra_Summary_TransRedef12() { return da_Sra_Summary_TransRedef12; }

    public DbsField getDa_Sra_Summary_Trans_Da_Sra_1() { return da_Sra_Summary_Trans_Da_Sra_1; }

    public DbsField getDa_Sra_Summary_Trans_Da_Sra_2() { return da_Sra_Summary_Trans_Da_Sra_2; }

    public DbsField getDa_Sra_Summary_Trans_Da_Sra_3() { return da_Sra_Summary_Trans_Da_Sra_3; }

    public DbsField getDa_Sra_Summary_Trans_Da_Sra_4() { return da_Sra_Summary_Trans_Da_Sra_4; }

    public DbsField getDa_Sra_Summary_Trans_Da_Sra_5() { return da_Sra_Summary_Trans_Da_Sra_5; }

    public DbsField getDa_Sra_Summary_Trans_Da_Sra_6() { return da_Sra_Summary_Trans_Da_Sra_6; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        da_Sra_Summary_Trans = newGroupInRecord("da_Sra_Summary_Trans", "DA-SRA-SUMMARY-TRANS");
        da_Sra_Summary_Trans_Code = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Code", "CODE", FieldType.NUMERIC, 1);
        da_Sra_Summary_Trans_Region_Code = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Region_Code", "REGION-CODE", FieldType.STRING, 3);
        da_Sra_Summary_Trans_Region_CodeRedef1 = da_Sra_Summary_Trans.newGroupInGroup("da_Sra_Summary_Trans_Region_CodeRedef1", "Redefines", da_Sra_Summary_Trans_Region_Code);
        da_Sra_Summary_Trans_Region_Num = da_Sra_Summary_Trans_Region_CodeRedef1.newFieldInGroup("da_Sra_Summary_Trans_Region_Num", "REGION-NUM", FieldType.STRING, 
            1);
        da_Sra_Summary_Trans_Region_Branch = da_Sra_Summary_Trans_Region_CodeRedef1.newFieldInGroup("da_Sra_Summary_Trans_Region_Branch", "REGION-BRANCH", 
            FieldType.STRING, 1);
        da_Sra_Summary_Trans_Region_Need = da_Sra_Summary_Trans_Region_CodeRedef1.newFieldInGroup("da_Sra_Summary_Trans_Region_Need", "REGION-NEED", FieldType.STRING, 
            1);
        da_Sra_Summary_Trans_College_Code = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_College_Code", "COLLEGE-CODE", FieldType.STRING, 
            5);
        da_Sra_Summary_Trans_College_CodeRedef2 = da_Sra_Summary_Trans.newGroupInGroup("da_Sra_Summary_Trans_College_CodeRedef2", "Redefines", da_Sra_Summary_Trans_College_Code);
        da_Sra_Summary_Trans_Coll_Code = da_Sra_Summary_Trans_College_CodeRedef2.newFieldInGroup("da_Sra_Summary_Trans_Coll_Code", "COLL-CODE", FieldType.STRING, 
            4);
        da_Sra_Summary_Trans_Coll_Code_Filler = da_Sra_Summary_Trans_College_CodeRedef2.newFieldInGroup("da_Sra_Summary_Trans_Coll_Code_Filler", "COLL-CODE-FILLER", 
            FieldType.STRING, 1);
        da_Sra_Summary_Trans_Tiaa_Contract = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Tiaa_Contract", "TIAA-CONTRACT", FieldType.STRING, 
            8);
        da_Sra_Summary_Trans_Tiaa_ContractRedef3 = da_Sra_Summary_Trans.newGroupInGroup("da_Sra_Summary_Trans_Tiaa_ContractRedef3", "Redefines", da_Sra_Summary_Trans_Tiaa_Contract);
        da_Sra_Summary_Trans_Pnd_Wk_Tcont1 = da_Sra_Summary_Trans_Tiaa_ContractRedef3.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_Tcont1", "#WK-TCONT1", 
            FieldType.STRING, 4);
        da_Sra_Summary_Trans_Pnd_Wk_Tcont2 = da_Sra_Summary_Trans_Tiaa_ContractRedef3.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_Tcont2", "#WK-TCONT2", 
            FieldType.STRING, 3);
        da_Sra_Summary_Trans_Pnd_Wk_Tcont3 = da_Sra_Summary_Trans_Tiaa_ContractRedef3.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_Tcont3", "#WK-TCONT3", 
            FieldType.STRING, 1);
        da_Sra_Summary_Trans_Trans_Type = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Trans_Type", "TRANS-TYPE", FieldType.NUMERIC, 2);
        da_Sra_Summary_Trans_Trans_Month = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Trans_Month", "TRANS-MONTH", FieldType.NUMERIC, 
            2);
        da_Sra_Summary_Trans_Trans_Day = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Trans_Day", "TRANS-DAY", FieldType.NUMERIC, 2);
        da_Sra_Summary_Trans_Trans_Year = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Trans_Year", "TRANS-YEAR", FieldType.NUMERIC, 2);
        da_Sra_Summary_Trans_Cref_Contract = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Cref_Contract", "CREF-CONTRACT", FieldType.STRING, 
            8);
        da_Sra_Summary_Trans_Cref_ContractRedef4 = da_Sra_Summary_Trans.newGroupInGroup("da_Sra_Summary_Trans_Cref_ContractRedef4", "Redefines", da_Sra_Summary_Trans_Cref_Contract);
        da_Sra_Summary_Trans_Pnd_Wk_Ccont1 = da_Sra_Summary_Trans_Cref_ContractRedef4.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_Ccont1", "#WK-CCONT1", 
            FieldType.STRING, 4);
        da_Sra_Summary_Trans_Pnd_Wk_Ccont2 = da_Sra_Summary_Trans_Cref_ContractRedef4.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_Ccont2", "#WK-CCONT2", 
            FieldType.STRING, 3);
        da_Sra_Summary_Trans_Pnd_Wk_Ccont3 = da_Sra_Summary_Trans_Cref_ContractRedef4.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_Ccont3", "#WK-CCONT3", 
            FieldType.STRING, 1);
        da_Sra_Summary_Trans_Name_Key = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Name_Key", "NAME-KEY", FieldType.STRING, 9);
        da_Sra_Summary_Trans_Soc_Sec_Num = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Soc_Sec_Num", "SOC-SEC-NUM", FieldType.NUMERIC, 
            9);
        da_Sra_Summary_Trans_Soc_Sec_NumRedef5 = da_Sra_Summary_Trans.newGroupInGroup("da_Sra_Summary_Trans_Soc_Sec_NumRedef5", "Redefines", da_Sra_Summary_Trans_Soc_Sec_Num);
        da_Sra_Summary_Trans_Pnd_Wk_Ssn1 = da_Sra_Summary_Trans_Soc_Sec_NumRedef5.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_Ssn1", "#WK-SSN1", FieldType.STRING, 
            3);
        da_Sra_Summary_Trans_Pnd_Wk_Ssn2 = da_Sra_Summary_Trans_Soc_Sec_NumRedef5.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_Ssn2", "#WK-SSN2", FieldType.STRING, 
            2);
        da_Sra_Summary_Trans_Pnd_Wk_Ssn3 = da_Sra_Summary_Trans_Soc_Sec_NumRedef5.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_Ssn3", "#WK-SSN3", FieldType.STRING, 
            4);
        da_Sra_Summary_Trans_Dob = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Dob", "DOB", FieldType.NUMERIC, 6);
        da_Sra_Summary_Trans_DobRedef6 = da_Sra_Summary_Trans.newGroupInGroup("da_Sra_Summary_Trans_DobRedef6", "Redefines", da_Sra_Summary_Trans_Dob);
        da_Sra_Summary_Trans_Pnd_Wk_Dob_Mm = da_Sra_Summary_Trans_DobRedef6.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_Dob_Mm", "#WK-DOB-MM", FieldType.STRING, 
            2);
        da_Sra_Summary_Trans_Pnd_Wk_Dob_Dd = da_Sra_Summary_Trans_DobRedef6.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_Dob_Dd", "#WK-DOB-DD", FieldType.STRING, 
            2);
        da_Sra_Summary_Trans_Pnd_Wk_Dob_Yy = da_Sra_Summary_Trans_DobRedef6.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_Dob_Yy", "#WK-DOB-YY", FieldType.STRING, 
            2);
        da_Sra_Summary_Trans_Sex = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Sex", "SEX", FieldType.STRING, 1);
        da_Sra_Summary_Trans_Currency = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Currency", "CURRENCY", FieldType.NUMERIC, 1);
        da_Sra_Summary_Trans_Ownership = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Ownership", "OWNERSHIP", FieldType.STRING, 1);
        da_Sra_Summary_Trans_Vesting_Date = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Vesting_Date", "VESTING-DATE", FieldType.NUMERIC, 
            4);
        da_Sra_Summary_Trans_Status = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Status", "STATUS", FieldType.STRING, 1);
        da_Sra_Summary_Trans_State_Code = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_State_Code", "STATE-CODE", FieldType.STRING, 2);
        da_Sra_Summary_Trans_Tiaa_Age_1st_Paymnt = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Tiaa_Age_1st_Paymnt", "TIAA-AGE-1ST-PAYMNT", 
            FieldType.NUMERIC, 4);
        da_Sra_Summary_Trans_Tiaa_Age_1st_PaymntRedef7 = da_Sra_Summary_Trans.newGroupInGroup("da_Sra_Summary_Trans_Tiaa_Age_1st_PaymntRedef7", "Redefines", 
            da_Sra_Summary_Trans_Tiaa_Age_1st_Paymnt);
        da_Sra_Summary_Trans_Pnd_Wk_T_A_Yy = da_Sra_Summary_Trans_Tiaa_Age_1st_PaymntRedef7.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_T_A_Yy", "#WK-T-A-YY", 
            FieldType.STRING, 2);
        da_Sra_Summary_Trans_Pnd_Wk_T_A_Mm = da_Sra_Summary_Trans_Tiaa_Age_1st_PaymntRedef7.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_T_A_Mm", "#WK-T-A-MM", 
            FieldType.STRING, 2);
        da_Sra_Summary_Trans_Cref_Age_1st_Paymnt = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Cref_Age_1st_Paymnt", "CREF-AGE-1ST-PAYMNT", 
            FieldType.NUMERIC, 4);
        da_Sra_Summary_Trans_Cref_Age_1st_PaymntRedef8 = da_Sra_Summary_Trans.newGroupInGroup("da_Sra_Summary_Trans_Cref_Age_1st_PaymntRedef8", "Redefines", 
            da_Sra_Summary_Trans_Cref_Age_1st_Paymnt);
        da_Sra_Summary_Trans_Pnd_Wk_C_A_Yy = da_Sra_Summary_Trans_Cref_Age_1st_PaymntRedef8.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_C_A_Yy", "#WK-C-A-YY", 
            FieldType.STRING, 2);
        da_Sra_Summary_Trans_Pnd_Wk_C_A_Mm = da_Sra_Summary_Trans_Cref_Age_1st_PaymntRedef8.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_C_A_Mm", "#WK-C-A-MM", 
            FieldType.STRING, 2);
        da_Sra_Summary_Trans_Tiaa_Doi = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Tiaa_Doi", "TIAA-DOI", FieldType.STRING, 4);
        da_Sra_Summary_Trans_Tiaa_DoiRedef9 = da_Sra_Summary_Trans.newGroupInGroup("da_Sra_Summary_Trans_Tiaa_DoiRedef9", "Redefines", da_Sra_Summary_Trans_Tiaa_Doi);
        da_Sra_Summary_Trans_Pnd_Wk_T_Doi_Mm = da_Sra_Summary_Trans_Tiaa_DoiRedef9.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_T_Doi_Mm", "#WK-T-DOI-MM", 
            FieldType.STRING, 2);
        da_Sra_Summary_Trans_Pnd_Wk_T_Doi_Yy = da_Sra_Summary_Trans_Tiaa_DoiRedef9.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_T_Doi_Yy", "#WK-T-DOI-YY", 
            FieldType.STRING, 2);
        da_Sra_Summary_Trans_Cref_Doi = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Cref_Doi", "CREF-DOI", FieldType.STRING, 4);
        da_Sra_Summary_Trans_Cref_DoiRedef10 = da_Sra_Summary_Trans.newGroupInGroup("da_Sra_Summary_Trans_Cref_DoiRedef10", "Redefines", da_Sra_Summary_Trans_Cref_Doi);
        da_Sra_Summary_Trans_Pnd_Wk_C_Doi_Mm = da_Sra_Summary_Trans_Cref_DoiRedef10.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_C_Doi_Mm", "#WK-C-DOI-MM", 
            FieldType.STRING, 2);
        da_Sra_Summary_Trans_Pnd_Wk_C_Doi_Yy = da_Sra_Summary_Trans_Cref_DoiRedef10.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_C_Doi_Yy", "#WK-C-DOI-YY", 
            FieldType.STRING, 2);
        da_Sra_Summary_Trans_Name = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Name", "NAME", FieldType.STRING, 38);
        da_Sra_Summary_Trans_Date_App_Recvd = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Date_App_Recvd", "DATE-APP-RECVD", FieldType.NUMERIC, 
            6);
        da_Sra_Summary_Trans_Date_App_RecvdRedef11 = da_Sra_Summary_Trans.newGroupInGroup("da_Sra_Summary_Trans_Date_App_RecvdRedef11", "Redefines", da_Sra_Summary_Trans_Date_App_Recvd);
        da_Sra_Summary_Trans_Pnd_Wk_App_Mm = da_Sra_Summary_Trans_Date_App_RecvdRedef11.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_App_Mm", "#WK-APP-MM", 
            FieldType.STRING, 2);
        da_Sra_Summary_Trans_Pnd_Wk_App_Dd = da_Sra_Summary_Trans_Date_App_RecvdRedef11.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_App_Dd", "#WK-APP-DD", 
            FieldType.STRING, 2);
        da_Sra_Summary_Trans_Pnd_Wk_App_Yy = da_Sra_Summary_Trans_Date_App_RecvdRedef11.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Wk_App_Yy", "#WK-APP-YY", 
            FieldType.STRING, 2);
        da_Sra_Summary_Trans_Alloc_Fmt = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Alloc_Fmt", "ALLOC-FMT", FieldType.STRING, 1);
        da_Sra_Summary_Trans_Alloc_Fund_Cde = da_Sra_Summary_Trans.newFieldArrayInGroup("da_Sra_Summary_Trans_Alloc_Fund_Cde", "ALLOC-FUND-CDE", FieldType.STRING, 
            10, new DbsArrayController(1,100));
        da_Sra_Summary_Trans_Alloc = da_Sra_Summary_Trans.newFieldArrayInGroup("da_Sra_Summary_Trans_Alloc", "ALLOC", FieldType.NUMERIC, 3, new DbsArrayController(1,
            100));
        da_Sra_Summary_Trans_Appl_Source = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Appl_Source", "APPL-SOURCE", FieldType.STRING, 1);
        da_Sra_Summary_Trans_Split_Code = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Split_Code", "SPLIT-CODE", FieldType.STRING, 2);
        da_Sra_Summary_Trans_Lob = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Lob", "LOB", FieldType.STRING, 1);
        da_Sra_Summary_Trans_Lob_Type = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Lob_Type", "LOB-TYPE", FieldType.STRING, 1);
        da_Sra_Summary_Trans_Cor_Hist_Ind = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Cor_Hist_Ind", "COR-HIST-IND", FieldType.STRING, 
            1);
        da_Sra_Summary_Trans_Pnd_Iis_Prem_Team_Code = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Pnd_Iis_Prem_Team_Code", "#IIS-PREM-TEAM-CODE", 
            FieldType.STRING, 8);
        da_Sra_Summary_Trans_Sg_Plan_No = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Sg_Plan_No", "SG-PLAN-NO", FieldType.STRING, 6);
        da_Sra_Summary_Trans_Sg_Subplan_No = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Sg_Subplan_No", "SG-SUBPLAN-NO", FieldType.STRING, 
            6);
        da_Sra_Summary_Trans_Sg_Part_Ext = da_Sra_Summary_Trans.newFieldInGroup("da_Sra_Summary_Trans_Sg_Part_Ext", "SG-PART-EXT", FieldType.STRING, 2);
        filler01 = da_Sra_Summary_Trans.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 44);
        da_Sra_Summary_TransRedef12 = newGroupInRecord("da_Sra_Summary_TransRedef12", "Redefines", da_Sra_Summary_Trans);
        da_Sra_Summary_Trans_Da_Sra_1 = da_Sra_Summary_TransRedef12.newFieldInGroup("da_Sra_Summary_Trans_Da_Sra_1", "DA-SRA-1", FieldType.STRING, 250);
        da_Sra_Summary_Trans_Da_Sra_2 = da_Sra_Summary_TransRedef12.newFieldInGroup("da_Sra_Summary_Trans_Da_Sra_2", "DA-SRA-2", FieldType.STRING, 250);
        da_Sra_Summary_Trans_Da_Sra_3 = da_Sra_Summary_TransRedef12.newFieldInGroup("da_Sra_Summary_Trans_Da_Sra_3", "DA-SRA-3", FieldType.STRING, 250);
        da_Sra_Summary_Trans_Da_Sra_4 = da_Sra_Summary_TransRedef12.newFieldInGroup("da_Sra_Summary_Trans_Da_Sra_4", "DA-SRA-4", FieldType.STRING, 250);
        da_Sra_Summary_Trans_Da_Sra_5 = da_Sra_Summary_TransRedef12.newFieldInGroup("da_Sra_Summary_Trans_Da_Sra_5", "DA-SRA-5", FieldType.STRING, 250);
        da_Sra_Summary_Trans_Da_Sra_6 = da_Sra_Summary_TransRedef12.newFieldInGroup("da_Sra_Summary_Trans_Da_Sra_6", "DA-SRA-6", FieldType.STRING, 250);

        this.setRecordName("LdaAppl230");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAppl230() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
