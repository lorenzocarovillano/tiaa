/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:11:56 PM
**        * FROM NATURAL LDA     : SCIL9000
************************************************************
**        * FILE NAME            : LdaScil9000.java
**        * CLASS NAME           : LdaScil9000
**        * INSTANCE NAME        : LdaScil9000
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaScil9000 extends DbsRecord
{
    // Properties
    private DbsGroup scil9000;
    private DbsField scil9000_Pnd_Plan_Id;
    private DbsField scil9000_Pnd_Plan_No;
    private DbsField scil9000_Pnd_Sub_Plan_No;
    private DbsField scil9000_Pnd_Sg_Part_Ext;
    private DbsField scil9000_Pnd_User_Id;
    private DbsField scil9000_Pnd_Pi_Dept;
    private DbsField scil9000_Pnd_Pi_Task_Id;
    private DbsField scil9000_Pnd_Pull_Code;
    private DbsField scil9000_Pnd_Companion_Ind;
    private DbsField scil9000_Pnd_Divorce_Ind;
    private DbsField scil9000_Pnd_Enrollment_Method;
    private DbsField scil9000_Pnd_Additional_Cref_Rqst;
    private DbsField scil9000_Pnd_Entry_Date;
    private DbsField scil9000_Pnd_Date_App_Recvd;
    private DbsField scil9000_Pnd_Annuity_Start_Date;
    private DbsField scil9000_Pnd_Tiaa_Date_Of_Issue;
    private DbsField scil9000_Pnd_Cref_Date_Of_Issue;
    private DbsField scil9000_Pnd_Tiaa_Contract_No;
    private DbsField scil9000_Pnd_Cref_Certificate_No;
    private DbsField scil9000_Pnd_Rl_Contract_No;
    private DbsField scil9000_Pnd_Loan_Ind;
    private DbsField scil9000_Pnd_Vesting_Code;
    private DbsField scil9000_Pnd_Vesting_Code_Override;
    private DbsField scil9000_Pnd_Pin_Nbr;
    private DbsField scil9000_Pnd_Ssn;
    private DbsField scil9000_Pnd_Prefix;
    private DbsField scil9000_Pnd_Last_Name;
    private DbsField scil9000_Pnd_First_Name;
    private DbsField scil9000_Pnd_Middle_Name;
    private DbsField scil9000_Pnd_Suffix;
    private DbsField scil9000_Pnd_Date_Of_Birth;
    private DbsField scil9000_Pnd_Sex;
    private DbsField scil9000_Pnd_Daytime_Phone;
    private DbsField scil9000_Pnd_Evening_Phone;
    private DbsField scil9000_Pnd_Email_Address;
    private DbsField scil9000_Pnd_Mailing_Address_Line;
    private DbsField scil9000_Pnd_Mailing_City;
    private DbsField scil9000_Pnd_Mailing_State;
    private DbsField scil9000_Pnd_Mailing_Zip_Code;
    private DbsField scil9000_Pnd_Residence_Address_Line;
    private DbsField scil9000_Pnd_Residence_City;
    private DbsField scil9000_Pnd_Residence_State;
    private DbsField scil9000_Pnd_Residence_Zip_Code;
    private DbsField scil9000_Pnd_Allocation;
    private DbsField scil9000_Pnd_Allocation_Fund_Ticker;
    private DbsField scil9000_Pnd_Alloc_Default_Ind;
    private DbsField scil9000_Pnd_Alloc_Model_Ind;
    private DbsField scil9000_Pnd_Bene_Type;
    private DbsField scil9000_Pnd_Bene_Ssn;
    private DbsField scil9000_Pnd_Bene_Name;
    private DbsField scil9000_Pnd_Bene_Relationship;
    private DbsField scil9000_Pnd_Bene_Relationship_Code;
    private DbsField scil9000_Pnd_Bene_Date_Of_Birth;
    private DbsField scil9000_Pnd_Bene_Alloc_Pct;
    private DbsField scil9000_Pnd_Bene_Numerator;
    private DbsField scil9000_Pnd_Bene_Denominator;
    private DbsField scil9000_Pnd_Bene_Special_Text_Ind;
    private DbsField scil9000_Pnd_Bene_Special_Text;
    private DbsField scil9000_Pnd_Bene_Mos_Ind;
    private DbsField scil9000_Pnd_Bene_Estate;
    private DbsField scil9000_Pnd_Bene_Trust;
    private DbsField scil9000_Pnd_Bene_Category;
    private DbsField scil9000_Pnd_Ica_Rcvd_Ind;
    private DbsField scil9000_Pnd_Plan_Type;
    private DbsField scil9000_Pnd_Employer_Type;
    private DbsField scil9000_Pnd_Erisa_Ind;
    private DbsField scil9000_Pnd_Oia_Ind;
    private DbsField scil9000_Pnd_Issue_State;
    private DbsField scil9000_Pnd_Mailing_Instructions;
    private DbsField scil9000_Pnd_Contract_Pick_Up_Ind;
    private DbsField scil9000_Pnd_Omni_Account_Issuance;
    private DbsField scil9000_Pnd_Incompl_Data_Ind;
    private DbsField scil9000_Pnd_Orig_Tiaa_Cntrct;
    private DbsField scil9000_Pnd_Orig_Cref_Cntrct;
    private DbsField scil9000_Pnd_Money_Invested_Ind;
    private DbsField scil9000_Pnd_Country_Code;
    private DbsField scil9000_Pnd_Div_Sub;
    private DbsField scil9000_Pnd_Sg_Replacement_Ind;
    private DbsField scil9000_Pnd_Sg_Register_Id;
    private DbsField scil9000_Pnd_Sg_Agent_Racf_Id;
    private DbsField scil9000_Pnd_Sg_Exempt_Ind;
    private DbsField scil9000_Pnd_Sg_Insurer_1;
    private DbsField scil9000_Pnd_Sg_Insurer_2;
    private DbsField scil9000_Pnd_Sg_Insurer_3;
    private DbsField scil9000_Pnd_Sg_1035_Exch_Ind_1;
    private DbsField scil9000_Pnd_Sg_1035_Exch_Ind_2;
    private DbsField scil9000_Pnd_Sg_1035_Exch_Ind_3;
    private DbsField scil9000_Pnd_Pl_Client_Id;
    private DbsField scil9000_Pnd_Model_Id;
    private DbsField scil9000_Pnd_As_Ind;
    private DbsField scil9000_Pnd_Curr_Def_Option;
    private DbsField scil9000_Pnd_Curr_Def_Amt;
    private DbsGroup scil9000_Pnd_Curr_Def_AmtRedef1;
    private DbsField scil9000_Pnd_Curr_Def_Amt_A;
    private DbsField scil9000_Pnd_Incr_Option;
    private DbsField scil9000_Pnd_Incr_Amt;
    private DbsGroup scil9000_Pnd_Incr_AmtRedef2;
    private DbsField scil9000_Pnd_Incr_Amt_A;
    private DbsField scil9000_Pnd_Max_Pct;
    private DbsGroup scil9000_Pnd_Max_PctRedef3;
    private DbsField scil9000_Pnd_Max_Pct_A;
    private DbsField scil9000_Pnd_Optout_Days;
    private DbsGroup scil9000_Pnd_Optout_DaysRedef4;
    private DbsField scil9000_Pnd_Optout_Days_A;
    private DbsField scil9000_Pnd_Tsv_Ind;
    private DbsField scil9000_Pnd_Indx_Guar_Rate_Ind;
    private DbsField scil9000_Pnd_Eir_Status;
    private DbsField scil9000_Pnd_Eir_From_T114_Ind;
    private DbsField scil9000_Pnd_Orchestration_Id;
    private DbsField scil9000_Pnd_Use_Bpel_Data;
    private DbsField scil9000_Pnd_Legal_Ann_Option;
    private DbsField scil9000_Pnd_Substitution_Contract_Ind;
    private DbsField scil9000_Pnd_Conversion_Issue_State;
    private DbsField scil9000_Pnd_Deceased_Ind;
    private DbsField scil9000_Pnd_Non_Proprietary_Pkg_Ind;
    private DbsField scil9000_Pnd_Tsr_Ind;
    private DbsField scil9000_Pnd_Decedent_Contract;
    private DbsField scil9000_Pnd_Relation_To_Decedent;
    private DbsField scil9000_Pnd_Ssn_Tin_Ind;
    private DbsField scil9000_Pnd_Oneira_Acct_No;
    private DbsField scil9000_Pnd_Omni_Folder_Name;
    private DbsField scil9000_Pnd_Allocation_Source_1;
    private DbsField scil9000_Pnd_Allocation_Source_2;
    private DbsGroup scil9000_Pnd_Alloc_Table_2;
    private DbsField scil9000_Pnd_Allocation_Pct_2;
    private DbsField scil9000_Pnd_Allocation_Ticker_2;
    private DbsField scil9000_Pnd_Participant_File;
    private DbsGroup scil9000_Pnd_Participant_FileRedef5;
    private DbsField scil9000_Pnd_Ph_Pin_Ssn;
    private DbsGroup scil9000_Pnd_Ph_Pin_SsnRedef6;
    private DbsField scil9000_Pnd_Ph_Unique_Id_Nbr;
    private DbsField scil9000_Pnd_Ph_Social_Security_No;
    private DbsGroup scil9000_Pnd_Ph_Social_Security_NoRedef7;
    private DbsField scil9000_Pnd_Ph_Ssn_A;
    private DbsField scil9000_Pnd_Ph_Frgn_Ssn_Nbr;
    private DbsGroup scil9000_Pnd_Ph_Frgn_Ssn_NbrRedef8;
    private DbsField scil9000_Pnd_Ph_Frgn_Ssn_Nbr_A;
    private DbsField scil9000_Pnd_Ph_Negative_Election_Cd;
    private DbsGroup scil9000_Pnd_Ph_Negative_Election_CdRedef9;
    private DbsField scil9000_Pnd_Ph_Negative_Elect_Cd_A;
    private DbsField scil9000_Pnd_Ph_Rltnshp_Cde;
    private DbsField scil9000_Pnd_Ph_Dob_Dte;
    private DbsField scil9000_Pnd_Ph_Dod_Dte;
    private DbsField scil9000_Pnd_Ph_Sex_Cde;
    private DbsField scil9000_Pnd_Ph_Xref_Pin;
    private DbsGroup scil9000_Pnd_Ph_Xref_PinRedef10;
    private DbsField scil9000_Pnd_Ph_Xref_Pin_A;
    private DbsField scil9000_Pnd_Ph_Prfx_Nme;
    private DbsField scil9000_Pnd_Ph_Last_Nme;
    private DbsField scil9000_Pnd_Ph_First_Nme;
    private DbsField scil9000_Pnd_Ph_Mddle_Nme;
    private DbsField scil9000_Pnd_Ph_Sffx_Nme;
    private DbsField scil9000_Pnd_Ph_Actve_Vip_Cde;
    private DbsField scil9000_Pnd_Ph_Mail_Cde;
    private DbsField scil9000_Pnd_Ph_Mail_Origin_Cde;
    private DbsField scil9000_Pnd_Cor_Ph_Last_Updte_Dte;
    private DbsField scil9000_Pnd_Cor_Ph_Last_Updte_Tme;
    private DbsGroup scil9000_Pnd_Cor_Ph_Last_Updte_TmeRedef11;
    private DbsField scil9000_Pnd_Cor_Ph_Last_Upd_Tme_A;
    private DbsGroup scil9000_Pnd_Cor_Ph_Last_Updte_TmeRedef12;
    private DbsField scil9000_Pnd_Cor_Ph_Last_Upd_Tme_1_6;
    private DbsField scil9000_Pnd_Cor_Ph_Last_Upd_Tme_7;
    private DbsField scil9000_Pnd_Cor_Ph_Last_Updte_Userid;
    private DbsField scil9000_Pnd_Change_Pin;
    private DbsField scil9000_Pnd_Change_Ssn;
    private DbsGroup scil9000_Pnd_Change_SsnRedef13;
    private DbsField scil9000_Pnd_Change_Ssn_A;
    private DbsField scil9000_Pnd_Filler_1;
    private DbsGroup scil9000Redef14;
    private DbsField scil9000_Pnd_Omni_Data_1;
    private DbsField scil9000_Pnd_Omni_Data_2;
    private DbsField scil9000_Pnd_Omni_Data_3;
    private DbsField scil9000_Pnd_Omni_Data_4;
    private DbsField scil9000_Pnd_Omni_Data_5;
    private DbsField scil9000_Pnd_Omni_Data_6;
    private DbsField scil9000_Pnd_Omni_Data_7;
    private DbsField scil9000_Pnd_Omni_Data_8;
    private DbsField scil9000_Pnd_Omni_Data_9;
    private DbsField scil9000_Pnd_Omni_Data_10;
    private DbsField scil9000_Pnd_Omni_Data_11;
    private DbsField scil9000_Pnd_Omni_Data_12;
    private DbsField scil9000_Pnd_Omni_Data_13;
    private DbsField scil9000_Pnd_Omni_Data_14;
    private DbsField scil9000_Pnd_Omni_Data_15;
    private DbsField scil9000_Pnd_Omni_Data_16;
    private DbsField scil9000_Pnd_Omni_Data_17;
    private DbsField scil9000_Pnd_Omni_Data_18;
    private DbsField scil9000_Pnd_Omni_Data_19;
    private DbsField scil9000_Pnd_Omni_Data_20;
    private DbsField scil9000_Pnd_Omni_Data_21;
    private DbsField scil9000_Pnd_Omni_Data_22;
    private DbsField scil9000_Pnd_Omni_Data_23;
    private DbsField scil9000_Pnd_Omni_Data_24;
    private DbsField scil9000_Pnd_Omni_Data_25;
    private DbsField scil9000_Pnd_Omni_Data_26;
    private DbsField scil9000_Pnd_Omni_Data_27;
    private DbsField scil9000_Pnd_Omni_Data_28;
    private DbsField scil9000_Pnd_Omni_Data_29;
    private DbsField scil9000_Pnd_Omni_Data_30;
    private DbsField scil9000_Pnd_Omni_Data_31;
    private DbsField scil9000_Pnd_Omni_Data_32;
    private DbsField scil9000_Pnd_Omni_Data_33;
    private DbsField scil9000_Pnd_Omni_Data_34;
    private DbsField scil9000_Pnd_Omni_Data_35;
    private DbsField scil9000_Pnd_Omni_Data_36;
    private DbsField scil9000_Pnd_Omni_Data_37;
    private DbsField scil9000_Pnd_Omni_Data_38;
    private DbsField scil9000_Pnd_Omni_Data_39;
    private DbsField scil9000_Pnd_Omni_Data_40;

    public DbsGroup getScil9000() { return scil9000; }

    public DbsField getScil9000_Pnd_Plan_Id() { return scil9000_Pnd_Plan_Id; }

    public DbsField getScil9000_Pnd_Plan_No() { return scil9000_Pnd_Plan_No; }

    public DbsField getScil9000_Pnd_Sub_Plan_No() { return scil9000_Pnd_Sub_Plan_No; }

    public DbsField getScil9000_Pnd_Sg_Part_Ext() { return scil9000_Pnd_Sg_Part_Ext; }

    public DbsField getScil9000_Pnd_User_Id() { return scil9000_Pnd_User_Id; }

    public DbsField getScil9000_Pnd_Pi_Dept() { return scil9000_Pnd_Pi_Dept; }

    public DbsField getScil9000_Pnd_Pi_Task_Id() { return scil9000_Pnd_Pi_Task_Id; }

    public DbsField getScil9000_Pnd_Pull_Code() { return scil9000_Pnd_Pull_Code; }

    public DbsField getScil9000_Pnd_Companion_Ind() { return scil9000_Pnd_Companion_Ind; }

    public DbsField getScil9000_Pnd_Divorce_Ind() { return scil9000_Pnd_Divorce_Ind; }

    public DbsField getScil9000_Pnd_Enrollment_Method() { return scil9000_Pnd_Enrollment_Method; }

    public DbsField getScil9000_Pnd_Additional_Cref_Rqst() { return scil9000_Pnd_Additional_Cref_Rqst; }

    public DbsField getScil9000_Pnd_Entry_Date() { return scil9000_Pnd_Entry_Date; }

    public DbsField getScil9000_Pnd_Date_App_Recvd() { return scil9000_Pnd_Date_App_Recvd; }

    public DbsField getScil9000_Pnd_Annuity_Start_Date() { return scil9000_Pnd_Annuity_Start_Date; }

    public DbsField getScil9000_Pnd_Tiaa_Date_Of_Issue() { return scil9000_Pnd_Tiaa_Date_Of_Issue; }

    public DbsField getScil9000_Pnd_Cref_Date_Of_Issue() { return scil9000_Pnd_Cref_Date_Of_Issue; }

    public DbsField getScil9000_Pnd_Tiaa_Contract_No() { return scil9000_Pnd_Tiaa_Contract_No; }

    public DbsField getScil9000_Pnd_Cref_Certificate_No() { return scil9000_Pnd_Cref_Certificate_No; }

    public DbsField getScil9000_Pnd_Rl_Contract_No() { return scil9000_Pnd_Rl_Contract_No; }

    public DbsField getScil9000_Pnd_Loan_Ind() { return scil9000_Pnd_Loan_Ind; }

    public DbsField getScil9000_Pnd_Vesting_Code() { return scil9000_Pnd_Vesting_Code; }

    public DbsField getScil9000_Pnd_Vesting_Code_Override() { return scil9000_Pnd_Vesting_Code_Override; }

    public DbsField getScil9000_Pnd_Pin_Nbr() { return scil9000_Pnd_Pin_Nbr; }

    public DbsField getScil9000_Pnd_Ssn() { return scil9000_Pnd_Ssn; }

    public DbsField getScil9000_Pnd_Prefix() { return scil9000_Pnd_Prefix; }

    public DbsField getScil9000_Pnd_Last_Name() { return scil9000_Pnd_Last_Name; }

    public DbsField getScil9000_Pnd_First_Name() { return scil9000_Pnd_First_Name; }

    public DbsField getScil9000_Pnd_Middle_Name() { return scil9000_Pnd_Middle_Name; }

    public DbsField getScil9000_Pnd_Suffix() { return scil9000_Pnd_Suffix; }

    public DbsField getScil9000_Pnd_Date_Of_Birth() { return scil9000_Pnd_Date_Of_Birth; }

    public DbsField getScil9000_Pnd_Sex() { return scil9000_Pnd_Sex; }

    public DbsField getScil9000_Pnd_Daytime_Phone() { return scil9000_Pnd_Daytime_Phone; }

    public DbsField getScil9000_Pnd_Evening_Phone() { return scil9000_Pnd_Evening_Phone; }

    public DbsField getScil9000_Pnd_Email_Address() { return scil9000_Pnd_Email_Address; }

    public DbsField getScil9000_Pnd_Mailing_Address_Line() { return scil9000_Pnd_Mailing_Address_Line; }

    public DbsField getScil9000_Pnd_Mailing_City() { return scil9000_Pnd_Mailing_City; }

    public DbsField getScil9000_Pnd_Mailing_State() { return scil9000_Pnd_Mailing_State; }

    public DbsField getScil9000_Pnd_Mailing_Zip_Code() { return scil9000_Pnd_Mailing_Zip_Code; }

    public DbsField getScil9000_Pnd_Residence_Address_Line() { return scil9000_Pnd_Residence_Address_Line; }

    public DbsField getScil9000_Pnd_Residence_City() { return scil9000_Pnd_Residence_City; }

    public DbsField getScil9000_Pnd_Residence_State() { return scil9000_Pnd_Residence_State; }

    public DbsField getScil9000_Pnd_Residence_Zip_Code() { return scil9000_Pnd_Residence_Zip_Code; }

    public DbsField getScil9000_Pnd_Allocation() { return scil9000_Pnd_Allocation; }

    public DbsField getScil9000_Pnd_Allocation_Fund_Ticker() { return scil9000_Pnd_Allocation_Fund_Ticker; }

    public DbsField getScil9000_Pnd_Alloc_Default_Ind() { return scil9000_Pnd_Alloc_Default_Ind; }

    public DbsField getScil9000_Pnd_Alloc_Model_Ind() { return scil9000_Pnd_Alloc_Model_Ind; }

    public DbsField getScil9000_Pnd_Bene_Type() { return scil9000_Pnd_Bene_Type; }

    public DbsField getScil9000_Pnd_Bene_Ssn() { return scil9000_Pnd_Bene_Ssn; }

    public DbsField getScil9000_Pnd_Bene_Name() { return scil9000_Pnd_Bene_Name; }

    public DbsField getScil9000_Pnd_Bene_Relationship() { return scil9000_Pnd_Bene_Relationship; }

    public DbsField getScil9000_Pnd_Bene_Relationship_Code() { return scil9000_Pnd_Bene_Relationship_Code; }

    public DbsField getScil9000_Pnd_Bene_Date_Of_Birth() { return scil9000_Pnd_Bene_Date_Of_Birth; }

    public DbsField getScil9000_Pnd_Bene_Alloc_Pct() { return scil9000_Pnd_Bene_Alloc_Pct; }

    public DbsField getScil9000_Pnd_Bene_Numerator() { return scil9000_Pnd_Bene_Numerator; }

    public DbsField getScil9000_Pnd_Bene_Denominator() { return scil9000_Pnd_Bene_Denominator; }

    public DbsField getScil9000_Pnd_Bene_Special_Text_Ind() { return scil9000_Pnd_Bene_Special_Text_Ind; }

    public DbsField getScil9000_Pnd_Bene_Special_Text() { return scil9000_Pnd_Bene_Special_Text; }

    public DbsField getScil9000_Pnd_Bene_Mos_Ind() { return scil9000_Pnd_Bene_Mos_Ind; }

    public DbsField getScil9000_Pnd_Bene_Estate() { return scil9000_Pnd_Bene_Estate; }

    public DbsField getScil9000_Pnd_Bene_Trust() { return scil9000_Pnd_Bene_Trust; }

    public DbsField getScil9000_Pnd_Bene_Category() { return scil9000_Pnd_Bene_Category; }

    public DbsField getScil9000_Pnd_Ica_Rcvd_Ind() { return scil9000_Pnd_Ica_Rcvd_Ind; }

    public DbsField getScil9000_Pnd_Plan_Type() { return scil9000_Pnd_Plan_Type; }

    public DbsField getScil9000_Pnd_Employer_Type() { return scil9000_Pnd_Employer_Type; }

    public DbsField getScil9000_Pnd_Erisa_Ind() { return scil9000_Pnd_Erisa_Ind; }

    public DbsField getScil9000_Pnd_Oia_Ind() { return scil9000_Pnd_Oia_Ind; }

    public DbsField getScil9000_Pnd_Issue_State() { return scil9000_Pnd_Issue_State; }

    public DbsField getScil9000_Pnd_Mailing_Instructions() { return scil9000_Pnd_Mailing_Instructions; }

    public DbsField getScil9000_Pnd_Contract_Pick_Up_Ind() { return scil9000_Pnd_Contract_Pick_Up_Ind; }

    public DbsField getScil9000_Pnd_Omni_Account_Issuance() { return scil9000_Pnd_Omni_Account_Issuance; }

    public DbsField getScil9000_Pnd_Incompl_Data_Ind() { return scil9000_Pnd_Incompl_Data_Ind; }

    public DbsField getScil9000_Pnd_Orig_Tiaa_Cntrct() { return scil9000_Pnd_Orig_Tiaa_Cntrct; }

    public DbsField getScil9000_Pnd_Orig_Cref_Cntrct() { return scil9000_Pnd_Orig_Cref_Cntrct; }

    public DbsField getScil9000_Pnd_Money_Invested_Ind() { return scil9000_Pnd_Money_Invested_Ind; }

    public DbsField getScil9000_Pnd_Country_Code() { return scil9000_Pnd_Country_Code; }

    public DbsField getScil9000_Pnd_Div_Sub() { return scil9000_Pnd_Div_Sub; }

    public DbsField getScil9000_Pnd_Sg_Replacement_Ind() { return scil9000_Pnd_Sg_Replacement_Ind; }

    public DbsField getScil9000_Pnd_Sg_Register_Id() { return scil9000_Pnd_Sg_Register_Id; }

    public DbsField getScil9000_Pnd_Sg_Agent_Racf_Id() { return scil9000_Pnd_Sg_Agent_Racf_Id; }

    public DbsField getScil9000_Pnd_Sg_Exempt_Ind() { return scil9000_Pnd_Sg_Exempt_Ind; }

    public DbsField getScil9000_Pnd_Sg_Insurer_1() { return scil9000_Pnd_Sg_Insurer_1; }

    public DbsField getScil9000_Pnd_Sg_Insurer_2() { return scil9000_Pnd_Sg_Insurer_2; }

    public DbsField getScil9000_Pnd_Sg_Insurer_3() { return scil9000_Pnd_Sg_Insurer_3; }

    public DbsField getScil9000_Pnd_Sg_1035_Exch_Ind_1() { return scil9000_Pnd_Sg_1035_Exch_Ind_1; }

    public DbsField getScil9000_Pnd_Sg_1035_Exch_Ind_2() { return scil9000_Pnd_Sg_1035_Exch_Ind_2; }

    public DbsField getScil9000_Pnd_Sg_1035_Exch_Ind_3() { return scil9000_Pnd_Sg_1035_Exch_Ind_3; }

    public DbsField getScil9000_Pnd_Pl_Client_Id() { return scil9000_Pnd_Pl_Client_Id; }

    public DbsField getScil9000_Pnd_Model_Id() { return scil9000_Pnd_Model_Id; }

    public DbsField getScil9000_Pnd_As_Ind() { return scil9000_Pnd_As_Ind; }

    public DbsField getScil9000_Pnd_Curr_Def_Option() { return scil9000_Pnd_Curr_Def_Option; }

    public DbsField getScil9000_Pnd_Curr_Def_Amt() { return scil9000_Pnd_Curr_Def_Amt; }

    public DbsGroup getScil9000_Pnd_Curr_Def_AmtRedef1() { return scil9000_Pnd_Curr_Def_AmtRedef1; }

    public DbsField getScil9000_Pnd_Curr_Def_Amt_A() { return scil9000_Pnd_Curr_Def_Amt_A; }

    public DbsField getScil9000_Pnd_Incr_Option() { return scil9000_Pnd_Incr_Option; }

    public DbsField getScil9000_Pnd_Incr_Amt() { return scil9000_Pnd_Incr_Amt; }

    public DbsGroup getScil9000_Pnd_Incr_AmtRedef2() { return scil9000_Pnd_Incr_AmtRedef2; }

    public DbsField getScil9000_Pnd_Incr_Amt_A() { return scil9000_Pnd_Incr_Amt_A; }

    public DbsField getScil9000_Pnd_Max_Pct() { return scil9000_Pnd_Max_Pct; }

    public DbsGroup getScil9000_Pnd_Max_PctRedef3() { return scil9000_Pnd_Max_PctRedef3; }

    public DbsField getScil9000_Pnd_Max_Pct_A() { return scil9000_Pnd_Max_Pct_A; }

    public DbsField getScil9000_Pnd_Optout_Days() { return scil9000_Pnd_Optout_Days; }

    public DbsGroup getScil9000_Pnd_Optout_DaysRedef4() { return scil9000_Pnd_Optout_DaysRedef4; }

    public DbsField getScil9000_Pnd_Optout_Days_A() { return scil9000_Pnd_Optout_Days_A; }

    public DbsField getScil9000_Pnd_Tsv_Ind() { return scil9000_Pnd_Tsv_Ind; }

    public DbsField getScil9000_Pnd_Indx_Guar_Rate_Ind() { return scil9000_Pnd_Indx_Guar_Rate_Ind; }

    public DbsField getScil9000_Pnd_Eir_Status() { return scil9000_Pnd_Eir_Status; }

    public DbsField getScil9000_Pnd_Eir_From_T114_Ind() { return scil9000_Pnd_Eir_From_T114_Ind; }

    public DbsField getScil9000_Pnd_Orchestration_Id() { return scil9000_Pnd_Orchestration_Id; }

    public DbsField getScil9000_Pnd_Use_Bpel_Data() { return scil9000_Pnd_Use_Bpel_Data; }

    public DbsField getScil9000_Pnd_Legal_Ann_Option() { return scil9000_Pnd_Legal_Ann_Option; }

    public DbsField getScil9000_Pnd_Substitution_Contract_Ind() { return scil9000_Pnd_Substitution_Contract_Ind; }

    public DbsField getScil9000_Pnd_Conversion_Issue_State() { return scil9000_Pnd_Conversion_Issue_State; }

    public DbsField getScil9000_Pnd_Deceased_Ind() { return scil9000_Pnd_Deceased_Ind; }

    public DbsField getScil9000_Pnd_Non_Proprietary_Pkg_Ind() { return scil9000_Pnd_Non_Proprietary_Pkg_Ind; }

    public DbsField getScil9000_Pnd_Tsr_Ind() { return scil9000_Pnd_Tsr_Ind; }

    public DbsField getScil9000_Pnd_Decedent_Contract() { return scil9000_Pnd_Decedent_Contract; }

    public DbsField getScil9000_Pnd_Relation_To_Decedent() { return scil9000_Pnd_Relation_To_Decedent; }

    public DbsField getScil9000_Pnd_Ssn_Tin_Ind() { return scil9000_Pnd_Ssn_Tin_Ind; }

    public DbsField getScil9000_Pnd_Oneira_Acct_No() { return scil9000_Pnd_Oneira_Acct_No; }

    public DbsField getScil9000_Pnd_Omni_Folder_Name() { return scil9000_Pnd_Omni_Folder_Name; }

    public DbsField getScil9000_Pnd_Allocation_Source_1() { return scil9000_Pnd_Allocation_Source_1; }

    public DbsField getScil9000_Pnd_Allocation_Source_2() { return scil9000_Pnd_Allocation_Source_2; }

    public DbsGroup getScil9000_Pnd_Alloc_Table_2() { return scil9000_Pnd_Alloc_Table_2; }

    public DbsField getScil9000_Pnd_Allocation_Pct_2() { return scil9000_Pnd_Allocation_Pct_2; }

    public DbsField getScil9000_Pnd_Allocation_Ticker_2() { return scil9000_Pnd_Allocation_Ticker_2; }

    public DbsField getScil9000_Pnd_Participant_File() { return scil9000_Pnd_Participant_File; }

    public DbsGroup getScil9000_Pnd_Participant_FileRedef5() { return scil9000_Pnd_Participant_FileRedef5; }

    public DbsField getScil9000_Pnd_Ph_Pin_Ssn() { return scil9000_Pnd_Ph_Pin_Ssn; }

    public DbsGroup getScil9000_Pnd_Ph_Pin_SsnRedef6() { return scil9000_Pnd_Ph_Pin_SsnRedef6; }

    public DbsField getScil9000_Pnd_Ph_Unique_Id_Nbr() { return scil9000_Pnd_Ph_Unique_Id_Nbr; }

    public DbsField getScil9000_Pnd_Ph_Social_Security_No() { return scil9000_Pnd_Ph_Social_Security_No; }

    public DbsGroup getScil9000_Pnd_Ph_Social_Security_NoRedef7() { return scil9000_Pnd_Ph_Social_Security_NoRedef7; }

    public DbsField getScil9000_Pnd_Ph_Ssn_A() { return scil9000_Pnd_Ph_Ssn_A; }

    public DbsField getScil9000_Pnd_Ph_Frgn_Ssn_Nbr() { return scil9000_Pnd_Ph_Frgn_Ssn_Nbr; }

    public DbsGroup getScil9000_Pnd_Ph_Frgn_Ssn_NbrRedef8() { return scil9000_Pnd_Ph_Frgn_Ssn_NbrRedef8; }

    public DbsField getScil9000_Pnd_Ph_Frgn_Ssn_Nbr_A() { return scil9000_Pnd_Ph_Frgn_Ssn_Nbr_A; }

    public DbsField getScil9000_Pnd_Ph_Negative_Election_Cd() { return scil9000_Pnd_Ph_Negative_Election_Cd; }

    public DbsGroup getScil9000_Pnd_Ph_Negative_Election_CdRedef9() { return scil9000_Pnd_Ph_Negative_Election_CdRedef9; }

    public DbsField getScil9000_Pnd_Ph_Negative_Elect_Cd_A() { return scil9000_Pnd_Ph_Negative_Elect_Cd_A; }

    public DbsField getScil9000_Pnd_Ph_Rltnshp_Cde() { return scil9000_Pnd_Ph_Rltnshp_Cde; }

    public DbsField getScil9000_Pnd_Ph_Dob_Dte() { return scil9000_Pnd_Ph_Dob_Dte; }

    public DbsField getScil9000_Pnd_Ph_Dod_Dte() { return scil9000_Pnd_Ph_Dod_Dte; }

    public DbsField getScil9000_Pnd_Ph_Sex_Cde() { return scil9000_Pnd_Ph_Sex_Cde; }

    public DbsField getScil9000_Pnd_Ph_Xref_Pin() { return scil9000_Pnd_Ph_Xref_Pin; }

    public DbsGroup getScil9000_Pnd_Ph_Xref_PinRedef10() { return scil9000_Pnd_Ph_Xref_PinRedef10; }

    public DbsField getScil9000_Pnd_Ph_Xref_Pin_A() { return scil9000_Pnd_Ph_Xref_Pin_A; }

    public DbsField getScil9000_Pnd_Ph_Prfx_Nme() { return scil9000_Pnd_Ph_Prfx_Nme; }

    public DbsField getScil9000_Pnd_Ph_Last_Nme() { return scil9000_Pnd_Ph_Last_Nme; }

    public DbsField getScil9000_Pnd_Ph_First_Nme() { return scil9000_Pnd_Ph_First_Nme; }

    public DbsField getScil9000_Pnd_Ph_Mddle_Nme() { return scil9000_Pnd_Ph_Mddle_Nme; }

    public DbsField getScil9000_Pnd_Ph_Sffx_Nme() { return scil9000_Pnd_Ph_Sffx_Nme; }

    public DbsField getScil9000_Pnd_Ph_Actve_Vip_Cde() { return scil9000_Pnd_Ph_Actve_Vip_Cde; }

    public DbsField getScil9000_Pnd_Ph_Mail_Cde() { return scil9000_Pnd_Ph_Mail_Cde; }

    public DbsField getScil9000_Pnd_Ph_Mail_Origin_Cde() { return scil9000_Pnd_Ph_Mail_Origin_Cde; }

    public DbsField getScil9000_Pnd_Cor_Ph_Last_Updte_Dte() { return scil9000_Pnd_Cor_Ph_Last_Updte_Dte; }

    public DbsField getScil9000_Pnd_Cor_Ph_Last_Updte_Tme() { return scil9000_Pnd_Cor_Ph_Last_Updte_Tme; }

    public DbsGroup getScil9000_Pnd_Cor_Ph_Last_Updte_TmeRedef11() { return scil9000_Pnd_Cor_Ph_Last_Updte_TmeRedef11; }

    public DbsField getScil9000_Pnd_Cor_Ph_Last_Upd_Tme_A() { return scil9000_Pnd_Cor_Ph_Last_Upd_Tme_A; }

    public DbsGroup getScil9000_Pnd_Cor_Ph_Last_Updte_TmeRedef12() { return scil9000_Pnd_Cor_Ph_Last_Updte_TmeRedef12; }

    public DbsField getScil9000_Pnd_Cor_Ph_Last_Upd_Tme_1_6() { return scil9000_Pnd_Cor_Ph_Last_Upd_Tme_1_6; }

    public DbsField getScil9000_Pnd_Cor_Ph_Last_Upd_Tme_7() { return scil9000_Pnd_Cor_Ph_Last_Upd_Tme_7; }

    public DbsField getScil9000_Pnd_Cor_Ph_Last_Updte_Userid() { return scil9000_Pnd_Cor_Ph_Last_Updte_Userid; }

    public DbsField getScil9000_Pnd_Change_Pin() { return scil9000_Pnd_Change_Pin; }

    public DbsField getScil9000_Pnd_Change_Ssn() { return scil9000_Pnd_Change_Ssn; }

    public DbsGroup getScil9000_Pnd_Change_SsnRedef13() { return scil9000_Pnd_Change_SsnRedef13; }

    public DbsField getScil9000_Pnd_Change_Ssn_A() { return scil9000_Pnd_Change_Ssn_A; }

    public DbsField getScil9000_Pnd_Filler_1() { return scil9000_Pnd_Filler_1; }

    public DbsGroup getScil9000Redef14() { return scil9000Redef14; }

    public DbsField getScil9000_Pnd_Omni_Data_1() { return scil9000_Pnd_Omni_Data_1; }

    public DbsField getScil9000_Pnd_Omni_Data_2() { return scil9000_Pnd_Omni_Data_2; }

    public DbsField getScil9000_Pnd_Omni_Data_3() { return scil9000_Pnd_Omni_Data_3; }

    public DbsField getScil9000_Pnd_Omni_Data_4() { return scil9000_Pnd_Omni_Data_4; }

    public DbsField getScil9000_Pnd_Omni_Data_5() { return scil9000_Pnd_Omni_Data_5; }

    public DbsField getScil9000_Pnd_Omni_Data_6() { return scil9000_Pnd_Omni_Data_6; }

    public DbsField getScil9000_Pnd_Omni_Data_7() { return scil9000_Pnd_Omni_Data_7; }

    public DbsField getScil9000_Pnd_Omni_Data_8() { return scil9000_Pnd_Omni_Data_8; }

    public DbsField getScil9000_Pnd_Omni_Data_9() { return scil9000_Pnd_Omni_Data_9; }

    public DbsField getScil9000_Pnd_Omni_Data_10() { return scil9000_Pnd_Omni_Data_10; }

    public DbsField getScil9000_Pnd_Omni_Data_11() { return scil9000_Pnd_Omni_Data_11; }

    public DbsField getScil9000_Pnd_Omni_Data_12() { return scil9000_Pnd_Omni_Data_12; }

    public DbsField getScil9000_Pnd_Omni_Data_13() { return scil9000_Pnd_Omni_Data_13; }

    public DbsField getScil9000_Pnd_Omni_Data_14() { return scil9000_Pnd_Omni_Data_14; }

    public DbsField getScil9000_Pnd_Omni_Data_15() { return scil9000_Pnd_Omni_Data_15; }

    public DbsField getScil9000_Pnd_Omni_Data_16() { return scil9000_Pnd_Omni_Data_16; }

    public DbsField getScil9000_Pnd_Omni_Data_17() { return scil9000_Pnd_Omni_Data_17; }

    public DbsField getScil9000_Pnd_Omni_Data_18() { return scil9000_Pnd_Omni_Data_18; }

    public DbsField getScil9000_Pnd_Omni_Data_19() { return scil9000_Pnd_Omni_Data_19; }

    public DbsField getScil9000_Pnd_Omni_Data_20() { return scil9000_Pnd_Omni_Data_20; }

    public DbsField getScil9000_Pnd_Omni_Data_21() { return scil9000_Pnd_Omni_Data_21; }

    public DbsField getScil9000_Pnd_Omni_Data_22() { return scil9000_Pnd_Omni_Data_22; }

    public DbsField getScil9000_Pnd_Omni_Data_23() { return scil9000_Pnd_Omni_Data_23; }

    public DbsField getScil9000_Pnd_Omni_Data_24() { return scil9000_Pnd_Omni_Data_24; }

    public DbsField getScil9000_Pnd_Omni_Data_25() { return scil9000_Pnd_Omni_Data_25; }

    public DbsField getScil9000_Pnd_Omni_Data_26() { return scil9000_Pnd_Omni_Data_26; }

    public DbsField getScil9000_Pnd_Omni_Data_27() { return scil9000_Pnd_Omni_Data_27; }

    public DbsField getScil9000_Pnd_Omni_Data_28() { return scil9000_Pnd_Omni_Data_28; }

    public DbsField getScil9000_Pnd_Omni_Data_29() { return scil9000_Pnd_Omni_Data_29; }

    public DbsField getScil9000_Pnd_Omni_Data_30() { return scil9000_Pnd_Omni_Data_30; }

    public DbsField getScil9000_Pnd_Omni_Data_31() { return scil9000_Pnd_Omni_Data_31; }

    public DbsField getScil9000_Pnd_Omni_Data_32() { return scil9000_Pnd_Omni_Data_32; }

    public DbsField getScil9000_Pnd_Omni_Data_33() { return scil9000_Pnd_Omni_Data_33; }

    public DbsField getScil9000_Pnd_Omni_Data_34() { return scil9000_Pnd_Omni_Data_34; }

    public DbsField getScil9000_Pnd_Omni_Data_35() { return scil9000_Pnd_Omni_Data_35; }

    public DbsField getScil9000_Pnd_Omni_Data_36() { return scil9000_Pnd_Omni_Data_36; }

    public DbsField getScil9000_Pnd_Omni_Data_37() { return scil9000_Pnd_Omni_Data_37; }

    public DbsField getScil9000_Pnd_Omni_Data_38() { return scil9000_Pnd_Omni_Data_38; }

    public DbsField getScil9000_Pnd_Omni_Data_39() { return scil9000_Pnd_Omni_Data_39; }

    public DbsField getScil9000_Pnd_Omni_Data_40() { return scil9000_Pnd_Omni_Data_40; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        scil9000 = newGroupInRecord("scil9000", "SCIL9000");
        scil9000_Pnd_Plan_Id = scil9000.newFieldInGroup("scil9000_Pnd_Plan_Id", "#PLAN-ID", FieldType.STRING, 6);
        scil9000_Pnd_Plan_No = scil9000.newFieldInGroup("scil9000_Pnd_Plan_No", "#PLAN-NO", FieldType.STRING, 6);
        scil9000_Pnd_Sub_Plan_No = scil9000.newFieldInGroup("scil9000_Pnd_Sub_Plan_No", "#SUB-PLAN-NO", FieldType.STRING, 6);
        scil9000_Pnd_Sg_Part_Ext = scil9000.newFieldInGroup("scil9000_Pnd_Sg_Part_Ext", "#SG-PART-EXT", FieldType.STRING, 2);
        scil9000_Pnd_User_Id = scil9000.newFieldInGroup("scil9000_Pnd_User_Id", "#USER-ID", FieldType.STRING, 8);
        scil9000_Pnd_Pi_Dept = scil9000.newFieldInGroup("scil9000_Pnd_Pi_Dept", "#PI-DEPT", FieldType.STRING, 2);
        scil9000_Pnd_Pi_Task_Id = scil9000.newFieldInGroup("scil9000_Pnd_Pi_Task_Id", "#PI-TASK-ID", FieldType.STRING, 15);
        scil9000_Pnd_Pull_Code = scil9000.newFieldInGroup("scil9000_Pnd_Pull_Code", "#PULL-CODE", FieldType.STRING, 1);
        scil9000_Pnd_Companion_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Companion_Ind", "#COMPANION-IND", FieldType.STRING, 1);
        scil9000_Pnd_Divorce_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Divorce_Ind", "#DIVORCE-IND", FieldType.STRING, 1);
        scil9000_Pnd_Enrollment_Method = scil9000.newFieldInGroup("scil9000_Pnd_Enrollment_Method", "#ENROLLMENT-METHOD", FieldType.STRING, 1);
        scil9000_Pnd_Additional_Cref_Rqst = scil9000.newFieldInGroup("scil9000_Pnd_Additional_Cref_Rqst", "#ADDITIONAL-CREF-RQST", FieldType.STRING, 1);
        scil9000_Pnd_Entry_Date = scil9000.newFieldInGroup("scil9000_Pnd_Entry_Date", "#ENTRY-DATE", FieldType.STRING, 8);
        scil9000_Pnd_Date_App_Recvd = scil9000.newFieldInGroup("scil9000_Pnd_Date_App_Recvd", "#DATE-APP-RECVD", FieldType.STRING, 8);
        scil9000_Pnd_Annuity_Start_Date = scil9000.newFieldInGroup("scil9000_Pnd_Annuity_Start_Date", "#ANNUITY-START-DATE", FieldType.STRING, 8);
        scil9000_Pnd_Tiaa_Date_Of_Issue = scil9000.newFieldInGroup("scil9000_Pnd_Tiaa_Date_Of_Issue", "#TIAA-DATE-OF-ISSUE", FieldType.STRING, 8);
        scil9000_Pnd_Cref_Date_Of_Issue = scil9000.newFieldInGroup("scil9000_Pnd_Cref_Date_Of_Issue", "#CREF-DATE-OF-ISSUE", FieldType.STRING, 8);
        scil9000_Pnd_Tiaa_Contract_No = scil9000.newFieldInGroup("scil9000_Pnd_Tiaa_Contract_No", "#TIAA-CONTRACT-NO", FieldType.STRING, 10);
        scil9000_Pnd_Cref_Certificate_No = scil9000.newFieldInGroup("scil9000_Pnd_Cref_Certificate_No", "#CREF-CERTIFICATE-NO", FieldType.STRING, 10);
        scil9000_Pnd_Rl_Contract_No = scil9000.newFieldInGroup("scil9000_Pnd_Rl_Contract_No", "#RL-CONTRACT-NO", FieldType.STRING, 10);
        scil9000_Pnd_Loan_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Loan_Ind", "#LOAN-IND", FieldType.STRING, 1);
        scil9000_Pnd_Vesting_Code = scil9000.newFieldInGroup("scil9000_Pnd_Vesting_Code", "#VESTING-CODE", FieldType.STRING, 1);
        scil9000_Pnd_Vesting_Code_Override = scil9000.newFieldInGroup("scil9000_Pnd_Vesting_Code_Override", "#VESTING-CODE-OVERRIDE", FieldType.STRING, 
            1);
        scil9000_Pnd_Pin_Nbr = scil9000.newFieldInGroup("scil9000_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.STRING, 12);
        scil9000_Pnd_Ssn = scil9000.newFieldInGroup("scil9000_Pnd_Ssn", "#SSN", FieldType.STRING, 9);
        scil9000_Pnd_Prefix = scil9000.newFieldInGroup("scil9000_Pnd_Prefix", "#PREFIX", FieldType.STRING, 8);
        scil9000_Pnd_Last_Name = scil9000.newFieldInGroup("scil9000_Pnd_Last_Name", "#LAST-NAME", FieldType.STRING, 30);
        scil9000_Pnd_First_Name = scil9000.newFieldInGroup("scil9000_Pnd_First_Name", "#FIRST-NAME", FieldType.STRING, 30);
        scil9000_Pnd_Middle_Name = scil9000.newFieldInGroup("scil9000_Pnd_Middle_Name", "#MIDDLE-NAME", FieldType.STRING, 30);
        scil9000_Pnd_Suffix = scil9000.newFieldInGroup("scil9000_Pnd_Suffix", "#SUFFIX", FieldType.STRING, 8);
        scil9000_Pnd_Date_Of_Birth = scil9000.newFieldInGroup("scil9000_Pnd_Date_Of_Birth", "#DATE-OF-BIRTH", FieldType.STRING, 8);
        scil9000_Pnd_Sex = scil9000.newFieldInGroup("scil9000_Pnd_Sex", "#SEX", FieldType.STRING, 1);
        scil9000_Pnd_Daytime_Phone = scil9000.newFieldInGroup("scil9000_Pnd_Daytime_Phone", "#DAYTIME-PHONE", FieldType.STRING, 20);
        scil9000_Pnd_Evening_Phone = scil9000.newFieldInGroup("scil9000_Pnd_Evening_Phone", "#EVENING-PHONE", FieldType.STRING, 20);
        scil9000_Pnd_Email_Address = scil9000.newFieldInGroup("scil9000_Pnd_Email_Address", "#EMAIL-ADDRESS", FieldType.STRING, 50);
        scil9000_Pnd_Mailing_Address_Line = scil9000.newFieldArrayInGroup("scil9000_Pnd_Mailing_Address_Line", "#MAILING-ADDRESS-LINE", FieldType.STRING, 
            35, new DbsArrayController(1,3));
        scil9000_Pnd_Mailing_City = scil9000.newFieldInGroup("scil9000_Pnd_Mailing_City", "#MAILING-CITY", FieldType.STRING, 27);
        scil9000_Pnd_Mailing_State = scil9000.newFieldInGroup("scil9000_Pnd_Mailing_State", "#MAILING-STATE", FieldType.STRING, 2);
        scil9000_Pnd_Mailing_Zip_Code = scil9000.newFieldInGroup("scil9000_Pnd_Mailing_Zip_Code", "#MAILING-ZIP-CODE", FieldType.STRING, 9);
        scil9000_Pnd_Residence_Address_Line = scil9000.newFieldArrayInGroup("scil9000_Pnd_Residence_Address_Line", "#RESIDENCE-ADDRESS-LINE", FieldType.STRING, 
            35, new DbsArrayController(1,3));
        scil9000_Pnd_Residence_City = scil9000.newFieldInGroup("scil9000_Pnd_Residence_City", "#RESIDENCE-CITY", FieldType.STRING, 27);
        scil9000_Pnd_Residence_State = scil9000.newFieldInGroup("scil9000_Pnd_Residence_State", "#RESIDENCE-STATE", FieldType.STRING, 2);
        scil9000_Pnd_Residence_Zip_Code = scil9000.newFieldInGroup("scil9000_Pnd_Residence_Zip_Code", "#RESIDENCE-ZIP-CODE", FieldType.STRING, 9);
        scil9000_Pnd_Allocation = scil9000.newFieldArrayInGroup("scil9000_Pnd_Allocation", "#ALLOCATION", FieldType.STRING, 3, new DbsArrayController(1,
            100));
        scil9000_Pnd_Allocation_Fund_Ticker = scil9000.newFieldArrayInGroup("scil9000_Pnd_Allocation_Fund_Ticker", "#ALLOCATION-FUND-TICKER", FieldType.STRING, 
            10, new DbsArrayController(1,100));
        scil9000_Pnd_Alloc_Default_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Alloc_Default_Ind", "#ALLOC-DEFAULT-IND", FieldType.STRING, 1);
        scil9000_Pnd_Alloc_Model_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Alloc_Model_Ind", "#ALLOC-MODEL-IND", FieldType.STRING, 1);
        scil9000_Pnd_Bene_Type = scil9000.newFieldArrayInGroup("scil9000_Pnd_Bene_Type", "#BENE-TYPE", FieldType.STRING, 1, new DbsArrayController(1,20));
        scil9000_Pnd_Bene_Ssn = scil9000.newFieldArrayInGroup("scil9000_Pnd_Bene_Ssn", "#BENE-SSN", FieldType.STRING, 9, new DbsArrayController(1,20));
        scil9000_Pnd_Bene_Name = scil9000.newFieldArrayInGroup("scil9000_Pnd_Bene_Name", "#BENE-NAME", FieldType.STRING, 35, new DbsArrayController(1,
            20));
        scil9000_Pnd_Bene_Relationship = scil9000.newFieldArrayInGroup("scil9000_Pnd_Bene_Relationship", "#BENE-RELATIONSHIP", FieldType.STRING, 15, new 
            DbsArrayController(1,20));
        scil9000_Pnd_Bene_Relationship_Code = scil9000.newFieldArrayInGroup("scil9000_Pnd_Bene_Relationship_Code", "#BENE-RELATIONSHIP-CODE", FieldType.STRING, 
            2, new DbsArrayController(1,20));
        scil9000_Pnd_Bene_Date_Of_Birth = scil9000.newFieldArrayInGroup("scil9000_Pnd_Bene_Date_Of_Birth", "#BENE-DATE-OF-BIRTH", FieldType.STRING, 8, 
            new DbsArrayController(1,20));
        scil9000_Pnd_Bene_Alloc_Pct = scil9000.newFieldArrayInGroup("scil9000_Pnd_Bene_Alloc_Pct", "#BENE-ALLOC-PCT", FieldType.STRING, 5, new DbsArrayController(1,
            20));
        scil9000_Pnd_Bene_Numerator = scil9000.newFieldArrayInGroup("scil9000_Pnd_Bene_Numerator", "#BENE-NUMERATOR", FieldType.STRING, 3, new DbsArrayController(1,
            20));
        scil9000_Pnd_Bene_Denominator = scil9000.newFieldArrayInGroup("scil9000_Pnd_Bene_Denominator", "#BENE-DENOMINATOR", FieldType.STRING, 3, new DbsArrayController(1,
            20));
        scil9000_Pnd_Bene_Special_Text_Ind = scil9000.newFieldArrayInGroup("scil9000_Pnd_Bene_Special_Text_Ind", "#BENE-SPECIAL-TEXT-IND", FieldType.STRING, 
            1, new DbsArrayController(1,20));
        scil9000_Pnd_Bene_Special_Text = scil9000.newFieldArrayInGroup("scil9000_Pnd_Bene_Special_Text", "#BENE-SPECIAL-TEXT", FieldType.STRING, 72, new 
            DbsArrayController(1,20,1,3));
        scil9000_Pnd_Bene_Mos_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Bene_Mos_Ind", "#BENE-MOS-IND", FieldType.STRING, 1);
        scil9000_Pnd_Bene_Estate = scil9000.newFieldInGroup("scil9000_Pnd_Bene_Estate", "#BENE-ESTATE", FieldType.STRING, 1);
        scil9000_Pnd_Bene_Trust = scil9000.newFieldInGroup("scil9000_Pnd_Bene_Trust", "#BENE-TRUST", FieldType.STRING, 1);
        scil9000_Pnd_Bene_Category = scil9000.newFieldInGroup("scil9000_Pnd_Bene_Category", "#BENE-CATEGORY", FieldType.STRING, 1);
        scil9000_Pnd_Ica_Rcvd_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Ica_Rcvd_Ind", "#ICA-RCVD-IND", FieldType.STRING, 1);
        scil9000_Pnd_Plan_Type = scil9000.newFieldInGroup("scil9000_Pnd_Plan_Type", "#PLAN-TYPE", FieldType.STRING, 2);
        scil9000_Pnd_Employer_Type = scil9000.newFieldInGroup("scil9000_Pnd_Employer_Type", "#EMPLOYER-TYPE", FieldType.STRING, 2);
        scil9000_Pnd_Erisa_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Erisa_Ind", "#ERISA-IND", FieldType.STRING, 1);
        scil9000_Pnd_Oia_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Oia_Ind", "#OIA-IND", FieldType.STRING, 2);
        scil9000_Pnd_Issue_State = scil9000.newFieldInGroup("scil9000_Pnd_Issue_State", "#ISSUE-STATE", FieldType.STRING, 2);
        scil9000_Pnd_Mailing_Instructions = scil9000.newFieldInGroup("scil9000_Pnd_Mailing_Instructions", "#MAILING-INSTRUCTIONS", FieldType.STRING, 1);
        scil9000_Pnd_Contract_Pick_Up_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Contract_Pick_Up_Ind", "#CONTRACT-PICK-UP-IND", FieldType.STRING, 1);
        scil9000_Pnd_Omni_Account_Issuance = scil9000.newFieldInGroup("scil9000_Pnd_Omni_Account_Issuance", "#OMNI-ACCOUNT-ISSUANCE", FieldType.STRING, 
            1);
        scil9000_Pnd_Incompl_Data_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Incompl_Data_Ind", "#INCOMPL-DATA-IND", FieldType.STRING, 1);
        scil9000_Pnd_Orig_Tiaa_Cntrct = scil9000.newFieldInGroup("scil9000_Pnd_Orig_Tiaa_Cntrct", "#ORIG-TIAA-CNTRCT", FieldType.STRING, 10);
        scil9000_Pnd_Orig_Cref_Cntrct = scil9000.newFieldInGroup("scil9000_Pnd_Orig_Cref_Cntrct", "#ORIG-CREF-CNTRCT", FieldType.STRING, 10);
        scil9000_Pnd_Money_Invested_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Money_Invested_Ind", "#MONEY-INVESTED-IND", FieldType.STRING, 1);
        scil9000_Pnd_Country_Code = scil9000.newFieldInGroup("scil9000_Pnd_Country_Code", "#COUNTRY-CODE", FieldType.STRING, 3);
        scil9000_Pnd_Div_Sub = scil9000.newFieldInGroup("scil9000_Pnd_Div_Sub", "#DIV-SUB", FieldType.STRING, 4);
        scil9000_Pnd_Sg_Replacement_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Sg_Replacement_Ind", "#SG-REPLACEMENT-IND", FieldType.STRING, 1);
        scil9000_Pnd_Sg_Register_Id = scil9000.newFieldInGroup("scil9000_Pnd_Sg_Register_Id", "#SG-REGISTER-ID", FieldType.STRING, 11);
        scil9000_Pnd_Sg_Agent_Racf_Id = scil9000.newFieldInGroup("scil9000_Pnd_Sg_Agent_Racf_Id", "#SG-AGENT-RACF-ID", FieldType.STRING, 15);
        scil9000_Pnd_Sg_Exempt_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Sg_Exempt_Ind", "#SG-EXEMPT-IND", FieldType.STRING, 1);
        scil9000_Pnd_Sg_Insurer_1 = scil9000.newFieldInGroup("scil9000_Pnd_Sg_Insurer_1", "#SG-INSURER-1", FieldType.STRING, 30);
        scil9000_Pnd_Sg_Insurer_2 = scil9000.newFieldInGroup("scil9000_Pnd_Sg_Insurer_2", "#SG-INSURER-2", FieldType.STRING, 30);
        scil9000_Pnd_Sg_Insurer_3 = scil9000.newFieldInGroup("scil9000_Pnd_Sg_Insurer_3", "#SG-INSURER-3", FieldType.STRING, 30);
        scil9000_Pnd_Sg_1035_Exch_Ind_1 = scil9000.newFieldInGroup("scil9000_Pnd_Sg_1035_Exch_Ind_1", "#SG-1035-EXCH-IND-1", FieldType.STRING, 1);
        scil9000_Pnd_Sg_1035_Exch_Ind_2 = scil9000.newFieldInGroup("scil9000_Pnd_Sg_1035_Exch_Ind_2", "#SG-1035-EXCH-IND-2", FieldType.STRING, 1);
        scil9000_Pnd_Sg_1035_Exch_Ind_3 = scil9000.newFieldInGroup("scil9000_Pnd_Sg_1035_Exch_Ind_3", "#SG-1035-EXCH-IND-3", FieldType.STRING, 1);
        scil9000_Pnd_Pl_Client_Id = scil9000.newFieldInGroup("scil9000_Pnd_Pl_Client_Id", "#PL-CLIENT-ID", FieldType.STRING, 6);
        scil9000_Pnd_Model_Id = scil9000.newFieldInGroup("scil9000_Pnd_Model_Id", "#MODEL-ID", FieldType.STRING, 4);
        scil9000_Pnd_As_Ind = scil9000.newFieldInGroup("scil9000_Pnd_As_Ind", "#AS-IND", FieldType.STRING, 1);
        scil9000_Pnd_Curr_Def_Option = scil9000.newFieldInGroup("scil9000_Pnd_Curr_Def_Option", "#CURR-DEF-OPTION", FieldType.STRING, 1);
        scil9000_Pnd_Curr_Def_Amt = scil9000.newFieldInGroup("scil9000_Pnd_Curr_Def_Amt", "#CURR-DEF-AMT", FieldType.DECIMAL, 9,2);
        scil9000_Pnd_Curr_Def_AmtRedef1 = scil9000.newGroupInGroup("scil9000_Pnd_Curr_Def_AmtRedef1", "Redefines", scil9000_Pnd_Curr_Def_Amt);
        scil9000_Pnd_Curr_Def_Amt_A = scil9000_Pnd_Curr_Def_AmtRedef1.newFieldInGroup("scil9000_Pnd_Curr_Def_Amt_A", "#CURR-DEF-AMT-A", FieldType.STRING, 
            9);
        scil9000_Pnd_Incr_Option = scil9000.newFieldInGroup("scil9000_Pnd_Incr_Option", "#INCR-OPTION", FieldType.STRING, 1);
        scil9000_Pnd_Incr_Amt = scil9000.newFieldInGroup("scil9000_Pnd_Incr_Amt", "#INCR-AMT", FieldType.DECIMAL, 9,2);
        scil9000_Pnd_Incr_AmtRedef2 = scil9000.newGroupInGroup("scil9000_Pnd_Incr_AmtRedef2", "Redefines", scil9000_Pnd_Incr_Amt);
        scil9000_Pnd_Incr_Amt_A = scil9000_Pnd_Incr_AmtRedef2.newFieldInGroup("scil9000_Pnd_Incr_Amt_A", "#INCR-AMT-A", FieldType.STRING, 9);
        scil9000_Pnd_Max_Pct = scil9000.newFieldInGroup("scil9000_Pnd_Max_Pct", "#MAX-PCT", FieldType.DECIMAL, 5,2);
        scil9000_Pnd_Max_PctRedef3 = scil9000.newGroupInGroup("scil9000_Pnd_Max_PctRedef3", "Redefines", scil9000_Pnd_Max_Pct);
        scil9000_Pnd_Max_Pct_A = scil9000_Pnd_Max_PctRedef3.newFieldInGroup("scil9000_Pnd_Max_Pct_A", "#MAX-PCT-A", FieldType.STRING, 5);
        scil9000_Pnd_Optout_Days = scil9000.newFieldInGroup("scil9000_Pnd_Optout_Days", "#OPTOUT-DAYS", FieldType.NUMERIC, 2);
        scil9000_Pnd_Optout_DaysRedef4 = scil9000.newGroupInGroup("scil9000_Pnd_Optout_DaysRedef4", "Redefines", scil9000_Pnd_Optout_Days);
        scil9000_Pnd_Optout_Days_A = scil9000_Pnd_Optout_DaysRedef4.newFieldInGroup("scil9000_Pnd_Optout_Days_A", "#OPTOUT-DAYS-A", FieldType.STRING, 
            2);
        scil9000_Pnd_Tsv_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Tsv_Ind", "#TSV-IND", FieldType.STRING, 1);
        scil9000_Pnd_Indx_Guar_Rate_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Indx_Guar_Rate_Ind", "#INDX-GUAR-RATE-IND", FieldType.STRING, 1);
        scil9000_Pnd_Eir_Status = scil9000.newFieldInGroup("scil9000_Pnd_Eir_Status", "#EIR-STATUS", FieldType.STRING, 2);
        scil9000_Pnd_Eir_From_T114_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Eir_From_T114_Ind", "#EIR-FROM-T114-IND", FieldType.STRING, 1);
        scil9000_Pnd_Orchestration_Id = scil9000.newFieldInGroup("scil9000_Pnd_Orchestration_Id", "#ORCHESTRATION-ID", FieldType.STRING, 15);
        scil9000_Pnd_Use_Bpel_Data = scil9000.newFieldInGroup("scil9000_Pnd_Use_Bpel_Data", "#USE-BPEL-DATA", FieldType.STRING, 1);
        scil9000_Pnd_Legal_Ann_Option = scil9000.newFieldInGroup("scil9000_Pnd_Legal_Ann_Option", "#LEGAL-ANN-OPTION", FieldType.STRING, 1);
        scil9000_Pnd_Substitution_Contract_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Substitution_Contract_Ind", "#SUBSTITUTION-CONTRACT-IND", FieldType.STRING, 
            1);
        scil9000_Pnd_Conversion_Issue_State = scil9000.newFieldInGroup("scil9000_Pnd_Conversion_Issue_State", "#CONVERSION-ISSUE-STATE", FieldType.STRING, 
            2);
        scil9000_Pnd_Deceased_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Deceased_Ind", "#DECEASED-IND", FieldType.STRING, 1);
        scil9000_Pnd_Non_Proprietary_Pkg_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Non_Proprietary_Pkg_Ind", "#NON-PROPRIETARY-PKG-IND", FieldType.STRING, 
            5);
        scil9000_Pnd_Tsr_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Tsr_Ind", "#TSR-IND", FieldType.STRING, 3);
        scil9000_Pnd_Decedent_Contract = scil9000.newFieldInGroup("scil9000_Pnd_Decedent_Contract", "#DECEDENT-CONTRACT", FieldType.STRING, 10);
        scil9000_Pnd_Relation_To_Decedent = scil9000.newFieldInGroup("scil9000_Pnd_Relation_To_Decedent", "#RELATION-TO-DECEDENT", FieldType.STRING, 1);
        scil9000_Pnd_Ssn_Tin_Ind = scil9000.newFieldInGroup("scil9000_Pnd_Ssn_Tin_Ind", "#SSN-TIN-IND", FieldType.STRING, 1);
        scil9000_Pnd_Oneira_Acct_No = scil9000.newFieldInGroup("scil9000_Pnd_Oneira_Acct_No", "#ONEIRA-ACCT-NO", FieldType.STRING, 10);
        scil9000_Pnd_Omni_Folder_Name = scil9000.newFieldInGroup("scil9000_Pnd_Omni_Folder_Name", "#OMNI-FOLDER-NAME", FieldType.STRING, 20);
        scil9000_Pnd_Allocation_Source_1 = scil9000.newFieldInGroup("scil9000_Pnd_Allocation_Source_1", "#ALLOCATION-SOURCE-1", FieldType.STRING, 1);
        scil9000_Pnd_Allocation_Source_2 = scil9000.newFieldInGroup("scil9000_Pnd_Allocation_Source_2", "#ALLOCATION-SOURCE-2", FieldType.STRING, 1);
        scil9000_Pnd_Alloc_Table_2 = scil9000.newGroupInGroup("scil9000_Pnd_Alloc_Table_2", "#ALLOC-TABLE-2");
        scil9000_Pnd_Allocation_Pct_2 = scil9000_Pnd_Alloc_Table_2.newFieldArrayInGroup("scil9000_Pnd_Allocation_Pct_2", "#ALLOCATION-PCT-2", FieldType.STRING, 
            3, new DbsArrayController(1,100));
        scil9000_Pnd_Allocation_Ticker_2 = scil9000_Pnd_Alloc_Table_2.newFieldArrayInGroup("scil9000_Pnd_Allocation_Ticker_2", "#ALLOCATION-TICKER-2", 
            FieldType.STRING, 10, new DbsArrayController(1,100));
        scil9000_Pnd_Participant_File = scil9000.newFieldInGroup("scil9000_Pnd_Participant_File", "#PARTICIPANT-FILE", FieldType.STRING, 354);
        scil9000_Pnd_Participant_FileRedef5 = scil9000.newGroupInGroup("scil9000_Pnd_Participant_FileRedef5", "Redefines", scil9000_Pnd_Participant_File);
        scil9000_Pnd_Ph_Pin_Ssn = scil9000_Pnd_Participant_FileRedef5.newFieldInGroup("scil9000_Pnd_Ph_Pin_Ssn", "#PH-PIN-SSN", FieldType.STRING, 21);
        scil9000_Pnd_Ph_Pin_SsnRedef6 = scil9000_Pnd_Participant_FileRedef5.newGroupInGroup("scil9000_Pnd_Ph_Pin_SsnRedef6", "Redefines", scil9000_Pnd_Ph_Pin_Ssn);
        scil9000_Pnd_Ph_Unique_Id_Nbr = scil9000_Pnd_Ph_Pin_SsnRedef6.newFieldInGroup("scil9000_Pnd_Ph_Unique_Id_Nbr", "#PH-UNIQUE-ID-NBR", FieldType.NUMERIC, 
            12);
        scil9000_Pnd_Ph_Social_Security_No = scil9000_Pnd_Ph_Pin_SsnRedef6.newFieldInGroup("scil9000_Pnd_Ph_Social_Security_No", "#PH-SOCIAL-SECURITY-NO", 
            FieldType.NUMERIC, 9);
        scil9000_Pnd_Ph_Social_Security_NoRedef7 = scil9000_Pnd_Ph_Pin_SsnRedef6.newGroupInGroup("scil9000_Pnd_Ph_Social_Security_NoRedef7", "Redefines", 
            scil9000_Pnd_Ph_Social_Security_No);
        scil9000_Pnd_Ph_Ssn_A = scil9000_Pnd_Ph_Social_Security_NoRedef7.newFieldInGroup("scil9000_Pnd_Ph_Ssn_A", "#PH-SSN-A", FieldType.STRING, 9);
        scil9000_Pnd_Ph_Frgn_Ssn_Nbr = scil9000_Pnd_Participant_FileRedef5.newFieldInGroup("scil9000_Pnd_Ph_Frgn_Ssn_Nbr", "#PH-FRGN-SSN-NBR", FieldType.NUMERIC, 
            9);
        scil9000_Pnd_Ph_Frgn_Ssn_NbrRedef8 = scil9000_Pnd_Participant_FileRedef5.newGroupInGroup("scil9000_Pnd_Ph_Frgn_Ssn_NbrRedef8", "Redefines", scil9000_Pnd_Ph_Frgn_Ssn_Nbr);
        scil9000_Pnd_Ph_Frgn_Ssn_Nbr_A = scil9000_Pnd_Ph_Frgn_Ssn_NbrRedef8.newFieldInGroup("scil9000_Pnd_Ph_Frgn_Ssn_Nbr_A", "#PH-FRGN-SSN-NBR-A", FieldType.STRING, 
            9);
        scil9000_Pnd_Ph_Negative_Election_Cd = scil9000_Pnd_Participant_FileRedef5.newFieldInGroup("scil9000_Pnd_Ph_Negative_Election_Cd", "#PH-NEGATIVE-ELECTION-CD", 
            FieldType.NUMERIC, 3);
        scil9000_Pnd_Ph_Negative_Election_CdRedef9 = scil9000_Pnd_Participant_FileRedef5.newGroupInGroup("scil9000_Pnd_Ph_Negative_Election_CdRedef9", 
            "Redefines", scil9000_Pnd_Ph_Negative_Election_Cd);
        scil9000_Pnd_Ph_Negative_Elect_Cd_A = scil9000_Pnd_Ph_Negative_Election_CdRedef9.newFieldInGroup("scil9000_Pnd_Ph_Negative_Elect_Cd_A", "#PH-NEGATIVE-ELECT-CD-A", 
            FieldType.STRING, 3);
        scil9000_Pnd_Ph_Rltnshp_Cde = scil9000_Pnd_Participant_FileRedef5.newFieldInGroup("scil9000_Pnd_Ph_Rltnshp_Cde", "#PH-RLTNSHP-CDE", FieldType.STRING, 
            1);
        scil9000_Pnd_Ph_Dob_Dte = scil9000_Pnd_Participant_FileRedef5.newFieldInGroup("scil9000_Pnd_Ph_Dob_Dte", "#PH-DOB-DTE", FieldType.NUMERIC, 8);
        scil9000_Pnd_Ph_Dod_Dte = scil9000_Pnd_Participant_FileRedef5.newFieldInGroup("scil9000_Pnd_Ph_Dod_Dte", "#PH-DOD-DTE", FieldType.NUMERIC, 8);
        scil9000_Pnd_Ph_Sex_Cde = scil9000_Pnd_Participant_FileRedef5.newFieldInGroup("scil9000_Pnd_Ph_Sex_Cde", "#PH-SEX-CDE", FieldType.STRING, 1);
        scil9000_Pnd_Ph_Xref_Pin = scil9000_Pnd_Participant_FileRedef5.newFieldInGroup("scil9000_Pnd_Ph_Xref_Pin", "#PH-XREF-PIN", FieldType.NUMERIC, 
            12);
        scil9000_Pnd_Ph_Xref_PinRedef10 = scil9000_Pnd_Participant_FileRedef5.newGroupInGroup("scil9000_Pnd_Ph_Xref_PinRedef10", "Redefines", scil9000_Pnd_Ph_Xref_Pin);
        scil9000_Pnd_Ph_Xref_Pin_A = scil9000_Pnd_Ph_Xref_PinRedef10.newFieldInGroup("scil9000_Pnd_Ph_Xref_Pin_A", "#PH-XREF-PIN-A", FieldType.STRING, 
            12);
        scil9000_Pnd_Ph_Prfx_Nme = scil9000_Pnd_Participant_FileRedef5.newFieldInGroup("scil9000_Pnd_Ph_Prfx_Nme", "#PH-PRFX-NME", FieldType.STRING, 8);
        scil9000_Pnd_Ph_Last_Nme = scil9000_Pnd_Participant_FileRedef5.newFieldInGroup("scil9000_Pnd_Ph_Last_Nme", "#PH-LAST-NME", FieldType.STRING, 30);
        scil9000_Pnd_Ph_First_Nme = scil9000_Pnd_Participant_FileRedef5.newFieldInGroup("scil9000_Pnd_Ph_First_Nme", "#PH-FIRST-NME", FieldType.STRING, 
            30);
        scil9000_Pnd_Ph_Mddle_Nme = scil9000_Pnd_Participant_FileRedef5.newFieldInGroup("scil9000_Pnd_Ph_Mddle_Nme", "#PH-MDDLE-NME", FieldType.STRING, 
            30);
        scil9000_Pnd_Ph_Sffx_Nme = scil9000_Pnd_Participant_FileRedef5.newFieldInGroup("scil9000_Pnd_Ph_Sffx_Nme", "#PH-SFFX-NME", FieldType.STRING, 8);
        scil9000_Pnd_Ph_Actve_Vip_Cde = scil9000_Pnd_Participant_FileRedef5.newFieldArrayInGroup("scil9000_Pnd_Ph_Actve_Vip_Cde", "#PH-ACTVE-VIP-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,8));
        scil9000_Pnd_Ph_Mail_Cde = scil9000_Pnd_Participant_FileRedef5.newFieldArrayInGroup("scil9000_Pnd_Ph_Mail_Cde", "#PH-MAIL-CDE", FieldType.STRING, 
            2, new DbsArrayController(1,25));
        scil9000_Pnd_Ph_Mail_Origin_Cde = scil9000_Pnd_Participant_FileRedef5.newFieldArrayInGroup("scil9000_Pnd_Ph_Mail_Origin_Cde", "#PH-MAIL-ORIGIN-CDE", 
            FieldType.STRING, 3, new DbsArrayController(1,25));
        scil9000_Pnd_Cor_Ph_Last_Updte_Dte = scil9000_Pnd_Participant_FileRedef5.newFieldInGroup("scil9000_Pnd_Cor_Ph_Last_Updte_Dte", "#COR-PH-LAST-UPDTE-DTE", 
            FieldType.NUMERIC, 8);
        scil9000_Pnd_Cor_Ph_Last_Updte_Tme = scil9000_Pnd_Participant_FileRedef5.newFieldInGroup("scil9000_Pnd_Cor_Ph_Last_Updte_Tme", "#COR-PH-LAST-UPDTE-TME", 
            FieldType.NUMERIC, 7);
        scil9000_Pnd_Cor_Ph_Last_Updte_TmeRedef11 = scil9000_Pnd_Participant_FileRedef5.newGroupInGroup("scil9000_Pnd_Cor_Ph_Last_Updte_TmeRedef11", "Redefines", 
            scil9000_Pnd_Cor_Ph_Last_Updte_Tme);
        scil9000_Pnd_Cor_Ph_Last_Upd_Tme_A = scil9000_Pnd_Cor_Ph_Last_Updte_TmeRedef11.newFieldInGroup("scil9000_Pnd_Cor_Ph_Last_Upd_Tme_A", "#COR-PH-LAST-UPD-TME-A", 
            FieldType.STRING, 7);
        scil9000_Pnd_Cor_Ph_Last_Updte_TmeRedef12 = scil9000_Pnd_Participant_FileRedef5.newGroupInGroup("scil9000_Pnd_Cor_Ph_Last_Updte_TmeRedef12", "Redefines", 
            scil9000_Pnd_Cor_Ph_Last_Updte_Tme);
        scil9000_Pnd_Cor_Ph_Last_Upd_Tme_1_6 = scil9000_Pnd_Cor_Ph_Last_Updte_TmeRedef12.newFieldInGroup("scil9000_Pnd_Cor_Ph_Last_Upd_Tme_1_6", "#COR-PH-LAST-UPD-TME-1-6", 
            FieldType.STRING, 6);
        scil9000_Pnd_Cor_Ph_Last_Upd_Tme_7 = scil9000_Pnd_Cor_Ph_Last_Updte_TmeRedef12.newFieldInGroup("scil9000_Pnd_Cor_Ph_Last_Upd_Tme_7", "#COR-PH-LAST-UPD-TME-7", 
            FieldType.STRING, 1);
        scil9000_Pnd_Cor_Ph_Last_Updte_Userid = scil9000_Pnd_Participant_FileRedef5.newFieldInGroup("scil9000_Pnd_Cor_Ph_Last_Updte_Userid", "#COR-PH-LAST-UPDTE-USERID", 
            FieldType.STRING, 8);
        scil9000_Pnd_Change_Pin = scil9000_Pnd_Participant_FileRedef5.newFieldInGroup("scil9000_Pnd_Change_Pin", "#CHANGE-PIN", FieldType.NUMERIC, 12);
        scil9000_Pnd_Change_Ssn = scil9000_Pnd_Participant_FileRedef5.newFieldInGroup("scil9000_Pnd_Change_Ssn", "#CHANGE-SSN", FieldType.NUMERIC, 9);
        scil9000_Pnd_Change_SsnRedef13 = scil9000_Pnd_Participant_FileRedef5.newGroupInGroup("scil9000_Pnd_Change_SsnRedef13", "Redefines", scil9000_Pnd_Change_Ssn);
        scil9000_Pnd_Change_Ssn_A = scil9000_Pnd_Change_SsnRedef13.newFieldInGroup("scil9000_Pnd_Change_Ssn_A", "#CHANGE-SSN-A", FieldType.STRING, 9);
        scil9000_Pnd_Filler_1 = scil9000.newFieldInGroup("scil9000_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 72);
        scil9000Redef14 = newGroupInRecord("scil9000Redef14", "Redefines", scil9000);
        scil9000_Pnd_Omni_Data_1 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_1", "#OMNI-DATA-1", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_2 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_2", "#OMNI-DATA-2", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_3 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_3", "#OMNI-DATA-3", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_4 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_4", "#OMNI-DATA-4", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_5 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_5", "#OMNI-DATA-5", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_6 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_6", "#OMNI-DATA-6", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_7 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_7", "#OMNI-DATA-7", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_8 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_8", "#OMNI-DATA-8", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_9 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_9", "#OMNI-DATA-9", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_10 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_10", "#OMNI-DATA-10", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_11 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_11", "#OMNI-DATA-11", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_12 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_12", "#OMNI-DATA-12", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_13 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_13", "#OMNI-DATA-13", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_14 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_14", "#OMNI-DATA-14", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_15 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_15", "#OMNI-DATA-15", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_16 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_16", "#OMNI-DATA-16", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_17 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_17", "#OMNI-DATA-17", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_18 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_18", "#OMNI-DATA-18", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_19 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_19", "#OMNI-DATA-19", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_20 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_20", "#OMNI-DATA-20", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_21 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_21", "#OMNI-DATA-21", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_22 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_22", "#OMNI-DATA-22", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_23 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_23", "#OMNI-DATA-23", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_24 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_24", "#OMNI-DATA-24", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_25 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_25", "#OMNI-DATA-25", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_26 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_26", "#OMNI-DATA-26", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_27 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_27", "#OMNI-DATA-27", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_28 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_28", "#OMNI-DATA-28", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_29 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_29", "#OMNI-DATA-29", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_30 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_30", "#OMNI-DATA-30", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_31 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_31", "#OMNI-DATA-31", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_32 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_32", "#OMNI-DATA-32", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_33 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_33", "#OMNI-DATA-33", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_34 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_34", "#OMNI-DATA-34", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_35 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_35", "#OMNI-DATA-35", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_36 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_36", "#OMNI-DATA-36", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_37 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_37", "#OMNI-DATA-37", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_38 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_38", "#OMNI-DATA-38", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_39 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_39", "#OMNI-DATA-39", FieldType.STRING, 250);
        scil9000_Pnd_Omni_Data_40 = scil9000Redef14.newFieldInGroup("scil9000_Pnd_Omni_Data_40", "#OMNI-DATA-40", FieldType.STRING, 156);

        this.setRecordName("LdaScil9000");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaScil9000() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
