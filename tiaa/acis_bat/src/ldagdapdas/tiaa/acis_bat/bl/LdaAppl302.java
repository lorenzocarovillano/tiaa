/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:51:14 PM
**        * FROM NATURAL LDA     : APPL302
************************************************************
**        * FILE NAME            : LdaAppl302.java
**        * CLASS NAME           : LdaAppl302
**        * INSTANCE NAME        : LdaAppl302
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl302 extends DbsRecord
{
    // Properties
    private DbsGroup cv_Prap_Rule1;
    private DbsField cv_Prap_Rule1_Cv_Record_Type;
    private DbsField cv_Prap_Rule1_Cv_Key;
    private DbsGroup cv_Prap_Rule1_Cv_KeyRedef1;
    private DbsField cv_Prap_Rule1_Cv_Inst_Link_Cde;
    private DbsField cv_Prap_Rule1_Cv_Mail_Ind;
    private DbsField cv_Prap_Rule1_Cv_Last_Nme;
    private DbsField cv_Prap_Rule1_Cv_First_Nme;
    private DbsField cv_Prap_Rule1_Cv_Mddle_Nme;
    private DbsField cv_Prap_Rule1_Cv_Coll_Code;
    private DbsField cv_Prap_Rule1_Cv_Cor_Last_Nme;
    private DbsField cv_Prap_Rule1_Cv_Soc_Sec;
    private DbsField cv_Prap_Rule1_Cv_Lob;
    private DbsGroup cv_Prap_Rule1_Cv_Tiaa_Contr;
    private DbsField cv_Prap_Rule1_Cv_Pref;
    private DbsField cv_Prap_Rule1_Cv_Cont;
    private DbsGroup cv_Prap_Rule1_Cv_Cref_Contr;
    private DbsField cv_Prap_Rule1_Cv_C_Pref;
    private DbsField cv_Prap_Rule1_Cv_C_Cont;
    private DbsField cv_Prap_Rule1_Cv_Status;
    private DbsField cv_Prap_Rule1_Cv_Lob_Type;
    private DbsField cv_Prap_Rule1_Cv_Cor_First_Nme;
    private DbsField cv_Prap_Rule1_Cv_Cor_Mddle_Nme;
    private DbsField cv_Prap_Rule1_Cv_Pin_Nbr;
    private DbsField cv_Prap_Rule1_Cv_Dob;
    private DbsField cv_Prap_Rule1_Cv_Tiaa_Doi;
    private DbsField cv_Prap_Rule1_Cv_Cref_Doi;
    private DbsField cv_Prap_Rule1_Cv_Imit_Log;
    private DbsField cv_Prap_Rule1_Cv_Imit_Oprtr;
    private DbsField cv_Prap_Rule1_Cv_Imit_Unit;
    private DbsField cv_Prap_Rule1_Cv_Imit_Wpid;
    private DbsField cv_Prap_Rule1_Cv_Imit_Step;
    private DbsField cv_Prap_Rule1_Cv_Imit_Status;
    private DbsField cv_Prap_Rule1_Cv_Day_Diff;
    private DbsField cv_Prap_Rule1_Cv_Region_Code;
    private DbsGroup cv_Prap_Rule1_Cv_Region_CodeRedef2;
    private DbsField cv_Prap_Rule1_Cv_Region;
    private DbsField cv_Prap_Rule1_Cv_Branch;
    private DbsField cv_Prap_Rule1_Cv_Need;
    private DbsField cv_Prap_Rule1_Cv_Ppg_Team_Cde;
    private DbsField cv_Prap_Rule1_Cv_Addr_Index;
    private DbsField cv_Prap_Rule1_Cv_Sgrd_Plan_No;
    private DbsField cv_Prap_Rule1_Cv_Sgrd_Subplan_No;
    private DbsField cv_Prap_Rule1_Cv_Sgrd_Part_Ext;
    private DbsField cv_Prap_Rule1_Cv_Sgrd_Divsub;
    private DbsField cv_Prap_Rule1_Cv_Filler;
    private DbsGroup cv_Prap_Rule1Redef3;
    private DbsField cv_Prap_Rule1_Cv_Rule_Data_L;
    private DbsField cv_Prap_Rule1_Cv_Rule_Data_M;

    public DbsGroup getCv_Prap_Rule1() { return cv_Prap_Rule1; }

    public DbsField getCv_Prap_Rule1_Cv_Record_Type() { return cv_Prap_Rule1_Cv_Record_Type; }

    public DbsField getCv_Prap_Rule1_Cv_Key() { return cv_Prap_Rule1_Cv_Key; }

    public DbsGroup getCv_Prap_Rule1_Cv_KeyRedef1() { return cv_Prap_Rule1_Cv_KeyRedef1; }

    public DbsField getCv_Prap_Rule1_Cv_Inst_Link_Cde() { return cv_Prap_Rule1_Cv_Inst_Link_Cde; }

    public DbsField getCv_Prap_Rule1_Cv_Mail_Ind() { return cv_Prap_Rule1_Cv_Mail_Ind; }

    public DbsField getCv_Prap_Rule1_Cv_Last_Nme() { return cv_Prap_Rule1_Cv_Last_Nme; }

    public DbsField getCv_Prap_Rule1_Cv_First_Nme() { return cv_Prap_Rule1_Cv_First_Nme; }

    public DbsField getCv_Prap_Rule1_Cv_Mddle_Nme() { return cv_Prap_Rule1_Cv_Mddle_Nme; }

    public DbsField getCv_Prap_Rule1_Cv_Coll_Code() { return cv_Prap_Rule1_Cv_Coll_Code; }

    public DbsField getCv_Prap_Rule1_Cv_Cor_Last_Nme() { return cv_Prap_Rule1_Cv_Cor_Last_Nme; }

    public DbsField getCv_Prap_Rule1_Cv_Soc_Sec() { return cv_Prap_Rule1_Cv_Soc_Sec; }

    public DbsField getCv_Prap_Rule1_Cv_Lob() { return cv_Prap_Rule1_Cv_Lob; }

    public DbsGroup getCv_Prap_Rule1_Cv_Tiaa_Contr() { return cv_Prap_Rule1_Cv_Tiaa_Contr; }

    public DbsField getCv_Prap_Rule1_Cv_Pref() { return cv_Prap_Rule1_Cv_Pref; }

    public DbsField getCv_Prap_Rule1_Cv_Cont() { return cv_Prap_Rule1_Cv_Cont; }

    public DbsGroup getCv_Prap_Rule1_Cv_Cref_Contr() { return cv_Prap_Rule1_Cv_Cref_Contr; }

    public DbsField getCv_Prap_Rule1_Cv_C_Pref() { return cv_Prap_Rule1_Cv_C_Pref; }

    public DbsField getCv_Prap_Rule1_Cv_C_Cont() { return cv_Prap_Rule1_Cv_C_Cont; }

    public DbsField getCv_Prap_Rule1_Cv_Status() { return cv_Prap_Rule1_Cv_Status; }

    public DbsField getCv_Prap_Rule1_Cv_Lob_Type() { return cv_Prap_Rule1_Cv_Lob_Type; }

    public DbsField getCv_Prap_Rule1_Cv_Cor_First_Nme() { return cv_Prap_Rule1_Cv_Cor_First_Nme; }

    public DbsField getCv_Prap_Rule1_Cv_Cor_Mddle_Nme() { return cv_Prap_Rule1_Cv_Cor_Mddle_Nme; }

    public DbsField getCv_Prap_Rule1_Cv_Pin_Nbr() { return cv_Prap_Rule1_Cv_Pin_Nbr; }

    public DbsField getCv_Prap_Rule1_Cv_Dob() { return cv_Prap_Rule1_Cv_Dob; }

    public DbsField getCv_Prap_Rule1_Cv_Tiaa_Doi() { return cv_Prap_Rule1_Cv_Tiaa_Doi; }

    public DbsField getCv_Prap_Rule1_Cv_Cref_Doi() { return cv_Prap_Rule1_Cv_Cref_Doi; }

    public DbsField getCv_Prap_Rule1_Cv_Imit_Log() { return cv_Prap_Rule1_Cv_Imit_Log; }

    public DbsField getCv_Prap_Rule1_Cv_Imit_Oprtr() { return cv_Prap_Rule1_Cv_Imit_Oprtr; }

    public DbsField getCv_Prap_Rule1_Cv_Imit_Unit() { return cv_Prap_Rule1_Cv_Imit_Unit; }

    public DbsField getCv_Prap_Rule1_Cv_Imit_Wpid() { return cv_Prap_Rule1_Cv_Imit_Wpid; }

    public DbsField getCv_Prap_Rule1_Cv_Imit_Step() { return cv_Prap_Rule1_Cv_Imit_Step; }

    public DbsField getCv_Prap_Rule1_Cv_Imit_Status() { return cv_Prap_Rule1_Cv_Imit_Status; }

    public DbsField getCv_Prap_Rule1_Cv_Day_Diff() { return cv_Prap_Rule1_Cv_Day_Diff; }

    public DbsField getCv_Prap_Rule1_Cv_Region_Code() { return cv_Prap_Rule1_Cv_Region_Code; }

    public DbsGroup getCv_Prap_Rule1_Cv_Region_CodeRedef2() { return cv_Prap_Rule1_Cv_Region_CodeRedef2; }

    public DbsField getCv_Prap_Rule1_Cv_Region() { return cv_Prap_Rule1_Cv_Region; }

    public DbsField getCv_Prap_Rule1_Cv_Branch() { return cv_Prap_Rule1_Cv_Branch; }

    public DbsField getCv_Prap_Rule1_Cv_Need() { return cv_Prap_Rule1_Cv_Need; }

    public DbsField getCv_Prap_Rule1_Cv_Ppg_Team_Cde() { return cv_Prap_Rule1_Cv_Ppg_Team_Cde; }

    public DbsField getCv_Prap_Rule1_Cv_Addr_Index() { return cv_Prap_Rule1_Cv_Addr_Index; }

    public DbsField getCv_Prap_Rule1_Cv_Sgrd_Plan_No() { return cv_Prap_Rule1_Cv_Sgrd_Plan_No; }

    public DbsField getCv_Prap_Rule1_Cv_Sgrd_Subplan_No() { return cv_Prap_Rule1_Cv_Sgrd_Subplan_No; }

    public DbsField getCv_Prap_Rule1_Cv_Sgrd_Part_Ext() { return cv_Prap_Rule1_Cv_Sgrd_Part_Ext; }

    public DbsField getCv_Prap_Rule1_Cv_Sgrd_Divsub() { return cv_Prap_Rule1_Cv_Sgrd_Divsub; }

    public DbsField getCv_Prap_Rule1_Cv_Filler() { return cv_Prap_Rule1_Cv_Filler; }

    public DbsGroup getCv_Prap_Rule1Redef3() { return cv_Prap_Rule1Redef3; }

    public DbsField getCv_Prap_Rule1_Cv_Rule_Data_L() { return cv_Prap_Rule1_Cv_Rule_Data_L; }

    public DbsField getCv_Prap_Rule1_Cv_Rule_Data_M() { return cv_Prap_Rule1_Cv_Rule_Data_M; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cv_Prap_Rule1 = newGroupInRecord("cv_Prap_Rule1", "CV-PRAP-RULE1");
        cv_Prap_Rule1_Cv_Record_Type = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Record_Type", "CV-RECORD-TYPE", FieldType.NUMERIC, 1);
        cv_Prap_Rule1_Cv_Key = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Key", "CV-KEY", FieldType.STRING, 112);
        cv_Prap_Rule1_Cv_KeyRedef1 = cv_Prap_Rule1.newGroupInGroup("cv_Prap_Rule1_Cv_KeyRedef1", "Redefines", cv_Prap_Rule1_Cv_Key);
        cv_Prap_Rule1_Cv_Inst_Link_Cde = cv_Prap_Rule1_Cv_KeyRedef1.newFieldInGroup("cv_Prap_Rule1_Cv_Inst_Link_Cde", "CV-INST-LINK-CDE", FieldType.NUMERIC, 
            6);
        cv_Prap_Rule1_Cv_Mail_Ind = cv_Prap_Rule1_Cv_KeyRedef1.newFieldInGroup("cv_Prap_Rule1_Cv_Mail_Ind", "CV-MAIL-IND", FieldType.STRING, 1);
        cv_Prap_Rule1_Cv_Last_Nme = cv_Prap_Rule1_Cv_KeyRedef1.newFieldInGroup("cv_Prap_Rule1_Cv_Last_Nme", "CV-LAST-NME", FieldType.STRING, 35);
        cv_Prap_Rule1_Cv_First_Nme = cv_Prap_Rule1_Cv_KeyRedef1.newFieldInGroup("cv_Prap_Rule1_Cv_First_Nme", "CV-FIRST-NME", FieldType.STRING, 35);
        cv_Prap_Rule1_Cv_Mddle_Nme = cv_Prap_Rule1_Cv_KeyRedef1.newFieldInGroup("cv_Prap_Rule1_Cv_Mddle_Nme", "CV-MDDLE-NME", FieldType.STRING, 35);
        cv_Prap_Rule1_Cv_Coll_Code = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Coll_Code", "CV-COLL-CODE", FieldType.STRING, 6);
        cv_Prap_Rule1_Cv_Cor_Last_Nme = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Cor_Last_Nme", "CV-COR-LAST-NME", FieldType.STRING, 30);
        cv_Prap_Rule1_Cv_Soc_Sec = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Soc_Sec", "CV-SOC-SEC", FieldType.PACKED_DECIMAL, 9);
        cv_Prap_Rule1_Cv_Lob = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Lob", "CV-LOB", FieldType.STRING, 1);
        cv_Prap_Rule1_Cv_Tiaa_Contr = cv_Prap_Rule1.newGroupInGroup("cv_Prap_Rule1_Cv_Tiaa_Contr", "CV-TIAA-CONTR");
        cv_Prap_Rule1_Cv_Pref = cv_Prap_Rule1_Cv_Tiaa_Contr.newFieldInGroup("cv_Prap_Rule1_Cv_Pref", "CV-PREF", FieldType.STRING, 1);
        cv_Prap_Rule1_Cv_Cont = cv_Prap_Rule1_Cv_Tiaa_Contr.newFieldInGroup("cv_Prap_Rule1_Cv_Cont", "CV-CONT", FieldType.STRING, 7);
        cv_Prap_Rule1_Cv_Cref_Contr = cv_Prap_Rule1.newGroupInGroup("cv_Prap_Rule1_Cv_Cref_Contr", "CV-CREF-CONTR");
        cv_Prap_Rule1_Cv_C_Pref = cv_Prap_Rule1_Cv_Cref_Contr.newFieldInGroup("cv_Prap_Rule1_Cv_C_Pref", "CV-C-PREF", FieldType.STRING, 1);
        cv_Prap_Rule1_Cv_C_Cont = cv_Prap_Rule1_Cv_Cref_Contr.newFieldInGroup("cv_Prap_Rule1_Cv_C_Cont", "CV-C-CONT", FieldType.STRING, 7);
        cv_Prap_Rule1_Cv_Status = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Status", "CV-STATUS", FieldType.STRING, 1);
        cv_Prap_Rule1_Cv_Lob_Type = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Lob_Type", "CV-LOB-TYPE", FieldType.STRING, 1);
        cv_Prap_Rule1_Cv_Cor_First_Nme = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Cor_First_Nme", "CV-COR-FIRST-NME", FieldType.STRING, 30);
        cv_Prap_Rule1_Cv_Cor_Mddle_Nme = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Cor_Mddle_Nme", "CV-COR-MDDLE-NME", FieldType.STRING, 30);
        cv_Prap_Rule1_Cv_Pin_Nbr = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Pin_Nbr", "CV-PIN-NBR", FieldType.NUMERIC, 12);
        cv_Prap_Rule1_Cv_Dob = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Dob", "CV-DOB", FieldType.NUMERIC, 8);
        cv_Prap_Rule1_Cv_Tiaa_Doi = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Tiaa_Doi", "CV-TIAA-DOI", FieldType.DATE);
        cv_Prap_Rule1_Cv_Cref_Doi = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Cref_Doi", "CV-CREF-DOI", FieldType.DATE);
        cv_Prap_Rule1_Cv_Imit_Log = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Imit_Log", "CV-IMIT-LOG", FieldType.STRING, 15);
        cv_Prap_Rule1_Cv_Imit_Oprtr = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Imit_Oprtr", "CV-IMIT-OPRTR", FieldType.STRING, 8);
        cv_Prap_Rule1_Cv_Imit_Unit = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Imit_Unit", "CV-IMIT-UNIT", FieldType.STRING, 8);
        cv_Prap_Rule1_Cv_Imit_Wpid = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Imit_Wpid", "CV-IMIT-WPID", FieldType.STRING, 6);
        cv_Prap_Rule1_Cv_Imit_Step = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Imit_Step", "CV-IMIT-STEP", FieldType.STRING, 6);
        cv_Prap_Rule1_Cv_Imit_Status = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Imit_Status", "CV-IMIT-STATUS", FieldType.STRING, 4);
        cv_Prap_Rule1_Cv_Day_Diff = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Day_Diff", "CV-DAY-DIFF", FieldType.PACKED_DECIMAL, 6);
        cv_Prap_Rule1_Cv_Region_Code = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Region_Code", "CV-REGION-CODE", FieldType.STRING, 3);
        cv_Prap_Rule1_Cv_Region_CodeRedef2 = cv_Prap_Rule1.newGroupInGroup("cv_Prap_Rule1_Cv_Region_CodeRedef2", "Redefines", cv_Prap_Rule1_Cv_Region_Code);
        cv_Prap_Rule1_Cv_Region = cv_Prap_Rule1_Cv_Region_CodeRedef2.newFieldInGroup("cv_Prap_Rule1_Cv_Region", "CV-REGION", FieldType.STRING, 1);
        cv_Prap_Rule1_Cv_Branch = cv_Prap_Rule1_Cv_Region_CodeRedef2.newFieldInGroup("cv_Prap_Rule1_Cv_Branch", "CV-BRANCH", FieldType.STRING, 1);
        cv_Prap_Rule1_Cv_Need = cv_Prap_Rule1_Cv_Region_CodeRedef2.newFieldInGroup("cv_Prap_Rule1_Cv_Need", "CV-NEED", FieldType.STRING, 1);
        cv_Prap_Rule1_Cv_Ppg_Team_Cde = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Ppg_Team_Cde", "CV-PPG-TEAM-CDE", FieldType.STRING, 8);
        cv_Prap_Rule1_Cv_Addr_Index = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Addr_Index", "CV-ADDR-INDEX", FieldType.NUMERIC, 2);
        cv_Prap_Rule1_Cv_Sgrd_Plan_No = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Sgrd_Plan_No", "CV-SGRD-PLAN-NO", FieldType.STRING, 6);
        cv_Prap_Rule1_Cv_Sgrd_Subplan_No = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Sgrd_Subplan_No", "CV-SGRD-SUBPLAN-NO", FieldType.STRING, 6);
        cv_Prap_Rule1_Cv_Sgrd_Part_Ext = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Sgrd_Part_Ext", "CV-SGRD-PART-EXT", FieldType.STRING, 2);
        cv_Prap_Rule1_Cv_Sgrd_Divsub = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Sgrd_Divsub", "CV-SGRD-DIVSUB", FieldType.STRING, 4);
        cv_Prap_Rule1_Cv_Filler = cv_Prap_Rule1.newFieldInGroup("cv_Prap_Rule1_Cv_Filler", "CV-FILLER", FieldType.STRING, 12);
        cv_Prap_Rule1Redef3 = newGroupInRecord("cv_Prap_Rule1Redef3", "Redefines", cv_Prap_Rule1);
        cv_Prap_Rule1_Cv_Rule_Data_L = cv_Prap_Rule1Redef3.newFieldInGroup("cv_Prap_Rule1_Cv_Rule_Data_L", "CV-RULE-DATA-L", FieldType.STRING, 250);
        cv_Prap_Rule1_Cv_Rule_Data_M = cv_Prap_Rule1Redef3.newFieldInGroup("cv_Prap_Rule1_Cv_Rule_Data_M", "CV-RULE-DATA-M", FieldType.STRING, 100);

        this.setRecordName("LdaAppl302");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAppl302() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
