/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:19:46 PM
**        * FROM NATURAL PDA     : ECTA051
************************************************************
**        * FILE NAME            : PdaEcta051.java
**        * CLASS NAME           : PdaEcta051
**        * INSTANCE NAME        : PdaEcta051
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaEcta051 extends PdaBase
{
    // Properties
    private DbsGroup ecta051;
    private DbsField ecta051_Acct_Ind;
    private DbsField ecta051_Effct_Dte;
    private DbsGroup ecta051_Effct_DteRedef1;
    private DbsField ecta051_Effct_Dte_A;
    private DbsField ecta051_Message;
    private DbsField ecta051_Message_Cde;
    private DbsField ecta051_Status_Cde;
    private DbsField ecta051_Appl_Id;

    public DbsGroup getEcta051() { return ecta051; }

    public DbsField getEcta051_Acct_Ind() { return ecta051_Acct_Ind; }

    public DbsField getEcta051_Effct_Dte() { return ecta051_Effct_Dte; }

    public DbsGroup getEcta051_Effct_DteRedef1() { return ecta051_Effct_DteRedef1; }

    public DbsField getEcta051_Effct_Dte_A() { return ecta051_Effct_Dte_A; }

    public DbsField getEcta051_Message() { return ecta051_Message; }

    public DbsField getEcta051_Message_Cde() { return ecta051_Message_Cde; }

    public DbsField getEcta051_Status_Cde() { return ecta051_Status_Cde; }

    public DbsField getEcta051_Appl_Id() { return ecta051_Appl_Id; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        ecta051 = dbsRecord.newGroupArrayInRecord("ecta051", "ECTA051", new DbsArrayController(1,20));
        ecta051.setParameterOption(ParameterOption.ByReference);
        ecta051_Acct_Ind = ecta051.newFieldInGroup("ecta051_Acct_Ind", "ACCT-IND", FieldType.NUMERIC, 2);
        ecta051_Effct_Dte = ecta051.newFieldInGroup("ecta051_Effct_Dte", "EFFCT-DTE", FieldType.NUMERIC, 8);
        ecta051_Effct_DteRedef1 = ecta051.newGroupInGroup("ecta051_Effct_DteRedef1", "Redefines", ecta051_Effct_Dte);
        ecta051_Effct_Dte_A = ecta051_Effct_DteRedef1.newFieldInGroup("ecta051_Effct_Dte_A", "EFFCT-DTE-A", FieldType.STRING, 8);
        ecta051_Message = ecta051.newFieldInGroup("ecta051_Message", "MESSAGE", FieldType.STRING, 30);
        ecta051_Message_Cde = ecta051.newFieldInGroup("ecta051_Message_Cde", "MESSAGE-CDE", FieldType.STRING, 1);
        ecta051_Status_Cde = ecta051.newFieldInGroup("ecta051_Status_Cde", "STATUS-CDE", FieldType.STRING, 1);
        ecta051_Appl_Id = ecta051.newFieldArrayInGroup("ecta051_Appl_Id", "APPL-ID", FieldType.NUMERIC, 2, new DbsArrayController(1,10));

        dbsRecord.reset();
    }

    // Constructors
    public PdaEcta051(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

