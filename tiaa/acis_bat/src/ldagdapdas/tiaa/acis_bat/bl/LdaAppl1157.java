/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:52 PM
**        * FROM NATURAL LDA     : APPL1157
************************************************************
**        * FILE NAME            : LdaAppl1157.java
**        * CLASS NAME           : LdaAppl1157
**        * INSTANCE NAME        : LdaAppl1157
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl1157 extends DbsRecord
{
    // Properties
    private DbsField doc_Ext_File_2;
    private DbsGroup doc_Ext_File_2Redef1;
    private DbsField doc_Ext_File_2_Dx2_Type_Cd;
    private DbsField doc_Ext_File_2_Dx2_Contract_Table;
    private DbsGroup doc_Ext_File_2_Dx2_Contract_TableRedef2;
    private DbsGroup doc_Ext_File_2_Dx2_Contract_Numbers;
    private DbsField doc_Ext_File_2_Dx2_Tiaa_Prfx;
    private DbsField doc_Ext_File_2_Dx2_Tiaa_No;
    private DbsField doc_Ext_File_2_Dx2_Cref_Prfx;
    private DbsField doc_Ext_File_2_Dx2_Cref_No;
    private DbsField doc_Ext_File_2_Dx2_Date_Table;
    private DbsField doc_Ext_File_2_Dx2_Age_First_Pymt;
    private DbsField doc_Ext_File_2_Dx2_Ownership;
    private DbsField doc_Ext_File_2_Dx2_Alloc_Disc;
    private DbsField doc_Ext_File_2_Dx2_Currency;
    private DbsField doc_Ext_File_2_Dx2_Tiaa_Form_No;
    private DbsField doc_Ext_File_2_Dx2_Cref_Form_No;
    private DbsField doc_Ext_File_2_Dx2_Tiaa_Ed_No;
    private DbsField doc_Ext_File_2_Dx2_Cref_Ed_No;
    private DbsField doc_Ext_File_2_Dx2_Annuitant_Name;
    private DbsGroup doc_Ext_File_2_Dx2_Eop_Name;
    private DbsField doc_Ext_File_2_Dx2_Eop_Last_Name;
    private DbsField doc_Ext_File_2_Dx2_Eop_First_Name;
    private DbsField doc_Ext_File_2_Dx2_Eop_Middle_Name;
    private DbsGroup doc_Ext_File_2_Dx2_Eop_Middle_NameRedef3;
    private DbsField doc_Ext_File_2_Dx2_Eop_Mid_Init;
    private DbsField doc_Ext_File_2_Dx2_Eop_Mid_Filler;
    private DbsField doc_Ext_File_2_Dx2_Eop_Title;
    private DbsField doc_Ext_File_2_Dx2_Eop_Prefix;
    private DbsField doc_Ext_File_2_Dx2_Soc_Sec;
    private DbsField doc_Ext_File_2_Dx2_Pin_No;
    private DbsField doc_Ext_File_2_Dx2_Address_Table;
    private DbsField doc_Ext_File_2_Dx2_Eop_Zipcode;
    private DbsField doc_Ext_File_2_Dx2_Participant_Status;
    private DbsField doc_Ext_File_2_Dx2_Email_Address;
    private DbsField doc_Ext_File_2_Dx2_Request_Package_Type;
    private DbsField doc_Ext_File_2_Dx2_Portfolio_Selected;
    private DbsField doc_Ext_File_2_Dx2_Divorce_Contract;
    private DbsField doc_Ext_File_2_Dx2_Access_Cd_Ind;
    private DbsField doc_Ext_File_2_Dx2_Address_Change;
    private DbsField doc_Ext_File_2_Dx2_Package_Version;
    private DbsField doc_Ext_File_2_Dx2_Unit_Name;
    private DbsField doc_Ext_File_2_Dx2_Print_Package_Type;
    private DbsField doc_Ext_File_2_Dx2_Future_Filler;
    private DbsField doc_Ext_File_2_Dx2_Bene_Estate;
    private DbsField doc_Ext_File_2_Dx2_Bene_Trust;
    private DbsField doc_Ext_File_2_Dx2_Bene_Category;
    private DbsField doc_Ext_File_2_Dx2_Sex;
    private DbsField doc_Ext_File_2_Dx2_Inst_Name_Table;
    private DbsField doc_Ext_File_2_Dx2_Inst_Name_76;
    private DbsField doc_Ext_File_2_Dx2_Inst_Address_Table;
    private DbsField doc_Ext_File_2_Dx2_Inst_Zip_Cd;
    private DbsField doc_Ext_File_2_Dx2_Mail_Instructions;
    private DbsField doc_Ext_File_2_Dx2_Orig_Iss_State;
    private DbsField doc_Ext_File_2_Dx2_Current_Iss_State;
    private DbsField doc_Ext_File_2_Dx2_Eop_State_Name;
    private DbsField doc_Ext_File_2_Dx2_Lob;
    private DbsField doc_Ext_File_2_Dx2_Lob_Type;
    private DbsField doc_Ext_File_2_Dx2_Inst_Code;
    private DbsField doc_Ext_File_2_Dx2_Inst_Bill_Cd;
    private DbsField doc_Ext_File_2_Dx2_Inst_Permit_Trans;
    private DbsField doc_Ext_File_2_Dx2_Perm_Gra_Trans;
    private DbsField doc_Ext_File_2_Dx2_Inst_Full_Immed_Vest;
    private DbsField doc_Ext_File_2_Dx2_Inst_Cashable_Ra;
    private DbsField doc_Ext_File_2_Dx2_Inst_Cashable_Gra;
    private DbsField doc_Ext_File_2_Dx2_Inst_Fixed_Per_Opt;
    private DbsField doc_Ext_File_2_Dx2_Inst_Cntrl_Consent;
    private DbsField doc_Ext_File_2_Dx2_Eop;
    private DbsField doc_Ext_File_2_Dx2_Gsra_Inst_St_Code;
    private DbsField doc_Ext_File_2_Dx2_Region_Cd;
    private DbsField doc_Ext_File_2_Dx2_Staff_Id;
    private DbsField doc_Ext_File_2_Dx2_Appl_Source;
    private DbsField doc_Ext_File_2_Dx2_Eop_Appl_Status;
    private DbsField doc_Ext_File_2_Dx2_Eop_Addl_Cref_Req;
    private DbsField doc_Ext_File_2_Dx2_Extract_Date;
    private DbsField doc_Ext_File_2_Dx2_Gsra_Loan_Ind;
    private DbsField doc_Ext_File_2_Dx2_Spec_Ppg_Ind;
    private DbsField doc_Ext_File_2_Dx2_Mail_Pull_Cd;
    private DbsField doc_Ext_File_2_Dx2_Mit_Log_Dt_Tm;
    private DbsField doc_Ext_File_2_Dx2_Addr_Page_Name;
    private DbsGroup doc_Ext_File_2_Dx2_Addr_Page_NameRedef4;
    private DbsField doc_Ext_File_2_Dx2_Addr_Pref;
    private DbsField doc_Ext_File_2_Dx2_Addr_Filler_1;
    private DbsField doc_Ext_File_2_Dx2_Addr_Frst;
    private DbsField doc_Ext_File_2_Dx2_Addr_Filler_2;
    private DbsField doc_Ext_File_2_Dx2_Addr_Mddle;
    private DbsField doc_Ext_File_2_Dx2_Addr_Filler_3;
    private DbsField doc_Ext_File_2_Dx2_Addr_Last;
    private DbsField doc_Ext_File_2_Dx2_Addr_Filler_4;
    private DbsField doc_Ext_File_2_Dx2_Addr_Sffx;
    private DbsField doc_Ext_File_2_Dx2_Welcome_Name;
    private DbsGroup doc_Ext_File_2_Dx2_Welcome_NameRedef5;
    private DbsField doc_Ext_File_2_Dx2_Welcome_Pref;
    private DbsField doc_Ext_File_2_Dx2_Welcome_Filler_1;
    private DbsField doc_Ext_File_2_Dx2_Welcome_Last;
    private DbsField doc_Ext_File_2_Dx2_Pin_Name;
    private DbsGroup doc_Ext_File_2_Dx2_Pin_NameRedef6;
    private DbsField doc_Ext_File_2_Dx2_Pin_First;
    private DbsField doc_Ext_File_2_Dx2_Pin_Filler_1;
    private DbsField doc_Ext_File_2_Dx2_Pin_Mddle;
    private DbsField doc_Ext_File_2_Dx2_Pin_Filler_2;
    private DbsField doc_Ext_File_2_Dx2_Pin_Last;
    private DbsField doc_Ext_File_2_Dx2_Zip;
    private DbsGroup doc_Ext_File_2_Dx2_ZipRedef7;
    private DbsField doc_Ext_File_2_Dx2_Zip_Plus4;
    private DbsField doc_Ext_File_2_Dx2_Zip_Plus1;
    private DbsField doc_Ext_File_2_Dx2_Suspension;
    private DbsField doc_Ext_File_2_Dx2_Mult_App_Version;
    private DbsField doc_Ext_File_2_Dx2_Mult_App_Status;
    private DbsField doc_Ext_File_2_Dx2_Mult_App_Lob;
    private DbsField doc_Ext_File_2_Dx2_Mult_App_Lob_Type;
    private DbsField doc_Ext_File_2_Dx2_Mult_App_Ppg;
    private DbsField doc_Ext_File_2_Dx2_K12_Ppg;
    private DbsField doc_Ext_File_2_Dx2_Ira_Rollover_Type;
    private DbsField doc_Ext_File_2_Dx2_Ira_Record_Type;
    private DbsField doc_Ext_File_2_Dx2_Rollover_Amt;
    private DbsField doc_Ext_File_2_Dx2_Mit_Unit;
    private DbsField doc_Ext_File_2_Dx2_Mit_Wpid;
    private DbsField doc_Ext_File_2_Dx2_New_Bene_Ind;
    private DbsField doc_Ext_File_2_Dx2_Irc_Sectn_Cde;
    private DbsField doc_Ext_File_2_Dx2_Released_Dt;
    private DbsField doc_Ext_File_2_Dx2_Irc_Sectn_Grp;
    private DbsField doc_Ext_File_2_Dx2_Enroll_Request_Type;
    private DbsField doc_Ext_File_2_Dx2_Erisa_Inst;
    private DbsField doc_Ext_File_2_Dx2_Reprint_Request_Type;
    private DbsField doc_Ext_File_2_Dx2_Print_Destination;
    private DbsField doc_Ext_File_2_Dx2_Correction_Type;
    private DbsField doc_Ext_File_2_Dx2_Processor_Name;
    private DbsField doc_Ext_File_2_Dx2_Mail_Date;
    private DbsField doc_Ext_File_2_Dx2_Alloc_Fmt;
    private DbsField doc_Ext_File_2_Dx2_Phone_No;
    private DbsField doc_Ext_File_2_Dx2_Fund_Cde;
    private DbsField doc_Ext_File_2_Dx2_Alloc_Pct;
    private DbsField doc_Ext_File_2_Dx2_Oia_Ind;
    private DbsField doc_Ext_File_2_Dx2_Product_Cde;
    private DbsField doc_Ext_File_2_Dx2_Acct_Sum_Sheet_Type;
    private DbsField doc_Ext_File_2_Dx2_Sg_Plan_No;
    private DbsField doc_Ext_File_2_Dx2_Sg_Subplan_No;
    private DbsField doc_Ext_File_2_Dx2_Sg_Subplan_Type;
    private DbsField doc_Ext_File_2_Dx2_Omni_Issue_Ind;
    private DbsField doc_Ext_File_2_Dx2_Effective_Date;
    private DbsField doc_Ext_File_2_Dx2_Employer_Name;
    private DbsField doc_Ext_File_2_Dx2_Tiaa_Rate;
    private DbsField doc_Ext_File_2_Dx2_Sg_Plan_Id;
    private DbsField doc_Ext_File_2_Dx2_Tiaa_Pg4_Numb;
    private DbsField doc_Ext_File_2_Dx2_Cref_Pg4_Numb;
    private DbsField doc_Ext_File_2_Dx2_Dflt_Access_Code;
    private DbsField doc_Ext_File_2_Dx2_Single_Issue_Ind;
    private DbsField doc_Ext_File_2_Dx2_Spec_Fund_Ind;
    private DbsField doc_Ext_File_2_Dx2_Product_Price_Level;
    private DbsField doc_Ext_File_2_Dx2_Roth_Ind;
    private DbsField doc_Ext_File_2_Dx2_Plan_Issue_State;
    private DbsField doc_Ext_File_2_Dx2_Sgrd_Client_Id;
    private DbsField doc_Ext_File_2_Dx2_Portfolio_Type;
    private DbsField doc_Ext_File_2_Dx2_Auto_Enroll_Ind;
    private DbsField doc_Ext_File_2_Dx2_Auto_Save_Ind;
    private DbsField doc_Ext_File_2_Dx2_As_Cur_Dflt_Amt;
    private DbsField doc_Ext_File_2_Dx2_As_Incr_Amt;
    private DbsField doc_Ext_File_2_Dx2_As_Max_Pct;
    private DbsField doc_Ext_File_2_Dx2_Ae_Opt_Out_Days;
    private DbsField doc_Ext_File_2_Dx2_Tsv_Ind;
    private DbsField doc_Ext_File_2_Dx2_Tiaa_Indx_Guarntd_Rte;
    private DbsField doc_Ext_File_2_Dx2_Agent_Crd_No;
    private DbsField doc_Ext_File_2_Dx2_Crd_Pull_Ind;
    private DbsField doc_Ext_File_2_Dx2_Agent_Name;
    private DbsField doc_Ext_File_2_Dx2_Agent_Work_Addr1;
    private DbsField doc_Ext_File_2_Dx2_Agent_Work_Addr2;
    private DbsField doc_Ext_File_2_Dx2_Agent_Work_City;
    private DbsField doc_Ext_File_2_Dx2_Agent_Work_State;
    private DbsField doc_Ext_File_2_Dx2_Agent_Work_Zip;
    private DbsField doc_Ext_File_2_Dx2_Agent_Work_Phone;
    private DbsField doc_Ext_File_2_Dx2_Tic_Ind;
    private DbsField doc_Ext_File_2_Dx2_Tic_Startdate;
    private DbsField doc_Ext_File_2_Dx2_Tic_Enddate;
    private DbsField doc_Ext_File_2_Dx2_Tic_Percentage;
    private DbsField doc_Ext_File_2_Dx2_Tic_Postdays;
    private DbsField doc_Ext_File_2_Dx2_Tic_Limit;
    private DbsField doc_Ext_File_2_Dx2_Tic_Postfreq;
    private DbsField doc_Ext_File_2_Dx2_Tic_Pl_Level;
    private DbsField doc_Ext_File_2_Dx2_Tic_Windowdays;
    private DbsField doc_Ext_File_2_Dx2_Tic_Reqdlywindow;
    private DbsField doc_Ext_File_2_Dx2_Tic_Recap_Prov;
    private DbsField doc_Ext_File_2_Dx2_Ecs_Dcs_E_Dlvry_Ind;
    private DbsField doc_Ext_File_2_Dx2_Ecs_Dcs_Annty_Optn_Ind;
    private DbsField doc_Ext_File_2_Dx2_Full_Middle_Name;
    private DbsField doc_Ext_File_2_Dx2_Substitution_Contract_Ind;
    private DbsField doc_Ext_File_2_Dx2_Non_Proprietary_Pgk_Ind;
    private DbsField doc_Ext_File_2_Dx2_Multi_Plan_Table;
    private DbsGroup doc_Ext_File_2_Dx2_Multi_Plan_TableRedef8;
    private DbsGroup doc_Ext_File_2_Dx2_Multi_Plan_Info;
    private DbsField doc_Ext_File_2_Dx2_Multi_Plan_No;
    private DbsField doc_Ext_File_2_Dx2_Multi_Sub_Plan;
    private DbsField doc_Ext_File_2_Dx2_Filler;
    private DbsField doc_Ext_File_2_Dx2_Tiaa_Init_Prem;
    private DbsField doc_Ext_File_2_Dx2_Cref_Init_Prem;
    private DbsField doc_Ext_File_2_Dx2_Related_Contract_Info;
    private DbsGroup doc_Ext_File_2_Dx2_Related_Contract_InfoRedef9;
    private DbsGroup doc_Ext_File_2_Dx2_Related_Contract;
    private DbsField doc_Ext_File_2_Dx2_Related_Contract_Type;
    private DbsField doc_Ext_File_2_Dx2_Related_Tiaa_No;
    private DbsField doc_Ext_File_2_Dx2_Related_Tiaa_Issue_Date;
    private DbsField doc_Ext_File_2_Dx2_Related_First_Payment_Date;
    private DbsField doc_Ext_File_2_Dx2_Related_Tiaa_Total_Amt;
    private DbsField doc_Ext_File_2_Dx2_Related_Last_Payment_Date;
    private DbsField doc_Ext_File_2_Dx2_Related_Payment_Frequency;
    private DbsField doc_Ext_File_2_Dx2_Related_Tiaa_Issue_State;
    private DbsField doc_Ext_File_2_Dx2_First_Tpa_Ipro_Ind;
    private DbsField doc_Ext_File_2_Dx2_Fund_Source_1;
    private DbsField doc_Ext_File_2_Dx2_Fund_Source_2;
    private DbsField doc_Ext_File_2_Dx2_Fund_Cde_2;
    private DbsField doc_Ext_File_2_Dx2_Alloc_Pct_2;
    private DbsGroup doc_Ext_File_2Redef10;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_1;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_2;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_3;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_4;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_5;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_6;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_7;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_8;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_9;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_10;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_11;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_12;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_13;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_14;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_15;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_16;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_17;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_18;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_19;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_20;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_21;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_22;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_23;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_24;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_25;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_26;
    private DbsField doc_Ext_File_2_Doc_Ext2_Data_27;

    public DbsField getDoc_Ext_File_2() { return doc_Ext_File_2; }

    public DbsGroup getDoc_Ext_File_2Redef1() { return doc_Ext_File_2Redef1; }

    public DbsField getDoc_Ext_File_2_Dx2_Type_Cd() { return doc_Ext_File_2_Dx2_Type_Cd; }

    public DbsField getDoc_Ext_File_2_Dx2_Contract_Table() { return doc_Ext_File_2_Dx2_Contract_Table; }

    public DbsGroup getDoc_Ext_File_2_Dx2_Contract_TableRedef2() { return doc_Ext_File_2_Dx2_Contract_TableRedef2; }

    public DbsGroup getDoc_Ext_File_2_Dx2_Contract_Numbers() { return doc_Ext_File_2_Dx2_Contract_Numbers; }

    public DbsField getDoc_Ext_File_2_Dx2_Tiaa_Prfx() { return doc_Ext_File_2_Dx2_Tiaa_Prfx; }

    public DbsField getDoc_Ext_File_2_Dx2_Tiaa_No() { return doc_Ext_File_2_Dx2_Tiaa_No; }

    public DbsField getDoc_Ext_File_2_Dx2_Cref_Prfx() { return doc_Ext_File_2_Dx2_Cref_Prfx; }

    public DbsField getDoc_Ext_File_2_Dx2_Cref_No() { return doc_Ext_File_2_Dx2_Cref_No; }

    public DbsField getDoc_Ext_File_2_Dx2_Date_Table() { return doc_Ext_File_2_Dx2_Date_Table; }

    public DbsField getDoc_Ext_File_2_Dx2_Age_First_Pymt() { return doc_Ext_File_2_Dx2_Age_First_Pymt; }

    public DbsField getDoc_Ext_File_2_Dx2_Ownership() { return doc_Ext_File_2_Dx2_Ownership; }

    public DbsField getDoc_Ext_File_2_Dx2_Alloc_Disc() { return doc_Ext_File_2_Dx2_Alloc_Disc; }

    public DbsField getDoc_Ext_File_2_Dx2_Currency() { return doc_Ext_File_2_Dx2_Currency; }

    public DbsField getDoc_Ext_File_2_Dx2_Tiaa_Form_No() { return doc_Ext_File_2_Dx2_Tiaa_Form_No; }

    public DbsField getDoc_Ext_File_2_Dx2_Cref_Form_No() { return doc_Ext_File_2_Dx2_Cref_Form_No; }

    public DbsField getDoc_Ext_File_2_Dx2_Tiaa_Ed_No() { return doc_Ext_File_2_Dx2_Tiaa_Ed_No; }

    public DbsField getDoc_Ext_File_2_Dx2_Cref_Ed_No() { return doc_Ext_File_2_Dx2_Cref_Ed_No; }

    public DbsField getDoc_Ext_File_2_Dx2_Annuitant_Name() { return doc_Ext_File_2_Dx2_Annuitant_Name; }

    public DbsGroup getDoc_Ext_File_2_Dx2_Eop_Name() { return doc_Ext_File_2_Dx2_Eop_Name; }

    public DbsField getDoc_Ext_File_2_Dx2_Eop_Last_Name() { return doc_Ext_File_2_Dx2_Eop_Last_Name; }

    public DbsField getDoc_Ext_File_2_Dx2_Eop_First_Name() { return doc_Ext_File_2_Dx2_Eop_First_Name; }

    public DbsField getDoc_Ext_File_2_Dx2_Eop_Middle_Name() { return doc_Ext_File_2_Dx2_Eop_Middle_Name; }

    public DbsGroup getDoc_Ext_File_2_Dx2_Eop_Middle_NameRedef3() { return doc_Ext_File_2_Dx2_Eop_Middle_NameRedef3; }

    public DbsField getDoc_Ext_File_2_Dx2_Eop_Mid_Init() { return doc_Ext_File_2_Dx2_Eop_Mid_Init; }

    public DbsField getDoc_Ext_File_2_Dx2_Eop_Mid_Filler() { return doc_Ext_File_2_Dx2_Eop_Mid_Filler; }

    public DbsField getDoc_Ext_File_2_Dx2_Eop_Title() { return doc_Ext_File_2_Dx2_Eop_Title; }

    public DbsField getDoc_Ext_File_2_Dx2_Eop_Prefix() { return doc_Ext_File_2_Dx2_Eop_Prefix; }

    public DbsField getDoc_Ext_File_2_Dx2_Soc_Sec() { return doc_Ext_File_2_Dx2_Soc_Sec; }

    public DbsField getDoc_Ext_File_2_Dx2_Pin_No() { return doc_Ext_File_2_Dx2_Pin_No; }

    public DbsField getDoc_Ext_File_2_Dx2_Address_Table() { return doc_Ext_File_2_Dx2_Address_Table; }

    public DbsField getDoc_Ext_File_2_Dx2_Eop_Zipcode() { return doc_Ext_File_2_Dx2_Eop_Zipcode; }

    public DbsField getDoc_Ext_File_2_Dx2_Participant_Status() { return doc_Ext_File_2_Dx2_Participant_Status; }

    public DbsField getDoc_Ext_File_2_Dx2_Email_Address() { return doc_Ext_File_2_Dx2_Email_Address; }

    public DbsField getDoc_Ext_File_2_Dx2_Request_Package_Type() { return doc_Ext_File_2_Dx2_Request_Package_Type; }

    public DbsField getDoc_Ext_File_2_Dx2_Portfolio_Selected() { return doc_Ext_File_2_Dx2_Portfolio_Selected; }

    public DbsField getDoc_Ext_File_2_Dx2_Divorce_Contract() { return doc_Ext_File_2_Dx2_Divorce_Contract; }

    public DbsField getDoc_Ext_File_2_Dx2_Access_Cd_Ind() { return doc_Ext_File_2_Dx2_Access_Cd_Ind; }

    public DbsField getDoc_Ext_File_2_Dx2_Address_Change() { return doc_Ext_File_2_Dx2_Address_Change; }

    public DbsField getDoc_Ext_File_2_Dx2_Package_Version() { return doc_Ext_File_2_Dx2_Package_Version; }

    public DbsField getDoc_Ext_File_2_Dx2_Unit_Name() { return doc_Ext_File_2_Dx2_Unit_Name; }

    public DbsField getDoc_Ext_File_2_Dx2_Print_Package_Type() { return doc_Ext_File_2_Dx2_Print_Package_Type; }

    public DbsField getDoc_Ext_File_2_Dx2_Future_Filler() { return doc_Ext_File_2_Dx2_Future_Filler; }

    public DbsField getDoc_Ext_File_2_Dx2_Bene_Estate() { return doc_Ext_File_2_Dx2_Bene_Estate; }

    public DbsField getDoc_Ext_File_2_Dx2_Bene_Trust() { return doc_Ext_File_2_Dx2_Bene_Trust; }

    public DbsField getDoc_Ext_File_2_Dx2_Bene_Category() { return doc_Ext_File_2_Dx2_Bene_Category; }

    public DbsField getDoc_Ext_File_2_Dx2_Sex() { return doc_Ext_File_2_Dx2_Sex; }

    public DbsField getDoc_Ext_File_2_Dx2_Inst_Name_Table() { return doc_Ext_File_2_Dx2_Inst_Name_Table; }

    public DbsField getDoc_Ext_File_2_Dx2_Inst_Name_76() { return doc_Ext_File_2_Dx2_Inst_Name_76; }

    public DbsField getDoc_Ext_File_2_Dx2_Inst_Address_Table() { return doc_Ext_File_2_Dx2_Inst_Address_Table; }

    public DbsField getDoc_Ext_File_2_Dx2_Inst_Zip_Cd() { return doc_Ext_File_2_Dx2_Inst_Zip_Cd; }

    public DbsField getDoc_Ext_File_2_Dx2_Mail_Instructions() { return doc_Ext_File_2_Dx2_Mail_Instructions; }

    public DbsField getDoc_Ext_File_2_Dx2_Orig_Iss_State() { return doc_Ext_File_2_Dx2_Orig_Iss_State; }

    public DbsField getDoc_Ext_File_2_Dx2_Current_Iss_State() { return doc_Ext_File_2_Dx2_Current_Iss_State; }

    public DbsField getDoc_Ext_File_2_Dx2_Eop_State_Name() { return doc_Ext_File_2_Dx2_Eop_State_Name; }

    public DbsField getDoc_Ext_File_2_Dx2_Lob() { return doc_Ext_File_2_Dx2_Lob; }

    public DbsField getDoc_Ext_File_2_Dx2_Lob_Type() { return doc_Ext_File_2_Dx2_Lob_Type; }

    public DbsField getDoc_Ext_File_2_Dx2_Inst_Code() { return doc_Ext_File_2_Dx2_Inst_Code; }

    public DbsField getDoc_Ext_File_2_Dx2_Inst_Bill_Cd() { return doc_Ext_File_2_Dx2_Inst_Bill_Cd; }

    public DbsField getDoc_Ext_File_2_Dx2_Inst_Permit_Trans() { return doc_Ext_File_2_Dx2_Inst_Permit_Trans; }

    public DbsField getDoc_Ext_File_2_Dx2_Perm_Gra_Trans() { return doc_Ext_File_2_Dx2_Perm_Gra_Trans; }

    public DbsField getDoc_Ext_File_2_Dx2_Inst_Full_Immed_Vest() { return doc_Ext_File_2_Dx2_Inst_Full_Immed_Vest; }

    public DbsField getDoc_Ext_File_2_Dx2_Inst_Cashable_Ra() { return doc_Ext_File_2_Dx2_Inst_Cashable_Ra; }

    public DbsField getDoc_Ext_File_2_Dx2_Inst_Cashable_Gra() { return doc_Ext_File_2_Dx2_Inst_Cashable_Gra; }

    public DbsField getDoc_Ext_File_2_Dx2_Inst_Fixed_Per_Opt() { return doc_Ext_File_2_Dx2_Inst_Fixed_Per_Opt; }

    public DbsField getDoc_Ext_File_2_Dx2_Inst_Cntrl_Consent() { return doc_Ext_File_2_Dx2_Inst_Cntrl_Consent; }

    public DbsField getDoc_Ext_File_2_Dx2_Eop() { return doc_Ext_File_2_Dx2_Eop; }

    public DbsField getDoc_Ext_File_2_Dx2_Gsra_Inst_St_Code() { return doc_Ext_File_2_Dx2_Gsra_Inst_St_Code; }

    public DbsField getDoc_Ext_File_2_Dx2_Region_Cd() { return doc_Ext_File_2_Dx2_Region_Cd; }

    public DbsField getDoc_Ext_File_2_Dx2_Staff_Id() { return doc_Ext_File_2_Dx2_Staff_Id; }

    public DbsField getDoc_Ext_File_2_Dx2_Appl_Source() { return doc_Ext_File_2_Dx2_Appl_Source; }

    public DbsField getDoc_Ext_File_2_Dx2_Eop_Appl_Status() { return doc_Ext_File_2_Dx2_Eop_Appl_Status; }

    public DbsField getDoc_Ext_File_2_Dx2_Eop_Addl_Cref_Req() { return doc_Ext_File_2_Dx2_Eop_Addl_Cref_Req; }

    public DbsField getDoc_Ext_File_2_Dx2_Extract_Date() { return doc_Ext_File_2_Dx2_Extract_Date; }

    public DbsField getDoc_Ext_File_2_Dx2_Gsra_Loan_Ind() { return doc_Ext_File_2_Dx2_Gsra_Loan_Ind; }

    public DbsField getDoc_Ext_File_2_Dx2_Spec_Ppg_Ind() { return doc_Ext_File_2_Dx2_Spec_Ppg_Ind; }

    public DbsField getDoc_Ext_File_2_Dx2_Mail_Pull_Cd() { return doc_Ext_File_2_Dx2_Mail_Pull_Cd; }

    public DbsField getDoc_Ext_File_2_Dx2_Mit_Log_Dt_Tm() { return doc_Ext_File_2_Dx2_Mit_Log_Dt_Tm; }

    public DbsField getDoc_Ext_File_2_Dx2_Addr_Page_Name() { return doc_Ext_File_2_Dx2_Addr_Page_Name; }

    public DbsGroup getDoc_Ext_File_2_Dx2_Addr_Page_NameRedef4() { return doc_Ext_File_2_Dx2_Addr_Page_NameRedef4; }

    public DbsField getDoc_Ext_File_2_Dx2_Addr_Pref() { return doc_Ext_File_2_Dx2_Addr_Pref; }

    public DbsField getDoc_Ext_File_2_Dx2_Addr_Filler_1() { return doc_Ext_File_2_Dx2_Addr_Filler_1; }

    public DbsField getDoc_Ext_File_2_Dx2_Addr_Frst() { return doc_Ext_File_2_Dx2_Addr_Frst; }

    public DbsField getDoc_Ext_File_2_Dx2_Addr_Filler_2() { return doc_Ext_File_2_Dx2_Addr_Filler_2; }

    public DbsField getDoc_Ext_File_2_Dx2_Addr_Mddle() { return doc_Ext_File_2_Dx2_Addr_Mddle; }

    public DbsField getDoc_Ext_File_2_Dx2_Addr_Filler_3() { return doc_Ext_File_2_Dx2_Addr_Filler_3; }

    public DbsField getDoc_Ext_File_2_Dx2_Addr_Last() { return doc_Ext_File_2_Dx2_Addr_Last; }

    public DbsField getDoc_Ext_File_2_Dx2_Addr_Filler_4() { return doc_Ext_File_2_Dx2_Addr_Filler_4; }

    public DbsField getDoc_Ext_File_2_Dx2_Addr_Sffx() { return doc_Ext_File_2_Dx2_Addr_Sffx; }

    public DbsField getDoc_Ext_File_2_Dx2_Welcome_Name() { return doc_Ext_File_2_Dx2_Welcome_Name; }

    public DbsGroup getDoc_Ext_File_2_Dx2_Welcome_NameRedef5() { return doc_Ext_File_2_Dx2_Welcome_NameRedef5; }

    public DbsField getDoc_Ext_File_2_Dx2_Welcome_Pref() { return doc_Ext_File_2_Dx2_Welcome_Pref; }

    public DbsField getDoc_Ext_File_2_Dx2_Welcome_Filler_1() { return doc_Ext_File_2_Dx2_Welcome_Filler_1; }

    public DbsField getDoc_Ext_File_2_Dx2_Welcome_Last() { return doc_Ext_File_2_Dx2_Welcome_Last; }

    public DbsField getDoc_Ext_File_2_Dx2_Pin_Name() { return doc_Ext_File_2_Dx2_Pin_Name; }

    public DbsGroup getDoc_Ext_File_2_Dx2_Pin_NameRedef6() { return doc_Ext_File_2_Dx2_Pin_NameRedef6; }

    public DbsField getDoc_Ext_File_2_Dx2_Pin_First() { return doc_Ext_File_2_Dx2_Pin_First; }

    public DbsField getDoc_Ext_File_2_Dx2_Pin_Filler_1() { return doc_Ext_File_2_Dx2_Pin_Filler_1; }

    public DbsField getDoc_Ext_File_2_Dx2_Pin_Mddle() { return doc_Ext_File_2_Dx2_Pin_Mddle; }

    public DbsField getDoc_Ext_File_2_Dx2_Pin_Filler_2() { return doc_Ext_File_2_Dx2_Pin_Filler_2; }

    public DbsField getDoc_Ext_File_2_Dx2_Pin_Last() { return doc_Ext_File_2_Dx2_Pin_Last; }

    public DbsField getDoc_Ext_File_2_Dx2_Zip() { return doc_Ext_File_2_Dx2_Zip; }

    public DbsGroup getDoc_Ext_File_2_Dx2_ZipRedef7() { return doc_Ext_File_2_Dx2_ZipRedef7; }

    public DbsField getDoc_Ext_File_2_Dx2_Zip_Plus4() { return doc_Ext_File_2_Dx2_Zip_Plus4; }

    public DbsField getDoc_Ext_File_2_Dx2_Zip_Plus1() { return doc_Ext_File_2_Dx2_Zip_Plus1; }

    public DbsField getDoc_Ext_File_2_Dx2_Suspension() { return doc_Ext_File_2_Dx2_Suspension; }

    public DbsField getDoc_Ext_File_2_Dx2_Mult_App_Version() { return doc_Ext_File_2_Dx2_Mult_App_Version; }

    public DbsField getDoc_Ext_File_2_Dx2_Mult_App_Status() { return doc_Ext_File_2_Dx2_Mult_App_Status; }

    public DbsField getDoc_Ext_File_2_Dx2_Mult_App_Lob() { return doc_Ext_File_2_Dx2_Mult_App_Lob; }

    public DbsField getDoc_Ext_File_2_Dx2_Mult_App_Lob_Type() { return doc_Ext_File_2_Dx2_Mult_App_Lob_Type; }

    public DbsField getDoc_Ext_File_2_Dx2_Mult_App_Ppg() { return doc_Ext_File_2_Dx2_Mult_App_Ppg; }

    public DbsField getDoc_Ext_File_2_Dx2_K12_Ppg() { return doc_Ext_File_2_Dx2_K12_Ppg; }

    public DbsField getDoc_Ext_File_2_Dx2_Ira_Rollover_Type() { return doc_Ext_File_2_Dx2_Ira_Rollover_Type; }

    public DbsField getDoc_Ext_File_2_Dx2_Ira_Record_Type() { return doc_Ext_File_2_Dx2_Ira_Record_Type; }

    public DbsField getDoc_Ext_File_2_Dx2_Rollover_Amt() { return doc_Ext_File_2_Dx2_Rollover_Amt; }

    public DbsField getDoc_Ext_File_2_Dx2_Mit_Unit() { return doc_Ext_File_2_Dx2_Mit_Unit; }

    public DbsField getDoc_Ext_File_2_Dx2_Mit_Wpid() { return doc_Ext_File_2_Dx2_Mit_Wpid; }

    public DbsField getDoc_Ext_File_2_Dx2_New_Bene_Ind() { return doc_Ext_File_2_Dx2_New_Bene_Ind; }

    public DbsField getDoc_Ext_File_2_Dx2_Irc_Sectn_Cde() { return doc_Ext_File_2_Dx2_Irc_Sectn_Cde; }

    public DbsField getDoc_Ext_File_2_Dx2_Released_Dt() { return doc_Ext_File_2_Dx2_Released_Dt; }

    public DbsField getDoc_Ext_File_2_Dx2_Irc_Sectn_Grp() { return doc_Ext_File_2_Dx2_Irc_Sectn_Grp; }

    public DbsField getDoc_Ext_File_2_Dx2_Enroll_Request_Type() { return doc_Ext_File_2_Dx2_Enroll_Request_Type; }

    public DbsField getDoc_Ext_File_2_Dx2_Erisa_Inst() { return doc_Ext_File_2_Dx2_Erisa_Inst; }

    public DbsField getDoc_Ext_File_2_Dx2_Reprint_Request_Type() { return doc_Ext_File_2_Dx2_Reprint_Request_Type; }

    public DbsField getDoc_Ext_File_2_Dx2_Print_Destination() { return doc_Ext_File_2_Dx2_Print_Destination; }

    public DbsField getDoc_Ext_File_2_Dx2_Correction_Type() { return doc_Ext_File_2_Dx2_Correction_Type; }

    public DbsField getDoc_Ext_File_2_Dx2_Processor_Name() { return doc_Ext_File_2_Dx2_Processor_Name; }

    public DbsField getDoc_Ext_File_2_Dx2_Mail_Date() { return doc_Ext_File_2_Dx2_Mail_Date; }

    public DbsField getDoc_Ext_File_2_Dx2_Alloc_Fmt() { return doc_Ext_File_2_Dx2_Alloc_Fmt; }

    public DbsField getDoc_Ext_File_2_Dx2_Phone_No() { return doc_Ext_File_2_Dx2_Phone_No; }

    public DbsField getDoc_Ext_File_2_Dx2_Fund_Cde() { return doc_Ext_File_2_Dx2_Fund_Cde; }

    public DbsField getDoc_Ext_File_2_Dx2_Alloc_Pct() { return doc_Ext_File_2_Dx2_Alloc_Pct; }

    public DbsField getDoc_Ext_File_2_Dx2_Oia_Ind() { return doc_Ext_File_2_Dx2_Oia_Ind; }

    public DbsField getDoc_Ext_File_2_Dx2_Product_Cde() { return doc_Ext_File_2_Dx2_Product_Cde; }

    public DbsField getDoc_Ext_File_2_Dx2_Acct_Sum_Sheet_Type() { return doc_Ext_File_2_Dx2_Acct_Sum_Sheet_Type; }

    public DbsField getDoc_Ext_File_2_Dx2_Sg_Plan_No() { return doc_Ext_File_2_Dx2_Sg_Plan_No; }

    public DbsField getDoc_Ext_File_2_Dx2_Sg_Subplan_No() { return doc_Ext_File_2_Dx2_Sg_Subplan_No; }

    public DbsField getDoc_Ext_File_2_Dx2_Sg_Subplan_Type() { return doc_Ext_File_2_Dx2_Sg_Subplan_Type; }

    public DbsField getDoc_Ext_File_2_Dx2_Omni_Issue_Ind() { return doc_Ext_File_2_Dx2_Omni_Issue_Ind; }

    public DbsField getDoc_Ext_File_2_Dx2_Effective_Date() { return doc_Ext_File_2_Dx2_Effective_Date; }

    public DbsField getDoc_Ext_File_2_Dx2_Employer_Name() { return doc_Ext_File_2_Dx2_Employer_Name; }

    public DbsField getDoc_Ext_File_2_Dx2_Tiaa_Rate() { return doc_Ext_File_2_Dx2_Tiaa_Rate; }

    public DbsField getDoc_Ext_File_2_Dx2_Sg_Plan_Id() { return doc_Ext_File_2_Dx2_Sg_Plan_Id; }

    public DbsField getDoc_Ext_File_2_Dx2_Tiaa_Pg4_Numb() { return doc_Ext_File_2_Dx2_Tiaa_Pg4_Numb; }

    public DbsField getDoc_Ext_File_2_Dx2_Cref_Pg4_Numb() { return doc_Ext_File_2_Dx2_Cref_Pg4_Numb; }

    public DbsField getDoc_Ext_File_2_Dx2_Dflt_Access_Code() { return doc_Ext_File_2_Dx2_Dflt_Access_Code; }

    public DbsField getDoc_Ext_File_2_Dx2_Single_Issue_Ind() { return doc_Ext_File_2_Dx2_Single_Issue_Ind; }

    public DbsField getDoc_Ext_File_2_Dx2_Spec_Fund_Ind() { return doc_Ext_File_2_Dx2_Spec_Fund_Ind; }

    public DbsField getDoc_Ext_File_2_Dx2_Product_Price_Level() { return doc_Ext_File_2_Dx2_Product_Price_Level; }

    public DbsField getDoc_Ext_File_2_Dx2_Roth_Ind() { return doc_Ext_File_2_Dx2_Roth_Ind; }

    public DbsField getDoc_Ext_File_2_Dx2_Plan_Issue_State() { return doc_Ext_File_2_Dx2_Plan_Issue_State; }

    public DbsField getDoc_Ext_File_2_Dx2_Sgrd_Client_Id() { return doc_Ext_File_2_Dx2_Sgrd_Client_Id; }

    public DbsField getDoc_Ext_File_2_Dx2_Portfolio_Type() { return doc_Ext_File_2_Dx2_Portfolio_Type; }

    public DbsField getDoc_Ext_File_2_Dx2_Auto_Enroll_Ind() { return doc_Ext_File_2_Dx2_Auto_Enroll_Ind; }

    public DbsField getDoc_Ext_File_2_Dx2_Auto_Save_Ind() { return doc_Ext_File_2_Dx2_Auto_Save_Ind; }

    public DbsField getDoc_Ext_File_2_Dx2_As_Cur_Dflt_Amt() { return doc_Ext_File_2_Dx2_As_Cur_Dflt_Amt; }

    public DbsField getDoc_Ext_File_2_Dx2_As_Incr_Amt() { return doc_Ext_File_2_Dx2_As_Incr_Amt; }

    public DbsField getDoc_Ext_File_2_Dx2_As_Max_Pct() { return doc_Ext_File_2_Dx2_As_Max_Pct; }

    public DbsField getDoc_Ext_File_2_Dx2_Ae_Opt_Out_Days() { return doc_Ext_File_2_Dx2_Ae_Opt_Out_Days; }

    public DbsField getDoc_Ext_File_2_Dx2_Tsv_Ind() { return doc_Ext_File_2_Dx2_Tsv_Ind; }

    public DbsField getDoc_Ext_File_2_Dx2_Tiaa_Indx_Guarntd_Rte() { return doc_Ext_File_2_Dx2_Tiaa_Indx_Guarntd_Rte; }

    public DbsField getDoc_Ext_File_2_Dx2_Agent_Crd_No() { return doc_Ext_File_2_Dx2_Agent_Crd_No; }

    public DbsField getDoc_Ext_File_2_Dx2_Crd_Pull_Ind() { return doc_Ext_File_2_Dx2_Crd_Pull_Ind; }

    public DbsField getDoc_Ext_File_2_Dx2_Agent_Name() { return doc_Ext_File_2_Dx2_Agent_Name; }

    public DbsField getDoc_Ext_File_2_Dx2_Agent_Work_Addr1() { return doc_Ext_File_2_Dx2_Agent_Work_Addr1; }

    public DbsField getDoc_Ext_File_2_Dx2_Agent_Work_Addr2() { return doc_Ext_File_2_Dx2_Agent_Work_Addr2; }

    public DbsField getDoc_Ext_File_2_Dx2_Agent_Work_City() { return doc_Ext_File_2_Dx2_Agent_Work_City; }

    public DbsField getDoc_Ext_File_2_Dx2_Agent_Work_State() { return doc_Ext_File_2_Dx2_Agent_Work_State; }

    public DbsField getDoc_Ext_File_2_Dx2_Agent_Work_Zip() { return doc_Ext_File_2_Dx2_Agent_Work_Zip; }

    public DbsField getDoc_Ext_File_2_Dx2_Agent_Work_Phone() { return doc_Ext_File_2_Dx2_Agent_Work_Phone; }

    public DbsField getDoc_Ext_File_2_Dx2_Tic_Ind() { return doc_Ext_File_2_Dx2_Tic_Ind; }

    public DbsField getDoc_Ext_File_2_Dx2_Tic_Startdate() { return doc_Ext_File_2_Dx2_Tic_Startdate; }

    public DbsField getDoc_Ext_File_2_Dx2_Tic_Enddate() { return doc_Ext_File_2_Dx2_Tic_Enddate; }

    public DbsField getDoc_Ext_File_2_Dx2_Tic_Percentage() { return doc_Ext_File_2_Dx2_Tic_Percentage; }

    public DbsField getDoc_Ext_File_2_Dx2_Tic_Postdays() { return doc_Ext_File_2_Dx2_Tic_Postdays; }

    public DbsField getDoc_Ext_File_2_Dx2_Tic_Limit() { return doc_Ext_File_2_Dx2_Tic_Limit; }

    public DbsField getDoc_Ext_File_2_Dx2_Tic_Postfreq() { return doc_Ext_File_2_Dx2_Tic_Postfreq; }

    public DbsField getDoc_Ext_File_2_Dx2_Tic_Pl_Level() { return doc_Ext_File_2_Dx2_Tic_Pl_Level; }

    public DbsField getDoc_Ext_File_2_Dx2_Tic_Windowdays() { return doc_Ext_File_2_Dx2_Tic_Windowdays; }

    public DbsField getDoc_Ext_File_2_Dx2_Tic_Reqdlywindow() { return doc_Ext_File_2_Dx2_Tic_Reqdlywindow; }

    public DbsField getDoc_Ext_File_2_Dx2_Tic_Recap_Prov() { return doc_Ext_File_2_Dx2_Tic_Recap_Prov; }

    public DbsField getDoc_Ext_File_2_Dx2_Ecs_Dcs_E_Dlvry_Ind() { return doc_Ext_File_2_Dx2_Ecs_Dcs_E_Dlvry_Ind; }

    public DbsField getDoc_Ext_File_2_Dx2_Ecs_Dcs_Annty_Optn_Ind() { return doc_Ext_File_2_Dx2_Ecs_Dcs_Annty_Optn_Ind; }

    public DbsField getDoc_Ext_File_2_Dx2_Full_Middle_Name() { return doc_Ext_File_2_Dx2_Full_Middle_Name; }

    public DbsField getDoc_Ext_File_2_Dx2_Substitution_Contract_Ind() { return doc_Ext_File_2_Dx2_Substitution_Contract_Ind; }

    public DbsField getDoc_Ext_File_2_Dx2_Non_Proprietary_Pgk_Ind() { return doc_Ext_File_2_Dx2_Non_Proprietary_Pgk_Ind; }

    public DbsField getDoc_Ext_File_2_Dx2_Multi_Plan_Table() { return doc_Ext_File_2_Dx2_Multi_Plan_Table; }

    public DbsGroup getDoc_Ext_File_2_Dx2_Multi_Plan_TableRedef8() { return doc_Ext_File_2_Dx2_Multi_Plan_TableRedef8; }

    public DbsGroup getDoc_Ext_File_2_Dx2_Multi_Plan_Info() { return doc_Ext_File_2_Dx2_Multi_Plan_Info; }

    public DbsField getDoc_Ext_File_2_Dx2_Multi_Plan_No() { return doc_Ext_File_2_Dx2_Multi_Plan_No; }

    public DbsField getDoc_Ext_File_2_Dx2_Multi_Sub_Plan() { return doc_Ext_File_2_Dx2_Multi_Sub_Plan; }

    public DbsField getDoc_Ext_File_2_Dx2_Filler() { return doc_Ext_File_2_Dx2_Filler; }

    public DbsField getDoc_Ext_File_2_Dx2_Tiaa_Init_Prem() { return doc_Ext_File_2_Dx2_Tiaa_Init_Prem; }

    public DbsField getDoc_Ext_File_2_Dx2_Cref_Init_Prem() { return doc_Ext_File_2_Dx2_Cref_Init_Prem; }

    public DbsField getDoc_Ext_File_2_Dx2_Related_Contract_Info() { return doc_Ext_File_2_Dx2_Related_Contract_Info; }

    public DbsGroup getDoc_Ext_File_2_Dx2_Related_Contract_InfoRedef9() { return doc_Ext_File_2_Dx2_Related_Contract_InfoRedef9; }

    public DbsGroup getDoc_Ext_File_2_Dx2_Related_Contract() { return doc_Ext_File_2_Dx2_Related_Contract; }

    public DbsField getDoc_Ext_File_2_Dx2_Related_Contract_Type() { return doc_Ext_File_2_Dx2_Related_Contract_Type; }

    public DbsField getDoc_Ext_File_2_Dx2_Related_Tiaa_No() { return doc_Ext_File_2_Dx2_Related_Tiaa_No; }

    public DbsField getDoc_Ext_File_2_Dx2_Related_Tiaa_Issue_Date() { return doc_Ext_File_2_Dx2_Related_Tiaa_Issue_Date; }

    public DbsField getDoc_Ext_File_2_Dx2_Related_First_Payment_Date() { return doc_Ext_File_2_Dx2_Related_First_Payment_Date; }

    public DbsField getDoc_Ext_File_2_Dx2_Related_Tiaa_Total_Amt() { return doc_Ext_File_2_Dx2_Related_Tiaa_Total_Amt; }

    public DbsField getDoc_Ext_File_2_Dx2_Related_Last_Payment_Date() { return doc_Ext_File_2_Dx2_Related_Last_Payment_Date; }

    public DbsField getDoc_Ext_File_2_Dx2_Related_Payment_Frequency() { return doc_Ext_File_2_Dx2_Related_Payment_Frequency; }

    public DbsField getDoc_Ext_File_2_Dx2_Related_Tiaa_Issue_State() { return doc_Ext_File_2_Dx2_Related_Tiaa_Issue_State; }

    public DbsField getDoc_Ext_File_2_Dx2_First_Tpa_Ipro_Ind() { return doc_Ext_File_2_Dx2_First_Tpa_Ipro_Ind; }

    public DbsField getDoc_Ext_File_2_Dx2_Fund_Source_1() { return doc_Ext_File_2_Dx2_Fund_Source_1; }

    public DbsField getDoc_Ext_File_2_Dx2_Fund_Source_2() { return doc_Ext_File_2_Dx2_Fund_Source_2; }

    public DbsField getDoc_Ext_File_2_Dx2_Fund_Cde_2() { return doc_Ext_File_2_Dx2_Fund_Cde_2; }

    public DbsField getDoc_Ext_File_2_Dx2_Alloc_Pct_2() { return doc_Ext_File_2_Dx2_Alloc_Pct_2; }

    public DbsGroup getDoc_Ext_File_2Redef10() { return doc_Ext_File_2Redef10; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_1() { return doc_Ext_File_2_Doc_Ext2_Data_1; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_2() { return doc_Ext_File_2_Doc_Ext2_Data_2; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_3() { return doc_Ext_File_2_Doc_Ext2_Data_3; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_4() { return doc_Ext_File_2_Doc_Ext2_Data_4; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_5() { return doc_Ext_File_2_Doc_Ext2_Data_5; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_6() { return doc_Ext_File_2_Doc_Ext2_Data_6; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_7() { return doc_Ext_File_2_Doc_Ext2_Data_7; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_8() { return doc_Ext_File_2_Doc_Ext2_Data_8; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_9() { return doc_Ext_File_2_Doc_Ext2_Data_9; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_10() { return doc_Ext_File_2_Doc_Ext2_Data_10; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_11() { return doc_Ext_File_2_Doc_Ext2_Data_11; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_12() { return doc_Ext_File_2_Doc_Ext2_Data_12; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_13() { return doc_Ext_File_2_Doc_Ext2_Data_13; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_14() { return doc_Ext_File_2_Doc_Ext2_Data_14; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_15() { return doc_Ext_File_2_Doc_Ext2_Data_15; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_16() { return doc_Ext_File_2_Doc_Ext2_Data_16; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_17() { return doc_Ext_File_2_Doc_Ext2_Data_17; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_18() { return doc_Ext_File_2_Doc_Ext2_Data_18; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_19() { return doc_Ext_File_2_Doc_Ext2_Data_19; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_20() { return doc_Ext_File_2_Doc_Ext2_Data_20; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_21() { return doc_Ext_File_2_Doc_Ext2_Data_21; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_22() { return doc_Ext_File_2_Doc_Ext2_Data_22; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_23() { return doc_Ext_File_2_Doc_Ext2_Data_23; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_24() { return doc_Ext_File_2_Doc_Ext2_Data_24; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_25() { return doc_Ext_File_2_Doc_Ext2_Data_25; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_26() { return doc_Ext_File_2_Doc_Ext2_Data_26; }

    public DbsField getDoc_Ext_File_2_Doc_Ext2_Data_27() { return doc_Ext_File_2_Doc_Ext2_Data_27; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        doc_Ext_File_2 = newFieldInRecord("doc_Ext_File_2", "DOC-EXT-FILE-2", FieldType.STRING, 6572);
        doc_Ext_File_2Redef1 = newGroupInRecord("doc_Ext_File_2Redef1", "Redefines", doc_Ext_File_2);
        doc_Ext_File_2_Dx2_Type_Cd = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Type_Cd", "DX2-TYPE-CD", FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Contract_Table = doc_Ext_File_2Redef1.newFieldArrayInGroup("doc_Ext_File_2_Dx2_Contract_Table", "DX2-CONTRACT-TABLE", FieldType.STRING, 
            16, new DbsArrayController(1,4));
        doc_Ext_File_2_Dx2_Contract_TableRedef2 = doc_Ext_File_2Redef1.newGroupInGroup("doc_Ext_File_2_Dx2_Contract_TableRedef2", "Redefines", doc_Ext_File_2_Dx2_Contract_Table);
        doc_Ext_File_2_Dx2_Contract_Numbers = doc_Ext_File_2_Dx2_Contract_TableRedef2.newGroupArrayInGroup("doc_Ext_File_2_Dx2_Contract_Numbers", "DX2-CONTRACT-NUMBERS", 
            new DbsArrayController(1,4));
        doc_Ext_File_2_Dx2_Tiaa_Prfx = doc_Ext_File_2_Dx2_Contract_Numbers.newFieldInGroup("doc_Ext_File_2_Dx2_Tiaa_Prfx", "DX2-TIAA-PRFX", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Tiaa_No = doc_Ext_File_2_Dx2_Contract_Numbers.newFieldInGroup("doc_Ext_File_2_Dx2_Tiaa_No", "DX2-TIAA-NO", FieldType.STRING, 
            7);
        doc_Ext_File_2_Dx2_Cref_Prfx = doc_Ext_File_2_Dx2_Contract_Numbers.newFieldInGroup("doc_Ext_File_2_Dx2_Cref_Prfx", "DX2-CREF-PRFX", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Cref_No = doc_Ext_File_2_Dx2_Contract_Numbers.newFieldInGroup("doc_Ext_File_2_Dx2_Cref_No", "DX2-CREF-NO", FieldType.STRING, 
            7);
        doc_Ext_File_2_Dx2_Date_Table = doc_Ext_File_2Redef1.newFieldArrayInGroup("doc_Ext_File_2_Dx2_Date_Table", "DX2-DATE-TABLE", FieldType.STRING, 
            8, new DbsArrayController(1,6));
        doc_Ext_File_2_Dx2_Age_First_Pymt = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Age_First_Pymt", "DX2-AGE-FIRST-PYMT", FieldType.STRING, 
            4);
        doc_Ext_File_2_Dx2_Ownership = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Ownership", "DX2-OWNERSHIP", FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Alloc_Disc = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Alloc_Disc", "DX2-ALLOC-DISC", FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Currency = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Currency", "DX2-CURRENCY", FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Tiaa_Form_No = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Tiaa_Form_No", "DX2-TIAA-FORM-NO", FieldType.STRING, 
            9);
        doc_Ext_File_2_Dx2_Cref_Form_No = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Cref_Form_No", "DX2-CREF-FORM-NO", FieldType.STRING, 
            9);
        doc_Ext_File_2_Dx2_Tiaa_Ed_No = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Tiaa_Ed_No", "DX2-TIAA-ED-NO", FieldType.STRING, 9);
        doc_Ext_File_2_Dx2_Cref_Ed_No = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Cref_Ed_No", "DX2-CREF-ED-NO", FieldType.STRING, 9);
        doc_Ext_File_2_Dx2_Annuitant_Name = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Annuitant_Name", "DX2-ANNUITANT-NAME", FieldType.STRING, 
            38);
        doc_Ext_File_2_Dx2_Eop_Name = doc_Ext_File_2Redef1.newGroupInGroup("doc_Ext_File_2_Dx2_Eop_Name", "DX2-EOP-NAME");
        doc_Ext_File_2_Dx2_Eop_Last_Name = doc_Ext_File_2_Dx2_Eop_Name.newFieldInGroup("doc_Ext_File_2_Dx2_Eop_Last_Name", "DX2-EOP-LAST-NAME", FieldType.STRING, 
            16);
        doc_Ext_File_2_Dx2_Eop_First_Name = doc_Ext_File_2_Dx2_Eop_Name.newFieldInGroup("doc_Ext_File_2_Dx2_Eop_First_Name", "DX2-EOP-FIRST-NAME", FieldType.STRING, 
            10);
        doc_Ext_File_2_Dx2_Eop_Middle_Name = doc_Ext_File_2_Dx2_Eop_Name.newFieldInGroup("doc_Ext_File_2_Dx2_Eop_Middle_Name", "DX2-EOP-MIDDLE-NAME", 
            FieldType.STRING, 10);
        doc_Ext_File_2_Dx2_Eop_Middle_NameRedef3 = doc_Ext_File_2_Dx2_Eop_Name.newGroupInGroup("doc_Ext_File_2_Dx2_Eop_Middle_NameRedef3", "Redefines", 
            doc_Ext_File_2_Dx2_Eop_Middle_Name);
        doc_Ext_File_2_Dx2_Eop_Mid_Init = doc_Ext_File_2_Dx2_Eop_Middle_NameRedef3.newFieldInGroup("doc_Ext_File_2_Dx2_Eop_Mid_Init", "DX2-EOP-MID-INIT", 
            FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Eop_Mid_Filler = doc_Ext_File_2_Dx2_Eop_Middle_NameRedef3.newFieldInGroup("doc_Ext_File_2_Dx2_Eop_Mid_Filler", "DX2-EOP-MID-FILLER", 
            FieldType.STRING, 9);
        doc_Ext_File_2_Dx2_Eop_Title = doc_Ext_File_2_Dx2_Eop_Name.newFieldInGroup("doc_Ext_File_2_Dx2_Eop_Title", "DX2-EOP-TITLE", FieldType.STRING, 
            4);
        doc_Ext_File_2_Dx2_Eop_Prefix = doc_Ext_File_2_Dx2_Eop_Name.newFieldInGroup("doc_Ext_File_2_Dx2_Eop_Prefix", "DX2-EOP-PREFIX", FieldType.STRING, 
            4);
        doc_Ext_File_2_Dx2_Soc_Sec = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Soc_Sec", "DX2-SOC-SEC", FieldType.STRING, 11);
        doc_Ext_File_2_Dx2_Pin_No = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Pin_No", "DX2-PIN-NO", FieldType.STRING, 12);
        doc_Ext_File_2_Dx2_Address_Table = doc_Ext_File_2Redef1.newFieldArrayInGroup("doc_Ext_File_2_Dx2_Address_Table", "DX2-ADDRESS-TABLE", FieldType.STRING, 
            35, new DbsArrayController(1,6));
        doc_Ext_File_2_Dx2_Eop_Zipcode = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Eop_Zipcode", "DX2-EOP-ZIPCODE", FieldType.STRING, 5);
        doc_Ext_File_2_Dx2_Participant_Status = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Participant_Status", "DX2-PARTICIPANT-STATUS", 
            FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Email_Address = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Email_Address", "DX2-EMAIL-ADDRESS", FieldType.STRING, 
            50);
        doc_Ext_File_2_Dx2_Request_Package_Type = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Request_Package_Type", "DX2-REQUEST-PACKAGE-TYPE", 
            FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Portfolio_Selected = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Portfolio_Selected", "DX2-PORTFOLIO-SELECTED", 
            FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Divorce_Contract = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Divorce_Contract", "DX2-DIVORCE-CONTRACT", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Access_Cd_Ind = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Access_Cd_Ind", "DX2-ACCESS-CD-IND", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Address_Change = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Address_Change", "DX2-ADDRESS-CHANGE", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Package_Version = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Package_Version", "DX2-PACKAGE-VERSION", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Unit_Name = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Unit_Name", "DX2-UNIT-NAME", FieldType.STRING, 10);
        doc_Ext_File_2_Dx2_Print_Package_Type = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Print_Package_Type", "DX2-PRINT-PACKAGE-TYPE", 
            FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Future_Filler = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Future_Filler", "DX2-FUTURE-FILLER", FieldType.STRING, 
            5);
        doc_Ext_File_2_Dx2_Bene_Estate = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Bene_Estate", "DX2-BENE-ESTATE", FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Bene_Trust = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Bene_Trust", "DX2-BENE-TRUST", FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Bene_Category = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Bene_Category", "DX2-BENE-CATEGORY", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Sex = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Sex", "DX2-SEX", FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Inst_Name_Table = doc_Ext_File_2Redef1.newFieldArrayInGroup("doc_Ext_File_2_Dx2_Inst_Name_Table", "DX2-INST-NAME-TABLE", FieldType.STRING, 
            30, new DbsArrayController(1,10));
        doc_Ext_File_2_Dx2_Inst_Name_76 = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Inst_Name_76", "DX2-INST-NAME-76", FieldType.STRING, 
            76);
        doc_Ext_File_2_Dx2_Inst_Address_Table = doc_Ext_File_2Redef1.newFieldArrayInGroup("doc_Ext_File_2_Dx2_Inst_Address_Table", "DX2-INST-ADDRESS-TABLE", 
            FieldType.STRING, 35, new DbsArrayController(1,6));
        doc_Ext_File_2_Dx2_Inst_Zip_Cd = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Inst_Zip_Cd", "DX2-INST-ZIP-CD", FieldType.STRING, 5);
        doc_Ext_File_2_Dx2_Mail_Instructions = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Mail_Instructions", "DX2-MAIL-INSTRUCTIONS", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Orig_Iss_State = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Orig_Iss_State", "DX2-ORIG-ISS-STATE", FieldType.STRING, 
            2);
        doc_Ext_File_2_Dx2_Current_Iss_State = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Current_Iss_State", "DX2-CURRENT-ISS-STATE", FieldType.STRING, 
            2);
        doc_Ext_File_2_Dx2_Eop_State_Name = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Eop_State_Name", "DX2-EOP-STATE-NAME", FieldType.STRING, 
            15);
        doc_Ext_File_2_Dx2_Lob = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Lob", "DX2-LOB", FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Lob_Type = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Lob_Type", "DX2-LOB-TYPE", FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Inst_Code = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Inst_Code", "DX2-INST-CODE", FieldType.STRING, 4);
        doc_Ext_File_2_Dx2_Inst_Bill_Cd = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Inst_Bill_Cd", "DX2-INST-BILL-CD", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Inst_Permit_Trans = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Inst_Permit_Trans", "DX2-INST-PERMIT-TRANS", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Perm_Gra_Trans = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Perm_Gra_Trans", "DX2-PERM-GRA-TRANS", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Inst_Full_Immed_Vest = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Inst_Full_Immed_Vest", "DX2-INST-FULL-IMMED-VEST", 
            FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Inst_Cashable_Ra = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Inst_Cashable_Ra", "DX2-INST-CASHABLE-RA", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Inst_Cashable_Gra = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Inst_Cashable_Gra", "DX2-INST-CASHABLE-GRA", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Inst_Fixed_Per_Opt = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Inst_Fixed_Per_Opt", "DX2-INST-FIXED-PER-OPT", 
            FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Inst_Cntrl_Consent = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Inst_Cntrl_Consent", "DX2-INST-CNTRL-CONSENT", 
            FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Eop = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Eop", "DX2-EOP", FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Gsra_Inst_St_Code = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Gsra_Inst_St_Code", "DX2-GSRA-INST-ST-CODE", FieldType.STRING, 
            2);
        doc_Ext_File_2_Dx2_Region_Cd = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Region_Cd", "DX2-REGION-CD", FieldType.STRING, 3);
        doc_Ext_File_2_Dx2_Staff_Id = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Staff_Id", "DX2-STAFF-ID", FieldType.STRING, 3);
        doc_Ext_File_2_Dx2_Appl_Source = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Appl_Source", "DX2-APPL-SOURCE", FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Eop_Appl_Status = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Eop_Appl_Status", "DX2-EOP-APPL-STATUS", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Eop_Addl_Cref_Req = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Eop_Addl_Cref_Req", "DX2-EOP-ADDL-CREF-REQ", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Extract_Date = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Extract_Date", "DX2-EXTRACT-DATE", FieldType.STRING, 
            6);
        doc_Ext_File_2_Dx2_Gsra_Loan_Ind = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Gsra_Loan_Ind", "DX2-GSRA-LOAN-IND", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Spec_Ppg_Ind = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Spec_Ppg_Ind", "DX2-SPEC-PPG-IND", FieldType.STRING, 
            2);
        doc_Ext_File_2_Dx2_Mail_Pull_Cd = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Mail_Pull_Cd", "DX2-MAIL-PULL-CD", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Mit_Log_Dt_Tm = doc_Ext_File_2Redef1.newFieldArrayInGroup("doc_Ext_File_2_Dx2_Mit_Log_Dt_Tm", "DX2-MIT-LOG-DT-TM", FieldType.STRING, 
            15, new DbsArrayController(1,5));
        doc_Ext_File_2_Dx2_Addr_Page_Name = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Addr_Page_Name", "DX2-ADDR-PAGE-NAME", FieldType.STRING, 
            81);
        doc_Ext_File_2_Dx2_Addr_Page_NameRedef4 = doc_Ext_File_2Redef1.newGroupInGroup("doc_Ext_File_2_Dx2_Addr_Page_NameRedef4", "Redefines", doc_Ext_File_2_Dx2_Addr_Page_Name);
        doc_Ext_File_2_Dx2_Addr_Pref = doc_Ext_File_2_Dx2_Addr_Page_NameRedef4.newFieldInGroup("doc_Ext_File_2_Dx2_Addr_Pref", "DX2-ADDR-PREF", FieldType.STRING, 
            8);
        doc_Ext_File_2_Dx2_Addr_Filler_1 = doc_Ext_File_2_Dx2_Addr_Page_NameRedef4.newFieldInGroup("doc_Ext_File_2_Dx2_Addr_Filler_1", "DX2-ADDR-FILLER-1", 
            FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Addr_Frst = doc_Ext_File_2_Dx2_Addr_Page_NameRedef4.newFieldInGroup("doc_Ext_File_2_Dx2_Addr_Frst", "DX2-ADDR-FRST", FieldType.STRING, 
            30);
        doc_Ext_File_2_Dx2_Addr_Filler_2 = doc_Ext_File_2_Dx2_Addr_Page_NameRedef4.newFieldInGroup("doc_Ext_File_2_Dx2_Addr_Filler_2", "DX2-ADDR-FILLER-2", 
            FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Addr_Mddle = doc_Ext_File_2_Dx2_Addr_Page_NameRedef4.newFieldInGroup("doc_Ext_File_2_Dx2_Addr_Mddle", "DX2-ADDR-MDDLE", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Addr_Filler_3 = doc_Ext_File_2_Dx2_Addr_Page_NameRedef4.newFieldInGroup("doc_Ext_File_2_Dx2_Addr_Filler_3", "DX2-ADDR-FILLER-3", 
            FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Addr_Last = doc_Ext_File_2_Dx2_Addr_Page_NameRedef4.newFieldInGroup("doc_Ext_File_2_Dx2_Addr_Last", "DX2-ADDR-LAST", FieldType.STRING, 
            30);
        doc_Ext_File_2_Dx2_Addr_Filler_4 = doc_Ext_File_2_Dx2_Addr_Page_NameRedef4.newFieldInGroup("doc_Ext_File_2_Dx2_Addr_Filler_4", "DX2-ADDR-FILLER-4", 
            FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Addr_Sffx = doc_Ext_File_2_Dx2_Addr_Page_NameRedef4.newFieldInGroup("doc_Ext_File_2_Dx2_Addr_Sffx", "DX2-ADDR-SFFX", FieldType.STRING, 
            8);
        doc_Ext_File_2_Dx2_Welcome_Name = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Welcome_Name", "DX2-WELCOME-NAME", FieldType.STRING, 
            39);
        doc_Ext_File_2_Dx2_Welcome_NameRedef5 = doc_Ext_File_2Redef1.newGroupInGroup("doc_Ext_File_2_Dx2_Welcome_NameRedef5", "Redefines", doc_Ext_File_2_Dx2_Welcome_Name);
        doc_Ext_File_2_Dx2_Welcome_Pref = doc_Ext_File_2_Dx2_Welcome_NameRedef5.newFieldInGroup("doc_Ext_File_2_Dx2_Welcome_Pref", "DX2-WELCOME-PREF", 
            FieldType.STRING, 8);
        doc_Ext_File_2_Dx2_Welcome_Filler_1 = doc_Ext_File_2_Dx2_Welcome_NameRedef5.newFieldInGroup("doc_Ext_File_2_Dx2_Welcome_Filler_1", "DX2-WELCOME-FILLER-1", 
            FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Welcome_Last = doc_Ext_File_2_Dx2_Welcome_NameRedef5.newFieldInGroup("doc_Ext_File_2_Dx2_Welcome_Last", "DX2-WELCOME-LAST", 
            FieldType.STRING, 30);
        doc_Ext_File_2_Dx2_Pin_Name = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Pin_Name", "DX2-PIN-NAME", FieldType.STRING, 63);
        doc_Ext_File_2_Dx2_Pin_NameRedef6 = doc_Ext_File_2Redef1.newGroupInGroup("doc_Ext_File_2_Dx2_Pin_NameRedef6", "Redefines", doc_Ext_File_2_Dx2_Pin_Name);
        doc_Ext_File_2_Dx2_Pin_First = doc_Ext_File_2_Dx2_Pin_NameRedef6.newFieldInGroup("doc_Ext_File_2_Dx2_Pin_First", "DX2-PIN-FIRST", FieldType.STRING, 
            30);
        doc_Ext_File_2_Dx2_Pin_Filler_1 = doc_Ext_File_2_Dx2_Pin_NameRedef6.newFieldInGroup("doc_Ext_File_2_Dx2_Pin_Filler_1", "DX2-PIN-FILLER-1", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Pin_Mddle = doc_Ext_File_2_Dx2_Pin_NameRedef6.newFieldInGroup("doc_Ext_File_2_Dx2_Pin_Mddle", "DX2-PIN-MDDLE", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Pin_Filler_2 = doc_Ext_File_2_Dx2_Pin_NameRedef6.newFieldInGroup("doc_Ext_File_2_Dx2_Pin_Filler_2", "DX2-PIN-FILLER-2", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Pin_Last = doc_Ext_File_2_Dx2_Pin_NameRedef6.newFieldInGroup("doc_Ext_File_2_Dx2_Pin_Last", "DX2-PIN-LAST", FieldType.STRING, 
            30);
        doc_Ext_File_2_Dx2_Zip = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Zip", "DX2-ZIP", FieldType.STRING, 5);
        doc_Ext_File_2_Dx2_ZipRedef7 = doc_Ext_File_2Redef1.newGroupInGroup("doc_Ext_File_2_Dx2_ZipRedef7", "Redefines", doc_Ext_File_2_Dx2_Zip);
        doc_Ext_File_2_Dx2_Zip_Plus4 = doc_Ext_File_2_Dx2_ZipRedef7.newFieldInGroup("doc_Ext_File_2_Dx2_Zip_Plus4", "DX2-ZIP-PLUS4", FieldType.STRING, 
            4);
        doc_Ext_File_2_Dx2_Zip_Plus1 = doc_Ext_File_2_Dx2_ZipRedef7.newFieldInGroup("doc_Ext_File_2_Dx2_Zip_Plus1", "DX2-ZIP-PLUS1", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Suspension = doc_Ext_File_2Redef1.newFieldArrayInGroup("doc_Ext_File_2_Dx2_Suspension", "DX2-SUSPENSION", FieldType.STRING, 
            1, new DbsArrayController(1,20));
        doc_Ext_File_2_Dx2_Mult_App_Version = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Mult_App_Version", "DX2-MULT-APP-VERSION", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Mult_App_Status = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Mult_App_Status", "DX2-MULT-APP-STATUS", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Mult_App_Lob = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Mult_App_Lob", "DX2-MULT-APP-LOB", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Mult_App_Lob_Type = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Mult_App_Lob_Type", "DX2-MULT-APP-LOB-TYPE", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Mult_App_Ppg = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Mult_App_Ppg", "DX2-MULT-APP-PPG", FieldType.STRING, 
            6);
        doc_Ext_File_2_Dx2_K12_Ppg = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_K12_Ppg", "DX2-K12-PPG", FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Ira_Rollover_Type = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Ira_Rollover_Type", "DX2-IRA-ROLLOVER-TYPE", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Ira_Record_Type = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Ira_Record_Type", "DX2-IRA-RECORD-TYPE", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Rollover_Amt = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Rollover_Amt", "DX2-ROLLOVER-AMT", FieldType.STRING, 
            14);
        doc_Ext_File_2_Dx2_Mit_Unit = doc_Ext_File_2Redef1.newFieldArrayInGroup("doc_Ext_File_2_Dx2_Mit_Unit", "DX2-MIT-UNIT", FieldType.STRING, 8, new 
            DbsArrayController(1,5));
        doc_Ext_File_2_Dx2_Mit_Wpid = doc_Ext_File_2Redef1.newFieldArrayInGroup("doc_Ext_File_2_Dx2_Mit_Wpid", "DX2-MIT-WPID", FieldType.STRING, 6, new 
            DbsArrayController(1,5));
        doc_Ext_File_2_Dx2_New_Bene_Ind = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_New_Bene_Ind", "DX2-NEW-BENE-IND", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Irc_Sectn_Cde = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Irc_Sectn_Cde", "DX2-IRC-SECTN-CDE", FieldType.NUMERIC, 
            2);
        doc_Ext_File_2_Dx2_Released_Dt = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Released_Dt", "DX2-RELEASED-DT", FieldType.NUMERIC, 
            8);
        doc_Ext_File_2_Dx2_Irc_Sectn_Grp = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Irc_Sectn_Grp", "DX2-IRC-SECTN-GRP", FieldType.NUMERIC, 
            2);
        doc_Ext_File_2_Dx2_Enroll_Request_Type = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Enroll_Request_Type", "DX2-ENROLL-REQUEST-TYPE", 
            FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Erisa_Inst = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Erisa_Inst", "DX2-ERISA-INST", FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Reprint_Request_Type = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Reprint_Request_Type", "DX2-REPRINT-REQUEST-TYPE", 
            FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Print_Destination = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Print_Destination", "DX2-PRINT-DESTINATION", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Correction_Type = doc_Ext_File_2Redef1.newFieldArrayInGroup("doc_Ext_File_2_Dx2_Correction_Type", "DX2-CORRECTION-TYPE", FieldType.STRING, 
            1, new DbsArrayController(1,20));
        doc_Ext_File_2_Dx2_Processor_Name = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Processor_Name", "DX2-PROCESSOR-NAME", FieldType.STRING, 
            40);
        doc_Ext_File_2_Dx2_Mail_Date = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Mail_Date", "DX2-MAIL-DATE", FieldType.STRING, 17);
        doc_Ext_File_2_Dx2_Alloc_Fmt = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Alloc_Fmt", "DX2-ALLOC-FMT", FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Phone_No = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Phone_No", "DX2-PHONE-NO", FieldType.STRING, 20);
        doc_Ext_File_2_Dx2_Fund_Cde = doc_Ext_File_2Redef1.newFieldArrayInGroup("doc_Ext_File_2_Dx2_Fund_Cde", "DX2-FUND-CDE", FieldType.STRING, 10, new 
            DbsArrayController(1,100));
        doc_Ext_File_2_Dx2_Alloc_Pct = doc_Ext_File_2Redef1.newFieldArrayInGroup("doc_Ext_File_2_Dx2_Alloc_Pct", "DX2-ALLOC-PCT", FieldType.STRING, 3, 
            new DbsArrayController(1,100));
        doc_Ext_File_2_Dx2_Oia_Ind = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Oia_Ind", "DX2-OIA-IND", FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Product_Cde = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Product_Cde", "DX2-PRODUCT-CDE", FieldType.STRING, 3);
        doc_Ext_File_2_Dx2_Acct_Sum_Sheet_Type = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Acct_Sum_Sheet_Type", "DX2-ACCT-SUM-SHEET-TYPE", 
            FieldType.STRING, 56);
        doc_Ext_File_2_Dx2_Sg_Plan_No = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Sg_Plan_No", "DX2-SG-PLAN-NO", FieldType.STRING, 6);
        doc_Ext_File_2_Dx2_Sg_Subplan_No = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Sg_Subplan_No", "DX2-SG-SUBPLAN-NO", FieldType.STRING, 
            6);
        doc_Ext_File_2_Dx2_Sg_Subplan_Type = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Sg_Subplan_Type", "DX2-SG-SUBPLAN-TYPE", FieldType.STRING, 
            3);
        doc_Ext_File_2_Dx2_Omni_Issue_Ind = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Omni_Issue_Ind", "DX2-OMNI-ISSUE-IND", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Effective_Date = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Effective_Date", "DX2-EFFECTIVE-DATE", FieldType.STRING, 
            8);
        doc_Ext_File_2_Dx2_Employer_Name = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Employer_Name", "DX2-EMPLOYER-NAME", FieldType.STRING, 
            40);
        doc_Ext_File_2_Dx2_Tiaa_Rate = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Tiaa_Rate", "DX2-TIAA-RATE", FieldType.STRING, 8);
        doc_Ext_File_2_Dx2_Sg_Plan_Id = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Sg_Plan_Id", "DX2-SG-PLAN-ID", FieldType.STRING, 6);
        doc_Ext_File_2_Dx2_Tiaa_Pg4_Numb = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Tiaa_Pg4_Numb", "DX2-TIAA-PG4-NUMB", FieldType.STRING, 
            15);
        doc_Ext_File_2_Dx2_Cref_Pg4_Numb = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Cref_Pg4_Numb", "DX2-CREF-PG4-NUMB", FieldType.STRING, 
            15);
        doc_Ext_File_2_Dx2_Dflt_Access_Code = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Dflt_Access_Code", "DX2-DFLT-ACCESS-CODE", FieldType.STRING, 
            9);
        doc_Ext_File_2_Dx2_Single_Issue_Ind = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Single_Issue_Ind", "DX2-SINGLE-ISSUE-IND", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Spec_Fund_Ind = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Spec_Fund_Ind", "DX2-SPEC-FUND-IND", FieldType.STRING, 
            3);
        doc_Ext_File_2_Dx2_Product_Price_Level = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Product_Price_Level", "DX2-PRODUCT-PRICE-LEVEL", 
            FieldType.STRING, 5);
        doc_Ext_File_2_Dx2_Roth_Ind = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Roth_Ind", "DX2-ROTH-IND", FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Plan_Issue_State = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Plan_Issue_State", "DX2-PLAN-ISSUE-STATE", FieldType.STRING, 
            2);
        doc_Ext_File_2_Dx2_Sgrd_Client_Id = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Sgrd_Client_Id", "DX2-SGRD-CLIENT-ID", FieldType.STRING, 
            6);
        doc_Ext_File_2_Dx2_Portfolio_Type = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Portfolio_Type", "DX2-PORTFOLIO-TYPE", FieldType.STRING, 
            4);
        doc_Ext_File_2_Dx2_Auto_Enroll_Ind = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Auto_Enroll_Ind", "DX2-AUTO-ENROLL-IND", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Auto_Save_Ind = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Auto_Save_Ind", "DX2-AUTO-SAVE-IND", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_As_Cur_Dflt_Amt = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_As_Cur_Dflt_Amt", "DX2-AS-CUR-DFLT-AMT", FieldType.STRING, 
            10);
        doc_Ext_File_2_Dx2_As_Incr_Amt = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_As_Incr_Amt", "DX2-AS-INCR-AMT", FieldType.STRING, 7);
        doc_Ext_File_2_Dx2_As_Max_Pct = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_As_Max_Pct", "DX2-AS-MAX-PCT", FieldType.STRING, 7);
        doc_Ext_File_2_Dx2_Ae_Opt_Out_Days = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Ae_Opt_Out_Days", "DX2-AE-OPT-OUT-DAYS", FieldType.STRING, 
            2);
        doc_Ext_File_2_Dx2_Tsv_Ind = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Tsv_Ind", "DX2-TSV-IND", FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Tiaa_Indx_Guarntd_Rte = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Tiaa_Indx_Guarntd_Rte", "DX2-TIAA-INDX-GUARNTD-RTE", 
            FieldType.STRING, 6);
        doc_Ext_File_2_Dx2_Agent_Crd_No = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Agent_Crd_No", "DX2-AGENT-CRD-NO", FieldType.STRING, 
            15);
        doc_Ext_File_2_Dx2_Crd_Pull_Ind = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Crd_Pull_Ind", "DX2-CRD-PULL-IND", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Agent_Name = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Agent_Name", "DX2-AGENT-NAME", FieldType.STRING, 90);
        doc_Ext_File_2_Dx2_Agent_Work_Addr1 = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Agent_Work_Addr1", "DX2-AGENT-WORK-ADDR1", FieldType.STRING, 
            35);
        doc_Ext_File_2_Dx2_Agent_Work_Addr2 = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Agent_Work_Addr2", "DX2-AGENT-WORK-ADDR2", FieldType.STRING, 
            35);
        doc_Ext_File_2_Dx2_Agent_Work_City = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Agent_Work_City", "DX2-AGENT-WORK-CITY", FieldType.STRING, 
            35);
        doc_Ext_File_2_Dx2_Agent_Work_State = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Agent_Work_State", "DX2-AGENT-WORK-STATE", FieldType.STRING, 
            2);
        doc_Ext_File_2_Dx2_Agent_Work_Zip = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Agent_Work_Zip", "DX2-AGENT-WORK-ZIP", FieldType.STRING, 
            10);
        doc_Ext_File_2_Dx2_Agent_Work_Phone = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Agent_Work_Phone", "DX2-AGENT-WORK-PHONE", FieldType.STRING, 
            25);
        doc_Ext_File_2_Dx2_Tic_Ind = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Tic_Ind", "DX2-TIC-IND", FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Tic_Startdate = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Tic_Startdate", "DX2-TIC-STARTDATE", FieldType.STRING, 
            8);
        doc_Ext_File_2_Dx2_Tic_Enddate = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Tic_Enddate", "DX2-TIC-ENDDATE", FieldType.STRING, 8);
        doc_Ext_File_2_Dx2_Tic_Percentage = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Tic_Percentage", "DX2-TIC-PERCENTAGE", FieldType.STRING, 
            9);
        doc_Ext_File_2_Dx2_Tic_Postdays = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Tic_Postdays", "DX2-TIC-POSTDAYS", FieldType.STRING, 
            2);
        doc_Ext_File_2_Dx2_Tic_Limit = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Tic_Limit", "DX2-TIC-LIMIT", FieldType.STRING, 11);
        doc_Ext_File_2_Dx2_Tic_Postfreq = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Tic_Postfreq", "DX2-TIC-POSTFREQ", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Tic_Pl_Level = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Tic_Pl_Level", "DX2-TIC-PL-LEVEL", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Tic_Windowdays = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Tic_Windowdays", "DX2-TIC-WINDOWDAYS", FieldType.STRING, 
            3);
        doc_Ext_File_2_Dx2_Tic_Reqdlywindow = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Tic_Reqdlywindow", "DX2-TIC-REQDLYWINDOW", FieldType.STRING, 
            3);
        doc_Ext_File_2_Dx2_Tic_Recap_Prov = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Tic_Recap_Prov", "DX2-TIC-RECAP-PROV", FieldType.STRING, 
            3);
        doc_Ext_File_2_Dx2_Ecs_Dcs_E_Dlvry_Ind = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Ecs_Dcs_E_Dlvry_Ind", "DX2-ECS-DCS-E-DLVRY-IND", 
            FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Ecs_Dcs_Annty_Optn_Ind = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Ecs_Dcs_Annty_Optn_Ind", "DX2-ECS-DCS-ANNTY-OPTN-IND", 
            FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Full_Middle_Name = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Full_Middle_Name", "DX2-FULL-MIDDLE-NAME", FieldType.STRING, 
            30);
        doc_Ext_File_2_Dx2_Substitution_Contract_Ind = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Substitution_Contract_Ind", "DX2-SUBSTITUTION-CONTRACT-IND", 
            FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Non_Proprietary_Pgk_Ind = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Non_Proprietary_Pgk_Ind", "DX2-NON-PROPRIETARY-PGK-IND", 
            FieldType.STRING, 5);
        doc_Ext_File_2_Dx2_Multi_Plan_Table = doc_Ext_File_2Redef1.newFieldArrayInGroup("doc_Ext_File_2_Dx2_Multi_Plan_Table", "DX2-MULTI-PLAN-TABLE", 
            FieldType.STRING, 12, new DbsArrayController(1,15));
        doc_Ext_File_2_Dx2_Multi_Plan_TableRedef8 = doc_Ext_File_2Redef1.newGroupInGroup("doc_Ext_File_2_Dx2_Multi_Plan_TableRedef8", "Redefines", doc_Ext_File_2_Dx2_Multi_Plan_Table);
        doc_Ext_File_2_Dx2_Multi_Plan_Info = doc_Ext_File_2_Dx2_Multi_Plan_TableRedef8.newGroupArrayInGroup("doc_Ext_File_2_Dx2_Multi_Plan_Info", "DX2-MULTI-PLAN-INFO", 
            new DbsArrayController(1,15));
        doc_Ext_File_2_Dx2_Multi_Plan_No = doc_Ext_File_2_Dx2_Multi_Plan_Info.newFieldInGroup("doc_Ext_File_2_Dx2_Multi_Plan_No", "DX2-MULTI-PLAN-NO", 
            FieldType.STRING, 6);
        doc_Ext_File_2_Dx2_Multi_Sub_Plan = doc_Ext_File_2_Dx2_Multi_Plan_Info.newFieldInGroup("doc_Ext_File_2_Dx2_Multi_Sub_Plan", "DX2-MULTI-SUB-PLAN", 
            FieldType.STRING, 6);
        doc_Ext_File_2_Dx2_Filler = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Filler", "DX2-FILLER", FieldType.STRING, 35);
        doc_Ext_File_2_Dx2_Tiaa_Init_Prem = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Tiaa_Init_Prem", "DX2-TIAA-INIT-PREM", FieldType.DECIMAL, 
            10,2);
        doc_Ext_File_2_Dx2_Cref_Init_Prem = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Cref_Init_Prem", "DX2-CREF-INIT-PREM", FieldType.DECIMAL, 
            10,2);
        doc_Ext_File_2_Dx2_Related_Contract_Info = doc_Ext_File_2Redef1.newFieldArrayInGroup("doc_Ext_File_2_Dx2_Related_Contract_Info", "DX2-RELATED-CONTRACT-INFO", 
            FieldType.STRING, 49, new DbsArrayController(1,30));
        doc_Ext_File_2_Dx2_Related_Contract_InfoRedef9 = doc_Ext_File_2Redef1.newGroupInGroup("doc_Ext_File_2_Dx2_Related_Contract_InfoRedef9", "Redefines", 
            doc_Ext_File_2_Dx2_Related_Contract_Info);
        doc_Ext_File_2_Dx2_Related_Contract = doc_Ext_File_2_Dx2_Related_Contract_InfoRedef9.newGroupArrayInGroup("doc_Ext_File_2_Dx2_Related_Contract", 
            "DX2-RELATED-CONTRACT", new DbsArrayController(1,30));
        doc_Ext_File_2_Dx2_Related_Contract_Type = doc_Ext_File_2_Dx2_Related_Contract.newFieldInGroup("doc_Ext_File_2_Dx2_Related_Contract_Type", "DX2-RELATED-CONTRACT-TYPE", 
            FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Related_Tiaa_No = doc_Ext_File_2_Dx2_Related_Contract.newFieldInGroup("doc_Ext_File_2_Dx2_Related_Tiaa_No", "DX2-RELATED-TIAA-NO", 
            FieldType.STRING, 10);
        doc_Ext_File_2_Dx2_Related_Tiaa_Issue_Date = doc_Ext_File_2_Dx2_Related_Contract.newFieldInGroup("doc_Ext_File_2_Dx2_Related_Tiaa_Issue_Date", 
            "DX2-RELATED-TIAA-ISSUE-DATE", FieldType.NUMERIC, 8);
        doc_Ext_File_2_Dx2_Related_First_Payment_Date = doc_Ext_File_2_Dx2_Related_Contract.newFieldInGroup("doc_Ext_File_2_Dx2_Related_First_Payment_Date", 
            "DX2-RELATED-FIRST-PAYMENT-DATE", FieldType.NUMERIC, 8);
        doc_Ext_File_2_Dx2_Related_Tiaa_Total_Amt = doc_Ext_File_2_Dx2_Related_Contract.newFieldInGroup("doc_Ext_File_2_Dx2_Related_Tiaa_Total_Amt", "DX2-RELATED-TIAA-TOTAL-AMT", 
            FieldType.DECIMAL, 10,2);
        doc_Ext_File_2_Dx2_Related_Last_Payment_Date = doc_Ext_File_2_Dx2_Related_Contract.newFieldInGroup("doc_Ext_File_2_Dx2_Related_Last_Payment_Date", 
            "DX2-RELATED-LAST-PAYMENT-DATE", FieldType.NUMERIC, 8);
        doc_Ext_File_2_Dx2_Related_Payment_Frequency = doc_Ext_File_2_Dx2_Related_Contract.newFieldInGroup("doc_Ext_File_2_Dx2_Related_Payment_Frequency", 
            "DX2-RELATED-PAYMENT-FREQUENCY", FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Related_Tiaa_Issue_State = doc_Ext_File_2_Dx2_Related_Contract.newFieldInGroup("doc_Ext_File_2_Dx2_Related_Tiaa_Issue_State", 
            "DX2-RELATED-TIAA-ISSUE-STATE", FieldType.STRING, 2);
        doc_Ext_File_2_Dx2_First_Tpa_Ipro_Ind = doc_Ext_File_2_Dx2_Related_Contract.newFieldInGroup("doc_Ext_File_2_Dx2_First_Tpa_Ipro_Ind", "DX2-FIRST-TPA-IPRO-IND", 
            FieldType.STRING, 1);
        doc_Ext_File_2_Dx2_Fund_Source_1 = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Fund_Source_1", "DX2-FUND-SOURCE-1", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Fund_Source_2 = doc_Ext_File_2Redef1.newFieldInGroup("doc_Ext_File_2_Dx2_Fund_Source_2", "DX2-FUND-SOURCE-2", FieldType.STRING, 
            1);
        doc_Ext_File_2_Dx2_Fund_Cde_2 = doc_Ext_File_2Redef1.newFieldArrayInGroup("doc_Ext_File_2_Dx2_Fund_Cde_2", "DX2-FUND-CDE-2", FieldType.STRING, 
            10, new DbsArrayController(1,100));
        doc_Ext_File_2_Dx2_Alloc_Pct_2 = doc_Ext_File_2Redef1.newFieldArrayInGroup("doc_Ext_File_2_Dx2_Alloc_Pct_2", "DX2-ALLOC-PCT-2", FieldType.STRING, 
            3, new DbsArrayController(1,100));
        doc_Ext_File_2Redef10 = newGroupInRecord("doc_Ext_File_2Redef10", "Redefines", doc_Ext_File_2);
        doc_Ext_File_2_Doc_Ext2_Data_1 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_1", "DOC-EXT2-DATA-1", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_2 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_2", "DOC-EXT2-DATA-2", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_3 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_3", "DOC-EXT2-DATA-3", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_4 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_4", "DOC-EXT2-DATA-4", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_5 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_5", "DOC-EXT2-DATA-5", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_6 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_6", "DOC-EXT2-DATA-6", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_7 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_7", "DOC-EXT2-DATA-7", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_8 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_8", "DOC-EXT2-DATA-8", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_9 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_9", "DOC-EXT2-DATA-9", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_10 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_10", "DOC-EXT2-DATA-10", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_11 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_11", "DOC-EXT2-DATA-11", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_12 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_12", "DOC-EXT2-DATA-12", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_13 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_13", "DOC-EXT2-DATA-13", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_14 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_14", "DOC-EXT2-DATA-14", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_15 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_15", "DOC-EXT2-DATA-15", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_16 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_16", "DOC-EXT2-DATA-16", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_17 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_17", "DOC-EXT2-DATA-17", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_18 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_18", "DOC-EXT2-DATA-18", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_19 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_19", "DOC-EXT2-DATA-19", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_20 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_20", "DOC-EXT2-DATA-20", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_21 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_21", "DOC-EXT2-DATA-21", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_22 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_22", "DOC-EXT2-DATA-22", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_23 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_23", "DOC-EXT2-DATA-23", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_24 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_24", "DOC-EXT2-DATA-24", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_25 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_25", "DOC-EXT2-DATA-25", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_26 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_26", "DOC-EXT2-DATA-26", FieldType.STRING, 
            250);
        doc_Ext_File_2_Doc_Ext2_Data_27 = doc_Ext_File_2Redef10.newFieldInGroup("doc_Ext_File_2_Doc_Ext2_Data_27", "DOC-EXT2-DATA-27", FieldType.STRING, 
            72);

        this.setRecordName("LdaAppl1157");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAppl1157() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
