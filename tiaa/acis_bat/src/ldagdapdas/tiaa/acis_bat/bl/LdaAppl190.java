/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:51:02 PM
**        * FROM NATURAL LDA     : APPL190
************************************************************
**        * FILE NAME            : LdaAppl190.java
**        * CLASS NAME           : LdaAppl190
**        * INSTANCE NAME        : LdaAppl190
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl190 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Grm_State_Table;
    private DbsField pnd_Grm_State_Table_Pnd_Grm_State_Data;
    private DbsGroup pnd_Grm_State_Table_Pnd_Grm_State_DataRedef1;
    private DbsField pnd_Grm_State_Table_Pnd_Grm_State_Cd;
    private DbsField pnd_Grm_State_Table_Pnd_Filler_State;
    private DbsField pnd_Grm_State_Table_Pnd_Grm_Form_Suffix;

    public DbsGroup getPnd_Grm_State_Table() { return pnd_Grm_State_Table; }

    public DbsField getPnd_Grm_State_Table_Pnd_Grm_State_Data() { return pnd_Grm_State_Table_Pnd_Grm_State_Data; }

    public DbsGroup getPnd_Grm_State_Table_Pnd_Grm_State_DataRedef1() { return pnd_Grm_State_Table_Pnd_Grm_State_DataRedef1; }

    public DbsField getPnd_Grm_State_Table_Pnd_Grm_State_Cd() { return pnd_Grm_State_Table_Pnd_Grm_State_Cd; }

    public DbsField getPnd_Grm_State_Table_Pnd_Filler_State() { return pnd_Grm_State_Table_Pnd_Filler_State; }

    public DbsField getPnd_Grm_State_Table_Pnd_Grm_Form_Suffix() { return pnd_Grm_State_Table_Pnd_Grm_Form_Suffix; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Grm_State_Table = newGroupArrayInRecord("pnd_Grm_State_Table", "#GRM-STATE-TABLE", new DbsArrayController(1,54));
        pnd_Grm_State_Table_Pnd_Grm_State_Data = pnd_Grm_State_Table.newFieldInGroup("pnd_Grm_State_Table_Pnd_Grm_State_Data", "#GRM-STATE-DATA", FieldType.STRING, 
            66);
        pnd_Grm_State_Table_Pnd_Grm_State_DataRedef1 = pnd_Grm_State_Table.newGroupInGroup("pnd_Grm_State_Table_Pnd_Grm_State_DataRedef1", "Redefines", 
            pnd_Grm_State_Table_Pnd_Grm_State_Data);
        pnd_Grm_State_Table_Pnd_Grm_State_Cd = pnd_Grm_State_Table_Pnd_Grm_State_DataRedef1.newFieldInGroup("pnd_Grm_State_Table_Pnd_Grm_State_Cd", "#GRM-STATE-CD", 
            FieldType.STRING, 2);
        pnd_Grm_State_Table_Pnd_Filler_State = pnd_Grm_State_Table_Pnd_Grm_State_DataRedef1.newFieldInGroup("pnd_Grm_State_Table_Pnd_Filler_State", "#FILLER-STATE", 
            FieldType.STRING, 1);
        pnd_Grm_State_Table_Pnd_Grm_Form_Suffix = pnd_Grm_State_Table_Pnd_Grm_State_DataRedef1.newFieldArrayInGroup("pnd_Grm_State_Table_Pnd_Grm_Form_Suffix", 
            "#GRM-FORM-SUFFIX", FieldType.STRING, 3, new DbsArrayController(1,20));

        this.setRecordName("LdaAppl190");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(1).setInitialValue("AL, A,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(2).setInitialValue("AK,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(3).setInitialValue("AZ, A,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(4).setInitialValue("AR,  ,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(5).setInitialValue("CA, A,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(6).setInitialValue("CO, A,AA,AA,AA,AA,AA,AA,AA,  ,  , A, A,BB,BB,BB,BB,BB,BB,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(7).setInitialValue("CT, A,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(8).setInitialValue("DE,  ,  , A,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(9).setInitialValue("DC,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(10).setInitialValue("FL,  ,  ,  ,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,AA,AA,AA,AA,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(11).setInitialValue("GA,  ,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(12).setInitialValue("HI, A,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(13).setInitialValue("ID, A,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(14).setInitialValue("IL,AA,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,AA,AA,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(15).setInitialValue("IN,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(16).setInitialValue("IA,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(17).setInitialValue("KS, A,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(18).setInitialValue("KY,  ,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(19).setInitialValue("LA,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(20).setInitialValue("ME, A,AA,AA,AA,AA,AA,AA,AA,  ,  , A, A,AA,AA,AA,AA,AA,AA,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(21).setInitialValue("MD,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,AA,AA,AA, A, A,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(22).setInitialValue("MA,  ,  ,  ,  ,  ,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,, A, A,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(23).setInitialValue("MI,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(24).setInitialValue("MN,  ,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(25).setInitialValue("MS,  ,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(26).setInitialValue("MO, A,AA,AA,AA,AA,AA,AA,AA,  ,  , A, A, A, A, A, A,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(27).setInitialValue("MT,  ,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,A ,A,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(28).setInitialValue("NE,AA,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(29).setInitialValue("NV, A,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(30).setInitialValue("NH,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(31).setInitialValue("NJ,  ,  ,  ,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(32).setInitialValue("NM, A,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(33).setInitialValue("NY,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(34).setInitialValue("NC, A,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(35).setInitialValue("ND,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(36).setInitialValue("OH, A,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(37).setInitialValue("OK,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(38).setInitialValue("OR,AA,AA,AA,AA,AA,AA,AA,AA,  ,  ,AA,AA,AA,AA,AA,AA,AA,AA,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(39).setInitialValue("PA,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(40).setInitialValue("PR,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(41).setInitialValue("RI, A,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(42).setInitialValue("SC,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(43).setInitialValue("SD, A,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(44).setInitialValue("TN,  ,AA,  ,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(45).setInitialValue("TX,AC,CC,CC,AA,AA,AA,AA,AA,  ,  ,BC,BC,CC,CC,CC,CC,CA,CA,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(46).setInitialValue("UT,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(47).setInitialValue("VT,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(48).setInitialValue("VA,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(49).setInitialValue("VI,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(50).setInitialValue("WA,  ,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(51).setInitialValue("WV,AA,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(52).setInitialValue("WI, A,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(53).setInitialValue("WY, A,AA,AA,AA,AA,AA,AA,AA,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
        pnd_Grm_State_Table_Pnd_Grm_State_Data.getValue(54).setInitialValue("FN,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,,  ,  ,  ,  ,");
    }

    // Constructor
    public LdaAppl190() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
