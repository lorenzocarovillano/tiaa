/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:47 PM
**        * FROM NATURAL LDA     : APPBL180
************************************************************
**        * FILE NAME            : LdaAppbl180.java
**        * CLASS NAME           : LdaAppbl180
**        * INSTANCE NAME        : LdaAppbl180
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppbl180 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Mdm_Data;
    private DbsField pnd_Mdm_Data_Requestor;
    private DbsField pnd_Mdm_Data_Function_Code;
    private DbsField pnd_Mdm_Data_Ph_Unique_Id_Nbr;
    private DbsField pnd_Mdm_Data_Ph_Social_Security_No;
    private DbsField pnd_Mdm_Data_Prefix;
    private DbsField pnd_Mdm_Data_Last_Name;
    private DbsField pnd_Mdm_Data_First_Name;
    private DbsField pnd_Mdm_Data_Middle_Name;
    private DbsField pnd_Mdm_Data_Suffix;
    private DbsField pnd_Mdm_Data_Date_Of_Birth;
    private DbsField pnd_Mdm_Data_Foreign_Soc_Sec_Nbr;
    private DbsField pnd_Mdm_Data_Sex_Code;
    private DbsField pnd_Mdm_Data_Date_Of_Death;
    private DbsField pnd_Mdm_Data_Address_Line_1;
    private DbsField pnd_Mdm_Data_Address_Line_2;
    private DbsField pnd_Mdm_Data_Address_Line_3;
    private DbsField pnd_Mdm_Data_Address_Line_4;
    private DbsField pnd_Mdm_Data_Address_Line_5;
    private DbsField pnd_Mdm_Data_Zip_Code;
    private DbsField pnd_Mdm_Data_City;
    private DbsField pnd_Mdm_Data_State;
    private DbsField pnd_Mdm_Data_Country;
    private DbsField pnd_Mdm_Data_Addr_Type;
    private DbsField pnd_Mdm_Data_Ssn_Tin_Indicator;
    private DbsField pnd_Mdm_Data_Plan_Number;
    private DbsField pnd_Mdm_Data_Subplan_Number;
    private DbsField pnd_Mdm_Data_Orig_Tiaa_Number;
    private DbsField pnd_Mdm_Data_Orig_Cref_Number;

    public DbsGroup getPnd_Mdm_Data() { return pnd_Mdm_Data; }

    public DbsField getPnd_Mdm_Data_Requestor() { return pnd_Mdm_Data_Requestor; }

    public DbsField getPnd_Mdm_Data_Function_Code() { return pnd_Mdm_Data_Function_Code; }

    public DbsField getPnd_Mdm_Data_Ph_Unique_Id_Nbr() { return pnd_Mdm_Data_Ph_Unique_Id_Nbr; }

    public DbsField getPnd_Mdm_Data_Ph_Social_Security_No() { return pnd_Mdm_Data_Ph_Social_Security_No; }

    public DbsField getPnd_Mdm_Data_Prefix() { return pnd_Mdm_Data_Prefix; }

    public DbsField getPnd_Mdm_Data_Last_Name() { return pnd_Mdm_Data_Last_Name; }

    public DbsField getPnd_Mdm_Data_First_Name() { return pnd_Mdm_Data_First_Name; }

    public DbsField getPnd_Mdm_Data_Middle_Name() { return pnd_Mdm_Data_Middle_Name; }

    public DbsField getPnd_Mdm_Data_Suffix() { return pnd_Mdm_Data_Suffix; }

    public DbsField getPnd_Mdm_Data_Date_Of_Birth() { return pnd_Mdm_Data_Date_Of_Birth; }

    public DbsField getPnd_Mdm_Data_Foreign_Soc_Sec_Nbr() { return pnd_Mdm_Data_Foreign_Soc_Sec_Nbr; }

    public DbsField getPnd_Mdm_Data_Sex_Code() { return pnd_Mdm_Data_Sex_Code; }

    public DbsField getPnd_Mdm_Data_Date_Of_Death() { return pnd_Mdm_Data_Date_Of_Death; }

    public DbsField getPnd_Mdm_Data_Address_Line_1() { return pnd_Mdm_Data_Address_Line_1; }

    public DbsField getPnd_Mdm_Data_Address_Line_2() { return pnd_Mdm_Data_Address_Line_2; }

    public DbsField getPnd_Mdm_Data_Address_Line_3() { return pnd_Mdm_Data_Address_Line_3; }

    public DbsField getPnd_Mdm_Data_Address_Line_4() { return pnd_Mdm_Data_Address_Line_4; }

    public DbsField getPnd_Mdm_Data_Address_Line_5() { return pnd_Mdm_Data_Address_Line_5; }

    public DbsField getPnd_Mdm_Data_Zip_Code() { return pnd_Mdm_Data_Zip_Code; }

    public DbsField getPnd_Mdm_Data_City() { return pnd_Mdm_Data_City; }

    public DbsField getPnd_Mdm_Data_State() { return pnd_Mdm_Data_State; }

    public DbsField getPnd_Mdm_Data_Country() { return pnd_Mdm_Data_Country; }

    public DbsField getPnd_Mdm_Data_Addr_Type() { return pnd_Mdm_Data_Addr_Type; }

    public DbsField getPnd_Mdm_Data_Ssn_Tin_Indicator() { return pnd_Mdm_Data_Ssn_Tin_Indicator; }

    public DbsField getPnd_Mdm_Data_Plan_Number() { return pnd_Mdm_Data_Plan_Number; }

    public DbsField getPnd_Mdm_Data_Subplan_Number() { return pnd_Mdm_Data_Subplan_Number; }

    public DbsField getPnd_Mdm_Data_Orig_Tiaa_Number() { return pnd_Mdm_Data_Orig_Tiaa_Number; }

    public DbsField getPnd_Mdm_Data_Orig_Cref_Number() { return pnd_Mdm_Data_Orig_Cref_Number; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Mdm_Data = newGroupInRecord("pnd_Mdm_Data", "#MDM-DATA");
        pnd_Mdm_Data_Requestor = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Requestor", "REQUESTOR", FieldType.STRING, 8);
        pnd_Mdm_Data_Function_Code = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Function_Code", "FUNCTION_CODE", FieldType.STRING, 3);
        pnd_Mdm_Data_Ph_Unique_Id_Nbr = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Ph_Unique_Id_Nbr", "PH_UNIQUE_ID_NBR", FieldType.STRING, 12);
        pnd_Mdm_Data_Ph_Social_Security_No = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Ph_Social_Security_No", "PH_SOCIAL_SECURITY_NO", FieldType.STRING, 
            9);
        pnd_Mdm_Data_Prefix = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Prefix", "PREFIX", FieldType.STRING, 8);
        pnd_Mdm_Data_Last_Name = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Last_Name", "LAST_NAME", FieldType.STRING, 30);
        pnd_Mdm_Data_First_Name = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_First_Name", "FIRST_NAME", FieldType.STRING, 30);
        pnd_Mdm_Data_Middle_Name = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Middle_Name", "MIDDLE_NAME", FieldType.STRING, 30);
        pnd_Mdm_Data_Suffix = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Suffix", "SUFFIX", FieldType.STRING, 8);
        pnd_Mdm_Data_Date_Of_Birth = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Date_Of_Birth", "DATE_OF_BIRTH", FieldType.STRING, 8);
        pnd_Mdm_Data_Foreign_Soc_Sec_Nbr = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Foreign_Soc_Sec_Nbr", "FOREIGN_SOC_SEC_NBR", FieldType.STRING, 9);
        pnd_Mdm_Data_Sex_Code = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Sex_Code", "SEX_CODE", FieldType.STRING, 1);
        pnd_Mdm_Data_Date_Of_Death = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Date_Of_Death", "DATE_OF_DEATH", FieldType.STRING, 8);
        pnd_Mdm_Data_Address_Line_1 = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Address_Line_1", "ADDRESS_LINE_1", FieldType.STRING, 35);
        pnd_Mdm_Data_Address_Line_2 = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Address_Line_2", "ADDRESS_LINE_2", FieldType.STRING, 35);
        pnd_Mdm_Data_Address_Line_3 = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Address_Line_3", "ADDRESS_LINE_3", FieldType.STRING, 35);
        pnd_Mdm_Data_Address_Line_4 = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Address_Line_4", "ADDRESS_LINE_4", FieldType.STRING, 35);
        pnd_Mdm_Data_Address_Line_5 = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Address_Line_5", "ADDRESS_LINE_5", FieldType.STRING, 35);
        pnd_Mdm_Data_Zip_Code = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Zip_Code", "ZIP_CODE", FieldType.STRING, 9);
        pnd_Mdm_Data_City = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_City", "CITY", FieldType.STRING, 35);
        pnd_Mdm_Data_State = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_State", "STATE", FieldType.STRING, 3);
        pnd_Mdm_Data_Country = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Country", "COUNTRY", FieldType.STRING, 50);
        pnd_Mdm_Data_Addr_Type = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Addr_Type", "ADDR_TYPE", FieldType.STRING, 1);
        pnd_Mdm_Data_Ssn_Tin_Indicator = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Ssn_Tin_Indicator", "SSN_TIN_INDICATOR", FieldType.STRING, 1);
        pnd_Mdm_Data_Plan_Number = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Plan_Number", "PLAN_NUMBER", FieldType.STRING, 6);
        pnd_Mdm_Data_Subplan_Number = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Subplan_Number", "SUBPLAN_NUMBER", FieldType.STRING, 6);
        pnd_Mdm_Data_Orig_Tiaa_Number = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Orig_Tiaa_Number", "ORIG_TIAA_NUMBER", FieldType.STRING, 10);
        pnd_Mdm_Data_Orig_Cref_Number = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Orig_Cref_Number", "ORIG_CREF_NUMBER", FieldType.STRING, 10);

        this.setRecordName("LdaAppbl180");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAppbl180() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
