/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:17:49 PM
**        * FROM NATURAL PDA     : APPA925
************************************************************
**        * FILE NAME            : PdaAppa925.java
**        * CLASS NAME           : PdaAppa925
**        * INSTANCE NAME        : PdaAppa925
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaAppa925 extends PdaBase
{
    // Properties
    private DbsField contract;
    private DbsGroup contractRedef1;
    private DbsField contract_Contract_Pfx;
    private DbsField contract_Contract_Num;
    private DbsGroup contractRedef2;
    private DbsField contract_Contract_Pfx2;
    private DbsGroup contract_Contract_Pfx2Redef3;
    private DbsField contract_Contract_Pfx2_1;
    private DbsField contract_Contract_Pfx2_2;
    private DbsField contract_Contract_Num5;
    private DbsField contract_Info;
    private DbsGroup contract_InfoRedef4;
    private DbsField contract_Info_Pnd_Contract_Type;
    private DbsField contract_Info_Pnd_Contract_Plus;
    private DbsField contract_Info_Pnd_Contract_Lob_Type;
    private DbsGroup contract_Info_Pnd_Contract_Lob_TypeRedef5;
    private DbsField contract_Info_Pnd_Product_Lob;
    private DbsField contract_Info_Pnd_Product_Lob_Type;
    private DbsField contract_Info_Pnd_Product_Code;
    private DbsField contract_Info_Pnd_Filler;
    private DbsField contract_Info_Pnd_Contract_Start;
    private DbsGroup contract_Info_Pnd_Contract_StartRedef6;
    private DbsField contract_Info_Pnd_Contract_Start_Pfx;
    private DbsField contract_Info_Pnd_Contract_Start_Num;
    private DbsGroup contract_Info_Pnd_Contract_StartRedef7;
    private DbsField contract_Info_Pnd_Contract_Start_Pfx2;
    private DbsField contract_Info_Pnd_Contract_Start_Num5;
    private DbsField contract_Info_Pnd_Contract_Start_Pfx_C;
    private DbsField contract_Info_Pnd_Contract_End;
    private DbsGroup contract_Info_Pnd_Contract_EndRedef8;
    private DbsField contract_Info_Pnd_Contract_End_Pfx;
    private DbsField contract_Info_Pnd_Contract_End_Num;
    private DbsGroup contract_Info_Pnd_Contract_EndRedef9;
    private DbsField contract_Info_Pnd_Contract_End_Pfx2;
    private DbsField contract_Info_Pnd_Contract_End_Num5;
    private DbsField contract_Info_Pnd_Contract_End_Pfx_C;

    public DbsField getContract() { return contract; }

    public DbsGroup getContractRedef1() { return contractRedef1; }

    public DbsField getContract_Contract_Pfx() { return contract_Contract_Pfx; }

    public DbsField getContract_Contract_Num() { return contract_Contract_Num; }

    public DbsGroup getContractRedef2() { return contractRedef2; }

    public DbsField getContract_Contract_Pfx2() { return contract_Contract_Pfx2; }

    public DbsGroup getContract_Contract_Pfx2Redef3() { return contract_Contract_Pfx2Redef3; }

    public DbsField getContract_Contract_Pfx2_1() { return contract_Contract_Pfx2_1; }

    public DbsField getContract_Contract_Pfx2_2() { return contract_Contract_Pfx2_2; }

    public DbsField getContract_Contract_Num5() { return contract_Contract_Num5; }

    public DbsField getContract_Info() { return contract_Info; }

    public DbsGroup getContract_InfoRedef4() { return contract_InfoRedef4; }

    public DbsField getContract_Info_Pnd_Contract_Type() { return contract_Info_Pnd_Contract_Type; }

    public DbsField getContract_Info_Pnd_Contract_Plus() { return contract_Info_Pnd_Contract_Plus; }

    public DbsField getContract_Info_Pnd_Contract_Lob_Type() { return contract_Info_Pnd_Contract_Lob_Type; }

    public DbsGroup getContract_Info_Pnd_Contract_Lob_TypeRedef5() { return contract_Info_Pnd_Contract_Lob_TypeRedef5; }

    public DbsField getContract_Info_Pnd_Product_Lob() { return contract_Info_Pnd_Product_Lob; }

    public DbsField getContract_Info_Pnd_Product_Lob_Type() { return contract_Info_Pnd_Product_Lob_Type; }

    public DbsField getContract_Info_Pnd_Product_Code() { return contract_Info_Pnd_Product_Code; }

    public DbsField getContract_Info_Pnd_Filler() { return contract_Info_Pnd_Filler; }

    public DbsField getContract_Info_Pnd_Contract_Start() { return contract_Info_Pnd_Contract_Start; }

    public DbsGroup getContract_Info_Pnd_Contract_StartRedef6() { return contract_Info_Pnd_Contract_StartRedef6; }

    public DbsField getContract_Info_Pnd_Contract_Start_Pfx() { return contract_Info_Pnd_Contract_Start_Pfx; }

    public DbsField getContract_Info_Pnd_Contract_Start_Num() { return contract_Info_Pnd_Contract_Start_Num; }

    public DbsGroup getContract_Info_Pnd_Contract_StartRedef7() { return contract_Info_Pnd_Contract_StartRedef7; }

    public DbsField getContract_Info_Pnd_Contract_Start_Pfx2() { return contract_Info_Pnd_Contract_Start_Pfx2; }

    public DbsField getContract_Info_Pnd_Contract_Start_Num5() { return contract_Info_Pnd_Contract_Start_Num5; }

    public DbsField getContract_Info_Pnd_Contract_Start_Pfx_C() { return contract_Info_Pnd_Contract_Start_Pfx_C; }

    public DbsField getContract_Info_Pnd_Contract_End() { return contract_Info_Pnd_Contract_End; }

    public DbsGroup getContract_Info_Pnd_Contract_EndRedef8() { return contract_Info_Pnd_Contract_EndRedef8; }

    public DbsField getContract_Info_Pnd_Contract_End_Pfx() { return contract_Info_Pnd_Contract_End_Pfx; }

    public DbsField getContract_Info_Pnd_Contract_End_Num() { return contract_Info_Pnd_Contract_End_Num; }

    public DbsGroup getContract_Info_Pnd_Contract_EndRedef9() { return contract_Info_Pnd_Contract_EndRedef9; }

    public DbsField getContract_Info_Pnd_Contract_End_Pfx2() { return contract_Info_Pnd_Contract_End_Pfx2; }

    public DbsField getContract_Info_Pnd_Contract_End_Num5() { return contract_Info_Pnd_Contract_End_Num5; }

    public DbsField getContract_Info_Pnd_Contract_End_Pfx_C() { return contract_Info_Pnd_Contract_End_Pfx_C; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        contract = dbsRecord.newFieldInRecord("contract", "CONTRACT", FieldType.STRING, 7);
        contract.setParameterOption(ParameterOption.ByReference);
        contractRedef1 = dbsRecord.newGroupInRecord("contractRedef1", "Redefines", contract);
        contract_Contract_Pfx = contractRedef1.newFieldInGroup("contract_Contract_Pfx", "CONTRACT-PFX", FieldType.STRING, 1);
        contract_Contract_Num = contractRedef1.newFieldInGroup("contract_Contract_Num", "CONTRACT-NUM", FieldType.STRING, 6);
        contractRedef2 = dbsRecord.newGroupInRecord("contractRedef2", "Redefines", contract);
        contract_Contract_Pfx2 = contractRedef2.newFieldInGroup("contract_Contract_Pfx2", "CONTRACT-PFX2", FieldType.STRING, 2);
        contract_Contract_Pfx2Redef3 = contractRedef2.newGroupInGroup("contract_Contract_Pfx2Redef3", "Redefines", contract_Contract_Pfx2);
        contract_Contract_Pfx2_1 = contract_Contract_Pfx2Redef3.newFieldInGroup("contract_Contract_Pfx2_1", "CONTRACT-PFX2-1", FieldType.STRING, 1);
        contract_Contract_Pfx2_2 = contract_Contract_Pfx2Redef3.newFieldInGroup("contract_Contract_Pfx2_2", "CONTRACT-PFX2-2", FieldType.STRING, 1);
        contract_Contract_Num5 = contractRedef2.newFieldInGroup("contract_Contract_Num5", "CONTRACT-NUM5", FieldType.STRING, 5);

        contract_Info = dbsRecord.newFieldInRecord("contract_Info", "CONTRACT-INFO", FieldType.STRING, 50);
        contract_Info.setParameterOption(ParameterOption.ByReference);
        contract_InfoRedef4 = dbsRecord.newGroupInRecord("contract_InfoRedef4", "Redefines", contract_Info);
        contract_Info_Pnd_Contract_Type = contract_InfoRedef4.newFieldInGroup("contract_Info_Pnd_Contract_Type", "#CONTRACT-TYPE", FieldType.STRING, 10);
        contract_Info_Pnd_Contract_Plus = contract_InfoRedef4.newFieldInGroup("contract_Info_Pnd_Contract_Plus", "#CONTRACT-PLUS", FieldType.STRING, 10);
        contract_Info_Pnd_Contract_Lob_Type = contract_InfoRedef4.newFieldInGroup("contract_Info_Pnd_Contract_Lob_Type", "#CONTRACT-LOB-TYPE", FieldType.STRING, 
            3);
        contract_Info_Pnd_Contract_Lob_TypeRedef5 = contract_InfoRedef4.newGroupInGroup("contract_Info_Pnd_Contract_Lob_TypeRedef5", "Redefines", contract_Info_Pnd_Contract_Lob_Type);
        contract_Info_Pnd_Product_Lob = contract_Info_Pnd_Contract_Lob_TypeRedef5.newFieldInGroup("contract_Info_Pnd_Product_Lob", "#PRODUCT-LOB", FieldType.STRING, 
            1);
        contract_Info_Pnd_Product_Lob_Type = contract_Info_Pnd_Contract_Lob_TypeRedef5.newFieldInGroup("contract_Info_Pnd_Product_Lob_Type", "#PRODUCT-LOB-TYPE", 
            FieldType.STRING, 2);
        contract_Info_Pnd_Product_Code = contract_InfoRedef4.newFieldInGroup("contract_Info_Pnd_Product_Code", "#PRODUCT-CODE", FieldType.STRING, 4);
        contract_Info_Pnd_Filler = contract_InfoRedef4.newFieldInGroup("contract_Info_Pnd_Filler", "#FILLER", FieldType.STRING, 1);
        contract_Info_Pnd_Contract_Start = contract_InfoRedef4.newFieldInGroup("contract_Info_Pnd_Contract_Start", "#CONTRACT-START", FieldType.STRING, 
            10);
        contract_Info_Pnd_Contract_StartRedef6 = contract_InfoRedef4.newGroupInGroup("contract_Info_Pnd_Contract_StartRedef6", "Redefines", contract_Info_Pnd_Contract_Start);
        contract_Info_Pnd_Contract_Start_Pfx = contract_Info_Pnd_Contract_StartRedef6.newFieldInGroup("contract_Info_Pnd_Contract_Start_Pfx", "#CONTRACT-START-PFX", 
            FieldType.STRING, 1);
        contract_Info_Pnd_Contract_Start_Num = contract_Info_Pnd_Contract_StartRedef6.newFieldInGroup("contract_Info_Pnd_Contract_Start_Num", "#CONTRACT-START-NUM", 
            FieldType.STRING, 6);
        contract_Info_Pnd_Contract_StartRedef7 = contract_InfoRedef4.newGroupInGroup("contract_Info_Pnd_Contract_StartRedef7", "Redefines", contract_Info_Pnd_Contract_Start);
        contract_Info_Pnd_Contract_Start_Pfx2 = contract_Info_Pnd_Contract_StartRedef7.newFieldInGroup("contract_Info_Pnd_Contract_Start_Pfx2", "#CONTRACT-START-PFX2", 
            FieldType.STRING, 2);
        contract_Info_Pnd_Contract_Start_Num5 = contract_Info_Pnd_Contract_StartRedef7.newFieldInGroup("contract_Info_Pnd_Contract_Start_Num5", "#CONTRACT-START-NUM5", 
            FieldType.STRING, 5);
        contract_Info_Pnd_Contract_Start_Pfx_C = contract_InfoRedef4.newFieldInGroup("contract_Info_Pnd_Contract_Start_Pfx_C", "#CONTRACT-START-PFX-C", 
            FieldType.STRING, 1);
        contract_Info_Pnd_Contract_End = contract_InfoRedef4.newFieldInGroup("contract_Info_Pnd_Contract_End", "#CONTRACT-END", FieldType.STRING, 10);
        contract_Info_Pnd_Contract_EndRedef8 = contract_InfoRedef4.newGroupInGroup("contract_Info_Pnd_Contract_EndRedef8", "Redefines", contract_Info_Pnd_Contract_End);
        contract_Info_Pnd_Contract_End_Pfx = contract_Info_Pnd_Contract_EndRedef8.newFieldInGroup("contract_Info_Pnd_Contract_End_Pfx", "#CONTRACT-END-PFX", 
            FieldType.STRING, 1);
        contract_Info_Pnd_Contract_End_Num = contract_Info_Pnd_Contract_EndRedef8.newFieldInGroup("contract_Info_Pnd_Contract_End_Num", "#CONTRACT-END-NUM", 
            FieldType.STRING, 6);
        contract_Info_Pnd_Contract_EndRedef9 = contract_InfoRedef4.newGroupInGroup("contract_Info_Pnd_Contract_EndRedef9", "Redefines", contract_Info_Pnd_Contract_End);
        contract_Info_Pnd_Contract_End_Pfx2 = contract_Info_Pnd_Contract_EndRedef9.newFieldInGroup("contract_Info_Pnd_Contract_End_Pfx2", "#CONTRACT-END-PFX2", 
            FieldType.STRING, 2);
        contract_Info_Pnd_Contract_End_Num5 = contract_Info_Pnd_Contract_EndRedef9.newFieldInGroup("contract_Info_Pnd_Contract_End_Num5", "#CONTRACT-END-NUM5", 
            FieldType.STRING, 5);
        contract_Info_Pnd_Contract_End_Pfx_C = contract_InfoRedef4.newFieldInGroup("contract_Info_Pnd_Contract_End_Pfx_C", "#CONTRACT-END-PFX-C", FieldType.STRING, 
            1);

        dbsRecord.reset();
    }

    // Constructors
    public PdaAppa925(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

