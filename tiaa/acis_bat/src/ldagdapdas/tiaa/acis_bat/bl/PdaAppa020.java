/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:17:47 PM
**        * FROM NATURAL PDA     : APPA020
************************************************************
**        * FILE NAME            : PdaAppa020.java
**        * CLASS NAME           : PdaAppa020
**        * INSTANCE NAME        : PdaAppa020
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaAppa020 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Parmdata;
    private DbsField pnd_Parmdata_Pnd_Start_Date;
    private DbsField pnd_Parmdata_Pnd_End_Date;
    private DbsField pnd_Parmdata_Pnd_Requestor;
    private DbsField pnd_Parmdata_Pnd_Product_Code;
    private DbsField pnd_Parmdata_Pnd_Get_Cntrct_Flag;
    private DbsField pnd_Parmdata_Pnd_Accum_Buckets_Flag;
    private DbsField pnd_Parmdata_Pnd_Use_Product_Code_Flag;
    private DbsField pnd_Parmdata_Pnd_Response_Message;
    private DbsField pnd_Parmdata_Pnd_Other_Activity_Message;
    private DbsGroup pnd_Parmdata_Pnd_Buckets_To_Get_G;
    private DbsField pnd_Parmdata_Pnd_Each_Bucket_Name;
    private DbsField pnd_Parmdata_Pnd_Where_Tally;
    private DbsField pnd_Parmdata_Pnd_Accum_Tally;
    private DbsField pnd_Parmdata_Pnd_Prev_Contract;
    private DbsField pnd_Parmdata_Pnd_Last_Contract;

    public DbsGroup getPnd_Parmdata() { return pnd_Parmdata; }

    public DbsField getPnd_Parmdata_Pnd_Start_Date() { return pnd_Parmdata_Pnd_Start_Date; }

    public DbsField getPnd_Parmdata_Pnd_End_Date() { return pnd_Parmdata_Pnd_End_Date; }

    public DbsField getPnd_Parmdata_Pnd_Requestor() { return pnd_Parmdata_Pnd_Requestor; }

    public DbsField getPnd_Parmdata_Pnd_Product_Code() { return pnd_Parmdata_Pnd_Product_Code; }

    public DbsField getPnd_Parmdata_Pnd_Get_Cntrct_Flag() { return pnd_Parmdata_Pnd_Get_Cntrct_Flag; }

    public DbsField getPnd_Parmdata_Pnd_Accum_Buckets_Flag() { return pnd_Parmdata_Pnd_Accum_Buckets_Flag; }

    public DbsField getPnd_Parmdata_Pnd_Use_Product_Code_Flag() { return pnd_Parmdata_Pnd_Use_Product_Code_Flag; }

    public DbsField getPnd_Parmdata_Pnd_Response_Message() { return pnd_Parmdata_Pnd_Response_Message; }

    public DbsField getPnd_Parmdata_Pnd_Other_Activity_Message() { return pnd_Parmdata_Pnd_Other_Activity_Message; }

    public DbsGroup getPnd_Parmdata_Pnd_Buckets_To_Get_G() { return pnd_Parmdata_Pnd_Buckets_To_Get_G; }

    public DbsField getPnd_Parmdata_Pnd_Each_Bucket_Name() { return pnd_Parmdata_Pnd_Each_Bucket_Name; }

    public DbsField getPnd_Parmdata_Pnd_Where_Tally() { return pnd_Parmdata_Pnd_Where_Tally; }

    public DbsField getPnd_Parmdata_Pnd_Accum_Tally() { return pnd_Parmdata_Pnd_Accum_Tally; }

    public DbsField getPnd_Parmdata_Pnd_Prev_Contract() { return pnd_Parmdata_Pnd_Prev_Contract; }

    public DbsField getPnd_Parmdata_Pnd_Last_Contract() { return pnd_Parmdata_Pnd_Last_Contract; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Parmdata = dbsRecord.newGroupInRecord("pnd_Parmdata", "#PARMDATA");
        pnd_Parmdata.setParameterOption(ParameterOption.ByReference);
        pnd_Parmdata_Pnd_Start_Date = pnd_Parmdata.newFieldInGroup("pnd_Parmdata_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);
        pnd_Parmdata_Pnd_End_Date = pnd_Parmdata.newFieldInGroup("pnd_Parmdata_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);
        pnd_Parmdata_Pnd_Requestor = pnd_Parmdata.newFieldInGroup("pnd_Parmdata_Pnd_Requestor", "#REQUESTOR", FieldType.STRING, 12);
        pnd_Parmdata_Pnd_Product_Code = pnd_Parmdata.newFieldInGroup("pnd_Parmdata_Pnd_Product_Code", "#PRODUCT-CODE", FieldType.STRING, 4);
        pnd_Parmdata_Pnd_Get_Cntrct_Flag = pnd_Parmdata.newFieldInGroup("pnd_Parmdata_Pnd_Get_Cntrct_Flag", "#GET-CNTRCT-FLAG", FieldType.STRING, 15);
        pnd_Parmdata_Pnd_Accum_Buckets_Flag = pnd_Parmdata.newFieldInGroup("pnd_Parmdata_Pnd_Accum_Buckets_Flag", "#ACCUM-BUCKETS-FLAG", FieldType.STRING, 
            15);
        pnd_Parmdata_Pnd_Use_Product_Code_Flag = pnd_Parmdata.newFieldInGroup("pnd_Parmdata_Pnd_Use_Product_Code_Flag", "#USE-PRODUCT-CODE-FLAG", FieldType.STRING, 
            15);
        pnd_Parmdata_Pnd_Response_Message = pnd_Parmdata.newFieldInGroup("pnd_Parmdata_Pnd_Response_Message", "#RESPONSE-MESSAGE", FieldType.STRING, 25);
        pnd_Parmdata_Pnd_Other_Activity_Message = pnd_Parmdata.newFieldInGroup("pnd_Parmdata_Pnd_Other_Activity_Message", "#OTHER-ACTIVITY-MESSAGE", FieldType.STRING, 
            25);
        pnd_Parmdata_Pnd_Buckets_To_Get_G = pnd_Parmdata.newGroupArrayInGroup("pnd_Parmdata_Pnd_Buckets_To_Get_G", "#BUCKETS-TO-GET-G", new DbsArrayController(1,
            99));
        pnd_Parmdata_Pnd_Each_Bucket_Name = pnd_Parmdata_Pnd_Buckets_To_Get_G.newFieldInGroup("pnd_Parmdata_Pnd_Each_Bucket_Name", "#EACH-BUCKET-NAME", 
            FieldType.STRING, 15);
        pnd_Parmdata_Pnd_Where_Tally = pnd_Parmdata_Pnd_Buckets_To_Get_G.newFieldArrayInGroup("pnd_Parmdata_Pnd_Where_Tally", "#WHERE-TALLY", FieldType.NUMERIC, 
            2, new DbsArrayController(1,5));
        pnd_Parmdata_Pnd_Accum_Tally = pnd_Parmdata_Pnd_Buckets_To_Get_G.newFieldInGroup("pnd_Parmdata_Pnd_Accum_Tally", "#ACCUM-TALLY", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Parmdata_Pnd_Prev_Contract = pnd_Parmdata.newFieldInGroup("pnd_Parmdata_Pnd_Prev_Contract", "#PREV-CONTRACT", FieldType.STRING, 7);
        pnd_Parmdata_Pnd_Last_Contract = pnd_Parmdata.newFieldInGroup("pnd_Parmdata_Pnd_Last_Contract", "#LAST-CONTRACT", FieldType.STRING, 7);

        dbsRecord.reset();
    }

    // Constructors
    public PdaAppa020(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

