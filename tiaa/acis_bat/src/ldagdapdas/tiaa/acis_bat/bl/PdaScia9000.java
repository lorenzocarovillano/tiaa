/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:23:23 PM
**        * FROM NATURAL PDA     : SCIA9000
************************************************************
**        * FILE NAME            : PdaScia9000.java
**        * CLASS NAME           : PdaScia9000
**        * INSTANCE NAME        : PdaScia9000
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaScia9000 extends PdaBase
{
    // Properties
    private DbsGroup scia9000;
    private DbsField scia9000_Pnd_Return_Code;
    private DbsField scia9000_Pnd_Return_Msg;
    private DbsField scia9000_Pnd_More_Data;
    private DbsField scia9000_Pnd_Iteration_Data;
    private DbsField scia9000_Pnd_Retry;
    private DbsField scia9000_Pnd_System_Requestor;
    private DbsField scia9000_Pnd_Racf_Id;
    private DbsField scia9000_Pnd_Processing_Location;
    private DbsField scia9000_Pnd_Print_Location;
    private DbsField scia9000_Pnd_Processing_Type;
    private DbsField scia9000_Pnd_Ppg_Code;
    private DbsField scia9000_Pnd_Pull_Code;
    private DbsField scia9000_Pnd_Bypass_Rules;
    private DbsField scia9000_Pnd_Companion_Ind;
    private DbsField scia9000_Pnd_Divorce_Ind;
    private DbsField scia9000_Pnd_Enrollment_Type;
    private DbsField scia9000_Pnd_Additional_Cref_Rqst;
    private DbsField scia9000_Pnd_Entry_Date;
    private DbsField scia9000_Pnd_Date_App_Recvd;
    private DbsField scia9000_Pnd_Date_Prem_Recvd;
    private DbsField scia9000_Pnd_Annuity_Start_Date;
    private DbsField scia9000_Pnd_Tiaa_Age_1st_Pymnt;
    private DbsField scia9000_Pnd_Cref_Age_1st_Pymnt;
    private DbsField scia9000_Pnd_Tiaa_Date_Of_Issue;
    private DbsField scia9000_Pnd_Cref_Date_Of_Issue;
    private DbsField scia9000_Pnd_Pf_Key;
    private DbsField scia9000_Pnd_Process_Ind;
    private DbsField scia9000_Pnd_Tiaa_Contract_No;
    private DbsField scia9000_Pnd_Cref_Certificate_No;
    private DbsField scia9000_Pnd_Rl_Contract_No;
    private DbsField scia9000_Pnd_Loan_Ind;
    private DbsField scia9000_Pnd_Contract_Type;
    private DbsField scia9000_Pnd_Lob;
    private DbsField scia9000_Pnd_Lob_Type;
    private DbsField scia9000_Pnd_Product_Code;
    private DbsField scia9000_Pnd_Vesting_Code;
    private DbsField scia9000_Pnd_Vesting_Code_Override;
    private DbsField scia9000_Pnd_Mit_Create_Folder;
    private DbsField scia9000_Pnd_Mit_Unit;
    private DbsField scia9000_Pnd_Mit_Wpid;
    private DbsField scia9000_Pnd_Mit_Status;
    private DbsField scia9000_Pnd_Mit_Log_Date_Time_Parent;
    private DbsField scia9000_Pnd_Mit_Log_Date_Time_Subrqst;
    private DbsField scia9000_Pnd_Pin_Nbr;
    private DbsField scia9000_Pnd_Ssn;
    private DbsField scia9000_Pnd_Prefix;
    private DbsField scia9000_Pnd_Last_Name;
    private DbsField scia9000_Pnd_First_Name;
    private DbsField scia9000_Pnd_Middle_Name;
    private DbsField scia9000_Pnd_Suffix;
    private DbsField scia9000_Pnd_Date_Of_Birth;
    private DbsField scia9000_Pnd_Sex;
    private DbsField scia9000_Pnd_Address_Line;
    private DbsField scia9000_Pnd_City;
    private DbsField scia9000_Pnd_State;
    private DbsField scia9000_Pnd_State_Nbr;
    private DbsField scia9000_Pnd_Zip_Code;
    private DbsField scia9000_Pnd_Email_Address;
    private DbsField scia9000_Pnd_Phone;
    private DbsField scia9000_Pnd_Res_Address_Line;
    private DbsField scia9000_Pnd_Res_City;
    private DbsField scia9000_Pnd_Res_State;
    private DbsField scia9000_Pnd_Res_Country_Code;
    private DbsField scia9000_Pnd_Res_Zip_Code;
    private DbsField scia9000_Pnd_Res_Phone;
    private DbsField scia9000_Pnd_Allocation_Fmt;
    private DbsField scia9000_Pnd_Allocation;
    private DbsField scia9000_Pnd_Allocation_Fund_Ticker;
    private DbsField scia9000_Pnd_Allocation_Model_Type;
    private DbsField scia9000_Pnd_Alloc_Discrepancy_Ind;
    private DbsField scia9000_Pnd_Bene_Type;
    private DbsField scia9000_Pnd_Bene_Ssn;
    private DbsField scia9000_Pnd_Bene_Name;
    private DbsField scia9000_Pnd_Bene_Extended_Name;
    private DbsField scia9000_Pnd_Bene_Relationship;
    private DbsField scia9000_Pnd_Bene_Relationship_Code;
    private DbsField scia9000_Pnd_Bene_Date_Of_Birth;
    private DbsField scia9000_Pnd_Bene_Alloc_Pct;
    private DbsField scia9000_Pnd_Bene_Numerator;
    private DbsField scia9000_Pnd_Bene_Denominator;
    private DbsField scia9000_Pnd_Bene_Special_Text_Ind;
    private DbsField scia9000_Pnd_Bene_Special_Text;
    private DbsField scia9000_Pnd_Bene_Addr1;
    private DbsField scia9000_Pnd_Bene_Addr2;
    private DbsField scia9000_Pnd_Bene_Addr3_City;
    private DbsField scia9000_Pnd_Bene_State;
    private DbsField scia9000_Pnd_Bene_Zip;
    private DbsField scia9000_Pnd_Bene_Phone;
    private DbsField scia9000_Pnd_Bene_Gender;
    private DbsField scia9000_Pnd_Bene_Country;
    private DbsField scia9000_Pnd_Bene_Mos_Ind;
    private DbsField scia9000_Pnd_Bene_Estate;
    private DbsField scia9000_Pnd_Bene_Trust;
    private DbsField scia9000_Pnd_Bene_Category;
    private DbsField scia9000_Pnd_Isn;
    private DbsField scia9000_Pnd_Field1;
    private DbsField scia9000_Pnd_Field2;
    private DbsField scia9000_Pnd_Field3;
    private DbsField scia9000_Pnd_Sg_Plan_Id;
    private DbsField scia9000_Pnd_Sg_Plan_No;
    private DbsField scia9000_Pnd_Sg_Subplan_No;
    private DbsField scia9000_Pnd_Sg_Part_Ext;
    private DbsField scia9000_Pnd_Sg_Divsub;
    private DbsField scia9000_Pnd_Sg_Text_Udf_1;
    private DbsField scia9000_Pnd_Sg_Text_Udf_2;
    private DbsField scia9000_Pnd_Sg_Text_Udf_3;
    private DbsField scia9000_Pnd_Irc_Section_Code;
    private DbsField scia9000_Pnd_Irc_Section_Grp_Code;
    private DbsField scia9000_Pnd_Issue_State;
    private DbsField scia9000_Pnd_Mailing_Instr;
    private DbsField scia9000_Pnd_Sg_Replacement_Ind;
    private DbsField scia9000_Pnd_Sg_Register_Id;
    private DbsField scia9000_Pnd_Sg_Agent_Racf_Id;
    private DbsField scia9000_Pnd_Sg_Exempt_Ind;
    private DbsField scia9000_Pnd_Sg_Insurer_1;
    private DbsField scia9000_Pnd_Sg_Insurer_2;
    private DbsField scia9000_Pnd_Sg_Insurer_3;
    private DbsField scia9000_Pnd_Sg_1035_Exch_Ind_1;
    private DbsField scia9000_Pnd_Sg_1035_Exch_Ind_2;
    private DbsField scia9000_Pnd_Sg_1035_Exch_Ind_3;
    private DbsField scia9000_Pnd_Indicators;
    private DbsGroup scia9000_Pnd_IndicatorsRedef1;
    private DbsField scia9000_Pnd_Ica_Ind;
    private DbsField scia9000_Pnd_Cip_Ind;
    private DbsField scia9000_Pnd_Erisa_Ind;
    private DbsField scia9000_Pnd_Omni_Account_Issuance;
    private DbsField scia9000_Pnd_Oia_Ind;
    private DbsField scia9000_Pnd_Incmpl_Acct_Ind;
    private DbsField scia9000_Pnd_As_Ind;
    private DbsField scia9000_Pnd_Curr_Def_Option;
    private DbsField scia9000_Pnd_Curr_Def_Amt;
    private DbsField scia9000_Pnd_Incr_Option;
    private DbsField scia9000_Pnd_Incr_Amt;
    private DbsField scia9000_Pnd_Max_Pct;
    private DbsField scia9000_Pnd_Optout_Days;
    private DbsField scia9000_Pnd_Orchestration_Id;
    private DbsField scia9000_Pnd_Mail_Addr_Country_Cd;
    private DbsField scia9000_Pnd_Legal_Ann_Option;
    private DbsField scia9000_Pnd_Substitution_Contract_Ind;
    private DbsField scia9000_Pnd_Conversion_Issue_State;
    private DbsField scia9000_Pnd_Deceased_Ind;
    private DbsField scia9000_Pnd_Non_Proprietary_Pkg_Ind;
    private DbsField scia9000_Pnd_Decedent_Contract;
    private DbsField scia9000_Pnd_Relation_To_Decedent;
    private DbsField scia9000_Pnd_Ssn_Tin_Ind;
    private DbsField scia9000_Pnd_Oneira_Acct_No;
    private DbsField scia9000_Pnd_Fund_Source_1;
    private DbsField scia9000_Pnd_Fund_Source_2;
    private DbsField scia9000_Pnd_Allocation_Fund_Ticker_2;
    private DbsField scia9000_Pnd_Allocation_2;

    public DbsGroup getScia9000() { return scia9000; }

    public DbsField getScia9000_Pnd_Return_Code() { return scia9000_Pnd_Return_Code; }

    public DbsField getScia9000_Pnd_Return_Msg() { return scia9000_Pnd_Return_Msg; }

    public DbsField getScia9000_Pnd_More_Data() { return scia9000_Pnd_More_Data; }

    public DbsField getScia9000_Pnd_Iteration_Data() { return scia9000_Pnd_Iteration_Data; }

    public DbsField getScia9000_Pnd_Retry() { return scia9000_Pnd_Retry; }

    public DbsField getScia9000_Pnd_System_Requestor() { return scia9000_Pnd_System_Requestor; }

    public DbsField getScia9000_Pnd_Racf_Id() { return scia9000_Pnd_Racf_Id; }

    public DbsField getScia9000_Pnd_Processing_Location() { return scia9000_Pnd_Processing_Location; }

    public DbsField getScia9000_Pnd_Print_Location() { return scia9000_Pnd_Print_Location; }

    public DbsField getScia9000_Pnd_Processing_Type() { return scia9000_Pnd_Processing_Type; }

    public DbsField getScia9000_Pnd_Ppg_Code() { return scia9000_Pnd_Ppg_Code; }

    public DbsField getScia9000_Pnd_Pull_Code() { return scia9000_Pnd_Pull_Code; }

    public DbsField getScia9000_Pnd_Bypass_Rules() { return scia9000_Pnd_Bypass_Rules; }

    public DbsField getScia9000_Pnd_Companion_Ind() { return scia9000_Pnd_Companion_Ind; }

    public DbsField getScia9000_Pnd_Divorce_Ind() { return scia9000_Pnd_Divorce_Ind; }

    public DbsField getScia9000_Pnd_Enrollment_Type() { return scia9000_Pnd_Enrollment_Type; }

    public DbsField getScia9000_Pnd_Additional_Cref_Rqst() { return scia9000_Pnd_Additional_Cref_Rqst; }

    public DbsField getScia9000_Pnd_Entry_Date() { return scia9000_Pnd_Entry_Date; }

    public DbsField getScia9000_Pnd_Date_App_Recvd() { return scia9000_Pnd_Date_App_Recvd; }

    public DbsField getScia9000_Pnd_Date_Prem_Recvd() { return scia9000_Pnd_Date_Prem_Recvd; }

    public DbsField getScia9000_Pnd_Annuity_Start_Date() { return scia9000_Pnd_Annuity_Start_Date; }

    public DbsField getScia9000_Pnd_Tiaa_Age_1st_Pymnt() { return scia9000_Pnd_Tiaa_Age_1st_Pymnt; }

    public DbsField getScia9000_Pnd_Cref_Age_1st_Pymnt() { return scia9000_Pnd_Cref_Age_1st_Pymnt; }

    public DbsField getScia9000_Pnd_Tiaa_Date_Of_Issue() { return scia9000_Pnd_Tiaa_Date_Of_Issue; }

    public DbsField getScia9000_Pnd_Cref_Date_Of_Issue() { return scia9000_Pnd_Cref_Date_Of_Issue; }

    public DbsField getScia9000_Pnd_Pf_Key() { return scia9000_Pnd_Pf_Key; }

    public DbsField getScia9000_Pnd_Process_Ind() { return scia9000_Pnd_Process_Ind; }

    public DbsField getScia9000_Pnd_Tiaa_Contract_No() { return scia9000_Pnd_Tiaa_Contract_No; }

    public DbsField getScia9000_Pnd_Cref_Certificate_No() { return scia9000_Pnd_Cref_Certificate_No; }

    public DbsField getScia9000_Pnd_Rl_Contract_No() { return scia9000_Pnd_Rl_Contract_No; }

    public DbsField getScia9000_Pnd_Loan_Ind() { return scia9000_Pnd_Loan_Ind; }

    public DbsField getScia9000_Pnd_Contract_Type() { return scia9000_Pnd_Contract_Type; }

    public DbsField getScia9000_Pnd_Lob() { return scia9000_Pnd_Lob; }

    public DbsField getScia9000_Pnd_Lob_Type() { return scia9000_Pnd_Lob_Type; }

    public DbsField getScia9000_Pnd_Product_Code() { return scia9000_Pnd_Product_Code; }

    public DbsField getScia9000_Pnd_Vesting_Code() { return scia9000_Pnd_Vesting_Code; }

    public DbsField getScia9000_Pnd_Vesting_Code_Override() { return scia9000_Pnd_Vesting_Code_Override; }

    public DbsField getScia9000_Pnd_Mit_Create_Folder() { return scia9000_Pnd_Mit_Create_Folder; }

    public DbsField getScia9000_Pnd_Mit_Unit() { return scia9000_Pnd_Mit_Unit; }

    public DbsField getScia9000_Pnd_Mit_Wpid() { return scia9000_Pnd_Mit_Wpid; }

    public DbsField getScia9000_Pnd_Mit_Status() { return scia9000_Pnd_Mit_Status; }

    public DbsField getScia9000_Pnd_Mit_Log_Date_Time_Parent() { return scia9000_Pnd_Mit_Log_Date_Time_Parent; }

    public DbsField getScia9000_Pnd_Mit_Log_Date_Time_Subrqst() { return scia9000_Pnd_Mit_Log_Date_Time_Subrqst; }

    public DbsField getScia9000_Pnd_Pin_Nbr() { return scia9000_Pnd_Pin_Nbr; }

    public DbsField getScia9000_Pnd_Ssn() { return scia9000_Pnd_Ssn; }

    public DbsField getScia9000_Pnd_Prefix() { return scia9000_Pnd_Prefix; }

    public DbsField getScia9000_Pnd_Last_Name() { return scia9000_Pnd_Last_Name; }

    public DbsField getScia9000_Pnd_First_Name() { return scia9000_Pnd_First_Name; }

    public DbsField getScia9000_Pnd_Middle_Name() { return scia9000_Pnd_Middle_Name; }

    public DbsField getScia9000_Pnd_Suffix() { return scia9000_Pnd_Suffix; }

    public DbsField getScia9000_Pnd_Date_Of_Birth() { return scia9000_Pnd_Date_Of_Birth; }

    public DbsField getScia9000_Pnd_Sex() { return scia9000_Pnd_Sex; }

    public DbsField getScia9000_Pnd_Address_Line() { return scia9000_Pnd_Address_Line; }

    public DbsField getScia9000_Pnd_City() { return scia9000_Pnd_City; }

    public DbsField getScia9000_Pnd_State() { return scia9000_Pnd_State; }

    public DbsField getScia9000_Pnd_State_Nbr() { return scia9000_Pnd_State_Nbr; }

    public DbsField getScia9000_Pnd_Zip_Code() { return scia9000_Pnd_Zip_Code; }

    public DbsField getScia9000_Pnd_Email_Address() { return scia9000_Pnd_Email_Address; }

    public DbsField getScia9000_Pnd_Phone() { return scia9000_Pnd_Phone; }

    public DbsField getScia9000_Pnd_Res_Address_Line() { return scia9000_Pnd_Res_Address_Line; }

    public DbsField getScia9000_Pnd_Res_City() { return scia9000_Pnd_Res_City; }

    public DbsField getScia9000_Pnd_Res_State() { return scia9000_Pnd_Res_State; }

    public DbsField getScia9000_Pnd_Res_Country_Code() { return scia9000_Pnd_Res_Country_Code; }

    public DbsField getScia9000_Pnd_Res_Zip_Code() { return scia9000_Pnd_Res_Zip_Code; }

    public DbsField getScia9000_Pnd_Res_Phone() { return scia9000_Pnd_Res_Phone; }

    public DbsField getScia9000_Pnd_Allocation_Fmt() { return scia9000_Pnd_Allocation_Fmt; }

    public DbsField getScia9000_Pnd_Allocation() { return scia9000_Pnd_Allocation; }

    public DbsField getScia9000_Pnd_Allocation_Fund_Ticker() { return scia9000_Pnd_Allocation_Fund_Ticker; }

    public DbsField getScia9000_Pnd_Allocation_Model_Type() { return scia9000_Pnd_Allocation_Model_Type; }

    public DbsField getScia9000_Pnd_Alloc_Discrepancy_Ind() { return scia9000_Pnd_Alloc_Discrepancy_Ind; }

    public DbsField getScia9000_Pnd_Bene_Type() { return scia9000_Pnd_Bene_Type; }

    public DbsField getScia9000_Pnd_Bene_Ssn() { return scia9000_Pnd_Bene_Ssn; }

    public DbsField getScia9000_Pnd_Bene_Name() { return scia9000_Pnd_Bene_Name; }

    public DbsField getScia9000_Pnd_Bene_Extended_Name() { return scia9000_Pnd_Bene_Extended_Name; }

    public DbsField getScia9000_Pnd_Bene_Relationship() { return scia9000_Pnd_Bene_Relationship; }

    public DbsField getScia9000_Pnd_Bene_Relationship_Code() { return scia9000_Pnd_Bene_Relationship_Code; }

    public DbsField getScia9000_Pnd_Bene_Date_Of_Birth() { return scia9000_Pnd_Bene_Date_Of_Birth; }

    public DbsField getScia9000_Pnd_Bene_Alloc_Pct() { return scia9000_Pnd_Bene_Alloc_Pct; }

    public DbsField getScia9000_Pnd_Bene_Numerator() { return scia9000_Pnd_Bene_Numerator; }

    public DbsField getScia9000_Pnd_Bene_Denominator() { return scia9000_Pnd_Bene_Denominator; }

    public DbsField getScia9000_Pnd_Bene_Special_Text_Ind() { return scia9000_Pnd_Bene_Special_Text_Ind; }

    public DbsField getScia9000_Pnd_Bene_Special_Text() { return scia9000_Pnd_Bene_Special_Text; }

    public DbsField getScia9000_Pnd_Bene_Addr1() { return scia9000_Pnd_Bene_Addr1; }

    public DbsField getScia9000_Pnd_Bene_Addr2() { return scia9000_Pnd_Bene_Addr2; }

    public DbsField getScia9000_Pnd_Bene_Addr3_City() { return scia9000_Pnd_Bene_Addr3_City; }

    public DbsField getScia9000_Pnd_Bene_State() { return scia9000_Pnd_Bene_State; }

    public DbsField getScia9000_Pnd_Bene_Zip() { return scia9000_Pnd_Bene_Zip; }

    public DbsField getScia9000_Pnd_Bene_Phone() { return scia9000_Pnd_Bene_Phone; }

    public DbsField getScia9000_Pnd_Bene_Gender() { return scia9000_Pnd_Bene_Gender; }

    public DbsField getScia9000_Pnd_Bene_Country() { return scia9000_Pnd_Bene_Country; }

    public DbsField getScia9000_Pnd_Bene_Mos_Ind() { return scia9000_Pnd_Bene_Mos_Ind; }

    public DbsField getScia9000_Pnd_Bene_Estate() { return scia9000_Pnd_Bene_Estate; }

    public DbsField getScia9000_Pnd_Bene_Trust() { return scia9000_Pnd_Bene_Trust; }

    public DbsField getScia9000_Pnd_Bene_Category() { return scia9000_Pnd_Bene_Category; }

    public DbsField getScia9000_Pnd_Isn() { return scia9000_Pnd_Isn; }

    public DbsField getScia9000_Pnd_Field1() { return scia9000_Pnd_Field1; }

    public DbsField getScia9000_Pnd_Field2() { return scia9000_Pnd_Field2; }

    public DbsField getScia9000_Pnd_Field3() { return scia9000_Pnd_Field3; }

    public DbsField getScia9000_Pnd_Sg_Plan_Id() { return scia9000_Pnd_Sg_Plan_Id; }

    public DbsField getScia9000_Pnd_Sg_Plan_No() { return scia9000_Pnd_Sg_Plan_No; }

    public DbsField getScia9000_Pnd_Sg_Subplan_No() { return scia9000_Pnd_Sg_Subplan_No; }

    public DbsField getScia9000_Pnd_Sg_Part_Ext() { return scia9000_Pnd_Sg_Part_Ext; }

    public DbsField getScia9000_Pnd_Sg_Divsub() { return scia9000_Pnd_Sg_Divsub; }

    public DbsField getScia9000_Pnd_Sg_Text_Udf_1() { return scia9000_Pnd_Sg_Text_Udf_1; }

    public DbsField getScia9000_Pnd_Sg_Text_Udf_2() { return scia9000_Pnd_Sg_Text_Udf_2; }

    public DbsField getScia9000_Pnd_Sg_Text_Udf_3() { return scia9000_Pnd_Sg_Text_Udf_3; }

    public DbsField getScia9000_Pnd_Irc_Section_Code() { return scia9000_Pnd_Irc_Section_Code; }

    public DbsField getScia9000_Pnd_Irc_Section_Grp_Code() { return scia9000_Pnd_Irc_Section_Grp_Code; }

    public DbsField getScia9000_Pnd_Issue_State() { return scia9000_Pnd_Issue_State; }

    public DbsField getScia9000_Pnd_Mailing_Instr() { return scia9000_Pnd_Mailing_Instr; }

    public DbsField getScia9000_Pnd_Sg_Replacement_Ind() { return scia9000_Pnd_Sg_Replacement_Ind; }

    public DbsField getScia9000_Pnd_Sg_Register_Id() { return scia9000_Pnd_Sg_Register_Id; }

    public DbsField getScia9000_Pnd_Sg_Agent_Racf_Id() { return scia9000_Pnd_Sg_Agent_Racf_Id; }

    public DbsField getScia9000_Pnd_Sg_Exempt_Ind() { return scia9000_Pnd_Sg_Exempt_Ind; }

    public DbsField getScia9000_Pnd_Sg_Insurer_1() { return scia9000_Pnd_Sg_Insurer_1; }

    public DbsField getScia9000_Pnd_Sg_Insurer_2() { return scia9000_Pnd_Sg_Insurer_2; }

    public DbsField getScia9000_Pnd_Sg_Insurer_3() { return scia9000_Pnd_Sg_Insurer_3; }

    public DbsField getScia9000_Pnd_Sg_1035_Exch_Ind_1() { return scia9000_Pnd_Sg_1035_Exch_Ind_1; }

    public DbsField getScia9000_Pnd_Sg_1035_Exch_Ind_2() { return scia9000_Pnd_Sg_1035_Exch_Ind_2; }

    public DbsField getScia9000_Pnd_Sg_1035_Exch_Ind_3() { return scia9000_Pnd_Sg_1035_Exch_Ind_3; }

    public DbsField getScia9000_Pnd_Indicators() { return scia9000_Pnd_Indicators; }

    public DbsGroup getScia9000_Pnd_IndicatorsRedef1() { return scia9000_Pnd_IndicatorsRedef1; }

    public DbsField getScia9000_Pnd_Ica_Ind() { return scia9000_Pnd_Ica_Ind; }

    public DbsField getScia9000_Pnd_Cip_Ind() { return scia9000_Pnd_Cip_Ind; }

    public DbsField getScia9000_Pnd_Erisa_Ind() { return scia9000_Pnd_Erisa_Ind; }

    public DbsField getScia9000_Pnd_Omni_Account_Issuance() { return scia9000_Pnd_Omni_Account_Issuance; }

    public DbsField getScia9000_Pnd_Oia_Ind() { return scia9000_Pnd_Oia_Ind; }

    public DbsField getScia9000_Pnd_Incmpl_Acct_Ind() { return scia9000_Pnd_Incmpl_Acct_Ind; }

    public DbsField getScia9000_Pnd_As_Ind() { return scia9000_Pnd_As_Ind; }

    public DbsField getScia9000_Pnd_Curr_Def_Option() { return scia9000_Pnd_Curr_Def_Option; }

    public DbsField getScia9000_Pnd_Curr_Def_Amt() { return scia9000_Pnd_Curr_Def_Amt; }

    public DbsField getScia9000_Pnd_Incr_Option() { return scia9000_Pnd_Incr_Option; }

    public DbsField getScia9000_Pnd_Incr_Amt() { return scia9000_Pnd_Incr_Amt; }

    public DbsField getScia9000_Pnd_Max_Pct() { return scia9000_Pnd_Max_Pct; }

    public DbsField getScia9000_Pnd_Optout_Days() { return scia9000_Pnd_Optout_Days; }

    public DbsField getScia9000_Pnd_Orchestration_Id() { return scia9000_Pnd_Orchestration_Id; }

    public DbsField getScia9000_Pnd_Mail_Addr_Country_Cd() { return scia9000_Pnd_Mail_Addr_Country_Cd; }

    public DbsField getScia9000_Pnd_Legal_Ann_Option() { return scia9000_Pnd_Legal_Ann_Option; }

    public DbsField getScia9000_Pnd_Substitution_Contract_Ind() { return scia9000_Pnd_Substitution_Contract_Ind; }

    public DbsField getScia9000_Pnd_Conversion_Issue_State() { return scia9000_Pnd_Conversion_Issue_State; }

    public DbsField getScia9000_Pnd_Deceased_Ind() { return scia9000_Pnd_Deceased_Ind; }

    public DbsField getScia9000_Pnd_Non_Proprietary_Pkg_Ind() { return scia9000_Pnd_Non_Proprietary_Pkg_Ind; }

    public DbsField getScia9000_Pnd_Decedent_Contract() { return scia9000_Pnd_Decedent_Contract; }

    public DbsField getScia9000_Pnd_Relation_To_Decedent() { return scia9000_Pnd_Relation_To_Decedent; }

    public DbsField getScia9000_Pnd_Ssn_Tin_Ind() { return scia9000_Pnd_Ssn_Tin_Ind; }

    public DbsField getScia9000_Pnd_Oneira_Acct_No() { return scia9000_Pnd_Oneira_Acct_No; }

    public DbsField getScia9000_Pnd_Fund_Source_1() { return scia9000_Pnd_Fund_Source_1; }

    public DbsField getScia9000_Pnd_Fund_Source_2() { return scia9000_Pnd_Fund_Source_2; }

    public DbsField getScia9000_Pnd_Allocation_Fund_Ticker_2() { return scia9000_Pnd_Allocation_Fund_Ticker_2; }

    public DbsField getScia9000_Pnd_Allocation_2() { return scia9000_Pnd_Allocation_2; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        scia9000 = dbsRecord.newGroupInRecord("scia9000", "SCIA9000");
        scia9000.setParameterOption(ParameterOption.ByReference);
        scia9000_Pnd_Return_Code = scia9000.newFieldInGroup("scia9000_Pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 4);
        scia9000_Pnd_Return_Msg = scia9000.newFieldInGroup("scia9000_Pnd_Return_Msg", "#RETURN-MSG", FieldType.STRING, 80);
        scia9000_Pnd_More_Data = scia9000.newFieldInGroup("scia9000_Pnd_More_Data", "#MORE-DATA", FieldType.STRING, 1);
        scia9000_Pnd_Iteration_Data = scia9000.newFieldInGroup("scia9000_Pnd_Iteration_Data", "#ITERATION-DATA", FieldType.STRING, 100);
        scia9000_Pnd_Retry = scia9000.newFieldInGroup("scia9000_Pnd_Retry", "#RETRY", FieldType.STRING, 1);
        scia9000_Pnd_System_Requestor = scia9000.newFieldInGroup("scia9000_Pnd_System_Requestor", "#SYSTEM-REQUESTOR", FieldType.STRING, 8);
        scia9000_Pnd_Racf_Id = scia9000.newFieldInGroup("scia9000_Pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        scia9000_Pnd_Processing_Location = scia9000.newFieldInGroup("scia9000_Pnd_Processing_Location", "#PROCESSING-LOCATION", FieldType.STRING, 1);
        scia9000_Pnd_Print_Location = scia9000.newFieldInGroup("scia9000_Pnd_Print_Location", "#PRINT-LOCATION", FieldType.STRING, 1);
        scia9000_Pnd_Processing_Type = scia9000.newFieldInGroup("scia9000_Pnd_Processing_Type", "#PROCESSING-TYPE", FieldType.STRING, 2);
        scia9000_Pnd_Ppg_Code = scia9000.newFieldInGroup("scia9000_Pnd_Ppg_Code", "#PPG-CODE", FieldType.STRING, 6);
        scia9000_Pnd_Pull_Code = scia9000.newFieldInGroup("scia9000_Pnd_Pull_Code", "#PULL-CODE", FieldType.STRING, 1);
        scia9000_Pnd_Bypass_Rules = scia9000.newFieldInGroup("scia9000_Pnd_Bypass_Rules", "#BYPASS-RULES", FieldType.STRING, 1);
        scia9000_Pnd_Companion_Ind = scia9000.newFieldInGroup("scia9000_Pnd_Companion_Ind", "#COMPANION-IND", FieldType.STRING, 1);
        scia9000_Pnd_Divorce_Ind = scia9000.newFieldInGroup("scia9000_Pnd_Divorce_Ind", "#DIVORCE-IND", FieldType.STRING, 1);
        scia9000_Pnd_Enrollment_Type = scia9000.newFieldInGroup("scia9000_Pnd_Enrollment_Type", "#ENROLLMENT-TYPE", FieldType.STRING, 1);
        scia9000_Pnd_Additional_Cref_Rqst = scia9000.newFieldInGroup("scia9000_Pnd_Additional_Cref_Rqst", "#ADDITIONAL-CREF-RQST", FieldType.STRING, 1);
        scia9000_Pnd_Entry_Date = scia9000.newFieldInGroup("scia9000_Pnd_Entry_Date", "#ENTRY-DATE", FieldType.STRING, 8);
        scia9000_Pnd_Date_App_Recvd = scia9000.newFieldInGroup("scia9000_Pnd_Date_App_Recvd", "#DATE-APP-RECVD", FieldType.STRING, 8);
        scia9000_Pnd_Date_Prem_Recvd = scia9000.newFieldInGroup("scia9000_Pnd_Date_Prem_Recvd", "#DATE-PREM-RECVD", FieldType.STRING, 8);
        scia9000_Pnd_Annuity_Start_Date = scia9000.newFieldInGroup("scia9000_Pnd_Annuity_Start_Date", "#ANNUITY-START-DATE", FieldType.STRING, 8);
        scia9000_Pnd_Tiaa_Age_1st_Pymnt = scia9000.newFieldInGroup("scia9000_Pnd_Tiaa_Age_1st_Pymnt", "#TIAA-AGE-1ST-PYMNT", FieldType.STRING, 4);
        scia9000_Pnd_Cref_Age_1st_Pymnt = scia9000.newFieldInGroup("scia9000_Pnd_Cref_Age_1st_Pymnt", "#CREF-AGE-1ST-PYMNT", FieldType.STRING, 4);
        scia9000_Pnd_Tiaa_Date_Of_Issue = scia9000.newFieldInGroup("scia9000_Pnd_Tiaa_Date_Of_Issue", "#TIAA-DATE-OF-ISSUE", FieldType.STRING, 8);
        scia9000_Pnd_Cref_Date_Of_Issue = scia9000.newFieldInGroup("scia9000_Pnd_Cref_Date_Of_Issue", "#CREF-DATE-OF-ISSUE", FieldType.STRING, 8);
        scia9000_Pnd_Pf_Key = scia9000.newFieldInGroup("scia9000_Pnd_Pf_Key", "#PF-KEY", FieldType.STRING, 4);
        scia9000_Pnd_Process_Ind = scia9000.newFieldInGroup("scia9000_Pnd_Process_Ind", "#PROCESS-IND", FieldType.STRING, 1);
        scia9000_Pnd_Tiaa_Contract_No = scia9000.newFieldInGroup("scia9000_Pnd_Tiaa_Contract_No", "#TIAA-CONTRACT-NO", FieldType.STRING, 10);
        scia9000_Pnd_Cref_Certificate_No = scia9000.newFieldInGroup("scia9000_Pnd_Cref_Certificate_No", "#CREF-CERTIFICATE-NO", FieldType.STRING, 10);
        scia9000_Pnd_Rl_Contract_No = scia9000.newFieldInGroup("scia9000_Pnd_Rl_Contract_No", "#RL-CONTRACT-NO", FieldType.STRING, 10);
        scia9000_Pnd_Loan_Ind = scia9000.newFieldInGroup("scia9000_Pnd_Loan_Ind", "#LOAN-IND", FieldType.STRING, 1);
        scia9000_Pnd_Contract_Type = scia9000.newFieldInGroup("scia9000_Pnd_Contract_Type", "#CONTRACT-TYPE", FieldType.STRING, 12);
        scia9000_Pnd_Lob = scia9000.newFieldInGroup("scia9000_Pnd_Lob", "#LOB", FieldType.STRING, 1);
        scia9000_Pnd_Lob_Type = scia9000.newFieldInGroup("scia9000_Pnd_Lob_Type", "#LOB-TYPE", FieldType.STRING, 1);
        scia9000_Pnd_Product_Code = scia9000.newFieldInGroup("scia9000_Pnd_Product_Code", "#PRODUCT-CODE", FieldType.STRING, 3);
        scia9000_Pnd_Vesting_Code = scia9000.newFieldInGroup("scia9000_Pnd_Vesting_Code", "#VESTING-CODE", FieldType.STRING, 1);
        scia9000_Pnd_Vesting_Code_Override = scia9000.newFieldInGroup("scia9000_Pnd_Vesting_Code_Override", "#VESTING-CODE-OVERRIDE", FieldType.STRING, 
            1);
        scia9000_Pnd_Mit_Create_Folder = scia9000.newFieldInGroup("scia9000_Pnd_Mit_Create_Folder", "#MIT-CREATE-FOLDER", FieldType.STRING, 1);
        scia9000_Pnd_Mit_Unit = scia9000.newFieldInGroup("scia9000_Pnd_Mit_Unit", "#MIT-UNIT", FieldType.STRING, 8);
        scia9000_Pnd_Mit_Wpid = scia9000.newFieldInGroup("scia9000_Pnd_Mit_Wpid", "#MIT-WPID", FieldType.STRING, 8);
        scia9000_Pnd_Mit_Status = scia9000.newFieldInGroup("scia9000_Pnd_Mit_Status", "#MIT-STATUS", FieldType.STRING, 4);
        scia9000_Pnd_Mit_Log_Date_Time_Parent = scia9000.newFieldInGroup("scia9000_Pnd_Mit_Log_Date_Time_Parent", "#MIT-LOG-DATE-TIME-PARENT", FieldType.STRING, 
            15);
        scia9000_Pnd_Mit_Log_Date_Time_Subrqst = scia9000.newFieldInGroup("scia9000_Pnd_Mit_Log_Date_Time_Subrqst", "#MIT-LOG-DATE-TIME-SUBRQST", FieldType.STRING, 
            15);
        scia9000_Pnd_Pin_Nbr = scia9000.newFieldInGroup("scia9000_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.STRING, 12);
        scia9000_Pnd_Ssn = scia9000.newFieldInGroup("scia9000_Pnd_Ssn", "#SSN", FieldType.STRING, 9);
        scia9000_Pnd_Prefix = scia9000.newFieldInGroup("scia9000_Pnd_Prefix", "#PREFIX", FieldType.STRING, 8);
        scia9000_Pnd_Last_Name = scia9000.newFieldInGroup("scia9000_Pnd_Last_Name", "#LAST-NAME", FieldType.STRING, 30);
        scia9000_Pnd_First_Name = scia9000.newFieldInGroup("scia9000_Pnd_First_Name", "#FIRST-NAME", FieldType.STRING, 30);
        scia9000_Pnd_Middle_Name = scia9000.newFieldInGroup("scia9000_Pnd_Middle_Name", "#MIDDLE-NAME", FieldType.STRING, 30);
        scia9000_Pnd_Suffix = scia9000.newFieldInGroup("scia9000_Pnd_Suffix", "#SUFFIX", FieldType.STRING, 8);
        scia9000_Pnd_Date_Of_Birth = scia9000.newFieldInGroup("scia9000_Pnd_Date_Of_Birth", "#DATE-OF-BIRTH", FieldType.STRING, 8);
        scia9000_Pnd_Sex = scia9000.newFieldInGroup("scia9000_Pnd_Sex", "#SEX", FieldType.STRING, 1);
        scia9000_Pnd_Address_Line = scia9000.newFieldArrayInGroup("scia9000_Pnd_Address_Line", "#ADDRESS-LINE", FieldType.STRING, 35, new DbsArrayController(1,
            5));
        scia9000_Pnd_City = scia9000.newFieldInGroup("scia9000_Pnd_City", "#CITY", FieldType.STRING, 27);
        scia9000_Pnd_State = scia9000.newFieldInGroup("scia9000_Pnd_State", "#STATE", FieldType.STRING, 2);
        scia9000_Pnd_State_Nbr = scia9000.newFieldInGroup("scia9000_Pnd_State_Nbr", "#STATE-NBR", FieldType.STRING, 2);
        scia9000_Pnd_Zip_Code = scia9000.newFieldInGroup("scia9000_Pnd_Zip_Code", "#ZIP-CODE", FieldType.STRING, 9);
        scia9000_Pnd_Email_Address = scia9000.newFieldInGroup("scia9000_Pnd_Email_Address", "#EMAIL-ADDRESS", FieldType.STRING, 50);
        scia9000_Pnd_Phone = scia9000.newFieldInGroup("scia9000_Pnd_Phone", "#PHONE", FieldType.STRING, 20);
        scia9000_Pnd_Res_Address_Line = scia9000.newFieldArrayInGroup("scia9000_Pnd_Res_Address_Line", "#RES-ADDRESS-LINE", FieldType.STRING, 35, new 
            DbsArrayController(1,3));
        scia9000_Pnd_Res_City = scia9000.newFieldInGroup("scia9000_Pnd_Res_City", "#RES-CITY", FieldType.STRING, 27);
        scia9000_Pnd_Res_State = scia9000.newFieldInGroup("scia9000_Pnd_Res_State", "#RES-STATE", FieldType.STRING, 2);
        scia9000_Pnd_Res_Country_Code = scia9000.newFieldInGroup("scia9000_Pnd_Res_Country_Code", "#RES-COUNTRY-CODE", FieldType.STRING, 3);
        scia9000_Pnd_Res_Zip_Code = scia9000.newFieldInGroup("scia9000_Pnd_Res_Zip_Code", "#RES-ZIP-CODE", FieldType.STRING, 9);
        scia9000_Pnd_Res_Phone = scia9000.newFieldInGroup("scia9000_Pnd_Res_Phone", "#RES-PHONE", FieldType.STRING, 20);
        scia9000_Pnd_Allocation_Fmt = scia9000.newFieldInGroup("scia9000_Pnd_Allocation_Fmt", "#ALLOCATION-FMT", FieldType.STRING, 1);
        scia9000_Pnd_Allocation = scia9000.newFieldArrayInGroup("scia9000_Pnd_Allocation", "#ALLOCATION", FieldType.STRING, 3, new DbsArrayController(1,
            100));
        scia9000_Pnd_Allocation_Fund_Ticker = scia9000.newFieldArrayInGroup("scia9000_Pnd_Allocation_Fund_Ticker", "#ALLOCATION-FUND-TICKER", FieldType.STRING, 
            10, new DbsArrayController(1,100));
        scia9000_Pnd_Allocation_Model_Type = scia9000.newFieldInGroup("scia9000_Pnd_Allocation_Model_Type", "#ALLOCATION-MODEL-TYPE", FieldType.STRING, 
            2);
        scia9000_Pnd_Alloc_Discrepancy_Ind = scia9000.newFieldInGroup("scia9000_Pnd_Alloc_Discrepancy_Ind", "#ALLOC-DISCREPANCY-IND", FieldType.STRING, 
            1);
        scia9000_Pnd_Bene_Type = scia9000.newFieldArrayInGroup("scia9000_Pnd_Bene_Type", "#BENE-TYPE", FieldType.STRING, 1, new DbsArrayController(1,20));
        scia9000_Pnd_Bene_Ssn = scia9000.newFieldArrayInGroup("scia9000_Pnd_Bene_Ssn", "#BENE-SSN", FieldType.STRING, 9, new DbsArrayController(1,20));
        scia9000_Pnd_Bene_Name = scia9000.newFieldArrayInGroup("scia9000_Pnd_Bene_Name", "#BENE-NAME", FieldType.STRING, 35, new DbsArrayController(1,
            20));
        scia9000_Pnd_Bene_Extended_Name = scia9000.newFieldArrayInGroup("scia9000_Pnd_Bene_Extended_Name", "#BENE-EXTENDED-NAME", FieldType.STRING, 35, 
            new DbsArrayController(1,20));
        scia9000_Pnd_Bene_Relationship = scia9000.newFieldArrayInGroup("scia9000_Pnd_Bene_Relationship", "#BENE-RELATIONSHIP", FieldType.STRING, 15, new 
            DbsArrayController(1,20));
        scia9000_Pnd_Bene_Relationship_Code = scia9000.newFieldArrayInGroup("scia9000_Pnd_Bene_Relationship_Code", "#BENE-RELATIONSHIP-CODE", FieldType.STRING, 
            2, new DbsArrayController(1,20));
        scia9000_Pnd_Bene_Date_Of_Birth = scia9000.newFieldArrayInGroup("scia9000_Pnd_Bene_Date_Of_Birth", "#BENE-DATE-OF-BIRTH", FieldType.STRING, 8, 
            new DbsArrayController(1,20));
        scia9000_Pnd_Bene_Alloc_Pct = scia9000.newFieldArrayInGroup("scia9000_Pnd_Bene_Alloc_Pct", "#BENE-ALLOC-PCT", FieldType.STRING, 5, new DbsArrayController(1,
            20));
        scia9000_Pnd_Bene_Numerator = scia9000.newFieldArrayInGroup("scia9000_Pnd_Bene_Numerator", "#BENE-NUMERATOR", FieldType.STRING, 3, new DbsArrayController(1,
            20));
        scia9000_Pnd_Bene_Denominator = scia9000.newFieldArrayInGroup("scia9000_Pnd_Bene_Denominator", "#BENE-DENOMINATOR", FieldType.STRING, 3, new DbsArrayController(1,
            20));
        scia9000_Pnd_Bene_Special_Text_Ind = scia9000.newFieldArrayInGroup("scia9000_Pnd_Bene_Special_Text_Ind", "#BENE-SPECIAL-TEXT-IND", FieldType.STRING, 
            1, new DbsArrayController(1,20));
        scia9000_Pnd_Bene_Special_Text = scia9000.newFieldArrayInGroup("scia9000_Pnd_Bene_Special_Text", "#BENE-SPECIAL-TEXT", FieldType.STRING, 72, new 
            DbsArrayController(1,20,1,3));
        scia9000_Pnd_Bene_Addr1 = scia9000.newFieldArrayInGroup("scia9000_Pnd_Bene_Addr1", "#BENE-ADDR1", FieldType.STRING, 35, new DbsArrayController(1,
            20));
        scia9000_Pnd_Bene_Addr2 = scia9000.newFieldArrayInGroup("scia9000_Pnd_Bene_Addr2", "#BENE-ADDR2", FieldType.STRING, 35, new DbsArrayController(1,
            20));
        scia9000_Pnd_Bene_Addr3_City = scia9000.newFieldArrayInGroup("scia9000_Pnd_Bene_Addr3_City", "#BENE-ADDR3-CITY", FieldType.STRING, 35, new DbsArrayController(1,
            20));
        scia9000_Pnd_Bene_State = scia9000.newFieldArrayInGroup("scia9000_Pnd_Bene_State", "#BENE-STATE", FieldType.STRING, 2, new DbsArrayController(1,
            20));
        scia9000_Pnd_Bene_Zip = scia9000.newFieldArrayInGroup("scia9000_Pnd_Bene_Zip", "#BENE-ZIP", FieldType.STRING, 10, new DbsArrayController(1,20));
        scia9000_Pnd_Bene_Phone = scia9000.newFieldArrayInGroup("scia9000_Pnd_Bene_Phone", "#BENE-PHONE", FieldType.STRING, 20, new DbsArrayController(1,
            20));
        scia9000_Pnd_Bene_Gender = scia9000.newFieldArrayInGroup("scia9000_Pnd_Bene_Gender", "#BENE-GENDER", FieldType.STRING, 2, new DbsArrayController(1,
            20));
        scia9000_Pnd_Bene_Country = scia9000.newFieldArrayInGroup("scia9000_Pnd_Bene_Country", "#BENE-COUNTRY", FieldType.STRING, 35, new DbsArrayController(1,
            20));
        scia9000_Pnd_Bene_Mos_Ind = scia9000.newFieldInGroup("scia9000_Pnd_Bene_Mos_Ind", "#BENE-MOS-IND", FieldType.STRING, 1);
        scia9000_Pnd_Bene_Estate = scia9000.newFieldInGroup("scia9000_Pnd_Bene_Estate", "#BENE-ESTATE", FieldType.STRING, 1);
        scia9000_Pnd_Bene_Trust = scia9000.newFieldInGroup("scia9000_Pnd_Bene_Trust", "#BENE-TRUST", FieldType.STRING, 1);
        scia9000_Pnd_Bene_Category = scia9000.newFieldInGroup("scia9000_Pnd_Bene_Category", "#BENE-CATEGORY", FieldType.STRING, 1);
        scia9000_Pnd_Isn = scia9000.newFieldInGroup("scia9000_Pnd_Isn", "#ISN", FieldType.STRING, 10);
        scia9000_Pnd_Field1 = scia9000.newFieldInGroup("scia9000_Pnd_Field1", "#FIELD1", FieldType.STRING, 30);
        scia9000_Pnd_Field2 = scia9000.newFieldInGroup("scia9000_Pnd_Field2", "#FIELD2", FieldType.STRING, 30);
        scia9000_Pnd_Field3 = scia9000.newFieldInGroup("scia9000_Pnd_Field3", "#FIELD3", FieldType.STRING, 30);
        scia9000_Pnd_Sg_Plan_Id = scia9000.newFieldInGroup("scia9000_Pnd_Sg_Plan_Id", "#SG-PLAN-ID", FieldType.STRING, 6);
        scia9000_Pnd_Sg_Plan_No = scia9000.newFieldInGroup("scia9000_Pnd_Sg_Plan_No", "#SG-PLAN-NO", FieldType.STRING, 6);
        scia9000_Pnd_Sg_Subplan_No = scia9000.newFieldInGroup("scia9000_Pnd_Sg_Subplan_No", "#SG-SUBPLAN-NO", FieldType.STRING, 6);
        scia9000_Pnd_Sg_Part_Ext = scia9000.newFieldInGroup("scia9000_Pnd_Sg_Part_Ext", "#SG-PART-EXT", FieldType.STRING, 2);
        scia9000_Pnd_Sg_Divsub = scia9000.newFieldInGroup("scia9000_Pnd_Sg_Divsub", "#SG-DIVSUB", FieldType.STRING, 4);
        scia9000_Pnd_Sg_Text_Udf_1 = scia9000.newFieldInGroup("scia9000_Pnd_Sg_Text_Udf_1", "#SG-TEXT-UDF-1", FieldType.STRING, 10);
        scia9000_Pnd_Sg_Text_Udf_2 = scia9000.newFieldInGroup("scia9000_Pnd_Sg_Text_Udf_2", "#SG-TEXT-UDF-2", FieldType.STRING, 10);
        scia9000_Pnd_Sg_Text_Udf_3 = scia9000.newFieldInGroup("scia9000_Pnd_Sg_Text_Udf_3", "#SG-TEXT-UDF-3", FieldType.STRING, 10);
        scia9000_Pnd_Irc_Section_Code = scia9000.newFieldInGroup("scia9000_Pnd_Irc_Section_Code", "#IRC-SECTION-CODE", FieldType.STRING, 2);
        scia9000_Pnd_Irc_Section_Grp_Code = scia9000.newFieldInGroup("scia9000_Pnd_Irc_Section_Grp_Code", "#IRC-SECTION-GRP-CODE", FieldType.STRING, 2);
        scia9000_Pnd_Issue_State = scia9000.newFieldInGroup("scia9000_Pnd_Issue_State", "#ISSUE-STATE", FieldType.STRING, 2);
        scia9000_Pnd_Mailing_Instr = scia9000.newFieldInGroup("scia9000_Pnd_Mailing_Instr", "#MAILING-INSTR", FieldType.STRING, 1);
        scia9000_Pnd_Sg_Replacement_Ind = scia9000.newFieldInGroup("scia9000_Pnd_Sg_Replacement_Ind", "#SG-REPLACEMENT-IND", FieldType.STRING, 1);
        scia9000_Pnd_Sg_Register_Id = scia9000.newFieldInGroup("scia9000_Pnd_Sg_Register_Id", "#SG-REGISTER-ID", FieldType.STRING, 11);
        scia9000_Pnd_Sg_Agent_Racf_Id = scia9000.newFieldInGroup("scia9000_Pnd_Sg_Agent_Racf_Id", "#SG-AGENT-RACF-ID", FieldType.STRING, 15);
        scia9000_Pnd_Sg_Exempt_Ind = scia9000.newFieldInGroup("scia9000_Pnd_Sg_Exempt_Ind", "#SG-EXEMPT-IND", FieldType.STRING, 1);
        scia9000_Pnd_Sg_Insurer_1 = scia9000.newFieldInGroup("scia9000_Pnd_Sg_Insurer_1", "#SG-INSURER-1", FieldType.STRING, 30);
        scia9000_Pnd_Sg_Insurer_2 = scia9000.newFieldInGroup("scia9000_Pnd_Sg_Insurer_2", "#SG-INSURER-2", FieldType.STRING, 30);
        scia9000_Pnd_Sg_Insurer_3 = scia9000.newFieldInGroup("scia9000_Pnd_Sg_Insurer_3", "#SG-INSURER-3", FieldType.STRING, 30);
        scia9000_Pnd_Sg_1035_Exch_Ind_1 = scia9000.newFieldInGroup("scia9000_Pnd_Sg_1035_Exch_Ind_1", "#SG-1035-EXCH-IND-1", FieldType.STRING, 1);
        scia9000_Pnd_Sg_1035_Exch_Ind_2 = scia9000.newFieldInGroup("scia9000_Pnd_Sg_1035_Exch_Ind_2", "#SG-1035-EXCH-IND-2", FieldType.STRING, 1);
        scia9000_Pnd_Sg_1035_Exch_Ind_3 = scia9000.newFieldInGroup("scia9000_Pnd_Sg_1035_Exch_Ind_3", "#SG-1035-EXCH-IND-3", FieldType.STRING, 1);
        scia9000_Pnd_Indicators = scia9000.newFieldInGroup("scia9000_Pnd_Indicators", "#INDICATORS", FieldType.STRING, 20);
        scia9000_Pnd_IndicatorsRedef1 = scia9000.newGroupInGroup("scia9000_Pnd_IndicatorsRedef1", "Redefines", scia9000_Pnd_Indicators);
        scia9000_Pnd_Ica_Ind = scia9000_Pnd_IndicatorsRedef1.newFieldInGroup("scia9000_Pnd_Ica_Ind", "#ICA-IND", FieldType.STRING, 1);
        scia9000_Pnd_Cip_Ind = scia9000_Pnd_IndicatorsRedef1.newFieldInGroup("scia9000_Pnd_Cip_Ind", "#CIP-IND", FieldType.STRING, 1);
        scia9000_Pnd_Erisa_Ind = scia9000_Pnd_IndicatorsRedef1.newFieldInGroup("scia9000_Pnd_Erisa_Ind", "#ERISA-IND", FieldType.STRING, 1);
        scia9000_Pnd_Omni_Account_Issuance = scia9000_Pnd_IndicatorsRedef1.newFieldInGroup("scia9000_Pnd_Omni_Account_Issuance", "#OMNI-ACCOUNT-ISSUANCE", 
            FieldType.STRING, 1);
        scia9000_Pnd_Oia_Ind = scia9000_Pnd_IndicatorsRedef1.newFieldInGroup("scia9000_Pnd_Oia_Ind", "#OIA-IND", FieldType.STRING, 2);
        scia9000_Pnd_Incmpl_Acct_Ind = scia9000.newFieldInGroup("scia9000_Pnd_Incmpl_Acct_Ind", "#INCMPL-ACCT-IND", FieldType.STRING, 1);
        scia9000_Pnd_As_Ind = scia9000.newFieldInGroup("scia9000_Pnd_As_Ind", "#AS-IND", FieldType.STRING, 1);
        scia9000_Pnd_Curr_Def_Option = scia9000.newFieldInGroup("scia9000_Pnd_Curr_Def_Option", "#CURR-DEF-OPTION", FieldType.STRING, 1);
        scia9000_Pnd_Curr_Def_Amt = scia9000.newFieldInGroup("scia9000_Pnd_Curr_Def_Amt", "#CURR-DEF-AMT", FieldType.STRING, 9);
        scia9000_Pnd_Incr_Option = scia9000.newFieldInGroup("scia9000_Pnd_Incr_Option", "#INCR-OPTION", FieldType.STRING, 1);
        scia9000_Pnd_Incr_Amt = scia9000.newFieldInGroup("scia9000_Pnd_Incr_Amt", "#INCR-AMT", FieldType.STRING, 9);
        scia9000_Pnd_Max_Pct = scia9000.newFieldInGroup("scia9000_Pnd_Max_Pct", "#MAX-PCT", FieldType.STRING, 5);
        scia9000_Pnd_Optout_Days = scia9000.newFieldInGroup("scia9000_Pnd_Optout_Days", "#OPTOUT-DAYS", FieldType.STRING, 2);
        scia9000_Pnd_Orchestration_Id = scia9000.newFieldInGroup("scia9000_Pnd_Orchestration_Id", "#ORCHESTRATION-ID", FieldType.STRING, 15);
        scia9000_Pnd_Mail_Addr_Country_Cd = scia9000.newFieldInGroup("scia9000_Pnd_Mail_Addr_Country_Cd", "#MAIL-ADDR-COUNTRY-CD", FieldType.STRING, 3);
        scia9000_Pnd_Legal_Ann_Option = scia9000.newFieldInGroup("scia9000_Pnd_Legal_Ann_Option", "#LEGAL-ANN-OPTION", FieldType.STRING, 1);
        scia9000_Pnd_Substitution_Contract_Ind = scia9000.newFieldInGroup("scia9000_Pnd_Substitution_Contract_Ind", "#SUBSTITUTION-CONTRACT-IND", FieldType.STRING, 
            1);
        scia9000_Pnd_Conversion_Issue_State = scia9000.newFieldInGroup("scia9000_Pnd_Conversion_Issue_State", "#CONVERSION-ISSUE-STATE", FieldType.STRING, 
            2);
        scia9000_Pnd_Deceased_Ind = scia9000.newFieldInGroup("scia9000_Pnd_Deceased_Ind", "#DECEASED-IND", FieldType.STRING, 1);
        scia9000_Pnd_Non_Proprietary_Pkg_Ind = scia9000.newFieldInGroup("scia9000_Pnd_Non_Proprietary_Pkg_Ind", "#NON-PROPRIETARY-PKG-IND", FieldType.STRING, 
            5);
        scia9000_Pnd_Decedent_Contract = scia9000.newFieldInGroup("scia9000_Pnd_Decedent_Contract", "#DECEDENT-CONTRACT", FieldType.STRING, 10);
        scia9000_Pnd_Relation_To_Decedent = scia9000.newFieldInGroup("scia9000_Pnd_Relation_To_Decedent", "#RELATION-TO-DECEDENT", FieldType.STRING, 1);
        scia9000_Pnd_Ssn_Tin_Ind = scia9000.newFieldInGroup("scia9000_Pnd_Ssn_Tin_Ind", "#SSN-TIN-IND", FieldType.STRING, 1);
        scia9000_Pnd_Oneira_Acct_No = scia9000.newFieldInGroup("scia9000_Pnd_Oneira_Acct_No", "#ONEIRA-ACCT-NO", FieldType.STRING, 10);
        scia9000_Pnd_Fund_Source_1 = scia9000.newFieldInGroup("scia9000_Pnd_Fund_Source_1", "#FUND-SOURCE-1", FieldType.STRING, 1);
        scia9000_Pnd_Fund_Source_2 = scia9000.newFieldInGroup("scia9000_Pnd_Fund_Source_2", "#FUND-SOURCE-2", FieldType.STRING, 1);
        scia9000_Pnd_Allocation_Fund_Ticker_2 = scia9000.newFieldArrayInGroup("scia9000_Pnd_Allocation_Fund_Ticker_2", "#ALLOCATION-FUND-TICKER-2", FieldType.STRING, 
            10, new DbsArrayController(1,100));
        scia9000_Pnd_Allocation_2 = scia9000.newFieldArrayInGroup("scia9000_Pnd_Allocation_2", "#ALLOCATION-2", FieldType.STRING, 3, new DbsArrayController(1,
            100));

        dbsRecord.reset();
    }

    // Constructors
    public PdaScia9000(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

