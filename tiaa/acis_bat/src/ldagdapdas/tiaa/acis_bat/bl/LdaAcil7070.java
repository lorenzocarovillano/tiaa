/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:49:19 PM
**        * FROM NATURAL LDA     : ACIL7070
************************************************************
**        * FILE NAME            : LdaAcil7070.java
**        * CLASS NAME           : LdaAcil7070
**        * INSTANCE NAME        : LdaAcil7070
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAcil7070 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_app_Table_Entry_View;
    private DbsField app_Table_Entry_View_Entry_Actve_Ind;
    private DbsField app_Table_Entry_View_Entry_Table_Id_Nbr;
    private DbsField app_Table_Entry_View_Entry_Table_Sub_Id;
    private DbsField app_Table_Entry_View_Entry_Cde;
    private DbsGroup app_Table_Entry_View_Entry_CdeRedef1;
    private DbsField app_Table_Entry_View_Entry_Team;
    private DbsField app_Table_Entry_View_Entry_Cde_Filler;
    private DbsField app_Table_Entry_View_Entry_Dscrptn_Txt;
    private DbsGroup app_Table_Entry_View_Entry_Dscrptn_TxtRedef2;
    private DbsField app_Table_Entry_View_Entry_Bucket_Dsc;
    private DbsField app_Table_Entry_View_Entry_Accum_Dsc;
    private DbsField app_Table_Entry_View_Entry_Team_Cde;
    private DbsField app_Table_Entry_View_Entry_Filler;
    private DbsField app_Table_Entry_View_Entry_User_Id;
    private DbsGroup app_Table_Entry_View_Entry_Trnsctn_Type_CdeMuGroup;
    private DbsField app_Table_Entry_View_Entry_Trnsctn_Type_Cde;
    private DbsGroup app_Table_Entry_View_Entry_Mit_Status_CdeMuGroup;
    private DbsField app_Table_Entry_View_Entry_Mit_Status_Cde;
    private DbsGroup app_Table_Entry_View_Addtnl_Dscrptn_TxtMuGroup;
    private DbsField app_Table_Entry_View_Addtnl_Dscrptn_Txt;
    private DbsGroup app_Table_Entry_View_Addtnl_Dscrptn_TxtRedef3;
    private DbsField app_Table_Entry_View_Addtnl_Short_Txt;
    private DbsField app_Table_Entry_View_Addtnl_Filler;
    private DbsField app_Table_Entry_View_Addtnl_Long_Txt;
    private DbsGroup app_Table_Entry_View_Entry_Prgam_CdeMuGroup;
    private DbsField app_Table_Entry_View_Entry_Prgam_Cde;
    private DbsField app_Table_Entry_View_Hold_Cde;

    public DataAccessProgramView getVw_app_Table_Entry_View() { return vw_app_Table_Entry_View; }

    public DbsField getApp_Table_Entry_View_Entry_Actve_Ind() { return app_Table_Entry_View_Entry_Actve_Ind; }

    public DbsField getApp_Table_Entry_View_Entry_Table_Id_Nbr() { return app_Table_Entry_View_Entry_Table_Id_Nbr; }

    public DbsField getApp_Table_Entry_View_Entry_Table_Sub_Id() { return app_Table_Entry_View_Entry_Table_Sub_Id; }

    public DbsField getApp_Table_Entry_View_Entry_Cde() { return app_Table_Entry_View_Entry_Cde; }

    public DbsGroup getApp_Table_Entry_View_Entry_CdeRedef1() { return app_Table_Entry_View_Entry_CdeRedef1; }

    public DbsField getApp_Table_Entry_View_Entry_Team() { return app_Table_Entry_View_Entry_Team; }

    public DbsField getApp_Table_Entry_View_Entry_Cde_Filler() { return app_Table_Entry_View_Entry_Cde_Filler; }

    public DbsField getApp_Table_Entry_View_Entry_Dscrptn_Txt() { return app_Table_Entry_View_Entry_Dscrptn_Txt; }

    public DbsGroup getApp_Table_Entry_View_Entry_Dscrptn_TxtRedef2() { return app_Table_Entry_View_Entry_Dscrptn_TxtRedef2; }

    public DbsField getApp_Table_Entry_View_Entry_Bucket_Dsc() { return app_Table_Entry_View_Entry_Bucket_Dsc; }

    public DbsField getApp_Table_Entry_View_Entry_Accum_Dsc() { return app_Table_Entry_View_Entry_Accum_Dsc; }

    public DbsField getApp_Table_Entry_View_Entry_Team_Cde() { return app_Table_Entry_View_Entry_Team_Cde; }

    public DbsField getApp_Table_Entry_View_Entry_Filler() { return app_Table_Entry_View_Entry_Filler; }

    public DbsField getApp_Table_Entry_View_Entry_User_Id() { return app_Table_Entry_View_Entry_User_Id; }

    public DbsGroup getApp_Table_Entry_View_Entry_Trnsctn_Type_CdeMuGroup() { return app_Table_Entry_View_Entry_Trnsctn_Type_CdeMuGroup; }

    public DbsField getApp_Table_Entry_View_Entry_Trnsctn_Type_Cde() { return app_Table_Entry_View_Entry_Trnsctn_Type_Cde; }

    public DbsGroup getApp_Table_Entry_View_Entry_Mit_Status_CdeMuGroup() { return app_Table_Entry_View_Entry_Mit_Status_CdeMuGroup; }

    public DbsField getApp_Table_Entry_View_Entry_Mit_Status_Cde() { return app_Table_Entry_View_Entry_Mit_Status_Cde; }

    public DbsGroup getApp_Table_Entry_View_Addtnl_Dscrptn_TxtMuGroup() { return app_Table_Entry_View_Addtnl_Dscrptn_TxtMuGroup; }

    public DbsField getApp_Table_Entry_View_Addtnl_Dscrptn_Txt() { return app_Table_Entry_View_Addtnl_Dscrptn_Txt; }

    public DbsGroup getApp_Table_Entry_View_Addtnl_Dscrptn_TxtRedef3() { return app_Table_Entry_View_Addtnl_Dscrptn_TxtRedef3; }

    public DbsField getApp_Table_Entry_View_Addtnl_Short_Txt() { return app_Table_Entry_View_Addtnl_Short_Txt; }

    public DbsField getApp_Table_Entry_View_Addtnl_Filler() { return app_Table_Entry_View_Addtnl_Filler; }

    public DbsField getApp_Table_Entry_View_Addtnl_Long_Txt() { return app_Table_Entry_View_Addtnl_Long_Txt; }

    public DbsGroup getApp_Table_Entry_View_Entry_Prgam_CdeMuGroup() { return app_Table_Entry_View_Entry_Prgam_CdeMuGroup; }

    public DbsField getApp_Table_Entry_View_Entry_Prgam_Cde() { return app_Table_Entry_View_Entry_Prgam_Cde; }

    public DbsField getApp_Table_Entry_View_Hold_Cde() { return app_Table_Entry_View_Hold_Cde; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_app_Table_Entry_View = new DataAccessProgramView(new NameInfo("vw_app_Table_Entry_View", "APP-TABLE-ENTRY-VIEW"), "APP_TABLE_ENTRY", "ACIS_APP_TBL_ENT");
        app_Table_Entry_View_Entry_Actve_Ind = vw_app_Table_Entry_View.getRecord().newFieldInGroup("app_Table_Entry_View_Entry_Actve_Ind", "ENTRY-ACTVE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ENTRY_ACTVE_IND");
        app_Table_Entry_View_Entry_Table_Id_Nbr = vw_app_Table_Entry_View.getRecord().newFieldInGroup("app_Table_Entry_View_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "ENTRY_TABLE_ID_NBR");
        app_Table_Entry_View_Entry_Table_Sub_Id = vw_app_Table_Entry_View.getRecord().newFieldInGroup("app_Table_Entry_View_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ENTRY_TABLE_SUB_ID");
        app_Table_Entry_View_Entry_Cde = vw_app_Table_Entry_View.getRecord().newFieldInGroup("app_Table_Entry_View_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "ENTRY_CDE");
        app_Table_Entry_View_Entry_CdeRedef1 = vw_app_Table_Entry_View.getRecord().newGroupInGroup("app_Table_Entry_View_Entry_CdeRedef1", "Redefines", 
            app_Table_Entry_View_Entry_Cde);
        app_Table_Entry_View_Entry_Team = app_Table_Entry_View_Entry_CdeRedef1.newFieldInGroup("app_Table_Entry_View_Entry_Team", "ENTRY-TEAM", FieldType.STRING, 
            8);
        app_Table_Entry_View_Entry_Cde_Filler = app_Table_Entry_View_Entry_CdeRedef1.newFieldInGroup("app_Table_Entry_View_Entry_Cde_Filler", "ENTRY-CDE-FILLER", 
            FieldType.STRING, 12);
        app_Table_Entry_View_Entry_Dscrptn_Txt = vw_app_Table_Entry_View.getRecord().newFieldInGroup("app_Table_Entry_View_Entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "ENTRY_DSCRPTN_TXT");
        app_Table_Entry_View_Entry_Dscrptn_TxtRedef2 = vw_app_Table_Entry_View.getRecord().newGroupInGroup("app_Table_Entry_View_Entry_Dscrptn_TxtRedef2", 
            "Redefines", app_Table_Entry_View_Entry_Dscrptn_Txt);
        app_Table_Entry_View_Entry_Bucket_Dsc = app_Table_Entry_View_Entry_Dscrptn_TxtRedef2.newFieldInGroup("app_Table_Entry_View_Entry_Bucket_Dsc", 
            "ENTRY-BUCKET-DSC", FieldType.STRING, 2);
        app_Table_Entry_View_Entry_Accum_Dsc = app_Table_Entry_View_Entry_Dscrptn_TxtRedef2.newFieldInGroup("app_Table_Entry_View_Entry_Accum_Dsc", "ENTRY-ACCUM-DSC", 
            FieldType.STRING, 2);
        app_Table_Entry_View_Entry_Team_Cde = app_Table_Entry_View_Entry_Dscrptn_TxtRedef2.newFieldInGroup("app_Table_Entry_View_Entry_Team_Cde", "ENTRY-TEAM-CDE", 
            FieldType.STRING, 2);
        app_Table_Entry_View_Entry_Filler = app_Table_Entry_View_Entry_Dscrptn_TxtRedef2.newFieldInGroup("app_Table_Entry_View_Entry_Filler", "ENTRY-FILLER", 
            FieldType.STRING, 54);
        app_Table_Entry_View_Entry_User_Id = vw_app_Table_Entry_View.getRecord().newFieldInGroup("app_Table_Entry_View_Entry_User_Id", "ENTRY-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_USER_ID");
        app_Table_Entry_View_Entry_Trnsctn_Type_CdeMuGroup = vw_app_Table_Entry_View.getRecord().newGroupInGroup("app_Table_Entry_View_Entry_Trnsctn_Type_CdeMuGroup", 
            "ENTRY_TRNSCTN_TYPE_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "ACIS_APP_TBL_ENT_ENTRY_TRNSCTN_TYPE_CDE");
        app_Table_Entry_View_Entry_Trnsctn_Type_Cde = app_Table_Entry_View_Entry_Trnsctn_Type_CdeMuGroup.newFieldArrayInGroup("app_Table_Entry_View_Entry_Trnsctn_Type_Cde", 
            "ENTRY-TRNSCTN-TYPE-CDE", FieldType.STRING, 1, new DbsArrayController(1,5), RepeatingFieldStrategy.SubTableFieldArray, "ENTRY_TRNSCTN_TYPE_CDE");
        app_Table_Entry_View_Entry_Mit_Status_CdeMuGroup = vw_app_Table_Entry_View.getRecord().newGroupInGroup("app_Table_Entry_View_Entry_Mit_Status_CdeMuGroup", 
            "ENTRY_MIT_STATUS_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "ACIS_APP_TBL_ENT_ENTRY_MIT_STATUS_CDE");
        app_Table_Entry_View_Entry_Mit_Status_Cde = app_Table_Entry_View_Entry_Mit_Status_CdeMuGroup.newFieldArrayInGroup("app_Table_Entry_View_Entry_Mit_Status_Cde", 
            "ENTRY-MIT-STATUS-CDE", FieldType.STRING, 4, new DbsArrayController(1,5), RepeatingFieldStrategy.SubTableFieldArray, "ENTRY_MIT_STATUS_CDE");
        app_Table_Entry_View_Addtnl_Dscrptn_TxtMuGroup = vw_app_Table_Entry_View.getRecord().newGroupInGroup("app_Table_Entry_View_Addtnl_Dscrptn_TxtMuGroup", 
            "ADDTNL_DSCRPTN_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "ACIS_APP_TBL_ENT_ADDTNL_DSCRPTN_TXT");
        app_Table_Entry_View_Addtnl_Dscrptn_Txt = app_Table_Entry_View_Addtnl_Dscrptn_TxtMuGroup.newFieldArrayInGroup("app_Table_Entry_View_Addtnl_Dscrptn_Txt", 
            "ADDTNL-DSCRPTN-TXT", FieldType.STRING, 60, new DbsArrayController(1,5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADDTNL_DSCRPTN_TXT");
        app_Table_Entry_View_Addtnl_Dscrptn_TxtRedef3 = vw_app_Table_Entry_View.getRecord().newGroupInGroup("app_Table_Entry_View_Addtnl_Dscrptn_TxtRedef3", 
            "Redefines", app_Table_Entry_View_Addtnl_Dscrptn_Txt);
        app_Table_Entry_View_Addtnl_Short_Txt = app_Table_Entry_View_Addtnl_Dscrptn_TxtRedef3.newFieldInGroup("app_Table_Entry_View_Addtnl_Short_Txt", 
            "ADDTNL-SHORT-TXT", FieldType.STRING, 7);
        app_Table_Entry_View_Addtnl_Filler = app_Table_Entry_View_Addtnl_Dscrptn_TxtRedef3.newFieldInGroup("app_Table_Entry_View_Addtnl_Filler", "ADDTNL-FILLER", 
            FieldType.STRING, 1);
        app_Table_Entry_View_Addtnl_Long_Txt = app_Table_Entry_View_Addtnl_Dscrptn_TxtRedef3.newFieldInGroup("app_Table_Entry_View_Addtnl_Long_Txt", "ADDTNL-LONG-TXT", 
            FieldType.STRING, 52);
        app_Table_Entry_View_Entry_Prgam_CdeMuGroup = vw_app_Table_Entry_View.getRecord().newGroupInGroup("app_Table_Entry_View_Entry_Prgam_CdeMuGroup", 
            "ENTRY_PRGAM_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "ACIS_APP_TBL_ENT_ENTRY_PRGAM_CDE");
        app_Table_Entry_View_Entry_Prgam_Cde = app_Table_Entry_View_Entry_Prgam_CdeMuGroup.newFieldArrayInGroup("app_Table_Entry_View_Entry_Prgam_Cde", 
            "ENTRY-PRGAM-CDE", FieldType.STRING, 8, new DbsArrayController(1,5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ENTRY_PRGAM_CDE");
        app_Table_Entry_View_Hold_Cde = vw_app_Table_Entry_View.getRecord().newFieldInGroup("app_Table_Entry_View_Hold_Cde", "HOLD-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "HOLD_CDE");

        this.setRecordName("LdaAcil7070");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_app_Table_Entry_View.reset();
    }

    // Constructor
    public LdaAcil7070() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
