/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:51:03 PM
**        * FROM NATURAL LDA     : APPL300
************************************************************
**        * FILE NAME            : LdaAppl300.java
**        * CLASS NAME           : LdaAppl300
**        * INSTANCE NAME        : LdaAppl300
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl300 extends DbsRecord
{
    // Properties
    private DbsGroup cv_Prap_Rule;
    private DbsField cv_Prap_Rule_Cv_Coll_Code;
    private DbsField cv_Prap_Rule_Cv_G_Key;
    private DbsField cv_Prap_Rule_Cv_G_Ind;
    private DbsField cv_Prap_Rule_Cv_Soc_Sec;
    private DbsField cv_Prap_Rule_Cv_Dob;
    private DbsField cv_Prap_Rule_Cv_Lob;
    private DbsField cv_Prap_Rule_Cv_Bill_Code;
    private DbsGroup cv_Prap_Rule_Cv_Tiaa_Contr;
    private DbsField cv_Prap_Rule_Cv_Pref;
    private DbsField cv_Prap_Rule_Cv_Cont;
    private DbsGroup cv_Prap_Rule_Cv_Cref_Contr;
    private DbsField cv_Prap_Rule_Cv_C_Pref;
    private DbsField cv_Prap_Rule_Cv_C_Cont;
    private DbsField cv_Prap_Rule_Cv_T_Doi;
    private DbsField cv_Prap_Rule_Cv_C_Doi;
    private DbsField cv_Prap_Rule_Cv_Curr;
    private DbsField cv_Prap_Rule_Cv_T_Age_1st;
    private DbsField cv_Prap_Rule_Cv_C_Age_1st;
    private DbsField cv_Prap_Rule_Cv_Ownership;
    private DbsField cv_Prap_Rule_Cv_Sex;
    private DbsField cv_Prap_Rule_Cv_Mail_Zip;
    private DbsField cv_Prap_Rule_Cv_Name_Addr_Cd;
    private DbsField cv_Prap_Rule_Cv_Dt_Ent_Sys;
    private DbsField cv_Prap_Rule_Cv_Dt_Released;
    private DbsField cv_Prap_Rule_Cv_Dt_Matched;
    private DbsField cv_Prap_Rule_Cv_Dt_Deleted;
    private DbsField cv_Prap_Rule_Cv_Dt_Withdrawn;
    private DbsField cv_Prap_Rule_Cv_Alloc_Discr;
    private DbsField cv_Prap_Rule_Cv_Release_Ind;
    private DbsField cv_Prap_Rule_Cv_Type;
    private DbsField cv_Prap_Rule_Cv_Other_Pols;
    private DbsField cv_Prap_Rule_Cv_Record_Type;
    private DbsField cv_Prap_Rule_Cv_Status;
    private DbsField cv_Prap_Rule_Cv_Numb_Dailys;
    private DbsField cv_Prap_Rule_Cv_Dt_App_Recvd;
    private DbsGroup cv_Prap_Rule_Cv_Allocation_Info;
    private DbsField cv_Prap_Rule_Cv_Allocation;
    private DbsField cv_Prap_Rule_Cv_App_Source;
    private DbsField cv_Prap_Rule_Cv_Region_Code;
    private DbsField cv_Prap_Rule_Cv_Orig_Issue_State;
    private DbsField cv_Prap_Rule_Cv_Ownership_Type;
    private DbsField cv_Prap_Rule_Cv_Lob_Type;
    private DbsField cv_Prap_Rule_Cv_Split_Code;
    private DbsGroup cv_Prap_Rule_Cv_Address_Info;
    private DbsField cv_Prap_Rule_Cv_Address_Line;
    private DbsField cv_Prap_Rule_Cv_City;
    private DbsField cv_Prap_Rule_Cv_Current_State_Code;
    private DbsField cv_Prap_Rule_Cv_Dana_Transaction_Cd;
    private DbsField cv_Prap_Rule_Cv_Dana_Stndrd_Rtn_Cd;
    private DbsField cv_Prap_Rule_Cv_Dana_Finalist_Reason_Cd;
    private DbsField cv_Prap_Rule_Cv_Dana_Addr_Stndrd_Code;
    private DbsField cv_Prap_Rule_Cv_Dana_Stndrd_Overide;
    private DbsField cv_Prap_Rule_Cv_Dana_Postal_Data_Fields;
    private DbsField cv_Prap_Rule_Cv_Dana_Addr_Geographic_Code;
    private DbsField cv_Prap_Rule_Cv_Annuity_Start_Date;
    private DbsGroup cv_Prap_Rule_Cv_Maximum_Alloc_Pct;
    private DbsField cv_Prap_Rule_Cv_Max_Alloc_Pct;
    private DbsField cv_Prap_Rule_Cv_Max_Alloc_Pct_Sign;
    private DbsField cv_Prap_Rule_Cv_Bene_Info_Type;
    private DbsField cv_Prap_Rule_Cv_Primary_Std_Ent;
    private DbsGroup cv_Prap_Rule_Cv_Bene_Primary_Info;
    private DbsField cv_Prap_Rule_Cv_Primary_Bene_Info;
    private DbsField cv_Prap_Rule_Cv_Contingent_Std_Ent;
    private DbsGroup cv_Prap_Rule_Cv_Bene_Contingent_Info;
    private DbsField cv_Prap_Rule_Cv_Contingent_Bene_Info;
    private DbsField cv_Prap_Rule_Cv_Bene_Estate;
    private DbsField cv_Prap_Rule_Cv_Bene_Trust;
    private DbsField cv_Prap_Rule_Cv_Bene_Category;
    private DbsField cv_Prap_Rule_Cv_Mail_Instructions;
    private DbsField cv_Prap_Rule_Cv_Eop_Addl_Cref_Request;
    private DbsField cv_Prap_Rule_Cv_Process_Status;
    private DbsField cv_Prap_Rule_Cv_Coll_St_Cd;
    private DbsField cv_Prap_Rule_Cv_Csm_Sec_Seg;
    private DbsField cv_Prap_Rule_Cv_Rlc_College;
    private DbsField cv_Prap_Rule_Cv_Rlc_Cref_Pref;
    private DbsField cv_Prap_Rule_Cv_Rlc_Cref_Cont;
    private DbsField cv_Prap_Rule_Cv_Cor_Prfx_Nme;
    private DbsField cv_Prap_Rule_Cv_Cor_Last_Nme;
    private DbsField cv_Prap_Rule_Cv_Cor_First_Nme;
    private DbsField cv_Prap_Rule_Cv_Cor_Mddle_Nme;
    private DbsField cv_Prap_Rule_Cv_Cor_Sffx_Nme;
    private DbsField cv_Prap_Rule_Cv_Ph_Hist_Ind;
    private DbsField cv_Prap_Rule_Cv_Rcrd_Updt_Tm_Stamp;
    private DbsField cv_Prap_Rule_Cv_Contact_Mode;
    private DbsField cv_Prap_Rule_Cv_Racf_Id;
    private DbsField cv_Prap_Rule_Cv_Pin_Nbr;
    private DbsGroup cv_Prap_Rule_Cv_Rqst_Log_Dte_Tm;
    private DbsField cv_Prap_Rule_Cv_Rqst_Log_Dte_Time;
    private DbsField cv_Prap_Rule_Cv_Sync_Ind;
    private DbsField cv_Prap_Rule_Cv_Cor_Ind;
    private DbsField cv_Prap_Rule_Cv_Alloc_Ind;
    private DbsField cv_Prap_Rule_Cv_Tiaa_Doi;
    private DbsField cv_Prap_Rule_Cv_Cref_Doi;
    private DbsGroup cv_Prap_Rule_Cv_Mit_Request;
    private DbsField cv_Prap_Rule_Cv_Mit_Unit;
    private DbsField cv_Prap_Rule_Cv_Mit_Wpid;
    private DbsField cv_Prap_Rule_Cv_Ira_Rollover_Type;
    private DbsField cv_Prap_Rule_Cv_Ira_Record_Type;
    private DbsField cv_Prap_Rule_Cv_Mult_App_Status;
    private DbsField cv_Prap_Rule_Cv_Mult_App_Lob;
    private DbsField cv_Prap_Rule_Cv_Mult_App_Lob_Type;
    private DbsField cv_Prap_Rule_Cv_Mult_App_Ppg;
    private DbsField cv_Prap_Rule_Cv_Print_Date;
    private DbsGroup cv_Prap_Rule_Cv_Cntrct_Info;
    private DbsField cv_Prap_Rule_Cv_Cntrct_Type;
    private DbsField cv_Prap_Rule_Cv_Cntrct_Nbr;
    private DbsField cv_Prap_Rule_Cv_Cntrct_Proceeds_Amt;
    private DbsGroup cv_Prap_Rule_Cv_Addr_Info;
    private DbsField cv_Prap_Rule_Cv_Address_Txt;
    private DbsField cv_Prap_Rule_Cv_Address_Dest_Name;
    private DbsField cv_Prap_Rule_Cv_Zip_Code;
    private DbsField cv_Prap_Rule_Cv_Bank_Pymnt_Acct_Nmbr;
    private DbsField cv_Prap_Rule_Cv_Bank_Aba_Acct_Nmbr;
    private DbsField cv_Prap_Rule_Cv_Addr_Usage_Code;
    private DbsField cv_Prap_Rule_Cv_Init_Paymt_Amt;
    private DbsField cv_Prap_Rule_Cv_Init_Paymt_Pct;
    private DbsField cv_Prap_Rule_Cv_Financial_1;
    private DbsField cv_Prap_Rule_Cv_Financial_2;
    private DbsField cv_Prap_Rule_Cv_Financial_3;
    private DbsField cv_Prap_Rule_Cv_Financial_4;
    private DbsField cv_Prap_Rule_Cv_Financial_5;
    private DbsField cv_Prap_Rule_Cv_Irc_Sectn_Grp_Cde;
    private DbsField cv_Prap_Rule_Cv_Irc_Sectn_Cde;
    private DbsField cv_Prap_Rule_Cv_Inst_Link_Cde;
    private DbsField cv_Prap_Rule_Cv_Applcnt_Req_Type;
    private DbsField cv_Prap_Rule_Cv_Addr_Sync_Ind;
    private DbsField cv_Prap_Rule_Cv_Tiaa_Service_Agent;
    private DbsField cv_Prap_Rule_Cv_Prap_Rsch_Mit_Ind;
    private DbsField cv_Prap_Rule_Cv_Ppg_Team_Cde;
    private DbsField cv_Prap_Rule_Cv_Tiaa_Cntrct;
    private DbsField cv_Prap_Rule_Cv_Cref_Cert;
    private DbsField cv_Prap_Rule_Cv_Rlc_Cref_Cert;
    private DbsField cv_Prap_Rule_Cv_Allocation_Model_Type;
    private DbsField cv_Prap_Rule_Cv_Divorce_Ind;
    private DbsField cv_Prap_Rule_Cv_Email_Address;
    private DbsField cv_Prap_Rule_Cv_E_Signed_Appl_Ind;
    private DbsField cv_Prap_Rule_Cv_Eft_Requested_Ind;
    private DbsField cv_Prap_Rule_Cv_Prap_Prem_Rsch_Ind;
    private DbsField cv_Prap_Rule_Cv_Address_Change_Ind;
    private DbsField cv_Prap_Rule_Cv_Allocation_Fmt;
    private DbsField cv_Prap_Rule_Cv_Phone_No;
    private DbsGroup cv_Prap_Rule_Cv_Fund_Identifier;
    private DbsField cv_Prap_Rule_Cv_Fund_Cde;
    private DbsField cv_Prap_Rule_Cv_Allocation_Pct;
    private DbsField cv_Prap_Rule_Cv_Sgrd_Plan_No;
    private DbsField cv_Prap_Rule_Cv_Sgrd_Subplan_No;
    private DbsField cv_Prap_Rule_Cv_Sgrd_Part_Ext;
    private DbsField cv_Prap_Rule_Cv_Sgrd_Divsub;
    private DbsField cv_Prap_Rule_Cv_Text_Udf_1;
    private DbsField cv_Prap_Rule_Cv_Text_Udf_2;
    private DbsField cv_Prap_Rule_Cv_Text_Udf_3;
    private DbsField cv_Prap_Rule_Cv_Replacement_Ind;
    private DbsField cv_Prap_Rule_Cv_Register_Id;
    private DbsField cv_Prap_Rule_Cv_Agent_Or_Racf_Id;
    private DbsField cv_Prap_Rule_Cv_Exempt_Ind;
    private DbsField cv_Prap_Rule_Cv_Arr_Insurer_1;
    private DbsField cv_Prap_Rule_Cv_Arr_Insurer_2;
    private DbsField cv_Prap_Rule_Cv_Arr_Insurer_3;
    private DbsField cv_Prap_Rule_Cv_Arr_1035_Exch_Ind_1;
    private DbsField cv_Prap_Rule_Cv_Arr_1035_Exch_Ind_2;
    private DbsField cv_Prap_Rule_Cv_Arr_1035_Exch_Ind_3;
    private DbsField cv_Prap_Rule_Cv_Autosave_Ind;
    private DbsField cv_Prap_Rule_Cv_As_Cur_Dflt_Opt;
    private DbsField cv_Prap_Rule_Cv_As_Cur_Dflt_Amt;
    private DbsField cv_Prap_Rule_Cv_As_Incr_Opt;
    private DbsField cv_Prap_Rule_Cv_As_Incr_Amt;
    private DbsField cv_Prap_Rule_Cv_As_Max_Pct;
    private DbsField cv_Prap_Rule_Cv_Ae_Opt_Out_Days;
    private DbsField cv_Prap_Rule_Cv_Incmpl_Acct_Ind;
    private DbsField cv_Prap_Rule_Cv_Delete_User_Id;
    private DbsField cv_Prap_Rule_Cv_Delete_Reason_Cd;
    private DbsField cv_Prap_Rule_Cv_Consent_Email_Address;
    private DbsField cv_Prap_Rule_Cv_Welc_E_Delivery_Ind;
    private DbsField cv_Prap_Rule_Cv_Legal_E_Delivery_Ind;
    private DbsField cv_Prap_Rule_Cv_Legal_Ann_Option;
    private DbsField cv_Prap_Rule_Cv_Orchestration_Id;
    private DbsField cv_Prap_Rule_Cv_Mail_Addr_Country_Cd;
    private DbsField cv_Prap_Rule_Cv_Tic_Startdate;
    private DbsField cv_Prap_Rule_Cv_Tic_Enddate;
    private DbsField cv_Prap_Rule_Cv_Tic_Percentage;
    private DbsField cv_Prap_Rule_Cv_Tic_Postdays;
    private DbsField cv_Prap_Rule_Cv_Tic_Limit;
    private DbsField cv_Prap_Rule_Cv_Tic_Postfreq;
    private DbsField cv_Prap_Rule_Cv_Tic_Pl_Level;
    private DbsField cv_Prap_Rule_Cv_Tic_Windowdays;
    private DbsField cv_Prap_Rule_Cv_Tic_Reqdlywindow;
    private DbsField cv_Prap_Rule_Cv_Tic_Recap_Prov;
    private DbsField cv_Prap_Rule_Cv_Ann_Funding_Dt;
    private DbsField cv_Prap_Rule_Cv_Tiaa_Ann_Issue_Dt;
    private DbsField cv_Prap_Rule_Cv_Cref_Ann_Issue_Dt;
    private DbsField cv_Prap_Rule_Cv_Substitution_Contract_Ind;
    private DbsField cv_Prap_Rule_Cv_Conv_Issue_State;
    private DbsField cv_Prap_Rule_Cv_Deceased_Ind;
    private DbsField cv_Prap_Rule_Cv_Non_Proprietary_Pkg_Ind;
    private DbsField cv_Prap_Rule_Cv_Ap_Decedent_Contract;
    private DbsField cv_Prap_Rule_Cv_Relation_To_Decedent;
    private DbsField cv_Prap_Rule_Cv_Ssn_Tin_Ind;
    private DbsField cv_Prap_Rule_Cv_Oneira_Acct_No;
    private DbsField cv_Prap_Rule_Cv_Fund_Source_Cde_1;
    private DbsField cv_Prap_Rule_Cv_Fund_Source_Cde_2;
    private DbsGroup cv_Prap_Rule_Cv_Fund_Identifier_2;
    private DbsField cv_Prap_Rule_Cv_Fund_Cde_2;
    private DbsField cv_Prap_Rule_Cv_Allocation_Pct_2;
    private DbsField cv_Prap_Rule_Cv_Filler;
    private DbsGroup cv_Prap_RuleRedef1;
    private DbsField cv_Prap_Rule_Cv_Rule_Data_1;
    private DbsField cv_Prap_Rule_Cv_Rule_Data_2;
    private DbsField cv_Prap_Rule_Cv_Rule_Data_3;
    private DbsField cv_Prap_Rule_Cv_Rule_Data_4;
    private DbsField cv_Prap_Rule_Cv_Rule_Data_5;
    private DbsField cv_Prap_Rule_Cv_Rule_Data_6;
    private DbsField cv_Prap_Rule_Cv_Rule_Data_7;
    private DbsField cv_Prap_Rule_Cv_Rule_Data_8;
    private DbsField cv_Prap_Rule_Cv_Rule_Data_9;
    private DbsField cv_Prap_Rule_Cv_Rule_Data_10;
    private DbsField cv_Prap_Rule_Cv_Rule_Data_11;
    private DbsField cv_Prap_Rule_Cv_Rule_Data_12;
    private DbsField cv_Prap_Rule_Cv_Rule_Data_13;
    private DbsField cv_Prap_Rule_Cv_Rule_Data_14;
    private DbsField cv_Prap_Rule_Cv_Rule_Data_15;
    private DbsField cv_Prap_Rule_Cv_Rule_Data_16;
    private DbsField cv_Prap_Rule_Cv_Rule_Data_17;
    private DbsField cv_Prap_Rule_Cv_Rule_Data_18;
    private DbsField cv_Prap_Rule_Cv_Rule_Data_19;

    public DbsGroup getCv_Prap_Rule() { return cv_Prap_Rule; }

    public DbsField getCv_Prap_Rule_Cv_Coll_Code() { return cv_Prap_Rule_Cv_Coll_Code; }

    public DbsField getCv_Prap_Rule_Cv_G_Key() { return cv_Prap_Rule_Cv_G_Key; }

    public DbsField getCv_Prap_Rule_Cv_G_Ind() { return cv_Prap_Rule_Cv_G_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Soc_Sec() { return cv_Prap_Rule_Cv_Soc_Sec; }

    public DbsField getCv_Prap_Rule_Cv_Dob() { return cv_Prap_Rule_Cv_Dob; }

    public DbsField getCv_Prap_Rule_Cv_Lob() { return cv_Prap_Rule_Cv_Lob; }

    public DbsField getCv_Prap_Rule_Cv_Bill_Code() { return cv_Prap_Rule_Cv_Bill_Code; }

    public DbsGroup getCv_Prap_Rule_Cv_Tiaa_Contr() { return cv_Prap_Rule_Cv_Tiaa_Contr; }

    public DbsField getCv_Prap_Rule_Cv_Pref() { return cv_Prap_Rule_Cv_Pref; }

    public DbsField getCv_Prap_Rule_Cv_Cont() { return cv_Prap_Rule_Cv_Cont; }

    public DbsGroup getCv_Prap_Rule_Cv_Cref_Contr() { return cv_Prap_Rule_Cv_Cref_Contr; }

    public DbsField getCv_Prap_Rule_Cv_C_Pref() { return cv_Prap_Rule_Cv_C_Pref; }

    public DbsField getCv_Prap_Rule_Cv_C_Cont() { return cv_Prap_Rule_Cv_C_Cont; }

    public DbsField getCv_Prap_Rule_Cv_T_Doi() { return cv_Prap_Rule_Cv_T_Doi; }

    public DbsField getCv_Prap_Rule_Cv_C_Doi() { return cv_Prap_Rule_Cv_C_Doi; }

    public DbsField getCv_Prap_Rule_Cv_Curr() { return cv_Prap_Rule_Cv_Curr; }

    public DbsField getCv_Prap_Rule_Cv_T_Age_1st() { return cv_Prap_Rule_Cv_T_Age_1st; }

    public DbsField getCv_Prap_Rule_Cv_C_Age_1st() { return cv_Prap_Rule_Cv_C_Age_1st; }

    public DbsField getCv_Prap_Rule_Cv_Ownership() { return cv_Prap_Rule_Cv_Ownership; }

    public DbsField getCv_Prap_Rule_Cv_Sex() { return cv_Prap_Rule_Cv_Sex; }

    public DbsField getCv_Prap_Rule_Cv_Mail_Zip() { return cv_Prap_Rule_Cv_Mail_Zip; }

    public DbsField getCv_Prap_Rule_Cv_Name_Addr_Cd() { return cv_Prap_Rule_Cv_Name_Addr_Cd; }

    public DbsField getCv_Prap_Rule_Cv_Dt_Ent_Sys() { return cv_Prap_Rule_Cv_Dt_Ent_Sys; }

    public DbsField getCv_Prap_Rule_Cv_Dt_Released() { return cv_Prap_Rule_Cv_Dt_Released; }

    public DbsField getCv_Prap_Rule_Cv_Dt_Matched() { return cv_Prap_Rule_Cv_Dt_Matched; }

    public DbsField getCv_Prap_Rule_Cv_Dt_Deleted() { return cv_Prap_Rule_Cv_Dt_Deleted; }

    public DbsField getCv_Prap_Rule_Cv_Dt_Withdrawn() { return cv_Prap_Rule_Cv_Dt_Withdrawn; }

    public DbsField getCv_Prap_Rule_Cv_Alloc_Discr() { return cv_Prap_Rule_Cv_Alloc_Discr; }

    public DbsField getCv_Prap_Rule_Cv_Release_Ind() { return cv_Prap_Rule_Cv_Release_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Type() { return cv_Prap_Rule_Cv_Type; }

    public DbsField getCv_Prap_Rule_Cv_Other_Pols() { return cv_Prap_Rule_Cv_Other_Pols; }

    public DbsField getCv_Prap_Rule_Cv_Record_Type() { return cv_Prap_Rule_Cv_Record_Type; }

    public DbsField getCv_Prap_Rule_Cv_Status() { return cv_Prap_Rule_Cv_Status; }

    public DbsField getCv_Prap_Rule_Cv_Numb_Dailys() { return cv_Prap_Rule_Cv_Numb_Dailys; }

    public DbsField getCv_Prap_Rule_Cv_Dt_App_Recvd() { return cv_Prap_Rule_Cv_Dt_App_Recvd; }

    public DbsGroup getCv_Prap_Rule_Cv_Allocation_Info() { return cv_Prap_Rule_Cv_Allocation_Info; }

    public DbsField getCv_Prap_Rule_Cv_Allocation() { return cv_Prap_Rule_Cv_Allocation; }

    public DbsField getCv_Prap_Rule_Cv_App_Source() { return cv_Prap_Rule_Cv_App_Source; }

    public DbsField getCv_Prap_Rule_Cv_Region_Code() { return cv_Prap_Rule_Cv_Region_Code; }

    public DbsField getCv_Prap_Rule_Cv_Orig_Issue_State() { return cv_Prap_Rule_Cv_Orig_Issue_State; }

    public DbsField getCv_Prap_Rule_Cv_Ownership_Type() { return cv_Prap_Rule_Cv_Ownership_Type; }

    public DbsField getCv_Prap_Rule_Cv_Lob_Type() { return cv_Prap_Rule_Cv_Lob_Type; }

    public DbsField getCv_Prap_Rule_Cv_Split_Code() { return cv_Prap_Rule_Cv_Split_Code; }

    public DbsGroup getCv_Prap_Rule_Cv_Address_Info() { return cv_Prap_Rule_Cv_Address_Info; }

    public DbsField getCv_Prap_Rule_Cv_Address_Line() { return cv_Prap_Rule_Cv_Address_Line; }

    public DbsField getCv_Prap_Rule_Cv_City() { return cv_Prap_Rule_Cv_City; }

    public DbsField getCv_Prap_Rule_Cv_Current_State_Code() { return cv_Prap_Rule_Cv_Current_State_Code; }

    public DbsField getCv_Prap_Rule_Cv_Dana_Transaction_Cd() { return cv_Prap_Rule_Cv_Dana_Transaction_Cd; }

    public DbsField getCv_Prap_Rule_Cv_Dana_Stndrd_Rtn_Cd() { return cv_Prap_Rule_Cv_Dana_Stndrd_Rtn_Cd; }

    public DbsField getCv_Prap_Rule_Cv_Dana_Finalist_Reason_Cd() { return cv_Prap_Rule_Cv_Dana_Finalist_Reason_Cd; }

    public DbsField getCv_Prap_Rule_Cv_Dana_Addr_Stndrd_Code() { return cv_Prap_Rule_Cv_Dana_Addr_Stndrd_Code; }

    public DbsField getCv_Prap_Rule_Cv_Dana_Stndrd_Overide() { return cv_Prap_Rule_Cv_Dana_Stndrd_Overide; }

    public DbsField getCv_Prap_Rule_Cv_Dana_Postal_Data_Fields() { return cv_Prap_Rule_Cv_Dana_Postal_Data_Fields; }

    public DbsField getCv_Prap_Rule_Cv_Dana_Addr_Geographic_Code() { return cv_Prap_Rule_Cv_Dana_Addr_Geographic_Code; }

    public DbsField getCv_Prap_Rule_Cv_Annuity_Start_Date() { return cv_Prap_Rule_Cv_Annuity_Start_Date; }

    public DbsGroup getCv_Prap_Rule_Cv_Maximum_Alloc_Pct() { return cv_Prap_Rule_Cv_Maximum_Alloc_Pct; }

    public DbsField getCv_Prap_Rule_Cv_Max_Alloc_Pct() { return cv_Prap_Rule_Cv_Max_Alloc_Pct; }

    public DbsField getCv_Prap_Rule_Cv_Max_Alloc_Pct_Sign() { return cv_Prap_Rule_Cv_Max_Alloc_Pct_Sign; }

    public DbsField getCv_Prap_Rule_Cv_Bene_Info_Type() { return cv_Prap_Rule_Cv_Bene_Info_Type; }

    public DbsField getCv_Prap_Rule_Cv_Primary_Std_Ent() { return cv_Prap_Rule_Cv_Primary_Std_Ent; }

    public DbsGroup getCv_Prap_Rule_Cv_Bene_Primary_Info() { return cv_Prap_Rule_Cv_Bene_Primary_Info; }

    public DbsField getCv_Prap_Rule_Cv_Primary_Bene_Info() { return cv_Prap_Rule_Cv_Primary_Bene_Info; }

    public DbsField getCv_Prap_Rule_Cv_Contingent_Std_Ent() { return cv_Prap_Rule_Cv_Contingent_Std_Ent; }

    public DbsGroup getCv_Prap_Rule_Cv_Bene_Contingent_Info() { return cv_Prap_Rule_Cv_Bene_Contingent_Info; }

    public DbsField getCv_Prap_Rule_Cv_Contingent_Bene_Info() { return cv_Prap_Rule_Cv_Contingent_Bene_Info; }

    public DbsField getCv_Prap_Rule_Cv_Bene_Estate() { return cv_Prap_Rule_Cv_Bene_Estate; }

    public DbsField getCv_Prap_Rule_Cv_Bene_Trust() { return cv_Prap_Rule_Cv_Bene_Trust; }

    public DbsField getCv_Prap_Rule_Cv_Bene_Category() { return cv_Prap_Rule_Cv_Bene_Category; }

    public DbsField getCv_Prap_Rule_Cv_Mail_Instructions() { return cv_Prap_Rule_Cv_Mail_Instructions; }

    public DbsField getCv_Prap_Rule_Cv_Eop_Addl_Cref_Request() { return cv_Prap_Rule_Cv_Eop_Addl_Cref_Request; }

    public DbsField getCv_Prap_Rule_Cv_Process_Status() { return cv_Prap_Rule_Cv_Process_Status; }

    public DbsField getCv_Prap_Rule_Cv_Coll_St_Cd() { return cv_Prap_Rule_Cv_Coll_St_Cd; }

    public DbsField getCv_Prap_Rule_Cv_Csm_Sec_Seg() { return cv_Prap_Rule_Cv_Csm_Sec_Seg; }

    public DbsField getCv_Prap_Rule_Cv_Rlc_College() { return cv_Prap_Rule_Cv_Rlc_College; }

    public DbsField getCv_Prap_Rule_Cv_Rlc_Cref_Pref() { return cv_Prap_Rule_Cv_Rlc_Cref_Pref; }

    public DbsField getCv_Prap_Rule_Cv_Rlc_Cref_Cont() { return cv_Prap_Rule_Cv_Rlc_Cref_Cont; }

    public DbsField getCv_Prap_Rule_Cv_Cor_Prfx_Nme() { return cv_Prap_Rule_Cv_Cor_Prfx_Nme; }

    public DbsField getCv_Prap_Rule_Cv_Cor_Last_Nme() { return cv_Prap_Rule_Cv_Cor_Last_Nme; }

    public DbsField getCv_Prap_Rule_Cv_Cor_First_Nme() { return cv_Prap_Rule_Cv_Cor_First_Nme; }

    public DbsField getCv_Prap_Rule_Cv_Cor_Mddle_Nme() { return cv_Prap_Rule_Cv_Cor_Mddle_Nme; }

    public DbsField getCv_Prap_Rule_Cv_Cor_Sffx_Nme() { return cv_Prap_Rule_Cv_Cor_Sffx_Nme; }

    public DbsField getCv_Prap_Rule_Cv_Ph_Hist_Ind() { return cv_Prap_Rule_Cv_Ph_Hist_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Rcrd_Updt_Tm_Stamp() { return cv_Prap_Rule_Cv_Rcrd_Updt_Tm_Stamp; }

    public DbsField getCv_Prap_Rule_Cv_Contact_Mode() { return cv_Prap_Rule_Cv_Contact_Mode; }

    public DbsField getCv_Prap_Rule_Cv_Racf_Id() { return cv_Prap_Rule_Cv_Racf_Id; }

    public DbsField getCv_Prap_Rule_Cv_Pin_Nbr() { return cv_Prap_Rule_Cv_Pin_Nbr; }

    public DbsGroup getCv_Prap_Rule_Cv_Rqst_Log_Dte_Tm() { return cv_Prap_Rule_Cv_Rqst_Log_Dte_Tm; }

    public DbsField getCv_Prap_Rule_Cv_Rqst_Log_Dte_Time() { return cv_Prap_Rule_Cv_Rqst_Log_Dte_Time; }

    public DbsField getCv_Prap_Rule_Cv_Sync_Ind() { return cv_Prap_Rule_Cv_Sync_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Cor_Ind() { return cv_Prap_Rule_Cv_Cor_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Alloc_Ind() { return cv_Prap_Rule_Cv_Alloc_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Tiaa_Doi() { return cv_Prap_Rule_Cv_Tiaa_Doi; }

    public DbsField getCv_Prap_Rule_Cv_Cref_Doi() { return cv_Prap_Rule_Cv_Cref_Doi; }

    public DbsGroup getCv_Prap_Rule_Cv_Mit_Request() { return cv_Prap_Rule_Cv_Mit_Request; }

    public DbsField getCv_Prap_Rule_Cv_Mit_Unit() { return cv_Prap_Rule_Cv_Mit_Unit; }

    public DbsField getCv_Prap_Rule_Cv_Mit_Wpid() { return cv_Prap_Rule_Cv_Mit_Wpid; }

    public DbsField getCv_Prap_Rule_Cv_Ira_Rollover_Type() { return cv_Prap_Rule_Cv_Ira_Rollover_Type; }

    public DbsField getCv_Prap_Rule_Cv_Ira_Record_Type() { return cv_Prap_Rule_Cv_Ira_Record_Type; }

    public DbsField getCv_Prap_Rule_Cv_Mult_App_Status() { return cv_Prap_Rule_Cv_Mult_App_Status; }

    public DbsField getCv_Prap_Rule_Cv_Mult_App_Lob() { return cv_Prap_Rule_Cv_Mult_App_Lob; }

    public DbsField getCv_Prap_Rule_Cv_Mult_App_Lob_Type() { return cv_Prap_Rule_Cv_Mult_App_Lob_Type; }

    public DbsField getCv_Prap_Rule_Cv_Mult_App_Ppg() { return cv_Prap_Rule_Cv_Mult_App_Ppg; }

    public DbsField getCv_Prap_Rule_Cv_Print_Date() { return cv_Prap_Rule_Cv_Print_Date; }

    public DbsGroup getCv_Prap_Rule_Cv_Cntrct_Info() { return cv_Prap_Rule_Cv_Cntrct_Info; }

    public DbsField getCv_Prap_Rule_Cv_Cntrct_Type() { return cv_Prap_Rule_Cv_Cntrct_Type; }

    public DbsField getCv_Prap_Rule_Cv_Cntrct_Nbr() { return cv_Prap_Rule_Cv_Cntrct_Nbr; }

    public DbsField getCv_Prap_Rule_Cv_Cntrct_Proceeds_Amt() { return cv_Prap_Rule_Cv_Cntrct_Proceeds_Amt; }

    public DbsGroup getCv_Prap_Rule_Cv_Addr_Info() { return cv_Prap_Rule_Cv_Addr_Info; }

    public DbsField getCv_Prap_Rule_Cv_Address_Txt() { return cv_Prap_Rule_Cv_Address_Txt; }

    public DbsField getCv_Prap_Rule_Cv_Address_Dest_Name() { return cv_Prap_Rule_Cv_Address_Dest_Name; }

    public DbsField getCv_Prap_Rule_Cv_Zip_Code() { return cv_Prap_Rule_Cv_Zip_Code; }

    public DbsField getCv_Prap_Rule_Cv_Bank_Pymnt_Acct_Nmbr() { return cv_Prap_Rule_Cv_Bank_Pymnt_Acct_Nmbr; }

    public DbsField getCv_Prap_Rule_Cv_Bank_Aba_Acct_Nmbr() { return cv_Prap_Rule_Cv_Bank_Aba_Acct_Nmbr; }

    public DbsField getCv_Prap_Rule_Cv_Addr_Usage_Code() { return cv_Prap_Rule_Cv_Addr_Usage_Code; }

    public DbsField getCv_Prap_Rule_Cv_Init_Paymt_Amt() { return cv_Prap_Rule_Cv_Init_Paymt_Amt; }

    public DbsField getCv_Prap_Rule_Cv_Init_Paymt_Pct() { return cv_Prap_Rule_Cv_Init_Paymt_Pct; }

    public DbsField getCv_Prap_Rule_Cv_Financial_1() { return cv_Prap_Rule_Cv_Financial_1; }

    public DbsField getCv_Prap_Rule_Cv_Financial_2() { return cv_Prap_Rule_Cv_Financial_2; }

    public DbsField getCv_Prap_Rule_Cv_Financial_3() { return cv_Prap_Rule_Cv_Financial_3; }

    public DbsField getCv_Prap_Rule_Cv_Financial_4() { return cv_Prap_Rule_Cv_Financial_4; }

    public DbsField getCv_Prap_Rule_Cv_Financial_5() { return cv_Prap_Rule_Cv_Financial_5; }

    public DbsField getCv_Prap_Rule_Cv_Irc_Sectn_Grp_Cde() { return cv_Prap_Rule_Cv_Irc_Sectn_Grp_Cde; }

    public DbsField getCv_Prap_Rule_Cv_Irc_Sectn_Cde() { return cv_Prap_Rule_Cv_Irc_Sectn_Cde; }

    public DbsField getCv_Prap_Rule_Cv_Inst_Link_Cde() { return cv_Prap_Rule_Cv_Inst_Link_Cde; }

    public DbsField getCv_Prap_Rule_Cv_Applcnt_Req_Type() { return cv_Prap_Rule_Cv_Applcnt_Req_Type; }

    public DbsField getCv_Prap_Rule_Cv_Addr_Sync_Ind() { return cv_Prap_Rule_Cv_Addr_Sync_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Tiaa_Service_Agent() { return cv_Prap_Rule_Cv_Tiaa_Service_Agent; }

    public DbsField getCv_Prap_Rule_Cv_Prap_Rsch_Mit_Ind() { return cv_Prap_Rule_Cv_Prap_Rsch_Mit_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Ppg_Team_Cde() { return cv_Prap_Rule_Cv_Ppg_Team_Cde; }

    public DbsField getCv_Prap_Rule_Cv_Tiaa_Cntrct() { return cv_Prap_Rule_Cv_Tiaa_Cntrct; }

    public DbsField getCv_Prap_Rule_Cv_Cref_Cert() { return cv_Prap_Rule_Cv_Cref_Cert; }

    public DbsField getCv_Prap_Rule_Cv_Rlc_Cref_Cert() { return cv_Prap_Rule_Cv_Rlc_Cref_Cert; }

    public DbsField getCv_Prap_Rule_Cv_Allocation_Model_Type() { return cv_Prap_Rule_Cv_Allocation_Model_Type; }

    public DbsField getCv_Prap_Rule_Cv_Divorce_Ind() { return cv_Prap_Rule_Cv_Divorce_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Email_Address() { return cv_Prap_Rule_Cv_Email_Address; }

    public DbsField getCv_Prap_Rule_Cv_E_Signed_Appl_Ind() { return cv_Prap_Rule_Cv_E_Signed_Appl_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Eft_Requested_Ind() { return cv_Prap_Rule_Cv_Eft_Requested_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Prap_Prem_Rsch_Ind() { return cv_Prap_Rule_Cv_Prap_Prem_Rsch_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Address_Change_Ind() { return cv_Prap_Rule_Cv_Address_Change_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Allocation_Fmt() { return cv_Prap_Rule_Cv_Allocation_Fmt; }

    public DbsField getCv_Prap_Rule_Cv_Phone_No() { return cv_Prap_Rule_Cv_Phone_No; }

    public DbsGroup getCv_Prap_Rule_Cv_Fund_Identifier() { return cv_Prap_Rule_Cv_Fund_Identifier; }

    public DbsField getCv_Prap_Rule_Cv_Fund_Cde() { return cv_Prap_Rule_Cv_Fund_Cde; }

    public DbsField getCv_Prap_Rule_Cv_Allocation_Pct() { return cv_Prap_Rule_Cv_Allocation_Pct; }

    public DbsField getCv_Prap_Rule_Cv_Sgrd_Plan_No() { return cv_Prap_Rule_Cv_Sgrd_Plan_No; }

    public DbsField getCv_Prap_Rule_Cv_Sgrd_Subplan_No() { return cv_Prap_Rule_Cv_Sgrd_Subplan_No; }

    public DbsField getCv_Prap_Rule_Cv_Sgrd_Part_Ext() { return cv_Prap_Rule_Cv_Sgrd_Part_Ext; }

    public DbsField getCv_Prap_Rule_Cv_Sgrd_Divsub() { return cv_Prap_Rule_Cv_Sgrd_Divsub; }

    public DbsField getCv_Prap_Rule_Cv_Text_Udf_1() { return cv_Prap_Rule_Cv_Text_Udf_1; }

    public DbsField getCv_Prap_Rule_Cv_Text_Udf_2() { return cv_Prap_Rule_Cv_Text_Udf_2; }

    public DbsField getCv_Prap_Rule_Cv_Text_Udf_3() { return cv_Prap_Rule_Cv_Text_Udf_3; }

    public DbsField getCv_Prap_Rule_Cv_Replacement_Ind() { return cv_Prap_Rule_Cv_Replacement_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Register_Id() { return cv_Prap_Rule_Cv_Register_Id; }

    public DbsField getCv_Prap_Rule_Cv_Agent_Or_Racf_Id() { return cv_Prap_Rule_Cv_Agent_Or_Racf_Id; }

    public DbsField getCv_Prap_Rule_Cv_Exempt_Ind() { return cv_Prap_Rule_Cv_Exempt_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Arr_Insurer_1() { return cv_Prap_Rule_Cv_Arr_Insurer_1; }

    public DbsField getCv_Prap_Rule_Cv_Arr_Insurer_2() { return cv_Prap_Rule_Cv_Arr_Insurer_2; }

    public DbsField getCv_Prap_Rule_Cv_Arr_Insurer_3() { return cv_Prap_Rule_Cv_Arr_Insurer_3; }

    public DbsField getCv_Prap_Rule_Cv_Arr_1035_Exch_Ind_1() { return cv_Prap_Rule_Cv_Arr_1035_Exch_Ind_1; }

    public DbsField getCv_Prap_Rule_Cv_Arr_1035_Exch_Ind_2() { return cv_Prap_Rule_Cv_Arr_1035_Exch_Ind_2; }

    public DbsField getCv_Prap_Rule_Cv_Arr_1035_Exch_Ind_3() { return cv_Prap_Rule_Cv_Arr_1035_Exch_Ind_3; }

    public DbsField getCv_Prap_Rule_Cv_Autosave_Ind() { return cv_Prap_Rule_Cv_Autosave_Ind; }

    public DbsField getCv_Prap_Rule_Cv_As_Cur_Dflt_Opt() { return cv_Prap_Rule_Cv_As_Cur_Dflt_Opt; }

    public DbsField getCv_Prap_Rule_Cv_As_Cur_Dflt_Amt() { return cv_Prap_Rule_Cv_As_Cur_Dflt_Amt; }

    public DbsField getCv_Prap_Rule_Cv_As_Incr_Opt() { return cv_Prap_Rule_Cv_As_Incr_Opt; }

    public DbsField getCv_Prap_Rule_Cv_As_Incr_Amt() { return cv_Prap_Rule_Cv_As_Incr_Amt; }

    public DbsField getCv_Prap_Rule_Cv_As_Max_Pct() { return cv_Prap_Rule_Cv_As_Max_Pct; }

    public DbsField getCv_Prap_Rule_Cv_Ae_Opt_Out_Days() { return cv_Prap_Rule_Cv_Ae_Opt_Out_Days; }

    public DbsField getCv_Prap_Rule_Cv_Incmpl_Acct_Ind() { return cv_Prap_Rule_Cv_Incmpl_Acct_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Delete_User_Id() { return cv_Prap_Rule_Cv_Delete_User_Id; }

    public DbsField getCv_Prap_Rule_Cv_Delete_Reason_Cd() { return cv_Prap_Rule_Cv_Delete_Reason_Cd; }

    public DbsField getCv_Prap_Rule_Cv_Consent_Email_Address() { return cv_Prap_Rule_Cv_Consent_Email_Address; }

    public DbsField getCv_Prap_Rule_Cv_Welc_E_Delivery_Ind() { return cv_Prap_Rule_Cv_Welc_E_Delivery_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Legal_E_Delivery_Ind() { return cv_Prap_Rule_Cv_Legal_E_Delivery_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Legal_Ann_Option() { return cv_Prap_Rule_Cv_Legal_Ann_Option; }

    public DbsField getCv_Prap_Rule_Cv_Orchestration_Id() { return cv_Prap_Rule_Cv_Orchestration_Id; }

    public DbsField getCv_Prap_Rule_Cv_Mail_Addr_Country_Cd() { return cv_Prap_Rule_Cv_Mail_Addr_Country_Cd; }

    public DbsField getCv_Prap_Rule_Cv_Tic_Startdate() { return cv_Prap_Rule_Cv_Tic_Startdate; }

    public DbsField getCv_Prap_Rule_Cv_Tic_Enddate() { return cv_Prap_Rule_Cv_Tic_Enddate; }

    public DbsField getCv_Prap_Rule_Cv_Tic_Percentage() { return cv_Prap_Rule_Cv_Tic_Percentage; }

    public DbsField getCv_Prap_Rule_Cv_Tic_Postdays() { return cv_Prap_Rule_Cv_Tic_Postdays; }

    public DbsField getCv_Prap_Rule_Cv_Tic_Limit() { return cv_Prap_Rule_Cv_Tic_Limit; }

    public DbsField getCv_Prap_Rule_Cv_Tic_Postfreq() { return cv_Prap_Rule_Cv_Tic_Postfreq; }

    public DbsField getCv_Prap_Rule_Cv_Tic_Pl_Level() { return cv_Prap_Rule_Cv_Tic_Pl_Level; }

    public DbsField getCv_Prap_Rule_Cv_Tic_Windowdays() { return cv_Prap_Rule_Cv_Tic_Windowdays; }

    public DbsField getCv_Prap_Rule_Cv_Tic_Reqdlywindow() { return cv_Prap_Rule_Cv_Tic_Reqdlywindow; }

    public DbsField getCv_Prap_Rule_Cv_Tic_Recap_Prov() { return cv_Prap_Rule_Cv_Tic_Recap_Prov; }

    public DbsField getCv_Prap_Rule_Cv_Ann_Funding_Dt() { return cv_Prap_Rule_Cv_Ann_Funding_Dt; }

    public DbsField getCv_Prap_Rule_Cv_Tiaa_Ann_Issue_Dt() { return cv_Prap_Rule_Cv_Tiaa_Ann_Issue_Dt; }

    public DbsField getCv_Prap_Rule_Cv_Cref_Ann_Issue_Dt() { return cv_Prap_Rule_Cv_Cref_Ann_Issue_Dt; }

    public DbsField getCv_Prap_Rule_Cv_Substitution_Contract_Ind() { return cv_Prap_Rule_Cv_Substitution_Contract_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Conv_Issue_State() { return cv_Prap_Rule_Cv_Conv_Issue_State; }

    public DbsField getCv_Prap_Rule_Cv_Deceased_Ind() { return cv_Prap_Rule_Cv_Deceased_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Non_Proprietary_Pkg_Ind() { return cv_Prap_Rule_Cv_Non_Proprietary_Pkg_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Ap_Decedent_Contract() { return cv_Prap_Rule_Cv_Ap_Decedent_Contract; }

    public DbsField getCv_Prap_Rule_Cv_Relation_To_Decedent() { return cv_Prap_Rule_Cv_Relation_To_Decedent; }

    public DbsField getCv_Prap_Rule_Cv_Ssn_Tin_Ind() { return cv_Prap_Rule_Cv_Ssn_Tin_Ind; }

    public DbsField getCv_Prap_Rule_Cv_Oneira_Acct_No() { return cv_Prap_Rule_Cv_Oneira_Acct_No; }

    public DbsField getCv_Prap_Rule_Cv_Fund_Source_Cde_1() { return cv_Prap_Rule_Cv_Fund_Source_Cde_1; }

    public DbsField getCv_Prap_Rule_Cv_Fund_Source_Cde_2() { return cv_Prap_Rule_Cv_Fund_Source_Cde_2; }

    public DbsGroup getCv_Prap_Rule_Cv_Fund_Identifier_2() { return cv_Prap_Rule_Cv_Fund_Identifier_2; }

    public DbsField getCv_Prap_Rule_Cv_Fund_Cde_2() { return cv_Prap_Rule_Cv_Fund_Cde_2; }

    public DbsField getCv_Prap_Rule_Cv_Allocation_Pct_2() { return cv_Prap_Rule_Cv_Allocation_Pct_2; }

    public DbsField getCv_Prap_Rule_Cv_Filler() { return cv_Prap_Rule_Cv_Filler; }

    public DbsGroup getCv_Prap_RuleRedef1() { return cv_Prap_RuleRedef1; }

    public DbsField getCv_Prap_Rule_Cv_Rule_Data_1() { return cv_Prap_Rule_Cv_Rule_Data_1; }

    public DbsField getCv_Prap_Rule_Cv_Rule_Data_2() { return cv_Prap_Rule_Cv_Rule_Data_2; }

    public DbsField getCv_Prap_Rule_Cv_Rule_Data_3() { return cv_Prap_Rule_Cv_Rule_Data_3; }

    public DbsField getCv_Prap_Rule_Cv_Rule_Data_4() { return cv_Prap_Rule_Cv_Rule_Data_4; }

    public DbsField getCv_Prap_Rule_Cv_Rule_Data_5() { return cv_Prap_Rule_Cv_Rule_Data_5; }

    public DbsField getCv_Prap_Rule_Cv_Rule_Data_6() { return cv_Prap_Rule_Cv_Rule_Data_6; }

    public DbsField getCv_Prap_Rule_Cv_Rule_Data_7() { return cv_Prap_Rule_Cv_Rule_Data_7; }

    public DbsField getCv_Prap_Rule_Cv_Rule_Data_8() { return cv_Prap_Rule_Cv_Rule_Data_8; }

    public DbsField getCv_Prap_Rule_Cv_Rule_Data_9() { return cv_Prap_Rule_Cv_Rule_Data_9; }

    public DbsField getCv_Prap_Rule_Cv_Rule_Data_10() { return cv_Prap_Rule_Cv_Rule_Data_10; }

    public DbsField getCv_Prap_Rule_Cv_Rule_Data_11() { return cv_Prap_Rule_Cv_Rule_Data_11; }

    public DbsField getCv_Prap_Rule_Cv_Rule_Data_12() { return cv_Prap_Rule_Cv_Rule_Data_12; }

    public DbsField getCv_Prap_Rule_Cv_Rule_Data_13() { return cv_Prap_Rule_Cv_Rule_Data_13; }

    public DbsField getCv_Prap_Rule_Cv_Rule_Data_14() { return cv_Prap_Rule_Cv_Rule_Data_14; }

    public DbsField getCv_Prap_Rule_Cv_Rule_Data_15() { return cv_Prap_Rule_Cv_Rule_Data_15; }

    public DbsField getCv_Prap_Rule_Cv_Rule_Data_16() { return cv_Prap_Rule_Cv_Rule_Data_16; }

    public DbsField getCv_Prap_Rule_Cv_Rule_Data_17() { return cv_Prap_Rule_Cv_Rule_Data_17; }

    public DbsField getCv_Prap_Rule_Cv_Rule_Data_18() { return cv_Prap_Rule_Cv_Rule_Data_18; }

    public DbsField getCv_Prap_Rule_Cv_Rule_Data_19() { return cv_Prap_Rule_Cv_Rule_Data_19; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cv_Prap_Rule = newGroupInRecord("cv_Prap_Rule", "CV-PRAP-RULE");
        cv_Prap_Rule_Cv_Coll_Code = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Coll_Code", "CV-COLL-CODE", FieldType.STRING, 6);
        cv_Prap_Rule_Cv_G_Key = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_G_Key", "CV-G-KEY", FieldType.PACKED_DECIMAL, 4);
        cv_Prap_Rule_Cv_G_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_G_Ind", "CV-G-IND", FieldType.NUMERIC, 1);
        cv_Prap_Rule_Cv_Soc_Sec = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Soc_Sec", "CV-SOC-SEC", FieldType.PACKED_DECIMAL, 9);
        cv_Prap_Rule_Cv_Dob = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Dob", "CV-DOB", FieldType.PACKED_DECIMAL, 8);
        cv_Prap_Rule_Cv_Lob = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Lob", "CV-LOB", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Bill_Code = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Bill_Code", "CV-BILL-CODE", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Tiaa_Contr = cv_Prap_Rule.newGroupInGroup("cv_Prap_Rule_Cv_Tiaa_Contr", "CV-TIAA-CONTR");
        cv_Prap_Rule_Cv_Pref = cv_Prap_Rule_Cv_Tiaa_Contr.newFieldInGroup("cv_Prap_Rule_Cv_Pref", "CV-PREF", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Cont = cv_Prap_Rule_Cv_Tiaa_Contr.newFieldInGroup("cv_Prap_Rule_Cv_Cont", "CV-CONT", FieldType.PACKED_DECIMAL, 7);
        cv_Prap_Rule_Cv_Cref_Contr = cv_Prap_Rule.newGroupInGroup("cv_Prap_Rule_Cv_Cref_Contr", "CV-CREF-CONTR");
        cv_Prap_Rule_Cv_C_Pref = cv_Prap_Rule_Cv_Cref_Contr.newFieldInGroup("cv_Prap_Rule_Cv_C_Pref", "CV-C-PREF", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_C_Cont = cv_Prap_Rule_Cv_Cref_Contr.newFieldInGroup("cv_Prap_Rule_Cv_C_Cont", "CV-C-CONT", FieldType.PACKED_DECIMAL, 7);
        cv_Prap_Rule_Cv_T_Doi = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_T_Doi", "CV-T-DOI", FieldType.PACKED_DECIMAL, 4);
        cv_Prap_Rule_Cv_C_Doi = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_C_Doi", "CV-C-DOI", FieldType.PACKED_DECIMAL, 4);
        cv_Prap_Rule_Cv_Curr = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Curr", "CV-CURR", FieldType.NUMERIC, 1);
        cv_Prap_Rule_Cv_T_Age_1st = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_T_Age_1st", "CV-T-AGE-1ST", FieldType.PACKED_DECIMAL, 4);
        cv_Prap_Rule_Cv_C_Age_1st = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_C_Age_1st", "CV-C-AGE-1ST", FieldType.PACKED_DECIMAL, 4);
        cv_Prap_Rule_Cv_Ownership = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Ownership", "CV-OWNERSHIP", FieldType.NUMERIC, 1);
        cv_Prap_Rule_Cv_Sex = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Sex", "CV-SEX", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Mail_Zip = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Mail_Zip", "CV-MAIL-ZIP", FieldType.STRING, 5);
        cv_Prap_Rule_Cv_Name_Addr_Cd = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Name_Addr_Cd", "CV-NAME-ADDR-CD", FieldType.STRING, 5);
        cv_Prap_Rule_Cv_Dt_Ent_Sys = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Dt_Ent_Sys", "CV-DT-ENT-SYS", FieldType.PACKED_DECIMAL, 8);
        cv_Prap_Rule_Cv_Dt_Released = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Dt_Released", "CV-DT-RELEASED", FieldType.PACKED_DECIMAL, 8);
        cv_Prap_Rule_Cv_Dt_Matched = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Dt_Matched", "CV-DT-MATCHED", FieldType.PACKED_DECIMAL, 8);
        cv_Prap_Rule_Cv_Dt_Deleted = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Dt_Deleted", "CV-DT-DELETED", FieldType.PACKED_DECIMAL, 8);
        cv_Prap_Rule_Cv_Dt_Withdrawn = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Dt_Withdrawn", "CV-DT-WITHDRAWN", FieldType.PACKED_DECIMAL, 8);
        cv_Prap_Rule_Cv_Alloc_Discr = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Alloc_Discr", "CV-ALLOC-DISCR", FieldType.NUMERIC, 1);
        cv_Prap_Rule_Cv_Release_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Release_Ind", "CV-RELEASE-IND", FieldType.NUMERIC, 1);
        cv_Prap_Rule_Cv_Type = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Type", "CV-TYPE", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Other_Pols = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Other_Pols", "CV-OTHER-POLS", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Record_Type = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Record_Type", "CV-RECORD-TYPE", FieldType.NUMERIC, 1);
        cv_Prap_Rule_Cv_Status = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Status", "CV-STATUS", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Numb_Dailys = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Numb_Dailys", "CV-NUMB-DAILYS", FieldType.NUMERIC, 1);
        cv_Prap_Rule_Cv_Dt_App_Recvd = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Dt_App_Recvd", "CV-DT-APP-RECVD", FieldType.PACKED_DECIMAL, 8);
        cv_Prap_Rule_Cv_Allocation_Info = cv_Prap_Rule.newGroupInGroup("cv_Prap_Rule_Cv_Allocation_Info", "CV-ALLOCATION-INFO");
        cv_Prap_Rule_Cv_Allocation = cv_Prap_Rule_Cv_Allocation_Info.newFieldArrayInGroup("cv_Prap_Rule_Cv_Allocation", "CV-ALLOCATION", FieldType.PACKED_DECIMAL, 
            3, new DbsArrayController(1,20));
        cv_Prap_Rule_Cv_App_Source = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_App_Source", "CV-APP-SOURCE", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Region_Code = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Region_Code", "CV-REGION-CODE", FieldType.STRING, 3);
        cv_Prap_Rule_Cv_Orig_Issue_State = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Orig_Issue_State", "CV-ORIG-ISSUE-STATE", FieldType.STRING, 2);
        cv_Prap_Rule_Cv_Ownership_Type = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Ownership_Type", "CV-OWNERSHIP-TYPE", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Lob_Type = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Lob_Type", "CV-LOB-TYPE", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Split_Code = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Split_Code", "CV-SPLIT-CODE", FieldType.STRING, 2);
        cv_Prap_Rule_Cv_Address_Info = cv_Prap_Rule.newGroupInGroup("cv_Prap_Rule_Cv_Address_Info", "CV-ADDRESS-INFO");
        cv_Prap_Rule_Cv_Address_Line = cv_Prap_Rule_Cv_Address_Info.newFieldArrayInGroup("cv_Prap_Rule_Cv_Address_Line", "CV-ADDRESS-LINE", FieldType.STRING, 
            35, new DbsArrayController(1,5));
        cv_Prap_Rule_Cv_City = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_City", "CV-CITY", FieldType.STRING, 27);
        cv_Prap_Rule_Cv_Current_State_Code = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Current_State_Code", "CV-CURRENT-STATE-CODE", FieldType.STRING, 
            2);
        cv_Prap_Rule_Cv_Dana_Transaction_Cd = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Dana_Transaction_Cd", "CV-DANA-TRANSACTION-CD", FieldType.STRING, 
            2);
        cv_Prap_Rule_Cv_Dana_Stndrd_Rtn_Cd = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Dana_Stndrd_Rtn_Cd", "CV-DANA-STNDRD-RTN-CD", FieldType.STRING, 
            2);
        cv_Prap_Rule_Cv_Dana_Finalist_Reason_Cd = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Dana_Finalist_Reason_Cd", "CV-DANA-FINALIST-REASON-CD", 
            FieldType.STRING, 10);
        cv_Prap_Rule_Cv_Dana_Addr_Stndrd_Code = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Dana_Addr_Stndrd_Code", "CV-DANA-ADDR-STNDRD-CODE", FieldType.STRING, 
            1);
        cv_Prap_Rule_Cv_Dana_Stndrd_Overide = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Dana_Stndrd_Overide", "CV-DANA-STNDRD-OVERIDE", FieldType.STRING, 
            1);
        cv_Prap_Rule_Cv_Dana_Postal_Data_Fields = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Dana_Postal_Data_Fields", "CV-DANA-POSTAL-DATA-FIELDS", 
            FieldType.STRING, 44);
        cv_Prap_Rule_Cv_Dana_Addr_Geographic_Code = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Dana_Addr_Geographic_Code", "CV-DANA-ADDR-GEOGRAPHIC-CODE", 
            FieldType.STRING, 2);
        cv_Prap_Rule_Cv_Annuity_Start_Date = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Annuity_Start_Date", "CV-ANNUITY-START-DATE", FieldType.STRING, 
            8);
        cv_Prap_Rule_Cv_Maximum_Alloc_Pct = cv_Prap_Rule.newGroupArrayInGroup("cv_Prap_Rule_Cv_Maximum_Alloc_Pct", "CV-MAXIMUM-ALLOC-PCT", new DbsArrayController(1,
            100));
        cv_Prap_Rule_Cv_Max_Alloc_Pct = cv_Prap_Rule_Cv_Maximum_Alloc_Pct.newFieldInGroup("cv_Prap_Rule_Cv_Max_Alloc_Pct", "CV-MAX-ALLOC-PCT", FieldType.STRING, 
            3);
        cv_Prap_Rule_Cv_Max_Alloc_Pct_Sign = cv_Prap_Rule_Cv_Maximum_Alloc_Pct.newFieldInGroup("cv_Prap_Rule_Cv_Max_Alloc_Pct_Sign", "CV-MAX-ALLOC-PCT-SIGN", 
            FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Bene_Info_Type = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Bene_Info_Type", "CV-BENE-INFO-TYPE", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Primary_Std_Ent = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Primary_Std_Ent", "CV-PRIMARY-STD-ENT", FieldType.NUMERIC, 1);
        cv_Prap_Rule_Cv_Bene_Primary_Info = cv_Prap_Rule.newGroupInGroup("cv_Prap_Rule_Cv_Bene_Primary_Info", "CV-BENE-PRIMARY-INFO");
        cv_Prap_Rule_Cv_Primary_Bene_Info = cv_Prap_Rule_Cv_Bene_Primary_Info.newFieldArrayInGroup("cv_Prap_Rule_Cv_Primary_Bene_Info", "CV-PRIMARY-BENE-INFO", 
            FieldType.STRING, 72, new DbsArrayController(1,5));
        cv_Prap_Rule_Cv_Contingent_Std_Ent = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Contingent_Std_Ent", "CV-CONTINGENT-STD-ENT", FieldType.NUMERIC, 
            1);
        cv_Prap_Rule_Cv_Bene_Contingent_Info = cv_Prap_Rule.newGroupInGroup("cv_Prap_Rule_Cv_Bene_Contingent_Info", "CV-BENE-CONTINGENT-INFO");
        cv_Prap_Rule_Cv_Contingent_Bene_Info = cv_Prap_Rule_Cv_Bene_Contingent_Info.newFieldArrayInGroup("cv_Prap_Rule_Cv_Contingent_Bene_Info", "CV-CONTINGENT-BENE-INFO", 
            FieldType.STRING, 72, new DbsArrayController(1,5));
        cv_Prap_Rule_Cv_Bene_Estate = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Bene_Estate", "CV-BENE-ESTATE", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Bene_Trust = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Bene_Trust", "CV-BENE-TRUST", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Bene_Category = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Bene_Category", "CV-BENE-CATEGORY", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Mail_Instructions = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Mail_Instructions", "CV-MAIL-INSTRUCTIONS", FieldType.STRING, 
            1);
        cv_Prap_Rule_Cv_Eop_Addl_Cref_Request = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Eop_Addl_Cref_Request", "CV-EOP-ADDL-CREF-REQUEST", FieldType.STRING, 
            1);
        cv_Prap_Rule_Cv_Process_Status = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Process_Status", "CV-PROCESS-STATUS", FieldType.STRING, 4);
        cv_Prap_Rule_Cv_Coll_St_Cd = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Coll_St_Cd", "CV-COLL-ST-CD", FieldType.STRING, 2);
        cv_Prap_Rule_Cv_Csm_Sec_Seg = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Csm_Sec_Seg", "CV-CSM-SEC-SEG", FieldType.STRING, 4);
        cv_Prap_Rule_Cv_Rlc_College = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Rlc_College", "CV-RLC-COLLEGE", FieldType.STRING, 4);
        cv_Prap_Rule_Cv_Rlc_Cref_Pref = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Rlc_Cref_Pref", "CV-RLC-CREF-PREF", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Rlc_Cref_Cont = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Rlc_Cref_Cont", "CV-RLC-CREF-CONT", FieldType.PACKED_DECIMAL, 7);
        cv_Prap_Rule_Cv_Cor_Prfx_Nme = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Cor_Prfx_Nme", "CV-COR-PRFX-NME", FieldType.STRING, 8);
        cv_Prap_Rule_Cv_Cor_Last_Nme = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Cor_Last_Nme", "CV-COR-LAST-NME", FieldType.STRING, 30);
        cv_Prap_Rule_Cv_Cor_First_Nme = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Cor_First_Nme", "CV-COR-FIRST-NME", FieldType.STRING, 30);
        cv_Prap_Rule_Cv_Cor_Mddle_Nme = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Cor_Mddle_Nme", "CV-COR-MDDLE-NME", FieldType.STRING, 30);
        cv_Prap_Rule_Cv_Cor_Sffx_Nme = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Cor_Sffx_Nme", "CV-COR-SFFX-NME", FieldType.STRING, 8);
        cv_Prap_Rule_Cv_Ph_Hist_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Ph_Hist_Ind", "CV-PH-HIST-IND", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Rcrd_Updt_Tm_Stamp = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Rcrd_Updt_Tm_Stamp", "CV-RCRD-UPDT-TM-STAMP", FieldType.STRING, 
            8);
        cv_Prap_Rule_Cv_Contact_Mode = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Contact_Mode", "CV-CONTACT-MODE", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Racf_Id = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Racf_Id", "CV-RACF-ID", FieldType.STRING, 16);
        cv_Prap_Rule_Cv_Pin_Nbr = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Pin_Nbr", "CV-PIN-NBR", FieldType.NUMERIC, 12);
        cv_Prap_Rule_Cv_Rqst_Log_Dte_Tm = cv_Prap_Rule.newGroupInGroup("cv_Prap_Rule_Cv_Rqst_Log_Dte_Tm", "CV-RQST-LOG-DTE-TM");
        cv_Prap_Rule_Cv_Rqst_Log_Dte_Time = cv_Prap_Rule_Cv_Rqst_Log_Dte_Tm.newFieldArrayInGroup("cv_Prap_Rule_Cv_Rqst_Log_Dte_Time", "CV-RQST-LOG-DTE-TIME", 
            FieldType.STRING, 15, new DbsArrayController(1,5));
        cv_Prap_Rule_Cv_Sync_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Sync_Ind", "CV-SYNC-IND", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Cor_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Cor_Ind", "CV-COR-IND", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Alloc_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Alloc_Ind", "CV-ALLOC-IND", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Tiaa_Doi = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Tiaa_Doi", "CV-TIAA-DOI", FieldType.DATE);
        cv_Prap_Rule_Cv_Cref_Doi = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Cref_Doi", "CV-CREF-DOI", FieldType.DATE);
        cv_Prap_Rule_Cv_Mit_Request = cv_Prap_Rule.newGroupArrayInGroup("cv_Prap_Rule_Cv_Mit_Request", "CV-MIT-REQUEST", new DbsArrayController(1,5));
        cv_Prap_Rule_Cv_Mit_Unit = cv_Prap_Rule_Cv_Mit_Request.newFieldInGroup("cv_Prap_Rule_Cv_Mit_Unit", "CV-MIT-UNIT", FieldType.STRING, 8);
        cv_Prap_Rule_Cv_Mit_Wpid = cv_Prap_Rule_Cv_Mit_Request.newFieldInGroup("cv_Prap_Rule_Cv_Mit_Wpid", "CV-MIT-WPID", FieldType.STRING, 6);
        cv_Prap_Rule_Cv_Ira_Rollover_Type = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Ira_Rollover_Type", "CV-IRA-ROLLOVER-TYPE", FieldType.STRING, 
            1);
        cv_Prap_Rule_Cv_Ira_Record_Type = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Ira_Record_Type", "CV-IRA-RECORD-TYPE", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Mult_App_Status = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Mult_App_Status", "CV-MULT-APP-STATUS", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Mult_App_Lob = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Mult_App_Lob", "CV-MULT-APP-LOB", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Mult_App_Lob_Type = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Mult_App_Lob_Type", "CV-MULT-APP-LOB-TYPE", FieldType.STRING, 
            1);
        cv_Prap_Rule_Cv_Mult_App_Ppg = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Mult_App_Ppg", "CV-MULT-APP-PPG", FieldType.STRING, 6);
        cv_Prap_Rule_Cv_Print_Date = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Print_Date", "CV-PRINT-DATE", FieldType.DATE);
        cv_Prap_Rule_Cv_Cntrct_Info = cv_Prap_Rule.newGroupArrayInGroup("cv_Prap_Rule_Cv_Cntrct_Info", "CV-CNTRCT-INFO", new DbsArrayController(1,15));
        cv_Prap_Rule_Cv_Cntrct_Type = cv_Prap_Rule_Cv_Cntrct_Info.newFieldInGroup("cv_Prap_Rule_Cv_Cntrct_Type", "CV-CNTRCT-TYPE", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Cntrct_Nbr = cv_Prap_Rule_Cv_Cntrct_Info.newFieldInGroup("cv_Prap_Rule_Cv_Cntrct_Nbr", "CV-CNTRCT-NBR", FieldType.STRING, 10);
        cv_Prap_Rule_Cv_Cntrct_Proceeds_Amt = cv_Prap_Rule_Cv_Cntrct_Info.newFieldInGroup("cv_Prap_Rule_Cv_Cntrct_Proceeds_Amt", "CV-CNTRCT-PROCEEDS-AMT", 
            FieldType.DECIMAL, 10,2);
        cv_Prap_Rule_Cv_Addr_Info = cv_Prap_Rule.newGroupInGroup("cv_Prap_Rule_Cv_Addr_Info", "CV-ADDR-INFO");
        cv_Prap_Rule_Cv_Address_Txt = cv_Prap_Rule_Cv_Addr_Info.newFieldArrayInGroup("cv_Prap_Rule_Cv_Address_Txt", "CV-ADDRESS-TXT", FieldType.STRING, 
            35, new DbsArrayController(1,5));
        cv_Prap_Rule_Cv_Address_Dest_Name = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Address_Dest_Name", "CV-ADDRESS-DEST-NAME", FieldType.STRING, 
            35);
        cv_Prap_Rule_Cv_Zip_Code = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Zip_Code", "CV-ZIP-CODE", FieldType.STRING, 9);
        cv_Prap_Rule_Cv_Bank_Pymnt_Acct_Nmbr = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Bank_Pymnt_Acct_Nmbr", "CV-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 
            21);
        cv_Prap_Rule_Cv_Bank_Aba_Acct_Nmbr = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Bank_Aba_Acct_Nmbr", "CV-BANK-ABA-ACCT-NMBR", FieldType.STRING, 
            9);
        cv_Prap_Rule_Cv_Addr_Usage_Code = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Addr_Usage_Code", "CV-ADDR-USAGE-CODE", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Init_Paymt_Amt = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Init_Paymt_Amt", "CV-INIT-PAYMT-AMT", FieldType.DECIMAL, 10,2);
        cv_Prap_Rule_Cv_Init_Paymt_Pct = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Init_Paymt_Pct", "CV-INIT-PAYMT-PCT", FieldType.NUMERIC, 2);
        cv_Prap_Rule_Cv_Financial_1 = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Financial_1", "CV-FINANCIAL-1", FieldType.DECIMAL, 10,2);
        cv_Prap_Rule_Cv_Financial_2 = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Financial_2", "CV-FINANCIAL-2", FieldType.DECIMAL, 10,2);
        cv_Prap_Rule_Cv_Financial_3 = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Financial_3", "CV-FINANCIAL-3", FieldType.DECIMAL, 10,2);
        cv_Prap_Rule_Cv_Financial_4 = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Financial_4", "CV-FINANCIAL-4", FieldType.DECIMAL, 10,2);
        cv_Prap_Rule_Cv_Financial_5 = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Financial_5", "CV-FINANCIAL-5", FieldType.DECIMAL, 10,2);
        cv_Prap_Rule_Cv_Irc_Sectn_Grp_Cde = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Irc_Sectn_Grp_Cde", "CV-IRC-SECTN-GRP-CDE", FieldType.NUMERIC, 
            2);
        cv_Prap_Rule_Cv_Irc_Sectn_Cde = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Irc_Sectn_Cde", "CV-IRC-SECTN-CDE", FieldType.NUMERIC, 2);
        cv_Prap_Rule_Cv_Inst_Link_Cde = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Inst_Link_Cde", "CV-INST-LINK-CDE", FieldType.NUMERIC, 6);
        cv_Prap_Rule_Cv_Applcnt_Req_Type = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Applcnt_Req_Type", "CV-APPLCNT-REQ-TYPE", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Addr_Sync_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Addr_Sync_Ind", "CV-ADDR-SYNC-IND", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Tiaa_Service_Agent = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Tiaa_Service_Agent", "CV-TIAA-SERVICE-AGENT", FieldType.STRING, 
            1);
        cv_Prap_Rule_Cv_Prap_Rsch_Mit_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Prap_Rsch_Mit_Ind", "CV-PRAP-RSCH-MIT-IND", FieldType.STRING, 
            1);
        cv_Prap_Rule_Cv_Ppg_Team_Cde = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Ppg_Team_Cde", "CV-PPG-TEAM-CDE", FieldType.STRING, 8);
        cv_Prap_Rule_Cv_Tiaa_Cntrct = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Tiaa_Cntrct", "CV-TIAA-CNTRCT", FieldType.STRING, 10);
        cv_Prap_Rule_Cv_Cref_Cert = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Cref_Cert", "CV-CREF-CERT", FieldType.STRING, 10);
        cv_Prap_Rule_Cv_Rlc_Cref_Cert = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Rlc_Cref_Cert", "CV-RLC-CREF-CERT", FieldType.STRING, 10);
        cv_Prap_Rule_Cv_Allocation_Model_Type = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Allocation_Model_Type", "CV-ALLOCATION-MODEL-TYPE", FieldType.STRING, 
            2);
        cv_Prap_Rule_Cv_Divorce_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Divorce_Ind", "CV-DIVORCE-IND", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Email_Address = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Email_Address", "CV-EMAIL-ADDRESS", FieldType.STRING, 50);
        cv_Prap_Rule_Cv_E_Signed_Appl_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_E_Signed_Appl_Ind", "CV-E-SIGNED-APPL-IND", FieldType.STRING, 
            1);
        cv_Prap_Rule_Cv_Eft_Requested_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Eft_Requested_Ind", "CV-EFT-REQUESTED-IND", FieldType.STRING, 
            1);
        cv_Prap_Rule_Cv_Prap_Prem_Rsch_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Prap_Prem_Rsch_Ind", "CV-PRAP-PREM-RSCH-IND", FieldType.STRING, 
            1);
        cv_Prap_Rule_Cv_Address_Change_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Address_Change_Ind", "CV-ADDRESS-CHANGE-IND", FieldType.STRING, 
            1);
        cv_Prap_Rule_Cv_Allocation_Fmt = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Allocation_Fmt", "CV-ALLOCATION-FMT", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Phone_No = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Phone_No", "CV-PHONE-NO", FieldType.STRING, 20);
        cv_Prap_Rule_Cv_Fund_Identifier = cv_Prap_Rule.newGroupArrayInGroup("cv_Prap_Rule_Cv_Fund_Identifier", "CV-FUND-IDENTIFIER", new DbsArrayController(1,
            100));
        cv_Prap_Rule_Cv_Fund_Cde = cv_Prap_Rule_Cv_Fund_Identifier.newFieldInGroup("cv_Prap_Rule_Cv_Fund_Cde", "CV-FUND-CDE", FieldType.STRING, 10);
        cv_Prap_Rule_Cv_Allocation_Pct = cv_Prap_Rule_Cv_Fund_Identifier.newFieldInGroup("cv_Prap_Rule_Cv_Allocation_Pct", "CV-ALLOCATION-PCT", FieldType.NUMERIC, 
            3);
        cv_Prap_Rule_Cv_Sgrd_Plan_No = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Sgrd_Plan_No", "CV-SGRD-PLAN-NO", FieldType.STRING, 6);
        cv_Prap_Rule_Cv_Sgrd_Subplan_No = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Sgrd_Subplan_No", "CV-SGRD-SUBPLAN-NO", FieldType.STRING, 6);
        cv_Prap_Rule_Cv_Sgrd_Part_Ext = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Sgrd_Part_Ext", "CV-SGRD-PART-EXT", FieldType.STRING, 2);
        cv_Prap_Rule_Cv_Sgrd_Divsub = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Sgrd_Divsub", "CV-SGRD-DIVSUB", FieldType.STRING, 4);
        cv_Prap_Rule_Cv_Text_Udf_1 = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Text_Udf_1", "CV-TEXT-UDF-1", FieldType.STRING, 10);
        cv_Prap_Rule_Cv_Text_Udf_2 = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Text_Udf_2", "CV-TEXT-UDF-2", FieldType.STRING, 10);
        cv_Prap_Rule_Cv_Text_Udf_3 = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Text_Udf_3", "CV-TEXT-UDF-3", FieldType.STRING, 10);
        cv_Prap_Rule_Cv_Replacement_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Replacement_Ind", "CV-REPLACEMENT-IND", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Register_Id = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Register_Id", "CV-REGISTER-ID", FieldType.STRING, 11);
        cv_Prap_Rule_Cv_Agent_Or_Racf_Id = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Agent_Or_Racf_Id", "CV-AGENT-OR-RACF-ID", FieldType.STRING, 15);
        cv_Prap_Rule_Cv_Exempt_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Exempt_Ind", "CV-EXEMPT-IND", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Arr_Insurer_1 = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Arr_Insurer_1", "CV-ARR-INSURER-1", FieldType.STRING, 30);
        cv_Prap_Rule_Cv_Arr_Insurer_2 = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Arr_Insurer_2", "CV-ARR-INSURER-2", FieldType.STRING, 30);
        cv_Prap_Rule_Cv_Arr_Insurer_3 = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Arr_Insurer_3", "CV-ARR-INSURER-3", FieldType.STRING, 30);
        cv_Prap_Rule_Cv_Arr_1035_Exch_Ind_1 = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Arr_1035_Exch_Ind_1", "CV-ARR-1035-EXCH-IND-1", FieldType.STRING, 
            1);
        cv_Prap_Rule_Cv_Arr_1035_Exch_Ind_2 = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Arr_1035_Exch_Ind_2", "CV-ARR-1035-EXCH-IND-2", FieldType.STRING, 
            1);
        cv_Prap_Rule_Cv_Arr_1035_Exch_Ind_3 = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Arr_1035_Exch_Ind_3", "CV-ARR-1035-EXCH-IND-3", FieldType.STRING, 
            1);
        cv_Prap_Rule_Cv_Autosave_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Autosave_Ind", "CV-AUTOSAVE-IND", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_As_Cur_Dflt_Opt = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_As_Cur_Dflt_Opt", "CV-AS-CUR-DFLT-OPT", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_As_Cur_Dflt_Amt = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_As_Cur_Dflt_Amt", "CV-AS-CUR-DFLT-AMT", FieldType.DECIMAL, 9,2);
        cv_Prap_Rule_Cv_As_Incr_Opt = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_As_Incr_Opt", "CV-AS-INCR-OPT", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_As_Incr_Amt = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_As_Incr_Amt", "CV-AS-INCR-AMT", FieldType.DECIMAL, 9,2);
        cv_Prap_Rule_Cv_As_Max_Pct = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_As_Max_Pct", "CV-AS-MAX-PCT", FieldType.DECIMAL, 7,2);
        cv_Prap_Rule_Cv_Ae_Opt_Out_Days = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Ae_Opt_Out_Days", "CV-AE-OPT-OUT-DAYS", FieldType.NUMERIC, 2);
        cv_Prap_Rule_Cv_Incmpl_Acct_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Incmpl_Acct_Ind", "CV-INCMPL-ACCT-IND", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Delete_User_Id = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Delete_User_Id", "CV-DELETE-USER-ID", FieldType.STRING, 16);
        cv_Prap_Rule_Cv_Delete_Reason_Cd = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Delete_Reason_Cd", "CV-DELETE-REASON-CD", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Consent_Email_Address = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Consent_Email_Address", "CV-CONSENT-EMAIL-ADDRESS", FieldType.STRING, 
            50);
        cv_Prap_Rule_Cv_Welc_E_Delivery_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Welc_E_Delivery_Ind", "CV-WELC-E-DELIVERY-IND", FieldType.STRING, 
            1);
        cv_Prap_Rule_Cv_Legal_E_Delivery_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Legal_E_Delivery_Ind", "CV-LEGAL-E-DELIVERY-IND", FieldType.STRING, 
            1);
        cv_Prap_Rule_Cv_Legal_Ann_Option = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Legal_Ann_Option", "CV-LEGAL-ANN-OPTION", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Orchestration_Id = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Orchestration_Id", "CV-ORCHESTRATION-ID", FieldType.STRING, 15);
        cv_Prap_Rule_Cv_Mail_Addr_Country_Cd = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Mail_Addr_Country_Cd", "CV-MAIL-ADDR-COUNTRY-CD", FieldType.STRING, 
            3);
        cv_Prap_Rule_Cv_Tic_Startdate = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Tic_Startdate", "CV-TIC-STARTDATE", FieldType.NUMERIC, 8);
        cv_Prap_Rule_Cv_Tic_Enddate = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Tic_Enddate", "CV-TIC-ENDDATE", FieldType.NUMERIC, 8);
        cv_Prap_Rule_Cv_Tic_Percentage = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Tic_Percentage", "CV-TIC-PERCENTAGE", FieldType.DECIMAL, 15,6);
        cv_Prap_Rule_Cv_Tic_Postdays = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Tic_Postdays", "CV-TIC-POSTDAYS", FieldType.NUMERIC, 2);
        cv_Prap_Rule_Cv_Tic_Limit = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Tic_Limit", "CV-TIC-LIMIT", FieldType.DECIMAL, 13,2);
        cv_Prap_Rule_Cv_Tic_Postfreq = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Tic_Postfreq", "CV-TIC-POSTFREQ", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Tic_Pl_Level = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Tic_Pl_Level", "CV-TIC-PL-LEVEL", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Tic_Windowdays = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Tic_Windowdays", "CV-TIC-WINDOWDAYS", FieldType.NUMERIC, 3);
        cv_Prap_Rule_Cv_Tic_Reqdlywindow = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Tic_Reqdlywindow", "CV-TIC-REQDLYWINDOW", FieldType.NUMERIC, 
            3);
        cv_Prap_Rule_Cv_Tic_Recap_Prov = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Tic_Recap_Prov", "CV-TIC-RECAP-PROV", FieldType.NUMERIC, 3);
        cv_Prap_Rule_Cv_Ann_Funding_Dt = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Ann_Funding_Dt", "CV-ANN-FUNDING-DT", FieldType.NUMERIC, 8);
        cv_Prap_Rule_Cv_Tiaa_Ann_Issue_Dt = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Tiaa_Ann_Issue_Dt", "CV-TIAA-ANN-ISSUE-DT", FieldType.NUMERIC, 
            8);
        cv_Prap_Rule_Cv_Cref_Ann_Issue_Dt = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Cref_Ann_Issue_Dt", "CV-CREF-ANN-ISSUE-DT", FieldType.NUMERIC, 
            8);
        cv_Prap_Rule_Cv_Substitution_Contract_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Substitution_Contract_Ind", "CV-SUBSTITUTION-CONTRACT-IND", 
            FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Conv_Issue_State = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Conv_Issue_State", "CV-CONV-ISSUE-STATE", FieldType.STRING, 2);
        cv_Prap_Rule_Cv_Deceased_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Deceased_Ind", "CV-DECEASED-IND", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Non_Proprietary_Pkg_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Non_Proprietary_Pkg_Ind", "CV-NON-PROPRIETARY-PKG-IND", 
            FieldType.STRING, 5);
        cv_Prap_Rule_Cv_Ap_Decedent_Contract = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Ap_Decedent_Contract", "CV-AP-DECEDENT-CONTRACT", FieldType.STRING, 
            10);
        cv_Prap_Rule_Cv_Relation_To_Decedent = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Relation_To_Decedent", "CV-RELATION-TO-DECEDENT", FieldType.STRING, 
            1);
        cv_Prap_Rule_Cv_Ssn_Tin_Ind = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Ssn_Tin_Ind", "CV-SSN-TIN-IND", FieldType.STRING, 1);
        cv_Prap_Rule_Cv_Oneira_Acct_No = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Oneira_Acct_No", "CV-ONEIRA-ACCT-NO", FieldType.STRING, 10);
        cv_Prap_Rule_Cv_Fund_Source_Cde_1 = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Fund_Source_Cde_1", "CV-FUND-SOURCE-CDE-1", FieldType.STRING, 
            1);
        cv_Prap_Rule_Cv_Fund_Source_Cde_2 = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Fund_Source_Cde_2", "CV-FUND-SOURCE-CDE-2", FieldType.STRING, 
            1);
        cv_Prap_Rule_Cv_Fund_Identifier_2 = cv_Prap_Rule.newGroupArrayInGroup("cv_Prap_Rule_Cv_Fund_Identifier_2", "CV-FUND-IDENTIFIER-2", new DbsArrayController(1,
            100));
        cv_Prap_Rule_Cv_Fund_Cde_2 = cv_Prap_Rule_Cv_Fund_Identifier_2.newFieldInGroup("cv_Prap_Rule_Cv_Fund_Cde_2", "CV-FUND-CDE-2", FieldType.STRING, 
            10);
        cv_Prap_Rule_Cv_Allocation_Pct_2 = cv_Prap_Rule_Cv_Fund_Identifier_2.newFieldInGroup("cv_Prap_Rule_Cv_Allocation_Pct_2", "CV-ALLOCATION-PCT-2", 
            FieldType.NUMERIC, 3);
        cv_Prap_Rule_Cv_Filler = cv_Prap_Rule.newFieldInGroup("cv_Prap_Rule_Cv_Filler", "CV-FILLER", FieldType.STRING, 84);
        cv_Prap_RuleRedef1 = newGroupInRecord("cv_Prap_RuleRedef1", "Redefines", cv_Prap_Rule);
        cv_Prap_Rule_Cv_Rule_Data_1 = cv_Prap_RuleRedef1.newFieldInGroup("cv_Prap_Rule_Cv_Rule_Data_1", "CV-RULE-DATA-1", FieldType.STRING, 250);
        cv_Prap_Rule_Cv_Rule_Data_2 = cv_Prap_RuleRedef1.newFieldInGroup("cv_Prap_Rule_Cv_Rule_Data_2", "CV-RULE-DATA-2", FieldType.STRING, 250);
        cv_Prap_Rule_Cv_Rule_Data_3 = cv_Prap_RuleRedef1.newFieldInGroup("cv_Prap_Rule_Cv_Rule_Data_3", "CV-RULE-DATA-3", FieldType.STRING, 250);
        cv_Prap_Rule_Cv_Rule_Data_4 = cv_Prap_RuleRedef1.newFieldInGroup("cv_Prap_Rule_Cv_Rule_Data_4", "CV-RULE-DATA-4", FieldType.STRING, 250);
        cv_Prap_Rule_Cv_Rule_Data_5 = cv_Prap_RuleRedef1.newFieldInGroup("cv_Prap_Rule_Cv_Rule_Data_5", "CV-RULE-DATA-5", FieldType.STRING, 250);
        cv_Prap_Rule_Cv_Rule_Data_6 = cv_Prap_RuleRedef1.newFieldInGroup("cv_Prap_Rule_Cv_Rule_Data_6", "CV-RULE-DATA-6", FieldType.STRING, 250);
        cv_Prap_Rule_Cv_Rule_Data_7 = cv_Prap_RuleRedef1.newFieldInGroup("cv_Prap_Rule_Cv_Rule_Data_7", "CV-RULE-DATA-7", FieldType.STRING, 250);
        cv_Prap_Rule_Cv_Rule_Data_8 = cv_Prap_RuleRedef1.newFieldInGroup("cv_Prap_Rule_Cv_Rule_Data_8", "CV-RULE-DATA-8", FieldType.STRING, 250);
        cv_Prap_Rule_Cv_Rule_Data_9 = cv_Prap_RuleRedef1.newFieldInGroup("cv_Prap_Rule_Cv_Rule_Data_9", "CV-RULE-DATA-9", FieldType.STRING, 250);
        cv_Prap_Rule_Cv_Rule_Data_10 = cv_Prap_RuleRedef1.newFieldInGroup("cv_Prap_Rule_Cv_Rule_Data_10", "CV-RULE-DATA-10", FieldType.STRING, 250);
        cv_Prap_Rule_Cv_Rule_Data_11 = cv_Prap_RuleRedef1.newFieldInGroup("cv_Prap_Rule_Cv_Rule_Data_11", "CV-RULE-DATA-11", FieldType.STRING, 250);
        cv_Prap_Rule_Cv_Rule_Data_12 = cv_Prap_RuleRedef1.newFieldInGroup("cv_Prap_Rule_Cv_Rule_Data_12", "CV-RULE-DATA-12", FieldType.STRING, 250);
        cv_Prap_Rule_Cv_Rule_Data_13 = cv_Prap_RuleRedef1.newFieldInGroup("cv_Prap_Rule_Cv_Rule_Data_13", "CV-RULE-DATA-13", FieldType.STRING, 250);
        cv_Prap_Rule_Cv_Rule_Data_14 = cv_Prap_RuleRedef1.newFieldInGroup("cv_Prap_Rule_Cv_Rule_Data_14", "CV-RULE-DATA-14", FieldType.STRING, 250);
        cv_Prap_Rule_Cv_Rule_Data_15 = cv_Prap_RuleRedef1.newFieldInGroup("cv_Prap_Rule_Cv_Rule_Data_15", "CV-RULE-DATA-15", FieldType.STRING, 250);
        cv_Prap_Rule_Cv_Rule_Data_16 = cv_Prap_RuleRedef1.newFieldInGroup("cv_Prap_Rule_Cv_Rule_Data_16", "CV-RULE-DATA-16", FieldType.STRING, 250);
        cv_Prap_Rule_Cv_Rule_Data_17 = cv_Prap_RuleRedef1.newFieldInGroup("cv_Prap_Rule_Cv_Rule_Data_17", "CV-RULE-DATA-17", FieldType.STRING, 250);
        cv_Prap_Rule_Cv_Rule_Data_18 = cv_Prap_RuleRedef1.newFieldInGroup("cv_Prap_Rule_Cv_Rule_Data_18", "CV-RULE-DATA-18", FieldType.STRING, 250);
        cv_Prap_Rule_Cv_Rule_Data_19 = cv_Prap_RuleRedef1.newFieldInGroup("cv_Prap_Rule_Cv_Rule_Data_19", "CV-RULE-DATA-19", FieldType.STRING, 1200);

        this.setRecordName("LdaAppl300");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAppl300() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
