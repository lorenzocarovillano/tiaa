/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:23:21 PM
**        * FROM NATURAL PDA     : SCIA8250
************************************************************
**        * FILE NAME            : PdaScia8250.java
**        * CLASS NAME           : PdaScia8250
**        * INSTANCE NAME        : PdaScia8250
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaScia8250 extends PdaBase
{
    // Properties
    private DbsField ppt_File_Open_Sw;
    private DbsGroup ppt_Part_Fund_Rec;
    private DbsField ppt_Part_Fund_Rec_Ppt_Request_Type;
    private DbsField ppt_Part_Fund_Rec_Ppt_Sent_Data;
    private DbsGroup ppt_Part_Fund_Rec_Ppt_Sent_DataRedef1;
    private DbsField ppt_Part_Fund_Rec_Ppt_Eof_Sw;
    private DbsField ppt_Part_Fund_Rec_Ppt_Plan_Num;
    private DbsField ppt_Part_Fund_Rec_Ppt_Part_Id;
    private DbsGroup ppt_Part_Fund_Rec_Ppt_Part_IdRedef2;
    private DbsField ppt_Part_Fund_Rec_Ppt_Part_Number;
    private DbsField ppt_Part_Fund_Rec_Ppt_Part_Sub_Plan;
    private DbsField ppt_Part_Fund_Rec_Ppt_Returned_Data;
    private DbsGroup ppt_Part_Fund_Rec_Ppt_Returned_DataRedef3;
    private DbsField ppt_Part_Fund_Rec_Ppt_First_Contrib_Date;
    private DbsField ppt_Part_Fund_Rec_Ppt_Last_Contrib_Date;
    private DbsField ppt_Part_Fund_Rec_Ppt_Status;
    private DbsField ppt_Part_Fund_Rec_Ppt_Num_Funds;
    private DbsField ppt_Part_Fund_Rec_Ppt_Return_Code;
    private DbsField ppt_Part_Fund_Rec_Ppt_Return_Msg_Parms;
    private DbsGroup ppt_Part_Fund_Rec_Ppt_Return_Msg_ParmsRedef4;
    private DbsField ppt_Part_Fund_Rec_Ppt_Return_Msg_Module;
    private DbsField ppt_Part_Fund_Rec_Ppt_Return_Error_Sw;
    private DbsField ppt_Part_Fund_Rec_Ppt_Return_File_Oper;
    private DbsField ppt_Part_Fund_Rec_Ppt_Return_Message;
    private DbsField ppt_Part_Fund_Rec_Ppt_Filler;

    public DbsField getPpt_File_Open_Sw() { return ppt_File_Open_Sw; }

    public DbsGroup getPpt_Part_Fund_Rec() { return ppt_Part_Fund_Rec; }

    public DbsField getPpt_Part_Fund_Rec_Ppt_Request_Type() { return ppt_Part_Fund_Rec_Ppt_Request_Type; }

    public DbsField getPpt_Part_Fund_Rec_Ppt_Sent_Data() { return ppt_Part_Fund_Rec_Ppt_Sent_Data; }

    public DbsGroup getPpt_Part_Fund_Rec_Ppt_Sent_DataRedef1() { return ppt_Part_Fund_Rec_Ppt_Sent_DataRedef1; }

    public DbsField getPpt_Part_Fund_Rec_Ppt_Eof_Sw() { return ppt_Part_Fund_Rec_Ppt_Eof_Sw; }

    public DbsField getPpt_Part_Fund_Rec_Ppt_Plan_Num() { return ppt_Part_Fund_Rec_Ppt_Plan_Num; }

    public DbsField getPpt_Part_Fund_Rec_Ppt_Part_Id() { return ppt_Part_Fund_Rec_Ppt_Part_Id; }

    public DbsGroup getPpt_Part_Fund_Rec_Ppt_Part_IdRedef2() { return ppt_Part_Fund_Rec_Ppt_Part_IdRedef2; }

    public DbsField getPpt_Part_Fund_Rec_Ppt_Part_Number() { return ppt_Part_Fund_Rec_Ppt_Part_Number; }

    public DbsField getPpt_Part_Fund_Rec_Ppt_Part_Sub_Plan() { return ppt_Part_Fund_Rec_Ppt_Part_Sub_Plan; }

    public DbsField getPpt_Part_Fund_Rec_Ppt_Returned_Data() { return ppt_Part_Fund_Rec_Ppt_Returned_Data; }

    public DbsGroup getPpt_Part_Fund_Rec_Ppt_Returned_DataRedef3() { return ppt_Part_Fund_Rec_Ppt_Returned_DataRedef3; }

    public DbsField getPpt_Part_Fund_Rec_Ppt_First_Contrib_Date() { return ppt_Part_Fund_Rec_Ppt_First_Contrib_Date; }

    public DbsField getPpt_Part_Fund_Rec_Ppt_Last_Contrib_Date() { return ppt_Part_Fund_Rec_Ppt_Last_Contrib_Date; }

    public DbsField getPpt_Part_Fund_Rec_Ppt_Status() { return ppt_Part_Fund_Rec_Ppt_Status; }

    public DbsField getPpt_Part_Fund_Rec_Ppt_Num_Funds() { return ppt_Part_Fund_Rec_Ppt_Num_Funds; }

    public DbsField getPpt_Part_Fund_Rec_Ppt_Return_Code() { return ppt_Part_Fund_Rec_Ppt_Return_Code; }

    public DbsField getPpt_Part_Fund_Rec_Ppt_Return_Msg_Parms() { return ppt_Part_Fund_Rec_Ppt_Return_Msg_Parms; }

    public DbsGroup getPpt_Part_Fund_Rec_Ppt_Return_Msg_ParmsRedef4() { return ppt_Part_Fund_Rec_Ppt_Return_Msg_ParmsRedef4; }

    public DbsField getPpt_Part_Fund_Rec_Ppt_Return_Msg_Module() { return ppt_Part_Fund_Rec_Ppt_Return_Msg_Module; }

    public DbsField getPpt_Part_Fund_Rec_Ppt_Return_Error_Sw() { return ppt_Part_Fund_Rec_Ppt_Return_Error_Sw; }

    public DbsField getPpt_Part_Fund_Rec_Ppt_Return_File_Oper() { return ppt_Part_Fund_Rec_Ppt_Return_File_Oper; }

    public DbsField getPpt_Part_Fund_Rec_Ppt_Return_Message() { return ppt_Part_Fund_Rec_Ppt_Return_Message; }

    public DbsField getPpt_Part_Fund_Rec_Ppt_Filler() { return ppt_Part_Fund_Rec_Ppt_Filler; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        ppt_File_Open_Sw = dbsRecord.newFieldInRecord("ppt_File_Open_Sw", "PPT-FILE-OPEN-SW", FieldType.STRING, 1);
        ppt_File_Open_Sw.setParameterOption(ParameterOption.ByReference);

        ppt_Part_Fund_Rec = dbsRecord.newGroupInRecord("ppt_Part_Fund_Rec", "PPT-PART-FUND-REC");
        ppt_Part_Fund_Rec.setParameterOption(ParameterOption.ByReference);
        ppt_Part_Fund_Rec_Ppt_Request_Type = ppt_Part_Fund_Rec.newFieldInGroup("ppt_Part_Fund_Rec_Ppt_Request_Type", "PPT-REQUEST-TYPE", FieldType.STRING, 
            1);
        ppt_Part_Fund_Rec_Ppt_Sent_Data = ppt_Part_Fund_Rec.newFieldInGroup("ppt_Part_Fund_Rec_Ppt_Sent_Data", "PPT-SENT-DATA", FieldType.STRING, 22);
        ppt_Part_Fund_Rec_Ppt_Sent_DataRedef1 = ppt_Part_Fund_Rec.newGroupInGroup("ppt_Part_Fund_Rec_Ppt_Sent_DataRedef1", "Redefines", ppt_Part_Fund_Rec_Ppt_Sent_Data);
        ppt_Part_Fund_Rec_Ppt_Eof_Sw = ppt_Part_Fund_Rec_Ppt_Sent_DataRedef1.newFieldInGroup("ppt_Part_Fund_Rec_Ppt_Eof_Sw", "PPT-EOF-SW", FieldType.STRING, 
            1);
        ppt_Part_Fund_Rec_Ppt_Plan_Num = ppt_Part_Fund_Rec_Ppt_Sent_DataRedef1.newFieldInGroup("ppt_Part_Fund_Rec_Ppt_Plan_Num", "PPT-PLAN-NUM", FieldType.STRING, 
            6);
        ppt_Part_Fund_Rec_Ppt_Part_Id = ppt_Part_Fund_Rec_Ppt_Sent_DataRedef1.newFieldInGroup("ppt_Part_Fund_Rec_Ppt_Part_Id", "PPT-PART-ID", FieldType.STRING, 
            15);
        ppt_Part_Fund_Rec_Ppt_Part_IdRedef2 = ppt_Part_Fund_Rec_Ppt_Sent_DataRedef1.newGroupInGroup("ppt_Part_Fund_Rec_Ppt_Part_IdRedef2", "Redefines", 
            ppt_Part_Fund_Rec_Ppt_Part_Id);
        ppt_Part_Fund_Rec_Ppt_Part_Number = ppt_Part_Fund_Rec_Ppt_Part_IdRedef2.newFieldInGroup("ppt_Part_Fund_Rec_Ppt_Part_Number", "PPT-PART-NUMBER", 
            FieldType.STRING, 9);
        ppt_Part_Fund_Rec_Ppt_Part_Sub_Plan = ppt_Part_Fund_Rec_Ppt_Part_IdRedef2.newFieldInGroup("ppt_Part_Fund_Rec_Ppt_Part_Sub_Plan", "PPT-PART-SUB-PLAN", 
            FieldType.STRING, 6);
        ppt_Part_Fund_Rec_Ppt_Returned_Data = ppt_Part_Fund_Rec.newFieldInGroup("ppt_Part_Fund_Rec_Ppt_Returned_Data", "PPT-RETURNED-DATA", FieldType.STRING, 
            25);
        ppt_Part_Fund_Rec_Ppt_Returned_DataRedef3 = ppt_Part_Fund_Rec.newGroupInGroup("ppt_Part_Fund_Rec_Ppt_Returned_DataRedef3", "Redefines", ppt_Part_Fund_Rec_Ppt_Returned_Data);
        ppt_Part_Fund_Rec_Ppt_First_Contrib_Date = ppt_Part_Fund_Rec_Ppt_Returned_DataRedef3.newFieldInGroup("ppt_Part_Fund_Rec_Ppt_First_Contrib_Date", 
            "PPT-FIRST-CONTRIB-DATE", FieldType.NUMERIC, 8);
        ppt_Part_Fund_Rec_Ppt_Last_Contrib_Date = ppt_Part_Fund_Rec_Ppt_Returned_DataRedef3.newFieldInGroup("ppt_Part_Fund_Rec_Ppt_Last_Contrib_Date", 
            "PPT-LAST-CONTRIB-DATE", FieldType.NUMERIC, 8);
        ppt_Part_Fund_Rec_Ppt_Status = ppt_Part_Fund_Rec_Ppt_Returned_DataRedef3.newFieldInGroup("ppt_Part_Fund_Rec_Ppt_Status", "PPT-STATUS", FieldType.NUMERIC, 
            2);
        ppt_Part_Fund_Rec_Ppt_Num_Funds = ppt_Part_Fund_Rec_Ppt_Returned_DataRedef3.newFieldInGroup("ppt_Part_Fund_Rec_Ppt_Num_Funds", "PPT-NUM-FUNDS", 
            FieldType.NUMERIC, 5);
        ppt_Part_Fund_Rec_Ppt_Return_Code = ppt_Part_Fund_Rec_Ppt_Returned_DataRedef3.newFieldInGroup("ppt_Part_Fund_Rec_Ppt_Return_Code", "PPT-RETURN-CODE", 
            FieldType.NUMERIC, 2);
        ppt_Part_Fund_Rec_Ppt_Return_Msg_Parms = ppt_Part_Fund_Rec.newFieldInGroup("ppt_Part_Fund_Rec_Ppt_Return_Msg_Parms", "PPT-RETURN-MSG-PARMS", FieldType.STRING, 
            40);
        ppt_Part_Fund_Rec_Ppt_Return_Msg_ParmsRedef4 = ppt_Part_Fund_Rec.newGroupInGroup("ppt_Part_Fund_Rec_Ppt_Return_Msg_ParmsRedef4", "Redefines", 
            ppt_Part_Fund_Rec_Ppt_Return_Msg_Parms);
        ppt_Part_Fund_Rec_Ppt_Return_Msg_Module = ppt_Part_Fund_Rec_Ppt_Return_Msg_ParmsRedef4.newFieldInGroup("ppt_Part_Fund_Rec_Ppt_Return_Msg_Module", 
            "PPT-RETURN-MSG-MODULE", FieldType.STRING, 8);
        ppt_Part_Fund_Rec_Ppt_Return_Error_Sw = ppt_Part_Fund_Rec_Ppt_Return_Msg_ParmsRedef4.newFieldInGroup("ppt_Part_Fund_Rec_Ppt_Return_Error_Sw", 
            "PPT-RETURN-ERROR-SW", FieldType.STRING, 1);
        ppt_Part_Fund_Rec_Ppt_Return_File_Oper = ppt_Part_Fund_Rec_Ppt_Return_Msg_ParmsRedef4.newFieldInGroup("ppt_Part_Fund_Rec_Ppt_Return_File_Oper", 
            "PPT-RETURN-FILE-OPER", FieldType.STRING, 4);
        ppt_Part_Fund_Rec_Ppt_Return_Message = ppt_Part_Fund_Rec_Ppt_Return_Msg_ParmsRedef4.newFieldInGroup("ppt_Part_Fund_Rec_Ppt_Return_Message", "PPT-RETURN-MESSAGE", 
            FieldType.STRING, 27);
        ppt_Part_Fund_Rec_Ppt_Filler = ppt_Part_Fund_Rec.newFieldArrayInGroup("ppt_Part_Fund_Rec_Ppt_Filler", "PPT-FILLER", FieldType.STRING, 1, new DbsArrayController(1,
            2512));

        dbsRecord.reset();
    }

    // Constructors
    public PdaScia8250(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

