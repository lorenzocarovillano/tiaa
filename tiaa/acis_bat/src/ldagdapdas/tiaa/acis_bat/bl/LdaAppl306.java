/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:51:14 PM
**        * FROM NATURAL LDA     : APPL306
************************************************************
**        * FILE NAME            : LdaAppl306.java
**        * CLASS NAME           : LdaAppl306
**        * INSTANCE NAME        : LdaAppl306
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl306 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_request_R;
    private DbsField request_R_Ppg_Code;
    private DbsField request_R_Request_Team;
    private DbsField request_R_Requested_Letter_Type;
    private DbsField request_R_Pullout_Code;
    private DbsField request_R_App_Prem_Rqst;
    private DbsField request_R_Iis_Team_Code;
    private DbsField request_R_Instn_Link_Cde;
    private DbsField request_R_System_Link_Cde;
    private DbsField request_R_Public_Link_Cde;
    private DbsField request_R_Update_By;
    private DbsField request_R_Update_Date;
    private DbsField request_R_Update_Time;
    private DbsGroup request_R_Mit_Request;
    private DbsField request_R_Mit_Rqst_Log_Dte_Tme;
    private DbsField request_R_Mit_Rqst_Log_Oprtr_Cde;
    private DbsField request_R_Mit_Original_Unit_Cde;
    private DbsField request_R_Mit_Work_Process_Id;
    private DbsField request_R_Mit_Step_Id;
    private DbsField request_R_Mit_Status_Cde;
    private DbsField request_R_Request_Id_Key;

    public DataAccessProgramView getVw_request_R() { return vw_request_R; }

    public DbsField getRequest_R_Ppg_Code() { return request_R_Ppg_Code; }

    public DbsField getRequest_R_Request_Team() { return request_R_Request_Team; }

    public DbsField getRequest_R_Requested_Letter_Type() { return request_R_Requested_Letter_Type; }

    public DbsField getRequest_R_Pullout_Code() { return request_R_Pullout_Code; }

    public DbsField getRequest_R_App_Prem_Rqst() { return request_R_App_Prem_Rqst; }

    public DbsField getRequest_R_Iis_Team_Code() { return request_R_Iis_Team_Code; }

    public DbsField getRequest_R_Instn_Link_Cde() { return request_R_Instn_Link_Cde; }

    public DbsField getRequest_R_System_Link_Cde() { return request_R_System_Link_Cde; }

    public DbsField getRequest_R_Public_Link_Cde() { return request_R_Public_Link_Cde; }

    public DbsField getRequest_R_Update_By() { return request_R_Update_By; }

    public DbsField getRequest_R_Update_Date() { return request_R_Update_Date; }

    public DbsField getRequest_R_Update_Time() { return request_R_Update_Time; }

    public DbsGroup getRequest_R_Mit_Request() { return request_R_Mit_Request; }

    public DbsField getRequest_R_Mit_Rqst_Log_Dte_Tme() { return request_R_Mit_Rqst_Log_Dte_Tme; }

    public DbsField getRequest_R_Mit_Rqst_Log_Oprtr_Cde() { return request_R_Mit_Rqst_Log_Oprtr_Cde; }

    public DbsField getRequest_R_Mit_Original_Unit_Cde() { return request_R_Mit_Original_Unit_Cde; }

    public DbsField getRequest_R_Mit_Work_Process_Id() { return request_R_Mit_Work_Process_Id; }

    public DbsField getRequest_R_Mit_Step_Id() { return request_R_Mit_Step_Id; }

    public DbsField getRequest_R_Mit_Status_Cde() { return request_R_Mit_Status_Cde; }

    public DbsField getRequest_R_Request_Id_Key() { return request_R_Request_Id_Key; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_request_R = new DataAccessProgramView(new NameInfo("vw_request_R", "REQUEST-R"), "ACIS_LETTER_TX_FILE", "ACIS_APP_TBL_ENT", DdmPeriodicGroups.getInstance().getGroups("ACIS_LETTER_TX_FILE"));
        request_R_Ppg_Code = vw_request_R.getRecord().newFieldInGroup("request_R_Ppg_Code", "PPG-CODE", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PPG_CODE");
        request_R_Request_Team = vw_request_R.getRecord().newFieldInGroup("request_R_Request_Team", "REQUEST-TEAM", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "REQUEST_TEAM");
        request_R_Requested_Letter_Type = vw_request_R.getRecord().newFieldInGroup("request_R_Requested_Letter_Type", "REQUESTED-LETTER-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "REQUESTED_LETTER_TYPE");
        request_R_Pullout_Code = vw_request_R.getRecord().newFieldInGroup("request_R_Pullout_Code", "PULLOUT-CODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PULLOUT_CODE");
        request_R_App_Prem_Rqst = vw_request_R.getRecord().newFieldInGroup("request_R_App_Prem_Rqst", "APP-PREM-RQST", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "APP_PREM_RQST");
        request_R_Iis_Team_Code = vw_request_R.getRecord().newFieldInGroup("request_R_Iis_Team_Code", "IIS-TEAM-CODE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "IIS_TEAM_CODE");
        request_R_Instn_Link_Cde = vw_request_R.getRecord().newFieldInGroup("request_R_Instn_Link_Cde", "INSTN-LINK-CDE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "INSTN_LINK_CDE");
        request_R_System_Link_Cde = vw_request_R.getRecord().newFieldInGroup("request_R_System_Link_Cde", "SYSTEM-LINK-CDE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "SYSTEM_LINK_CDE");
        request_R_Public_Link_Cde = vw_request_R.getRecord().newFieldInGroup("request_R_Public_Link_Cde", "PUBLIC-LINK-CDE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "PUBLIC_LINK_CDE");
        request_R_Update_By = vw_request_R.getRecord().newFieldInGroup("request_R_Update_By", "UPDATE-BY", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UPDATE_BY");
        request_R_Update_Date = vw_request_R.getRecord().newFieldInGroup("request_R_Update_Date", "UPDATE-DATE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "UPDATE_DATE");
        request_R_Update_Time = vw_request_R.getRecord().newFieldInGroup("request_R_Update_Time", "UPDATE-TIME", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "UPDATE_TIME");
        request_R_Mit_Request = vw_request_R.getRecord().newGroupInGroup("request_R_Mit_Request", "MIT-REQUEST", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_APP_TBL_ENT_MIT_REQUEST");
        request_R_Mit_Rqst_Log_Dte_Tme = request_R_Mit_Request.newFieldArrayInGroup("request_R_Mit_Rqst_Log_Dte_Tme", "MIT-RQST-LOG-DTE-TME", FieldType.STRING, 
            15, new DbsArrayController(1,2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "MIT_RQST_LOG_DTE_TME", "ACIS_APP_TBL_ENT_MIT_REQUEST");
        request_R_Mit_Rqst_Log_Oprtr_Cde = request_R_Mit_Request.newFieldArrayInGroup("request_R_Mit_Rqst_Log_Oprtr_Cde", "MIT-RQST-LOG-OPRTR-CDE", FieldType.STRING, 
            8, new DbsArrayController(1,2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "MIT_RQST_LOG_OPRTR_CDE", "ACIS_APP_TBL_ENT_MIT_REQUEST");
        request_R_Mit_Original_Unit_Cde = request_R_Mit_Request.newFieldArrayInGroup("request_R_Mit_Original_Unit_Cde", "MIT-ORIGINAL-UNIT-CDE", FieldType.STRING, 
            8, new DbsArrayController(1,2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "MIT_ORIGINAL_UNIT_CDE", "ACIS_APP_TBL_ENT_MIT_REQUEST");
        request_R_Mit_Work_Process_Id = request_R_Mit_Request.newFieldArrayInGroup("request_R_Mit_Work_Process_Id", "MIT-WORK-PROCESS-ID", FieldType.STRING, 
            6, new DbsArrayController(1,2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "MIT_WORK_PROCESS_ID", "ACIS_APP_TBL_ENT_MIT_REQUEST");
        request_R_Mit_Step_Id = request_R_Mit_Request.newFieldArrayInGroup("request_R_Mit_Step_Id", "MIT-STEP-ID", FieldType.STRING, 6, new DbsArrayController(1,2) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "MIT_STEP_ID", "ACIS_APP_TBL_ENT_MIT_REQUEST");
        request_R_Mit_Status_Cde = request_R_Mit_Request.newFieldArrayInGroup("request_R_Mit_Status_Cde", "MIT-STATUS-CDE", FieldType.STRING, 4, new DbsArrayController(1,2) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "MIT_STATUS_CDE", "ACIS_APP_TBL_ENT_MIT_REQUEST");
        request_R_Request_Id_Key = vw_request_R.getRecord().newFieldInGroup("request_R_Request_Id_Key", "REQUEST-ID-KEY", FieldType.STRING, 23, RepeatingFieldStrategy.None, 
            "REQUEST_ID_KEY");
        vw_request_R.setUniquePeList();

        this.setRecordName("LdaAppl306");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_request_R.reset();
    }

    // Constructor
    public LdaAppl306() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
