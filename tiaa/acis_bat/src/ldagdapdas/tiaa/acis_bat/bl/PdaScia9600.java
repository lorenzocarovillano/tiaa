/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:23:24 PM
**        * FROM NATURAL PDA     : SCIA9600
************************************************************
**        * FILE NAME            : PdaScia9600.java
**        * CLASS NAME           : PdaScia9600
**        * INSTANCE NAME        : PdaScia9600
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaScia9600 extends PdaBase
{
    // Properties
    private DbsGroup scia9600;
    private DbsField scia9600_Pnd_P_Return_Code;
    private DbsField scia9600_Pnd_P_Return_Msg;
    private DbsField scia9600_Pnd_P_Rec_Type;
    private DbsField scia9600_Pnd_P_Plan;
    private DbsField scia9600_Pnd_P_Subplan;
    private DbsField scia9600_Pnd_P_Ssn;
    private DbsField scia9600_Pnd_P_Pin;
    private DbsField scia9600_Pnd_P_Tiaa_Contract;
    private DbsField scia9600_Pnd_P_New_Ssn;
    private DbsField scia9600_Pnd_P_New_Subplan;
    private DbsField scia9600_Pnd_P_New_Def_Enrl_Ind;

    public DbsGroup getScia9600() { return scia9600; }

    public DbsField getScia9600_Pnd_P_Return_Code() { return scia9600_Pnd_P_Return_Code; }

    public DbsField getScia9600_Pnd_P_Return_Msg() { return scia9600_Pnd_P_Return_Msg; }

    public DbsField getScia9600_Pnd_P_Rec_Type() { return scia9600_Pnd_P_Rec_Type; }

    public DbsField getScia9600_Pnd_P_Plan() { return scia9600_Pnd_P_Plan; }

    public DbsField getScia9600_Pnd_P_Subplan() { return scia9600_Pnd_P_Subplan; }

    public DbsField getScia9600_Pnd_P_Ssn() { return scia9600_Pnd_P_Ssn; }

    public DbsField getScia9600_Pnd_P_Pin() { return scia9600_Pnd_P_Pin; }

    public DbsField getScia9600_Pnd_P_Tiaa_Contract() { return scia9600_Pnd_P_Tiaa_Contract; }

    public DbsField getScia9600_Pnd_P_New_Ssn() { return scia9600_Pnd_P_New_Ssn; }

    public DbsField getScia9600_Pnd_P_New_Subplan() { return scia9600_Pnd_P_New_Subplan; }

    public DbsField getScia9600_Pnd_P_New_Def_Enrl_Ind() { return scia9600_Pnd_P_New_Def_Enrl_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        scia9600 = dbsRecord.newGroupInRecord("scia9600", "SCIA9600");
        scia9600.setParameterOption(ParameterOption.ByReference);
        scia9600_Pnd_P_Return_Code = scia9600.newFieldInGroup("scia9600_Pnd_P_Return_Code", "#P-RETURN-CODE", FieldType.STRING, 4);
        scia9600_Pnd_P_Return_Msg = scia9600.newFieldInGroup("scia9600_Pnd_P_Return_Msg", "#P-RETURN-MSG", FieldType.STRING, 80);
        scia9600_Pnd_P_Rec_Type = scia9600.newFieldInGroup("scia9600_Pnd_P_Rec_Type", "#P-REC-TYPE", FieldType.STRING, 4);
        scia9600_Pnd_P_Plan = scia9600.newFieldInGroup("scia9600_Pnd_P_Plan", "#P-PLAN", FieldType.STRING, 6);
        scia9600_Pnd_P_Subplan = scia9600.newFieldInGroup("scia9600_Pnd_P_Subplan", "#P-SUBPLAN", FieldType.STRING, 6);
        scia9600_Pnd_P_Ssn = scia9600.newFieldInGroup("scia9600_Pnd_P_Ssn", "#P-SSN", FieldType.STRING, 9);
        scia9600_Pnd_P_Pin = scia9600.newFieldInGroup("scia9600_Pnd_P_Pin", "#P-PIN", FieldType.NUMERIC, 12);
        scia9600_Pnd_P_Tiaa_Contract = scia9600.newFieldInGroup("scia9600_Pnd_P_Tiaa_Contract", "#P-TIAA-CONTRACT", FieldType.STRING, 10);
        scia9600_Pnd_P_New_Ssn = scia9600.newFieldInGroup("scia9600_Pnd_P_New_Ssn", "#P-NEW-SSN", FieldType.STRING, 9);
        scia9600_Pnd_P_New_Subplan = scia9600.newFieldInGroup("scia9600_Pnd_P_New_Subplan", "#P-NEW-SUBPLAN", FieldType.STRING, 6);
        scia9600_Pnd_P_New_Def_Enrl_Ind = scia9600.newFieldInGroup("scia9600_Pnd_P_New_Def_Enrl_Ind", "#P-NEW-DEF-ENRL-IND", FieldType.STRING, 1);

        dbsRecord.reset();
    }

    // Constructors
    public PdaScia9600(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

