/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:51:15 PM
**        * FROM NATURAL LDA     : APPL3100
************************************************************
**        * FILE NAME            : LdaAppl3100.java
**        * CLASS NAME           : LdaAppl3100
**        * INSTANCE NAME        : LdaAppl3100
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl3100 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Ap_Work_File;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_Code;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_CodeRedef1;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_Code_1_4;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_Code_5_6;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_G_Key;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_G_Ind;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Soc_Sec;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dob;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_DobRedef2;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dob_Yyyy;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dob_Mm;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dob_Dd;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Lob;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Bill_Code;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_T_Contract;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_T_ContractRedef3;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_Tiaa_Contr;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_App_Pref;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_App_Cont;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_C_Contract;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_C_ContractRedef4;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_Cref_Contr;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_App_C_Pref;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_App_C_Cont;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_T_Doi;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_T_DoiRedef5;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_T_Doi_A;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_C_Doi;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_C_DoiRedef6;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_C_Doi_A;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Curr;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_T_Age_1st;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_C_Age_1st;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Ownership;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Sex;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Mail_Zip;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Name_Addr_Cd;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_SysRedef7;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys_Yyyy;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys_Mm;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys_Dd;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_ReleasedRedef8;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released_Yyyy;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released_Mm;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released_Dd;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_MatchedRedef9;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched_Yyyy;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched_Mm;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched_Dd;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_DeletedRedef10;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted_Yyyy;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted_Mm;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted_Dd;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_WithdrawnRedef11;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn_Yyyy;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn_Mm;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn_Dd;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Alloc_Discr;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Release_Ind;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Type;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Other_Pols;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Record_Type;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Status;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Numb_Dailys;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_RecvdRedef12;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd_Yyyy;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd_Mm;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd_Dd;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Alloc_Fmt;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_Allocation_Info;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Allocation;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Ident;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_App_Source;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_Region_CodeRedef13;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code_1_1;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code_2_2;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code_3_3;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Orig_Issue_State;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Ownership_Type;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Lob_Type;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Split_Code;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_Address_Info;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Address_Line;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_City;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Current_State_Code;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Transaction_Cd;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Stndrd_Rtn_Cd;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Finalist_Reason_Cd;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Addr_Stndrd_Code;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Stndrd_Overide;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Postal_Data_Fields;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Addr_Geographic_Code;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Annuity_Start_Date;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_Maximum_Alloc_Pct;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Max_Alloc_Pct;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Max_Alloc_Pct_Sign;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Mail_Instructions;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Eop_Addl_Cref_Request;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Process_Status;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_St_Cd;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Csm_Sec_Seg;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Rlc_College;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Rlc_Cref_Pref;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Rlc_Cref_Cont;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Prfx_Nme;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Last_Nme;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Last_NmeRedef14;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Last_Nme_1_12;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_First_Nme;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_First_NmeRedef15;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_First_Nme_1_5;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Mddle_Nme;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Mddle_NmeRedef16;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Mddle_Nme_1_1;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Sffx_Nme;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Ph_Hist_Ind;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Rcrd_Updt_Tm_Stamp;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Contact_Mode;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Racf_Id;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Pin_Nbr;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_Rqst_Log_Dte_Tm;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Rqst_Log_Dte_Time;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Sync_Ind;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Ind;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Alloc_Ind;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Tiaa_Doi;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Cref_Doi;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_Mit_Request;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Mit_Unit;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Mit_Wpid;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Ira_Rollover_Type;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Ira_Record_Type;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Status;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Lob;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Lob_Type;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Ppg;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Print_Date;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Info;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Type;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Nbr;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Proceeds_Amt;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_Addr_Info;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Address_Txt;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Address_Dest_Name;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Zip_Code;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Bank_Aba_Acct_Nmbr;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Addr_Usage_Code;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Init_Paymt_Amt;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Init_Paymt_Pct;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Financial_1;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Financial_2;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Financial_3;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Financial_4;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Financial_5;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Irc_Sectn_Grp_Cde;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Irc_Sectn_Cde;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Inst_Link_Cde;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Applcnt_Req_Type;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Addr_Sync_Ind;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Tiaa_Service_Agent;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Prap_Rsch_Mit_Ind;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Ppg_Team_Cde;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Source_1;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Source_2;
    private DbsGroup pnd_Ap_Work_File_Pnd_Ap_Ap_Allocation_Info_2;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Allocation_2;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Ident_2;
    private DbsGroup pnd_Ap_Work_FileRedef17;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Data_1;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Data_2;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Data_3;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Data_4;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Data_5;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Data_6;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Data_7;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Data_8;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Data_9;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Data_10;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Data_11;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Data_12;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Data_13;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Data_14;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Data_15;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Data_16;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Data_17;
    private DbsField pnd_Ap_Work_File_Pnd_Ap_Data_18;
    private DbsGroup pnd_Ap_Add_Work_File;
    private DbsGroup pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Prap;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Isn;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Add_Sort_Lob_Type;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code;
    private DbsGroup pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_CodeRedef18;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code_1_1;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code_2_2;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code_3_3;
    private DbsGroup pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Appl_Or_Prem;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Add_Pin_Nbr;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Add_Soc_Sec;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Add_Dob;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Add_Coll_Code;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Add_Lob_Type;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Add_T_Contract;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Add_C_Contract;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Last_Nme;
    private DbsGroup pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Last_NmeRedef19;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Last_Nme_1_12;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_First_Nme;
    private DbsGroup pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_First_NmeRedef20;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_First_Nme_1_5;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Mddle_Nme;
    private DbsGroup pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Mddle_NmeRedef21;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Mddle_Nme_1_1;
    private DbsGroup pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Prem;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Add_Ph_Rltnshp_Cde;
    private DbsGroup pnd_Ap_Add_Work_File_Pnd_Ap_Misc;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Add_Match_Ind;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Filler_2;
    private DbsGroup pnd_Ap_Add_Work_FileRedef22;
    private DbsField pnd_Ap_Add_Work_File_Pnd_Ap_Add_Data_1;
    private DbsGroup pnd_Ap_Key;
    private DbsField pnd_Ap_Key_Pnd_Ap_Key_Region_Code;
    private DbsGroup pnd_Ap_Key_Pnd_Ap_Key_Region_CodeRedef23;
    private DbsField pnd_Ap_Key_Pnd_Ap_Key_Region_Code_1_1;
    private DbsField pnd_Ap_Key_Pnd_Ap_Key_Region_Code_2_2;
    private DbsField pnd_Ap_Key_Pnd_Ap_Key_Region_Code_3_3;
    private DbsField pnd_Ap_Key_Pnd_Ap_Key_Ppg_Team_Cde;
    private DbsField pnd_Ap_Key_Pnd_Ap_Key_Wpid;
    private DbsField pnd_Ap_Key_Pnd_Ap_Key_Coll_Code;
    private DbsField pnd_Ap_Key_Pnd_Ap_Key_Soc_Sec;
    private DbsGroup pnd_Ap_Key_Pnd_Ap_Fields_From_Appl_Or_Prem_2;
    private DbsField pnd_Ap_Key_Pnd_Ap_Add_Mit_Unit;
    private DbsField pnd_Ap_Key_Pnd_Ap_Add_Mit_Wpid;
    private DbsField pnd_Ap_Key_Pnd_Ap_Add_Prap_Rsch_Mit_Ind;
    private DbsField pnd_Ap_Key_Pnd_Ap_Ap_Prap_Prem_Rsch_Ind;

    public DbsGroup getPnd_Ap_Work_File() { return pnd_Ap_Work_File; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Coll_Code() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_Code; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_Coll_CodeRedef1() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_CodeRedef1; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Coll_Code_1_4() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_Code_1_4; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Coll_Code_5_6() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_Code_5_6; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_G_Key() { return pnd_Ap_Work_File_Pnd_Ap_Ap_G_Key; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_G_Ind() { return pnd_Ap_Work_File_Pnd_Ap_Ap_G_Ind; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Soc_Sec() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Soc_Sec; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dob() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dob; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_DobRedef2() { return pnd_Ap_Work_File_Pnd_Ap_Ap_DobRedef2; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dob_Yyyy() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dob_Yyyy; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dob_Mm() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dob_Mm; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dob_Dd() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dob_Dd; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Lob() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Lob; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Bill_Code() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Bill_Code; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_T_Contract() { return pnd_Ap_Work_File_Pnd_Ap_Ap_T_Contract; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_T_ContractRedef3() { return pnd_Ap_Work_File_Pnd_Ap_Ap_T_ContractRedef3; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_Tiaa_Contr() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Tiaa_Contr; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_App_Pref() { return pnd_Ap_Work_File_Pnd_Ap_App_Pref; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_App_Cont() { return pnd_Ap_Work_File_Pnd_Ap_App_Cont; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_C_Contract() { return pnd_Ap_Work_File_Pnd_Ap_Ap_C_Contract; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_C_ContractRedef4() { return pnd_Ap_Work_File_Pnd_Ap_Ap_C_ContractRedef4; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_Cref_Contr() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Cref_Contr; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_App_C_Pref() { return pnd_Ap_Work_File_Pnd_Ap_App_C_Pref; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_App_C_Cont() { return pnd_Ap_Work_File_Pnd_Ap_App_C_Cont; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_T_Doi() { return pnd_Ap_Work_File_Pnd_Ap_Ap_T_Doi; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_T_DoiRedef5() { return pnd_Ap_Work_File_Pnd_Ap_Ap_T_DoiRedef5; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_T_Doi_A() { return pnd_Ap_Work_File_Pnd_Ap_Ap_T_Doi_A; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_C_Doi() { return pnd_Ap_Work_File_Pnd_Ap_Ap_C_Doi; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_C_DoiRedef6() { return pnd_Ap_Work_File_Pnd_Ap_Ap_C_DoiRedef6; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_C_Doi_A() { return pnd_Ap_Work_File_Pnd_Ap_Ap_C_Doi_A; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Curr() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Curr; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_T_Age_1st() { return pnd_Ap_Work_File_Pnd_Ap_Ap_T_Age_1st; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_C_Age_1st() { return pnd_Ap_Work_File_Pnd_Ap_Ap_C_Age_1st; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Ownership() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Ownership; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Sex() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Sex; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Mail_Zip() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Mail_Zip; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Name_Addr_Cd() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Name_Addr_Cd; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_SysRedef7() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_SysRedef7; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys_Yyyy() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys_Yyyy; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys_Mm() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys_Mm; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys_Dd() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys_Dd; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_ReleasedRedef8() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_ReleasedRedef8; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released_Yyyy() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released_Yyyy; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released_Mm() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released_Mm; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released_Dd() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released_Dd; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_MatchedRedef9() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_MatchedRedef9; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched_Yyyy() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched_Yyyy; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched_Mm() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched_Mm; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched_Dd() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched_Dd; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_DeletedRedef10() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_DeletedRedef10; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted_Yyyy() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted_Yyyy; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted_Mm() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted_Mm; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted_Dd() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted_Dd; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_WithdrawnRedef11() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_WithdrawnRedef11; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn_Yyyy() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn_Yyyy; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn_Mm() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn_Mm; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn_Dd() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn_Dd; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Alloc_Discr() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Alloc_Discr; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Release_Ind() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Release_Ind; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Type() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Type; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Other_Pols() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Other_Pols; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Record_Type() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Record_Type; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Status() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Status; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Numb_Dailys() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Numb_Dailys; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_RecvdRedef12() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_RecvdRedef12; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd_Yyyy() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd_Yyyy; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd_Mm() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd_Mm; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd_Dd() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd_Dd; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Alloc_Fmt() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Alloc_Fmt; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_Allocation_Info() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Allocation_Info; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Allocation() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Allocation; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Ident() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Ident; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_App_Source() { return pnd_Ap_Work_File_Pnd_Ap_Ap_App_Source; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_Region_CodeRedef13() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Region_CodeRedef13; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code_1_1() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code_1_1; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code_2_2() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code_2_2; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code_3_3() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code_3_3; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Orig_Issue_State() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Orig_Issue_State; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Ownership_Type() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Ownership_Type; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Lob_Type() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Lob_Type; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Split_Code() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Split_Code; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_Address_Info() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Address_Info; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Address_Line() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Address_Line; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_City() { return pnd_Ap_Work_File_Pnd_Ap_Ap_City; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Current_State_Code() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Current_State_Code; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Transaction_Cd() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Transaction_Cd; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Stndrd_Rtn_Cd() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Stndrd_Rtn_Cd; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Finalist_Reason_Cd() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Finalist_Reason_Cd; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Addr_Stndrd_Code() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Addr_Stndrd_Code; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Stndrd_Overide() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Stndrd_Overide; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Postal_Data_Fields() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Postal_Data_Fields; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Addr_Geographic_Code() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Addr_Geographic_Code; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Annuity_Start_Date() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Annuity_Start_Date; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_Maximum_Alloc_Pct() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Maximum_Alloc_Pct; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Max_Alloc_Pct() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Max_Alloc_Pct; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Max_Alloc_Pct_Sign() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Max_Alloc_Pct_Sign; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Mail_Instructions() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Mail_Instructions; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Eop_Addl_Cref_Request() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Eop_Addl_Cref_Request; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Process_Status() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Process_Status; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Coll_St_Cd() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_St_Cd; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Csm_Sec_Seg() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Csm_Sec_Seg; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Rlc_College() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Rlc_College; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Rlc_Cref_Pref() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Rlc_Cref_Pref; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Rlc_Cref_Cont() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Rlc_Cref_Cont; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Prfx_Nme() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Prfx_Nme; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Last_Nme() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Last_Nme; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Last_NmeRedef14() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Last_NmeRedef14; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Last_Nme_1_12() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Last_Nme_1_12; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Cor_First_Nme() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_First_Nme; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_Cor_First_NmeRedef15() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_First_NmeRedef15; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Cor_First_Nme_1_5() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_First_Nme_1_5; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Mddle_Nme() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Mddle_Nme; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Mddle_NmeRedef16() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Mddle_NmeRedef16; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Mddle_Nme_1_1() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Mddle_Nme_1_1; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Sffx_Nme() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Sffx_Nme; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Ph_Hist_Ind() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Ph_Hist_Ind; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Rcrd_Updt_Tm_Stamp() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Rcrd_Updt_Tm_Stamp; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Contact_Mode() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Contact_Mode; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Racf_Id() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Racf_Id; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Pin_Nbr() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Pin_Nbr; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_Rqst_Log_Dte_Tm() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Rqst_Log_Dte_Tm; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Rqst_Log_Dte_Time() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Rqst_Log_Dte_Time; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Sync_Ind() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Sync_Ind; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Ind() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Ind; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Alloc_Ind() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Alloc_Ind; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Tiaa_Doi() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Tiaa_Doi; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Cref_Doi() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Cref_Doi; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_Mit_Request() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Mit_Request; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Mit_Unit() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Mit_Unit; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Mit_Wpid() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Mit_Wpid; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Ira_Rollover_Type() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Ira_Rollover_Type; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Ira_Record_Type() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Ira_Record_Type; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Status() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Status; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Lob() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Lob; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Lob_Type() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Lob_Type; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Ppg() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Ppg; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Print_Date() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Print_Date; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Info() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Info; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Type() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Type; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Nbr() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Nbr; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Proceeds_Amt() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Proceeds_Amt; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_Addr_Info() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Addr_Info; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Address_Txt() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Address_Txt; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Address_Dest_Name() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Address_Dest_Name; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Zip_Code() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Zip_Code; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Bank_Pymnt_Acct_Nmbr() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Bank_Pymnt_Acct_Nmbr; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Bank_Aba_Acct_Nmbr() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Bank_Aba_Acct_Nmbr; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Addr_Usage_Code() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Addr_Usage_Code; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Init_Paymt_Amt() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Init_Paymt_Amt; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Init_Paymt_Pct() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Init_Paymt_Pct; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Financial_1() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Financial_1; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Financial_2() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Financial_2; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Financial_3() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Financial_3; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Financial_4() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Financial_4; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Financial_5() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Financial_5; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Irc_Sectn_Grp_Cde() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Irc_Sectn_Grp_Cde; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Irc_Sectn_Cde() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Irc_Sectn_Cde; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Inst_Link_Cde() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Inst_Link_Cde; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Applcnt_Req_Type() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Applcnt_Req_Type; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Addr_Sync_Ind() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Addr_Sync_Ind; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Tiaa_Service_Agent() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Tiaa_Service_Agent; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Prap_Rsch_Mit_Ind() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Prap_Rsch_Mit_Ind; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Ppg_Team_Cde() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Ppg_Team_Cde; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Source_1() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Source_1; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Source_2() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Source_2; }

    public DbsGroup getPnd_Ap_Work_File_Pnd_Ap_Ap_Allocation_Info_2() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Allocation_Info_2; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Allocation_2() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Allocation_2; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Ident_2() { return pnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Ident_2; }

    public DbsGroup getPnd_Ap_Work_FileRedef17() { return pnd_Ap_Work_FileRedef17; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Data_1() { return pnd_Ap_Work_File_Pnd_Ap_Data_1; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Data_2() { return pnd_Ap_Work_File_Pnd_Ap_Data_2; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Data_3() { return pnd_Ap_Work_File_Pnd_Ap_Data_3; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Data_4() { return pnd_Ap_Work_File_Pnd_Ap_Data_4; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Data_5() { return pnd_Ap_Work_File_Pnd_Ap_Data_5; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Data_6() { return pnd_Ap_Work_File_Pnd_Ap_Data_6; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Data_7() { return pnd_Ap_Work_File_Pnd_Ap_Data_7; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Data_8() { return pnd_Ap_Work_File_Pnd_Ap_Data_8; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Data_9() { return pnd_Ap_Work_File_Pnd_Ap_Data_9; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Data_10() { return pnd_Ap_Work_File_Pnd_Ap_Data_10; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Data_11() { return pnd_Ap_Work_File_Pnd_Ap_Data_11; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Data_12() { return pnd_Ap_Work_File_Pnd_Ap_Data_12; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Data_13() { return pnd_Ap_Work_File_Pnd_Ap_Data_13; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Data_14() { return pnd_Ap_Work_File_Pnd_Ap_Data_14; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Data_15() { return pnd_Ap_Work_File_Pnd_Ap_Data_15; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Data_16() { return pnd_Ap_Work_File_Pnd_Ap_Data_16; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Data_17() { return pnd_Ap_Work_File_Pnd_Ap_Data_17; }

    public DbsField getPnd_Ap_Work_File_Pnd_Ap_Data_18() { return pnd_Ap_Work_File_Pnd_Ap_Data_18; }

    public DbsGroup getPnd_Ap_Add_Work_File() { return pnd_Ap_Add_Work_File; }

    public DbsGroup getPnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Prap() { return pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Prap; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Isn() { return pnd_Ap_Add_Work_File_Pnd_Ap_Isn; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Sort_Lob_Type() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Sort_Lob_Type; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code; }

    public DbsGroup getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_CodeRedef18() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_CodeRedef18; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code_1_1() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code_1_1; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code_2_2() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code_2_2; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code_3_3() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code_3_3; }

    public DbsGroup getPnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Appl_Or_Prem() { return pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Appl_Or_Prem; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Pin_Nbr() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Pin_Nbr; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Soc_Sec() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Soc_Sec; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Dob() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Dob; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Coll_Code() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Coll_Code; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Lob_Type() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Lob_Type; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Add_T_Contract() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_T_Contract; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Add_C_Contract() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_C_Contract; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Last_Nme() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Last_Nme; }

    public DbsGroup getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Last_NmeRedef19() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Last_NmeRedef19; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Last_Nme_1_12() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Last_Nme_1_12; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_First_Nme() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_First_Nme; }

    public DbsGroup getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_First_NmeRedef20() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_First_NmeRedef20; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_First_Nme_1_5() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_First_Nme_1_5; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Mddle_Nme() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Mddle_Nme; }

    public DbsGroup getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Mddle_NmeRedef21() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Mddle_NmeRedef21; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Mddle_Nme_1_1() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Mddle_Nme_1_1; }

    public DbsGroup getPnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Prem() { return pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Prem; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Ph_Rltnshp_Cde() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Ph_Rltnshp_Cde; }

    public DbsGroup getPnd_Ap_Add_Work_File_Pnd_Ap_Misc() { return pnd_Ap_Add_Work_File_Pnd_Ap_Misc; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Match_Ind() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Match_Ind; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Filler_2() { return pnd_Ap_Add_Work_File_Pnd_Ap_Filler_2; }

    public DbsGroup getPnd_Ap_Add_Work_FileRedef22() { return pnd_Ap_Add_Work_FileRedef22; }

    public DbsField getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Data_1() { return pnd_Ap_Add_Work_File_Pnd_Ap_Add_Data_1; }

    public DbsGroup getPnd_Ap_Key() { return pnd_Ap_Key; }

    public DbsField getPnd_Ap_Key_Pnd_Ap_Key_Region_Code() { return pnd_Ap_Key_Pnd_Ap_Key_Region_Code; }

    public DbsGroup getPnd_Ap_Key_Pnd_Ap_Key_Region_CodeRedef23() { return pnd_Ap_Key_Pnd_Ap_Key_Region_CodeRedef23; }

    public DbsField getPnd_Ap_Key_Pnd_Ap_Key_Region_Code_1_1() { return pnd_Ap_Key_Pnd_Ap_Key_Region_Code_1_1; }

    public DbsField getPnd_Ap_Key_Pnd_Ap_Key_Region_Code_2_2() { return pnd_Ap_Key_Pnd_Ap_Key_Region_Code_2_2; }

    public DbsField getPnd_Ap_Key_Pnd_Ap_Key_Region_Code_3_3() { return pnd_Ap_Key_Pnd_Ap_Key_Region_Code_3_3; }

    public DbsField getPnd_Ap_Key_Pnd_Ap_Key_Ppg_Team_Cde() { return pnd_Ap_Key_Pnd_Ap_Key_Ppg_Team_Cde; }

    public DbsField getPnd_Ap_Key_Pnd_Ap_Key_Wpid() { return pnd_Ap_Key_Pnd_Ap_Key_Wpid; }

    public DbsField getPnd_Ap_Key_Pnd_Ap_Key_Coll_Code() { return pnd_Ap_Key_Pnd_Ap_Key_Coll_Code; }

    public DbsField getPnd_Ap_Key_Pnd_Ap_Key_Soc_Sec() { return pnd_Ap_Key_Pnd_Ap_Key_Soc_Sec; }

    public DbsGroup getPnd_Ap_Key_Pnd_Ap_Fields_From_Appl_Or_Prem_2() { return pnd_Ap_Key_Pnd_Ap_Fields_From_Appl_Or_Prem_2; }

    public DbsField getPnd_Ap_Key_Pnd_Ap_Add_Mit_Unit() { return pnd_Ap_Key_Pnd_Ap_Add_Mit_Unit; }

    public DbsField getPnd_Ap_Key_Pnd_Ap_Add_Mit_Wpid() { return pnd_Ap_Key_Pnd_Ap_Add_Mit_Wpid; }

    public DbsField getPnd_Ap_Key_Pnd_Ap_Add_Prap_Rsch_Mit_Ind() { return pnd_Ap_Key_Pnd_Ap_Add_Prap_Rsch_Mit_Ind; }

    public DbsField getPnd_Ap_Key_Pnd_Ap_Ap_Prap_Prem_Rsch_Ind() { return pnd_Ap_Key_Pnd_Ap_Ap_Prap_Prem_Rsch_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Ap_Work_File = newGroupInRecord("pnd_Ap_Work_File", "#AP-WORK-FILE");
        pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_Code = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_Code", "#AP-AP-COLL-CODE", FieldType.STRING, 
            6);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_CodeRedef1 = pnd_Ap_Work_File.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_CodeRedef1", "Redefines", pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_Code);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_Code_1_4 = pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_CodeRedef1.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_Code_1_4", 
            "#AP-AP-COLL-CODE-1-4", FieldType.STRING, 4);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_Code_5_6 = pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_CodeRedef1.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_Code_5_6", 
            "#AP-AP-COLL-CODE-5-6", FieldType.STRING, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_G_Key = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_G_Key", "#AP-AP-G-KEY", FieldType.NUMERIC, 4);
        pnd_Ap_Work_File_Pnd_Ap_Ap_G_Ind = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_G_Ind", "#AP-AP-G-IND", FieldType.NUMERIC, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Soc_Sec = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Soc_Sec", "#AP-AP-SOC-SEC", FieldType.NUMERIC, 
            9);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dob = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dob", "#AP-AP-DOB", FieldType.NUMERIC, 8);
        pnd_Ap_Work_File_Pnd_Ap_Ap_DobRedef2 = pnd_Ap_Work_File.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_DobRedef2", "Redefines", pnd_Ap_Work_File_Pnd_Ap_Ap_Dob);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dob_Yyyy = pnd_Ap_Work_File_Pnd_Ap_Ap_DobRedef2.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dob_Yyyy", "#AP-AP-DOB-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dob_Mm = pnd_Ap_Work_File_Pnd_Ap_Ap_DobRedef2.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dob_Mm", "#AP-AP-DOB-MM", 
            FieldType.NUMERIC, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dob_Dd = pnd_Ap_Work_File_Pnd_Ap_Ap_DobRedef2.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dob_Dd", "#AP-AP-DOB-DD", 
            FieldType.NUMERIC, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Lob = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Lob", "#AP-AP-LOB", FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Bill_Code = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Bill_Code", "#AP-AP-BILL-CODE", FieldType.STRING, 
            1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_T_Contract = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_T_Contract", "#AP-AP-T-CONTRACT", FieldType.STRING, 
            8);
        pnd_Ap_Work_File_Pnd_Ap_Ap_T_ContractRedef3 = pnd_Ap_Work_File.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_T_ContractRedef3", "Redefines", pnd_Ap_Work_File_Pnd_Ap_Ap_T_Contract);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Tiaa_Contr = pnd_Ap_Work_File_Pnd_Ap_Ap_T_ContractRedef3.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Tiaa_Contr", "#AP-AP-TIAA-CONTR");
        pnd_Ap_Work_File_Pnd_Ap_App_Pref = pnd_Ap_Work_File_Pnd_Ap_Ap_Tiaa_Contr.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_App_Pref", "#AP-APP-PREF", FieldType.STRING, 
            1);
        pnd_Ap_Work_File_Pnd_Ap_App_Cont = pnd_Ap_Work_File_Pnd_Ap_Ap_Tiaa_Contr.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_App_Cont", "#AP-APP-CONT", FieldType.STRING, 
            7);
        pnd_Ap_Work_File_Pnd_Ap_Ap_C_Contract = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_C_Contract", "#AP-AP-C-CONTRACT", FieldType.STRING, 
            8);
        pnd_Ap_Work_File_Pnd_Ap_Ap_C_ContractRedef4 = pnd_Ap_Work_File.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_C_ContractRedef4", "Redefines", pnd_Ap_Work_File_Pnd_Ap_Ap_C_Contract);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Cref_Contr = pnd_Ap_Work_File_Pnd_Ap_Ap_C_ContractRedef4.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Cref_Contr", "#AP-AP-CREF-CONTR");
        pnd_Ap_Work_File_Pnd_Ap_App_C_Pref = pnd_Ap_Work_File_Pnd_Ap_Ap_Cref_Contr.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_App_C_Pref", "#AP-APP-C-PREF", 
            FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_App_C_Cont = pnd_Ap_Work_File_Pnd_Ap_Ap_Cref_Contr.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_App_C_Cont", "#AP-APP-C-CONT", 
            FieldType.STRING, 7);
        pnd_Ap_Work_File_Pnd_Ap_Ap_T_Doi = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_T_Doi", "#AP-AP-T-DOI", FieldType.NUMERIC, 4);
        pnd_Ap_Work_File_Pnd_Ap_Ap_T_DoiRedef5 = pnd_Ap_Work_File.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_T_DoiRedef5", "Redefines", pnd_Ap_Work_File_Pnd_Ap_Ap_T_Doi);
        pnd_Ap_Work_File_Pnd_Ap_Ap_T_Doi_A = pnd_Ap_Work_File_Pnd_Ap_Ap_T_DoiRedef5.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_T_Doi_A", "#AP-AP-T-DOI-A", 
            FieldType.STRING, 4);
        pnd_Ap_Work_File_Pnd_Ap_Ap_C_Doi = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_C_Doi", "#AP-AP-C-DOI", FieldType.NUMERIC, 4);
        pnd_Ap_Work_File_Pnd_Ap_Ap_C_DoiRedef6 = pnd_Ap_Work_File.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_C_DoiRedef6", "Redefines", pnd_Ap_Work_File_Pnd_Ap_Ap_C_Doi);
        pnd_Ap_Work_File_Pnd_Ap_Ap_C_Doi_A = pnd_Ap_Work_File_Pnd_Ap_Ap_C_DoiRedef6.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_C_Doi_A", "#AP-AP-C-DOI-A", 
            FieldType.STRING, 4);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Curr = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Curr", "#AP-AP-CURR", FieldType.NUMERIC, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_T_Age_1st = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_T_Age_1st", "#AP-AP-T-AGE-1ST", FieldType.NUMERIC, 
            4);
        pnd_Ap_Work_File_Pnd_Ap_Ap_C_Age_1st = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_C_Age_1st", "#AP-AP-C-AGE-1ST", FieldType.NUMERIC, 
            4);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Ownership = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Ownership", "#AP-AP-OWNERSHIP", FieldType.NUMERIC, 
            1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Sex = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Sex", "#AP-AP-SEX", FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Mail_Zip = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Mail_Zip", "#AP-AP-MAIL-ZIP", FieldType.STRING, 
            5);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Name_Addr_Cd = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Name_Addr_Cd", "#AP-AP-NAME-ADDR-CD", FieldType.STRING, 
            5);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys", "#AP-AP-DT-ENT-SYS", FieldType.NUMERIC, 
            8);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_SysRedef7 = pnd_Ap_Work_File.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_SysRedef7", "Redefines", pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys_Yyyy = pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_SysRedef7.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys_Yyyy", 
            "#AP-AP-DT-ENT-SYS-YYYY", FieldType.NUMERIC, 4);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys_Mm = pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_SysRedef7.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys_Mm", 
            "#AP-AP-DT-ENT-SYS-MM", FieldType.NUMERIC, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys_Dd = pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_SysRedef7.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys_Dd", 
            "#AP-AP-DT-ENT-SYS-DD", FieldType.NUMERIC, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released", "#AP-AP-DT-RELEASED", FieldType.NUMERIC, 
            8);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_ReleasedRedef8 = pnd_Ap_Work_File.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_ReleasedRedef8", "Redefines", pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released_Yyyy = pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_ReleasedRedef8.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released_Yyyy", 
            "#AP-AP-DT-RELEASED-YYYY", FieldType.NUMERIC, 4);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released_Mm = pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_ReleasedRedef8.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released_Mm", 
            "#AP-AP-DT-RELEASED-MM", FieldType.NUMERIC, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released_Dd = pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_ReleasedRedef8.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released_Dd", 
            "#AP-AP-DT-RELEASED-DD", FieldType.NUMERIC, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched", "#AP-AP-DT-MATCHED", FieldType.NUMERIC, 
            8);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_MatchedRedef9 = pnd_Ap_Work_File.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_MatchedRedef9", "Redefines", pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched_Yyyy = pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_MatchedRedef9.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched_Yyyy", 
            "#AP-AP-DT-MATCHED-YYYY", FieldType.NUMERIC, 4);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched_Mm = pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_MatchedRedef9.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched_Mm", 
            "#AP-AP-DT-MATCHED-MM", FieldType.NUMERIC, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched_Dd = pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_MatchedRedef9.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched_Dd", 
            "#AP-AP-DT-MATCHED-DD", FieldType.NUMERIC, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted", "#AP-AP-DT-DELETED", FieldType.NUMERIC, 
            8);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_DeletedRedef10 = pnd_Ap_Work_File.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_DeletedRedef10", "Redefines", pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted_Yyyy = pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_DeletedRedef10.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted_Yyyy", 
            "#AP-AP-DT-DELETED-YYYY", FieldType.NUMERIC, 4);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted_Mm = pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_DeletedRedef10.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted_Mm", 
            "#AP-AP-DT-DELETED-MM", FieldType.NUMERIC, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted_Dd = pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_DeletedRedef10.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted_Dd", 
            "#AP-AP-DT-DELETED-DD", FieldType.NUMERIC, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn", "#AP-AP-DT-WITHDRAWN", FieldType.NUMERIC, 
            8);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_WithdrawnRedef11 = pnd_Ap_Work_File.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_WithdrawnRedef11", "Redefines", 
            pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn_Yyyy = pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_WithdrawnRedef11.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn_Yyyy", 
            "#AP-AP-DT-WITHDRAWN-YYYY", FieldType.NUMERIC, 4);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn_Mm = pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_WithdrawnRedef11.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn_Mm", 
            "#AP-AP-DT-WITHDRAWN-MM", FieldType.NUMERIC, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn_Dd = pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_WithdrawnRedef11.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn_Dd", 
            "#AP-AP-DT-WITHDRAWN-DD", FieldType.NUMERIC, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Alloc_Discr = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Alloc_Discr", "#AP-AP-ALLOC-DISCR", FieldType.NUMERIC, 
            1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Release_Ind = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Release_Ind", "#AP-AP-RELEASE-IND", FieldType.NUMERIC, 
            1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Type = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Type", "#AP-AP-TYPE", FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Other_Pols = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Other_Pols", "#AP-AP-OTHER-POLS", FieldType.STRING, 
            1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Record_Type = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Record_Type", "#AP-AP-RECORD-TYPE", FieldType.NUMERIC, 
            1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Status = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Status", "#AP-AP-STATUS", FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Numb_Dailys = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Numb_Dailys", "#AP-AP-NUMB-DAILYS", FieldType.NUMERIC, 
            1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd", "#AP-AP-DT-APP-RECVD", FieldType.NUMERIC, 
            8);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_RecvdRedef12 = pnd_Ap_Work_File.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_RecvdRedef12", "Redefines", 
            pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd_Yyyy = pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_RecvdRedef12.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd_Yyyy", 
            "#AP-AP-DT-APP-RECVD-YYYY", FieldType.NUMERIC, 4);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd_Mm = pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_RecvdRedef12.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd_Mm", 
            "#AP-AP-DT-APP-RECVD-MM", FieldType.NUMERIC, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd_Dd = pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_RecvdRedef12.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd_Dd", 
            "#AP-AP-DT-APP-RECVD-DD", FieldType.NUMERIC, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Alloc_Fmt = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Alloc_Fmt", "#AP-AP-ALLOC-FMT", FieldType.STRING, 
            1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Allocation_Info = pnd_Ap_Work_File.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Allocation_Info", "#AP-AP-ALLOCATION-INFO");
        pnd_Ap_Work_File_Pnd_Ap_Ap_Allocation = pnd_Ap_Work_File_Pnd_Ap_Ap_Allocation_Info.newFieldArrayInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Allocation", 
            "#AP-AP-ALLOCATION", FieldType.NUMERIC, 3, new DbsArrayController(1,100));
        pnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Ident = pnd_Ap_Work_File_Pnd_Ap_Ap_Allocation_Info.newFieldArrayInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Ident", 
            "#AP-AP-FUND-IDENT", FieldType.STRING, 10, new DbsArrayController(1,100));
        pnd_Ap_Work_File_Pnd_Ap_Ap_App_Source = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_App_Source", "#AP-AP-APP-SOURCE", FieldType.STRING, 
            1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code", "#AP-AP-REGION-CODE", FieldType.STRING, 
            3);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Region_CodeRedef13 = pnd_Ap_Work_File.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Region_CodeRedef13", "Redefines", 
            pnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code_1_1 = pnd_Ap_Work_File_Pnd_Ap_Ap_Region_CodeRedef13.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code_1_1", 
            "#AP-AP-REGION-CODE-1-1", FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code_2_2 = pnd_Ap_Work_File_Pnd_Ap_Ap_Region_CodeRedef13.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code_2_2", 
            "#AP-AP-REGION-CODE-2-2", FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code_3_3 = pnd_Ap_Work_File_Pnd_Ap_Ap_Region_CodeRedef13.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code_3_3", 
            "#AP-AP-REGION-CODE-3-3", FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Orig_Issue_State = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Orig_Issue_State", "#AP-AP-ORIG-ISSUE-STATE", 
            FieldType.STRING, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Ownership_Type = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Ownership_Type", "#AP-AP-OWNERSHIP-TYPE", 
            FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Lob_Type = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Lob_Type", "#AP-AP-LOB-TYPE", FieldType.STRING, 
            1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Split_Code = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Split_Code", "#AP-AP-SPLIT-CODE", FieldType.STRING, 
            2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Address_Info = pnd_Ap_Work_File.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Address_Info", "#AP-AP-ADDRESS-INFO");
        pnd_Ap_Work_File_Pnd_Ap_Ap_Address_Line = pnd_Ap_Work_File_Pnd_Ap_Ap_Address_Info.newFieldArrayInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Address_Line", 
            "#AP-AP-ADDRESS-LINE", FieldType.STRING, 35, new DbsArrayController(1,5));
        pnd_Ap_Work_File_Pnd_Ap_Ap_City = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_City", "#AP-AP-CITY", FieldType.STRING, 27);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Current_State_Code = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Current_State_Code", "#AP-AP-CURRENT-STATE-CODE", 
            FieldType.STRING, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Transaction_Cd = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Transaction_Cd", "#AP-AP-DANA-TRANSACTION-CD", 
            FieldType.STRING, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Stndrd_Rtn_Cd = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Stndrd_Rtn_Cd", "#AP-AP-DANA-STNDRD-RTN-CD", 
            FieldType.STRING, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Finalist_Reason_Cd = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Finalist_Reason_Cd", "#AP-AP-DANA-FINALIST-REASON-CD", 
            FieldType.STRING, 10);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Addr_Stndrd_Code = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Addr_Stndrd_Code", "#AP-AP-DANA-ADDR-STNDRD-CODE", 
            FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Stndrd_Overide = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Stndrd_Overide", "#AP-AP-DANA-STNDRD-OVERIDE", 
            FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Postal_Data_Fields = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Postal_Data_Fields", "#AP-AP-DANA-POSTAL-DATA-FIELDS", 
            FieldType.STRING, 44);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Addr_Geographic_Code = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Addr_Geographic_Code", 
            "#AP-AP-DANA-ADDR-GEOGRAPHIC-CODE", FieldType.STRING, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Annuity_Start_Date = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Annuity_Start_Date", "#AP-AP-ANNUITY-START-DATE", 
            FieldType.STRING, 8);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Maximum_Alloc_Pct = pnd_Ap_Work_File.newGroupArrayInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Maximum_Alloc_Pct", "#AP-AP-MAXIMUM-ALLOC-PCT", 
            new DbsArrayController(1,100));
        pnd_Ap_Work_File_Pnd_Ap_Ap_Max_Alloc_Pct = pnd_Ap_Work_File_Pnd_Ap_Ap_Maximum_Alloc_Pct.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Max_Alloc_Pct", 
            "#AP-AP-MAX-ALLOC-PCT", FieldType.STRING, 3);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Max_Alloc_Pct_Sign = pnd_Ap_Work_File_Pnd_Ap_Ap_Maximum_Alloc_Pct.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Max_Alloc_Pct_Sign", 
            "#AP-AP-MAX-ALLOC-PCT-SIGN", FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Mail_Instructions = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Mail_Instructions", "#AP-AP-MAIL-INSTRUCTIONS", 
            FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Eop_Addl_Cref_Request = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Eop_Addl_Cref_Request", "#AP-AP-EOP-ADDL-CREF-REQUEST", 
            FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Process_Status = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Process_Status", "#AP-AP-PROCESS-STATUS", 
            FieldType.STRING, 4);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_St_Cd = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Coll_St_Cd", "#AP-AP-COLL-ST-CD", FieldType.STRING, 
            2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Csm_Sec_Seg = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Csm_Sec_Seg", "#AP-AP-CSM-SEC-SEG", FieldType.STRING, 
            4);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Rlc_College = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Rlc_College", "#AP-AP-RLC-COLLEGE", FieldType.STRING, 
            4);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Rlc_Cref_Pref = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Rlc_Cref_Pref", "#AP-AP-RLC-CREF-PREF", 
            FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Rlc_Cref_Cont = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Rlc_Cref_Cont", "#AP-AP-RLC-CREF-CONT", 
            FieldType.STRING, 7);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Prfx_Nme = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Prfx_Nme", "#AP-AP-COR-PRFX-NME", FieldType.STRING, 
            8);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Last_Nme = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Last_Nme", "#AP-AP-COR-LAST-NME", FieldType.STRING, 
            30);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Last_NmeRedef14 = pnd_Ap_Work_File.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Last_NmeRedef14", "Redefines", 
            pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Last_Nme);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Last_Nme_1_12 = pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Last_NmeRedef14.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Last_Nme_1_12", 
            "#AP-AP-COR-LAST-NME-1-12", FieldType.STRING, 12);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_First_Nme = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_First_Nme", "#AP-AP-COR-FIRST-NME", 
            FieldType.STRING, 30);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_First_NmeRedef15 = pnd_Ap_Work_File.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_First_NmeRedef15", "Redefines", 
            pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_First_Nme);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_First_Nme_1_5 = pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_First_NmeRedef15.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_First_Nme_1_5", 
            "#AP-AP-COR-FIRST-NME-1-5", FieldType.STRING, 5);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Mddle_Nme = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Mddle_Nme", "#AP-AP-COR-MDDLE-NME", 
            FieldType.STRING, 30);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Mddle_NmeRedef16 = pnd_Ap_Work_File.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Mddle_NmeRedef16", "Redefines", 
            pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Mddle_Nme);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Mddle_Nme_1_1 = pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Mddle_NmeRedef16.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Mddle_Nme_1_1", 
            "#AP-AP-COR-MDDLE-NME-1-1", FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Sffx_Nme = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Sffx_Nme", "#AP-AP-COR-SFFX-NME", FieldType.STRING, 
            8);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Ph_Hist_Ind = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Ph_Hist_Ind", "#AP-AP-PH-HIST-IND", FieldType.STRING, 
            1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Rcrd_Updt_Tm_Stamp = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Rcrd_Updt_Tm_Stamp", "#AP-AP-RCRD-UPDT-TM-STAMP", 
            FieldType.STRING, 8);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Contact_Mode = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Contact_Mode", "#AP-AP-CONTACT-MODE", FieldType.STRING, 
            1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Racf_Id = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Racf_Id", "#AP-AP-RACF-ID", FieldType.STRING, 
            16);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Pin_Nbr = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Pin_Nbr", "#AP-AP-PIN-NBR", FieldType.NUMERIC, 
            12);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Rqst_Log_Dte_Tm = pnd_Ap_Work_File.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Rqst_Log_Dte_Tm", "#AP-AP-RQST-LOG-DTE-TM");
        pnd_Ap_Work_File_Pnd_Ap_Ap_Rqst_Log_Dte_Time = pnd_Ap_Work_File_Pnd_Ap_Ap_Rqst_Log_Dte_Tm.newFieldArrayInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Rqst_Log_Dte_Time", 
            "#AP-AP-RQST-LOG-DTE-TIME", FieldType.STRING, 15, new DbsArrayController(1,5));
        pnd_Ap_Work_File_Pnd_Ap_Ap_Sync_Ind = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Sync_Ind", "#AP-AP-SYNC-IND", FieldType.STRING, 
            1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Ind = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Ind", "#AP-AP-COR-IND", FieldType.STRING, 
            1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Alloc_Ind = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Alloc_Ind", "#AP-AP-ALLOC-IND", FieldType.STRING, 
            1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Tiaa_Doi = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Tiaa_Doi", "#AP-AP-TIAA-DOI", FieldType.DATE);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Cref_Doi = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Cref_Doi", "#AP-AP-CREF-DOI", FieldType.DATE);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Mit_Request = pnd_Ap_Work_File.newGroupArrayInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Mit_Request", "#AP-AP-MIT-REQUEST", 
            new DbsArrayController(1,5));
        pnd_Ap_Work_File_Pnd_Ap_Ap_Mit_Unit = pnd_Ap_Work_File_Pnd_Ap_Ap_Mit_Request.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Mit_Unit", "#AP-AP-MIT-UNIT", 
            FieldType.STRING, 8);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Mit_Wpid = pnd_Ap_Work_File_Pnd_Ap_Ap_Mit_Request.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Mit_Wpid", "#AP-AP-MIT-WPID", 
            FieldType.STRING, 6);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Ira_Rollover_Type = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Ira_Rollover_Type", "#AP-AP-IRA-ROLLOVER-TYPE", 
            FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Ira_Record_Type = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Ira_Record_Type", "#AP-AP-IRA-RECORD-TYPE", 
            FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Status = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Status", "#AP-AP-MULT-APP-STATUS", 
            FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Lob = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Lob", "#AP-AP-MULT-APP-LOB", FieldType.STRING, 
            1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Lob_Type = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Lob_Type", "#AP-AP-MULT-APP-LOB-TYPE", 
            FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Ppg = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Ppg", "#AP-AP-MULT-APP-PPG", FieldType.STRING, 
            6);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Print_Date = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Print_Date", "#AP-AP-PRINT-DATE", FieldType.DATE);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Info = pnd_Ap_Work_File.newGroupArrayInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Info", "#AP-AP-CNTRCT-INFO", 
            new DbsArrayController(1,15));
        pnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Type = pnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Info.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Type", "#AP-AP-CNTRCT-TYPE", 
            FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Nbr = pnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Info.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Nbr", "#AP-AP-CNTRCT-NBR", 
            FieldType.STRING, 10);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Proceeds_Amt = pnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Info.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Proceeds_Amt", 
            "#AP-AP-CNTRCT-PROCEEDS-AMT", FieldType.DECIMAL, 10,2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Addr_Info = pnd_Ap_Work_File.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Addr_Info", "#AP-AP-ADDR-INFO");
        pnd_Ap_Work_File_Pnd_Ap_Ap_Address_Txt = pnd_Ap_Work_File_Pnd_Ap_Ap_Addr_Info.newFieldArrayInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Address_Txt", "#AP-AP-ADDRESS-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1,5));
        pnd_Ap_Work_File_Pnd_Ap_Ap_Address_Dest_Name = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Address_Dest_Name", "#AP-AP-ADDRESS-DEST-NAME", 
            FieldType.STRING, 35);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Zip_Code = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Zip_Code", "#AP-AP-ZIP-CODE", FieldType.STRING, 
            9);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Bank_Pymnt_Acct_Nmbr = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Bank_Pymnt_Acct_Nmbr", "#AP-AP-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 21);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Bank_Aba_Acct_Nmbr = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Bank_Aba_Acct_Nmbr", "#AP-AP-BANK-ABA-ACCT-NMBR", 
            FieldType.STRING, 9);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Addr_Usage_Code = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Addr_Usage_Code", "#AP-AP-ADDR-USAGE-CODE", 
            FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Init_Paymt_Amt = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Init_Paymt_Amt", "#AP-AP-INIT-PAYMT-AMT", 
            FieldType.DECIMAL, 10,2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Init_Paymt_Pct = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Init_Paymt_Pct", "#AP-AP-INIT-PAYMT-PCT", 
            FieldType.NUMERIC, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Financial_1 = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Financial_1", "#AP-AP-FINANCIAL-1", FieldType.DECIMAL, 
            10,2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Financial_2 = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Financial_2", "#AP-AP-FINANCIAL-2", FieldType.DECIMAL, 
            10,2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Financial_3 = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Financial_3", "#AP-AP-FINANCIAL-3", FieldType.DECIMAL, 
            10,2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Financial_4 = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Financial_4", "#AP-AP-FINANCIAL-4", FieldType.DECIMAL, 
            10,2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Financial_5 = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Financial_5", "#AP-AP-FINANCIAL-5", FieldType.DECIMAL, 
            10,2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Irc_Sectn_Grp_Cde = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Irc_Sectn_Grp_Cde", "#AP-AP-IRC-SECTN-GRP-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Irc_Sectn_Cde = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Irc_Sectn_Cde", "#AP-AP-IRC-SECTN-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Inst_Link_Cde = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Inst_Link_Cde", "#AP-AP-INST-LINK-CDE", 
            FieldType.NUMERIC, 6);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Applcnt_Req_Type = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Applcnt_Req_Type", "#AP-AP-APPLCNT-REQ-TYPE", 
            FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Addr_Sync_Ind = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Addr_Sync_Ind", "#AP-AP-ADDR-SYNC-IND", 
            FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Tiaa_Service_Agent = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Tiaa_Service_Agent", "#AP-AP-TIAA-SERVICE-AGENT", 
            FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Prap_Rsch_Mit_Ind = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Prap_Rsch_Mit_Ind", "#AP-AP-PRAP-RSCH-MIT-IND", 
            FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Ppg_Team_Cde = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Ppg_Team_Cde", "#AP-AP-PPG-TEAM-CDE", FieldType.STRING, 
            8);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Source_1 = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Source_1", "#AP-AP-FUND-SOURCE-1", 
            FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Source_2 = pnd_Ap_Work_File.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Source_2", "#AP-AP-FUND-SOURCE-2", 
            FieldType.STRING, 1);
        pnd_Ap_Work_File_Pnd_Ap_Ap_Allocation_Info_2 = pnd_Ap_Work_File.newGroupInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Allocation_Info_2", "#AP-AP-ALLOCATION-INFO-2");
        pnd_Ap_Work_File_Pnd_Ap_Ap_Allocation_2 = pnd_Ap_Work_File_Pnd_Ap_Ap_Allocation_Info_2.newFieldArrayInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Allocation_2", 
            "#AP-AP-ALLOCATION-2", FieldType.NUMERIC, 3, new DbsArrayController(1,100));
        pnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Ident_2 = pnd_Ap_Work_File_Pnd_Ap_Ap_Allocation_Info_2.newFieldArrayInGroup("pnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Ident_2", 
            "#AP-AP-FUND-IDENT-2", FieldType.STRING, 10, new DbsArrayController(1,100));
        pnd_Ap_Work_FileRedef17 = newGroupInRecord("pnd_Ap_Work_FileRedef17", "Redefines", pnd_Ap_Work_File);
        pnd_Ap_Work_File_Pnd_Ap_Data_1 = pnd_Ap_Work_FileRedef17.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Data_1", "#AP-DATA-1", FieldType.STRING, 250);
        pnd_Ap_Work_File_Pnd_Ap_Data_2 = pnd_Ap_Work_FileRedef17.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Data_2", "#AP-DATA-2", FieldType.STRING, 250);
        pnd_Ap_Work_File_Pnd_Ap_Data_3 = pnd_Ap_Work_FileRedef17.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Data_3", "#AP-DATA-3", FieldType.STRING, 250);
        pnd_Ap_Work_File_Pnd_Ap_Data_4 = pnd_Ap_Work_FileRedef17.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Data_4", "#AP-DATA-4", FieldType.STRING, 250);
        pnd_Ap_Work_File_Pnd_Ap_Data_5 = pnd_Ap_Work_FileRedef17.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Data_5", "#AP-DATA-5", FieldType.STRING, 250);
        pnd_Ap_Work_File_Pnd_Ap_Data_6 = pnd_Ap_Work_FileRedef17.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Data_6", "#AP-DATA-6", FieldType.STRING, 250);
        pnd_Ap_Work_File_Pnd_Ap_Data_7 = pnd_Ap_Work_FileRedef17.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Data_7", "#AP-DATA-7", FieldType.STRING, 250);
        pnd_Ap_Work_File_Pnd_Ap_Data_8 = pnd_Ap_Work_FileRedef17.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Data_8", "#AP-DATA-8", FieldType.STRING, 250);
        pnd_Ap_Work_File_Pnd_Ap_Data_9 = pnd_Ap_Work_FileRedef17.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Data_9", "#AP-DATA-9", FieldType.STRING, 250);
        pnd_Ap_Work_File_Pnd_Ap_Data_10 = pnd_Ap_Work_FileRedef17.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Data_10", "#AP-DATA-10", FieldType.STRING, 
            250);
        pnd_Ap_Work_File_Pnd_Ap_Data_11 = pnd_Ap_Work_FileRedef17.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Data_11", "#AP-DATA-11", FieldType.STRING, 
            250);
        pnd_Ap_Work_File_Pnd_Ap_Data_12 = pnd_Ap_Work_FileRedef17.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Data_12", "#AP-DATA-12", FieldType.STRING, 
            250);
        pnd_Ap_Work_File_Pnd_Ap_Data_13 = pnd_Ap_Work_FileRedef17.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Data_13", "#AP-DATA-13", FieldType.STRING, 
            250);
        pnd_Ap_Work_File_Pnd_Ap_Data_14 = pnd_Ap_Work_FileRedef17.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Data_14", "#AP-DATA-14", FieldType.STRING, 
            250);
        pnd_Ap_Work_File_Pnd_Ap_Data_15 = pnd_Ap_Work_FileRedef17.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Data_15", "#AP-DATA-15", FieldType.STRING, 
            250);
        pnd_Ap_Work_File_Pnd_Ap_Data_16 = pnd_Ap_Work_FileRedef17.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Data_16", "#AP-DATA-16", FieldType.STRING, 
            250);
        pnd_Ap_Work_File_Pnd_Ap_Data_17 = pnd_Ap_Work_FileRedef17.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Data_17", "#AP-DATA-17", FieldType.STRING, 
            250);
        pnd_Ap_Work_File_Pnd_Ap_Data_18 = pnd_Ap_Work_FileRedef17.newFieldInGroup("pnd_Ap_Work_File_Pnd_Ap_Data_18", "#AP-DATA-18", FieldType.STRING, 
            154);

        pnd_Ap_Add_Work_File = newGroupInRecord("pnd_Ap_Add_Work_File", "#AP-ADD-WORK-FILE");
        pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Prap = pnd_Ap_Add_Work_File.newGroupInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Prap", "#AP-FIELDS-FROM-PRAP");
        pnd_Ap_Add_Work_File_Pnd_Ap_Isn = pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Prap.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Isn", "#AP-ISN", FieldType.NUMERIC, 
            10);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Sort_Lob_Type = pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Prap.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Sort_Lob_Type", 
            "#AP-ADD-SORT-LOB-TYPE", FieldType.STRING, 1);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code = pnd_Ap_Add_Work_File.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code", "#AP-ADD-REGION-CODE", 
            FieldType.STRING, 3);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_CodeRedef18 = pnd_Ap_Add_Work_File.newGroupInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_CodeRedef18", 
            "Redefines", pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code_1_1 = pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_CodeRedef18.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code_1_1", 
            "#AP-ADD-REGION-CODE-1-1", FieldType.STRING, 1);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code_2_2 = pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_CodeRedef18.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code_2_2", 
            "#AP-ADD-REGION-CODE-2-2", FieldType.STRING, 1);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code_3_3 = pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_CodeRedef18.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code_3_3", 
            "#AP-ADD-REGION-CODE-3-3", FieldType.STRING, 1);
        pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Appl_Or_Prem = pnd_Ap_Add_Work_File.newGroupInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Appl_Or_Prem", 
            "#AP-FIELDS-FROM-APPL-OR-PREM");
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Pin_Nbr = pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Appl_Or_Prem.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Pin_Nbr", 
            "#AP-ADD-PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Soc_Sec = pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Appl_Or_Prem.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Soc_Sec", 
            "#AP-ADD-SOC-SEC", FieldType.NUMERIC, 9);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Dob = pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Appl_Or_Prem.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Dob", 
            "#AP-ADD-DOB", FieldType.NUMERIC, 8);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Coll_Code = pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Appl_Or_Prem.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Coll_Code", 
            "#AP-ADD-COLL-CODE", FieldType.STRING, 6);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Lob_Type = pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Appl_Or_Prem.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Lob_Type", 
            "#AP-ADD-LOB-TYPE", FieldType.STRING, 1);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_T_Contract = pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Appl_Or_Prem.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_T_Contract", 
            "#AP-ADD-T-CONTRACT", FieldType.STRING, 9);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_C_Contract = pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Appl_Or_Prem.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_C_Contract", 
            "#AP-ADD-C-CONTRACT", FieldType.STRING, 9);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Last_Nme = pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Appl_Or_Prem.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Last_Nme", 
            "#AP-ADD-COR-LAST-NME", FieldType.STRING, 30);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Last_NmeRedef19 = pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Appl_Or_Prem.newGroupInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Last_NmeRedef19", 
            "Redefines", pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Last_Nme);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Last_Nme_1_12 = pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Last_NmeRedef19.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Last_Nme_1_12", 
            "#AP-ADD-COR-LAST-NME-1-12", FieldType.STRING, 12);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_First_Nme = pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Appl_Or_Prem.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_First_Nme", 
            "#AP-ADD-COR-FIRST-NME", FieldType.STRING, 30);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_First_NmeRedef20 = pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Appl_Or_Prem.newGroupInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_First_NmeRedef20", 
            "Redefines", pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_First_Nme);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_First_Nme_1_5 = pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_First_NmeRedef20.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_First_Nme_1_5", 
            "#AP-ADD-COR-FIRST-NME-1-5", FieldType.STRING, 5);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Mddle_Nme = pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Appl_Or_Prem.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Mddle_Nme", 
            "#AP-ADD-COR-MDDLE-NME", FieldType.STRING, 30);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Mddle_NmeRedef21 = pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Appl_Or_Prem.newGroupInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Mddle_NmeRedef21", 
            "Redefines", pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Mddle_Nme);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Mddle_Nme_1_1 = pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Mddle_NmeRedef21.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Cor_Mddle_Nme_1_1", 
            "#AP-ADD-COR-MDDLE-NME-1-1", FieldType.STRING, 1);
        pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Prem = pnd_Ap_Add_Work_File.newGroupInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Prem", "#AP-FIELDS-FROM-PREM");
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Ph_Rltnshp_Cde = pnd_Ap_Add_Work_File_Pnd_Ap_Fields_From_Prem.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Ph_Rltnshp_Cde", 
            "#AP-ADD-PH-RLTNSHP-CDE", FieldType.STRING, 1);
        pnd_Ap_Add_Work_File_Pnd_Ap_Misc = pnd_Ap_Add_Work_File.newGroupInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Misc", "#AP-MISC");
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Match_Ind = pnd_Ap_Add_Work_File_Pnd_Ap_Misc.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Match_Ind", "#AP-ADD-MATCH-IND", 
            FieldType.STRING, 2);
        pnd_Ap_Add_Work_File_Pnd_Ap_Filler_2 = pnd_Ap_Add_Work_File.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Filler_2", "#AP-FILLER-2", FieldType.STRING, 
            94);
        pnd_Ap_Add_Work_FileRedef22 = newGroupInRecord("pnd_Ap_Add_Work_FileRedef22", "Redefines", pnd_Ap_Add_Work_File);
        pnd_Ap_Add_Work_File_Pnd_Ap_Add_Data_1 = pnd_Ap_Add_Work_FileRedef22.newFieldInGroup("pnd_Ap_Add_Work_File_Pnd_Ap_Add_Data_1", "#AP-ADD-DATA-1", 
            FieldType.STRING, 250);

        pnd_Ap_Key = newGroupInRecord("pnd_Ap_Key", "#AP-KEY");
        pnd_Ap_Key_Pnd_Ap_Key_Region_Code = pnd_Ap_Key.newFieldInGroup("pnd_Ap_Key_Pnd_Ap_Key_Region_Code", "#AP-KEY-REGION-CODE", FieldType.STRING, 3);
        pnd_Ap_Key_Pnd_Ap_Key_Region_CodeRedef23 = pnd_Ap_Key.newGroupInGroup("pnd_Ap_Key_Pnd_Ap_Key_Region_CodeRedef23", "Redefines", pnd_Ap_Key_Pnd_Ap_Key_Region_Code);
        pnd_Ap_Key_Pnd_Ap_Key_Region_Code_1_1 = pnd_Ap_Key_Pnd_Ap_Key_Region_CodeRedef23.newFieldInGroup("pnd_Ap_Key_Pnd_Ap_Key_Region_Code_1_1", "#AP-KEY-REGION-CODE-1-1", 
            FieldType.STRING, 1);
        pnd_Ap_Key_Pnd_Ap_Key_Region_Code_2_2 = pnd_Ap_Key_Pnd_Ap_Key_Region_CodeRedef23.newFieldInGroup("pnd_Ap_Key_Pnd_Ap_Key_Region_Code_2_2", "#AP-KEY-REGION-CODE-2-2", 
            FieldType.STRING, 1);
        pnd_Ap_Key_Pnd_Ap_Key_Region_Code_3_3 = pnd_Ap_Key_Pnd_Ap_Key_Region_CodeRedef23.newFieldInGroup("pnd_Ap_Key_Pnd_Ap_Key_Region_Code_3_3", "#AP-KEY-REGION-CODE-3-3", 
            FieldType.STRING, 1);
        pnd_Ap_Key_Pnd_Ap_Key_Ppg_Team_Cde = pnd_Ap_Key.newFieldInGroup("pnd_Ap_Key_Pnd_Ap_Key_Ppg_Team_Cde", "#AP-KEY-PPG-TEAM-CDE", FieldType.STRING, 
            8);
        pnd_Ap_Key_Pnd_Ap_Key_Wpid = pnd_Ap_Key.newFieldArrayInGroup("pnd_Ap_Key_Pnd_Ap_Key_Wpid", "#AP-KEY-WPID", FieldType.STRING, 6, new DbsArrayController(1,
            2));
        pnd_Ap_Key_Pnd_Ap_Key_Coll_Code = pnd_Ap_Key.newFieldInGroup("pnd_Ap_Key_Pnd_Ap_Key_Coll_Code", "#AP-KEY-COLL-CODE", FieldType.STRING, 6);
        pnd_Ap_Key_Pnd_Ap_Key_Soc_Sec = pnd_Ap_Key.newFieldInGroup("pnd_Ap_Key_Pnd_Ap_Key_Soc_Sec", "#AP-KEY-SOC-SEC", FieldType.NUMERIC, 9);
        pnd_Ap_Key_Pnd_Ap_Fields_From_Appl_Or_Prem_2 = pnd_Ap_Key.newGroupInGroup("pnd_Ap_Key_Pnd_Ap_Fields_From_Appl_Or_Prem_2", "#AP-FIELDS-FROM-APPL-OR-PREM-2");
        pnd_Ap_Key_Pnd_Ap_Add_Mit_Unit = pnd_Ap_Key_Pnd_Ap_Fields_From_Appl_Or_Prem_2.newFieldInGroup("pnd_Ap_Key_Pnd_Ap_Add_Mit_Unit", "#AP-ADD-MIT-UNIT", 
            FieldType.STRING, 8);
        pnd_Ap_Key_Pnd_Ap_Add_Mit_Wpid = pnd_Ap_Key_Pnd_Ap_Fields_From_Appl_Or_Prem_2.newFieldInGroup("pnd_Ap_Key_Pnd_Ap_Add_Mit_Wpid", "#AP-ADD-MIT-WPID", 
            FieldType.STRING, 6);
        pnd_Ap_Key_Pnd_Ap_Add_Prap_Rsch_Mit_Ind = pnd_Ap_Key_Pnd_Ap_Fields_From_Appl_Or_Prem_2.newFieldInGroup("pnd_Ap_Key_Pnd_Ap_Add_Prap_Rsch_Mit_Ind", 
            "#AP-ADD-PRAP-RSCH-MIT-IND", FieldType.STRING, 1);
        pnd_Ap_Key_Pnd_Ap_Ap_Prap_Prem_Rsch_Ind = pnd_Ap_Key_Pnd_Ap_Fields_From_Appl_Or_Prem_2.newFieldInGroup("pnd_Ap_Key_Pnd_Ap_Ap_Prap_Prem_Rsch_Ind", 
            "#AP-AP-PRAP-PREM-RSCH-IND", FieldType.STRING, 1);

        this.setRecordName("LdaAppl3100");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAppl3100() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
