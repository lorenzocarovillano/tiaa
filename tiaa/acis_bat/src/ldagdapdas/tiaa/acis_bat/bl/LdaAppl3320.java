/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:51:16 PM
**        * FROM NATURAL LDA     : APPL3320
************************************************************
**        * FILE NAME            : LdaAppl3320.java
**        * CLASS NAME           : LdaAppl3320
**        * INSTANCE NAME        : LdaAppl3320
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl3320 extends DbsRecord
{
    // Properties
    private DbsGroup sgrd_Ext_File;
    private DbsField sgrd_Ext_File_Ex_Sgrd_Plan_No;
    private DbsField sgrd_Ext_File_Ex_Sgrd_Subplan_No;
    private DbsField sgrd_Ext_File_Ex_Sgrd_Part_Ext;
    private DbsField sgrd_Ext_File_Ex_Sgrd_Divsub;
    private DbsField sgrd_Ext_File_Ex_Cor_Last_Name;
    private DbsField sgrd_Ext_File_Ex_Cor_First_Name;
    private DbsField sgrd_Ext_File_Ex_Soc_Sec;
    private DbsField sgrd_Ext_File_Ex_Tiaa_Cntrct;
    private DbsField sgrd_Ext_File_Ex_Cref_Cert;
    private DbsField sgrd_Ext_File_Ex_Day_Diff;
    private DbsField sgrd_Ext_File_Ex_Pin_Nbr;
    private DbsField sgrd_Ext_File_Ex_Lob;
    private DbsField sgrd_Ext_File_Ex_Lob_Type;
    private DbsField sgrd_Ext_File_Ex_Dob;
    private DbsField sgrd_Ext_File_Ex_Applcnt_Req_Type;
    private DbsField sgrd_Ext_File_Ex_Filler;

    public DbsGroup getSgrd_Ext_File() { return sgrd_Ext_File; }

    public DbsField getSgrd_Ext_File_Ex_Sgrd_Plan_No() { return sgrd_Ext_File_Ex_Sgrd_Plan_No; }

    public DbsField getSgrd_Ext_File_Ex_Sgrd_Subplan_No() { return sgrd_Ext_File_Ex_Sgrd_Subplan_No; }

    public DbsField getSgrd_Ext_File_Ex_Sgrd_Part_Ext() { return sgrd_Ext_File_Ex_Sgrd_Part_Ext; }

    public DbsField getSgrd_Ext_File_Ex_Sgrd_Divsub() { return sgrd_Ext_File_Ex_Sgrd_Divsub; }

    public DbsField getSgrd_Ext_File_Ex_Cor_Last_Name() { return sgrd_Ext_File_Ex_Cor_Last_Name; }

    public DbsField getSgrd_Ext_File_Ex_Cor_First_Name() { return sgrd_Ext_File_Ex_Cor_First_Name; }

    public DbsField getSgrd_Ext_File_Ex_Soc_Sec() { return sgrd_Ext_File_Ex_Soc_Sec; }

    public DbsField getSgrd_Ext_File_Ex_Tiaa_Cntrct() { return sgrd_Ext_File_Ex_Tiaa_Cntrct; }

    public DbsField getSgrd_Ext_File_Ex_Cref_Cert() { return sgrd_Ext_File_Ex_Cref_Cert; }

    public DbsField getSgrd_Ext_File_Ex_Day_Diff() { return sgrd_Ext_File_Ex_Day_Diff; }

    public DbsField getSgrd_Ext_File_Ex_Pin_Nbr() { return sgrd_Ext_File_Ex_Pin_Nbr; }

    public DbsField getSgrd_Ext_File_Ex_Lob() { return sgrd_Ext_File_Ex_Lob; }

    public DbsField getSgrd_Ext_File_Ex_Lob_Type() { return sgrd_Ext_File_Ex_Lob_Type; }

    public DbsField getSgrd_Ext_File_Ex_Dob() { return sgrd_Ext_File_Ex_Dob; }

    public DbsField getSgrd_Ext_File_Ex_Applcnt_Req_Type() { return sgrd_Ext_File_Ex_Applcnt_Req_Type; }

    public DbsField getSgrd_Ext_File_Ex_Filler() { return sgrd_Ext_File_Ex_Filler; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        sgrd_Ext_File = newGroupInRecord("sgrd_Ext_File", "SGRD-EXT-FILE");
        sgrd_Ext_File_Ex_Sgrd_Plan_No = sgrd_Ext_File.newFieldInGroup("sgrd_Ext_File_Ex_Sgrd_Plan_No", "EX-SGRD-PLAN-NO", FieldType.STRING, 6);
        sgrd_Ext_File_Ex_Sgrd_Subplan_No = sgrd_Ext_File.newFieldInGroup("sgrd_Ext_File_Ex_Sgrd_Subplan_No", "EX-SGRD-SUBPLAN-NO", FieldType.STRING, 6);
        sgrd_Ext_File_Ex_Sgrd_Part_Ext = sgrd_Ext_File.newFieldInGroup("sgrd_Ext_File_Ex_Sgrd_Part_Ext", "EX-SGRD-PART-EXT", FieldType.STRING, 2);
        sgrd_Ext_File_Ex_Sgrd_Divsub = sgrd_Ext_File.newFieldInGroup("sgrd_Ext_File_Ex_Sgrd_Divsub", "EX-SGRD-DIVSUB", FieldType.STRING, 4);
        sgrd_Ext_File_Ex_Cor_Last_Name = sgrd_Ext_File.newFieldInGroup("sgrd_Ext_File_Ex_Cor_Last_Name", "EX-COR-LAST-NAME", FieldType.STRING, 30);
        sgrd_Ext_File_Ex_Cor_First_Name = sgrd_Ext_File.newFieldInGroup("sgrd_Ext_File_Ex_Cor_First_Name", "EX-COR-FIRST-NAME", FieldType.STRING, 30);
        sgrd_Ext_File_Ex_Soc_Sec = sgrd_Ext_File.newFieldInGroup("sgrd_Ext_File_Ex_Soc_Sec", "EX-SOC-SEC", FieldType.STRING, 9);
        sgrd_Ext_File_Ex_Tiaa_Cntrct = sgrd_Ext_File.newFieldInGroup("sgrd_Ext_File_Ex_Tiaa_Cntrct", "EX-TIAA-CNTRCT", FieldType.STRING, 10);
        sgrd_Ext_File_Ex_Cref_Cert = sgrd_Ext_File.newFieldInGroup("sgrd_Ext_File_Ex_Cref_Cert", "EX-CREF-CERT", FieldType.STRING, 10);
        sgrd_Ext_File_Ex_Day_Diff = sgrd_Ext_File.newFieldInGroup("sgrd_Ext_File_Ex_Day_Diff", "EX-DAY-DIFF", FieldType.NUMERIC, 6);
        sgrd_Ext_File_Ex_Pin_Nbr = sgrd_Ext_File.newFieldInGroup("sgrd_Ext_File_Ex_Pin_Nbr", "EX-PIN-NBR", FieldType.NUMERIC, 12);
        sgrd_Ext_File_Ex_Lob = sgrd_Ext_File.newFieldInGroup("sgrd_Ext_File_Ex_Lob", "EX-LOB", FieldType.STRING, 1);
        sgrd_Ext_File_Ex_Lob_Type = sgrd_Ext_File.newFieldInGroup("sgrd_Ext_File_Ex_Lob_Type", "EX-LOB-TYPE", FieldType.STRING, 1);
        sgrd_Ext_File_Ex_Dob = sgrd_Ext_File.newFieldInGroup("sgrd_Ext_File_Ex_Dob", "EX-DOB", FieldType.PACKED_DECIMAL, 8);
        sgrd_Ext_File_Ex_Applcnt_Req_Type = sgrd_Ext_File.newFieldInGroup("sgrd_Ext_File_Ex_Applcnt_Req_Type", "EX-APPLCNT-REQ-TYPE", FieldType.STRING, 
            1);
        sgrd_Ext_File_Ex_Filler = sgrd_Ext_File.newFieldInGroup("sgrd_Ext_File_Ex_Filler", "EX-FILLER", FieldType.STRING, 67);

        this.setRecordName("LdaAppl3320");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAppl3320() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
