/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:51:04 PM
**        * FROM NATURAL LDA     : APPL301
************************************************************
**        * FILE NAME            : LdaAppl301.java
**        * CLASS NAME           : LdaAppl301
**        * INSTANCE NAME        : LdaAppl301
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppl301 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_prap;
    private DbsField prap_Ap_Coll_Code;
    private DbsField prap_Ap_G_Key;
    private DbsField prap_Ap_G_Ind;
    private DbsField prap_Ap_Soc_Sec;
    private DbsField prap_Ap_Dob;
    private DbsField prap_Ap_Lob;
    private DbsField prap_Ap_Bill_Code;
    private DbsGroup prap_Ap_Tiaa_Contr;
    private DbsField prap_App_Pref;
    private DbsField prap_App_Cont;
    private DbsGroup prap_Ap_Cref_Contr;
    private DbsField prap_App_C_Pref;
    private DbsField prap_App_C_Cont;
    private DbsField prap_Ap_T_Doi;
    private DbsField prap_Ap_C_Doi;
    private DbsField prap_Ap_Curr;
    private DbsField prap_Ap_T_Age_1st;
    private DbsField prap_Ap_C_Age_1st;
    private DbsField prap_Ap_Ownership;
    private DbsField prap_Ap_Sex;
    private DbsField prap_Ap_Mail_Zip;
    private DbsField prap_Ap_Name_Addr_Cd;
    private DbsField prap_Ap_Dt_Ent_Sys;
    private DbsField prap_Ap_Dt_Released;
    private DbsField prap_Ap_Dt_Matched;
    private DbsField prap_Ap_Dt_Deleted;
    private DbsField prap_Ap_Dt_Withdrawn;
    private DbsField prap_Ap_Alloc_Discr;
    private DbsField prap_Ap_Release_Ind;
    private DbsField prap_Ap_Type;
    private DbsField prap_Ap_Other_Pols;
    private DbsField prap_Ap_Record_Type;
    private DbsField prap_Ap_Status;
    private DbsField prap_Ap_Numb_Dailys;
    private DbsField prap_Ap_Dt_App_Recvd;
    private DbsGroup prap_Ap_Allocation_Info;
    private DbsField prap_Ap_Allocation;
    private DbsField prap_Ap_App_Source;
    private DbsField prap_Ap_Region_Code;
    private DbsField prap_Ap_Orig_Issue_State;
    private DbsField prap_Ap_Ownership_Type;
    private DbsField prap_Ap_Lob_Type;
    private DbsField prap_Ap_Split_Code;
    private DbsGroup prap_Ap_Address_Info;
    private DbsField prap_Ap_Address_Line;
    private DbsField prap_Ap_City;
    private DbsField prap_Ap_Current_State_Code;
    private DbsField prap_Ap_Dana_Transaction_Cd;
    private DbsField prap_Ap_Dana_Stndrd_Rtn_Cd;
    private DbsField prap_Ap_Dana_Finalist_Reason_Cd;
    private DbsField prap_Ap_Dana_Addr_Stndrd_Code;
    private DbsField prap_Ap_Dana_Stndrd_Overide;
    private DbsField prap_Ap_Dana_Postal_Data_Fields;
    private DbsField prap_Ap_Dana_Addr_Geographic_Code;
    private DbsField prap_Ap_Annuity_Start_Date;
    private DbsGroup prap_Ap_Maximum_Alloc_Pct;
    private DbsField prap_Ap_Max_Alloc_Pct;
    private DbsField prap_Ap_Max_Alloc_Pct_Sign;
    private DbsField prap_Ap_Bene_Info_Type;
    private DbsField prap_Ap_Primary_Std_Ent;
    private DbsGroup prap_Ap_Bene_Primary_Info;
    private DbsField prap_Ap_Primary_Bene_Info;
    private DbsField prap_Ap_Contingent_Std_Ent;
    private DbsGroup prap_Ap_Bene_Contingent_Info;
    private DbsField prap_Ap_Contingent_Bene_Info;
    private DbsField prap_Ap_Bene_Estate;
    private DbsField prap_Ap_Bene_Trust;
    private DbsField prap_Ap_Bene_Category;
    private DbsField prap_Ap_Mail_Instructions;
    private DbsField prap_Ap_Eop_Addl_Cref_Request;
    private DbsField prap_Ap_Process_Status;
    private DbsField prap_Ap_Coll_St_Cd;
    private DbsField prap_Ap_Csm_Sec_Seg;
    private DbsField prap_Ap_Rlc_College;
    private DbsField prap_Ap_Rlc_Cref_Pref;
    private DbsField prap_Ap_Rlc_Cref_Cont;
    private DbsField prap_Ap_Cor_Prfx_Nme;
    private DbsField prap_Ap_Cor_Last_Nme;
    private DbsField prap_Ap_Cor_First_Nme;
    private DbsField prap_Ap_Cor_Mddle_Nme;
    private DbsField prap_Ap_Cor_Sffx_Nme;
    private DbsField prap_Ap_Ph_Hist_Ind;
    private DbsField prap_Ap_Rcrd_Updt_Tm_Stamp;
    private DbsField prap_Ap_Contact_Mode;
    private DbsField prap_Ap_Racf_Id;
    private DbsField prap_Ap_Pin_Nbr;
    private DbsGroup prap_Ap_Rqst_Log_Dte_Tm;
    private DbsField prap_Ap_Rqst_Log_Dte_Time;
    private DbsField prap_Ap_Sync_Ind;
    private DbsField prap_Ap_Cor_Ind;
    private DbsField prap_Ap_Alloc_Ind;
    private DbsField prap_Ap_Tiaa_Doi;
    private DbsField prap_Ap_Cref_Doi;
    private DbsGroup prap_Ap_Mit_Request;
    private DbsField prap_Ap_Mit_Unit;
    private DbsField prap_Ap_Mit_Wpid;
    private DbsField prap_Ap_Ira_Rollover_Type;
    private DbsField prap_Ap_Ira_Record_Type;
    private DbsField prap_Ap_Mult_App_Status;
    private DbsField prap_Ap_Mult_App_Lob;
    private DbsField prap_Ap_Mult_App_Lob_Type;
    private DbsField prap_Ap_Mult_App_Ppg;
    private DbsField prap_Ap_Print_Date;
    private DbsGroup prap_Ap_Cntrct_Info;
    private DbsField prap_Ap_Cntrct_Type;
    private DbsField prap_Ap_Cntrct_Nbr;
    private DbsField prap_Ap_Cntrct_Proceeds_Amt;
    private DbsGroup prap_Ap_Addr_Info;
    private DbsField prap_Ap_Address_Txt;
    private DbsField prap_Ap_Address_Dest_Name;
    private DbsField prap_Ap_Zip_Code;
    private DbsField prap_Ap_Bank_Pymnt_Acct_Nmbr;
    private DbsField prap_Ap_Bank_Aba_Acct_Nmbr;
    private DbsField prap_Ap_Addr_Usage_Code;
    private DbsField prap_Ap_Init_Paymt_Amt;
    private DbsField prap_Ap_Init_Paymt_Pct;
    private DbsField prap_Ap_Financial_1;
    private DbsField prap_Ap_Financial_2;
    private DbsField prap_Ap_Financial_3;
    private DbsField prap_Ap_Financial_4;
    private DbsField prap_Ap_Financial_5;
    private DbsField prap_Ap_Irc_Sectn_Grp_Cde;
    private DbsField prap_Ap_Irc_Sectn_Cde;
    private DbsField prap_Ap_Inst_Link_Cde;
    private DbsField prap_Ap_Applcnt_Req_Type;
    private DbsField prap_Ap_Addr_Sync_Ind;
    private DbsField prap_Ap_Tiaa_Service_Agent;
    private DbsField prap_Ap_Prap_Rsch_Mit_Ind;
    private DbsField prap_Ap_Ppg_Team_Cde;
    private DbsField prap_Ap_Tiaa_Cntrct;
    private DbsField prap_Ap_Cref_Cert;
    private DbsField prap_Ap_Rlc_Cref_Cert;
    private DbsField prap_Ap_Allocation_Model_Type;
    private DbsField prap_Ap_Divorce_Ind;
    private DbsField prap_Ap_Email_Address;
    private DbsField prap_Ap_E_Signed_Appl_Ind;
    private DbsField prap_Ap_Eft_Requested_Ind;
    private DbsField prap_Ap_Prap_Prem_Rsch_Ind;
    private DbsField prap_Ap_Address_Change_Ind;
    private DbsField prap_Ap_Allocation_Fmt;
    private DbsField prap_Ap_Phone_No;
    private DbsGroup prap_Ap_Fund_Identifier;
    private DbsField prap_Ap_Fund_Cde;
    private DbsField prap_Ap_Allocation_Pct;
    private DbsField prap_Ap_Sgrd_Plan_No;
    private DbsField prap_Ap_Sgrd_Subplan_No;
    private DbsField prap_Ap_Sgrd_Part_Ext;
    private DbsField prap_Ap_Sgrd_Divsub;
    private DbsField prap_Ap_Text_Udf_1;
    private DbsField prap_Ap_Text_Udf_2;
    private DbsField prap_Ap_Text_Udf_3;
    private DbsField prap_Ap_Replacement_Ind;
    private DbsField prap_Ap_Register_Id;
    private DbsField prap_Ap_Agent_Or_Racf_Id;
    private DbsField prap_Ap_Exempt_Ind;
    private DbsField prap_Ap_Arr_Insurer_1;
    private DbsField prap_Ap_Arr_Insurer_2;
    private DbsField prap_Ap_Arr_Insurer_3;
    private DbsField prap_Ap_Arr_1035_Exch_Ind_1;
    private DbsField prap_Ap_Arr_1035_Exch_Ind_2;
    private DbsField prap_Ap_Arr_1035_Exch_Ind_3;
    private DbsField prap_Ap_Autosave_Ind;
    private DbsField prap_Ap_As_Cur_Dflt_Opt;
    private DbsField prap_Ap_As_Cur_Dflt_Amt;
    private DbsField prap_Ap_As_Incr_Opt;
    private DbsField prap_Ap_As_Incr_Amt;
    private DbsField prap_Ap_As_Max_Pct;
    private DbsField prap_Ap_Ae_Opt_Out_Days;
    private DbsField prap_Ap_Incmpl_Acct_Ind;
    private DbsField prap_Ap_Delete_User_Id;
    private DbsField prap_Ap_Delete_Reason_Cd;
    private DbsField prap_Ap_Consent_Email_Address;
    private DbsField prap_Ap_Welc_E_Delivery_Ind;
    private DbsField prap_Ap_Legal_E_Delivery_Ind;
    private DbsField prap_Ap_Legal_Ann_Option;
    private DbsField prap_Ap_Orchestration_Id;
    private DbsField prap_Ap_Mail_Addr_Country_Cd;
    private DbsField prap_Ap_Tic_Startdate;
    private DbsField prap_Ap_Tic_Enddate;
    private DbsField prap_Ap_Tic_Percentage;
    private DbsField prap_Ap_Tic_Postdays;
    private DbsField prap_Ap_Tic_Limit;
    private DbsField prap_Ap_Tic_Postfreq;
    private DbsField prap_Ap_Tic_Pl_Level;
    private DbsField prap_Ap_Tic_Windowdays;
    private DbsField prap_Ap_Tic_Reqdlywindow;
    private DbsField prap_Ap_Tic_Recap_Prov;
    private DbsField prap_Ap_Ann_Funding_Dt;
    private DbsField prap_Ap_Tiaa_Ann_Issue_Dt;
    private DbsField prap_Ap_Cref_Ann_Issue_Dt;
    private DbsField prap_Ap_Substitution_Contract_Ind;
    private DbsField prap_Ap_Conv_Issue_State;
    private DbsField prap_Ap_Deceased_Ind;
    private DbsField prap_Ap_Non_Proprietary_Pkg_Ind;
    private DbsField prap_Ap_Decedent_Contract;
    private DbsField prap_Ap_Relation_To_Decedent;
    private DbsField prap_Ap_Ssn_Tin_Ind;
    private DbsField prap_Ap_Oneira_Acct_No;
    private DbsField prap_Ap_Fund_Source_Cde_1;
    private DbsField prap_Ap_Fund_Source_Cde_2;
    private DbsGroup prap_Ap_Fund_Identifier_2;
    private DbsField prap_Ap_Fund_Cde_2;
    private DbsField prap_Ap_Allocation_Pct_2;

    public DataAccessProgramView getVw_prap() { return vw_prap; }

    public DbsField getPrap_Ap_Coll_Code() { return prap_Ap_Coll_Code; }

    public DbsField getPrap_Ap_G_Key() { return prap_Ap_G_Key; }

    public DbsField getPrap_Ap_G_Ind() { return prap_Ap_G_Ind; }

    public DbsField getPrap_Ap_Soc_Sec() { return prap_Ap_Soc_Sec; }

    public DbsField getPrap_Ap_Dob() { return prap_Ap_Dob; }

    public DbsField getPrap_Ap_Lob() { return prap_Ap_Lob; }

    public DbsField getPrap_Ap_Bill_Code() { return prap_Ap_Bill_Code; }

    public DbsGroup getPrap_Ap_Tiaa_Contr() { return prap_Ap_Tiaa_Contr; }

    public DbsField getPrap_App_Pref() { return prap_App_Pref; }

    public DbsField getPrap_App_Cont() { return prap_App_Cont; }

    public DbsGroup getPrap_Ap_Cref_Contr() { return prap_Ap_Cref_Contr; }

    public DbsField getPrap_App_C_Pref() { return prap_App_C_Pref; }

    public DbsField getPrap_App_C_Cont() { return prap_App_C_Cont; }

    public DbsField getPrap_Ap_T_Doi() { return prap_Ap_T_Doi; }

    public DbsField getPrap_Ap_C_Doi() { return prap_Ap_C_Doi; }

    public DbsField getPrap_Ap_Curr() { return prap_Ap_Curr; }

    public DbsField getPrap_Ap_T_Age_1st() { return prap_Ap_T_Age_1st; }

    public DbsField getPrap_Ap_C_Age_1st() { return prap_Ap_C_Age_1st; }

    public DbsField getPrap_Ap_Ownership() { return prap_Ap_Ownership; }

    public DbsField getPrap_Ap_Sex() { return prap_Ap_Sex; }

    public DbsField getPrap_Ap_Mail_Zip() { return prap_Ap_Mail_Zip; }

    public DbsField getPrap_Ap_Name_Addr_Cd() { return prap_Ap_Name_Addr_Cd; }

    public DbsField getPrap_Ap_Dt_Ent_Sys() { return prap_Ap_Dt_Ent_Sys; }

    public DbsField getPrap_Ap_Dt_Released() { return prap_Ap_Dt_Released; }

    public DbsField getPrap_Ap_Dt_Matched() { return prap_Ap_Dt_Matched; }

    public DbsField getPrap_Ap_Dt_Deleted() { return prap_Ap_Dt_Deleted; }

    public DbsField getPrap_Ap_Dt_Withdrawn() { return prap_Ap_Dt_Withdrawn; }

    public DbsField getPrap_Ap_Alloc_Discr() { return prap_Ap_Alloc_Discr; }

    public DbsField getPrap_Ap_Release_Ind() { return prap_Ap_Release_Ind; }

    public DbsField getPrap_Ap_Type() { return prap_Ap_Type; }

    public DbsField getPrap_Ap_Other_Pols() { return prap_Ap_Other_Pols; }

    public DbsField getPrap_Ap_Record_Type() { return prap_Ap_Record_Type; }

    public DbsField getPrap_Ap_Status() { return prap_Ap_Status; }

    public DbsField getPrap_Ap_Numb_Dailys() { return prap_Ap_Numb_Dailys; }

    public DbsField getPrap_Ap_Dt_App_Recvd() { return prap_Ap_Dt_App_Recvd; }

    public DbsGroup getPrap_Ap_Allocation_Info() { return prap_Ap_Allocation_Info; }

    public DbsField getPrap_Ap_Allocation() { return prap_Ap_Allocation; }

    public DbsField getPrap_Ap_App_Source() { return prap_Ap_App_Source; }

    public DbsField getPrap_Ap_Region_Code() { return prap_Ap_Region_Code; }

    public DbsField getPrap_Ap_Orig_Issue_State() { return prap_Ap_Orig_Issue_State; }

    public DbsField getPrap_Ap_Ownership_Type() { return prap_Ap_Ownership_Type; }

    public DbsField getPrap_Ap_Lob_Type() { return prap_Ap_Lob_Type; }

    public DbsField getPrap_Ap_Split_Code() { return prap_Ap_Split_Code; }

    public DbsGroup getPrap_Ap_Address_Info() { return prap_Ap_Address_Info; }

    public DbsField getPrap_Ap_Address_Line() { return prap_Ap_Address_Line; }

    public DbsField getPrap_Ap_City() { return prap_Ap_City; }

    public DbsField getPrap_Ap_Current_State_Code() { return prap_Ap_Current_State_Code; }

    public DbsField getPrap_Ap_Dana_Transaction_Cd() { return prap_Ap_Dana_Transaction_Cd; }

    public DbsField getPrap_Ap_Dana_Stndrd_Rtn_Cd() { return prap_Ap_Dana_Stndrd_Rtn_Cd; }

    public DbsField getPrap_Ap_Dana_Finalist_Reason_Cd() { return prap_Ap_Dana_Finalist_Reason_Cd; }

    public DbsField getPrap_Ap_Dana_Addr_Stndrd_Code() { return prap_Ap_Dana_Addr_Stndrd_Code; }

    public DbsField getPrap_Ap_Dana_Stndrd_Overide() { return prap_Ap_Dana_Stndrd_Overide; }

    public DbsField getPrap_Ap_Dana_Postal_Data_Fields() { return prap_Ap_Dana_Postal_Data_Fields; }

    public DbsField getPrap_Ap_Dana_Addr_Geographic_Code() { return prap_Ap_Dana_Addr_Geographic_Code; }

    public DbsField getPrap_Ap_Annuity_Start_Date() { return prap_Ap_Annuity_Start_Date; }

    public DbsGroup getPrap_Ap_Maximum_Alloc_Pct() { return prap_Ap_Maximum_Alloc_Pct; }

    public DbsField getPrap_Ap_Max_Alloc_Pct() { return prap_Ap_Max_Alloc_Pct; }

    public DbsField getPrap_Ap_Max_Alloc_Pct_Sign() { return prap_Ap_Max_Alloc_Pct_Sign; }

    public DbsField getPrap_Ap_Bene_Info_Type() { return prap_Ap_Bene_Info_Type; }

    public DbsField getPrap_Ap_Primary_Std_Ent() { return prap_Ap_Primary_Std_Ent; }

    public DbsGroup getPrap_Ap_Bene_Primary_Info() { return prap_Ap_Bene_Primary_Info; }

    public DbsField getPrap_Ap_Primary_Bene_Info() { return prap_Ap_Primary_Bene_Info; }

    public DbsField getPrap_Ap_Contingent_Std_Ent() { return prap_Ap_Contingent_Std_Ent; }

    public DbsGroup getPrap_Ap_Bene_Contingent_Info() { return prap_Ap_Bene_Contingent_Info; }

    public DbsField getPrap_Ap_Contingent_Bene_Info() { return prap_Ap_Contingent_Bene_Info; }

    public DbsField getPrap_Ap_Bene_Estate() { return prap_Ap_Bene_Estate; }

    public DbsField getPrap_Ap_Bene_Trust() { return prap_Ap_Bene_Trust; }

    public DbsField getPrap_Ap_Bene_Category() { return prap_Ap_Bene_Category; }

    public DbsField getPrap_Ap_Mail_Instructions() { return prap_Ap_Mail_Instructions; }

    public DbsField getPrap_Ap_Eop_Addl_Cref_Request() { return prap_Ap_Eop_Addl_Cref_Request; }

    public DbsField getPrap_Ap_Process_Status() { return prap_Ap_Process_Status; }

    public DbsField getPrap_Ap_Coll_St_Cd() { return prap_Ap_Coll_St_Cd; }

    public DbsField getPrap_Ap_Csm_Sec_Seg() { return prap_Ap_Csm_Sec_Seg; }

    public DbsField getPrap_Ap_Rlc_College() { return prap_Ap_Rlc_College; }

    public DbsField getPrap_Ap_Rlc_Cref_Pref() { return prap_Ap_Rlc_Cref_Pref; }

    public DbsField getPrap_Ap_Rlc_Cref_Cont() { return prap_Ap_Rlc_Cref_Cont; }

    public DbsField getPrap_Ap_Cor_Prfx_Nme() { return prap_Ap_Cor_Prfx_Nme; }

    public DbsField getPrap_Ap_Cor_Last_Nme() { return prap_Ap_Cor_Last_Nme; }

    public DbsField getPrap_Ap_Cor_First_Nme() { return prap_Ap_Cor_First_Nme; }

    public DbsField getPrap_Ap_Cor_Mddle_Nme() { return prap_Ap_Cor_Mddle_Nme; }

    public DbsField getPrap_Ap_Cor_Sffx_Nme() { return prap_Ap_Cor_Sffx_Nme; }

    public DbsField getPrap_Ap_Ph_Hist_Ind() { return prap_Ap_Ph_Hist_Ind; }

    public DbsField getPrap_Ap_Rcrd_Updt_Tm_Stamp() { return prap_Ap_Rcrd_Updt_Tm_Stamp; }

    public DbsField getPrap_Ap_Contact_Mode() { return prap_Ap_Contact_Mode; }

    public DbsField getPrap_Ap_Racf_Id() { return prap_Ap_Racf_Id; }

    public DbsField getPrap_Ap_Pin_Nbr() { return prap_Ap_Pin_Nbr; }

    public DbsGroup getPrap_Ap_Rqst_Log_Dte_Tm() { return prap_Ap_Rqst_Log_Dte_Tm; }

    public DbsField getPrap_Ap_Rqst_Log_Dte_Time() { return prap_Ap_Rqst_Log_Dte_Time; }

    public DbsField getPrap_Ap_Sync_Ind() { return prap_Ap_Sync_Ind; }

    public DbsField getPrap_Ap_Cor_Ind() { return prap_Ap_Cor_Ind; }

    public DbsField getPrap_Ap_Alloc_Ind() { return prap_Ap_Alloc_Ind; }

    public DbsField getPrap_Ap_Tiaa_Doi() { return prap_Ap_Tiaa_Doi; }

    public DbsField getPrap_Ap_Cref_Doi() { return prap_Ap_Cref_Doi; }

    public DbsGroup getPrap_Ap_Mit_Request() { return prap_Ap_Mit_Request; }

    public DbsField getPrap_Ap_Mit_Unit() { return prap_Ap_Mit_Unit; }

    public DbsField getPrap_Ap_Mit_Wpid() { return prap_Ap_Mit_Wpid; }

    public DbsField getPrap_Ap_Ira_Rollover_Type() { return prap_Ap_Ira_Rollover_Type; }

    public DbsField getPrap_Ap_Ira_Record_Type() { return prap_Ap_Ira_Record_Type; }

    public DbsField getPrap_Ap_Mult_App_Status() { return prap_Ap_Mult_App_Status; }

    public DbsField getPrap_Ap_Mult_App_Lob() { return prap_Ap_Mult_App_Lob; }

    public DbsField getPrap_Ap_Mult_App_Lob_Type() { return prap_Ap_Mult_App_Lob_Type; }

    public DbsField getPrap_Ap_Mult_App_Ppg() { return prap_Ap_Mult_App_Ppg; }

    public DbsField getPrap_Ap_Print_Date() { return prap_Ap_Print_Date; }

    public DbsGroup getPrap_Ap_Cntrct_Info() { return prap_Ap_Cntrct_Info; }

    public DbsField getPrap_Ap_Cntrct_Type() { return prap_Ap_Cntrct_Type; }

    public DbsField getPrap_Ap_Cntrct_Nbr() { return prap_Ap_Cntrct_Nbr; }

    public DbsField getPrap_Ap_Cntrct_Proceeds_Amt() { return prap_Ap_Cntrct_Proceeds_Amt; }

    public DbsGroup getPrap_Ap_Addr_Info() { return prap_Ap_Addr_Info; }

    public DbsField getPrap_Ap_Address_Txt() { return prap_Ap_Address_Txt; }

    public DbsField getPrap_Ap_Address_Dest_Name() { return prap_Ap_Address_Dest_Name; }

    public DbsField getPrap_Ap_Zip_Code() { return prap_Ap_Zip_Code; }

    public DbsField getPrap_Ap_Bank_Pymnt_Acct_Nmbr() { return prap_Ap_Bank_Pymnt_Acct_Nmbr; }

    public DbsField getPrap_Ap_Bank_Aba_Acct_Nmbr() { return prap_Ap_Bank_Aba_Acct_Nmbr; }

    public DbsField getPrap_Ap_Addr_Usage_Code() { return prap_Ap_Addr_Usage_Code; }

    public DbsField getPrap_Ap_Init_Paymt_Amt() { return prap_Ap_Init_Paymt_Amt; }

    public DbsField getPrap_Ap_Init_Paymt_Pct() { return prap_Ap_Init_Paymt_Pct; }

    public DbsField getPrap_Ap_Financial_1() { return prap_Ap_Financial_1; }

    public DbsField getPrap_Ap_Financial_2() { return prap_Ap_Financial_2; }

    public DbsField getPrap_Ap_Financial_3() { return prap_Ap_Financial_3; }

    public DbsField getPrap_Ap_Financial_4() { return prap_Ap_Financial_4; }

    public DbsField getPrap_Ap_Financial_5() { return prap_Ap_Financial_5; }

    public DbsField getPrap_Ap_Irc_Sectn_Grp_Cde() { return prap_Ap_Irc_Sectn_Grp_Cde; }

    public DbsField getPrap_Ap_Irc_Sectn_Cde() { return prap_Ap_Irc_Sectn_Cde; }

    public DbsField getPrap_Ap_Inst_Link_Cde() { return prap_Ap_Inst_Link_Cde; }

    public DbsField getPrap_Ap_Applcnt_Req_Type() { return prap_Ap_Applcnt_Req_Type; }

    public DbsField getPrap_Ap_Addr_Sync_Ind() { return prap_Ap_Addr_Sync_Ind; }

    public DbsField getPrap_Ap_Tiaa_Service_Agent() { return prap_Ap_Tiaa_Service_Agent; }

    public DbsField getPrap_Ap_Prap_Rsch_Mit_Ind() { return prap_Ap_Prap_Rsch_Mit_Ind; }

    public DbsField getPrap_Ap_Ppg_Team_Cde() { return prap_Ap_Ppg_Team_Cde; }

    public DbsField getPrap_Ap_Tiaa_Cntrct() { return prap_Ap_Tiaa_Cntrct; }

    public DbsField getPrap_Ap_Cref_Cert() { return prap_Ap_Cref_Cert; }

    public DbsField getPrap_Ap_Rlc_Cref_Cert() { return prap_Ap_Rlc_Cref_Cert; }

    public DbsField getPrap_Ap_Allocation_Model_Type() { return prap_Ap_Allocation_Model_Type; }

    public DbsField getPrap_Ap_Divorce_Ind() { return prap_Ap_Divorce_Ind; }

    public DbsField getPrap_Ap_Email_Address() { return prap_Ap_Email_Address; }

    public DbsField getPrap_Ap_E_Signed_Appl_Ind() { return prap_Ap_E_Signed_Appl_Ind; }

    public DbsField getPrap_Ap_Eft_Requested_Ind() { return prap_Ap_Eft_Requested_Ind; }

    public DbsField getPrap_Ap_Prap_Prem_Rsch_Ind() { return prap_Ap_Prap_Prem_Rsch_Ind; }

    public DbsField getPrap_Ap_Address_Change_Ind() { return prap_Ap_Address_Change_Ind; }

    public DbsField getPrap_Ap_Allocation_Fmt() { return prap_Ap_Allocation_Fmt; }

    public DbsField getPrap_Ap_Phone_No() { return prap_Ap_Phone_No; }

    public DbsGroup getPrap_Ap_Fund_Identifier() { return prap_Ap_Fund_Identifier; }

    public DbsField getPrap_Ap_Fund_Cde() { return prap_Ap_Fund_Cde; }

    public DbsField getPrap_Ap_Allocation_Pct() { return prap_Ap_Allocation_Pct; }

    public DbsField getPrap_Ap_Sgrd_Plan_No() { return prap_Ap_Sgrd_Plan_No; }

    public DbsField getPrap_Ap_Sgrd_Subplan_No() { return prap_Ap_Sgrd_Subplan_No; }

    public DbsField getPrap_Ap_Sgrd_Part_Ext() { return prap_Ap_Sgrd_Part_Ext; }

    public DbsField getPrap_Ap_Sgrd_Divsub() { return prap_Ap_Sgrd_Divsub; }

    public DbsField getPrap_Ap_Text_Udf_1() { return prap_Ap_Text_Udf_1; }

    public DbsField getPrap_Ap_Text_Udf_2() { return prap_Ap_Text_Udf_2; }

    public DbsField getPrap_Ap_Text_Udf_3() { return prap_Ap_Text_Udf_3; }

    public DbsField getPrap_Ap_Replacement_Ind() { return prap_Ap_Replacement_Ind; }

    public DbsField getPrap_Ap_Register_Id() { return prap_Ap_Register_Id; }

    public DbsField getPrap_Ap_Agent_Or_Racf_Id() { return prap_Ap_Agent_Or_Racf_Id; }

    public DbsField getPrap_Ap_Exempt_Ind() { return prap_Ap_Exempt_Ind; }

    public DbsField getPrap_Ap_Arr_Insurer_1() { return prap_Ap_Arr_Insurer_1; }

    public DbsField getPrap_Ap_Arr_Insurer_2() { return prap_Ap_Arr_Insurer_2; }

    public DbsField getPrap_Ap_Arr_Insurer_3() { return prap_Ap_Arr_Insurer_3; }

    public DbsField getPrap_Ap_Arr_1035_Exch_Ind_1() { return prap_Ap_Arr_1035_Exch_Ind_1; }

    public DbsField getPrap_Ap_Arr_1035_Exch_Ind_2() { return prap_Ap_Arr_1035_Exch_Ind_2; }

    public DbsField getPrap_Ap_Arr_1035_Exch_Ind_3() { return prap_Ap_Arr_1035_Exch_Ind_3; }

    public DbsField getPrap_Ap_Autosave_Ind() { return prap_Ap_Autosave_Ind; }

    public DbsField getPrap_Ap_As_Cur_Dflt_Opt() { return prap_Ap_As_Cur_Dflt_Opt; }

    public DbsField getPrap_Ap_As_Cur_Dflt_Amt() { return prap_Ap_As_Cur_Dflt_Amt; }

    public DbsField getPrap_Ap_As_Incr_Opt() { return prap_Ap_As_Incr_Opt; }

    public DbsField getPrap_Ap_As_Incr_Amt() { return prap_Ap_As_Incr_Amt; }

    public DbsField getPrap_Ap_As_Max_Pct() { return prap_Ap_As_Max_Pct; }

    public DbsField getPrap_Ap_Ae_Opt_Out_Days() { return prap_Ap_Ae_Opt_Out_Days; }

    public DbsField getPrap_Ap_Incmpl_Acct_Ind() { return prap_Ap_Incmpl_Acct_Ind; }

    public DbsField getPrap_Ap_Delete_User_Id() { return prap_Ap_Delete_User_Id; }

    public DbsField getPrap_Ap_Delete_Reason_Cd() { return prap_Ap_Delete_Reason_Cd; }

    public DbsField getPrap_Ap_Consent_Email_Address() { return prap_Ap_Consent_Email_Address; }

    public DbsField getPrap_Ap_Welc_E_Delivery_Ind() { return prap_Ap_Welc_E_Delivery_Ind; }

    public DbsField getPrap_Ap_Legal_E_Delivery_Ind() { return prap_Ap_Legal_E_Delivery_Ind; }

    public DbsField getPrap_Ap_Legal_Ann_Option() { return prap_Ap_Legal_Ann_Option; }

    public DbsField getPrap_Ap_Orchestration_Id() { return prap_Ap_Orchestration_Id; }

    public DbsField getPrap_Ap_Mail_Addr_Country_Cd() { return prap_Ap_Mail_Addr_Country_Cd; }

    public DbsField getPrap_Ap_Tic_Startdate() { return prap_Ap_Tic_Startdate; }

    public DbsField getPrap_Ap_Tic_Enddate() { return prap_Ap_Tic_Enddate; }

    public DbsField getPrap_Ap_Tic_Percentage() { return prap_Ap_Tic_Percentage; }

    public DbsField getPrap_Ap_Tic_Postdays() { return prap_Ap_Tic_Postdays; }

    public DbsField getPrap_Ap_Tic_Limit() { return prap_Ap_Tic_Limit; }

    public DbsField getPrap_Ap_Tic_Postfreq() { return prap_Ap_Tic_Postfreq; }

    public DbsField getPrap_Ap_Tic_Pl_Level() { return prap_Ap_Tic_Pl_Level; }

    public DbsField getPrap_Ap_Tic_Windowdays() { return prap_Ap_Tic_Windowdays; }

    public DbsField getPrap_Ap_Tic_Reqdlywindow() { return prap_Ap_Tic_Reqdlywindow; }

    public DbsField getPrap_Ap_Tic_Recap_Prov() { return prap_Ap_Tic_Recap_Prov; }

    public DbsField getPrap_Ap_Ann_Funding_Dt() { return prap_Ap_Ann_Funding_Dt; }

    public DbsField getPrap_Ap_Tiaa_Ann_Issue_Dt() { return prap_Ap_Tiaa_Ann_Issue_Dt; }

    public DbsField getPrap_Ap_Cref_Ann_Issue_Dt() { return prap_Ap_Cref_Ann_Issue_Dt; }

    public DbsField getPrap_Ap_Substitution_Contract_Ind() { return prap_Ap_Substitution_Contract_Ind; }

    public DbsField getPrap_Ap_Conv_Issue_State() { return prap_Ap_Conv_Issue_State; }

    public DbsField getPrap_Ap_Deceased_Ind() { return prap_Ap_Deceased_Ind; }

    public DbsField getPrap_Ap_Non_Proprietary_Pkg_Ind() { return prap_Ap_Non_Proprietary_Pkg_Ind; }

    public DbsField getPrap_Ap_Decedent_Contract() { return prap_Ap_Decedent_Contract; }

    public DbsField getPrap_Ap_Relation_To_Decedent() { return prap_Ap_Relation_To_Decedent; }

    public DbsField getPrap_Ap_Ssn_Tin_Ind() { return prap_Ap_Ssn_Tin_Ind; }

    public DbsField getPrap_Ap_Oneira_Acct_No() { return prap_Ap_Oneira_Acct_No; }

    public DbsField getPrap_Ap_Fund_Source_Cde_1() { return prap_Ap_Fund_Source_Cde_1; }

    public DbsField getPrap_Ap_Fund_Source_Cde_2() { return prap_Ap_Fund_Source_Cde_2; }

    public DbsGroup getPrap_Ap_Fund_Identifier_2() { return prap_Ap_Fund_Identifier_2; }

    public DbsField getPrap_Ap_Fund_Cde_2() { return prap_Ap_Fund_Cde_2; }

    public DbsField getPrap_Ap_Allocation_Pct_2() { return prap_Ap_Allocation_Pct_2; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_prap = new DataAccessProgramView(new NameInfo("vw_prap", "PRAP"), "ANNTY_ACTVTY_PRAP_12", "ACIS_ANNTY_PRAP", DdmPeriodicGroups.getInstance().getGroups("ANNTY_ACTVTY_PRAP_12"));
        prap_Ap_Coll_Code = vw_prap.getRecord().newFieldInGroup("prap_Ap_Coll_Code", "AP-COLL-CODE", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_COLL_CODE");
        prap_Ap_G_Key = vw_prap.getRecord().newFieldInGroup("prap_Ap_G_Key", "AP-G-KEY", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, "AP_G_KEY");
        prap_Ap_G_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_G_Ind", "AP-G-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_G_IND");
        prap_Ap_Soc_Sec = vw_prap.getRecord().newFieldInGroup("prap_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, 
            "AP_SOC_SEC");
        prap_Ap_Dob = vw_prap.getRecord().newFieldInGroup("prap_Ap_Dob", "AP-DOB", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DOB");
        prap_Ap_Lob = vw_prap.getRecord().newFieldInGroup("prap_Ap_Lob", "AP-LOB", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LOB");
        prap_Ap_Lob.setDdmHeader("LOB");
        prap_Ap_Bill_Code = vw_prap.getRecord().newFieldInGroup("prap_Ap_Bill_Code", "AP-BILL-CODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_BILL_CODE");
        prap_Ap_Tiaa_Contr = vw_prap.getRecord().newGroupInGroup("prap_Ap_Tiaa_Contr", "AP-TIAA-CONTR");
        prap_App_Pref = prap_Ap_Tiaa_Contr.newFieldInGroup("prap_App_Pref", "APP-PREF", FieldType.STRING, 1, RepeatingFieldStrategy.None, "APP_PREF");
        prap_App_Cont = prap_Ap_Tiaa_Contr.newFieldInGroup("prap_App_Cont", "APP-CONT", FieldType.PACKED_DECIMAL, 7, RepeatingFieldStrategy.None, "APP_CONT");
        prap_Ap_Cref_Contr = vw_prap.getRecord().newGroupInGroup("prap_Ap_Cref_Contr", "AP-CREF-CONTR");
        prap_App_C_Pref = prap_Ap_Cref_Contr.newFieldInGroup("prap_App_C_Pref", "APP-C-PREF", FieldType.STRING, 1, RepeatingFieldStrategy.None, "APP_C_PREF");
        prap_App_C_Cont = prap_Ap_Cref_Contr.newFieldInGroup("prap_App_C_Cont", "APP-C-CONT", FieldType.PACKED_DECIMAL, 7, RepeatingFieldStrategy.None, 
            "APP_C_CONT");
        prap_Ap_T_Doi = vw_prap.getRecord().newFieldInGroup("prap_Ap_T_Doi", "AP-T-DOI", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, "AP_T_DOI");
        prap_Ap_C_Doi = vw_prap.getRecord().newFieldInGroup("prap_Ap_C_Doi", "AP-C-DOI", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, "AP_C_DOI");
        prap_Ap_Curr = vw_prap.getRecord().newFieldInGroup("prap_Ap_Curr", "AP-CURR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_CURR");
        prap_Ap_T_Age_1st = vw_prap.getRecord().newFieldInGroup("prap_Ap_T_Age_1st", "AP-T-AGE-1ST", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, 
            "AP_T_AGE_1ST");
        prap_Ap_C_Age_1st = vw_prap.getRecord().newFieldInGroup("prap_Ap_C_Age_1st", "AP-C-AGE-1ST", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, 
            "AP_C_AGE_1ST");
        prap_Ap_Ownership = vw_prap.getRecord().newFieldInGroup("prap_Ap_Ownership", "AP-OWNERSHIP", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_OWNERSHIP");
        prap_Ap_Sex = vw_prap.getRecord().newFieldInGroup("prap_Ap_Sex", "AP-SEX", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_SEX");
        prap_Ap_Sex.setDdmHeader("SEX CDE");
        prap_Ap_Mail_Zip = vw_prap.getRecord().newFieldInGroup("prap_Ap_Mail_Zip", "AP-MAIL-ZIP", FieldType.STRING, 5, RepeatingFieldStrategy.None, "AP_MAIL_ZIP");
        prap_Ap_Name_Addr_Cd = vw_prap.getRecord().newFieldInGroup("prap_Ap_Name_Addr_Cd", "AP-NAME-ADDR-CD", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "AP_NAME_ADDR_CD");
        prap_Ap_Dt_Ent_Sys = vw_prap.getRecord().newFieldInGroup("prap_Ap_Dt_Ent_Sys", "AP-DT-ENT-SYS", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, 
            "AP_DT_ENT_SYS");
        prap_Ap_Dt_Released = vw_prap.getRecord().newFieldInGroup("prap_Ap_Dt_Released", "AP-DT-RELEASED", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, 
            "AP_DT_RELEASED");
        prap_Ap_Dt_Matched = vw_prap.getRecord().newFieldInGroup("prap_Ap_Dt_Matched", "AP-DT-MATCHED", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, 
            "AP_DT_MATCHED");
        prap_Ap_Dt_Deleted = vw_prap.getRecord().newFieldInGroup("prap_Ap_Dt_Deleted", "AP-DT-DELETED", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, 
            "AP_DT_DELETED");
        prap_Ap_Dt_Withdrawn = vw_prap.getRecord().newFieldInGroup("prap_Ap_Dt_Withdrawn", "AP-DT-WITHDRAWN", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, 
            "AP_DT_WITHDRAWN");
        prap_Ap_Alloc_Discr = vw_prap.getRecord().newFieldInGroup("prap_Ap_Alloc_Discr", "AP-ALLOC-DISCR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_ALLOC_DISCR");
        prap_Ap_Release_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Release_Ind", "AP-RELEASE-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_RELEASE_IND");
        prap_Ap_Type = vw_prap.getRecord().newFieldInGroup("prap_Ap_Type", "AP-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_TYPE");
        prap_Ap_Other_Pols = vw_prap.getRecord().newFieldInGroup("prap_Ap_Other_Pols", "AP-OTHER-POLS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_OTHER_POLS");
        prap_Ap_Record_Type = vw_prap.getRecord().newFieldInGroup("prap_Ap_Record_Type", "AP-RECORD-TYPE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_RECORD_TYPE");
        prap_Ap_Status = vw_prap.getRecord().newFieldInGroup("prap_Ap_Status", "AP-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_STATUS");
        prap_Ap_Numb_Dailys = vw_prap.getRecord().newFieldInGroup("prap_Ap_Numb_Dailys", "AP-NUMB-DAILYS", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_NUMB_DAILYS");
        prap_Ap_Dt_App_Recvd = vw_prap.getRecord().newFieldInGroup("prap_Ap_Dt_App_Recvd", "AP-DT-APP-RECVD", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, 
            "AP_DT_APP_RECVD");
        prap_Ap_Allocation_Info = vw_prap.getRecord().newGroupInGroup("prap_Ap_Allocation_Info", "AP-ALLOCATION-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_ANNTY_PRAP_AP_ALLOCATION_INFO");
        prap_Ap_Allocation = prap_Ap_Allocation_Info.newFieldArrayInGroup("prap_Ap_Allocation", "AP-ALLOCATION", FieldType.PACKED_DECIMAL, 3, new DbsArrayController(1,20) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ALLOCATION", "ACIS_ANNTY_PRAP_AP_ALLOCATION_INFO");
        prap_Ap_App_Source = vw_prap.getRecord().newFieldInGroup("prap_Ap_App_Source", "AP-APP-SOURCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_APP_SOURCE");
        prap_Ap_App_Source.setDdmHeader("SOURCE");
        prap_Ap_Region_Code = vw_prap.getRecord().newFieldInGroup("prap_Ap_Region_Code", "AP-REGION-CODE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "AP_REGION_CODE");
        prap_Ap_Orig_Issue_State = vw_prap.getRecord().newFieldInGroup("prap_Ap_Orig_Issue_State", "AP-ORIG-ISSUE-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "AP_ORIG_ISSUE_STATE");
        prap_Ap_Ownership_Type = vw_prap.getRecord().newFieldInGroup("prap_Ap_Ownership_Type", "AP-OWNERSHIP-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_OWNERSHIP_TYPE");
        prap_Ap_Ownership_Type.setDdmHeader("CNTRCT OWNERSHIP");
        prap_Ap_Lob_Type = vw_prap.getRecord().newFieldInGroup("prap_Ap_Lob_Type", "AP-LOB-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LOB_TYPE");
        prap_Ap_Lob_Type.setDdmHeader("LOB/TYPE");
        prap_Ap_Split_Code = vw_prap.getRecord().newFieldInGroup("prap_Ap_Split_Code", "AP-SPLIT-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "AP_SPLIT_CODE");
        prap_Ap_Address_Info = vw_prap.getRecord().newGroupInGroup("prap_Ap_Address_Info", "AP-ADDRESS-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        prap_Ap_Address_Line = prap_Ap_Address_Info.newFieldArrayInGroup("prap_Ap_Address_Line", "AP-ADDRESS-LINE", FieldType.STRING, 35, new DbsArrayController(1,5) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ADDRESS_LINE", "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        prap_Ap_City = vw_prap.getRecord().newFieldInGroup("prap_Ap_City", "AP-CITY", FieldType.STRING, 27, RepeatingFieldStrategy.None, "AP_CITY");
        prap_Ap_Current_State_Code = vw_prap.getRecord().newFieldInGroup("prap_Ap_Current_State_Code", "AP-CURRENT-STATE-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "AP_CURRENT_STATE_CODE");
        prap_Ap_Dana_Transaction_Cd = vw_prap.getRecord().newFieldInGroup("prap_Ap_Dana_Transaction_Cd", "AP-DANA-TRANSACTION-CD", FieldType.STRING, 2, 
            RepeatingFieldStrategy.None, "AP_DANA_TRANSACTION_CD");
        prap_Ap_Dana_Stndrd_Rtn_Cd = vw_prap.getRecord().newFieldInGroup("prap_Ap_Dana_Stndrd_Rtn_Cd", "AP-DANA-STNDRD-RTN-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "AP_DANA_STNDRD_RTN_CD");
        prap_Ap_Dana_Finalist_Reason_Cd = vw_prap.getRecord().newFieldInGroup("prap_Ap_Dana_Finalist_Reason_Cd", "AP-DANA-FINALIST-REASON-CD", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "AP_DANA_FINALIST_REASON_CD");
        prap_Ap_Dana_Addr_Stndrd_Code = vw_prap.getRecord().newFieldInGroup("prap_Ap_Dana_Addr_Stndrd_Code", "AP-DANA-ADDR-STNDRD-CODE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_DANA_ADDR_STNDRD_CODE");
        prap_Ap_Dana_Stndrd_Overide = vw_prap.getRecord().newFieldInGroup("prap_Ap_Dana_Stndrd_Overide", "AP-DANA-STNDRD-OVERIDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "AP_DANA_STNDRD_OVERIDE");
        prap_Ap_Dana_Postal_Data_Fields = vw_prap.getRecord().newFieldInGroup("prap_Ap_Dana_Postal_Data_Fields", "AP-DANA-POSTAL-DATA-FIELDS", FieldType.STRING, 
            44, RepeatingFieldStrategy.None, "AP_DANA_POSTAL_DATA_FIELDS");
        prap_Ap_Dana_Addr_Geographic_Code = vw_prap.getRecord().newFieldInGroup("prap_Ap_Dana_Addr_Geographic_Code", "AP-DANA-ADDR-GEOGRAPHIC-CODE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "AP_DANA_ADDR_GEOGRAPHIC_CODE");
        prap_Ap_Annuity_Start_Date = vw_prap.getRecord().newFieldInGroup("prap_Ap_Annuity_Start_Date", "AP-ANNUITY-START-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "AP_ANNUITY_START_DATE");
        prap_Ap_Maximum_Alloc_Pct = vw_prap.getRecord().newGroupInGroup("prap_Ap_Maximum_Alloc_Pct", "AP-MAXIMUM-ALLOC-PCT", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        prap_Ap_Max_Alloc_Pct = prap_Ap_Maximum_Alloc_Pct.newFieldArrayInGroup("prap_Ap_Max_Alloc_Pct", "AP-MAX-ALLOC-PCT", FieldType.STRING, 3, new DbsArrayController(1,100) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MAX_ALLOC_PCT", "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        prap_Ap_Max_Alloc_Pct_Sign = prap_Ap_Maximum_Alloc_Pct.newFieldArrayInGroup("prap_Ap_Max_Alloc_Pct_Sign", "AP-MAX-ALLOC-PCT-SIGN", FieldType.STRING, 
            1, new DbsArrayController(1,100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MAX_ALLOC_PCT_SIGN", "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        prap_Ap_Bene_Info_Type = vw_prap.getRecord().newFieldInGroup("prap_Ap_Bene_Info_Type", "AP-BENE-INFO-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_BENE_INFO_TYPE");
        prap_Ap_Primary_Std_Ent = vw_prap.getRecord().newFieldInGroup("prap_Ap_Primary_Std_Ent", "AP-PRIMARY-STD-ENT", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_PRIMARY_STD_ENT");
        prap_Ap_Bene_Primary_Info = vw_prap.getRecord().newGroupInGroup("prap_Ap_Bene_Primary_Info", "AP-BENE-PRIMARY-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_ANNTY_PRAP_AP_BENE_PRIMARY_INFO");
        prap_Ap_Primary_Bene_Info = prap_Ap_Bene_Primary_Info.newFieldArrayInGroup("prap_Ap_Primary_Bene_Info", "AP-PRIMARY-BENE-INFO", FieldType.STRING, 
            72, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_PRIMARY_BENE_INFO", "ACIS_ANNTY_PRAP_AP_BENE_PRIMARY_INFO");
        prap_Ap_Contingent_Std_Ent = vw_prap.getRecord().newFieldInGroup("prap_Ap_Contingent_Std_Ent", "AP-CONTINGENT-STD-ENT", FieldType.NUMERIC, 1, 
            RepeatingFieldStrategy.None, "AP_CONTINGENT_STD_ENT");
        prap_Ap_Bene_Contingent_Info = vw_prap.getRecord().newGroupInGroup("prap_Ap_Bene_Contingent_Info", "AP-BENE-CONTINGENT-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_ANNTY_PRAP_AP_BENE_CONTINGENT_INFO");
        prap_Ap_Contingent_Bene_Info = prap_Ap_Bene_Contingent_Info.newFieldArrayInGroup("prap_Ap_Contingent_Bene_Info", "AP-CONTINGENT-BENE-INFO", FieldType.STRING, 
            72, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CONTINGENT_BENE_INFO", "ACIS_ANNTY_PRAP_AP_BENE_CONTINGENT_INFO");
        prap_Ap_Bene_Estate = vw_prap.getRecord().newFieldInGroup("prap_Ap_Bene_Estate", "AP-BENE-ESTATE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_BENE_ESTATE");
        prap_Ap_Bene_Trust = vw_prap.getRecord().newFieldInGroup("prap_Ap_Bene_Trust", "AP-BENE-TRUST", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_BENE_TRUST");
        prap_Ap_Bene_Category = vw_prap.getRecord().newFieldInGroup("prap_Ap_Bene_Category", "AP-BENE-CATEGORY", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_BENE_CATEGORY");
        prap_Ap_Mail_Instructions = vw_prap.getRecord().newFieldInGroup("prap_Ap_Mail_Instructions", "AP-MAIL-INSTRUCTIONS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_MAIL_INSTRUCTIONS");
        prap_Ap_Eop_Addl_Cref_Request = vw_prap.getRecord().newFieldInGroup("prap_Ap_Eop_Addl_Cref_Request", "AP-EOP-ADDL-CREF-REQUEST", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_EOP_ADDL_CREF_REQUEST");
        prap_Ap_Process_Status = vw_prap.getRecord().newFieldInGroup("prap_Ap_Process_Status", "AP-PROCESS-STATUS", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "AP_PROCESS_STATUS");
        prap_Ap_Coll_St_Cd = vw_prap.getRecord().newFieldInGroup("prap_Ap_Coll_St_Cd", "AP-COLL-ST-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "AP_COLL_ST_CD");
        prap_Ap_Csm_Sec_Seg = vw_prap.getRecord().newFieldInGroup("prap_Ap_Csm_Sec_Seg", "AP-CSM-SEC-SEG", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "AP_CSM_SEC_SEG");
        prap_Ap_Rlc_College = vw_prap.getRecord().newFieldInGroup("prap_Ap_Rlc_College", "AP-RLC-COLLEGE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "AP_RLC_COLLEGE");
        prap_Ap_Rlc_Cref_Pref = vw_prap.getRecord().newFieldInGroup("prap_Ap_Rlc_Cref_Pref", "AP-RLC-CREF-PREF", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_RLC_CREF_PREF");
        prap_Ap_Rlc_Cref_Cont = vw_prap.getRecord().newFieldInGroup("prap_Ap_Rlc_Cref_Cont", "AP-RLC-CREF-CONT", FieldType.PACKED_DECIMAL, 7, RepeatingFieldStrategy.None, 
            "AP_RLC_CREF_CONT");
        prap_Ap_Cor_Prfx_Nme = vw_prap.getRecord().newFieldInGroup("prap_Ap_Cor_Prfx_Nme", "AP-COR-PRFX-NME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "AP_COR_PRFX_NME");
        prap_Ap_Cor_Last_Nme = vw_prap.getRecord().newFieldInGroup("prap_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "AP_COR_LAST_NME");
        prap_Ap_Cor_First_Nme = vw_prap.getRecord().newFieldInGroup("prap_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "AP_COR_FIRST_NME");
        prap_Ap_Cor_Mddle_Nme = vw_prap.getRecord().newFieldInGroup("prap_Ap_Cor_Mddle_Nme", "AP-COR-MDDLE-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "AP_COR_MDDLE_NME");
        prap_Ap_Cor_Sffx_Nme = vw_prap.getRecord().newFieldInGroup("prap_Ap_Cor_Sffx_Nme", "AP-COR-SFFX-NME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "AP_COR_SFFX_NME");
        prap_Ap_Ph_Hist_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Ph_Hist_Ind", "AP-PH-HIST-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_PH_HIST_IND");
        prap_Ap_Rcrd_Updt_Tm_Stamp = vw_prap.getRecord().newFieldInGroup("prap_Ap_Rcrd_Updt_Tm_Stamp", "AP-RCRD-UPDT-TM-STAMP", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "AP_RCRD_UPDT_TM_STAMP");
        prap_Ap_Contact_Mode = vw_prap.getRecord().newFieldInGroup("prap_Ap_Contact_Mode", "AP-CONTACT-MODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_CONTACT_MODE");
        prap_Ap_Racf_Id = vw_prap.getRecord().newFieldInGroup("prap_Ap_Racf_Id", "AP-RACF-ID", FieldType.STRING, 16, RepeatingFieldStrategy.None, "AP_RACF_ID");
        prap_Ap_Pin_Nbr = vw_prap.getRecord().newFieldInGroup("prap_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "AP_PIN_NBR");
        prap_Ap_Rqst_Log_Dte_Tm = vw_prap.getRecord().newGroupInGroup("prap_Ap_Rqst_Log_Dte_Tm", "AP-RQST-LOG-DTE-TM", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        prap_Ap_Rqst_Log_Dte_Time = prap_Ap_Rqst_Log_Dte_Tm.newFieldArrayInGroup("prap_Ap_Rqst_Log_Dte_Time", "AP-RQST-LOG-DTE-TIME", FieldType.STRING, 
            15, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_RQST_LOG_DTE_TIME", "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        prap_Ap_Sync_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Sync_Ind", "AP-SYNC-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_SYNC_IND");
        prap_Ap_Cor_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Cor_Ind", "AP-COR-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_COR_IND");
        prap_Ap_Alloc_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Alloc_Ind", "AP-ALLOC-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_ALLOC_IND");
        prap_Ap_Tiaa_Doi = vw_prap.getRecord().newFieldInGroup("prap_Ap_Tiaa_Doi", "AP-TIAA-DOI", FieldType.DATE, RepeatingFieldStrategy.None, "AP_TIAA_DOI");
        prap_Ap_Cref_Doi = vw_prap.getRecord().newFieldInGroup("prap_Ap_Cref_Doi", "AP-CREF-DOI", FieldType.DATE, RepeatingFieldStrategy.None, "AP_CREF_DOI");
        prap_Ap_Mit_Request = vw_prap.getRecord().newGroupInGroup("prap_Ap_Mit_Request", "AP-MIT-REQUEST", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        prap_Ap_Mit_Unit = prap_Ap_Mit_Request.newFieldArrayInGroup("prap_Ap_Mit_Unit", "AP-MIT-UNIT", FieldType.STRING, 8, new DbsArrayController(1,5) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MIT_UNIT", "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        prap_Ap_Mit_Wpid = prap_Ap_Mit_Request.newFieldArrayInGroup("prap_Ap_Mit_Wpid", "AP-MIT-WPID", FieldType.STRING, 6, new DbsArrayController(1,5) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MIT_WPID", "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        prap_Ap_Ira_Rollover_Type = vw_prap.getRecord().newFieldInGroup("prap_Ap_Ira_Rollover_Type", "AP-IRA-ROLLOVER-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_IRA_ROLLOVER_TYPE");
        prap_Ap_Ira_Record_Type = vw_prap.getRecord().newFieldInGroup("prap_Ap_Ira_Record_Type", "AP-IRA-RECORD-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_IRA_RECORD_TYPE");
        prap_Ap_Mult_App_Status = vw_prap.getRecord().newFieldInGroup("prap_Ap_Mult_App_Status", "AP-MULT-APP-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_MULT_APP_STATUS");
        prap_Ap_Mult_App_Lob = vw_prap.getRecord().newFieldInGroup("prap_Ap_Mult_App_Lob", "AP-MULT-APP-LOB", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_MULT_APP_LOB");
        prap_Ap_Mult_App_Lob_Type = vw_prap.getRecord().newFieldInGroup("prap_Ap_Mult_App_Lob_Type", "AP-MULT-APP-LOB-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_MULT_APP_LOB_TYPE");
        prap_Ap_Mult_App_Ppg = vw_prap.getRecord().newFieldInGroup("prap_Ap_Mult_App_Ppg", "AP-MULT-APP-PPG", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_MULT_APP_PPG");
        prap_Ap_Print_Date = vw_prap.getRecord().newFieldInGroup("prap_Ap_Print_Date", "AP-PRINT-DATE", FieldType.DATE, RepeatingFieldStrategy.None, "AP_PRINT_DATE");
        prap_Ap_Cntrct_Info = vw_prap.getRecord().newGroupInGroup("prap_Ap_Cntrct_Info", "AP-CNTRCT-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");
        prap_Ap_Cntrct_Type = prap_Ap_Cntrct_Info.newFieldArrayInGroup("prap_Ap_Cntrct_Type", "AP-CNTRCT-TYPE", FieldType.STRING, 1, new DbsArrayController(1,15) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CNTRCT_TYPE", "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");
        prap_Ap_Cntrct_Nbr = prap_Ap_Cntrct_Info.newFieldArrayInGroup("prap_Ap_Cntrct_Nbr", "AP-CNTRCT-NBR", FieldType.STRING, 10, new DbsArrayController(1,15) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CNTRCT_NBR", "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");
        prap_Ap_Cntrct_Proceeds_Amt = prap_Ap_Cntrct_Info.newFieldArrayInGroup("prap_Ap_Cntrct_Proceeds_Amt", "AP-CNTRCT-PROCEEDS-AMT", FieldType.NUMERIC, 
            10, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CNTRCT_PROCEEDS_AMT", "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");
        prap_Ap_Addr_Info = vw_prap.getRecord().newGroupInGroup("prap_Ap_Addr_Info", "AP-ADDR-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_ANNTY_PRAP_AP_ADDR_INFO");
        prap_Ap_Address_Txt = prap_Ap_Addr_Info.newFieldArrayInGroup("prap_Ap_Address_Txt", "AP-ADDRESS-TXT", FieldType.STRING, 35, new DbsArrayController(1,5) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ADDRESS_TXT", "ACIS_ANNTY_PRAP_AP_ADDR_INFO");
        prap_Ap_Address_Dest_Name = vw_prap.getRecord().newFieldInGroup("prap_Ap_Address_Dest_Name", "AP-ADDRESS-DEST-NAME", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "AP_ADDRESS_DEST_NAME");
        prap_Ap_Zip_Code = vw_prap.getRecord().newFieldInGroup("prap_Ap_Zip_Code", "AP-ZIP-CODE", FieldType.STRING, 9, RepeatingFieldStrategy.None, "AP_ZIP_CODE");
        prap_Ap_Bank_Pymnt_Acct_Nmbr = vw_prap.getRecord().newFieldInGroup("prap_Ap_Bank_Pymnt_Acct_Nmbr", "AP-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 
            21, RepeatingFieldStrategy.None, "AP_BANK_PYMNT_ACCT_NMBR");
        prap_Ap_Bank_Aba_Acct_Nmbr = vw_prap.getRecord().newFieldInGroup("prap_Ap_Bank_Aba_Acct_Nmbr", "AP-BANK-ABA-ACCT-NMBR", FieldType.STRING, 9, RepeatingFieldStrategy.None, 
            "AP_BANK_ABA_ACCT_NMBR");
        prap_Ap_Addr_Usage_Code = vw_prap.getRecord().newFieldInGroup("prap_Ap_Addr_Usage_Code", "AP-ADDR-USAGE-CODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_ADDR_USAGE_CODE");
        prap_Ap_Init_Paymt_Amt = vw_prap.getRecord().newFieldInGroup("prap_Ap_Init_Paymt_Amt", "AP-INIT-PAYMT-AMT", FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, 
            "AP_INIT_PAYMT_AMT");
        prap_Ap_Init_Paymt_Pct = vw_prap.getRecord().newFieldInGroup("prap_Ap_Init_Paymt_Pct", "AP-INIT-PAYMT-PCT", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "AP_INIT_PAYMT_PCT");
        prap_Ap_Financial_1 = vw_prap.getRecord().newFieldInGroup("prap_Ap_Financial_1", "AP-FINANCIAL-1", FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, 
            "AP_FINANCIAL_1");
        prap_Ap_Financial_2 = vw_prap.getRecord().newFieldInGroup("prap_Ap_Financial_2", "AP-FINANCIAL-2", FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, 
            "AP_FINANCIAL_2");
        prap_Ap_Financial_3 = vw_prap.getRecord().newFieldInGroup("prap_Ap_Financial_3", "AP-FINANCIAL-3", FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, 
            "AP_FINANCIAL_3");
        prap_Ap_Financial_4 = vw_prap.getRecord().newFieldInGroup("prap_Ap_Financial_4", "AP-FINANCIAL-4", FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, 
            "AP_FINANCIAL_4");
        prap_Ap_Financial_5 = vw_prap.getRecord().newFieldInGroup("prap_Ap_Financial_5", "AP-FINANCIAL-5", FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, 
            "AP_FINANCIAL_5");
        prap_Ap_Irc_Sectn_Grp_Cde = vw_prap.getRecord().newFieldInGroup("prap_Ap_Irc_Sectn_Grp_Cde", "AP-IRC-SECTN-GRP-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "AP_IRC_SECTN_GRP_CDE");
        prap_Ap_Irc_Sectn_Cde = vw_prap.getRecord().newFieldInGroup("prap_Ap_Irc_Sectn_Cde", "AP-IRC-SECTN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "AP_IRC_SECTN_CDE");
        prap_Ap_Inst_Link_Cde = vw_prap.getRecord().newFieldInGroup("prap_Ap_Inst_Link_Cde", "AP-INST-LINK-CDE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "AP_INST_LINK_CDE");
        prap_Ap_Applcnt_Req_Type = vw_prap.getRecord().newFieldInGroup("prap_Ap_Applcnt_Req_Type", "AP-APPLCNT-REQ-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_APPLCNT_REQ_TYPE");
        prap_Ap_Applcnt_Req_Type.setDdmHeader("APP REQ TYPE");
        prap_Ap_Addr_Sync_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Addr_Sync_Ind", "AP-ADDR-SYNC-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_ADDR_SYNC_IND");
        prap_Ap_Tiaa_Service_Agent = vw_prap.getRecord().newFieldInGroup("prap_Ap_Tiaa_Service_Agent", "AP-TIAA-SERVICE-AGENT", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_TIAA_SERVICE_AGENT");
        prap_Ap_Prap_Rsch_Mit_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Prap_Rsch_Mit_Ind", "AP-PRAP-RSCH-MIT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_PRAP_RSCH_MIT_IND");
        prap_Ap_Ppg_Team_Cde = vw_prap.getRecord().newFieldInGroup("prap_Ap_Ppg_Team_Cde", "AP-PPG-TEAM-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "AP_PPG_TEAM_CDE");
        prap_Ap_Tiaa_Cntrct = vw_prap.getRecord().newFieldInGroup("prap_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TIAA_CNTRCT");
        prap_Ap_Cref_Cert = vw_prap.getRecord().newFieldInGroup("prap_Ap_Cref_Cert", "AP-CREF-CERT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_CREF_CERT");
        prap_Ap_Rlc_Cref_Cert = vw_prap.getRecord().newFieldInGroup("prap_Ap_Rlc_Cref_Cert", "AP-RLC-CREF-CERT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_RLC_CREF_CERT");
        prap_Ap_Allocation_Model_Type = vw_prap.getRecord().newFieldInGroup("prap_Ap_Allocation_Model_Type", "AP-ALLOCATION-MODEL-TYPE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "AP_ALLOCATION_MODEL_TYPE");
        prap_Ap_Divorce_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Divorce_Ind", "AP-DIVORCE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_DIVORCE_IND");
        prap_Ap_Email_Address = vw_prap.getRecord().newFieldInGroup("prap_Ap_Email_Address", "AP-EMAIL-ADDRESS", FieldType.STRING, 50, RepeatingFieldStrategy.None, 
            "AP_EMAIL_ADDRESS");
        prap_Ap_E_Signed_Appl_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_E_Signed_Appl_Ind", "AP-E-SIGNED-APPL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_E_SIGNED_APPL_IND");
        prap_Ap_Eft_Requested_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Eft_Requested_Ind", "AP-EFT-REQUESTED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_EFT_REQUESTED_IND");
        prap_Ap_Prap_Prem_Rsch_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Prap_Prem_Rsch_Ind", "AP-PRAP-PREM-RSCH-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_PRAP_PREM_RSCH_IND");
        prap_Ap_Address_Change_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Address_Change_Ind", "AP-ADDRESS-CHANGE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_ADDRESS_CHANGE_IND");
        prap_Ap_Allocation_Fmt = vw_prap.getRecord().newFieldInGroup("prap_Ap_Allocation_Fmt", "AP-ALLOCATION-FMT", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_ALLOCATION_FMT");
        prap_Ap_Phone_No = vw_prap.getRecord().newFieldInGroup("prap_Ap_Phone_No", "AP-PHONE-NO", FieldType.STRING, 20, RepeatingFieldStrategy.None, "AP_PHONE_NO");
        prap_Ap_Fund_Identifier = vw_prap.getRecord().newGroupInGroup("prap_Ap_Fund_Identifier", "AP-FUND-IDENTIFIER", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        prap_Ap_Fund_Cde = prap_Ap_Fund_Identifier.newFieldArrayInGroup("prap_Ap_Fund_Cde", "AP-FUND-CDE", FieldType.STRING, 10, new DbsArrayController(1,100) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_FUND_CDE", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        prap_Ap_Allocation_Pct = prap_Ap_Fund_Identifier.newFieldArrayInGroup("prap_Ap_Allocation_Pct", "AP-ALLOCATION-PCT", FieldType.NUMERIC, 3, new 
            DbsArrayController(1,100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ALLOCATION_PCT", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        prap_Ap_Sgrd_Plan_No = vw_prap.getRecord().newFieldInGroup("prap_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_SGRD_PLAN_NO");
        prap_Ap_Sgrd_Subplan_No = vw_prap.getRecord().newFieldInGroup("prap_Ap_Sgrd_Subplan_No", "AP-SGRD-SUBPLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_SGRD_SUBPLAN_NO");
        prap_Ap_Sgrd_Part_Ext = vw_prap.getRecord().newFieldInGroup("prap_Ap_Sgrd_Part_Ext", "AP-SGRD-PART-EXT", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "AP_SGRD_PART_EXT");
        prap_Ap_Sgrd_Divsub = vw_prap.getRecord().newFieldInGroup("prap_Ap_Sgrd_Divsub", "AP-SGRD-DIVSUB", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "AP_SGRD_DIVSUB");
        prap_Ap_Text_Udf_1 = vw_prap.getRecord().newFieldInGroup("prap_Ap_Text_Udf_1", "AP-TEXT-UDF-1", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TEXT_UDF_1");
        prap_Ap_Text_Udf_2 = vw_prap.getRecord().newFieldInGroup("prap_Ap_Text_Udf_2", "AP-TEXT-UDF-2", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TEXT_UDF_2");
        prap_Ap_Text_Udf_3 = vw_prap.getRecord().newFieldInGroup("prap_Ap_Text_Udf_3", "AP-TEXT-UDF-3", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TEXT_UDF_3");
        prap_Ap_Replacement_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Replacement_Ind", "AP-REPLACEMENT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_REPLACEMENT_IND");
        prap_Ap_Register_Id = vw_prap.getRecord().newFieldInGroup("prap_Ap_Register_Id", "AP-REGISTER-ID", FieldType.STRING, 11, RepeatingFieldStrategy.None, 
            "AP_REGISTER_ID");
        prap_Ap_Agent_Or_Racf_Id = vw_prap.getRecord().newFieldInGroup("prap_Ap_Agent_Or_Racf_Id", "AP-AGENT-OR-RACF-ID", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "AP_AGENT_OR_RACF_ID");
        prap_Ap_Exempt_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Exempt_Ind", "AP-EXEMPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_EXEMPT_IND");
        prap_Ap_Arr_Insurer_1 = vw_prap.getRecord().newFieldInGroup("prap_Ap_Arr_Insurer_1", "AP-ARR-INSURER-1", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "AP_ARR_INSURER_1");
        prap_Ap_Arr_Insurer_2 = vw_prap.getRecord().newFieldInGroup("prap_Ap_Arr_Insurer_2", "AP-ARR-INSURER-2", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "AP_ARR_INSURER_2");
        prap_Ap_Arr_Insurer_3 = vw_prap.getRecord().newFieldInGroup("prap_Ap_Arr_Insurer_3", "AP-ARR-INSURER-3", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "AP_ARR_INSURER_3");
        prap_Ap_Arr_1035_Exch_Ind_1 = vw_prap.getRecord().newFieldInGroup("prap_Ap_Arr_1035_Exch_Ind_1", "AP-ARR-1035-EXCH-IND-1", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "AP_ARR_1035_EXCH_IND_1");
        prap_Ap_Arr_1035_Exch_Ind_2 = vw_prap.getRecord().newFieldInGroup("prap_Ap_Arr_1035_Exch_Ind_2", "AP-ARR-1035-EXCH-IND-2", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "AP_ARR_1035_EXCH_IND_2");
        prap_Ap_Arr_1035_Exch_Ind_3 = vw_prap.getRecord().newFieldInGroup("prap_Ap_Arr_1035_Exch_Ind_3", "AP-ARR-1035-EXCH-IND-3", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "AP_ARR_1035_EXCH_IND_3");
        prap_Ap_Autosave_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Autosave_Ind", "AP-AUTOSAVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_AUTOSAVE_IND");
        prap_Ap_As_Cur_Dflt_Opt = vw_prap.getRecord().newFieldInGroup("prap_Ap_As_Cur_Dflt_Opt", "AP-AS-CUR-DFLT-OPT", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_AS_CUR_DFLT_OPT");
        prap_Ap_As_Cur_Dflt_Amt = vw_prap.getRecord().newFieldInGroup("prap_Ap_As_Cur_Dflt_Amt", "AP-AS-CUR-DFLT-AMT", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, 
            "AP_AS_CUR_DFLT_AMT");
        prap_Ap_As_Incr_Opt = vw_prap.getRecord().newFieldInGroup("prap_Ap_As_Incr_Opt", "AP-AS-INCR-OPT", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_AS_INCR_OPT");
        prap_Ap_As_Incr_Amt = vw_prap.getRecord().newFieldInGroup("prap_Ap_As_Incr_Amt", "AP-AS-INCR-AMT", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, 
            "AP_AS_INCR_AMT");
        prap_Ap_As_Max_Pct = vw_prap.getRecord().newFieldInGroup("prap_Ap_As_Max_Pct", "AP-AS-MAX-PCT", FieldType.NUMERIC, 7, 2, RepeatingFieldStrategy.None, 
            "AP_AS_MAX_PCT");
        prap_Ap_Ae_Opt_Out_Days = vw_prap.getRecord().newFieldInGroup("prap_Ap_Ae_Opt_Out_Days", "AP-AE-OPT-OUT-DAYS", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "AP_AE_OPT_OUT_DAYS");
        prap_Ap_Incmpl_Acct_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Incmpl_Acct_Ind", "AP-INCMPL-ACCT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_INCMPL_ACCT_IND");
        prap_Ap_Delete_User_Id = vw_prap.getRecord().newFieldInGroup("prap_Ap_Delete_User_Id", "AP-DELETE-USER-ID", FieldType.STRING, 16, RepeatingFieldStrategy.None, 
            "AP_DELETE_USER_ID");
        prap_Ap_Delete_Reason_Cd = vw_prap.getRecord().newFieldInGroup("prap_Ap_Delete_Reason_Cd", "AP-DELETE-REASON-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_DELETE_REASON_CD");
        prap_Ap_Consent_Email_Address = vw_prap.getRecord().newFieldInGroup("prap_Ap_Consent_Email_Address", "AP-CONSENT-EMAIL-ADDRESS", FieldType.STRING, 
            50, RepeatingFieldStrategy.None, "AP_CONSENT_EMAIL_ADDRESS");
        prap_Ap_Welc_E_Delivery_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Welc_E_Delivery_Ind", "AP-WELC-E-DELIVERY-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "AP_WELC_E_DELIVERY_IND");
        prap_Ap_Welc_E_Delivery_Ind.setDdmHeader("WELC EDLVRY IND");
        prap_Ap_Legal_E_Delivery_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Legal_E_Delivery_Ind", "AP-LEGAL-E-DELIVERY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_LEGAL_E_DELIVERY_IND");
        prap_Ap_Legal_E_Delivery_Ind.setDdmHeader("LGL EDLVRY IND");
        prap_Ap_Legal_Ann_Option = vw_prap.getRecord().newFieldInGroup("prap_Ap_Legal_Ann_Option", "AP-LEGAL-ANN-OPTION", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_LEGAL_ANN_OPTION");
        prap_Ap_Orchestration_Id = vw_prap.getRecord().newFieldInGroup("prap_Ap_Orchestration_Id", "AP-ORCHESTRATION-ID", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "AP_ORCHESTRATION_ID");
        prap_Ap_Mail_Addr_Country_Cd = vw_prap.getRecord().newFieldInGroup("prap_Ap_Mail_Addr_Country_Cd", "AP-MAIL-ADDR-COUNTRY-CD", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "AP_MAIL_ADDR_COUNTRY_CD");
        prap_Ap_Tic_Startdate = vw_prap.getRecord().newFieldInGroup("prap_Ap_Tic_Startdate", "AP-TIC-STARTDATE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "AP_TIC_STARTDATE");
        prap_Ap_Tic_Enddate = vw_prap.getRecord().newFieldInGroup("prap_Ap_Tic_Enddate", "AP-TIC-ENDDATE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "AP_TIC_ENDDATE");
        prap_Ap_Tic_Percentage = vw_prap.getRecord().newFieldInGroup("prap_Ap_Tic_Percentage", "AP-TIC-PERCENTAGE", FieldType.NUMERIC, 15, 6, RepeatingFieldStrategy.None, 
            "AP_TIC_PERCENTAGE");
        prap_Ap_Tic_Postdays = vw_prap.getRecord().newFieldInGroup("prap_Ap_Tic_Postdays", "AP-TIC-POSTDAYS", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "AP_TIC_POSTDAYS");
        prap_Ap_Tic_Limit = vw_prap.getRecord().newFieldInGroup("prap_Ap_Tic_Limit", "AP-TIC-LIMIT", FieldType.NUMERIC, 13, 2, RepeatingFieldStrategy.None, 
            "AP_TIC_LIMIT");
        prap_Ap_Tic_Postfreq = vw_prap.getRecord().newFieldInGroup("prap_Ap_Tic_Postfreq", "AP-TIC-POSTFREQ", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_TIC_POSTFREQ");
        prap_Ap_Tic_Pl_Level = vw_prap.getRecord().newFieldInGroup("prap_Ap_Tic_Pl_Level", "AP-TIC-PL-LEVEL", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_TIC_PL_LEVEL");
        prap_Ap_Tic_Windowdays = vw_prap.getRecord().newFieldInGroup("prap_Ap_Tic_Windowdays", "AP-TIC-WINDOWDAYS", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "AP_TIC_WINDOWDAYS");
        prap_Ap_Tic_Reqdlywindow = vw_prap.getRecord().newFieldInGroup("prap_Ap_Tic_Reqdlywindow", "AP-TIC-REQDLYWINDOW", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "AP_TIC_REQDLYWINDOW");
        prap_Ap_Tic_Recap_Prov = vw_prap.getRecord().newFieldInGroup("prap_Ap_Tic_Recap_Prov", "AP-TIC-RECAP-PROV", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "AP_TIC_RECAP_PROV");
        prap_Ap_Ann_Funding_Dt = vw_prap.getRecord().newFieldInGroup("prap_Ap_Ann_Funding_Dt", "AP-ANN-FUNDING-DT", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "AP_ANN_FUNDING_DT");
        prap_Ap_Tiaa_Ann_Issue_Dt = vw_prap.getRecord().newFieldInGroup("prap_Ap_Tiaa_Ann_Issue_Dt", "AP-TIAA-ANN-ISSUE-DT", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "AP_TIAA_ANN_ISSUE_DT");
        prap_Ap_Cref_Ann_Issue_Dt = vw_prap.getRecord().newFieldInGroup("prap_Ap_Cref_Ann_Issue_Dt", "AP-CREF-ANN-ISSUE-DT", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "AP_CREF_ANN_ISSUE_DT");
        prap_Ap_Substitution_Contract_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Substitution_Contract_Ind", "AP-SUBSTITUTION-CONTRACT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_SUBSTITUTION_CONTRACT_IND");
        prap_Ap_Conv_Issue_State = vw_prap.getRecord().newFieldInGroup("prap_Ap_Conv_Issue_State", "AP-CONV-ISSUE-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "AP_CONV_ISSUE_STATE");
        prap_Ap_Deceased_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Deceased_Ind", "AP-DECEASED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_DECEASED_IND");
        prap_Ap_Non_Proprietary_Pkg_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Non_Proprietary_Pkg_Ind", "AP-NON-PROPRIETARY-PKG-IND", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "AP_NON_PROPRIETARY_PKG_IND");
        prap_Ap_Decedent_Contract = vw_prap.getRecord().newFieldInGroup("prap_Ap_Decedent_Contract", "AP-DECEDENT-CONTRACT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_DECEDENT_CONTRACT");
        prap_Ap_Relation_To_Decedent = vw_prap.getRecord().newFieldInGroup("prap_Ap_Relation_To_Decedent", "AP-RELATION-TO-DECEDENT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_RELATION_TO_DECEDENT");
        prap_Ap_Ssn_Tin_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Ssn_Tin_Ind", "AP-SSN-TIN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_SSN_TIN_IND");
        prap_Ap_Oneira_Acct_No = vw_prap.getRecord().newFieldInGroup("prap_Ap_Oneira_Acct_No", "AP-ONEIRA-ACCT-NO", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_ONEIRA_ACCT_NO");
        prap_Ap_Fund_Source_Cde_1 = vw_prap.getRecord().newFieldInGroup("prap_Ap_Fund_Source_Cde_1", "AP-FUND-SOURCE-CDE-1", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_FUND_SOURCE_CDE_1");
        prap_Ap_Fund_Source_Cde_2 = vw_prap.getRecord().newFieldInGroup("prap_Ap_Fund_Source_Cde_2", "AP-FUND-SOURCE-CDE-2", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_FUND_SOURCE_CDE_2");
        prap_Ap_Fund_Identifier_2 = vw_prap.getRecord().newGroupInGroup("prap_Ap_Fund_Identifier_2", "AP-FUND-IDENTIFIER-2", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER_2");
        prap_Ap_Fund_Cde_2 = prap_Ap_Fund_Identifier_2.newFieldArrayInGroup("prap_Ap_Fund_Cde_2", "AP-FUND-CDE-2", FieldType.STRING, 10, new DbsArrayController(1,100) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_FUND_CDE_2", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER_2");
        prap_Ap_Allocation_Pct_2 = prap_Ap_Fund_Identifier_2.newFieldArrayInGroup("prap_Ap_Allocation_Pct_2", "AP-ALLOCATION-PCT-2", FieldType.NUMERIC, 
            3, new DbsArrayController(1,100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ALLOCATION_PCT_2", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER_2");
        vw_prap.setUniquePeList();

        this.setRecordName("LdaAppl301");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_prap.reset();
    }

    // Constructor
    public LdaAppl301() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
