/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:47 PM
**        * FROM NATURAL LDA     : APPBL170
************************************************************
**        * FILE NAME            : LdaAppbl170.java
**        * CLASS NAME           : LdaAppbl170
**        * INSTANCE NAME        : LdaAppbl170
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAppbl170 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Mdm_Data;
    private DbsField pnd_Mdm_Data_Requestor;
    private DbsField pnd_Mdm_Data_Function_Code;
    private DbsField pnd_Mdm_Data_Source_System_Nm;
    private DbsField pnd_Mdm_Data_Foreign_Soc_Sec_Nbr;
    private DbsField pnd_Mdm_Data_Ph_Unique_Id_Nbr;
    private DbsField pnd_Mdm_Data_Ph_Social_Security_No;
    private DbsField pnd_Mdm_Data_Cntrct_Nbr;
    private DbsField pnd_Mdm_Data_Cntrct_Payee_Cde;
    private DbsField pnd_Mdm_Data_Contract_Party_Role;
    private DbsField pnd_Mdm_Data_Cntrct_Status_Cde;
    private DbsField pnd_Mdm_Data_Cntrct_Issue_Dte;
    private DbsField pnd_Mdm_Data_Cntrct_Lob_Cde;
    private DbsField pnd_Mdm_Data_Cntrct_Cref_Nbr;
    private DbsField pnd_Mdm_Data_Cntrct_Cref_Issued_Ind;
    private DbsField pnd_Mdm_Data_Cntrct_Indctrs_And_Codes;
    private DbsField pnd_Mdm_Data_Cntrct_Optn_Cde;
    private DbsField pnd_Mdm_Data_Cntrct_Plan_Cde;
    private DbsField pnd_Mdm_Data_Cntrct_Fund_Cde;
    private DbsField pnd_Mdm_Data_Cntrct_Social_Cde;
    private DbsField pnd_Mdm_Data_Cust_Agreement_Received_Ind;
    private DbsField pnd_Mdm_Data_Asset_Allocation_Service_Ind;
    private DbsField pnd_Mdm_Data_Platform_Ind;
    private DbsField pnd_Mdm_Data_Multi_Source_Ind;
    private DbsField pnd_Mdm_Data_Tiaa_Mailed_Ind;
    private DbsField pnd_Mdm_Data_Cref_Mailed_Ind;
    private DbsField pnd_Mdm_Data_Online_Enrollment_Ind;
    private DbsField pnd_Mdm_Data_Address_Line_1;
    private DbsField pnd_Mdm_Data_Address_Line_2;
    private DbsField pnd_Mdm_Data_Address_Line_3;
    private DbsField pnd_Mdm_Data_Address_Line_4;
    private DbsField pnd_Mdm_Data_Address_Line_5;
    private DbsField pnd_Mdm_Data_Zip_Code;
    private DbsField pnd_Mdm_Data_City;
    private DbsField pnd_Mdm_Data_State;
    private DbsField pnd_Mdm_Data_Plan_Nbr;
    private DbsField pnd_Mdm_Data_Sub_Plan_Nbr;
    private DbsField pnd_Mdm_Data_Home_Phone_Nbr;
    private DbsField pnd_Mdm_Data_Work_Phone_Nbr;
    private DbsField pnd_Mdm_Data_Country_Code;
    private DbsField pnd_Mdm_Data_Country_Code_Type;
    private DbsField pnd_Mdm_Data_Mail_Type;
    private DbsField pnd_Mdm_Data_R_Address_Line_1;
    private DbsField pnd_Mdm_Data_R_Address_Line_2;
    private DbsField pnd_Mdm_Data_R_Address_Line_3;
    private DbsField pnd_Mdm_Data_R_Address_Line_4;
    private DbsField pnd_Mdm_Data_R_Address_Line_5;
    private DbsField pnd_Mdm_Data_R_Zip_Code;
    private DbsField pnd_Mdm_Data_R_City;
    private DbsField pnd_Mdm_Data_R_State;
    private DbsField pnd_Mdm_Data_R_Country_Code;
    private DbsField pnd_Mdm_Data_R_Country_Code_Type;
    private DbsField pnd_Mdm_Data_R_Mail_Type;
    private DbsField pnd_Mdm_Data_Email_Address;
    private DbsField pnd_Mdm_Data_Last_Name;
    private DbsField pnd_Mdm_Data_Date_Of_Birth;
    private DbsField pnd_Mdm_Data_Conversion_Type;
    private DbsField pnd_Mdm_Data_Status_Year;
    private DbsField pnd_Mdm_Data_Default_Enroll;
    private DbsField pnd_Mdm_Data_Orig_Contract_Num;
    private DbsField pnd_Mdm_Data_Orig_Cref_Num;
    private DbsField pnd_Mdm_Data_Divsub_Num;
    private DbsField pnd_Mdm_Data_Ownership;
    private DbsField pnd_Mdm_Data_Bip_Decedent_Contract;
    private DbsField pnd_Mdm_Data_Bip_Relation_To_Decedent;
    private DbsField pnd_Mdm_Data_Bip_Ssn_Tin_Ind;
    private DbsField pnd_Mdm_Data_Bip_Acceptance_Ind;

    public DbsGroup getPnd_Mdm_Data() { return pnd_Mdm_Data; }

    public DbsField getPnd_Mdm_Data_Requestor() { return pnd_Mdm_Data_Requestor; }

    public DbsField getPnd_Mdm_Data_Function_Code() { return pnd_Mdm_Data_Function_Code; }

    public DbsField getPnd_Mdm_Data_Source_System_Nm() { return pnd_Mdm_Data_Source_System_Nm; }

    public DbsField getPnd_Mdm_Data_Foreign_Soc_Sec_Nbr() { return pnd_Mdm_Data_Foreign_Soc_Sec_Nbr; }

    public DbsField getPnd_Mdm_Data_Ph_Unique_Id_Nbr() { return pnd_Mdm_Data_Ph_Unique_Id_Nbr; }

    public DbsField getPnd_Mdm_Data_Ph_Social_Security_No() { return pnd_Mdm_Data_Ph_Social_Security_No; }

    public DbsField getPnd_Mdm_Data_Cntrct_Nbr() { return pnd_Mdm_Data_Cntrct_Nbr; }

    public DbsField getPnd_Mdm_Data_Cntrct_Payee_Cde() { return pnd_Mdm_Data_Cntrct_Payee_Cde; }

    public DbsField getPnd_Mdm_Data_Contract_Party_Role() { return pnd_Mdm_Data_Contract_Party_Role; }

    public DbsField getPnd_Mdm_Data_Cntrct_Status_Cde() { return pnd_Mdm_Data_Cntrct_Status_Cde; }

    public DbsField getPnd_Mdm_Data_Cntrct_Issue_Dte() { return pnd_Mdm_Data_Cntrct_Issue_Dte; }

    public DbsField getPnd_Mdm_Data_Cntrct_Lob_Cde() { return pnd_Mdm_Data_Cntrct_Lob_Cde; }

    public DbsField getPnd_Mdm_Data_Cntrct_Cref_Nbr() { return pnd_Mdm_Data_Cntrct_Cref_Nbr; }

    public DbsField getPnd_Mdm_Data_Cntrct_Cref_Issued_Ind() { return pnd_Mdm_Data_Cntrct_Cref_Issued_Ind; }

    public DbsField getPnd_Mdm_Data_Cntrct_Indctrs_And_Codes() { return pnd_Mdm_Data_Cntrct_Indctrs_And_Codes; }

    public DbsField getPnd_Mdm_Data_Cntrct_Optn_Cde() { return pnd_Mdm_Data_Cntrct_Optn_Cde; }

    public DbsField getPnd_Mdm_Data_Cntrct_Plan_Cde() { return pnd_Mdm_Data_Cntrct_Plan_Cde; }

    public DbsField getPnd_Mdm_Data_Cntrct_Fund_Cde() { return pnd_Mdm_Data_Cntrct_Fund_Cde; }

    public DbsField getPnd_Mdm_Data_Cntrct_Social_Cde() { return pnd_Mdm_Data_Cntrct_Social_Cde; }

    public DbsField getPnd_Mdm_Data_Cust_Agreement_Received_Ind() { return pnd_Mdm_Data_Cust_Agreement_Received_Ind; }

    public DbsField getPnd_Mdm_Data_Asset_Allocation_Service_Ind() { return pnd_Mdm_Data_Asset_Allocation_Service_Ind; }

    public DbsField getPnd_Mdm_Data_Platform_Ind() { return pnd_Mdm_Data_Platform_Ind; }

    public DbsField getPnd_Mdm_Data_Multi_Source_Ind() { return pnd_Mdm_Data_Multi_Source_Ind; }

    public DbsField getPnd_Mdm_Data_Tiaa_Mailed_Ind() { return pnd_Mdm_Data_Tiaa_Mailed_Ind; }

    public DbsField getPnd_Mdm_Data_Cref_Mailed_Ind() { return pnd_Mdm_Data_Cref_Mailed_Ind; }

    public DbsField getPnd_Mdm_Data_Online_Enrollment_Ind() { return pnd_Mdm_Data_Online_Enrollment_Ind; }

    public DbsField getPnd_Mdm_Data_Address_Line_1() { return pnd_Mdm_Data_Address_Line_1; }

    public DbsField getPnd_Mdm_Data_Address_Line_2() { return pnd_Mdm_Data_Address_Line_2; }

    public DbsField getPnd_Mdm_Data_Address_Line_3() { return pnd_Mdm_Data_Address_Line_3; }

    public DbsField getPnd_Mdm_Data_Address_Line_4() { return pnd_Mdm_Data_Address_Line_4; }

    public DbsField getPnd_Mdm_Data_Address_Line_5() { return pnd_Mdm_Data_Address_Line_5; }

    public DbsField getPnd_Mdm_Data_Zip_Code() { return pnd_Mdm_Data_Zip_Code; }

    public DbsField getPnd_Mdm_Data_City() { return pnd_Mdm_Data_City; }

    public DbsField getPnd_Mdm_Data_State() { return pnd_Mdm_Data_State; }

    public DbsField getPnd_Mdm_Data_Plan_Nbr() { return pnd_Mdm_Data_Plan_Nbr; }

    public DbsField getPnd_Mdm_Data_Sub_Plan_Nbr() { return pnd_Mdm_Data_Sub_Plan_Nbr; }

    public DbsField getPnd_Mdm_Data_Home_Phone_Nbr() { return pnd_Mdm_Data_Home_Phone_Nbr; }

    public DbsField getPnd_Mdm_Data_Work_Phone_Nbr() { return pnd_Mdm_Data_Work_Phone_Nbr; }

    public DbsField getPnd_Mdm_Data_Country_Code() { return pnd_Mdm_Data_Country_Code; }

    public DbsField getPnd_Mdm_Data_Country_Code_Type() { return pnd_Mdm_Data_Country_Code_Type; }

    public DbsField getPnd_Mdm_Data_Mail_Type() { return pnd_Mdm_Data_Mail_Type; }

    public DbsField getPnd_Mdm_Data_R_Address_Line_1() { return pnd_Mdm_Data_R_Address_Line_1; }

    public DbsField getPnd_Mdm_Data_R_Address_Line_2() { return pnd_Mdm_Data_R_Address_Line_2; }

    public DbsField getPnd_Mdm_Data_R_Address_Line_3() { return pnd_Mdm_Data_R_Address_Line_3; }

    public DbsField getPnd_Mdm_Data_R_Address_Line_4() { return pnd_Mdm_Data_R_Address_Line_4; }

    public DbsField getPnd_Mdm_Data_R_Address_Line_5() { return pnd_Mdm_Data_R_Address_Line_5; }

    public DbsField getPnd_Mdm_Data_R_Zip_Code() { return pnd_Mdm_Data_R_Zip_Code; }

    public DbsField getPnd_Mdm_Data_R_City() { return pnd_Mdm_Data_R_City; }

    public DbsField getPnd_Mdm_Data_R_State() { return pnd_Mdm_Data_R_State; }

    public DbsField getPnd_Mdm_Data_R_Country_Code() { return pnd_Mdm_Data_R_Country_Code; }

    public DbsField getPnd_Mdm_Data_R_Country_Code_Type() { return pnd_Mdm_Data_R_Country_Code_Type; }

    public DbsField getPnd_Mdm_Data_R_Mail_Type() { return pnd_Mdm_Data_R_Mail_Type; }

    public DbsField getPnd_Mdm_Data_Email_Address() { return pnd_Mdm_Data_Email_Address; }

    public DbsField getPnd_Mdm_Data_Last_Name() { return pnd_Mdm_Data_Last_Name; }

    public DbsField getPnd_Mdm_Data_Date_Of_Birth() { return pnd_Mdm_Data_Date_Of_Birth; }

    public DbsField getPnd_Mdm_Data_Conversion_Type() { return pnd_Mdm_Data_Conversion_Type; }

    public DbsField getPnd_Mdm_Data_Status_Year() { return pnd_Mdm_Data_Status_Year; }

    public DbsField getPnd_Mdm_Data_Default_Enroll() { return pnd_Mdm_Data_Default_Enroll; }

    public DbsField getPnd_Mdm_Data_Orig_Contract_Num() { return pnd_Mdm_Data_Orig_Contract_Num; }

    public DbsField getPnd_Mdm_Data_Orig_Cref_Num() { return pnd_Mdm_Data_Orig_Cref_Num; }

    public DbsField getPnd_Mdm_Data_Divsub_Num() { return pnd_Mdm_Data_Divsub_Num; }

    public DbsField getPnd_Mdm_Data_Ownership() { return pnd_Mdm_Data_Ownership; }

    public DbsField getPnd_Mdm_Data_Bip_Decedent_Contract() { return pnd_Mdm_Data_Bip_Decedent_Contract; }

    public DbsField getPnd_Mdm_Data_Bip_Relation_To_Decedent() { return pnd_Mdm_Data_Bip_Relation_To_Decedent; }

    public DbsField getPnd_Mdm_Data_Bip_Ssn_Tin_Ind() { return pnd_Mdm_Data_Bip_Ssn_Tin_Ind; }

    public DbsField getPnd_Mdm_Data_Bip_Acceptance_Ind() { return pnd_Mdm_Data_Bip_Acceptance_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Mdm_Data = newGroupInRecord("pnd_Mdm_Data", "#MDM-DATA");
        pnd_Mdm_Data_Requestor = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Requestor", "REQUESTOR", FieldType.STRING, 8);
        pnd_Mdm_Data_Function_Code = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Function_Code", "FUNCTION_CODE", FieldType.STRING, 3);
        pnd_Mdm_Data_Source_System_Nm = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Source_System_Nm", "SOURCE_SYSTEM_NM", FieldType.STRING, 40);
        pnd_Mdm_Data_Foreign_Soc_Sec_Nbr = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Foreign_Soc_Sec_Nbr", "FOREIGN_SOC_SEC_NBR", FieldType.STRING, 9);
        pnd_Mdm_Data_Ph_Unique_Id_Nbr = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Ph_Unique_Id_Nbr", "PH_UNIQUE_ID_NBR", FieldType.STRING, 12);
        pnd_Mdm_Data_Ph_Social_Security_No = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Ph_Social_Security_No", "PH_SOCIAL_SECURITY_NO", FieldType.STRING, 
            9);
        pnd_Mdm_Data_Cntrct_Nbr = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Cntrct_Nbr", "CNTRCT_NBR", FieldType.STRING, 10);
        pnd_Mdm_Data_Cntrct_Payee_Cde = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Cntrct_Payee_Cde", "CNTRCT_PAYEE_CDE", FieldType.STRING, 2);
        pnd_Mdm_Data_Contract_Party_Role = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Contract_Party_Role", "CONTRACT_PARTY_ROLE", FieldType.STRING, 20);
        pnd_Mdm_Data_Cntrct_Status_Cde = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Cntrct_Status_Cde", "CNTRCT_STATUS_CDE", FieldType.STRING, 1);
        pnd_Mdm_Data_Cntrct_Issue_Dte = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Cntrct_Issue_Dte", "CNTRCT_ISSUE_DTE", FieldType.STRING, 8);
        pnd_Mdm_Data_Cntrct_Lob_Cde = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Cntrct_Lob_Cde", "CNTRCT_LOB_CDE", FieldType.STRING, 1);
        pnd_Mdm_Data_Cntrct_Cref_Nbr = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Cntrct_Cref_Nbr", "CNTRCT_CREF_NBR", FieldType.STRING, 10);
        pnd_Mdm_Data_Cntrct_Cref_Issued_Ind = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Cntrct_Cref_Issued_Ind", "CNTRCT_CREF_ISSUED_IND", FieldType.STRING, 
            1);
        pnd_Mdm_Data_Cntrct_Indctrs_And_Codes = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Cntrct_Indctrs_And_Codes", "CNTRCT_INDCTRS_AND_CODES", FieldType.STRING, 
            1);
        pnd_Mdm_Data_Cntrct_Optn_Cde = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Cntrct_Optn_Cde", "CNTRCT_OPTN_CDE", FieldType.STRING, 2);
        pnd_Mdm_Data_Cntrct_Plan_Cde = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Cntrct_Plan_Cde", "CNTRCT_PLAN_CDE", FieldType.STRING, 5);
        pnd_Mdm_Data_Cntrct_Fund_Cde = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Cntrct_Fund_Cde", "CNTRCT_FUND_CDE", FieldType.STRING, 3);
        pnd_Mdm_Data_Cntrct_Social_Cde = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Cntrct_Social_Cde", "CNTRCT_SOCIAL_CDE", FieldType.STRING, 3);
        pnd_Mdm_Data_Cust_Agreement_Received_Ind = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Cust_Agreement_Received_Ind", "CUST_AGREEMENT_RECEIVED_IND", 
            FieldType.STRING, 1);
        pnd_Mdm_Data_Asset_Allocation_Service_Ind = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Asset_Allocation_Service_Ind", "ASSET_ALLOCATION_SERVICE_IND", 
            FieldType.STRING, 1);
        pnd_Mdm_Data_Platform_Ind = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Platform_Ind", "PLATFORM_IND", FieldType.STRING, 1);
        pnd_Mdm_Data_Multi_Source_Ind = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Multi_Source_Ind", "MULTI_SOURCE_IND", FieldType.STRING, 1);
        pnd_Mdm_Data_Tiaa_Mailed_Ind = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Tiaa_Mailed_Ind", "TIAA_MAILED_IND", FieldType.STRING, 1);
        pnd_Mdm_Data_Cref_Mailed_Ind = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Cref_Mailed_Ind", "CREF_MAILED_IND", FieldType.STRING, 1);
        pnd_Mdm_Data_Online_Enrollment_Ind = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Online_Enrollment_Ind", "ONLINE_ENROLLMENT_IND", FieldType.STRING, 
            1);
        pnd_Mdm_Data_Address_Line_1 = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Address_Line_1", "ADDRESS_LINE_1", FieldType.STRING, 35);
        pnd_Mdm_Data_Address_Line_2 = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Address_Line_2", "ADDRESS_LINE_2", FieldType.STRING, 35);
        pnd_Mdm_Data_Address_Line_3 = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Address_Line_3", "ADDRESS_LINE_3", FieldType.STRING, 35);
        pnd_Mdm_Data_Address_Line_4 = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Address_Line_4", "ADDRESS_LINE_4", FieldType.STRING, 35);
        pnd_Mdm_Data_Address_Line_5 = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Address_Line_5", "ADDRESS_LINE_5", FieldType.STRING, 35);
        pnd_Mdm_Data_Zip_Code = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Zip_Code", "ZIP_CODE", FieldType.STRING, 9);
        pnd_Mdm_Data_City = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_City", "CITY", FieldType.STRING, 35);
        pnd_Mdm_Data_State = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_State", "STATE", FieldType.STRING, 3);
        pnd_Mdm_Data_Plan_Nbr = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Plan_Nbr", "PLAN_NBR", FieldType.STRING, 6);
        pnd_Mdm_Data_Sub_Plan_Nbr = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Sub_Plan_Nbr", "SUB_PLAN_NBR", FieldType.STRING, 6);
        pnd_Mdm_Data_Home_Phone_Nbr = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Home_Phone_Nbr", "HOME_PHONE_NBR", FieldType.STRING, 10);
        pnd_Mdm_Data_Work_Phone_Nbr = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Work_Phone_Nbr", "WORK_PHONE_NBR", FieldType.STRING, 14);
        pnd_Mdm_Data_Country_Code = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Country_Code", "COUNTRY_CODE", FieldType.STRING, 3);
        pnd_Mdm_Data_Country_Code_Type = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Country_Code_Type", "COUNTRY_CODE_TYPE", FieldType.STRING, 1);
        pnd_Mdm_Data_Mail_Type = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Mail_Type", "MAIL_TYPE", FieldType.STRING, 1);
        pnd_Mdm_Data_R_Address_Line_1 = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_R_Address_Line_1", "R_ADDRESS_LINE_1", FieldType.STRING, 35);
        pnd_Mdm_Data_R_Address_Line_2 = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_R_Address_Line_2", "R_ADDRESS_LINE_2", FieldType.STRING, 35);
        pnd_Mdm_Data_R_Address_Line_3 = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_R_Address_Line_3", "R_ADDRESS_LINE_3", FieldType.STRING, 35);
        pnd_Mdm_Data_R_Address_Line_4 = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_R_Address_Line_4", "R_ADDRESS_LINE_4", FieldType.STRING, 35);
        pnd_Mdm_Data_R_Address_Line_5 = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_R_Address_Line_5", "R_ADDRESS_LINE_5", FieldType.STRING, 35);
        pnd_Mdm_Data_R_Zip_Code = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_R_Zip_Code", "R_ZIP_CODE", FieldType.STRING, 9);
        pnd_Mdm_Data_R_City = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_R_City", "R_CITY", FieldType.STRING, 35);
        pnd_Mdm_Data_R_State = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_R_State", "R_STATE", FieldType.STRING, 3);
        pnd_Mdm_Data_R_Country_Code = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_R_Country_Code", "R_COUNTRY_CODE", FieldType.STRING, 3);
        pnd_Mdm_Data_R_Country_Code_Type = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_R_Country_Code_Type", "R_COUNTRY_CODE_TYPE", FieldType.STRING, 1);
        pnd_Mdm_Data_R_Mail_Type = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_R_Mail_Type", "R_MAIL_TYPE", FieldType.STRING, 1);
        pnd_Mdm_Data_Email_Address = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Email_Address", "EMAIL_ADDRESS", FieldType.STRING, 50);
        pnd_Mdm_Data_Last_Name = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Last_Name", "LAST_NAME", FieldType.STRING, 35);
        pnd_Mdm_Data_Date_Of_Birth = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Date_Of_Birth", "DATE_OF_BIRTH", FieldType.NUMERIC, 8);
        pnd_Mdm_Data_Conversion_Type = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Conversion_Type", "CONVERSION_TYPE", FieldType.STRING, 1);
        pnd_Mdm_Data_Status_Year = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Status_Year", "STATUS_YEAR", FieldType.NUMERIC, 4);
        pnd_Mdm_Data_Default_Enroll = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Default_Enroll", "DEFAULT_ENROLL", FieldType.STRING, 1);
        pnd_Mdm_Data_Orig_Contract_Num = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Orig_Contract_Num", "ORIG_CONTRACT_NUM", FieldType.STRING, 10);
        pnd_Mdm_Data_Orig_Cref_Num = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Orig_Cref_Num", "ORIG_CREF_NUM", FieldType.STRING, 10);
        pnd_Mdm_Data_Divsub_Num = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Divsub_Num", "DIVSUB_NUM", FieldType.STRING, 4);
        pnd_Mdm_Data_Ownership = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Ownership", "OWNERSHIP", FieldType.STRING, 1);
        pnd_Mdm_Data_Bip_Decedent_Contract = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Bip_Decedent_Contract", "BIP_DECEDENT_CONTRACT", FieldType.STRING, 
            10);
        pnd_Mdm_Data_Bip_Relation_To_Decedent = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Bip_Relation_To_Decedent", "BIP_RELATION_TO_DECEDENT", FieldType.STRING, 
            1);
        pnd_Mdm_Data_Bip_Ssn_Tin_Ind = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Bip_Ssn_Tin_Ind", "BIP_SSN_TIN_IND", FieldType.STRING, 1);
        pnd_Mdm_Data_Bip_Acceptance_Ind = pnd_Mdm_Data.newFieldInGroup("pnd_Mdm_Data_Bip_Acceptance_Ind", "BIP_ACCEPTANCE_IND", FieldType.STRING, 1);

        this.setRecordName("LdaAppbl170");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAppbl170() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
