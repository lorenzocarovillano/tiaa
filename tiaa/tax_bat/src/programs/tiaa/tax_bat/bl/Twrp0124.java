/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:26:24 PM
**        * FROM NATURAL PROGRAM : Twrp0124
************************************************************
**        * FILE NAME            : Twrp0124.java
**        * CLASS NAME           : Twrp0124
**        * INSTANCE NAME        : Twrp0124
************************************************************
************************************************************************
**
** PROGRAM:  TWRP0124 - FORM SELECTION (WAS TWRP0117, FOR 480.6A)
** SYSTEM :  TAX WITHHOLDING & REPORTING SYSTEM
** AUTHOR :  A.WILNER
** PURPOSE:  MAGNETIC REPORT TO PUERTO RICO
**                '480.6A'  =  FORM NUMBER 5
**                '480.6B'  =  FORM NUMBER 6
**
** SPECIAL:  MUST USE INPUT PARAMETERS TO WHICH PROCESSING FORM
**
** HISTORY.....:
** --------------------------------------------------------------------
** 11/11/2005  MS REMOVE UNUSED LDAS.
** 02/15/2001  FELIX ORTIZ
**             TAX YEAR IS NOW AN OPTIONAL INPUT PARAMETER. SYSTEM
**             CALCULATES TAX YEAR WHEN PARAMETER IS NOT PASSED.
**             THIS DATE IS USED TO POPULATE THE OUTPUT FILE.
** 02/08/2002  JHH - ADD DETAIL REPORT AND TOTALS
** 08/13/2002  J.ROTHOLZ - RECOMPILED DUE TO INCREASE IN #COMP-NAME
**             LENGTH IN TWRLCOMP & TWRACOMP
** 09/10/2002  BOTTERI - CHANGE CALL TWRACOMP TO TWRACOM2 & ADD BREAK
**             BY COMPANY CODE AND ACCUMULATORS/DISPLAYS FOR EACH.
** 11/19/02 - MN - RECOMPILED DUE TO FIELDS ADDED TO TWRL5001
** 12/09/02 - EB - RECOMPILED DUE TO FIELDS ADDED TO TWRL117A & 117B,
**                 ALSO RENAMED COLUMN 'Gross' TO 'Pension'.
** 02/05/03 - RM - CHANGE FIELD IN 480.5, UPDATE TWRL117A AND CHANGE
**                 COMPANY NAME INDIRECTLY VIA COMPANY TABLE.
** 02/08/05 - AY - REVISED TO POPULATE #TWRACOM2.#TAX-YEAR BEFORE
**                 CALLING COMPANY CODE TABLE.
**               - REVISED TO CALL COMPANY CODE TABLE (ONCE) WITH 'S'
**                 VALUE TO GET AND STORE 'Services' DATA.
**               - REVISED TO PRINT REPORTS WITH FORM FILE COMPANY DATA,
**                 WHILE WRITING OUTPUT DETAIL AND SUMMARY RECORDS
**                 WITH 'S' (SERVICES) DATA.  COMPANY CODE TABLE VALUES
**                 REPLACED HARD-CODING WHEREVER APPLICABLE.
** 02/01/06 - RM - UPDATE TWRL117A TO ADD NEW FIELDS AND EXPAND RECORD
**                 LENGTH FROM 700 TO 2000, FOR 2005 NEW 480.6A & 480.5
**                 LAYOUT.
**                 CHANGE RELATED CODING TO ACCORMODATE THE NEW OR
**                 CHANGED FIELDS.
**                 ALTHOUGH THERE ARE NO 480.6B FORMS BEING PRODUCED
**                 CURRENTLY, TWRL117B IS UPDATED FOR THE NEW FIELDS
**                 AND NEW RECORD LENGTH.
** 02/20/07 - RM - CHANGES WERE DONE FOR 2006 TO ACCORMODATE PR REQUIRE-
**                 MENT OF E-FILE VIA MOBIUS. BUT USER LATER FOUND THAT
**                 WAS NOT APPLICABLE FOR TIAA. THEREFORE, PROCESS AND
**                 PROGRAMS HAD TO BE REVERTED BACK TO 2005 VERSION.
**                 CHANGED MODULES SAVED TO 'TEM*'.
**                 MINOR CHANGES DONE IN TWRL117A AND THIS PGM FOR 2006.
**                 NO CHANGES FOR FORM 480.6B YET.
** 11/24/2020 - PALDE - RESTOW FOR IRS REPORTING 2020 CHANGES
***********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0124 extends BLNatBase
{
    // Data Areas
    private LdaTwrl117a ldaTwrl117a;
    private LdaTwrl117b ldaTwrl117b;
    private LdaTwrl9710 ldaTwrl9710;
    private PdaTwracom2 pdaTwracom2;
    private PdaTwratbl2 pdaTwratbl2;
    private PdaTwratbl4 pdaTwratbl4;
    private LdaTwrltb4r ldaTwrltb4r;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_form1;
    private DbsField form1_Tirf_Irs_Rpt_Ind;
    private DbsField form1_Tirf_Irs_Rpt_Date;
    private DbsField form1_Tirf_Lu_User;
    private DbsField form1_Tirf_Lu_Ts;
    private DbsField pnd_In_Parm;

    private DbsGroup pnd_In_Parm__R_Field_1;
    private DbsField pnd_In_Parm_Pnd_In_Active_Ind;
    private DbsField pnd_In_Parm_Pnd_In_Form_Type;
    private DbsField pnd_In_Parm_Pnd_In_Inc_Old;
    private DbsField pnd_In_Parm_Pnd_In_Update;
    private DbsField pnd_In_Parm_Pnd_In_Tax_Year;

    private DbsGroup pnd_In_Parm__R_Field_2;
    private DbsField pnd_In_Parm_Pnd_In_Tax_Yearn;
    private DbsField pnd_Tirf_Superde_3;

    private DbsGroup pnd_Tirf_Superde_3__R_Field_3;
    private DbsField pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year;

    private DbsGroup pnd_Tirf_Superde_3__R_Field_4;
    private DbsField pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn;
    private DbsField pnd_Tirf_Superde_3_Pnd_Tirf_Active_Ind;
    private DbsField pnd_Tirf_Superde_3_Pnd_Tirf_Form_Type;

    private DbsGroup pnd_Work_Area;
    private DbsField pnd_Work_Area_Pnd_Ws_Et;
    private DbsField pnd_Work_Area_Pnd_Ws_Isn;
    private DbsField pnd_Work_Area_Pnd_Temp_Tax_Year;

    private DbsGroup pnd_Work_Area__R_Field_5;
    private DbsField pnd_Work_Area_Pnd_Temp_Tax_Yearcc;
    private DbsField pnd_Work_Area_Pnd_Temp_Tax_Yearyy;
    private DbsField pnd_Work_Area_Pnd_Ws_Num_Of_Doc;
    private DbsField pnd_Work_Area_Pnd_Ws_Amt_Withld;
    private DbsField pnd_Work_Area_Pnd_Ws_Amout_Paid;
    private DbsField pnd_Work_Area_Pnd_Ws_Amount;
    private DbsField pnd_Work_Area_Pnd_Empty_Form_Cnt;
    private DbsField pnd_Work_Area_Pnd_Reject;
    private DbsField pnd_Work_Area_Pnd_Rej_Count;
    private DbsField pnd_Work_Area_Pnd_Old_Cmpny;
    private DbsField pnd_Work_Area_Pnd_Counter;
    private DbsField pnd_Work_Area_Pnd_Cmpnys;
    private DbsField pnd_Work_Area_Pnd_I;
    private DbsField pnd_Work_Area_Pnd_First;
    private DbsField pnd_Work_Area_Pnd_Last;

    private DbsGroup pnd_Work_Area_Pnd_Ws_Totales;
    private DbsField pnd_Work_Area_Pnd_Ws_Cnt_Tot;
    private DbsField pnd_Work_Area_Pnd_Ws_Gross_Tot;
    private DbsField pnd_Work_Area_Pnd_Ws_Int_Tot;
    private DbsField pnd_Work_Area_Pnd_Ws_Cnt;
    private DbsField pnd_Work_Area_Pnd_Ws_Gross;
    private DbsField pnd_Work_Area_Pnd_Ws_Int;

    private DbsGroup pnd_S_Output_Data;
    private DbsField pnd_S_Output_Data_Pnd_Ret_Code;
    private DbsField pnd_S_Output_Data_Pnd_Ret_Msg;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Short_Name;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Name;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Corr_Name;
    private DbsField pnd_S_Output_Data_Pnd_Fed_Id;
    private DbsField pnd_S_Output_Data_Pnd_Nr4_Id;
    private DbsField pnd_S_Output_Data_Pnd_Contact_Name;
    private DbsField pnd_S_Output_Data_Pnd_Contact_Email_Addr;
    private DbsField pnd_S_Output_Data_Pnd_Phone;
    private DbsField pnd_S_Output_Data_Pnd_Address;
    private DbsField pnd_S_Output_Data_Pnd_City;
    private DbsField pnd_S_Output_Data_Pnd_State;
    private DbsField pnd_S_Output_Data_Pnd_Zip_9;

    private DbsGroup pnd_S_Output_Data__R_Field_6;
    private DbsField pnd_S_Output_Data_Pnd_Zip;
    private DbsField pnd_S_Output_Data_Pnd_Zip_4;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Tcc;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Trans_A;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Trans_B;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Payer_A;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Payer_B;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Agent_A;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Agent_B;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Retrn_A;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Retrn_B;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Futr1_A;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Futr1_B;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Futr2_A;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Futr2_B;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl117a = new LdaTwrl117a();
        registerRecord(ldaTwrl117a);
        ldaTwrl117b = new LdaTwrl117b();
        registerRecord(ldaTwrl117b);
        ldaTwrl9710 = new LdaTwrl9710();
        registerRecord(ldaTwrl9710);
        registerRecord(ldaTwrl9710.getVw_form());
        localVariables = new DbsRecord();
        pdaTwracom2 = new PdaTwracom2(localVariables);
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        pdaTwratbl4 = new PdaTwratbl4(localVariables);
        ldaTwrltb4r = new LdaTwrltb4r();
        registerRecord(ldaTwrltb4r);
        registerRecord(ldaTwrltb4r.getVw_tircntl_Rpt());

        // Local Variables

        vw_form1 = new DataAccessProgramView(new NameInfo("vw_form1", "FORM1"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form1_Tirf_Irs_Rpt_Ind = vw_form1.getRecord().newFieldInGroup("form1_Tirf_Irs_Rpt_Ind", "TIRF-IRS-RPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_IND");
        form1_Tirf_Irs_Rpt_Ind.setDdmHeader("IRS/RPT/IND");
        form1_Tirf_Irs_Rpt_Date = vw_form1.getRecord().newFieldInGroup("form1_Tirf_Irs_Rpt_Date", "TIRF-IRS-RPT-DATE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_DATE");
        form1_Tirf_Irs_Rpt_Date.setDdmHeader("IRS/RPT/DATE");
        form1_Tirf_Lu_User = vw_form1.getRecord().newFieldInGroup("form1_Tirf_Lu_User", "TIRF-LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_LU_USER");
        form1_Tirf_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        form1_Tirf_Lu_Ts = vw_form1.getRecord().newFieldInGroup("form1_Tirf_Lu_Ts", "TIRF-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "TIRF_LU_TS");
        form1_Tirf_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        registerRecord(vw_form1);

        pnd_In_Parm = localVariables.newFieldInRecord("pnd_In_Parm", "#IN-PARM", FieldType.STRING, 79);

        pnd_In_Parm__R_Field_1 = localVariables.newGroupInRecord("pnd_In_Parm__R_Field_1", "REDEFINE", pnd_In_Parm);
        pnd_In_Parm_Pnd_In_Active_Ind = pnd_In_Parm__R_Field_1.newFieldInGroup("pnd_In_Parm_Pnd_In_Active_Ind", "#IN-ACTIVE-IND", FieldType.STRING, 1);
        pnd_In_Parm_Pnd_In_Form_Type = pnd_In_Parm__R_Field_1.newFieldInGroup("pnd_In_Parm_Pnd_In_Form_Type", "#IN-FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_In_Parm_Pnd_In_Inc_Old = pnd_In_Parm__R_Field_1.newFieldInGroup("pnd_In_Parm_Pnd_In_Inc_Old", "#IN-INC-OLD", FieldType.STRING, 1);
        pnd_In_Parm_Pnd_In_Update = pnd_In_Parm__R_Field_1.newFieldInGroup("pnd_In_Parm_Pnd_In_Update", "#IN-UPDATE", FieldType.STRING, 1);
        pnd_In_Parm_Pnd_In_Tax_Year = pnd_In_Parm__R_Field_1.newFieldInGroup("pnd_In_Parm_Pnd_In_Tax_Year", "#IN-TAX-YEAR", FieldType.STRING, 4);

        pnd_In_Parm__R_Field_2 = pnd_In_Parm__R_Field_1.newGroupInGroup("pnd_In_Parm__R_Field_2", "REDEFINE", pnd_In_Parm_Pnd_In_Tax_Year);
        pnd_In_Parm_Pnd_In_Tax_Yearn = pnd_In_Parm__R_Field_2.newFieldInGroup("pnd_In_Parm_Pnd_In_Tax_Yearn", "#IN-TAX-YEARN", FieldType.NUMERIC, 4);
        pnd_Tirf_Superde_3 = localVariables.newFieldInRecord("pnd_Tirf_Superde_3", "#TIRF-SUPERDE-3", FieldType.STRING, 33);

        pnd_Tirf_Superde_3__R_Field_3 = localVariables.newGroupInRecord("pnd_Tirf_Superde_3__R_Field_3", "REDEFINE", pnd_Tirf_Superde_3);
        pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year = pnd_Tirf_Superde_3__R_Field_3.newFieldInGroup("pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year", "#TIRF-TAX-YEAR", 
            FieldType.STRING, 4);

        pnd_Tirf_Superde_3__R_Field_4 = pnd_Tirf_Superde_3__R_Field_3.newGroupInGroup("pnd_Tirf_Superde_3__R_Field_4", "REDEFINE", pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year);
        pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn = pnd_Tirf_Superde_3__R_Field_4.newFieldInGroup("pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn", "#TIRF-TAX-YEARN", 
            FieldType.NUMERIC, 4);
        pnd_Tirf_Superde_3_Pnd_Tirf_Active_Ind = pnd_Tirf_Superde_3__R_Field_3.newFieldInGroup("pnd_Tirf_Superde_3_Pnd_Tirf_Active_Ind", "#TIRF-ACTIVE-IND", 
            FieldType.STRING, 1);
        pnd_Tirf_Superde_3_Pnd_Tirf_Form_Type = pnd_Tirf_Superde_3__R_Field_3.newFieldInGroup("pnd_Tirf_Superde_3_Pnd_Tirf_Form_Type", "#TIRF-FORM-TYPE", 
            FieldType.NUMERIC, 2);

        pnd_Work_Area = localVariables.newGroupInRecord("pnd_Work_Area", "#WORK-AREA");
        pnd_Work_Area_Pnd_Ws_Et = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Et", "#WS-ET", FieldType.PACKED_DECIMAL, 5);
        pnd_Work_Area_Pnd_Ws_Isn = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Isn", "#WS-ISN", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Temp_Tax_Year = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Temp_Tax_Year", "#TEMP-TAX-YEAR", FieldType.NUMERIC, 4);

        pnd_Work_Area__R_Field_5 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area__R_Field_5", "REDEFINE", pnd_Work_Area_Pnd_Temp_Tax_Year);
        pnd_Work_Area_Pnd_Temp_Tax_Yearcc = pnd_Work_Area__R_Field_5.newFieldInGroup("pnd_Work_Area_Pnd_Temp_Tax_Yearcc", "#TEMP-TAX-YEARCC", FieldType.NUMERIC, 
            2);
        pnd_Work_Area_Pnd_Temp_Tax_Yearyy = pnd_Work_Area__R_Field_5.newFieldInGroup("pnd_Work_Area_Pnd_Temp_Tax_Yearyy", "#TEMP-TAX-YEARYY", FieldType.NUMERIC, 
            2);
        pnd_Work_Area_Pnd_Ws_Num_Of_Doc = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Num_Of_Doc", "#WS-NUM-OF-DOC", FieldType.NUMERIC, 10);
        pnd_Work_Area_Pnd_Ws_Amt_Withld = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Amt_Withld", "#WS-AMT-WITHLD", FieldType.NUMERIC, 15, 2);
        pnd_Work_Area_Pnd_Ws_Amout_Paid = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Amout_Paid", "#WS-AMOUT-PAID", FieldType.NUMERIC, 15, 2);
        pnd_Work_Area_Pnd_Ws_Amount = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Amount", "#WS-AMOUNT", FieldType.NUMERIC, 12);
        pnd_Work_Area_Pnd_Empty_Form_Cnt = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Empty_Form_Cnt", "#EMPTY-FORM-CNT", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Work_Area_Pnd_Reject = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Reject", "#REJECT", FieldType.BOOLEAN, 1);
        pnd_Work_Area_Pnd_Rej_Count = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Rej_Count", "#REJ-COUNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Work_Area_Pnd_Old_Cmpny = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Old_Cmpny", "#OLD-CMPNY", FieldType.STRING, 4);
        pnd_Work_Area_Pnd_Counter = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Counter", "#COUNTER", FieldType.PACKED_DECIMAL, 9);
        pnd_Work_Area_Pnd_Cmpnys = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Cmpnys", "#CMPNYS", FieldType.NUMERIC, 2);
        pnd_Work_Area_Pnd_I = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 9);
        pnd_Work_Area_Pnd_First = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Work_Area_Pnd_Last = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Last", "#LAST", FieldType.BOOLEAN, 1);

        pnd_Work_Area_Pnd_Ws_Totales = pnd_Work_Area.newGroupArrayInGroup("pnd_Work_Area_Pnd_Ws_Totales", "#WS-TOTALES", new DbsArrayController(1, 3));
        pnd_Work_Area_Pnd_Ws_Cnt_Tot = pnd_Work_Area_Pnd_Ws_Totales.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Cnt_Tot", "#WS-CNT-TOT", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Work_Area_Pnd_Ws_Gross_Tot = pnd_Work_Area_Pnd_Ws_Totales.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Gross_Tot", "#WS-GROSS-TOT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Work_Area_Pnd_Ws_Int_Tot = pnd_Work_Area_Pnd_Ws_Totales.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Int_Tot", "#WS-INT-TOT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Work_Area_Pnd_Ws_Cnt = pnd_Work_Area_Pnd_Ws_Totales.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Cnt", "#WS-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Work_Area_Pnd_Ws_Gross = pnd_Work_Area_Pnd_Ws_Totales.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Gross", "#WS-GROSS", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Work_Area_Pnd_Ws_Int = pnd_Work_Area_Pnd_Ws_Totales.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Int", "#WS-INT", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_S_Output_Data = localVariables.newGroupInRecord("pnd_S_Output_Data", "#S-OUTPUT-DATA");
        pnd_S_Output_Data_Pnd_Ret_Code = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Ret_Code", "#RET-CODE", FieldType.BOOLEAN, 1);
        pnd_S_Output_Data_Pnd_Ret_Msg = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Ret_Msg", "#RET-MSG", FieldType.STRING, 35);
        pnd_S_Output_Data_Pnd_Comp_Short_Name = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Short_Name", "#COMP-SHORT-NAME", FieldType.STRING, 
            4);
        pnd_S_Output_Data_Pnd_Comp_Name = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Name", "#COMP-NAME", FieldType.STRING, 55);
        pnd_S_Output_Data_Pnd_Comp_Corr_Name = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Corr_Name", "#COMP-CORR-NAME", FieldType.STRING, 
            18);
        pnd_S_Output_Data_Pnd_Fed_Id = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Fed_Id", "#FED-ID", FieldType.STRING, 9);
        pnd_S_Output_Data_Pnd_Nr4_Id = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Nr4_Id", "#NR4-ID", FieldType.STRING, 9);
        pnd_S_Output_Data_Pnd_Contact_Name = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Contact_Name", "#CONTACT-NAME", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Contact_Email_Addr = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Contact_Email_Addr", "#CONTACT-EMAIL-ADDR", 
            FieldType.STRING, 35);
        pnd_S_Output_Data_Pnd_Phone = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Phone", "#PHONE", FieldType.STRING, 14);
        pnd_S_Output_Data_Pnd_Address = pnd_S_Output_Data.newFieldArrayInGroup("pnd_S_Output_Data_Pnd_Address", "#ADDRESS", FieldType.STRING, 30, new 
            DbsArrayController(1, 2));
        pnd_S_Output_Data_Pnd_City = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_City", "#CITY", FieldType.STRING, 9);
        pnd_S_Output_Data_Pnd_State = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_S_Output_Data_Pnd_Zip_9 = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Zip_9", "#ZIP-9", FieldType.STRING, 9);

        pnd_S_Output_Data__R_Field_6 = pnd_S_Output_Data.newGroupInGroup("pnd_S_Output_Data__R_Field_6", "REDEFINE", pnd_S_Output_Data_Pnd_Zip_9);
        pnd_S_Output_Data_Pnd_Zip = pnd_S_Output_Data__R_Field_6.newFieldInGroup("pnd_S_Output_Data_Pnd_Zip", "#ZIP", FieldType.STRING, 5);
        pnd_S_Output_Data_Pnd_Zip_4 = pnd_S_Output_Data__R_Field_6.newFieldInGroup("pnd_S_Output_Data_Pnd_Zip_4", "#ZIP-4", FieldType.STRING, 4);
        pnd_S_Output_Data_Pnd_Comp_Tcc = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Tcc", "#COMP-TCC", FieldType.STRING, 10);
        pnd_S_Output_Data_Pnd_Comp_Trans_A = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Trans_A", "#COMP-TRANS-A", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Trans_B = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Trans_B", "#COMP-TRANS-B", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Payer_A = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Payer_A", "#COMP-PAYER-A", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Payer_B = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Payer_B", "#COMP-PAYER-B", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Agent_A = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Agent_A", "#COMP-AGENT-A", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Agent_B = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Agent_B", "#COMP-AGENT-B", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Retrn_A = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Retrn_A", "#COMP-RETRN-A", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Retrn_B = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Retrn_B", "#COMP-RETRN-B", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Futr1_A = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Futr1_A", "#COMP-FUTR1-A", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Futr1_B = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Futr1_B", "#COMP-FUTR1-B", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Futr2_A = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Futr2_A", "#COMP-FUTR2-A", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Futr2_B = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Futr2_B", "#COMP-FUTR2-B", FieldType.STRING, 
            40);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_form1.reset();

        ldaTwrl117a.initializeValues();
        ldaTwrl117b.initializeValues();
        ldaTwrl9710.initializeValues();
        ldaTwrltb4r.initializeValues();

        localVariables.reset();
        pnd_Work_Area_Pnd_First.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0124() throws Exception
    {
        super("Twrp0124");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp0124|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*  -----------------------------------------
                //*  FO 02/01
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                //*  TOTALS
                //*  ACCEPTED
                //*  REJECTED
                //*                                                                                                                                                       //Natural: FORMAT PS = 56 LS = 133 ES = ON;//Natural: FORMAT ( 1 ) PS = 60 LS = 133 ES = ON;//Natural: FORMAT ( 2 ) PS = 60 LS = 133 ES = ON;//Natural: FORMAT ( 3 ) PS = 60 LS = 133 ES = ON
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_In_Parm);                                                                                          //Natural: INPUT #IN-PARM
                //*  FO 02/01
                //*  TAX YEAR IS ALWAYS CALENDAR
                if (condition(pnd_In_Parm_Pnd_In_Tax_Year.equals(" ")))                                                                                                   //Natural: IF #IN-TAX-YEAR = ' '
                {
                    pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year.compute(new ComputeParameters(false, pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year), Global.getDATN().divide(10000).subtract(1)); //Natural: ASSIGN #TIRF-TAX-YEAR := *DATN / 10000 - 1
                    //*  YEAR MINUS 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year.setValue(pnd_In_Parm_Pnd_In_Tax_Year);                                                                           //Natural: ASSIGN #TIRF-TAX-YEAR := #IN-TAX-YEAR
                }                                                                                                                                                         //Natural: END-IF
                //*  GET 'S' (SERVICES) COMPANY CODE DATA.
                //*  02-08-2005
                                                                                                                                                                          //Natural: PERFORM COMPANY-BREAK
                sub_Company_Break();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  02-08-2005
                pnd_S_Output_Data.setValuesByName(pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data());                                                                         //Natural: MOVE BY NAME #TWRACOM2.#OUTPUT-DATA TO #S-OUTPUT-DATA
                //*  ==============================
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 1 ) TITLE LEFT *TIMX ( EM = MM/DD/YYYY�HH:IIAP ) 28T 'TaxWaRS - Annual Original Tax Report - Totals  ' 92T 'Page:' *PAGE-NUMBER ( 1 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 34T #TIRF-TAX-YEAR '- Puerto Rico - Form 480.6A' /// 26T '  Accepted       Rejected            Total' / 26T '  --------       --------            -----'
                //*  02-08-2005
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 2 ) TITLE LEFT *TIMX ( EM = MM/DD/YYYY�HH:IIAP ) 28T 'TaxWaRS - Annual Original Tax Report - Accepted' 92T 'Page:' *PAGE-NUMBER ( 2 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 32T #TIRF-TAX-YEAR '-' #TWRACOM2.#COMP-SHORT-NAME '- Puerto Rico - Form 480.6A' /
                //*  02-08-2005
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 3 ) TITLE LEFT *TIMX ( EM = MM/DD/YYYY�HH:IIAP ) 28T 'TaxWaRS - Annual Original Tax Report - Rejected' 92T 'Page:' *PAGE-NUMBER ( 3 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 32T #TIRF-TAX-YEAR '-' #TWRACOM2.#COMP-SHORT-NAME '- Puerto Rico - Form 480.6A' /
                //*  -----------------------------------------------
                pnd_Tirf_Superde_3_Pnd_Tirf_Active_Ind.setValue(pnd_In_Parm_Pnd_In_Active_Ind);                                                                           //Natural: ASSIGN #TIRF-ACTIVE-IND := #IN-ACTIVE-IND
                pnd_Tirf_Superde_3_Pnd_Tirf_Form_Type.setValue(pnd_In_Parm_Pnd_In_Form_Type);                                                                             //Natural: ASSIGN #TIRF-FORM-TYPE := #IN-FORM-TYPE
                ldaTwrl9710.getVw_form().startDatabaseRead                                                                                                                //Natural: READ FORM WITH TIRF-SUPERDE-3 = #TIRF-SUPERDE-3
                (
                "READ01",
                new Wc[] { new Wc("TIRF_SUPERDE_3", ">=", pnd_Tirf_Superde_3, WcType.BY) },
                new Oc[] { new Oc("TIRF_SUPERDE_3", "ASC") }
                );
                READ01:
                while (condition(ldaTwrl9710.getVw_form().readNextRow("READ01")))
                {
                    if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().notEquals(pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year) || ldaTwrl9710.getForm_Tirf_Active_Ind().notEquals(pnd_Tirf_Superde_3_Pnd_Tirf_Active_Ind)  //Natural: IF TIRF-TAX-YEAR NE #TIRF-TAX-YEAR OR TIRF-ACTIVE-IND NE #TIRF-ACTIVE-IND OR TIRF-FORM-TYPE NE #TIRF-FORM-TYPE
                        || ldaTwrl9710.getForm_Tirf_Form_Type().notEquals(pnd_Tirf_Superde_3_Pnd_Tirf_Form_Type)))
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl9710.getForm_Tirf_Empty_Form().getBoolean()))                                                                                    //Natural: IF FORM.TIRF-EMPTY-FORM
                    {
                        pnd_Work_Area_Pnd_Empty_Form_Cnt.nadd(1);                                                                                                         //Natural: ADD 1 TO #EMPTY-FORM-CNT
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_In_Parm_Pnd_In_Inc_Old.equals("N") && ldaTwrl9710.getForm_Tirf_Irs_Rpt_Ind().notEquals(" ")))                                       //Natural: IF #IN-INC-OLD EQ 'N' AND FORM.TIRF-IRS-RPT-IND NE ' '
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Work_Area_Pnd_Counter.nadd(1);                                                                                                                    //Natural: ADD 1 TO #COUNTER
                    if (condition(ldaTwrl9710.getForm_Tirf_Company_Cde().notEquals(pnd_Work_Area_Pnd_Old_Cmpny)))                                                         //Natural: IF FORM.TIRF-COMPANY-CDE NE #OLD-CMPNY
                    {
                                                                                                                                                                          //Natural: PERFORM COMPANY-BREAK
                        sub_Company_Break();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        getReports().newPage(new ReportSpecification(2));                                                                                                 //Natural: NEWPAGE ( 2 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().newPage(new ReportSpecification(3));                                                                                                 //Natural: NEWPAGE ( 3 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Work_Area_Pnd_Reject.reset();                                                                                                                     //Natural: RESET #REJECT
                    pnd_Work_Area_Pnd_Ws_Isn.setValue(ldaTwrl9710.getVw_form().getAstISN("Read01"));                                                                      //Natural: MOVE *ISN TO #WS-ISN
                    if (condition(pnd_In_Parm_Pnd_In_Update.equals("Y")))                                                                                                 //Natural: IF #IN-UPDATE EQ 'Y'
                    {
                        GET1:                                                                                                                                             //Natural: GET FORM1 #WS-ISN
                        vw_form1.readByID(pnd_Work_Area_Pnd_Ws_Isn.getLong(), "GET1");
                        //*  FO 02/01 FLAG REJECTED RECORDS
                        //*  FO 02/01 CHANGED TO HELD
                        if (condition(ldaTwrl9710.getForm_Tirf_Irs_Reject_Ind().greater(" ")))                                                                            //Natural: IF TIRF-IRS-REJECT-IND GT ' '
                        {
                            form1_Tirf_Irs_Rpt_Ind.setValue("H");                                                                                                         //Natural: ASSIGN FORM1.TIRF-IRS-RPT-IND := 'H'
                                                                                                                                                                          //Natural: PERFORM WRITE-REJECT-REPORT
                            sub_Write_Reject_Report();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            pnd_Work_Area_Pnd_Reject.setValue(true);                                                                                                      //Natural: ASSIGN #REJECT := TRUE
                            //*  FO 02/01 CHANGED TO ORIGINAL
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            form1_Tirf_Irs_Rpt_Ind.setValue("O");                                                                                                         //Natural: ASSIGN FORM1.TIRF-IRS-RPT-IND := 'O'
                        }                                                                                                                                                 //Natural: END-IF
                        form1_Tirf_Lu_User.setValue(Global.getINIT_USER());                                                                                               //Natural: ASSIGN FORM1.TIRF-LU-USER := *INIT-USER
                        form1_Tirf_Lu_Ts.setValue(Global.getDATX());                                                                                                      //Natural: ASSIGN FORM1.TIRF-LU-TS := *DATX
                        form1_Tirf_Irs_Rpt_Date.setValue(Global.getDATX());                                                                                               //Natural: ASSIGN FORM1.TIRF-IRS-RPT-DATE := *DATX
                        vw_form1.updateDBRow("GET1");                                                                                                                     //Natural: UPDATE ( GET1. )
                        pnd_Work_Area_Pnd_Ws_Et.nadd(1);                                                                                                                  //Natural: ADD 1 TO #WS-ET
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Work_Area_Pnd_Ws_Et.greater(250)))                                                                                                  //Natural: IF #WS-ET GT 250
                    {
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                        pnd_Work_Area_Pnd_Ws_Et.reset();                                                                                                                  //Natural: RESET #WS-ET
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Work_Area_Pnd_Reject.getBoolean()))                                                                                                 //Natural: IF #REJECT
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ADD 1 TO #WS-SEQ-NUM
                    if (condition(pnd_Tirf_Superde_3_Pnd_Tirf_Form_Type.equals(5)))                                                                                       //Natural: IF #TIRF-FORM-TYPE = 5
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE-480-6A-REC
                        sub_Write_480_6a_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Tirf_Superde_3_Pnd_Tirf_Form_Type.equals(6)))                                                                                       //Natural: IF #TIRF-FORM-TYPE = 6
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE-480-6B-REC
                        sub_Write_480_6b_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DISPLAY-ACCEPTED-FORM
                    sub_Display_Accepted_Form();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                //*   02/01/06  RM
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_Work_Area_Pnd_Last.setValue(true);                                                                                                                    //Natural: ASSIGN #LAST := TRUE
                                                                                                                                                                          //Natural: PERFORM COMPANY-BREAK
                sub_Company_Break();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  ----------------------------------------
                getReports().write(1, "Total Gross         ",pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(2),  //Natural: WRITE ( 1 ) 'Total Gross         ' #WS-GROSS-TOT ( 1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) #WS-GROSS-TOT ( 2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) #WS-GROSS-TOT ( 3 ) ( EM = ZZZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(3), new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(1, "Total Interest      ",pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(1), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(2),  //Natural: WRITE ( 1 ) 'Total Interest      ' #WS-INT-TOT ( 1 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-INT-TOT ( 2 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) 3X #WS-INT-TOT ( 3 ) ( EM = ZZZZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(3), new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(1, "Total Forms       ",new TabSetting(30),pnd_Work_Area_Pnd_Ws_Cnt_Tot.getValue(1),new TabSetting(45),pnd_Work_Area_Pnd_Ws_Cnt_Tot.getValue(2),new  //Natural: WRITE ( 1 ) 'Total Forms       ' 30T #WS-CNT-TOT ( 1 ) 45T #WS-CNT-TOT ( 2 ) 62T #WS-CNT-TOT ( 3 )
                    TabSetting(62),pnd_Work_Area_Pnd_Ws_Cnt_Tot.getValue(3));
                if (Global.isEscape()) return;
                getReports().write(1, "* -","-",new RepeatItem(55));                                                                                                      //Natural: WRITE ( 1 ) '* -' '-' ( 55 )
                if (Global.isEscape()) return;
                //*  --------------------------------------------
                //*  5 = 6A; 6 = 6B
                //*  UPDATE 6A & 6B CONTROL RECORDS
                if (condition(pnd_In_Parm_Pnd_In_Update.equals("Y")))                                                                                                     //Natural: IF #IN-UPDATE = 'Y'
                {
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn);                                                           //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #TIRF-TAX-YEARN
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(pnd_In_Parm_Pnd_In_Form_Type);                                                                    //Natural: ASSIGN #TWRATBL4.#FORM-IND := #IN-FORM-TYPE
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(false);                                                                                          //Natural: ASSIGN #TWRATBL4.#ABEND-IND := FALSE
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Rec_Type().setValue(" ");                                                                                             //Natural: ASSIGN #TWRATBL4.#REC-TYPE := ' '
                    FOR01:                                                                                                                                                //Natural: FOR #TWRATBL4.#FORM-IND = 5 TO 6
                    for (pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(5); condition(pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().lessOrEqual(6)); 
                        pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().nadd(1))
                    {
                        DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4());                                                        //Natural: CALLNAT 'TWRNTB4R' #TWRATBL4
                        if (condition(Global.isEscape())) return;
                        if (condition(! (pdaTwratbl4.getPnd_Twratbl4_Pnd_Ret_Code().getBoolean())))                                                                       //Natural: IF NOT #TWRATBL4.#RET-CODE
                        {
                            DbsUtil.terminate(112);  if (true) return;                                                                                                    //Natural: TERMINATE 112
                        }                                                                                                                                                 //Natural: END-IF
                        pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(1).setValue(Global.getDATX());                                                             //Natural: ASSIGN #TWRATBL4.TIRCNTL-RPT-DTE ( 1 ) := *DATX
                        DbsUtil.callnat(Twrntb4u.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4());                                                        //Natural: CALLNAT 'TWRNTB4U' #TWRATBL4
                        if (condition(Global.isEscape())) return;
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //*  480.5  SUMMARY RECORD  02/01/06 RM
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-REC
                sub_Write_Summary_Rec();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                //*  -----------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-480-6A-REC
                //*  ================================
                //*  PERFORM GET-COMPANY-INFO
                //*  POPULATE REPORTING RECORD WITH 'Services' VALUES.
                //*  #TWRL117A-PAYEE-ZIP-EXT     := 0
                //*  02/08/2005 - #TWRL117A-TRANS-CODE VALUES: ' ' = ORIGINAL REPORTING
                //*                                            'D' = DUPLICATE REPORTING
                //*                                            'A' = AMENDED REPORTING
                //*  #TWRL117A-TRANS-CODE        := ' '
                //*  #TWRL117A-TRANS-CODE VALUES: 'O' = ORIGINAL REPORTING    02/01/2006 RM
                //*     (DOCUMENT TYPE)           'C' = CORRECTED REPORTING
                //*                               'A' = AMENDED REPORTING
                //*                                                          02/01/2006 RM
                //*  #TWRL117A-SEQ-NUMBER  CHANGED TO #TWRL117A-CONTROL-NUMBER
                //*  #TWRL117A-PAYMENT-DATE WAS DELETED
                //*  #TWRL117A-PAYMENT-DATE-DDMM := '3112'
                //*  #TWRL117A-PAYMENT-DATE-CCYY := VAL(FORM.TIRF-TAX-YEAR)
                //*  -----------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-480-6B-REC
                //*  #TWRL117B-PAYMENT-DATE-DDMM  := '3112'
                //*  #TEMP-TAX-YEAR               := VAL(FORM.TIRF-TAX-YEAR)
                //*  COMPUTE #TEMP-TAX-YEAR        = #TEMP-TAX-YEAR - 1
                //*  #TWRL117B-PAYMENT-DATE-YY    := #TEMP-TAX-YEARYY
                //*  -----------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPANY-BREAK
                //*  ------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-ACCEPTED-FORM
                //*  =====================================
                //*  ------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REJECT-REPORT
                //*  -----------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUMMARY-REC
                //*  ADD 1 TO #WS-SEQ-NUM
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Write_480_6a_Rec() throws Exception                                                                                                                  //Natural: WRITE-480-6A-REC
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                             //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
        pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwratbl2.getTwratbl2_Pnd_Tax_Year()), ldaTwrl9710.getForm_Tirf_Tax_Year().val());  //Natural: ASSIGN TWRATBL2.#TAX-YEAR := VAL ( FORM.TIRF-TAX-YEAR )
        pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().setValue(false);                                                                                                          //Natural: ASSIGN TWRATBL2.#ABEND-IND := FALSE
        pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().setValue(false);                                                                                                        //Natural: ASSIGN TWRATBL2.#DISPLAY-IND := FALSE
        pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "0", ldaTwrl9710.getForm_Tirf_Geo_Cde()));                       //Natural: COMPRESS '0' FORM.TIRF-GEO-CDE INTO TWRATBL2.#STATE-CDE LEAVING NO
        DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        //*  02-08-2005
        //*  02-08-2005
        //*  02-08-2005
        //*  02-08-2005
        //*  02-08-2005
        //*  LINE 2 CONTAINS C,S,Z  02-20-2007
        //*  02-08-2005
        //*  02-08-2005
        //*  02-08-2005
        //*  02-08-2005
        ldaTwrl117a.getPnd_Twrl117a().reset();                                                                                                                            //Natural: RESET #TWRL117A
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Form_Type().setValue(2);                                                                                                 //Natural: ASSIGN #TWRL117A-FORM-TYPE := 2
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Tax_Year().compute(new ComputeParameters(false, ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Tax_Year()),                    //Natural: ASSIGN #TWRL117A-TAX-YEAR := VAL ( FORM.TIRF-TAX-YEAR )
            ldaTwrl9710.getForm_Tirf_Tax_Year().val());
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Payers_Id().compute(new ComputeParameters(false, ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Payers_Id()),                  //Natural: ASSIGN #TWRL117A-PAYERS-ID := VAL ( #S-OUTPUT-DATA.#FED-ID )
            pnd_S_Output_Data_Pnd_Fed_Id.val());
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Payers_Name().setValue(pnd_S_Output_Data_Pnd_Comp_Payer_A);                                                              //Natural: ASSIGN #TWRL117A-PAYERS-NAME := #S-OUTPUT-DATA.#COMP-PAYER-A
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Payers_Address_1().setValue(pnd_S_Output_Data_Pnd_Address.getValue(1));                                                  //Natural: ASSIGN #TWRL117A-PAYERS-ADDRESS-1 := #S-OUTPUT-DATA.#ADDRESS ( 1 )
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Payers_Address_2().setValue(" ");                                                                                        //Natural: ASSIGN #TWRL117A-PAYERS-ADDRESS-2 := ' '
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Payers_Town().setValue(pnd_S_Output_Data_Pnd_City);                                                                      //Natural: ASSIGN #TWRL117A-PAYERS-TOWN := #S-OUTPUT-DATA.#CITY
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Payers_State().setValue(pnd_S_Output_Data_Pnd_State);                                                                    //Natural: ASSIGN #TWRL117A-PAYERS-STATE := #S-OUTPUT-DATA.#STATE
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Payers_Zip_Full().setValue(pnd_S_Output_Data_Pnd_Zip_9);                                                                 //Natural: ASSIGN #TWRL117A-PAYERS-ZIP-FULL := #S-OUTPUT-DATA.#ZIP-9
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Payee_Ssn().compute(new ComputeParameters(false, ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Payee_Ssn()),                  //Natural: ASSIGN #TWRL117A-PAYEE-SSN := VAL ( FORM.TIRF-TIN )
            ldaTwrl9710.getForm_Tirf_Tin().val());
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Payee_Bank().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl9710.getForm_Tirf_Contract_Nbr(),           //Natural: COMPRESS TIRF-CONTRACT-NBR TIRF-PAYEE-CDE INTO #TWRL117A-PAYEE-BANK LEAVING NO
            ldaTwrl9710.getForm_Tirf_Payee_Cde()));
        //*  02-20-2007
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Payee_Name().setValue(DbsUtil.compress(ldaTwrl9710.getForm_Tirf_Part_First_Nme(), ldaTwrl9710.getForm_Tirf_Part_Mddle_Nme(),  //Natural: COMPRESS FORM.TIRF-PART-FIRST-NME FORM.TIRF-PART-MDDLE-NME FORM.TIRF-PART-LAST-NME INTO #TWRL117A-PAYEE-NAME
            ldaTwrl9710.getForm_Tirf_Part_Last_Nme()));
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Payee_Address_1().setValue(ldaTwrl9710.getForm_Tirf_Street_Addr());                                                      //Natural: ASSIGN #TWRL117A-PAYEE-ADDRESS-1 := FORM.TIRF-STREET-ADDR
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Payee_Address_2().setValue(ldaTwrl9710.getForm_Tirf_Street_Addr_Cont_1());                                               //Natural: ASSIGN #TWRL117A-PAYEE-ADDRESS-2 := FORM.TIRF-STREET-ADDR-CONT-1
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Payee_Town().setValue(ldaTwrl9710.getForm_Tirf_City());                                                                  //Natural: ASSIGN #TWRL117A-PAYEE-TOWN := FORM.TIRF-CITY
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Payee_State().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code());                                              //Natural: ASSIGN #TWRL117A-PAYEE-STATE := TIRCNTL-STATE-ALPHA-CODE
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Payee_Zip_Full().setValue(ldaTwrl9710.getForm_Tirf_Zip());                                                               //Natural: ASSIGN #TWRL117A-PAYEE-ZIP-FULL := FORM.TIRF-ZIP
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Ser_Redeem().setValue(0);                                                                                                //Natural: ASSIGN #TWRL117A-SER-REDEEM := 0
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Ser_Corp().setValue(0);                                                                                                  //Natural: ASSIGN #TWRL117A-SER-CORP := 0
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Commission().setValue(0);                                                                                                //Natural: ASSIGN #TWRL117A-COMMISSION := 0
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Rats().setValue(0);                                                                                                      //Natural: ASSIGN #TWRL117A-RATS := 0
        //*  02-20-2007
        pnd_Work_Area_Pnd_Ws_Amount.compute(new ComputeParameters(false, pnd_Work_Area_Pnd_Ws_Amount), ldaTwrl9710.getForm_Tirf_Int_Amt().multiply(100));                 //Natural: COMPUTE #WS-AMOUNT = FORM.TIRF-INT-AMT * 100
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Interst().setValueEdited(pnd_Work_Area_Pnd_Ws_Amount,new ReportEditMask("999999999999"));                                //Natural: MOVE EDITED #WS-AMOUNT ( EM = 999999999999 ) TO #TWRL117A-INTERST
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Part_Disrib().setValue(0);                                                                                               //Natural: ASSIGN #TWRL117A-PART-DISRIB := 0
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Dividend().setValue(0);                                                                                                  //Natural: ASSIGN #TWRL117A-DIVIDEND := 0
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Other_Payment().setValue(0);                                                                                             //Natural: ASSIGN #TWRL117A-OTHER-PAYMENT := 0
        //*  #TWRL117A-GROSS-PROCEEDS  := FORM.TIRF-GROSS-AMT  /* EB 12/09/02
        //*  #TWRL117A-PPD-NO-WTHLDNG    := FORM.TIRF-GROSS-AMT      /* 02-20-2007
        //*  02-20-2007
        pnd_Work_Area_Pnd_Ws_Amount.compute(new ComputeParameters(false, pnd_Work_Area_Pnd_Ws_Amount), ldaTwrl9710.getForm_Tirf_Gross_Amt().multiply(100));               //Natural: COMPUTE #WS-AMOUNT = FORM.TIRF-GROSS-AMT * 100
        //*  02-08-2005
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Ppd_No_Wthldng().setValueEdited(pnd_Work_Area_Pnd_Ws_Amount,new ReportEditMask("999999999999"));                         //Natural: MOVE EDITED #WS-AMOUNT ( EM = 999999999999 ) TO #TWRL117A-PPD-NO-WTHLDNG
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Trans_Code().setValue("O");                                                                                              //Natural: ASSIGN #TWRL117A-TRANS-CODE := 'O'
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Control_Number().setValue("7X216");                                                                                      //Natural: ASSIGN #TWRL117A-CONTROL-NUMBER := '7X216'
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_Rec_Type().setValue(1);                                                                                                  //Natural: ASSIGN #TWRL117A-REC-TYPE := 1
        getWorkFiles().write(1, false, ldaTwrl117a.getPnd_Twrl117a());                                                                                                    //Natural: WRITE WORK FILE 1 #TWRL117A
        pnd_Work_Area_Pnd_Ws_Num_Of_Doc.nadd(1);                                                                                                                          //Natural: ADD 1 TO #WS-NUM-OF-DOC
        pnd_Work_Area_Pnd_Ws_Amt_Withld.reset();                                                                                                                          //Natural: RESET #WS-AMT-WITHLD
        pnd_Work_Area_Pnd_Ws_Amout_Paid.nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                                       //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-AMOUT-PAID
        pnd_Work_Area_Pnd_Ws_Amout_Paid.nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                         //Natural: ADD FORM.TIRF-INT-AMT TO #WS-AMOUT-PAID
    }
    private void sub_Write_480_6b_Rec() throws Exception                                                                                                                  //Natural: WRITE-480-6B-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        //*  CURRENTLY THERE ARE NO 480.6B FORMS BEING PRODUCED,
        //*  BUT CODINGS ARE UPDATED FOR THE NEW FIELDS NEVERTHELESS. 02/01/2006 RM
        //*  NOTHING DONE FOR 480-6B YET.                             02/20/2007 RM
        //*  02-08-2005
        //*  02-08-2005
        //*  02-08-2005
        //*  02-08-2005
        //*  02-08-2005
        //*  02-08-2005
        ldaTwrl117b.getPnd_Twrl117b().reset();                                                                                                                            //Natural: RESET #TWRL117B
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Form_Type().setValue("3");                                                                                               //Natural: ASSIGN #TWRL117B-FORM-TYPE := '3'
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Tax_Year().setValue(ldaTwrl9710.getForm_Tirf_Tax_Year());                                                                //Natural: ASSIGN #TWRL117B-TAX-YEAR := FORM.TIRF-TAX-YEAR
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Agent_Id().compute(new ComputeParameters(false, ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Agent_Id()),                    //Natural: ASSIGN #TWRL117B-AGENT-ID := VAL ( #S-OUTPUT-DATA.#FED-ID )
            pnd_S_Output_Data_Pnd_Fed_Id.val());
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Agent_Name().setValue(pnd_S_Output_Data_Pnd_Comp_Agent_A);                                                               //Natural: ASSIGN #TWRL117B-AGENT-NAME := #S-OUTPUT-DATA.#COMP-AGENT-A
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Agent_Address_1().setValue(pnd_S_Output_Data_Pnd_Address.getValue(1));                                                   //Natural: ASSIGN #TWRL117B-AGENT-ADDRESS-1 := #S-OUTPUT-DATA.#ADDRESS ( 1 )
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Agent_Address_2().setValue(" ");                                                                                         //Natural: ASSIGN #TWRL117B-AGENT-ADDRESS-2 := ' '
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Agent_Town().setValue(pnd_S_Output_Data_Pnd_City);                                                                       //Natural: ASSIGN #TWRL117B-AGENT-TOWN := #S-OUTPUT-DATA.#CITY
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Agent_State().setValue(pnd_S_Output_Data_Pnd_State);                                                                     //Natural: ASSIGN #TWRL117B-AGENT-STATE := #S-OUTPUT-DATA.#STATE
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Agent_Zip().compute(new ComputeParameters(false, ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Agent_Zip()),                  //Natural: ASSIGN #TWRL117B-AGENT-ZIP := VAL ( #S-OUTPUT-DATA.#ZIP )
            pnd_S_Output_Data_Pnd_Zip.val());
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Agent_Zip_Ext().setValue(0);                                                                                             //Natural: ASSIGN #TWRL117B-AGENT-ZIP-EXT := 0
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Payee_Ssn().setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                    //Natural: ASSIGN #TWRL117B-PAYEE-SSN := FORM.TIRF-TIN
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Payee_Bank().setValue(0);                                                                                                //Natural: ASSIGN #TWRL117B-PAYEE-BANK := 0
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Payee_Name().setValue(DbsUtil.compress(ldaTwrl9710.getForm_Tirf_Part_Last_Nme(), ldaTwrl9710.getForm_Tirf_Part_First_Nme(),  //Natural: COMPRESS FORM.TIRF-PART-LAST-NME FORM.TIRF-PART-FIRST-NME FORM.TIRF-PART-MDDLE-NME INTO #TWRL117B-PAYEE-NAME
            ldaTwrl9710.getForm_Tirf_Part_Mddle_Nme()));
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Payee_Address_1().setValue(ldaTwrl9710.getForm_Tirf_Street_Addr());                                                      //Natural: ASSIGN #TWRL117B-PAYEE-ADDRESS-1 := FORM.TIRF-STREET-ADDR
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Payee_Address_2().setValue(" ");                                                                                         //Natural: ASSIGN #TWRL117B-PAYEE-ADDRESS-2 := ' '
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Payee_Town().setValue(ldaTwrl9710.getForm_Tirf_City());                                                                  //Natural: ASSIGN #TWRL117B-PAYEE-TOWN := FORM.TIRF-CITY
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Payee_Zip_Full().setValue(ldaTwrl9710.getForm_Tirf_Zip());                                                               //Natural: ASSIGN #TWRL117B-PAYEE-ZIP-FULL := FORM.TIRF-ZIP
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Indev().setValue(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                         //Natural: ASSIGN #TWRL117B-AMT-PAID-INDEV := FORM.TIRF-GROSS-AMT
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Indev().setValue(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                                    //Natural: ASSIGN #TWRL117B-AMT-WTHLD-INDEV := FORM.TIRF-FED-TAX-WTHLD
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Part().setValue(0);                                                                                             //Natural: ASSIGN #TWRL117B-AMT-PAID-PART := 0
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Part().setValue(0);                                                                                            //Natural: ASSIGN #TWRL117B-AMT-WTHLD-PART := 0
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Extra().setValue(0);                                                                                            //Natural: ASSIGN #TWRL117B-AMT-PAID-EXTRA := 0
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Extra().setValue(0);                                                                                           //Natural: ASSIGN #TWRL117B-AMT-WTHLD-EXTRA := 0
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Dividend().setValue(0);                                                                                         //Natural: ASSIGN #TWRL117B-AMT-PAID-DIVIDEND := 0
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Dividend().setValue(0);                                                                                        //Natural: ASSIGN #TWRL117B-AMT-WTHLD-DIVIDEND := 0
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Part_Dis().setValue(0);                                                                                         //Natural: ASSIGN #TWRL117B-AMT-PAID-PART-DIS := 0
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Part_Dis().setValue(0);                                                                                        //Natural: ASSIGN #TWRL117B-AMT-WTHLD-PART-DIS := 0
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Interst().setValue(0);                                                                                          //Natural: ASSIGN #TWRL117B-AMT-PAID-INTERST := 0
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Interst().setValue(0);                                                                                         //Natural: ASSIGN #TWRL117B-AMT-WTHLD-INTERST := 0
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Act_26().setValue(0);                                                                                           //Natural: ASSIGN #TWRL117B-AMT-PAID-ACT-26 := 0
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Act_26().setValue(0);                                                                                          //Natural: ASSIGN #TWRL117B-AMT-WTHLD-ACT-26 := 0
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Act_8().setValue(0);                                                                                            //Natural: ASSIGN #TWRL117B-AMT-PAID-ACT-8 := 0
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Act_8().setValue(0);                                                                                           //Natural: ASSIGN #TWRL117B-AMT-WTHLD-ACT-8 := 0
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Pension().setValue(0);                                                                                          //Natural: ASSIGN #TWRL117B-AMT-PAID-PENSION := 0
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Pension().setValue(0);                                                                                         //Natural: ASSIGN #TWRL117B-AMT-WTHLD-PENSION := 0
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Others().setValue(0);                                                                                           //Natural: ASSIGN #TWRL117B-AMT-PAID-OTHERS := 0
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Others().setValue(0);                                                                                          //Natural: ASSIGN #TWRL117B-AMT-WTHLD-OTHERS := 0
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Control_Number().setValue("7X216");                                                                                      //Natural: ASSIGN #TWRL117B-CONTROL-NUMBER := '7X216'
        ldaTwrl117b.getPnd_Twrl117b_Pnd_Twrl117b_Rec_Type().setValue("1");                                                                                                //Natural: ASSIGN #TWRL117B-REC-TYPE := '1'
        //*  WRITE WORK FILE 1 #TWRL117B
    }
    private void sub_Company_Break() throws Exception                                                                                                                     //Natural: COMPANY-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        if (condition(pnd_Work_Area_Pnd_Counter.greater(1)))                                                                                                              //Natural: IF #COUNTER GT 1
        {
            //*  02-08-2005
            getReports().write(1, pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name()," Gross:        ",pnd_Work_Area_Pnd_Ws_Gross.getValue(1), new ReportEditMask          //Natural: WRITE ( 1 ) #TWRACOM2.#COMP-SHORT-NAME ' Gross:        ' #WS-GROSS ( 1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) #WS-GROSS ( 2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 3X #WS-GROSS ( 3 ) ( EM = ZZZ,ZZZ,ZZ9.99 )
                ("ZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Gross.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Work_Area_Pnd_Ws_Gross.getValue(3), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            //*  02-08-2005
            getReports().write(1, pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name()," Interest:     ",pnd_Work_Area_Pnd_Ws_Int.getValue(1), new ReportEditMask            //Natural: WRITE ( 1 ) #TWRACOM2.#COMP-SHORT-NAME ' Interest:     ' #WS-INT ( 1 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-INT ( 2 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) 3X #WS-INT ( 3 ) ( EM = ZZZZ,ZZZ,ZZ9.99 )
                ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Int.getValue(2), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Work_Area_Pnd_Ws_Int.getValue(3), 
                new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            //*  02-08-2005
            getReports().write(1, pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name()," Forms:        ",new TabSetting(30),pnd_Work_Area_Pnd_Ws_Cnt.getValue(1),new         //Natural: WRITE ( 1 ) #TWRACOM2.#COMP-SHORT-NAME ' Forms:        ' 30T #WS-CNT ( 1 ) 45T #WS-CNT ( 2 ) 62T #WS-CNT ( 3 )
                TabSetting(45),pnd_Work_Area_Pnd_Ws_Cnt.getValue(2),new TabSetting(62),pnd_Work_Area_Pnd_Ws_Cnt.getValue(3));
            if (Global.isEscape()) return;
            getReports().write(1, "* -","-",new RepeatItem(55));                                                                                                          //Natural: WRITE ( 1 ) '* -' '-' ( 55 )
            if (Global.isEscape()) return;
            getReports().write(2, NEWLINE,new TabSetting(3),"Total",pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),"Accepted:",pnd_Work_Area_Pnd_Ws_Cnt.getValue(1),new  //Natural: WRITE ( 2 ) / 3T 'Total' #TWRACOM2.#COMP-SHORT-NAME 'Accepted:' #WS-CNT ( 1 ) 32T #WS-GROSS ( 1 ) ( EM = ZZ,ZZZ,ZZ9.99 ) #WS-INT ( 1 ) ( EM = ZZZ,ZZ9.99 )
                TabSetting(32),pnd_Work_Area_Pnd_Ws_Gross.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Int.getValue(1), new ReportEditMask 
                ("ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            getReports().write(3, NEWLINE,new TabSetting(3),"Total",pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),"Rejected:",pnd_Work_Area_Pnd_Ws_Cnt.getValue(2),new  //Natural: WRITE ( 3 ) / 3T 'Total' #TWRACOM2.#COMP-SHORT-NAME 'Rejected:' #WS-CNT ( 2 ) 32T #WS-GROSS ( 2 ) ( EM = ZZ,ZZZ,ZZ9.99 ) #WS-INT ( 2 ) ( EM = ZZZ,ZZ9.99 )
                TabSetting(32),pnd_Work_Area_Pnd_Ws_Gross.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Int.getValue(2), new ReportEditMask 
                ("ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Work_Area_Pnd_Ws_Cnt.getValue("*").reset();                                                                                                               //Natural: RESET #WS-CNT ( * ) #WS-GROSS ( * ) #WS-INT ( * )
            pnd_Work_Area_Pnd_Ws_Gross.getValue("*").reset();
            pnd_Work_Area_Pnd_Ws_Int.getValue("*").reset();
        }                                                                                                                                                                 //Natural: END-IF
        //*  02-08-2005
        if (condition(! (pnd_Work_Area_Pnd_Last.getBoolean())))                                                                                                           //Natural: IF NOT #LAST
        {
            pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().setValue(pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn);                                                                   //Natural: ASSIGN #TWRACOM2.#TAX-YEAR := #TIRF-TAX-YEARN
            //*  02-08-2005
            //*  SERVICES         /* 02-08-2005
            //*  02-08-2005
            if (condition(pnd_Work_Area_Pnd_First.getBoolean()))                                                                                                          //Natural: IF #FIRST
            {
                pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue("S");                                                                                                //Natural: ASSIGN #TWRACOM2.#COMP-CODE := 'S'
                pnd_Work_Area_Pnd_First.setValue(false);                                                                                                                  //Natural: ASSIGN #FIRST := FALSE
                //*  02-08-2005
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue(ldaTwrl9710.getForm_Tirf_Company_Cde());                                                             //Natural: ASSIGN #TWRACOM2.#COMP-CODE := FORM.TIRF-COMPANY-CDE
            }                                                                                                                                                             //Natural: END-IF
            pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(5);                                                                                                      //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 5
            DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOM2' #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
            pnd_Work_Area_Pnd_Old_Cmpny.setValue(ldaTwrl9710.getForm_Tirf_Company_Cde());                                                                                 //Natural: ASSIGN #OLD-CMPNY := FORM.TIRF-COMPANY-CDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  EB
    private void sub_Display_Accepted_Form() throws Exception                                                                                                             //Natural: DISPLAY-ACCEPTED-FORM
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(2, "/ TIN ",                                                                                                                                 //Natural: DISPLAY ( 2 ) '/ TIN ' FORM.TIRF-TIN 'TIN/Typ' FORM.TIRF-TAX-ID-TYPE '/Contract' FORM.TIRF-CONTRACT-NBR '/Payee' FORM.TIRF-PAYEE-CDE '/  Pension' FORM.TIRF-GROSS-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) '/ Interest' FORM.TIRF-INT-AMT ( EM = ZZZ,ZZ9.99 )
        		ldaTwrl9710.getForm_Tirf_Tin(),"TIN/Typ",
        		ldaTwrl9710.getForm_Tirf_Tax_Id_Type(),"/Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"/Payee",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"/  Pension",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"/ Interest",
        		ldaTwrl9710.getForm_Tirf_Int_Amt(), new ReportEditMask ("ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Work_Area_Pnd_Ws_Cnt.getValue(1).nadd(1);                                                                                                                     //Natural: ADD 1 TO #WS-CNT ( 1 )
        pnd_Work_Area_Pnd_Ws_Cnt.getValue(3).nadd(1);                                                                                                                     //Natural: ADD 1 TO #WS-CNT ( 3 )
        pnd_Work_Area_Pnd_Ws_Gross.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                                //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS ( 1 )
        pnd_Work_Area_Pnd_Ws_Gross.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                                //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS ( 3 )
        pnd_Work_Area_Pnd_Ws_Int.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                    //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT ( 1 )
        pnd_Work_Area_Pnd_Ws_Int.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                    //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT ( 3 )
        pnd_Work_Area_Pnd_Ws_Cnt_Tot.getValue(1).nadd(1);                                                                                                                 //Natural: ADD 1 TO #WS-CNT-TOT ( 1 )
        pnd_Work_Area_Pnd_Ws_Cnt_Tot.getValue(3).nadd(1);                                                                                                                 //Natural: ADD 1 TO #WS-CNT-TOT ( 3 )
        pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                            //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS-TOT ( 1 )
        pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                            //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS-TOT ( 3 )
        pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT-TOT ( 1 )
        pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT-TOT ( 3 )
        //*  DISPLAY-ACCEPTED
    }
    private void sub_Write_Reject_Report() throws Exception                                                                                                               //Natural: WRITE-REJECT-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ===================================
        pnd_Work_Area_Pnd_Rej_Count.nadd(1);                                                                                                                              //Natural: ADD 1 TO #REJ-COUNT
        getReports().display(3, "/ TIN ",                                                                                                                                 //Natural: DISPLAY ( 3 ) '/ TIN ' FORM.TIRF-TIN 'TIN/Typ' FORM.TIRF-TAX-ID-TYPE '/Contract' FORM.TIRF-CONTRACT-NBR '/Payee' FORM.TIRF-PAYEE-CDE '/  Pension' FORM.TIRF-GROSS-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) '/ Interest' FORM.TIRF-INT-AMT ( EM = ZZZ,ZZ9.99 )
        		ldaTwrl9710.getForm_Tirf_Tin(),"TIN/Typ",
        		ldaTwrl9710.getForm_Tirf_Tax_Id_Type(),"/Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"/Payee",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"/  Pension",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"/ Interest",
        		ldaTwrl9710.getForm_Tirf_Int_Amt(), new ReportEditMask ("ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Work_Area_Pnd_Ws_Cnt.getValue(2).nadd(1);                                                                                                                     //Natural: ADD 1 TO #WS-CNT ( 2 )
        pnd_Work_Area_Pnd_Ws_Cnt.getValue(3).nadd(1);                                                                                                                     //Natural: ADD 1 TO #WS-CNT ( 3 )
        pnd_Work_Area_Pnd_Ws_Gross.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                                //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS ( 2 )
        pnd_Work_Area_Pnd_Ws_Gross.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                                //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS ( 3 )
        pnd_Work_Area_Pnd_Ws_Int.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                    //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT ( 2 )
        pnd_Work_Area_Pnd_Ws_Int.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                    //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT ( 3 )
        pnd_Work_Area_Pnd_Ws_Cnt_Tot.getValue(2).nadd(1);                                                                                                                 //Natural: ADD 1 TO #WS-CNT-TOT ( 2 )
        pnd_Work_Area_Pnd_Ws_Cnt_Tot.getValue(3).nadd(1);                                                                                                                 //Natural: ADD 1 TO #WS-CNT-TOT ( 3 )
        pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                            //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS-TOT ( 2 )
        pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                            //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS-TOT ( 3 )
        pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT-TOT ( 2 )
        pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT-TOT ( 3 )
        //*  WRITE-REJECT-REPORT
    }
    private void sub_Write_Summary_Rec() throws Exception                                                                                                                 //Natural: WRITE-SUMMARY-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*  =================================
        //*  REC INCREASED FR 700 TO 2000   02/01/06  RM
        //*     02/01/06  RM
        ldaTwrl117a.getPnd_Twrl117a().reset();                                                                                                                            //Natural: RESET #TWRL117A
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_S_Control_Number().setValue(0);                                                                                          //Natural: ASSIGN #TWRL117A-S-CONTROL-NUMBER := 0000000000
        //*  480.6A
        if (condition(pnd_Tirf_Superde_3_Pnd_Tirf_Form_Type.equals(5)))                                                                                                   //Natural: IF #TIRF-FORM-TYPE = 5
        {
            ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_S_Form_Type().setValue(2);                                                                                           //Natural: ASSIGN #TWRL117A-S-FORM-TYPE := 2
            //*  480.6B
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_S_Form_Type().setValue(3);                                                                                           //Natural: ASSIGN #TWRL117A-S-FORM-TYPE := 3
            //*  SUMMARY
            //*  FORM 480.5   /*    02/01/06  RM
            //*  02/20/07  RM
            //*  02-08-2005
            //*  02-08-2005
            //*  02-08-2005
            //*  02-20-2007
            //*  02-08-2005
            //*  02-08-2005
            //*  02-08-2005
            //*  NEW FOR 2005    02/01/06  RM
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_S_Rec_Type().setValue(2);                                                                                                //Natural: ASSIGN #TWRL117A-S-REC-TYPE := 2
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_S_Trans_Code().setValue("O");                                                                                            //Natural: ASSIGN #TWRL117A-S-TRANS-CODE := 'O'
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_S_Tax_Year().compute(new ComputeParameters(false, ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_S_Tax_Year()),                //Natural: ASSIGN #TWRL117A-S-TAX-YEAR := VAL ( FORM.TIRF-TAX-YEAR )
            ldaTwrl9710.getForm_Tirf_Tax_Year().val());
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_S_Payers_Id().compute(new ComputeParameters(false, ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_S_Payers_Id()),              //Natural: ASSIGN #TWRL117A-S-PAYERS-ID := VAL ( #S-OUTPUT-DATA.#FED-ID )
            pnd_S_Output_Data_Pnd_Fed_Id.val());
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_S_Payers_Name().setValue(pnd_S_Output_Data_Pnd_Comp_Payer_A);                                                            //Natural: ASSIGN #TWRL117A-S-PAYERS-NAME := #S-OUTPUT-DATA.#COMP-PAYER-A
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_S_Payers_Address_1().setValue(pnd_S_Output_Data_Pnd_Address.getValue(1));                                                //Natural: ASSIGN #TWRL117A-S-PAYERS-ADDRESS-1 := #S-OUTPUT-DATA.#ADDRESS ( 1 )
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_S_Payers_Address_2().setValue(pnd_S_Output_Data_Pnd_Address.getValue(2));                                                //Natural: ASSIGN #TWRL117A-S-PAYERS-ADDRESS-2 := #S-OUTPUT-DATA.#ADDRESS ( 2 )
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_S_Payers_Town().setValue(pnd_S_Output_Data_Pnd_City);                                                                    //Natural: ASSIGN #TWRL117A-S-PAYERS-TOWN := #S-OUTPUT-DATA.#CITY
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_S_Payers_State().setValue(pnd_S_Output_Data_Pnd_State);                                                                  //Natural: ASSIGN #TWRL117A-S-PAYERS-STATE := #S-OUTPUT-DATA.#STATE
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip_Full().setValue(pnd_S_Output_Data_Pnd_Zip_9);                                                               //Natural: ASSIGN #TWRL117A-S-PAYERS-ZIP-FULL := #S-OUTPUT-DATA.#ZIP-9
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_S_Num_Of_Doc().setValue(pnd_Work_Area_Pnd_Ws_Num_Of_Doc);                                                                //Natural: ASSIGN #TWRL117A-S-NUM-OF-DOC := #WS-NUM-OF-DOC
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_S_Amt_Withld().setValue(pnd_Work_Area_Pnd_Ws_Amt_Withld);                                                                //Natural: ASSIGN #TWRL117A-S-AMT-WITHLD := #WS-AMT-WITHLD
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_S_Amout_Paid().setValue(pnd_Work_Area_Pnd_Ws_Amout_Paid);                                                                //Natural: ASSIGN #TWRL117A-S-AMOUT-PAID := #WS-AMOUT-PAID
        ldaTwrl117a.getPnd_Twrl117a_Pnd_Twrl117a_S_Taxpayer_Type().setValue("C");                                                                                         //Natural: ASSIGN #TWRL117A-S-TAXPAYER-TYPE := 'C'
        getWorkFiles().write(1, false, ldaTwrl117a.getPnd_Twrl117a());                                                                                                    //Natural: WRITE WORK FILE 1 #TWRL117A
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=56 LS=133 ES=ON");
        Global.format(1, "PS=60 LS=133 ES=ON");
        Global.format(2, "PS=60 LS=133 ES=ON");
        Global.format(3, "PS=60 LS=133 ES=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:IIAP"),new TabSetting(28),"TaxWaRS - Annual Original Tax Report - Totals  ",new 
            TabSetting(92),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(34),pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year,"- Puerto Rico - Form 480.6A",NEWLINE,NEWLINE,NEWLINE,new TabSetting(26),"  Accepted       Rejected            Total",NEWLINE,new 
            TabSetting(26),"  --------       --------            -----");
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:IIAP"),new TabSetting(28),"TaxWaRS - Annual Original Tax Report - Accepted",new 
            TabSetting(92),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(32),pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year,"-",pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),"- Puerto Rico - Form 480.6A",NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:IIAP"),new TabSetting(28),"TaxWaRS - Annual Original Tax Report - Rejected",new 
            TabSetting(92),"Page:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(32),pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year,"-",pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),"- Puerto Rico - Form 480.6A",NEWLINE);

        getReports().setDisplayColumns(2, "/ TIN ",
        		ldaTwrl9710.getForm_Tirf_Tin(),"TIN/Typ",
        		ldaTwrl9710.getForm_Tirf_Tax_Id_Type(),"/Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"/Payee",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"/  Pension",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"/ Interest",
        		ldaTwrl9710.getForm_Tirf_Int_Amt(), new ReportEditMask ("ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(3, "/ TIN ",
        		ldaTwrl9710.getForm_Tirf_Tin(),"TIN/Typ",
        		ldaTwrl9710.getForm_Tirf_Tax_Id_Type(),"/Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"/Payee",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"/  Pension",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"/ Interest",
        		ldaTwrl9710.getForm_Tirf_Int_Amt(), new ReportEditMask ("ZZZ,ZZ9.99"));
    }
}
