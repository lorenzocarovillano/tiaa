/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:44:55 PM
**        * FROM NATURAL PROGRAM : Twrp8170
************************************************************
**        * FILE NAME            : Twrp8170.java
**        * CLASS NAME           : Twrp8170
**        * INSTANCE NAME        : Twrp8170
************************************************************
************************************************************************
* PROGRAM     :  TWRP8170
* SYSTEM      :  TAX WITHHOLDINGS AND REPORTING SYSTEM.
* AUTHOR      :  RIAD A. LOUTFI
* PURPOSE     :  CREATE A TABLE OF NEW TAX YEAR REPORTING FORMS USING
*                AN OLD TAX YEAR REPORTING FORMS TABLE.
*                TABLE NUMBER 5
* DATE        :  09/15/2000
*
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp8170 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_rf;
    private DbsField rf_Tircntl_Tbl_Nbr;
    private DbsField rf_Tircntl_Tax_Year;
    private DbsField rf_Tircntl_Seq_Nbr;

    private DbsGroup rf_Tircntl_Feeder_Sys_Tbl;
    private DbsField rf_Tircntl_Rpt_Source_Code;
    private DbsField rf_Tircntl_Company_Cde;
    private DbsField rf_Tircntl_Frm_Intrfce_Dte;
    private DbsField rf_Tircntl_To_Intrfce_Dte;
    private DbsField rf_Tircntl_Input_Recs_Tot;
    private DbsField rf_Tircntl_Input_Gross_Amt;
    private DbsField rf_Tircntl_Input_Ivt_Amt;
    private DbsField rf_Tircntl_Input_Int_Amt;
    private DbsField rf_Tircntl_Input_Nra_Amt;
    private DbsField rf_Tircntl_Input_Can_Amt;
    private DbsField rf_Tircntl_Input_Fed_Wthldg_Amt;
    private DbsField rf_Tircntl_Input_State_Wthldg_Amt;
    private DbsField rf_Tircntl_Input_Local_Wthldg_Amt;
    private DbsField rf_Tircntl_Rpt_Source_Code_Desc;
    private DbsField rf_Tircntl_Rpt_Update_Dte_Time;

    private DataAccessProgramView vw_uf;
    private DbsField uf_Tircntl_Tbl_Nbr;
    private DbsField uf_Tircntl_Tax_Year;
    private DbsField uf_Tircntl_Seq_Nbr;

    private DbsGroup uf_Tircntl_Feeder_Sys_Tbl;
    private DbsField uf_Tircntl_Rpt_Source_Code;
    private DbsField uf_Tircntl_Company_Cde;
    private DbsField uf_Tircntl_Frm_Intrfce_Dte;
    private DbsField uf_Tircntl_To_Intrfce_Dte;
    private DbsField uf_Tircntl_Input_Recs_Tot;
    private DbsField uf_Tircntl_Input_Gross_Amt;
    private DbsField uf_Tircntl_Input_Ivt_Amt;
    private DbsField uf_Tircntl_Input_Int_Amt;
    private DbsField uf_Tircntl_Input_Nra_Amt;
    private DbsField uf_Tircntl_Input_Can_Amt;
    private DbsField uf_Tircntl_Input_Fed_Wthldg_Amt;
    private DbsField uf_Tircntl_Input_State_Wthldg_Amt;
    private DbsField uf_Tircntl_Input_Local_Wthldg_Amt;
    private DbsField uf_Tircntl_Rpt_Source_Code_Desc;
    private DbsField uf_Tircntl_Rpt_Update_Dte_Time;

    private DataAccessProgramView vw_sf;
    private DbsField sf_Tircntl_Tbl_Nbr;
    private DbsField sf_Tircntl_Tax_Year;
    private DbsField sf_Tircntl_Seq_Nbr;

    private DbsGroup sf_Tircntl_Feeder_Sys_Tbl;
    private DbsField sf_Tircntl_Rpt_Source_Code;
    private DbsField sf_Tircntl_Company_Cde;
    private DbsField sf_Tircntl_Frm_Intrfce_Dte;
    private DbsField sf_Tircntl_To_Intrfce_Dte;
    private DbsField sf_Tircntl_Input_Recs_Tot;
    private DbsField sf_Tircntl_Input_Gross_Amt;
    private DbsField sf_Tircntl_Input_Ivt_Amt;
    private DbsField sf_Tircntl_Input_Int_Amt;
    private DbsField sf_Tircntl_Input_Nra_Amt;
    private DbsField sf_Tircntl_Input_Can_Amt;
    private DbsField sf_Tircntl_Input_Fed_Wthldg_Amt;
    private DbsField sf_Tircntl_Input_State_Wthldg_Amt;
    private DbsField sf_Tircntl_Input_Local_Wthldg_Amt;
    private DbsField sf_Tircntl_Rpt_Source_Code_Desc;
    private DbsField sf_Tircntl_Rpt_Update_Dte_Time;
    private DbsField pnd_Read1_Ctr;
    private DbsField pnd_Read2_Ctr;
    private DbsField pnd_Updt_Ctr;
    private DbsField pnd_Store_Ctr;
    private DbsField pnd_Old_Tax_Year;

    private DbsGroup pnd_Old_Tax_Year__R_Field_1;
    private DbsField pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N;
    private DbsField pnd_New_Tax_Year;

    private DbsGroup pnd_New_Tax_Year__R_Field_2;
    private DbsField pnd_New_Tax_Year_Pnd_New_Tax_Year_N;
    private DbsField pnd_Super1;

    private DbsGroup pnd_Super1__R_Field_3;
    private DbsField pnd_Super1_Pnd_S1_Tbl_Nbr;
    private DbsField pnd_Super1_Pnd_S1_Tax_Year;
    private DbsField pnd_Super1_Pnd_S1_Source_Code;
    private DbsField pnd_Super2;

    private DbsGroup pnd_Super2__R_Field_4;
    private DbsField pnd_Super2_Pnd_S2_Tbl_Nbr;
    private DbsField pnd_Super2_Pnd_S2_Tax_Year;
    private DbsField pnd_Super2_Pnd_S2_Source_Code;
    private DbsField pnd_New_Record_Found;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_rf = new DataAccessProgramView(new NameInfo("vw_rf", "RF"), "TIRCNTL_FEEDER_SYS_TBL_VIEW", "TIR_CONTROL");
        rf_Tircntl_Tbl_Nbr = vw_rf.getRecord().newFieldInGroup("rf_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        rf_Tircntl_Tax_Year = vw_rf.getRecord().newFieldInGroup("rf_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        rf_Tircntl_Seq_Nbr = vw_rf.getRecord().newFieldInGroup("rf_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        rf_Tircntl_Feeder_Sys_Tbl = vw_rf.getRecord().newGroupInGroup("RF_TIRCNTL_FEEDER_SYS_TBL", "TIRCNTL-FEEDER-SYS-TBL");
        rf_Tircntl_Rpt_Source_Code = rf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("rf_Tircntl_Rpt_Source_Code", "TIRCNTL-RPT-SOURCE-CODE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TIRCNTL_RPT_SOURCE_CODE");
        rf_Tircntl_Company_Cde = rf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("rf_Tircntl_Company_Cde", "TIRCNTL-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_COMPANY_CDE");
        rf_Tircntl_Frm_Intrfce_Dte = rf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("rf_Tircntl_Frm_Intrfce_Dte", "TIRCNTL-FRM-INTRFCE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_FRM_INTRFCE_DTE");
        rf_Tircntl_To_Intrfce_Dte = rf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("rf_Tircntl_To_Intrfce_Dte", "TIRCNTL-TO-INTRFCE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_TO_INTRFCE_DTE");
        rf_Tircntl_Input_Recs_Tot = rf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("rf_Tircntl_Input_Recs_Tot", "TIRCNTL-INPUT-RECS-TOT", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_RECS_TOT");
        rf_Tircntl_Input_Gross_Amt = rf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("rf_Tircntl_Input_Gross_Amt", "TIRCNTL-INPUT-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_GROSS_AMT");
        rf_Tircntl_Input_Ivt_Amt = rf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("rf_Tircntl_Input_Ivt_Amt", "TIRCNTL-INPUT-IVT-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_IVT_AMT");
        rf_Tircntl_Input_Int_Amt = rf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("rf_Tircntl_Input_Int_Amt", "TIRCNTL-INPUT-INT-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_INT_AMT");
        rf_Tircntl_Input_Nra_Amt = rf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("rf_Tircntl_Input_Nra_Amt", "TIRCNTL-INPUT-NRA-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_NRA_AMT");
        rf_Tircntl_Input_Can_Amt = rf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("rf_Tircntl_Input_Can_Amt", "TIRCNTL-INPUT-CAN-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_CAN_AMT");
        rf_Tircntl_Input_Fed_Wthldg_Amt = rf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("rf_Tircntl_Input_Fed_Wthldg_Amt", "TIRCNTL-INPUT-FED-WTHLDG-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_FED_WTHLDG_AMT");
        rf_Tircntl_Input_State_Wthldg_Amt = rf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("rf_Tircntl_Input_State_Wthldg_Amt", "TIRCNTL-INPUT-STATE-WTHLDG-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_STATE_WTHLDG_AMT");
        rf_Tircntl_Input_Local_Wthldg_Amt = rf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("rf_Tircntl_Input_Local_Wthldg_Amt", "TIRCNTL-INPUT-LOCAL-WTHLDG-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_LOCAL_WTHLDG_AMT");
        rf_Tircntl_Rpt_Source_Code_Desc = rf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("rf_Tircntl_Rpt_Source_Code_Desc", "TIRCNTL-RPT-SOURCE-CODE-DESC", 
            FieldType.STRING, 23, RepeatingFieldStrategy.None, "TIRCNTL_RPT_SOURCE_CODE_DESC");
        rf_Tircntl_Rpt_Update_Dte_Time = rf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("rf_Tircntl_Rpt_Update_Dte_Time", "TIRCNTL-RPT-UPDATE-DTE-TIME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "TIRCNTL_RPT_UPDATE_DTE_TIME");
        registerRecord(vw_rf);

        vw_uf = new DataAccessProgramView(new NameInfo("vw_uf", "UF"), "TIRCNTL_FEEDER_SYS_TBL_VIEW", "TIR_CONTROL");
        uf_Tircntl_Tbl_Nbr = vw_uf.getRecord().newFieldInGroup("uf_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        uf_Tircntl_Tax_Year = vw_uf.getRecord().newFieldInGroup("uf_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        uf_Tircntl_Seq_Nbr = vw_uf.getRecord().newFieldInGroup("uf_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        uf_Tircntl_Feeder_Sys_Tbl = vw_uf.getRecord().newGroupInGroup("UF_TIRCNTL_FEEDER_SYS_TBL", "TIRCNTL-FEEDER-SYS-TBL");
        uf_Tircntl_Rpt_Source_Code = uf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("uf_Tircntl_Rpt_Source_Code", "TIRCNTL-RPT-SOURCE-CODE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TIRCNTL_RPT_SOURCE_CODE");
        uf_Tircntl_Company_Cde = uf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("uf_Tircntl_Company_Cde", "TIRCNTL-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_COMPANY_CDE");
        uf_Tircntl_Frm_Intrfce_Dte = uf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("uf_Tircntl_Frm_Intrfce_Dte", "TIRCNTL-FRM-INTRFCE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_FRM_INTRFCE_DTE");
        uf_Tircntl_To_Intrfce_Dte = uf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("uf_Tircntl_To_Intrfce_Dte", "TIRCNTL-TO-INTRFCE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_TO_INTRFCE_DTE");
        uf_Tircntl_Input_Recs_Tot = uf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("uf_Tircntl_Input_Recs_Tot", "TIRCNTL-INPUT-RECS-TOT", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_RECS_TOT");
        uf_Tircntl_Input_Gross_Amt = uf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("uf_Tircntl_Input_Gross_Amt", "TIRCNTL-INPUT-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_GROSS_AMT");
        uf_Tircntl_Input_Ivt_Amt = uf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("uf_Tircntl_Input_Ivt_Amt", "TIRCNTL-INPUT-IVT-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_IVT_AMT");
        uf_Tircntl_Input_Int_Amt = uf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("uf_Tircntl_Input_Int_Amt", "TIRCNTL-INPUT-INT-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_INT_AMT");
        uf_Tircntl_Input_Nra_Amt = uf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("uf_Tircntl_Input_Nra_Amt", "TIRCNTL-INPUT-NRA-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_NRA_AMT");
        uf_Tircntl_Input_Can_Amt = uf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("uf_Tircntl_Input_Can_Amt", "TIRCNTL-INPUT-CAN-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_CAN_AMT");
        uf_Tircntl_Input_Fed_Wthldg_Amt = uf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("uf_Tircntl_Input_Fed_Wthldg_Amt", "TIRCNTL-INPUT-FED-WTHLDG-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_FED_WTHLDG_AMT");
        uf_Tircntl_Input_State_Wthldg_Amt = uf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("uf_Tircntl_Input_State_Wthldg_Amt", "TIRCNTL-INPUT-STATE-WTHLDG-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_STATE_WTHLDG_AMT");
        uf_Tircntl_Input_Local_Wthldg_Amt = uf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("uf_Tircntl_Input_Local_Wthldg_Amt", "TIRCNTL-INPUT-LOCAL-WTHLDG-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_LOCAL_WTHLDG_AMT");
        uf_Tircntl_Rpt_Source_Code_Desc = uf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("uf_Tircntl_Rpt_Source_Code_Desc", "TIRCNTL-RPT-SOURCE-CODE-DESC", 
            FieldType.STRING, 23, RepeatingFieldStrategy.None, "TIRCNTL_RPT_SOURCE_CODE_DESC");
        uf_Tircntl_Rpt_Update_Dte_Time = uf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("uf_Tircntl_Rpt_Update_Dte_Time", "TIRCNTL-RPT-UPDATE-DTE-TIME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "TIRCNTL_RPT_UPDATE_DTE_TIME");
        registerRecord(vw_uf);

        vw_sf = new DataAccessProgramView(new NameInfo("vw_sf", "SF"), "TIRCNTL_FEEDER_SYS_TBL_VIEW", "TIR_CONTROL");
        sf_Tircntl_Tbl_Nbr = vw_sf.getRecord().newFieldInGroup("sf_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        sf_Tircntl_Tax_Year = vw_sf.getRecord().newFieldInGroup("sf_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        sf_Tircntl_Seq_Nbr = vw_sf.getRecord().newFieldInGroup("sf_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        sf_Tircntl_Feeder_Sys_Tbl = vw_sf.getRecord().newGroupInGroup("SF_TIRCNTL_FEEDER_SYS_TBL", "TIRCNTL-FEEDER-SYS-TBL");
        sf_Tircntl_Rpt_Source_Code = sf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("sf_Tircntl_Rpt_Source_Code", "TIRCNTL-RPT-SOURCE-CODE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TIRCNTL_RPT_SOURCE_CODE");
        sf_Tircntl_Company_Cde = sf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("sf_Tircntl_Company_Cde", "TIRCNTL-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_COMPANY_CDE");
        sf_Tircntl_Frm_Intrfce_Dte = sf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("sf_Tircntl_Frm_Intrfce_Dte", "TIRCNTL-FRM-INTRFCE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_FRM_INTRFCE_DTE");
        sf_Tircntl_To_Intrfce_Dte = sf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("sf_Tircntl_To_Intrfce_Dte", "TIRCNTL-TO-INTRFCE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_TO_INTRFCE_DTE");
        sf_Tircntl_Input_Recs_Tot = sf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("sf_Tircntl_Input_Recs_Tot", "TIRCNTL-INPUT-RECS-TOT", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_RECS_TOT");
        sf_Tircntl_Input_Gross_Amt = sf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("sf_Tircntl_Input_Gross_Amt", "TIRCNTL-INPUT-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_GROSS_AMT");
        sf_Tircntl_Input_Ivt_Amt = sf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("sf_Tircntl_Input_Ivt_Amt", "TIRCNTL-INPUT-IVT-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_IVT_AMT");
        sf_Tircntl_Input_Int_Amt = sf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("sf_Tircntl_Input_Int_Amt", "TIRCNTL-INPUT-INT-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_INT_AMT");
        sf_Tircntl_Input_Nra_Amt = sf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("sf_Tircntl_Input_Nra_Amt", "TIRCNTL-INPUT-NRA-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_NRA_AMT");
        sf_Tircntl_Input_Can_Amt = sf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("sf_Tircntl_Input_Can_Amt", "TIRCNTL-INPUT-CAN-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_CAN_AMT");
        sf_Tircntl_Input_Fed_Wthldg_Amt = sf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("sf_Tircntl_Input_Fed_Wthldg_Amt", "TIRCNTL-INPUT-FED-WTHLDG-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_FED_WTHLDG_AMT");
        sf_Tircntl_Input_State_Wthldg_Amt = sf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("sf_Tircntl_Input_State_Wthldg_Amt", "TIRCNTL-INPUT-STATE-WTHLDG-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_STATE_WTHLDG_AMT");
        sf_Tircntl_Input_Local_Wthldg_Amt = sf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("sf_Tircntl_Input_Local_Wthldg_Amt", "TIRCNTL-INPUT-LOCAL-WTHLDG-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_LOCAL_WTHLDG_AMT");
        sf_Tircntl_Rpt_Source_Code_Desc = sf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("sf_Tircntl_Rpt_Source_Code_Desc", "TIRCNTL-RPT-SOURCE-CODE-DESC", 
            FieldType.STRING, 23, RepeatingFieldStrategy.None, "TIRCNTL_RPT_SOURCE_CODE_DESC");
        sf_Tircntl_Rpt_Update_Dte_Time = sf_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("sf_Tircntl_Rpt_Update_Dte_Time", "TIRCNTL-RPT-UPDATE-DTE-TIME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "TIRCNTL_RPT_UPDATE_DTE_TIME");
        registerRecord(vw_sf);

        pnd_Read1_Ctr = localVariables.newFieldInRecord("pnd_Read1_Ctr", "#READ1-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Read2_Ctr = localVariables.newFieldInRecord("pnd_Read2_Ctr", "#READ2-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Updt_Ctr = localVariables.newFieldInRecord("pnd_Updt_Ctr", "#UPDT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Store_Ctr = localVariables.newFieldInRecord("pnd_Store_Ctr", "#STORE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Old_Tax_Year = localVariables.newFieldInRecord("pnd_Old_Tax_Year", "#OLD-TAX-YEAR", FieldType.STRING, 4);

        pnd_Old_Tax_Year__R_Field_1 = localVariables.newGroupInRecord("pnd_Old_Tax_Year__R_Field_1", "REDEFINE", pnd_Old_Tax_Year);
        pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N = pnd_Old_Tax_Year__R_Field_1.newFieldInGroup("pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N", "#OLD-TAX-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_New_Tax_Year = localVariables.newFieldInRecord("pnd_New_Tax_Year", "#NEW-TAX-YEAR", FieldType.STRING, 4);

        pnd_New_Tax_Year__R_Field_2 = localVariables.newGroupInRecord("pnd_New_Tax_Year__R_Field_2", "REDEFINE", pnd_New_Tax_Year);
        pnd_New_Tax_Year_Pnd_New_Tax_Year_N = pnd_New_Tax_Year__R_Field_2.newFieldInGroup("pnd_New_Tax_Year_Pnd_New_Tax_Year_N", "#NEW-TAX-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_Super1 = localVariables.newFieldInRecord("pnd_Super1", "#SUPER1", FieldType.STRING, 7);

        pnd_Super1__R_Field_3 = localVariables.newGroupInRecord("pnd_Super1__R_Field_3", "REDEFINE", pnd_Super1);
        pnd_Super1_Pnd_S1_Tbl_Nbr = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Tbl_Nbr", "#S1-TBL-NBR", FieldType.NUMERIC, 1);
        pnd_Super1_Pnd_S1_Tax_Year = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Tax_Year", "#S1-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Super1_Pnd_S1_Source_Code = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Source_Code", "#S1-SOURCE-CODE", FieldType.STRING, 2);
        pnd_Super2 = localVariables.newFieldInRecord("pnd_Super2", "#SUPER2", FieldType.STRING, 7);

        pnd_Super2__R_Field_4 = localVariables.newGroupInRecord("pnd_Super2__R_Field_4", "REDEFINE", pnd_Super2);
        pnd_Super2_Pnd_S2_Tbl_Nbr = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Tbl_Nbr", "#S2-TBL-NBR", FieldType.NUMERIC, 1);
        pnd_Super2_Pnd_S2_Tax_Year = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Tax_Year", "#S2-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Super2_Pnd_S2_Source_Code = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Source_Code", "#S2-SOURCE-CODE", FieldType.STRING, 2);
        pnd_New_Record_Found = localVariables.newFieldInRecord("pnd_New_Record_Found", "#NEW-RECORD-FOUND", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_rf.reset();
        vw_uf.reset();
        vw_sf.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp8170() throws Exception
    {
        super("Twrp8170");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp8170|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Old_Tax_Year,pnd_New_Tax_Year);                                                                    //Natural: INPUT #OLD-TAX-YEAR #NEW-TAX-YEAR
                getReports().display(0, pnd_Old_Tax_Year,pnd_New_Tax_Year);                                                                                               //Natural: DISPLAY ( 00 ) #OLD-TAX-YEAR #NEW-TAX-YEAR
                if (Global.isEscape()) return;
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                pnd_Super1_Pnd_S1_Tbl_Nbr.setValue(5);                                                                                                                    //Natural: ASSIGN #S1-TBL-NBR := 5
                pnd_Super1_Pnd_S1_Tax_Year.setValue(pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N);                                                                                 //Natural: ASSIGN #S1-TAX-YEAR := #OLD-TAX-YEAR-N
                pnd_Super1_Pnd_S1_Source_Code.setValue(" ");                                                                                                              //Natural: ASSIGN #S1-SOURCE-CODE := ' '
                vw_rf.startDatabaseRead                                                                                                                                   //Natural: READ RF WITH TIRCNTL-NBR-YEAR-FEEDER-CDE = #SUPER1
                (
                "READ01",
                new Wc[] { new Wc("TIRCNTL_NBR_YEAR_FEEDER_CDE", ">=", pnd_Super1, WcType.BY) },
                new Oc[] { new Oc("TIRCNTL_NBR_YEAR_FEEDER_CDE", "ASC") }
                );
                READ01:
                while (condition(vw_rf.readNextRow("READ01")))
                {
                    if (condition(rf_Tircntl_Tbl_Nbr.equals(pnd_Super1_Pnd_S1_Tbl_Nbr) && rf_Tircntl_Tax_Year.equals(pnd_Super1_Pnd_S1_Tax_Year)))                        //Natural: IF RF.TIRCNTL-TBL-NBR = #S1-TBL-NBR AND RF.TIRCNTL-TAX-YEAR = #S1-TAX-YEAR
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(rf_Tircntl_Rpt_Source_Code_Desc.equals(" ")))                                                                                           //Natural: REJECT IF RF.TIRCNTL-RPT-SOURCE-CODE-DESC = ' '
                    {
                        continue;
                    }
                    pnd_Read1_Ctr.nadd(1);                                                                                                                                //Natural: ADD 1 TO #READ1-CTR
                    pnd_New_Record_Found.setValue(false);                                                                                                                 //Natural: ASSIGN #NEW-RECORD-FOUND := FALSE
                    pnd_Super2_Pnd_S2_Tbl_Nbr.setValue(5);                                                                                                                //Natural: ASSIGN #S2-TBL-NBR := 5
                    pnd_Super2_Pnd_S2_Tax_Year.setValue(pnd_New_Tax_Year_Pnd_New_Tax_Year_N);                                                                             //Natural: ASSIGN #S2-TAX-YEAR := #NEW-TAX-YEAR-N
                    pnd_Super2_Pnd_S2_Source_Code.setValue(rf_Tircntl_Rpt_Source_Code);                                                                                   //Natural: ASSIGN #S2-SOURCE-CODE := RF.TIRCNTL-RPT-SOURCE-CODE
                    vw_uf.startDatabaseRead                                                                                                                               //Natural: READ UF WITH TIRCNTL-NBR-YEAR-FEEDER-CDE = #SUPER2
                    (
                    "RL2",
                    new Wc[] { new Wc("TIRCNTL_NBR_YEAR_FEEDER_CDE", ">=", pnd_Super2, WcType.BY) },
                    new Oc[] { new Oc("TIRCNTL_NBR_YEAR_FEEDER_CDE", "ASC") }
                    );
                    RL2:
                    while (condition(vw_uf.readNextRow("RL2")))
                    {
                        if (condition(uf_Tircntl_Tbl_Nbr.equals(pnd_Super2_Pnd_S2_Tbl_Nbr) && uf_Tircntl_Tax_Year.equals(pnd_Super2_Pnd_S2_Tax_Year) &&                   //Natural: IF UF.TIRCNTL-TBL-NBR = #S2-TBL-NBR AND UF.TIRCNTL-TAX-YEAR = #S2-TAX-YEAR AND UF.TIRCNTL-RPT-SOURCE-CODE = #S2-SOURCE-CODE
                            uf_Tircntl_Rpt_Source_Code.equals(pnd_Super2_Pnd_S2_Source_Code)))
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(uf_Tircntl_Rpt_Source_Code_Desc.equals(" ")))                                                                                       //Natural: REJECT IF UF.TIRCNTL-RPT-SOURCE-CODE-DESC = ' '
                        {
                            continue;
                        }
                        pnd_New_Record_Found.setValue(true);                                                                                                              //Natural: ASSIGN #NEW-RECORD-FOUND := TRUE
                        pnd_Read2_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #READ2-CTR
                        //*  (RL2.)
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_New_Record_Found.equals(false)))                                                                                                    //Natural: IF #NEW-RECORD-FOUND = FALSE
                    {
                        sf_Tircntl_Tbl_Nbr.setValue(rf_Tircntl_Tbl_Nbr);                                                                                                  //Natural: ASSIGN SF.TIRCNTL-TBL-NBR := RF.TIRCNTL-TBL-NBR
                        sf_Tircntl_Tax_Year.setValue(pnd_New_Tax_Year_Pnd_New_Tax_Year_N);                                                                                //Natural: ASSIGN SF.TIRCNTL-TAX-YEAR := #NEW-TAX-YEAR-N
                        sf_Tircntl_Seq_Nbr.setValue(rf_Tircntl_Seq_Nbr);                                                                                                  //Natural: ASSIGN SF.TIRCNTL-SEQ-NBR := RF.TIRCNTL-SEQ-NBR
                        sf_Tircntl_Rpt_Source_Code.setValue(rf_Tircntl_Rpt_Source_Code);                                                                                  //Natural: ASSIGN SF.TIRCNTL-RPT-SOURCE-CODE := RF.TIRCNTL-RPT-SOURCE-CODE
                        sf_Tircntl_Rpt_Source_Code_Desc.setValue(rf_Tircntl_Rpt_Source_Code_Desc);                                                                        //Natural: ASSIGN SF.TIRCNTL-RPT-SOURCE-CODE-DESC := RF.TIRCNTL-RPT-SOURCE-CODE-DESC
                        vw_sf.insertDBRow();                                                                                                                              //Natural: STORE SF
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                        pnd_Store_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STORE-CTR
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (RL1.)
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                pnd_Super1_Pnd_S1_Tbl_Nbr.setValue(5);                                                                                                                    //Natural: ASSIGN #S1-TBL-NBR := 5
                pnd_Super1_Pnd_S1_Tax_Year.setValue(0);                                                                                                                   //Natural: ASSIGN #S1-TAX-YEAR := 0
                pnd_Super1_Pnd_S1_Source_Code.setValue(" ");                                                                                                              //Natural: ASSIGN #S1-SOURCE-CODE := ' '
                vw_rf.startDatabaseRead                                                                                                                                   //Natural: READ RF WITH TIRCNTL-NBR-YEAR-FEEDER-CDE = #SUPER1
                (
                "RL3",
                new Wc[] { new Wc("TIRCNTL_NBR_YEAR_FEEDER_CDE", ">=", pnd_Super1, WcType.BY) },
                new Oc[] { new Oc("TIRCNTL_NBR_YEAR_FEEDER_CDE", "ASC") }
                );
                RL3:
                while (condition(vw_rf.readNextRow("RL3")))
                {
                    if (condition(rf_Tircntl_Tbl_Nbr.equals(5)))                                                                                                          //Natural: IF RF.TIRCNTL-TBL-NBR = 5
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(rf_Tircntl_Rpt_Source_Code_Desc.equals(" ")))                                                                                           //Natural: REJECT IF RF.TIRCNTL-RPT-SOURCE-CODE-DESC = ' '
                    {
                        continue;
                    }
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),rf_Tircntl_Tbl_Nbr,new TabSetting(4),rf_Tircntl_Tax_Year,new          //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T TIRCNTL-TBL-NBR 04T TIRCNTL-TAX-YEAR 10T TIRCNTL-SEQ-NBR 16T TIRCNTL-RPT-SOURCE-CODE 19T TIRCNTL-COMPANY-CDE 21T TIRCNTL-FRM-INTRFCE-DTE ( EM = ZZZZZZZZ ) 31T TIRCNTL-TO-INTRFCE-DTE ( EM = ZZZZZZZZ ) 41T TIRCNTL-INPUT-RECS-TOT ( EM = ZZZZ,ZZZ ) 52T TIRCNTL-INPUT-GROSS-AMT 68T TIRCNTL-INPUT-FED-WTHLDG-AMT 84T TIRCNTL-INPUT-STATE-WTHLDG-AMT 100T TIRCNTL-INPUT-LOCAL-WTHLDG-AMT 116T TIRCNTL-RPT-UPDATE-DTE-TIME
                        TabSetting(10),rf_Tircntl_Seq_Nbr,new TabSetting(16),rf_Tircntl_Rpt_Source_Code,new TabSetting(19),rf_Tircntl_Company_Cde,new TabSetting(21),rf_Tircntl_Frm_Intrfce_Dte, 
                        new ReportEditMask ("ZZZZZZZZ"),new TabSetting(31),rf_Tircntl_To_Intrfce_Dte, new ReportEditMask ("ZZZZZZZZ"),new TabSetting(41),rf_Tircntl_Input_Recs_Tot, 
                        new ReportEditMask ("ZZZZ,ZZZ"),new TabSetting(52),rf_Tircntl_Input_Gross_Amt,new TabSetting(68),rf_Tircntl_Input_Fed_Wthldg_Amt,new 
                        TabSetting(84),rf_Tircntl_Input_State_Wthldg_Amt,new TabSetting(100),rf_Tircntl_Input_Local_Wthldg_Amt,new TabSetting(116),rf_Tircntl_Rpt_Update_Dte_Time);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),"Desc :",rf_Tircntl_Rpt_Source_Code_Desc,new TabSetting(52),rf_Tircntl_Input_Ivt_Amt,new  //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T 'Desc :' TIRCNTL-RPT-SOURCE-CODE-DESC 52T TIRCNTL-INPUT-IVT-AMT 68T TIRCNTL-INPUT-INT-AMT 84T TIRCNTL-INPUT-NRA-AMT 100T TIRCNTL-INPUT-CAN-AMT
                        TabSetting(68),rf_Tircntl_Input_Int_Amt,new TabSetting(84),rf_Tircntl_Input_Nra_Amt,new TabSetting(100),rf_Tircntl_Input_Can_Amt);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  (RL3.)
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getReports().skip(0, 4);                                                                                                                                  //Natural: SKIP ( 00 ) 4
                getReports().print(0, "Old Tax Year Records Read................",pnd_Read1_Ctr);                                                                         //Natural: PRINT ( 00 ) 'Old Tax Year Records Read................' #READ1-CTR
                getReports().print(0, "New Tax Year Records Found...............",pnd_Read2_Ctr);                                                                         //Natural: PRINT ( 00 ) 'New Tax Year Records Found...............' #READ2-CTR
                getReports().print(0, "New Tax Year Records Stored..............",pnd_Store_Ctr);                                                                         //Natural: PRINT ( 00 ) 'New Tax Year Records Stored..............' #STORE-CTR
                getReports().skip(1, 4);                                                                                                                                  //Natural: SKIP ( 01 ) 4
                getReports().print(1, "Old Tax Year Records Read................",pnd_Read1_Ctr);                                                                         //Natural: PRINT ( 01 ) 'Old Tax Year Records Read................' #READ1-CTR
                getReports().print(1, "New Tax Year Records Found...............",pnd_Read2_Ctr);                                                                         //Natural: PRINT ( 01 ) 'New Tax Year Records Found...............' #READ2-CTR
                getReports().print(1, "New Tax Year Records Stored..............",pnd_Store_Ctr);                                                                         //Natural: PRINT ( 01 ) 'New Tax Year Records Stored..............' #STORE-CTR
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( 01 )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"Feeder System Table Report",new          //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 53T 'Feeder System Table Report' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                    getReports().skip(1, 2);                                                                                                                              //Natural: SKIP ( 01 ) 2 LINES
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TBL",new TabSetting(5),"Tax ",new TabSetting(11),"Seq",new TabSetting(16),"SR",new      //Natural: WRITE ( 01 ) NOTITLE 01T 'TBL' 05T 'Tax ' 11T 'Seq' 16T 'SR' 19T 'C' 21T 'intrface' 31T '   To   ' 41T 'Records ' 52T ' Gross Amount  ' 68T ' Fed Wtlhd Amt ' 84T ' Sta Wthld Amt ' 100T ' Loc Wthld Amt ' 117T '  Update Date  '
                        TabSetting(19),"C",new TabSetting(21),"intrface",new TabSetting(31),"   To   ",new TabSetting(41),"Records ",new TabSetting(52)," Gross Amount  ",new 
                        TabSetting(68)," Fed Wtlhd Amt ",new TabSetting(84)," Sta Wthld Amt ",new TabSetting(100)," Loc Wthld Amt ",new TabSetting(117),
                        "  Update Date  ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"NBR",new TabSetting(5),"Year",new TabSetting(11),"No.",new TabSetting(16),"CE",new      //Natural: WRITE ( 01 ) NOTITLE 01T 'NBR' 05T 'Year' 11T 'No.' 16T 'CE' 19T 'O' 21T '  Date  ' 31T '  Date  ' 41T 'Procesed' 52T '  IVC Amount   ' 68T '  INT Amount   ' 84T '  NRA Amount   ' 100T '  CAN Amount   ' 117T '    and  Time  '
                        TabSetting(19),"O",new TabSetting(21),"  Date  ",new TabSetting(31),"  Date  ",new TabSetting(41),"Procesed",new TabSetting(52),"  IVC Amount   ",new 
                        TabSetting(68),"  INT Amount   ",new TabSetting(84),"  NRA Amount   ",new TabSetting(100),"  CAN Amount   ",new TabSetting(117),
                        "    and  Time  ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"===",new TabSetting(5),"====",new TabSetting(11),"===",new TabSetting(16),"==",new      //Natural: WRITE ( 01 ) NOTITLE 01T '===' 05T '====' 11T '===' 16T '==' 19T '=' 21T '========' 31T '========' 41T '========' 52T '===============' 68T '===============' 84T '===============' 100T '===============' 117T '==============='
                        TabSetting(19),"=",new TabSetting(21),"========",new TabSetting(31),"========",new TabSetting(41),"========",new TabSetting(52),"===============",new 
                        TabSetting(68),"===============",new TabSetting(84),"===============",new TabSetting(100),"===============",new TabSetting(117),
                        "===============");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");

        getReports().setDisplayColumns(0, pnd_Old_Tax_Year,pnd_New_Tax_Year);
    }
}
