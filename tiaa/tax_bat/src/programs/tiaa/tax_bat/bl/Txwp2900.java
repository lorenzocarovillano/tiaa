/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:46:03 PM
**        * FROM NATURAL PROGRAM : Txwp2900
************************************************************
**        * FILE NAME            : Txwp2900.java
**        * CLASS NAME           : Txwp2900
**        * INSTANCE NAME        : Txwp2900
************************************************************
************************************************************************
* PROGRAM...: TXWP2900
* FUNCTION..: THIS PROGRAM IS TO UPDATE PREVIOUS IA CUTOFF DATE WITH
*             INACTIVE MODE AND TO POPULATE NEW IA CUTOFF DATE WITH
*             ACTIVE MODE EACH MONTH. (RUN MONTHLY).
* PROGRAMER.: TIAA/CREF. (06/22/2001)
* HISTORY...:
*
* 12/02/02 RM UPDATE TXWL2900 TO INCLUDE THE 2ND IA CUTOFF DATE, SO TO
*             KEEP IN SYNC WITH THE UPDATE IN TXWL2905 (JOB P1280TWM).
*             THIS PROGRAM (JOB P1100TXM) SHOULD BE RUN 2 DAYS BEFORE
*             P1280TWM WAS RUN.
*
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Txwp2900 extends BLNatBase
{
    // Data Areas
    private LdaTxwl2900 ldaTxwl2900;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Ia_Date;
    private DbsField pnd_Ws_Pnd_Ia_Ts;
    private DbsField pnd_Ws_Pnd_Eff_Ts_X;
    private DbsField pnd_Ws_Pnd_Curr_Ts;
    private DbsField pnd_Ws_Pnd_Work_Ts;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Pnd_Work_Ts_Num;
    private DbsField pnd_Ws_Pnd_Inverse_Lu_Ts;
    private DbsField pnd_Ws_Pnd_Inverse_Eff_Ts;
    private DbsField pnd_Ws_Pnd_Msg;
    private DbsField pnd_T_Date;

    private DbsGroup pnd_T_Date__R_Field_2;
    private DbsField pnd_T_Date_Pnd_T_Date_D;
    private DbsField pnd_Ia_Dte_Ts;

    private DbsGroup pnd_Ia_Dte_Ts__R_Field_3;
    private DbsField pnd_Ia_Dte_Ts_Pnd_Ia_D;
    private DbsField pnd_Ia_Dd;
    private DbsField pnd_Ia_Eff2_Ts;
    private DbsField pnd_Ia_Eff_Ts;

    private DbsGroup pnd_Ia_Eff_Ts__R_Field_4;
    private DbsField pnd_Ia_Eff_Ts_Pnd_Ia_Eff_Ts_Num;
    private DbsField pnd_Ia_Inverse_Eff_Ts;
    private DbsField pnd_Upd_Ctr;
    private DbsField pnd_Rec_Found;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTxwl2900 = new LdaTxwl2900();
        registerRecord(ldaTxwl2900);
        registerRecord(ldaTxwl2900.getVw_ref_Tab());

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Ia_Date = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ia_Date", "#IA-DATE", FieldType.DATE);
        pnd_Ws_Pnd_Ia_Ts = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ia_Ts", "#IA-TS", FieldType.TIME);
        pnd_Ws_Pnd_Eff_Ts_X = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Eff_Ts_X", "#EFF-TS-X", FieldType.STRING, 15);
        pnd_Ws_Pnd_Curr_Ts = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Curr_Ts", "#CURR-TS", FieldType.TIME);
        pnd_Ws_Pnd_Work_Ts = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Work_Ts", "#WORK-TS", FieldType.TIME);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Work_Ts);
        pnd_Ws_Pnd_Work_Ts_Num = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Work_Ts_Num", "#WORK-TS-NUM", FieldType.PACKED_DECIMAL, 13);
        pnd_Ws_Pnd_Inverse_Lu_Ts = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Inverse_Lu_Ts", "#INVERSE-LU-TS", FieldType.PACKED_DECIMAL, 13);
        pnd_Ws_Pnd_Inverse_Eff_Ts = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Inverse_Eff_Ts", "#INVERSE-EFF-TS", FieldType.PACKED_DECIMAL, 13);
        pnd_Ws_Pnd_Msg = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Msg", "#MSG", FieldType.STRING, 20);
        pnd_T_Date = localVariables.newFieldInRecord("pnd_T_Date", "#T-DATE", FieldType.STRING, 15);

        pnd_T_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_T_Date__R_Field_2", "REDEFINE", pnd_T_Date);
        pnd_T_Date_Pnd_T_Date_D = pnd_T_Date__R_Field_2.newFieldInGroup("pnd_T_Date_Pnd_T_Date_D", "#T-DATE-D", FieldType.STRING, 8);
        pnd_Ia_Dte_Ts = localVariables.newFieldInRecord("pnd_Ia_Dte_Ts", "#IA-DTE-TS", FieldType.STRING, 15);

        pnd_Ia_Dte_Ts__R_Field_3 = localVariables.newGroupInRecord("pnd_Ia_Dte_Ts__R_Field_3", "REDEFINE", pnd_Ia_Dte_Ts);
        pnd_Ia_Dte_Ts_Pnd_Ia_D = pnd_Ia_Dte_Ts__R_Field_3.newFieldInGroup("pnd_Ia_Dte_Ts_Pnd_Ia_D", "#IA-D", FieldType.STRING, 8);
        pnd_Ia_Dd = localVariables.newFieldInRecord("pnd_Ia_Dd", "#IA-DD", FieldType.DATE);
        pnd_Ia_Eff2_Ts = localVariables.newFieldInRecord("pnd_Ia_Eff2_Ts", "#IA-EFF2-TS", FieldType.TIME);
        pnd_Ia_Eff_Ts = localVariables.newFieldInRecord("pnd_Ia_Eff_Ts", "#IA-EFF-TS", FieldType.TIME);

        pnd_Ia_Eff_Ts__R_Field_4 = localVariables.newGroupInRecord("pnd_Ia_Eff_Ts__R_Field_4", "REDEFINE", pnd_Ia_Eff_Ts);
        pnd_Ia_Eff_Ts_Pnd_Ia_Eff_Ts_Num = pnd_Ia_Eff_Ts__R_Field_4.newFieldInGroup("pnd_Ia_Eff_Ts_Pnd_Ia_Eff_Ts_Num", "#IA-EFF-TS-NUM", FieldType.PACKED_DECIMAL, 
            13);
        pnd_Ia_Inverse_Eff_Ts = localVariables.newFieldInRecord("pnd_Ia_Inverse_Eff_Ts", "#IA-INVERSE-EFF-TS", FieldType.PACKED_DECIMAL, 13);
        pnd_Upd_Ctr = localVariables.newFieldInRecord("pnd_Upd_Ctr", "#UPD-CTR", FieldType.PACKED_DECIMAL, 3);
        pnd_Rec_Found = localVariables.newFieldInRecord("pnd_Rec_Found", "#REC-FOUND", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTxwl2900.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Txwp2900() throws Exception
    {
        super("Txwp2900");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 0 ) PS = 23 LS = 80;//Natural: WRITE ( 0 ) TITLE LEFT *TIMX ( EM = MM/DD/YYYY�HH:IIAP ) 30T 'TIAA CREF Tax System' 67T 'Page  :' *PAGE-NUMBER ( 0 ) ( EM = Z,ZZ9 ) / *PROGRAM 30T 'IA CUTOFF TABLE' 67T 'Report:  RPT0' //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-LAST-ACTIVE-IA-CUTOFF-DATE
        sub_Get_Last_Active_Ia_Cutoff_Date();
        if (condition(Global.isEscape())) {return;}
        //*  NO ACTIVE IA CUTOFF DATE RECORD
        if (condition(pnd_Rec_Found.equals(false)))                                                                                                                       //Natural: IF #REC-FOUND = FALSE
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ++ IA CUTOFF DATE TS ++
        //*  MOVE EDITED *DATX (EM=YYYYMMDD) TO #IA-DTE-TS
        setValueToSubstring(pnd_T_Date_Pnd_T_Date_D,pnd_Ia_Dte_Ts,1,8);                                                                                                   //Natural: MOVE #T-DATE-D TO SUBSTR ( #IA-DTE-TS,1,8 )
        setValueToSubstring("0000001",pnd_Ia_Dte_Ts,9,7);                                                                                                                 //Natural: MOVE '0000001' TO SUBSTR ( #IA-DTE-TS,9,7 )
        pnd_Ia_Dd.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ia_Dte_Ts_Pnd_Ia_D);                                                                                  //Natural: MOVE EDITED #IA-D TO #IA-DD ( EM = YYYYMMDD )
        pnd_Ia_Dd.nadd(32);                                                                                                                                               //Natural: ADD 32 TO #IA-DD
        pnd_Ia_Dte_Ts_Pnd_Ia_D.setValueEdited(pnd_Ia_Dd,new ReportEditMask("YYYYMM01"));                                                                                  //Natural: MOVE EDITED #IA-DD ( EM = YYYYMM01 ) TO #IA-D
        pnd_Ia_Eff_Ts.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Ia_Dte_Ts);                                                                                //Natural: MOVE EDITED #IA-DTE-TS TO #IA-EFF-TS ( EM = YYYYMMDDHHIISST )
        pnd_Ia_Inverse_Eff_Ts.compute(new ComputeParameters(false, pnd_Ia_Inverse_Eff_Ts), new DbsDecimal("1000000000000").subtract(pnd_Ia_Eff_Ts_Pnd_Ia_Eff_Ts_Num));    //Natural: ASSIGN #IA-INVERSE-EFF-TS := 1000000000000 - #IA-EFF-TS-NUM
        //*  ++ LAST UPDATE DATE TS
        pnd_Ws_Pnd_Ia_Date.setValue(Global.getDATX());                                                                                                                    //Natural: MOVE *DATX TO #WS.#IA-DATE
        pnd_Ws_Pnd_Curr_Ts.setValue(Global.getTIMX());                                                                                                                    //Natural: ASSIGN #WS.#CURR-TS := #WS.#WORK-TS := *TIMX
        pnd_Ws_Pnd_Work_Ts.setValue(Global.getTIMX());
        pnd_Ws_Pnd_Inverse_Lu_Ts.compute(new ComputeParameters(false, pnd_Ws_Pnd_Inverse_Lu_Ts), new DbsDecimal("1000000000000").subtract(pnd_Ws_Pnd_Work_Ts_Num));       //Natural: ASSIGN #WS.#INVERSE-LU-TS := 1000000000000 - #WS.#WORK-TS-NUM
        ldaTxwl2900.getVw_ref_Tab().startDatabaseRead                                                                                                                     //Natural: READ ( 1 ) REF-TAB BY RT-SUPER1 STARTING FROM 'ATX005'
        (
        "READ2",
        new Wc[] { new Wc("RT_SUPER1", ">=", "ATX005", WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") },
        1
        );
        READ2:
        while (condition(ldaTxwl2900.getVw_ref_Tab().readNextRow("READ2")))
        {
            if (condition(ldaTxwl2900.getRef_Tab_Rt_Table_Id().notEquals("TX005")))                                                                                       //Natural: IF REF-TAB.RT-TABLE-ID NE 'TX005'
            {
                if (true) break READ2;                                                                                                                                    //Natural: ESCAPE BOTTOM ( READ2. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTxwl2900.getRef_Tab_Rt_A_I_Ind().notEquals("A")))                                                                                            //Natural: IF REF-TAB.RT-A-I-IND NE 'A'
            {
                if (true) break READ2;                                                                                                                                    //Natural: ESCAPE BOTTOM ( READ2. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Msg.setValue("Update Inactive Rec");                                                                                                               //Natural: ASSIGN #WS.#MSG := 'Update Inactive Rec'
            ldaTxwl2900.getRef_Tab_Rt_A_I_Ind().setValue("I");                                                                                                            //Natural: ASSIGN REF-TAB.RT-A-I-IND := 'I'
            ldaTxwl2900.getRef_Tab_Rt_Short_Key().setValue("Update IA Cutoff Dt");                                                                                        //Natural: ASSIGN REF-TAB.RT-SHORT-KEY := 'Update IA Cutoff Dt'
            ldaTxwl2900.getRef_Tab_Rt_Inverse_Lu_Ts().setValue(pnd_Ws_Pnd_Inverse_Lu_Ts);                                                                                 //Natural: ASSIGN REF-TAB.RT-INVERSE-LU-TS := #WS.#INVERSE-LU-TS
            ldaTxwl2900.getRef_Tab_Rt_Lu_User_Id().setValue(Global.getINIT_USER());                                                                                       //Natural: ASSIGN REF-TAB.RT-LU-USER-ID := *INIT-USER
            ldaTxwl2900.getRef_Tab_Rt_Lu_Ts().setValue(pnd_Ws_Pnd_Curr_Ts);                                                                                               //Natural: ASSIGN REF-TAB.RT-LU-TS := #WS.#CURR-TS
                                                                                                                                                                          //Natural: PERFORM DISPLAY-RECORD
            sub_Display_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaTxwl2900.getVw_ref_Tab().updateDBRow("READ2");                                                                                                             //Natural: UPDATE
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ------------------------------------------
        //*  READ REF-TAB BY RT-SUPER3 = 'TX005'
        //*    IF REF-TAB.RT-TABLE-ID NE 'TX005'
        //*     ESCAPE BOTTOM
        //*   END-IF
        //*   #WS.#MSG                := 'Deleting old Record'
        //*   PERFORM DISPLAY-RECORD
        //*   DELETE
        //*   END TRANSACTION
        //*  END-READ
        //*  ------------------------------------------
        ldaTxwl2900.getVw_ref_Tab().reset();                                                                                                                              //Natural: RESET REF-TAB
        ldaTxwl2900.getRef_Tab_Rt_A_I_Ind().setValue("A");                                                                                                                //Natural: ASSIGN REF-TAB.RT-A-I-IND := 'A'
        ldaTxwl2900.getRef_Tab_Rt_Table_Id().setValue("TX005");                                                                                                           //Natural: ASSIGN REF-TAB.RT-TABLE-ID := 'TX005'
        ldaTxwl2900.getRef_Tab_Rt_Short_Key().setValue("IA Cutoff Date Table");                                                                                           //Natural: ASSIGN REF-TAB.RT-SHORT-KEY := 'IA Cutoff Date Table'
        ldaTxwl2900.getRef_Tab_Rt_Inverse_Lu_Ts().setValue(pnd_Ws_Pnd_Inverse_Lu_Ts);                                                                                     //Natural: ASSIGN REF-TAB.RT-INVERSE-LU-TS := #WS.#INVERSE-LU-TS
        ldaTxwl2900.getRef_Tab_Rt_Inverse_Effective_Ts().setValue(pnd_Ia_Inverse_Eff_Ts);                                                                                 //Natural: ASSIGN REF-TAB.RT-INVERSE-EFFECTIVE-TS := #IA-INVERSE-EFF-TS
        ldaTxwl2900.getRef_Tab_Rt_Effective_Ts().setValue(pnd_Ia_Eff_Ts);                                                                                                 //Natural: ASSIGN REF-TAB.RT-EFFECTIVE-TS := #IA-EFF-TS
        ldaTxwl2900.getRef_Tab_Rt_Create_User_Id().setValue(Global.getINIT_USER());                                                                                       //Natural: ASSIGN REF-TAB.RT-CREATE-USER-ID := *INIT-USER
        ldaTxwl2900.getRef_Tab_Rt_Create_Ts().setValue(pnd_Ws_Pnd_Curr_Ts);                                                                                               //Natural: ASSIGN REF-TAB.RT-CREATE-TS := #WS.#CURR-TS
        ldaTxwl2900.getRef_Tab_Rt_Lu_User_Id().setValue(Global.getINIT_USER());                                                                                           //Natural: ASSIGN REF-TAB.RT-LU-USER-ID := *INIT-USER
        ldaTxwl2900.getRef_Tab_Rt_Lu_Ts().setValue(pnd_Ws_Pnd_Curr_Ts);                                                                                                   //Natural: ASSIGN REF-TAB.RT-LU-TS := #WS.#CURR-TS
        ldaTxwl2900.getRef_Tab_Rt_Ia_Cutoff2_Ts().setValue(pnd_Ia_Eff2_Ts);                                                                                               //Natural: ASSIGN REF-TAB.RT-IA-CUTOFF2-TS := #IA-EFF2-TS
        pnd_Ws_Pnd_Msg.setValue("New Active Rec");                                                                                                                        //Natural: ASSIGN #WS.#MSG := 'New Active Rec'
                                                                                                                                                                          //Natural: PERFORM DISPLAY-RECORD
        sub_Display_Record();
        if (condition(Global.isEscape())) {return;}
        ldaTxwl2900.getVw_ref_Tab().insertDBRow();                                                                                                                        //Natural: STORE REF-TAB
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(0, "END OF PROCESSING");                                                                                                                       //Natural: WRITE 'END OF PROCESSING'
        if (Global.isEscape()) return;
        //* ***********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-LAST-ACTIVE-IA-CUTOFF-DATE
        //* ***********************************************
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-RECORD
    }
    private void sub_Get_Last_Active_Ia_Cutoff_Date() throws Exception                                                                                                    //Natural: GET-LAST-ACTIVE-IA-CUTOFF-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Rec_Found.setValue(false);                                                                                                                                    //Natural: ASSIGN #REC-FOUND := FALSE
        ldaTxwl2900.getVw_ref_Tab().startDatabaseRead                                                                                                                     //Natural: READ ( 1 ) REF-TAB BY RT-SUPER1 STARTING FROM 'ATX005'
        (
        "READ1",
        new Wc[] { new Wc("RT_SUPER1", ">=", "ATX005", WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") },
        1
        );
        READ1:
        while (condition(ldaTxwl2900.getVw_ref_Tab().readNextRow("READ1")))
        {
            if (condition(ldaTxwl2900.getRef_Tab_Rt_Table_Id().notEquals("TX005")))                                                                                       //Natural: IF REF-TAB.RT-TABLE-ID NE 'TX005'
            {
                if (true) break READ1;                                                                                                                                    //Natural: ESCAPE BOTTOM ( READ1. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTxwl2900.getRef_Tab_Rt_A_I_Ind().notEquals("A")))                                                                                            //Natural: IF REF-TAB.RT-A-I-IND NE 'A'
            {
                if (true) break READ1;                                                                                                                                    //Natural: ESCAPE BOTTOM ( READ1. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Rec_Found.setValue(true);                                                                                                                                 //Natural: ASSIGN #REC-FOUND := TRUE
            pnd_Ia_Inverse_Eff_Ts.setValue(ldaTxwl2900.getRef_Tab_Rt_Inverse_Effective_Ts());                                                                             //Natural: ASSIGN #IA-INVERSE-EFF-TS := REF-TAB.RT-INVERSE-EFFECTIVE-TS
            pnd_Ia_Eff_Ts.setValue(ldaTxwl2900.getRef_Tab_Rt_Effective_Ts());                                                                                             //Natural: ASSIGN #IA-EFF-TS := REF-TAB.RT-EFFECTIVE-TS
            pnd_T_Date.setValueEdited(pnd_Ia_Eff_Ts,new ReportEditMask("YYYYMMDDHHIISST"));                                                                               //Natural: MOVE EDITED #IA-EFF-TS ( EM = YYYYMMDDHHIISST ) TO #T-DATE
            pnd_Ia_Eff2_Ts.setValue(ldaTxwl2900.getRef_Tab_Rt_Ia_Cutoff2_Ts());                                                                                           //Natural: ASSIGN #IA-EFF2-TS := REF-TAB.RT-IA-CUTOFF2-TS
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Rec_Found.equals(false)))                                                                                                                       //Natural: IF #REC-FOUND = FALSE
        {
            getReports().write(0, "************************************");                                                                                                //Natural: WRITE '************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "Job Failed - call system immediately");                                                                                                //Natural: WRITE 'Job Failed - call system immediately'
            if (Global.isEscape()) return;
            getReports().write(0, "==========");                                                                                                                          //Natural: WRITE '=========='
            if (Global.isEscape()) return;
            getReports().write(0, "No Active IA Cutoff Date Record");                                                                                                     //Natural: WRITE 'No Active IA Cutoff Date Record'
            if (Global.isEscape()) return;
            getReports().write(0, "************************************");                                                                                                //Natural: WRITE '************************************'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Display_Record() throws Exception                                                                                                                    //Natural: DISPLAY-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        getReports().display(0, "/Table",                                                                                                                                 //Natural: DISPLAY '/Table' REF-TAB.RT-TABLE-ID 'Act/Ind' REF-TAB.RT-A-I-IND 'Eff. TS' REF-TAB.RT-EFFECTIVE-TS ( EM = MM/DD/YYYY�HH:II:SS.T ) / 'IA Cutoff2' REF-TAB.RT-IA-CUTOFF2-TS ( EM = MM/DD/YYYY�HH:II:SS.T ) / 'Create TS' REF-TAB.RT-CREATE-TS ( EM = MM/DD/YYYY�HH:II:SS.T ) / 'LU     TS' REF-TAB.RT-LU-TS ( EM = MM/DD/YYYY�HH:II:SS.T ) 'Inv. Eff TS' REF-TAB.RT-INVERSE-EFFECTIVE-TS / 'Inv. LU  TS' REF-TAB.RT-INVERSE-LU-TS 'Create User' REF-TAB.RT-CREATE-USER-ID / 'LU     User' REF-TAB.RT-LU-USER-ID 'Message' #WS.#MSG
        		ldaTxwl2900.getRef_Tab_Rt_Table_Id(),"Act/Ind",
        		ldaTxwl2900.getRef_Tab_Rt_A_I_Ind(),"Eff. TS",
        		ldaTxwl2900.getRef_Tab_Rt_Effective_Ts(), new ReportEditMask ("MM/DD/YYYY HH:II:SS.T"),NEWLINE,"IA Cutoff2",
        		ldaTxwl2900.getRef_Tab_Rt_Ia_Cutoff2_Ts(), new ReportEditMask ("MM/DD/YYYY HH:II:SS.T"),NEWLINE,"Create TS",
        		ldaTxwl2900.getRef_Tab_Rt_Create_Ts(), new ReportEditMask ("MM/DD/YYYY HH:II:SS.T"),NEWLINE,"LU     TS",
        		ldaTxwl2900.getRef_Tab_Rt_Lu_Ts(), new ReportEditMask ("MM/DD/YYYY HH:II:SS.T"),"Inv. Eff TS",
        		ldaTxwl2900.getRef_Tab_Rt_Inverse_Effective_Ts(),NEWLINE,"Inv. LU  TS",
        		ldaTxwl2900.getRef_Tab_Rt_Inverse_Lu_Ts(),"Create User",
        		ldaTxwl2900.getRef_Tab_Rt_Create_User_Id(),NEWLINE,"LU     User",
        		ldaTxwl2900.getRef_Tab_Rt_Lu_User_Id(),"Message",
        		pnd_Ws_Pnd_Msg);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=80");

        getReports().write(0, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:IIAP"),new TabSetting(30),"TIAA CREF Tax System",new 
            TabSetting(67),"Page  :",getReports().getPageNumberDbs(0), new ReportEditMask ("Z,ZZ9"),NEWLINE,Global.getPROGRAM(),new TabSetting(30),"IA CUTOFF TABLE",new 
            TabSetting(67),"Report:  RPT0",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(0, "/Table",
        		ldaTxwl2900.getRef_Tab_Rt_Table_Id(),"Act/Ind",
        		ldaTxwl2900.getRef_Tab_Rt_A_I_Ind(),"Eff. TS",
        		ldaTxwl2900.getRef_Tab_Rt_Effective_Ts(), new ReportEditMask ("MM/DD/YYYY HH:II:SS.T"),NEWLINE,"IA Cutoff2",
        		ldaTxwl2900.getRef_Tab_Rt_Ia_Cutoff2_Ts(), new ReportEditMask ("MM/DD/YYYY HH:II:SS.T"),NEWLINE,"Create TS",
        		ldaTxwl2900.getRef_Tab_Rt_Create_Ts(), new ReportEditMask ("MM/DD/YYYY HH:II:SS.T"),NEWLINE,"LU     TS",
        		ldaTxwl2900.getRef_Tab_Rt_Lu_Ts(), new ReportEditMask ("MM/DD/YYYY HH:II:SS.T"),"Inv. Eff TS",
        		ldaTxwl2900.getRef_Tab_Rt_Inverse_Effective_Ts(),NEWLINE,"Inv. LU  TS",
        		ldaTxwl2900.getRef_Tab_Rt_Inverse_Lu_Ts(),"Create User",
        		ldaTxwl2900.getRef_Tab_Rt_Create_User_Id(),NEWLINE,"LU     User",
        		ldaTxwl2900.getRef_Tab_Rt_Lu_User_Id(),"Message",
        		pnd_Ws_Pnd_Msg);
    }
}
