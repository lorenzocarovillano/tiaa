/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:30:46 PM
**        * FROM NATURAL PROGRAM : Twrp0640
************************************************************
**        * FILE NAME            : Twrp0640.java
**        * CLASS NAME           : Twrp0640
**        * INSTANCE NAME        : Twrp0640
************************************************************
************************************************************************
* PROGRAM  : TWRP0640
* SYSTEM   : TWR - TWR PAYMENTS SYSTEM
* TITLE    : AUTOMATED TAX MAINTENANCE
* CREATED  : 03/09/99 BY M. SUPONITSKY
* HISTORY  :
*    08/03/1999 - MICHAEL SUPONITSKY - PAYMENT FILE SUPER CHANGE.
*             M. SUPONITSKY 07/01/99 - FIX FOR MULTI RECORD PROCESSING
*                                      (OVER 24 OCCURRENCES).
*    11/17/99   - TOM TRAINOR - ELIMINATE MSGS 7 AND 12-17 ON THE
*                               IVC CHANGE REPORT - PRINT MSG 21 ONLY
*                               ADD "eyecatcher" TO MSG 21
*    12-6-99    - TOM TRAINOR - ADD CHECK FOR VALID DATE IN SUBROUTINES
*     (V1.03)                    PROCESS-DOD1, 2
*
*     1-4-00    - TOM TRAINOR - PER MIKE SUPONITSKY:
*     (V1.04)                 IF INTERFACE MONTH IS 01 (JAN) OR IF IT IS
*                             02 (FEB) & SPECIAL-RUN-INDICATOR IS ON
*                             THEN SUBTRACT ONE FROM INTERFACE YEAR TO
*                             CAUSE PROCESSING OF PREVIOUS YEAR's rcds
*
*     5/15/00   - FELIX ORTIZ
*                            PROPAGATE 1001 AND 1078 INDICATORS FOR IVC
*                            ADJUSTMENTS.  ADD 1001 AND 1078 MIXED IND
*                            ERROR MESSAGE.  ALSO REJECT WHEN IVC IS
*                            GREATER THAN GROSS.
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0640 extends BLNatBase
{
    // Data Areas
    private PdaTwratbl1 pdaTwratbl1;
    private LdaTwrl6400 ldaTwrl6400;
    private LdaTwrl9415 ldaTwrl9415;
    private LdaTwrl9420 ldaTwrl9420;
    private LdaTwrl9425 ldaTwrl9425;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Vers_Id;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;
    private DbsField pnd_Input_Origin;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Date_N;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Pnd_Date_X;

    private DbsGroup pnd_Ws__R_Field_2;
    private DbsField pnd_Ws_Pnd_Yyyy_N;
    private DbsField pnd_Ws_Pnd_Mm_N;
    private DbsField pnd_Ws_Pnd_Dd_N;
    private DbsField pnd_Ws_Pnd_Pymnt_Dte;
    private DbsField pnd_Ws_Pnd_F_P_Date;
    private DbsField pnd_Ws_Pnd_Today;
    private DbsField pnd_Ws_Pnd_Cntrl_Dte;
    private DbsField pnd_Ws_Pnd_Date_D;
    private DbsField pnd_Ws_Pnd_Dob1_Cv;
    private DbsField pnd_Ws_Pnd_Dob2_Cv;
    private DbsField pnd_Ws_Pnd_Dod1_Cv;
    private DbsField pnd_Ws_Pnd_Dod2_Cv;
    private DbsField pnd_Ws_Pnd_Ivc_Cv;
    private DbsField pnd_Ws_Pnd_Known_Ivc;
    private DbsField pnd_Ws_Pnd_Tic_Update_Ind;
    private DbsField pnd_Ws_Pnd_Ivc_Update_Ind;
    private DbsField pnd_Ws_Pnd_Accept_Ind;
    private DbsField pnd_Ws_Pnd_Accept_First;
    private DbsField pnd_Ws_Pnd_1001_Mix;
    private DbsField pnd_Ws_Pnd_1078_Mix;
    private DbsField pnd_Ws_Pnd_1001_Ind;
    private DbsField pnd_Ws_Pnd_1078_Ind;
    private DbsField pnd_Ws_Pnd_Ivc_Protect_Set_Ind;
    private DbsField pnd_Ws_Pnd_Special_Run_Ind;
    private DbsField pnd_Ws_Pnd_Rec_Accepted;
    private DbsField pnd_Ws_Pnd_Tran_Cnt;
    private DbsField pnd_Ws_Pnd_Invalid_Tran_Cnt;
    private DbsField pnd_Ws_Pnd_Res_Tran_Cnt;
    private DbsField pnd_Ws_Pnd_Ssn_Tran_Cnt;
    private DbsField pnd_Ws_Pnd_Cit_Tran_Cnt;
    private DbsField pnd_Ws_Pnd_Dob1_Tran_Cnt;
    private DbsField pnd_Ws_Pnd_Dob2_Tran_Cnt;
    private DbsField pnd_Ws_Pnd_Dod1_Tran_Cnt;
    private DbsField pnd_Ws_Pnd_Dod2_Tran_Cnt;
    private DbsField pnd_Ws_Pnd_Ivc_Tran_Cnt;
    private DbsField pnd_Ws_Pnd_Ric_Tran_Cnt;
    private DbsField pnd_Ws_Pnd_Tic_Tran_Cnt;
    private DbsField pnd_Ws_Pnd_Ivc_Tran_Rejected;
    private DbsField pnd_Ws_Pnd_Tot_Tran_Cnt;
    private DbsField pnd_Ws_Pnd_Rtb_Ivc_Reject;
    private DbsField pnd_Ws_Pnd_Tic_Ivc_Reject;
    private DbsField pnd_Ws_Pnd_Tot_Tran_Reject;
    private DbsField pnd_Ws_Pnd_Tot_Tran_Accept;
    private DbsField pnd_Ws_Pnd_Ivc_Tran_Accepted;
    private DbsField pnd_Ws_Pnd_Rtb_Ivc_Accept;
    private DbsField pnd_Ws_Pnd_Tic_Ivc_Accept;
    private DbsField pnd_Ws_Pnd_Et_Limit;
    private DbsField pnd_Ws_Pnd_Et_Cnt;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_Top;
    private DbsField pnd_Ws_Pnd_Prev_Cntrct_Nbr;
    private DbsField pnd_Ws_Pnd_Prev_Payee_Cde;
    private DbsField pnd_Ws_Pnd_Prev_Maint_Cde;
    private DbsField pnd_Ws_Pnd_Prev_Tax_Id_Nbr;
    private DbsField pnd_Ws_Pnd_Prev_Distribution_Cde;
    private DbsField pnd_Ws_Pnd_Prev_Tax_Citizenship;
    private DbsField pnd_Ws_Pnd_Prev_Residency_Type;
    private DbsField pnd_Ws_Pnd_Prev_Residency_Code;
    private DbsField pnd_Ws_Pnd_Prev_Locality_Cde;
    private DbsField pnd_Ws_Pnd_Tot_Ivc;
    private DbsField pnd_Ws_Pnd_Calc_Ivc;
    private DbsField pnd_Ws_Pnd_Adj_Ivc;
    private DbsField pnd_Ws_Pnd_Calc_Gross;
    private DbsField pnd_Ws_Pnd_Ivc_Ytd_Amt_R;
    private DbsField pnd_Ws_Pnd_Ivc_Tot_Amt_R;
    private DbsField pnd_Ws_Pnd_Tot_Ytd_Amt_R;
    private DbsField pnd_Ws_Pnd_Rtb_Ytd_Amt_R;
    private DbsField pnd_Ws_Pnd_Tic_Ytd_Amt_R;
    private DbsField pnd_Ws_Pnd_Tot_Tot_Amt_R;
    private DbsField pnd_Ws_Pnd_Rtb_Tot_Amt_R;
    private DbsField pnd_Ws_Pnd_Tic_Tot_Amt_R;
    private DbsField pnd_Ws_Pnd_Ivc_Ytd_Amt;
    private DbsField pnd_Ws_Pnd_Ivc_Tot_Amt;
    private DbsField pnd_Ws_Pnd_Rtb_Ytd_Amt;
    private DbsField pnd_Ws_Pnd_Rtb_Tot_Amt;
    private DbsField pnd_Ws_Pnd_Tic_Ytd_Amt;
    private DbsField pnd_Ws_Pnd_Tic_Tot_Amt;
    private DbsField pnd_Ws_Pnd_Tot_Ytd_Amt;
    private DbsField pnd_Ws_Pnd_Tot_Tot_Amt;
    private DbsField pnd_Ws_Pnd_Rpt6_Ytd_Ivc_Tot;
    private DbsField pnd_Ws_Pnd_Rpt6_Tot_Ivc_Tot;
    private DbsField pnd_Ws_Pnd_Rpt8_Ytd_Ivc_Tot;
    private DbsField pnd_Ws_Pnd_Rpt8_Tot_Ivc_Tot;
    private DbsField pnd_Ws_Pnd_Header;
    private DbsField pnd_Ws_Pnd_Eyecatcher;
    private DbsField pnd_Ws_Pnd_Msg;
    private DbsField pnd_Ws_Pnd_Paymt_Category;
    private DbsField pnd_Con_Pay_Sd;

    private DbsGroup pnd_Con_Pay_Sd__R_Field_3;
    private DbsField pnd_Con_Pay_Sd_Pnd_Tax_Year;
    private DbsField pnd_Con_Pay_Sd_Pnd_Contract_Nbr;
    private DbsField pnd_Con_Pay_Sd_Pnd_Payee_Cde;

    private DbsGroup pnd_Form_Sd_Detail;
    private DbsField pnd_Form_Sd_Detail_Pnd_F_Tax_Year;
    private DbsField pnd_Form_Sd_Detail_Pnd_F_Tax_Id_Nbr;
    private DbsField pnd_Form_Sd_Detail_Pnd_F_Contract_Nbr;
    private DbsField pnd_Form_Sd_Detail_Pnd_F_Payee_Cde;
    private DbsField pnd_Form_Sd_Detail_Pnd_F_Company_Cde;
    private DbsField pnd_Form_Sd_Detail_Pnd_F_Tax_Citizenship;
    private DbsField pnd_Form_Sd_Detail_Pnd_F_Paymt_Category;
    private DbsField pnd_Form_Sd_Detail_Pnd_F_Distribution_Cde;
    private DbsField pnd_Form_Sd_Detail_Pnd_F_Residency_Type;
    private DbsField pnd_Form_Sd_Detail_Pnd_F_Residency_Code;
    private DbsField pnd_Form_Sd_Detail_Pnd_F_Locality_Cde;
    private DbsField pnd_Form_Sd_Detail_Pnd_F_Contract_Seq_No;

    private DbsGroup pnd_Form_Sd_Detail__R_Field_4;
    private DbsField pnd_Form_Sd_Detail_Pnd_Form_Sd_Start;
    private DbsField pnd_Form_Sd_End;

    private DbsGroup pnd_Work_Rec;
    private DbsField pnd_Work_Rec_Pnd_Tax_Year;
    private DbsField pnd_Work_Rec_Pnd_Case_Num;

    private DbsGroup pnd_Work_Rec_Pnd_Work_Det;
    private DbsField pnd_Work_Rec_Pnd_Record_Type;
    private DbsField pnd_Work_Rec_Pnd_Msg_Idx;
    private DbsField pnd_Work_Rec_Pnd_Isn;
    private DbsField pnd_Work_Rec_Pnd_Multi_Case;
    private DbsField pnd_Work_Rec_Pnd_Multi_Residency;
    private DbsField pnd_Work_Rec_Pnd_Multi_Record;

    private DbsGroup pnd_Save_Rec;
    private DbsField pnd_Save_Rec_Pnd_Tax_Year;
    private DbsField pnd_Save_Rec_Pnd_Case_Num;
    private DbsField pnd_Save_Rec_Pnd_Record_Type;
    private DbsField pnd_Save_Rec_Pnd_Multi_Case;
    private DbsField pnd_Save_Rec_Pnd_Multi_Residency;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwratbl1 = new PdaTwratbl1(localVariables);
        ldaTwrl6400 = new LdaTwrl6400();
        registerRecord(ldaTwrl6400);
        ldaTwrl9415 = new LdaTwrl9415();
        registerRecord(ldaTwrl9415);
        registerRecord(ldaTwrl9415.getVw_pay());
        ldaTwrl9420 = new LdaTwrl9420();
        registerRecord(ldaTwrl9420);
        registerRecord(ldaTwrl9420.getVw_payr());
        ldaTwrl9425 = new LdaTwrl9425();
        registerRecord(ldaTwrl9425);
        registerRecord(ldaTwrl9425.getVw_payu());

        // Local Variables
        pnd_Vers_Id = localVariables.newFieldInRecord("pnd_Vers_Id", "#VERS_ID", FieldType.STRING, 6);

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);
        pnd_Input_Origin = localVariables.newFieldInRecord("pnd_Input_Origin", "#INPUT-ORIGIN", FieldType.STRING, 2);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Date_N = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Date_N);
        pnd_Ws_Pnd_Date_X = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Date_X", "#DATE-X", FieldType.STRING, 8);

        pnd_Ws__R_Field_2 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_2", "REDEFINE", pnd_Ws_Pnd_Date_N);
        pnd_Ws_Pnd_Yyyy_N = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_Yyyy_N", "#YYYY-N", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Mm_N = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_Mm_N", "#MM-N", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_Dd_N = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_Dd_N", "#DD-N", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_Pymnt_Dte = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Dte", "#PYMNT-DTE", FieldType.STRING, 8);
        pnd_Ws_Pnd_F_P_Date = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_F_P_Date", "#F-P-DATE", FieldType.STRING, 8);
        pnd_Ws_Pnd_Today = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Today", "#TODAY", FieldType.TIME);
        pnd_Ws_Pnd_Cntrl_Dte = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cntrl_Dte", "#CNTRL-DTE", FieldType.DATE);
        pnd_Ws_Pnd_Date_D = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Date_D", "#DATE-D", FieldType.DATE);
        pnd_Ws_Pnd_Dob1_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Dob1_Cv", "#DOB1-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Dob2_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Dob2_Cv", "#DOB2-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Dod1_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Dod1_Cv", "#DOD1-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Dod2_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Dod2_Cv", "#DOD2-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Ivc_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ivc_Cv", "#IVC-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Known_Ivc = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Known_Ivc", "#KNOWN-IVC", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Tic_Update_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tic_Update_Ind", "#TIC-UPDATE-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Ivc_Update_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ivc_Update_Ind", "#IVC-UPDATE-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Accept_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Accept_Ind", "#ACCEPT-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Accept_First = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Accept_First", "#ACCEPT-FIRST", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_1001_Mix = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_1001_Mix", "#1001-MIX", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_1078_Mix = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_1078_Mix", "#1078-MIX", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_1001_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_1001_Ind", "#1001-IND", FieldType.STRING, 1);
        pnd_Ws_Pnd_1078_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_1078_Ind", "#1078-IND", FieldType.STRING, 1);
        pnd_Ws_Pnd_Ivc_Protect_Set_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ivc_Protect_Set_Ind", "#IVC-PROTECT-SET-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Special_Run_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Special_Run_Ind", "#SPECIAL-RUN-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Rec_Accepted = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Accepted", "#REC-ACCEPTED", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Tran_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tran_Cnt", "#TRAN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Invalid_Tran_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Invalid_Tran_Cnt", "#INVALID-TRAN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Res_Tran_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Res_Tran_Cnt", "#RES-TRAN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Ssn_Tran_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ssn_Tran_Cnt", "#SSN-TRAN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Cit_Tran_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cit_Tran_Cnt", "#CIT-TRAN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Dob1_Tran_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Dob1_Tran_Cnt", "#DOB1-TRAN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Dob2_Tran_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Dob2_Tran_Cnt", "#DOB2-TRAN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Dod1_Tran_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Dod1_Tran_Cnt", "#DOD1-TRAN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Dod2_Tran_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Dod2_Tran_Cnt", "#DOD2-TRAN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Ivc_Tran_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ivc_Tran_Cnt", "#IVC-TRAN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Ric_Tran_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ric_Tran_Cnt", "#RIC-TRAN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Tic_Tran_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tic_Tran_Cnt", "#TIC-TRAN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Ivc_Tran_Rejected = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ivc_Tran_Rejected", "#IVC-TRAN-REJECTED", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Tot_Tran_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Tran_Cnt", "#TOT-TRAN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Rtb_Ivc_Reject = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rtb_Ivc_Reject", "#RTB-IVC-REJECT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Tic_Ivc_Reject = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tic_Ivc_Reject", "#TIC-IVC-REJECT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Tot_Tran_Reject = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Tran_Reject", "#TOT-TRAN-REJECT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Tot_Tran_Accept = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Tran_Accept", "#TOT-TRAN-ACCEPT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Ivc_Tran_Accepted = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ivc_Tran_Accepted", "#IVC-TRAN-ACCEPTED", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Rtb_Ivc_Accept = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rtb_Ivc_Accept", "#RTB-IVC-ACCEPT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Tic_Ivc_Accept = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tic_Ivc_Accept", "#TIC-IVC-ACCEPT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Et_Limit = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Et_Limit", "#ET-LIMIT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Et_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Et_Cnt", "#ET-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Top = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Top", "#TOP", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Prev_Cntrct_Nbr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Cntrct_Nbr", "#PREV-CNTRCT-NBR", FieldType.STRING, 8);
        pnd_Ws_Pnd_Prev_Payee_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Payee_Cde", "#PREV-PAYEE-CDE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Prev_Maint_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Maint_Cde", "#PREV-MAINT-CDE", FieldType.STRING, 3);
        pnd_Ws_Pnd_Prev_Tax_Id_Nbr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Tax_Id_Nbr", "#PREV-TAX-ID-NBR", FieldType.STRING, 10);
        pnd_Ws_Pnd_Prev_Distribution_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Distribution_Cde", "#PREV-DISTRIBUTION-CDE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Prev_Tax_Citizenship = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Tax_Citizenship", "#PREV-TAX-CITIZENSHIP", FieldType.STRING, 1);
        pnd_Ws_Pnd_Prev_Residency_Type = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Residency_Type", "#PREV-RESIDENCY-TYPE", FieldType.STRING, 1);
        pnd_Ws_Pnd_Prev_Residency_Code = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Residency_Code", "#PREV-RESIDENCY-CODE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Prev_Locality_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Locality_Cde", "#PREV-LOCALITY-CDE", FieldType.STRING, 3);
        pnd_Ws_Pnd_Tot_Ivc = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Ivc", "#TOT-IVC", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Calc_Ivc = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Calc_Ivc", "#CALC-IVC", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Adj_Ivc = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Adj_Ivc", "#ADJ-IVC", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Calc_Gross = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Calc_Gross", "#CALC-GROSS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Ivc_Ytd_Amt_R = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ivc_Ytd_Amt_R", "#IVC-YTD-AMT-R", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Ivc_Tot_Amt_R = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ivc_Tot_Amt_R", "#IVC-TOT-AMT-R", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Tot_Ytd_Amt_R = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Ytd_Amt_R", "#TOT-YTD-AMT-R", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Rtb_Ytd_Amt_R = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rtb_Ytd_Amt_R", "#RTB-YTD-AMT-R", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Tic_Ytd_Amt_R = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tic_Ytd_Amt_R", "#TIC-YTD-AMT-R", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Tot_Tot_Amt_R = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Tot_Amt_R", "#TOT-TOT-AMT-R", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Rtb_Tot_Amt_R = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rtb_Tot_Amt_R", "#RTB-TOT-AMT-R", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Tic_Tot_Amt_R = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tic_Tot_Amt_R", "#TIC-TOT-AMT-R", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Ivc_Ytd_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ivc_Ytd_Amt", "#IVC-YTD-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Ivc_Tot_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ivc_Tot_Amt", "#IVC-TOT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Rtb_Ytd_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rtb_Ytd_Amt", "#RTB-YTD-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Rtb_Tot_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rtb_Tot_Amt", "#RTB-TOT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Tic_Ytd_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tic_Ytd_Amt", "#TIC-YTD-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Tic_Tot_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tic_Tot_Amt", "#TIC-TOT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Tot_Ytd_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Ytd_Amt", "#TOT-YTD-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Tot_Tot_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Tot_Amt", "#TOT-TOT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Rpt6_Ytd_Ivc_Tot = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rpt6_Ytd_Ivc_Tot", "#RPT6-YTD-IVC-TOT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Rpt6_Tot_Ivc_Tot = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rpt6_Tot_Ivc_Tot", "#RPT6-TOT-IVC-TOT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Rpt8_Ytd_Ivc_Tot = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rpt8_Ytd_Ivc_Tot", "#RPT8-YTD-IVC-TOT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Rpt8_Tot_Ivc_Tot = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rpt8_Tot_Ivc_Tot", "#RPT8-TOT-IVC-TOT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Header = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Header", "#HEADER", FieldType.STRING, 32);
        pnd_Ws_Pnd_Eyecatcher = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Eyecatcher", "#EYECATCHER", FieldType.STRING, 3);
        pnd_Ws_Pnd_Msg = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Msg", "#MSG", FieldType.STRING, 33, new DbsArrayController(1, 27));
        pnd_Ws_Pnd_Paymt_Category = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Paymt_Category", "#PAYMT-CATEGORY", FieldType.STRING, 1);
        pnd_Con_Pay_Sd = localVariables.newFieldInRecord("pnd_Con_Pay_Sd", "#CON-PAY-SD", FieldType.STRING, 14);

        pnd_Con_Pay_Sd__R_Field_3 = localVariables.newGroupInRecord("pnd_Con_Pay_Sd__R_Field_3", "REDEFINE", pnd_Con_Pay_Sd);
        pnd_Con_Pay_Sd_Pnd_Tax_Year = pnd_Con_Pay_Sd__R_Field_3.newFieldInGroup("pnd_Con_Pay_Sd_Pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);
        pnd_Con_Pay_Sd_Pnd_Contract_Nbr = pnd_Con_Pay_Sd__R_Field_3.newFieldInGroup("pnd_Con_Pay_Sd_Pnd_Contract_Nbr", "#CONTRACT-NBR", FieldType.STRING, 
            8);
        pnd_Con_Pay_Sd_Pnd_Payee_Cde = pnd_Con_Pay_Sd__R_Field_3.newFieldInGroup("pnd_Con_Pay_Sd_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.STRING, 2);

        pnd_Form_Sd_Detail = localVariables.newGroupInRecord("pnd_Form_Sd_Detail", "#FORM-SD-DETAIL");
        pnd_Form_Sd_Detail_Pnd_F_Tax_Year = pnd_Form_Sd_Detail.newFieldInGroup("pnd_Form_Sd_Detail_Pnd_F_Tax_Year", "#F-TAX-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Form_Sd_Detail_Pnd_F_Tax_Id_Nbr = pnd_Form_Sd_Detail.newFieldInGroup("pnd_Form_Sd_Detail_Pnd_F_Tax_Id_Nbr", "#F-TAX-ID-NBR", FieldType.STRING, 
            10);
        pnd_Form_Sd_Detail_Pnd_F_Contract_Nbr = pnd_Form_Sd_Detail.newFieldInGroup("pnd_Form_Sd_Detail_Pnd_F_Contract_Nbr", "#F-CONTRACT-NBR", FieldType.STRING, 
            8);
        pnd_Form_Sd_Detail_Pnd_F_Payee_Cde = pnd_Form_Sd_Detail.newFieldInGroup("pnd_Form_Sd_Detail_Pnd_F_Payee_Cde", "#F-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Form_Sd_Detail_Pnd_F_Company_Cde = pnd_Form_Sd_Detail.newFieldInGroup("pnd_Form_Sd_Detail_Pnd_F_Company_Cde", "#F-COMPANY-CDE", FieldType.STRING, 
            1);
        pnd_Form_Sd_Detail_Pnd_F_Tax_Citizenship = pnd_Form_Sd_Detail.newFieldInGroup("pnd_Form_Sd_Detail_Pnd_F_Tax_Citizenship", "#F-TAX-CITIZENSHIP", 
            FieldType.STRING, 1);
        pnd_Form_Sd_Detail_Pnd_F_Paymt_Category = pnd_Form_Sd_Detail.newFieldInGroup("pnd_Form_Sd_Detail_Pnd_F_Paymt_Category", "#F-PAYMT-CATEGORY", FieldType.STRING, 
            1);
        pnd_Form_Sd_Detail_Pnd_F_Distribution_Cde = pnd_Form_Sd_Detail.newFieldInGroup("pnd_Form_Sd_Detail_Pnd_F_Distribution_Cde", "#F-DISTRIBUTION-CDE", 
            FieldType.STRING, 2);
        pnd_Form_Sd_Detail_Pnd_F_Residency_Type = pnd_Form_Sd_Detail.newFieldInGroup("pnd_Form_Sd_Detail_Pnd_F_Residency_Type", "#F-RESIDENCY-TYPE", FieldType.STRING, 
            1);
        pnd_Form_Sd_Detail_Pnd_F_Residency_Code = pnd_Form_Sd_Detail.newFieldInGroup("pnd_Form_Sd_Detail_Pnd_F_Residency_Code", "#F-RESIDENCY-CODE", FieldType.STRING, 
            2);
        pnd_Form_Sd_Detail_Pnd_F_Locality_Cde = pnd_Form_Sd_Detail.newFieldInGroup("pnd_Form_Sd_Detail_Pnd_F_Locality_Cde", "#F-LOCALITY-CDE", FieldType.STRING, 
            3);
        pnd_Form_Sd_Detail_Pnd_F_Contract_Seq_No = pnd_Form_Sd_Detail.newFieldInGroup("pnd_Form_Sd_Detail_Pnd_F_Contract_Seq_No", "#F-CONTRACT-SEQ-NO", 
            FieldType.NUMERIC, 2);

        pnd_Form_Sd_Detail__R_Field_4 = localVariables.newGroupInRecord("pnd_Form_Sd_Detail__R_Field_4", "REDEFINE", pnd_Form_Sd_Detail);
        pnd_Form_Sd_Detail_Pnd_Form_Sd_Start = pnd_Form_Sd_Detail__R_Field_4.newFieldInGroup("pnd_Form_Sd_Detail_Pnd_Form_Sd_Start", "#FORM-SD-START", 
            FieldType.STRING, 37);
        pnd_Form_Sd_End = localVariables.newFieldInRecord("pnd_Form_Sd_End", "#FORM-SD-END", FieldType.STRING, 37);

        pnd_Work_Rec = localVariables.newGroupInRecord("pnd_Work_Rec", "#WORK-REC");
        pnd_Work_Rec_Pnd_Tax_Year = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);
        pnd_Work_Rec_Pnd_Case_Num = pnd_Work_Rec.newFieldInGroup("pnd_Work_Rec_Pnd_Case_Num", "#CASE-NUM", FieldType.PACKED_DECIMAL, 5);

        pnd_Work_Rec_Pnd_Work_Det = pnd_Work_Rec.newGroupInGroup("pnd_Work_Rec_Pnd_Work_Det", "#WORK-DET");
        pnd_Work_Rec_Pnd_Record_Type = pnd_Work_Rec_Pnd_Work_Det.newFieldInGroup("pnd_Work_Rec_Pnd_Record_Type", "#RECORD-TYPE", FieldType.STRING, 1);
        pnd_Work_Rec_Pnd_Msg_Idx = pnd_Work_Rec_Pnd_Work_Det.newFieldInGroup("pnd_Work_Rec_Pnd_Msg_Idx", "#MSG-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Work_Rec_Pnd_Isn = pnd_Work_Rec_Pnd_Work_Det.newFieldInGroup("pnd_Work_Rec_Pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_Work_Rec_Pnd_Multi_Case = pnd_Work_Rec_Pnd_Work_Det.newFieldInGroup("pnd_Work_Rec_Pnd_Multi_Case", "#MULTI-CASE", FieldType.BOOLEAN, 1);
        pnd_Work_Rec_Pnd_Multi_Residency = pnd_Work_Rec_Pnd_Work_Det.newFieldInGroup("pnd_Work_Rec_Pnd_Multi_Residency", "#MULTI-RESIDENCY", FieldType.BOOLEAN, 
            1);
        pnd_Work_Rec_Pnd_Multi_Record = pnd_Work_Rec_Pnd_Work_Det.newFieldInGroup("pnd_Work_Rec_Pnd_Multi_Record", "#MULTI-RECORD", FieldType.BOOLEAN, 
            1);

        pnd_Save_Rec = localVariables.newGroupInRecord("pnd_Save_Rec", "#SAVE-REC");
        pnd_Save_Rec_Pnd_Tax_Year = pnd_Save_Rec.newFieldInGroup("pnd_Save_Rec_Pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);
        pnd_Save_Rec_Pnd_Case_Num = pnd_Save_Rec.newFieldInGroup("pnd_Save_Rec_Pnd_Case_Num", "#CASE-NUM", FieldType.PACKED_DECIMAL, 5);
        pnd_Save_Rec_Pnd_Record_Type = pnd_Save_Rec.newFieldInGroup("pnd_Save_Rec_Pnd_Record_Type", "#RECORD-TYPE", FieldType.STRING, 1);
        pnd_Save_Rec_Pnd_Multi_Case = pnd_Save_Rec.newFieldInGroup("pnd_Save_Rec_Pnd_Multi_Case", "#MULTI-CASE", FieldType.BOOLEAN, 1);
        pnd_Save_Rec_Pnd_Multi_Residency = pnd_Save_Rec.newFieldInGroup("pnd_Save_Rec_Pnd_Multi_Residency", "#MULTI-RESIDENCY", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl6400.initializeValues();
        ldaTwrl9415.initializeValues();
        ldaTwrl9420.initializeValues();
        ldaTwrl9425.initializeValues();

        localVariables.reset();
        pnd_Vers_Id.setInitialValue("V1.04P");
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
        pnd_Ws_Pnd_Known_Ivc.setInitialValue(true);
        pnd_Ws_Pnd_Ivc_Protect_Set_Ind.setInitialValue(false);
        pnd_Ws_Pnd_Special_Run_Ind.setInitialValue(false);
        pnd_Ws_Pnd_Et_Limit.setInitialValue(100);
        pnd_Ws_Pnd_Header.setInitialValue("Automated ?? Tax Maintenance for");
        pnd_Ws_Pnd_Eyecatcher.setInitialValue("<--");
        pnd_Ws_Pnd_Msg.getValue(0 + 1).setInitialValue("?");
        pnd_Ws_Pnd_Msg.getValue(1 + 1).setInitialValue("No Contract / Payee On DataBase  ");
        pnd_Ws_Pnd_Msg.getValue(2 + 1).setInitialValue("Invalid IVC Code                 ");
        pnd_Ws_Pnd_Msg.getValue(3 + 1).setInitialValue("IVC Not Updtd - Multi Transaction");
        pnd_Ws_Pnd_Msg.getValue(4 + 1).setInitialValue("Payment To Update Not Found      ");
        pnd_Ws_Pnd_Msg.getValue(5 + 1).setInitialValue("Form Federal Gross Amt Is Zero   ");
        pnd_Ws_Pnd_Msg.getValue(6 + 1).setInitialValue("Case Not Updated                 ");
        pnd_Ws_Pnd_Msg.getValue(7 + 1).setInitialValue("Total Contract Ivc Updated       ");
        pnd_Ws_Pnd_Msg.getValue(8 + 1).setInitialValue("Tran & 'TWR' Same IVC - No ADJ.  ");
        pnd_Ws_Pnd_Msg.getValue(9 + 1).setInitialValue("More Than One Case On 'TWR'      ");
        pnd_Ws_Pnd_Msg.getValue(10 + 1).setInitialValue("Multi Residency Case             ");
        pnd_Ws_Pnd_Msg.getValue(11 + 1).setInitialValue("Total Contract Ivc NOT Updated   ");
        pnd_Ws_Pnd_Msg.getValue(12 + 1).setInitialValue("IVC Updated From 'U' To 'K' Zero ");
        pnd_Ws_Pnd_Msg.getValue(13 + 1).setInitialValue("IVC Updated From 'U' To 'K' $    ");
        pnd_Ws_Pnd_Msg.getValue(14 + 1).setInitialValue("IVC Updated From 'K' 0 To 'K' $  ");
        pnd_Ws_Pnd_Msg.getValue(15 + 1).setInitialValue("IVC Updated From 'K' $ To 'K' 0  ");
        pnd_Ws_Pnd_Msg.getValue(16 + 1).setInitialValue("IVC Updated From 'K' $ To 'K' $  ");
        pnd_Ws_Pnd_Msg.getValue(17 + 1).setInitialValue("IVC Updated From 'K' $ To 'K' $  ");
        pnd_Ws_Pnd_Msg.getValue(18 + 1).setInitialValue("IVC From 'U' to 'K' $. Made 'K' 0");
        pnd_Ws_Pnd_Msg.getValue(19 + 1).setInitialValue("IVC From 'K' 0 to 'K' $.         ");
        pnd_Ws_Pnd_Msg.getValue(20 + 1).setInitialValue("IVC From 'K' $ to 'K' $.         ");
        pnd_Ws_Pnd_Msg.getValue(21 + 1).setInitialValue("=======>  Case Updated  <======= ");
        pnd_Ws_Pnd_Msg.getValue(22 + 1).setInitialValue("Case has 24 or more payments");
        pnd_Ws_Pnd_Msg.getValue(23 + 1).setInitialValue("IVC 'protect' set for prev pmt(s)");
        pnd_Ws_Pnd_Msg.getValue(24 + 1).setInitialValue("IVC not updated - Mixed 1001 ind ");
        pnd_Ws_Pnd_Msg.getValue(25 + 1).setInitialValue("IVC not updated - Mixed 1078 ind ");
        pnd_Ws_Pnd_Msg.getValue(26 + 1).setInitialValue("IVC greater than gross amt       ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Twrp0640() throws Exception
    {
        super("Twrp0640");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 02 ) PS = 58 LS = 80 ZP = ON;//Natural: FORMAT ( 03 ) PS = 58 LS = 80 ZP = ON;//Natural: FORMAT ( 04 ) PS = 58 LS = 80 ZP = ON;//Natural: FORMAT ( 05 ) PS = 58 LS = 80 ZP = ON;//Natural: FORMAT ( 06 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 07 ) PS = 58 LS = 80 ZP = ON;//Natural: FORMAT ( 08 ) PS = 58 LS = 133 ZP = ON
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 57T 'TWR Payment System' 120T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T #HEADER #CNTRL-DTE ( EM = MM/YYYY ) 120T 'Report: RPT1' / 13T #VERS_ID 45T 'Control and Invalid Transaction Type Reports' //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 02 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 34T 'TWR Payment System' 68T 'Page:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 24T #HEADER #CNTRL-DTE ( EM = MM/YYYY ) 68T 'Report: RPT2' / 13T #VERS_ID 34T 'Citizenship Changes' //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 03 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 34T 'TWR Payment System' 68T 'Page:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 24T #HEADER #CNTRL-DTE ( EM = MM/YYYY ) 68T 'Report: RPT3' / 13T #VERS_ID 33T 'Date of Birth Changes' //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 04 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 34T 'TWR Payment System' 68T 'Page:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 24T #HEADER #CNTRL-DTE ( EM = MM/YYYY ) 68T 'Report: RPT4' / 13T #VERS_ID 33T 'Date of Death Changes' //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 05 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 34T 'TWR Payment System' 68T 'Page:' *PAGE-NUMBER ( 05 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 24T #HEADER #CNTRL-DTE ( EM = MM/YYYY ) 68T 'Report: RPT5' / 13T #VERS_ID 34T 'Tax Id(SSN) Changes' //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 06 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 57T 'TWR Payment System' 120T 'Page:' *PAGE-NUMBER ( 06 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T #HEADER #CNTRL-DTE ( EM = MM/YYYY ) 120T 'Report: RPT6' / 13T #VERS_ID 52T 'Transaction Rejection Report' //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 07 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 34T 'TWR Payment System' 68T 'Page:' *PAGE-NUMBER ( 07 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 24T #HEADER #CNTRL-DTE ( EM = MM/YYYY ) 68T 'Report: RPT7' / 13T #VERS_ID 35T 'Residency Changes' //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 08 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 57T 'TWR Payment System' 120T 'Page:' *PAGE-NUMBER ( 08 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T #HEADER #CNTRL-DTE ( EM = MM/YYYY ) 120T 'Report: RPT8' / 13T #VERS_ID 45T 'IVC, "RTB IVC", and "Total IVC" Change Report' //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT-PARM
        sub_Process_Input_Parm();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-HEADER
        sub_Process_Header();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Today.setValue(Global.getTIMX());                                                                                                                      //Natural: ASSIGN #TODAY := *TIMX
        pdaTwratbl1.getTwratbl1_Pnd_Function().setValue("1");                                                                                                             //Natural: ASSIGN TWRATBL1.#FUNCTION := '1'
        pdaTwratbl1.getTwratbl1_Pnd_Abend_Ind().setValue(true);                                                                                                           //Natural: ASSIGN TWRATBL1.#ABEND-IND := TRUE
        pdaTwratbl1.getTwratbl1_Pnd_Display_Ind().setValue(false);                                                                                                        //Natural: ASSIGN TWRATBL1.#DISPLAY-IND := FALSE
        pdaTwratbl1.getTwratbl1_Pnd_Ref_Num().setValue("H'FF'");                                                                                                          //Natural: ASSIGN TWRATBL1.#REF-NUM := H'FF'
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 RECORD #NPD-TRAN
        while (condition(getWorkFiles().read(1, ldaTwrl6400.getPnd_Npd_Tran())))
        {
            pnd_Ws_Pnd_Tran_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #TRAN-CNT
            pnd_Work_Rec_Pnd_Msg_Idx.reset();                                                                                                                             //Natural: RESET #WORK-REC.#MSG-IDX
            short decideConditionsMet755 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #I-MAINT-CDE;//Natural: VALUE 'IVC'
            if (condition((ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("IVC"))))
            {
                decideConditionsMet755++;
                pnd_Ws_Pnd_Ivc_Tran_Cnt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #IVC-TRAN-CNT
                if (condition(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Per_Ivc_Cde().notEquals("2")))                                                                            //Natural: IF #I-PER-IVC-CDE NE '2'
                {
                    pnd_Work_Rec_Pnd_Msg_Idx.setValue(2);                                                                                                                 //Natural: ASSIGN #MSG-IDX := 2
                    //*  PERIODIC PAYMENT
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_Paymt_Category.setValue("1");                                                                                                                  //Natural: ASSIGN #PAYMT-CATEGORY := '1'
                                                                                                                                                                          //Natural: PERFORM TAX-MAINTENANCE
                sub_Tax_Maintenance();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'RIC'
            else if (condition((ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("RIC"))))
            {
                decideConditionsMet755++;
                pnd_Ws_Pnd_Ric_Tran_Cnt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #RIC-TRAN-CNT
                if (condition(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Rtb_Ivc_Cde().notEquals("2")))                                                                            //Natural: IF #I-RTB-IVC-CDE NE '2'
                {
                    pnd_Work_Rec_Pnd_Msg_Idx.setValue(2);                                                                                                                 //Natural: ASSIGN #MSG-IDX := 2
                    //*  RTB
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_Paymt_Category.setValue("2");                                                                                                                  //Natural: ASSIGN #PAYMT-CATEGORY := '2'
                                                                                                                                                                          //Natural: PERFORM TAX-MAINTENANCE
                sub_Tax_Maintenance();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'TIC'
            else if (condition((ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("TIC"))))
            {
                decideConditionsMet755++;
                //*  PERIODIC PAYMENT
                pnd_Ws_Pnd_Tic_Tran_Cnt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #TIC-TRAN-CNT
                pnd_Ws_Pnd_Paymt_Category.setValue("1");                                                                                                                  //Natural: ASSIGN #PAYMT-CATEGORY := '1'
                                                                                                                                                                          //Natural: PERFORM TAX-MAINTENANCE
                sub_Tax_Maintenance();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'RES'
            else if (condition((ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("RES"))))
            {
                decideConditionsMet755++;
                pnd_Ws_Pnd_Res_Tran_Cnt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #RES-TRAN-CNT
                                                                                                                                                                          //Natural: PERFORM RESIDENCY-CHANGE-REPORT
                sub_Residency_Change_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'SSN'
            else if (condition((ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("SSN"))))
            {
                decideConditionsMet755++;
                pnd_Ws_Pnd_Ssn_Tran_Cnt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #SSN-TRAN-CNT
                                                                                                                                                                          //Natural: PERFORM SSN-REPORT
                sub_Ssn_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'CIT'
            else if (condition((ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("CIT"))))
            {
                decideConditionsMet755++;
                pnd_Ws_Pnd_Cit_Tran_Cnt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CIT-TRAN-CNT
                                                                                                                                                                          //Natural: PERFORM CITIZENSHIP-REPORT
                sub_Citizenship_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'DB1'
            else if (condition((ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("DB1"))))
            {
                decideConditionsMet755++;
                pnd_Ws_Pnd_Dob1_Tran_Cnt.nadd(1);                                                                                                                         //Natural: ADD 1 TO #DOB1-TRAN-CNT
                                                                                                                                                                          //Natural: PERFORM PROCESS-DOB1
                sub_Process_Dob1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'DB2'
            else if (condition((ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("DB2"))))
            {
                decideConditionsMet755++;
                pnd_Ws_Pnd_Dob2_Tran_Cnt.nadd(1);                                                                                                                         //Natural: ADD 1 TO #DOB2-TRAN-CNT
                                                                                                                                                                          //Natural: PERFORM PROCESS-DOB2
                sub_Process_Dob2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'DD1'
            else if (condition((ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("DD1"))))
            {
                decideConditionsMet755++;
                pnd_Ws_Pnd_Dod1_Tran_Cnt.nadd(1);                                                                                                                         //Natural: ADD 1 TO #DOD1-TRAN-CNT
                                                                                                                                                                          //Natural: PERFORM PROCESS-DOD1
                sub_Process_Dod1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'DD2'
            else if (condition((ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("DD2"))))
            {
                decideConditionsMet755++;
                pnd_Ws_Pnd_Dod2_Tran_Cnt.nadd(1);                                                                                                                         //Natural: ADD 1 TO #DOD2-TRAN-CNT
                                                                                                                                                                          //Natural: PERFORM PROCESS-DOD2
                sub_Process_Dod2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Ws_Pnd_Invalid_Tran_Cnt.nadd(1);                                                                                                                      //Natural: ADD 1 TO #INVALID-TRAN-CNT
                                                                                                                                                                          //Natural: PERFORM INVALID-TRAN-REPORT
                sub_Invalid_Tran_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getWorkFiles().close(2);                                                                                                                                          //Natural: CLOSE WORK FILE 2
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 2 #WORK-REC #I-MAINT-CDE #I-CNTRCT-NBR #I-PAYEE-CDE #I-TAX-ID #I-PYMNT-MONTH #I-CITIZENSHIP #I-NEW-RSDNCY-CDE #I-TRANS-CDE #I-PER-IVC-CDE #I-RTB-IVC-CDE #I-YTD-IVC-AMT #I-TOT-IVC-AMT #I-FIRST-PYMNT-DTE
        while (condition(getWorkFiles().read(2, pnd_Work_Rec, ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr(), 
            ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tax_Id(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Pymnt_Month(), 
            ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Citizenship(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Rsdncy_Cde(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Trans_Cde(), 
            ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Per_Ivc_Cde(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Rtb_Ivc_Cde(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt(), 
            ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_First_Pymnt_Dte())))
        {
            getSort().writeSortInData(pnd_Work_Rec_Pnd_Tax_Year, ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tax_Id(),               //Natural: END-ALL
                ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde(), pnd_Work_Rec_Pnd_Case_Num, pnd_Work_Rec_Pnd_Record_Type, 
                ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Pymnt_Month(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Citizenship(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Rsdncy_Cde(), 
                ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Trans_Cde(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Per_Ivc_Cde(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Rtb_Ivc_Cde(), 
                ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_First_Pymnt_Dte(), 
                pnd_Work_Rec_Pnd_Msg_Idx, pnd_Work_Rec_Pnd_Isn, pnd_Work_Rec_Pnd_Multi_Case, pnd_Work_Rec_Pnd_Multi_Residency, pnd_Work_Rec_Pnd_Multi_Record);
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(pnd_Work_Rec_Pnd_Tax_Year, ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tax_Id(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr(),  //Natural: SORT BY #WORK-REC.#TAX-YEAR #I-MAINT-CDE #I-TAX-ID #I-CNTRCT-NBR #I-PAYEE-CDE #WORK-REC.#CASE-NUM #WORK-REC.#RECORD-TYPE USING #I-PYMNT-MONTH #I-CITIZENSHIP #I-NEW-RSDNCY-CDE #I-TRANS-CDE #I-PER-IVC-CDE #I-RTB-IVC-CDE #I-YTD-IVC-AMT #I-TOT-IVC-AMT #I-FIRST-PYMNT-DTE #WORK-REC.#MSG-IDX #WORK-REC.#ISN #WORK-REC.#MULTI-CASE #WORK-REC.#MULTI-RESIDENCY #WORK-REC.#MULTI-RECORD
            ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde(), pnd_Work_Rec_Pnd_Case_Num, pnd_Work_Rec_Pnd_Record_Type);
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Work_Rec_Pnd_Tax_Year, ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tax_Id(), 
            ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde(), pnd_Work_Rec_Pnd_Case_Num, pnd_Work_Rec_Pnd_Record_Type, 
            ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Pymnt_Month(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Citizenship(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Rsdncy_Cde(), 
            ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Trans_Cde(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Per_Ivc_Cde(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Rtb_Ivc_Cde(), 
            ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_First_Pymnt_Dte(), 
            pnd_Work_Rec_Pnd_Msg_Idx, pnd_Work_Rec_Pnd_Isn, pnd_Work_Rec_Pnd_Multi_Case, pnd_Work_Rec_Pnd_Multi_Residency, pnd_Work_Rec_Pnd_Multi_Record)))
        {
            if (condition(pnd_Work_Rec_Pnd_Record_Type.equals(pnd_Ws_Const_Low_Values)))                                                                                  //Natural: IF #WORK-REC.#RECORD-TYPE = LOW-VALUES
            {
                pnd_Save_Rec.setValuesByName(pnd_Work_Rec);                                                                                                               //Natural: MOVE BY NAME #WORK-REC TO #SAVE-REC
                if (condition(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("IVC") || ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("RIC")))                //Natural: IF #I-MAINT-CDE = 'IVC' OR = 'RIC'
                {
                    pnd_Ws_Pnd_Ivc_Cv.setValue("AD=D");                                                                                                                   //Natural: ASSIGN #IVC-CV := ( AD = D )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_Ivc_Cv.setValue("AD=N");                                                                                                                   //Natural: ASSIGN #IVC-CV := ( AD = N )
                }                                                                                                                                                         //Natural: END-IF
                Global.format("IS=OFF");                                                                                                                                  //Natural: SUSPEND IDENTICAL SUPPRESS ( 08 )
                if (condition(pnd_Work_Rec_Pnd_Msg_Idx.notEquals(getZero())))                                                                                             //Natural: IF #WORK-REC.#MSG-IDX NE 0
                {
                                                                                                                                                                          //Natural: PERFORM TWR-ERROR-REPORT
                    sub_Twr_Error_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Work_Rec_Pnd_Multi_Case.getBoolean()))                                                                                                  //Natural: IF #WORK-REC.#MULTI-CASE
                {
                    pnd_Work_Rec_Pnd_Msg_Idx.setValue(9);                                                                                                                 //Natural: ASSIGN #MSG-IDX := 9
                                                                                                                                                                          //Natural: PERFORM IVC-CHANGE-REPORT
                    sub_Ivc_Change_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Work_Rec_Pnd_Multi_Residency.getBoolean()))                                                                                             //Natural: IF #WORK-REC.#MULTI-RESIDENCY
                {
                    pnd_Work_Rec_Pnd_Msg_Idx.setValue(10);                                                                                                                //Natural: ASSIGN #MSG-IDX := 10
                                                                                                                                                                          //Natural: PERFORM IVC-CHANGE-REPORT
                    sub_Ivc_Change_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-CHANGES
            sub_Process_Changes();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-TRAILER
        sub_Process_Trailer();
        if (condition(Global.isEscape())) {return;}
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-DUP-TRAN
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TAX-MAINTENANCE
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORKFILE
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MULTIPLE-PYMNT-RECORDS
        //* ***************************************
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CHANGES
        //* ********************************
        //*  11-15-99 TT - ISSUE MSG IF IVC "protect" STATUS HAS BEEN SET
        //*  11-17-99 TT - THIS MSG WILL NOT BE ISSUED FOR NOW
        //*  IF #IVC-PROTECT-SET-IND THEN
        //*    #MSG-IDX                       := 23
        //*    PERFORM IVC-CHANGE-REPORT
        //*  END-IF
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ET-PROCESSING
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TIC
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-IVC
        //*  11-15-99 TT - ISSUE MSG IF IVC "protect" STATUS HAS BEEN SET
        //*  11-17-99 TT - THIS MSG WILL NOT BE ISSUED FOR NOW
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-DOB1
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-DOB2
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-DOD1
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-DOD2
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INVALID-TRAN-REPORT
        //* *************************************
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CITIZENSHIP-REPORT
        //* ***********************************
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DOB-REPORT
        //* ***************************
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DOD-REPORT
        //* ***************************
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SSN-REPORT
        //* ***************************
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TWR-ERROR-REPORT
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESIDENCY-CHANGE-REPORT
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IVC-CHANGE-REPORT
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-PROGRAM-PROCESSING
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TRAILER
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-HEADER
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INPUT-PARM
        //* ***********************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Check_Dup_Tran() throws Exception                                                                                                                    //Natural: CHECK-DUP-TRAN
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        if (condition(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr().equals(pnd_Ws_Pnd_Prev_Cntrct_Nbr) && ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde().equals(pnd_Ws_Pnd_Prev_Payee_Cde)  //Natural: IF #I-CNTRCT-NBR = #PREV-CNTRCT-NBR AND #I-PAYEE-CDE = #PREV-PAYEE-CDE AND #I-MAINT-CDE = #PREV-MAINT-CDE
            && ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals(pnd_Ws_Pnd_Prev_Maint_Cde)))
        {
            pnd_Work_Rec_Pnd_Msg_Idx.setValue(3);                                                                                                                         //Natural: ASSIGN #MSG-IDX := 3
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Prev_Cntrct_Nbr.setValue(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr());                                                                          //Natural: ASSIGN #PREV-CNTRCT-NBR := #I-CNTRCT-NBR
            pnd_Ws_Pnd_Prev_Payee_Cde.setValue(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde());                                                                            //Natural: ASSIGN #PREV-PAYEE-CDE := #I-PAYEE-CDE
            pnd_Ws_Pnd_Prev_Maint_Cde.setValue(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde());                                                                            //Natural: ASSIGN #PREV-MAINT-CDE := #I-MAINT-CDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Tax_Maintenance() throws Exception                                                                                                                   //Natural: TAX-MAINTENANCE
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pnd_Work_Rec_Pnd_Work_Det.reset();                                                                                                                                //Natural: RESET #WORK-REC.#WORK-DET #REC-ACCEPTED #ACCEPT-IND
        pnd_Ws_Pnd_Rec_Accepted.reset();
        pnd_Ws_Pnd_Accept_Ind.reset();
        pnd_Ws_Pnd_Accept_First.setValue(true);                                                                                                                           //Natural: ASSIGN #ACCEPT-FIRST := TRUE
                                                                                                                                                                          //Natural: PERFORM CHECK-DUP-TRAN
        sub_Check_Dup_Tran();
        if (condition(Global.isEscape())) {return;}
        //* * V1.04  1-4-00 TT / MIKE S.
        //* * FF. STMT DISABLED - #WORK-REC.#TAX-YEAR IS SET IN
        //* *   PROCESS-INPUT-PARM
        //* * MOVE EDITED #CNTRL-DTE(EM=YYYY) TO #WORK-REC.#TAX-YEAR
        pnd_Work_Rec_Pnd_Case_Num.nadd(1);                                                                                                                                //Natural: ADD 1 TO #WORK-REC.#CASE-NUM
        if (condition(pnd_Work_Rec_Pnd_Msg_Idx.notEquals(getZero())))                                                                                                     //Natural: IF #WORK-REC.#MSG-IDX NE 0
        {
            pnd_Work_Rec_Pnd_Record_Type.setValue(pnd_Ws_Const_Low_Values);                                                                                               //Natural: ASSIGN #WORK-REC.#RECORD-TYPE := LOW-VALUES
                                                                                                                                                                          //Natural: PERFORM WRITE-WORKFILE
            sub_Write_Workfile();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Con_Pay_Sd_Pnd_Tax_Year.setValue(pnd_Work_Rec_Pnd_Tax_Year);                                                                                                  //Natural: ASSIGN #CON-PAY-SD.#TAX-YEAR := #WORK-REC.#TAX-YEAR
        pdaTwratbl1.getTwratbl1_Pnd_Tax_Year().setValueEdited(new ReportEditMask("9999"),pnd_Work_Rec_Pnd_Tax_Year);                                                      //Natural: MOVE EDITED #WORK-REC.#TAX-YEAR TO TWRATBL1.#TAX-YEAR ( EM = 9999 )
        pnd_Con_Pay_Sd_Pnd_Contract_Nbr.setValue(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr());                                                                         //Natural: ASSIGN #CON-PAY-SD.#CONTRACT-NBR := #I-CNTRCT-NBR
        pnd_Con_Pay_Sd_Pnd_Payee_Cde.setValue(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde());                                                                             //Natural: ASSIGN #CON-PAY-SD.#PAYEE-CDE := #I-PAYEE-CDE
        ldaTwrl9415.getVw_pay().startDatabaseRead                                                                                                                         //Natural: READ PAY BY TWRPYMNT-CON-PAY-SD = #CON-PAY-SD THRU #CON-PAY-SD WHERE TWRPYMNT-CONTRACT-SEQ-NO = 01
        (
        "RD_1",
        new Wc[] { new Wc("TWRPYMNT_CONTRACT_SEQ_NO", "=", 1, WcType.WHERE) ,
        new Wc("TWRPYMNT_CON_PAY_SD", ">=", pnd_Con_Pay_Sd, "And", WcType.BY) ,
        new Wc("TWRPYMNT_CON_PAY_SD", "<=", pnd_Con_Pay_Sd, WcType.BY) },
        new Oc[] { new Oc("TWRPYMNT_CON_PAY_SD", "ASC") }
        );
        RD_1:
        while (condition(ldaTwrl9415.getVw_pay().readNextRow("RD_1")))
        {
            if (condition(!(ldaTwrl9415.getPay_Twrpymnt_Paymt_Category().equals(pnd_Ws_Pnd_Paymt_Category))))                                                             //Natural: ACCEPT IF TWRPYMNT-PAYMT-CATEGORY = #PAYMT-CATEGORY
            {
                continue;
            }
            pnd_Work_Rec_Pnd_Multi_Record.reset();                                                                                                                        //Natural: RESET #WORK-REC.#MULTI-RECORD
            if (condition(ldaTwrl9415.getPay_Count_Casttwrpymnt_Payments().greaterOrEqual(24)))                                                                           //Natural: IF C*TWRPYMNT-PAYMENTS GE 24
            {
                                                                                                                                                                          //Natural: PERFORM MULTIPLE-PYMNT-RECORDS
                sub_Multiple_Pymnt_Records();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD_1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().notEquals("TIC") && ldaTwrl9415.getPay_Count_Casttwrpymnt_Payments().equals(24)))             //Natural: IF #I-MAINT-CDE NE 'TIC' AND C*TWRPYMNT-PAYMENTS = 24
                {
                    pnd_Work_Rec_Pnd_Multi_Record.setValue(true);                                                                                                         //Natural: ASSIGN #WORK-REC.#MULTI-RECORD := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl9415.getPay_Twrpymnt_Sum_Gross_Amt().equals(new DbsDecimal("0.00")) && ldaTwrl9415.getPay_Twrpymnt_Sum_Int_Amt().notEquals(new           //Natural: IF TWRPYMNT-SUM-GROSS-AMT = 0.00 AND TWRPYMNT-SUM-INT-AMT NE 0.00
                DbsDecimal("0.00"))))
            {
                //*  INTEREST ONLY
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl9415.getPay_Twrpymnt_Pymnt_Refer_Nbr().getValue(1).notEquals(pdaTwratbl1.getTwratbl1_Pnd_Ref_Num())))                                    //Natural: IF PAY.TWRPYMNT-PYMNT-REFER-NBR ( 1 ) NE TWRATBL1.#REF-NUM
            {
                pdaTwratbl1.getTwratbl1_Pnd_Ref_Num().setValue(ldaTwrl9415.getPay_Twrpymnt_Pymnt_Refer_Nbr().getValue(1));                                                //Natural: ASSIGN TWRATBL1.#REF-NUM := PAY.TWRPYMNT-PYMNT-REFER-NBR ( 1 )
                DbsUtil.callnat(Twrntbl1.class , getCurrentProcessState(), pdaTwratbl1.getTwratbl1_Input_Parms(), new AttributeParameter("O"), pdaTwratbl1.getTwratbl1_Output_Data(),  //Natural: CALLNAT 'TWRNTBL1' USING TWRATBL1.INPUT-PARMS ( AD = O ) TWRATBL1.OUTPUT-DATA ( AD = M )
                    new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(pdaTwratbl1.getTwratbl1_Tircntl_Dist_Method().equals("C"))))                                                                                  //Natural: ACCEPT IF TWRATBL1.TIRCNTL-DIST-METHOD = 'C'
            {
                continue;
            }
            if (condition(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("TIC")))                                                                                   //Natural: IF #I-MAINT-CDE = 'TIC'
            {
                pnd_Ws_Pnd_Accept_Ind.setValue(true);                                                                                                                     //Natural: ASSIGN #ACCEPT-IND := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                FOR01:                                                                                                                                                    //Natural: FOR #I = 1 TO C*TWRPYMNT-PAYMENTS
                for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl9415.getPay_Count_Casttwrpymnt_Payments())); pnd_Ws_Pnd_I.nadd(1))
                {
                    if (condition((((ldaTwrl9415.getPay_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals(" ") || ldaTwrl9415.getPay_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals("C"))  //Natural: IF TWRPYMNT-PYMNT-STATUS ( #I ) = ' ' OR = 'C' AND ( TWRPYMNT-PYMNT-DTE ( #I ) LT #PYMNT-DTE OR TWRPYMNT-PYMNT-DTE ( #I ) = #PYMNT-DTE AND TWRPYMNT-UPDTE-SRCE-CDE ( #I ) = 'TM' ) AND NOT ( TWRPYMNT-GROSS-AMT ( #I ) = 0.00 AND TWRPYMNT-INT-AMT ( #I ) NE 0.00 )
                        && (ldaTwrl9415.getPay_Twrpymnt_Pymnt_Dte().getValue(pnd_Ws_Pnd_I).less(pnd_Ws_Pnd_Pymnt_Dte) || (ldaTwrl9415.getPay_Twrpymnt_Pymnt_Dte().getValue(pnd_Ws_Pnd_I).equals(pnd_Ws_Pnd_Pymnt_Dte) 
                        && ldaTwrl9415.getPay_Twrpymnt_Updte_Srce_Cde().getValue(pnd_Ws_Pnd_I).equals("TM")))) && ! ((ldaTwrl9415.getPay_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I).equals(new 
                        DbsDecimal("0.00")) && ldaTwrl9415.getPay_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00")))))))
                    {
                        pnd_Ws_Pnd_Accept_Ind.setValue(true);                                                                                                             //Natural: ASSIGN #ACCEPT-IND := TRUE
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD_1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(pnd_Ws_Pnd_Accept_Ind.getBoolean())))                                                                                                         //Natural: ACCEPT IF #ACCEPT-IND
            {
                continue;
            }
            pnd_Ws_Pnd_Rec_Accepted.nadd(1);                                                                                                                              //Natural: ADD 1 TO #REC-ACCEPTED
            if (condition(ldaTwrl9415.getPay_Twrpymnt_Tax_Citizenship().notEquals("U")))                                                                                  //Natural: IF TWRPYMNT-TAX-CITIZENSHIP NE 'U'
            {
                ldaTwrl9415.getPay_Twrpymnt_Distribution_Cde().reset();                                                                                                   //Natural: RESET TWRPYMNT-DISTRIBUTION-CDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl9415.getPay_Twrpymnt_Sum_Gross_Amt().notEquals(new DbsDecimal("0.00"))))                                                                 //Natural: IF TWRPYMNT-SUM-GROSS-AMT NE 0.00
            {
                if (condition(pnd_Ws_Pnd_Accept_First.getBoolean()))                                                                                                      //Natural: IF #ACCEPT-FIRST
                {
                    pnd_Ws_Pnd_Prev_Tax_Id_Nbr.setValue(ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr());                                                                        //Natural: ASSIGN #PREV-TAX-ID-NBR := TWRPYMNT-TAX-ID-NBR
                    pnd_Ws_Pnd_Prev_Distribution_Cde.setValue(ldaTwrl9415.getPay_Twrpymnt_Distribution_Cde());                                                            //Natural: ASSIGN #PREV-DISTRIBUTION-CDE := TWRPYMNT-DISTRIBUTION-CDE
                    pnd_Ws_Pnd_Prev_Tax_Citizenship.setValue(ldaTwrl9415.getPay_Twrpymnt_Tax_Citizenship());                                                              //Natural: ASSIGN #PREV-TAX-CITIZENSHIP := TWRPYMNT-TAX-CITIZENSHIP
                    pnd_Ws_Pnd_Prev_Residency_Type.setValue(ldaTwrl9415.getPay_Twrpymnt_Residency_Type());                                                                //Natural: ASSIGN #PREV-RESIDENCY-TYPE := TWRPYMNT-RESIDENCY-TYPE
                    pnd_Ws_Pnd_Prev_Residency_Code.setValue(ldaTwrl9415.getPay_Twrpymnt_Residency_Code());                                                                //Natural: ASSIGN #PREV-RESIDENCY-CODE := TWRPYMNT-RESIDENCY-CODE
                    pnd_Ws_Pnd_Prev_Locality_Cde.setValue(ldaTwrl9415.getPay_Twrpymnt_Locality_Cde());                                                                    //Natural: ASSIGN #PREV-LOCALITY-CDE := TWRPYMNT-LOCALITY-CDE
                    pnd_Ws_Pnd_Accept_First.setValue(false);                                                                                                              //Natural: ASSIGN #ACCEPT-FIRST := FALSE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr().notEquals(pnd_Ws_Pnd_Prev_Tax_Id_Nbr) || ldaTwrl9415.getPay_Twrpymnt_Distribution_Cde().notEquals(pnd_Ws_Pnd_Prev_Distribution_Cde))) //Natural: IF TWRPYMNT-TAX-ID-NBR NE #PREV-TAX-ID-NBR OR TWRPYMNT-DISTRIBUTION-CDE NE #PREV-DISTRIBUTION-CDE
                    {
                        pnd_Work_Rec_Pnd_Multi_Case.setValue(true);                                                                                                       //Natural: ASSIGN #WORK-REC.#MULTI-CASE := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl9415.getPay_Twrpymnt_Tax_Citizenship().notEquals(pnd_Ws_Pnd_Prev_Tax_Citizenship) || ldaTwrl9415.getPay_Twrpymnt_Residency_Type().notEquals(pnd_Ws_Pnd_Prev_Residency_Type)  //Natural: IF TWRPYMNT-TAX-CITIZENSHIP NE #PREV-TAX-CITIZENSHIP OR TWRPYMNT-RESIDENCY-TYPE NE #PREV-RESIDENCY-TYPE OR TWRPYMNT-RESIDENCY-CODE NE #PREV-RESIDENCY-CODE OR TWRPYMNT-LOCALITY-CDE NE #PREV-LOCALITY-CDE
                        || ldaTwrl9415.getPay_Twrpymnt_Residency_Code().notEquals(pnd_Ws_Pnd_Prev_Residency_Code) || ldaTwrl9415.getPay_Twrpymnt_Locality_Cde().notEquals(pnd_Ws_Pnd_Prev_Locality_Cde)))
                    {
                        pnd_Work_Rec_Pnd_Multi_Residency.setValue(true);                                                                                                  //Natural: ASSIGN #WORK-REC.#MULTI-RESIDENCY := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Rec_Pnd_Isn.setValue(ldaTwrl9415.getVw_pay().getAstISN("RD_1"));                                                                                     //Natural: ASSIGN #WORK-REC.#ISN := *ISN
                                                                                                                                                                          //Natural: PERFORM WRITE-WORKFILE
            sub_Write_Workfile();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD_1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Work_Rec_Pnd_Record_Type.setValue(pnd_Ws_Const_Low_Values);                                                                                                   //Natural: ASSIGN #WORK-REC.#RECORD-TYPE := LOW-VALUES
        short decideConditionsMet1000 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN *COUNTER ( RD-1. ) = 0
        if (condition(ldaTwrl9415.getVw_pay().getAstCOUNTER().equals(getZero())))
        {
            decideConditionsMet1000++;
            pnd_Work_Rec_Pnd_Msg_Idx.setValue(1);                                                                                                                         //Natural: ASSIGN #WORK-REC.#MSG-IDX := 1
        }                                                                                                                                                                 //Natural: WHEN #REC-ACCEPTED = 0
        else if (condition(pnd_Ws_Pnd_Rec_Accepted.equals(getZero())))
        {
            decideConditionsMet1000++;
            pnd_Work_Rec_Pnd_Msg_Idx.setValue(4);                                                                                                                         //Natural: ASSIGN #WORK-REC.#MSG-IDX := 4
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORKFILE
        sub_Write_Workfile();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Write_Workfile() throws Exception                                                                                                                    //Natural: WRITE-WORKFILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        getWorkFiles().write(2, false, pnd_Work_Rec, ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde(),  //Natural: WRITE WORK FILE 2 #WORK-REC #I-MAINT-CDE #I-CNTRCT-NBR #I-PAYEE-CDE #I-TAX-ID #I-PYMNT-MONTH #I-CITIZENSHIP #I-NEW-RSDNCY-CDE #I-TRANS-CDE #I-PER-IVC-CDE #I-RTB-IVC-CDE #I-YTD-IVC-AMT #I-TOT-IVC-AMT #I-FIRST-PYMNT-DTE
            ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tax_Id(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Pymnt_Month(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Citizenship(), 
            ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Rsdncy_Cde(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Trans_Cde(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Per_Ivc_Cde(), 
            ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Rtb_Ivc_Cde(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt(), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt(), 
            ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_First_Pymnt_Dte());
    }
    private void sub_Multiple_Pymnt_Records() throws Exception                                                                                                            //Natural: MULTIPLE-PYMNT-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Form_Sd_Detail_Pnd_F_Tax_Year.setValue(ldaTwrl9415.getPay_Twrpymnt_Tax_Year());                                                                               //Natural: ASSIGN #F-TAX-YEAR := PAY.TWRPYMNT-TAX-YEAR
        pnd_Form_Sd_Detail_Pnd_F_Tax_Id_Nbr.setValue(ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr());                                                                           //Natural: ASSIGN #F-TAX-ID-NBR := PAY.TWRPYMNT-TAX-ID-NBR
        pnd_Form_Sd_Detail_Pnd_F_Contract_Nbr.setValue(ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr());                                                                       //Natural: ASSIGN #F-CONTRACT-NBR := PAY.TWRPYMNT-CONTRACT-NBR
        pnd_Form_Sd_Detail_Pnd_F_Payee_Cde.setValue(ldaTwrl9415.getPay_Twrpymnt_Payee_Cde());                                                                             //Natural: ASSIGN #F-PAYEE-CDE := PAY.TWRPYMNT-PAYEE-CDE
        pnd_Form_Sd_Detail_Pnd_F_Company_Cde.setValue(ldaTwrl9415.getPay_Twrpymnt_Company_Cde());                                                                         //Natural: ASSIGN #F-COMPANY-CDE := PAY.TWRPYMNT-COMPANY-CDE
        pnd_Form_Sd_Detail_Pnd_F_Tax_Citizenship.setValue(ldaTwrl9415.getPay_Twrpymnt_Tax_Citizenship());                                                                 //Natural: ASSIGN #F-TAX-CITIZENSHIP := PAY.TWRPYMNT-TAX-CITIZENSHIP
        pnd_Form_Sd_Detail_Pnd_F_Paymt_Category.setValue(ldaTwrl9415.getPay_Twrpymnt_Paymt_Category());                                                                   //Natural: ASSIGN #F-PAYMT-CATEGORY := PAY.TWRPYMNT-PAYMT-CATEGORY
        pnd_Form_Sd_Detail_Pnd_F_Distribution_Cde.setValue(ldaTwrl9415.getPay_Twrpymnt_Distribution_Cde());                                                               //Natural: ASSIGN #F-DISTRIBUTION-CDE := PAY.TWRPYMNT-DISTRIBUTION-CDE
        pnd_Form_Sd_Detail_Pnd_F_Residency_Type.setValue(ldaTwrl9415.getPay_Twrpymnt_Residency_Type());                                                                   //Natural: ASSIGN #F-RESIDENCY-TYPE := PAY.TWRPYMNT-RESIDENCY-TYPE
        pnd_Form_Sd_Detail_Pnd_F_Residency_Code.setValue(ldaTwrl9415.getPay_Twrpymnt_Residency_Code());                                                                   //Natural: ASSIGN #F-RESIDENCY-CODE := PAY.TWRPYMNT-RESIDENCY-CODE
        pnd_Form_Sd_Detail_Pnd_F_Locality_Cde.setValue(ldaTwrl9415.getPay_Twrpymnt_Locality_Cde());                                                                       //Natural: ASSIGN #F-LOCALITY-CDE := PAY.TWRPYMNT-LOCALITY-CDE
        pnd_Form_Sd_Detail_Pnd_F_Contract_Seq_No.setValue(2);                                                                                                             //Natural: ASSIGN #F-CONTRACT-SEQ-NO := 02
        pnd_Form_Sd_End.setValue(pnd_Form_Sd_Detail_Pnd_Form_Sd_Start);                                                                                                   //Natural: ASSIGN #FORM-SD-END := #FORM-SD-START
        setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Form_Sd_End,36,1);                                                                                               //Natural: MOVE HIGH-VALUES TO SUBSTR ( #FORM-SD-END,36,1 )
        ldaTwrl9420.getVw_payr().startDatabaseRead                                                                                                                        //Natural: READ PAYR BY TWRPYMNT-FORM-SD = #FORM-SD-START THRU #FORM-SD-END
        (
        "RD_2",
        new Wc[] { new Wc("TWRPYMNT_FORM_SD", ">=", pnd_Form_Sd_Detail_Pnd_Form_Sd_Start, "And", WcType.BY) ,
        new Wc("TWRPYMNT_FORM_SD", "<=", pnd_Form_Sd_End, WcType.BY) },
        new Oc[] { new Oc("TWRPYMNT_FORM_SD", "ASC") }
        );
        RD_2:
        while (condition(ldaTwrl9420.getVw_payr().readNextRow("RD_2")))
        {
            pnd_Work_Rec_Pnd_Multi_Record.setValue(true);                                                                                                                 //Natural: ASSIGN #WORK-REC.#MULTI-RECORD := TRUE
            ldaTwrl9415.getPay_Twrpymnt_Sum_Gross_Amt().nadd(ldaTwrl9420.getPayr_Twrpymnt_Sum_Gross_Amt());                                                               //Natural: ADD PAYR.TWRPYMNT-SUM-GROSS-AMT TO PAY.TWRPYMNT-SUM-GROSS-AMT
            ldaTwrl9415.getPay_Twrpymnt_Sum_Int_Amt().nadd(ldaTwrl9420.getPayr_Twrpymnt_Sum_Int_Amt());                                                                   //Natural: ADD PAYR.TWRPYMNT-SUM-INT-AMT TO PAY.TWRPYMNT-SUM-INT-AMT
            if (condition(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("TIC") || pnd_Ws_Pnd_Accept_Ind.getBoolean()))                                             //Natural: IF #I-MAINT-CDE = 'TIC' OR #ACCEPT-IND
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            FOR02:                                                                                                                                                        //Natural: FOR #I = 1 TO C*TWRPYMNT-PAYMENTS
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl9420.getPayr_Count_Casttwrpymnt_Payments())); pnd_Ws_Pnd_I.nadd(1))
            {
                if (condition((((ldaTwrl9420.getPayr_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals(" ") || ldaTwrl9420.getPayr_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals("C"))  //Natural: IF PAYR.TWRPYMNT-PYMNT-STATUS ( #I ) = ' ' OR = 'C' AND ( PAYR.TWRPYMNT-PYMNT-DTE ( #I ) LT #PYMNT-DTE OR PAYR.TWRPYMNT-PYMNT-DTE ( #I ) = #PYMNT-DTE AND PAYR.TWRPYMNT-UPDTE-SRCE-CDE ( #I ) = 'TM' ) AND NOT ( PAYR.TWRPYMNT-GROSS-AMT ( #I ) = 0.00 AND PAYR.TWRPYMNT-INT-AMT ( #I ) NE 0.00 )
                    && (ldaTwrl9420.getPayr_Twrpymnt_Pymnt_Dte().getValue(pnd_Ws_Pnd_I).less(pnd_Ws_Pnd_Pymnt_Dte) || (ldaTwrl9420.getPayr_Twrpymnt_Pymnt_Dte().getValue(pnd_Ws_Pnd_I).equals(pnd_Ws_Pnd_Pymnt_Dte) 
                    && ldaTwrl9420.getPayr_Twrpymnt_Updte_Srce_Cde().getValue(pnd_Ws_Pnd_I).equals("TM")))) && ! ((ldaTwrl9420.getPayr_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I).equals(new 
                    DbsDecimal("0.00")) && ldaTwrl9420.getPayr_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00")))))))
                {
                    pnd_Ws_Pnd_Accept_Ind.setValue(true);                                                                                                                 //Natural: ASSIGN #ACCEPT-IND := TRUE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD_2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD_2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Process_Changes() throws Exception                                                                                                                   //Natural: PROCESS-CHANGES
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_Tot_Ivc.setValue(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt());                                                                                     //Natural: ASSIGN #TOT-IVC := #I-TOT-IVC-AMT
        short decideConditionsMet1047 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #I-MAINT-CDE;//Natural: VALUE 'IVC'
        if (condition((ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("IVC"))))
        {
            decideConditionsMet1047++;
            pnd_Ws_Pnd_Ivc_Tran_Accepted.nadd(1);                                                                                                                         //Natural: ADD 1 TO #IVC-TRAN-ACCEPTED
            pnd_Ws_Pnd_Ivc_Ytd_Amt.nadd(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt());                                                                                 //Natural: ADD #I-YTD-IVC-AMT TO #IVC-YTD-AMT
            pnd_Ws_Pnd_Ivc_Tot_Amt.nadd(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt());                                                                                 //Natural: ADD #I-TOT-IVC-AMT TO #IVC-TOT-AMT
        }                                                                                                                                                                 //Natural: VALUE 'RIC'
        else if (condition((ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("RIC"))))
        {
            decideConditionsMet1047++;
            pnd_Ws_Pnd_Rtb_Ivc_Accept.nadd(1);                                                                                                                            //Natural: ADD 1 TO #RTB-IVC-ACCEPT
            pnd_Ws_Pnd_Rtb_Ytd_Amt.nadd(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt());                                                                                 //Natural: ADD #I-YTD-IVC-AMT TO #RTB-YTD-AMT
            pnd_Ws_Pnd_Rtb_Tot_Amt.nadd(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt());                                                                                 //Natural: ADD #I-TOT-IVC-AMT TO #RTB-TOT-AMT
        }                                                                                                                                                                 //Natural: VALUE 'TIC'
        else if (condition((ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("TIC"))))
        {
            decideConditionsMet1047++;
            pnd_Ws_Pnd_Tic_Ivc_Accept.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TIC-IVC-ACCEPT
            pnd_Ws_Pnd_Tic_Ytd_Amt.nadd(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt());                                                                                 //Natural: ADD #I-YTD-IVC-AMT TO #TIC-YTD-AMT
            pnd_Ws_Pnd_Tic_Tot_Amt.nadd(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt());                                                                                 //Natural: ADD #I-TOT-IVC-AMT TO #TIC-TOT-AMT
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        G_U:                                                                                                                                                              //Natural: GET PAYU #WORK-REC.#ISN
        ldaTwrl9425.getVw_payu().readByID(pnd_Work_Rec_Pnd_Isn.getLong(), "G_U");
        pnd_Ws_Pnd_Et_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ET-CNT
        short decideConditionsMet1066 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #WORK-REC.#MULTI-RECORD
        if (condition(pnd_Work_Rec_Pnd_Multi_Record.getBoolean()))
        {
            decideConditionsMet1066++;
            pnd_Work_Rec_Pnd_Msg_Idx.setValue(22);                                                                                                                        //Natural: ASSIGN #MSG-IDX := 22
        }                                                                                                                                                                 //Natural: WHEN PAYU.TWRPYMNT-SUM-GROSS-AMT = 0.00
        else if (condition(ldaTwrl9425.getPayu_Twrpymnt_Sum_Gross_Amt().equals(new DbsDecimal("0.00"))))
        {
            decideConditionsMet1066++;
            pnd_Work_Rec_Pnd_Msg_Idx.setValue(5);                                                                                                                         //Natural: ASSIGN #MSG-IDX := 5
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet1066 > 0))
        {
                                                                                                                                                                          //Natural: PERFORM IVC-CHANGE-REPORT
            sub_Ivc_Change_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Work_Rec_Pnd_Msg_Idx.setValue(6);                                                                                                                         //Natural: ASSIGN #MSG-IDX := 6
                                                                                                                                                                          //Natural: PERFORM IVC-CHANGE-REPORT
            sub_Ivc_Change_Report();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ET-PROCESSING
            sub_Et_Processing();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Ws_Pnd_Tic_Update_Ind.reset();                                                                                                                                //Natural: RESET #TIC-UPDATE-IND #IVC-UPDATE-IND
        pnd_Ws_Pnd_Ivc_Update_Ind.reset();
        short decideConditionsMet1081 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #I-MAINT-CDE;//Natural: VALUE 'IVC'
        if (condition((ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("IVC"))))
        {
            decideConditionsMet1081++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIC
            sub_Process_Tic();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-IVC
            sub_Process_Ivc();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'RIC'
        else if (condition((ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("RIC"))))
        {
            decideConditionsMet1081++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-IVC
            sub_Process_Ivc();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'TIC'
        else if (condition((ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("TIC"))))
        {
            decideConditionsMet1081++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIC
            sub_Process_Tic();
            if (condition(Global.isEscape())) {return;}
            if (condition(! (pnd_Ws_Pnd_Tic_Update_Ind.getBoolean())))                                                                                                    //Natural: IF NOT #TIC-UPDATE-IND
            {
                pnd_Work_Rec_Pnd_Msg_Idx.setValue(11);                                                                                                                    //Natural: ASSIGN #MSG-IDX := 11
                                                                                                                                                                          //Natural: PERFORM IVC-CHANGE-REPORT
                sub_Ivc_Change_Report();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Ws_Pnd_Tic_Update_Ind.getBoolean() || pnd_Ws_Pnd_Ivc_Update_Ind.getBoolean()))                                                                  //Natural: IF #TIC-UPDATE-IND OR #IVC-UPDATE-IND
        {
            ldaTwrl9425.getPayu_Twrpymnt_Updt_Dte().setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                      //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO PAYU.TWRPYMNT-UPDT-DTE
            ldaTwrl9425.getPayu_Twrpymnt_Updt_Time().setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                      //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO PAYU.TWRPYMNT-UPDT-TIME
            ldaTwrl9425.getPayu_Twrpymnt_Lu_Ts().setValue(Global.getTIMX());                                                                                              //Natural: ASSIGN PAYU.TWRPYMNT-LU-TS := *TIMX
            ldaTwrl9425.getVw_payu().updateDBRow("G_U");                                                                                                                  //Natural: UPDATE ( G-U. )
            pnd_Ws_Pnd_Ivc_Protect_Set_Ind.setValue(false);                                                                                                               //Natural: ASSIGN #IVC-PROTECT-SET-IND := FALSE
            pnd_Work_Rec_Pnd_Msg_Idx.setValue(21);                                                                                                                        //Natural: ASSIGN #MSG-IDX := 21
                                                                                                                                                                          //Natural: PERFORM IVC-CHANGE-REPORT
            sub_Ivc_Change_Report();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Work_Rec_Pnd_Msg_Idx.setValue(6);                                                                                                                         //Natural: ASSIGN #MSG-IDX := 6
                                                                                                                                                                          //Natural: PERFORM IVC-CHANGE-REPORT
            sub_Ivc_Change_Report();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ET-PROCESSING
        sub_Et_Processing();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Et_Processing() throws Exception                                                                                                                     //Natural: ET-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        if (condition(pnd_Ws_Pnd_Et_Cnt.greater(pnd_Ws_Pnd_Et_Limit)))                                                                                                    //Natural: IF #ET-CNT > #ET-LIMIT
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Ws_Pnd_Et_Cnt.setValue(0);                                                                                                                                //Natural: ASSIGN #ET-CNT := 0
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Tic() throws Exception                                                                                                                       //Natural: PROCESS-TIC
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        if (condition(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_First_Pymnt_Dte().equals(getZero())))                                                                             //Natural: IF #I-FIRST-PYMNT-DTE = 0
        {
            pnd_Ws_Pnd_F_P_Date.reset();                                                                                                                                  //Natural: RESET #F-P-DATE #TOT-IVC
            pnd_Ws_Pnd_Tot_Ivc.reset();
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Date_N.compute(new ComputeParameters(false, pnd_Ws_Pnd_Date_N), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_First_Pymnt_Dte().multiply(100).add(1));         //Natural: ASSIGN #DATE-N := #I-FIRST-PYMNT-DTE * 100 + 1
            if (condition(pnd_Ws_Pnd_Yyyy_N.equals(ldaTwrl9425.getPayu_Twrpymnt_Tax_Year())))                                                                             //Natural: IF #YYYY-N = PAYU.TWRPYMNT-TAX-YEAR
            {
                pnd_Ws_Pnd_F_P_Date.setValue(pnd_Ws_Pnd_Date_X);                                                                                                          //Natural: ASSIGN #F-P-DATE := #DATE-X
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_F_P_Date.reset();                                                                                                                              //Natural: RESET #F-P-DATE #TOT-IVC
                pnd_Ws_Pnd_Tot_Ivc.reset();
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("IVC") && pnd_Ws_Pnd_Tot_Ivc.equals(new DbsDecimal("0.00"))))                                  //Natural: IF #I-MAINT-CDE = 'IVC' AND #TOT-IVC = 0.00
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9425.getPayu_Twrpymnt_Tot_Contractual_Ivc().equals(pnd_Ws_Pnd_Tot_Ivc) && ldaTwrl9425.getPayu_Twrpymnt_Contract_Issue_Dte().equals(pnd_Ws_Pnd_F_P_Date))) //Natural: IF PAYU.TWRPYMNT-TOT-CONTRACTUAL-IVC = #TOT-IVC AND PAYU.TWRPYMNT-CONTRACT-ISSUE-DTE = #F-P-DATE
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Tic_Update_Ind.setValue(true);                                                                                                                     //Natural: ASSIGN #TIC-UPDATE-IND := TRUE
            ldaTwrl9425.getPayu_Twrpymnt_Tot_Contractual_Ivc().setValue(pnd_Ws_Pnd_Tot_Ivc);                                                                              //Natural: ASSIGN PAYU.TWRPYMNT-TOT-CONTRACTUAL-IVC := #TOT-IVC
            ldaTwrl9425.getPayu_Twrpymnt_Contract_Issue_Dte().setValue(pnd_Ws_Pnd_F_P_Date);                                                                              //Natural: ASSIGN PAYU.TWRPYMNT-CONTRACT-ISSUE-DTE := #F-P-DATE
            pnd_Work_Rec_Pnd_Msg_Idx.setValue(7);                                                                                                                         //Natural: ASSIGN #MSG-IDX := 7
                                                                                                                                                                          //Natural: PERFORM IVC-CHANGE-REPORT
            sub_Ivc_Change_Report();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Ivc() throws Exception                                                                                                                       //Natural: PROCESS-IVC
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        //*  FO 5/15/00
        //*  FO 5/15/00
        //*  FO 5/15/00
        //*  FO 5/15/00
        //*  FO 5/15/00
        pnd_Ws_Pnd_Calc_Ivc.resetInitial();                                                                                                                               //Natural: RESET INITIAL #CALC-IVC #KNOWN-IVC #1001-IND #1001-MIX #1078-IND #1078-MIX #CALC-GROSS
        pnd_Ws_Pnd_Known_Ivc.resetInitial();
        pnd_Ws_Pnd_1001_Ind.resetInitial();
        pnd_Ws_Pnd_1001_Mix.resetInitial();
        pnd_Ws_Pnd_1078_Ind.resetInitial();
        pnd_Ws_Pnd_1078_Mix.resetInitial();
        pnd_Ws_Pnd_Calc_Gross.resetInitial();
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO PAYU.C*TWRPYMNT-PAYMENTS
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl9425.getPayu_Count_Casttwrpymnt_Payments())); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition((((ldaTwrl9425.getPayu_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals(" ") || ldaTwrl9425.getPayu_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals("C"))  //Natural: IF PAYU.TWRPYMNT-PYMNT-STATUS ( #I ) = ' ' OR = 'C' AND ( PAYU.TWRPYMNT-PYMNT-DTE ( #I ) LT #PYMNT-DTE OR PAYU.TWRPYMNT-PYMNT-DTE ( #I ) = #PYMNT-DTE AND PAYU.TWRPYMNT-UPDTE-SRCE-CDE ( #I ) = 'TM' ) AND NOT ( PAYU.TWRPYMNT-GROSS-AMT ( #I ) = 0.00 AND PAYU.TWRPYMNT-INT-AMT ( #I ) NE 0.00 )
                && (ldaTwrl9425.getPayu_Twrpymnt_Pymnt_Dte().getValue(pnd_Ws_Pnd_I).less(pnd_Ws_Pnd_Pymnt_Dte) || (ldaTwrl9425.getPayu_Twrpymnt_Pymnt_Dte().getValue(pnd_Ws_Pnd_I).equals(pnd_Ws_Pnd_Pymnt_Dte) 
                && ldaTwrl9425.getPayu_Twrpymnt_Updte_Srce_Cde().getValue(pnd_Ws_Pnd_I).equals("TM")))) && ! ((ldaTwrl9425.getPayu_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I).equals(new 
                DbsDecimal("0.00")) && ldaTwrl9425.getPayu_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00")))))))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (ldaTwrl9425.getPayu_Twrpymnt_Ivc_Protect().getValue(pnd_Ws_Pnd_I).equals(true)) && ldaTwrl9425.getPayu_Twrpymnt_Ivc_Ind().getValue(pnd_Ws_Pnd_I).notEquals("K"))) //Natural: IF NOT PAYU.TWRPYMNT-IVC-PROTECT ( #I ) AND PAYU.TWRPYMNT-IVC-IND ( #I ) NE 'K'
            {
                pnd_Ws_Pnd_Known_Ivc.setValue(false);                                                                                                                     //Natural: ASSIGN #KNOWN-IVC := FALSE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Calc_Ivc.nadd(ldaTwrl9425.getPayu_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                                                                      //Natural: ADD PAYU.TWRPYMNT-IVC-AMT ( #I ) TO #CALC-IVC
            //*  FO 5/15/00
            pnd_Ws_Pnd_Calc_Gross.nadd(ldaTwrl9425.getPayu_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                                                  //Natural: ADD PAYU.TWRPYMNT-GROSS-AMT ( #I ) TO #CALC-GROSS
            //*  FO 5/15/00
            if (condition(! (pnd_Ws_Pnd_1001_Mix.getBoolean())))                                                                                                          //Natural: IF NOT #1001-MIX
            {
                //*  FIRST TIME
                if (condition(pnd_Ws_Pnd_1001_Ind.equals(" ")))                                                                                                           //Natural: IF #1001-IND = ' '
                {
                    pnd_Ws_Pnd_1001_Ind.setValue(ldaTwrl9425.getPayu_Twrpymnt_Frm1001_Ind().getValue(pnd_Ws_Pnd_I));                                                      //Natural: ASSIGN #1001-IND := PAYU.TWRPYMNT-FRM1001-IND ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Ws_Pnd_1001_Ind.notEquals(ldaTwrl9425.getPayu_Twrpymnt_Frm1001_Ind().getValue(pnd_Ws_Pnd_I))))                                      //Natural: IF #1001-IND NE PAYU.TWRPYMNT-FRM1001-IND ( #I )
                    {
                        pnd_Ws_Pnd_1001_Mix.setValue(true);                                                                                                               //Natural: ASSIGN #1001-MIX := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  FO 5/15/00
            if (condition(! (pnd_Ws_Pnd_1078_Mix.getBoolean())))                                                                                                          //Natural: IF NOT #1078-MIX
            {
                //*  FIRST TIME
                if (condition(pnd_Ws_Pnd_1078_Ind.equals(" ")))                                                                                                           //Natural: IF #1078-IND = ' '
                {
                    pnd_Ws_Pnd_1078_Ind.setValue(ldaTwrl9425.getPayu_Twrpymnt_Frm1078_Ind().getValue(pnd_Ws_Pnd_I));                                                      //Natural: ASSIGN #1078-IND := PAYU.TWRPYMNT-FRM1078-IND ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Ws_Pnd_1078_Ind.notEquals(ldaTwrl9425.getPayu_Twrpymnt_Frm1078_Ind().getValue(pnd_Ws_Pnd_I))))                                      //Natural: IF #1078-IND NE PAYU.TWRPYMNT-FRM1078-IND ( #I )
                    {
                        pnd_Ws_Pnd_1078_Mix.setValue(true);                                                                                                               //Natural: ASSIGN #1078-MIX := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_Known_Ivc.getBoolean() && ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt().equals(pnd_Ws_Pnd_Calc_Ivc)))                                  //Natural: IF #KNOWN-IVC AND #I-YTD-IVC-AMT = #CALC-IVC
        {
            if (condition(! (pnd_Ws_Pnd_Tic_Update_Ind.getBoolean())))                                                                                                    //Natural: IF NOT #TIC-UPDATE-IND
            {
                pnd_Work_Rec_Pnd_Msg_Idx.setValue(11);                                                                                                                    //Natural: ASSIGN #MSG-IDX := 11
                                                                                                                                                                          //Natural: PERFORM IVC-CHANGE-REPORT
                sub_Ivc_Change_Report();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Rec_Pnd_Msg_Idx.setValue(8);                                                                                                                         //Natural: ASSIGN #MSG-IDX := 8
                                                                                                                                                                          //Natural: PERFORM IVC-CHANGE-REPORT
            sub_Ivc_Change_Report();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  FO 5/15/00
        if (condition(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt().greater(pnd_Ws_Pnd_Calc_Gross)))                                                                    //Natural: IF #I-YTD-IVC-AMT GT #CALC-GROSS
        {
            pnd_Work_Rec_Pnd_Msg_Idx.setValue(26);                                                                                                                        //Natural: ASSIGN #MSG-IDX := 26
                                                                                                                                                                          //Natural: PERFORM IVC-CHANGE-REPORT
            sub_Ivc_Change_Report();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  FO 5/15/00
        short decideConditionsMet1205 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #1001-MIX
        if (condition(pnd_Ws_Pnd_1001_Mix.getBoolean()))
        {
            decideConditionsMet1205++;
            pnd_Work_Rec_Pnd_Msg_Idx.setValue(24);                                                                                                                        //Natural: ASSIGN #MSG-IDX := 24
                                                                                                                                                                          //Natural: PERFORM IVC-CHANGE-REPORT
            sub_Ivc_Change_Report();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #1078-MIX
        if (condition(pnd_Ws_Pnd_1078_Mix.getBoolean()))
        {
            decideConditionsMet1205++;
            pnd_Work_Rec_Pnd_Msg_Idx.setValue(25);                                                                                                                        //Natural: ASSIGN #MSG-IDX := 25
                                                                                                                                                                          //Natural: PERFORM IVC-CHANGE-REPORT
            sub_Ivc_Change_Report();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet1205 > 0))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet1205 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Save_Rec_Pnd_Multi_Residency.getBoolean()))                                                                                                     //Natural: IF #SAVE-REC.#MULTI-RESIDENCY
        {
            short decideConditionsMet1219 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN NOT #KNOWN-IVC AND #I-YTD-IVC-AMT = 0.00
            if (condition(! ((pnd_Ws_Pnd_Known_Ivc.getBoolean()) && ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt().equals(new DbsDecimal("0.00")))))
            {
                decideConditionsMet1219++;
                pnd_Work_Rec_Pnd_Msg_Idx.setValue(12);                                                                                                                    //Natural: ASSIGN #MSG-IDX := 12
            }                                                                                                                                                             //Natural: WHEN NOT #KNOWN-IVC AND #I-YTD-IVC-AMT NE 0.00
            else if (condition(! ((pnd_Ws_Pnd_Known_Ivc.getBoolean()) && ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt().notEquals(new DbsDecimal("0.00")))))
            {
                decideConditionsMet1219++;
                pnd_Ws_Pnd_Calc_Ivc.setValue(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt());                                                                            //Natural: ASSIGN #CALC-IVC := #I-YTD-IVC-AMT
                pnd_Work_Rec_Pnd_Msg_Idx.setValue(18);                                                                                                                    //Natural: ASSIGN #MSG-IDX := 18
            }                                                                                                                                                             //Natural: WHEN #CALC-IVC = 0.00 AND #I-YTD-IVC-AMT NE 0.00
            else if (condition(pnd_Ws_Pnd_Calc_Ivc.equals(new DbsDecimal("0.00")) && ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt().notEquals(new DbsDecimal("0.00"))))
            {
                decideConditionsMet1219++;
                pnd_Work_Rec_Pnd_Msg_Idx.setValue(19);                                                                                                                    //Natural: ASSIGN #MSG-IDX := 19
                                                                                                                                                                          //Natural: PERFORM IVC-CHANGE-REPORT
                sub_Ivc_Change_Report();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: WHEN #CALC-IVC NE 0.00 AND #I-YTD-IVC-AMT = 0.00
            else if (condition(pnd_Ws_Pnd_Calc_Ivc.notEquals(new DbsDecimal("0.00")) && ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt().equals(new DbsDecimal("0.00"))))
            {
                decideConditionsMet1219++;
                pnd_Work_Rec_Pnd_Msg_Idx.setValue(15);                                                                                                                    //Natural: ASSIGN #MSG-IDX := 15
            }                                                                                                                                                             //Natural: WHEN #CALC-IVC NE 0.00 AND #I-YTD-IVC-AMT NE 0.00
            else if (condition(pnd_Ws_Pnd_Calc_Ivc.notEquals(new DbsDecimal("0.00")) && ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt().notEquals(new 
                DbsDecimal("0.00"))))
            {
                decideConditionsMet1219++;
                pnd_Work_Rec_Pnd_Msg_Idx.setValue(20);                                                                                                                    //Natural: ASSIGN #MSG-IDX := 20
                                                                                                                                                                          //Natural: PERFORM IVC-CHANGE-REPORT
                sub_Ivc_Change_Report();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Work_Rec_Pnd_Msg_Idx.reset();                                                                                                                         //Natural: RESET #MSG-IDX
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            short decideConditionsMet1239 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN NOT #KNOWN-IVC AND #I-YTD-IVC-AMT = 0.00
            if (condition(! ((pnd_Ws_Pnd_Known_Ivc.getBoolean()) && ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt().equals(new DbsDecimal("0.00")))))
            {
                decideConditionsMet1239++;
                pnd_Work_Rec_Pnd_Msg_Idx.setValue(12);                                                                                                                    //Natural: ASSIGN #MSG-IDX := 12
            }                                                                                                                                                             //Natural: WHEN NOT #KNOWN-IVC AND #I-YTD-IVC-AMT NE 0.00
            else if (condition(! ((pnd_Ws_Pnd_Known_Ivc.getBoolean()) && ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt().notEquals(new DbsDecimal("0.00")))))
            {
                decideConditionsMet1239++;
                pnd_Work_Rec_Pnd_Msg_Idx.setValue(13);                                                                                                                    //Natural: ASSIGN #MSG-IDX := 13
            }                                                                                                                                                             //Natural: WHEN #CALC-IVC = 0.00 AND #I-YTD-IVC-AMT NE 0.00
            else if (condition(pnd_Ws_Pnd_Calc_Ivc.equals(new DbsDecimal("0.00")) && ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt().notEquals(new DbsDecimal("0.00"))))
            {
                decideConditionsMet1239++;
                pnd_Work_Rec_Pnd_Msg_Idx.setValue(14);                                                                                                                    //Natural: ASSIGN #MSG-IDX := 14
            }                                                                                                                                                             //Natural: WHEN #CALC-IVC NE 0.00 AND #I-YTD-IVC-AMT = 0.00
            else if (condition(pnd_Ws_Pnd_Calc_Ivc.notEquals(new DbsDecimal("0.00")) && ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt().equals(new DbsDecimal("0.00"))))
            {
                decideConditionsMet1239++;
                pnd_Work_Rec_Pnd_Msg_Idx.setValue(15);                                                                                                                    //Natural: ASSIGN #MSG-IDX := 15
            }                                                                                                                                                             //Natural: WHEN #CALC-IVC NE 0.00 AND #I-YTD-IVC-AMT NE 0.00
            else if (condition(pnd_Ws_Pnd_Calc_Ivc.notEquals(new DbsDecimal("0.00")) && ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt().notEquals(new 
                DbsDecimal("0.00"))))
            {
                decideConditionsMet1239++;
                pnd_Work_Rec_Pnd_Msg_Idx.setValue(16);                                                                                                                    //Natural: ASSIGN #MSG-IDX := 16
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Work_Rec_Pnd_Msg_Idx.reset();                                                                                                                         //Natural: RESET #MSG-IDX
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Adj_Ivc.compute(new ComputeParameters(false, pnd_Ws_Pnd_Adj_Ivc), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt().subtract(pnd_Ws_Pnd_Calc_Ivc));      //Natural: ASSIGN #ADJ-IVC := #I-YTD-IVC-AMT - #CALC-IVC
        pnd_Ws_Pnd_Ivc_Update_Ind.setValue(true);                                                                                                                         //Natural: ASSIGN #IVC-UPDATE-IND := TRUE
                                                                                                                                                                          //Natural: PERFORM IVC-CHANGE-REPORT
        sub_Ivc_Change_Report();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Top.compute(new ComputeParameters(false, pnd_Ws_Pnd_Top), ldaTwrl9425.getPayu_Count_Casttwrpymnt_Payments().add(1));                                   //Natural: ASSIGN #TOP := PAYU.C*TWRPYMNT-PAYMENTS + 1
        ldaTwrl9425.getPayu_Twrpymnt_Sum_Ivc_Amt().reset();                                                                                                               //Natural: RESET PAYU.TWRPYMNT-SUM-IVC-AMT
        pnd_Ws_Pnd_Known_Ivc.resetInitial();                                                                                                                              //Natural: RESET INITIAL #KNOWN-IVC
        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 TO PAYU.C*TWRPYMNT-PAYMENTS
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl9425.getPayu_Count_Casttwrpymnt_Payments())); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(((ldaTwrl9425.getPayu_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals(" ") || ldaTwrl9425.getPayu_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals("C"))  //Natural: IF PAYU.TWRPYMNT-PYMNT-STATUS ( #I ) = ' ' OR = 'C' AND NOT ( PAYU.TWRPYMNT-GROSS-AMT ( #I ) = 0.00 AND PAYU.TWRPYMNT-INT-AMT ( #I ) NE 0.00 )
                && ! ((ldaTwrl9425.getPayu_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I).equals(new DbsDecimal("0.00")) && ldaTwrl9425.getPayu_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new 
                DbsDecimal("0.00")))))))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl9425.getPayu_Twrpymnt_Sum_Ivc_Amt().nadd(ldaTwrl9425.getPayu_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                                               //Natural: ADD PAYU.TWRPYMNT-IVC-AMT ( #I ) TO PAYU.TWRPYMNT-SUM-IVC-AMT
            if (condition(ldaTwrl9425.getPayu_Twrpymnt_Pymnt_Dte().getValue(pnd_Ws_Pnd_I).less(pnd_Ws_Pnd_Pymnt_Dte)))                                                    //Natural: IF PAYU.TWRPYMNT-PYMNT-DTE ( #I ) LT #PYMNT-DTE
            {
                ldaTwrl9425.getPayu_Twrpymnt_Ivc_Protect().getValue(pnd_Ws_Pnd_I).setValue(true);                                                                         //Natural: ASSIGN PAYU.TWRPYMNT-IVC-PROTECT ( #I ) := TRUE
                pnd_Ws_Pnd_Ivc_Protect_Set_Ind.setValue(true);                                                                                                            //Natural: ASSIGN #IVC-PROTECT-SET-IND := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl9425.getPayu_Twrpymnt_Ivc_Ind().getValue(pnd_Ws_Pnd_I).notEquals("K")))                                                              //Natural: IF PAYU.TWRPYMNT-IVC-IND ( #I ) NE 'K'
                {
                    pnd_Ws_Pnd_Known_Ivc.setValue(false);                                                                                                                 //Natural: ASSIGN #KNOWN-IVC := FALSE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_Known_Ivc.getBoolean()))                                                                                                                 //Natural: IF #KNOWN-IVC
        {
            ldaTwrl9425.getPayu_Twrpymnt_Ivc_Indicator().setValue("K");                                                                                                   //Natural: ASSIGN PAYU.TWRPYMNT-IVC-INDICATOR := 'K'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl9425.getPayu_Twrpymnt_Ivc_Indicator().setValue("M");                                                                                                   //Natural: ASSIGN PAYU.TWRPYMNT-IVC-INDICATOR := 'M'
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl9425.getPayu_Twrpymnt_Pymnt_Dte().getValue(pnd_Ws_Pnd_Top).setValue(pnd_Ws_Pnd_Pymnt_Dte);                                                                 //Natural: ASSIGN PAYU.TWRPYMNT-PYMNT-DTE ( #TOP ) := #PYMNT-DTE
        ldaTwrl9425.getPayu_Twrpymnt_Pymnt_Intfce_Dte().getValue(pnd_Ws_Pnd_Top).setValueEdited(pnd_Ws_Pnd_Today,new ReportEditMask("YYYYMMDD"));                         //Natural: MOVE EDITED #TODAY ( EM = YYYYMMDD ) TO PAYU.TWRPYMNT-PYMNT-INTFCE-DTE ( #TOP )
        ldaTwrl9425.getPayu_Twrpymnt_Updte_Srce_Cde().getValue(pnd_Ws_Pnd_Top).setValue("TM");                                                                            //Natural: ASSIGN PAYU.TWRPYMNT-UPDTE-SRCE-CDE ( #TOP ) := 'TM'
        ldaTwrl9425.getPayu_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_Ws_Pnd_Top).setValue(pnd_Input_Origin);                                                                 //Natural: ASSIGN PAYU.TWRPYMNT-ORGN-SRCE-CDE ( #TOP ) := #INPUT-ORIGIN
        ldaTwrl9425.getPayu_Twrpymnt_Dist_Method().getValue(pnd_Ws_Pnd_Top).setValue("G");                                                                                //Natural: ASSIGN PAYU.TWRPYMNT-DIST-METHOD ( #TOP ) := 'G'
        ldaTwrl9425.getPayu_Twrpymnt_Ivc_Ind().getValue(pnd_Ws_Pnd_Top).setValue("K");                                                                                    //Natural: ASSIGN PAYU.TWRPYMNT-IVC-IND ( #TOP ) := 'K'
        ldaTwrl9425.getPayu_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_Top).setValue(pnd_Ws_Pnd_Adj_Ivc);                                                                     //Natural: ASSIGN PAYU.TWRPYMNT-IVC-AMT ( #TOP ) := #ADJ-IVC
        //*  FO 5/15/00
        //*  FO 5/15/00
        ldaTwrl9425.getPayu_Twrpymnt_Sum_Ivc_Amt().nadd(ldaTwrl9425.getPayu_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_Top));                                                 //Natural: ADD PAYU.TWRPYMNT-IVC-AMT ( #TOP ) TO PAYU.TWRPYMNT-SUM-IVC-AMT
        ldaTwrl9425.getPayu_Twrpymnt_Frm1001_Ind().getValue(pnd_Ws_Pnd_Top).setValue(pnd_Ws_Pnd_1001_Ind);                                                                //Natural: ASSIGN PAYU.TWRPYMNT-FRM1001-IND ( #TOP ) := #1001-IND
        ldaTwrl9425.getPayu_Twrpymnt_Frm1078_Ind().getValue(pnd_Ws_Pnd_Top).setValue(pnd_Ws_Pnd_1078_Ind);                                                                //Natural: ASSIGN PAYU.TWRPYMNT-FRM1078-IND ( #TOP ) := #1078-IND
        ldaTwrl9425.getPayu_Twrpymnt_Dist_Updte_User().setValue(Global.getINIT_USER());                                                                                   //Natural: ASSIGN PAYU.TWRPYMNT-DIST-UPDTE-USER := PAYU.TWRPYMNT-UPDTE-USER ( #TOP ) := *INIT-USER
        ldaTwrl9425.getPayu_Twrpymnt_Updte_User().getValue(pnd_Ws_Pnd_Top).setValue(Global.getINIT_USER());
        ldaTwrl9425.getPayu_Twrpymnt_Sys_Dte_Time().getValue(pnd_Ws_Pnd_Top).setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                      //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO PAYU.TWRPYMNT-SYS-DTE-TIME ( #TOP )
        ldaTwrl9425.getPayu_Twrpymnt_Gtn_Ts().getValue(pnd_Ws_Pnd_Top).setValue(Global.getTIMX());                                                                        //Natural: ASSIGN PAYU.TWRPYMNT-GTN-TS ( #TOP ) := *TIMX
        ldaTwrl9425.getPayu_Twrpymnt_Dist_Updte_Dte().setValueEdited(pnd_Ws_Pnd_Today,new ReportEditMask("YYYYMMDD"));                                                    //Natural: MOVE EDITED #TODAY ( EM = YYYYMMDD ) TO PAYU.TWRPYMNT-DIST-UPDTE-DTE
        ldaTwrl9425.getPayu_Twrpymnt_Dist_Updte_Time().setValueEdited(pnd_Ws_Pnd_Today,new ReportEditMask("HHIISST"));                                                    //Natural: MOVE EDITED #TODAY ( EM = HHIISST ) TO PAYU.TWRPYMNT-DIST-UPDTE-TIME
    }
    private void sub_Process_Dob1() throws Exception                                                                                                                      //Natural: PROCESS-DOB1
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        if (condition(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Dob_F().equals(getZero())))                                                                                   //Natural: IF #I-NEW-DOB-F = 0
        {
            pnd_Ws_Pnd_Date_D.reset();                                                                                                                                    //Natural: RESET #DATE-D
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Date_N.setValue(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Dob_F());                                                                                    //Natural: ASSIGN #DATE-N := #I-NEW-DOB-F
            pnd_Ws_Pnd_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ws_Pnd_Date_X);                                                                           //Natural: MOVE EDITED #DATE-X TO #DATE-D ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Dob1_Cv.setValue("AD=D");                                                                                                                              //Natural: ASSIGN #DOB1-CV := ( AD = D )
        pnd_Ws_Pnd_Dob2_Cv.setValue("AD=N");                                                                                                                              //Natural: ASSIGN #DOB2-CV := ( AD = N )
                                                                                                                                                                          //Natural: PERFORM DOB-REPORT
        sub_Dob_Report();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Process_Dob2() throws Exception                                                                                                                      //Natural: PROCESS-DOB2
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        if (condition(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Dob_S().equals(getZero())))                                                                                   //Natural: IF #I-NEW-DOB-S = 0
        {
            pnd_Ws_Pnd_Date_D.reset();                                                                                                                                    //Natural: RESET #DATE-D
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Date_N.setValue(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Dob_S());                                                                                    //Natural: ASSIGN #DATE-N := #I-NEW-DOB-S
            pnd_Ws_Pnd_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ws_Pnd_Date_X);                                                                           //Natural: MOVE EDITED #DATE-X TO #DATE-D ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Dob1_Cv.setValue("AD=N");                                                                                                                              //Natural: ASSIGN #DOB1-CV := ( AD = N )
        pnd_Ws_Pnd_Dob2_Cv.setValue("AD=D");                                                                                                                              //Natural: ASSIGN #DOB2-CV := ( AD = D )
                                                                                                                                                                          //Natural: PERFORM DOB-REPORT
        sub_Dob_Report();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Process_Dod1() throws Exception                                                                                                                      //Natural: PROCESS-DOD1
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        if (condition(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Dod_F().equals(getZero())))                                                                                   //Natural: IF #I-NEW-DOD-F = 0
        {
            pnd_Ws_Pnd_Date_D.reset();                                                                                                                                    //Natural: RESET #DATE-D
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Date_N.compute(new ComputeParameters(false, pnd_Ws_Pnd_Date_N), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Dod_F().multiply(100).add(1));               //Natural: ASSIGN #DATE-N := #I-NEW-DOD-F * 100 + 1
            //*  V1.02: CHECK FOR VALID MONTH SPEC - TOM T.
            if (condition(pnd_Ws_Pnd_Mm_N.less(1) || pnd_Ws_Pnd_Mm_N.greater(12)))                                                                                        //Natural: IF #MM-N < 1 OR #MM-N > 12 THEN
            {
                pnd_Ws_Pnd_Invalid_Tran_Cnt.nadd(1);                                                                                                                      //Natural: ADD 1 TO #INVALID-TRAN-CNT
                                                                                                                                                                          //Natural: PERFORM INVALID-TRAN-REPORT
                sub_Invalid_Tran_Report();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ws_Pnd_Date_X);                                                                       //Natural: MOVE EDITED #DATE-X TO #DATE-D ( EM = YYYYMMDD )
                pnd_Ws_Pnd_Dod1_Cv.setValue("AD=D");                                                                                                                      //Natural: ASSIGN #DOD1-CV := ( AD = D )
                pnd_Ws_Pnd_Dod2_Cv.setValue("AD=N");                                                                                                                      //Natural: ASSIGN #DOD2-CV := ( AD = N )
                                                                                                                                                                          //Natural: PERFORM DOD-REPORT
                sub_Dod_Report();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Dod2() throws Exception                                                                                                                      //Natural: PROCESS-DOD2
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        if (condition(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Dod_S().equals(getZero())))                                                                                   //Natural: IF #I-NEW-DOD-S = 0
        {
            pnd_Ws_Pnd_Date_D.reset();                                                                                                                                    //Natural: RESET #DATE-D
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Date_N.compute(new ComputeParameters(false, pnd_Ws_Pnd_Date_N), ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Dod_S().multiply(100).add(1));               //Natural: ASSIGN #DATE-N := #I-NEW-DOD-S * 100 + 1
            //*  V1.02: CHECK FOR VALID MONTH SPEC - TOM T.
            if (condition(pnd_Ws_Pnd_Mm_N.less(1) || pnd_Ws_Pnd_Mm_N.greater(12)))                                                                                        //Natural: IF #MM-N < 1 OR #MM-N > 12 THEN
            {
                pnd_Ws_Pnd_Invalid_Tran_Cnt.nadd(1);                                                                                                                      //Natural: ADD 1 TO #INVALID-TRAN-CNT
                                                                                                                                                                          //Natural: PERFORM INVALID-TRAN-REPORT
                sub_Invalid_Tran_Report();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ws_Pnd_Date_X);                                                                       //Natural: MOVE EDITED #DATE-X TO #DATE-D ( EM = YYYYMMDD )
                pnd_Ws_Pnd_Dod1_Cv.setValue("AD=N");                                                                                                                      //Natural: ASSIGN #DOD1-CV := ( AD = N )
                pnd_Ws_Pnd_Dod2_Cv.setValue("AD=D");                                                                                                                      //Natural: ASSIGN #DOD2-CV := ( AD = D )
                                                                                                                                                                          //Natural: PERFORM DOD-REPORT
                sub_Dod_Report();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Invalid_Tran_Report() throws Exception                                                                                                               //Natural: INVALID-TRAN-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Tran/Type",                                                         //Natural: DISPLAY ( 01 ) ( HC = R ) '/Tran/Type' #I-MAINT-CDE '//Contract' #I-CNTRCT-NBR '//Payee' #I-PAYEE-CDE ( LC = �� ) '/Old Tax Id' #I-TAX-ID ( EM = 999-99-9999 ) / 'New Tax Id' #I-NEW-TAX-ID ( EM = 999-99-9999 ) '/Maint/Month' #I-PYMNT-MONTH ( LC = �� ) '/Residency' #I-NEW-RSDNCY-CDE ( LC = ����� ) / 'Citizenship' #I-NEW-CTZN-CDE ( LC = ����� ) '/   IVC code' #I-PER-IVC-CDE ( LC = ����� ) / 'RTB IVC Code' #I-RTB-IVC-CDE ( LC = ����� ) '/YTD IVC amt' #I-YTD-IVC-AMT ( EM = -Z,ZZZ,ZZ9.99 ) / 'Total contract IVC' #I-TOT-IVC-AMT ( EM = -Z,ZZZ,ZZ9.99 ) '/1st Annt DOB' #I-NEW-DOB-F / '2nd Annt DOB' #I-NEW-DOB-S '/1st Annt DOD' #I-NEW-DOD-F / '2nd Annt DOD' #I-NEW-DOD-S '1st/Payment/Date' #I-FIRST-PYMNT-DTE
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde(),"//Contract",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr(),"//Payee",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde(), new FieldAttributes("LC=��"),"/Old Tax Id",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tax_Id(), new ReportEditMask ("999-99-9999"),NEWLINE,"New Tax Id",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Tax_Id(), new ReportEditMask ("999-99-9999"),"/Maint/Month",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Pymnt_Month(), new FieldAttributes("LC=��"),"/Residency",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Rsdncy_Cde(), new FieldAttributes("LC=�����"),NEWLINE,"Citizenship",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Ctzn_Cde(), new FieldAttributes("LC=�����"),"/   IVC code",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Per_Ivc_Cde(), new FieldAttributes("LC=�����"),NEWLINE,"RTB IVC Code",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Rtb_Ivc_Cde(), new FieldAttributes("LC=�����"),"/YTD IVC amt",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt(), new ReportEditMask ("-Z,ZZZ,ZZ9.99"),NEWLINE,"Total contract IVC",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt(), new ReportEditMask ("-Z,ZZZ,ZZ9.99"),"/1st Annt DOB",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Dob_F(),NEWLINE,"2nd Annt DOB",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Dob_S(),"/1st Annt DOD",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Dod_F(),NEWLINE,"2nd Annt DOD",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Dod_S(),"1st/Payment/Date",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_First_Pymnt_Dte());
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
    }
    private void sub_Citizenship_Report() throws Exception                                                                                                                //Natural: CITIZENSHIP-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(2, new TabSetting(14),"Tran/Type",                                                                                                           //Natural: DISPLAY ( 02 ) 14T 'Tran/Type' #I-MAINT-CDE '/Contract' #I-CNTRCT-NBR '/Payee' #I-PAYEE-CDE ( LC = �� ) '/Tax Id' #I-TAX-ID ( EM = 999-99-9999 ) 'Maint/Month' #I-PYMNT-MONTH ( LC = � ) '/Citizenship' #I-NEW-CTZN-CDE ( LC = ����� )
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde(),"/Contract",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr(),"/Payee",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde(), new FieldAttributes("LC=��"),"/Tax Id",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tax_Id(), new ReportEditMask ("999-99-9999"),"Maint/Month",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Pymnt_Month(), new FieldAttributes("LC=�"),"/Citizenship",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Ctzn_Cde(), new FieldAttributes("LC=�����"));
        if (Global.isEscape()) return;
    }
    private void sub_Dob_Report() throws Exception                                                                                                                        //Natural: DOB-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(3, new TabSetting(7),"Tran/Type",                                                                                                            //Natural: DISPLAY ( 03 ) 7T 'Tran/Type' #I-MAINT-CDE '/Contract' #I-CNTRCT-NBR '/Payee' #I-PAYEE-CDE ( LC = �� ) '/Tax Id' #I-TAX-ID ( EM = 999-99-9999 ) 'Maint/Month' #I-PYMNT-MONTH ( LC = � ) '1st Annuitant/DOB' #DATE-D ( EM = �MM/DD/YYYY CV = #DOB1-CV ) '2nd Annuitant/DOB' #DATE-D ( EM = �MM/DD/YYYY CV = #DOB2-CV )
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde(),"/Contract",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr(),"/Payee",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde(), new FieldAttributes("LC=��"),"/Tax Id",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tax_Id(), new ReportEditMask ("999-99-9999"),"Maint/Month",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Pymnt_Month(), new FieldAttributes("LC=�"),"1st Annuitant/DOB",
        		pnd_Ws_Pnd_Date_D, new ReportEditMask (" MM/DD/YYYY"), pnd_Ws_Pnd_Dob1_Cv,"2nd Annuitant/DOB",
        		pnd_Ws_Pnd_Date_D, new ReportEditMask (" MM/DD/YYYY"), pnd_Ws_Pnd_Dob2_Cv);
        if (Global.isEscape()) return;
    }
    private void sub_Dod_Report() throws Exception                                                                                                                        //Natural: DOD-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(4, new TabSetting(7),"Tran/Type",                                                                                                            //Natural: DISPLAY ( 04 ) 7T 'Tran/Type' #I-MAINT-CDE '/Contract' #I-CNTRCT-NBR '/Payee' #I-PAYEE-CDE ( LC = �� ) '/Tax Id' #I-TAX-ID ( EM = 999-99-9999 ) 'Maint/Month' #I-PYMNT-MONTH ( LC = � ) '1st Annuitant/DOD' #DATE-D ( EM = ���MM/YYYY CV = #DOD1-CV ) '2ND Annuitant/DOD' #DATE-D ( EM = ���MM/YYYY CV = #DOD2-CV )
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde(),"/Contract",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr(),"/Payee",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde(), new FieldAttributes("LC=��"),"/Tax Id",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tax_Id(), new ReportEditMask ("999-99-9999"),"Maint/Month",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Pymnt_Month(), new FieldAttributes("LC=�"),"1st Annuitant/DOD",
        		pnd_Ws_Pnd_Date_D, new ReportEditMask ("   MM/YYYY"), pnd_Ws_Pnd_Dod1_Cv,"2ND Annuitant/DOD",
        		pnd_Ws_Pnd_Date_D, new ReportEditMask ("   MM/YYYY"), pnd_Ws_Pnd_Dod2_Cv);
        if (Global.isEscape()) return;
    }
    private void sub_Ssn_Report() throws Exception                                                                                                                        //Natural: SSN-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(5, new TabSetting(14),"Tran/Type",                                                                                                           //Natural: DISPLAY ( 05 ) 14T 'Tran/Type' #I-MAINT-CDE '/Contract' #I-CNTRCT-NBR '/Payee' #I-PAYEE-CDE ( LC = �� ) 'Old/Tax Id' #I-TAX-ID ( EM = 999-99-9999 ) 'New/Tax Id' #I-NEW-TAX-ID ( EM = 999-99-9999 ) 'Maint/Month' #I-PYMNT-MONTH ( LC = � )
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde(),"/Contract",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr(),"/Payee",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde(), new FieldAttributes("LC=��"),"Old/Tax Id",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tax_Id(), new ReportEditMask ("999-99-9999"),"New/Tax Id",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Tax_Id(), new ReportEditMask ("999-99-9999"),"Maint/Month",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Pymnt_Month(), new FieldAttributes("LC=�"));
        if (Global.isEscape()) return;
    }
    private void sub_Twr_Error_Report() throws Exception                                                                                                                  //Natural: TWR-ERROR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        short decideConditionsMet1377 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #I-MAINT-CDE;//Natural: VALUE 'IVC'
        if (condition((ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("IVC"))))
        {
            decideConditionsMet1377++;
            pnd_Ws_Pnd_Ivc_Tran_Rejected.nadd(1);                                                                                                                         //Natural: ADD 1 TO #IVC-TRAN-REJECTED
            pnd_Ws_Pnd_Ivc_Ytd_Amt_R.nadd(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt());                                                                               //Natural: ADD #I-YTD-IVC-AMT TO #IVC-YTD-AMT-R
            pnd_Ws_Pnd_Ivc_Tot_Amt_R.nadd(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt());                                                                               //Natural: ADD #I-TOT-IVC-AMT TO #IVC-TOT-AMT-R
        }                                                                                                                                                                 //Natural: VALUE 'RIC'
        else if (condition((ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("RIC"))))
        {
            decideConditionsMet1377++;
            pnd_Ws_Pnd_Rtb_Ivc_Reject.nadd(1);                                                                                                                            //Natural: ADD 1 TO #RTB-IVC-REJECT
            pnd_Ws_Pnd_Rtb_Ytd_Amt_R.nadd(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt());                                                                               //Natural: ADD #I-YTD-IVC-AMT TO #RTB-YTD-AMT-R
            pnd_Ws_Pnd_Rtb_Tot_Amt_R.nadd(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt());                                                                               //Natural: ADD #I-TOT-IVC-AMT TO #RTB-TOT-AMT-R
        }                                                                                                                                                                 //Natural: VALUE 'TIC'
        else if (condition((ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde().equals("TIC"))))
        {
            decideConditionsMet1377++;
            pnd_Ws_Pnd_Tic_Ivc_Reject.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TIC-IVC-REJECT
            pnd_Ws_Pnd_Tic_Ytd_Amt_R.nadd(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt());                                                                               //Natural: ADD #I-YTD-IVC-AMT TO #TIC-YTD-AMT-R
            pnd_Ws_Pnd_Tic_Tot_Amt_R.nadd(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt());                                                                               //Natural: ADD #I-TOT-IVC-AMT TO #TIC-TOT-AMT-R
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        getReports().display(6, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Tran/Type",                                                         //Natural: DISPLAY ( 06 ) ( HC = R ) '/Tran/Type' #I-MAINT-CDE '//Contract' #I-CNTRCT-NBR '//Payee' #I-PAYEE-CDE ( LC = �� ) '//Tax ID' #I-TAX-ID ( EM = 999-99-9999 ) '/Maint/Month' #I-PYMNT-MONTH ( LC = ��� ) 'Citi/zen/ship' #I-CITIZENSHIP '//Residency' #I-NEW-RSDNCY-CDE ( LC = ����� ) '/Tran/Code' #I-TRANS-CDE ( LC = � ) '/   IVC Cde' #I-PER-IVC-CDE ( LC = ����� ) / 'RTB IVC Cde' #I-RTB-IVC-CDE ( LC = ����� ) '/YTD IVC Amt' #I-YTD-IVC-AMT ( EM = -Z,ZZZ,ZZ9.99 CV = #IVC-CV ) / 'Total Contract IVC' #I-TOT-IVC-AMT ( EM = -Z,ZZZ,ZZ9.99 ) '1st/Payment/Date' #I-FIRST-PYMNT-DTE ( EM = 9999/99 ) '/' #MSG ( #MSG-IDX )
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde(),"//Contract",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr(),"//Payee",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde(), new FieldAttributes("LC=��"),"//Tax ID",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tax_Id(), new ReportEditMask ("999-99-9999"),"/Maint/Month",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Pymnt_Month(), new FieldAttributes("LC=���"),"Citi/zen/ship",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Citizenship(),"//Residency",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Rsdncy_Cde(), new FieldAttributes("LC=�����"),"/Tran/Code",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Trans_Cde(), new FieldAttributes("LC=�"),"/   IVC Cde",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Per_Ivc_Cde(), new FieldAttributes("LC=�����"),NEWLINE,"RTB IVC Cde",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Rtb_Ivc_Cde(), new FieldAttributes("LC=�����"),"/YTD IVC Amt",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt(), new ReportEditMask ("-Z,ZZZ,ZZ9.99"), pnd_Ws_Pnd_Ivc_Cv,NEWLINE,"Total Contract IVC",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt(), new ReportEditMask ("-Z,ZZZ,ZZ9.99"),"1st/Payment/Date",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_First_Pymnt_Dte(), new ReportEditMask ("9999/99"),"/",
        		pnd_Ws_Pnd_Msg.getValue(pnd_Work_Rec_Pnd_Msg_Idx.getInt() + 1));
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Rpt6_Ytd_Ivc_Tot.nadd(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt());                                                                                //Natural: ADD #I-YTD-IVC-AMT TO #RPT6-YTD-IVC-TOT
        pnd_Ws_Pnd_Rpt6_Tot_Ivc_Tot.nadd(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt());                                                                                //Natural: ADD #I-TOT-IVC-AMT TO #RPT6-TOT-IVC-TOT
    }
    private void sub_Residency_Change_Report() throws Exception                                                                                                           //Natural: RESIDENCY-CHANGE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        getReports().display(7, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new TabSetting(14),"/Tran/Type",                                      //Natural: DISPLAY ( 07 ) ( HC = R ) 14T '/Tran/Type' #I-MAINT-CDE '//Contract' #I-CNTRCT-NBR '//Payee' #I-PAYEE-CDE ( LC = �� ) '//Tax Id' #I-TAX-ID ( EM = 999-99-9999 ) '/Maint/Month' #I-PYMNT-MONTH ( LC = �� ) '//Residency' #I-NEW-RSDNCY-CDE ( LC = ����� )
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde(),"//Contract",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr(),"//Payee",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde(), new FieldAttributes("LC=��"),"//Tax Id",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tax_Id(), new ReportEditMask ("999-99-9999"),"/Maint/Month",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Pymnt_Month(), new FieldAttributes("LC=��"),"//Residency",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Rsdncy_Cde(), new FieldAttributes("LC=�����"));
        if (Global.isEscape()) return;
    }
    private void sub_Ivc_Change_Report() throws Exception                                                                                                                 //Natural: IVC-CHANGE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        //*  11-17-99 TT
        //*  NOTE: MSGS 7 AND 12-17 ARE NO LONGER PRINTED ON THE IVC-CHANGE
        //*        REPORT
        //*        FOR MSG 21 "case updated" ADD "eyecatcher" ARROW ON RIGHT SIDE
        //* **********************************
        if (condition(pnd_Work_Rec_Pnd_Msg_Idx.equals(21)))                                                                                                               //Natural: IF #MSG-IDX = 21 THEN
        {
            pnd_Ws_Pnd_Eyecatcher.setValue("<--");                                                                                                                        //Natural: ASSIGN #EYECATCHER := '<--'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Eyecatcher.setValue(" ");                                                                                                                          //Natural: ASSIGN #EYECATCHER := ' '
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Work_Rec_Pnd_Msg_Idx.notEquals(7) && (pnd_Work_Rec_Pnd_Msg_Idx.less(12) || pnd_Work_Rec_Pnd_Msg_Idx.greater(17))))                              //Natural: IF #MSG-IDX <> 7 AND ( #MSG-IDX < 12 OR #MSG-IDX > 17 )
        {
            getReports().display(8, new IdenticalSuppress(true),new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), //Natural: DISPLAY ( 08 ) ( IS = ON ES = ON HC = R ) '/Tran/Type' #I-MAINT-CDE '//Contract' #I-CNTRCT-NBR '//Payee' #I-PAYEE-CDE ( EM = ��XX ) '//Tax ID' #I-TAX-ID ( EM = 999-99-9999 ) '/Maint/Month' #I-PYMNT-MONTH ( LC = �� ) '/IVC/Cde' #I-PER-IVC-CDE ( LC = � ) 'RTB/IVC/Cde' #I-RTB-IVC-CDE ( LC = � ) '/Tran/Code' #I-TRANS-CDE ( LC = � ) '//YTD IVC Amt' #I-YTD-IVC-AMT ( EM = -Z,ZZZ,ZZ9.99 CV = #IVC-CV ) 'Total/Contract/IVC' #I-TOT-IVC-AMT ( EM = -Z,ZZZ,ZZ9.99 ) '1st/Payment/Date' #I-FIRST-PYMNT-DTE ( EM = 9999/99 ) '/' #MSG ( #MSG-IDX ) ( IS = OFF ) '/' #EYECATCHER ( IS = OFF )
                "/Tran/Type",
            		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde(),"//Contract",
            		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr(),"//Payee",
            		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde(), new ReportEditMask ("  XX"),"//Tax ID",
            		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tax_Id(), new ReportEditMask ("999-99-9999"),"/Maint/Month",
            		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Pymnt_Month(), new FieldAttributes("LC=��"),"/IVC/Cde",
            		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Per_Ivc_Cde(), new FieldAttributes("LC=�"),"RTB/IVC/Cde",
            		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Rtb_Ivc_Cde(), new FieldAttributes("LC=�"),"/Tran/Code",
            		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Trans_Cde(), new FieldAttributes("LC=�"),"//YTD IVC Amt",
            		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt(), new ReportEditMask ("-Z,ZZZ,ZZ9.99"), pnd_Ws_Pnd_Ivc_Cv,"Total/Contract/IVC",
            		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt(), new ReportEditMask ("-Z,ZZZ,ZZ9.99"),"1st/Payment/Date",
            		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_First_Pymnt_Dte(), new ReportEditMask ("9999/99"),"/",
            		pnd_Ws_Pnd_Msg.getValue(pnd_Work_Rec_Pnd_Msg_Idx.getInt() + 1), new IdenticalSuppress(false),"/",
            		pnd_Ws_Pnd_Eyecatcher, new IdenticalSuppress(false));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Rpt8_Ytd_Ivc_Tot.nadd(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt());                                                                                //Natural: ADD #I-YTD-IVC-AMT TO #RPT8-YTD-IVC-TOT
        pnd_Ws_Pnd_Rpt8_Tot_Ivc_Tot.nadd(ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt());                                                                                //Natural: ADD #I-TOT-IVC-AMT TO #RPT8-TOT-IVC-TOT
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().skip(1, 3);                                                                                                                                          //Natural: SKIP ( 01 ) 3 LINES
        getReports().write(1, NEWLINE,new TabSetting(7),"Transaction records read...........:",new ColumnSpacing(3),pnd_Ws_Pnd_Tran_Cnt, new ReportEditMask               //Natural: WRITE ( 01 ) / 7T 'Transaction records read...........:' 3X #TRAN-CNT / 7T 'IVC transactions read..............:' 3X #IVC-TRAN-CNT / 7T '"RTB IVC" transactions read........:' 3X #RIC-TRAN-CNT / 7T '"Total IVC" transactions read......:' 3X #TIC-TRAN-CNT / 7T 'Residency transactions read........:' 3X #RES-TRAN-CNT / 7T 'Citizenship transactions read......:' 3X #CIT-TRAN-CNT / 7T 'Tax Id transactions read...........:' 3X #SSN-TRAN-CNT / 7T '1st Annuitant DOB transactions read:' 3X #DOB1-TRAN-CNT / 7T '2nd Annuitant DOB transactions read:' 3X #DOB2-TRAN-CNT / 7T '1st Annuitant DOD transactions read:' 3X #DOD1-TRAN-CNT / 7T '2nd Annuitant DOD transactions read:' 3X #DOD2-TRAN-CNT / 7T 'Invalid transactions count.........:' 3X #INVALID-TRAN-CNT
            ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"IVC transactions read..............:",new ColumnSpacing(3),pnd_Ws_Pnd_Ivc_Tran_Cnt, new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"'RTB IVC' transactions read........:",new ColumnSpacing(3),pnd_Ws_Pnd_Ric_Tran_Cnt, new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"'Total IVC' transactions read......:",new ColumnSpacing(3),pnd_Ws_Pnd_Tic_Tran_Cnt, new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"Residency transactions read........:",new ColumnSpacing(3),pnd_Ws_Pnd_Res_Tran_Cnt, new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"Citizenship transactions read......:",new ColumnSpacing(3),pnd_Ws_Pnd_Cit_Tran_Cnt, new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"Tax Id transactions read...........:",new ColumnSpacing(3),pnd_Ws_Pnd_Ssn_Tran_Cnt, new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"1st Annuitant DOB transactions read:",new ColumnSpacing(3),pnd_Ws_Pnd_Dob1_Tran_Cnt, new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"2nd Annuitant DOB transactions read:",new ColumnSpacing(3),pnd_Ws_Pnd_Dob2_Tran_Cnt, new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"1st Annuitant DOD transactions read:",new ColumnSpacing(3),pnd_Ws_Pnd_Dod1_Tran_Cnt, new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"2nd Annuitant DOD transactions read:",new ColumnSpacing(3),pnd_Ws_Pnd_Dod2_Tran_Cnt, new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"Invalid transactions count.........:",new ColumnSpacing(3),pnd_Ws_Pnd_Invalid_Tran_Cnt, new ReportEditMask 
            ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Tot_Tran_Cnt.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tot_Tran_Cnt), pnd_Ws_Pnd_Ivc_Tran_Cnt.add(pnd_Ws_Pnd_Ric_Tran_Cnt).add(pnd_Ws_Pnd_Tic_Tran_Cnt)); //Natural: ASSIGN #TOT-TRAN-CNT := #IVC-TRAN-CNT + #RIC-TRAN-CNT + #TIC-TRAN-CNT
        pnd_Ws_Pnd_Tot_Tran_Accept.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tot_Tran_Accept), pnd_Ws_Pnd_Ivc_Tran_Accepted.add(pnd_Ws_Pnd_Rtb_Ivc_Accept).add(pnd_Ws_Pnd_Tic_Ivc_Accept)); //Natural: ASSIGN #TOT-TRAN-ACCEPT := #IVC-TRAN-ACCEPTED + #RTB-IVC-ACCEPT + #TIC-IVC-ACCEPT
        pnd_Ws_Pnd_Tot_Tran_Reject.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tot_Tran_Reject), pnd_Ws_Pnd_Ivc_Tran_Rejected.add(pnd_Ws_Pnd_Rtb_Ivc_Reject).add(pnd_Ws_Pnd_Tic_Ivc_Reject)); //Natural: ASSIGN #TOT-TRAN-REJECT := #IVC-TRAN-REJECTED + #RTB-IVC-REJECT + #TIC-IVC-REJECT
        pnd_Ws_Pnd_Tot_Ytd_Amt.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tot_Ytd_Amt), pnd_Ws_Pnd_Ivc_Ytd_Amt.add(pnd_Ws_Pnd_Rtb_Ytd_Amt).add(pnd_Ws_Pnd_Tic_Ytd_Amt)); //Natural: ASSIGN #TOT-YTD-AMT := #IVC-YTD-AMT + #RTB-YTD-AMT + #TIC-YTD-AMT
        pnd_Ws_Pnd_Tot_Tot_Amt.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tot_Tot_Amt), pnd_Ws_Pnd_Ivc_Tot_Amt.add(pnd_Ws_Pnd_Rtb_Tot_Amt).add(pnd_Ws_Pnd_Tic_Tot_Amt)); //Natural: ASSIGN #TOT-TOT-AMT := #IVC-TOT-AMT + #RTB-TOT-AMT + #TIC-TOT-AMT
        pnd_Ws_Pnd_Tot_Ytd_Amt_R.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tot_Ytd_Amt_R), pnd_Ws_Pnd_Ivc_Ytd_Amt_R.add(pnd_Ws_Pnd_Rtb_Ytd_Amt_R).add(pnd_Ws_Pnd_Tic_Ytd_Amt_R)); //Natural: ASSIGN #TOT-YTD-AMT-R := #IVC-YTD-AMT-R + #RTB-YTD-AMT-R + #TIC-YTD-AMT-R
        pnd_Ws_Pnd_Tot_Tot_Amt_R.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tot_Tot_Amt_R), pnd_Ws_Pnd_Ivc_Tot_Amt_R.add(pnd_Ws_Pnd_Rtb_Tot_Amt_R).add(pnd_Ws_Pnd_Tic_Tot_Amt_R)); //Natural: ASSIGN #TOT-TOT-AMT-R := #IVC-TOT-AMT-R + #RTB-TOT-AMT-R + #TIC-TOT-AMT-R
        getReports().skip(1, 4);                                                                                                                                          //Natural: SKIP ( 01 ) 4 LINES
        getReports().write(1, new TabSetting(7),"T W R   A U T O M A T E D   M A I N T E N A N C E",NEWLINE,NEWLINE,new TabSetting(7),"Tot Transactions Read..............:",new  //Natural: WRITE ( 01 ) 7T 'T W R   A U T O M A T E D   M A I N T E N A N C E' / / 7T 'Tot Transactions Read..............:' 3X #TOT-TRAN-CNT / 7T '                 Accepted..........:' 3X #TOT-TRAN-ACCEPT / 7T '                 Rejected..........:' 3X #TOT-TRAN-REJECT / 7T '    Year-to-Date Accepted..........:' #TOT-YTD-AMT / 7T '           Total Accepted..........:' #TOT-TOT-AMT / 7T '    Year-to-Date Rejected..........:' #TOT-YTD-AMT-R / 7T '           Total Rejected..........:' #TOT-TOT-AMT-R / / 7T 'IVC Transactions Read..............:' 3X #IVC-TRAN-CNT / 7T '                 Accepted..........:' 3X #IVC-TRAN-ACCEPTED / 7T '                 Rejected..........:' 3X #IVC-TRAN-REJECTED / 7T '    Year-to-Date Accepted..........:' #IVC-YTD-AMT / 7T '           Total Accepted..........:' #IVC-TOT-AMT / 7T '    Year-to-Date Rejected..........:' #IVC-YTD-AMT-R / 7T '           Total Rejected..........:' #IVC-TOT-AMT-R / / 7T '"RTB IVC" Transactions Read........:' 3X #RIC-TRAN-CNT / 7T '                 Accepted..........:' 3X #RTB-IVC-ACCEPT / 7T '                 Rejected..........:' 3X #RTB-IVC-REJECT / 7T '    Year-to-Date Accepted..........:' #RTB-YTD-AMT / 7T '           Total Accepted..........:' #RTB-TOT-AMT / 7T '    Year-to-Date Rejected..........:' #RTB-YTD-AMT-R / 7T '           Total Rejected..........:' #RTB-TOT-AMT-R / / 7T '"Total IVC" Transactions Read......:' 3X #TIC-TRAN-CNT / 7T '                 Accepted..........:' 3X #TIC-IVC-ACCEPT / 7T '                 Rejected..........:' 3X #TIC-IVC-REJECT / 7T '    Year-to-Date Accepted..........:' #TIC-YTD-AMT / 7T '           Total Accepted..........:' #TIC-TOT-AMT / 7T '    Year-to-Date Rejected..........:' #TIC-YTD-AMT-R / 7T '           Total Rejected..........:' #TIC-TOT-AMT-R
            ColumnSpacing(3),pnd_Ws_Pnd_Tot_Tran_Cnt, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"                 Accepted..........:",new 
            ColumnSpacing(3),pnd_Ws_Pnd_Tot_Tran_Accept, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"                 Rejected..........:",new 
            ColumnSpacing(3),pnd_Ws_Pnd_Tot_Tran_Reject, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"    Year-to-Date Accepted..........:",pnd_Ws_Pnd_Tot_Ytd_Amt, 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(7),"           Total Accepted..........:",pnd_Ws_Pnd_Tot_Tot_Amt, new ReportEditMask 
            ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(7),"    Year-to-Date Rejected..........:",pnd_Ws_Pnd_Tot_Ytd_Amt_R, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(7),"           Total Rejected..........:",pnd_Ws_Pnd_Tot_Tot_Amt_R, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,new TabSetting(7),"IVC Transactions Read..............:",new 
            ColumnSpacing(3),pnd_Ws_Pnd_Ivc_Tran_Cnt, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"                 Accepted..........:",new 
            ColumnSpacing(3),pnd_Ws_Pnd_Ivc_Tran_Accepted, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"                 Rejected..........:",new 
            ColumnSpacing(3),pnd_Ws_Pnd_Ivc_Tran_Rejected, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"    Year-to-Date Accepted..........:",pnd_Ws_Pnd_Ivc_Ytd_Amt, 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(7),"           Total Accepted..........:",pnd_Ws_Pnd_Ivc_Tot_Amt, new ReportEditMask 
            ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(7),"    Year-to-Date Rejected..........:",pnd_Ws_Pnd_Ivc_Ytd_Amt_R, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(7),"           Total Rejected..........:",pnd_Ws_Pnd_Ivc_Tot_Amt_R, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,new TabSetting(7),"'RTB IVC' Transactions Read........:",new 
            ColumnSpacing(3),pnd_Ws_Pnd_Ric_Tran_Cnt, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"                 Accepted..........:",new 
            ColumnSpacing(3),pnd_Ws_Pnd_Rtb_Ivc_Accept, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"                 Rejected..........:",new 
            ColumnSpacing(3),pnd_Ws_Pnd_Rtb_Ivc_Reject, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"    Year-to-Date Accepted..........:",pnd_Ws_Pnd_Rtb_Ytd_Amt, 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(7),"           Total Accepted..........:",pnd_Ws_Pnd_Rtb_Tot_Amt, new ReportEditMask 
            ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(7),"    Year-to-Date Rejected..........:",pnd_Ws_Pnd_Rtb_Ytd_Amt_R, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(7),"           Total Rejected..........:",pnd_Ws_Pnd_Rtb_Tot_Amt_R, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,new TabSetting(7),"'Total IVC' Transactions Read......:",new 
            ColumnSpacing(3),pnd_Ws_Pnd_Tic_Tran_Cnt, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"                 Accepted..........:",new 
            ColumnSpacing(3),pnd_Ws_Pnd_Tic_Ivc_Accept, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"                 Rejected..........:",new 
            ColumnSpacing(3),pnd_Ws_Pnd_Tic_Ivc_Reject, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"    Year-to-Date Accepted..........:",pnd_Ws_Pnd_Tic_Ytd_Amt, 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(7),"           Total Accepted..........:",pnd_Ws_Pnd_Tic_Tot_Amt, new ReportEditMask 
            ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(7),"    Year-to-Date Rejected..........:",pnd_Ws_Pnd_Tic_Ytd_Amt_R, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(7),"           Total Rejected..........:",pnd_Ws_Pnd_Tic_Tot_Amt_R, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(14),"Citizenship transactions read:",pnd_Ws_Pnd_Cit_Tran_Cnt, new ReportEditMask             //Natural: WRITE ( 02 ) //// 14T 'Citizenship transactions read:' #CIT-TRAN-CNT
            ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(3, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(7),"1st Annuitant DOB transactions read:",pnd_Ws_Pnd_Dob1_Tran_Cnt, new ReportEditMask       //Natural: WRITE ( 03 ) /// / 7T '1st Annuitant DOB transactions read:' #DOB1-TRAN-CNT / 7T '2nd Annuitant DOB transactions read:' #DOB2-TRAN-CNT
            ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"2nd Annuitant DOB transactions read:",pnd_Ws_Pnd_Dob2_Tran_Cnt, new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(4, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(7),"1st Annuitant DOD transactions read:",pnd_Ws_Pnd_Dod1_Tran_Cnt, new ReportEditMask       //Natural: WRITE ( 04 ) /// / 7T '1st Annuitant DOD transactions read:' #DOD1-TRAN-CNT / 7T '2nd Annuitant DOD transactions read:' #DOD2-TRAN-CNT
            ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"2nd Annuitant DOD transactions read:",pnd_Ws_Pnd_Dod2_Tran_Cnt, new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(5, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(14),"Tax Id transactions read:",pnd_Ws_Pnd_Ssn_Tran_Cnt, new ReportEditMask                  //Natural: WRITE ( 05 ) //// 14T 'Tax Id transactions read:' #SSN-TRAN-CNT
            ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(6, new TabSetting(71),"==============",NEWLINE,new TabSetting(70),pnd_Ws_Pnd_Rpt6_Ytd_Ivc_Tot, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new  //Natural: WRITE ( 06 ) 71T '==============' / 70T #RPT6-YTD-IVC-TOT / 70T #RPT6-TOT-IVC-TOT //// / 7T 'IVC Transactions Rejected........:' #IVC-TRAN-REJECTED / 7T '"RTB IVC" Transactions Rejected..:' #RTB-IVC-REJECT / 7T '"TOTAL IVC" Transactions Rejected:' #TIC-IVC-REJECT
            TabSetting(70),pnd_Ws_Pnd_Rpt6_Tot_Ivc_Tot, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(7),"IVC Transactions Rejected........:",pnd_Ws_Pnd_Ivc_Tran_Rejected, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"'RTB IVC' Transactions Rejected..:",pnd_Ws_Pnd_Rtb_Ivc_Reject, new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"'TOTAL IVC' Transactions Rejected:",pnd_Ws_Pnd_Tic_Ivc_Reject, new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(7, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(14),"Residency transactions read:",pnd_Ws_Pnd_Res_Tran_Cnt, new ReportEditMask               //Natural: WRITE ( 07 ) //// 14T 'Residency transactions read:' #RES-TRAN-CNT
            ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(8, new TabSetting(52),"============================",NEWLINE,new TabSetting(49),pnd_Ws_Pnd_Rpt8_Ytd_Ivc_Tot, new ReportEditMask                //Natural: WRITE ( 08 ) 52T '============================' / 49T #RPT8-YTD-IVC-TOT #RPT8-TOT-IVC-TOT //// / 7T 'IVC Transactions Accepted........:' #IVC-TRAN-ACCEPTED / 7T '"RTB IVC" Transactions Accepted..:' #RTB-IVC-ACCEPT / 7T '"Total IVC" Transactions Accepted:' #TIC-IVC-ACCEPT
            ("-ZZZ,ZZZ,ZZ9.99"),pnd_Ws_Pnd_Rpt8_Tot_Ivc_Tot, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(7),"IVC Transactions Accepted........:",pnd_Ws_Pnd_Ivc_Tran_Accepted, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"'RTB IVC' Transactions Accepted..:",pnd_Ws_Pnd_Rtb_Ivc_Accept, new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"'Total IVC' Transactions Accepted:",pnd_Ws_Pnd_Tic_Ivc_Accept, new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Process_Trailer() throws Exception                                                                                                                   //Natural: PROCESS-TRAILER
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        if (condition(pnd_Ws_Pnd_Tran_Cnt.notEquals(ldaTwrl6400.getPnd_Header_Record_Pnd_Rec_Count())))                                                                   //Natural: IF #TRAN-CNT NE #REC-COUNT
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,NEWLINE,"***",new TabSetting(25),"Trailer and Actual record counts do not agree",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE / '***' 25T 'Trailer and Actual record counts do not agree' 77T '***' / '***' 25T 'Actual  count:' #TRAN-CNT 77T '***' / '***' 25T 'Trailer count:' #REC-COUNT ( EM = -Z,ZZZ,ZZ9 ) 77T '***'
                TabSetting(25),"Actual  count:",pnd_Ws_Pnd_Tran_Cnt, new ReportEditMask ("-Z,ZZZ,ZZ9"),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Trailer count:",ldaTwrl6400.getPnd_Header_Record_Pnd_Rec_Count(), 
                new ReportEditMask ("-Z,ZZZ,ZZ9"),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(52);  if (true) return;                                                                                                                     //Natural: TERMINATE 52
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Header() throws Exception                                                                                                                    //Natural: PROCESS-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        getWorkFiles().read(1, ldaTwrl6400.getPnd_Header_Record());                                                                                                       //Natural: READ WORK FILE 01 ONCE #HEADER-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Input file is empty",new TabSetting(77),"***");                                          //Natural: WRITE ( 0 ) '***' 25T 'Input file is empty' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(50);  if (true) return;                                                                                                                     //Natural: TERMINATE 50
        }                                                                                                                                                                 //Natural: END-ENDFILE
        if (condition(!ldaTwrl6400.getPnd_Header_Record_Pnd_Header_Type().getSubstring(2,2).equals("HD")))                                                                //Natural: IF SUBSTR ( #HEADER-TYPE,2,2 ) NE 'HD'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Header record is not on the input file",new TabSetting(77),"***");                       //Natural: WRITE '***' 25T 'Header record is not on the input file' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(51);  if (true) return;                                                                                                                     //Natural: TERMINATE 51
        }                                                                                                                                                                 //Natural: END-IF
        //* * V1.04: IF NEW SPECIAL RUN INDICATOR IS PRESENT IN HDR RCD, THEN
        //*          TURN LOCAL SW ON
        if (condition(ldaTwrl6400.getPnd_Header_Record_Pnd_Header_Special_Run_Ind().equals("SPECIAL")))                                                                   //Natural: IF #HEADER-SPECIAL-RUN-IND = 'SPECIAL' THEN
        {
            pnd_Ws_Pnd_Special_Run_Ind.setValue(true);                                                                                                                    //Natural: ASSIGN #SPECIAL-RUN-IND := TRUE
            getReports().write(0, ReportOption.NOTITLE,"*** Special Run Indicator was found present in header record ***");                                               //Natural: WRITE '*** Special Run Indicator was found present in header record ***'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Date_N.compute(new ComputeParameters(false, pnd_Ws_Pnd_Date_N), ldaTwrl6400.getPnd_Header_Record_Pnd_Interface_Dte().multiply(100).add(1));            //Natural: ASSIGN #DATE-N := #INTERFACE-DTE * 100 + 1
        pnd_Ws_Pnd_Cntrl_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ws_Pnd_Date_X);                                                                            //Natural: MOVE EDITED #DATE-X TO #CNTRL-DTE ( EM = YYYYMMDD )
        pnd_Ws_Pnd_Pymnt_Dte.setValue(pnd_Ws_Pnd_Date_X);                                                                                                                 //Natural: ASSIGN #PYMNT-DTE := #DATE-X
        //* *IF 2000/01 OR 2000/02 AND SPECIAL RUN
        //*  JANUARY RUN FOR THE PREVIOUS TAX YEAR
        if (condition(pnd_Ws_Pnd_Mm_N.equals(1) || ((pnd_Ws_Pnd_Mm_N.equals(2)) && pnd_Ws_Pnd_Special_Run_Ind.getBoolean())))                                             //Natural: IF #WS.#MM-N = 01 OR ( ( #WS.#MM-N = 02 ) AND #SPECIAL-RUN-IND ) THEN
        {
            pnd_Work_Rec_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Work_Rec_Pnd_Tax_Year), ldaTwrl6400.getPnd_Header_Record_Pnd_Interface_Dte().divide(100).subtract(1)); //Natural: ASSIGN #WORK-REC.#TAX-YEAR := #INTERFACE-DTE / 100 - 1
            getReports().write(0, ReportOption.NOTITLE,"*** JANUARY RUN or SPECIAL RUN: Will process records for ","previous year ***");                                  //Natural: WRITE '*** JANUARY RUN or SPECIAL RUN: Will process records for ' 'previous year ***'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Work_Rec_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Work_Rec_Pnd_Tax_Year), ldaTwrl6400.getPnd_Header_Record_Pnd_Interface_Dte().divide(100)); //Natural: ASSIGN #WORK-REC.#TAX-YEAR := #INTERFACE-DTE / 100
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Input_Parm() throws Exception                                                                                                                //Natural: PROCESS-INPUT-PARM
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("TWRP0640|sub_Process_Input_Parm");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Origin);                                                                                     //Natural: INPUT #INPUT-ORIGIN
                if (condition(pnd_Input_Origin.equals("AP") || pnd_Input_Origin.equals("IA")))                                                                            //Natural: IF #INPUT-ORIGIN = 'AP' OR = 'IA'
                {
                    setValueToSubstring(pnd_Input_Origin,pnd_Ws_Pnd_Header,11,2);                                                                                         //Natural: MOVE #INPUT-ORIGIN TO SUBSTR ( #HEADER,11,2 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Unknown input parameter:",pnd_Input_Origin,new TabSetting(77),                   //Natural: WRITE '***' 25T 'Unknown input parameter:' #INPUT-ORIGIN 77T '***'
                        "***");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate(52);  if (true) return;                                                                                                             //Natural: TERMINATE 52
                }                                                                                                                                                         //Natural: END-IF
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 0 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 0 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 0 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=133 ZP=ON");
        Global.format(2, "PS=58 LS=80 ZP=ON");
        Global.format(3, "PS=58 LS=80 ZP=ON");
        Global.format(4, "PS=58 LS=80 ZP=ON");
        Global.format(5, "PS=58 LS=80 ZP=ON");
        Global.format(6, "PS=58 LS=133 ZP=ON");
        Global.format(7, "PS=58 LS=80 ZP=ON");
        Global.format(8, "PS=58 LS=133 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(57),"TWR Payment System",new TabSetting(120),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(47),pnd_Ws_Pnd_Header,pnd_Ws_Pnd_Cntrl_Dte, new ReportEditMask ("MM/YYYY"),new TabSetting(120),"Report: RPT1",NEWLINE,new TabSetting(13),pnd_Vers_Id,new 
            TabSetting(45),"Control and Invalid Transaction Type Reports",NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(34),"TWR Payment System",new TabSetting(68),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(24),pnd_Ws_Pnd_Header,pnd_Ws_Pnd_Cntrl_Dte, new ReportEditMask ("MM/YYYY"),new TabSetting(68),"Report: RPT2",NEWLINE,new TabSetting(13),pnd_Vers_Id,new 
            TabSetting(34),"Citizenship Changes",NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(34),"TWR Payment System",new TabSetting(68),"Page:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(24),pnd_Ws_Pnd_Header,pnd_Ws_Pnd_Cntrl_Dte, new ReportEditMask ("MM/YYYY"),new TabSetting(68),"Report: RPT3",NEWLINE,new TabSetting(13),pnd_Vers_Id,new 
            TabSetting(33),"Date of Birth Changes",NEWLINE,NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(34),"TWR Payment System",new TabSetting(68),"Page:",getReports().getPageNumberDbs(4), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(24),pnd_Ws_Pnd_Header,pnd_Ws_Pnd_Cntrl_Dte, new ReportEditMask ("MM/YYYY"),new TabSetting(68),"Report: RPT4",NEWLINE,new TabSetting(13),pnd_Vers_Id,new 
            TabSetting(33),"Date of Death Changes",NEWLINE,NEWLINE);
        getReports().write(5, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(34),"TWR Payment System",new TabSetting(68),"Page:",getReports().getPageNumberDbs(5), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(24),pnd_Ws_Pnd_Header,pnd_Ws_Pnd_Cntrl_Dte, new ReportEditMask ("MM/YYYY"),new TabSetting(68),"Report: RPT5",NEWLINE,new TabSetting(13),pnd_Vers_Id,new 
            TabSetting(34),"Tax Id(SSN) Changes",NEWLINE,NEWLINE);
        getReports().write(6, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(57),"TWR Payment System",new TabSetting(120),"Page:",getReports().getPageNumberDbs(6), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(47),pnd_Ws_Pnd_Header,pnd_Ws_Pnd_Cntrl_Dte, new ReportEditMask ("MM/YYYY"),new TabSetting(120),"Report: RPT6",NEWLINE,new TabSetting(13),pnd_Vers_Id,new 
            TabSetting(52),"Transaction Rejection Report",NEWLINE,NEWLINE);
        getReports().write(7, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(34),"TWR Payment System",new TabSetting(68),"Page:",getReports().getPageNumberDbs(7), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(24),pnd_Ws_Pnd_Header,pnd_Ws_Pnd_Cntrl_Dte, new ReportEditMask ("MM/YYYY"),new TabSetting(68),"Report: RPT7",NEWLINE,new TabSetting(13),pnd_Vers_Id,new 
            TabSetting(35),"Residency Changes",NEWLINE,NEWLINE);
        getReports().write(8, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(57),"TWR Payment System",new TabSetting(120),"Page:",getReports().getPageNumberDbs(8), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(47),pnd_Ws_Pnd_Header,pnd_Ws_Pnd_Cntrl_Dte, new ReportEditMask ("MM/YYYY"),new TabSetting(120),"Report: RPT8",NEWLINE,new TabSetting(13),pnd_Vers_Id,new 
            TabSetting(45),"IVC, 'RTB IVC', and 'Total IVC' Change Report",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Tran/Type",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde(),"//Contract",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr(),"//Payee",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde(), new FieldAttributes("LC=��"),"/Old Tax Id",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tax_Id(), new ReportEditMask ("999-99-9999"),NEWLINE,"New Tax Id",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Tax_Id(), new ReportEditMask ("999-99-9999"),"/Maint/Month",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Pymnt_Month(), new FieldAttributes("LC=��"),"/Residency",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Rsdncy_Cde(), new FieldAttributes("LC=�����"),NEWLINE,"Citizenship",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Ctzn_Cde(), new FieldAttributes("LC=�����"),"/   IVC code",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Per_Ivc_Cde(), new FieldAttributes("LC=�����"),NEWLINE,"RTB IVC Code",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Rtb_Ivc_Cde(), new FieldAttributes("LC=�����"),"/YTD IVC amt",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt(), new ReportEditMask ("-Z,ZZZ,ZZ9.99"),NEWLINE,"Total contract IVC",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt(), new ReportEditMask ("-Z,ZZZ,ZZ9.99"),"/1st Annt DOB",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Dob_F(),NEWLINE,"2nd Annt DOB",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Dob_S(),"/1st Annt DOD",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Dod_F(),NEWLINE,"2nd Annt DOD",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Dod_S(),"1st/Payment/Date",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_First_Pymnt_Dte());
        getReports().setDisplayColumns(2, new TabSetting(14),"Tran/Type",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde(),"/Contract",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr(),"/Payee",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde(), new FieldAttributes("LC=��"),"/Tax Id",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tax_Id(), new ReportEditMask ("999-99-9999"),"Maint/Month",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Pymnt_Month(), new FieldAttributes("LC=�"),"/Citizenship",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Ctzn_Cde(), new FieldAttributes("LC=�����"));
        getReports().setDisplayColumns(3, new TabSetting(7),"Tran/Type",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde(),"/Contract",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr(),"/Payee",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde(), new FieldAttributes("LC=��"),"/Tax Id",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tax_Id(), new ReportEditMask ("999-99-9999"),"Maint/Month",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Pymnt_Month(), new FieldAttributes("LC=�"),"1st Annuitant/DOB",
        		pnd_Ws_Pnd_Date_D, new ReportEditMask (" MM/DD/YYYY"), pnd_Ws_Pnd_Dob1_Cv,"2nd Annuitant/DOB",
        		pnd_Ws_Pnd_Date_D, new ReportEditMask (" MM/DD/YYYY"), pnd_Ws_Pnd_Dob2_Cv);
        getReports().setDisplayColumns(4, new TabSetting(7),"Tran/Type",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde(),"/Contract",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr(),"/Payee",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde(), new FieldAttributes("LC=��"),"/Tax Id",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tax_Id(), new ReportEditMask ("999-99-9999"),"Maint/Month",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Pymnt_Month(), new FieldAttributes("LC=�"),"1st Annuitant/DOD",
        		pnd_Ws_Pnd_Date_D, new ReportEditMask ("   MM/YYYY"), pnd_Ws_Pnd_Dod1_Cv,"2ND Annuitant/DOD",
        		pnd_Ws_Pnd_Date_D, new ReportEditMask ("   MM/YYYY"), pnd_Ws_Pnd_Dod2_Cv);
        getReports().setDisplayColumns(5, new TabSetting(14),"Tran/Type",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde(),"/Contract",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr(),"/Payee",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde(), new FieldAttributes("LC=��"),"Old/Tax Id",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tax_Id(), new ReportEditMask ("999-99-9999"),"New/Tax Id",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Tax_Id(), new ReportEditMask ("999-99-9999"),"Maint/Month",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Pymnt_Month(), new FieldAttributes("LC=�"));
        getReports().setDisplayColumns(6, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Tran/Type",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde(),"//Contract",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr(),"//Payee",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde(), new FieldAttributes("LC=��"),"//Tax ID",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tax_Id(), new ReportEditMask ("999-99-9999"),"/Maint/Month",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Pymnt_Month(), new FieldAttributes("LC=���"),"Citi/zen/ship",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Citizenship(),"//Residency",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Rsdncy_Cde(), new FieldAttributes("LC=�����"),"/Tran/Code",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Trans_Cde(), new FieldAttributes("LC=�"),"/   IVC Cde",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Per_Ivc_Cde(), new FieldAttributes("LC=�����"),NEWLINE,"RTB IVC Cde",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Rtb_Ivc_Cde(), new FieldAttributes("LC=�����"),"/YTD IVC Amt",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt(), new ReportEditMask ("-Z,ZZZ,ZZ9.99"), pnd_Ws_Pnd_Ivc_Cv,NEWLINE,"Total Contract IVC",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt(), new ReportEditMask ("-Z,ZZZ,ZZ9.99"),"1st/Payment/Date",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_First_Pymnt_Dte(), new ReportEditMask ("9999/99"),"/",
        		pnd_Ws_Pnd_Msg);
        getReports().setDisplayColumns(7, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new TabSetting(14),"/Tran/Type",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde(),"//Contract",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr(),"//Payee",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde(), new FieldAttributes("LC=��"),"//Tax Id",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tax_Id(), new ReportEditMask ("999-99-9999"),"/Maint/Month",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Pymnt_Month(), new FieldAttributes("LC=��"),"//Residency",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_New_Rsdncy_Cde(), new FieldAttributes("LC=�����"));
        getReports().setDisplayColumns(8, new IdenticalSuppress(true),new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
            "/Tran/Type",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Maint_Cde(),"//Contract",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr(),"//Payee",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Payee_Cde(), new ReportEditMask ("  XX"),"//Tax ID",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tax_Id(), new ReportEditMask ("999-99-9999"),"/Maint/Month",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Pymnt_Month(), new FieldAttributes("LC=��"),"/IVC/Cde",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Per_Ivc_Cde(), new FieldAttributes("LC=�"),"RTB/IVC/Cde",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Rtb_Ivc_Cde(), new FieldAttributes("LC=�"),"/Tran/Code",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Trans_Cde(), new FieldAttributes("LC=�"),"//YTD IVC Amt",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt(), new ReportEditMask ("-Z,ZZZ,ZZ9.99"), pnd_Ws_Pnd_Ivc_Cv,"Total/Contract/IVC",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt(), new ReportEditMask ("-Z,ZZZ,ZZ9.99"),"1st/Payment/Date",
        		ldaTwrl6400.getPnd_Npd_Tran_Pnd_I_First_Pymnt_Dte(), new ReportEditMask ("9999/99"),"/",
        		pnd_Ws_Pnd_Msg, new IdenticalSuppress(false),"/",
        		pnd_Ws_Pnd_Eyecatcher, new IdenticalSuppress(false));
    }
}
