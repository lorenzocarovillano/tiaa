/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:31:21 PM
**        * FROM NATURAL PROGRAM : Twrp0706
************************************************************
**        * FILE NAME            : Twrp0706.java
**        * CLASS NAME           : Twrp0706
**        * INSTANCE NAME        : Twrp0706
************************************************************
************************************************************************
** PROGRAM NAME:  TWRP0706
** SYSTEM      :  TAX REPORTING SYSTEM
** AUTHOR      :  ROSE MA
** PURPOSE     :  MONTHLY SUMMARY REPORT OF OMNI DAILY FEED REJECTS
**                (ALSO FOR MSR OF NAVISYS DAILY FEED REJECTS)
** INPUT FILE  :  REJECTED TAX TRANSACTION DETAIL FILE (SORTED)
**             :  (ACCUMULATED FROM JOB PTW1110D OR PTW1111D)
**                         (OR FROM JOB PTW1170D OR PTW1171D)
** DATE        :  11/13/2007
**
** 05/04/2017 WEBBJ PIN EXPANSION STOW ONLY.
**
************************************************************************
** MODIFICATION LOG
************************************************************************
** DATE          PURPOSE
**----------------------------------------------
** 05/12/08 RM - ROTH 401K/403B PROJECT
**               RECOMPILED DUE TO UPDATED TWRL0700.
** 02/04/08 RM - ADD LOGIC TO HANDLE EMPTY INPUT FILE.
** 10/18/11 RC - NON-ERISA TDA NEW COMPANY CODE 'F'
** 04/24/12 RC - SUNY. INCLUDE PALN,SUBPLAN,ORIGINATING SUBPLAN AND
**               ORIGINATING CONTRACT IN REPORT
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0706 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0700 ldaTwrl0700;
    private PdaTwracomp pdaTwracomp;
    private LdaTwrl9804 ldaTwrl9804;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Comp_Max;
    private DbsField pnd_Ws_Const_Source_Max;
    private DbsField pnd_Ws_Const_Err_Max;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Rec_Cnt;
    private DbsField pnd_Ws_Pnd_Idx;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Ws_Pnd_Comp_Idx;
    private DbsField pnd_Ws_Pnd_Source_Idx;
    private DbsField pnd_Ws_Pnd_E;
    private DbsField pnd_Ws_Pnd_Errors;
    private DbsField pnd_Ws_Pnd_Comp_Name;
    private DbsField pnd_Ws_Pnd_Comp_Name_Disp;
    private DbsField pnd_Ws_Pnd_Puri;
    private DbsField pnd_Ws_Pnd_State_Amt;
    private DbsField pnd_Ws_Pnd_Twrt_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Twrt_Interest_Amt;
    private DbsField pnd_Ws_Pnd_Twrt_Fed_Whhld_Amt;
    private DbsField pnd_Ws_Pnd_Twrt_Nra_Whhld_Amt;
    private DbsField pnd_Ws_Pnd_Pnd_Puri;
    private DbsField pnd_Ws_Pnd_Twrt_Ivc_Amt;
    private DbsField pnd_Ws_Pnd_Pnd_State_Amt;
    private DbsField pnd_Ws_Pnd_Er_Desc;
    private DbsField pnd_Ws_Pnd_Comp_Break;
    private DbsField pnd_Ws_Pnd_Source_Break;
    private DbsField pnd_Ws_Pnd_T_Source_Cnt;
    private DbsField pnd_Ws_Pnd_T_Source_Code;
    private DbsField pnd_Ws_Pnd_T_Source_Name;
    private DbsField pnd_Ws_Pnd_Er_Su;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws__Filler1;
    private DbsField pnd_Ws_Pnd_Er_Yr;
    private DbsField pnd_Ws_Pnd_Er_Nbr;
    private DbsField pnd_Ws_Pnd_So_Su;

    private DbsGroup pnd_Ws__R_Field_2;
    private DbsField pnd_Ws__Filler2;
    private DbsField pnd_Ws_Pnd_So_Yr;
    private DbsField pnd_Ws_Pnd_So_Cde;
    private DbsField pnd_Ws_Pnd_Dist_Su;

    private DbsGroup pnd_Ws__R_Field_3;
    private DbsField pnd_Ws_Pnd_Di_T_Num;
    private DbsField pnd_Ws_Pnd_Di_T_Yr;
    private DbsField pnd_Ws_Pnd_St_Su;

    private DbsGroup pnd_Ws__R_Field_4;
    private DbsField pnd_Ws__Filler3;
    private DbsField pnd_Ws_Pnd_St_Yr;
    private DbsField pnd_Ws_Pnd_Cn_Su;

    private DbsGroup pnd_Ws__R_Field_5;
    private DbsField pnd_Ws__Filler4;
    private DbsField pnd_Ws_Pnd_Cn_Yr;

    private DbsGroup pnd_Ws_Pnd_Rej_Grp;
    private DbsField pnd_Ws_Pnd_Rej_Cnt;
    private DbsField pnd_Ws_Pnd_Rej_Gross_Unkn;
    private DbsField pnd_Ws_Pnd_Rej_Gross_Roll;
    private DbsField pnd_Ws_Pnd_Rej_Gross;
    private DbsField pnd_Ws_Pnd_Rej_Ivc;
    private DbsField pnd_Ws_Pnd_Rej_Int;
    private DbsField pnd_Ws_Pnd_Rej_Fed;
    private DbsField pnd_Ws_Pnd_Rej_Nra;
    private DbsField pnd_Ws_Pnd_Rej_Pr;
    private DbsField pnd_Ws_Pnd_Rej_State;

    private DbsGroup pnd_Ws_Pnd_Comb_Grp;
    private DbsField pnd_Ws_Pnd_Comb_Cnt;
    private DbsField pnd_Ws_Pnd_Comb_Gross;
    private DbsField pnd_Ws_Pnd_Comb_Ivc;
    private DbsField pnd_Ws_Pnd_Comb_Int;
    private DbsField pnd_Ws_Pnd_Comb_Fed;
    private DbsField pnd_Ws_Pnd_Comb_Nra;
    private DbsField pnd_Ws_Pnd_Comb_Pr;
    private DbsField pnd_Ws_Pnd_Comb_State;
    private DbsField pnd_Super_Dist_Code;

    private DbsGroup pnd_Super_Dist_Code__R_Field_6;
    private DbsField pnd_Super_Dist_Code_Pnd_S_Tbl_Nbr;
    private DbsField pnd_Super_Dist_Code_Pnd_S_Tax_Year;
    private DbsField pnd_Super_Dist_Code_Pnd_S_Settl_Type;
    private DbsField pnd_Super_Dist_Code_Pnd_S_Pay_Type;
    private DbsField pnd_Super_Dist_Code_Pnd_S_Dist_Code;
    private DbsField pnd_Search_Cat_Dist;

    private DbsGroup pnd_Search_Cat_Dist__R_Field_7;
    private DbsField pnd_Search_Cat_Dist_Pnd_Search_Cat;
    private DbsField pnd_Search_Cat_Dist_Pnd_Search_Dist;
    private DbsField pnd_Cat_Dist_Table;

    private DbsGroup pnd_Cat_Dist_Table__R_Field_8;

    private DbsGroup pnd_Cat_Dist_Table_Pnd_Cat_Dist_Entries;
    private DbsField pnd_Cat_Dist_Table_Pnd_Cat_Dist_Method;

    private DbsGroup pnd_Cat_Dist_Table__R_Field_9;
    private DbsField pnd_Cat_Dist_Table_Pnd_Pay_Category;
    private DbsField pnd_Cat_Dist_Table_Pnd_Dist_Method;
    private DbsField pnd_Cat_Dist_Table_Pnd_Dist_Method_Code;
    private DbsField pnd_Twrpymnt_Tax_Citizenship;
    private DbsField pnd_Twrpymnt_Paymt_Category;
    private DbsField pnd_Twrpymnt_Dist_Method;
    private DbsField pnd_Twrpymnt_Residency_Type;
    private DbsField pnd_Twrpymnt_Pymnt_Refer_Nbr;
    private DbsField pnd_Twrpymnt_Locality_Cde;
    private DbsField pnd_Twrpymnt_Contract_Seq_No;
    private DbsField pnd_Twrt_Locality_Cde;
    private DbsField i;
    private DbsField j;
    private DbsField k;

    private DataAccessProgramView vw_cntl;
    private DbsField cntl_Tircntl_5_Y_Sc_Co_To_Frm_Sp;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp;

    private DbsGroup pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_10;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Tbl_5;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Tax_Year;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Source;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Company;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_To_Date;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Interface_Date;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0700 = new LdaTwrl0700();
        registerRecord(ldaTwrl0700);
        localVariables = new DbsRecord();
        pdaTwracomp = new PdaTwracomp(localVariables);
        ldaTwrl9804 = new LdaTwrl9804();
        registerRecord(ldaTwrl9804);
        registerRecord(ldaTwrl9804.getVw_tircntl_Feeder_Sys_Tbl_View_View());

        // Local Variables

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Comp_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Comp_Max", "COMP-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Const_Source_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Source_Max", "SOURCE-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Const_Err_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Err_Max", "ERR-MAX", FieldType.PACKED_DECIMAL, 3);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Rec_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Idx", "#IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Comp_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Comp_Idx", "#COMP-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Source_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Source_Idx", "#SOURCE-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_E = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_E", "#E", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_Errors = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Errors", "#ERRORS", FieldType.NUMERIC, 2, new DbsArrayController(1, 60));
        pnd_Ws_Pnd_Comp_Name = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Comp_Name", "#COMP-NAME", FieldType.STRING, 4, new DbsArrayController(1, 7));
        pnd_Ws_Pnd_Comp_Name_Disp = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Comp_Name_Disp", "#COMP-NAME-DISP", FieldType.STRING, 4);
        pnd_Ws_Pnd_Puri = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Puri", "#PURI", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_State_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_State_Amt", "#STATE-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Twrt_Gross_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Twrt_Gross_Amt", "#TWRT-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Twrt_Interest_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Twrt_Interest_Amt", "#TWRT-INTEREST-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Twrt_Fed_Whhld_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Twrt_Fed_Whhld_Amt", "#TWRT-FED-WHHLD-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Twrt_Nra_Whhld_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Twrt_Nra_Whhld_Amt", "#TWRT-NRA-WHHLD-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Pnd_Puri = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pnd_Puri", "##PURI", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Twrt_Ivc_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Twrt_Ivc_Amt", "#TWRT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Pnd_State_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pnd_State_Amt", "##STATE-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Er_Desc = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Er_Desc", "#ER-DESC", FieldType.STRING, 40, new DbsArrayController(1, 5));
        pnd_Ws_Pnd_Comp_Break = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Comp_Break", "#COMP-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Source_Break = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Source_Break", "#SOURCE-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_T_Source_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_T_Source_Cnt", "#T-SOURCE-CNT", FieldType.PACKED_DECIMAL, 4);
        pnd_Ws_Pnd_T_Source_Code = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_T_Source_Code", "#T-SOURCE-CODE", FieldType.STRING, 2, new DbsArrayController(1, 
            50));
        pnd_Ws_Pnd_T_Source_Name = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_T_Source_Name", "#T-SOURCE-NAME", FieldType.STRING, 23, new DbsArrayController(1, 
            50));
        pnd_Ws_Pnd_Er_Su = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Er_Su", "#ER-SU", FieldType.NUMERIC, 8);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Er_Su);
        pnd_Ws__Filler1 = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Ws_Pnd_Er_Yr = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Er_Yr", "#ER-YR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Er_Nbr = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Er_Nbr", "#ER-NBR", FieldType.NUMERIC, 3);
        pnd_Ws_Pnd_So_Su = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_So_Su", "#SO-SU", FieldType.STRING, 7);

        pnd_Ws__R_Field_2 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_2", "REDEFINE", pnd_Ws_Pnd_So_Su);
        pnd_Ws__Filler2 = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_Ws_Pnd_So_Yr = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_So_Yr", "#SO-YR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_So_Cde = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_So_Cde", "#SO-CDE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Dist_Su = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Dist_Su", "#DIST-SU", FieldType.STRING, 5);

        pnd_Ws__R_Field_3 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_3", "REDEFINE", pnd_Ws_Pnd_Dist_Su);
        pnd_Ws_Pnd_Di_T_Num = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Di_T_Num", "#DI-T-NUM", FieldType.NUMERIC, 1);
        pnd_Ws_Pnd_Di_T_Yr = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Di_T_Yr", "#DI-T-YR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_St_Su = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_St_Su", "#ST-SU", FieldType.STRING, 5);

        pnd_Ws__R_Field_4 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_4", "REDEFINE", pnd_Ws_Pnd_St_Su);
        pnd_Ws__Filler3 = pnd_Ws__R_Field_4.newFieldInGroup("pnd_Ws__Filler3", "_FILLER3", FieldType.STRING, 1);
        pnd_Ws_Pnd_St_Yr = pnd_Ws__R_Field_4.newFieldInGroup("pnd_Ws_Pnd_St_Yr", "#ST-YR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Cn_Su = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cn_Su", "#CN-SU", FieldType.STRING, 5);

        pnd_Ws__R_Field_5 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_5", "REDEFINE", pnd_Ws_Pnd_Cn_Su);
        pnd_Ws__Filler4 = pnd_Ws__R_Field_5.newFieldInGroup("pnd_Ws__Filler4", "_FILLER4", FieldType.STRING, 1);
        pnd_Ws_Pnd_Cn_Yr = pnd_Ws__R_Field_5.newFieldInGroup("pnd_Ws_Pnd_Cn_Yr", "#CN-YR", FieldType.NUMERIC, 4);

        pnd_Ws_Pnd_Rej_Grp = pnd_Ws.newGroupArrayInGroup("pnd_Ws_Pnd_Rej_Grp", "#REJ-GRP", new DbsArrayController(1, 7));
        pnd_Ws_Pnd_Rej_Cnt = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_Cnt", "#REJ-CNT", FieldType.PACKED_DECIMAL, 6);
        pnd_Ws_Pnd_Rej_Gross_Unkn = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_Gross_Unkn", "#REJ-GROSS-UNKN", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Rej_Gross_Roll = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_Gross_Roll", "#REJ-GROSS-ROLL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Rej_Gross = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_Gross", "#REJ-GROSS", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Rej_Ivc = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_Ivc", "#REJ-IVC", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Rej_Int = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_Int", "#REJ-INT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Rej_Fed = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_Fed", "#REJ-FED", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Rej_Nra = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_Nra", "#REJ-NRA", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Rej_Pr = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_Pr", "#REJ-PR", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Rej_State = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_State", "#REJ-STATE", FieldType.PACKED_DECIMAL, 9, 2);

        pnd_Ws_Pnd_Comb_Grp = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Comb_Grp", "#COMB-GRP");
        pnd_Ws_Pnd_Comb_Cnt = pnd_Ws_Pnd_Comb_Grp.newFieldInGroup("pnd_Ws_Pnd_Comb_Cnt", "#COMB-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Comb_Gross = pnd_Ws_Pnd_Comb_Grp.newFieldInGroup("pnd_Ws_Pnd_Comb_Gross", "#COMB-GROSS", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Ws_Pnd_Comb_Ivc = pnd_Ws_Pnd_Comb_Grp.newFieldInGroup("pnd_Ws_Pnd_Comb_Ivc", "#COMB-IVC", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Ws_Pnd_Comb_Int = pnd_Ws_Pnd_Comb_Grp.newFieldInGroup("pnd_Ws_Pnd_Comb_Int", "#COMB-INT", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Comb_Fed = pnd_Ws_Pnd_Comb_Grp.newFieldInGroup("pnd_Ws_Pnd_Comb_Fed", "#COMB-FED", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Comb_Nra = pnd_Ws_Pnd_Comb_Grp.newFieldInGroup("pnd_Ws_Pnd_Comb_Nra", "#COMB-NRA", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Comb_Pr = pnd_Ws_Pnd_Comb_Grp.newFieldInGroup("pnd_Ws_Pnd_Comb_Pr", "#COMB-PR", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Comb_State = pnd_Ws_Pnd_Comb_Grp.newFieldInGroup("pnd_Ws_Pnd_Comb_State", "#COMB-STATE", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Super_Dist_Code = localVariables.newFieldInRecord("pnd_Super_Dist_Code", "#SUPER-DIST-CODE", FieldType.STRING, 8);

        pnd_Super_Dist_Code__R_Field_6 = localVariables.newGroupInRecord("pnd_Super_Dist_Code__R_Field_6", "REDEFINE", pnd_Super_Dist_Code);
        pnd_Super_Dist_Code_Pnd_S_Tbl_Nbr = pnd_Super_Dist_Code__R_Field_6.newFieldInGroup("pnd_Super_Dist_Code_Pnd_S_Tbl_Nbr", "#S-TBL-NBR", FieldType.NUMERIC, 
            1);
        pnd_Super_Dist_Code_Pnd_S_Tax_Year = pnd_Super_Dist_Code__R_Field_6.newFieldInGroup("pnd_Super_Dist_Code_Pnd_S_Tax_Year", "#S-TAX-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Super_Dist_Code_Pnd_S_Settl_Type = pnd_Super_Dist_Code__R_Field_6.newFieldInGroup("pnd_Super_Dist_Code_Pnd_S_Settl_Type", "#S-SETTL-TYPE", 
            FieldType.STRING, 1);
        pnd_Super_Dist_Code_Pnd_S_Pay_Type = pnd_Super_Dist_Code__R_Field_6.newFieldInGroup("pnd_Super_Dist_Code_Pnd_S_Pay_Type", "#S-PAY-TYPE", FieldType.STRING, 
            1);
        pnd_Super_Dist_Code_Pnd_S_Dist_Code = pnd_Super_Dist_Code__R_Field_6.newFieldInGroup("pnd_Super_Dist_Code_Pnd_S_Dist_Code", "#S-DIST-CODE", FieldType.STRING, 
            1);
        pnd_Search_Cat_Dist = localVariables.newFieldInRecord("pnd_Search_Cat_Dist", "#SEARCH-CAT-DIST", FieldType.STRING, 2);

        pnd_Search_Cat_Dist__R_Field_7 = localVariables.newGroupInRecord("pnd_Search_Cat_Dist__R_Field_7", "REDEFINE", pnd_Search_Cat_Dist);
        pnd_Search_Cat_Dist_Pnd_Search_Cat = pnd_Search_Cat_Dist__R_Field_7.newFieldInGroup("pnd_Search_Cat_Dist_Pnd_Search_Cat", "#SEARCH-CAT", FieldType.STRING, 
            1);
        pnd_Search_Cat_Dist_Pnd_Search_Dist = pnd_Search_Cat_Dist__R_Field_7.newFieldInGroup("pnd_Search_Cat_Dist_Pnd_Search_Dist", "#SEARCH-DIST", FieldType.STRING, 
            1);
        pnd_Cat_Dist_Table = localVariables.newFieldInRecord("pnd_Cat_Dist_Table", "#CAT-DIST-TABLE", FieldType.STRING, 18);

        pnd_Cat_Dist_Table__R_Field_8 = localVariables.newGroupInRecord("pnd_Cat_Dist_Table__R_Field_8", "REDEFINE", pnd_Cat_Dist_Table);

        pnd_Cat_Dist_Table_Pnd_Cat_Dist_Entries = pnd_Cat_Dist_Table__R_Field_8.newGroupArrayInGroup("pnd_Cat_Dist_Table_Pnd_Cat_Dist_Entries", "#CAT-DIST-ENTRIES", 
            new DbsArrayController(1, 6));
        pnd_Cat_Dist_Table_Pnd_Cat_Dist_Method = pnd_Cat_Dist_Table_Pnd_Cat_Dist_Entries.newFieldInGroup("pnd_Cat_Dist_Table_Pnd_Cat_Dist_Method", "#CAT-DIST-METHOD", 
            FieldType.STRING, 2);

        pnd_Cat_Dist_Table__R_Field_9 = pnd_Cat_Dist_Table_Pnd_Cat_Dist_Entries.newGroupInGroup("pnd_Cat_Dist_Table__R_Field_9", "REDEFINE", pnd_Cat_Dist_Table_Pnd_Cat_Dist_Method);
        pnd_Cat_Dist_Table_Pnd_Pay_Category = pnd_Cat_Dist_Table__R_Field_9.newFieldInGroup("pnd_Cat_Dist_Table_Pnd_Pay_Category", "#PAY-CATEGORY", FieldType.STRING, 
            1);
        pnd_Cat_Dist_Table_Pnd_Dist_Method = pnd_Cat_Dist_Table__R_Field_9.newFieldInGroup("pnd_Cat_Dist_Table_Pnd_Dist_Method", "#DIST-METHOD", FieldType.STRING, 
            1);
        pnd_Cat_Dist_Table_Pnd_Dist_Method_Code = pnd_Cat_Dist_Table_Pnd_Cat_Dist_Entries.newFieldInGroup("pnd_Cat_Dist_Table_Pnd_Dist_Method_Code", "#DIST-METHOD-CODE", 
            FieldType.STRING, 1);
        pnd_Twrpymnt_Tax_Citizenship = localVariables.newFieldInRecord("pnd_Twrpymnt_Tax_Citizenship", "#TWRPYMNT-TAX-CITIZENSHIP", FieldType.STRING, 
            1);
        pnd_Twrpymnt_Paymt_Category = localVariables.newFieldInRecord("pnd_Twrpymnt_Paymt_Category", "#TWRPYMNT-PAYMT-CATEGORY", FieldType.STRING, 1);
        pnd_Twrpymnt_Dist_Method = localVariables.newFieldInRecord("pnd_Twrpymnt_Dist_Method", "#TWRPYMNT-DIST-METHOD", FieldType.STRING, 1);
        pnd_Twrpymnt_Residency_Type = localVariables.newFieldInRecord("pnd_Twrpymnt_Residency_Type", "#TWRPYMNT-RESIDENCY-TYPE", FieldType.STRING, 1);
        pnd_Twrpymnt_Pymnt_Refer_Nbr = localVariables.newFieldInRecord("pnd_Twrpymnt_Pymnt_Refer_Nbr", "#TWRPYMNT-PYMNT-REFER-NBR", FieldType.STRING, 
            2);
        pnd_Twrpymnt_Locality_Cde = localVariables.newFieldInRecord("pnd_Twrpymnt_Locality_Cde", "#TWRPYMNT-LOCALITY-CDE", FieldType.STRING, 3);
        pnd_Twrpymnt_Contract_Seq_No = localVariables.newFieldInRecord("pnd_Twrpymnt_Contract_Seq_No", "#TWRPYMNT-CONTRACT-SEQ-NO", FieldType.NUMERIC, 
            2);
        pnd_Twrt_Locality_Cde = localVariables.newFieldInRecord("pnd_Twrt_Locality_Cde", "#TWRT-LOCALITY-CDE", FieldType.STRING, 3);
        i = localVariables.newFieldInRecord("i", "I", FieldType.PACKED_DECIMAL, 3);
        j = localVariables.newFieldInRecord("j", "J", FieldType.PACKED_DECIMAL, 3);
        k = localVariables.newFieldInRecord("k", "K", FieldType.PACKED_DECIMAL, 3);

        vw_cntl = new DataAccessProgramView(new NameInfo("vw_cntl", "CNTL"), "TIRCNTL_FEEDER_SYS_TBL_VIEW", "TIR_CONTROL");
        cntl_Tircntl_5_Y_Sc_Co_To_Frm_Sp = vw_cntl.getRecord().newFieldInGroup("cntl_Tircntl_5_Y_Sc_Co_To_Frm_Sp", "TIRCNTL-5-Y-SC-CO-TO-FRM-SP", FieldType.STRING, 
            24, RepeatingFieldStrategy.None, "TIRCNTL_5_Y_SC_CO_TO_FRM_SP");
        cntl_Tircntl_5_Y_Sc_Co_To_Frm_Sp.setSuperDescriptor(true);
        registerRecord(vw_cntl);

        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp = localVariables.newFieldInRecord("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp", "#TIRCNTL-5-Y-SC-CO-TO-FRM-SP", FieldType.STRING, 
            24);

        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_10 = localVariables.newGroupInRecord("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_10", "REDEFINE", pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Tbl_5 = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_10.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Tbl_5", 
            "#CNTL-TBL-5", FieldType.STRING, 1);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Tax_Year = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_10.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Tax_Year", 
            "#CNTL-TAX-YEAR", FieldType.STRING, 4);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Source = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_10.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Source", 
            "#CNTL-SOURCE", FieldType.STRING, 2);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Company = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_10.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Company", 
            "#CNTL-COMPANY", FieldType.STRING, 1);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_To_Date = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_10.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_To_Date", 
            "#CNTL-TO-DATE", FieldType.STRING, 8);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Interface_Date = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_10.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Interface_Date", 
            "#CNTL-INTERFACE-DATE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cntl.reset();

        ldaTwrl0700.initializeValues();
        ldaTwrl9804.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Comp_Max.setInitialValue(6);
        pnd_Ws_Const_Source_Max.setInitialValue(50);
        pnd_Ws_Const_Err_Max.setInitialValue(60);
        pnd_Ws_Pnd_Comp_Name.getValue(0 + 1).setInitialValue("????");
        pnd_Ws_Pnd_Er_Su.setInitialValue(70000000);
        pnd_Ws_Pnd_So_Su.setInitialValue("5");
        pnd_Ws_Pnd_Dist_Su.setInitialValue("1");
        pnd_Ws_Pnd_St_Su.setInitialValue("2");
        pnd_Ws_Pnd_Cn_Su.setInitialValue("3");
        pnd_Cat_Dist_Table.setInitialValue("PCAPRBLCDLRERCDRRE");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0706() throws Exception
    {
        super("Twrp0706");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* **
        //*  REJECTED RECORDS REPORT                                                                                                                                      //Natural: FORMAT ( 00 ) LS = 133 PS = 60
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) LS = 133 PS = 61
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 01 RECORD TWRT-RECORD
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaTwrl0700.getTwrt_Record())))
        {
            CheckAtStartofData378();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            pnd_Ws_Pnd_Rec_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #REC-CNT
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //*                                                                                                                                                           //Natural: AT BREAK OF TWRT-COMPANY-CDE
            //*                                                                                                                                                           //Natural: AT BREAK OF TWRT-SOURCE
            if (condition(pnd_Ws_Pnd_Comp_Break.getBoolean()))                                                                                                            //Natural: IF #WS.#COMP-BREAK
            {
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY
                sub_Get_Company();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ws_Pnd_Comp_Break.reset();                                                                                                                            //Natural: RESET #WS.#COMP-BREAK
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_Source_Break.getBoolean()))                                                                                                          //Natural: IF #WS.#SOURCE-BREAK
            {
                                                                                                                                                                          //Natural: PERFORM GET-SOURCE-IDX
                sub_Get_Source_Idx();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ws_Pnd_Source_Break.reset();                                                                                                                          //Natural: RESET #WS.#SOURCE-BREAK
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Rej_Gross.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                                            //Natural: ADD TWRT-GROSS-AMT TO #REJ-GROSS ( #COMP-IDX )
            pnd_Ws_Pnd_Rej_Int.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt());                                           //Natural: ADD TWRT-INTEREST-AMT TO #REJ-INT ( #COMP-IDX )
            pnd_Ws_Pnd_Rej_Fed.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt());                                          //Natural: ADD TWRT-FED-WHHLD-AMT TO #REJ-FED ( #COMP-IDX )
            pnd_Ws_Pnd_Rej_Nra.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt());                                          //Natural: ADD TWRT-NRA-WHHLD-AMT TO #REJ-NRA ( #COMP-IDX )
            pnd_Ws_Pnd_Rej_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(pnd_Ws_Pnd_Puri);                                                                           //Natural: ADD #PURI TO #REJ-PR ( #COMP-IDX )
            pnd_Ws_Pnd_Rej_Ivc.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt());                                                //Natural: ADD TWRT-IVC-AMT TO #REJ-IVC ( #COMP-IDX )
            pnd_Ws_Pnd_Rej_State.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(pnd_Ws_Pnd_State_Amt);                                                                   //Natural: ADD #STATE-AMT TO #REJ-STATE ( #COMP-IDX )
            pnd_Ws_Pnd_Rej_Cnt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(1);                                                                                        //Natural: ADD 1 TO #REJ-CNT ( #COMP-IDX )
            if (condition(getReports().getAstLineCount(1).greater(58)))                                                                                                   //Natural: IF *LINE-COUNT ( 01 ) > 58
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 01 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("42") || ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("PR") ||                   //Natural: IF TWRT-STATE-RSDNCY = '42' OR = 'PR' OR = 'RQ'
                ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("RQ")))
            {
                pnd_Ws_Pnd_Puri.setValue(ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt());                                                                              //Natural: ASSIGN #PURI := TWRT-STATE-WHHLD-AMT
                pnd_Ws_Pnd_State_Amt.reset();                                                                                                                             //Natural: RESET #STATE-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Puri.reset();                                                                                                                                  //Natural: RESET #PURI
                pnd_Ws_Pnd_State_Amt.setValue(ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt());                                                                         //Natural: ASSIGN #STATE-AMT := TWRT-STATE-WHHLD-AMT
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr(),ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde(),new               //Natural: WRITE ( 01 ) / TWRT-CNTRCT-NBR TWRT-PAYEE-CDE 1X TWRT-PAYMT-DTE TWRT-TAX-ID TWRT-TAX-ID-TYPE TWRT-GROSS-AMT TWRT-INTEREST-AMT 2X TWRT-IVC-AMT 3X TWRT-FED-WHHLD-AMT #STATE-AMT 4X TWRT-STATE-RSDNCY TWRT-CITIZENSHIP 2X TWRT-PLAN-NUM 2X TWRT-ORIG-CONTRACT-NBR
                ColumnSpacing(1),ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Dte(),ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id(),ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type(),ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt(),new 
                ColumnSpacing(2),ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt(),new ColumnSpacing(3),ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt(),pnd_Ws_Pnd_State_Amt,new 
                ColumnSpacing(4),ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy(),ldaTwrl0700.getTwrt_Record_Twrt_Citizenship(),new ColumnSpacing(2),ldaTwrl0700.getTwrt_Record_Twrt_Plan_Num(),new 
                ColumnSpacing(2),ldaTwrl0700.getTwrt_Record_Twrt_Orig_Contract_Nbr());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  WRITE (01) 101X #PURI 13X TWRT-LOB-CDE
            getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(76),ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt(),pnd_Ws_Pnd_Puri,new ColumnSpacing(12),ldaTwrl0700.getTwrt_Record_Twrt_Subplan(),new  //Natural: WRITE ( 01 ) 76X TWRT-NRA-WHHLD-AMT #PURI 12X TWRT-SUBPLAN 2X TWRT-ORIG-SUBPLAN
                ColumnSpacing(2),ldaTwrl0700.getTwrt_Record_Twrt_Orig_Subplan());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_Rec_Cnt.equals(getZero())))                                                                                                              //Natural: IF #REC-CNT = 0
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"*** Input File is empty, no Monthly Report had been produced"," ***",NEWLINE,             //Natural: WRITE ( 1 ) /// '*** Input File is empty, no Monthly Report had been produced' ' ***' ///
                NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*** Total Rejected Records For This Month:",pnd_Ws_Pnd_Rec_Cnt, new ReportEditMask                //Natural: WRITE ( 01 ) // '*** Total Rejected Records For This Month:' #REC-CNT
                ("-Z,ZZZ,ZZ9"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-COMPANY
        //* ******************************
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-COMPANY
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-SOURCE-TABLE
        //* ***********************************
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-SOURCE-IDX
        //* *******************
        //* *******************
        //* *****************
        //*  REPORT HEADERS *
        //* *****************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 01 )
        //* **
    }
    private void sub_Load_Company() throws Exception                                                                                                                      //Natural: LOAD-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #COMP-IDX = 1 TO COMP-MAX
        for (pnd_Ws_Pnd_Comp_Idx.setValue(1); condition(pnd_Ws_Pnd_Comp_Idx.lessOrEqual(pnd_Ws_Const_Comp_Max)); pnd_Ws_Pnd_Comp_Idx.nadd(1))
        {
            //*  RCC
            short decideConditionsMet518 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #COMP-IDX;//Natural: VALUE 1
            if (condition((pnd_Ws_Pnd_Comp_Idx.equals(1))))
            {
                decideConditionsMet518++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("L");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'L'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Ws_Pnd_Comp_Idx.equals(2))))
            {
                decideConditionsMet518++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("S");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'S'
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Ws_Pnd_Comp_Idx.equals(3))))
            {
                decideConditionsMet518++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("T");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'T'
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pnd_Ws_Pnd_Comp_Idx.equals(4))))
            {
                decideConditionsMet518++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("X");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'X'
            }                                                                                                                                                             //Natural: VALUE 5
            else if (condition((pnd_Ws_Pnd_Comp_Idx.equals(5))))
            {
                decideConditionsMet518++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("C");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'C'
            }                                                                                                                                                             //Natural: VALUE 6
            else if (condition((pnd_Ws_Pnd_Comp_Idx.equals(6))))
            {
                decideConditionsMet518++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("F");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'F'
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet518 > 0))
            {
                DbsUtil.callnat(Twrncomp.class , getCurrentProcessState(), pdaTwracomp.getPnd_Twracomp_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNCOMP' USING #TWRACOMP.#INPUT-PARMS ( AD = O ) #TWRACOMP.#OUTPUT-DATA ( AD = M )
                    pdaTwracomp.getPnd_Twracomp_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                if (condition(pdaTwracomp.getPnd_Twracomp_Pnd_Ret_Code().getBoolean()))                                                                                   //Natural: IF #TWRACOMP.#RET-CODE
                {
                    pnd_Ws_Pnd_Comp_Name.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name());                          //Natural: ASSIGN #WS.#COMP-NAME ( #COMP-IDX ) := #TWRACOMP.#COMP-SHORT-NAME
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_Comp_Idx.reset();                                                                                                                          //Natural: RESET #COMP-IDX
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Get_Company() throws Exception                                                                                                                       //Natural: GET-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        //*  RCC
        short decideConditionsMet549 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF TWRT-COMPANY-CDE;//Natural: VALUE 'L'
        if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().equals("L"))))
        {
            decideConditionsMet549++;
            pnd_Ws_Pnd_Comp_Idx.setValue(1);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 1
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().equals("S"))))
        {
            decideConditionsMet549++;
            pnd_Ws_Pnd_Comp_Idx.setValue(2);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 2
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().equals("T"))))
        {
            decideConditionsMet549++;
            pnd_Ws_Pnd_Comp_Idx.setValue(3);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 3
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().equals("X"))))
        {
            decideConditionsMet549++;
            pnd_Ws_Pnd_Comp_Idx.setValue(4);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 4
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().equals("C"))))
        {
            decideConditionsMet549++;
            pnd_Ws_Pnd_Comp_Idx.setValue(5);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 5
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().equals("F"))))
        {
            decideConditionsMet549++;
            pnd_Ws_Pnd_Comp_Idx.setValue(6);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 6
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Ws_Pnd_Comp_Idx.reset();                                                                                                                                  //Natural: RESET #COMP-IDX
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Ws_Pnd_Comp_Name_Disp.setValue(pnd_Ws_Pnd_Comp_Name.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1));                                                              //Natural: ASSIGN #COMP-NAME-DISP := #WS.#COMP-NAME ( #COMP-IDX )
    }
    //*  TABLE 5 - FEEDER SYSTEM TABLE
    private void sub_Load_Source_Table() throws Exception                                                                                                                 //Natural: LOAD-SOURCE-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_So_Yr.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year());                                                                                            //Natural: ASSIGN #SO-YR := TWRT-TAX-YEAR
        ldaTwrl9804.getVw_tircntl_Feeder_Sys_Tbl_View_View().startDatabaseRead                                                                                            //Natural: READ TIRCNTL-FEEDER-SYS-TBL-VIEW-VIEW BY TIRCNTL-5-Y-SC-CO-TO-FRM-SP = #SO-SU
        (
        "READ02",
        new Wc[] { new Wc("TIRCNTL_5_Y_SC_CO_TO_FRM_SP", ">=", pnd_Ws_Pnd_So_Su, WcType.BY) },
        new Oc[] { new Oc("TIRCNTL_5_Y_SC_CO_TO_FRM_SP", "ASC") }
        );
        READ02:
        while (condition(ldaTwrl9804.getVw_tircntl_Feeder_Sys_Tbl_View_View().readNextRow("READ02")))
        {
            if (condition(ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Tbl_Nbr().equals(5) && ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Tax_Year().equals(pnd_Ws_Pnd_So_Yr))) //Natural: IF TIRCNTL-TBL-NBR = 5 AND TIRCNTL-TAX-YEAR = #SO-YR
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Company_Cde().equals(" "))))                                                          //Natural: ACCEPT IF TIRCNTL-COMPANY-CDE = ' '
            {
                continue;
            }
            pnd_Ws_Pnd_T_Source_Cnt.nadd(1);                                                                                                                              //Natural: ADD 1 TO #T-SOURCE-CNT
            pnd_Ws_Pnd_T_Source_Code.getValue(pnd_Ws_Pnd_T_Source_Cnt).setValue(ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Rpt_Source_Code());               //Natural: ASSIGN #T-SOURCE-CODE ( #T-SOURCE-CNT ) := TIRCNTL-RPT-SOURCE-CODE
            pnd_Ws_Pnd_T_Source_Name.getValue(pnd_Ws_Pnd_T_Source_Cnt).setValue(ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Rpt_Source_Code_Desc());          //Natural: ASSIGN #T-SOURCE-NAME ( #T-SOURCE-CNT ) := TIRCNTL-RPT-SOURCE-CODE-DESC
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_T_Source_Cnt.equals(getZero())))                                                                                                         //Natural: IF #T-SOURCE-CNT = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Source Code Table For Tax Year:",ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year(),"Is Missing",new  //Natural: WRITE ( 0 ) '***' 25T 'Source Code Table For Tax Year:' TWRT-TAX-YEAR 'Is Missing' 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(107);  if (true) return;                                                                                                                    //Natural: TERMINATE 107
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Source_Idx() throws Exception                                                                                                                    //Natural: GET-SOURCE-IDX
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        DbsUtil.examine(new ExamineSource(pnd_Ws_Pnd_T_Source_Code.getValue(1,":",pnd_Ws_Pnd_T_Source_Cnt),true), new ExamineSearch(ldaTwrl0700.getTwrt_Record_Twrt_Source(),  //Natural: EXAMINE FULL #T-SOURCE-CODE ( 1:#T-SOURCE-CNT ) FOR FULL TWRT-SOURCE GIVING INDEX IN #WS.#SOURCE-IDX
            true), new ExamineGivingIndex(pnd_Ws_Pnd_Source_Idx));
        if (condition(pnd_Ws_Pnd_Source_Idx.equals(getZero())))                                                                                                           //Natural: IF #WS.#SOURCE-IDX = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"INVALID SOURCE CODE",ldaTwrl0700.getTwrt_Record_Twrt_Source(),new TabSetting(77),        //Natural: WRITE ( 0 ) '***' 25T 'INVALID SOURCE CODE' TWRT-SOURCE 77T '***' /
                "***",NEWLINE);
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(112);  if (true) return;                                                                                                                    //Natural: TERMINATE 112
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 0 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 0 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 0 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    if (condition(pnd_Ws_Pnd_Source_Idx.greater(getZero())))                                                                                              //Natural: IF #SOURCE-IDX GT 0
                    {
                        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name:",Global.getINIT_USER(),NEWLINE,"Program:",Global.getPROGRAM(),"- 01",new //Natural: WRITE ( 01 ) NOTITLE NOHDR 'Job Name:' *INIT-USER / 'Program:' *PROGRAM '- 01' 16X 'Tax Reporting And Withholding System' 30X 'Page:   ' *PAGE-NUMBER ( 01 ) / 'System -' #T-SOURCE-NAME ( #SOURCE-IDX ) 8X 'Monthly Rejected Tax Transactions' 31X 'Date: '*DATU / 54X #WS.#COMP-NAME-DISP / / 'Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest' '      IVC         FED TAX      STATE    Res Ctz  Plan    Orgntng' / 'Number   Code  Date    Ind.ID No  Type     Amount       Amount' '      Amount      NRA Amt      Amount   Cde Cde  SubPlan Cntrct/' / '                                                              ' '                            PUERTO-RICO                  Subplan'
                            ColumnSpacing(16),"Tax Reporting And Withholding System",new ColumnSpacing(30),"Page:   ",getReports().getPageNumberDbs(1),NEWLINE,"System -",pnd_Ws_Pnd_T_Source_Name.getValue(pnd_Ws_Pnd_Source_Idx),new 
                            ColumnSpacing(8),"Monthly Rejected Tax Transactions",new ColumnSpacing(31),"Date: ",Global.getDATU(),NEWLINE,new ColumnSpacing(54),
                            pnd_Ws_Pnd_Comp_Name_Disp,NEWLINE,NEWLINE,"Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest","      IVC         FED TAX      STATE    Res Ctz  Plan    Orgntng",
                            NEWLINE,"Number   Code  Date    Ind.ID No  Type     Amount       Amount","      Amount      NRA Amt      Amount   Cde Cde  SubPlan Cntrct/",
                            NEWLINE,"                                                              ","                            PUERTO-RICO                  Subplan");
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaTwrl0700_getTwrt_Record_Twrt_Company_CdeIsBreak = ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().isBreak(endOfData);
        boolean ldaTwrl0700_getTwrt_Record_Twrt_SourceIsBreak = ldaTwrl0700.getTwrt_Record_Twrt_Source().isBreak(endOfData);
        if (condition(ldaTwrl0700_getTwrt_Record_Twrt_Company_CdeIsBreak || ldaTwrl0700_getTwrt_Record_Twrt_SourceIsBreak))
        {
            //*  ----------------------------
            //*  COMPANY BREAK PROCESSING
            //*  PRINT REJECTED REPORT FOR THE PREVIOUS COMPANY
            //*  ----------------------------
            if (condition(pnd_Ws_Pnd_Rej_Cnt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).greater(getZero())))                                                              //Natural: IF #REJ-CNT ( #COMP-IDX ) > 0
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"*",new RepeatItem(131),NEWLINE,"* Totals By Company:",new ColumnSpacing(15),pnd_Ws_Pnd_Rej_Gross.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1), //Natural: WRITE ( 01 ) / '*' ( 131 ) / '* Totals By Company:' 15X #REJ-GROSS ( #COMP-IDX ) #REJ-INT ( #COMP-IDX ) #REJ-IVC ( #COMP-IDX ) #REJ-FED ( #COMP-IDX ) #REJ-NRA ( #COMP-IDX ) #REJ-STATE ( #COMP-IDX )
                    pnd_Ws_Pnd_Rej_Int.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Ws_Pnd_Rej_Ivc.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Ws_Pnd_Rej_Fed.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),
                    pnd_Ws_Pnd_Rej_Nra.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Ws_Pnd_Rej_State.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1));
                if (condition(Global.isEscape())) return;
                if (condition(pnd_Ws_Pnd_Rej_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).greater(getZero())))                                                           //Natural: IF #REJ-PR ( #COMP-IDX ) > 0
                {
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(101),pnd_Ws_Pnd_Rej_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1));                      //Natural: WRITE ( 01 ) 101X #REJ-PR ( #COMP-IDX )
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,"* Total Rejected Records by Comp:",new ColumnSpacing(4),pnd_Ws_Pnd_Rej_Cnt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),  //Natural: WRITE ( 01 ) '* Total Rejected Records by Comp:' 4X #REJ-CNT ( #COMP-IDX )
                    new ReportEditMask ("-ZZZ,ZZ9"));
                if (condition(Global.isEscape())) return;
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 01 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Comp_Break.setValue(true);                                                                                                                         //Natural: ASSIGN #WS.#COMP-BREAK := TRUE
            //*  END OF COMPANY BREAK PROCESSING
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaTwrl0700_getTwrt_Record_Twrt_SourceIsBreak))
        {
            //* *-----------------------
            //*  SOURCE CODE BREAK PROCESSING
            //*  PRINT REJECTED REPORTS FOR THE PREVIOUS SOURCE CODE
            //* *
            pnd_Ws_Pnd_Comb_Gross.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Gross), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rej_Gross.getValue("*")));           //Natural: COMPUTE #COMB-GROSS = 0.00 + #REJ-GROSS ( * )
            pnd_Ws_Pnd_Comb_Int.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Int), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rej_Int.getValue("*")));                 //Natural: COMPUTE #COMB-INT = 0.00 + #REJ-INT ( * )
            pnd_Ws_Pnd_Comb_Ivc.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Ivc), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rej_Ivc.getValue("*")));                 //Natural: COMPUTE #COMB-IVC = 0.00 + #REJ-IVC ( * )
            pnd_Ws_Pnd_Comb_Fed.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Fed), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rej_Fed.getValue("*")));                 //Natural: COMPUTE #COMB-FED = 0.00 + #REJ-FED ( * )
            pnd_Ws_Pnd_Comb_Nra.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Nra), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rej_Nra.getValue("*")));                 //Natural: COMPUTE #COMB-NRA = 0.00 + #REJ-NRA ( * )
            pnd_Ws_Pnd_Comb_State.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_State), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rej_State.getValue("*")));           //Natural: COMPUTE #COMB-STATE = 0.00 + #REJ-STATE ( * )
            pnd_Ws_Pnd_Comb_Pr.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Pr), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rej_Pr.getValue("*")));                    //Natural: COMPUTE #COMB-PR = 0.00 + #REJ-PR ( * )
            pnd_Ws_Pnd_Comb_Cnt.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Cnt), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rej_Cnt.getValue("*")));                 //Natural: COMPUTE #COMB-CNT = 0.00 + #REJ-CNT ( * )
            if (condition(pnd_Ws_Pnd_Rej_Cnt.getValue("*").greater(getZero())))                                                                                           //Natural: IF #REJ-CNT ( * ) > 0
            {
                pnd_Ws_Pnd_Comp_Name_Disp.reset();                                                                                                                        //Natural: RESET #WS.#COMP-NAME-DISP
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(131),NEWLINE,"** Totals By Source Code:",new ColumnSpacing(9),              //Natural: WRITE ( 01 ) // '*' ( 131 ) / '** Totals By Source Code:' 9X #COMB-GROSS #COMB-INT #COMB-IVC #COMB-FED #COMB-NRA #COMB-STATE
                    pnd_Ws_Pnd_Comb_Gross,pnd_Ws_Pnd_Comb_Int,pnd_Ws_Pnd_Comb_Ivc,pnd_Ws_Pnd_Comb_Fed,pnd_Ws_Pnd_Comb_Nra,pnd_Ws_Pnd_Comb_State);
                if (condition(Global.isEscape())) return;
                if (condition(pnd_Ws_Pnd_Comb_Pr.greater(getZero())))                                                                                                     //Natural: IF #COMB-PR > 0
                {
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(101),pnd_Ws_Pnd_Comb_Pr);                                                                //Natural: WRITE ( 01 ) 101X #COMB-PR
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,"** Total Rejected Records by SC:",new ColumnSpacing(3),pnd_Ws_Pnd_Comb_Cnt, new ReportEditMask                //Natural: WRITE ( 01 ) '** Total Rejected Records by SC:' 3X #COMB-CNT
                    ("-Z,ZZZ,ZZ9"));
                if (condition(Global.isEscape())) return;
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 01 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Rej_Grp.getValue("*").reset();                                                                                                                     //Natural: RESET #REJ-GRP ( * )
            pnd_Ws_Pnd_Source_Break.setValue(true);                                                                                                                       //Natural: ASSIGN #SOURCE-BREAK := TRUE
            //*  END OF SOURCE CODE BREAK PROCESSING
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60");
        Global.format(1, "LS=133 PS=61");
    }
    private void CheckAtStartofData378() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM LOAD-COMPANY
            sub_Load_Company();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY
            sub_Get_Company();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM LOAD-SOURCE-TABLE
            sub_Load_Source_Table();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-SOURCE-IDX
            sub_Get_Source_Idx();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Pnd_Comp_Break.setValue(true);                                                                                                                         //Natural: ASSIGN #WS.#COMP-BREAK := #WS.#SOURCE-BREAK := TRUE
            pnd_Ws_Pnd_Source_Break.setValue(true);
        }                                                                                                                                                                 //Natural: END-START
    }
}
