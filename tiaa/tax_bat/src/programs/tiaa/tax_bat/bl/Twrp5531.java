/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:41:15 PM
**        * FROM NATURAL PROGRAM : Twrp5531
************************************************************
**        * FILE NAME            : Twrp5531.java
**        * CLASS NAME           : Twrp5531
**        * INSTANCE NAME        : Twrp5531
************************************************************
************************************************************************
** PROGRAM : TWRP5531
** SYSTEM  : TAXWARS
** AUTHOR  : MICHAEL SUPONITSKY
** FUNCTION: RECONCILIATION REPORT FOR 1099-R AND 1099-INT FROM THE
**           PAYMENT FILE. (TAX-CITIZENSHIP = U.
** HISTORY :
** ----------------------------------------------
** 12/06/05 - BK. TAX YEAR ADDED TO TWRNCOMP PARMS
** 09/06/2005 - MS. - COPY OF TWRP5501. USED FOR TAX YEAR 2004
**                    RECOMPILED BECAUSE OF CHANGES IN TWRA4001
** 11/12/2004 - M. SUPONITSKY - TOPS - TRUST & TIAA - >= 2004.
** 07/2012    - J. BREMER     - RESTOW FOR CUNY2 - IAAATXIA
** 02/18/15: OS - RECOMPILED FOR UPDATED TWRL0900
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5531 extends BLNatBase
{
    // Data Areas
    private PdaIaaatxia pdaIaaatxia;
    private PdaTwracomp pdaTwracomp;
    private PdaTwratbl2 pdaTwratbl2;
    private PdaTwra4001 pdaTwra4001;
    private LdaTwrl0900 ldaTwrl0900;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;
    private DbsField pnd_Ws_Const_Pnd_Cntl_Max;
    private DbsField pnd_Ws_Const_Pnd_St_Tot_Max;
    private DbsField pnd_Ws_Const_Pnd_1099r_Max;

    private DbsGroup pnd_Key_Detail;
    private DbsField pnd_Key_Detail_Twrpymnt_Tax_Year;
    private DbsField pnd_Key_Detail_Twrpymnt_Company_Cde_Form;
    private DbsField pnd_Key_Detail_Twrpymnt_Tax_Id_Nbr;
    private DbsField pnd_Key_Detail_Twrpymnt_Contract_Nbr;
    private DbsField pnd_Key_Detail_Twrpymnt_Payee_Cde;
    private DbsField pnd_Key_Detail_Twrpymnt_Distribution_Cde;
    private DbsField pnd_Key_Detail_Twrpymnt_Paymt_Category;
    private DbsField pnd_Key_Detail_Twrpymnt_Residency_Type;
    private DbsField pnd_Key_Detail_Twrpymnt_Residency_Code;

    private DbsGroup pnd_Key_Detail__R_Field_1;
    private DbsField pnd_Key_Detail_Pnd_Key;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_Company_Line;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Ws_Pnd_Idx;
    private DbsField pnd_Ws_Pnd_S1;
    private DbsField pnd_Ws_Pnd_S2;
    private DbsField pnd_Ws_Pnd_Contract_Type;
    private DbsField pnd_Ws_Pnd_Contract_Type_Mixed;
    private DbsField pnd_Ws_Pnd_New_Res;
    private DbsField pnd_Ws_Pnd_Tran_Cv;
    private DbsField pnd_Ws_Pnd_Gross_Cv;
    private DbsField pnd_Ws_Pnd_Ivc_Cv;
    private DbsField pnd_Ws_Pnd_Taxable_Cv;
    private DbsField pnd_Ws_Pnd_Fed_Cv;
    private DbsField pnd_Ws_Pnd_Sta_Cv;
    private DbsField pnd_Ws_Pnd_Loc_Cv;
    private DbsField pnd_Ws_Pnd_Int_Cv;
    private DbsField pnd_Ws_Pnd_Int_Back_Cv;
    private DbsField pnd_Ws_Pnd_1099;

    private DbsGroup pnd_Case_Fields;
    private DbsField pnd_Case_Fields_Pnd_Roll;
    private DbsField pnd_Case_Fields_Pnd_Rechar;

    private DbsGroup pnd_Case_Fields_Pnd_1099_Cases;
    private DbsField pnd_Case_Fields_Pnd_Payment_Type;
    private DbsField pnd_Case_Fields_Pnd_Settle_Type;
    private DbsField pnd_Case_Fields_Pnd_Case_Present;
    private DbsField pnd_Case_Fields_Pnd_Tnd;
    private DbsField pnd_Case_Fields_Pnd_Trad_Clas_Dist;
    private DbsField pnd_Case_Fields_Pnd_Gross_Amt;
    private DbsField pnd_Case_Fields_Pnd_Ivc_Amt;

    private DbsGroup pnd_St_Case;
    private DbsField pnd_St_Case_Pnd_Case_Present;
    private DbsField pnd_St_Case_Pnd_Tnd;
    private DbsField pnd_St_Case_Pnd_Tran_Cnt;
    private DbsField pnd_St_Case_Pnd_Gross_Amt;
    private DbsField pnd_St_Case_Pnd_Ivc_Amt;
    private DbsField pnd_St_Case_Pnd_Ivc_Adj;
    private DbsField pnd_St_Case_Pnd_Taxable_Amt;
    private DbsField pnd_St_Case_Pnd_Taxable_Adj;
    private DbsField pnd_St_Case_Pnd_Sta_Tax;
    private DbsField pnd_St_Case_Pnd_Loc_Tax;
    private DbsField pnd_St_Case_Pnd_State_Distr;
    private DbsField pnd_St_Case_Pnd_Reportable_Distr;

    private DbsGroup pnd_Prev_Val;
    private DbsField pnd_Prev_Val_Pnd_Paymt_Category;
    private DbsField pnd_Prev_Val_Pnd_Contract_Nbr;
    private DbsField pnd_Prev_Val_Pnd_Payee_Cde;

    private DbsGroup pnd_Prev_Val__R_Field_2;
    private DbsField pnd_Prev_Val_Pnd_Payee_Cde_N;
    private DbsField pnd_Prev_Val_Pnd_Distr_Cde;

    private DbsGroup pnd_Cntl;
    private DbsField pnd_Cntl_Pnd_Cntl_Text;
    private DbsField pnd_Cntl_Pnd_Cntl_Text1;
    private DbsField pnd_Cntl_Pnd_Tran_Cnt_Cv;
    private DbsField pnd_Cntl_Pnd_Gross_Amt_Cv;
    private DbsField pnd_Cntl_Pnd_Ivc_Amt_Cv;
    private DbsField pnd_Cntl_Pnd_Taxable_Amt_Cv;
    private DbsField pnd_Cntl_Pnd_Fed_Tax_Cv;
    private DbsField pnd_Cntl_Pnd_Sta_Tax_Cv;
    private DbsField pnd_Cntl_Pnd_Loc_Tax_Cv;
    private DbsField pnd_Cntl_Pnd_Int_Amt_Cv;
    private DbsField pnd_Cntl_Pnd_Int_Back_Tax_Cv;

    private DbsGroup pnd_Cntl_Pnd_Totals;
    private DbsField pnd_Cntl_Pnd_Tran_Cnt;
    private DbsField pnd_Cntl_Pnd_Gross_Amt;
    private DbsField pnd_Cntl_Pnd_Ivc_Amt;
    private DbsField pnd_Cntl_Pnd_Taxable_Amt;
    private DbsField pnd_Cntl_Pnd_Fed_Tax;
    private DbsField pnd_Cntl_Pnd_Sta_Tax;
    private DbsField pnd_Cntl_Pnd_Loc_Tax;
    private DbsField pnd_Cntl_Pnd_Int_Amt;
    private DbsField pnd_Cntl_Pnd_Int_Tax;

    private DbsGroup pnd_St_Cntl;
    private DbsField pnd_St_Cntl_Pnd_State_Desc;
    private DbsField pnd_St_Cntl_Pnd_State_Rule;
    private DbsField pnd_St_Cntl_Pnd_State_Cmb;
    private DbsField pnd_St_Cntl_Pnd_State_Med;
    private DbsField pnd_St_Cntl_Pnd_State_Gross_Rpt;

    private DbsGroup pnd_St_Tot;
    private DbsField pnd_St_Tot_Pnd_Tran_Cnt;
    private DbsField pnd_St_Tot_Pnd_Gross_Amt;
    private DbsField pnd_St_Tot_Pnd_Pymnt_Ivc;
    private DbsField pnd_St_Tot_Pnd_Ivc_Adj;
    private DbsField pnd_St_Tot_Pnd_Form_Ivc;
    private DbsField pnd_St_Tot_Pnd_Sta_Tax;
    private DbsField pnd_St_Tot_Pnd_Loc_Tax;

    private DbsGroup pnd_St_To1;
    private DbsField pnd_St_To1_Pnd_Pmnt_Taxable;
    private DbsField pnd_St_To1_Pnd_Taxable_Adj;
    private DbsField pnd_St_To1_Pnd_Form_Taxable;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Twrpymnt_Distribution_CdeOld;
    private DbsField readWork01Twrpymnt_Paymt_CategoryOld;
    private DbsField readWork01Twrpymnt_Contract_NbrOld;
    private DbsField readWork01Twrpymnt_Payee_CdeOld;
    private DbsField readWork01Twrpymnt_Company_Cde_FormOld;
    private DbsField readWork01Twrpymnt_Tax_YearOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaaatxia = new PdaIaaatxia(localVariables);
        pdaTwracomp = new PdaTwracomp(localVariables);
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        pdaTwra4001 = new PdaTwra4001(localVariables);
        ldaTwrl0900 = new LdaTwrl0900();
        registerRecord(ldaTwrl0900);

        // Local Variables

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_Pnd_Cntl_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_Cntl_Max", "#CNTL-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Const_Pnd_St_Tot_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_St_Tot_Max", "#ST-TOT-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Const_Pnd_1099r_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_1099r_Max", "#1099R-MAX", FieldType.PACKED_DECIMAL, 3);

        pnd_Key_Detail = localVariables.newGroupInRecord("pnd_Key_Detail", "#KEY-DETAIL");
        pnd_Key_Detail_Twrpymnt_Tax_Year = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Twrpymnt_Tax_Year", "TWRPYMNT-TAX-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Key_Detail_Twrpymnt_Company_Cde_Form = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Twrpymnt_Company_Cde_Form", "TWRPYMNT-COMPANY-CDE-FORM", 
            FieldType.STRING, 1);
        pnd_Key_Detail_Twrpymnt_Tax_Id_Nbr = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Twrpymnt_Tax_Id_Nbr", "TWRPYMNT-TAX-ID-NBR", FieldType.STRING, 
            10);
        pnd_Key_Detail_Twrpymnt_Contract_Nbr = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Twrpymnt_Contract_Nbr", "TWRPYMNT-CONTRACT-NBR", FieldType.STRING, 
            8);
        pnd_Key_Detail_Twrpymnt_Payee_Cde = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Twrpymnt_Payee_Cde", "TWRPYMNT-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Key_Detail_Twrpymnt_Distribution_Cde = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Twrpymnt_Distribution_Cde", "TWRPYMNT-DISTRIBUTION-CDE", 
            FieldType.STRING, 2);
        pnd_Key_Detail_Twrpymnt_Paymt_Category = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Twrpymnt_Paymt_Category", "TWRPYMNT-PAYMT-CATEGORY", FieldType.STRING, 
            1);
        pnd_Key_Detail_Twrpymnt_Residency_Type = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Twrpymnt_Residency_Type", "TWRPYMNT-RESIDENCY-TYPE", FieldType.STRING, 
            1);
        pnd_Key_Detail_Twrpymnt_Residency_Code = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Twrpymnt_Residency_Code", "TWRPYMNT-RESIDENCY-CODE", FieldType.STRING, 
            2);

        pnd_Key_Detail__R_Field_1 = localVariables.newGroupInRecord("pnd_Key_Detail__R_Field_1", "REDEFINE", pnd_Key_Detail);
        pnd_Key_Detail_Pnd_Key = pnd_Key_Detail__R_Field_1.newFieldInGroup("pnd_Key_Detail_Pnd_Key", "#KEY", FieldType.STRING, 31);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Tax_Year = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Company_Line = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Line", "#COMPANY-LINE", FieldType.STRING, 72);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Idx", "#IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S1", "#S1", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S2", "#S2", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Contract_Type = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Contract_Type", "#CONTRACT-TYPE", FieldType.STRING, 5);
        pnd_Ws_Pnd_Contract_Type_Mixed = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Contract_Type_Mixed", "#CONTRACT-TYPE-MIXED", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_New_Res = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_New_Res", "#NEW-RES", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Tran_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tran_Cv", "#TRAN-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Gross_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Gross_Cv", "#GROSS-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Ivc_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ivc_Cv", "#IVC-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Taxable_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Taxable_Cv", "#TAXABLE-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Fed_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Fed_Cv", "#FED-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Sta_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Sta_Cv", "#STA-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Loc_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Loc_Cv", "#LOC-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Int_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Int_Cv", "#INT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Int_Back_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Int_Back_Cv", "#INT-BACK-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_1099 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_1099", "#1099", FieldType.PACKED_DECIMAL, 3);

        pnd_Case_Fields = localVariables.newGroupInRecord("pnd_Case_Fields", "#CASE-FIELDS");
        pnd_Case_Fields_Pnd_Roll = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Roll", "#ROLL", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Rechar = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Rechar", "#RECHAR", FieldType.BOOLEAN, 1);

        pnd_Case_Fields_Pnd_1099_Cases = pnd_Case_Fields.newGroupArrayInGroup("pnd_Case_Fields_Pnd_1099_Cases", "#1099-CASES", new DbsArrayController(1, 
            5));
        pnd_Case_Fields_Pnd_Payment_Type = pnd_Case_Fields_Pnd_1099_Cases.newFieldInGroup("pnd_Case_Fields_Pnd_Payment_Type", "#PAYMENT-TYPE", FieldType.STRING, 
            1);
        pnd_Case_Fields_Pnd_Settle_Type = pnd_Case_Fields_Pnd_1099_Cases.newFieldInGroup("pnd_Case_Fields_Pnd_Settle_Type", "#SETTLE-TYPE", FieldType.STRING, 
            1);
        pnd_Case_Fields_Pnd_Case_Present = pnd_Case_Fields_Pnd_1099_Cases.newFieldInGroup("pnd_Case_Fields_Pnd_Case_Present", "#CASE-PRESENT", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Tnd = pnd_Case_Fields_Pnd_1099_Cases.newFieldInGroup("pnd_Case_Fields_Pnd_Tnd", "#TND", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Trad_Clas_Dist = pnd_Case_Fields_Pnd_1099_Cases.newFieldInGroup("pnd_Case_Fields_Pnd_Trad_Clas_Dist", "#TRAD-CLAS-DIST", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Gross_Amt = pnd_Case_Fields_Pnd_1099_Cases.newFieldArrayInGroup("pnd_Case_Fields_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 2));
        pnd_Case_Fields_Pnd_Ivc_Amt = pnd_Case_Fields_Pnd_1099_Cases.newFieldArrayInGroup("pnd_Case_Fields_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 2));

        pnd_St_Case = localVariables.newGroupArrayInRecord("pnd_St_Case", "#ST-CASE", new DbsArrayController(1, 5, 1, 63));
        pnd_St_Case_Pnd_Case_Present = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_Case_Present", "#CASE-PRESENT", FieldType.BOOLEAN, 1);
        pnd_St_Case_Pnd_Tnd = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_Tnd", "#TND", FieldType.BOOLEAN, 1);
        pnd_St_Case_Pnd_Tran_Cnt = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_Tran_Cnt", "#TRAN-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_St_Case_Pnd_Gross_Amt = pnd_St_Case.newFieldArrayInGroup("pnd_St_Case_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            2));
        pnd_St_Case_Pnd_Ivc_Amt = pnd_St_Case.newFieldArrayInGroup("pnd_St_Case_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            2));
        pnd_St_Case_Pnd_Ivc_Adj = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_Ivc_Adj", "#IVC-ADJ", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_St_Case_Pnd_Taxable_Amt = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_St_Case_Pnd_Taxable_Adj = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_Taxable_Adj", "#TAXABLE-ADJ", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_St_Case_Pnd_Sta_Tax = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_Sta_Tax", "#STA-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_St_Case_Pnd_Loc_Tax = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_Loc_Tax", "#LOC-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_St_Case_Pnd_State_Distr = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_State_Distr", "#STATE-DISTR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_St_Case_Pnd_Reportable_Distr = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_Reportable_Distr", "#REPORTABLE-DISTR", FieldType.BOOLEAN, 1);

        pnd_Prev_Val = localVariables.newGroupInRecord("pnd_Prev_Val", "#PREV-VAL");
        pnd_Prev_Val_Pnd_Paymt_Category = pnd_Prev_Val.newFieldInGroup("pnd_Prev_Val_Pnd_Paymt_Category", "#PAYMT-CATEGORY", FieldType.STRING, 1);
        pnd_Prev_Val_Pnd_Contract_Nbr = pnd_Prev_Val.newFieldInGroup("pnd_Prev_Val_Pnd_Contract_Nbr", "#CONTRACT-NBR", FieldType.STRING, 8);
        pnd_Prev_Val_Pnd_Payee_Cde = pnd_Prev_Val.newFieldInGroup("pnd_Prev_Val_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.STRING, 2);

        pnd_Prev_Val__R_Field_2 = pnd_Prev_Val.newGroupInGroup("pnd_Prev_Val__R_Field_2", "REDEFINE", pnd_Prev_Val_Pnd_Payee_Cde);
        pnd_Prev_Val_Pnd_Payee_Cde_N = pnd_Prev_Val__R_Field_2.newFieldInGroup("pnd_Prev_Val_Pnd_Payee_Cde_N", "#PAYEE-CDE-N", FieldType.NUMERIC, 2);
        pnd_Prev_Val_Pnd_Distr_Cde = pnd_Prev_Val.newFieldInGroup("pnd_Prev_Val_Pnd_Distr_Cde", "#DISTR-CDE", FieldType.STRING, 2);

        pnd_Cntl = localVariables.newGroupArrayInRecord("pnd_Cntl", "#CNTL", new DbsArrayController(1, 9));
        pnd_Cntl_Pnd_Cntl_Text = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Cntl_Text", "#CNTL-TEXT", FieldType.STRING, 26);
        pnd_Cntl_Pnd_Cntl_Text1 = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Cntl_Text1", "#CNTL-TEXT1", FieldType.STRING, 26);
        pnd_Cntl_Pnd_Tran_Cnt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Tran_Cnt_Cv", "#TRAN-CNT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Gross_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Gross_Amt_Cv", "#GROSS-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Ivc_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Ivc_Amt_Cv", "#IVC-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Taxable_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Taxable_Amt_Cv", "#TAXABLE-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Fed_Tax_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Fed_Tax_Cv", "#FED-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Sta_Tax_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Sta_Tax_Cv", "#STA-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Loc_Tax_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Loc_Tax_Cv", "#LOC-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Int_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Int_Amt_Cv", "#INT-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Int_Back_Tax_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Int_Back_Tax_Cv", "#INT-BACK-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 2);

        pnd_Cntl_Pnd_Totals = pnd_Cntl.newGroupArrayInGroup("pnd_Cntl_Pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Cntl_Pnd_Tran_Cnt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Tran_Cnt", "#TRAN-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Cntl_Pnd_Gross_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Cntl_Pnd_Ivc_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Taxable_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Cntl_Pnd_Fed_Tax = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Fed_Tax", "#FED-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Sta_Tax = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Sta_Tax", "#STA-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Loc_Tax = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Loc_Tax", "#LOC-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Int_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Int_Tax = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Int_Tax", "#INT-TAX", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_St_Cntl = localVariables.newGroupArrayInRecord("pnd_St_Cntl", "#ST-CNTL", new DbsArrayController(1, 63));
        pnd_St_Cntl_Pnd_State_Desc = pnd_St_Cntl.newFieldInGroup("pnd_St_Cntl_Pnd_State_Desc", "#STATE-DESC", FieldType.STRING, 18);
        pnd_St_Cntl_Pnd_State_Rule = pnd_St_Cntl.newFieldInGroup("pnd_St_Cntl_Pnd_State_Rule", "#STATE-RULE", FieldType.STRING, 1);
        pnd_St_Cntl_Pnd_State_Cmb = pnd_St_Cntl.newFieldInGroup("pnd_St_Cntl_Pnd_State_Cmb", "#STATE-CMB", FieldType.STRING, 1);
        pnd_St_Cntl_Pnd_State_Med = pnd_St_Cntl.newFieldInGroup("pnd_St_Cntl_Pnd_State_Med", "#STATE-MED", FieldType.STRING, 1);
        pnd_St_Cntl_Pnd_State_Gross_Rpt = pnd_St_Cntl.newFieldInGroup("pnd_St_Cntl_Pnd_State_Gross_Rpt", "#STATE-GROSS-RPT", FieldType.BOOLEAN, 1);

        pnd_St_Tot = localVariables.newGroupArrayInRecord("pnd_St_Tot", "#ST-TOT", new DbsArrayController(1, 63, 1, 2, 1, 5));
        pnd_St_Tot_Pnd_Tran_Cnt = pnd_St_Tot.newFieldInGroup("pnd_St_Tot_Pnd_Tran_Cnt", "#TRAN-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_St_Tot_Pnd_Gross_Amt = pnd_St_Tot.newFieldInGroup("pnd_St_Tot_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_St_Tot_Pnd_Pymnt_Ivc = pnd_St_Tot.newFieldInGroup("pnd_St_Tot_Pnd_Pymnt_Ivc", "#PYMNT-IVC", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_St_Tot_Pnd_Ivc_Adj = pnd_St_Tot.newFieldInGroup("pnd_St_Tot_Pnd_Ivc_Adj", "#IVC-ADJ", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_St_Tot_Pnd_Form_Ivc = pnd_St_Tot.newFieldInGroup("pnd_St_Tot_Pnd_Form_Ivc", "#FORM-IVC", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_St_Tot_Pnd_Sta_Tax = pnd_St_Tot.newFieldInGroup("pnd_St_Tot_Pnd_Sta_Tax", "#STA-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_St_Tot_Pnd_Loc_Tax = pnd_St_Tot.newFieldInGroup("pnd_St_Tot_Pnd_Loc_Tax", "#LOC-TAX", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_St_To1 = localVariables.newGroupArrayInRecord("pnd_St_To1", "#ST-TO1", new DbsArrayController(1, 63, 1, 2, 1, 5));
        pnd_St_To1_Pnd_Pmnt_Taxable = pnd_St_To1.newFieldInGroup("pnd_St_To1_Pnd_Pmnt_Taxable", "#PMNT-TAXABLE", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_St_To1_Pnd_Taxable_Adj = pnd_St_To1.newFieldInGroup("pnd_St_To1_Pnd_Taxable_Adj", "#TAXABLE-ADJ", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_St_To1_Pnd_Form_Taxable = pnd_St_To1.newFieldInGroup("pnd_St_To1_Pnd_Form_Taxable", "#FORM-TAXABLE", FieldType.PACKED_DECIMAL, 13, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Twrpymnt_Distribution_CdeOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrpymnt_Distribution_Cde_OLD", "Twrpymnt_Distribution_Cde_OLD", 
            FieldType.STRING, 2);
        readWork01Twrpymnt_Paymt_CategoryOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrpymnt_Paymt_Category_OLD", "Twrpymnt_Paymt_Category_OLD", 
            FieldType.STRING, 1);
        readWork01Twrpymnt_Contract_NbrOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrpymnt_Contract_Nbr_OLD", "Twrpymnt_Contract_Nbr_OLD", 
            FieldType.STRING, 8);
        readWork01Twrpymnt_Payee_CdeOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrpymnt_Payee_Cde_OLD", "Twrpymnt_Payee_Cde_OLD", FieldType.STRING, 
            2);
        readWork01Twrpymnt_Company_Cde_FormOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrpymnt_Company_Cde_Form_OLD", "Twrpymnt_Company_Cde_Form_OLD", 
            FieldType.STRING, 1);
        readWork01Twrpymnt_Tax_YearOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrpymnt_Tax_Year_OLD", "Twrpymnt_Tax_Year_OLD", FieldType.NUMERIC, 
            4);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaTwrl0900.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
        pnd_Ws_Const_Pnd_Cntl_Max.setInitialValue(9);
        pnd_Ws_Const_Pnd_St_Tot_Max.setInitialValue(62);
        pnd_Ws_Const_Pnd_1099r_Max.setInitialValue(5);
        pnd_Ws_Pnd_1099.setInitialValue(1);
        pnd_Case_Fields_Pnd_Payment_Type.getValue(2).setInitialValue("N");
        pnd_Case_Fields_Pnd_Payment_Type.getValue(3).setInitialValue("N");
        pnd_Case_Fields_Pnd_Payment_Type.getValue(4).setInitialValue("N");
        pnd_Case_Fields_Pnd_Payment_Type.getValue(5).setInitialValue("R");
        pnd_Case_Fields_Pnd_Settle_Type.getValue(2).setInitialValue("X");
        pnd_Case_Fields_Pnd_Settle_Type.getValue(3).setInitialValue("I");
        pnd_Case_Fields_Pnd_Settle_Type.getValue(4).setInitialValue("H");
        pnd_Case_Fields_Pnd_Settle_Type.getValue(5).setInitialValue("E");
        pnd_Cntl_Pnd_Cntl_Text.getValue(1).setInitialValue("* All Payments");
        pnd_Cntl_Pnd_Cntl_Text.getValue(2).setInitialValue("Active Payments");
        pnd_Cntl_Pnd_Cntl_Text.getValue(3).setInitialValue("- Distributions from");
        pnd_Cntl_Pnd_Cntl_Text.getValue(4).setInitialValue("- IRA Excess Contributions");
        pnd_Cntl_Pnd_Cntl_Text.getValue(5).setInitialValue("- 'U' IVC with $");
        pnd_Cntl_Pnd_Cntl_Text.getValue(6).setInitialValue("- Taxable not Determined");
        pnd_Cntl_Pnd_Cntl_Text.getValue(7).setInitialValue("- Puerto Rico");
        pnd_Cntl_Pnd_Cntl_Text.getValue(8).setInitialValue("- Foreign Residency");
        pnd_Cntl_Pnd_Cntl_Text.getValue(9).setInitialValue("= 1099-R & 1099-INT forms");
        pnd_Cntl_Pnd_Cntl_Text1.getValue(3).setInitialValue("  Trad. & Classic IRAs");
        pnd_Cntl_Pnd_Cntl_Text1.getValue(4).setInitialValue("  & REAC");
        pnd_Cntl_Pnd_Cntl_Text1.getValue(6).setInitialValue("  (Not Rollovers)");
        pnd_Cntl_Pnd_Tran_Cnt_Cv.setInitialAttributeValue("AD=I)#CNTL.#TRAN-CNT-CV(2)	(AD=I)#CNTL.#TRAN-CNT-CV(3)	(AD=N)#CNTL.#TRAN-CNT-CV(4)	(AD=N)#CNTL.#TRAN-CNT-CV(5)	(AD=N)#CNTL.#TRAN-CNT-CV(6)	(AD=N)#CNTL.#TRAN-CNT-CV(7)	(AD=N)#CNTL.#TRAN-CNT-CV(8)	(AD=N)#CNTL.#TRAN-CNT-CV(9)	(AD=N");
        pnd_Cntl_Pnd_Gross_Amt_Cv.setInitialAttributeValue("AD=I)#CNTL.#GROSS-AMT-CV(2)	(AD=I)#CNTL.#GROSS-AMT-CV(3)	(AD=N)#CNTL.#GROSS-AMT-CV(4)	(AD=N)#CNTL.#GROSS-AMT-CV(5)	(AD=N)#CNTL.#GROSS-AMT-CV(6)	(AD=N)#CNTL.#GROSS-AMT-CV(7)	(AD=N)#CNTL.#GROSS-AMT-CV(8)	(AD=N)#CNTL.#GROSS-AMT-CV(9)	(AD=I");
        pnd_Cntl_Pnd_Ivc_Amt_Cv.setInitialAttributeValue("AD=I)#CNTL.#IVC-AMT-CV(2)	(AD=I)#CNTL.#IVC-AMT-CV(3)	(AD=I)#CNTL.#IVC-AMT-CV(4)	(AD=I)#CNTL.#IVC-AMT-CV(5)	(AD=I)#CNTL.#IVC-AMT-CV(6)	(AD=I)#CNTL.#IVC-AMT-CV(7)	(AD=N)#CNTL.#IVC-AMT-CV(8)	(AD=N)#CNTL.#IVC-AMT-CV(9)	(AD=I");
        pnd_Cntl_Pnd_Taxable_Amt_Cv.setInitialAttributeValue("AD=N)#CNTL.#TAXABLE-AMT-CV(2)	(AD=I)#CNTL.#TAXABLE-AMT-CV(3)	(AD=I)#CNTL.#TAXABLE-AMT-CV(4)	(AD=N)#CNTL.#TAXABLE-AMT-CV(5)	(AD=N)#CNTL.#TAXABLE-AMT-CV(6)	(AD=I)#CNTL.#TAXABLE-AMT-CV(7)	(AD=N)#CNTL.#TAXABLE-AMT-CV(8)	(AD=N)#CNTL.#TAXABLE-AMT-CV(9)	(AD=I");
        pnd_Cntl_Pnd_Fed_Tax_Cv.setInitialAttributeValue("AD=I)#CNTL.#FED-TAX-CV(2)	(AD=I)#CNTL.#FED-TAX-CV(3)	(AD=N)#CNTL.#FED-TAX-CV(4)	(AD=N)#CNTL.#FED-TAX-CV(5)	(AD=N)#CNTL.#FED-TAX-CV(6)	(AD=N)#CNTL.#FED-TAX-CV(7)	(AD=N)#CNTL.#FED-TAX-CV(8)	(AD=N)#CNTL.#FED-TAX-CV(9)	(AD=I");
        pnd_Cntl_Pnd_Sta_Tax_Cv.setInitialAttributeValue("AD=I)#CNTL.#STA-TAX-CV(2)	(AD=I)#CNTL.#STA-TAX-CV(3)	(AD=N)#CNTL.#STA-TAX-CV(4)	(AD=N)#CNTL.#STA-TAX-CV(5)	(AD=N)#CNTL.#STA-TAX-CV(6)	(AD=N)#CNTL.#STA-TAX-CV(7)	(AD=I)#CNTL.#STA-TAX-CV(8)	(AD=I)#CNTL.#STA-TAX-CV(9)	(AD=I");
        pnd_Cntl_Pnd_Loc_Tax_Cv.setInitialAttributeValue("AD=I)#CNTL.#LOC-TAX-CV(2)	(AD=I)#CNTL.#LOC-TAX-CV(3)	(AD=N)#CNTL.#LOC-TAX-CV(4)	(AD=N)#CNTL.#LOC-TAX-CV(5)	(AD=N)#CNTL.#LOC-TAX-CV(6)	(AD=N)#CNTL.#LOC-TAX-CV(7)	(AD=I)#CNTL.#LOC-TAX-CV(8)	(AD=I)#CNTL.#LOC-TAX-CV(9)	(AD=I");
        pnd_Cntl_Pnd_Int_Amt_Cv.setInitialAttributeValue("AD=I)#CNTL.#INT-AMT-CV(2)	(AD=I)#CNTL.#INT-AMT-CV(3)	(AD=N)#CNTL.#INT-AMT-CV(4)	(AD=N)#CNTL.#INT-AMT-CV(5)	(AD=N)#CNTL.#INT-AMT-CV(6)	(AD=N)#CNTL.#INT-AMT-CV(7)	(AD=N)#CNTL.#INT-AMT-CV(8)	(AD=N)#CNTL.#INT-AMT-CV(9)	(AD=I");
        pnd_Cntl_Pnd_Int_Back_Tax_Cv.setInitialAttributeValue("AD=I)#CNTL.#INT-BACK-TAX-CV(2)	(AD=I)#CNTL.#INT-BACK-TAX-CV(3)	(AD=N)#CNTL.#INT-BACK-TAX-CV(4)	(AD=N)#CNTL.#INT-BACK-TAX-CV(5)	(AD=N)#CNTL.#INT-BACK-TAX-CV(6)	(AD=N)#CNTL.#INT-BACK-TAX-CV(7)	(AD=N)#CNTL.#INT-BACK-TAX-CV(8)	(AD=N)#CNTL.#INT-BACK-TAX-CV(9)	(AD=I");
        pnd_St_Cntl_Pnd_State_Desc.getValue(59 + 1).setInitialValue("Other US residency");
        pnd_St_Cntl_Pnd_State_Desc.getValue(60 + 1).setInitialValue("Form Total");
        pnd_St_Cntl_Pnd_State_Desc.getValue(61 + 1).setInitialValue("Non US residency");
        pnd_St_Cntl_Pnd_State_Desc.getValue(62 + 1).setInitialValue("Grand Total");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5531() throws Exception
    {
        super("Twrp5531");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 02 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 03 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 04 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 05 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 06 ) PS = 58 LS = 133 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T 'Summary of Federal Payment Transactions' 120T 'Report: RPT1' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / #WS.#COMPANY-LINE //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 02 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 57T 'State Summary Totals' 120T 'Report: RPT2' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / #WS.#COMPANY-LINE //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 03 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 48T 'State Summary Totals - Non-Reportable' 120T 'Report: RPT3' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / #WS.#COMPANY-LINE //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 04 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T 'State Summary Totals - Gross reportable' 120T 'Report: RPT4' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / #WS.#COMPANY-LINE //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 05 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 05 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'State Summary Totals - Taxable reportable' 120T 'Report: RPT5' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / #WS.#COMPANY-LINE //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 06 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 06 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'State Summary Totals - HardCopy reportable' 120T 'Report: RPT6' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / #WS.#COMPANY-LINE //
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 RECORD #XTAXYR-F94
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaTwrl0900.getPnd_Xtaxyr_F94())))
        {
            CheckAtStartofData413();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: AT START OF DATA;//Natural: BEFORE BREAK PROCESSING
            pnd_Key_Detail.setValuesByName(ldaTwrl0900.getPnd_Xtaxyr_F94());                                                                                              //Natural: MOVE BY NAME #XTAXYR-F94 TO #KEY-DETAIL
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //*                                                                                                                                                           //Natural: AT BREAK OF #KEY;//Natural: AT BREAK OF #KEY /28/;//Natural: AT BREAK OF #KEY /25/;//Natural: AT BREAK OF #XTAXYR-F94.TWRPYMNT-COMPANY-CDE-FORM
            if (condition(pnd_Ws_Pnd_New_Res.getBoolean()))                                                                                                               //Natural: IF #WS.#NEW-RES
            {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-RES-IDX
                sub_Determine_Res_Idx();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            FOR04:                                                                                                                                                        //Natural: FOR #I = 1 TO #XTAXYR-F94.#C-TWRPYMNT-PAYMENTS
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments())); pnd_Ws_Pnd_I.nadd(1))
            {
                //*    DISPLAY
                //*      'RES'        #XTAXYR-F94.TWRPYMNT-RESIDENCY-CODE
                //*      'TAX ID'     #XTAXYR-F94.TWRPYMNT-TAX-ID-NBR
                //*      'DIST'       #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE
                //*      'CONTRACT'   #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR
                //*      'PAYEE'      #XTAXYR-F94.TWRPYMNT-PAYEE-CDE
                //*      'TAX CIT'    #XTAXYR-F94.TWRPYMNT-TAX-CITIZENSHIP
                //*      'PYMT/TYPE'  #XTAXYR-F94.TWRPYMNT-PAYMENT-TYPE    (#I)
                //*      'PYMT/TYPE'  #XTAXYR-F94.TWRPYMNT-SETTLE-TYPE     (#I)
                //*      'GROSS'      #XTAXYR-F94.TWRPYMNT-GROSS-AMT       (#I)
                //*      'IVC'        #XTAXYR-F94.TWRPYMNT-IVC-AMT         (#I)
                //*      'IVC IND'    #XTAXYR-F94.TWRPYMNT-IVC-IND         (#I)
                //*      'PRT'        #XTAXYR-F94.TWRPYMNT-IVC-PROTECT     (#I)
                //*      'INTEREST'   #XTAXYR-F94.TWRPYMNT-INT-AMT         (#I)
                //*      'FEDERAL'    #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT   (#I)
                //*      'NRA'        #XTAXYR-F94.TWRPYMNT-NRA-WTHLD-AMT   (#I)
                pnd_Cntl_Pnd_Tran_Cnt.getValue(1,1).nadd(1);                                                                                                              //Natural: ADD 1 TO #CNTL.#TRAN-CNT ( 1,1 )
                pnd_Cntl_Pnd_Gross_Amt.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 1,1 )
                pnd_Cntl_Pnd_Ivc_Amt.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                                         //Natural: ADD #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) TO #CNTL.#IVC-AMT ( 1,1 )
                pnd_Cntl_Pnd_Fed_Tax.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                                   //Natural: ADD #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #CNTL.#FED-TAX ( 1,1 )
                pnd_Cntl_Pnd_Sta_Tax.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) TO #CNTL.#STA-TAX ( 1,1 )
                pnd_Cntl_Pnd_Loc_Tax.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Local_Wthld().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-LOCAL-WTHLD ( #I ) TO #CNTL.#LOC-TAX ( 1,1 )
                pnd_Cntl_Pnd_Int_Amt.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                         //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#INT-AMT ( 1,1 )
                pnd_Cntl_Pnd_Int_Tax.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Bkup_Wthld().getValue(pnd_Ws_Pnd_I));                                  //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-BKUP-WTHLD ( #I ) TO #CNTL.#INT-TAX ( 1,1 )
                if (condition(! (ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals(" ") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals("C")))) //Natural: IF NOT #XTAXYR-F94.TWRPYMNT-PYMNT-STATUS ( #I ) = ' ' OR = 'C'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Type().getValue(pnd_Ws_Pnd_I).notEquals(" ")))                                              //Natural: IF #XTAXYR-F94.TWRPYMNT-CONTRACT-TYPE ( #I ) NE ' '
                {
                    if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Type().getValue(pnd_Ws_Pnd_I).notEquals(pnd_Ws_Pnd_Contract_Type) &&                    //Natural: IF #XTAXYR-F94.TWRPYMNT-CONTRACT-TYPE ( #I ) NE #WS.#CONTRACT-TYPE AND #WS.#CONTRACT-TYPE NE ' '
                        pnd_Ws_Pnd_Contract_Type.notEquals(" ")))
                    {
                        pnd_Ws_Pnd_Contract_Type_Mixed.setValue(true);                                                                                                    //Natural: ASSIGN #WS.#CONTRACT-TYPE-MIXED := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Pnd_Contract_Type.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Type().getValue(pnd_Ws_Pnd_I));                                     //Natural: ASSIGN #WS.#CONTRACT-TYPE := #XTAXYR-F94.TWRPYMNT-CONTRACT-TYPE ( #I )
                }                                                                                                                                                         //Natural: END-IF
                //*                                                   ACTIVE PAYMENTS
                pnd_Cntl_Pnd_Fed_Tax.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                                   //Natural: ADD #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #CNTL.#FED-TAX ( 2,1 )
                pnd_Cntl_Pnd_Sta_Tax.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) TO #CNTL.#STA-TAX ( 2,1 )
                pnd_Cntl_Pnd_Loc_Tax.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Local_Wthld().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-LOCAL-WTHLD ( #I ) TO #CNTL.#LOC-TAX ( 2,1 )
                pnd_Cntl_Pnd_Int_Amt.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                         //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#INT-AMT ( 2,1 )
                pnd_Cntl_Pnd_Int_Tax.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Bkup_Wthld().getValue(pnd_Ws_Pnd_I));                                  //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-BKUP-WTHLD ( #I ) TO #CNTL.#INT-TAX ( 2,1 )
                //*  INTEREST
                if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I).equals(new DbsDecimal("0.00")) && (ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new  //Natural: IF #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) = 0.00 AND ( #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) NE 0.00 OR #XTAXYR-F94.TWRPYMNT-INT-BKUP-WTHLD ( #I ) NE 0.00 )
                    DbsDecimal("0.00")) || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Bkup_Wthld().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00")))))
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_1099.resetInitial();                                                                                                                           //Natural: RESET INITIAL #WS.#1099
                FOR05:                                                                                                                                                    //Natural: FOR #J = 2 TO #1099R-MAX
                for (pnd_Ws_Pnd_J.setValue(2); condition(pnd_Ws_Pnd_J.lessOrEqual(pnd_Ws_Const_Pnd_1099r_Max)); pnd_Ws_Pnd_J.nadd(1))
                {
                    if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payment_Type().getValue(pnd_Ws_Pnd_I).equals(pnd_Case_Fields_Pnd_Payment_Type.getValue(pnd_Ws_Pnd_J))  //Natural: IF #XTAXYR-F94.TWRPYMNT-PAYMENT-TYPE ( #I ) = #CASE-FIELDS.#PAYMENT-TYPE ( #J ) AND #XTAXYR-F94.TWRPYMNT-SETTLE-TYPE ( #I ) = #CASE-FIELDS.#SETTLE-TYPE ( #J )
                        && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals(pnd_Case_Fields_Pnd_Settle_Type.getValue(pnd_Ws_Pnd_J))))
                    {
                        pnd_Ws_Pnd_1099.setValue(pnd_Ws_Pnd_J);                                                                                                           //Natural: ASSIGN #WS.#1099 := #J
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Case_Fields_Pnd_Case_Present.getValue(pnd_Ws_Pnd_1099).setValue(true);                                                                                //Natural: ASSIGN #CASE-FIELDS.#CASE-PRESENT ( #1099 ) := TRUE
                pnd_Cntl_Pnd_Tran_Cnt.getValue(2,1).nadd(1);                                                                                                              //Natural: ADD 1 TO #CNTL.#TRAN-CNT ( 2,1 )
                pnd_Cntl_Pnd_Gross_Amt.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 2,1 )
                pnd_Cntl_Pnd_Ivc_Amt.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                                         //Natural: ADD #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) TO #CNTL.#IVC-AMT ( 2,1 )
                //*  KNOWN IVC
                if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Ind().getValue(pnd_Ws_Pnd_I).equals("K") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Protect().getValue(pnd_Ws_Pnd_I).getBoolean())) //Natural: IF #XTAXYR-F94.TWRPYMNT-IVC-IND ( #I ) = 'K' OR #XTAXYR-F94.TWRPYMNT-IVC-PROTECT ( #I )
                {
                    pnd_Ws_Pnd_J.setValue(1);                                                                                                                             //Natural: ASSIGN #WS.#J := 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Case_Fields_Pnd_Tnd.getValue(pnd_Ws_Pnd_1099).setValue(true);                                                                                     //Natural: ASSIGN #CASE-FIELDS.#TND ( #1099 ) := TRUE
                    pnd_St_Case_Pnd_Tnd.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).setValue(true);                                                             //Natural: ASSIGN #ST-CASE.#TND ( #1099,#IDX ) := TRUE
                    pnd_Ws_Pnd_J.setValue(2);                                                                                                                             //Natural: ASSIGN #WS.#J := 2
                }                                                                                                                                                         //Natural: END-IF
                pnd_Case_Fields_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_1099,pnd_Ws_Pnd_J).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));     //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CASE-FIELDS.#GROSS-AMT ( #1099,#J )
                pnd_Case_Fields_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099,pnd_Ws_Pnd_J).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));         //Natural: ADD #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) TO #CASE-FIELDS.#IVC-AMT ( #1099,#J )
                short decideConditionsMet712 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #XTAXYR-F94.TWRPYMNT-RESIDENCY-CODE = '42' OR = 'RQ'
                if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code().equals("42") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code().equals("RQ")))
                {
                    decideConditionsMet712++;
                    pnd_Cntl_Pnd_Sta_Tax.getValue(7,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) TO #CNTL.#STA-TAX ( 7,1 )
                    pnd_Cntl_Pnd_Loc_Tax.getValue(7,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Local_Wthld().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-LOCAL-WTHLD ( #I ) TO #CNTL.#LOC-TAX ( 7,1 )
                }                                                                                                                                                         //Natural: WHEN #XTAXYR-F94.TWRPYMNT-RESIDENCY-TYPE NE '1'
                else if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Type().notEquals("1")))
                {
                    decideConditionsMet712++;
                    pnd_Cntl_Pnd_Sta_Tax.getValue(8,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) TO #CNTL.#STA-TAX ( 8,1 )
                    pnd_Cntl_Pnd_Loc_Tax.getValue(8,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Local_Wthld().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-LOCAL-WTHLD ( #I ) TO #CNTL.#LOC-TAX ( 8,1 )
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //*    STATE REPORTS
                pnd_St_Case_Pnd_Case_Present.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).setValue(true);                                                        //Natural: ASSIGN #ST-CASE.#CASE-PRESENT ( #1099,#IDX ) := TRUE
                pnd_St_Case_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).nadd(1);                                                                   //Natural: ADD 1 TO #ST-CASE.#TRAN-CNT ( #1099,#IDX )
                pnd_St_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx,pnd_Ws_Pnd_J).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I)); //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #ST-CASE.#GROSS-AMT ( #1099,#IDX,#J )
                pnd_St_Case_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx,pnd_Ws_Pnd_J).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I)); //Natural: ADD #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) TO #ST-CASE.#IVC-AMT ( #1099,#IDX,#J )
                pnd_St_Case_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I)); //Natural: ADD #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) TO #ST-CASE.#STA-TAX ( #1099,#IDX )
                pnd_St_Case_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Local_Wthld().getValue(pnd_Ws_Pnd_I)); //Natural: ADD #XTAXYR-F94.TWRPYMNT-LOCAL-WTHLD ( #I ) TO #ST-CASE.#LOC-TAX ( #1099,#IDX )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            readWork01Twrpymnt_Distribution_CdeOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde());                                                   //Natural: END-WORK
            readWork01Twrpymnt_Paymt_CategoryOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Paymt_Category());
            readWork01Twrpymnt_Contract_NbrOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr());
            readWork01Twrpymnt_Payee_CdeOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payee_Cde());
            readWork01Twrpymnt_Company_Cde_FormOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form());
            readWork01Twrpymnt_Tax_YearOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Company_Line.setValue("Grand Totals");                                                                                                                 //Natural: ASSIGN #WS.#COMPANY-LINE := 'Grand Totals'
        pnd_Ws_Pnd_S2.setValue(2);                                                                                                                                        //Natural: ASSIGN #WS.#S2 := 2
                                                                                                                                                                          //Natural: PERFORM WRITE-FED-REPORT
        sub_Write_Fed_Report();
        if (condition(Global.isEscape())) {return;}
        //* **********************
        //*  S U B R O U T I N E S
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-FED-REPORT
        //* *********************************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-RES-IDX
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-CONTRACT-TYPE
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COMPANY
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-STATE-TABLE
        //* *********************************
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-STATE-ENTRY
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Write_Fed_Report() throws Exception                                                                                                                  //Natural: WRITE-FED-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Cntl_Pnd_Gross_Amt.getValue(9,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Gross_Amt.getValue(2,pnd_Ws_Pnd_S2));                                                      //Natural: ASSIGN #CNTL.#GROSS-AMT ( 9,#S2 ) := #CNTL.#GROSS-AMT ( 2,#S2 )
        pnd_Cntl_Pnd_Int_Amt.getValue(9,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Int_Amt.getValue(2,pnd_Ws_Pnd_S2));                                                          //Natural: ASSIGN #CNTL.#INT-AMT ( 9,#S2 ) := #CNTL.#INT-AMT ( 2,#S2 )
        pnd_Cntl_Pnd_Ivc_Amt.getValue(9,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Ivc_Amt.getValue(9,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Ivc_Amt.getValue(2, //Natural: ASSIGN #CNTL.#IVC-AMT ( 9,#S2 ) := #CNTL.#IVC-AMT ( 2,#S2 ) - #CNTL.#IVC-AMT ( 3:8,#S2 )
            pnd_Ws_Pnd_S2).subtract(pnd_Cntl_Pnd_Ivc_Amt.getValue(3,":",8,pnd_Ws_Pnd_S2)));
        pnd_Cntl_Pnd_Taxable_Amt.getValue(9,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Taxable_Amt.getValue(9,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Taxable_Amt.getValue(2, //Natural: ASSIGN #CNTL.#TAXABLE-AMT ( 9,#S2 ) := #CNTL.#TAXABLE-AMT ( 2,#S2 ) - #CNTL.#TAXABLE-AMT ( 3:8,#S2 )
            pnd_Ws_Pnd_S2).subtract(pnd_Cntl_Pnd_Taxable_Amt.getValue(3,":",8,pnd_Ws_Pnd_S2)));
        pnd_Cntl_Pnd_Fed_Tax.getValue(9,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Fed_Tax.getValue(2,pnd_Ws_Pnd_S2));                                                          //Natural: ASSIGN #CNTL.#FED-TAX ( 9,#S2 ) := #CNTL.#FED-TAX ( 2,#S2 )
        pnd_Cntl_Pnd_Int_Tax.getValue(9,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Int_Tax.getValue(2,pnd_Ws_Pnd_S2));                                                          //Natural: ASSIGN #CNTL.#INT-TAX ( 9,#S2 ) := #CNTL.#INT-TAX ( 2,#S2 )
        pnd_Cntl_Pnd_Sta_Tax.getValue(9,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Sta_Tax.getValue(9,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Sta_Tax.getValue(2, //Natural: ASSIGN #CNTL.#STA-TAX ( 9,#S2 ) := #CNTL.#STA-TAX ( 2,#S2 ) - #CNTL.#STA-TAX ( 3:8,#S2 )
            pnd_Ws_Pnd_S2).subtract(pnd_Cntl_Pnd_Sta_Tax.getValue(3,":",8,pnd_Ws_Pnd_S2)));
        pnd_Cntl_Pnd_Loc_Tax.getValue(9,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Loc_Tax.getValue(9,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Loc_Tax.getValue(2, //Natural: ASSIGN #CNTL.#LOC-TAX ( 9,#S2 ) := #CNTL.#LOC-TAX ( 2,#S2 ) - #CNTL.#LOC-TAX ( 3:8,#S2 )
            pnd_Ws_Pnd_S2).subtract(pnd_Cntl_Pnd_Loc_Tax.getValue(3,":",8,pnd_Ws_Pnd_S2)));
        FOR06:                                                                                                                                                            //Natural: FOR #WS.#S1 = 1 TO #CNTL-MAX
        for (pnd_Ws_Pnd_S1.setValue(1); condition(pnd_Ws_Pnd_S1.lessOrEqual(pnd_Ws_Const_Pnd_Cntl_Max)); pnd_Ws_Pnd_S1.nadd(1))
        {
            pnd_Ws_Pnd_Tran_Cv.setValue(pnd_Cntl_Pnd_Tran_Cnt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                //Natural: ASSIGN #WS.#TRAN-CV := #CNTL.#TRAN-CNT-CV ( #S1 )
            pnd_Ws_Pnd_Gross_Cv.setValue(pnd_Cntl_Pnd_Gross_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                              //Natural: ASSIGN #WS.#GROSS-CV := #CNTL.#GROSS-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Ivc_Cv.setValue(pnd_Cntl_Pnd_Ivc_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#IVC-CV := #CNTL.#IVC-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Taxable_Cv.setValue(pnd_Cntl_Pnd_Taxable_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                          //Natural: ASSIGN #WS.#TAXABLE-CV := #CNTL.#TAXABLE-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Fed_Cv.setValue(pnd_Cntl_Pnd_Fed_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#FED-CV := #CNTL.#FED-TAX-CV ( #S1 )
            pnd_Ws_Pnd_Sta_Cv.setValue(pnd_Cntl_Pnd_Sta_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#STA-CV := #CNTL.#STA-TAX-CV ( #S1 )
            pnd_Ws_Pnd_Loc_Cv.setValue(pnd_Cntl_Pnd_Loc_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#LOC-CV := #CNTL.#LOC-TAX-CV ( #S1 )
            pnd_Ws_Pnd_Int_Cv.setValue(pnd_Cntl_Pnd_Int_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#INT-CV := #CNTL.#INT-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Int_Back_Cv.setValue(pnd_Cntl_Pnd_Int_Back_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                        //Natural: ASSIGN #WS.#INT-BACK-CV := #CNTL.#INT-BACK-TAX-CV ( #S1 )
            getReports().display(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",                         //Natural: DISPLAY ( 1 ) ( HC = R ES = ON ) '/' #CNTL-TEXT ( #S1 ) / '/' #CNTL-TEXT1 ( #S1 ) '/Trans Count' #CNTL.#TRAN-CNT ( #S1,#S2 ) ( CV = #TRAN-CV ) 'Gross Amount' #CNTL.#GROSS-AMT ( #S1,#S2 ) ( CV = #GROSS-CV ) / 3X 'Interest' #CNTL.#INT-AMT ( #S1,#S2 ) ( CV = #INT-CV ) '/IVC Amount' #CNTL.#IVC-AMT ( #S1,#S2 ) ( CV = #IVC-CV ) '/Taxable Amount' #CNTL.#TAXABLE-AMT ( #S1,#S2 ) ( CV = #TAXABLE-CV ) 'Federal Tax' #CNTL.#FED-TAX ( #S1,#S2 ) ( CV = #FED-CV ) / 'Backup Tax' #CNTL.#INT-TAX ( #S1,#S2 ) ( CV = #INT-BACK-CV ) 'State   Tax' #CNTL.#STA-TAX ( #S1,#S2 ) ( CV = #STA-CV ) / 'Local   Tax' #CNTL.#LOC-TAX ( #S1,#S2 ) ( CV = #LOC-CV )
            		pnd_Cntl_Pnd_Cntl_Text.getValue(pnd_Ws_Pnd_S1),NEWLINE,"/",
            		pnd_Cntl_Pnd_Cntl_Text1.getValue(pnd_Ws_Pnd_S1),"/Trans Count",
            		pnd_Cntl_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Tran_Cv,"Gross Amount",
            		pnd_Cntl_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Gross_Cv,NEWLINE,new ColumnSpacing(3),"Interest",
            		pnd_Cntl_Pnd_Int_Amt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Int_Cv,"/IVC Amount",
            		pnd_Cntl_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Ivc_Cv,"/Taxable Amount",
            		pnd_Cntl_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Taxable_Cv,"Federal Tax",
            		pnd_Cntl_Pnd_Fed_Tax.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Fed_Cv,NEWLINE,"Backup Tax",
            		pnd_Cntl_Pnd_Int_Tax.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Int_Back_Cv,"State   Tax",
            		pnd_Cntl_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Sta_Cv,NEWLINE,"Local   Tax",
            		pnd_Cntl_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Loc_Cv);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1 LINES
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR07:                                                                                                                                                            //Natural: FOR #I = 1 TO 5
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(5)); pnd_Ws_Pnd_I.nadd(1))
        {
            pnd_St_Tot_Pnd_Tran_Cnt.getValue(60 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Tot_Pnd_Tran_Cnt.getValue(0 + 1,":",59 + 1,1,pnd_Ws_Pnd_I));                              //Natural: ADD #ST-TOT.#TRAN-CNT ( 0:59,1,#I ) TO #ST-TOT.#TRAN-CNT ( 60,1,#I )
            pnd_St_Tot_Pnd_Tran_Cnt.getValue(62 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Tot_Pnd_Tran_Cnt.getValue(60 + 1,":",61 + 1,1,pnd_Ws_Pnd_I));                             //Natural: ADD #ST-TOT.#TRAN-CNT ( 60:61,1,#I ) TO #ST-TOT.#TRAN-CNT ( 62,1,#I )
            pnd_St_Tot_Pnd_Gross_Amt.getValue(60 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Tot_Pnd_Gross_Amt.getValue(0 + 1,":",59 + 1,1,pnd_Ws_Pnd_I));                            //Natural: ADD #ST-TOT.#GROSS-AMT ( 0:59,1,#I ) TO #ST-TOT.#GROSS-AMT ( 60,1,#I )
            pnd_St_Tot_Pnd_Gross_Amt.getValue(62 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Tot_Pnd_Gross_Amt.getValue(60 + 1,":",61 + 1,1,pnd_Ws_Pnd_I));                           //Natural: ADD #ST-TOT.#GROSS-AMT ( 60:61,1,#I ) TO #ST-TOT.#GROSS-AMT ( 62,1,#I )
            pnd_St_Tot_Pnd_Pymnt_Ivc.getValue(60 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Tot_Pnd_Pymnt_Ivc.getValue(0 + 1,":",59 + 1,1,pnd_Ws_Pnd_I));                            //Natural: ADD #ST-TOT.#PYMNT-IVC ( 0:59,1,#I ) TO #ST-TOT.#PYMNT-IVC ( 60,1,#I )
            pnd_St_Tot_Pnd_Pymnt_Ivc.getValue(62 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Tot_Pnd_Pymnt_Ivc.getValue(60 + 1,":",61 + 1,1,pnd_Ws_Pnd_I));                           //Natural: ADD #ST-TOT.#PYMNT-IVC ( 60:61,1,#I ) TO #ST-TOT.#PYMNT-IVC ( 62,1,#I )
            pnd_St_Tot_Pnd_Ivc_Adj.getValue(60 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Tot_Pnd_Ivc_Adj.getValue(0 + 1,":",59 + 1,1,pnd_Ws_Pnd_I));                                //Natural: ADD #ST-TOT.#IVC-ADJ ( 0:59,1,#I ) TO #ST-TOT.#IVC-ADJ ( 60,1,#I )
            pnd_St_Tot_Pnd_Ivc_Adj.getValue(62 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Tot_Pnd_Ivc_Adj.getValue(60 + 1,":",61 + 1,1,pnd_Ws_Pnd_I));                               //Natural: ADD #ST-TOT.#IVC-ADJ ( 60:61,1,#I ) TO #ST-TOT.#IVC-ADJ ( 62,1,#I )
            pnd_St_Tot_Pnd_Form_Ivc.getValue(0 + 1,":",59 + 1,1,pnd_Ws_Pnd_I).compute(new ComputeParameters(false, pnd_St_Tot_Pnd_Form_Ivc.getValue(0                     //Natural: ASSIGN #ST-TOT.#FORM-IVC ( 0:59,1,#I ) := #ST-TOT.#PYMNT-IVC ( 0:59,1,#I ) - #ST-TOT.#IVC-ADJ ( 0:59,1,#I )
                + 1,":",59 + 1,1,pnd_Ws_Pnd_I)), pnd_St_Tot_Pnd_Pymnt_Ivc.getValue(0 + 1,":",59 + 1,1,pnd_Ws_Pnd_I).subtract(pnd_St_Tot_Pnd_Ivc_Adj.getValue(0 
                + 1,":",59 + 1,1,pnd_Ws_Pnd_I)));
            pnd_St_Tot_Pnd_Form_Ivc.getValue(61 + 1,1,pnd_Ws_Pnd_I).compute(new ComputeParameters(false, pnd_St_Tot_Pnd_Form_Ivc.getValue(61 + 1,1,pnd_Ws_Pnd_I)),        //Natural: ASSIGN #ST-TOT.#FORM-IVC ( 61,1,#I ) := #ST-TOT.#PYMNT-IVC ( 61,1,#I ) - #ST-TOT.#IVC-ADJ ( 61,1,#I )
                pnd_St_Tot_Pnd_Pymnt_Ivc.getValue(61 + 1,1,pnd_Ws_Pnd_I).subtract(pnd_St_Tot_Pnd_Ivc_Adj.getValue(61 + 1,1,pnd_Ws_Pnd_I)));
            pnd_St_Tot_Pnd_Form_Ivc.getValue(60 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Tot_Pnd_Form_Ivc.getValue(0 + 1,":",59 + 1,1,pnd_Ws_Pnd_I));                              //Natural: ADD #ST-TOT.#FORM-IVC ( 0:59,1,#I ) TO #ST-TOT.#FORM-IVC ( 60,1,#I )
            pnd_St_Tot_Pnd_Form_Ivc.getValue(62 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Tot_Pnd_Form_Ivc.getValue(60 + 1,":",61 + 1,1,pnd_Ws_Pnd_I));                             //Natural: ADD #ST-TOT.#FORM-IVC ( 60:61,1,#I ) TO #ST-TOT.#FORM-IVC ( 62,1,#I )
            pnd_St_To1_Pnd_Pmnt_Taxable.getValue(60 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_To1_Pnd_Pmnt_Taxable.getValue(0 + 1,":",59 + 1,1,pnd_Ws_Pnd_I));                      //Natural: ADD #ST-TO1.#PMNT-TAXABLE ( 0:59,1,#I ) TO #ST-TO1.#PMNT-TAXABLE ( 60,1,#I )
            pnd_St_To1_Pnd_Pmnt_Taxable.getValue(62 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_To1_Pnd_Pmnt_Taxable.getValue(60 + 1,":",61 + 1,1,pnd_Ws_Pnd_I));                     //Natural: ADD #ST-TO1.#PMNT-TAXABLE ( 60:61,1,#I ) TO #ST-TO1.#PMNT-TAXABLE ( 62,1,#I )
            pnd_St_To1_Pnd_Taxable_Adj.getValue(60 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_To1_Pnd_Taxable_Adj.getValue(0 + 1,":",59 + 1,1,pnd_Ws_Pnd_I));                        //Natural: ADD #ST-TO1.#TAXABLE-ADJ ( 0:59,1,#I ) TO #ST-TO1.#TAXABLE-ADJ ( 60,1,#I )
            pnd_St_To1_Pnd_Taxable_Adj.getValue(62 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_To1_Pnd_Taxable_Adj.getValue(60 + 1,":",61 + 1,1,pnd_Ws_Pnd_I));                       //Natural: ADD #ST-TO1.#TAXABLE-ADJ ( 60:61,1,#I ) TO #ST-TO1.#TAXABLE-ADJ ( 62,1,#I )
            pnd_St_To1_Pnd_Form_Taxable.getValue(0 + 1,":",59 + 1,1,pnd_Ws_Pnd_I).compute(new ComputeParameters(false, pnd_St_To1_Pnd_Form_Taxable.getValue(0             //Natural: ASSIGN #ST-TO1.#FORM-TAXABLE ( 0:59,1,#I ) := #ST-TO1.#PMNT-TAXABLE ( 0:59,1,#I ) + #ST-TO1.#TAXABLE-ADJ ( 0:59,1,#I )
                + 1,":",59 + 1,1,pnd_Ws_Pnd_I)), pnd_St_To1_Pnd_Pmnt_Taxable.getValue(0 + 1,":",59 + 1,1,pnd_Ws_Pnd_I).add(pnd_St_To1_Pnd_Taxable_Adj.getValue(0 
                + 1,":",59 + 1,1,pnd_Ws_Pnd_I)));
            pnd_St_To1_Pnd_Form_Taxable.getValue(61 + 1,1,pnd_Ws_Pnd_I).compute(new ComputeParameters(false, pnd_St_To1_Pnd_Form_Taxable.getValue(61 +                    //Natural: ASSIGN #ST-TO1.#FORM-TAXABLE ( 61,1,#I ) := #ST-TO1.#PMNT-TAXABLE ( 61,1,#I ) + #ST-TO1.#TAXABLE-ADJ ( 61,1,#I )
                1,1,pnd_Ws_Pnd_I)), pnd_St_To1_Pnd_Pmnt_Taxable.getValue(61 + 1,1,pnd_Ws_Pnd_I).add(pnd_St_To1_Pnd_Taxable_Adj.getValue(61 + 1,1,pnd_Ws_Pnd_I)));
            pnd_St_To1_Pnd_Form_Taxable.getValue(60 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_To1_Pnd_Form_Taxable.getValue(0 + 1,":",59 + 1,1,pnd_Ws_Pnd_I));                      //Natural: ADD #ST-TO1.#FORM-TAXABLE ( 0:59,1,#I ) TO #ST-TO1.#FORM-TAXABLE ( 60,1,#I )
            pnd_St_To1_Pnd_Form_Taxable.getValue(62 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_To1_Pnd_Form_Taxable.getValue(60 + 1,":",61 + 1,1,pnd_Ws_Pnd_I));                     //Natural: ADD #ST-TO1.#FORM-TAXABLE ( 60:61,1,#I ) TO #ST-TO1.#FORM-TAXABLE ( 62,1,#I )
            pnd_St_Tot_Pnd_Sta_Tax.getValue(60 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Tot_Pnd_Sta_Tax.getValue(0 + 1,":",59 + 1,1,pnd_Ws_Pnd_I));                                //Natural: ADD #ST-TOT.#STA-TAX ( 0:59,1,#I ) TO #ST-TOT.#STA-TAX ( 60,1,#I )
            pnd_St_Tot_Pnd_Sta_Tax.getValue(62 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Tot_Pnd_Sta_Tax.getValue(60 + 1,":",61 + 1,1,pnd_Ws_Pnd_I));                               //Natural: ADD #ST-TOT.#STA-TAX ( 60:61,1,#I ) TO #ST-TOT.#STA-TAX ( 62,1,#I )
            pnd_St_Tot_Pnd_Loc_Tax.getValue(60 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Tot_Pnd_Loc_Tax.getValue(0 + 1,":",59 + 1,1,pnd_Ws_Pnd_I));                                //Natural: ADD #ST-TOT.#LOC-TAX ( 0:59,1,#I ) TO #ST-TOT.#LOC-TAX ( 60,1,#I )
            pnd_St_Tot_Pnd_Loc_Tax.getValue(62 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Tot_Pnd_Loc_Tax.getValue(60 + 1,":",61 + 1,1,pnd_Ws_Pnd_I));                               //Natural: ADD #ST-TOT.#LOC-TAX ( 60:61,1,#I ) TO #ST-TOT.#LOC-TAX ( 62,1,#I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(3));                                                                                                                 //Natural: NEWPAGE ( 3 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(4));                                                                                                                 //Natural: NEWPAGE ( 4 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(5));                                                                                                                 //Natural: NEWPAGE ( 5 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(6));                                                                                                                 //Natural: NEWPAGE ( 6 )
        if (condition(Global.isEscape())){return;}
        FOR08:                                                                                                                                                            //Natural: FOR #WS.#S1 = 0 TO #ST-TOT-MAX
        for (pnd_Ws_Pnd_S1.setValue(0); condition(pnd_Ws_Pnd_S1.lessOrEqual(pnd_Ws_Const_Pnd_St_Tot_Max)); pnd_Ws_Pnd_S1.nadd(1))
        {
            if (condition(pnd_St_Cntl_Pnd_State_Desc.getValue(pnd_Ws_Pnd_S1.getInt() + 1).notEquals(" ")))                                                                //Natural: IF #ST-CNTL.#STATE-DESC ( #S1 ) NE ' '
            {
                getReports().display(2, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",                     //Natural: DISPLAY ( 2 ) ( HC = R ES = ON ) '/' #ST-CNTL.#STATE-DESC ( #S1 ) '//Trans Count' #ST-TOT.#TRAN-CNT ( #S1,#S2,1 ) '//Gross Amount' #ST-TOT.#GROSS-AMT ( #S1,#S2,1 ) 'Payment IVC' #ST-TOT.#PYMNT-IVC ( #S1,#S2,1 ) / 'Adjustment IVC' #ST-TOT.#IVC-ADJ ( #S1,#S2,1 ) / 'Form IVC' #ST-TOT.#FORM-IVC ( #S1,#S2,1 ) 'Payment Taxable' #ST-TO1.#PMNT-TAXABLE ( #S1,#S2,1 ) / 'Adjustment Taxable' #ST-TO1.#TAXABLE-ADJ ( #S1,#S2,1 ) / 'Form Taxable' #ST-TO1.#FORM-TAXABLE ( #S1,#S2,1 ) '//State Tax' #ST-TOT.#STA-TAX ( #S1,#S2,1 ) '//Local Tax' #ST-TOT.#LOC-TAX ( #S1,#S2,1 )
                		pnd_St_Cntl_Pnd_State_Desc.getValue(pnd_Ws_Pnd_S1.getInt() + 1),"//Trans Count",
                		pnd_St_Tot_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1),"//Gross Amount",
                		pnd_St_Tot_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1),"Payment IVC",
                		pnd_St_Tot_Pnd_Pymnt_Ivc.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1),NEWLINE,"Adjustment IVC",
                		pnd_St_Tot_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1),NEWLINE,"Form IVC",
                		pnd_St_Tot_Pnd_Form_Ivc.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1),"Payment Taxable",
                		pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1),NEWLINE,"Adjustment Taxable",
                		pnd_St_To1_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1),NEWLINE,"Form Taxable",
                		pnd_St_To1_Pnd_Form_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1),"//State Tax",
                		pnd_St_Tot_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1),"//Local Tax",
                		pnd_St_Tot_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_S1.equals(59) || pnd_Ws_Pnd_S1.equals(61)))                                                                                      //Natural: IF #WS.#S1 = 59 OR = 61
                {
                    getReports().write(2, new ReportTAsterisk(pnd_St_Tot_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1)),"-",new RepeatItem(12),new    //Natural: WRITE ( 2 ) T*#ST-TOT.#TRAN-CNT ( #S1,#S2,1 ) '-' ( 12 ) T*#ST-TOT.#GROSS-AMT ( #S1,#S2,1 ) '-' ( 18 ) T*#ST-TOT.#PYMNT-IVC ( #S1,#S2,1 ) '-' ( 15 ) T*#ST-TO1.#PMNT-TAXABLE ( #S1,#S2,1 ) '-' ( 18 ) T*#ST-TOT.#STA-TAX ( #S1,#S2,1 ) '-' ( 15 ) T*#ST-TOT.#LOC-TAX ( #S1,#S2,1 ) '-' ( 15 )
                        ReportTAsterisk(pnd_St_Tot_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1)),"-",new RepeatItem(18),new ReportTAsterisk(pnd_St_Tot_Pnd_Pymnt_Ivc.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1)),"-",new 
                        RepeatItem(15),new ReportTAsterisk(pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1)),"-",new RepeatItem(18),new 
                        ReportTAsterisk(pnd_St_Tot_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1)),"-",new RepeatItem(15),new ReportTAsterisk(pnd_St_Tot_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1)),"-",new 
                        RepeatItem(15));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_St_Cntl_Pnd_State_Desc.getValue(pnd_Ws_Pnd_S1.getInt() + 1).notEquals(" ")))                                                                //Natural: IF #ST-CNTL.#STATE-DESC ( #S1 ) NE ' '
            {
                getReports().display(3, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",                     //Natural: DISPLAY ( 3 ) ( HC = R ES = ON ) '/' #ST-CNTL.#STATE-DESC ( #S1 ) '//Trans Count' #ST-TOT.#TRAN-CNT ( #S1,#S2,2 ) '//Gross Amount' #ST-TOT.#GROSS-AMT ( #S1,#S2,2 ) 'Payment IVC' #ST-TOT.#PYMNT-IVC ( #S1,#S2,2 ) / 'Adjustment IVC' #ST-TOT.#IVC-ADJ ( #S1,#S2,2 ) / 'Form IVC' #ST-TOT.#FORM-IVC ( #S1,#S2,2 ) 'Payment Taxable' #ST-TO1.#PMNT-TAXABLE ( #S1,#S2,2 ) / 'Adjustment Taxable' #ST-TO1.#TAXABLE-ADJ ( #S1,#S2,2 ) / 'Form Taxable' #ST-TO1.#FORM-TAXABLE ( #S1,#S2,2 ) '//State Tax' #ST-TOT.#STA-TAX ( #S1,#S2,2 ) '//Local Tax' #ST-TOT.#LOC-TAX ( #S1,#S2,2 )
                		pnd_St_Cntl_Pnd_State_Desc.getValue(pnd_Ws_Pnd_S1.getInt() + 1),"//Trans Count",
                		pnd_St_Tot_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2),"//Gross Amount",
                		pnd_St_Tot_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2),"Payment IVC",
                		pnd_St_Tot_Pnd_Pymnt_Ivc.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2),NEWLINE,"Adjustment IVC",
                		pnd_St_Tot_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2),NEWLINE,"Form IVC",
                		pnd_St_Tot_Pnd_Form_Ivc.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2),"Payment Taxable",
                		pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2),NEWLINE,"Adjustment Taxable",
                		pnd_St_To1_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2),NEWLINE,"Form Taxable",
                		pnd_St_To1_Pnd_Form_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2),"//State Tax",
                		pnd_St_Tot_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2),"//Local Tax",
                		pnd_St_Tot_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_S1.equals(59) || pnd_Ws_Pnd_S1.equals(61)))                                                                                      //Natural: IF #WS.#S1 = 59 OR = 61
                {
                    getReports().write(3, new ReportTAsterisk(pnd_St_Tot_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2)),"-",new RepeatItem(12),new    //Natural: WRITE ( 3 ) T*#ST-TOT.#TRAN-CNT ( #S1,#S2,2 ) '-' ( 12 ) T*#ST-TOT.#GROSS-AMT ( #S1,#S2,2 ) '-' ( 18 ) T*#ST-TOT.#PYMNT-IVC ( #S1,#S2,2 ) '-' ( 15 ) T*#ST-TO1.#PMNT-TAXABLE ( #S1,#S2,2 ) '-' ( 18 ) T*#ST-TOT.#STA-TAX ( #S1,#S2,2 ) '-' ( 15 ) T*#ST-TOT.#LOC-TAX ( #S1,#S2,2 ) '-' ( 15 )
                        ReportTAsterisk(pnd_St_Tot_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2)),"-",new RepeatItem(18),new ReportTAsterisk(pnd_St_Tot_Pnd_Pymnt_Ivc.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2)),"-",new 
                        RepeatItem(15),new ReportTAsterisk(pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2)),"-",new RepeatItem(18),new 
                        ReportTAsterisk(pnd_St_Tot_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2)),"-",new RepeatItem(15),new ReportTAsterisk(pnd_St_Tot_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2)),"-",new 
                        RepeatItem(15));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_St_Cntl_Pnd_State_Desc.getValue(pnd_Ws_Pnd_S1.getInt() + 1).notEquals(" ") && pnd_St_Cntl_Pnd_State_Gross_Rpt.getValue(pnd_Ws_Pnd_S1.getInt()  //Natural: IF #ST-CNTL.#STATE-DESC ( #S1 ) NE ' ' AND #ST-CNTL.#STATE-GROSS-RPT ( #S1 )
                + 1).getBoolean()))
            {
                getReports().display(4, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",                     //Natural: DISPLAY ( 4 ) ( HC = R ES = ON ) '/' #ST-CNTL.#STATE-DESC ( #S1 ) '//Trans Count' #ST-TOT.#TRAN-CNT ( #S1,#S2,3 ) '//Gross Amount' #ST-TOT.#GROSS-AMT ( #S1,#S2,3 ) 'Payment IVC' #ST-TOT.#PYMNT-IVC ( #S1,#S2,3 ) / 'Adjustment IVC' #ST-TOT.#IVC-ADJ ( #S1,#S2,3 ) / 'Form IVC' #ST-TOT.#FORM-IVC ( #S1,#S2,3 ) 'Payment Taxable' #ST-TO1.#PMNT-TAXABLE ( #S1,#S2,3 ) / 'Adjustment Taxable' #ST-TO1.#TAXABLE-ADJ ( #S1,#S2,3 ) / 'Form Taxable' #ST-TO1.#FORM-TAXABLE ( #S1,#S2,3 ) '//State Tax' #ST-TOT.#STA-TAX ( #S1,#S2,3 ) '//Local Tax' #ST-TOT.#LOC-TAX ( #S1,#S2,3 )
                		pnd_St_Cntl_Pnd_State_Desc.getValue(pnd_Ws_Pnd_S1.getInt() + 1),"//Trans Count",
                		pnd_St_Tot_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3),"//Gross Amount",
                		pnd_St_Tot_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3),"Payment IVC",
                		pnd_St_Tot_Pnd_Pymnt_Ivc.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3),NEWLINE,"Adjustment IVC",
                		pnd_St_Tot_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3),NEWLINE,"Form IVC",
                		pnd_St_Tot_Pnd_Form_Ivc.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3),"Payment Taxable",
                		pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3),NEWLINE,"Adjustment Taxable",
                		pnd_St_To1_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3),NEWLINE,"Form Taxable",
                		pnd_St_To1_Pnd_Form_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3),"//State Tax",
                		pnd_St_Tot_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3),"//Local Tax",
                		pnd_St_Tot_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_S1.equals(59) || pnd_Ws_Pnd_S1.equals(61)))                                                                                          //Natural: IF #WS.#S1 = 59 OR = 61
            {
                getReports().write(4, new ReportTAsterisk(pnd_St_Tot_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3)),"-",new RepeatItem(12),new        //Natural: WRITE ( 4 ) T*#ST-TOT.#TRAN-CNT ( #S1,#S2,3 ) '-' ( 12 ) T*#ST-TOT.#GROSS-AMT ( #S1,#S2,3 ) '-' ( 18 ) T*#ST-TOT.#PYMNT-IVC ( #S1,#S2,3 ) '-' ( 15 ) T*#ST-TO1.#PMNT-TAXABLE ( #S1,#S2,3 ) '-' ( 18 ) T*#ST-TOT.#STA-TAX ( #S1,#S2,3 ) '-' ( 15 ) T*#ST-TOT.#LOC-TAX ( #S1,#S2,3 ) '-' ( 15 )
                    ReportTAsterisk(pnd_St_Tot_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3)),"-",new RepeatItem(18),new ReportTAsterisk(pnd_St_Tot_Pnd_Pymnt_Ivc.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3)),"-",new 
                    RepeatItem(15),new ReportTAsterisk(pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3)),"-",new RepeatItem(18),new 
                    ReportTAsterisk(pnd_St_Tot_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3)),"-",new RepeatItem(15),new ReportTAsterisk(pnd_St_Tot_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3)),"-",new 
                    RepeatItem(15));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((pnd_St_Cntl_Pnd_State_Desc.getValue(pnd_Ws_Pnd_S1.getInt() + 1).notEquals(" ") && ! (pnd_St_Cntl_Pnd_State_Gross_Rpt.getValue(pnd_Ws_Pnd_S1.getInt()  //Natural: IF #ST-CNTL.#STATE-DESC ( #S1 ) NE ' ' AND NOT #ST-CNTL.#STATE-GROSS-RPT ( #S1 ) OR #WS.#S1 = 60
                + 1).equals(true))) || pnd_Ws_Pnd_S1.equals(60))))
            {
                getReports().display(5, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",                     //Natural: DISPLAY ( 5 ) ( HC = R ES = ON ) '/' #ST-CNTL.#STATE-DESC ( #S1 ) '//Trans Count' #ST-TOT.#TRAN-CNT ( #S1,#S2,4 ) '//Gross Amount' #ST-TOT.#GROSS-AMT ( #S1,#S2,4 ) 'Payment IVC' #ST-TOT.#PYMNT-IVC ( #S1,#S2,4 ) / 'Adjustment IVC' #ST-TOT.#IVC-ADJ ( #S1,#S2,4 ) / 'Form IVC' #ST-TOT.#FORM-IVC ( #S1,#S2,4 ) 'Payment Taxable' #ST-TO1.#PMNT-TAXABLE ( #S1,#S2,4 ) / 'Adjustment Taxable' #ST-TO1.#TAXABLE-ADJ ( #S1,#S2,4 ) / 'Form Taxable' #ST-TO1.#FORM-TAXABLE ( #S1,#S2,4 ) '//State Tax' #ST-TOT.#STA-TAX ( #S1,#S2,4 ) '//Local Tax' #ST-TOT.#LOC-TAX ( #S1,#S2,4 )
                		pnd_St_Cntl_Pnd_State_Desc.getValue(pnd_Ws_Pnd_S1.getInt() + 1),"//Trans Count",
                		pnd_St_Tot_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4),"//Gross Amount",
                		pnd_St_Tot_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4),"Payment IVC",
                		pnd_St_Tot_Pnd_Pymnt_Ivc.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4),NEWLINE,"Adjustment IVC",
                		pnd_St_Tot_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4),NEWLINE,"Form IVC",
                		pnd_St_Tot_Pnd_Form_Ivc.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4),"Payment Taxable",
                		pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4),NEWLINE,"Adjustment Taxable",
                		pnd_St_To1_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4),NEWLINE,"Form Taxable",
                		pnd_St_To1_Pnd_Form_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4),"//State Tax",
                		pnd_St_Tot_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4),"//Local Tax",
                		pnd_St_Tot_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_S1.equals(59)))                                                                                                                      //Natural: IF #WS.#S1 = 59
            {
                getReports().write(5, new ReportTAsterisk(pnd_St_Tot_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4)),"-",new RepeatItem(12),new        //Natural: WRITE ( 5 ) T*#ST-TOT.#TRAN-CNT ( #S1,#S2,4 ) '-' ( 12 ) T*#ST-TOT.#GROSS-AMT ( #S1,#S2,4 ) '-' ( 18 ) T*#ST-TOT.#PYMNT-IVC ( #S1,#S2,4 ) '-' ( 15 ) T*#ST-TO1.#PMNT-TAXABLE ( #S1,#S2,4 ) '-' ( 18 ) T*#ST-TOT.#STA-TAX ( #S1,#S2,4 ) '-' ( 15 ) T*#ST-TOT.#LOC-TAX ( #S1,#S2,4 ) '-' ( 15 )
                    ReportTAsterisk(pnd_St_Tot_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4)),"-",new RepeatItem(18),new ReportTAsterisk(pnd_St_Tot_Pnd_Pymnt_Ivc.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4)),"-",new 
                    RepeatItem(15),new ReportTAsterisk(pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4)),"-",new RepeatItem(18),new 
                    ReportTAsterisk(pnd_St_Tot_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4)),"-",new RepeatItem(15),new ReportTAsterisk(pnd_St_Tot_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4)),"-",new 
                    RepeatItem(15));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_St_Cntl_Pnd_State_Desc.getValue(pnd_Ws_Pnd_S1.getInt() + 1).notEquals(" ") && pnd_St_Cntl_Pnd_State_Med.getValue(pnd_Ws_Pnd_S1.getInt()     //Natural: IF #ST-CNTL.#STATE-DESC ( #S1 ) NE ' ' AND #ST-CNTL.#STATE-MED ( #S1 ) NE ' ' OR #WS.#S1 = 60
                + 1).notEquals(" ") || pnd_Ws_Pnd_S1.equals(60)))
            {
                getReports().display(6, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",                     //Natural: DISPLAY ( 6 ) ( HC = R ES = ON ) '/' #ST-CNTL.#STATE-DESC ( #S1 ) '//Trans Count' #ST-TOT.#TRAN-CNT ( #S1,#S2,5 ) '//Gross Amount' #ST-TOT.#GROSS-AMT ( #S1,#S2,5 ) 'Payment IVC' #ST-TOT.#PYMNT-IVC ( #S1,#S2,5 ) / 'Adjustment IVC' #ST-TOT.#IVC-ADJ ( #S1,#S2,5 ) / 'Form IVC' #ST-TOT.#FORM-IVC ( #S1,#S2,5 ) 'Payment Taxable' #ST-TO1.#PMNT-TAXABLE ( #S1,#S2,5 ) / 'Adjustment Taxable' #ST-TO1.#TAXABLE-ADJ ( #S1,#S2,5 ) / 'Form Taxable' #ST-TO1.#FORM-TAXABLE ( #S1,#S2,5 ) '//State Tax' #ST-TOT.#STA-TAX ( #S1,#S2,5 ) '//Local Tax' #ST-TOT.#LOC-TAX ( #S1,#S2,5 )
                		pnd_St_Cntl_Pnd_State_Desc.getValue(pnd_Ws_Pnd_S1.getInt() + 1),"//Trans Count",
                		pnd_St_Tot_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5),"//Gross Amount",
                		pnd_St_Tot_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5),"Payment IVC",
                		pnd_St_Tot_Pnd_Pymnt_Ivc.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5),NEWLINE,"Adjustment IVC",
                		pnd_St_Tot_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5),NEWLINE,"Form IVC",
                		pnd_St_Tot_Pnd_Form_Ivc.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5),"Payment Taxable",
                		pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5),NEWLINE,"Adjustment Taxable",
                		pnd_St_To1_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5),NEWLINE,"Form Taxable",
                		pnd_St_To1_Pnd_Form_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5),"//State Tax",
                		pnd_St_Tot_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5),"//Local Tax",
                		pnd_St_Tot_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_S1.equals(59)))                                                                                                                      //Natural: IF #WS.#S1 = 59
            {
                getReports().write(6, new ReportTAsterisk(pnd_St_Tot_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5)),"-",new RepeatItem(12),new        //Natural: WRITE ( 6 ) T*#ST-TOT.#TRAN-CNT ( #S1,#S2,5 ) '-' ( 12 ) T*#ST-TOT.#GROSS-AMT ( #S1,#S2,5 ) '-' ( 18 ) T*#ST-TOT.#PYMNT-IVC ( #S1,#S2,5 ) '-' ( 15 ) T*#ST-TO1.#PMNT-TAXABLE ( #S1,#S2,5 ) '-' ( 18 ) T*#ST-TOT.#STA-TAX ( #S1,#S2,5 ) '-' ( 15 ) T*#ST-TOT.#LOC-TAX ( #S1,#S2,5 ) '-' ( 15 )
                    ReportTAsterisk(pnd_St_Tot_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5)),"-",new RepeatItem(18),new ReportTAsterisk(pnd_St_Tot_Pnd_Pymnt_Ivc.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5)),"-",new 
                    RepeatItem(15),new ReportTAsterisk(pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5)),"-",new RepeatItem(18),new 
                    ReportTAsterisk(pnd_St_Tot_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5)),"-",new RepeatItem(15),new ReportTAsterisk(pnd_St_Tot_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5)),"-",new 
                    RepeatItem(15));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Determine_Res_Idx() throws Exception                                                                                                                 //Natural: DETERMINE-RES-IDX
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Type().equals("1")))                                                                               //Natural: IF #XTAXYR-F94.TWRPYMNT-RESIDENCY-TYPE = '1'
        {
            short decideConditionsMet852 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #XTAXYR-F94.TWRPYMNT-RESIDENCY-CODE;//Natural: VALUE '00' : '57'
            if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code().equals("00' : '57"))))
            {
                decideConditionsMet852++;
                if (condition(DbsUtil.maskMatches(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code(),"99")))                                                         //Natural: IF #XTAXYR-F94.TWRPYMNT-RESIDENCY-CODE = MASK ( 99 )
                {
                    pnd_Ws_Pnd_Idx.compute(new ComputeParameters(false, pnd_Ws_Pnd_Idx), ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code().val());                  //Natural: ASSIGN #WS.#IDX := VAL ( #XTAXYR-F94.TWRPYMNT-RESIDENCY-CODE )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE '97'
            else if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code().equals("97"))))
            {
                decideConditionsMet852++;
                pnd_Ws_Pnd_Idx.setValue(58);                                                                                                                              //Natural: ASSIGN #WS.#IDX := 58
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Ws_Pnd_Idx.setValue(59);                                                                                                                              //Natural: ASSIGN #WS.#IDX := 59
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Idx.setValue(61);                                                                                                                                  //Natural: ASSIGN #WS.#IDX := 61
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_New_Res.reset();                                                                                                                                       //Natural: RESET #WS.#NEW-RES
    }
    private void sub_Determine_Contract_Type() throws Exception                                                                                                           //Natural: DETERMINE-CONTRACT-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        pnd_Ws_Pnd_Contract_Type_Mixed.reset();                                                                                                                           //Natural: RESET #WS.#CONTRACT-TYPE-MIXED
        //*  LUMP SUM
        if (condition(pnd_Prev_Val_Pnd_Paymt_Category.equals("3")))                                                                                                       //Natural: IF #PREV-VAL.#PAYMT-CATEGORY = '3'
        {
            if (condition(pdaTwra4001.getTwra4001_Pnd_Contract().notEquals(pnd_Prev_Val_Pnd_Contract_Nbr)))                                                               //Natural: IF TWRA4001.#CONTRACT NE #PREV-VAL.#CONTRACT-NBR
            {
                pdaTwra4001.getTwra4001_Pnd_Contract().setValue(pnd_Prev_Val_Pnd_Contract_Nbr);                                                                           //Natural: ASSIGN TWRA4001.#CONTRACT := #PREV-VAL.#CONTRACT-NBR
                DbsUtil.callnat(Twrn4001.class , getCurrentProcessState(), pdaTwra4001.getTwra4001());                                                                    //Natural: CALLNAT 'TWRN4001' USING TWRA4001
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTwra4001.getTwra4001_Pnd_Cntrct_Found().getBoolean()))                                                                                       //Natural: IF TWRA4001.#CNTRCT-FOUND
            {
                if (condition(pdaTwra4001.getTwra4001_Pnd_Ira_Type().equals("20")))                                                                                       //Natural: IF TWRA4001.#IRA-TYPE = '20'
                {
                    pnd_Ws_Pnd_Contract_Type.setValue("ROTH");                                                                                                            //Natural: ASSIGN #WS.#CONTRACT-TYPE := 'ROTH'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_Contract_Type.setValue("IRA");                                                                                                             //Natural: ASSIGN #WS.#CONTRACT-TYPE := 'IRA'
                }                                                                                                                                                         //Natural: END-IF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaIaaatxia.getIaaatxia_Pnd_Cntrct_Ppcn_Nbr().notEquals(pnd_Prev_Val_Pnd_Contract_Nbr) || pdaIaaatxia.getIaaatxia_Pnd_Cntrct_Payee_Cde().notEquals(pnd_Prev_Val_Pnd_Payee_Cde_N))) //Natural: IF IAAATXIA.#CNTRCT-PPCN-NBR NE #PREV-VAL.#CONTRACT-NBR OR IAAATXIA.#CNTRCT-PAYEE-CDE NE #PREV-VAL.#PAYEE-CDE-N
        {
            pdaIaaatxia.getIaaatxia_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Prev_Val_Pnd_Contract_Nbr);                                                                        //Natural: ASSIGN IAAATXIA.#CNTRCT-PPCN-NBR := #PREV-VAL.#CONTRACT-NBR
            pdaIaaatxia.getIaaatxia_Pnd_Cntrct_Payee_Cde().setValue(pnd_Prev_Val_Pnd_Payee_Cde_N);                                                                        //Natural: ASSIGN IAAATXIA.#CNTRCT-PAYEE-CDE := #PREV-VAL.#PAYEE-CDE-N
            DbsUtil.callnat(Iaantxi1.class , getCurrentProcessState(), pdaIaaatxia.getIaaatxia());                                                                        //Natural: CALLNAT 'IAANTXI1' USING IAAATXIA
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaIaaatxia.getIaaatxia_Pnd_Successful().getBoolean()))                                                                                             //Natural: IF IAAATXIA.#SUCCESSFUL
        {
            if (condition(pdaIaaatxia.getIaaatxia_Pnd_Money_Source().equals(" ") || pdaIaaatxia.getIaaatxia_Pnd_Money_Source().equals("SPIA")))                           //Natural: IF IAAATXIA.#MONEY-SOURCE = ' ' OR = 'SPIA'
            {
                pnd_Ws_Pnd_Contract_Type.setValue("DA-IA");                                                                                                               //Natural: ASSIGN #WS.#CONTRACT-TYPE := 'DA-IA'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Contract_Type.setValue(pdaIaaatxia.getIaaatxia_Pnd_Money_Source());                                                                            //Natural: ASSIGN #WS.#CONTRACT-TYPE := IAAATXIA.#MONEY-SOURCE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Company() throws Exception                                                                                                                   //Natural: PROCESS-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        DbsUtil.callnat(Twrncomp.class , getCurrentProcessState(), pdaTwracomp.getPnd_Twracomp_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracomp.getPnd_Twracomp_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOMP' USING #TWRACOMP.#INPUT-PARMS ( AD = O ) #TWRACOMP.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Load_State_Table() throws Exception                                                                                                                  //Natural: LOAD-STATE-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                             //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
        pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().setValue(false);                                                                                                          //Natural: ASSIGN TWRATBL2.#ABEND-IND := TWRATBL2.#DISPLAY-IND := FALSE
        pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().setValue(false);
        pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                                             //Natural: ASSIGN TWRATBL2.#TAX-YEAR := #WS.#TAX-YEAR
        pnd_St_Cntl_Pnd_State_Gross_Rpt.getValue("*").setValue(true);                                                                                                     //Natural: ASSIGN #ST-CNTL.#STATE-GROSS-RPT ( * ) := TRUE
        FOR09:                                                                                                                                                            //Natural: FOR #WS.#I = 0 TO 57
        for (pnd_Ws_Pnd_I.setValue(0); condition(pnd_Ws_Pnd_I.lessOrEqual(57)); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(pnd_Ws_Pnd_I.equals(42)))                                                                                                                       //Natural: IF #WS.#I = 42
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Idx.setValue(pnd_Ws_Pnd_I);                                                                                                                        //Natural: ASSIGN #WS.#IDX := #WS.#I
                                                                                                                                                                          //Natural: PERFORM LOAD-STATE-ENTRY
            sub_Load_State_Entry();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_I.setValue(97);                                                                                                                                        //Natural: ASSIGN #WS.#I := 97
        pnd_Ws_Pnd_Idx.setValue(58);                                                                                                                                      //Natural: ASSIGN #WS.#IDX := 58
                                                                                                                                                                          //Natural: PERFORM LOAD-STATE-ENTRY
        sub_Load_State_Entry();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Load_State_Entry() throws Exception                                                                                                                  //Natural: LOAD-STATE-ENTRY
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValueEdited(pnd_Ws_Pnd_I,new ReportEditMask("999"));                                                                   //Natural: MOVE EDITED #WS.#I ( EM = 999 ) TO TWRATBL2.#STATE-CDE
        DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        if (condition(pdaTwratbl2.getTwratbl2_Pnd_Return_Cde().getBoolean()))                                                                                             //Natural: IF TWRATBL2.#RETURN-CDE
        {
            pnd_St_Cntl_Pnd_State_Desc.getValue(pnd_Ws_Pnd_Idx.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Full_Name());                                 //Natural: ASSIGN #ST-CNTL.#STATE-DESC ( #IDX ) := TIRCNTL-STATE-FULL-NAME
            if (condition(pdaTwratbl2.getTwratbl2_Tircntl_State_Ind().equals(" ")))                                                                                       //Natural: IF TWRATBL2.TIRCNTL-STATE-IND = ' '
            {
                pnd_St_Cntl_Pnd_State_Rule.getValue(pnd_Ws_Pnd_Idx.getInt() + 1).setValue("9");                                                                           //Natural: ASSIGN #ST-CNTL.#STATE-RULE ( #IDX ) := '9'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_St_Cntl_Pnd_State_Rule.getValue(pnd_Ws_Pnd_Idx.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Ind());                                   //Natural: ASSIGN #ST-CNTL.#STATE-RULE ( #IDX ) := TWRATBL2.TIRCNTL-STATE-IND
            }                                                                                                                                                             //Natural: END-IF
            pnd_St_Cntl_Pnd_State_Cmb.getValue(pnd_Ws_Pnd_Idx.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_Comb_Fed_Ind());                                     //Natural: ASSIGN #ST-CNTL.#STATE-CMB ( #IDX ) := TWRATBL2.TIRCNTL-COMB-FED-IND
            pnd_St_Cntl_Pnd_State_Med.getValue(pnd_Ws_Pnd_Idx.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_Media_Code());                                       //Natural: ASSIGN #ST-CNTL.#STATE-MED ( #IDX ) := TWRATBL2.TIRCNTL-MEDIA-CODE
            if (condition(pnd_St_Cntl_Pnd_State_Rule.getValue(pnd_Ws_Pnd_Idx.getInt() + 1).equals("1") || pnd_St_Cntl_Pnd_State_Rule.getValue(pnd_Ws_Pnd_Idx.getInt()     //Natural: IF #ST-CNTL.#STATE-RULE ( #IDX ) = '1' OR = '2' OR = '5' OR = '6'
                + 1).equals("2") || pnd_St_Cntl_Pnd_State_Rule.getValue(pnd_Ws_Pnd_Idx.getInt() + 1).equals("5") || pnd_St_Cntl_Pnd_State_Rule.getValue(pnd_Ws_Pnd_Idx.getInt() 
                + 1).equals("6")))
            {
                pnd_St_Cntl_Pnd_State_Gross_Rpt.getValue(pnd_Ws_Pnd_Idx.getInt() + 1).reset();                                                                            //Natural: RESET #ST-CNTL.#STATE-GROSS-RPT ( #IDX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Key_Detail_Pnd_KeyIsBreak = pnd_Key_Detail_Pnd_Key.isBreak(endOfData);
        boolean pnd_Key_Detail_Pnd_Key28IsBreak = pnd_Key_Detail_Pnd_Key.isBreak(endOfData);
        boolean pnd_Key_Detail_Pnd_Key25IsBreak = pnd_Key_Detail_Pnd_Key.isBreak(endOfData);
        boolean ldaTwrl0900_getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_FormIsBreak = ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form().isBreak(endOfData);
        if (condition(pnd_Key_Detail_Pnd_KeyIsBreak || pnd_Key_Detail_Pnd_Key28IsBreak || pnd_Key_Detail_Pnd_Key25IsBreak || ldaTwrl0900_getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_FormIsBreak))
        {
            pnd_Ws_Pnd_New_Res.setValue(true);                                                                                                                            //Natural: ASSIGN #WS.#NEW-RES := TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Key_Detail_Pnd_KeyIsBreak || pnd_Key_Detail_Pnd_Key28IsBreak || pnd_Key_Detail_Pnd_Key25IsBreak || ldaTwrl0900_getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_FormIsBreak))
        {
            pnd_Prev_Val_Pnd_Distr_Cde.setValue(readWork01Twrpymnt_Distribution_CdeOld);                                                                                  //Natural: ASSIGN #PREV-VAL.#DISTR-CDE := OLD ( #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE )
            if (condition(pnd_Ws_Pnd_Contract_Type.equals(" ") || pnd_Ws_Pnd_Contract_Type_Mixed.getBoolean() || readWork01Twrpymnt_Paymt_CategoryOld.equals("3")))       //Natural: IF #WS.#CONTRACT-TYPE = ' ' OR #WS.#CONTRACT-TYPE-MIXED OR OLD ( #XTAXYR-F94.TWRPYMNT-PAYMT-CATEGORY ) = '3'
            {
                pnd_Prev_Val_Pnd_Paymt_Category.setValue(readWork01Twrpymnt_Paymt_CategoryOld);                                                                           //Natural: ASSIGN #PREV-VAL.#PAYMT-CATEGORY := OLD ( #XTAXYR-F94.TWRPYMNT-PAYMT-CATEGORY )
                pnd_Prev_Val_Pnd_Contract_Nbr.setValue(readWork01Twrpymnt_Contract_NbrOld);                                                                               //Natural: ASSIGN #PREV-VAL.#CONTRACT-NBR := OLD ( #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR )
                pnd_Prev_Val_Pnd_Payee_Cde.setValue(readWork01Twrpymnt_Payee_CdeOld);                                                                                     //Natural: ASSIGN #PREV-VAL.#PAYEE-CDE := OLD ( #XTAXYR-F94.TWRPYMNT-PAYEE-CDE )
                                                                                                                                                                          //Natural: PERFORM DETERMINE-CONTRACT-TYPE
                sub_Determine_Contract_Type();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #WS.#1099 = 1 TO #1099R-MAX
            for (pnd_Ws_Pnd_1099.setValue(1); condition(pnd_Ws_Pnd_1099.lessOrEqual(pnd_Ws_Const_Pnd_1099r_Max)); pnd_Ws_Pnd_1099.nadd(1))
            {
                if (condition(! (pnd_Case_Fields_Pnd_Case_Present.getValue(pnd_Ws_Pnd_1099).equals(true))))                                                               //Natural: IF NOT #CASE-FIELDS.#CASE-PRESENT ( #1099 )
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Case_Fields_Pnd_Rechar.getBoolean() || pnd_Case_Fields_Pnd_Roll.getBoolean()))                                                          //Natural: IF #CASE-FIELDS.#RECHAR OR #CASE-FIELDS.#ROLL
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cntl_Pnd_Taxable_Amt.getValue(2,1).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Taxable_Amt.getValue(2,1)), pnd_Cntl_Pnd_Taxable_Amt.getValue(2, //Natural: ASSIGN #CNTL.#TAXABLE-AMT ( 2,1 ) := #CNTL.#TAXABLE-AMT ( 2,1 ) + #CASE-FIELDS.#GROSS-AMT ( #1099,1 ) - #CASE-FIELDS.#IVC-AMT ( #1099,1 )
                        1).add(pnd_Case_Fields_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_1099,1)).subtract(pnd_Case_Fields_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099,
                        1)));
                }                                                                                                                                                         //Natural: END-IF
                if (condition(((((((pnd_Prev_Val_Pnd_Distr_Cde.equals("1") || pnd_Prev_Val_Pnd_Distr_Cde.equals("2")) || pnd_Prev_Val_Pnd_Distr_Cde.equals("3"))          //Natural: IF #PREV-VAL.#DISTR-CDE = '1' OR = '2' OR = '3' OR = '4' OR = '7' AND #1099 = 1 AND #WS.#CONTRACT-TYPE = 'IRA'
                    || pnd_Prev_Val_Pnd_Distr_Cde.equals("4")) || pnd_Prev_Val_Pnd_Distr_Cde.equals("7")) && pnd_Ws_Pnd_1099.equals(1)) && pnd_Ws_Pnd_Contract_Type.equals("IRA"))))
                {
                    pnd_Case_Fields_Pnd_Trad_Clas_Dist.getValue(pnd_Ws_Pnd_1099).setValue(true);                                                                          //Natural: ASSIGN #TRAD-CLAS-DIST ( #1099 ) := TRUE
                    pnd_Cntl_Pnd_Ivc_Amt.getValue(3,1).nadd(pnd_Case_Fields_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099,1));                                                     //Natural: ASSIGN #CNTL.#IVC-AMT ( 3,1 ) := #CNTL.#IVC-AMT ( 3,1 ) + #CASE-FIELDS.#IVC-AMT ( #1099,1 )
                    pnd_Cntl_Pnd_Taxable_Amt.getValue(3,1).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Taxable_Amt.getValue(3,1)), pnd_Cntl_Pnd_Taxable_Amt.getValue(3, //Natural: ASSIGN #CNTL.#TAXABLE-AMT ( 3,1 ) := #CNTL.#TAXABLE-AMT ( 3,1 ) - #CASE-FIELDS.#GROSS-AMT ( #1099,2 ) - #CASE-FIELDS.#IVC-AMT ( #1099,1 )
                        1).subtract(pnd_Case_Fields_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_1099,2)).subtract(pnd_Case_Fields_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099,
                        1)));
                }                                                                                                                                                         //Natural: END-IF
                pnd_Cntl_Pnd_Ivc_Amt.getValue(5,1).nadd(pnd_Case_Fields_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099,2));                                                         //Natural: ASSIGN #CNTL.#IVC-AMT ( 5,1 ) := #CNTL.#IVC-AMT ( 5,1 ) + #CASE-FIELDS.#IVC-AMT ( #1099,2 )
                if (condition(pnd_Case_Fields_Pnd_Tnd.getValue(pnd_Ws_Pnd_1099).equals(true) && ! (pnd_Case_Fields_Pnd_Roll.getBoolean()) && ! (pnd_Case_Fields_Pnd_Rechar.getBoolean())  //Natural: IF #CASE-FIELDS.#TND ( #1099 ) AND NOT #CASE-FIELDS.#ROLL AND NOT #CASE-FIELDS.#RECHAR AND NOT #TRAD-CLAS-DIST ( #1099 )
                    && ! (pnd_Case_Fields_Pnd_Trad_Clas_Dist.getValue(pnd_Ws_Pnd_1099).equals(true))))
                {
                    pnd_Cntl_Pnd_Ivc_Amt.getValue(6,1).nadd(pnd_Case_Fields_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099,1));                                                     //Natural: ASSIGN #CNTL.#IVC-AMT ( 6,1 ) := #CNTL.#IVC-AMT ( 6,1 ) + #CASE-FIELDS.#IVC-AMT ( #1099,1 )
                    pnd_Cntl_Pnd_Taxable_Amt.getValue(6,1).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Taxable_Amt.getValue(6,1)), pnd_Cntl_Pnd_Taxable_Amt.getValue(6, //Natural: ASSIGN #CNTL.#TAXABLE-AMT ( 6,1 ) := #CNTL.#TAXABLE-AMT ( 6,1 ) + #CASE-FIELDS.#GROSS-AMT ( #1099,1 ) - #CASE-FIELDS.#IVC-AMT ( #1099,1 )
                        1).add(pnd_Case_Fields_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_1099,1)).subtract(pnd_Case_Fields_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099,
                        1)));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IRA EXCESS & REAC
                    if (condition(pnd_Ws_Pnd_1099.equals(3) || pnd_Ws_Pnd_1099.equals(4) || pnd_Ws_Pnd_1099.equals(5)))                                                   //Natural: IF #1099 = 3 OR = 4 OR = 5
                    {
                        pnd_Cntl_Pnd_Ivc_Amt.getValue(4,1).nadd(pnd_Case_Fields_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099,1));                                                 //Natural: ASSIGN #CNTL.#IVC-AMT ( 4,1 ) := #CNTL.#IVC-AMT ( 4,1 ) + #CASE-FIELDS.#IVC-AMT ( #1099,1 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape())) return;
            FOR02:                                                                                                                                                        //Natural: FOR #WS.#1099 = 1 TO #1099R-MAX
            for (pnd_Ws_Pnd_1099.setValue(1); condition(pnd_Ws_Pnd_1099.lessOrEqual(pnd_Ws_Const_Pnd_1099r_Max)); pnd_Ws_Pnd_1099.nadd(1))
            {
                FOR03:                                                                                                                                                    //Natural: FOR #WS.#IDX = 0 TO #ST-TOT-MAX
                for (pnd_Ws_Pnd_Idx.setValue(0); condition(pnd_Ws_Pnd_Idx.lessOrEqual(pnd_Ws_Const_Pnd_St_Tot_Max)); pnd_Ws_Pnd_Idx.nadd(1))
                {
                    if (condition(! (pnd_St_Case_Pnd_Case_Present.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).equals(true))))                                   //Natural: IF NOT #ST-CASE.#CASE-PRESENT ( #1099,#IDX )
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_St_Tot_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,1).nadd(pnd_St_Case_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,                //Natural: ADD #ST-CASE.#TRAN-CNT ( #1099,#IDX ) TO #ST-TOT.#TRAN-CNT ( #IDX,1,1 )
                        pnd_Ws_Pnd_Idx));
                    pnd_St_Tot_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,1).nadd(pnd_St_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,              //Natural: ADD #ST-CASE.#GROSS-AMT ( #1099,#IDX,* ) TO #ST-TOT.#GROSS-AMT ( #IDX,1,1 )
                        pnd_Ws_Pnd_Idx,"*"));
                    pnd_St_Tot_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,1).nadd(pnd_St_Case_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_1099.getInt() + 1,                  //Natural: ADD #ST-CASE.#STA-TAX ( #1099,#IDX ) TO #ST-TOT.#STA-TAX ( #IDX,1,1 )
                        pnd_Ws_Pnd_Idx));
                    pnd_St_Tot_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,1).nadd(pnd_St_Case_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_1099.getInt() + 1,                  //Natural: ADD #ST-CASE.#LOC-TAX ( #1099,#IDX ) TO #ST-TOT.#LOC-TAX ( #IDX,1,1 )
                        pnd_Ws_Pnd_Idx));
                    pnd_St_Tot_Pnd_Pymnt_Ivc.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,1).nadd(pnd_St_Case_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,                //Natural: ADD #ST-CASE.#IVC-AMT ( #1099,#IDX,* ) TO #ST-TOT.#PYMNT-IVC ( #IDX,1,1 )
                        pnd_Ws_Pnd_Idx,"*"));
                    if (condition(pnd_Case_Fields_Pnd_Roll.getBoolean() || pnd_Case_Fields_Pnd_Rechar.getBoolean()))                                                      //Natural: IF #CASE-FIELDS.#ROLL OR #CASE-FIELDS.#RECHAR
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_St_Case_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).compute(new ComputeParameters(false, pnd_St_Case_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099.getInt()  //Natural: ASSIGN #ST-CASE.#TAXABLE-AMT ( #1099,#IDX ) := #ST-CASE.#GROSS-AMT ( #1099,#IDX,1 ) - #ST-CASE.#IVC-AMT ( #1099,#IDX,1 )
                            + 1,pnd_Ws_Pnd_Idx)), pnd_St_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx,1).subtract(pnd_St_Case_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099.getInt() 
                            + 1,pnd_Ws_Pnd_Idx,1)));
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Case_Fields_Pnd_Trad_Clas_Dist.getValue(pnd_Ws_Pnd_1099).getBoolean()))                                                             //Natural: IF #TRAD-CLAS-DIST ( #1099 )
                    {
                        pnd_St_Case_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).compute(new ComputeParameters(false, pnd_St_Case_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_1099.getInt()  //Natural: ASSIGN #ST-CASE.#IVC-ADJ ( #1099,#IDX ) := #ST-CASE.#IVC-AMT ( #1099,#IDX,1 ) + #ST-CASE.#IVC-AMT ( #1099,#IDX,2 )
                            + 1,pnd_Ws_Pnd_Idx)), pnd_St_Case_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx,1).add(pnd_St_Case_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099.getInt() 
                            + 1,pnd_Ws_Pnd_Idx,2)));
                        pnd_St_Case_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).compute(new ComputeParameters(false, pnd_St_Case_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_1099.getInt()  //Natural: ASSIGN #ST-CASE.#TAXABLE-ADJ ( #1099,#IDX ) := #ST-CASE.#GROSS-AMT ( #1099,#IDX,2 ) + #ST-CASE.#IVC-AMT ( #1099,#IDX,1 )
                            + 1,pnd_Ws_Pnd_Idx)), pnd_St_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx,2).add(pnd_St_Case_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099.getInt() 
                            + 1,pnd_Ws_Pnd_Idx,1)));
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_St_Case_Pnd_Tnd.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).equals(true) && ! (pnd_Case_Fields_Pnd_Roll.getBoolean())     //Natural: IF #ST-CASE.#TND ( #1099,#IDX ) AND NOT #CASE-FIELDS.#ROLL AND NOT #CASE-FIELDS.#RECHAR AND NOT #TRAD-CLAS-DIST ( #1099 )
                        && ! (pnd_Case_Fields_Pnd_Rechar.getBoolean()) && ! (pnd_Case_Fields_Pnd_Trad_Clas_Dist.getValue(pnd_Ws_Pnd_1099).equals(true))))
                    {
                        pnd_St_Case_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).compute(new ComputeParameters(false, pnd_St_Case_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_1099.getInt()  //Natural: ASSIGN #ST-CASE.#IVC-ADJ ( #1099,#IDX ) := #ST-CASE.#IVC-AMT ( #1099,#IDX,1 ) + #ST-CASE.#IVC-AMT ( #1099,#IDX,2 )
                            + 1,pnd_Ws_Pnd_Idx)), pnd_St_Case_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx,1).add(pnd_St_Case_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099.getInt() 
                            + 1,pnd_Ws_Pnd_Idx,2)));
                        pnd_St_Case_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).compute(new ComputeParameters(false, pnd_St_Case_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_1099.getInt()  //Natural: ASSIGN #ST-CASE.#TAXABLE-ADJ ( #1099,#IDX ) := - #ST-CASE.#TAXABLE-AMT ( #1099,#IDX )
                            + 1,pnd_Ws_Pnd_Idx)), DbsField.subtract(0,pnd_St_Case_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx)));
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  IRA EXCESS CONTRIBUTION & REAC
                        if (condition(pnd_Ws_Pnd_1099.equals(3) || pnd_Ws_Pnd_1099.equals(4) || pnd_Ws_Pnd_1099.equals(5)))                                               //Natural: IF #1099 = 3 OR = 4 OR = 5
                        {
                            pnd_St_Case_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).compute(new ComputeParameters(false, pnd_St_Case_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_1099.getInt()  //Natural: ASSIGN #ST-CASE.#IVC-ADJ ( #1099,#IDX ) := #ST-CASE.#IVC-AMT ( #1099,#IDX,1 ) + #ST-CASE.#IVC-AMT ( #1099,#IDX,2 )
                                + 1,pnd_Ws_Pnd_Idx)), pnd_St_Case_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx,1).add(pnd_St_Case_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099.getInt() 
                                + 1,pnd_Ws_Pnd_Idx,2)));
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,1).nadd(pnd_St_Case_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,         //Natural: ADD #ST-CASE.#TAXABLE-AMT ( #1099,#IDX ) TO #ST-TO1.#PMNT-TAXABLE ( #IDX,1,1 )
                        pnd_Ws_Pnd_Idx));
                    pnd_St_Tot_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,1).nadd(pnd_St_Case_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_1099.getInt() + 1,                  //Natural: ADD #ST-CASE.#IVC-ADJ ( #1099,#IDX ) TO #ST-TOT.#IVC-ADJ ( #IDX,1,1 )
                        pnd_Ws_Pnd_Idx));
                    pnd_St_To1_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,1).nadd(pnd_St_Case_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_1099.getInt() + 1,          //Natural: ADD #ST-CASE.#TAXABLE-ADJ ( #1099,#IDX ) TO #ST-TO1.#TAXABLE-ADJ ( #IDX,1,1 )
                        pnd_Ws_Pnd_Idx));
                    if (condition(pnd_St_Cntl_Pnd_State_Gross_Rpt.getValue(pnd_Ws_Pnd_Idx.getInt() + 1).getBoolean()))                                                    //Natural: IF #ST-CNTL.#STATE-GROSS-RPT ( #IDX )
                    {
                        pnd_St_Case_Pnd_State_Distr.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).compute(new ComputeParameters(false, pnd_St_Case_Pnd_State_Distr.getValue(pnd_Ws_Pnd_1099.getInt()  //Natural: ASSIGN #ST-CASE.#STATE-DISTR ( #1099,#IDX ) := #ST-CASE.#GROSS-AMT ( #1099,#IDX,1 ) + #ST-CASE.#GROSS-AMT ( #1099,#IDX,2 )
                            + 1,pnd_Ws_Pnd_Idx)), pnd_St_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx,1).add(pnd_St_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_1099.getInt() 
                            + 1,pnd_Ws_Pnd_Idx,2)));
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_St_Case_Pnd_State_Distr.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).compute(new ComputeParameters(false, pnd_St_Case_Pnd_State_Distr.getValue(pnd_Ws_Pnd_1099.getInt()  //Natural: ASSIGN #ST-CASE.#STATE-DISTR ( #1099,#IDX ) := #ST-CASE.#TAXABLE-AMT ( #1099,#IDX ) + #ST-CASE.#TAXABLE-ADJ ( #1099,#IDX )
                            + 1,pnd_Ws_Pnd_Idx)), pnd_St_Case_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).add(pnd_St_Case_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_1099.getInt() 
                            + 1,pnd_Ws_Pnd_Idx)));
                    }                                                                                                                                                     //Natural: END-IF
                    //*                                                                                                                                                   //Natural: DECIDE FOR FIRST CONDITION
                    short decideConditionsMet503 = 0;                                                                                                                     //Natural: WHEN #ST-CASE.#STATE-DISTR ( #1099,#IDX ) = 0.00 AND #ST-CASE.#STA-TAX ( #1099,#IDX ) = 0.00 AND #ST-CASE.#LOC-TAX ( #1099,#IDX ) = 0.00
                    if (condition(pnd_St_Case_Pnd_State_Distr.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).equals(new DbsDecimal("0.00")) && 
                        pnd_St_Case_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).equals(new DbsDecimal("0.00")) && pnd_St_Case_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_1099.getInt() 
                        + 1,pnd_Ws_Pnd_Idx).equals(new DbsDecimal("0.00"))))
                    {
                        decideConditionsMet503++;
                        ignore();
                    }                                                                                                                                                     //Natural: WHEN #WS.#IDX = 41
                    else if (condition(pnd_Ws_Pnd_Idx.equals(41)))
                    {
                        decideConditionsMet503++;
                        if (condition(pnd_Prev_Val_Pnd_Distr_Cde.equals("1") || pnd_St_Case_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).notEquals(new  //Natural: IF #PREV-VAL.#DISTR-CDE = '1' OR #ST-CASE.#STA-TAX ( #1099,#IDX ) NE 0.00 OR #ST-CASE.#LOC-TAX ( #1099,#IDX ) NE 0.00
                            DbsDecimal("0.00")) || pnd_St_Case_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).notEquals(new DbsDecimal("0.00"))))
                        {
                            pnd_St_Case_Pnd_Reportable_Distr.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).setValue(true);                                        //Natural: ASSIGN #ST-CASE.#REPORTABLE-DISTR ( #1099,#IDX ) := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN #ST-CNTL.#STATE-RULE ( #IDX ) = ' ' OR = '2' OR = '4' OR = '9'
                    else if (condition(pnd_St_Cntl_Pnd_State_Rule.getValue(pnd_Ws_Pnd_Idx.getInt() + 1).equals(" ") || pnd_St_Cntl_Pnd_State_Rule.getValue(pnd_Ws_Pnd_Idx.getInt() 
                        + 1).equals("2") || pnd_St_Cntl_Pnd_State_Rule.getValue(pnd_Ws_Pnd_Idx.getInt() + 1).equals("4") || pnd_St_Cntl_Pnd_State_Rule.getValue(pnd_Ws_Pnd_Idx.getInt() 
                        + 1).equals("9")))
                    {
                        decideConditionsMet503++;
                        if (condition(pnd_St_Case_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).notEquals(new DbsDecimal("0.00"))                     //Natural: IF #ST-CASE.#STA-TAX ( #1099,#IDX ) NE 0.00 OR #ST-CASE.#LOC-TAX ( #1099,#IDX ) NE 0.00
                            || pnd_St_Case_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).notEquals(new DbsDecimal("0.00"))))
                        {
                            pnd_St_Case_Pnd_Reportable_Distr.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).setValue(true);                                        //Natural: ASSIGN #ST-CASE.#REPORTABLE-DISTR ( #1099,#IDX ) := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        pnd_St_Case_Pnd_Reportable_Distr.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).setValue(true);                                            //Natural: ASSIGN #ST-CASE.#REPORTABLE-DISTR ( #1099,#IDX ) := TRUE
                    }                                                                                                                                                     //Natural: END-DECIDE
                    if (condition(pnd_St_Case_Pnd_Reportable_Distr.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).getBoolean()))                                   //Natural: IF #ST-CASE.#REPORTABLE-DISTR ( #1099,#IDX )
                    {
                        if (condition(pnd_St_Cntl_Pnd_State_Gross_Rpt.getValue(pnd_Ws_Pnd_Idx.getInt() + 1).getBoolean()))                                                //Natural: IF #ST-CNTL.#STATE-GROSS-RPT ( #IDX )
                        {
                            pnd_Ws_Pnd_I.setValue(3);                                                                                                                     //Natural: ASSIGN #WS.#I := 3
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Ws_Pnd_I.setValue(4);                                                                                                                     //Natural: ASSIGN #WS.#I := 4
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Pnd_I.setValue(2);                                                                                                                         //Natural: ASSIGN #WS.#I := 2
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_St_Tot_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Case_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,     //Natural: ADD #ST-CASE.#TRAN-CNT ( #1099,#IDX ) TO #ST-TOT.#TRAN-CNT ( #IDX,1,#I )
                        pnd_Ws_Pnd_Idx));
                    pnd_St_Tot_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,   //Natural: ADD #ST-CASE.#GROSS-AMT ( #1099,#IDX,* ) TO #ST-TOT.#GROSS-AMT ( #IDX,1,#I )
                        pnd_Ws_Pnd_Idx,"*"));
                    pnd_St_Tot_Pnd_Pymnt_Ivc.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Case_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,     //Natural: ADD #ST-CASE.#IVC-AMT ( #1099,#IDX,* ) TO #ST-TOT.#PYMNT-IVC ( #IDX,1,#I )
                        pnd_Ws_Pnd_Idx,"*"));
                    pnd_St_Tot_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Case_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_1099.getInt() + 1,       //Natural: ADD #ST-CASE.#IVC-ADJ ( #1099,#IDX ) TO #ST-TOT.#IVC-ADJ ( #IDX,1,#I )
                        pnd_Ws_Pnd_Idx));
                    pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Case_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099.getInt() + 1, //Natural: ADD #ST-CASE.#TAXABLE-AMT ( #1099,#IDX ) TO #ST-TO1.#PMNT-TAXABLE ( #IDX,1,#I )
                        pnd_Ws_Pnd_Idx));
                    pnd_St_To1_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Case_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_1099.getInt() + 1, //Natural: ADD #ST-CASE.#TAXABLE-ADJ ( #1099,#IDX ) TO #ST-TO1.#TAXABLE-ADJ ( #IDX,1,#I )
                        pnd_Ws_Pnd_Idx));
                    pnd_St_Tot_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Case_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_1099.getInt() + 1,       //Natural: ADD #ST-CASE.#STA-TAX ( #1099,#IDX ) TO #ST-TOT.#STA-TAX ( #IDX,1,#I )
                        pnd_Ws_Pnd_Idx));
                    pnd_St_Tot_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Case_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_1099.getInt() + 1,       //Natural: ADD #ST-CASE.#LOC-TAX ( #1099,#IDX ) TO #ST-TOT.#LOC-TAX ( #IDX,1,#I )
                        pnd_Ws_Pnd_Idx));
                    if (condition(pnd_St_Case_Pnd_Reportable_Distr.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).getBoolean()))                                   //Natural: IF #ST-CASE.#REPORTABLE-DISTR ( #1099,#IDX )
                    {
                        //*  P,R,M
                        if (condition(pnd_St_Cntl_Pnd_State_Med.getValue(pnd_Ws_Pnd_Idx.getInt() + 1).notEquals(" ") && (pnd_St_Case_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_1099.getInt()  //Natural: IF #ST-CNTL.#STATE-MED ( #IDX ) NE ' ' AND ( #ST-CASE.#STA-TAX ( #1099,#IDX ) NE 0.00 OR #ST-CASE.#LOC-TAX ( #1099,#IDX ) NE 0.00 )
                            + 1,pnd_Ws_Pnd_Idx).notEquals(new DbsDecimal("0.00")) || pnd_St_Case_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_1099.getInt() + 1,pnd_Ws_Pnd_Idx).notEquals(new 
                            DbsDecimal("0.00")))))
                        {
                            pnd_St_Tot_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,5).nadd(pnd_St_Case_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,        //Natural: ADD #ST-CASE.#TRAN-CNT ( #1099,#IDX ) TO #ST-TOT.#TRAN-CNT ( #IDX,1,5 )
                                pnd_Ws_Pnd_Idx));
                            pnd_St_Tot_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,5).nadd(pnd_St_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,      //Natural: ADD #ST-CASE.#GROSS-AMT ( #1099,#IDX,* ) TO #ST-TOT.#GROSS-AMT ( #IDX,1,5 )
                                pnd_Ws_Pnd_Idx,"*"));
                            pnd_St_Tot_Pnd_Pymnt_Ivc.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,5).nadd(pnd_St_Case_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099.getInt() + 1,        //Natural: ADD #ST-CASE.#IVC-AMT ( #1099,#IDX,* ) TO #ST-TOT.#PYMNT-IVC ( #IDX,1,5 )
                                pnd_Ws_Pnd_Idx,"*"));
                            pnd_St_Tot_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,5).nadd(pnd_St_Case_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_1099.getInt() + 1,          //Natural: ADD #ST-CASE.#IVC-ADJ ( #1099,#IDX ) TO #ST-TOT.#IVC-ADJ ( #IDX,1,5 )
                                pnd_Ws_Pnd_Idx));
                            pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,5).nadd(pnd_St_Case_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099.getInt() + 1, //Natural: ADD #ST-CASE.#TAXABLE-AMT ( #1099,#IDX ) TO #ST-TO1.#PMNT-TAXABLE ( #IDX,1,5 )
                                pnd_Ws_Pnd_Idx));
                            pnd_St_To1_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,5).nadd(pnd_St_Case_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_1099.getInt() + 1,  //Natural: ADD #ST-CASE.#TAXABLE-ADJ ( #1099,#IDX ) TO #ST-TO1.#TAXABLE-ADJ ( #IDX,1,5 )
                                pnd_Ws_Pnd_Idx));
                            pnd_St_Tot_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,5).nadd(pnd_St_Case_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_1099.getInt() + 1,          //Natural: ADD #ST-CASE.#STA-TAX ( #1099,#IDX ) TO #ST-TOT.#STA-TAX ( #IDX,1,5 )
                                pnd_Ws_Pnd_Idx));
                            pnd_St_Tot_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,5).nadd(pnd_St_Case_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_1099.getInt() + 1,          //Natural: ADD #ST-CASE.#LOC-TAX ( #1099,#IDX ) TO #ST-TOT.#LOC-TAX ( #IDX,1,5 )
                                pnd_Ws_Pnd_Idx));
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  $$$$
                    //*        IF #WS.#IDX NE 1
                    //*          ESCAPE TOP
                    //*        END-IF
                    //*        #ST-TOT.#FORM-TAXABLE   (0:59,1) :=
                    //*         #ST-TOT.#PMNT-TAXABLE  (0:59,1)  + #ST-TOT.#TAXABLE-ADJ(0:59,1)
                    //*        DISPLAY
                    //*          '/Tin'            OLD(#XTAXYR-F94.TWRPYMNT-TAX-ID-NBR)
                    //*          '/Contract'       OLD(#XTAXYR-F94.TWRPYMNT-CONTRACT-NBR)
                    //*          '/PY'             OLD(#XTAXYR-F94.TWRPYMNT-PAYEE-CDE)
                    //*          'Dist/Code'       OLD(#XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE)
                    //*          '/1099'           #1099
                    //*          '/IDX'            #IDX
                    //*          'Ro/ll'           #CASE-FIELDS.#ROLL(EM=N/Y)
                    //*          'Re/ch'           #CASE-FIELDS.#RECHAR(EM=N/Y)
                    //*          'IRA/Dist'        #TRAD-CLAS-DIST     (EM=N/Y)
                    //*          'T/N/D'           #ST-CASE.#TND        (1,#IDX)(EM=N/Y)
                    //*          '/Gross'          #ST-TOT.#GROSS-AMT   (#IDX,1)
                    //*          'PMNT IVC'        #ST-TOT.#PYMNT-IVC   (#IDX,1)  /
                    //*          'Adj IVC'         #ST-TOT.#IVC-ADJ     (#IDX,1)  /
                    //*          'FRM IVC'         #ST-TOT.#FORM-IVC    (#IDX,1)
                    //*          'PMNT Taxable'    #ST-TOT.#PMNT-TAXABLE(#IDX,1)  /
                    //*          'Adj Taxable'     #ST-TOT.#TAXABLE-ADJ (#IDX,1)  /
                    //*          'FRM Taxable'     #ST-TOT.#FORM-TAXABLE(#IDX,1)
                    //*        WRITE WORK FILE 5
                    //*          OLD(#XTAXYR-F94.TWRPYMNT-TAX-ID-NBR)
                    //*          OLD(#XTAXYR-F94.TWRPYMNT-CONTRACT-NBR)
                    //*          OLD(#XTAXYR-F94.TWRPYMNT-PAYEE-CDE)
                    //*          OLD(#XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE)
                    //*          #ST-TOT.#FORM-TAXABLE(#IDX,1)
                    //*        RESET
                    //*          #ST-TOT.#TRAN-CNT                 (*,1)
                    //*          #ST-TOT.#GROSS-AMT                (*,1)
                    //*          #ST-TOT.#PYMNT-IVC                (*,1)
                    //*          #ST-TOT.#IVC-ADJ                  (*,1)
                    //*          #ST-TOT.#FORM-IVC                 (*,1)
                    //*          #ST-TOT.#STA-TAX                  (*,1)
                    //*          #ST-TOT.#LOC-TAX                  (*,1)
                    //*          #ST-TOT.#PMNT-TAXABLE             (*,1)
                    //*          #ST-TOT.#TAXABLE-ADJ              (*,1)
                    //*          #ST-TOT.#FORM-TAXABLE             (*,1)
                    //*  $$$$
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape())) return;
            pnd_Case_Fields.resetInitial();                                                                                                                               //Natural: RESET INITIAL #CASE-FIELDS #ST-CASE ( *,* )
            pnd_St_Case.getValue("*","*").resetInitial();
            //*  ROLLOVERS & TAX-FREE EXCHANGE
            //*  RECHARACTERIZATIONS
            short decideConditionsMet597 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE;//Natural: VALUE 'G', 'Z', '6'
            if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("G") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("Z") 
                || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("6"))))
            {
                decideConditionsMet597++;
                pnd_Case_Fields_Pnd_Roll.setValue(true);                                                                                                                  //Natural: ASSIGN #CASE-FIELDS.#ROLL := TRUE
            }                                                                                                                                                             //Natural: VALUE 'N', 'R'
            else if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("N") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("R"))))
            {
                decideConditionsMet597++;
                pnd_Case_Fields_Pnd_Rechar.setValue(true);                                                                                                                //Natural: ASSIGN #CASE-FIELDS.#RECHAR := TRUE
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Key_Detail_Pnd_KeyIsBreak || pnd_Key_Detail_Pnd_Key28IsBreak || pnd_Key_Detail_Pnd_Key25IsBreak || ldaTwrl0900_getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_FormIsBreak))
        {
            pnd_Ws_Pnd_Contract_Type.reset();                                                                                                                             //Natural: RESET #WS.#CONTRACT-TYPE #WS.#CONTRACT-TYPE-MIXED
            pnd_Ws_Pnd_Contract_Type_Mixed.reset();
            //*  BK
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaTwrl0900_getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_FormIsBreak))
        {
            pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(readWork01Twrpymnt_Company_Cde_FormOld);                                                                 //Natural: ASSIGN #TWRACOMP.#COMP-CODE := OLD ( #XTAXYR-F94.TWRPYMNT-COMPANY-CDE-FORM )
            pdaTwracomp.getPnd_Twracomp_Pnd_Tax_Year().setValue(readWork01Twrpymnt_Tax_YearOld);                                                                          //Natural: ASSIGN #TWRACOMP.#TAX-YEAR := OLD ( #XTAXYR-F94.TWRPYMNT-TAX-YEAR )
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMPANY
            sub_Process_Company();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Pnd_Company_Line.setValue("Company:");                                                                                                                 //Natural: ASSIGN #WS.#COMPANY-LINE := 'Company:'
            setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),pnd_Ws_Pnd_Company_Line,11,4);                                                          //Natural: MOVE #TWRACOMP.#COMP-SHORT-NAME TO SUBSTR ( #WS.#COMPANY-LINE,11,4 )
            setValueToSubstring("-",pnd_Ws_Pnd_Company_Line,16,1);                                                                                                        //Natural: MOVE '-' TO SUBSTR ( #WS.#COMPANY-LINE,16,1 )
            setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Name(),pnd_Ws_Pnd_Company_Line,18,55);                                                               //Natural: MOVE #TWRACOMP.#COMP-NAME TO SUBSTR ( #WS.#COMPANY-LINE,18,55 )
            pnd_Ws_Pnd_S2.setValue(1);                                                                                                                                    //Natural: ASSIGN #WS.#S2 := 1
                                                                                                                                                                          //Natural: PERFORM WRITE-FED-REPORT
            sub_Write_Fed_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Cntl_Pnd_Tran_Cnt.getValue("*",2).nadd(pnd_Cntl_Pnd_Tran_Cnt.getValue("*",1));                                                                            //Natural: ADD #CNTL.#TRAN-CNT ( *,1 ) TO #CNTL.#TRAN-CNT ( *,2 )
            pnd_Cntl_Pnd_Gross_Amt.getValue("*",2).nadd(pnd_Cntl_Pnd_Gross_Amt.getValue("*",1));                                                                          //Natural: ADD #CNTL.#GROSS-AMT ( *,1 ) TO #CNTL.#GROSS-AMT ( *,2 )
            pnd_Cntl_Pnd_Ivc_Amt.getValue("*",2).nadd(pnd_Cntl_Pnd_Ivc_Amt.getValue("*",1));                                                                              //Natural: ADD #CNTL.#IVC-AMT ( *,1 ) TO #CNTL.#IVC-AMT ( *,2 )
            pnd_Cntl_Pnd_Taxable_Amt.getValue("*",2).nadd(pnd_Cntl_Pnd_Taxable_Amt.getValue("*",1));                                                                      //Natural: ADD #CNTL.#TAXABLE-AMT ( *,1 ) TO #CNTL.#TAXABLE-AMT ( *,2 )
            pnd_Cntl_Pnd_Fed_Tax.getValue("*",2).nadd(pnd_Cntl_Pnd_Fed_Tax.getValue("*",1));                                                                              //Natural: ADD #CNTL.#FED-TAX ( *,1 ) TO #CNTL.#FED-TAX ( *,2 )
            pnd_Cntl_Pnd_Sta_Tax.getValue("*",2).nadd(pnd_Cntl_Pnd_Sta_Tax.getValue("*",1));                                                                              //Natural: ADD #CNTL.#STA-TAX ( *,1 ) TO #CNTL.#STA-TAX ( *,2 )
            pnd_Cntl_Pnd_Loc_Tax.getValue("*",2).nadd(pnd_Cntl_Pnd_Loc_Tax.getValue("*",1));                                                                              //Natural: ADD #CNTL.#LOC-TAX ( *,1 ) TO #CNTL.#LOC-TAX ( *,2 )
            pnd_Cntl_Pnd_Int_Amt.getValue("*",2).nadd(pnd_Cntl_Pnd_Int_Amt.getValue("*",1));                                                                              //Natural: ADD #CNTL.#INT-AMT ( *,1 ) TO #CNTL.#INT-AMT ( *,2 )
            pnd_Cntl_Pnd_Int_Tax.getValue("*",2).nadd(pnd_Cntl_Pnd_Int_Tax.getValue("*",1));                                                                              //Natural: ADD #CNTL.#INT-TAX ( *,1 ) TO #CNTL.#INT-TAX ( *,2 )
            pnd_St_Tot_Pnd_Tran_Cnt.getValue("*",2,"*").nadd(pnd_St_Tot_Pnd_Tran_Cnt.getValue("*",1,"*"));                                                                //Natural: ADD #ST-TOT.#TRAN-CNT ( *,1,* ) TO #ST-TOT.#TRAN-CNT ( *,2,* )
            pnd_St_Tot_Pnd_Gross_Amt.getValue("*",2,"*").nadd(pnd_St_Tot_Pnd_Gross_Amt.getValue("*",1,"*"));                                                              //Natural: ADD #ST-TOT.#GROSS-AMT ( *,1,* ) TO #ST-TOT.#GROSS-AMT ( *,2,* )
            pnd_St_Tot_Pnd_Pymnt_Ivc.getValue("*",2,"*").nadd(pnd_St_Tot_Pnd_Pymnt_Ivc.getValue("*",1,"*"));                                                              //Natural: ADD #ST-TOT.#PYMNT-IVC ( *,1,* ) TO #ST-TOT.#PYMNT-IVC ( *,2,* )
            pnd_St_Tot_Pnd_Ivc_Adj.getValue("*",2,"*").nadd(pnd_St_Tot_Pnd_Ivc_Adj.getValue("*",1,"*"));                                                                  //Natural: ADD #ST-TOT.#IVC-ADJ ( *,1,* ) TO #ST-TOT.#IVC-ADJ ( *,2,* )
            pnd_St_Tot_Pnd_Form_Ivc.getValue("*",2,"*").nadd(pnd_St_Tot_Pnd_Form_Ivc.getValue("*",1,"*"));                                                                //Natural: ADD #ST-TOT.#FORM-IVC ( *,1,* ) TO #ST-TOT.#FORM-IVC ( *,2,* )
            pnd_St_To1_Pnd_Pmnt_Taxable.getValue("*",2,"*").nadd(pnd_St_To1_Pnd_Pmnt_Taxable.getValue("*",1,"*"));                                                        //Natural: ADD #ST-TO1.#PMNT-TAXABLE ( *,1,* ) TO #ST-TO1.#PMNT-TAXABLE ( *,2,* )
            pnd_St_To1_Pnd_Taxable_Adj.getValue("*",2,"*").nadd(pnd_St_To1_Pnd_Taxable_Adj.getValue("*",1,"*"));                                                          //Natural: ADD #ST-TO1.#TAXABLE-ADJ ( *,1,* ) TO #ST-TO1.#TAXABLE-ADJ ( *,2,* )
            pnd_St_To1_Pnd_Form_Taxable.getValue("*",2,"*").nadd(pnd_St_To1_Pnd_Form_Taxable.getValue("*",1,"*"));                                                        //Natural: ADD #ST-TO1.#FORM-TAXABLE ( *,1,* ) TO #ST-TO1.#FORM-TAXABLE ( *,2,* )
            pnd_St_Tot_Pnd_Sta_Tax.getValue("*",2,"*").nadd(pnd_St_Tot_Pnd_Sta_Tax.getValue("*",1,"*"));                                                                  //Natural: ADD #ST-TOT.#STA-TAX ( *,1,* ) TO #ST-TOT.#STA-TAX ( *,2,* )
            pnd_St_Tot_Pnd_Loc_Tax.getValue("*",2,"*").nadd(pnd_St_Tot_Pnd_Loc_Tax.getValue("*",1,"*"));                                                                  //Natural: ADD #ST-TOT.#LOC-TAX ( *,1,* ) TO #ST-TOT.#LOC-TAX ( *,2,* )
            pnd_Cntl_Pnd_Totals.getValue("*",1).reset();                                                                                                                  //Natural: RESET #CNTL.#TOTALS ( *,1 ) #ST-TOT ( *,1,* ) #ST-TO1 ( *,1,* )
            pnd_St_Tot.getValue("*",1,"*").reset();
            pnd_St_To1.getValue("*",1,"*").reset();
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=133 ZP=ON");
        Global.format(2, "PS=58 LS=133 ZP=ON");
        Global.format(3, "PS=58 LS=133 ZP=ON");
        Global.format(4, "PS=58 LS=133 ZP=ON");
        Global.format(5, "PS=58 LS=133 ZP=ON");
        Global.format(6, "PS=58 LS=133 ZP=ON");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(47),"Summary of Federal Payment Transactions",new TabSetting(120),"Report: RPT1",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(57),"State Summary Totals",new TabSetting(120),"Report: RPT2",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, new ReportEditMask 
            ("9999"), new SignPosition (false),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(48),"State Summary Totals - Non-Reportable",new TabSetting(120),"Report: RPT3",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(4), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(47),"State Summary Totals - Gross reportable",new TabSetting(120),"Report: RPT4",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(5, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(5), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"State Summary Totals - Taxable reportable",new TabSetting(120),"Report: RPT5",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(6, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(6), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"State Summary Totals - HardCopy reportable",new TabSetting(120),"Report: RPT6",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",
            
        		pnd_Cntl_Pnd_Cntl_Text,NEWLINE,"/",
        		pnd_Cntl_Pnd_Cntl_Text1,"/Trans Count",
        		pnd_Cntl_Pnd_Tran_Cnt, pnd_Ws_Pnd_Tran_Cv,"Gross Amount",
        		pnd_Cntl_Pnd_Gross_Amt, pnd_Ws_Pnd_Gross_Cv,NEWLINE,new ColumnSpacing(3),"Interest",
        		pnd_Cntl_Pnd_Int_Amt, pnd_Ws_Pnd_Int_Cv,"/IVC Amount",
        		pnd_Cntl_Pnd_Ivc_Amt, pnd_Ws_Pnd_Ivc_Cv,"/Taxable Amount",
        		pnd_Cntl_Pnd_Taxable_Amt, pnd_Ws_Pnd_Taxable_Cv,"Federal Tax",
        		pnd_Cntl_Pnd_Fed_Tax, pnd_Ws_Pnd_Fed_Cv,NEWLINE,"Backup Tax",
        		pnd_Cntl_Pnd_Int_Tax, pnd_Ws_Pnd_Int_Back_Cv,"State   Tax",
        		pnd_Cntl_Pnd_Sta_Tax, pnd_Ws_Pnd_Sta_Cv,NEWLINE,"Local   Tax",
        		pnd_Cntl_Pnd_Loc_Tax, pnd_Ws_Pnd_Loc_Cv);
        getReports().setDisplayColumns(2, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",
            
        		pnd_St_Cntl_Pnd_State_Desc,"//Trans Count",
        		pnd_St_Tot_Pnd_Tran_Cnt,"//Gross Amount",
        		pnd_St_Tot_Pnd_Gross_Amt,"Payment IVC",
        		pnd_St_Tot_Pnd_Pymnt_Ivc,NEWLINE,"Adjustment IVC",
        		pnd_St_Tot_Pnd_Ivc_Adj,NEWLINE,"Form IVC",
        		pnd_St_Tot_Pnd_Form_Ivc,"Payment Taxable",
        		pnd_St_To1_Pnd_Pmnt_Taxable,NEWLINE,"Adjustment Taxable",
        		pnd_St_To1_Pnd_Taxable_Adj,NEWLINE,"Form Taxable",
        		pnd_St_To1_Pnd_Form_Taxable,"//State Tax",
        		pnd_St_Tot_Pnd_Sta_Tax,"//Local Tax",
        		pnd_St_Tot_Pnd_Loc_Tax);
        getReports().setDisplayColumns(3, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",
            
        		pnd_St_Cntl_Pnd_State_Desc,"//Trans Count",
        		pnd_St_Tot_Pnd_Tran_Cnt,"//Gross Amount",
        		pnd_St_Tot_Pnd_Gross_Amt,"Payment IVC",
        		pnd_St_Tot_Pnd_Pymnt_Ivc,NEWLINE,"Adjustment IVC",
        		pnd_St_Tot_Pnd_Ivc_Adj,NEWLINE,"Form IVC",
        		pnd_St_Tot_Pnd_Form_Ivc,"Payment Taxable",
        		pnd_St_To1_Pnd_Pmnt_Taxable,NEWLINE,"Adjustment Taxable",
        		pnd_St_To1_Pnd_Taxable_Adj,NEWLINE,"Form Taxable",
        		pnd_St_To1_Pnd_Form_Taxable,"//State Tax",
        		pnd_St_Tot_Pnd_Sta_Tax,"//Local Tax",
        		pnd_St_Tot_Pnd_Loc_Tax);
        getReports().setDisplayColumns(4, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",
            
        		pnd_St_Cntl_Pnd_State_Desc,"//Trans Count",
        		pnd_St_Tot_Pnd_Tran_Cnt,"//Gross Amount",
        		pnd_St_Tot_Pnd_Gross_Amt,"Payment IVC",
        		pnd_St_Tot_Pnd_Pymnt_Ivc,NEWLINE,"Adjustment IVC",
        		pnd_St_Tot_Pnd_Ivc_Adj,NEWLINE,"Form IVC",
        		pnd_St_Tot_Pnd_Form_Ivc,"Payment Taxable",
        		pnd_St_To1_Pnd_Pmnt_Taxable,NEWLINE,"Adjustment Taxable",
        		pnd_St_To1_Pnd_Taxable_Adj,NEWLINE,"Form Taxable",
        		pnd_St_To1_Pnd_Form_Taxable,"//State Tax",
        		pnd_St_Tot_Pnd_Sta_Tax,"//Local Tax",
        		pnd_St_Tot_Pnd_Loc_Tax);
        getReports().setDisplayColumns(5, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",
            
        		pnd_St_Cntl_Pnd_State_Desc,"//Trans Count",
        		pnd_St_Tot_Pnd_Tran_Cnt,"//Gross Amount",
        		pnd_St_Tot_Pnd_Gross_Amt,"Payment IVC",
        		pnd_St_Tot_Pnd_Pymnt_Ivc,NEWLINE,"Adjustment IVC",
        		pnd_St_Tot_Pnd_Ivc_Adj,NEWLINE,"Form IVC",
        		pnd_St_Tot_Pnd_Form_Ivc,"Payment Taxable",
        		pnd_St_To1_Pnd_Pmnt_Taxable,NEWLINE,"Adjustment Taxable",
        		pnd_St_To1_Pnd_Taxable_Adj,NEWLINE,"Form Taxable",
        		pnd_St_To1_Pnd_Form_Taxable,"//State Tax",
        		pnd_St_Tot_Pnd_Sta_Tax,"//Local Tax",
        		pnd_St_Tot_Pnd_Loc_Tax);
        getReports().setDisplayColumns(6, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",
            
        		pnd_St_Cntl_Pnd_State_Desc,"//Trans Count",
        		pnd_St_Tot_Pnd_Tran_Cnt,"//Gross Amount",
        		pnd_St_Tot_Pnd_Gross_Amt,"Payment IVC",
        		pnd_St_Tot_Pnd_Pymnt_Ivc,NEWLINE,"Adjustment IVC",
        		pnd_St_Tot_Pnd_Ivc_Adj,NEWLINE,"Form IVC",
        		pnd_St_Tot_Pnd_Form_Ivc,"Payment Taxable",
        		pnd_St_To1_Pnd_Pmnt_Taxable,NEWLINE,"Adjustment Taxable",
        		pnd_St_To1_Pnd_Taxable_Adj,NEWLINE,"Form Taxable",
        		pnd_St_To1_Pnd_Form_Taxable,"//State Tax",
        		pnd_St_Tot_Pnd_Sta_Tax,"//Local Tax",
        		pnd_St_Tot_Pnd_Loc_Tax);
    }
    private void CheckAtStartofData413() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Ws_Pnd_Tax_Year.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());                                                                              //Natural: ASSIGN #WS.#TAX-YEAR := #XTAXYR-F94.TWRPYMNT-TAX-YEAR
                                                                                                                                                                          //Natural: PERFORM LOAD-STATE-TABLE
            sub_Load_State_Table();
            if (condition(Global.isEscape())) {return;}
            //*  ROLLOVERS & TAX-FREE EXCHANGE
            //*  RECHARACTERIZATIONS
            short decideConditionsMet418 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE;//Natural: VALUE 'G', 'Z', '6'
            if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("G") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("Z") 
                || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("6"))))
            {
                decideConditionsMet418++;
                pnd_Case_Fields_Pnd_Roll.setValue(true);                                                                                                                  //Natural: ASSIGN #CASE-FIELDS.#ROLL := TRUE
            }                                                                                                                                                             //Natural: VALUE 'N', 'R'
            else if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("N") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("R"))))
            {
                decideConditionsMet418++;
                pnd_Case_Fields_Pnd_Rechar.setValue(true);                                                                                                                //Natural: ASSIGN #CASE-FIELDS.#RECHAR := TRUE
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM DETERMINE-RES-IDX
            sub_Determine_Res_Idx();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
