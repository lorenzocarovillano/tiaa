/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:34:57 PM
**        * FROM NATURAL PROGRAM : Twrp3207
************************************************************
**        * FILE NAME            : Twrp3207.java
**        * CLASS NAME           : Twrp3207
**        * INSTANCE NAME        : Twrp3207
************************************************************
***********************************************************************
* PROGRAM  : TWRP3207
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : PRODUCES MAG. MEDIA FILES FOR MAG. MEDIA STATE REPORTING.
* CREATED  : 06 / 25 / 2020.
*   BY     : ARIVU
* FUNCTION : PROGRAM PRODUCES IL QUARTERLY FILE
*
***********************************************************************
* MODIFICATION
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3207 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField re_In;

    private DbsGroup re_In__R_Field_1;
    private DbsField re_In_E_Filler_1;
    private DbsField re_In_E_Ret_Qtr;
    private DbsField re_In_E_Tax_Year;
    private DbsField re_In_E_State_Ein;
    private DbsField re_In_E_Buss_Name;
    private DbsField re_In_E_Add_Line_1;
    private DbsField re_In_E_Add_Line_2;
    private DbsField re_In_E_City;
    private DbsField re_In_E_Country;
    private DbsField re_In_E_Filler_2;
    private DbsField re_In_E_Zip_Code;
    private DbsField re_In_E_Filler_3;
    private DbsField re_In_E_Nbr_Employees;
    private DbsField re_In_E_Wh_Tot_Wages;
    private DbsField re_In_E_Wh_Tot_Income_Tax;
    private DbsField re_In_E_Wh_Taxable_Wages;
    private DbsField re_In_E_Month1_Employees;
    private DbsField re_In_E_Month2_Employees;
    private DbsField re_In_E_Month3_Employees;
    private DbsField re_In_E_Filler_4;
    private DbsField re_In_E_Transmitter;
    private DbsField re_In_E_Filler_5;

    private DbsGroup re_In__R_Field_2;
    private DbsField re_In_Rs_Soc_Sec_Nbr;
    private DbsField re_In_Rs_First_Name;
    private DbsField re_In_Rs_Middle_Name;
    private DbsField re_In_Rs_Last_Name;
    private DbsField re_In_Rs_Filler_1;
    private DbsField re_In_Rs_Wage_Plan_Code;
    private DbsField re_In_Rs_Filler_2;
    private DbsField re_In_Rs_Rpt_Prd;
    private DbsField re_In_Rs_Tot_Wages;
    private DbsField re_In_Rs_Filler_3;
    private DbsField re_In_Rs_State_Ein;
    private DbsField re_In_Rs_Filler_A;
    private DbsField re_In_Rs_Qtr_Gross_Amt;
    private DbsField re_In_Rs_Filler_4;
    private DbsField re_In_Rs_Tax_Withhold;
    private DbsField re_In_Rs_Filler_5;
    private DbsField re_In_Rs_Loc_Withhold;
    private DbsField re_In_Rs_Filler_6;
    private DbsField re_In_Rs_Transmitter;
    private DbsField re_In_Rs_Filler_7;

    private DbsGroup pnd_Work_Area;
    private DbsField pnd_Work_Area_Pnd_Tot_Outrec;
    private DbsField pnd_Work_Area_Pnd_Rec_Seq_Number;
    private DbsField pnd_Work_Area_Pnd_Ws_Name;
    private DbsField pnd_Out_Ny_Record;

    private DbsGroup pnd_Out_Ny_Record__R_Field_3;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Il_Name;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Il_Blank1;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Il_Tin;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Il_Blank2;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Il_Gross;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Il_Blank3;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Il_Tax_Wthld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        re_In = localVariables.newFieldInRecord("re_In", "RE-IN", FieldType.STRING, 512);

        re_In__R_Field_1 = localVariables.newGroupInRecord("re_In__R_Field_1", "REDEFINE", re_In);
        re_In_E_Filler_1 = re_In__R_Field_1.newFieldInGroup("re_In_E_Filler_1", "E-FILLER-1", FieldType.STRING, 12);
        re_In_E_Ret_Qtr = re_In__R_Field_1.newFieldInGroup("re_In_E_Ret_Qtr", "E-RET-QTR", FieldType.STRING, 1);
        re_In_E_Tax_Year = re_In__R_Field_1.newFieldInGroup("re_In_E_Tax_Year", "E-TAX-YEAR", FieldType.STRING, 4);
        re_In_E_State_Ein = re_In__R_Field_1.newFieldInGroup("re_In_E_State_Ein", "E-STATE-EIN", FieldType.STRING, 8);
        re_In_E_Buss_Name = re_In__R_Field_1.newFieldInGroup("re_In_E_Buss_Name", "E-BUSS-NAME", FieldType.STRING, 60);
        re_In_E_Add_Line_1 = re_In__R_Field_1.newFieldInGroup("re_In_E_Add_Line_1", "E-ADD-LINE-1", FieldType.STRING, 30);
        re_In_E_Add_Line_2 = re_In__R_Field_1.newFieldInGroup("re_In_E_Add_Line_2", "E-ADD-LINE-2", FieldType.STRING, 30);
        re_In_E_City = re_In__R_Field_1.newFieldInGroup("re_In_E_City", "E-CITY", FieldType.STRING, 28);
        re_In_E_Country = re_In__R_Field_1.newFieldInGroup("re_In_E_Country", "E-COUNTRY", FieldType.STRING, 3);
        re_In_E_Filler_2 = re_In__R_Field_1.newFieldInGroup("re_In_E_Filler_2", "E-FILLER-2", FieldType.STRING, 2);
        re_In_E_Zip_Code = re_In__R_Field_1.newFieldInGroup("re_In_E_Zip_Code", "E-ZIP-CODE", FieldType.STRING, 10);
        re_In_E_Filler_3 = re_In__R_Field_1.newFieldInGroup("re_In_E_Filler_3", "E-FILLER-3", FieldType.STRING, 2);
        re_In_E_Nbr_Employees = re_In__R_Field_1.newFieldInGroup("re_In_E_Nbr_Employees", "E-NBR-EMPLOYEES", FieldType.NUMERIC, 8);
        re_In_E_Wh_Tot_Wages = re_In__R_Field_1.newFieldInGroup("re_In_E_Wh_Tot_Wages", "E-WH-TOT-WAGES", FieldType.NUMERIC, 13, 2);
        re_In_E_Wh_Tot_Income_Tax = re_In__R_Field_1.newFieldInGroup("re_In_E_Wh_Tot_Income_Tax", "E-WH-TOT-INCOME-TAX", FieldType.NUMERIC, 13, 2);
        re_In_E_Wh_Taxable_Wages = re_In__R_Field_1.newFieldInGroup("re_In_E_Wh_Taxable_Wages", "E-WH-TAXABLE-WAGES", FieldType.NUMERIC, 13, 2);
        re_In_E_Month1_Employees = re_In__R_Field_1.newFieldInGroup("re_In_E_Month1_Employees", "E-MONTH1-EMPLOYEES", FieldType.NUMERIC, 7);
        re_In_E_Month2_Employees = re_In__R_Field_1.newFieldInGroup("re_In_E_Month2_Employees", "E-MONTH2-EMPLOYEES", FieldType.NUMERIC, 7);
        re_In_E_Month3_Employees = re_In__R_Field_1.newFieldInGroup("re_In_E_Month3_Employees", "E-MONTH3-EMPLOYEES", FieldType.NUMERIC, 7);
        re_In_E_Filler_4 = re_In__R_Field_1.newFieldInGroup("re_In_E_Filler_4", "E-FILLER-4", FieldType.STRING, 93);
        re_In_E_Transmitter = re_In__R_Field_1.newFieldInGroup("re_In_E_Transmitter", "E-TRANSMITTER", FieldType.STRING, 1);
        re_In_E_Filler_5 = re_In__R_Field_1.newFieldInGroup("re_In_E_Filler_5", "E-FILLER-5", FieldType.STRING, 160);

        re_In__R_Field_2 = localVariables.newGroupInRecord("re_In__R_Field_2", "REDEFINE", re_In);
        re_In_Rs_Soc_Sec_Nbr = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Soc_Sec_Nbr", "RS-SOC-SEC-NBR", FieldType.STRING, 9);
        re_In_Rs_First_Name = re_In__R_Field_2.newFieldInGroup("re_In_Rs_First_Name", "RS-FIRST-NAME", FieldType.STRING, 15);
        re_In_Rs_Middle_Name = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Middle_Name", "RS-MIDDLE-NAME", FieldType.STRING, 15);
        re_In_Rs_Last_Name = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Last_Name", "RS-LAST-NAME", FieldType.STRING, 23);
        re_In_Rs_Filler_1 = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Filler_1", "RS-FILLER-1", FieldType.STRING, 123);
        re_In_Rs_Wage_Plan_Code = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Wage_Plan_Code", "RS-WAGE-PLAN-CODE", FieldType.STRING, 1);
        re_In_Rs_Filler_2 = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Filler_2", "RS-FILLER-2", FieldType.STRING, 1);
        re_In_Rs_Rpt_Prd = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Rpt_Prd", "RS-RPT-PRD", FieldType.STRING, 6);
        re_In_Rs_Tot_Wages = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Tot_Wages", "RS-TOT-WAGES", FieldType.NUMERIC, 11, 2);
        re_In_Rs_Filler_3 = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Filler_3", "RS-FILLER-3", FieldType.STRING, 34);
        re_In_Rs_State_Ein = re_In__R_Field_2.newFieldInGroup("re_In_Rs_State_Ein", "RS-STATE-EIN", FieldType.STRING, 8);
        re_In_Rs_Filler_A = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Filler_A", "RS-FILLER-A", FieldType.STRING, 20);
        re_In_Rs_Qtr_Gross_Amt = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Qtr_Gross_Amt", "RS-QTR-GROSS-AMT", FieldType.NUMERIC, 11, 2);
        re_In_Rs_Filler_4 = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Filler_4", "RS-FILLER-4", FieldType.STRING, 1);
        re_In_Rs_Tax_Withhold = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Tax_Withhold", "RS-TAX-WITHHOLD", FieldType.NUMERIC, 10, 2);
        re_In_Rs_Filler_5 = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Filler_5", "RS-FILLER-5", FieldType.STRING, 1);
        re_In_Rs_Loc_Withhold = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Loc_Withhold", "RS-LOC-WITHHOLD", FieldType.NUMERIC, 10, 2);
        re_In_Rs_Filler_6 = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Filler_6", "RS-FILLER-6", FieldType.STRING, 52);
        re_In_Rs_Transmitter = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Transmitter", "RS-TRANSMITTER", FieldType.STRING, 1);
        re_In_Rs_Filler_7 = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Filler_7", "RS-FILLER-7", FieldType.STRING, 160);

        pnd_Work_Area = localVariables.newGroupInRecord("pnd_Work_Area", "#WORK-AREA");
        pnd_Work_Area_Pnd_Tot_Outrec = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Outrec", "#TOT-OUTREC", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Rec_Seq_Number = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Rec_Seq_Number", "#REC-SEQ-NUMBER", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Ws_Name = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Name", "#WS-NAME", FieldType.STRING, 30);
        pnd_Out_Ny_Record = localVariables.newFieldInRecord("pnd_Out_Ny_Record", "#OUT-NY-RECORD", FieldType.STRING, 68);

        pnd_Out_Ny_Record__R_Field_3 = localVariables.newGroupInRecord("pnd_Out_Ny_Record__R_Field_3", "REDEFINE", pnd_Out_Ny_Record);
        pnd_Out_Ny_Record_Pnd_Outw_Il_Name = pnd_Out_Ny_Record__R_Field_3.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Il_Name", "#OUTW-IL-NAME", FieldType.STRING, 
            30);
        pnd_Out_Ny_Record_Pnd_Outw_Il_Blank1 = pnd_Out_Ny_Record__R_Field_3.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Il_Blank1", "#OUTW-IL-BLANK1", 
            FieldType.STRING, 1);
        pnd_Out_Ny_Record_Pnd_Outw_Il_Tin = pnd_Out_Ny_Record__R_Field_3.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Il_Tin", "#OUTW-IL-TIN", FieldType.STRING, 
            9);
        pnd_Out_Ny_Record_Pnd_Outw_Il_Blank2 = pnd_Out_Ny_Record__R_Field_3.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Il_Blank2", "#OUTW-IL-BLANK2", 
            FieldType.STRING, 1);
        pnd_Out_Ny_Record_Pnd_Outw_Il_Gross = pnd_Out_Ny_Record__R_Field_3.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Il_Gross", "#OUTW-IL-GROSS", FieldType.NUMERIC, 
            13, 2);
        pnd_Out_Ny_Record_Pnd_Outw_Il_Blank3 = pnd_Out_Ny_Record__R_Field_3.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Il_Blank3", "#OUTW-IL-BLANK3", 
            FieldType.STRING, 1);
        pnd_Out_Ny_Record_Pnd_Outw_Il_Tax_Wthld = pnd_Out_Ny_Record__R_Field_3.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Il_Tax_Wthld", "#OUTW-IL-TAX-WTHLD", 
            FieldType.NUMERIC, 13, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3207() throws Exception
    {
        super("Twrp3207");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* *--------
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 RE-IN
        while (condition(getWorkFiles().read(1, re_In)))
        {
            pnd_Work_Area_Pnd_Rec_Seq_Number.nadd(1);                                                                                                                     //Natural: ADD 1 TO #REC-SEQ-NUMBER
            if (condition(re_In_E_Transmitter.equals("D")))                                                                                                               //Natural: IF E-TRANSMITTER = 'D'
            {
                                                                                                                                                                          //Natural: PERFORM REFORMAT-REC
                sub_Reformat_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-RECORD
                sub_Write_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RECORD
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REFORMAT-REC
    }
    private void sub_Write_Record() throws Exception                                                                                                                      //Natural: WRITE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        pnd_Work_Area_Pnd_Tot_Outrec.nadd(1);                                                                                                                             //Natural: ADD 1 TO #TOT-OUTREC
        getWorkFiles().write(2, false, pnd_Out_Ny_Record);                                                                                                                //Natural: WRITE WORK FILE 02 #OUT-NY-RECORD
    }
    private void sub_Reformat_Rec() throws Exception                                                                                                                      //Natural: REFORMAT-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        pnd_Work_Area_Pnd_Ws_Name.setValue(DbsUtil.compress(re_In_Rs_First_Name, re_In_Rs_Middle_Name, re_In_Rs_Last_Name));                                              //Natural: COMPRESS RS-FIRST-NAME RS-MIDDLE-NAME RS-LAST-NAME INTO #WS-NAME
        DbsUtil.examine(new ExamineSource(pnd_Work_Area_Pnd_Ws_Name), new ExamineSearch(","), new ExamineDelete());                                                       //Natural: EXAMINE #WS-NAME FOR ',' DELETE
        pnd_Out_Ny_Record_Pnd_Outw_Il_Tin.setValue(re_In_Rs_Soc_Sec_Nbr);                                                                                                 //Natural: ASSIGN #OUTW-IL-TIN := RS-SOC-SEC-NBR
        pnd_Out_Ny_Record_Pnd_Outw_Il_Name.setValue(pnd_Work_Area_Pnd_Ws_Name);                                                                                           //Natural: ASSIGN #OUTW-IL-NAME := #WS-NAME
        pnd_Out_Ny_Record_Pnd_Outw_Il_Blank1.setValue(" ");                                                                                                               //Natural: ASSIGN #OUTW-IL-BLANK1 := ' '
        pnd_Out_Ny_Record_Pnd_Outw_Il_Gross.setValue(re_In_Rs_Qtr_Gross_Amt);                                                                                             //Natural: ASSIGN #OUTW-IL-GROSS := RS-QTR-GROSS-AMT
        pnd_Out_Ny_Record_Pnd_Outw_Il_Blank2.setValue(" ");                                                                                                               //Natural: ASSIGN #OUTW-IL-BLANK2 := ' '
        pnd_Out_Ny_Record_Pnd_Outw_Il_Blank3.setValue(" ");                                                                                                               //Natural: ASSIGN #OUTW-IL-BLANK3 := ' '
        pnd_Out_Ny_Record_Pnd_Outw_Il_Tax_Wthld.setValue(re_In_Rs_Tax_Withhold);                                                                                          //Natural: ASSIGN #OUTW-IL-TAX-WTHLD := RS-TAX-WITHHOLD
    }

    //
}
