/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:33:15 PM
**        * FROM NATURAL PROGRAM : Twrp1730
************************************************************
**        * FILE NAME            : Twrp1730.java
**        * CLASS NAME           : Twrp1730
**        * INSTANCE NAME        : Twrp1730
************************************************************
************************************************************************
** PROGRAM NAME:  TWRP1730
** SYSTEM      :  TAX REPORTING SYSTEM - TAXWARS
** AUTHOR      :  M. BERLIN
** PURPOSE     :  CONVERT MCCAMISH TRANSACTION FILE INTO
**             :  TAXWARS ACCEPTED FORMAT.
** INPUT       :  1. TRANSACTION FILE EXTRACT FROM MCCAMISH
**----------------------------------------------------------------------
** OUTPUT      :  2. CONVERTED FILE IN TAXWARS ACCEPTED FORMAT
**             :     DETAIL RECORD LAYOUT  IS LOCAL TWRL0700
**             :     CONTROL RECORD LAYOUT IS LOCAL TWRL0710
**----------------------------------------------------------------------
** DATE        :  09/13/2011
************************************************************************
** MODIFICATION LOG
** 10/18/2011  :  R. CARREON  -  NON-ERISA TDA. NEW COMPANY CODE 'F'
** 01/18/2012  :  R. CARREON  -  RE-STOW SUNY/CUNY
** 04/21/17    :  J BREMER PIN EXPANSION SCAN 08/2017
** 04/11/18    'VL'ADDED WITH TWRT-SOURCE'AM' FOR DAILY PROCESS- VIKRAM2
**    THE VALUES OF T COMPANY CODE HAS BEEN ADDED TO CORRESPONDING FIELD
**       VALUES IN L COMPANY CODE VALUES FOR SUMMARY RECORD.   /*VIKRAM2
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp1730 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0700 ldaTwrl0700;
    private LdaTwrl0710 ldaTwrl0710;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Wk_Input;

    private DbsGroup pnd_Wk_Input__R_Field_1;

    private DbsGroup pnd_Wk_Input_Pnd_Wk_Input_Detail;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Company_Cde;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Source;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Cntrct_Nbr;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Payee_Cde;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Tax_Id_Type;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Tax_Id;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Pin_Id;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Paymt_Dte;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Pay_Set_Types;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Currency_Cde;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Locality_Cde;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Rollover_Ind;
    private DbsField pnd_Wk_Input_Pnd_Twrt_State_Rsdncy;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Citizenship;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Dob_A;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Gross_Amt;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Fed_Whhld_Amt;
    private DbsField pnd_Wk_Input_Pnd_Twrt_State_Whhld_Amt;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Interest_Amt;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Ivc_Amt;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Ivc_Cde;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Na_Line;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Tax_Elct_Cde;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Local_Whhld_Amt;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Tot_Ivc_9b_Amt;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Disability_Cde;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Lob_Cde;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Tax_Year;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Dod;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Can_Whhld_Ind;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Can_Whhld_Amt;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Nra_Whhld_Amt;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Payee_Tax_Res_Sts;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Pymnt_Tax_Form_1078;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Pymnt_Tax_Form_1001;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Pymnt_Tax_Exempt_Ind;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Pymnt_Acctg_Dte;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Pymnt_Intrfce_Dte;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Ivc_Rcvry_Cde;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Total_Distr_Amt;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Distr_Pct;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Addr_Chg_Ind;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Rej_Err_Nbr;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Type_Refer;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Distrib_Cde;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Flag;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Contract_Type;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Annt_Residency_Code;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Annt_Locality_Code;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Election_Code;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Fed_Nra_Tax_Rate;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Payment_Form1001;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Distributing_Irc_Cde;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Receiving_Irc_Cde;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Pymnt_Reqst_Nbr;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Hardship_Cde;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Termination_Of_Empl_Cde;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Distribution_Code;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Nra_Exemption_Code;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Tax_Type;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Plan_Type;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Reason_Code;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Op_Rev_Ind;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Op_Adj_Ind;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Op_Maint_Ind;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Distribution_Type;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Check_Reason;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Roth_Qual_Ind;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Roth_Death_Ind;
    private DbsField pnd_Wk_Input_Pnd_Twrt_Filler;

    private DbsGroup pnd_Wk_Input__R_Field_2;

    private DbsGroup pnd_Wk_Input_Pnd_Wk_Input_Control;
    private DbsField pnd_Wk_Input_Pnd_Z;
    private DbsField pnd_Wk_Input_Pnd_Source_Code;
    private DbsField pnd_Wk_Input_Pnd_T_Comp_Code;
    private DbsField pnd_Wk_Input_Pnd_T_Rec_Cnt;
    private DbsField pnd_Wk_Input_Pnd_T_Gross_A;
    private DbsField pnd_Wk_Input_Pnd_T_Ivc_A;
    private DbsField pnd_Wk_Input_Pnd_T_Int_A;
    private DbsField pnd_Wk_Input_Pnd_T_Fed_A;
    private DbsField pnd_Wk_Input_Pnd_T_Nra_A;
    private DbsField pnd_Wk_Input_Pnd_T_State_A;
    private DbsField pnd_Wk_Input_Pnd_T_Local_A;
    private DbsField pnd_Wk_Input_Pnd_T_Canadian_A;
    private DbsField pnd_Wk_Input_Pnd_C_Comp_Code;
    private DbsField pnd_Wk_Input_Pnd_C_Rec_Cnt;
    private DbsField pnd_Wk_Input_Pnd_C_Gross_A;
    private DbsField pnd_Wk_Input_Pnd_C_Ivc_A;
    private DbsField pnd_Wk_Input_Pnd_C_Int_A;
    private DbsField pnd_Wk_Input_Pnd_C_Fed_A;
    private DbsField pnd_Wk_Input_Pnd_C_Nra_A;
    private DbsField pnd_Wk_Input_Pnd_C_State_A;
    private DbsField pnd_Wk_Input_Pnd_C_Local_A;
    private DbsField pnd_Wk_Input_Pnd_C_Canadian_A;
    private DbsField pnd_Wk_Input_Pnd_L_Comp_Code;
    private DbsField pnd_Wk_Input_Pnd_L_Rec_Cnt;
    private DbsField pnd_Wk_Input_Pnd_L_Gross_A;
    private DbsField pnd_Wk_Input_Pnd_L_Ivc_A;
    private DbsField pnd_Wk_Input_Pnd_L_Int_A;
    private DbsField pnd_Wk_Input_Pnd_L_Fed_A;
    private DbsField pnd_Wk_Input_Pnd_L_Nra_A;
    private DbsField pnd_Wk_Input_Pnd_L_State_A;
    private DbsField pnd_Wk_Input_Pnd_L_Local_A;
    private DbsField pnd_Wk_Input_Pnd_L_Canadian_A;
    private DbsField pnd_Wk_Input_Pnd_From_Dte;
    private DbsField pnd_Wk_Input_Pnd_To_Dte;
    private DbsField pnd_Wk_Input_Pnd_Tx_Year;
    private DbsField pnd_Wk_Input_Pnd_Interface_Dte;
    private DbsField pnd_Wk_Input_Pnd_S_Comp_Code;
    private DbsField pnd_Wk_Input_Pnd_S_Rec_Cnt;
    private DbsField pnd_Wk_Input_Pnd_S_Gross_A;
    private DbsField pnd_Wk_Input_Pnd_S_Ivc_A;
    private DbsField pnd_Wk_Input_Pnd_S_Int_A;
    private DbsField pnd_Wk_Input_Pnd_S_Fed_A;
    private DbsField pnd_Wk_Input_Pnd_S_Nra_A;
    private DbsField pnd_Wk_Input_Pnd_S_State_A;
    private DbsField pnd_Wk_Input_Pnd_S_Local_A;
    private DbsField pnd_Wk_Input_Pnd_S_Canadian_A;
    private DbsField pnd_Wk_Input_Pnd_F_Comp_Code;
    private DbsField pnd_Wk_Input_Pnd_F_Rec_Cnt;
    private DbsField pnd_Wk_Input_Pnd_F_Gross_A;
    private DbsField pnd_Wk_Input_Pnd_F_Ivc_A;
    private DbsField pnd_Wk_Input_Pnd_F_Int_A;
    private DbsField pnd_Wk_Input_Pnd_F_Fed_A;
    private DbsField pnd_Wk_Input_Pnd_F_Nra_A;
    private DbsField pnd_Wk_Input_Pnd_F_State_A;
    private DbsField pnd_Wk_Input_Pnd_F_Local_A;
    private DbsField pnd_Wk_Input_Pnd_F_Canadian_A;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0700 = new LdaTwrl0700();
        registerRecord(ldaTwrl0700);
        ldaTwrl0710 = new LdaTwrl0710();
        registerRecord(ldaTwrl0710);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Wk_Input = localVariables.newFieldInRecord("pnd_Wk_Input", "#WK-INPUT", FieldType.STRING, 671);

        pnd_Wk_Input__R_Field_1 = localVariables.newGroupInRecord("pnd_Wk_Input__R_Field_1", "REDEFINE", pnd_Wk_Input);

        pnd_Wk_Input_Pnd_Wk_Input_Detail = pnd_Wk_Input__R_Field_1.newGroupInGroup("pnd_Wk_Input_Pnd_Wk_Input_Detail", "#WK-INPUT-DETAIL");
        pnd_Wk_Input_Pnd_Twrt_Company_Cde = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Company_Cde", "#TWRT-COMPANY-CDE", 
            FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Source = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Source", "#TWRT-SOURCE", FieldType.STRING, 
            2);
        pnd_Wk_Input_Pnd_Twrt_Cntrct_Nbr = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Cntrct_Nbr", "#TWRT-CNTRCT-NBR", FieldType.STRING, 
            8);
        pnd_Wk_Input_Pnd_Twrt_Payee_Cde = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Payee_Cde", "#TWRT-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Wk_Input_Pnd_Twrt_Tax_Id_Type = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Tax_Id_Type", "#TWRT-TAX-ID-TYPE", 
            FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Tax_Id = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Tax_Id", "#TWRT-TAX-ID", FieldType.STRING, 
            10);
        pnd_Wk_Input_Pnd_Twrt_Pin_Id = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Pin_Id", "#TWRT-PIN-ID", FieldType.STRING, 
            12);
        pnd_Wk_Input_Pnd_Twrt_Paymt_Dte = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Paymt_Dte", "#TWRT-PAYMT-DTE", FieldType.NUMERIC, 
            8);
        pnd_Wk_Input_Pnd_Twrt_Pay_Set_Types = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Pay_Set_Types", "#TWRT-PAY-SET-TYPES", 
            FieldType.STRING, 2);
        pnd_Wk_Input_Pnd_Twrt_Currency_Cde = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Currency_Cde", "#TWRT-CURRENCY-CDE", 
            FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Locality_Cde = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Locality_Cde", "#TWRT-LOCALITY-CDE", 
            FieldType.STRING, 3);
        pnd_Wk_Input_Pnd_Twrt_Rollover_Ind = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Rollover_Ind", "#TWRT-ROLLOVER-IND", 
            FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_State_Rsdncy = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_State_Rsdncy", "#TWRT-STATE-RSDNCY", 
            FieldType.STRING, 2);
        pnd_Wk_Input_Pnd_Twrt_Citizenship = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Citizenship", "#TWRT-CITIZENSHIP", 
            FieldType.NUMERIC, 2);
        pnd_Wk_Input_Pnd_Twrt_Dob_A = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Dob_A", "#TWRT-DOB-A", FieldType.STRING, 
            8);
        pnd_Wk_Input_Pnd_Twrt_Gross_Amt = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Gross_Amt", "#TWRT-GROSS-AMT", FieldType.STRING, 
            13);
        pnd_Wk_Input_Pnd_Twrt_Fed_Whhld_Amt = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Fed_Whhld_Amt", "#TWRT-FED-WHHLD-AMT", 
            FieldType.STRING, 11);
        pnd_Wk_Input_Pnd_Twrt_State_Whhld_Amt = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_State_Whhld_Amt", "#TWRT-STATE-WHHLD-AMT", 
            FieldType.STRING, 11);
        pnd_Wk_Input_Pnd_Twrt_Interest_Amt = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Interest_Amt", "#TWRT-INTEREST-AMT", 
            FieldType.STRING, 11);
        pnd_Wk_Input_Pnd_Twrt_Ivc_Amt = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Ivc_Amt", "#TWRT-IVC-AMT", FieldType.STRING, 
            11);
        pnd_Wk_Input_Pnd_Twrt_Ivc_Cde = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Ivc_Cde", "#TWRT-IVC-CDE", FieldType.STRING, 
            1);
        pnd_Wk_Input_Pnd_Twrt_Na_Line = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldArrayInGroup("pnd_Wk_Input_Pnd_Twrt_Na_Line", "#TWRT-NA-LINE", FieldType.STRING, 
            35, new DbsArrayController(1, 8));
        pnd_Wk_Input_Pnd_Twrt_Tax_Elct_Cde = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Tax_Elct_Cde", "#TWRT-TAX-ELCT-CDE", 
            FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Local_Whhld_Amt = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Local_Whhld_Amt", "#TWRT-LOCAL-WHHLD-AMT", 
            FieldType.STRING, 11);
        pnd_Wk_Input_Pnd_Twrt_Tot_Ivc_9b_Amt = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Tot_Ivc_9b_Amt", "#TWRT-TOT-IVC-9B-AMT", 
            FieldType.STRING, 11);
        pnd_Wk_Input_Pnd_Twrt_Disability_Cde = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Disability_Cde", "#TWRT-DISABILITY-CDE", 
            FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Lob_Cde = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Lob_Cde", "#TWRT-LOB-CDE", FieldType.STRING, 
            4);
        pnd_Wk_Input_Pnd_Twrt_Tax_Year = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Tax_Year", "#TWRT-TAX-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Wk_Input_Pnd_Twrt_Dod = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Dod", "#TWRT-DOD", FieldType.NUMERIC, 8);
        pnd_Wk_Input_Pnd_Twrt_Can_Whhld_Ind = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Can_Whhld_Ind", "#TWRT-CAN-WHHLD-IND", 
            FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Can_Whhld_Amt = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Can_Whhld_Amt", "#TWRT-CAN-WHHLD-AMT", 
            FieldType.STRING, 11);
        pnd_Wk_Input_Pnd_Twrt_Nra_Whhld_Amt = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Nra_Whhld_Amt", "#TWRT-NRA-WHHLD-AMT", 
            FieldType.STRING, 11);
        pnd_Wk_Input_Pnd_Twrt_Payee_Tax_Res_Sts = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Payee_Tax_Res_Sts", "#TWRT-PAYEE-TAX-RES-STS", 
            FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Pymnt_Tax_Form_1078 = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Pymnt_Tax_Form_1078", "#TWRT-PYMNT-TAX-FORM-1078", 
            FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Pymnt_Tax_Form_1001 = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Pymnt_Tax_Form_1001", "#TWRT-PYMNT-TAX-FORM-1001", 
            FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Pymnt_Tax_Exempt_Ind = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Pymnt_Tax_Exempt_Ind", "#TWRT-PYMNT-TAX-EXEMPT-IND", 
            FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Pymnt_Settlmnt_Dte = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Pymnt_Settlmnt_Dte", "#TWRT-PYMNT-SETTLMNT-DTE", 
            FieldType.STRING, 8);
        pnd_Wk_Input_Pnd_Twrt_Pymnt_Acctg_Dte = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Pymnt_Acctg_Dte", "#TWRT-PYMNT-ACCTG-DTE", 
            FieldType.STRING, 8);
        pnd_Wk_Input_Pnd_Twrt_Pymnt_Intrfce_Dte = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Pymnt_Intrfce_Dte", "#TWRT-PYMNT-INTRFCE-DTE", 
            FieldType.STRING, 8);
        pnd_Wk_Input_Pnd_Twrt_Ivc_Rcvry_Cde = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Ivc_Rcvry_Cde", "#TWRT-IVC-RCVRY-CDE", 
            FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Total_Distr_Amt = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Total_Distr_Amt", "#TWRT-TOTAL-DISTR-AMT", 
            FieldType.STRING, 13);
        pnd_Wk_Input_Pnd_Twrt_Distr_Pct = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Distr_Pct", "#TWRT-DISTR-PCT", FieldType.STRING, 
            8);
        pnd_Wk_Input_Pnd_Twrt_Addr_Chg_Ind = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Addr_Chg_Ind", "#TWRT-ADDR-CHG-IND", 
            FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Rej_Err_Nbr = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Rej_Err_Nbr", "#TWRT-REJ-ERR-NBR", 
            FieldType.STRING, 2);
        pnd_Wk_Input_Pnd_Twrt_Type_Refer = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Type_Refer", "#TWRT-TYPE-REFER", FieldType.STRING, 
            2);
        pnd_Wk_Input_Pnd_Twrt_Distrib_Cde = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Distrib_Cde", "#TWRT-DISTRIB-CDE", 
            FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Flag = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Flag", "#TWRT-FLAG", FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Contract_Type = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Contract_Type", "#TWRT-CONTRACT-TYPE", 
            FieldType.STRING, 5);
        pnd_Wk_Input_Pnd_Twrt_Annt_Residency_Code = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Annt_Residency_Code", "#TWRT-ANNT-RESIDENCY-CODE", 
            FieldType.STRING, 3);
        pnd_Wk_Input_Pnd_Twrt_Annt_Locality_Code = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Annt_Locality_Code", "#TWRT-ANNT-LOCALITY-CODE", 
            FieldType.STRING, 3);
        pnd_Wk_Input_Pnd_Twrt_Election_Code = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Election_Code", "#TWRT-ELECTION-CODE", 
            FieldType.NUMERIC, 3);
        pnd_Wk_Input_Pnd_Twrt_Fed_Nra_Tax_Rate = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Fed_Nra_Tax_Rate", "#TWRT-FED-NRA-TAX-RATE", 
            FieldType.STRING, 6);
        pnd_Wk_Input_Pnd_Twrt_Payment_Form1001 = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Payment_Form1001", "#TWRT-PAYMENT-FORM1001", 
            FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Distributing_Irc_Cde = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Distributing_Irc_Cde", "#TWRT-DISTRIBUTING-IRC-CDE", 
            FieldType.STRING, 20);
        pnd_Wk_Input_Pnd_Twrt_Receiving_Irc_Cde = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Receiving_Irc_Cde", "#TWRT-RECEIVING-IRC-CDE", 
            FieldType.STRING, 2);
        pnd_Wk_Input_Pnd_Twrt_Pymnt_Reqst_Nbr = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Pymnt_Reqst_Nbr", "#TWRT-PYMNT-REQST-NBR", 
            FieldType.STRING, 35);
        pnd_Wk_Input_Pnd_Twrt_Hardship_Cde = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Hardship_Cde", "#TWRT-HARDSHIP-CDE", 
            FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Termination_Of_Empl_Cde = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Termination_Of_Empl_Cde", 
            "#TWRT-TERMINATION-OF-EMPL-CDE", FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Distribution_Code = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Distribution_Code", "#TWRT-DISTRIBUTION-CODE", 
            FieldType.STRING, 2);
        pnd_Wk_Input_Pnd_Twrt_Nra_Exemption_Code = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Nra_Exemption_Code", "#TWRT-NRA-EXEMPTION-CODE", 
            FieldType.STRING, 2);
        pnd_Wk_Input_Pnd_Twrt_Tax_Type = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Tax_Type", "#TWRT-TAX-TYPE", FieldType.STRING, 
            1);
        pnd_Wk_Input_Pnd_Twrt_Plan_Type = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Plan_Type", "#TWRT-PLAN-TYPE", FieldType.STRING, 
            1);
        pnd_Wk_Input_Pnd_Twrt_Reason_Code = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Reason_Code", "#TWRT-REASON-CODE", 
            FieldType.STRING, 2);
        pnd_Wk_Input_Pnd_Twrt_Op_Rev_Ind = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Op_Rev_Ind", "#TWRT-OP-REV-IND", FieldType.STRING, 
            1);
        pnd_Wk_Input_Pnd_Twrt_Op_Adj_Ind = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Op_Adj_Ind", "#TWRT-OP-ADJ-IND", FieldType.STRING, 
            1);
        pnd_Wk_Input_Pnd_Twrt_Op_Maint_Ind = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Op_Maint_Ind", "#TWRT-OP-MAINT-IND", 
            FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Distribution_Type = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Distribution_Type", "#TWRT-DISTRIBUTION-TYPE", 
            FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Check_Reason = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Check_Reason", "#TWRT-CHECK-REASON", 
            FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Roth_Qual_Ind = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Roth_Qual_Ind", "#TWRT-ROTH-QUAL-IND", 
            FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Roth_Death_Ind = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Roth_Death_Ind", "#TWRT-ROTH-DEATH-IND", 
            FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Twrt_Filler = pnd_Wk_Input_Pnd_Wk_Input_Detail.newFieldInGroup("pnd_Wk_Input_Pnd_Twrt_Filler", "#TWRT-FILLER", FieldType.STRING, 
            3);

        pnd_Wk_Input__R_Field_2 = localVariables.newGroupInRecord("pnd_Wk_Input__R_Field_2", "REDEFINE", pnd_Wk_Input);

        pnd_Wk_Input_Pnd_Wk_Input_Control = pnd_Wk_Input__R_Field_2.newGroupInGroup("pnd_Wk_Input_Pnd_Wk_Input_Control", "#WK-INPUT-CONTROL");
        pnd_Wk_Input_Pnd_Z = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_Z", "#Z", FieldType.STRING, 1);
        pnd_Wk_Input_Pnd_Source_Code = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_Source_Code", "#SOURCE-CODE", FieldType.STRING, 
            2);
        pnd_Wk_Input_Pnd_T_Comp_Code = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_T_Comp_Code", "#T-COMP-CODE", FieldType.STRING, 
            1);
        pnd_Wk_Input_Pnd_T_Rec_Cnt = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_T_Rec_Cnt", "#T-REC-CNT", FieldType.STRING, 7);
        pnd_Wk_Input_Pnd_T_Gross_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_T_Gross_A", "#T-GROSS-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_T_Ivc_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_T_Ivc_A", "#T-IVC-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_T_Int_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_T_Int_A", "#T-INT-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_T_Fed_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_T_Fed_A", "#T-FED-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_T_Nra_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_T_Nra_A", "#T-NRA-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_T_State_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_T_State_A", "#T-STATE-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_T_Local_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_T_Local_A", "#T-LOCAL-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_T_Canadian_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_T_Canadian_A", "#T-CANADIAN-A", FieldType.STRING, 
            15);
        pnd_Wk_Input_Pnd_C_Comp_Code = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_C_Comp_Code", "#C-COMP-CODE", FieldType.STRING, 
            1);
        pnd_Wk_Input_Pnd_C_Rec_Cnt = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_C_Rec_Cnt", "#C-REC-CNT", FieldType.STRING, 7);
        pnd_Wk_Input_Pnd_C_Gross_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_C_Gross_A", "#C-GROSS-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_C_Ivc_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_C_Ivc_A", "#C-IVC-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_C_Int_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_C_Int_A", "#C-INT-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_C_Fed_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_C_Fed_A", "#C-FED-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_C_Nra_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_C_Nra_A", "#C-NRA-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_C_State_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_C_State_A", "#C-STATE-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_C_Local_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_C_Local_A", "#C-LOCAL-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_C_Canadian_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_C_Canadian_A", "#C-CANADIAN-A", FieldType.STRING, 
            15);
        pnd_Wk_Input_Pnd_L_Comp_Code = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_L_Comp_Code", "#L-COMP-CODE", FieldType.STRING, 
            1);
        pnd_Wk_Input_Pnd_L_Rec_Cnt = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_L_Rec_Cnt", "#L-REC-CNT", FieldType.STRING, 7);
        pnd_Wk_Input_Pnd_L_Gross_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_L_Gross_A", "#L-GROSS-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_L_Ivc_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_L_Ivc_A", "#L-IVC-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_L_Int_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_L_Int_A", "#L-INT-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_L_Fed_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_L_Fed_A", "#L-FED-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_L_Nra_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_L_Nra_A", "#L-NRA-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_L_State_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_L_State_A", "#L-STATE-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_L_Local_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_L_Local_A", "#L-LOCAL-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_L_Canadian_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_L_Canadian_A", "#L-CANADIAN-A", FieldType.STRING, 
            15);
        pnd_Wk_Input_Pnd_From_Dte = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_From_Dte", "#FROM-DTE", FieldType.STRING, 8);
        pnd_Wk_Input_Pnd_To_Dte = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_To_Dte", "#TO-DTE", FieldType.STRING, 8);
        pnd_Wk_Input_Pnd_Tx_Year = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_Tx_Year", "#TX-YEAR", FieldType.STRING, 4);
        pnd_Wk_Input_Pnd_Interface_Dte = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_Interface_Dte", "#INTERFACE-DTE", FieldType.STRING, 
            8);
        pnd_Wk_Input_Pnd_S_Comp_Code = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_S_Comp_Code", "#S-COMP-CODE", FieldType.STRING, 
            1);
        pnd_Wk_Input_Pnd_S_Rec_Cnt = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_S_Rec_Cnt", "#S-REC-CNT", FieldType.STRING, 7);
        pnd_Wk_Input_Pnd_S_Gross_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_S_Gross_A", "#S-GROSS-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_S_Ivc_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_S_Ivc_A", "#S-IVC-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_S_Int_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_S_Int_A", "#S-INT-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_S_Fed_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_S_Fed_A", "#S-FED-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_S_Nra_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_S_Nra_A", "#S-NRA-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_S_State_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_S_State_A", "#S-STATE-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_S_Local_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_S_Local_A", "#S-LOCAL-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_S_Canadian_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_S_Canadian_A", "#S-CANADIAN-A", FieldType.STRING, 
            15);
        pnd_Wk_Input_Pnd_F_Comp_Code = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_F_Comp_Code", "#F-COMP-CODE", FieldType.STRING, 
            1);
        pnd_Wk_Input_Pnd_F_Rec_Cnt = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_F_Rec_Cnt", "#F-REC-CNT", FieldType.STRING, 7);
        pnd_Wk_Input_Pnd_F_Gross_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_F_Gross_A", "#F-GROSS-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_F_Ivc_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_F_Ivc_A", "#F-IVC-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_F_Int_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_F_Int_A", "#F-INT-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_F_Fed_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_F_Fed_A", "#F-FED-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_F_Nra_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_F_Nra_A", "#F-NRA-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_F_State_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_F_State_A", "#F-STATE-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_F_Local_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_F_Local_A", "#F-LOCAL-A", FieldType.STRING, 15);
        pnd_Wk_Input_Pnd_F_Canadian_A = pnd_Wk_Input_Pnd_Wk_Input_Control.newFieldInGroup("pnd_Wk_Input_Pnd_F_Canadian_A", "#F-CANADIAN-A", FieldType.STRING, 
            15);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0700.initializeValues();
        ldaTwrl0710.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp1730() throws Exception
    {
        super("Twrp1730");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* ******************************************************
        WK01:                                                                                                                                                             //Natural: READ WORK FILE 1 RECORD #WK-INPUT
        while (condition(getWorkFiles().read(1, pnd_Wk_Input)))
        {
            //* VIKRAM2
            short decideConditionsMet471 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #WK-INPUT.#TWRT-COMPANY-CDE EQ 'L' OR #WK-INPUT.#TWRT-COMPANY-CDE EQ 'T'
            if (condition(pnd_Wk_Input_Pnd_Twrt_Company_Cde.equals("L") || pnd_Wk_Input_Pnd_Twrt_Company_Cde.equals("T")))
            {
                decideConditionsMet471++;
                                                                                                                                                                          //Natural: PERFORM S010-WRITE-DETAIL
                sub_S010_Write_Detail();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("WK01"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("WK01"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN #WK-INPUT.#TWRT-COMPANY-CDE EQ 'Z'
            else if (condition(pnd_Wk_Input_Pnd_Twrt_Company_Cde.equals("Z")))
            {
                decideConditionsMet471++;
                                                                                                                                                                          //Natural: PERFORM S020-WRITE-CONTROL
                sub_S020_Write_Control();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("WK01"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("WK01"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                getReports().write(0, "wrong company code on transmission file:");                                                                                        //Natural: WRITE 'wrong company code on transmission file:'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("WK01"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("WK01"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "=",pnd_Wk_Input_Pnd_Twrt_Company_Cde);                                                                                             //Natural: WRITE '=' #WK-INPUT.#TWRT-COMPANY-CDE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("WK01"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("WK01"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(32);  if (true) return;                                                                                                                 //Natural: TERMINATE 32
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  (WK01.)
        }                                                                                                                                                                 //Natural: END-WORK
        WK01_Exit:
        if (Global.isEscape()) return;
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: S010-WRITE-DETAIL
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: S020-WRITE-CONTROL
        //* **********************************************************
    }
    private void sub_S010_Write_Detail() throws Exception                                                                                                                 //Natural: S010-WRITE-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        ldaTwrl0700.getTwrt_Record().reset();                                                                                                                             //Natural: RESET TWRT-RECORD
        ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt().compute(new ComputeParameters(false, ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt()), pnd_Wk_Input_Pnd_Twrt_Gross_Amt.val()); //Natural: ASSIGN TWRT-GROSS-AMT := VAL ( #WK-INPUT.#TWRT-GROSS-AMT )
        ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt().compute(new ComputeParameters(false, ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt()), pnd_Wk_Input_Pnd_Twrt_Fed_Whhld_Amt.val()); //Natural: ASSIGN TWRT-FED-WHHLD-AMT := VAL ( #WK-INPUT.#TWRT-FED-WHHLD-AMT )
        ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt().compute(new ComputeParameters(false, ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt()), pnd_Wk_Input_Pnd_Twrt_State_Whhld_Amt.val()); //Natural: ASSIGN TWRT-STATE-WHHLD-AMT := VAL ( #WK-INPUT.#TWRT-STATE-WHHLD-AMT )
        ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt().compute(new ComputeParameters(false, ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt()), pnd_Wk_Input_Pnd_Twrt_Interest_Amt.val()); //Natural: ASSIGN TWRT-INTEREST-AMT := VAL ( #WK-INPUT.#TWRT-INTEREST-AMT )
        ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt().compute(new ComputeParameters(false, ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt()), pnd_Wk_Input_Pnd_Twrt_Ivc_Amt.val());  //Natural: ASSIGN TWRT-IVC-AMT := VAL ( #WK-INPUT.#TWRT-IVC-AMT )
        ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt().compute(new ComputeParameters(false, ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt()), pnd_Wk_Input_Pnd_Twrt_Local_Whhld_Amt.val()); //Natural: ASSIGN TWRT-LOCAL-WHHLD-AMT := VAL ( #WK-INPUT.#TWRT-LOCAL-WHHLD-AMT )
        ldaTwrl0700.getTwrt_Record_Twrt_Tot_Ivc_9b_Amt().compute(new ComputeParameters(false, ldaTwrl0700.getTwrt_Record_Twrt_Tot_Ivc_9b_Amt()), pnd_Wk_Input_Pnd_Twrt_Tot_Ivc_9b_Amt.val()); //Natural: ASSIGN TWRT-TOT-IVC-9B-AMT := VAL ( #WK-INPUT.#TWRT-TOT-IVC-9B-AMT )
        ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Amt().compute(new ComputeParameters(false, ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Amt()), pnd_Wk_Input_Pnd_Twrt_Can_Whhld_Amt.val()); //Natural: ASSIGN TWRT-CAN-WHHLD-AMT := VAL ( #WK-INPUT.#TWRT-CAN-WHHLD-AMT )
        ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt().compute(new ComputeParameters(false, ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt()), pnd_Wk_Input_Pnd_Twrt_Nra_Whhld_Amt.val()); //Natural: ASSIGN TWRT-NRA-WHHLD-AMT := VAL ( #WK-INPUT.#TWRT-NRA-WHHLD-AMT )
        ldaTwrl0700.getTwrt_Record_Twrt_Total_Distr_Amt().compute(new ComputeParameters(false, ldaTwrl0700.getTwrt_Record_Twrt_Total_Distr_Amt()), pnd_Wk_Input_Pnd_Twrt_Total_Distr_Amt.val()); //Natural: ASSIGN TWRT-TOTAL-DISTR-AMT := VAL ( #WK-INPUT.#TWRT-TOTAL-DISTR-AMT )
        ldaTwrl0700.getTwrt_Record_Twrt_Distr_Pct().compute(new ComputeParameters(false, ldaTwrl0700.getTwrt_Record_Twrt_Distr_Pct()), pnd_Wk_Input_Pnd_Twrt_Distr_Pct.val()); //Natural: ASSIGN TWRT-DISTR-PCT := VAL ( #WK-INPUT.#TWRT-DISTR-PCT )
        ldaTwrl0700.getTwrt_Record_Twrt_Fed_Nra_Tax_Rate().compute(new ComputeParameters(false, ldaTwrl0700.getTwrt_Record_Twrt_Fed_Nra_Tax_Rate()), pnd_Wk_Input_Pnd_Twrt_Fed_Nra_Tax_Rate.val()); //Natural: ASSIGN TWRT-FED-NRA-TAX-RATE := VAL ( #WK-INPUT.#TWRT-FED-NRA-TAX-RATE )
        //* **
        ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().setValue(pnd_Wk_Input_Pnd_Twrt_Company_Cde);                                                                        //Natural: MOVE #WK-INPUT.#TWRT-COMPANY-CDE TO TWRT-COMPANY-CDE
        ldaTwrl0700.getTwrt_Record_Twrt_Source().setValue(pnd_Wk_Input_Pnd_Twrt_Source);                                                                                  //Natural: MOVE #WK-INPUT.#TWRT-SOURCE TO TWRT-SOURCE
        ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().setValue(pnd_Wk_Input_Pnd_Twrt_Cntrct_Nbr);                                                                          //Natural: MOVE #WK-INPUT.#TWRT-CNTRCT-NBR TO TWRT-CNTRCT-NBR
        ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde().setValue(pnd_Wk_Input_Pnd_Twrt_Payee_Cde);                                                                            //Natural: MOVE #WK-INPUT.#TWRT-PAYEE-CDE TO TWRT-PAYEE-CDE
        ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type().setValue(pnd_Wk_Input_Pnd_Twrt_Tax_Id_Type);                                                                        //Natural: MOVE #WK-INPUT.#TWRT-TAX-ID-TYPE TO TWRT-TAX-ID-TYPE
        ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id().setValue(pnd_Wk_Input_Pnd_Twrt_Tax_Id);                                                                                  //Natural: MOVE #WK-INPUT.#TWRT-TAX-ID TO TWRT-TAX-ID
        //*  PIN EXP 08/2017
        pnd_Wk_Input_Pnd_Twrt_Pin_Id.setValue(pnd_Wk_Input_Pnd_Twrt_Pin_Id, MoveOption.RightJustified);                                                                   //Natural: MOVE RIGHT #TWRT-PIN-ID TO #TWRT-PIN-ID
        //*  PIN EXP 08/2017
        DbsUtil.examine(new ExamineSource(pnd_Wk_Input_Pnd_Twrt_Pin_Id,true), new ExamineSearch(" "), new ExamineReplace("0"));                                           //Natural: EXAMINE FULL #TWRT-PIN-ID FOR ' ' REPLACE WITH '0'
        ldaTwrl0700.getTwrt_Record_Twrt_Pin_Id().setValue(pnd_Wk_Input_Pnd_Twrt_Pin_Id);                                                                                  //Natural: MOVE #WK-INPUT.#TWRT-PIN-ID TO TWRT-PIN-ID
        ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Dte().setValue(pnd_Wk_Input_Pnd_Twrt_Paymt_Dte);                                                                            //Natural: MOVE #WK-INPUT.#TWRT-PAYMT-DTE TO TWRT-PAYMT-DTE
        ldaTwrl0700.getTwrt_Record_Twrt_Pay_Set_Types().setValue(pnd_Wk_Input_Pnd_Twrt_Pay_Set_Types);                                                                    //Natural: MOVE #WK-INPUT.#TWRT-PAY-SET-TYPES TO TWRT-PAY-SET-TYPES
        ldaTwrl0700.getTwrt_Record_Twrt_Currency_Cde().setValue(pnd_Wk_Input_Pnd_Twrt_Currency_Cde);                                                                      //Natural: MOVE #WK-INPUT.#TWRT-CURRENCY-CDE TO TWRT-CURRENCY-CDE
        ldaTwrl0700.getTwrt_Record_Twrt_Locality_Cde().setValue(pnd_Wk_Input_Pnd_Twrt_Locality_Cde);                                                                      //Natural: MOVE #WK-INPUT.#TWRT-LOCALITY-CDE TO TWRT-LOCALITY-CDE
        ldaTwrl0700.getTwrt_Record_Twrt_Rollover_Ind().setValue(pnd_Wk_Input_Pnd_Twrt_Rollover_Ind);                                                                      //Natural: MOVE #WK-INPUT.#TWRT-ROLLOVER-IND TO TWRT-ROLLOVER-IND
        ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().setValue(pnd_Wk_Input_Pnd_Twrt_State_Rsdncy);                                                                      //Natural: MOVE #WK-INPUT.#TWRT-STATE-RSDNCY TO TWRT-STATE-RSDNCY
        ldaTwrl0700.getTwrt_Record_Twrt_Citizenship().setValue(pnd_Wk_Input_Pnd_Twrt_Citizenship);                                                                        //Natural: MOVE #WK-INPUT.#TWRT-CITIZENSHIP TO TWRT-CITIZENSHIP
        ldaTwrl0700.getTwrt_Record_Twrt_Dob_A().setValue(pnd_Wk_Input_Pnd_Twrt_Dob_A);                                                                                    //Natural: MOVE #WK-INPUT.#TWRT-DOB-A TO TWRT-DOB-A
        ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().setValue(pnd_Wk_Input_Pnd_Twrt_Ivc_Cde);                                                                                //Natural: MOVE #WK-INPUT.#TWRT-IVC-CDE TO TWRT-IVC-CDE
        ldaTwrl0700.getTwrt_Record_Twrt_Na_Line().getValue("*").setValue(pnd_Wk_Input_Pnd_Twrt_Na_Line.getValue("*"));                                                    //Natural: MOVE #WK-INPUT.#TWRT-NA-LINE ( * ) TO TWRT-NA-LINE ( * )
        ldaTwrl0700.getTwrt_Record_Twrt_Tax_Elct_Cde().setValue(pnd_Wk_Input_Pnd_Twrt_Tax_Elct_Cde);                                                                      //Natural: MOVE #WK-INPUT.#TWRT-TAX-ELCT-CDE TO TWRT-TAX-ELCT-CDE
        ldaTwrl0700.getTwrt_Record_Twrt_Disability_Cde().setValue(pnd_Wk_Input_Pnd_Twrt_Disability_Cde);                                                                  //Natural: MOVE #WK-INPUT.#TWRT-DISABILITY-CDE TO TWRT-DISABILITY-CDE
        ldaTwrl0700.getTwrt_Record_Twrt_Lob_Cde().setValue(pnd_Wk_Input_Pnd_Twrt_Lob_Cde);                                                                                //Natural: MOVE #WK-INPUT.#TWRT-LOB-CDE TO TWRT-LOB-CDE
        ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year().setValue(pnd_Wk_Input_Pnd_Twrt_Tax_Year);                                                                              //Natural: MOVE #WK-INPUT.#TWRT-TAX-YEAR TO TWRT-TAX-YEAR
        ldaTwrl0700.getTwrt_Record_Twrt_Dod().setValue(pnd_Wk_Input_Pnd_Twrt_Dod);                                                                                        //Natural: MOVE #WK-INPUT.#TWRT-DOD TO TWRT-DOD
        ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Ind().setValue(pnd_Wk_Input_Pnd_Twrt_Can_Whhld_Ind);                                                                    //Natural: MOVE #WK-INPUT.#TWRT-CAN-WHHLD-IND TO TWRT-CAN-WHHLD-IND
        ldaTwrl0700.getTwrt_Record_Twrt_Payee_Tax_Res_Sts().setValue(pnd_Wk_Input_Pnd_Twrt_Payee_Tax_Res_Sts);                                                            //Natural: MOVE #WK-INPUT.#TWRT-PAYEE-TAX-RES-STS TO TWRT-PAYEE-TAX-RES-STS
        ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().setValue(pnd_Wk_Input_Pnd_Twrt_Pymnt_Tax_Form_1078);                                                        //Natural: MOVE #WK-INPUT.#TWRT-PYMNT-TAX-FORM-1078 TO TWRT-PYMNT-TAX-FORM-1078
        ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001().setValue(pnd_Wk_Input_Pnd_Twrt_Pymnt_Tax_Form_1001);                                                        //Natural: MOVE #WK-INPUT.#TWRT-PYMNT-TAX-FORM-1001 TO TWRT-PYMNT-TAX-FORM-1001
        ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Tax_Exempt_Ind().setValue(pnd_Wk_Input_Pnd_Twrt_Pymnt_Tax_Exempt_Ind);                                                      //Natural: MOVE #WK-INPUT.#TWRT-PYMNT-TAX-EXEMPT-IND TO TWRT-PYMNT-TAX-EXEMPT-IND
        ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Settlmnt_Dte().setValue(pnd_Wk_Input_Pnd_Twrt_Pymnt_Settlmnt_Dte);                                                          //Natural: MOVE #WK-INPUT.#TWRT-PYMNT-SETTLMNT-DTE TO TWRT-PYMNT-SETTLMNT-DTE
        ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Acctg_Dte().setValue(pnd_Wk_Input_Pnd_Twrt_Pymnt_Acctg_Dte);                                                                //Natural: MOVE #WK-INPUT.#TWRT-PYMNT-ACCTG-DTE TO TWRT-PYMNT-ACCTG-DTE
        ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Intrfce_Dte().setValue(pnd_Wk_Input_Pnd_Twrt_Pymnt_Intrfce_Dte);                                                            //Natural: MOVE #WK-INPUT.#TWRT-PYMNT-INTRFCE-DTE TO TWRT-PYMNT-INTRFCE-DTE
        ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Rcvry_Cde().setValue(pnd_Wk_Input_Pnd_Twrt_Ivc_Rcvry_Cde);                                                                    //Natural: MOVE #WK-INPUT.#TWRT-IVC-RCVRY-CDE TO TWRT-IVC-RCVRY-CDE
        ldaTwrl0700.getTwrt_Record_Twrt_Addr_Chg_Ind().setValue(pnd_Wk_Input_Pnd_Twrt_Addr_Chg_Ind);                                                                      //Natural: MOVE #WK-INPUT.#TWRT-ADDR-CHG-IND TO TWRT-ADDR-CHG-IND
        ldaTwrl0700.getTwrt_Record_Twrt_Rej_Err_Nbr().setValue(pnd_Wk_Input_Pnd_Twrt_Rej_Err_Nbr);                                                                        //Natural: MOVE #WK-INPUT.#TWRT-REJ-ERR-NBR TO TWRT-REJ-ERR-NBR
        ldaTwrl0700.getTwrt_Record_Twrt_Type_Refer().setValue(pnd_Wk_Input_Pnd_Twrt_Type_Refer);                                                                          //Natural: MOVE #WK-INPUT.#TWRT-TYPE-REFER TO TWRT-TYPE-REFER
        ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde().setValue(pnd_Wk_Input_Pnd_Twrt_Distrib_Cde);                                                                        //Natural: MOVE #WK-INPUT.#TWRT-DISTRIB-CDE TO TWRT-DISTRIB-CDE
        ldaTwrl0700.getTwrt_Record_Twrt_Flag().setValue(pnd_Wk_Input_Pnd_Twrt_Flag);                                                                                      //Natural: MOVE #WK-INPUT.#TWRT-FLAG TO TWRT-FLAG
        ldaTwrl0700.getTwrt_Record_Twrt_Contract_Type().setValue(pnd_Wk_Input_Pnd_Twrt_Contract_Type);                                                                    //Natural: MOVE #WK-INPUT.#TWRT-CONTRACT-TYPE TO TWRT-CONTRACT-TYPE
        ldaTwrl0700.getTwrt_Record_Twrt_Annt_Residency_Code().setValue(pnd_Wk_Input_Pnd_Twrt_Annt_Residency_Code);                                                        //Natural: MOVE #WK-INPUT.#TWRT-ANNT-RESIDENCY-CODE TO TWRT-ANNT-RESIDENCY-CODE
        ldaTwrl0700.getTwrt_Record_Twrt_Annt_Locality_Code().setValue(pnd_Wk_Input_Pnd_Twrt_Annt_Locality_Code);                                                          //Natural: MOVE #WK-INPUT.#TWRT-ANNT-LOCALITY-CODE TO TWRT-ANNT-LOCALITY-CODE
        ldaTwrl0700.getTwrt_Record_Twrt_Election_Code().setValue(pnd_Wk_Input_Pnd_Twrt_Election_Code);                                                                    //Natural: MOVE #WK-INPUT.#TWRT-ELECTION-CODE TO TWRT-ELECTION-CODE
        ldaTwrl0700.getTwrt_Record_Twrt_Annt_Locality_Code().setValue(pnd_Wk_Input_Pnd_Twrt_Payment_Form1001);                                                            //Natural: MOVE #WK-INPUT.#TWRT-PAYMENT-FORM1001 TO TWRT-ANNT-LOCALITY-CODE
        ldaTwrl0700.getTwrt_Record_Twrt_Distributing_Irc_Cde().setValue(pnd_Wk_Input_Pnd_Twrt_Distributing_Irc_Cde);                                                      //Natural: MOVE #WK-INPUT.#TWRT-DISTRIBUTING-IRC-CDE TO TWRT-DISTRIBUTING-IRC-CDE
        ldaTwrl0700.getTwrt_Record_Twrt_Receiving_Irc_Cde().setValue(pnd_Wk_Input_Pnd_Twrt_Receiving_Irc_Cde);                                                            //Natural: MOVE #WK-INPUT.#TWRT-RECEIVING-IRC-CDE TO TWRT-RECEIVING-IRC-CDE
        ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Reqst_Nbr().setValue(pnd_Wk_Input_Pnd_Twrt_Pymnt_Reqst_Nbr);                                                                //Natural: MOVE #WK-INPUT.#TWRT-PYMNT-REQST-NBR TO TWRT-PYMNT-REQST-NBR
        ldaTwrl0700.getTwrt_Record_Twrt_Hardship_Cde().setValue(pnd_Wk_Input_Pnd_Twrt_Hardship_Cde);                                                                      //Natural: MOVE #WK-INPUT.#TWRT-HARDSHIP-CDE TO TWRT-HARDSHIP-CDE
        ldaTwrl0700.getTwrt_Record_Twrt_Termination_Of_Empl_Cde().setValue(pnd_Wk_Input_Pnd_Twrt_Termination_Of_Empl_Cde);                                                //Natural: MOVE #WK-INPUT.#TWRT-TERMINATION-OF-EMPL-CDE TO TWRT-TERMINATION-OF-EMPL-CDE
        ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().setValue(pnd_Wk_Input_Pnd_Twrt_Distribution_Code);                                                            //Natural: MOVE #WK-INPUT.#TWRT-DISTRIBUTION-CODE TO TWRT-DISTRIBUTION-CODE
        ldaTwrl0700.getTwrt_Record_Twrt_Nra_Exemption_Code().setValue(pnd_Wk_Input_Pnd_Twrt_Nra_Exemption_Code);                                                          //Natural: MOVE #WK-INPUT.#TWRT-NRA-EXEMPTION-CODE TO TWRT-NRA-EXEMPTION-CODE
        ldaTwrl0700.getTwrt_Record_Twrt_Tax_Type().setValue(pnd_Wk_Input_Pnd_Twrt_Tax_Type);                                                                              //Natural: MOVE #WK-INPUT.#TWRT-TAX-TYPE TO TWRT-TAX-TYPE
        ldaTwrl0700.getTwrt_Record_Twrt_Plan_Type().setValue(pnd_Wk_Input_Pnd_Twrt_Plan_Type);                                                                            //Natural: MOVE #WK-INPUT.#TWRT-PLAN-TYPE TO TWRT-PLAN-TYPE
        ldaTwrl0700.getTwrt_Record_Twrt_Reason_Code().setValue(pnd_Wk_Input_Pnd_Twrt_Reason_Code);                                                                        //Natural: MOVE #WK-INPUT.#TWRT-REASON-CODE TO TWRT-REASON-CODE
        ldaTwrl0700.getTwrt_Record_Twrt_Op_Rev_Ind().setValue(pnd_Wk_Input_Pnd_Twrt_Op_Rev_Ind);                                                                          //Natural: MOVE #WK-INPUT.#TWRT-OP-REV-IND TO TWRT-OP-REV-IND
        ldaTwrl0700.getTwrt_Record_Twrt_Op_Adj_Ind().setValue(pnd_Wk_Input_Pnd_Twrt_Op_Adj_Ind);                                                                          //Natural: MOVE #WK-INPUT.#TWRT-OP-ADJ-IND TO TWRT-OP-ADJ-IND
        ldaTwrl0700.getTwrt_Record_Twrt_Op_Maint_Ind().setValue(pnd_Wk_Input_Pnd_Twrt_Op_Maint_Ind);                                                                      //Natural: MOVE #WK-INPUT.#TWRT-OP-MAINT-IND TO TWRT-OP-MAINT-IND
        ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Type().setValue(pnd_Wk_Input_Pnd_Twrt_Distribution_Type);                                                            //Natural: MOVE #WK-INPUT.#TWRT-DISTRIBUTION-TYPE TO TWRT-DISTRIBUTION-TYPE
        ldaTwrl0700.getTwrt_Record_Twrt_Check_Reason().setValue(pnd_Wk_Input_Pnd_Twrt_Check_Reason);                                                                      //Natural: MOVE #WK-INPUT.#TWRT-CHECK-REASON TO TWRT-CHECK-REASON
        ldaTwrl0700.getTwrt_Record_Twrt_Roth_Qual_Ind().setValue(pnd_Wk_Input_Pnd_Twrt_Roth_Qual_Ind);                                                                    //Natural: MOVE #WK-INPUT.#TWRT-ROTH-QUAL-IND TO TWRT-ROTH-QUAL-IND
        ldaTwrl0700.getTwrt_Record_Twrt_Roth_Death_Ind().setValue(pnd_Wk_Input_Pnd_Twrt_Roth_Death_Ind);                                                                  //Natural: MOVE #WK-INPUT.#TWRT-ROTH-DEATH-IND TO TWRT-ROTH-DEATH-IND
        ldaTwrl0700.getTwrt_Record_Twrt_Filler().setValue(pnd_Wk_Input_Pnd_Twrt_Filler);                                                                                  //Natural: MOVE #WK-INPUT.#TWRT-FILLER TO TWRT-FILLER
        //* **
        getWorkFiles().write(2, false, ldaTwrl0700.getTwrt_Record());                                                                                                     //Natural: WRITE WORK FILE 2 TWRT-RECORD
        //*  S010-WRITE-DETAIL
    }
    private void sub_S020_Write_Control() throws Exception                                                                                                                //Natural: S020-WRITE-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        ldaTwrl0710.getPnd_Inp_Summary().reset();                                                                                                                         //Natural: RESET #INP-SUMMARY TWRT-RECORD
        ldaTwrl0700.getTwrt_Record().reset();
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_Z().setValue(pnd_Wk_Input_Pnd_Z);                                                                                              //Natural: MOVE #WK-INPUT.#Z TO #INP-SUMMARY.#Z
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_Source_Code().setValue(pnd_Wk_Input_Pnd_Source_Code);                                                                          //Natural: MOVE #WK-INPUT.#SOURCE-CODE TO #INP-SUMMARY.#SOURCE-CODE
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Comp_Code().setValue(pnd_Wk_Input_Pnd_T_Comp_Code);                                                                          //Natural: MOVE #WK-INPUT.#T-COMP-CODE TO #INP-SUMMARY.#T-COMP-CODE
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Rec_Cnt().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Rec_Cnt()), pnd_Wk_Input_Pnd_T_Rec_Cnt.val()); //Natural: ASSIGN #INP-SUMMARY.#T-REC-CNT := VAL ( #WK-INPUT.#T-REC-CNT )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Gross_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Gross_A()), pnd_Wk_Input_Pnd_T_Gross_A.val()); //Natural: ASSIGN #INP-SUMMARY.#T-GROSS-A := VAL ( #WK-INPUT.#T-GROSS-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Ivc_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Ivc_A()), pnd_Wk_Input_Pnd_T_Ivc_A.val()); //Natural: ASSIGN #INP-SUMMARY.#T-IVC-A := VAL ( #WK-INPUT.#T-IVC-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Int_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Int_A()), pnd_Wk_Input_Pnd_T_Int_A.val()); //Natural: ASSIGN #INP-SUMMARY.#T-INT-A := VAL ( #WK-INPUT.#T-INT-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Fed_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Fed_A()), pnd_Wk_Input_Pnd_T_Fed_A.val()); //Natural: ASSIGN #INP-SUMMARY.#T-FED-A := VAL ( #WK-INPUT.#T-FED-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Nra_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Nra_A()), pnd_Wk_Input_Pnd_T_Nra_A.val()); //Natural: ASSIGN #INP-SUMMARY.#T-NRA-A := VAL ( #WK-INPUT.#T-NRA-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_State_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_State_A()), pnd_Wk_Input_Pnd_T_State_A.val()); //Natural: ASSIGN #INP-SUMMARY.#T-STATE-A := VAL ( #WK-INPUT.#T-STATE-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Local_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Local_A()), pnd_Wk_Input_Pnd_T_Local_A.val()); //Natural: ASSIGN #INP-SUMMARY.#T-LOCAL-A := VAL ( #WK-INPUT.#T-LOCAL-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Canadian_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Canadian_A()), pnd_Wk_Input_Pnd_T_Canadian_A.val()); //Natural: ASSIGN #INP-SUMMARY.#T-CANADIAN-A := VAL ( #WK-INPUT.#T-CANADIAN-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Comp_Code().setValue(pnd_Wk_Input_Pnd_C_Comp_Code);                                                                          //Natural: MOVE #WK-INPUT.#C-COMP-CODE TO #INP-SUMMARY.#C-COMP-CODE
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Rec_Cnt().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Rec_Cnt()), pnd_Wk_Input_Pnd_C_Rec_Cnt.val()); //Natural: ASSIGN #INP-SUMMARY.#C-REC-CNT := VAL ( #WK-INPUT.#C-REC-CNT )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Gross_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Gross_A()), pnd_Wk_Input_Pnd_C_Gross_A.val()); //Natural: ASSIGN #INP-SUMMARY.#C-GROSS-A := VAL ( #WK-INPUT.#C-GROSS-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Ivc_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Ivc_A()), pnd_Wk_Input_Pnd_C_Ivc_A.val()); //Natural: ASSIGN #INP-SUMMARY.#C-IVC-A := VAL ( #WK-INPUT.#C-IVC-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Int_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Int_A()), pnd_Wk_Input_Pnd_C_Int_A.val()); //Natural: ASSIGN #INP-SUMMARY.#C-INT-A := VAL ( #WK-INPUT.#C-INT-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Fed_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Fed_A()), pnd_Wk_Input_Pnd_C_Fed_A.val()); //Natural: ASSIGN #INP-SUMMARY.#C-FED-A := VAL ( #WK-INPUT.#C-FED-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Nra_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Nra_A()), pnd_Wk_Input_Pnd_C_Nra_A.val()); //Natural: ASSIGN #INP-SUMMARY.#C-NRA-A := VAL ( #WK-INPUT.#C-NRA-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_State_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_State_A()), pnd_Wk_Input_Pnd_C_State_A.val()); //Natural: ASSIGN #INP-SUMMARY.#C-STATE-A := VAL ( #WK-INPUT.#C-STATE-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Local_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Local_A()), pnd_Wk_Input_Pnd_C_Local_A.val()); //Natural: ASSIGN #INP-SUMMARY.#C-LOCAL-A := VAL ( #WK-INPUT.#C-LOCAL-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Canadian_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Canadian_A()), pnd_Wk_Input_Pnd_C_Canadian_A.val()); //Natural: ASSIGN #INP-SUMMARY.#C-CANADIAN-A := VAL ( #WK-INPUT.#C-CANADIAN-A )
        //*  VIKRAM2
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Comp_Code().setValue(pnd_Wk_Input_Pnd_L_Comp_Code);                                                                          //Natural: MOVE #WK-INPUT.#L-COMP-CODE TO #INP-SUMMARY.#L-COMP-CODE
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Rec_Cnt().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Rec_Cnt()), pnd_Wk_Input_Pnd_L_Rec_Cnt.val()); //Natural: ASSIGN #INP-SUMMARY.#L-REC-CNT := VAL ( #WK-INPUT.#L-REC-CNT )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Gross_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Gross_A()), pnd_Wk_Input_Pnd_L_Gross_A.val()); //Natural: ASSIGN #INP-SUMMARY.#L-GROSS-A := VAL ( #WK-INPUT.#L-GROSS-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Ivc_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Ivc_A()), pnd_Wk_Input_Pnd_L_Ivc_A.val()); //Natural: ASSIGN #INP-SUMMARY.#L-IVC-A := VAL ( #WK-INPUT.#L-IVC-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Int_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Int_A()), pnd_Wk_Input_Pnd_L_Int_A.val()); //Natural: ASSIGN #INP-SUMMARY.#L-INT-A := VAL ( #WK-INPUT.#L-INT-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Fed_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Fed_A()), pnd_Wk_Input_Pnd_L_Fed_A.val()); //Natural: ASSIGN #INP-SUMMARY.#L-FED-A := VAL ( #WK-INPUT.#L-FED-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Nra_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Nra_A()), pnd_Wk_Input_Pnd_L_Nra_A.val()); //Natural: ASSIGN #INP-SUMMARY.#L-NRA-A := VAL ( #WK-INPUT.#L-NRA-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_State_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_State_A()), pnd_Wk_Input_Pnd_L_State_A.val()); //Natural: ASSIGN #INP-SUMMARY.#L-STATE-A := VAL ( #WK-INPUT.#L-STATE-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Local_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Local_A()), pnd_Wk_Input_Pnd_L_Local_A.val()); //Natural: ASSIGN #INP-SUMMARY.#L-LOCAL-A := VAL ( #WK-INPUT.#L-LOCAL-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Canadian_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Canadian_A()), pnd_Wk_Input_Pnd_L_Canadian_A.val()); //Natural: ASSIGN #INP-SUMMARY.#L-CANADIAN-A := VAL ( #WK-INPUT.#L-CANADIAN-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_From_Dte().setValue(pnd_Wk_Input_Pnd_From_Dte);                                                                                //Natural: MOVE #WK-INPUT.#FROM-DTE TO #INP-SUMMARY.#FROM-DTE
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_To_Dte().setValue(pnd_Wk_Input_Pnd_To_Dte);                                                                                    //Natural: MOVE #WK-INPUT.#TO-DTE TO #INP-SUMMARY.#TO-DTE
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_Tx_Year().setValue(pnd_Wk_Input_Pnd_Tx_Year);                                                                                  //Natural: MOVE #WK-INPUT.#TX-YEAR TO #INP-SUMMARY.#TX-YEAR
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_Interface_Dte().setValue(pnd_Wk_Input_Pnd_Interface_Dte);                                                                      //Natural: MOVE #WK-INPUT.#INTERFACE-DTE TO #INP-SUMMARY.#INTERFACE-DTE
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Comp_Code().setValue(pnd_Wk_Input_Pnd_S_Comp_Code);                                                                          //Natural: MOVE #WK-INPUT.#S-COMP-CODE TO #INP-SUMMARY.#S-COMP-CODE
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Rec_Cnt().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Rec_Cnt()), pnd_Wk_Input_Pnd_S_Rec_Cnt.val()); //Natural: ASSIGN #INP-SUMMARY.#S-REC-CNT := VAL ( #WK-INPUT.#S-REC-CNT )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Gross_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Gross_A()), pnd_Wk_Input_Pnd_S_Gross_A.val()); //Natural: ASSIGN #INP-SUMMARY.#S-GROSS-A := VAL ( #WK-INPUT.#S-GROSS-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Ivc_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Ivc_A()), pnd_Wk_Input_Pnd_S_Ivc_A.val()); //Natural: ASSIGN #INP-SUMMARY.#S-IVC-A := VAL ( #WK-INPUT.#S-IVC-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Int_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Int_A()), pnd_Wk_Input_Pnd_S_Int_A.val()); //Natural: ASSIGN #INP-SUMMARY.#S-INT-A := VAL ( #WK-INPUT.#S-INT-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Fed_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Fed_A()), pnd_Wk_Input_Pnd_S_Fed_A.val()); //Natural: ASSIGN #INP-SUMMARY.#S-FED-A := VAL ( #WK-INPUT.#S-FED-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Nra_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Nra_A()), pnd_Wk_Input_Pnd_S_Nra_A.val()); //Natural: ASSIGN #INP-SUMMARY.#S-NRA-A := VAL ( #WK-INPUT.#S-NRA-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_State_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_State_A()), pnd_Wk_Input_Pnd_S_State_A.val()); //Natural: ASSIGN #INP-SUMMARY.#S-STATE-A := VAL ( #WK-INPUT.#S-STATE-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Local_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Local_A()), pnd_Wk_Input_Pnd_S_Local_A.val()); //Natural: ASSIGN #INP-SUMMARY.#S-LOCAL-A := VAL ( #WK-INPUT.#S-LOCAL-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Canadian_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Canadian_A()), pnd_Wk_Input_Pnd_S_Canadian_A.val()); //Natural: ASSIGN #INP-SUMMARY.#S-CANADIAN-A := VAL ( #WK-INPUT.#S-CANADIAN-A )
        //*                                                              /* RCC
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Comp_Code().setValue(pnd_Wk_Input_Pnd_F_Comp_Code);                                                                          //Natural: MOVE #WK-INPUT.#F-COMP-CODE TO #INP-SUMMARY.#F-COMP-CODE
        pnd_Wk_Input_Pnd_F_Rec_Cnt.setValue(0);                                                                                                                           //Natural: MOVE 0 TO #WK-INPUT.#F-REC-CNT
        pnd_Wk_Input_Pnd_F_Gross_A.setValue(0);                                                                                                                           //Natural: MOVE 0 TO #WK-INPUT.#F-GROSS-A
        pnd_Wk_Input_Pnd_F_Ivc_A.setValue(0);                                                                                                                             //Natural: MOVE 0 TO #WK-INPUT.#F-IVC-A
        pnd_Wk_Input_Pnd_F_Int_A.setValue(0);                                                                                                                             //Natural: MOVE 0 TO #WK-INPUT.#F-INT-A
        pnd_Wk_Input_Pnd_F_Fed_A.setValue(0);                                                                                                                             //Natural: MOVE 0 TO #WK-INPUT.#F-FED-A
        pnd_Wk_Input_Pnd_F_Nra_A.setValue(0);                                                                                                                             //Natural: MOVE 0 TO #WK-INPUT.#F-NRA-A
        pnd_Wk_Input_Pnd_F_State_A.setValue(0);                                                                                                                           //Natural: MOVE 0 TO #WK-INPUT.#F-STATE-A
        pnd_Wk_Input_Pnd_F_Local_A.setValue(0);                                                                                                                           //Natural: MOVE 0 TO #WK-INPUT.#F-LOCAL-A
        pnd_Wk_Input_Pnd_F_Canadian_A.setValue(0);                                                                                                                        //Natural: MOVE 0 TO #WK-INPUT.#F-CANADIAN-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Rec_Cnt().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Rec_Cnt()), pnd_Wk_Input_Pnd_F_Rec_Cnt.val()); //Natural: ASSIGN #INP-SUMMARY.#F-REC-CNT := VAL ( #WK-INPUT.#F-REC-CNT )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Gross_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Gross_A()), pnd_Wk_Input_Pnd_F_Gross_A.val()); //Natural: ASSIGN #INP-SUMMARY.#F-GROSS-A := VAL ( #WK-INPUT.#F-GROSS-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Ivc_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Ivc_A()), pnd_Wk_Input_Pnd_F_Ivc_A.val()); //Natural: ASSIGN #INP-SUMMARY.#F-IVC-A := VAL ( #WK-INPUT.#F-IVC-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Int_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Int_A()), pnd_Wk_Input_Pnd_F_Int_A.val()); //Natural: ASSIGN #INP-SUMMARY.#F-INT-A := VAL ( #WK-INPUT.#F-INT-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Fed_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Fed_A()), pnd_Wk_Input_Pnd_F_Fed_A.val()); //Natural: ASSIGN #INP-SUMMARY.#F-FED-A := VAL ( #WK-INPUT.#F-FED-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Nra_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Nra_A()), pnd_Wk_Input_Pnd_F_Nra_A.val()); //Natural: ASSIGN #INP-SUMMARY.#F-NRA-A := VAL ( #WK-INPUT.#F-NRA-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_State_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_State_A()), pnd_Wk_Input_Pnd_F_State_A.val()); //Natural: ASSIGN #INP-SUMMARY.#F-STATE-A := VAL ( #WK-INPUT.#F-STATE-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Local_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Local_A()), pnd_Wk_Input_Pnd_F_Local_A.val()); //Natural: ASSIGN #INP-SUMMARY.#F-LOCAL-A := VAL ( #WK-INPUT.#F-LOCAL-A )
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Canadian_A().compute(new ComputeParameters(false, ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Canadian_A()), pnd_Wk_Input_Pnd_F_Canadian_A.val()); //Natural: ASSIGN #INP-SUMMARY.#F-CANADIAN-A := VAL ( #WK-INPUT.#F-CANADIAN-A )
        //*  MOVE #WK-INPUT.#FILLER-1 TO #INP-SUMMARY.#FILLER-1          /*
        //* **
        getWorkFiles().write(2, false, ldaTwrl0710.getPnd_Inp_Summary());                                                                                                 //Natural: WRITE WORK FILE 2 #INP-SUMMARY
    }

    //
}
