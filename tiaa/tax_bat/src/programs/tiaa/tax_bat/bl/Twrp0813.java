/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:31:30 PM
**        * FROM NATURAL PROGRAM : Twrp0813
************************************************************
**        * FILE NAME            : Twrp0813.java
**        * CLASS NAME           : Twrp0813
**        * INSTANCE NAME        : Twrp0813
************************************************************
***********************************************************************
*
* PROGRAM  : TWRP0813
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : UPDATE TAX TRANSACTION FILE CITED RECORDS.
* CREATED  : 12 / 23 / 1998.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM UPDATES THE CITATION INDICATOR ON THE TAX
*            TRANSACTION FILE.  RECORDS CITED HAVE DUPLICATE PAYMENT
*            DATES, AND DIFFERENT CITIZENSHIP, RESIDENCY, OR TAX FORMS
*            (1001, & 1078).
*
* 01/13/12 - RCC - REPLACED TWRL810A WITH TWRL0702 FOR SUNY/CUNY
* 05/08/08 - RM  - ROTH 401K/403B PROJECT
*                  RECOMPILED DUE TO UPDATED TWRL810A & TWRL813A
* 04/24/17 - J BREMER PIN EXPANSION - STOW FOR TWRL0702
***********************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0813 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0702 ldaTwrl0702;
    private LdaTwrl813a ldaTwrl813a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Cited_Ctr;
    private DbsField pnd_Write_Ctr;
    private DbsField pnd_Citation_Message_Code;
    private DbsField pnd_End_Of_Cited_File;
    private DbsField z;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0702 = new LdaTwrl0702();
        registerRecord(ldaTwrl0702);
        ldaTwrl813a = new LdaTwrl813a();
        registerRecord(ldaTwrl813a);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Cited_Ctr = localVariables.newFieldInRecord("pnd_Cited_Ctr", "#CITED-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Write_Ctr = localVariables.newFieldInRecord("pnd_Write_Ctr", "#WRITE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Citation_Message_Code = localVariables.newFieldInRecord("pnd_Citation_Message_Code", "#CITATION-MESSAGE-CODE", FieldType.NUMERIC, 2);
        pnd_End_Of_Cited_File = localVariables.newFieldInRecord("pnd_End_Of_Cited_File", "#END-OF-CITED-FILE", FieldType.BOOLEAN, 1);
        z = localVariables.newFieldInRecord("z", "Z", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0702.initializeValues();
        ldaTwrl813a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0813() throws Exception
    {
        super("Twrp0813");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP0813", onError);
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 02 ) PS = 60 LS = 133 ZP = ON
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* *============**
        //*  MAIN PROGRAM *
        //* *============**
        pnd_End_Of_Cited_File.setValue(false);                                                                                                                            //Natural: ASSIGN #END-OF-CITED-FILE := FALSE
                                                                                                                                                                          //Natural: PERFORM READ-CITED-FILE
        sub_Read_Cited_File();
        if (condition(Global.isEscape())) {return;}
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 RECORD TWRT-RECORD
        while (condition(getWorkFiles().read(1, ldaTwrl0702.getTwrt_Record())))
        {
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            if (condition(pnd_End_Of_Cited_File.equals(true) || ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id().less(ldaTwrl813a.getCited_Record_Twrt_Tax_Id())                  //Natural: IF #END-OF-CITED-FILE = TRUE OR TWRT-RECORD.TWRT-TAX-ID < CITED-RECORD.TWRT-TAX-ID OR ( TWRT-RECORD.TWRT-TAX-ID = CITED-RECORD.TWRT-TAX-ID AND TWRT-RECORD.TWRT-PAYMT-DTE-ALPHA < CITED-RECORD.TWRT-PAYMT-DTE ) OR ( TWRT-RECORD.TWRT-TAX-ID = CITED-RECORD.TWRT-TAX-ID AND TWRT-RECORD.TWRT-PAYMT-DTE-ALPHA = CITED-RECORD.TWRT-PAYMT-DTE AND TWRT-RECORD.TWRT-SOURCE NE CITED-RECORD.TWRT-SOURCE )
                || (ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id().equals(ldaTwrl813a.getCited_Record_Twrt_Tax_Id()) && ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha().less(ldaTwrl813a.getCited_Record_Twrt_Paymt_Dte())) 
                || (ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id().equals(ldaTwrl813a.getCited_Record_Twrt_Tax_Id()) && ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha().equals(ldaTwrl813a.getCited_Record_Twrt_Paymt_Dte()) 
                && ldaTwrl0702.getTwrt_Record_Twrt_Source().notEquals(ldaTwrl813a.getCited_Record_Twrt_Source()))))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id().equals(ldaTwrl813a.getCited_Record_Twrt_Tax_Id()) && ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha().equals(ldaTwrl813a.getCited_Record_Twrt_Paymt_Dte()))) //Natural: IF TWRT-RECORD.TWRT-TAX-ID = CITED-RECORD.TWRT-TAX-ID AND TWRT-RECORD.TWRT-PAYMT-DTE-ALPHA = CITED-RECORD.TWRT-PAYMT-DTE
                {
                    //*    AND TWRT-RECORD.TWRT-SOURCE          =  CITED-RECORD.TWRT-SOURCE
                    if (condition(ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals(ldaTwrl813a.getCited_Record_Twrt_State_Rsdncy()) && ldaTwrl0702.getTwrt_Record_Twrt_Citizenship().equals(ldaTwrl813a.getCited_Record_Twrt_Citizenship())  //Natural: IF TWRT-RECORD.TWRT-STATE-RSDNCY = CITED-RECORD.TWRT-STATE-RSDNCY AND TWRT-RECORD.TWRT-CITIZENSHIP = CITED-RECORD.TWRT-CITIZENSHIP AND TWRT-RECORD.TWRT-PYMNT-TAX-FORM-1001 = CITED-RECORD.TWRT-PYMNT-TAX-FORM-1001 AND TWRT-RECORD.TWRT-PYMNT-TAX-FORM-1078 = CITED-RECORD.TWRT-PYMNT-TAX-FORM-1078 AND TWRT-RECORD.TWRT-LOCALITY-CDE = CITED-RECORD.TWRT-LOCALITY-CDE
                        && ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001().equals(ldaTwrl813a.getCited_Record_Twrt_Pymnt_Tax_Form_1001()) && ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().equals(ldaTwrl813a.getCited_Record_Twrt_Pymnt_Tax_Form_1078()) 
                        && ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde().equals(ldaTwrl813a.getCited_Record_Twrt_Locality_Cde())))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaTwrl0702.getTwrt_Record_Twrt_Flag().setValue("C");                                                                                             //Natural: ASSIGN TWRT-RECORD.TWRT-FLAG := 'C'
                        pnd_Cited_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #CITED-CTR
                        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().notEquals(ldaTwrl813a.getCited_Record_Twrt_State_Rsdncy())))                         //Natural: IF TWRT-RECORD.TWRT-STATE-RSDNCY NE CITED-RECORD.TWRT-STATE-RSDNCY
                        {
                            pnd_Citation_Message_Code.setValue(52);                                                                                                       //Natural: ASSIGN #CITATION-MESSAGE-CODE := 52
                                                                                                                                                                          //Natural: PERFORM UPDATE-CITATION-TABLE
                            sub_Update_Citation_Table();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Citizenship().notEquals(ldaTwrl813a.getCited_Record_Twrt_Citizenship())))                           //Natural: IF TWRT-RECORD.TWRT-CITIZENSHIP NE CITED-RECORD.TWRT-CITIZENSHIP
                        {
                            pnd_Citation_Message_Code.setValue(62);                                                                                                       //Natural: ASSIGN #CITATION-MESSAGE-CODE := 62
                                                                                                                                                                          //Natural: PERFORM UPDATE-CITATION-TABLE
                            sub_Update_Citation_Table();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001().notEquals(ldaTwrl813a.getCited_Record_Twrt_Pymnt_Tax_Form_1001())))           //Natural: IF TWRT-RECORD.TWRT-PYMNT-TAX-FORM-1001 NE CITED-RECORD.TWRT-PYMNT-TAX-FORM-1001
                        {
                            pnd_Citation_Message_Code.setValue(63);                                                                                                       //Natural: ASSIGN #CITATION-MESSAGE-CODE := 63
                                                                                                                                                                          //Natural: PERFORM UPDATE-CITATION-TABLE
                            sub_Update_Citation_Table();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().notEquals(ldaTwrl813a.getCited_Record_Twrt_Pymnt_Tax_Form_1078())))           //Natural: IF TWRT-RECORD.TWRT-PYMNT-TAX-FORM-1078 NE CITED-RECORD.TWRT-PYMNT-TAX-FORM-1078
                        {
                            pnd_Citation_Message_Code.setValue(64);                                                                                                       //Natural: ASSIGN #CITATION-MESSAGE-CODE := 64
                                                                                                                                                                          //Natural: PERFORM UPDATE-CITATION-TABLE
                            sub_Update_Citation_Table();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde().notEquals(ldaTwrl813a.getCited_Record_Twrt_Locality_Cde())))                         //Natural: IF TWRT-RECORD.TWRT-LOCALITY-CDE NE CITED-RECORD.TWRT-LOCALITY-CDE
                        {
                            pnd_Citation_Message_Code.setValue(46);                                                                                                       //Natural: ASSIGN #CITATION-MESSAGE-CODE := 46
                                                                                                                                                                          //Natural: PERFORM UPDATE-CITATION-TABLE
                            sub_Update_Citation_Table();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM REPORT-DUPLICATE-DATE
                        sub_Report_Duplicate_Date();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM READ-CITED-FILE
                    sub_Read_Cited_File();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(3, false, ldaTwrl0702.getTwrt_Record());                                                                                                 //Natural: WRITE WORK FILE 03 TWRT-RECORD
            pnd_Write_Ctr.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #WRITE-CTR
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* *------
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CITED-FILE
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CITATION-TABLE
        //* *--------------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-DUPLICATE-DATE
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-PROGRAM-PROCESSING
        //* *------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 49T 'Participants Duplicate Date Report' 120T 'REPORT: RPT1' //
        //* *-------------------
        //* *-------                                                                                                                                                      //Natural: ON ERROR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
    }
    private void sub_Read_Cited_File() throws Exception                                                                                                                   //Natural: READ-CITED-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------
        getWorkFiles().read(2, ldaTwrl813a.getCited_Record());                                                                                                            //Natural: READ WORK FILE 02 ONCE RECORD CITED-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            pnd_End_Of_Cited_File.setValue(true);                                                                                                                         //Natural: ASSIGN #END-OF-CITED-FILE := TRUE
        }                                                                                                                                                                 //Natural: END-ENDFILE
    }
    private void sub_Update_Citation_Table() throws Exception                                                                                                             //Natural: UPDATE-CITATION-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR Z = 1 TO 60
        for (z.setValue(1); condition(z.lessOrEqual(60)); z.nadd(1))
        {
            if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Error_Table().getValue(z).equals(getZero())))                                                                   //Natural: IF TWRT-RECORD.TWRT-ERROR-TABLE ( Z ) = 0
            {
                ldaTwrl0702.getTwrt_Record_Twrt_Error_Table().getValue(z).setValue(pnd_Citation_Message_Code);                                                            //Natural: ASSIGN TWRT-RECORD.TWRT-ERROR-TABLE ( Z ) := #CITATION-MESSAGE-CODE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Report_Duplicate_Date() throws Exception                                                                                                             //Natural: REPORT-DUPLICATE-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------
        getReports().display(1, "T/C",                                                                                                                                    //Natural: DISPLAY ( 01 ) 'T/C' TWRT-RECORD.TWRT-COMPANY-CDE 'SR/CE' TWRT-RECORD.TWRT-SOURCE 'Contract' TWRT-RECORD.TWRT-CNTRCT-NBR 'PY/EE' TWRT-RECORD.TWRT-PAYEE-CDE 'Tax ID' TWRT-RECORD.TWRT-TAX-ID 'T/Y' TWRT-RECORD.TWRT-TAX-ID-TYPE 'Pin ID' TWRT-RECORD.TWRT-PIN-ID 'Name' TWRT-RECORD.TWRT-NA-LINE ( 1 ) 'Payment' TWRT-RECORD.TWRT-PAYMT-DTE-ALPHA ( EM = XXXX/XX/XX ) 'P/Y' TWRT-RECORD.TWRT-PAYMT-TYPE 'S/E' TWRT-RECORD.TWRT-SETTL-TYPE 'RS/DN' TWRT-RECORD.TWRT-STATE-RSDNCY 'Loc' TWRT-RECORD.TWRT-LOCALITY-CDE 'CI/TI' TWRT-RECORD.TWRT-CITIZENSHIP 'Tax/Year' TWRT-RECORD.TWRT-TAX-YEAR 'Ro/ll' TWRT-RECORD.TWRT-ROLLOVER-IND 'Ci/ta' TWRT-RECORD.TWRT-FLAG 'D-O-B' TWRT-RECORD.TWRT-DOB-A ( EM = XXXX/XX/XX )
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"));
        if (Global.isEscape()) return;
        //*        'D-O-D'     TWRT-RECORD.TWRT-DOD    (EM=XXXX/XX/XX)
        getReports().print(1, "Citation Reason Codes ...",ldaTwrl0702.getTwrt_Record_Twrt_Error_Table().getValue(1,":",30), new ReportEditMask ("ZZ"));                   //Natural: PRINT ( 01 ) 'Citation Reason Codes ...' TWRT-RECORD.TWRT-ERROR-TABLE ( 1:30 ) ( EM = ZZ )
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, new TabSetting(1),"Tax Transaction Records Read...............",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                            //Natural: WRITE ( 00 ) 01T 'Tax Transaction Records Read...............' #READ-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Tax Transaction Records Cited..............",pnd_Cited_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                   //Natural: WRITE ( 00 ) / 01T 'Tax Transaction Records Cited..............' #CITED-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Tax Transaction Records Written............",pnd_Write_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                   //Natural: WRITE ( 00 ) / 01T 'Tax Transaction Records Written............' #WRITE-CTR
        if (Global.isEscape()) return;
        getReports().skip(1, 3);                                                                                                                                          //Natural: SKIP ( 01 ) 3
        getReports().write(1, NEWLINE,new TabSetting(1),"Tax Transaction Records Cited..............",pnd_Cited_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                   //Natural: WRITE ( 01 ) / 01T 'Tax Transaction Records Cited..............' #CITED-CTR
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
        sub_Error_Display_Start();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
        sub_Error_Display_End();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133 ZP=ON");
        Global.format(1, "PS=60 LS=133 ZP=ON");
        Global.format(2, "PS=60 LS=133 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(49),"Participants Duplicate Date Report",new TabSetting(120),"REPORT: RPT1",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, "T/C",
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line(),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"));
    }
}
