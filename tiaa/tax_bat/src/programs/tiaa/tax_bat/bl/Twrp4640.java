/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:40:05 PM
**        * FROM NATURAL PROGRAM : Twrp4640
************************************************************
**        * FILE NAME            : Twrp4640.java
**        * CLASS NAME           : Twrp4640
**        * INSTANCE NAME        : Twrp4640
************************************************************
***********************************************************************
*
* PROGRAM  : TWRP4640
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : EDIT / UPDATE THE PARTICIPANT FILE.
* CREATED  : 07 / 15 / 1999.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM EDITS & UPDATES THE PARTICIPANT DATA BASE FILE
*            USING THE IRA CONTRIBUTION FEED EXTRACT.
* HISTORY.....:
*    04/18/05   MS - NEW PROVINCE CODE 88 - NU
*    11/21/05   BK - SYNCHRONIZED LOCAL TWRL464E TO PROD
*    04/09/15   FENDAYA COR/NAS SUNSET. FE201504
*    11/10/16   MUKHERR  - ADDED ROLL-UP FIELDS IN LDA
*                          TWRL464B
*                          TWRL464C
*    12/17/16   KISHORE - INITIALISED NEW ROLLUP FIELDS TO
*                        FIX COR/NAAD ISSUE TAG: KISHORE
*    04/03/17   SINHASN - ADD LOGIC FOR ZERO VALUE CHECKING
*                        FOR DATE OF DEATH  TAG: SINHASN
*
* 04/27/2017  WEBBJ - PIN EXPANSION SCAN JW0427
*                     ALSO REPLCE MDMN101A/#MDMA101-> MDMN100A/#MDMA100
* 11/24/2020 - PALDE - RESTOW FOR IRS REPORTING 2020 - 5498 CHANGES
***********************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp4640 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private LdaTwrl464a ldaTwrl464a;
    private LdaTwrl464b ldaTwrl464b;
    private LdaTwrl464c ldaTwrl464c;
    private LdaTwrl464d ldaTwrl464d;
    private LdaTwrl464e ldaTwrl464e;
    private PdaMdma101 pdaMdma101;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_oldpar;
    private DbsField oldpar_Twrparti_Citation_Ind;
    private DbsField oldpar_Twrparti_Part_Eff_Start_Dte;
    private DbsField oldpar_Twrparti_Part_Eff_End_Dte;
    private DbsField oldpar_Twrparti_Part_Invrse_End_Dte;

    private DataAccessProgramView vw_country;
    private DbsField country_Tircntl_Nbr_Year_Alpha_Cde;

    private DbsGroup country__R_Field_1;
    private DbsField country_Country_Tbl_Nbr;
    private DbsField country_Country_Tax_Year;
    private DbsField country_Country_Alpha_Code;
    private DbsField pnd_S_Twrparti_Curr_Rec;

    private DbsGroup pnd_S_Twrparti_Curr_Rec__R_Field_2;
    private DbsField pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Tax_Id;
    private DbsField pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Status;
    private DbsField pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Eff_End_Dte;
    private DbsField pnd_Source_Code;
    private DbsField pnd_Prev_Source;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Reject_Ctr;
    private DbsField pnd_Bypass_Ctr;
    private DbsField pnd_Duplicate_Ctr;
    private DbsField pnd_Retroactive_Ctr;
    private DbsField pnd_New_Store_Ctr;
    private DbsField pnd_Old_Store_Ctr;
    private DbsField pnd_Update_Ctr;
    private DbsField pnd_Oc_Residency_Ctr;
    private DbsField pnd_Conv_Resdency_Ctr;
    private DbsField pnd_Feed_Address_Ctr;
    private DbsField pnd_Country_Found_Ctr;
    private DbsField pnd_Country_Not_Found;
    private DbsField pnd_Default_New_Ctr;
    private DbsField pnd_Default_File_Ctr;
    private DbsField pnd_Twrparti_Residency_Type;
    private DbsField pnd_Twrparti_Frm1078_Eff_Dte;
    private DbsField pnd_Twrparti_Frm1001_Eff_Dte;
    private DbsField pnd_Twrparti_Residency_Code;
    private DbsField pnd_Twrparti_Country_Desc;
    private DbsField pnd_Upd_Part_Eff_End_Dte;

    private DbsGroup pnd_Upd_Part_Eff_End_Dte__R_Field_3;
    private DbsField pnd_Upd_Part_Eff_End_Dte_Pnd_Upd_Part_Eff_End_Dte_N;
    private DbsField pnd_Addrss_Geographic_Cde;
    private DbsField pnd_Citation_Message_Code;
    private DbsField pnd_Country_3_Yr_Alpha_Code;

    private DbsGroup pnd_Country_3_Yr_Alpha_Code__R_Field_4;
    private DbsField pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Tbl_Nbr;
    private DbsField pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Tax_Year;
    private DbsField pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Alpha_Code;
    private DbsField pnd_Exp_1001_Ccyymmdd_A;

    private DbsGroup pnd_Exp_1001_Ccyymmdd_A__R_Field_5;
    private DbsField pnd_Exp_1001_Ccyymmdd_A_Pnd_Exp_1001_Ccyymmdd_N;

    private DbsGroup pnd_Exp_1001_Ccyymmdd_A__R_Field_6;
    private DbsField pnd_Exp_1001_Ccyymmdd_A_Pnd_Exp_1001_Ccyy;
    private DbsField pnd_Exp_1001_Ccyymmdd_A_Pnd_Exp_1001_Mmdd;
    private DbsField pnd_Country_Init;

    private DbsGroup pnd_Country_Init__R_Field_7;
    private DbsField pnd_Country_Init_Pnd_Country;
    private DbsField pnd_Participant_Found;
    private DbsField pnd_Store_Participant;
    private DbsField pnd_Start_Date_Found;
    private DbsField pnd_Retroactive_Found;
    private DbsField pnd_Country_Found;
    private DbsField pnd_Naa_Found;
    private DbsField pnd_Residency_Found;
    private DbsField pnd_Multi_Country;
    private DbsField pnd_Locality_Found;
    private DbsField pnd_Start_Date_Cite;
    private DbsField pnd_Timn_N;

    private DbsGroup pnd_Timn_N__R_Field_8;
    private DbsField pnd_Timn_N_Pnd_Timn_A;
    private DbsField pnd_Et_Ctr;
    private DbsField pnd_Date_D;
    private DbsField pnd_Part_Isn;
    private DbsField w;
    private DbsField x;
    private DbsField y;
    private DbsField z;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Temp_Line;
    private DbsField pnd_Ws_Pnd_Temp_Zip;
    private DbsField pnd_Rc;
    private DbsField pnd_I;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        ldaTwrl464a = new LdaTwrl464a();
        registerRecord(ldaTwrl464a);
        ldaTwrl464b = new LdaTwrl464b();
        registerRecord(ldaTwrl464b);
        registerRecord(ldaTwrl464b.getVw_par());
        ldaTwrl464c = new LdaTwrl464c();
        registerRecord(ldaTwrl464c);
        registerRecord(ldaTwrl464c.getVw_newpar());
        ldaTwrl464d = new LdaTwrl464d();
        registerRecord(ldaTwrl464d);
        registerRecord(ldaTwrl464d.getVw_updpar());
        ldaTwrl464e = new LdaTwrl464e();
        registerRecord(ldaTwrl464e);
        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);

        // Local Variables

        vw_oldpar = new DataAccessProgramView(new NameInfo("vw_oldpar", "OLDPAR"), "TWRPARTI_PARTICIPANT_FILE", "TWR_PARTICIPANT");
        oldpar_Twrparti_Citation_Ind = vw_oldpar.getRecord().newFieldInGroup("oldpar_Twrparti_Citation_Ind", "TWRPARTI-CITATION-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPARTI_CITATION_IND");
        oldpar_Twrparti_Part_Eff_Start_Dte = vw_oldpar.getRecord().newFieldInGroup("oldpar_Twrparti_Part_Eff_Start_Dte", "TWRPARTI-PART-EFF-START-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRPARTI_PART_EFF_START_DTE");
        oldpar_Twrparti_Part_Eff_End_Dte = vw_oldpar.getRecord().newFieldInGroup("oldpar_Twrparti_Part_Eff_End_Dte", "TWRPARTI-PART-EFF-END-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_PART_EFF_END_DTE");
        oldpar_Twrparti_Part_Invrse_End_Dte = vw_oldpar.getRecord().newFieldInGroup("oldpar_Twrparti_Part_Invrse_End_Dte", "TWRPARTI-PART-INVRSE-END-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TWRPARTI_PART_INVRSE_END_DTE");
        registerRecord(vw_oldpar);

        vw_country = new DataAccessProgramView(new NameInfo("vw_country", "COUNTRY"), "TIRCNTL_COUNTRY_CODE_TBL_VIEW", "TIR_CONTROL");
        country_Tircntl_Nbr_Year_Alpha_Cde = vw_country.getRecord().newFieldInGroup("country_Tircntl_Nbr_Year_Alpha_Cde", "TIRCNTL-NBR-YEAR-ALPHA-CDE", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "TIRCNTL_NBR_YEAR_ALPHA_CDE");
        country_Tircntl_Nbr_Year_Alpha_Cde.setSuperDescriptor(true);

        country__R_Field_1 = vw_country.getRecord().newGroupInGroup("country__R_Field_1", "REDEFINE", country_Tircntl_Nbr_Year_Alpha_Cde);
        country_Country_Tbl_Nbr = country__R_Field_1.newFieldInGroup("country_Country_Tbl_Nbr", "COUNTRY-TBL-NBR", FieldType.STRING, 1);
        country_Country_Tax_Year = country__R_Field_1.newFieldInGroup("country_Country_Tax_Year", "COUNTRY-TAX-YEAR", FieldType.STRING, 4);
        country_Country_Alpha_Code = country__R_Field_1.newFieldInGroup("country_Country_Alpha_Code", "COUNTRY-ALPHA-CODE", FieldType.STRING, 2);
        registerRecord(vw_country);

        pnd_S_Twrparti_Curr_Rec = localVariables.newFieldInRecord("pnd_S_Twrparti_Curr_Rec", "#S-TWRPARTI-CURR-REC", FieldType.STRING, 19);

        pnd_S_Twrparti_Curr_Rec__R_Field_2 = localVariables.newGroupInRecord("pnd_S_Twrparti_Curr_Rec__R_Field_2", "REDEFINE", pnd_S_Twrparti_Curr_Rec);
        pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Tax_Id = pnd_S_Twrparti_Curr_Rec__R_Field_2.newFieldInGroup("pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Tax_Id", 
            "#S-TWRPARTI-TAX-ID", FieldType.STRING, 10);
        pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Status = pnd_S_Twrparti_Curr_Rec__R_Field_2.newFieldInGroup("pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Status", 
            "#S-TWRPARTI-STATUS", FieldType.STRING, 1);
        pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Eff_End_Dte = pnd_S_Twrparti_Curr_Rec__R_Field_2.newFieldInGroup("pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Eff_End_Dte", 
            "#S-TWRPARTI-EFF-END-DTE", FieldType.STRING, 8);
        pnd_Source_Code = localVariables.newFieldInRecord("pnd_Source_Code", "#SOURCE-CODE", FieldType.STRING, 2);
        pnd_Prev_Source = localVariables.newFieldInRecord("pnd_Prev_Source", "#PREV-SOURCE", FieldType.STRING, 2);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Reject_Ctr = localVariables.newFieldInRecord("pnd_Reject_Ctr", "#REJECT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Bypass_Ctr = localVariables.newFieldInRecord("pnd_Bypass_Ctr", "#BYPASS-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Duplicate_Ctr = localVariables.newFieldInRecord("pnd_Duplicate_Ctr", "#DUPLICATE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Retroactive_Ctr = localVariables.newFieldInRecord("pnd_Retroactive_Ctr", "#RETROACTIVE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_New_Store_Ctr = localVariables.newFieldInRecord("pnd_New_Store_Ctr", "#NEW-STORE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Old_Store_Ctr = localVariables.newFieldInRecord("pnd_Old_Store_Ctr", "#OLD-STORE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Update_Ctr = localVariables.newFieldInRecord("pnd_Update_Ctr", "#UPDATE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Oc_Residency_Ctr = localVariables.newFieldInRecord("pnd_Oc_Residency_Ctr", "#OC-RESIDENCY-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Conv_Resdency_Ctr = localVariables.newFieldInRecord("pnd_Conv_Resdency_Ctr", "#CONV-RESDENCY-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Feed_Address_Ctr = localVariables.newFieldInRecord("pnd_Feed_Address_Ctr", "#FEED-ADDRESS-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Country_Found_Ctr = localVariables.newFieldInRecord("pnd_Country_Found_Ctr", "#COUNTRY-FOUND-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Country_Not_Found = localVariables.newFieldInRecord("pnd_Country_Not_Found", "#COUNTRY-NOT-FOUND", FieldType.PACKED_DECIMAL, 7);
        pnd_Default_New_Ctr = localVariables.newFieldInRecord("pnd_Default_New_Ctr", "#DEFAULT-NEW-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Default_File_Ctr = localVariables.newFieldInRecord("pnd_Default_File_Ctr", "#DEFAULT-FILE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Twrparti_Residency_Type = localVariables.newFieldInRecord("pnd_Twrparti_Residency_Type", "#TWRPARTI-RESIDENCY-TYPE", FieldType.STRING, 1);
        pnd_Twrparti_Frm1078_Eff_Dte = localVariables.newFieldInRecord("pnd_Twrparti_Frm1078_Eff_Dte", "#TWRPARTI-FRM1078-EFF-DTE", FieldType.STRING, 
            8);
        pnd_Twrparti_Frm1001_Eff_Dte = localVariables.newFieldInRecord("pnd_Twrparti_Frm1001_Eff_Dte", "#TWRPARTI-FRM1001-EFF-DTE", FieldType.STRING, 
            8);
        pnd_Twrparti_Residency_Code = localVariables.newFieldInRecord("pnd_Twrparti_Residency_Code", "#TWRPARTI-RESIDENCY-CODE", FieldType.STRING, 2);
        pnd_Twrparti_Country_Desc = localVariables.newFieldInRecord("pnd_Twrparti_Country_Desc", "#TWRPARTI-COUNTRY-DESC", FieldType.STRING, 35);
        pnd_Upd_Part_Eff_End_Dte = localVariables.newFieldInRecord("pnd_Upd_Part_Eff_End_Dte", "#UPD-PART-EFF-END-DTE", FieldType.STRING, 8);

        pnd_Upd_Part_Eff_End_Dte__R_Field_3 = localVariables.newGroupInRecord("pnd_Upd_Part_Eff_End_Dte__R_Field_3", "REDEFINE", pnd_Upd_Part_Eff_End_Dte);
        pnd_Upd_Part_Eff_End_Dte_Pnd_Upd_Part_Eff_End_Dte_N = pnd_Upd_Part_Eff_End_Dte__R_Field_3.newFieldInGroup("pnd_Upd_Part_Eff_End_Dte_Pnd_Upd_Part_Eff_End_Dte_N", 
            "#UPD-PART-EFF-END-DTE-N", FieldType.NUMERIC, 8);
        pnd_Addrss_Geographic_Cde = localVariables.newFieldInRecord("pnd_Addrss_Geographic_Cde", "#ADDRSS-GEOGRAPHIC-CDE", FieldType.STRING, 2);
        pnd_Citation_Message_Code = localVariables.newFieldInRecord("pnd_Citation_Message_Code", "#CITATION-MESSAGE-CODE", FieldType.NUMERIC, 2);
        pnd_Country_3_Yr_Alpha_Code = localVariables.newFieldInRecord("pnd_Country_3_Yr_Alpha_Code", "#COUNTRY-3-YR-ALPHA-CODE", FieldType.STRING, 7);

        pnd_Country_3_Yr_Alpha_Code__R_Field_4 = localVariables.newGroupInRecord("pnd_Country_3_Yr_Alpha_Code__R_Field_4", "REDEFINE", pnd_Country_3_Yr_Alpha_Code);
        pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Tbl_Nbr = pnd_Country_3_Yr_Alpha_Code__R_Field_4.newFieldInGroup("pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Tbl_Nbr", 
            "#COUNTRY-TBL-NBR", FieldType.STRING, 1);
        pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Tax_Year = pnd_Country_3_Yr_Alpha_Code__R_Field_4.newFieldInGroup("pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Tax_Year", 
            "#COUNTRY-TAX-YEAR", FieldType.STRING, 4);
        pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Alpha_Code = pnd_Country_3_Yr_Alpha_Code__R_Field_4.newFieldInGroup("pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Alpha_Code", 
            "#COUNTRY-ALPHA-CODE", FieldType.STRING, 2);
        pnd_Exp_1001_Ccyymmdd_A = localVariables.newFieldInRecord("pnd_Exp_1001_Ccyymmdd_A", "#EXP-1001-CCYYMMDD-A", FieldType.STRING, 8);

        pnd_Exp_1001_Ccyymmdd_A__R_Field_5 = localVariables.newGroupInRecord("pnd_Exp_1001_Ccyymmdd_A__R_Field_5", "REDEFINE", pnd_Exp_1001_Ccyymmdd_A);
        pnd_Exp_1001_Ccyymmdd_A_Pnd_Exp_1001_Ccyymmdd_N = pnd_Exp_1001_Ccyymmdd_A__R_Field_5.newFieldInGroup("pnd_Exp_1001_Ccyymmdd_A_Pnd_Exp_1001_Ccyymmdd_N", 
            "#EXP-1001-CCYYMMDD-N", FieldType.NUMERIC, 8);

        pnd_Exp_1001_Ccyymmdd_A__R_Field_6 = localVariables.newGroupInRecord("pnd_Exp_1001_Ccyymmdd_A__R_Field_6", "REDEFINE", pnd_Exp_1001_Ccyymmdd_A);
        pnd_Exp_1001_Ccyymmdd_A_Pnd_Exp_1001_Ccyy = pnd_Exp_1001_Ccyymmdd_A__R_Field_6.newFieldInGroup("pnd_Exp_1001_Ccyymmdd_A_Pnd_Exp_1001_Ccyy", "#EXP-1001-CCYY", 
            FieldType.NUMERIC, 4);
        pnd_Exp_1001_Ccyymmdd_A_Pnd_Exp_1001_Mmdd = pnd_Exp_1001_Ccyymmdd_A__R_Field_6.newFieldInGroup("pnd_Exp_1001_Ccyymmdd_A_Pnd_Exp_1001_Mmdd", "#EXP-1001-MMDD", 
            FieldType.STRING, 4);
        pnd_Country_Init = localVariables.newFieldInRecord("pnd_Country_Init", "#COUNTRY-INIT", FieldType.STRING, 24);

        pnd_Country_Init__R_Field_7 = localVariables.newGroupInRecord("pnd_Country_Init__R_Field_7", "REDEFINE", pnd_Country_Init);
        pnd_Country_Init_Pnd_Country = pnd_Country_Init__R_Field_7.newFieldArrayInGroup("pnd_Country_Init_Pnd_Country", "#COUNTRY", FieldType.STRING, 
            2, new DbsArrayController(1, 12));
        pnd_Participant_Found = localVariables.newFieldInRecord("pnd_Participant_Found", "#PARTICIPANT-FOUND", FieldType.BOOLEAN, 1);
        pnd_Store_Participant = localVariables.newFieldInRecord("pnd_Store_Participant", "#STORE-PARTICIPANT", FieldType.BOOLEAN, 1);
        pnd_Start_Date_Found = localVariables.newFieldInRecord("pnd_Start_Date_Found", "#START-DATE-FOUND", FieldType.BOOLEAN, 1);
        pnd_Retroactive_Found = localVariables.newFieldInRecord("pnd_Retroactive_Found", "#RETROACTIVE-FOUND", FieldType.BOOLEAN, 1);
        pnd_Country_Found = localVariables.newFieldInRecord("pnd_Country_Found", "#COUNTRY-FOUND", FieldType.BOOLEAN, 1);
        pnd_Naa_Found = localVariables.newFieldInRecord("pnd_Naa_Found", "#NAA-FOUND", FieldType.BOOLEAN, 1);
        pnd_Residency_Found = localVariables.newFieldInRecord("pnd_Residency_Found", "#RESIDENCY-FOUND", FieldType.BOOLEAN, 1);
        pnd_Multi_Country = localVariables.newFieldInRecord("pnd_Multi_Country", "#MULTI-COUNTRY", FieldType.BOOLEAN, 1);
        pnd_Locality_Found = localVariables.newFieldInRecord("pnd_Locality_Found", "#LOCALITY-FOUND", FieldType.BOOLEAN, 1);
        pnd_Start_Date_Cite = localVariables.newFieldInRecord("pnd_Start_Date_Cite", "#START-DATE-CITE", FieldType.BOOLEAN, 1);
        pnd_Timn_N = localVariables.newFieldInRecord("pnd_Timn_N", "#TIMN-N", FieldType.NUMERIC, 7);

        pnd_Timn_N__R_Field_8 = localVariables.newGroupInRecord("pnd_Timn_N__R_Field_8", "REDEFINE", pnd_Timn_N);
        pnd_Timn_N_Pnd_Timn_A = pnd_Timn_N__R_Field_8.newFieldInGroup("pnd_Timn_N_Pnd_Timn_A", "#TIMN-A", FieldType.STRING, 7);
        pnd_Et_Ctr = localVariables.newFieldInRecord("pnd_Et_Ctr", "#ET-CTR", FieldType.PACKED_DECIMAL, 4);
        pnd_Date_D = localVariables.newFieldInRecord("pnd_Date_D", "#DATE-D", FieldType.DATE);
        pnd_Part_Isn = localVariables.newFieldInRecord("pnd_Part_Isn", "#PART-ISN", FieldType.PACKED_DECIMAL, 12);
        w = localVariables.newFieldInRecord("w", "W", FieldType.PACKED_DECIMAL, 4);
        x = localVariables.newFieldInRecord("x", "X", FieldType.PACKED_DECIMAL, 4);
        y = localVariables.newFieldInRecord("y", "Y", FieldType.PACKED_DECIMAL, 4);
        z = localVariables.newFieldInRecord("z", "Z", FieldType.PACKED_DECIMAL, 4);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Temp_Line = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Temp_Line", "#TEMP-LINE", FieldType.STRING, 35);
        pnd_Ws_Pnd_Temp_Zip = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Temp_Zip", "#TEMP-ZIP", FieldType.STRING, 10);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_oldpar.reset();
        vw_country.reset();

        ldaTwrl464a.initializeValues();
        ldaTwrl464b.initializeValues();
        ldaTwrl464c.initializeValues();
        ldaTwrl464d.initializeValues();
        ldaTwrl464e.initializeValues();

        localVariables.reset();
        pnd_Country_Init.setInitialValue("BFCGJAMYNTPORSSPTCUKVCVI");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp4640() throws Exception
    {
        super("Twrp4640");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP4640", onError);
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 02 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 03 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 04 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 05 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 06 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 07 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 08 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 09 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 10 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 11 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 12 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 13 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 14 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 15 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 16 ) PS = 60 LS = 133 ZP = ON
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  FE201504
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
        sub_Open_Mq();
        if (condition(Global.isEscape())) {return;}
        //* *============**
        //*  MAIN PROGRAM *
        //* *============**
        pnd_Source_Code.setValue("IR");                                                                                                                                   //Natural: ASSIGN #SOURCE-CODE := 'IR'
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 RECORD IRA-RECORD
        while (condition(getWorkFiles().read(1, ldaTwrl464a.getIra_Record())))
        {
            //*  ACCEPT IF IRA-TAX-ID = '901440298'
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            //*  PERFORM  REPORT-TAX-TRANSACTION-READ
            if (condition(ldaTwrl464a.getIra_Record_Ira_Addr_1().equals(" ") || ldaTwrl464a.getIra_Record_Ira_Foreign_Address().equals("Y")))                             //Natural: IF IRA-ADDR-1 = ' ' OR IRA-FOREIGN-ADDRESS = 'Y'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Temp_Zip.setValue(ldaTwrl464a.getIra_Record_Ira_Zip_Code());                                                                                   //Natural: ASSIGN #TEMP-ZIP := IRA-ZIP-CODE
                if (condition(!ldaTwrl464a.getIra_Record_Ira_Zip_Code().getSubstring(6,4).equals(" ")))                                                                   //Natural: IF SUBSTR ( IRA-ZIP-CODE,6,4 ) NE ' '
                {
                    setValueToSubstring("-",pnd_Ws_Pnd_Temp_Zip,6,1);                                                                                                     //Natural: MOVE '-' TO SUBSTR ( #TEMP-ZIP,6,1 )
                    setValueToSubstring(ldaTwrl464a.getIra_Record_Ira_Zip_Code().getSubstring(6,4),pnd_Ws_Pnd_Temp_Zip,7,4);                                              //Natural: MOVE SUBSTR ( IRA-ZIP-CODE,6,4 ) TO SUBSTR ( #TEMP-ZIP,7,4 )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_Temp_Line.setValue(DbsUtil.compress(ldaTwrl464a.getIra_Record_Ira_City(), ldaTwrl464a.getIra_Record_Ira_State(), pnd_Ws_Pnd_Temp_Zip));        //Natural: COMPRESS IRA-CITY IRA-STATE #TEMP-ZIP INTO #TEMP-LINE
                short decideConditionsMet695 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN IRA-ADDR-2 = ' '
                if (condition(ldaTwrl464a.getIra_Record_Ira_Addr_2().equals(" ")))
                {
                    decideConditionsMet695++;
                    ldaTwrl464a.getIra_Record_Ira_Addr_2().setValue(pnd_Ws_Pnd_Temp_Line);                                                                                //Natural: ASSIGN IRA-ADDR-2 := #TEMP-LINE
                }                                                                                                                                                         //Natural: WHEN IRA-ADDR-3 = ' '
                else if (condition(ldaTwrl464a.getIra_Record_Ira_Addr_3().equals(" ")))
                {
                    decideConditionsMet695++;
                    ldaTwrl464a.getIra_Record_Ira_Addr_3().setValue(pnd_Ws_Pnd_Temp_Line);                                                                                //Natural: ASSIGN IRA-ADDR-3 := #TEMP-LINE
                }                                                                                                                                                         //Natural: WHEN IRA-ADDR-4 = ' '
                else if (condition(ldaTwrl464a.getIra_Record_Ira_Addr_4().equals(" ")))
                {
                    decideConditionsMet695++;
                    ldaTwrl464a.getIra_Record_Ira_Addr_4().setValue(pnd_Ws_Pnd_Temp_Line);                                                                                //Natural: ASSIGN IRA-ADDR-4 := #TEMP-LINE
                }                                                                                                                                                         //Natural: WHEN IRA-ADDR-5 = ' '
                else if (condition(ldaTwrl464a.getIra_Record_Ira_Addr_5().equals(" ")))
                {
                    decideConditionsMet695++;
                    ldaTwrl464a.getIra_Record_Ira_Addr_5().setValue(pnd_Ws_Pnd_Temp_Line);                                                                                //Natural: ASSIGN IRA-ADDR-5 := #TEMP-LINE
                }                                                                                                                                                         //Natural: WHEN IRA-ADDR-6 = ' '
                else if (condition(ldaTwrl464a.getIra_Record_Ira_Addr_6().equals(" ")))
                {
                    decideConditionsMet695++;
                    ldaTwrl464a.getIra_Record_Ira_Addr_6().setValue(pnd_Ws_Pnd_Temp_Line);                                                                                //Natural: ASSIGN IRA-ADDR-6 := #TEMP-LINE
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Part_Isn.setValue(0);                                                                                                                                     //Natural: ASSIGN #PART-ISN := 0
                                                                                                                                                                          //Natural: PERFORM LOOKUP-PARTICIPANT
            sub_Lookup_Participant();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Participant_Found.equals(false)))                                                                                                           //Natural: IF #PARTICIPANT-FOUND = FALSE
            {
                                                                                                                                                                          //Natural: PERFORM STORE-NEW-PARTICIPANT
                sub_Store_New_Participant();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Store_Participant.equals(true)))                                                                                                        //Natural: IF #STORE-PARTICIPANT = TRUE
                {
                                                                                                                                                                          //Natural: PERFORM STORE-NEW-PARTICIPANT-FROM-OLD
                    sub_Store_New_Participant_From_Old();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM REPORT-PARTICIPANT-ADDED
                    sub_Report_Participant_Added();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(2, false, ldaTwrl464a.getIra_Record(), pnd_Part_Isn);                                                                                    //Natural: WRITE WORK FILE 02 IRA-RECORD #PART-ISN
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* *------
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        if (condition(pnd_Read_Ctr.equals(getZero())))                                                                                                                    //Natural: IF #READ-CTR = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"Tax Transaction File (Work File 01) Is Empty",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new  //Natural: WRITE ( 00 ) '***' 06T 'Tax Transaction File (Work File 01) Is Empty' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-PARTICIPANT
        //* *-----------------------------------
        //*          (PAR.TWRPARTI-PARTICIPANT-DOD  =  ' '
        //*        #CITATION-MESSAGE-CODE  :=  52
        //*  IF PAR.TWRPARTI-FRM1001-IND  =  'Y'
        //*  ND TWRT-PYMNT-TAX-FORM-1001  =  'N'
        //*     #EXP-1001-CCYYMMDD-A  :=  PAR.TWRPARTI-PART-EFF-START-DTE
        //*     #EXP-1001-CCYY  :=  #EXP-1001-CCYY  +  2
        //*     #EXP-1001-MMDD  :=  '1231'
        //*     IF TWRT-PAYMT-DTE  >=  #EXP-1001-CCYYMMDD-A
        //*        ADD  1  TO  #F1001-Y-TO-N-CTR
        //* *      TWRT-CITATION-FLAG         :=  'C'
        //* *      PAR.TWRPARTI-CITATION-IND  :=  'C'
        //* *      #CITATION-MESSAGE-CODE     :=  53
        //* *
        //* *      PERFORM  UPDATE-CITATION-TABLE
        //*        PERFORM  REPORT-FORM1001-Y-TO-N-ADDED
        //*     ELSE
        //*        IF PAR.TWRPARTI-RESIDENCY-CODE  =  #TWRPARTI-RESIDENCY-CODE
        //*       AND PAR.TWRPARTI-CITIZEN-CDE     =  TWRT-CITIZENSHIP
        //*       AND PAR.TWRPARTI-FRM1078-IND     =  TWRT-PYMNT-TAX-FORM-1078
        //*       AND PAR.TWRPARTI-LOCALITY-CDE    =  TWRT-LOCALITY-CDE
        //*       AND
        //*          (PAR.TWRPARTI-ADDR-LN1        =  TWRT-NA-LINE (2)
        //*        OR TWRT-NA-LINE (2)             =  ' ')
        //*           ADD  1  TO  #F1001-Y-TO-N-CTR2
        //*           TWRT-CITATION-FLAG      :=  'C'
        //*           #CITATION-MESSAGE-CODE  :=  53
        //*           PERFORM  UPDATE-CITATION-TABLE
        //*           PERFORM  REPORT-FORM1001-Y-TO-N-BYPASS
        //*           PERFORM  CITE-PARTICIPANT-RECORD
        //*           ESCAPE ROUTINE
        //*        ELSE
        //*           ADD  1  TO  #F1001-Y-TO-N-CTR
        //*           TWRT-CITATION-FLAG         :=  'C'
        //*           PAR.TWRPARTI-CITATION-IND  :=  'C'
        //*           #CITATION-MESSAGE-CODE     :=  53
        //*           PERFORM  UPDATE-CITATION-TABLE
        //*           PERFORM  REPORT-FORM1001-Y-TO-N-ADDED
        //*        END-IF
        //*     END-IF
        //*  END-IF
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-NEW-PARTICIPANT-FROM-OLD
        //* *-----------------------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-NEW-PARTICIPANT
        //* * START - BELOW FIELDS ADDED AS PART OF COR/NAAD FIX **** KISHORE
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-EXISTING-PARTICIPANT
        //* *--------------------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-RESIDENCY-TYPE
        //* *--------------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-RESIDENCY-CODE
        //* *--------------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-NAME-AND-ADDRESS
        //* *----------------------------------------
        //* * #MDMA100.#I-PIN-A7 := IRA-PIN
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-COUNTRY-CODE
        //* *------------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SELECT-MULTI-COUNTRY-CODES
        //* *-------------------------------------------
        //*             PRINT '=' #TWRPARTI-COUNTRY-DESC  '=' #LOCAL-NAME (X)
        //*                   '=' IRA-PIN
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-LAST-ADDRESS-LINE
        //* *-----------------------------------------
        //* *
        //* * #MDMA100.#I-PIN-A7 := IRA-PIN
        //* *------------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-TAX-TRANSACTION-READ
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-TAX-TRANSACTION-BYPASSED
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-NEW-PARTICIPANTS-ADDED
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-RETROACTIVE-DATE-BYPASS
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-DUPLICATE-DATE-BYPASS
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-OC-RESIDENCY-CODE
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-CONVERTED-RESIDENCY-CODE
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-ADDRESS-FROM-FEEDER
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-LOCALITY-FOUND
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-DEFAULT-LOCALITY-NEW
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-DEFAULT-LOCALITY-FILE
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-LOCALITY-NOT-FOUND
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-PARTICIPANT-ADDED
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-PROGRAM-PROCESSING
        //* *------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 52T 'Tax Transactions Records Read' 120T 'REPORT: RPT1' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 02 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 50T 'Tax Transactions Records Bypassed' 120T 'REPORT: RPT2' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 03 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 48T 'Participants Added For The First Time' 120T 'REPORT: RPT3' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 04 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 50T 'Retroactive Participants Bypassed' 120T 'REPORT: RPT4' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 05 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 05 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 48T 'Participants Duplicate Date Bypassed' 120T 'REPORT: RPT5' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 06 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 06 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 50T 'Participants "OC" Residency Code' 120T 'REPORT: RPT6' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 07 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 07 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T 'Participants Converted Residency Code' 120T 'REPORT: RPT7' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 08 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 08 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 51T 'Participants Address From Feeder' 120T 'REPORT: RPT8' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 09 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 09 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T 'Participant Country Locality Code Found' 120T 'REPORT: RPT9' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 10 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 10 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'Participant Country Locality Default New' 120T 'REPORT: RPT10' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 11 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 11 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'Participant File Country Locality Default' 120T 'REPORT: RPT11' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 12 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 12 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 45T 'Participant Country Locality Code NOT Found' 120T 'REPORT: RPT12'
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 13 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 13 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 48T 'Participants Added (Already On File)' 120T 'REPORT: RPT13' //
        //* *-------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-MQ
        //*  FE201504
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* *-------                                                                                                                                                      //Natural: ON ERROR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
    }
    private void sub_Lookup_Participant() throws Exception                                                                                                                //Natural: LOOKUP-PARTICIPANT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Participant_Found.setValue(false);                                                                                                                            //Natural: ASSIGN #PARTICIPANT-FOUND := FALSE
        pnd_Store_Participant.setValue(false);                                                                                                                            //Natural: ASSIGN #STORE-PARTICIPANT := FALSE
        pnd_Start_Date_Found.setValue(false);                                                                                                                             //Natural: ASSIGN #START-DATE-FOUND := FALSE
        pnd_Retroactive_Found.setValue(false);                                                                                                                            //Natural: ASSIGN #RETROACTIVE-FOUND := FALSE
        pnd_Country_Found.setValue(false);                                                                                                                                //Natural: ASSIGN #COUNTRY-FOUND := FALSE
        pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Tax_Id.setValue(ldaTwrl464a.getIra_Record_Ira_Tax_Id());                                                                   //Natural: ASSIGN #S-TWRPARTI-TAX-ID := IRA-TAX-ID
        pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Status.setValue(" ");                                                                                                      //Natural: ASSIGN #S-TWRPARTI-STATUS := ' '
        pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Eff_End_Dte.setValue("99999999");                                                                                          //Natural: ASSIGN #S-TWRPARTI-EFF-END-DTE := '99999999'
                                                                                                                                                                          //Natural: PERFORM LOOKUP-RESIDENCY-TYPE
        sub_Lookup_Residency_Type();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaTwrl464a.getIra_Record_Ira_Residency_Code().greaterOrEqual("96") && ldaTwrl464a.getIra_Record_Ira_Residency_Code().lessOrEqual("99")))           //Natural: IF IRA-RESIDENCY-CODE = '96' THRU '99'
        {
            if (condition(ldaTwrl464a.getIra_Record_Ira_Pin().notEquals(" ")))                                                                                            //Natural: IF IRA-PIN NE ' '
            {
                pnd_Residency_Found.setValue(false);                                                                                                                      //Natural: ASSIGN #RESIDENCY-FOUND := FALSE
                                                                                                                                                                          //Natural: PERFORM LOOKUP-RESIDENCY-CODE
                sub_Lookup_Residency_Code();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Residency_Found.equals(true)))                                                                                                          //Natural: IF #RESIDENCY-FOUND = TRUE
                {
                    ldaTwrl464a.getIra_Record_Ira_Residency_Code().setValue(pnd_Twrparti_Residency_Code);                                                                 //Natural: ASSIGN IRA-RESIDENCY-CODE := #TWRPARTI-RESIDENCY-CODE
                                                                                                                                                                          //Natural: PERFORM LOOKUP-RESIDENCY-TYPE
                    sub_Lookup_Residency_Type();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Twrparti_Residency_Code.setValue(ldaTwrl464a.getIra_Record_Ira_Residency_Code());                                                                 //Natural: ASSIGN #TWRPARTI-RESIDENCY-CODE := IRA-RESIDENCY-CODE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl464a.getIra_Record_Ira_Citizenship_Code().equals("01")))                                                                             //Natural: IF IRA-CITIZENSHIP-CODE = '01'
                {
                    pnd_Twrparti_Residency_Code.setValue(ldaTwrl464a.getIra_Record_Ira_Residency_Code());                                                                 //Natural: ASSIGN #TWRPARTI-RESIDENCY-CODE := IRA-RESIDENCY-CODE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Twrparti_Residency_Code.setValue("OC");                                                                                                           //Natural: ASSIGN #TWRPARTI-RESIDENCY-CODE := 'OC'
                    ldaTwrl464a.getIra_Record_Ira_Residency_Code().setValue("OC");                                                                                        //Natural: ASSIGN IRA-RESIDENCY-CODE := 'OC'
                                                                                                                                                                          //Natural: PERFORM REPORT-OC-RESIDENCY-CODE
                    sub_Report_Oc_Residency_Code();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Twrparti_Residency_Code.setValue(ldaTwrl464a.getIra_Record_Ira_Residency_Code());                                                                         //Natural: ASSIGN #TWRPARTI-RESIDENCY-CODE := IRA-RESIDENCY-CODE
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl464b.getVw_par().startDatabaseRead                                                                                                                         //Natural: READ ( 1 ) PAR WITH TWRPARTI-CURR-REC-SD = #S-TWRPARTI-CURR-REC
        (
        "RL1",
        new Wc[] { new Wc("TWRPARTI_CURR_REC_SD", ">=", pnd_S_Twrparti_Curr_Rec, WcType.BY) },
        new Oc[] { new Oc("TWRPARTI_CURR_REC_SD", "ASC") },
        1
        );
        RL1:
        while (condition(ldaTwrl464b.getVw_par().readNextRow("RL1")))
        {
            if (condition(ldaTwrl464b.getPar_Twrparti_Tax_Id().equals(ldaTwrl464a.getIra_Record_Ira_Tax_Id()) && ldaTwrl464b.getPar_Twrparti_Status().equals(" ")         //Natural: IF PAR.TWRPARTI-TAX-ID = IRA-TAX-ID AND PAR.TWRPARTI-STATUS = ' ' AND PAR.TWRPARTI-PART-EFF-END-DTE = '99999999'
                && ldaTwrl464b.getPar_Twrparti_Part_Eff_End_Dte().equals("99999999")))
            {
                pnd_Participant_Found.setValue(true);                                                                                                                     //Natural: ASSIGN #PARTICIPANT-FOUND := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*   ADD PARTICIPANT FOR THE FIRST TIME
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  IF IRA-CITIZENSHIP-CODE      =  '01'
            //*  OR #TWRPARTI-RESIDENCY-CODE  =  MASK (NN)
            //*  OR #TWRPARTI-RESIDENCY-CODE  =  'OC'
            //*     IGNORE
            //*  ELSE
            //*     IF PAR.TWRPARTI-LOCALITY-CDE  NE  ' '
            //*    AND PAR.TWRPARTI-LOCALITY-CDE  NE  '3E'
            //*        IRA-LOCALITY-CODE  :=  PAR.TWRPARTI-LOCALITY-CDE
            //*        PERFORM  REPORT-DEFAULT-LOCALITY-FILE
            //*     ELSE
            //*        IF #TWRPARTI-RESIDENCY-CODE  =  MASK (AA)
            //*           #MULTI-COUNTRY   :=  FALSE
            //*           #LOCALITY-FOUND  :=  FALSE
            //*           PERFORM  SELECT-MULTI-COUNTRY-CODES
            //*           IF #MULTI-COUNTRY   =  TRUE
            //*              IF #LOCALITY-FOUND  =  TRUE
            //*                 PRINT 'OLD'
            //*                 PERFORM  REPORT-LOCALITY-FOUND
            //*              ELSE
            //*                 PERFORM  REPORT-LOCALITY-NOT-FOUND
            //*              END-IF
            //*           END-IF
            //*        END-IF
            //*     END-IF
            //*  END-IF
            if (condition(ldaTwrl464b.getPar_Twrparti_Residency_Code().equals(pnd_Twrparti_Residency_Code)))                                                              //Natural: IF PAR.TWRPARTI-RESIDENCY-CODE = #TWRPARTI-RESIDENCY-CODE
            {
                //*  ND PAR.TWRPARTI-CITIZEN-CDE     =  IRA-CITIZENSHIP-CODE
                //*  ND
                //*    (PAR.TWRPARTI-LOCALITY-CDE    =  IRA-LOCALITY-CODE
                //*  OR IRA-LOCALITY-CODE            =  ' ')
                //*  ND
                //*    (PAR.TWRPARTI-ADDR-LN1        =  IRA-ADDR-1
                //*  OR IRA-ADDR-1                   =  ' ')
                if (condition(ldaTwrl464b.getPar_Twrparti_Pin().equals(ldaTwrl464a.getIra_Record_Ira_Pin()) && ldaTwrl464b.getPar_Twrparti_Participant_Dob().equals(ldaTwrl464a.getIra_Record_Ira_Dob())  //Natural: IF PAR.TWRPARTI-PIN = IRA-PIN AND PAR.TWRPARTI-PARTICIPANT-DOB = IRA-DOB AND PAR.TWRPARTI-PARTICIPANT-DOD = IRA-DOD AND PAR.TWRPARTI-ADDR-LN1 = IRA-ADDR-1 AND PAR.TWRPARTI-ADDR-LN2 = IRA-ADDR-2 AND PAR.TWRPARTI-ADDR-LN3 = IRA-ADDR-3 AND PAR.TWRPARTI-ADDR-LN4 = IRA-ADDR-4 AND PAR.TWRPARTI-ADDR-LN5 = IRA-ADDR-5 AND PAR.TWRPARTI-ADDR-LN6 = IRA-ADDR-6 AND ( PAR.TWRPARTI-PARTICIPANT-NAME = IRA-NAME OR IRA-NAME = ' ' )
                    && ldaTwrl464b.getPar_Twrparti_Participant_Dod().equals(ldaTwrl464a.getIra_Record_Ira_Dod()) && ldaTwrl464b.getPar_Twrparti_Addr_Ln1().equals(ldaTwrl464a.getIra_Record_Ira_Addr_1()) 
                    && ldaTwrl464b.getPar_Twrparti_Addr_Ln2().equals(ldaTwrl464a.getIra_Record_Ira_Addr_2()) && ldaTwrl464b.getPar_Twrparti_Addr_Ln3().equals(ldaTwrl464a.getIra_Record_Ira_Addr_3()) 
                    && ldaTwrl464b.getPar_Twrparti_Addr_Ln4().equals(ldaTwrl464a.getIra_Record_Ira_Addr_4()) && ldaTwrl464b.getPar_Twrparti_Addr_Ln5().equals(ldaTwrl464a.getIra_Record_Ira_Addr_5()) 
                    && ldaTwrl464b.getPar_Twrparti_Addr_Ln6().equals(ldaTwrl464a.getIra_Record_Ira_Addr_6()) && (ldaTwrl464b.getPar_Twrparti_Participant_Name().equals(ldaTwrl464a.getIra_Record_Ira_Name()) 
                    || ldaTwrl464a.getIra_Record_Ira_Name().equals(" "))))
                {
                    pnd_Bypass_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #BYPASS-CTR
                    //*         PERFORM  REPORT-TAX-TRANSACTION-BYPASSED
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //* SINHASN
                    if (condition(((((((ldaTwrl464b.getPar_Twrparti_Pin().equals(" ") && ldaTwrl464a.getIra_Record_Ira_Pin().notEquals(" ")) || ldaTwrl464b.getPar_Twrparti_Addr_Ln1().notEquals(ldaTwrl464a.getIra_Record_Ira_Addr_1()))  //Natural: IF ( PAR.TWRPARTI-PIN = ' ' AND IRA-PIN NE ' ' ) OR PAR.TWRPARTI-ADDR-LN1 NE IRA-ADDR-1 OR PAR.TWRPARTI-ADDR-LN2 NE IRA-ADDR-2 OR ( PAR.TWRPARTI-PARTICIPANT-DOB = ' ' AND IRA-DOB NE ' ' ) OR ( PAR.TWRPARTI-PARTICIPANT-DOD = ' ' OR = '00000000' AND IRA-DOD NE ' ' ) OR ( PAR.TWRPARTI-PARTICIPANT-NAME = ' ' AND IRA-NAME NE ' ' )
                        || ldaTwrl464b.getPar_Twrparti_Addr_Ln2().notEquals(ldaTwrl464a.getIra_Record_Ira_Addr_2())) || (ldaTwrl464b.getPar_Twrparti_Participant_Dob().equals(" ") 
                        && ldaTwrl464a.getIra_Record_Ira_Dob().notEquals(" "))) || ((ldaTwrl464b.getPar_Twrparti_Participant_Dod().equals(" ") || ldaTwrl464b.getPar_Twrparti_Participant_Dod().equals("00000000")) 
                        && ldaTwrl464a.getIra_Record_Ira_Dod().notEquals(" "))) || (ldaTwrl464b.getPar_Twrparti_Participant_Name().equals(" ") && ldaTwrl464a.getIra_Record_Ira_Name().notEquals(" ")))))
                    {
                                                                                                                                                                          //Natural: PERFORM UPDATE-EXISTING-PARTICIPANT
                        sub_Update_Existing_Participant();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RL1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl464b.getPar_Twrparti_Residency_Code().equals(pnd_Twrparti_Residency_Code)))                                                              //Natural: IF PAR.TWRPARTI-RESIDENCY-CODE = #TWRPARTI-RESIDENCY-CODE
            {
                pnd_Bypass_Ctr.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #BYPASS-CTR
                //*     PERFORM  REPORT-TAX-TRANSACTION-BYPASSED
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  RESIDENCE, CITIZENSHIP, OR TAX FORMS CHANGED...
            if (condition(ldaTwrl464b.getPar_Twrparti_Part_Eff_Start_Dte().greater(ldaTwrl464a.getIra_Record_Ira_Simulation_Date())))                                     //Natural: IF PAR.TWRPARTI-PART-EFF-START-DTE > IRA-SIMULATION-DATE
            {
                pnd_Retroactive_Found.setValue(true);                                                                                                                     //Natural: ASSIGN #RETROACTIVE-FOUND := TRUE
                pnd_Retroactive_Ctr.nadd(1);                                                                                                                              //Natural: ADD 1 TO #RETROACTIVE-CTR
                //*     PERFORM  CITE-PARTICIPANT-RECORD
                //*     TWRT-CITATION-FLAG      :=  'C'
                //*     #CITATION-MESSAGE-CODE  :=  51
                //*     PERFORM  UPDATE-CITATION-TABLE
                                                                                                                                                                          //Natural: PERFORM REPORT-RETROACTIVE-DATE-BYPASS
                sub_Report_Retroactive_Date_Bypass();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RL1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl464b.getPar_Twrparti_Part_Eff_Start_Dte().equals(ldaTwrl464a.getIra_Record_Ira_Simulation_Date())))                                      //Natural: IF PAR.TWRPARTI-PART-EFF-START-DTE = IRA-SIMULATION-DATE
            {
                pnd_Start_Date_Cite.setValue(false);                                                                                                                      //Natural: ASSIGN #START-DATE-CITE := FALSE
                if (condition(ldaTwrl464b.getPar_Twrparti_Residency_Code().notEquals(pnd_Twrparti_Residency_Code)))                                                       //Natural: IF PAR.TWRPARTI-RESIDENCY-CODE NE #TWRPARTI-RESIDENCY-CODE
                {
                    pnd_Start_Date_Cite.setValue(true);                                                                                                                   //Natural: ASSIGN #START-DATE-CITE := TRUE
                    //*        PERFORM  UPDATE-CITATION-TABLE
                }                                                                                                                                                         //Natural: END-IF
                //*     IF PAR.TWRPARTI-CITIZEN-CDE  NE  IRA-CITIZENSHIP-CODE
                //*        #CITATION-MESSAGE-CODE  :=  62
                //*        #START-DATE-CITE        :=  TRUE
                //*        PERFORM  UPDATE-CITATION-TABLE
                //*     END-IF
                //*     IF PAR.TWRPARTI-LOCALITY-CDE  NE  TWRT-LOCALITY-CDE
                //*        #CITATION-MESSAGE-CODE  :=  46
                //*        #START-DATE-CITE        :=  TRUE
                //*        PERFORM  UPDATE-CITATION-TABLE
                //*     END-IF
                if (condition(pnd_Start_Date_Cite.equals(true)))                                                                                                          //Natural: IF #START-DATE-CITE = TRUE
                {
                    pnd_Duplicate_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #DUPLICATE-CTR
                    pnd_Start_Date_Found.setValue(true);                                                                                                                  //Natural: ASSIGN #START-DATE-FOUND := TRUE
                    //*        TWRT-CITATION-FLAG  :=  'C'
                    //*        PERFORM  CITE-PARTICIPANT-RECORD
                                                                                                                                                                          //Natural: PERFORM REPORT-DUPLICATE-DATE-BYPASS
                    sub_Report_Duplicate_Date_Bypass();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*        WRITE  WORK FILE 03  TWRT-RECORD  /*  TO CITE THE OTHER SAME
                    //*                                          /*  PAYMENT DATE RECORD
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Store_Participant.setValue(true);                                                                                                                         //Natural: ASSIGN #STORE-PARTICIPANT := TRUE
            G2:                                                                                                                                                           //Natural: GET OLDPAR *ISN ( RL1. )
            vw_oldpar.readByID(ldaTwrl464b.getVw_par().getAstISN("RL1"), "G2");
            if (condition(ldaTwrl464a.getIra_Record_Ira_Simulation_Date().equals(oldpar_Twrparti_Part_Eff_Start_Dte)))                                                    //Natural: IF IRA-SIMULATION-DATE = OLDPAR.TWRPARTI-PART-EFF-START-DTE
            {
                pnd_Upd_Part_Eff_End_Dte.setValue(ldaTwrl464a.getIra_Record_Ira_Simulation_Date());                                                                       //Natural: ASSIGN #UPD-PART-EFF-END-DTE := IRA-SIMULATION-DATE
                oldpar_Twrparti_Part_Eff_End_Dte.setValue(pnd_Upd_Part_Eff_End_Dte);                                                                                      //Natural: ASSIGN OLDPAR.TWRPARTI-PART-EFF-END-DTE := #UPD-PART-EFF-END-DTE
                oldpar_Twrparti_Part_Invrse_End_Dte.compute(new ComputeParameters(false, oldpar_Twrparti_Part_Invrse_End_Dte), DbsField.subtract(100000000,               //Natural: ASSIGN OLDPAR.TWRPARTI-PART-INVRSE-END-DTE := 100000000 - #UPD-PART-EFF-END-DTE-N
                    pnd_Upd_Part_Eff_End_Dte_Pnd_Upd_Part_Eff_End_Dte_N));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaTwrl464a.getIra_Record_Ira_Simulation_Date());                                                //Natural: MOVE EDITED IRA-SIMULATION-DATE TO #DATE-D ( EM = YYYYMMDD )
                pnd_Date_D.nsubtract(1);                                                                                                                                  //Natural: ASSIGN #DATE-D := #DATE-D - 1
                pnd_Upd_Part_Eff_End_Dte.setValueEdited(pnd_Date_D,new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED #DATE-D ( EM = YYYYMMDD ) TO #UPD-PART-EFF-END-DTE
                oldpar_Twrparti_Part_Eff_End_Dte.setValue(pnd_Upd_Part_Eff_End_Dte);                                                                                      //Natural: ASSIGN OLDPAR.TWRPARTI-PART-EFF-END-DTE := #UPD-PART-EFF-END-DTE
                oldpar_Twrparti_Part_Invrse_End_Dte.compute(new ComputeParameters(false, oldpar_Twrparti_Part_Invrse_End_Dte), DbsField.subtract(100000000,               //Natural: ASSIGN OLDPAR.TWRPARTI-PART-INVRSE-END-DTE := 100000000 - #UPD-PART-EFF-END-DTE-N
                    pnd_Upd_Part_Eff_End_Dte_Pnd_Upd_Part_Eff_End_Dte_N));
            }                                                                                                                                                             //Natural: END-IF
            vw_oldpar.updateDBRow("G2");                                                                                                                                  //Natural: UPDATE ( G2. )
            pnd_Et_Ctr.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #ET-CTR
            if (condition(ldaTwrl464b.getPar_Twrparti_Residency_Code().equals(pnd_Twrparti_Residency_Code)))                                                              //Natural: IF PAR.TWRPARTI-RESIDENCY-CODE = #TWRPARTI-RESIDENCY-CODE
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl464b.getPar_Twrparti_Residency_Code().setValue(pnd_Twrparti_Residency_Code);                                                                       //Natural: ASSIGN PAR.TWRPARTI-RESIDENCY-CODE := #TWRPARTI-RESIDENCY-CODE
                //*     IF PAR.TWRPARTI-CITIZEN-CDE  NE  '1'
                //*    AND #TWRPARTI-RESIDENCY-CODE  =  MASK (AA)
                //*        PERFORM  SELECT-MULTI-COUNTRY-CODES
                //*     END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  IF PAR.TWRPARTI-CITIZEN-CDE  =  IRA-CITIZENSHIP-CODE
            //*     IGNORE
            //*  ELSE
            //*     PAR.TWRPARTI-CITIZEN-CDE  :=  IRA-CITIZENSHIP-CODE
            //*  END-IF
            if (condition(ldaTwrl464b.getPar_Twrparti_Pin().equals(ldaTwrl464a.getIra_Record_Ira_Pin())))                                                                 //Natural: IF PAR.TWRPARTI-PIN = IRA-PIN
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl464b.getPar_Twrparti_Pin().setValue(ldaTwrl464a.getIra_Record_Ira_Pin());                                                                          //Natural: ASSIGN PAR.TWRPARTI-PIN := IRA-PIN
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl464b.getPar_Twrparti_Participant_Name().equals(ldaTwrl464a.getIra_Record_Ira_Name())))                                                   //Natural: IF PAR.TWRPARTI-PARTICIPANT-NAME = IRA-NAME
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl464b.getPar_Twrparti_Participant_Name().equals(" ")))                                                                                //Natural: IF PAR.TWRPARTI-PARTICIPANT-NAME = ' '
                {
                    ldaTwrl464b.getPar_Twrparti_Participant_Name().setValue(ldaTwrl464a.getIra_Record_Ira_Name());                                                        //Natural: ASSIGN PAR.TWRPARTI-PARTICIPANT-NAME := IRA-NAME
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl464b.getPar_Twrparti_Participant_Dob().equals(ldaTwrl464a.getIra_Record_Ira_Dob())))                                                     //Natural: IF PAR.TWRPARTI-PARTICIPANT-DOB = IRA-DOB
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl464b.getPar_Twrparti_Participant_Dob().equals(" ")))                                                                                 //Natural: IF PAR.TWRPARTI-PARTICIPANT-DOB = ' '
                {
                    ldaTwrl464b.getPar_Twrparti_Participant_Dob().setValue(ldaTwrl464a.getIra_Record_Ira_Dob());                                                          //Natural: ASSIGN PAR.TWRPARTI-PARTICIPANT-DOB := IRA-DOB
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl464b.getPar_Twrparti_Participant_Dod().equals(ldaTwrl464a.getIra_Record_Ira_Dod())))                                                     //Natural: IF PAR.TWRPARTI-PARTICIPANT-DOD = IRA-DOD
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*    IF PAR.TWRPARTI-PARTICIPANT-DOD  =  ' '                 /* SINHASN
                //*  SINHASN
                if (condition(ldaTwrl464b.getPar_Twrparti_Participant_Dod().equals(" ") || ldaTwrl464b.getPar_Twrparti_Participant_Dod().equals("00000000")))             //Natural: IF PAR.TWRPARTI-PARTICIPANT-DOD = ' ' OR = '00000000'
                {
                    ldaTwrl464b.getPar_Twrparti_Participant_Dod().setValue(ldaTwrl464a.getIra_Record_Ira_Dod());                                                          //Natural: ASSIGN PAR.TWRPARTI-PARTICIPANT-DOD := IRA-DOD
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  IF PAR.TWRPARTI-LOCALITY-CDE  =  TWRT-LOCALITY-CDE
            //*     IGNORE
            //*  ELSE
            //*     PAR.TWRPARTI-LOCALITY-CDE  :=  TWRT-LOCALITY-CDE
            //*  END-IF
            if (condition(ldaTwrl464b.getPar_Twrparti_Addr_Ln1().equals(ldaTwrl464a.getIra_Record_Ira_Addr_1())))                                                         //Natural: IF PAR.TWRPARTI-ADDR-LN1 = IRA-ADDR-1
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl464b.getPar_Twrparti_Addr_Ln1().setValue(ldaTwrl464a.getIra_Record_Ira_Addr_1());                                                                  //Natural: ASSIGN PAR.TWRPARTI-ADDR-LN1 := IRA-ADDR-1
                if (condition(ldaTwrl464a.getIra_Record_Ira_Addr_1().equals(" ") && ldaTwrl464a.getIra_Record_Ira_Addr_2().equals(" ")))                                  //Natural: IF IRA-ADDR-1 = ' ' AND IRA-ADDR-2 = ' '
                {
                    ldaTwrl464b.getPar_Twrparti_Addr_Source().setValue(" ");                                                                                              //Natural: ASSIGN PAR.TWRPARTI-ADDR-SOURCE := ' '
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaTwrl464b.getPar_Twrparti_Addr_Source().setValue("P");                                                                                              //Natural: ASSIGN PAR.TWRPARTI-ADDR-SOURCE := 'P'
                                                                                                                                                                          //Natural: PERFORM REPORT-ADDRESS-FROM-FEEDER
                    sub_Report_Address_From_Feeder();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl464b.getPar_Twrparti_Addr_Ln2().equals(ldaTwrl464a.getIra_Record_Ira_Addr_2())))                                                         //Natural: IF PAR.TWRPARTI-ADDR-LN2 = IRA-ADDR-2
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl464b.getPar_Twrparti_Addr_Ln2().setValue(ldaTwrl464a.getIra_Record_Ira_Addr_2());                                                                  //Natural: ASSIGN PAR.TWRPARTI-ADDR-LN2 := IRA-ADDR-2
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl464b.getPar_Twrparti_Addr_Ln3().equals(ldaTwrl464a.getIra_Record_Ira_Addr_3())))                                                         //Natural: IF PAR.TWRPARTI-ADDR-LN3 = IRA-ADDR-3
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl464b.getPar_Twrparti_Addr_Ln3().setValue(ldaTwrl464a.getIra_Record_Ira_Addr_3());                                                                  //Natural: ASSIGN PAR.TWRPARTI-ADDR-LN3 := IRA-ADDR-3
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl464b.getPar_Twrparti_Addr_Ln4().equals(ldaTwrl464a.getIra_Record_Ira_Addr_4())))                                                         //Natural: IF PAR.TWRPARTI-ADDR-LN4 = IRA-ADDR-4
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl464b.getPar_Twrparti_Addr_Ln4().setValue(ldaTwrl464a.getIra_Record_Ira_Addr_4());                                                                  //Natural: ASSIGN PAR.TWRPARTI-ADDR-LN4 := IRA-ADDR-4
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl464b.getPar_Twrparti_Addr_Ln5().equals(ldaTwrl464a.getIra_Record_Ira_Addr_5())))                                                         //Natural: IF PAR.TWRPARTI-ADDR-LN5 = IRA-ADDR-5
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl464b.getPar_Twrparti_Addr_Ln5().setValue(ldaTwrl464a.getIra_Record_Ira_Addr_5());                                                                  //Natural: ASSIGN PAR.TWRPARTI-ADDR-LN5 := IRA-ADDR-5
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl464b.getPar_Twrparti_Addr_Ln6().equals(ldaTwrl464a.getIra_Record_Ira_Addr_6())))                                                         //Natural: IF PAR.TWRPARTI-ADDR-LN6 = IRA-ADDR-6
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl464b.getPar_Twrparti_Addr_Ln6().setValue(ldaTwrl464a.getIra_Record_Ira_Addr_6());                                                                  //Natural: ASSIGN PAR.TWRPARTI-ADDR-LN6 := IRA-ADDR-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Store_New_Participant_From_Old() throws Exception                                                                                                    //Natural: STORE-NEW-PARTICIPANT-FROM-OLD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Timn_N.setValue(Global.getTIMN());                                                                                                                            //Natural: ASSIGN #TIMN-N := *TIMN
        ldaTwrl464c.getVw_newpar().setValuesByName(ldaTwrl464b.getVw_par());                                                                                              //Natural: MOVE BY NAME PAR TO NEWPAR
        ldaTwrl464c.getNewpar_Twrparti_Residency_Type().setValue(pnd_Twrparti_Residency_Type);                                                                            //Natural: ASSIGN NEWPAR.TWRPARTI-RESIDENCY-TYPE := #TWRPARTI-RESIDENCY-TYPE
        ldaTwrl464c.getNewpar_Twrparti_Part_Eff_Start_Dte().setValue(ldaTwrl464a.getIra_Record_Ira_Simulation_Date());                                                    //Natural: ASSIGN NEWPAR.TWRPARTI-PART-EFF-START-DTE := IRA-SIMULATION-DATE
        ldaTwrl464c.getNewpar_Twrparti_Part_Eff_End_Dte().setValue("99999999");                                                                                           //Natural: ASSIGN NEWPAR.TWRPARTI-PART-EFF-END-DTE := '99999999'
        ldaTwrl464c.getNewpar_Twrparti_Part_Invrse_End_Dte().setValue(11111111);                                                                                          //Natural: ASSIGN NEWPAR.TWRPARTI-PART-INVRSE-END-DTE := 11111111
        ldaTwrl464c.getNewpar_Twrparti_Origin().setValue("IR");                                                                                                           //Natural: ASSIGN NEWPAR.TWRPARTI-ORIGIN := 'IR'
        ldaTwrl464c.getNewpar_Twrparti_Updt_User().setValue(Global.getINIT_USER());                                                                                       //Natural: ASSIGN NEWPAR.TWRPARTI-UPDT-USER := *INIT-USER
        ldaTwrl464c.getNewpar_Twrparti_Updt_Dte().setValue(Global.getDATN());                                                                                             //Natural: ASSIGN NEWPAR.TWRPARTI-UPDT-DTE := *DATN
        ldaTwrl464c.getNewpar_Twrparti_Updt_Time().setValue(pnd_Timn_N_Pnd_Timn_A);                                                                                       //Natural: ASSIGN NEWPAR.TWRPARTI-UPDT-TIME := #TIMN-A
        ldaTwrl464c.getNewpar_Twrparti_Lu_Ts().setValue(Global.getTIMX());                                                                                                //Natural: ASSIGN NEWPAR.TWRPARTI-LU-TS := *TIMX
        ST1:                                                                                                                                                              //Natural: STORE NEWPAR
        ldaTwrl464c.getVw_newpar().insertDBRow("ST1");
        pnd_Part_Isn.setValue(ldaTwrl464c.getVw_newpar().getAstISN("ST1"));                                                                                               //Natural: ASSIGN #PART-ISN := *ISN ( ST1. )
        pnd_Et_Ctr.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #ET-CTR
        if (condition(pnd_Et_Ctr.greater(200)))                                                                                                                           //Natural: IF #ET-CTR > 200
        {
            pnd_Et_Ctr.setValue(0);                                                                                                                                       //Natural: ASSIGN #ET-CTR := 0
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Old_Store_Ctr.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #OLD-STORE-CTR
    }
    private void sub_Store_New_Participant() throws Exception                                                                                                             //Natural: STORE-NEW-PARTICIPANT
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------
        if (condition(ldaTwrl464a.getIra_Record_Ira_Citizenship_Code().equals("01") || DbsUtil.maskMatches(pnd_Twrparti_Residency_Code,"NN") || pnd_Twrparti_Residency_Code.equals("OC"))) //Natural: IF IRA-CITIZENSHIP-CODE = '01' OR #TWRPARTI-RESIDENCY-CODE = MASK ( NN ) OR #TWRPARTI-RESIDENCY-CODE = 'OC'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(DbsUtil.maskMatches(pnd_Twrparti_Residency_Code,"AA")))                                                                                         //Natural: IF #TWRPARTI-RESIDENCY-CODE = MASK ( AA )
            {
                pnd_Multi_Country.setValue(false);                                                                                                                        //Natural: ASSIGN #MULTI-COUNTRY := FALSE
                pnd_Locality_Found.setValue(false);                                                                                                                       //Natural: ASSIGN #LOCALITY-FOUND := FALSE
                                                                                                                                                                          //Natural: PERFORM SELECT-MULTI-COUNTRY-CODES
                sub_Select_Multi_Country_Codes();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Multi_Country.equals(true)))                                                                                                            //Natural: IF #MULTI-COUNTRY = TRUE
                {
                    if (condition(pnd_Locality_Found.equals(true)))                                                                                                       //Natural: IF #LOCALITY-FOUND = TRUE
                    {
                                                                                                                                                                          //Natural: PERFORM REPORT-LOCALITY-FOUND
                        sub_Report_Locality_Found();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaTwrl464a.getIra_Record_Ira_Locality_Code().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Twrparti_Residency_Code,               //Natural: COMPRESS #TWRPARTI-RESIDENCY-CODE '1' INTO IRA-LOCALITY-CODE LEAVING NO SPACE
                            "1"));
                        //*            TWRT-CITATION-FLAG      :=  'C'
                        //*            #CITATION-MESSAGE-CODE  :=  37
                        //*            PERFORM  UPDATE-CITATION-TABLE
                                                                                                                                                                          //Natural: PERFORM REPORT-DEFAULT-LOCALITY-NEW
                        sub_Report_Default_Locality_New();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Timn_N.setValue(Global.getTIMN());                                                                                                                            //Natural: ASSIGN #TIMN-N := *TIMN
        pnd_Twrparti_Frm1078_Eff_Dte.setValue("00000000");                                                                                                                //Natural: ASSIGN #TWRPARTI-FRM1078-EFF-DTE := '00000000'
        pnd_Twrparti_Frm1001_Eff_Dte.setValue("00000000");                                                                                                                //Natural: ASSIGN #TWRPARTI-FRM1001-EFF-DTE := '00000000'
        if (condition(ldaTwrl464a.getIra_Record_Ira_Addr_1().equals(" ") && ldaTwrl464a.getIra_Record_Ira_Addr_2().equals(" ")))                                          //Natural: IF IRA-ADDR-1 = ' ' AND IRA-ADDR-2 = ' '
        {
            ldaTwrl464c.getNewpar_Twrparti_Addr_Source().setValue(" ");                                                                                                   //Natural: ASSIGN NEWPAR.TWRPARTI-ADDR-SOURCE := ' '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl464c.getNewpar_Twrparti_Addr_Source().setValue("P");                                                                                                   //Natural: ASSIGN NEWPAR.TWRPARTI-ADDR-SOURCE := 'P'
                                                                                                                                                                          //Natural: PERFORM REPORT-ADDRESS-FROM-FEEDER
            sub_Report_Address_From_Feeder();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl464c.getNewpar_Twrparti_Status().setValue(" ");                                                                                                            //Natural: ASSIGN NEWPAR.TWRPARTI-STATUS := ' '
        ldaTwrl464c.getNewpar_Twrparti_Residency_Type().setValue(pnd_Twrparti_Residency_Type);                                                                            //Natural: ASSIGN NEWPAR.TWRPARTI-RESIDENCY-TYPE := #TWRPARTI-RESIDENCY-TYPE
        ldaTwrl464c.getNewpar_Twrparti_Tax_Id_Type().setValue(ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type());                                                               //Natural: ASSIGN NEWPAR.TWRPARTI-TAX-ID-TYPE := IRA-TAX-ID-TYPE
        ldaTwrl464c.getNewpar_Twrparti_Tax_Id().setValue(ldaTwrl464a.getIra_Record_Ira_Tax_Id());                                                                         //Natural: ASSIGN NEWPAR.TWRPARTI-TAX-ID := IRA-TAX-ID
        ldaTwrl464c.getNewpar_Twrparti_Citation_Ind().setValue(" ");                                                                                                      //Natural: ASSIGN NEWPAR.TWRPARTI-CITATION-IND := ' '
        ldaTwrl464c.getNewpar_Twrparti_Part_Eff_Start_Dte().setValue(ldaTwrl464a.getIra_Record_Ira_Simulation_Date());                                                    //Natural: ASSIGN NEWPAR.TWRPARTI-PART-EFF-START-DTE := IRA-SIMULATION-DATE
        ldaTwrl464c.getNewpar_Twrparti_Part_Eff_End_Dte().setValue("99999999");                                                                                           //Natural: ASSIGN NEWPAR.TWRPARTI-PART-EFF-END-DTE := '99999999'
        ldaTwrl464c.getNewpar_Twrparti_Part_Invrse_End_Dte().setValue(11111111);                                                                                          //Natural: ASSIGN NEWPAR.TWRPARTI-PART-INVRSE-END-DTE := 11111111
        ldaTwrl464c.getNewpar_Twrparti_Pin().setValue(ldaTwrl464a.getIra_Record_Ira_Pin());                                                                               //Natural: ASSIGN NEWPAR.TWRPARTI-PIN := IRA-PIN
        ldaTwrl464c.getNewpar_Twrparti_Participant_Name().setValue(ldaTwrl464a.getIra_Record_Ira_Name());                                                                 //Natural: ASSIGN NEWPAR.TWRPARTI-PARTICIPANT-NAME := IRA-NAME
        ldaTwrl464c.getNewpar_Twrparti_Participant_Dob().setValue(ldaTwrl464a.getIra_Record_Ira_Dob());                                                                   //Natural: ASSIGN NEWPAR.TWRPARTI-PARTICIPANT-DOB := IRA-DOB
        ldaTwrl464c.getNewpar_Twrparti_Participant_Dod().setValue(ldaTwrl464a.getIra_Record_Ira_Dod());                                                                   //Natural: ASSIGN NEWPAR.TWRPARTI-PARTICIPANT-DOD := IRA-DOD
        ldaTwrl464c.getNewpar_Twrparti_Residency_Code().setValue(pnd_Twrparti_Residency_Code);                                                                            //Natural: ASSIGN NEWPAR.TWRPARTI-RESIDENCY-CODE := #TWRPARTI-RESIDENCY-CODE
        ldaTwrl464c.getNewpar_Twrparti_Locality_Cde().setValue(ldaTwrl464a.getIra_Record_Ira_Locality_Code());                                                            //Natural: ASSIGN NEWPAR.TWRPARTI-LOCALITY-CDE := IRA-LOCALITY-CODE
        ldaTwrl464c.getNewpar_Twrparti_Citizen_Cde().setValue(ldaTwrl464a.getIra_Record_Ira_Citizenship_Code());                                                          //Natural: ASSIGN NEWPAR.TWRPARTI-CITIZEN-CDE := IRA-CITIZENSHIP-CODE
        ldaTwrl464c.getNewpar_Twrparti_Addr_Ln1().setValue(ldaTwrl464a.getIra_Record_Ira_Addr_1());                                                                       //Natural: ASSIGN NEWPAR.TWRPARTI-ADDR-LN1 := IRA-ADDR-1
        ldaTwrl464c.getNewpar_Twrparti_Addr_Ln2().setValue(ldaTwrl464a.getIra_Record_Ira_Addr_2());                                                                       //Natural: ASSIGN NEWPAR.TWRPARTI-ADDR-LN2 := IRA-ADDR-2
        ldaTwrl464c.getNewpar_Twrparti_Addr_Ln3().setValue(ldaTwrl464a.getIra_Record_Ira_Addr_3());                                                                       //Natural: ASSIGN NEWPAR.TWRPARTI-ADDR-LN3 := IRA-ADDR-3
        ldaTwrl464c.getNewpar_Twrparti_Addr_Ln4().setValue(ldaTwrl464a.getIra_Record_Ira_Addr_4());                                                                       //Natural: ASSIGN NEWPAR.TWRPARTI-ADDR-LN4 := IRA-ADDR-4
        ldaTwrl464c.getNewpar_Twrparti_Addr_Ln5().setValue(ldaTwrl464a.getIra_Record_Ira_Addr_5());                                                                       //Natural: ASSIGN NEWPAR.TWRPARTI-ADDR-LN5 := IRA-ADDR-5
        ldaTwrl464c.getNewpar_Twrparti_Addr_Ln6().setValue(ldaTwrl464a.getIra_Record_Ira_Addr_6());                                                                       //Natural: ASSIGN NEWPAR.TWRPARTI-ADDR-LN6 := IRA-ADDR-6
        ldaTwrl464c.getNewpar_Twrparti_Frm1001_Ind().setValue("N");                                                                                                       //Natural: ASSIGN NEWPAR.TWRPARTI-FRM1001-IND := 'N'
        ldaTwrl464c.getNewpar_Twrparti_Frm1001_Eff_Dte().setValue(pnd_Twrparti_Frm1001_Eff_Dte);                                                                          //Natural: ASSIGN NEWPAR.TWRPARTI-FRM1001-EFF-DTE := #TWRPARTI-FRM1001-EFF-DTE
        ldaTwrl464c.getNewpar_Twrparti_Frm1078_Ind().setValue("N");                                                                                                       //Natural: ASSIGN NEWPAR.TWRPARTI-FRM1078-IND := 'N'
        ldaTwrl464c.getNewpar_Twrparti_Frm1078_Eff_Dte().setValue(pnd_Twrparti_Frm1078_Eff_Dte);                                                                          //Natural: ASSIGN NEWPAR.TWRPARTI-FRM1078-EFF-DTE := #TWRPARTI-FRM1078-EFF-DTE
        ldaTwrl464c.getNewpar_Twrparti_Origin().setValue("IR");                                                                                                           //Natural: ASSIGN NEWPAR.TWRPARTI-ORIGIN := 'IR'
        ldaTwrl464c.getNewpar_Twrparti_Updt_User().setValue(Global.getINIT_USER());                                                                                       //Natural: ASSIGN NEWPAR.TWRPARTI-UPDT-USER := *INIT-USER
        ldaTwrl464c.getNewpar_Twrparti_Updt_Dte().setValue(Global.getDATN());                                                                                             //Natural: ASSIGN NEWPAR.TWRPARTI-UPDT-DTE := *DATN
        ldaTwrl464c.getNewpar_Twrparti_Updt_Time().setValue(pnd_Timn_N_Pnd_Timn_A);                                                                                       //Natural: ASSIGN NEWPAR.TWRPARTI-UPDT-TIME := #TIMN-A
        ldaTwrl464c.getNewpar_Twrparti_Lu_Ts().setValue(Global.getTIMX());                                                                                                //Natural: ASSIGN NEWPAR.TWRPARTI-LU-TS := *TIMX
        ldaTwrl464c.getNewpar_Twrparti_Rlup_Last_Name().setValue(" ");                                                                                                    //Natural: ASSIGN NEWPAR.TWRPARTI-RLUP-LAST-NAME := ' '
        ldaTwrl464c.getNewpar_Twrparti_Rlup_First_Name().setValue(" ");                                                                                                   //Natural: ASSIGN NEWPAR.TWRPARTI-RLUP-FIRST-NAME := ' '
        ldaTwrl464c.getNewpar_Twrparti_Rlup_Middle_Name().setValue(" ");                                                                                                  //Natural: ASSIGN NEWPAR.TWRPARTI-RLUP-MIDDLE-NAME := ' '
        ldaTwrl464c.getNewpar_Twrparti_Rlup_Part_Name().setValue(" ");                                                                                                    //Natural: ASSIGN NEWPAR.TWRPARTI-RLUP-PART-NAME := ' '
        ldaTwrl464c.getNewpar_Twrparti_Rlup_Dob().setValue(" ");                                                                                                          //Natural: ASSIGN NEWPAR.TWRPARTI-RLUP-DOB := ' '
        ldaTwrl464c.getNewpar_Twrparti_Rlup_Dod().setValue(" ");                                                                                                          //Natural: ASSIGN NEWPAR.TWRPARTI-RLUP-DOD := ' '
        ldaTwrl464c.getNewpar_Twrparti_Rlup_Addr_Ln1().setValue(" ");                                                                                                     //Natural: ASSIGN NEWPAR.TWRPARTI-RLUP-ADDR-LN1 := ' '
        ldaTwrl464c.getNewpar_Twrparti_Rlup_Addr_Ln2().setValue(" ");                                                                                                     //Natural: ASSIGN NEWPAR.TWRPARTI-RLUP-ADDR-LN2 := ' '
        ldaTwrl464c.getNewpar_Twrparti_Rlup_Addr_Ln3().setValue(" ");                                                                                                     //Natural: ASSIGN NEWPAR.TWRPARTI-RLUP-ADDR-LN3 := ' '
        ldaTwrl464c.getNewpar_Twrparti_Rlup_Addr_Ln4().setValue(" ");                                                                                                     //Natural: ASSIGN NEWPAR.TWRPARTI-RLUP-ADDR-LN4 := ' '
        ldaTwrl464c.getNewpar_Twrparti_Rlup_Addr_Ln5().setValue(" ");                                                                                                     //Natural: ASSIGN NEWPAR.TWRPARTI-RLUP-ADDR-LN5 := ' '
        ldaTwrl464c.getNewpar_Twrparti_Rlup_Addr_Ln6().setValue(" ");                                                                                                     //Natural: ASSIGN NEWPAR.TWRPARTI-RLUP-ADDR-LN6 := ' '
        ldaTwrl464c.getNewpar_Twrparti_Rlup_Postal_Data().setValue(" ");                                                                                                  //Natural: ASSIGN NEWPAR.TWRPARTI-RLUP-POSTAL-DATA := ' '
        ldaTwrl464c.getNewpar_Twrparti_Rlup_Type_Cde().setValue(" ");                                                                                                     //Natural: ASSIGN NEWPAR.TWRPARTI-RLUP-TYPE-CDE := ' '
        ldaTwrl464c.getNewpar_Twrparti_Rlup_Geo_Code().setValue(" ");                                                                                                     //Natural: ASSIGN NEWPAR.TWRPARTI-RLUP-GEO-CODE := ' '
        ldaTwrl464c.getNewpar_Twrparti_Rlup_Email_Pref().setValue(" ");                                                                                                   //Natural: ASSIGN NEWPAR.TWRPARTI-RLUP-EMAIL-PREF := ' '
        ldaTwrl464c.getNewpar_Twrparti_Rlup_Email_Pref_Dte().setValue(" ");                                                                                               //Natural: ASSIGN NEWPAR.TWRPARTI-RLUP-EMAIL-PREF-DTE := ' '
        ldaTwrl464c.getNewpar_Twrparti_Rlup_Email_Addr().setValue(" ");                                                                                                   //Natural: ASSIGN NEWPAR.TWRPARTI-RLUP-EMAIL-ADDR := ' '
        ldaTwrl464c.getNewpar_Twrparti_Rlup_Email_Addr_Dte().setValue(" ");                                                                                               //Natural: ASSIGN NEWPAR.TWRPARTI-RLUP-EMAIL-ADDR-DTE := ' '
        ldaTwrl464c.getNewpar_Twrparti_Rlup_Pin().setValue(" ");                                                                                                          //Natural: ASSIGN NEWPAR.TWRPARTI-RLUP-PIN := ' '
        //* * END - ABOVE FIELDS ADDED AS PART OF COR/NAAD FIX **** KISHORE
        ST2:                                                                                                                                                              //Natural: STORE NEWPAR
        ldaTwrl464c.getVw_newpar().insertDBRow("ST2");
        pnd_Part_Isn.setValue(ldaTwrl464c.getVw_newpar().getAstISN("ST2"));                                                                                               //Natural: ASSIGN #PART-ISN := *ISN ( ST2. )
        pnd_Et_Ctr.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #ET-CTR
        if (condition(pnd_Et_Ctr.greater(200)))                                                                                                                           //Natural: IF #ET-CTR > 200
        {
            pnd_Et_Ctr.setValue(0);                                                                                                                                       //Natural: ASSIGN #ET-CTR := 0
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        pnd_New_Store_Ctr.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #NEW-STORE-CTR
                                                                                                                                                                          //Natural: PERFORM REPORT-NEW-PARTICIPANTS-ADDED
        sub_Report_New_Participants_Added();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Update_Existing_Participant() throws Exception                                                                                                       //Natural: UPDATE-EXISTING-PARTICIPANT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Timn_N.setValue(Global.getTIMN());                                                                                                                            //Natural: ASSIGN #TIMN-N := *TIMN
        G3:                                                                                                                                                               //Natural: GET UPDPAR *ISN ( RL1. )
        ldaTwrl464d.getVw_updpar().readByID(ldaTwrl464b.getVw_par().getAstISN("RL1"), "G3");
        if (condition(ldaTwrl464d.getUpdpar_Twrparti_Pin().equals(" ") && ldaTwrl464a.getIra_Record_Ira_Pin().notEquals(" ")))                                            //Natural: IF UPDPAR.TWRPARTI-PIN = ' ' AND IRA-PIN NE ' '
        {
            ldaTwrl464d.getUpdpar_Twrparti_Pin().setValue(ldaTwrl464a.getIra_Record_Ira_Pin());                                                                           //Natural: ASSIGN UPDPAR.TWRPARTI-PIN := IRA-PIN
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl464d.getUpdpar_Twrparti_Participant_Name().equals(" ") && ldaTwrl464a.getIra_Record_Ira_Name().notEquals(" ")))                              //Natural: IF UPDPAR.TWRPARTI-PARTICIPANT-NAME = ' ' AND IRA-NAME NE ' '
        {
            ldaTwrl464d.getUpdpar_Twrparti_Participant_Name().setValue(ldaTwrl464a.getIra_Record_Ira_Name());                                                             //Natural: ASSIGN UPDPAR.TWRPARTI-PARTICIPANT-NAME := IRA-NAME
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl464d.getUpdpar_Twrparti_Participant_Dob().equals(" ") && ldaTwrl464a.getIra_Record_Ira_Dob().notEquals(" ")))                                //Natural: IF UPDPAR.TWRPARTI-PARTICIPANT-DOB = ' ' AND IRA-DOB NE ' '
        {
            ldaTwrl464d.getUpdpar_Twrparti_Participant_Dob().setValue(ldaTwrl464a.getIra_Record_Ira_Dob());                                                               //Natural: ASSIGN UPDPAR.TWRPARTI-PARTICIPANT-DOB := IRA-DOB
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF UPDPAR.TWRPARTI-PARTICIPANT-DOD  =  ' '                /* SINHASN
        //*  SINHASN
        if (condition(((ldaTwrl464d.getUpdpar_Twrparti_Participant_Dod().equals(" ") || ldaTwrl464d.getUpdpar_Twrparti_Participant_Dod().equals("00000000"))              //Natural: IF UPDPAR.TWRPARTI-PARTICIPANT-DOD = ' ' OR = '00000000' AND IRA-DOD NE ' '
            && ldaTwrl464a.getIra_Record_Ira_Dod().notEquals(" "))))
        {
            ldaTwrl464d.getUpdpar_Twrparti_Participant_Dod().setValue(ldaTwrl464a.getIra_Record_Ira_Dod());                                                               //Natural: ASSIGN UPDPAR.TWRPARTI-PARTICIPANT-DOD := IRA-DOD
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl464d.getUpdpar_Twrparti_Addr_Ln1().setValue(ldaTwrl464a.getIra_Record_Ira_Addr_1());                                                                       //Natural: ASSIGN UPDPAR.TWRPARTI-ADDR-LN1 := IRA-ADDR-1
        ldaTwrl464d.getUpdpar_Twrparti_Addr_Ln2().setValue(ldaTwrl464a.getIra_Record_Ira_Addr_2());                                                                       //Natural: ASSIGN UPDPAR.TWRPARTI-ADDR-LN2 := IRA-ADDR-2
        ldaTwrl464d.getUpdpar_Twrparti_Addr_Ln3().setValue(ldaTwrl464a.getIra_Record_Ira_Addr_3());                                                                       //Natural: ASSIGN UPDPAR.TWRPARTI-ADDR-LN3 := IRA-ADDR-3
        ldaTwrl464d.getUpdpar_Twrparti_Addr_Ln4().setValue(ldaTwrl464a.getIra_Record_Ira_Addr_4());                                                                       //Natural: ASSIGN UPDPAR.TWRPARTI-ADDR-LN4 := IRA-ADDR-4
        ldaTwrl464d.getUpdpar_Twrparti_Addr_Ln5().setValue(ldaTwrl464a.getIra_Record_Ira_Addr_5());                                                                       //Natural: ASSIGN UPDPAR.TWRPARTI-ADDR-LN5 := IRA-ADDR-5
        ldaTwrl464d.getUpdpar_Twrparti_Addr_Ln6().setValue(ldaTwrl464a.getIra_Record_Ira_Addr_6());                                                                       //Natural: ASSIGN UPDPAR.TWRPARTI-ADDR-LN6 := IRA-ADDR-6
        if (condition(ldaTwrl464a.getIra_Record_Ira_Addr_1().equals(" ") && ldaTwrl464a.getIra_Record_Ira_Addr_2().equals(" ")))                                          //Natural: IF IRA-ADDR-1 = ' ' AND IRA-ADDR-2 = ' '
        {
            ldaTwrl464d.getUpdpar_Twrparti_Addr_Source().setValue(" ");                                                                                                   //Natural: ASSIGN UPDPAR.TWRPARTI-ADDR-SOURCE := ' '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl464d.getUpdpar_Twrparti_Addr_Source().setValue("P");                                                                                                   //Natural: ASSIGN UPDPAR.TWRPARTI-ADDR-SOURCE := 'P'
                                                                                                                                                                          //Natural: PERFORM REPORT-ADDRESS-FROM-FEEDER
            sub_Report_Address_From_Feeder();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl464d.getUpdpar_Twrparti_Origin().setValue("IR");                                                                                                           //Natural: ASSIGN UPDPAR.TWRPARTI-ORIGIN := 'IR'
        ldaTwrl464d.getUpdpar_Twrparti_Updt_User().setValue(Global.getINIT_USER());                                                                                       //Natural: ASSIGN UPDPAR.TWRPARTI-UPDT-USER := *INIT-USER
        ldaTwrl464d.getUpdpar_Twrparti_Updt_Dte().setValue(Global.getDATN());                                                                                             //Natural: ASSIGN UPDPAR.TWRPARTI-UPDT-DTE := *DATN
        ldaTwrl464d.getUpdpar_Twrparti_Updt_Time().setValue(pnd_Timn_N_Pnd_Timn_A);                                                                                       //Natural: ASSIGN UPDPAR.TWRPARTI-UPDT-TIME := #TIMN-A
        ldaTwrl464d.getUpdpar_Twrparti_Lu_Ts().setValue(Global.getTIMX());                                                                                                //Natural: ASSIGN UPDPAR.TWRPARTI-LU-TS := *TIMX
        ldaTwrl464d.getVw_updpar().updateDBRow("G3");                                                                                                                     //Natural: UPDATE ( G3. )
        pnd_Part_Isn.setValue(ldaTwrl464d.getVw_updpar().getAstISN("G3"));                                                                                                //Natural: ASSIGN #PART-ISN := *ISN ( G3. )
        pnd_Et_Ctr.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #ET-CTR
        if (condition(pnd_Et_Ctr.greater(400)))                                                                                                                           //Natural: IF #ET-CTR > 400
        {
            pnd_Et_Ctr.setValue(0);                                                                                                                                       //Natural: ASSIGN #ET-CTR := 0
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Update_Ctr.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #UPDATE-CTR
    }
    private void sub_Lookup_Residency_Type() throws Exception                                                                                                             //Natural: LOOKUP-RESIDENCY-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Twrparti_Residency_Type.setValue(" ");                                                                                                                        //Natural: ASSIGN #TWRPARTI-RESIDENCY-TYPE := ' '
        //*  CANADA
        if (condition(ldaTwrl464a.getIra_Record_Ira_Residency_Code().equals("CA")))                                                                                       //Natural: IF IRA-RESIDENCY-CODE = 'CA'
        {
            pnd_Twrparti_Residency_Type.setValue("4");                                                                                                                    //Natural: ASSIGN #TWRPARTI-RESIDENCY-TYPE := '4'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  COMMONWEALTH OF PUERTO RICO
        if (condition(ldaTwrl464a.getIra_Record_Ira_Residency_Code().equals("42") || ldaTwrl464a.getIra_Record_Ira_Residency_Code().equals("PR") || ldaTwrl464a.getIra_Record_Ira_Residency_Code().equals("RQ"))) //Natural: IF IRA-RESIDENCY-CODE = '42' OR = 'PR' OR = 'RQ'
        {
            pnd_Twrparti_Residency_Type.setValue("3");                                                                                                                    //Natural: ASSIGN #TWRPARTI-RESIDENCY-TYPE := '3'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  PROVINCE OF CANADA
        if (condition(((ldaTwrl464a.getIra_Record_Ira_Residency_Code().greaterOrEqual("74") && ldaTwrl464a.getIra_Record_Ira_Residency_Code().lessOrEqual("88"))          //Natural: IF IRA-RESIDENCY-CODE = '74' THRU '88' OR IRA-RESIDENCY-CODE = '96'
            || ldaTwrl464a.getIra_Record_Ira_Residency_Code().equals("96"))))
        {
            pnd_Twrparti_Residency_Type.setValue("5");                                                                                                                    //Natural: ASSIGN #TWRPARTI-RESIDENCY-TYPE := '5'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  STATE
        if (condition((((ldaTwrl464a.getIra_Record_Ira_Residency_Code().greaterOrEqual("00") && ldaTwrl464a.getIra_Record_Ira_Residency_Code().lessOrEqual("41"))         //Natural: IF IRA-RESIDENCY-CODE = '00' THRU '41' OR IRA-RESIDENCY-CODE = '43' THRU '57' OR IRA-RESIDENCY-CODE = '97' OR = 'US'
            || (ldaTwrl464a.getIra_Record_Ira_Residency_Code().greaterOrEqual("43") && ldaTwrl464a.getIra_Record_Ira_Residency_Code().lessOrEqual("57"))) 
            || (ldaTwrl464a.getIra_Record_Ira_Residency_Code().equals("97") || ldaTwrl464a.getIra_Record_Ira_Residency_Code().equals("US")))))
        {
            pnd_Twrparti_Residency_Type.setValue("1");                                                                                                                    //Natural: ASSIGN #TWRPARTI-RESIDENCY-TYPE := '1'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  COUNTRY
        if (condition(DbsUtil.maskMatches(ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"AA") || ldaTwrl464a.getIra_Record_Ira_Residency_Code().equals("98")             //Natural: IF IRA-RESIDENCY-CODE = MASK ( AA ) OR = '98' OR = '99'
            || ldaTwrl464a.getIra_Record_Ira_Residency_Code().equals("99")))
        {
            pnd_Twrparti_Residency_Type.setValue("2");                                                                                                                    //Natural: ASSIGN #TWRPARTI-RESIDENCY-TYPE := '2'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Lookup_Residency_Code() throws Exception                                                                                                             //Natural: LOOKUP-RESIDENCY-CODE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Addrss_Geographic_Cde.setValue(" ");                                                                                                                          //Natural: ASSIGN #ADDRSS-GEOGRAPHIC-CDE := ' '
        if (condition(ldaTwrl464a.getIra_Record_Ira_Citizenship_Code().equals("01")))                                                                                     //Natural: IF IRA-CITIZENSHIP-CODE = '01'
        {
                                                                                                                                                                          //Natural: PERFORM LOOKUP-NAME-AND-ADDRESS
            sub_Lookup_Name_And_Address();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Naa_Found.equals(true)))                                                                                                                    //Natural: IF #NAA-FOUND = TRUE
            {
                if (condition(DbsUtil.maskMatches(pnd_Addrss_Geographic_Cde,"AA")))                                                                                       //Natural: IF #ADDRSS-GEOGRAPHIC-CDE = MASK ( AA )
                {
                    pnd_Country_Found.setValue(false);                                                                                                                    //Natural: ASSIGN #COUNTRY-FOUND := FALSE
                                                                                                                                                                          //Natural: PERFORM LOOKUP-COUNTRY-CODE
                    sub_Lookup_Country_Code();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(pnd_Country_Found.equals(true)))                                                                                                        //Natural: IF #COUNTRY-FOUND = TRUE
                    {
                        pnd_Twrparti_Residency_Code.setValue(pnd_Addrss_Geographic_Cde);                                                                                  //Natural: ASSIGN #TWRPARTI-RESIDENCY-CODE := #ADDRSS-GEOGRAPHIC-CDE
                        pnd_Residency_Found.setValue(true);                                                                                                               //Natural: ASSIGN #RESIDENCY-FOUND := TRUE
                                                                                                                                                                          //Natural: PERFORM REPORT-CONVERTED-RESIDENCY-CODE
                        sub_Report_Converted_Residency_Code();
                        if (condition(Global.isEscape())) {return;}
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Twrparti_Residency_Code.setValue(pnd_Addrss_Geographic_Cde);                                                                                      //Natural: ASSIGN #TWRPARTI-RESIDENCY-CODE := #ADDRSS-GEOGRAPHIC-CDE
                    pnd_Residency_Found.setValue(true);                                                                                                                   //Natural: ASSIGN #RESIDENCY-FOUND := TRUE
                                                                                                                                                                          //Natural: PERFORM REPORT-CONVERTED-RESIDENCY-CODE
                    sub_Report_Converted_Residency_Code();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM LOOKUP-NAME-AND-ADDRESS
            sub_Lookup_Name_And_Address();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Naa_Found.equals(true)))                                                                                                                    //Natural: IF #NAA-FOUND = TRUE
            {
                if (condition(pnd_Addrss_Geographic_Cde.greaterOrEqual("74") && pnd_Addrss_Geographic_Cde.lessOrEqual("88")))                                             //Natural: IF #ADDRSS-GEOGRAPHIC-CDE = '74' THRU '88'
                {
                    pnd_Twrparti_Residency_Code.setValue(pnd_Addrss_Geographic_Cde);                                                                                      //Natural: ASSIGN #TWRPARTI-RESIDENCY-CODE := #ADDRSS-GEOGRAPHIC-CDE
                    pnd_Residency_Found.setValue(true);                                                                                                                   //Natural: ASSIGN #RESIDENCY-FOUND := TRUE
                                                                                                                                                                          //Natural: PERFORM REPORT-CONVERTED-RESIDENCY-CODE
                    sub_Report_Converted_Residency_Code();
                    if (condition(Global.isEscape())) {return;}
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(DbsUtil.maskMatches(pnd_Addrss_Geographic_Cde,"AA")))                                                                                   //Natural: IF #ADDRSS-GEOGRAPHIC-CDE = MASK ( AA )
                    {
                        pnd_Country_Found.setValue(false);                                                                                                                //Natural: ASSIGN #COUNTRY-FOUND := FALSE
                                                                                                                                                                          //Natural: PERFORM LOOKUP-COUNTRY-CODE
                        sub_Lookup_Country_Code();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(pnd_Country_Found.equals(true)))                                                                                                    //Natural: IF #COUNTRY-FOUND = TRUE
                        {
                            pnd_Twrparti_Residency_Code.setValue(pnd_Addrss_Geographic_Cde);                                                                              //Natural: ASSIGN #TWRPARTI-RESIDENCY-CODE := #ADDRSS-GEOGRAPHIC-CDE
                            pnd_Residency_Found.setValue(true);                                                                                                           //Natural: ASSIGN #RESIDENCY-FOUND := TRUE
                                                                                                                                                                          //Natural: PERFORM REPORT-CONVERTED-RESIDENCY-CODE
                            sub_Report_Converted_Residency_Code();
                            if (condition(Global.isEscape())) {return;}
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Twrparti_Residency_Code.setValue("OC");                                                                                                                   //Natural: ASSIGN #TWRPARTI-RESIDENCY-CODE := 'OC'
            pnd_Residency_Found.setValue(true);                                                                                                                           //Natural: ASSIGN #RESIDENCY-FOUND := TRUE
                                                                                                                                                                          //Natural: PERFORM REPORT-OC-RESIDENCY-CODE
            sub_Report_Oc_Residency_Code();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Lookup_Name_And_Address() throws Exception                                                                                                           //Natural: LOOKUP-NAME-AND-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Naa_Found.setValue(false);                                                                                                                                    //Natural: ASSIGN #NAA-FOUND := FALSE
        //*   #PH-UNQUE-ID-NMBR   :=  IRA-PIN /* NOT USED JW0427
        //*   #PH-BSE-ADDRSS-IND  :=  'B'     /* NOT USED JW0427
        //*  NA1.                              /* FE201504 START COMMENT OUT
        //*  READ (1)   NAA   WITH   PIN-BASE-KEY  =  #PIN-BASE-KEY
        //*  IF PH-UNQUE-ID-NMBR  (NA1.)  =  #PH-UNQUE-ID-NMBR-N
        //*      AND PH-BSE-ADDRSS-IND (NA1.)  =  'B'
        //*    IF ADDRSS-GEOGRAPHIC-CDE (NA1.)  =  '  '  OR =  '00'  OR =  '96'
        //*        OR =  '97'  OR =  '98'  OR =  '99'
        //*      IGNORE
        //*    ELSE
        //*      #NAA-FOUND  :=  TRUE
        //*      #ADDRSS-GEOGRAPHIC-CDE  :=  ADDRSS-GEOGRAPHIC-CDE (NA1.)
        //*    END-IF
        //*    ESCAPE ROUTINE
        //*  ELSE
        //*    #PH-BSE-ADDRSS-IND  :=  ' '
        //*    NA2.
        //*    READ (1)   NAA   WITH   PIN-CNTRCT-KEY  =  #PIN-BASE-KEY
        //*      IF PH-UNQUE-ID-NMBR (NA2.)  =  #PH-UNQUE-ID-NMBR-N
        //*        IF ADDRSS-GEOGRAPHIC-CDE (NA2.)  =  '  '  OR =  '00'
        //*            OR =  '96'  OR =  '97'
        //*            OR =  '98'  OR =  '99'
        //*          IGNORE
        //*        ELSE
        //*          #NAA-FOUND  :=  TRUE
        //*          #ADDRSS-GEOGRAPHIC-CDE  :=  ADDRSS-GEOGRAPHIC-CDE (NA2.)
        //*        END-IF
        //*        ESCAPE ROUTINE
        //*      END-IF
        //*  END-READ                 /* FE201504 END COMMENT OUT
        //*                       /* FE201504 START
        //*  JW0427-> #MDMA101
        //*   JW0427 -> A12, #MDMA101
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_A12().setValue(ldaTwrl464a.getIra_Record_Ira_Pin());                                                                          //Natural: ASSIGN #MDMA101.#I-PIN-A12 := IRA-PIN
        //*  JW0427
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        //*  JW0427
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA101.#O-RETURN-CODE EQ '0000'
        {
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().equals(" ") || pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().equals("00")  //Natural: IF #MDMA101.#O-BASE-ADDRESS-GEOGRAPHIC-CODE = ' ' OR = '00' OR = '96' OR = '97' OR = '98' OR = '99'
                || pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().equals("96") || pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().equals("97") 
                || pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().equals("98") || pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().equals("99")))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Naa_Found.setValue(true);                                                                                                                             //Natural: ASSIGN #NAA-FOUND := TRUE
                pnd_Addrss_Geographic_Cde.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code());                                                       //Natural: ASSIGN #ADDRSS-GEOGRAPHIC-CDE := #MDMA101.#O-BASE-ADDRESS-GEOGRAPHIC-CODE
            }                                                                                                                                                             //Natural: END-IF
            //*  FE201504 END
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Lookup_Country_Code() throws Exception                                                                                                               //Natural: LOOKUP-COUNTRY-CODE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Tbl_Nbr.setValue("3");                                                                                                    //Natural: ASSIGN #COUNTRY-TBL-NBR := '3'
        pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Tax_Year.setValue(ldaTwrl464a.getIra_Record_Ira_Tax_Year());                                                              //Natural: ASSIGN #COUNTRY-TAX-YEAR := IRA-TAX-YEAR
        pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Alpha_Code.setValue(pnd_Addrss_Geographic_Cde);                                                                           //Natural: ASSIGN #COUNTRY-ALPHA-CODE := #ADDRSS-GEOGRAPHIC-CDE
        vw_country.createHistogram                                                                                                                                        //Natural: HISTOGRAM ( 1 ) COUNTRY TIRCNTL-NBR-YEAR-ALPHA-CDE STARTING FROM #COUNTRY-3-YR-ALPHA-CODE
        (
        "HIST01",
        "TIRCNTL_NBR_YEAR_ALPHA_CDE",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_ALPHA_CDE", ">=", pnd_Country_3_Yr_Alpha_Code, WcType.WITH) },
        1
        );
        HIST01:
        while (condition(vw_country.readNextRow("HIST01")))
        {
            if (condition(country_Country_Tbl_Nbr.equals(pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Tbl_Nbr) && country_Country_Tax_Year.equals(pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Tax_Year)  //Natural: IF COUNTRY-TBL-NBR = #COUNTRY-TBL-NBR AND COUNTRY-TAX-YEAR = #COUNTRY-TAX-YEAR AND COUNTRY-ALPHA-CODE = #COUNTRY-ALPHA-CODE
                && country_Country_Alpha_Code.equals(pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Alpha_Code)))
            {
                pnd_Country_Found.setValue(true);                                                                                                                         //Natural: ASSIGN #COUNTRY-FOUND := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
    }
    private void sub_Select_Multi_Country_Codes() throws Exception                                                                                                        //Natural: SELECT-MULTI-COUNTRY-CODES
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Multi_Country.setValue(false);                                                                                                                                //Natural: ASSIGN #MULTI-COUNTRY := FALSE
        pnd_Locality_Found.setValue(false);                                                                                                                               //Natural: ASSIGN #LOCALITY-FOUND := FALSE
        pnd_Naa_Found.setValue(false);                                                                                                                                    //Natural: ASSIGN #NAA-FOUND := FALSE
        FOR01:                                                                                                                                                            //Natural: FOR W = 1 TO 12
        for (w.setValue(1); condition(w.lessOrEqual(12)); w.nadd(1))
        {
            if (condition(pnd_Twrparti_Residency_Code.equals(pnd_Country_Init_Pnd_Country.getValue(w))))                                                                  //Natural: IF #TWRPARTI-RESIDENCY-CODE = #COUNTRY ( W )
            {
                pnd_Multi_Country.setValue(true);                                                                                                                         //Natural: ASSIGN #MULTI-COUNTRY := TRUE
                                                                                                                                                                          //Natural: PERFORM LOOKUP-LAST-ADDRESS-LINE
                sub_Lookup_Last_Address_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Naa_Found.equals(false) || pnd_Twrparti_Country_Desc.equals(" ")))                                                                      //Natural: IF #NAA-FOUND = FALSE OR #TWRPARTI-COUNTRY-DESC = ' '
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                    //*  BK
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    FOR02:                                                                                                                                                //Natural: FOR X = 1 TO 39
                    for (x.setValue(1); condition(x.lessOrEqual(39)); x.nadd(1))
                    {
                        if (condition(pnd_Twrparti_Country_Desc.contains (ldaTwrl464e.getPnd_Locality_Table_Pnd_Local_Name ().getValue(x))))                              //Natural: IF #TWRPARTI-COUNTRY-DESC = SCAN #LOCAL-NAME ( X )
                        {
                            pnd_Locality_Found.setValue(true);                                                                                                            //Natural: ASSIGN #LOCALITY-FOUND := TRUE
                            ldaTwrl464a.getIra_Record_Ira_Locality_Code().setValue(ldaTwrl464e.getPnd_Locality_Table_Pnd_Local_Code().getValue(x));                       //Natural: ASSIGN IRA-LOCALITY-CODE := #LOCAL-CODE ( X )
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Lookup_Last_Address_Line() throws Exception                                                                                                          //Natural: LOOKUP-LAST-ADDRESS-LINE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Naa_Found.setValue(false);                                                                                                                                    //Natural: ASSIGN #NAA-FOUND := FALSE
        pnd_Twrparti_Country_Desc.setValue(" ");                                                                                                                          //Natural: ASSIGN #TWRPARTI-COUNTRY-DESC := ' '
        //* * #PH-UNQUE-ID-NMBR   :=  IRA-PIN   /* NOT USED JW0427
        //* * #PH-BSE-ADDRSS-IND  :=  'B'      /* NOT USED JW0427
        //*  NA3.
        //*  READ (1)   NAA   WITH   PIN-BASE-KEY  =  #PIN-BASE-KEY
        //*   IF PH-UNQUE-ID-NMBR      (NA3.)  =  #PH-UNQUE-ID-NMBR-N
        //*       AND PH-BSE-ADDRSS-IND     (NA3.)  =  'B'
        //*       AND ADDRSS-GEOGRAPHIC-CDE (NA3.)  =  #TWRPARTI-RESIDENCY-CODE
        //*  INT '=' ADDRSS-GEOGRAPHIC-CDE (NA3.)  '='  #TWRPARTI-RESIDENCY-CODE
        //*      'ONE'
        //*     #NAA-FOUND  :=  TRUE
        //*                       /* FE201504 START
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_A12().setValue(ldaTwrl464a.getIra_Record_Ira_Pin());                                                                          //Natural: ASSIGN #MDMA101.#I-PIN-A12 := IRA-PIN
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000") && pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().equals(pnd_Twrparti_Residency_Code))) //Natural: IF #MDMA101.#O-RETURN-CODE EQ '0000' AND #MDMA101.#O-BASE-ADDRESS-GEOGRAPHIC-CODE = #TWRPARTI-RESIDENCY-CODE
        {
            pnd_Naa_Found.setValue(true);                                                                                                                                 //Natural: ASSIGN #NAA-FOUND := TRUE
            //*     IF ADDRSS-LNE-6 (NA3.)  =  ' '
            //*       IGNORE
            //*     ELSE
            //*       #TWRPARTI-COUNTRY-DESC  :=  ADDRSS-LNE-6 (NA3.)
            //*       ESCAPE ROUTINE
            //*     END-IF
            //*     IF ADDRSS-LNE-5 (NA3.)  =  ' '
            //*       IGNORE
            //*     ELSE
            //*      #TWRPARTI-COUNTRY-DESC  :=  ADDRSS-LNE-5 (NA3.)
            //*      ESCAPE ROUTINE
            //*    END-IF
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_4().equals(" ")))                                                                             //Natural: IF #MDMA101.#O-BASE-ADDRESS-LINE-4 = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Twrparti_Country_Desc.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_4());                                                                //Natural: ASSIGN #TWRPARTI-COUNTRY-DESC := #O-BASE-ADDRESS-LINE-4
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_3().equals(" ")))                                                                             //Natural: IF #MDMA101.#O-BASE-ADDRESS-LINE-3 = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Twrparti_Country_Desc.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_3());                                                                //Natural: ASSIGN #TWRPARTI-COUNTRY-DESC := #O-BASE-ADDRESS-LINE-3
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_2().equals(" ")))                                                                             //Natural: IF #MDMA101.#O-BASE-ADDRESS-LINE-2 = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Twrparti_Country_Desc.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_2());                                                                //Natural: ASSIGN #TWRPARTI-COUNTRY-DESC := #O-BASE-ADDRESS-LINE-2
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_1().equals(" ")))                                                                             //Natural: IF #MDMA101.#O-BASE-ADDRESS-LINE-1 = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Twrparti_Country_Desc.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_1());                                                                //Natural: ASSIGN #TWRPARTI-COUNTRY-DESC := #O-BASE-ADDRESS-LINE-1
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ELSE
        //*    #PH-BSE-ADDRSS-IND  :=  ' '
        //*    NA4.
        //*    READ (1)   NAA   WITH   PIN-CNTRCT-KEY  =  #PIN-BASE-KEY
        //*      IF PH-UNQUE-ID-NMBR      (NA4.)  =  #PH-UNQUE-ID-NMBR-N
        //*          AND ADDRSS-GEOGRAPHIC-CDE (NA4.)  =  #TWRPARTI-RESIDENCY-CODE
        //*  INT '=' ADDRSS-GEOGRAPHIC-CDE (NA4.)  '='  #TWRPARTI-RESIDENCY-CODE
        //*      'TWO'
        //*        #NAA-FOUND  :=  TRUE
        //*        IF ADDRSS-LNE-6 (NA4.)  =  ' '
        //*          IGNORE
        //*        ELSE
        //*          #TWRPARTI-COUNTRY-DESC  :=  ADDRSS-LNE-6 (NA4.)
        //*          ESCAPE ROUTINE
        //*        END-IF
        //*        IF ADDRSS-LNE-5 (NA4.)  =  ' '
        //*          IGNORE
        //*        ELSE
        //*          #TWRPARTI-COUNTRY-DESC  :=  ADDRSS-LNE-5 (NA4.)
        //*          ESCAPE ROUTINE
        //*        END-IF
        //*        IF ADDRSS-LNE-4 (NA4.)  =  ' '
        //*          IGNORE
        //*        ELSE
        //*          #TWRPARTI-COUNTRY-DESC  :=  ADDRSS-LNE-4 (NA4.)
        //*          ESCAPE ROUTINE
        //*        END-IF
        //*        IF ADDRSS-LNE-3 (NA4.)  =  ' '
        //*          IGNORE
        //*        ELSE
        //*          #TWRPARTI-COUNTRY-DESC  :=  ADDRSS-LNE-3 (NA4.)
        //*          ESCAPE ROUTINE
        //*        END-IF
        //*        IF ADDRSS-LNE-2 (NA4.)  =  ' '
        //*          IGNORE
        //*        ELSE
        //*          #TWRPARTI-COUNTRY-DESC  :=  ADDRSS-LNE-2 (NA4.)
        //*          ESCAPE ROUTINE
        //*        END-IF
        //*        IF ADDRSS-LNE-1 (NA4.)  =  ' '
        //*          IGNORE
        //*        ELSE
        //*          #TWRPARTI-COUNTRY-DESC  :=  ADDRSS-LNE-1 (NA4.)
        //*          ESCAPE ROUTINE
        //*         END-IF
        //*       END-IF
        //*     END-READ
        //*   END-IF
        //*  END-READ   /* FE201504 END COMMENT OUT
    }
    private void sub_Report_Tax_Transaction_Read() throws Exception                                                                                                       //Natural: REPORT-TAX-TRANSACTION-READ
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------
        getReports().display(1, "C/O",                                                                                                                                    //Natural: DISPLAY ( 01 ) 'C/O' IRA-COMPANY-CODE 'SR/CE' #SOURCE-CODE 'Contract' IRA-CONTRACT 'PY/EE' IRA-PAYEE 'Tax ID' IRA-TAX-ID 'T/Y' IRA-TAX-ID-TYPE 'Pin ID' IRA-PIN 'Name' IRA-NAME 'Simulation' IRA-SIMULATION-DATE ( EM = XXXX/XX/XX ) 'RS/DN' IRA-RESIDENCY-CODE 'Loc' IRA-LOCALITY-CODE 'CI/TI' IRA-CITIZENSHIP-CODE 'Tax/Year' IRA-TAX-YEAR 'D-O-B' IRA-DOB ( EM = XXXX/XX/XX )
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        if (Global.isEscape()) return;
        //*        'D-O-D'      IRA-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_Tax_Transaction_Bypassed() throws Exception                                                                                                   //Natural: REPORT-TAX-TRANSACTION-BYPASSED
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------------
        getReports().display(2, "C/O",                                                                                                                                    //Natural: DISPLAY ( 02 ) 'C/O' IRA-COMPANY-CODE 'SR/CE' #SOURCE-CODE 'Contract' IRA-CONTRACT 'PY/EE' IRA-PAYEE 'Tax ID' IRA-TAX-ID 'T/Y' IRA-TAX-ID-TYPE 'Pin ID' IRA-PIN 'Name' IRA-NAME 'Simulation' IRA-SIMULATION-DATE ( EM = XXXX/XX/XX ) 'RS/DN' IRA-RESIDENCY-CODE 'Loc' IRA-LOCALITY-CODE 'CI/TI' IRA-CITIZENSHIP-CODE 'Tax/Year' IRA-TAX-YEAR 'D-O-B' IRA-DOB ( EM = XXXX/XX/XX )
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        if (Global.isEscape()) return;
        //*        'D-O-D'      IRA-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_New_Participants_Added() throws Exception                                                                                                     //Natural: REPORT-NEW-PARTICIPANTS-ADDED
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------
        getReports().display(3, "C/O",                                                                                                                                    //Natural: DISPLAY ( 03 ) 'C/O' IRA-COMPANY-CODE 'SR/CE' #SOURCE-CODE 'Contract' IRA-CONTRACT 'PY/EE' IRA-PAYEE 'Tax ID' IRA-TAX-ID 'T/Y' IRA-TAX-ID-TYPE 'Pin ID' IRA-PIN 'Name' IRA-NAME 'Simulation' IRA-SIMULATION-DATE ( EM = XXXX/XX/XX ) 'RS/DN' IRA-RESIDENCY-CODE 'Loc' IRA-LOCALITY-CODE 'CI/TI' IRA-CITIZENSHIP-CODE 'Tax/Year' IRA-TAX-YEAR 'D-O-B' IRA-DOB ( EM = XXXX/XX/XX )
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        if (Global.isEscape()) return;
        //*        'D-O-D'      IRA-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_Retroactive_Date_Bypass() throws Exception                                                                                                    //Natural: REPORT-RETROACTIVE-DATE-BYPASS
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------------------
        getReports().display(4, "C/O",                                                                                                                                    //Natural: DISPLAY ( 04 ) 'C/O' IRA-COMPANY-CODE 'SR/CE' #SOURCE-CODE 'Contract' IRA-CONTRACT 'PY/EE' IRA-PAYEE 'Tax ID' IRA-TAX-ID 'T/Y' IRA-TAX-ID-TYPE 'Pin ID' IRA-PIN 'Name' IRA-NAME 'Simulation' IRA-SIMULATION-DATE ( EM = XXXX/XX/XX ) 'RS/DN' IRA-RESIDENCY-CODE 'Loc' IRA-LOCALITY-CODE 'CI/TI' IRA-CITIZENSHIP-CODE 'Tax/Year' IRA-TAX-YEAR 'D-O-B' IRA-DOB ( EM = XXXX/XX/XX )
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        if (Global.isEscape()) return;
        //*        'D-O-D'      IRA-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_Duplicate_Date_Bypass() throws Exception                                                                                                      //Natural: REPORT-DUPLICATE-DATE-BYPASS
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------------------
        getReports().display(5, "C/O",                                                                                                                                    //Natural: DISPLAY ( 05 ) 'C/O' IRA-COMPANY-CODE 'SR/CE' #SOURCE-CODE 'Contract' IRA-CONTRACT 'PY/EE' IRA-PAYEE 'Tax ID' IRA-TAX-ID 'T/Y' IRA-TAX-ID-TYPE 'Pin ID' IRA-PIN 'Name' IRA-NAME 'Simulation' IRA-SIMULATION-DATE ( EM = XXXX/XX/XX ) 'RS/DN' IRA-RESIDENCY-CODE 'Loc' IRA-LOCALITY-CODE 'CI/TI' IRA-CITIZENSHIP-CODE 'Tax/Year' IRA-TAX-YEAR 'D-O-B' IRA-DOB ( EM = XXXX/XX/XX )
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        if (Global.isEscape()) return;
        //*        'D-O-D'      IRA-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_Oc_Residency_Code() throws Exception                                                                                                          //Natural: REPORT-OC-RESIDENCY-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------------
        pnd_Oc_Residency_Ctr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #OC-RESIDENCY-CTR
        getReports().display(6, "C/O",                                                                                                                                    //Natural: DISPLAY ( 06 ) 'C/O' IRA-COMPANY-CODE 'SR/CE' #SOURCE-CODE 'Contract' IRA-CONTRACT 'PY/EE' IRA-PAYEE 'Tax ID' IRA-TAX-ID 'T/Y' IRA-TAX-ID-TYPE 'Pin ID' IRA-PIN 'Name' IRA-NAME 'Simulation' IRA-SIMULATION-DATE ( EM = XXXX/XX/XX ) 'RS/DN' IRA-RESIDENCY-CODE 'Loc' IRA-LOCALITY-CODE 'CI/TI' IRA-CITIZENSHIP-CODE 'Tax/Year' IRA-TAX-YEAR 'D-O-B' IRA-DOB ( EM = XXXX/XX/XX )
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        if (Global.isEscape()) return;
        //*        'D-O-D'      IRA-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_Converted_Residency_Code() throws Exception                                                                                                   //Natural: REPORT-CONVERTED-RESIDENCY-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------------
        pnd_Conv_Resdency_Ctr.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #CONV-RESDENCY-CTR
        getReports().display(7, "C/O",                                                                                                                                    //Natural: DISPLAY ( 07 ) 'C/O' IRA-COMPANY-CODE 'SR/CE' #SOURCE-CODE 'Contract' IRA-CONTRACT 'PY/EE' IRA-PAYEE 'Tax ID' IRA-TAX-ID 'T/Y' IRA-TAX-ID-TYPE 'Pin ID' IRA-PIN 'Name' IRA-NAME 'Simulation' IRA-SIMULATION-DATE ( EM = XXXX/XX/XX ) 'RS/DN' IRA-RESIDENCY-CODE 'Loc' IRA-LOCALITY-CODE 'CI/TI' IRA-CITIZENSHIP-CODE 'Tax/Year' IRA-TAX-YEAR 'D-O-B' IRA-DOB ( EM = XXXX/XX/XX )
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        if (Global.isEscape()) return;
        //*        'D-O-D'      IRA-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_Address_From_Feeder() throws Exception                                                                                                        //Natural: REPORT-ADDRESS-FROM-FEEDER
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------------
        pnd_Feed_Address_Ctr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #FEED-ADDRESS-CTR
        getReports().display(8, "C/O",                                                                                                                                    //Natural: DISPLAY ( 08 ) 'C/O' IRA-COMPANY-CODE 'SR/CE' #SOURCE-CODE 'Contract' IRA-CONTRACT 'PY/EE' IRA-PAYEE 'Tax ID' IRA-TAX-ID 'T/Y' IRA-TAX-ID-TYPE 'Pin ID' IRA-PIN 'Name' IRA-NAME 'Simulation' IRA-SIMULATION-DATE ( EM = XXXX/XX/XX ) 'RS/DN' IRA-RESIDENCY-CODE 'Loc' IRA-LOCALITY-CODE 'CI/TI' IRA-CITIZENSHIP-CODE 'Tax/Year' IRA-TAX-YEAR 'D-O-B' IRA-DOB ( EM = XXXX/XX/XX )
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        if (Global.isEscape()) return;
        //*        'D-O-D'      IRA-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_Locality_Found() throws Exception                                                                                                             //Natural: REPORT-LOCALITY-FOUND
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------
        pnd_Country_Found_Ctr.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #COUNTRY-FOUND-CTR
        getReports().display(9, "C/O",                                                                                                                                    //Natural: DISPLAY ( 09 ) 'C/O' IRA-COMPANY-CODE 'SR/CE' #SOURCE-CODE 'Contract' IRA-CONTRACT 'PY/EE' IRA-PAYEE 'Tax ID' IRA-TAX-ID 'T/Y' IRA-TAX-ID-TYPE 'Pin ID' IRA-PIN 'Name' IRA-NAME 'Simulation' IRA-SIMULATION-DATE ( EM = XXXX/XX/XX ) 'RS/DN' IRA-RESIDENCY-CODE 'Loc' IRA-LOCALITY-CODE 'CI/TI' IRA-CITIZENSHIP-CODE 'Tax/Year' IRA-TAX-YEAR 'D-O-B' IRA-DOB ( EM = XXXX/XX/XX )
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        if (Global.isEscape()) return;
        //*        'D-O-D'      IRA-DOD    (EM=XXXX/XX/XX)
        getReports().print(9, pnd_Twrparti_Country_Desc);                                                                                                                 //Natural: PRINT ( 09 ) #TWRPARTI-COUNTRY-DESC
    }
    private void sub_Report_Default_Locality_New() throws Exception                                                                                                       //Natural: REPORT-DEFAULT-LOCALITY-NEW
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------
        pnd_Default_New_Ctr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #DEFAULT-NEW-CTR
        getReports().display(10, "C/O",                                                                                                                                   //Natural: DISPLAY ( 10 ) 'C/O' IRA-COMPANY-CODE 'SR/CE' #SOURCE-CODE 'Contract' IRA-CONTRACT 'PY/EE' IRA-PAYEE 'Tax ID' IRA-TAX-ID 'T/Y' IRA-TAX-ID-TYPE 'Pin ID' IRA-PIN 'Name' IRA-NAME 'Simulation' IRA-SIMULATION-DATE ( EM = XXXX/XX/XX ) 'RS/DN' IRA-RESIDENCY-CODE 'Loc' IRA-LOCALITY-CODE 'CI/TI' IRA-CITIZENSHIP-CODE 'Tax/Year' IRA-TAX-YEAR 'D-O-B' IRA-DOB ( EM = XXXX/XX/XX )
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        if (Global.isEscape()) return;
        //*        'D-O-D'      IRA-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_Default_Locality_File() throws Exception                                                                                                      //Natural: REPORT-DEFAULT-LOCALITY-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------------------
        pnd_Default_File_Ctr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #DEFAULT-FILE-CTR
        getReports().display(11, "C/O",                                                                                                                                   //Natural: DISPLAY ( 11 ) 'C/O' IRA-COMPANY-CODE 'SR/CE' #SOURCE-CODE 'Contract' IRA-CONTRACT 'PY/EE' IRA-PAYEE 'Tax ID' IRA-TAX-ID 'T/Y' IRA-TAX-ID-TYPE 'Pin ID' IRA-PIN 'Name' IRA-NAME 'Simulation' IRA-SIMULATION-DATE ( EM = XXXX/XX/XX ) 'RS/DN' IRA-RESIDENCY-CODE 'Loc' IRA-LOCALITY-CODE 'CI/TI' IRA-CITIZENSHIP-CODE 'Tax/Year' IRA-TAX-YEAR 'D-O-B' IRA-DOB ( EM = XXXX/XX/XX )
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        if (Global.isEscape()) return;
        //*        'D-O-D'      IRA-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_Locality_Not_Found() throws Exception                                                                                                         //Natural: REPORT-LOCALITY-NOT-FOUND
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        pnd_Country_Not_Found.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #COUNTRY-NOT-FOUND
        getReports().display(12, "C/O",                                                                                                                                   //Natural: DISPLAY ( 12 ) 'C/O' IRA-COMPANY-CODE 'SR/CE' #SOURCE-CODE 'Contract' IRA-CONTRACT 'PY/EE' IRA-PAYEE 'Tax ID' IRA-TAX-ID 'T/Y' IRA-TAX-ID-TYPE 'Pin ID' IRA-PIN 'Name' IRA-NAME 'Simulation' IRA-SIMULATION-DATE ( EM = XXXX/XX/XX ) 'RS/DN' IRA-RESIDENCY-CODE 'Loc' IRA-LOCALITY-CODE 'CI/TI' IRA-CITIZENSHIP-CODE 'Tax/Year' IRA-TAX-YEAR 'D-O-B' IRA-DOB ( EM = XXXX/XX/XX )
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        if (Global.isEscape()) return;
        //*        'D-O-D'      IRA-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_Participant_Added() throws Exception                                                                                                          //Natural: REPORT-PARTICIPANT-ADDED
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------------
        getReports().display(13, "C/O",                                                                                                                                   //Natural: DISPLAY ( 13 ) 'C/O' IRA-COMPANY-CODE 'SR/CE' #SOURCE-CODE 'Contract' IRA-CONTRACT 'PY/EE' IRA-PAYEE 'Tax ID' IRA-TAX-ID 'T/Y' IRA-TAX-ID-TYPE 'Pin ID' IRA-PIN 'Name' IRA-NAME 'Simulation' IRA-SIMULATION-DATE ( EM = XXXX/XX/XX ) 'RS/DN' IRA-RESIDENCY-CODE 'Loc' IRA-LOCALITY-CODE 'CI/TI' IRA-CITIZENSHIP-CODE 'Tax/Year' IRA-TAX-YEAR 'D-O-B' IRA-DOB ( EM = XXXX/XX/XX )
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        if (Global.isEscape()) return;
        //*        'D-O-D'      IRA-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, new TabSetting(1),"IRA Transaction Records Read...............",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                            //Natural: WRITE ( 00 ) 01T 'IRA Transaction Records Read...............' #READ-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"IRA Transaction Records Bypassed...........",pnd_Bypass_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                  //Natural: WRITE ( 00 ) / 01T 'IRA Transaction Records Bypassed...........' #BYPASS-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Duplicate Contrib. Dates Bypassed..........",pnd_Duplicate_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));               //Natural: WRITE ( 00 ) / 01T 'Duplicate Contrib. Dates Bypassed..........' #DUPLICATE-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Retroactive Contrib. Records Bypassed......",pnd_Retroactive_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));             //Natural: WRITE ( 00 ) / 01T 'Retroactive Contrib. Records Bypassed......' #RETROACTIVE-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Participants Added (First Time)............",pnd_New_Store_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));               //Natural: WRITE ( 00 ) / 01T 'Participants Added (First Time)............' #NEW-STORE-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Participants Added (Already On File).......",pnd_Old_Store_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));               //Natural: WRITE ( 00 ) / 01T 'Participants Added (Already On File).......' #OLD-STORE-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Participants Updated.......................",pnd_Update_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                  //Natural: WRITE ( 00 ) / 01T 'Participants Updated.......................' #UPDATE-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Participants With 'OC' Residency Code......",pnd_Oc_Residency_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));            //Natural: WRITE ( 00 ) / 01T 'Participants With "OC" Residency Code......' #OC-RESIDENCY-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Participants With Converted Residency......",pnd_Conv_Resdency_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 00 ) / 01T 'Participants With Converted Residency......' #CONV-RESDENCY-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Participants With Feeder Address...........",pnd_Feed_Address_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));            //Natural: WRITE ( 00 ) / 01T 'Participants With Feeder Address...........' #FEED-ADDRESS-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Multi-Country Locality Code Found..........",pnd_Country_Found_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 00 ) / 01T 'Multi-Country Locality Code Found..........' #COUNTRY-FOUND-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Multi-Country Locality Code Default New....",pnd_Default_New_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));             //Natural: WRITE ( 00 ) / 01T 'Multi-Country Locality Code Default New....' #DEFAULT-NEW-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Multi-Country Locality Code Default File...",pnd_Default_File_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));            //Natural: WRITE ( 00 ) / 01T 'Multi-Country Locality Code Default File...' #DEFAULT-FILE-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Multi-Country Locality Code Not Found......",pnd_Country_Not_Found, new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 00 ) / 01T 'Multi-Country Locality Code Not Found......' #COUNTRY-NOT-FOUND
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,new TabSetting(1),"IRA Transaction Records Read...............",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                    //Natural: WRITE ( 01 ) / 01T 'IRA Transaction Records Read...............' #READ-CTR
        if (Global.isEscape()) return;
        getReports().write(2, NEWLINE,new TabSetting(1),"IRA Transaction Records Bypassed...........",pnd_Bypass_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                  //Natural: WRITE ( 02 ) / 01T 'IRA Transaction Records Bypassed...........' #BYPASS-CTR
        if (Global.isEscape()) return;
        getReports().write(3, NEWLINE,new TabSetting(1),"Participants Added (First Time)............",pnd_New_Store_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));               //Natural: WRITE ( 03 ) / 01T 'Participants Added (First Time)............' #NEW-STORE-CTR
        if (Global.isEscape()) return;
        getReports().write(4, NEWLINE,new TabSetting(1),"Retroactive Contrib. Records Bypassed......",pnd_Retroactive_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));             //Natural: WRITE ( 04 ) / 01T 'Retroactive Contrib. Records Bypassed......' #RETROACTIVE-CTR
        if (Global.isEscape()) return;
        getReports().write(5, NEWLINE,new TabSetting(1),"Duplicate Contrib. Dates Bypassed..........",pnd_Duplicate_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));               //Natural: WRITE ( 05 ) / 01T 'Duplicate Contrib. Dates Bypassed..........' #DUPLICATE-CTR
        if (Global.isEscape()) return;
        getReports().write(6, NEWLINE,new TabSetting(1),"Participants With 'OC' Residency Code......",pnd_Oc_Residency_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));            //Natural: WRITE ( 06 ) / 01T 'Participants With "OC" Residency Code......' #OC-RESIDENCY-CTR
        if (Global.isEscape()) return;
        getReports().write(7, NEWLINE,new TabSetting(1),"Participants With Converted Residency......",pnd_Conv_Resdency_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 07 ) / 01T 'Participants With Converted Residency......' #CONV-RESDENCY-CTR
        if (Global.isEscape()) return;
        getReports().write(8, NEWLINE,new TabSetting(1),"Participants With Feeder Address...........",pnd_Feed_Address_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));            //Natural: WRITE ( 08 ) / 01T 'Participants With Feeder Address...........' #FEED-ADDRESS-CTR
        if (Global.isEscape()) return;
        getReports().write(9, NEWLINE,new TabSetting(1),"Multi-Country Locality Code Found..........",pnd_Country_Found_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 09 ) / 01T 'Multi-Country Locality Code Found..........' #COUNTRY-FOUND-CTR
        if (Global.isEscape()) return;
        getReports().write(10, NEWLINE,new TabSetting(1),"Multi-Country Locality Code Default New....",pnd_Default_New_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));            //Natural: WRITE ( 10 ) / 01T 'Multi-Country Locality Code Default New....' #DEFAULT-NEW-CTR
        if (Global.isEscape()) return;
        getReports().write(11, NEWLINE,new TabSetting(1),"Multi-Country Locality Code Default File...",pnd_Default_File_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 11 ) / 01T 'Multi-Country Locality Code Default File...' #DEFAULT-FILE-CTR
        if (Global.isEscape()) return;
        getReports().write(12, NEWLINE,new TabSetting(1),"Multi-Country Locality Code Not Found......",pnd_Country_Not_Found, new ReportEditMask ("Z,ZZZ,ZZ9"));          //Natural: WRITE ( 12 ) / 01T 'Multi-Country Locality Code Not Found......' #COUNTRY-NOT-FOUND
        if (Global.isEscape()) return;
        getReports().write(13, NEWLINE,new TabSetting(1),"Participants Added (Already On File).......",pnd_Old_Store_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));              //Natural: WRITE ( 13 ) / 01T 'Participants Added (Already On File).......' #OLD-STORE-CTR
        if (Global.isEscape()) return;
    }
    //*  FE201504
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************
        //*  OPEN MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(2).equals("0")  //Natural: IF ##DATA-RESPONSE ( 1 ) = '0' AND ##DATA-RESPONSE ( 2 ) = '0' AND ##DATA-RESPONSE ( 3 ) = '0' AND ##DATA-RESPONSE ( 4 ) = '0'
            && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(3).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(4).equals("0")))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO 60
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(60)); pnd_I.nadd(1))
        {
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_I)));            //Natural: COMPRESS #RC ##DATA-RESPONSE ( #I ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Rc);                                                                                                                                //Natural: WRITE '=' #RC
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,NEWLINE,"****************************", //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
        sub_Error_Display_Start();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
        sub_Error_Display_End();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133 ZP=ON");
        Global.format(1, "PS=60 LS=133 ZP=ON");
        Global.format(2, "PS=60 LS=133 ZP=ON");
        Global.format(3, "PS=60 LS=133 ZP=ON");
        Global.format(4, "PS=60 LS=133 ZP=ON");
        Global.format(5, "PS=60 LS=133 ZP=ON");
        Global.format(6, "PS=60 LS=133 ZP=ON");
        Global.format(7, "PS=60 LS=133 ZP=ON");
        Global.format(8, "PS=60 LS=133 ZP=ON");
        Global.format(9, "PS=60 LS=133 ZP=ON");
        Global.format(10, "PS=60 LS=133 ZP=ON");
        Global.format(11, "PS=60 LS=133 ZP=ON");
        Global.format(12, "PS=60 LS=133 ZP=ON");
        Global.format(13, "PS=60 LS=133 ZP=ON");
        Global.format(14, "PS=60 LS=133 ZP=ON");
        Global.format(15, "PS=60 LS=133 ZP=ON");
        Global.format(16, "PS=60 LS=133 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(52),"Tax Transactions Records Read",new TabSetting(120),"REPORT: RPT1",NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(50),"Tax Transactions Records Bypassed",new TabSetting(120),"REPORT: RPT2",NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(48),"Participants Added For The First Time",new TabSetting(120),"REPORT: RPT3",NEWLINE,NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(4), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(50),"Retroactive Participants Bypassed",new TabSetting(120),"REPORT: RPT4",NEWLINE,NEWLINE);
        getReports().write(5, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(5), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(48),"Participants Duplicate Date Bypassed",new TabSetting(120),"REPORT: RPT5",NEWLINE,NEWLINE);
        getReports().write(6, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(6), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(50),"Participants 'OC' Residency Code",new TabSetting(120),"REPORT: RPT6",NEWLINE,NEWLINE);
        getReports().write(7, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(7), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(47),"Participants Converted Residency Code",new TabSetting(120),"REPORT: RPT7",NEWLINE,NEWLINE);
        getReports().write(8, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(8), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(51),"Participants Address From Feeder",new TabSetting(120),"REPORT: RPT8",NEWLINE,NEWLINE);
        getReports().write(9, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(9), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(47),"Participant Country Locality Code Found",new TabSetting(120),"REPORT: RPT9",NEWLINE,NEWLINE);
        getReports().write(10, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(10), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"Participant Country Locality Default New",new TabSetting(120),"REPORT: RPT10",NEWLINE,NEWLINE);
        getReports().write(11, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(11), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"Participant File Country Locality Default",new TabSetting(120),"REPORT: RPT11",NEWLINE,NEWLINE);
        getReports().write(12, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(12), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(45),"Participant Country Locality Code NOT Found",new TabSetting(120),"REPORT: RPT12");
        getReports().write(13, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(13), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(48),"Participants Added (Already On File)",new TabSetting(120),"REPORT: RPT13",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, "C/O",
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        getReports().setDisplayColumns(2, "C/O",
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        getReports().setDisplayColumns(3, "C/O",
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        getReports().setDisplayColumns(4, "C/O",
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        getReports().setDisplayColumns(5, "C/O",
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        getReports().setDisplayColumns(6, "C/O",
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        getReports().setDisplayColumns(7, "C/O",
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        getReports().setDisplayColumns(8, "C/O",
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        getReports().setDisplayColumns(9, "C/O",
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        getReports().setDisplayColumns(10, "C/O",
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        getReports().setDisplayColumns(11, "C/O",
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        getReports().setDisplayColumns(12, "C/O",
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
        getReports().setDisplayColumns(13, "C/O",
        		ldaTwrl464a.getIra_Record_Ira_Company_Code(),"SR/CE",
        		pnd_Source_Code,"Contract",
        		ldaTwrl464a.getIra_Record_Ira_Contract(),"PY/EE",
        		ldaTwrl464a.getIra_Record_Ira_Payee(),"Tax ID",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id(),"T/Y",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Id_Type(),"Pin ID",
        		ldaTwrl464a.getIra_Record_Ira_Pin(),"Name",
        		ldaTwrl464a.getIra_Record_Ira_Name(),"Simulation",
        		ldaTwrl464a.getIra_Record_Ira_Simulation_Date(), new ReportEditMask ("XXXX/XX/XX"),"RS/DN",
        		ldaTwrl464a.getIra_Record_Ira_Residency_Code(),"Loc",
        		ldaTwrl464a.getIra_Record_Ira_Locality_Code(),"CI/TI",
        		ldaTwrl464a.getIra_Record_Ira_Citizenship_Code(),"Tax/Year",
        		ldaTwrl464a.getIra_Record_Ira_Tax_Year(),"D-O-B",
        		ldaTwrl464a.getIra_Record_Ira_Dob(), new ReportEditMask ("XXXX/XX/XX"));
    }
}
