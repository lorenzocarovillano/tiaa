/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:37:48 PM
**        * FROM NATURAL PROGRAM : Twrp3554
************************************************************
**        * FILE NAME            : Twrp3554.java
**        * CLASS NAME           : Twrp3554
**        * INSTANCE NAME        : Twrp3554
************************************************************
************************************************************************
** PROGRAM : TWRP3554
** SYSTEM  : TAXWARS
** AUTHOR  : JON ROTHOLZ
** FUNCTION: STATES CORRECTION REPORTING
**           USED FOR TAX YEARS >= 2011
** HISTORY.....:
**    02/04/2015 FENDAYA-RESTOW TO PICK UP NEW VERSION OF TWRAFORM.
**    06/12/2012 MAGNETIC MEDIA STATES CORRECTION REPORTING
**    10/05/2018 ARIVU - EIN CHANGE - TAG: EINCHG
**    04/04/2019 VIKRAM - RESTOW COMPONENT TWRL3507
**    04/12/2019 PALDE - MAG-MEDIA CHANGES FOR ALL STATES  /*DP
**    10/13/2020 - RE-STOW COMPONENT FOR 5498           /* SECURE-ACT
** 12/04/2020 - RE-STOW COMPONENT FOR 5498 IRS REPORTING  2020
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3554 extends BLNatBase
{
    // Data Areas
    private PdaTwracom2 pdaTwracom2;
    private PdaTwraform pdaTwraform;
    private PdaTwratbl2 pdaTwratbl2;
    private PdaTwratbl4 pdaTwratbl4;
    private PdaTwradist pdaTwradist;
    private PdaTwratin pdaTwratin;
    private LdaTwrl9710 ldaTwrl9710;
    private LdaTwrl9715 ldaTwrl9715;
    private PdaTwra5052 pdaTwra5052;
    private LdaTwrl3321 ldaTwrl3321;
    private LdaTwrl3507 ldaTwrl3507;
    private LdaTwrl3323 ldaTwrl3323;
    private LdaTwrl3324 ldaTwrl3324;
    private LdaTwrl3325 ldaTwrl3325;
    private LdaTwrl3326 ldaTwrl3326;
    private LdaTwrl3508 ldaTwrl3508;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_form_U;
    private DbsField form_U_Tirf_Lu_User;
    private DbsField form_U_Tirf_Lu_Ts;
    private DbsField form_U_Count_Casttirf_1099_R_State_Grp;

    private DbsGroup form_U_Tirf_1099_R_State_Grp;
    private DbsField form_U_Tirf_State_Hardcopy_Ind;
    private DbsField form_U_Tirf_State_Code;
    private DbsField form_U_Tirf_State_Auth_Rpt_Ind;
    private DbsField form_U_Tirf_State_Auth_Rpt_Date;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;
    private DbsField pnd_Ws_Const_Pnd_State_Max;

    private DbsGroup pnd_Ws_Const__R_Field_1;
    private DbsField pnd_Ws_Const_Pnd_State_Max_A;
    private DbsField pnd_Ws_Const_Pnd_Str_Max_Lines;
    private DbsField pnd_Ws_Const_Pnd_Stw_Max_Lines;
    private DbsField pnd_Ws_Const_Pnd_Stg_Max_Lines;
    private DbsField pnd_Special_Data;

    private DbsGroup pnd_Special_Data__R_Field_2;
    private DbsField pnd_Special_Data_Pnd_Ut_State_Id;
    private DbsField pnd_Special_Data_Pnd_Ut_State_Tax_Id;
    private DbsField pnd_Special_Data_Pnd_Ut_State_Distrib;

    private DbsGroup pnd_Special_Data__R_Field_3;
    private DbsField pnd_Special_Data_Pnd_Vt_State_Tax_Id;

    private DbsGroup pnd_Special_Data__R_Field_4;
    private DbsField pnd_Special_Data_Pnd_Ct_State_Tax_Id;

    private DbsGroup pnd_Special_Data__R_Field_5;
    private DbsField pnd_Special_Data_Pnd_De_State_Tax_Id;

    private DbsGroup pnd_Special_Data__R_Field_6;
    private DbsField pnd_Special_Data_Pnd_Wi_State_Tax_Id;

    private DbsGroup pnd_Special_Data__R_Field_7;
    private DbsField pnd_Special_Data_Pnd_Va_State_Tax_Id;

    private DbsGroup pnd_Special_Data__R_Field_8;
    private DbsField pnd_Special_Data_Pnd_La_State_Tax_Id;

    private DbsGroup pnd_Special_Data__R_Field_9;
    private DbsField pnd_Special_Data_Pnd_Ga_State_Tax_Id;

    private DbsGroup pnd_Special_Data__R_Field_10;
    private DbsField pnd_Special_Data_Pnd_Or_State_Tax_Id;
    private DbsField pnd_Special_Data_Pnd_Or_State_Code;

    private DbsGroup pnd_Special_Data__R_Field_11;
    private DbsField pnd_Special_Data_Pnd_Ks_State_Tax_Id;

    private DbsGroup pnd_Special_Data__R_Field_12;
    private DbsField pnd_Special_Data_Pnd_Nd_State_Tax_Id;

    private DbsGroup pnd_Special_Data__R_Field_13;
    private DbsField pnd_Special_Data_Pnd_Ky_State_Code;
    private DbsField pnd_Special_Data_Pnd_Ky_State_Tax_Id;

    private DbsGroup pnd_Special_Data__R_Field_14;
    private DbsField pnd_Special_Data_Pnd_Nc_State_Code;
    private DbsField pnd_Special_Data_Pnd_Nc_State_Tax_Id;

    private DbsGroup pnd_Special_Data__R_Field_15;
    private DbsField pnd_Special_Data_Pnd_Spec_Pay_Amt1_Alt;
    private DbsField pnd_Special_Data_Pnd_Spec_Filler;
    private DbsField pnd_Special_Data_Pnd_Spec_State_Tax_Withheld;
    private DbsField pnd_Special_Data_Pnd_Spec_Data_Extra;
    private DbsField pnd_Temp_State_Id_20;
    private DbsField pnd_Corr_Ind2;

    private DbsGroup pnd_A_Rec;
    private DbsField pnd_A_Rec_Pnd_Generate;

    private DbsGroup pnd_C_Rec;
    private DbsField pnd_C_Rec_Pnd_Generate;
    private DbsField pnd_C_Rec_Pnd_Num_Payees;
    private DbsField pnd_C_Rec_Pnd_Amt_1;
    private DbsField pnd_C_Rec_Pnd_Amt_2;
    private DbsField pnd_C_Rec_Pnd_Amt_4;
    private DbsField pnd_C_Rec_Pnd_Amt_5;
    private DbsField pnd_C_Rec_Pnd_Amt_9;
    private DbsField pnd_C_Rec_Pnd_Amt_A;
    private DbsField pnd_C_Rec_Pnd_Amt_B;

    private DbsGroup pnd_K_Rec;
    private DbsField pnd_K_Rec_Pnd_Generate;

    private DbsGroup pnd_K_Rec_Pnd_K_Rec_Array;
    private DbsField pnd_K_Rec_Pnd_Num_Payees;
    private DbsField pnd_K_Rec_Pnd_Amt_1;
    private DbsField pnd_K_Rec_Pnd_Amt_2;
    private DbsField pnd_K_Rec_Pnd_Amt_4;
    private DbsField pnd_K_Rec_Pnd_Amt_5;
    private DbsField pnd_K_Rec_Pnd_Amt_9;
    private DbsField pnd_K_Rec_Pnd_Amt_A;
    private DbsField pnd_K_Rec_Pnd_Amt_B;
    private DbsField pnd_K_Rec_Pnd_State_Tax;
    private DbsField pnd_K_Rec_Pnd_Local_Tax;

    private DbsGroup pnd_T_Rec;
    private DbsField pnd_T_Rec_Pnd_Num_Payees_Tiaa;
    private DbsField pnd_T_Rec_Pnd_Num_Payees_Trst;
    private DbsField pnd_Irsr_Max_Lines;
    private DbsField pnd_Prov_Min;
    private DbsField pnd_Prov_Max;

    private DbsGroup pnd_Prov_Table;
    private DbsField pnd_Prov_Table_Tircntl_State_Alpha_Code;

    private DbsGroup pnd_Hardcopy_Ws;
    private DbsField pnd_Hardcopy_Ws_Pnd_Process_Type;
    private DbsField pnd_Hardcopy_Ws_Pnd_State_Name;
    private DbsField pnd_Hardcopy_Ws_Pnd_Form_Page_Cnt;
    private DbsField pnd_Hardcopy_Ws_Pnd_Ws_Isn;
    private DbsField pnd_Hardcopy_Ws_Pnd_M1_Add1;
    private DbsField pnd_Hardcopy_Ws_Pnd_M1_Add2;
    private DbsField pnd_Hardcopy_Ws_Pnd_M1_Add3;
    private DbsField pnd_Hardcopy_Ws_Pnd_M1_Add4;
    private DbsField pnd_Hardcopy_Ws_Pnd_M1_Add5;
    private DbsField pnd_Hardcopy_Ws_Pnd_M1_Add6;
    private DbsField pnd_Hardcopy_Ws_Pnd_M1_Fullname;
    private DbsField pnd_Hardcopy_Ws_Pnd_M1_Firstname;
    private DbsField pnd_Hardcopy_Ws_Pnd_M1_Lasname;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Input_Parms;
    private DbsField pnd_Ws_Pnd_State;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_K;
    private DbsField pnd_Ws_Pnd_St_Occ;
    private DbsField pnd_Ws_Pnd_S2;
    private DbsField pnd_Ws_Pnd_Datx;
    private DbsField pnd_Ws_Pnd_Timx;
    private DbsField pnd_Ws_Pnd_Prev_Rpt_Date;
    private DbsField pnd_Ws_Pnd_Valid_State;
    private DbsField pnd_Ws_Pnd_Company_Break;
    private DbsField pnd_Ws_Pnd_Final_Break;
    private DbsField pnd_Ws_Pnd_A_Rec_Sw;
    private DbsField pnd_Ws_Pnd_Accepted_Form_Cnt;
    private DbsField pnd_Ws_Pnd_Company_Line;
    private DbsField pnd_Ws_Pnd_Hold_Company;
    private DbsField pnd_Ws_Pnd_Disp_Rec;
    private DbsField pnd_Ws_Pnd_S8;
    private DbsField pnd_Ws_Pnd_Empty_Cnt;
    private DbsField pnd_Ws_Pnd_Empty_Tot_Cnt;
    private DbsField pnd_Ws_Pnd_Empty_Frm2;
    private DbsField pnd_Ws_Pnd_Empty_Tot_Frm2;
    private DbsField pnd_Ws_Pnd_System_Year;
    private DbsField pnd_Ws_Pnd_Form_Typ;
    private DbsField pnd_Ws_Pnd_Terminate;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Ws_Pnd_Err;
    private DbsField pnd_Ws_Pnd_S3_Link;
    private DbsField pnd_Ws_Pnd_S3_Start;
    private DbsField pnd_Ws_Pnd_S3_End;
    private DbsField pnd_Ws_Pnd_S4_Start;
    private DbsField pnd_Ws_Pnd_S4_End;
    private DbsField pnd_Ws_Pnd_Seq_Num_Tiaa;
    private DbsField pnd_Ws_Pnd_Seq_Num_Trst;
    private DbsField pnd_Ws_Pnd_Corr_Idx;
    private DbsField pnd_Ws_Pnd_Temp_Seq;

    private DbsGroup pnd_Ws__R_Field_16;
    private DbsField pnd_Ws_Pnd_Temp_Seq_N;
    private DbsField pnd_Ws_Pnd_New_Res;
    private DbsField pnd_Ws_Pnd_Year_N4;

    private DbsGroup pnd_State_Table;
    private DbsField pnd_State_Table_Tircntl_Seq_Nbr;
    private DbsField pnd_State_Table_Tircntl_State_Old_Code;
    private DbsField pnd_State_Table_Tircntl_State_Alpha_Code;
    private DbsField pnd_State_Table_Tircntl_State_Full_Name;
    private DbsField pnd_State_Table_Tircntl_State_Tax_Id_Tiaa;
    private DbsField pnd_State_Table_Tircntl_State_Tax_Id_Trust;
    private DbsField pnd_State_Table_Tircntl_Comb_Fed_Ind;
    private DbsField pnd_State_Table_Tircntl_State_Ind;
    private DbsField pnd_State_Table_Tircntl_Media_Code;

    private DbsGroup pnd_State_Table_X;
    private DbsField pnd_State_Table_X_Tircntl_Seq_Nbr;
    private DbsField pnd_State_Table_X_Tircntl_State_Alpha_Code;
    private DbsField pnd_State_Table_X_Tircntl_Comb_Fed_Ind;
    private DbsField pnd_Sort_Rec;

    private DbsGroup pnd_Sort_Rec__R_Field_17;

    private DbsGroup pnd_Sort_Rec_Pnd_Sort_Rec_Detail;
    private DbsField pnd_Sort_Rec_Tirf_Tax_Year;
    private DbsField pnd_Sort_Rec_Tirf_Form_Type;
    private DbsField pnd_Sort_Rec_Tirf_Company_Cde;
    private DbsField pnd_Sort_Rec_Tirf_Tin;
    private DbsField pnd_Sort_Rec_Tirf_Contract_Nbr;
    private DbsField pnd_Sort_Rec_Tirf_Payee_Cde;
    private DbsField pnd_Sort_Rec_Tirf_Key;
    private DbsField pnd_S1_Start;

    private DbsGroup pnd_S1_Start__R_Field_18;

    private DbsGroup pnd_S1_Start_Pnd_S1_Detail;
    private DbsField pnd_S1_Start_Tirf_Tax_Year;
    private DbsField pnd_S1_Start_Tirf_Tin;
    private DbsField pnd_S1_Start_Tirf_Form_Type;
    private DbsField pnd_S1_Start_Tirf_Contract_Nbr;
    private DbsField pnd_S1_Start_Tirf_Payee_Cde;
    private DbsField pnd_S1_Start_Tirf_Key;
    private DbsField pnd_S1_End;

    private DbsGroup pnd_Case_Fields2;
    private DbsField pnd_Case_Fields2_Pnd_Recon_Only;
    private DbsField pnd_Case_Fields2_Pnd_Recon_And_Reporting;
    private DbsField pnd_Case_Fields2_Pnd_Special_Processing;

    private DbsGroup pnd_Case_Fields;
    private DbsField pnd_Case_Fields_Pnd_State_Dist;
    private DbsField pnd_Case_Fields_Pnd_St_Occ_A;
    private DbsField pnd_Case_Fields_Pnd_St_Occ_I;
    private DbsField pnd_Case_Fields_Pnd_St_Max;
    private DbsField pnd_Case_Fields_Pnd_Active_Isn;
    private DbsField pnd_Case_Fields_Pnd_State_Added;
    private DbsField pnd_Case_Fields_Pnd_Prior_Reported;
    private DbsField pnd_Case_Fields_Pnd_Prior_Held;
    private DbsField pnd_Case_Fields_Pnd_Prior_Record;
    private DbsField pnd_Case_Fields_Pnd_Latest_Form;
    private DbsField pnd_Case_Fields_Pnd_No_Change;
    private DbsField pnd_Case_Fields_Pnd_Recon_Summary;
    private DbsField pnd_Case_Fields_Pnd_Recon_Detail;
    private DbsField pnd_Case_Fields_Pnd_Process_Recon_Reporting;
    private DbsField pnd_Case_Fields_Pnd_Process_Recon;
    private DbsField pnd_Case_Fields_Pnd_Prior_Reported_For_Reporting;
    private DbsField pnd_Case_Fields_Pnd_Prior_Recon;
    private DbsField pnd_Case_Fields_Pnd_Process_Recon_Reporting_Only;
    private DbsField pnd_Case_Fields_Pnd_Process_Reporting;
    private DbsField pnd_Case_Fields_Pnd_Process_Recon_Active_Rec;
    private DbsField pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec;
    private DbsField pnd_Case_Fields_Pnd_Update_With_Reporting;
    private DbsField pnd_Case_Fields_Pnd_Already_Reported;
    private DbsField pnd_Case_Fields_Pnd_Already_Recon;
    private DbsField pnd_Case_Fields_Pnd_Update_Ind;
    private DbsField pnd_Case_Fields_Pnd_Geo;

    private DbsGroup pnd_Report_Indexes;
    private DbsField pnd_Report_Indexes_Pnd_Str1;
    private DbsField pnd_Report_Indexes_Pnd_Stw1;
    private DbsField pnd_Report_Indexes_Pnd_Stw2;
    private DbsField pnd_Report_Indexes_Pnd_Stg1;
    private DbsField pnd_Report_Indexes_Pnd_Irsr1;

    private DbsGroup pnd_Str;
    private DbsField pnd_Str_Pnd_Header1;
    private DbsField pnd_Str_Pnd_Header2;

    private DbsGroup pnd_Str_Pnd_Totals;
    private DbsField pnd_Str_Pnd_Form_Cnt;
    private DbsField pnd_Str_Pnd_State_Distr;
    private DbsField pnd_Str_Pnd_State_Tax;
    private DbsField pnd_Str_Pnd_Local_Distr;
    private DbsField pnd_Str_Pnd_Local_Tax;

    private DbsGroup pnd_Stw;
    private DbsField pnd_Stw_Pnd_Header;

    private DbsGroup pnd_Stw_Pnd_Totals;
    private DbsField pnd_Stw_Pnd_Form_Cnt;
    private DbsField pnd_Stw_Pnd_State_Distr;
    private DbsField pnd_Stw_Pnd_State_Tax;
    private DbsField pnd_Stw_Pnd_Local_Distr;
    private DbsField pnd_Stw_Pnd_Local_Tax;

    private DbsGroup pnd_Stg;
    private DbsField pnd_Stg_Pnd_Header;

    private DbsGroup pnd_Stg_Pnd_Totals;
    private DbsField pnd_Stg_Pnd_Form_Cnt;
    private DbsField pnd_Stg_Pnd_State_Distr;
    private DbsField pnd_Stg_Pnd_State_Tax;
    private DbsField pnd_Stg_Pnd_Local_Distr;
    private DbsField pnd_Stg_Pnd_Local_Tax;

    private DbsGroup pnd_Sta;
    private DbsField pnd_Sta_Pnd_Form_Cnt;
    private DbsField pnd_Sta_Pnd_State_Distr;
    private DbsField pnd_Sta_Pnd_State_Tax;
    private DbsField pnd_Sta_Pnd_Local_Distr;
    private DbsField pnd_Sta_Pnd_Local_Tax;

    private DbsGroup pnd_Recon_Case;
    private DbsField pnd_Recon_Case_Pnd_Tin;
    private DbsField pnd_Recon_Case_Pnd_Cntrct_Py;

    private DbsGroup pnd_Recon_Case_Pnd_Recon_Case_Detail;
    private DbsField pnd_Recon_Case_Pnd_Header;
    private DbsField pnd_Recon_Case_Pnd_Form_Cnt;
    private DbsField pnd_Recon_Case_Pnd_State_Distr;
    private DbsField pnd_Recon_Case_Pnd_State_Tax;
    private DbsField pnd_Recon_Case_Pnd_Local_Distr;
    private DbsField pnd_Recon_Case_Pnd_Local_Tax;

    private DbsGroup pnd_Hold_Fields;
    private DbsField pnd_Hold_Fields_Pnd_Hold_Comb_Fed_Ind;
    private DbsField pnd_Hold_Fields_Pnd_Hold_Rej_Ind;
    private DbsField pnd_Irs_B_Special_Entry_Temp_De;
    private DbsField pnd_Irs_B_Special_Entry_Temp_Ga;
    private DbsField pnd_Irs_B_Special_Entry_Temp_Ks;
    private DbsField pnd_Irs_B_Special_Entry_Temp_La;
    private DbsField pnd_Irs_B_Special_Entry_Temp_Or;
    private DbsField pnd_Irs_B_Special_Entry_Temp_Va;
    private DbsField pnd_Irs_B_Special_Entry_Temp_Wi;
    private DbsField pnd_Irs_B_Special_Entry_Temp_Nd;
    private DbsField pnd_Seg2;
    private DbsField pnd_Seg3;
    private DbsField pnd_Irs_A_Filler_4_Ne;

    private DbsGroup pnd_Irs_A_Filler_4_Ne__R_Field_19;
    private DbsField pnd_Irs_A_Filler_4_Ne_Pnd_Irs_A_Filler_Sp1;
    private DbsField pnd_Irs_A_Filler_4_Ne_Pnd_Irs_A_Filler_Sp2;
    private DbsField pnd_Irs_A_Filler_4_Ne_Pnd_Irs_A_Filler_Sp3;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Tirf_Company_CdeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwracom2 = new PdaTwracom2(localVariables);
        pdaTwraform = new PdaTwraform(localVariables);
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        pdaTwratbl4 = new PdaTwratbl4(localVariables);
        pdaTwradist = new PdaTwradist(localVariables);
        pdaTwratin = new PdaTwratin(localVariables);
        ldaTwrl9710 = new LdaTwrl9710();
        registerRecord(ldaTwrl9710);
        registerRecord(ldaTwrl9710.getVw_form());
        ldaTwrl9715 = new LdaTwrl9715();
        registerRecord(ldaTwrl9715);
        registerRecord(ldaTwrl9715.getVw_form_R());
        pdaTwra5052 = new PdaTwra5052(localVariables);
        ldaTwrl3321 = new LdaTwrl3321();
        registerRecord(ldaTwrl3321);
        ldaTwrl3507 = new LdaTwrl3507();
        registerRecord(ldaTwrl3507);
        ldaTwrl3323 = new LdaTwrl3323();
        registerRecord(ldaTwrl3323);
        ldaTwrl3324 = new LdaTwrl3324();
        registerRecord(ldaTwrl3324);
        ldaTwrl3325 = new LdaTwrl3325();
        registerRecord(ldaTwrl3325);
        ldaTwrl3326 = new LdaTwrl3326();
        registerRecord(ldaTwrl3326);
        ldaTwrl3508 = new LdaTwrl3508();
        registerRecord(ldaTwrl3508);

        // Local Variables

        vw_form_U = new DataAccessProgramView(new NameInfo("vw_form_U", "FORM-U"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE", DdmPeriodicGroups.getInstance().getGroups("TWRFRM_FORM_FILE"));
        form_U_Tirf_Lu_User = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Lu_User", "TIRF-LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_LU_USER");
        form_U_Tirf_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        form_U_Tirf_Lu_Ts = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Lu_Ts", "TIRF-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "TIRF_LU_TS");
        form_U_Tirf_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        form_U_Count_Casttirf_1099_R_State_Grp = vw_form_U.getRecord().newFieldInGroup("form_U_Count_Casttirf_1099_R_State_Grp", "C*TIRF-1099-R-STATE-GRP", 
            RepeatingFieldStrategy.CAsteriskVariable, "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");

        form_U_Tirf_1099_R_State_Grp = vw_form_U.getRecord().newGroupInGroup("form_U_Tirf_1099_R_State_Grp", "TIRF-1099-R-STATE-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_U_Tirf_1099_R_State_Grp.setDdmHeader("1099-R/STATE/GROUP");
        form_U_Tirf_State_Hardcopy_Ind = form_U_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_U_Tirf_State_Hardcopy_Ind", "TIRF-STATE-HARDCOPY-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_HARDCOPY_IND", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_U_Tirf_State_Hardcopy_Ind.setDdmHeader("HARD-/COPY/IND");
        form_U_Tirf_State_Code = form_U_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_U_Tirf_State_Code", "TIRF-STATE-CODE", FieldType.STRING, 2, new 
            DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_CODE", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_U_Tirf_State_Code.setDdmHeader("RESI-/DENCY/CODE");
        form_U_Tirf_State_Auth_Rpt_Ind = form_U_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_U_Tirf_State_Auth_Rpt_Ind", "TIRF-STATE-AUTH-RPT-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_AUTH_RPT_IND", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_U_Tirf_State_Auth_Rpt_Ind.setDdmHeader("STATE/RPT/IND");
        form_U_Tirf_State_Auth_Rpt_Date = form_U_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_U_Tirf_State_Auth_Rpt_Date", "TIRF-STATE-AUTH-RPT-DATE", 
            FieldType.DATE, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_AUTH_RPT_DATE", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_U_Tirf_State_Auth_Rpt_Date.setDdmHeader("STATE/AUTH RPT/DATE");
        registerRecord(vw_form_U);

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_Pnd_State_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_State_Max", "#STATE-MAX", FieldType.NUMERIC, 2);

        pnd_Ws_Const__R_Field_1 = pnd_Ws_Const.newGroupInGroup("pnd_Ws_Const__R_Field_1", "REDEFINE", pnd_Ws_Const_Pnd_State_Max);
        pnd_Ws_Const_Pnd_State_Max_A = pnd_Ws_Const__R_Field_1.newFieldInGroup("pnd_Ws_Const_Pnd_State_Max_A", "#STATE-MAX-A", FieldType.STRING, 2);
        pnd_Ws_Const_Pnd_Str_Max_Lines = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_Str_Max_Lines", "#STR-MAX-LINES", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Const_Pnd_Stw_Max_Lines = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_Stw_Max_Lines", "#STW-MAX-LINES", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Const_Pnd_Stg_Max_Lines = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_Stg_Max_Lines", "#STG-MAX-LINES", FieldType.PACKED_DECIMAL, 3);
        pnd_Special_Data = localVariables.newFieldInRecord("pnd_Special_Data", "#SPECIAL-DATA", FieldType.STRING, 60);

        pnd_Special_Data__R_Field_2 = localVariables.newGroupInRecord("pnd_Special_Data__R_Field_2", "REDEFINE", pnd_Special_Data);
        pnd_Special_Data_Pnd_Ut_State_Id = pnd_Special_Data__R_Field_2.newFieldInGroup("pnd_Special_Data_Pnd_Ut_State_Id", "#UT-STATE-ID", FieldType.STRING, 
            2);
        pnd_Special_Data_Pnd_Ut_State_Tax_Id = pnd_Special_Data__R_Field_2.newFieldInGroup("pnd_Special_Data_Pnd_Ut_State_Tax_Id", "#UT-STATE-TAX-ID", 
            FieldType.STRING, 14);
        pnd_Special_Data_Pnd_Ut_State_Distrib = pnd_Special_Data__R_Field_2.newFieldInGroup("pnd_Special_Data_Pnd_Ut_State_Distrib", "#UT-STATE-DISTRIB", 
            FieldType.NUMERIC, 13, 2);

        pnd_Special_Data__R_Field_3 = localVariables.newGroupInRecord("pnd_Special_Data__R_Field_3", "REDEFINE", pnd_Special_Data);
        pnd_Special_Data_Pnd_Vt_State_Tax_Id = pnd_Special_Data__R_Field_3.newFieldInGroup("pnd_Special_Data_Pnd_Vt_State_Tax_Id", "#VT-STATE-TAX-ID", 
            FieldType.STRING, 15);

        pnd_Special_Data__R_Field_4 = localVariables.newGroupInRecord("pnd_Special_Data__R_Field_4", "REDEFINE", pnd_Special_Data);
        pnd_Special_Data_Pnd_Ct_State_Tax_Id = pnd_Special_Data__R_Field_4.newFieldInGroup("pnd_Special_Data_Pnd_Ct_State_Tax_Id", "#CT-STATE-TAX-ID", 
            FieldType.STRING, 15);

        pnd_Special_Data__R_Field_5 = localVariables.newGroupInRecord("pnd_Special_Data__R_Field_5", "REDEFINE", pnd_Special_Data);
        pnd_Special_Data_Pnd_De_State_Tax_Id = pnd_Special_Data__R_Field_5.newFieldInGroup("pnd_Special_Data_Pnd_De_State_Tax_Id", "#DE-STATE-TAX-ID", 
            FieldType.STRING, 15);

        pnd_Special_Data__R_Field_6 = localVariables.newGroupInRecord("pnd_Special_Data__R_Field_6", "REDEFINE", pnd_Special_Data);
        pnd_Special_Data_Pnd_Wi_State_Tax_Id = pnd_Special_Data__R_Field_6.newFieldInGroup("pnd_Special_Data_Pnd_Wi_State_Tax_Id", "#WI-STATE-TAX-ID", 
            FieldType.STRING, 15);

        pnd_Special_Data__R_Field_7 = localVariables.newGroupInRecord("pnd_Special_Data__R_Field_7", "REDEFINE", pnd_Special_Data);
        pnd_Special_Data_Pnd_Va_State_Tax_Id = pnd_Special_Data__R_Field_7.newFieldInGroup("pnd_Special_Data_Pnd_Va_State_Tax_Id", "#VA-STATE-TAX-ID", 
            FieldType.STRING, 15);

        pnd_Special_Data__R_Field_8 = localVariables.newGroupInRecord("pnd_Special_Data__R_Field_8", "REDEFINE", pnd_Special_Data);
        pnd_Special_Data_Pnd_La_State_Tax_Id = pnd_Special_Data__R_Field_8.newFieldInGroup("pnd_Special_Data_Pnd_La_State_Tax_Id", "#LA-STATE-TAX-ID", 
            FieldType.STRING, 15);

        pnd_Special_Data__R_Field_9 = localVariables.newGroupInRecord("pnd_Special_Data__R_Field_9", "REDEFINE", pnd_Special_Data);
        pnd_Special_Data_Pnd_Ga_State_Tax_Id = pnd_Special_Data__R_Field_9.newFieldInGroup("pnd_Special_Data_Pnd_Ga_State_Tax_Id", "#GA-STATE-TAX-ID", 
            FieldType.STRING, 15);

        pnd_Special_Data__R_Field_10 = localVariables.newGroupInRecord("pnd_Special_Data__R_Field_10", "REDEFINE", pnd_Special_Data);
        pnd_Special_Data_Pnd_Or_State_Tax_Id = pnd_Special_Data__R_Field_10.newFieldInGroup("pnd_Special_Data_Pnd_Or_State_Tax_Id", "#OR-STATE-TAX-ID", 
            FieldType.STRING, 17);
        pnd_Special_Data_Pnd_Or_State_Code = pnd_Special_Data__R_Field_10.newFieldInGroup("pnd_Special_Data_Pnd_Or_State_Code", "#OR-STATE-CODE", FieldType.STRING, 
            2);

        pnd_Special_Data__R_Field_11 = localVariables.newGroupInRecord("pnd_Special_Data__R_Field_11", "REDEFINE", pnd_Special_Data);
        pnd_Special_Data_Pnd_Ks_State_Tax_Id = pnd_Special_Data__R_Field_11.newFieldInGroup("pnd_Special_Data_Pnd_Ks_State_Tax_Id", "#KS-STATE-TAX-ID", 
            FieldType.STRING, 19);

        pnd_Special_Data__R_Field_12 = localVariables.newGroupInRecord("pnd_Special_Data__R_Field_12", "REDEFINE", pnd_Special_Data);
        pnd_Special_Data_Pnd_Nd_State_Tax_Id = pnd_Special_Data__R_Field_12.newFieldInGroup("pnd_Special_Data_Pnd_Nd_State_Tax_Id", "#ND-STATE-TAX-ID", 
            FieldType.STRING, 15);

        pnd_Special_Data__R_Field_13 = localVariables.newGroupInRecord("pnd_Special_Data__R_Field_13", "REDEFINE", pnd_Special_Data);
        pnd_Special_Data_Pnd_Ky_State_Code = pnd_Special_Data__R_Field_13.newFieldInGroup("pnd_Special_Data_Pnd_Ky_State_Code", "#KY-STATE-CODE", FieldType.STRING, 
            2);
        pnd_Special_Data_Pnd_Ky_State_Tax_Id = pnd_Special_Data__R_Field_13.newFieldInGroup("pnd_Special_Data_Pnd_Ky_State_Tax_Id", "#KY-STATE-TAX-ID", 
            FieldType.STRING, 15);

        pnd_Special_Data__R_Field_14 = localVariables.newGroupInRecord("pnd_Special_Data__R_Field_14", "REDEFINE", pnd_Special_Data);
        pnd_Special_Data_Pnd_Nc_State_Code = pnd_Special_Data__R_Field_14.newFieldInGroup("pnd_Special_Data_Pnd_Nc_State_Code", "#NC-STATE-CODE", FieldType.STRING, 
            2);
        pnd_Special_Data_Pnd_Nc_State_Tax_Id = pnd_Special_Data__R_Field_14.newFieldInGroup("pnd_Special_Data_Pnd_Nc_State_Tax_Id", "#NC-STATE-TAX-ID", 
            FieldType.STRING, 15);

        pnd_Special_Data__R_Field_15 = localVariables.newGroupInRecord("pnd_Special_Data__R_Field_15", "REDEFINE", pnd_Special_Data);
        pnd_Special_Data_Pnd_Spec_Pay_Amt1_Alt = pnd_Special_Data__R_Field_15.newFieldInGroup("pnd_Special_Data_Pnd_Spec_Pay_Amt1_Alt", "#SPEC-PAY-AMT1-ALT", 
            FieldType.NUMERIC, 10, 2);
        pnd_Special_Data_Pnd_Spec_Filler = pnd_Special_Data__R_Field_15.newFieldInGroup("pnd_Special_Data_Pnd_Spec_Filler", "#SPEC-FILLER", FieldType.STRING, 
            10);
        pnd_Special_Data_Pnd_Spec_State_Tax_Withheld = pnd_Special_Data__R_Field_15.newFieldInGroup("pnd_Special_Data_Pnd_Spec_State_Tax_Withheld", "#SPEC-STATE-TAX-WITHHELD", 
            FieldType.NUMERIC, 10, 2);
        pnd_Special_Data_Pnd_Spec_Data_Extra = pnd_Special_Data__R_Field_15.newFieldInGroup("pnd_Special_Data_Pnd_Spec_Data_Extra", "#SPEC-DATA-EXTRA", 
            FieldType.STRING, 30);
        pnd_Temp_State_Id_20 = localVariables.newFieldInRecord("pnd_Temp_State_Id_20", "#TEMP-STATE-ID-20", FieldType.STRING, 20);
        pnd_Corr_Ind2 = localVariables.newFieldArrayInRecord("pnd_Corr_Ind2", "#CORR-IND2", FieldType.STRING, 1, new DbsArrayController(1, 3));

        pnd_A_Rec = localVariables.newGroupArrayInRecord("pnd_A_Rec", "#A-REC", new DbsArrayController(1, 3));
        pnd_A_Rec_Pnd_Generate = pnd_A_Rec.newFieldInGroup("pnd_A_Rec_Pnd_Generate", "#GENERATE", FieldType.BOOLEAN, 1);

        pnd_C_Rec = localVariables.newGroupArrayInRecord("pnd_C_Rec", "#C-REC", new DbsArrayController(1, 3));
        pnd_C_Rec_Pnd_Generate = pnd_C_Rec.newFieldInGroup("pnd_C_Rec_Pnd_Generate", "#GENERATE", FieldType.BOOLEAN, 1);
        pnd_C_Rec_Pnd_Num_Payees = pnd_C_Rec.newFieldInGroup("pnd_C_Rec_Pnd_Num_Payees", "#NUM-PAYEES", FieldType.PACKED_DECIMAL, 8);
        pnd_C_Rec_Pnd_Amt_1 = pnd_C_Rec.newFieldInGroup("pnd_C_Rec_Pnd_Amt_1", "#AMT-1", FieldType.PACKED_DECIMAL, 18, 2);
        pnd_C_Rec_Pnd_Amt_2 = pnd_C_Rec.newFieldInGroup("pnd_C_Rec_Pnd_Amt_2", "#AMT-2", FieldType.PACKED_DECIMAL, 18, 2);
        pnd_C_Rec_Pnd_Amt_4 = pnd_C_Rec.newFieldInGroup("pnd_C_Rec_Pnd_Amt_4", "#AMT-4", FieldType.PACKED_DECIMAL, 18, 2);
        pnd_C_Rec_Pnd_Amt_5 = pnd_C_Rec.newFieldInGroup("pnd_C_Rec_Pnd_Amt_5", "#AMT-5", FieldType.PACKED_DECIMAL, 18, 2);
        pnd_C_Rec_Pnd_Amt_9 = pnd_C_Rec.newFieldInGroup("pnd_C_Rec_Pnd_Amt_9", "#AMT-9", FieldType.PACKED_DECIMAL, 18, 2);
        pnd_C_Rec_Pnd_Amt_A = pnd_C_Rec.newFieldInGroup("pnd_C_Rec_Pnd_Amt_A", "#AMT-A", FieldType.PACKED_DECIMAL, 18, 2);
        pnd_C_Rec_Pnd_Amt_B = pnd_C_Rec.newFieldInGroup("pnd_C_Rec_Pnd_Amt_B", "#AMT-B", FieldType.PACKED_DECIMAL, 18, 2);

        pnd_K_Rec = localVariables.newGroupArrayInRecord("pnd_K_Rec", "#K-REC", new DbsArrayController(1, 3));
        pnd_K_Rec_Pnd_Generate = pnd_K_Rec.newFieldInGroup("pnd_K_Rec_Pnd_Generate", "#GENERATE", FieldType.BOOLEAN, 1);

        pnd_K_Rec_Pnd_K_Rec_Array = pnd_K_Rec.newGroupArrayInGroup("pnd_K_Rec_Pnd_K_Rec_Array", "#K-REC-ARRAY", new DbsArrayController(1, 57));
        pnd_K_Rec_Pnd_Num_Payees = pnd_K_Rec_Pnd_K_Rec_Array.newFieldInGroup("pnd_K_Rec_Pnd_Num_Payees", "#NUM-PAYEES", FieldType.PACKED_DECIMAL, 8);
        pnd_K_Rec_Pnd_Amt_1 = pnd_K_Rec_Pnd_K_Rec_Array.newFieldInGroup("pnd_K_Rec_Pnd_Amt_1", "#AMT-1", FieldType.PACKED_DECIMAL, 18, 2);
        pnd_K_Rec_Pnd_Amt_2 = pnd_K_Rec_Pnd_K_Rec_Array.newFieldInGroup("pnd_K_Rec_Pnd_Amt_2", "#AMT-2", FieldType.PACKED_DECIMAL, 18, 2);
        pnd_K_Rec_Pnd_Amt_4 = pnd_K_Rec_Pnd_K_Rec_Array.newFieldInGroup("pnd_K_Rec_Pnd_Amt_4", "#AMT-4", FieldType.PACKED_DECIMAL, 18, 2);
        pnd_K_Rec_Pnd_Amt_5 = pnd_K_Rec_Pnd_K_Rec_Array.newFieldInGroup("pnd_K_Rec_Pnd_Amt_5", "#AMT-5", FieldType.PACKED_DECIMAL, 18, 2);
        pnd_K_Rec_Pnd_Amt_9 = pnd_K_Rec_Pnd_K_Rec_Array.newFieldInGroup("pnd_K_Rec_Pnd_Amt_9", "#AMT-9", FieldType.PACKED_DECIMAL, 18, 2);
        pnd_K_Rec_Pnd_Amt_A = pnd_K_Rec_Pnd_K_Rec_Array.newFieldInGroup("pnd_K_Rec_Pnd_Amt_A", "#AMT-A", FieldType.PACKED_DECIMAL, 18, 2);
        pnd_K_Rec_Pnd_Amt_B = pnd_K_Rec_Pnd_K_Rec_Array.newFieldInGroup("pnd_K_Rec_Pnd_Amt_B", "#AMT-B", FieldType.PACKED_DECIMAL, 18, 2);
        pnd_K_Rec_Pnd_State_Tax = pnd_K_Rec_Pnd_K_Rec_Array.newFieldInGroup("pnd_K_Rec_Pnd_State_Tax", "#STATE-TAX", FieldType.PACKED_DECIMAL, 18, 2);
        pnd_K_Rec_Pnd_Local_Tax = pnd_K_Rec_Pnd_K_Rec_Array.newFieldInGroup("pnd_K_Rec_Pnd_Local_Tax", "#LOCAL-TAX", FieldType.PACKED_DECIMAL, 18, 2);

        pnd_T_Rec = localVariables.newGroupInRecord("pnd_T_Rec", "#T-REC");
        pnd_T_Rec_Pnd_Num_Payees_Tiaa = pnd_T_Rec.newFieldInGroup("pnd_T_Rec_Pnd_Num_Payees_Tiaa", "#NUM-PAYEES-TIAA", FieldType.PACKED_DECIMAL, 8);
        pnd_T_Rec_Pnd_Num_Payees_Trst = pnd_T_Rec.newFieldInGroup("pnd_T_Rec_Pnd_Num_Payees_Trst", "#NUM-PAYEES-TRST", FieldType.PACKED_DECIMAL, 8);
        pnd_Irsr_Max_Lines = localVariables.newFieldInRecord("pnd_Irsr_Max_Lines", "#IRSR-MAX-LINES", FieldType.PACKED_DECIMAL, 3);
        pnd_Prov_Min = localVariables.newFieldInRecord("pnd_Prov_Min", "#PROV-MIN", FieldType.PACKED_DECIMAL, 3);
        pnd_Prov_Max = localVariables.newFieldInRecord("pnd_Prov_Max", "#PROV-MAX", FieldType.PACKED_DECIMAL, 3);

        pnd_Prov_Table = localVariables.newGroupArrayInRecord("pnd_Prov_Table", "#PROV-TABLE", new DbsArrayController(74, 88));
        pnd_Prov_Table_Tircntl_State_Alpha_Code = pnd_Prov_Table.newFieldInGroup("pnd_Prov_Table_Tircntl_State_Alpha_Code", "TIRCNTL-STATE-ALPHA-CODE", 
            FieldType.STRING, 3);

        pnd_Hardcopy_Ws = localVariables.newGroupInRecord("pnd_Hardcopy_Ws", "#HARDCOPY-WS");
        pnd_Hardcopy_Ws_Pnd_Process_Type = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_Process_Type", "#PROCESS-TYPE", FieldType.STRING, 1);
        pnd_Hardcopy_Ws_Pnd_State_Name = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_State_Name", "#STATE-NAME", FieldType.STRING, 19);
        pnd_Hardcopy_Ws_Pnd_Form_Page_Cnt = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_Form_Page_Cnt", "#FORM-PAGE-CNT", FieldType.PACKED_DECIMAL, 
            2);
        pnd_Hardcopy_Ws_Pnd_Ws_Isn = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_Ws_Isn", "#WS-ISN", FieldType.NUMERIC, 8);
        pnd_Hardcopy_Ws_Pnd_M1_Add1 = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_M1_Add1", "#M1-ADD1", FieldType.STRING, 35);
        pnd_Hardcopy_Ws_Pnd_M1_Add2 = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_M1_Add2", "#M1-ADD2", FieldType.STRING, 35);
        pnd_Hardcopy_Ws_Pnd_M1_Add3 = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_M1_Add3", "#M1-ADD3", FieldType.STRING, 35);
        pnd_Hardcopy_Ws_Pnd_M1_Add4 = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_M1_Add4", "#M1-ADD4", FieldType.STRING, 35);
        pnd_Hardcopy_Ws_Pnd_M1_Add5 = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_M1_Add5", "#M1-ADD5", FieldType.STRING, 35);
        pnd_Hardcopy_Ws_Pnd_M1_Add6 = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_M1_Add6", "#M1-ADD6", FieldType.STRING, 35);
        pnd_Hardcopy_Ws_Pnd_M1_Fullname = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_M1_Fullname", "#M1-FULLNAME", FieldType.STRING, 35);
        pnd_Hardcopy_Ws_Pnd_M1_Firstname = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_M1_Firstname", "#M1-FIRSTNAME", FieldType.STRING, 35);
        pnd_Hardcopy_Ws_Pnd_M1_Lasname = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_M1_Lasname", "#M1-LASNAME", FieldType.STRING, 35);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Input_Parms = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Ws_Pnd_State = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_K = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_St_Occ = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_St_Occ", "#ST-OCC", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S2", "#S2", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Datx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Datx", "#DATX", FieldType.DATE);
        pnd_Ws_Pnd_Timx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Timx", "#TIMX", FieldType.TIME);
        pnd_Ws_Pnd_Prev_Rpt_Date = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Rpt_Date", "#PREV-RPT-DATE", FieldType.DATE);
        pnd_Ws_Pnd_Valid_State = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Valid_State", "#VALID-STATE", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Company_Break = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Break", "#COMPANY-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Final_Break = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Final_Break", "#FINAL-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_A_Rec_Sw = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_A_Rec_Sw", "#A-REC-SW", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Accepted_Form_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Accepted_Form_Cnt", "#ACCEPTED-FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Company_Line = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Line", "#COMPANY-LINE", FieldType.STRING, 72);
        pnd_Ws_Pnd_Hold_Company = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Hold_Company", "#HOLD-COMPANY", FieldType.STRING, 1);
        pnd_Ws_Pnd_Disp_Rec = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Disp_Rec", "#DISP-REC", FieldType.STRING, 5);
        pnd_Ws_Pnd_S8 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S8", "#S8", FieldType.STRING, 8);
        pnd_Ws_Pnd_Empty_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Empty_Cnt", "#EMPTY-CNT", FieldType.NUMERIC, 5);
        pnd_Ws_Pnd_Empty_Tot_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Empty_Tot_Cnt", "#EMPTY-TOT-CNT", FieldType.NUMERIC, 5);
        pnd_Ws_Pnd_Empty_Frm2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Empty_Frm2", "#EMPTY-FRM2", FieldType.NUMERIC, 5);
        pnd_Ws_Pnd_Empty_Tot_Frm2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Empty_Tot_Frm2", "#EMPTY-TOT-FRM2", FieldType.NUMERIC, 5);
        pnd_Ws_Pnd_System_Year = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_System_Year", "#SYSTEM-YEAR", FieldType.STRING, 4);
        pnd_Ws_Pnd_Form_Typ = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Form_Typ", "#FORM-TYP", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_Terminate = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Terminate", "#TERMINATE", FieldType.NUMERIC, 3);
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Err = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Err", "#ERR", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S3_Link = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S3_Link", "#S3-LINK", FieldType.STRING, 33);
        pnd_Ws_Pnd_S3_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S3_Start", "#S3-START", FieldType.STRING, 8);
        pnd_Ws_Pnd_S3_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S3_End", "#S3-END", FieldType.STRING, 8);
        pnd_Ws_Pnd_S4_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S4_Start", "#S4-START", FieldType.STRING, 32);
        pnd_Ws_Pnd_S4_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S4_End", "#S4-END", FieldType.STRING, 32);
        pnd_Ws_Pnd_Seq_Num_Tiaa = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Seq_Num_Tiaa", "#SEQ-NUM-TIAA", FieldType.PACKED_DECIMAL, 9);
        pnd_Ws_Pnd_Seq_Num_Trst = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Seq_Num_Trst", "#SEQ-NUM-TRST", FieldType.PACKED_DECIMAL, 9);
        pnd_Ws_Pnd_Corr_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Corr_Idx", "#CORR-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Temp_Seq = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Temp_Seq", "#TEMP-SEQ", FieldType.STRING, 3);

        pnd_Ws__R_Field_16 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_16", "REDEFINE", pnd_Ws_Pnd_Temp_Seq);
        pnd_Ws_Pnd_Temp_Seq_N = pnd_Ws__R_Field_16.newFieldInGroup("pnd_Ws_Pnd_Temp_Seq_N", "#TEMP-SEQ-N", FieldType.NUMERIC, 3);
        pnd_Ws_Pnd_New_Res = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_New_Res", "#NEW-RES", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Year_N4 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Year_N4", "#YEAR-N4", FieldType.NUMERIC, 4);

        pnd_State_Table = localVariables.newGroupInRecord("pnd_State_Table", "#STATE-TABLE");
        pnd_State_Table_Tircntl_Seq_Nbr = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 2);
        pnd_State_Table_Tircntl_State_Old_Code = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_State_Old_Code", "TIRCNTL-STATE-OLD-CODE", FieldType.STRING, 
            3);
        pnd_State_Table_Tircntl_State_Alpha_Code = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_State_Alpha_Code", "TIRCNTL-STATE-ALPHA-CODE", 
            FieldType.STRING, 3);
        pnd_State_Table_Tircntl_State_Full_Name = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_State_Full_Name", "TIRCNTL-STATE-FULL-NAME", 
            FieldType.STRING, 19);
        pnd_State_Table_Tircntl_State_Tax_Id_Tiaa = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_State_Tax_Id_Tiaa", "TIRCNTL-STATE-TAX-ID-TIAA", 
            FieldType.STRING, 20);
        pnd_State_Table_Tircntl_State_Tax_Id_Trust = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_State_Tax_Id_Trust", "TIRCNTL-STATE-TAX-ID-TRUST", 
            FieldType.STRING, 20);
        pnd_State_Table_Tircntl_Comb_Fed_Ind = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_Comb_Fed_Ind", "TIRCNTL-COMB-FED-IND", FieldType.STRING, 
            1);
        pnd_State_Table_Tircntl_State_Ind = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_State_Ind", "TIRCNTL-STATE-IND", FieldType.STRING, 
            1);
        pnd_State_Table_Tircntl_Media_Code = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_Media_Code", "TIRCNTL-MEDIA-CODE", FieldType.STRING, 
            1);

        pnd_State_Table_X = localVariables.newGroupArrayInRecord("pnd_State_Table_X", "#STATE-TABLE-X", new DbsArrayController(1, 59));
        pnd_State_Table_X_Tircntl_Seq_Nbr = pnd_State_Table_X.newFieldInGroup("pnd_State_Table_X_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 
            2);
        pnd_State_Table_X_Tircntl_State_Alpha_Code = pnd_State_Table_X.newFieldInGroup("pnd_State_Table_X_Tircntl_State_Alpha_Code", "TIRCNTL-STATE-ALPHA-CODE", 
            FieldType.STRING, 3);
        pnd_State_Table_X_Tircntl_Comb_Fed_Ind = pnd_State_Table_X.newFieldInGroup("pnd_State_Table_X_Tircntl_Comb_Fed_Ind", "TIRCNTL-COMB-FED-IND", FieldType.STRING, 
            1);
        pnd_Sort_Rec = localVariables.newFieldInRecord("pnd_Sort_Rec", "#SORT-REC", FieldType.STRING, 32);

        pnd_Sort_Rec__R_Field_17 = localVariables.newGroupInRecord("pnd_Sort_Rec__R_Field_17", "REDEFINE", pnd_Sort_Rec);

        pnd_Sort_Rec_Pnd_Sort_Rec_Detail = pnd_Sort_Rec__R_Field_17.newGroupInGroup("pnd_Sort_Rec_Pnd_Sort_Rec_Detail", "#SORT-REC-DETAIL");
        pnd_Sort_Rec_Tirf_Tax_Year = pnd_Sort_Rec_Pnd_Sort_Rec_Detail.newFieldInGroup("pnd_Sort_Rec_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Sort_Rec_Tirf_Form_Type = pnd_Sort_Rec_Pnd_Sort_Rec_Detail.newFieldInGroup("pnd_Sort_Rec_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 
            2);
        pnd_Sort_Rec_Tirf_Company_Cde = pnd_Sort_Rec_Pnd_Sort_Rec_Detail.newFieldInGroup("pnd_Sort_Rec_Tirf_Company_Cde", "TIRF-COMPANY-CDE", FieldType.STRING, 
            1);
        pnd_Sort_Rec_Tirf_Tin = pnd_Sort_Rec_Pnd_Sort_Rec_Detail.newFieldInGroup("pnd_Sort_Rec_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10);
        pnd_Sort_Rec_Tirf_Contract_Nbr = pnd_Sort_Rec_Pnd_Sort_Rec_Detail.newFieldInGroup("pnd_Sort_Rec_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 
            8);
        pnd_Sort_Rec_Tirf_Payee_Cde = pnd_Sort_Rec_Pnd_Sort_Rec_Detail.newFieldInGroup("pnd_Sort_Rec_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Sort_Rec_Tirf_Key = pnd_Sort_Rec_Pnd_Sort_Rec_Detail.newFieldInGroup("pnd_Sort_Rec_Tirf_Key", "TIRF-KEY", FieldType.STRING, 5);
        pnd_S1_Start = localVariables.newFieldInRecord("pnd_S1_Start", "#S1-START", FieldType.STRING, 32);

        pnd_S1_Start__R_Field_18 = localVariables.newGroupInRecord("pnd_S1_Start__R_Field_18", "REDEFINE", pnd_S1_Start);

        pnd_S1_Start_Pnd_S1_Detail = pnd_S1_Start__R_Field_18.newGroupInGroup("pnd_S1_Start_Pnd_S1_Detail", "#S1-DETAIL");
        pnd_S1_Start_Tirf_Tax_Year = pnd_S1_Start_Pnd_S1_Detail.newFieldInGroup("pnd_S1_Start_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 4);
        pnd_S1_Start_Tirf_Tin = pnd_S1_Start_Pnd_S1_Detail.newFieldInGroup("pnd_S1_Start_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10);
        pnd_S1_Start_Tirf_Form_Type = pnd_S1_Start_Pnd_S1_Detail.newFieldInGroup("pnd_S1_Start_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_S1_Start_Tirf_Contract_Nbr = pnd_S1_Start_Pnd_S1_Detail.newFieldInGroup("pnd_S1_Start_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 
            8);
        pnd_S1_Start_Tirf_Payee_Cde = pnd_S1_Start_Pnd_S1_Detail.newFieldInGroup("pnd_S1_Start_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 2);
        pnd_S1_Start_Tirf_Key = pnd_S1_Start_Pnd_S1_Detail.newFieldInGroup("pnd_S1_Start_Tirf_Key", "TIRF-KEY", FieldType.STRING, 5);
        pnd_S1_End = localVariables.newFieldInRecord("pnd_S1_End", "#S1-END", FieldType.STRING, 32);

        pnd_Case_Fields2 = localVariables.newGroupInRecord("pnd_Case_Fields2", "#CASE-FIELDS2");
        pnd_Case_Fields2_Pnd_Recon_Only = pnd_Case_Fields2.newFieldInGroup("pnd_Case_Fields2_Pnd_Recon_Only", "#RECON-ONLY", FieldType.BOOLEAN, 1);
        pnd_Case_Fields2_Pnd_Recon_And_Reporting = pnd_Case_Fields2.newFieldInGroup("pnd_Case_Fields2_Pnd_Recon_And_Reporting", "#RECON-AND-REPORTING", 
            FieldType.BOOLEAN, 1);
        pnd_Case_Fields2_Pnd_Special_Processing = pnd_Case_Fields2.newFieldInGroup("pnd_Case_Fields2_Pnd_Special_Processing", "#SPECIAL-PROCESSING", FieldType.BOOLEAN, 
            1);

        pnd_Case_Fields = localVariables.newGroupInRecord("pnd_Case_Fields", "#CASE-FIELDS");
        pnd_Case_Fields_Pnd_State_Dist = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_State_Dist", "#STATE-DIST", FieldType.NUMERIC, 18, 2);
        pnd_Case_Fields_Pnd_St_Occ_A = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_St_Occ_A", "#ST-OCC-A", FieldType.PACKED_DECIMAL, 3);
        pnd_Case_Fields_Pnd_St_Occ_I = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_St_Occ_I", "#ST-OCC-I", FieldType.PACKED_DECIMAL, 3);
        pnd_Case_Fields_Pnd_St_Max = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_St_Max", "#ST-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Case_Fields_Pnd_Active_Isn = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Active_Isn", "#ACTIVE-ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_Case_Fields_Pnd_State_Added = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_State_Added", "#STATE-ADDED", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Prior_Reported = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Prior_Reported", "#PRIOR-REPORTED", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Prior_Held = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Prior_Held", "#PRIOR-HELD", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Prior_Record = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Prior_Record", "#PRIOR-RECORD", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Latest_Form = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Latest_Form", "#LATEST-FORM", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_No_Change = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_No_Change", "#NO-CHANGE", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Recon_Summary = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Recon_Summary", "#RECON-SUMMARY", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Recon_Detail = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Recon_Detail", "#RECON-DETAIL", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Process_Recon_Reporting = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Process_Recon_Reporting", "#PROCESS-RECON-REPORTING", 
            FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Process_Recon = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Process_Recon", "#PROCESS-RECON", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Prior_Reported_For_Reporting = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Prior_Reported_For_Reporting", "#PRIOR-REPORTED-FOR-REPORTING", 
            FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Prior_Recon = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Prior_Recon", "#PRIOR-RECON", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Process_Recon_Reporting_Only = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Process_Recon_Reporting_Only", "#PROCESS-RECON-REPORTING-ONLY", 
            FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Process_Reporting = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Process_Reporting", "#PROCESS-REPORTING", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Process_Recon_Active_Rec = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Process_Recon_Active_Rec", "#PROCESS-RECON-ACTIVE-REC", 
            FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec", "#PROCESS-REPORTING-ACTIVE-REC", 
            FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Update_With_Reporting = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Update_With_Reporting", "#UPDATE-WITH-REPORTING", 
            FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Already_Reported = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Already_Reported", "#ALREADY-REPORTED", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Already_Recon = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Already_Recon", "#ALREADY-RECON", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Update_Ind = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Update_Ind", "#UPDATE-IND", FieldType.STRING, 1);
        pnd_Case_Fields_Pnd_Geo = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Geo", "#GEO", FieldType.STRING, 2);

        pnd_Report_Indexes = localVariables.newGroupInRecord("pnd_Report_Indexes", "#REPORT-INDEXES");
        pnd_Report_Indexes_Pnd_Str1 = pnd_Report_Indexes.newFieldInGroup("pnd_Report_Indexes_Pnd_Str1", "#STR1", FieldType.PACKED_DECIMAL, 7);
        pnd_Report_Indexes_Pnd_Stw1 = pnd_Report_Indexes.newFieldInGroup("pnd_Report_Indexes_Pnd_Stw1", "#STW1", FieldType.PACKED_DECIMAL, 7);
        pnd_Report_Indexes_Pnd_Stw2 = pnd_Report_Indexes.newFieldInGroup("pnd_Report_Indexes_Pnd_Stw2", "#STW2", FieldType.PACKED_DECIMAL, 7);
        pnd_Report_Indexes_Pnd_Stg1 = pnd_Report_Indexes.newFieldInGroup("pnd_Report_Indexes_Pnd_Stg1", "#STG1", FieldType.PACKED_DECIMAL, 7);
        pnd_Report_Indexes_Pnd_Irsr1 = pnd_Report_Indexes.newFieldInGroup("pnd_Report_Indexes_Pnd_Irsr1", "#IRSR1", FieldType.PACKED_DECIMAL, 7);

        pnd_Str = localVariables.newGroupArrayInRecord("pnd_Str", "#STR", new DbsArrayController(1, 7));
        pnd_Str_Pnd_Header1 = pnd_Str.newFieldInGroup("pnd_Str_Pnd_Header1", "#HEADER1", FieldType.STRING, 19);
        pnd_Str_Pnd_Header2 = pnd_Str.newFieldInGroup("pnd_Str_Pnd_Header2", "#HEADER2", FieldType.STRING, 19);

        pnd_Str_Pnd_Totals = pnd_Str.newGroupArrayInGroup("pnd_Str_Pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Str_Pnd_Form_Cnt = pnd_Str_Pnd_Totals.newFieldInGroup("pnd_Str_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Str_Pnd_State_Distr = pnd_Str_Pnd_Totals.newFieldInGroup("pnd_Str_Pnd_State_Distr", "#STATE-DISTR", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Str_Pnd_State_Tax = pnd_Str_Pnd_Totals.newFieldInGroup("pnd_Str_Pnd_State_Tax", "#STATE-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Str_Pnd_Local_Distr = pnd_Str_Pnd_Totals.newFieldInGroup("pnd_Str_Pnd_Local_Distr", "#LOCAL-DISTR", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Str_Pnd_Local_Tax = pnd_Str_Pnd_Totals.newFieldInGroup("pnd_Str_Pnd_Local_Tax", "#LOCAL-TAX", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Stw = localVariables.newGroupArrayInRecord("pnd_Stw", "#STW", new DbsArrayController(1, 6));
        pnd_Stw_Pnd_Header = pnd_Stw.newFieldInGroup("pnd_Stw_Pnd_Header", "#HEADER", FieldType.STRING, 14);

        pnd_Stw_Pnd_Totals = pnd_Stw.newGroupArrayInGroup("pnd_Stw_Pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Stw_Pnd_Form_Cnt = pnd_Stw_Pnd_Totals.newFieldInGroup("pnd_Stw_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Stw_Pnd_State_Distr = pnd_Stw_Pnd_Totals.newFieldInGroup("pnd_Stw_Pnd_State_Distr", "#STATE-DISTR", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Stw_Pnd_State_Tax = pnd_Stw_Pnd_Totals.newFieldInGroup("pnd_Stw_Pnd_State_Tax", "#STATE-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Stw_Pnd_Local_Distr = pnd_Stw_Pnd_Totals.newFieldInGroup("pnd_Stw_Pnd_Local_Distr", "#LOCAL-DISTR", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Stw_Pnd_Local_Tax = pnd_Stw_Pnd_Totals.newFieldInGroup("pnd_Stw_Pnd_Local_Tax", "#LOCAL-TAX", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Stg = localVariables.newGroupArrayInRecord("pnd_Stg", "#STG", new DbsArrayController(1, 3));
        pnd_Stg_Pnd_Header = pnd_Stg.newFieldInGroup("pnd_Stg_Pnd_Header", "#HEADER", FieldType.STRING, 19);

        pnd_Stg_Pnd_Totals = pnd_Stg.newGroupArrayInGroup("pnd_Stg_Pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Stg_Pnd_Form_Cnt = pnd_Stg_Pnd_Totals.newFieldInGroup("pnd_Stg_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Stg_Pnd_State_Distr = pnd_Stg_Pnd_Totals.newFieldInGroup("pnd_Stg_Pnd_State_Distr", "#STATE-DISTR", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Stg_Pnd_State_Tax = pnd_Stg_Pnd_Totals.newFieldInGroup("pnd_Stg_Pnd_State_Tax", "#STATE-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Stg_Pnd_Local_Distr = pnd_Stg_Pnd_Totals.newFieldInGroup("pnd_Stg_Pnd_Local_Distr", "#LOCAL-DISTR", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Stg_Pnd_Local_Tax = pnd_Stg_Pnd_Totals.newFieldInGroup("pnd_Stg_Pnd_Local_Tax", "#LOCAL-TAX", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Sta = localVariables.newGroupInRecord("pnd_Sta", "#STA");
        pnd_Sta_Pnd_Form_Cnt = pnd_Sta.newFieldInGroup("pnd_Sta_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Sta_Pnd_State_Distr = pnd_Sta.newFieldInGroup("pnd_Sta_Pnd_State_Distr", "#STATE-DISTR", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Sta_Pnd_State_Tax = pnd_Sta.newFieldInGroup("pnd_Sta_Pnd_State_Tax", "#STATE-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Sta_Pnd_Local_Distr = pnd_Sta.newFieldInGroup("pnd_Sta_Pnd_Local_Distr", "#LOCAL-DISTR", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Sta_Pnd_Local_Tax = pnd_Sta.newFieldInGroup("pnd_Sta_Pnd_Local_Tax", "#LOCAL-TAX", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Recon_Case = localVariables.newGroupInRecord("pnd_Recon_Case", "#RECON-CASE");
        pnd_Recon_Case_Pnd_Tin = pnd_Recon_Case.newFieldInGroup("pnd_Recon_Case_Pnd_Tin", "#TIN", FieldType.STRING, 11);
        pnd_Recon_Case_Pnd_Cntrct_Py = pnd_Recon_Case.newFieldInGroup("pnd_Recon_Case_Pnd_Cntrct_Py", "#CNTRCT-PY", FieldType.STRING, 12);

        pnd_Recon_Case_Pnd_Recon_Case_Detail = pnd_Recon_Case.newGroupArrayInGroup("pnd_Recon_Case_Pnd_Recon_Case_Detail", "#RECON-CASE-DETAIL", new DbsArrayController(1, 
            3));
        pnd_Recon_Case_Pnd_Header = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_Header", "#HEADER", FieldType.STRING, 10);
        pnd_Recon_Case_Pnd_Form_Cnt = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Recon_Case_Pnd_State_Distr = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_State_Distr", "#STATE-DISTR", FieldType.PACKED_DECIMAL, 
            14, 2);
        pnd_Recon_Case_Pnd_State_Tax = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_State_Tax", "#STATE-TAX", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Recon_Case_Pnd_Local_Distr = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_Local_Distr", "#LOCAL-DISTR", FieldType.PACKED_DECIMAL, 
            14, 2);
        pnd_Recon_Case_Pnd_Local_Tax = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_Local_Tax", "#LOCAL-TAX", FieldType.PACKED_DECIMAL, 
            11, 2);

        pnd_Hold_Fields = localVariables.newGroupInRecord("pnd_Hold_Fields", "#HOLD-FIELDS");
        pnd_Hold_Fields_Pnd_Hold_Comb_Fed_Ind = pnd_Hold_Fields.newFieldInGroup("pnd_Hold_Fields_Pnd_Hold_Comb_Fed_Ind", "#HOLD-COMB-FED-IND", FieldType.STRING, 
            1);
        pnd_Hold_Fields_Pnd_Hold_Rej_Ind = pnd_Hold_Fields.newFieldInGroup("pnd_Hold_Fields_Pnd_Hold_Rej_Ind", "#HOLD-REJ-IND", FieldType.STRING, 1);
        pnd_Irs_B_Special_Entry_Temp_De = localVariables.newFieldInRecord("pnd_Irs_B_Special_Entry_Temp_De", "#IRS-B-SPECIAL-ENTRY-TEMP-DE", FieldType.STRING, 
            20);
        pnd_Irs_B_Special_Entry_Temp_Ga = localVariables.newFieldInRecord("pnd_Irs_B_Special_Entry_Temp_Ga", "#IRS-B-SPECIAL-ENTRY-TEMP-GA", FieldType.STRING, 
            20);
        pnd_Irs_B_Special_Entry_Temp_Ks = localVariables.newFieldInRecord("pnd_Irs_B_Special_Entry_Temp_Ks", "#IRS-B-SPECIAL-ENTRY-TEMP-KS", FieldType.STRING, 
            20);
        pnd_Irs_B_Special_Entry_Temp_La = localVariables.newFieldInRecord("pnd_Irs_B_Special_Entry_Temp_La", "#IRS-B-SPECIAL-ENTRY-TEMP-LA", FieldType.STRING, 
            20);
        pnd_Irs_B_Special_Entry_Temp_Or = localVariables.newFieldInRecord("pnd_Irs_B_Special_Entry_Temp_Or", "#IRS-B-SPECIAL-ENTRY-TEMP-OR", FieldType.STRING, 
            20);
        pnd_Irs_B_Special_Entry_Temp_Va = localVariables.newFieldInRecord("pnd_Irs_B_Special_Entry_Temp_Va", "#IRS-B-SPECIAL-ENTRY-TEMP-VA", FieldType.STRING, 
            20);
        pnd_Irs_B_Special_Entry_Temp_Wi = localVariables.newFieldInRecord("pnd_Irs_B_Special_Entry_Temp_Wi", "#IRS-B-SPECIAL-ENTRY-TEMP-WI", FieldType.STRING, 
            20);
        pnd_Irs_B_Special_Entry_Temp_Nd = localVariables.newFieldInRecord("pnd_Irs_B_Special_Entry_Temp_Nd", "#IRS-B-SPECIAL-ENTRY-TEMP-ND", FieldType.STRING, 
            20);
        pnd_Seg2 = localVariables.newFieldInRecord("pnd_Seg2", "#SEG2", FieldType.STRING, 9);
        pnd_Seg3 = localVariables.newFieldInRecord("pnd_Seg3", "#SEG3", FieldType.STRING, 3);
        pnd_Irs_A_Filler_4_Ne = localVariables.newFieldInRecord("pnd_Irs_A_Filler_4_Ne", "#IRS-A-FILLER-4-NE", FieldType.STRING, 260);

        pnd_Irs_A_Filler_4_Ne__R_Field_19 = localVariables.newGroupInRecord("pnd_Irs_A_Filler_4_Ne__R_Field_19", "REDEFINE", pnd_Irs_A_Filler_4_Ne);
        pnd_Irs_A_Filler_4_Ne_Pnd_Irs_A_Filler_Sp1 = pnd_Irs_A_Filler_4_Ne__R_Field_19.newFieldInGroup("pnd_Irs_A_Filler_4_Ne_Pnd_Irs_A_Filler_Sp1", "#IRS-A-FILLER-SP1", 
            FieldType.STRING, 131);
        pnd_Irs_A_Filler_4_Ne_Pnd_Irs_A_Filler_Sp2 = pnd_Irs_A_Filler_4_Ne__R_Field_19.newFieldInGroup("pnd_Irs_A_Filler_4_Ne_Pnd_Irs_A_Filler_Sp2", "#IRS-A-FILLER-SP2", 
            FieldType.STRING, 9);
        pnd_Irs_A_Filler_4_Ne_Pnd_Irs_A_Filler_Sp3 = pnd_Irs_A_Filler_4_Ne__R_Field_19.newFieldInGroup("pnd_Irs_A_Filler_4_Ne_Pnd_Irs_A_Filler_Sp3", "#IRS-A-FILLER-SP3", 
            FieldType.STRING, 120);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Tirf_Company_CdeOld = internalLoopRecord.newFieldInRecord("Sort01_Tirf_Company_Cde_OLD", "Tirf_Company_Cde_OLD", FieldType.STRING, 1);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_form_U.reset();
        internalLoopRecord.reset();

        ldaTwrl9710.initializeValues();
        ldaTwrl9715.initializeValues();
        ldaTwrl3321.initializeValues();
        ldaTwrl3507.initializeValues();
        ldaTwrl3323.initializeValues();
        ldaTwrl3324.initializeValues();
        ldaTwrl3325.initializeValues();
        ldaTwrl3326.initializeValues();
        ldaTwrl3508.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
        pnd_Ws_Const_Pnd_State_Max.setInitialValue(57);
        pnd_Ws_Const_Pnd_Str_Max_Lines.setInitialValue(7);
        pnd_Ws_Const_Pnd_Stw_Max_Lines.setInitialValue(6);
        pnd_Ws_Const_Pnd_Stg_Max_Lines.setInitialValue(3);
        pnd_Corr_Ind2.getValue(1).setInitialValue(" ");
        pnd_Corr_Ind2.getValue(2).setInitialValue("C");
        pnd_Irsr_Max_Lines.setInitialValue(10);
        pnd_Prov_Min.setInitialValue(74);
        pnd_Prov_Max.setInitialValue(88);
        pnd_Ws_Pnd_Company_Break.setInitialValue(true);
        pnd_Ws_Pnd_Final_Break.setInitialValue(false);
        pnd_Ws_Pnd_A_Rec_Sw.setInitialValue(false);
        pnd_Str_Pnd_Header1.getValue(1).setInitialValue("Form Database Total");
        pnd_Str_Pnd_Header1.getValue(2).setInitialValue("Actve Records");
        pnd_Str_Pnd_Header1.getValue(3).setInitialValue("Actve Rec Not Rept");
        pnd_Str_Pnd_Header1.getValue(4).setInitialValue("Non Rept Zero Forms");
        pnd_Str_Pnd_Header1.getValue(5).setInitialValue("Prev Rejected Forms");
        pnd_Str_Pnd_Header1.getValue(6).setInitialValue("New  Rejected Forms");
        pnd_Str_Pnd_Header1.getValue(7).setInitialValue("Accepted Forms");
        pnd_Str_Pnd_Header2.getValue(2).setInitialValue(" (Prev. Reported)");
        pnd_Stw_Pnd_Header.getValue(1).setInitialValue("Accepted");
        pnd_Stw_Pnd_Header.getValue(2).setInitialValue("Prev. Reported");
        pnd_Stw_Pnd_Header.getValue(3).setInitialValue("Net Change");
        pnd_Stw_Pnd_Header.getValue(4).setInitialValue("Rejected");
        pnd_Stw_Pnd_Header.getValue(5).setInitialValue("Prev. Reported");
        pnd_Stw_Pnd_Header.getValue(6).setInitialValue("Net Change");
        pnd_Stg_Pnd_Header.getValue(1).setInitialValue("Form Database Total");
        pnd_Stg_Pnd_Header.getValue(2).setInitialValue("Prev. Reconciled");
        pnd_Stg_Pnd_Header.getValue(3).setInitialValue("Net Change");
        pnd_Recon_Case_Pnd_Header.getValue(1).setInitialValue("New Form");
        pnd_Recon_Case_Pnd_Header.getValue(2).setInitialValue("Original");
        pnd_Recon_Case_Pnd_Header.getValue(3).setInitialValue("Net Change");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Twrp3554() throws Exception
    {
        super("Twrp3554");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* **
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 80 ZP = ON;//Natural: FORMAT ( 02 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 03 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 04 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 05 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 06 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 07 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 08 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 09 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 10 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 11 ) PS = 58 LS = 133 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 37T 'TaxWaRS' 68T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 34T 'Control Report' 68T 'Report: RPT1' / //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 02 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 50T 'State Correction Reporting Summary' 120T 'Report: RPT2' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 60T #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME / #WS.#COMPANY-LINE //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 03 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'State Correction Reporting Rejected Detail' 120T 'Report: RPT3' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 60T #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME / #WS.#COMPANY-LINE //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 04 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T 'State Correction Return Accepted Detail' 120T 'Report: RPT4a' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 60T #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME / #WS.#COMPANY-LINE //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 05 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 05 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T 'State Withholding Reconciliation Summary' 120T 'Report: RPT5' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 60T #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME / 44T 'Reporting period' #WS.#PREV-RPT-DATE ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / #WS.#COMPANY-LINE //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 06 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 06 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 43T 'State Withholding Reconciliation Accepted Detail' 120T 'Report: RPT6' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 60T #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME / 44T 'Reporting period' #WS.#PREV-RPT-DATE ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / #WS.#COMPANY-LINE //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 07 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 07 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 43T 'State Withholding Reconciliation Rejected Detail' 120T 'Report: RPT7' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 60T #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME / 44T 'Reporting period' #WS.#PREV-RPT-DATE ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / #WS.#COMPANY-LINE //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 08 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 08 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'State General Ledger Reconciliation Summary' 120T 'Report: RPT8' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 60T #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME / 44T 'Reporting period' #WS.#PREV-RPT-DATE ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / #WS.#COMPANY-LINE //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 09 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 09 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 45T 'State General Ledger Reconciliation Detail' 120T 'Report: RPT9' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 60T #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME / 44T 'Reporting period' #WS.#PREV-RPT-DATE ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / #WS.#COMPANY-LINE //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 10 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 10 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T 'State Correction Return Accepted Detail' 120T 'Report: RPT4b' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 60T #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME / 44T 'Reporting period' #WS.#PREV-RPT-DATE ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / #WS.#COMPANY-LINE //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 11 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 11 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 51T 'State Correction Return Detail' 120T 'Report: RPT4c' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 60T #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME / #WS.#COMPANY-LINE //
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT-PARMS
        sub_Process_Input_Parms();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM LOAD-STATE-TABLE
        sub_Load_State_Table();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM LOAD-PROVINCE-TABLE
        sub_Load_Province_Table();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_S8.setValueEdited(pnd_Ws_Pnd_Tax_Year,new ReportEditMask("9999"));                                                                                     //Natural: MOVE EDITED #WS.#TAX-YEAR ( EM = 9999 ) TO #WS.#S8
        setValueToSubstring("01",pnd_Ws_Pnd_S8,5,2);                                                                                                                      //Natural: MOVE '01' TO SUBSTR ( #WS.#S8,5,2 )
        setValueToSubstring(pnd_Ws_Pnd_State,pnd_Ws_Pnd_S8,7,2);                                                                                                          //Natural: MOVE #WS.#STATE TO SUBSTR ( #WS.#S8,7,2 )
        ldaTwrl9710.getVw_form().startDatabaseFind                                                                                                                        //Natural: FIND FORM WITH FORM.TIRF-SUPERDE-8 = #WS.#S8
        (
        "F_STATE",
        new Wc[] { new Wc("TIRF_SUPERDE_8", "=", pnd_Ws_Pnd_S8, WcType.WITH) }
        );
        F_STATE:
        while (condition(ldaTwrl9710.getVw_form().readNextRow("F_STATE")))
        {
            ldaTwrl9710.getVw_form().setIfNotFoundControlFlag(false);
            if (condition(!(ldaTwrl9710.getForm_Tirf_Active_Ind().equals("A"))))                                                                                          //Natural: ACCEPT IF FORM.TIRF-ACTIVE-IND = 'A'
            {
                continue;
            }
            pnd_Sort_Rec_Pnd_Sort_Rec_Detail.setValuesByName(ldaTwrl9710.getVw_form());                                                                                   //Natural: MOVE BY NAME FORM TO #SORT-REC-DETAIL
            getSort().writeSortInData(pnd_Sort_Rec);                                                                                                                      //Natural: END-ALL
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        getSort().sortData(pnd_Sort_Rec);                                                                                                                                 //Natural: SORT BY #SORT-REC USING KEYS
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Sort_Rec)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            if (condition(pnd_Ws_Pnd_Company_Break.getBoolean()))                                                                                                         //Natural: AT BREAK OF #SORT-REC.TIRF-COMPANY-CDE;//Natural: AT BREAK OF #SORT-REC.TIRF-FORM-TYPE;//Natural: IF #WS.#COMPANY-BREAK
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMPANY
                sub_Process_Company();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_S1_Start_Tirf_Tax_Year.setValue(pnd_Sort_Rec_Tirf_Tax_Year);                                                                                              //Natural: ASSIGN #S1-START.TIRF-TAX-YEAR := #SORT-REC.TIRF-TAX-YEAR
            pnd_S1_Start_Tirf_Tin.setValue(pnd_Sort_Rec_Tirf_Tin);                                                                                                        //Natural: ASSIGN #S1-START.TIRF-TIN := #SORT-REC.TIRF-TIN
            pnd_S1_Start_Tirf_Form_Type.setValue(pnd_Sort_Rec_Tirf_Form_Type);                                                                                            //Natural: ASSIGN #S1-START.TIRF-FORM-TYPE := #SORT-REC.TIRF-FORM-TYPE
            pnd_S1_Start_Tirf_Contract_Nbr.setValue(pnd_Sort_Rec_Tirf_Contract_Nbr);                                                                                      //Natural: ASSIGN #S1-START.TIRF-CONTRACT-NBR := #SORT-REC.TIRF-CONTRACT-NBR
            pnd_S1_Start_Tirf_Payee_Cde.setValue(pnd_Sort_Rec_Tirf_Payee_Cde);                                                                                            //Natural: ASSIGN #S1-START.TIRF-PAYEE-CDE := #SORT-REC.TIRF-PAYEE-CDE
            pnd_S1_Start_Tirf_Key.setValue(pnd_Sort_Rec_Tirf_Key);                                                                                                        //Natural: ASSIGN #S1-START.TIRF-KEY := #SORT-REC.TIRF-KEY
            pnd_S1_End.setValue(pnd_S1_Start);                                                                                                                            //Natural: ASSIGN #S1-END := #S1-START
            setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_S1_Start,32,1);                                                                                               //Natural: MOVE LOW-VALUES TO SUBSTR ( #S1-START,32,1 )
            setValueToSubstring(pnd_Ws_Const_High_Values,pnd_S1_End,32,1);                                                                                                //Natural: MOVE HIGH-VALUES TO SUBSTR ( #S1-END,32,1 )
                                                                                                                                                                          //Natural: PERFORM PROCESS-CASE
            sub_Process_Case();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            sort01Tirf_Company_CdeOld.setValue(pnd_Sort_Rec_Tirf_Company_Cde);                                                                                            //Natural: END-SORT
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        if (condition(pnd_Ws_Pnd_Accepted_Form_Cnt.equals(getZero())))                                                                                                    //Natural: IF #WS.#ACCEPTED-FORM-CNT = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"No Forms were selected for the",new TabSetting(77),"***",NEWLINE,"***",new               //Natural: WRITE ( 1 ) '***' 25T 'No Forms were selected for the' 77T '***' / '***' 25T 'State Correction Reporting' 77T '***'
                TabSetting(25),"State Correction Reporting",new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-T-RECORD
        sub_Create_T_Record();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-F-RECORD
        sub_Create_F_Record();
        if (condition(Global.isEscape())) {return;}
        //* ************************
        //*  S U B R O U T I N E S
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CASE
        //*                                         IN CASE OF NO INACTIVE RECORDS
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ACTIVE-REC
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-STR
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-STW
        //* **************************
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-STG
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-STG1
        //* ***************************
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-STA
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-FORM
        //* ****************************
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-RECON-CASE
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-B-RECORD
        //*  FOR #I = 1 TO #TWRAFORM.C-1099-R
        //* *#WS.#TEMP-CORR-IND           :=
        //*  IF #CASE-FIELDS.#LATEST-FORM
        //*  IF #TWRAFORM.TIRF-RES-CMB-IND(#I) = 'Y'
        //*  END-IF
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-SPECIAL-STATE-DATA
        //* *    VALUE  'T'
        //* *    VALUE  'T'
        //*     //
        //*     //
        //*  //
        //*  //
        //*    //
        //*    ///
        //*   ///
        //*   ///
        //*    ///
        //*    ///
        //*   ///
        //*   ///
        //*  ///
        //*  ///
        //*    ///
        //*    ///
        //*    ///
        //*   ///
        //* *    VALUE  'T'
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-B-REC
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-A-RECORDS
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-C-RECORDS
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-K-RECORDS
        //*  IF #K-REC.#GENERATE(#I)
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-T-RECORD
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-F-RECORD
        //* ********************************
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STR-DETAIL-ACCEPT
        //*  '#/of/Sta/tes'     #TWRAFORM.C-1099-R                (EM=ZZ9 HC=R)
        //*  'S/T/A/T'          #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE
        //*  '/I/R/S'           #TWRAFORM.TIRF-STATE-IRS-RPT-IND (#ST-OCC)
        //*  'H/A/R/D'          #TWRAFORM.TIRF-STATE-HARDCOPY-IND(#ST-OCC)
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STR-DETAIL-REJECT
        //*  'H/A/R/D'          FORM-R.TIRF-STATE-HARDCOPY-IND   (#ST-OCC-A)
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STW-DETAIL-ACCEPT
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-4B
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STW-DETAIL-REJECT
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STG-DETAIL
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STA-REPORT-4C
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-REPORTS
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-REPORTS-C
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-REPORTS-W
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-REPORTS-G
        //* ***********************************
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-REPORTS-A
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TIN
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COMPANY
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INPUT-PARMS
        //* *************************************
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-STATE-TABLE
        //* *********************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-PROVINCE-TABLE
        //* ************************************
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CONTROL
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CONTROL
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-DISTRIB
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Process_Case() throws Exception                                                                                                                      //Natural: PROCESS-CASE
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        pnd_Case_Fields.reset();                                                                                                                                          //Natural: RESET #CASE-FIELDS
        ldaTwrl9710.getVw_form().startDatabaseRead                                                                                                                        //Natural: READ FORM BY TIRF-SUPERDE-1 = #S1-START THRU #S1-END WHERE FORM.TIRF-COMPANY-CDE = #SORT-REC.TIRF-COMPANY-CDE
        (
        "RD_FORM",
        new Wc[] { new Wc("TIRF_COMPANY_CDE", "=", pnd_Sort_Rec_Tirf_Company_Cde, WcType.WHERE) ,
        new Wc("TIRF_SUPERDE_1", ">=", pnd_S1_Start.getBinary(), "And", WcType.BY) ,
        new Wc("TIRF_SUPERDE_1", "<=", pnd_S1_End.getBinary(), WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_1", "ASC") }
        );
        RD_FORM:
        while (condition(ldaTwrl9710.getVw_form().readNextRow("RD_FORM")))
        {
            pnd_Case_Fields_Pnd_St_Max.setValue(ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp());                                                                   //Natural: ASSIGN #CASE-FIELDS.#ST-MAX := FORM.C*TIRF-1099-R-STATE-GRP
            if (condition(pnd_Case_Fields_Pnd_St_Max.equals(getZero())))                                                                                                  //Natural: IF #CASE-FIELDS.#ST-MAX = 0
            {
                pnd_Ws_Pnd_St_Occ.reset();                                                                                                                                //Natural: RESET #WS.#ST-OCC
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                DbsUtil.examine(new ExamineSource(ldaTwrl9710.getForm_Tirf_State_Code().getValue(1,":",pnd_Case_Fields_Pnd_St_Max),true), new ExamineSearch(pnd_Ws_Pnd_State,  //Natural: EXAMINE FULL FORM.TIRF-STATE-CODE ( 1:#ST-MAX ) FOR FULL #WS.#STATE GIVING INDEX IN #WS.#ST-OCC
                    true), new ExamineGivingIndex(pnd_Ws_Pnd_St_Occ));
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl9710.getForm_Tirf_Active_Ind().notEquals("A") && pnd_Ws_Pnd_St_Occ.equals(getZero())))                                                   //Natural: REJECT IF FORM.TIRF-ACTIVE-IND NE 'A' AND #WS.#ST-OCC = 0
            {
                continue;
            }
            //*  ACTIVE RECORD
            if (condition(ldaTwrl9710.getForm_Tirf_Active_Ind().equals("A")))                                                                                             //Natural: IF FORM.TIRF-ACTIVE-IND = 'A'
            {
                ldaTwrl9715.getVw_form_R().setValuesByName(ldaTwrl9710.getVw_form());                                                                                     //Natural: MOVE BY NAME FORM TO FORM-R
                pnd_Case_Fields_Pnd_St_Occ_A.setValue(pnd_Ws_Pnd_St_Occ);                                                                                                 //Natural: ASSIGN #CASE-FIELDS.#ST-OCC-A := #WS.#ST-OCC
                pnd_Report_Indexes_Pnd_Str1.setValue(1);                                                                                                                  //Natural: ASSIGN #REPORT-INDEXES.#STR1 := 1
                if (condition(ldaTwrl9715.getForm_R_Tirf_Empty_Form().getBoolean()))                                                                                      //Natural: IF FORM-R.TIRF-EMPTY-FORM
                {
                    pnd_Ws_Pnd_Empty_Frm2.nadd(1);                                                                                                                        //Natural: ADD 1 TO #EMPTY-FRM2
                    pnd_Ws_Pnd_Empty_Tot_Frm2.nadd(1);                                                                                                                    //Natural: ADD 1 TO #EMPTY-TOT-FRM2
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ACCUM-STR
                sub_Accum_Str();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(new DbsDecimal("0.00")) && ldaTwrl9715.getForm_R_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(new  //Natural: IF FORM-R.TIRF-STATE-TAX-WTHLD ( #ST-OCC-A ) = 0.00 AND FORM-R.TIRF-LOC-TAX-WTHLD ( #ST-OCC-A ) = 0.00
                    DbsDecimal("0.00"))))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM ACCUM-STA
                    sub_Accum_Sta();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM STA-REPORT-4C
                    sub_Sta_Report_4c();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl9715.getForm_R_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A).notEquals(" ")))                                     //Natural: IF FORM-R.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-A ) NE ' '
                {
                                                                                                                                                                          //Natural: PERFORM ACCUM-STG
                    sub_Accum_Stg();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  ALREADY REPORTED
                if (condition(! (ldaTwrl9715.getForm_R_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(" ") || ldaTwrl9715.getForm_R_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals("H")  //Natural: IF NOT FORM-R.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-A ) = ' ' OR = 'H' OR = 'J'
                    || ldaTwrl9715.getForm_R_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals("J"))))
                {
                    pnd_Report_Indexes_Pnd_Str1.setValue(2);                                                                                                              //Natural: ASSIGN #REPORT-INDEXES.#STR1 := 2
                                                                                                                                                                          //Natural: PERFORM ACCUM-STR
                    sub_Accum_Str();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Case_Fields2_Pnd_Recon_And_Reporting.getBoolean()))                                                                                     //Natural: IF #RECON-AND-REPORTING
                {
                    if (condition(ldaTwrl9715.getForm_R_Tirf_State_Reporting().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals("Y")))                                       //Natural: IF FORM-R.TIRF-STATE-REPORTING ( #ST-OCC-A ) = 'Y'
                    {
                        pnd_Case_Fields_Pnd_Process_Reporting.setValue(true);                                                                                             //Natural: ASSIGN #PROCESS-REPORTING := TRUE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Report_Indexes_Pnd_Str1.setValue(3);                                                                                                          //Natural: ASSIGN #REPORT-INDEXES.#STR1 := 3
                                                                                                                                                                          //Natural: PERFORM ACCUM-STR
                        sub_Accum_Str();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl9715.getForm_R_Tirf_State_Hardcopy_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A).notEquals(" ") || ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A).notEquals(new  //Natural: IF FORM-R.TIRF-STATE-HARDCOPY-IND ( #ST-OCC-A ) NE ' ' OR FORM-R.TIRF-STATE-TAX-WTHLD ( #ST-OCC-A ) NE 0.00
                        DbsDecimal("0.00"))))
                    {
                        pnd_Case_Fields_Pnd_Process_Recon.setValue(true);                                                                                                 //Natural: ASSIGN #PROCESS-RECON := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Case_Fields_Pnd_Process_Reporting.getBoolean() || pnd_Case_Fields_Pnd_Process_Recon.getBoolean()))                                  //Natural: IF #PROCESS-REPORTING OR #PROCESS-RECON
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Case_Fields2_Pnd_Recon_Only.getBoolean()))                                                                                              //Natural: IF #RECON-ONLY
                {
                    if (condition(ldaTwrl9715.getForm_R_Tirf_State_Hardcopy_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A).notEquals(" ") || ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A).notEquals(new  //Natural: IF FORM-R.TIRF-STATE-HARDCOPY-IND ( #ST-OCC-A ) NE ' ' OR FORM-R.TIRF-STATE-TAX-WTHLD ( #ST-OCC-A ) NE 0.00
                        DbsDecimal("0.00"))))
                    {
                        pnd_Case_Fields_Pnd_Process_Recon.setValue(true);                                                                                                 //Natural: ASSIGN #PROCESS-RECON := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Case_Fields_Pnd_Process_Recon.getBoolean()))                                                                                        //Natural: IF #PROCESS-RECON
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Case_Fields_Pnd_Active_Isn.setValue(ldaTwrl9710.getVw_form().getAstISN("RD_FORM"));                                                                   //Natural: ASSIGN #CASE-FIELDS.#ACTIVE-ISN := *ISN ( RD-FORM. )
                pnd_Case_Fields_Pnd_St_Occ_I.setValue(pnd_Ws_Pnd_St_Occ);                                                                                                 //Natural: ASSIGN #CASE-FIELDS.#ST-OCC-I := #WS.#ST-OCC
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Case_Fields_Pnd_St_Occ_I.setValue(pnd_Ws_Pnd_St_Occ);                                                                                                     //Natural: ASSIGN #CASE-FIELDS.#ST-OCC-I := #WS.#ST-OCC
            //* **********************************************************
            //* * THE FOLLOWING CODE SEARCHES FOR INACTIVE PRIOR RECORD
            //* **********************************************************
            pnd_Case_Fields_Pnd_Prior_Reported_For_Reporting.reset();                                                                                                     //Natural: RESET #CASE-FIELDS.#PRIOR-REPORTED-FOR-REPORTING #CASE-FIELDS.#PRIOR-RECON #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC
            pnd_Case_Fields_Pnd_Prior_Recon.reset();
            pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.reset();
            pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.reset();
            if (condition(pnd_Case_Fields_Pnd_Process_Reporting.getBoolean() && ! (pnd_Case_Fields_Pnd_Already_Reported.getBoolean())))                                   //Natural: IF #PROCESS-REPORTING AND NOT #ALREADY-REPORTED
            {
                if (condition(pnd_Case_Fields2_Pnd_Special_Processing.getBoolean()))                                                                                      //Natural: IF #SPECIAL-PROCESSING
                {
                    if (condition(ldaTwrl9710.getForm_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals("O") || ldaTwrl9710.getForm_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals("C"))) //Natural: IF FORM.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-I ) = 'O' OR = 'C'
                    {
                        pnd_Case_Fields_Pnd_Already_Reported.setValue(true);                                                                                              //Natural: ASSIGN #CASE-FIELDS.#ALREADY-REPORTED := TRUE
                        pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.setValue(true);                                                                                  //Natural: ASSIGN #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC := TRUE
                        pnd_Case_Fields_Pnd_Prior_Reported_For_Reporting.setValue(true);                                                                                  //Natural: ASSIGN #CASE-FIELDS.#PRIOR-REPORTED-FOR-REPORTING := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition((((ldaTwrl9710.getForm_Tirf_State_Irs_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals("Y") && ldaTwrl9710.getForm_Tirf_Res_Cmb_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals("Y"))  //Natural: IF ( FORM.TIRF-STATE-IRS-RPT-IND ( #ST-OCC-I ) = 'Y' AND FORM.TIRF-RES-CMB-IND ( #ST-OCC-I ) = 'Y' AND ( FORM.TIRF-IRS-RPT-IND = 'O' OR = 'C' ) ) OR ( FORM.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-I ) = 'O' OR = 'C' )
                        && (ldaTwrl9710.getForm_Tirf_Irs_Rpt_Ind().equals("O") || ldaTwrl9710.getForm_Tirf_Irs_Rpt_Ind().equals("C"))) || (ldaTwrl9710.getForm_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals("O") 
                        || ldaTwrl9710.getForm_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals("C")))))
                    {
                        pnd_Case_Fields_Pnd_Already_Reported.setValue(true);                                                                                              //Natural: ASSIGN #CASE-FIELDS.#ALREADY-REPORTED := TRUE
                        pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.setValue(true);                                                                                  //Natural: ASSIGN #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC := TRUE
                        pnd_Case_Fields_Pnd_Prior_Reported_For_Reporting.setValue(true);                                                                                  //Natural: ASSIGN #CASE-FIELDS.#PRIOR-REPORTED-FOR-REPORTING := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Case_Fields_Pnd_Process_Recon.getBoolean() && ! (pnd_Case_Fields_Pnd_Already_Recon.getBoolean())))                                          //Natural: IF #PROCESS-RECON AND NOT #ALREADY-RECON
            {
                if (condition(ldaTwrl9710.getForm_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals("O") || ldaTwrl9710.getForm_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals("C")  //Natural: IF FORM.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-I ) = 'O' OR = 'C' OR = 'X'
                    || ldaTwrl9710.getForm_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals("X")))
                {
                    pnd_Case_Fields_Pnd_Already_Recon.setValue(true);                                                                                                     //Natural: ASSIGN #CASE-FIELDS.#ALREADY-RECON := TRUE
                    pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.setValue(true);                                                                                          //Natural: ASSIGN #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC := TRUE
                    pnd_Case_Fields_Pnd_Prior_Recon.setValue(true);                                                                                                       //Natural: ASSIGN #CASE-FIELDS.#PRIOR-RECON := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.getBoolean() || pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.getBoolean()))                    //Natural: IF #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC OR #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ACTIVE-REC
                sub_Process_Active_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.reset();                                                                                                         //Natural: RESET #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC
        pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.reset();                                                                                                             //Natural: RESET #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC
        if (condition(pnd_Case_Fields_Pnd_Process_Reporting.getBoolean() && ! (pnd_Case_Fields_Pnd_Already_Reported.getBoolean())))                                       //Natural: IF #CASE-FIELDS.#PROCESS-REPORTING AND NOT #ALREADY-REPORTED
        {
            pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.setValue(true);                                                                                              //Natural: ASSIGN #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Process_Recon.getBoolean() && ! (pnd_Case_Fields_Pnd_Already_Recon.getBoolean())))                                              //Natural: IF #CASE-FIELDS.#PROCESS-RECON AND NOT #ALREADY-RECON
        {
            pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.setValue(true);                                                                                                  //Natural: ASSIGN #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.getBoolean() || pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.getBoolean()))                        //Natural: IF #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC OR #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ACTIVE-REC
            sub_Process_Active_Rec();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Active_Rec() throws Exception                                                                                                                //Natural: PROCESS-ACTIVE-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        if (condition(! (pnd_Case_Fields_Pnd_Prior_Reported_For_Reporting.getBoolean()) && pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.getBoolean()                  //Natural: IF NOT #CASE-FIELDS.#PRIOR-REPORTED-FOR-REPORTING AND #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC AND FORM-R.TIRF-STATE-DISTR ( #ST-OCC-A ) = 0.00 AND FORM-R.TIRF-STATE-TAX-WTHLD ( #ST-OCC-A ) = 0.00 AND FORM-R.TIRF-LOC-DISTR ( #ST-OCC-A ) = 0.00 AND FORM-R.TIRF-LOC-TAX-WTHLD ( #ST-OCC-A ) = 0.00
            && ldaTwrl9715.getForm_R_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(new DbsDecimal("0.00")) && ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(new 
            DbsDecimal("0.00")) && ldaTwrl9715.getForm_R_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(new DbsDecimal("0.00")) && ldaTwrl9715.getForm_R_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(new 
            DbsDecimal("0.00"))))
        {
            pnd_Report_Indexes_Pnd_Str1.setValue(4);                                                                                                                      //Natural: ASSIGN #REPORT-INDEXES.#STR1 := 4
                                                                                                                                                                          //Natural: PERFORM ACCUM-STR
            sub_Accum_Str();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.getBoolean()))                                                                                     //Natural: IF #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC
            {
                pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.reset();                                                                                                 //Natural: RESET #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Prior_Recon.getBoolean() && pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.getBoolean()))                                         //Natural: IF #CASE-FIELDS.#PRIOR-RECON AND #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC
        {
            pnd_Case_Fields_Pnd_Prior_Record.setValue(true);                                                                                                              //Natural: ASSIGN #CASE-FIELDS.#PRIOR-RECORD := TRUE
                                                                                                                                                                          //Natural: PERFORM SETUP-RECON-CASE
            sub_Setup_Recon_Case();
            if (condition(Global.isEscape())) {return;}
            if (condition(! (pnd_Case_Fields_Pnd_Recon_Summary.getBoolean())))                                                                                            //Natural: IF NOT #CASE-FIELDS.#RECON-SUMMARY
            {
                if (condition(pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.getBoolean()))                                                                             //Natural: IF #PROCESS-REPORTING-ACTIVE-REC
                {
                    pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.reset();                                                                                                 //Natural: RESET #PROCESS-RECON-ACTIVE-REC
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.getBoolean() && ! (pnd_Case_Fields_Pnd_Prior_Recon.getBoolean())))                                     //Natural: IF #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC AND NOT #CASE-FIELDS.#PRIOR-RECON
        {
            pnd_Case_Fields_Pnd_Prior_Record.setValue(false);                                                                                                             //Natural: ASSIGN #CASE-FIELDS.#PRIOR-RECORD := FALSE
                                                                                                                                                                          //Natural: PERFORM SETUP-RECON-CASE
            sub_Setup_Recon_Case();
            if (condition(Global.isEscape())) {return;}
            if (condition(! (pnd_Case_Fields_Pnd_Recon_Summary.getBoolean())))                                                                                            //Natural: IF NOT #CASE-FIELDS.#RECON-SUMMARY
            {
                if (condition(pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.getBoolean()))                                                                             //Natural: IF #PROCESS-REPORTING-ACTIVE-REC
                {
                    pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.reset();                                                                                                 //Natural: RESET #PROCESS-RECON-ACTIVE-REC
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.getBoolean()))                                                                                         //Natural: IF #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC
        {
                                                                                                                                                                          //Natural: PERFORM STG-DETAIL
            sub_Stg_Detail();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaTwratbl2.getTwratbl2_Tircntl_Corr_Media_Code().equals("M")))                                                                                     //Natural: IF TWRATBL2.TIRCNTL-CORR-MEDIA-CODE = 'M'
        {
            pnd_Hold_Fields_Pnd_Hold_Rej_Ind.setValue(ldaTwrl9715.getForm_R_Tirf_Irs_Reject_Ind());                                                                       //Natural: ASSIGN #HOLD-REJ-IND := FORM-R.TIRF-IRS-REJECT-IND
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Hold_Fields_Pnd_Hold_Rej_Ind.setValue(ldaTwrl9715.getForm_R_Tirf_Res_Reject_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                                //Natural: ASSIGN #HOLD-REJ-IND := FORM-R.TIRF-RES-REJECT-IND ( #ST-OCC-A )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Hold_Fields_Pnd_Hold_Rej_Ind.equals("Y")))                                                                                                      //Natural: IF #HOLD-REJ-IND = 'Y'
        {
            if (condition(pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.getBoolean()))                                                                                 //Natural: IF #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC
            {
                                                                                                                                                                          //Natural: PERFORM STR-DETAIL-REJECT
                sub_Str_Detail_Reject();
                if (condition(Global.isEscape())) {return;}
                //*  OLD REJECTS
                if (condition(ldaTwrl9715.getForm_R_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals("H")))                                        //Natural: IF FORM-R.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-A ) = 'H'
                {
                    pnd_Report_Indexes_Pnd_Str1.setValue(5);                                                                                                              //Natural: ASSIGN #REPORT-INDEXES.#STR1 := 5
                                                                                                                                                                          //Natural: PERFORM ACCUM-STR
                    sub_Accum_Str();
                    if (condition(Global.isEscape())) {return;}
                    //*  FIRST TIME REJECT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Report_Indexes_Pnd_Str1.setValue(6);                                                                                                              //Natural: ASSIGN #REPORT-INDEXES.#STR1 := 6
                                                                                                                                                                          //Natural: PERFORM ACCUM-STR
                    sub_Accum_Str();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.getBoolean()))                                                                                     //Natural: IF #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC
            {
                                                                                                                                                                          //Natural: PERFORM STW-DETAIL-REJECT
                sub_Stw_Detail_Reject();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  OLD REJECTS
            if (condition(ldaTwrl9715.getForm_R_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals("H")))                                            //Natural: IF FORM-R.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-A ) = 'H'
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM UPDATE-FORM
            sub_Update_Form();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.getBoolean()))                                                                                     //Natural: IF #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC
        {
            pnd_Report_Indexes_Pnd_Str1.setValue(7);                                                                                                                      //Natural: ASSIGN #REPORT-INDEXES.#STR1 := 7
                                                                                                                                                                          //Natural: PERFORM ACCUM-STR
            sub_Accum_Str();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.getBoolean()))                                                                                     //Natural: IF #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC
        {
            pdaTwraform.getPnd_Twraform_Pnd_Twraform_Det().setValuesByName(ldaTwrl9715.getVw_form_R());                                                                   //Natural: MOVE BY NAME FORM-R TO #TWRAFORM-DET
            pdaTwraform.getPnd_Twraform_C_1099_R().setValue(ldaTwrl9715.getForm_R_Count_Casttirf_1099_R_State_Grp());                                                     //Natural: ASSIGN #TWRAFORM.C-1099-R := FORM-R.C*TIRF-1099-R-STATE-GRP
            pnd_Ws_Pnd_St_Occ.setValue(pnd_Case_Fields_Pnd_St_Occ_A);                                                                                                     //Natural: ASSIGN #WS.#ST-OCC := #CASE-FIELDS.#ST-OCC-A
            pnd_Ws_Pnd_Disp_Rec.setValue("N E W");                                                                                                                        //Natural: ASSIGN #WS.#DISP-REC := 'N E W'
                                                                                                                                                                          //Natural: PERFORM STR-DETAIL-ACCEPT
            sub_Str_Detail_Accept();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-B-RECORD
            sub_Create_B_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Prior_Reported_For_Reporting.getBoolean() && pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.getBoolean()))                    //Natural: IF #CASE-FIELDS.#PRIOR-REPORTED-FOR-REPORTING AND #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC
        {
            pdaTwraform.getPnd_Twraform_Pnd_Twraform_Det().setValuesByName(ldaTwrl9710.getVw_form());                                                                     //Natural: MOVE BY NAME FORM TO #TWRAFORM-DET
            pdaTwraform.getPnd_Twraform_C_1099_R().setValue(ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp());                                                       //Natural: ASSIGN #TWRAFORM.C-1099-R := FORM.C*TIRF-1099-R-STATE-GRP
            pnd_Ws_Pnd_St_Occ.setValue(pnd_Case_Fields_Pnd_St_Occ_I);                                                                                                     //Natural: ASSIGN #WS.#ST-OCC := #CASE-FIELDS.#ST-OCC-I
            pnd_Ws_Pnd_Disp_Rec.setValue("O L D");                                                                                                                        //Natural: ASSIGN #WS.#DISP-REC := 'O L D'
                                                                                                                                                                          //Natural: PERFORM STR-DETAIL-ACCEPT
            sub_Str_Detail_Accept();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 4 ) 1 LINES
        if (condition(pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.getBoolean()))                                                                                         //Natural: IF #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC
        {
                                                                                                                                                                          //Natural: PERFORM STW-DETAIL-ACCEPT
            sub_Stw_Detail_Accept();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM UPDATE-FORM
        sub_Update_Form();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Accum_Str() throws Exception                                                                                                                         //Natural: ACCUM-STR
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        pnd_Str_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Str1,1).nadd(1);                                                                                             //Natural: ADD 1 TO #STR.#FORM-CNT ( #STR1,1 )
        pnd_Str_Pnd_State_Distr.getValue(pnd_Report_Indexes_Pnd_Str1,1).nadd(ldaTwrl9715.getForm_R_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A));            //Natural: ADD FORM-R.TIRF-STATE-DISTR ( #ST-OCC-A ) TO #STR.#STATE-DISTR ( #STR1,1 )
        pnd_Str_Pnd_State_Tax.getValue(pnd_Report_Indexes_Pnd_Str1,1).nadd(ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A));          //Natural: ADD FORM-R.TIRF-STATE-TAX-WTHLD ( #ST-OCC-A ) TO #STR.#STATE-TAX ( #STR1,1 )
        pnd_Str_Pnd_Local_Distr.getValue(pnd_Report_Indexes_Pnd_Str1,1).nadd(ldaTwrl9715.getForm_R_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A));              //Natural: ADD FORM-R.TIRF-LOC-DISTR ( #ST-OCC-A ) TO #STR.#LOCAL-DISTR ( #STR1,1 )
        pnd_Str_Pnd_Local_Tax.getValue(pnd_Report_Indexes_Pnd_Str1,1).nadd(ldaTwrl9715.getForm_R_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A));            //Natural: ADD FORM-R.TIRF-LOC-TAX-WTHLD ( #ST-OCC-A ) TO #STR.#LOCAL-TAX ( #STR1,1 )
    }
    private void sub_Accum_Stw() throws Exception                                                                                                                         //Natural: ACCUM-STW
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #WS.#K = 1 TO 3
        for (pnd_Ws_Pnd_K.setValue(1); condition(pnd_Ws_Pnd_K.lessOrEqual(3)); pnd_Ws_Pnd_K.nadd(1))
        {
            pnd_Report_Indexes_Pnd_Stw2.compute(new ComputeParameters(false, pnd_Report_Indexes_Pnd_Stw2), pnd_Report_Indexes_Pnd_Stw1.add(pnd_Ws_Pnd_K));                //Natural: ASSIGN #REPORT-INDEXES.#STW2 := #REPORT-INDEXES.#STW1 + #WS.#K
            pnd_Stw_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Stw2,1).nadd(pnd_Recon_Case_Pnd_Form_Cnt.getValue(pnd_Ws_Pnd_K));                                        //Natural: ADD #RECON-CASE.#FORM-CNT ( #K ) TO #STW.#FORM-CNT ( #STW2,1 )
            pnd_Stw_Pnd_State_Distr.getValue(pnd_Report_Indexes_Pnd_Stw2,1).nadd(pnd_Recon_Case_Pnd_State_Distr.getValue(pnd_Ws_Pnd_K));                                  //Natural: ADD #RECON-CASE.#STATE-DISTR ( #K ) TO #STW.#STATE-DISTR ( #STW2,1 )
            pnd_Stw_Pnd_State_Tax.getValue(pnd_Report_Indexes_Pnd_Stw2,1).nadd(pnd_Recon_Case_Pnd_State_Tax.getValue(pnd_Ws_Pnd_K));                                      //Natural: ADD #RECON-CASE.#STATE-TAX ( #K ) TO #STW.#STATE-TAX ( #STW2,1 )
            pnd_Stw_Pnd_Local_Distr.getValue(pnd_Report_Indexes_Pnd_Stw2,1).nadd(pnd_Recon_Case_Pnd_Local_Distr.getValue(pnd_Ws_Pnd_K));                                  //Natural: ADD #RECON-CASE.#LOCAL-DISTR ( #K ) TO #STW.#LOCAL-DISTR ( #STW2,1 )
            pnd_Stw_Pnd_Local_Tax.getValue(pnd_Report_Indexes_Pnd_Stw2,1).nadd(pnd_Recon_Case_Pnd_Local_Tax.getValue(pnd_Ws_Pnd_K));                                      //Natural: ADD #RECON-CASE.#LOCAL-TAX ( #K ) TO #STW.#LOCAL-TAX ( #STW2,1 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Accum_Stg() throws Exception                                                                                                                         //Natural: ACCUM-STG
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        if (condition(ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(new DbsDecimal("0.00")) && ldaTwrl9715.getForm_R_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(new  //Natural: IF FORM-R.TIRF-STATE-TAX-WTHLD ( #ST-OCC-A ) = 0.00 AND FORM-R.TIRF-LOC-TAX-WTHLD ( #ST-OCC-A ) = 0.00 AND FORM-R.TIRF-STATE-DISTR ( #ST-OCC-A ) = 0.00 AND FORM-R.TIRF-LOC-DISTR ( #ST-OCC-A ) = 0.00
            DbsDecimal("0.00")) && ldaTwrl9715.getForm_R_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(new DbsDecimal("0.00")) && ldaTwrl9715.getForm_R_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(new 
            DbsDecimal("0.00"))))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Stg_Pnd_Form_Cnt.getValue(1,":",2,1).nadd(1);                                                                                                                 //Natural: ADD 1 TO #STG.#FORM-CNT ( 1:2,1 )
        pnd_Stg_Pnd_State_Distr.getValue(1,":",2,1).nadd(ldaTwrl9715.getForm_R_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                                //Natural: ADD FORM-R.TIRF-STATE-DISTR ( #ST-OCC-A ) TO #STG.#STATE-DISTR ( 1:2,1 )
        pnd_Stg_Pnd_State_Tax.getValue(1,":",2,1).nadd(ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                              //Natural: ADD FORM-R.TIRF-STATE-TAX-WTHLD ( #ST-OCC-A ) TO #STG.#STATE-TAX ( 1:2,1 )
        pnd_Stg_Pnd_Local_Distr.getValue(1,":",2,1).nadd(ldaTwrl9715.getForm_R_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                                  //Natural: ADD FORM-R.TIRF-LOC-DISTR ( #ST-OCC-A ) TO #STG.#LOCAL-DISTR ( 1:2,1 )
        pnd_Stg_Pnd_Local_Tax.getValue(1,":",2,1).nadd(ldaTwrl9715.getForm_R_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                                //Natural: ADD FORM-R.TIRF-LOC-TAX-WTHLD ( #ST-OCC-A ) TO #STG.#LOCAL-TAX ( 1:2,1 )
    }
    private void sub_Accum_Stg1() throws Exception                                                                                                                        //Natural: ACCUM-STG1
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #REPORT-INDEXES.#STG1 = 1 TO 3
        for (pnd_Report_Indexes_Pnd_Stg1.setValue(1); condition(pnd_Report_Indexes_Pnd_Stg1.lessOrEqual(3)); pnd_Report_Indexes_Pnd_Stg1.nadd(1))
        {
            pnd_Stg_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Stg1,1).nadd(pnd_Recon_Case_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Stg1));                         //Natural: ADD #RECON-CASE.#FORM-CNT ( #STG1 ) TO #STG.#FORM-CNT ( #STG1,1 )
            pnd_Stg_Pnd_State_Distr.getValue(pnd_Report_Indexes_Pnd_Stg1,1).nadd(pnd_Recon_Case_Pnd_State_Distr.getValue(pnd_Report_Indexes_Pnd_Stg1));                   //Natural: ADD #RECON-CASE.#STATE-DISTR ( #STG1 ) TO #STG.#STATE-DISTR ( #STG1,1 )
            pnd_Stg_Pnd_State_Tax.getValue(pnd_Report_Indexes_Pnd_Stg1,1).nadd(pnd_Recon_Case_Pnd_State_Tax.getValue(pnd_Report_Indexes_Pnd_Stg1));                       //Natural: ADD #RECON-CASE.#STATE-TAX ( #STG1 ) TO #STG.#STATE-TAX ( #STG1,1 )
            pnd_Stg_Pnd_Local_Distr.getValue(pnd_Report_Indexes_Pnd_Stg1,1).nadd(pnd_Recon_Case_Pnd_Local_Distr.getValue(pnd_Report_Indexes_Pnd_Stg1));                   //Natural: ADD #RECON-CASE.#LOCAL-DISTR ( #STG1 ) TO #STG.#LOCAL-DISTR ( #STG1,1 )
            pnd_Stg_Pnd_Local_Tax.getValue(pnd_Report_Indexes_Pnd_Stg1,1).nadd(pnd_Recon_Case_Pnd_Local_Tax.getValue(pnd_Report_Indexes_Pnd_Stg1));                       //Natural: ADD #RECON-CASE.#LOCAL-TAX ( #STG1 ) TO #STG.#LOCAL-TAX ( #STG1,1 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Accum_Sta() throws Exception                                                                                                                         //Natural: ACCUM-STA
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        pnd_Sta_Pnd_Form_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #STA.#FORM-CNT
        pnd_Sta_Pnd_State_Distr.nadd(ldaTwrl9715.getForm_R_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                                                    //Natural: ADD FORM-R.TIRF-STATE-DISTR ( #ST-OCC-A ) TO #STA.#STATE-DISTR
        pnd_Sta_Pnd_State_Tax.nadd(ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                                                  //Natural: ADD FORM-R.TIRF-STATE-TAX-WTHLD ( #ST-OCC-A ) TO #STA.#STATE-TAX
        pnd_Sta_Pnd_Local_Distr.nadd(ldaTwrl9715.getForm_R_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                                                      //Natural: ADD FORM-R.TIRF-LOC-DISTR ( #ST-OCC-A ) TO #STA.#LOCAL-DISTR
        pnd_Sta_Pnd_Local_Tax.nadd(ldaTwrl9715.getForm_R_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                                                    //Natural: ADD FORM-R.TIRF-LOC-TAX-WTHLD ( #ST-OCC-A ) TO #STA.#LOCAL-TAX
    }
    private void sub_Update_Form() throws Exception                                                                                                                       //Natural: UPDATE-FORM
    {
        if (BLNatReinput.isReinput()) return;

        G_FORM:                                                                                                                                                           //Natural: GET FORM-U #CASE-FIELDS.#ACTIVE-ISN
        vw_form_U.readByID(pnd_Case_Fields_Pnd_Active_Isn.getLong(), "G_FORM");
        if (condition(pnd_Case_Fields_Pnd_State_Added.getBoolean()))                                                                                                      //Natural: IF #CASE-FIELDS.#STATE-ADDED
        {
            pnd_Case_Fields_Pnd_St_Occ_A.compute(new ComputeParameters(false, pnd_Case_Fields_Pnd_St_Occ_A), form_U_Count_Casttirf_1099_R_State_Grp.add(1));              //Natural: ASSIGN #CASE-FIELDS.#ST-OCC-A := FORM-U.C*TIRF-1099-R-STATE-GRP + 1
            form_U_Tirf_State_Code.getValue(pnd_Case_Fields_Pnd_St_Occ_A).setValue(pnd_Ws_Pnd_State);                                                                     //Natural: ASSIGN FORM-U.TIRF-STATE-CODE ( #ST-OCC-A ) := #WS.#STATE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.getBoolean()))                                                                                     //Natural: IF #PROCESS-REPORTING-ACTIVE-REC
        {
            pnd_Case_Fields_Pnd_Update_With_Reporting.setValue(true);                                                                                                     //Natural: ASSIGN #UPDATE-WITH-REPORTING := TRUE
            if (condition(pnd_Hold_Fields_Pnd_Hold_Rej_Ind.equals("Y")))                                                                                                  //Natural: IF #HOLD-REJ-IND = 'Y'
            {
                form_U_Tirf_State_Auth_Rpt_Ind.getValue(pnd_Case_Fields_Pnd_St_Occ_A).setValue("H");                                                                      //Natural: ASSIGN FORM-U.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-A ) := 'H'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                form_U_Tirf_State_Auth_Rpt_Ind.getValue(pnd_Case_Fields_Pnd_St_Occ_A).setValue("C");                                                                      //Natural: ASSIGN FORM-U.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-A ) := 'C'
            }                                                                                                                                                             //Natural: END-IF
            form_U_Tirf_State_Auth_Rpt_Date.getValue(pnd_Case_Fields_Pnd_St_Occ_A).setValue(pnd_Ws_Pnd_Datx);                                                             //Natural: ASSIGN FORM-U.TIRF-STATE-AUTH-RPT-DATE ( #ST-OCC-A ) := #WS.#DATX
            form_U_Tirf_Lu_User.setValue(Global.getINIT_USER());                                                                                                          //Natural: ASSIGN FORM-U.TIRF-LU-USER := *INIT-USER
            form_U_Tirf_Lu_Ts.setValue(pnd_Ws_Pnd_Timx);                                                                                                                  //Natural: ASSIGN FORM-U.TIRF-LU-TS := #WS.#TIMX
            vw_form_U.updateDBRow("G_FORM");                                                                                                                              //Natural: UPDATE ( G-FORM. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.getBoolean() && ! (pnd_Case_Fields_Pnd_Update_With_Reporting.getBoolean())))                       //Natural: IF #PROCESS-RECON-ACTIVE-REC AND NOT #UPDATE-WITH-REPORTING
            {
                if (condition(ldaTwrl9715.getForm_R_Tirf_State_Hardcopy_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(" ")))                                        //Natural: IF FORM-R.TIRF-STATE-HARDCOPY-IND ( #ST-OCC-A ) EQ ' '
                {
                    form_U_Tirf_State_Auth_Rpt_Ind.getValue(pnd_Case_Fields_Pnd_St_Occ_A).setValue(" ");                                                                  //Natural: ASSIGN FORM-U.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-A ) := ' '
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Hold_Fields_Pnd_Hold_Rej_Ind.equals("Y")))                                                                                          //Natural: IF #HOLD-REJ-IND = 'Y'
                    {
                        form_U_Tirf_State_Auth_Rpt_Ind.getValue(pnd_Case_Fields_Pnd_St_Occ_A).setValue("J");                                                              //Natural: ASSIGN FORM-U.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-A ) := 'J'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        form_U_Tirf_State_Auth_Rpt_Ind.getValue(pnd_Case_Fields_Pnd_St_Occ_A).setValue("X");                                                              //Natural: ASSIGN FORM-U.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-A ) := 'X'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                form_U_Tirf_State_Auth_Rpt_Date.getValue(pnd_Case_Fields_Pnd_St_Occ_A).setValue(pnd_Ws_Pnd_Datx);                                                         //Natural: ASSIGN FORM-U.TIRF-STATE-AUTH-RPT-DATE ( #ST-OCC-A ) := #WS.#DATX
                form_U_Tirf_Lu_User.setValue(Global.getINIT_USER());                                                                                                      //Natural: ASSIGN FORM-U.TIRF-LU-USER := *INIT-USER
                form_U_Tirf_Lu_Ts.setValue(pnd_Ws_Pnd_Timx);                                                                                                              //Natural: ASSIGN FORM-U.TIRF-LU-TS := #WS.#TIMX
                vw_form_U.updateDBRow("G_FORM");                                                                                                                          //Natural: UPDATE ( G-FORM. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Setup_Recon_Case() throws Exception                                                                                                                  //Natural: SETUP-RECON-CASE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pnd_Case_Fields_Pnd_Recon_Summary.reset();                                                                                                                        //Natural: RESET #CASE-FIELDS.#RECON-SUMMARY #CASE-FIELDS.#RECON-DETAIL
        pnd_Case_Fields_Pnd_Recon_Detail.reset();
        if (condition(pnd_Case_Fields_Pnd_Prior_Record.getBoolean() && ldaTwrl9710.getForm_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals(new       //Natural: IF #CASE-FIELDS.#PRIOR-RECORD AND FORM.TIRF-STATE-TAX-WTHLD ( #ST-OCC-I ) = 0.00 AND FORM.TIRF-LOC-TAX-WTHLD ( #ST-OCC-I ) = 0.00 AND FORM.TIRF-STATE-DISTR ( #ST-OCC-I ) = 0.00 AND FORM.TIRF-LOC-DISTR ( #ST-OCC-I ) = 0.00
            DbsDecimal("0.00")) && ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals(new DbsDecimal("0.00")) && ldaTwrl9710.getForm_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals(new 
            DbsDecimal("0.00")) && ldaTwrl9710.getForm_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals(new DbsDecimal("0.00"))))
        {
            pnd_Case_Fields_Pnd_Prior_Record.reset();                                                                                                                     //Natural: RESET #CASE-FIELDS.#PRIOR-RECORD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A).notEquals(new DbsDecimal("0.00")) || ldaTwrl9715.getForm_R_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A).notEquals(new  //Natural: IF ( FORM-R.TIRF-STATE-TAX-WTHLD ( #ST-OCC-A ) NE 0.00 OR FORM-R.TIRF-LOC-TAX-WTHLD ( #ST-OCC-A ) NE 0.00 OR FORM-R.TIRF-STATE-DISTR ( #ST-OCC-A ) NE 0.00 OR FORM-R.TIRF-LOC-DISTR ( #ST-OCC-A ) NE 0.00 ) OR #CASE-FIELDS.#PRIOR-RECORD AND ( FORM.TIRF-STATE-TAX-WTHLD ( #ST-OCC-I ) NE 0.00 OR FORM.TIRF-LOC-TAX-WTHLD ( #ST-OCC-I ) NE 0.00 OR FORM.TIRF-STATE-DISTR ( #ST-OCC-I ) NE 0.00 OR FORM.TIRF-LOC-DISTR ( #ST-OCC-I ) NE 0.00 )
            DbsDecimal("0.00")) || ldaTwrl9715.getForm_R_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A).notEquals(new DbsDecimal("0.00")) || 
            ldaTwrl9715.getForm_R_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A).notEquals(new DbsDecimal("0.00"))) || pnd_Case_Fields_Pnd_Prior_Record.getBoolean() 
            && (ldaTwrl9710.getForm_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_I).notEquals(new DbsDecimal("0.00")) || ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_I).notEquals(new 
            DbsDecimal("0.00")) || ldaTwrl9710.getForm_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_I).notEquals(new DbsDecimal("0.00")) || ldaTwrl9710.getForm_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_I).notEquals(new 
            DbsDecimal("0.00")))))
        {
            pnd_Case_Fields_Pnd_Recon_Summary.setValue(true);                                                                                                             //Natural: ASSIGN #CASE-FIELDS.#RECON-SUMMARY := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9715.getForm_R_Tirf_Empty_Form().getBoolean()))                                                                                              //Natural: IF FORM-R.TIRF-EMPTY-FORM
        {
            pnd_Ws_Pnd_Empty_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #EMPTY-CNT
            pnd_Ws_Pnd_Empty_Tot_Cnt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #EMPTY-TOT-CNT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Recon_Case.resetInitial();                                                                                                                                    //Natural: RESET INITIAL #RECON-CASE
        if (condition(pdaTwratin.getTwratin_Pnd_I_Tin_Type().notEquals(ldaTwrl9715.getForm_R_Tirf_Tax_Id_Type()) || pdaTwratin.getTwratin_Pnd_I_Tin().notEquals(ldaTwrl9715.getForm_R_Tirf_Tin()))) //Natural: IF TWRATIN.#I-TIN-TYPE NE FORM-R.TIRF-TAX-ID-TYPE OR TWRATIN.#I-TIN NE FORM-R.TIRF-TIN
        {
            pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(ldaTwrl9715.getForm_R_Tirf_Tax_Id_Type());                                                                    //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := FORM-R.TIRF-TAX-ID-TYPE
            pdaTwratin.getTwratin_Pnd_I_Tin().setValue(ldaTwrl9715.getForm_R_Tirf_Tin());                                                                                 //Natural: ASSIGN TWRATIN.#I-TIN := FORM-R.TIRF-TIN
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIN
            sub_Process_Tin();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Recon_Case_Pnd_Tin.setValue(pdaTwratin.getTwratin_Pnd_O_Tin());                                                                                               //Natural: ASSIGN #RECON-CASE.#TIN := TWRATIN.#O-TIN
        pnd_Recon_Case_Pnd_Cntrct_Py.setValueEdited(ldaTwrl9715.getForm_R_Tirf_Contract_Nbr(),new ReportEditMask("XXXXXXX-X/"));                                          //Natural: MOVE EDITED FORM-R.TIRF-CONTRACT-NBR ( EM = XXXXXXX-X/ ) TO #RECON-CASE.#CNTRCT-PY
        setValueToSubstring(ldaTwrl9715.getForm_R_Tirf_Payee_Cde(),pnd_Recon_Case_Pnd_Cntrct_Py,11,2);                                                                    //Natural: MOVE FORM-R.TIRF-PAYEE-CDE TO SUBSTR ( #RECON-CASE.#CNTRCT-PY,11,2 )
        pnd_Recon_Case_Pnd_Form_Cnt.getValue(1).nadd(1);                                                                                                                  //Natural: ADD 1 TO #RECON-CASE.#FORM-CNT ( 1 )
        pnd_Recon_Case_Pnd_State_Distr.getValue(1).setValue(ldaTwrl9715.getForm_R_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                             //Natural: ASSIGN #RECON-CASE.#STATE-DISTR ( 1 ) := FORM-R.TIRF-STATE-DISTR ( #ST-OCC-A )
        pnd_Recon_Case_Pnd_State_Tax.getValue(1).setValue(ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                           //Natural: ASSIGN #RECON-CASE.#STATE-TAX ( 1 ) := FORM-R.TIRF-STATE-TAX-WTHLD ( #ST-OCC-A )
        pnd_Recon_Case_Pnd_Local_Distr.getValue(1).setValue(ldaTwrl9715.getForm_R_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                               //Natural: ASSIGN #RECON-CASE.#LOCAL-DISTR ( 1 ) := FORM-R.TIRF-LOC-DISTR ( #ST-OCC-A )
        pnd_Recon_Case_Pnd_Local_Tax.getValue(1).setValue(ldaTwrl9715.getForm_R_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                             //Natural: ASSIGN #RECON-CASE.#LOCAL-TAX ( 1 ) := FORM-R.TIRF-LOC-TAX-WTHLD ( #ST-OCC-A )
        if (condition(pnd_Case_Fields_Pnd_Prior_Record.getBoolean()))                                                                                                     //Natural: IF #CASE-FIELDS.#PRIOR-RECORD
        {
            pnd_Recon_Case_Pnd_Form_Cnt.getValue(2).nadd(1);                                                                                                              //Natural: ADD 1 TO #RECON-CASE.#FORM-CNT ( 2 )
            pnd_Recon_Case_Pnd_State_Distr.getValue(2).setValue(ldaTwrl9710.getForm_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_I));                           //Natural: ASSIGN #RECON-CASE.#STATE-DISTR ( 2 ) := FORM.TIRF-STATE-DISTR ( #ST-OCC-I )
            pnd_Recon_Case_Pnd_State_Tax.getValue(2).setValue(ldaTwrl9710.getForm_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_I));                         //Natural: ASSIGN #RECON-CASE.#STATE-TAX ( 2 ) := FORM.TIRF-STATE-TAX-WTHLD ( #ST-OCC-I )
            pnd_Recon_Case_Pnd_Local_Distr.getValue(2).setValue(ldaTwrl9710.getForm_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_I));                             //Natural: ASSIGN #RECON-CASE.#LOCAL-DISTR ( 2 ) := FORM.TIRF-LOC-DISTR ( #ST-OCC-I )
            pnd_Recon_Case_Pnd_Local_Tax.getValue(2).setValue(ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_I));                           //Natural: ASSIGN #RECON-CASE.#LOCAL-TAX ( 2 ) := FORM.TIRF-LOC-TAX-WTHLD ( #ST-OCC-I )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Recon_Case_Pnd_Form_Cnt.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Form_Cnt.getValue(3)), pnd_Recon_Case_Pnd_Form_Cnt.getValue(1).subtract(pnd_Recon_Case_Pnd_Form_Cnt.getValue(2))); //Natural: ASSIGN #RECON-CASE.#FORM-CNT ( 3 ) := #RECON-CASE.#FORM-CNT ( 1 ) - #RECON-CASE.#FORM-CNT ( 2 )
        if (condition(pnd_Recon_Case_Pnd_State_Tax.getValue(1).equals(new DbsDecimal("0.00")) && pnd_Recon_Case_Pnd_Local_Tax.getValue(1).equals(new DbsDecimal("0.00"))  //Natural: IF #RECON-CASE.#STATE-TAX ( 1 ) = 0.00 AND #RECON-CASE.#LOCAL-TAX ( 1 ) = 0.00 AND #RECON-CASE.#STATE-DISTR ( 1 ) = 0.00 AND #RECON-CASE.#LOCAL-DISTR ( 1 ) = 0.00
            && pnd_Recon_Case_Pnd_State_Distr.getValue(1).equals(new DbsDecimal("0.00")) && pnd_Recon_Case_Pnd_Local_Distr.getValue(1).equals(new DbsDecimal("0.00"))))
        {
            pnd_Recon_Case_Pnd_State_Distr.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_State_Distr.getValue(3)), pnd_Recon_Case_Pnd_State_Distr.getValue(2).multiply(-1)); //Natural: ASSIGN #RECON-CASE.#STATE-DISTR ( 3 ) := #RECON-CASE.#STATE-DISTR ( 2 ) * -1
            pnd_Recon_Case_Pnd_State_Tax.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_State_Tax.getValue(3)), pnd_Recon_Case_Pnd_State_Tax.getValue(2).multiply(-1)); //Natural: ASSIGN #RECON-CASE.#STATE-TAX ( 3 ) := #RECON-CASE.#STATE-TAX ( 2 ) * -1
            pnd_Recon_Case_Pnd_Local_Distr.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Local_Distr.getValue(3)), pnd_Recon_Case_Pnd_Local_Distr.getValue(2).multiply(-1)); //Natural: ASSIGN #RECON-CASE.#LOCAL-DISTR ( 3 ) := #RECON-CASE.#LOCAL-DISTR ( 2 ) * -1
            pnd_Recon_Case_Pnd_Local_Tax.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Local_Tax.getValue(3)), pnd_Recon_Case_Pnd_Local_Tax.getValue(2).multiply(-1)); //Natural: ASSIGN #RECON-CASE.#LOCAL-TAX ( 3 ) := #RECON-CASE.#LOCAL-TAX ( 2 ) * -1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Recon_Case_Pnd_State_Distr.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_State_Distr.getValue(3)), pnd_Recon_Case_Pnd_State_Distr.getValue(1).subtract(pnd_Recon_Case_Pnd_State_Distr.getValue(2))); //Natural: ASSIGN #RECON-CASE.#STATE-DISTR ( 3 ) := #RECON-CASE.#STATE-DISTR ( 1 ) - #RECON-CASE.#STATE-DISTR ( 2 )
            pnd_Recon_Case_Pnd_State_Tax.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_State_Tax.getValue(3)), pnd_Recon_Case_Pnd_State_Tax.getValue(1).subtract(pnd_Recon_Case_Pnd_State_Tax.getValue(2))); //Natural: ASSIGN #RECON-CASE.#STATE-TAX ( 3 ) := #RECON-CASE.#STATE-TAX ( 1 ) - #RECON-CASE.#STATE-TAX ( 2 )
            pnd_Recon_Case_Pnd_Local_Distr.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Local_Distr.getValue(3)), pnd_Recon_Case_Pnd_Local_Distr.getValue(1).subtract(pnd_Recon_Case_Pnd_Local_Distr.getValue(2))); //Natural: ASSIGN #RECON-CASE.#LOCAL-DISTR ( 3 ) := #RECON-CASE.#LOCAL-DISTR ( 1 ) - #RECON-CASE.#LOCAL-DISTR ( 2 )
            pnd_Recon_Case_Pnd_Local_Tax.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Local_Tax.getValue(3)), pnd_Recon_Case_Pnd_Local_Tax.getValue(1).subtract(pnd_Recon_Case_Pnd_Local_Tax.getValue(2))); //Natural: ASSIGN #RECON-CASE.#LOCAL-TAX ( 3 ) := #RECON-CASE.#LOCAL-TAX ( 1 ) - #RECON-CASE.#LOCAL-TAX ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Recon_Case_Pnd_Form_Cnt.getValue(3).equals(new DbsDecimal("0.00")) && pnd_Recon_Case_Pnd_State_Distr.getValue(3).equals(new                     //Natural: IF #RECON-CASE.#FORM-CNT ( 3 ) = 0.00 AND #RECON-CASE.#STATE-DISTR ( 3 ) = 0.00 AND #RECON-CASE.#STATE-TAX ( 3 ) = 0.00 AND #RECON-CASE.#LOCAL-DISTR ( 3 ) = 0.00 AND #RECON-CASE.#LOCAL-TAX ( 3 ) = 0.00
            DbsDecimal("0.00")) && pnd_Recon_Case_Pnd_State_Tax.getValue(3).equals(new DbsDecimal("0.00")) && pnd_Recon_Case_Pnd_Local_Distr.getValue(3).equals(new 
            DbsDecimal("0.00")) && pnd_Recon_Case_Pnd_Local_Tax.getValue(3).equals(new DbsDecimal("0.00"))))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Case_Fields_Pnd_Recon_Detail.setValue(true);                                                                                                              //Natural: ASSIGN #CASE-FIELDS.#RECON-DETAIL := TRUE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_B_Record() throws Exception                                                                                                                   //Natural: CREATE-B-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        //*  WRITE 'Begin Subroutine: CREATE-B-RECORD'
        //*    3X #TWRAFORM.TIRF-CONTRACT-NBR
        //*  MOVE BY NAME FORM               TO #TWRAFORM-DET
        //*  #TWRAFORM.C-1099-R                 := FORM.C*TIRF-1099-R-STATE-GRP
        ldaTwrl3508.getPnd_Twrl3508().resetInitial();                                                                                                                     //Natural: RESET INITIAL #TWRL3508 IRS-B-REC
        ldaTwrl3507.getIrs_B_Rec().resetInitial();
        ldaTwrl3507.getIrs_B_Rec_Irs_B_1099_R_Area().reset();                                                                                                             //Natural: RESET IRS-B-REC.IRS-B-1099-R-AREA
        if (condition(pnd_Case_Fields_Pnd_Already_Reported.getBoolean()))                                                                                                 //Natural: IF #ALREADY-REPORTED
        {
            ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind().setValue("C");                                                                                                     //Natural: ASSIGN #TWRL3508.#CORR-IND := 'C'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind().setValue(" ");                                                                                                     //Natural: ASSIGN #TWRL3508.#CORR-IND := ' '
        }                                                                                                                                                                 //Natural: END-IF
        //* *#CASE-FIELDS.#LATEST-FORM          := TRUE
        //*  03/06/2007
        ldaTwrl3507.getIrs_B_Rec_Irs_B_Year().setValue(pdaTwraform.getPnd_Twraform_Tirf_Tax_Year());                                                                      //Natural: MOVE #TWRAFORM.TIRF-TAX-YEAR TO IRS-B-YEAR
        ldaTwrl3508.getPnd_Twrl3508_Pnd_Tax_Year().setValue(ldaTwrl3507.getIrs_B_Rec_Irs_B_Year());                                                                       //Natural: ASSIGN #TWRL3508.#TAX-YEAR := IRS-B-YEAR
        ldaTwrl3508.getPnd_Twrl3508_Pnd_Company_Code().setValue(pdaTwraform.getPnd_Twraform_Tirf_Company_Cde());                                                          //Natural: ASSIGN #TWRL3508.#COMPANY-CODE := #TWRAFORM.TIRF-COMPANY-CDE
        ldaTwrl3508.getPnd_Twrl3508_Pnd_Form().setValue(pdaTwraform.getPnd_Twraform_Tirf_Form_Type());                                                                    //Natural: ASSIGN #TWRL3508.#FORM := #TWRAFORM.TIRF-FORM-TYPE
        pdaTwra5052.getPnd_Twra5052_Pnd_Lname().setValue(pdaTwraform.getPnd_Twraform_Tirf_Part_Last_Nme());                                                               //Natural: ASSIGN #TWRA5052.#LNAME := #TWRAFORM.TIRF-PART-LAST-NME
        //*  03/06/2007
        DbsUtil.callnat(Twrn5052.class , getCurrentProcessState(), pdaTwra5052.getPnd_Twra5052());                                                                        //Natural: CALLNAT 'TWRN5052' #TWRA5052
        if (condition(Global.isEscape())) return;
        //*  05/01/2007
        if (condition(pdaTwra5052.getPnd_Twra5052_Pnd_Ret_Code().equals(false)))                                                                                          //Natural: IF #TWRA5052.#RET-CODE = FALSE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"Abend In Name Control Routine 'TWRN5052'",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Return Message...:",pdaTwra5052.getPnd_Twra5052_Pnd_Ret_Msg(),new  //Natural: WRITE ( 00 ) '***' 06T 'Abend In Name Control Routine "TWRN5052"' 77T '***' / '***' 06T 'Return Message...:' #TWRA5052.#RET-MSG 77T '***' / '***' 06T 'Program..........:' *PROGRAM 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Program..........:",Global.getPROGRAM(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(93);  if (true) return;                                                                                                                     //Natural: TERMINATE 93
            //*  05/01/2007
            //*  03/06/2007
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl3507.getIrs_B_Rec_Irs_B_Name_Control().setValue(pdaTwra5052.getPnd_Twra5052_Pnd_Lname_Control());                                                          //Natural: ASSIGN IRS-B-NAME-CONTROL := #TWRA5052.#LNAME-CONTROL
        short decideConditionsMet2302 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #TWRAFORM.TIRF-TAX-ID-TYPE;//Natural: VALUE '1', '3'
        if (condition((pdaTwraform.getPnd_Twraform_Tirf_Tax_Id_Type().equals("1") || pdaTwraform.getPnd_Twraform_Tirf_Tax_Id_Type().equals("3"))))
        {
            decideConditionsMet2302++;
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Tin_Type().setValue("2");                                                                                                      //Natural: ASSIGN IRS-B-TIN-TYPE := '2'
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Tin().setValue(pdaTwraform.getPnd_Twraform_Tirf_Tin());                                                                        //Natural: ASSIGN IRS-B-TIN := #TWRAFORM.TIRF-TIN
        }                                                                                                                                                                 //Natural: VALUE '2'
        else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Tax_Id_Type().equals("2"))))
        {
            decideConditionsMet2302++;
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Tin_Type().setValue("1");                                                                                                      //Natural: ASSIGN IRS-B-TIN-TYPE := '1'
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Tin().setValue(pdaTwraform.getPnd_Twraform_Tirf_Tin());                                                                        //Natural: ASSIGN IRS-B-TIN := #TWRAFORM.TIRF-TIN
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pdaTwraform.getPnd_Twraform_Tirf_Tin().getSubstring(10,1).equals(" ")))                                                                             //Natural: IF SUBSTR ( #TWRAFORM.TIRF-TIN,10,1 ) = ' '
        {
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Contract_Payee().setValue(pdaTwraform.getPnd_Twraform_Tirf_Tin().getSubstring(6,4));                                           //Natural: MOVE SUBSTR ( #TWRAFORM.TIRF-TIN,6,4 ) TO IRS-B-CONTRACT-PAYEE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Contract_Payee().setValue(pdaTwraform.getPnd_Twraform_Tirf_Tin().getSubstring(7,4));                                           //Natural: MOVE SUBSTR ( #TWRAFORM.TIRF-TIN,7,4 ) TO IRS-B-CONTRACT-PAYEE
        }                                                                                                                                                                 //Natural: END-IF
        setValueToSubstring(pdaTwraform.getPnd_Twraform_Tirf_Contract_Nbr(),ldaTwrl3507.getIrs_B_Rec_Irs_B_Contract_Payee(),5,8);                                         //Natural: MOVE #TWRAFORM.TIRF-CONTRACT-NBR TO SUBSTR ( IRS-B-CONTRACT-PAYEE, 5,8 )
        setValueToSubstring(pdaTwraform.getPnd_Twraform_Tirf_Payee_Cde(),ldaTwrl3507.getIrs_B_Rec_Irs_B_Contract_Payee(),13,2);                                           //Natural: MOVE #TWRAFORM.TIRF-PAYEE-CDE TO SUBSTR ( IRS-B-CONTRACT-PAYEE,13,2 )
        pnd_Ws_Pnd_Temp_Seq_N.setValue(pdaTwraform.getPnd_Twraform_Tirf_Irs_Form_Seq());                                                                                  //Natural: ASSIGN #WS.#TEMP-SEQ-N := #TWRAFORM.TIRF-IRS-FORM-SEQ
        setValueToSubstring(pnd_Ws_Pnd_Temp_Seq,ldaTwrl3507.getIrs_B_Rec_Irs_B_Contract_Payee(),15,3);                                                                    //Natural: MOVE #WS.#TEMP-SEQ TO SUBSTR ( IRS-B-CONTRACT-PAYEE,15,3 )
        ldaTwrl3507.getIrs_B_Rec_Irs_B_Name_Line_1().setValue(DbsUtil.compress(pdaTwraform.getPnd_Twraform_Tirf_Part_Last_Nme(), pdaTwraform.getPnd_Twraform_Tirf_Part_First_Nme(),  //Natural: COMPRESS #TWRAFORM.TIRF-PART-LAST-NME #TWRAFORM.TIRF-PART-FIRST-NME #TWRAFORM.TIRF-PART-MDDLE-NME INTO IRS-B-NAME-LINE-1
            pdaTwraform.getPnd_Twraform_Tirf_Part_Mddle_Nme()));
        ldaTwrl3507.getIrs_B_Rec_Irs_B_Address_Line().setValue(pdaTwraform.getPnd_Twraform_Tirf_Street_Addr());                                                           //Natural: ASSIGN IRS-B-ADDRESS-LINE := #TWRAFORM.TIRF-STREET-ADDR
        pnd_Ws_Pnd_I.reset();                                                                                                                                             //Natural: RESET #I
        if (condition(pdaTwraform.getPnd_Twraform_Tirf_Geo_Cde().equals("00")))                                                                                           //Natural: IF #TWRAFORM.TIRF-GEO-CDE = '00'
        {
            pnd_Case_Fields_Pnd_Geo.setValue(pdaTwraform.getPnd_Twraform_Tirf_Apo_Geo_Cde());                                                                             //Natural: ASSIGN #CASE-FIELDS.#GEO := #TWRAFORM.TIRF-APO-GEO-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  -ALPHA
            if (condition(pdaTwraform.getPnd_Twraform_Tirf_Geo_Cde().greaterOrEqual("01") && pdaTwraform.getPnd_Twraform_Tirf_Geo_Cde().lessOrEqual(pnd_Ws_Const_Pnd_State_Max_A)  //Natural: IF #TWRAFORM.TIRF-GEO-CDE = '01' THRU #STATE-MAX-A AND #TWRAFORM.TIRF-GEO-CDE = MASK ( 99 )
                && DbsUtil.maskMatches(pdaTwraform.getPnd_Twraform_Tirf_Geo_Cde(),"99")))
            {
                pnd_Ws_Pnd_I.compute(new ComputeParameters(false, pnd_Ws_Pnd_I), pdaTwraform.getPnd_Twraform_Tirf_Geo_Cde().val());                                       //Natural: ASSIGN #WS.#I := VAL ( #TWRAFORM.TIRF-GEO-CDE )
                pnd_Case_Fields_Pnd_Geo.setValue(pnd_State_Table_X_Tircntl_State_Alpha_Code.getValue(pnd_Ws_Pnd_I.getInt() + 2));                                         //Natural: ASSIGN #CASE-FIELDS.#GEO := #STATE-TABLE-X.TIRCNTL-STATE-ALPHA-CODE ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaTwraform.getPnd_Twraform_Tirf_Foreign_Addr().equals("Y")))                                                                                       //Natural: IF #TWRAFORM.TIRF-FOREIGN-ADDR = 'Y'
        {
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Foreign_Ind().setValue("1");                                                                                                   //Natural: ASSIGN IRS-B-FOREIGN-IND := '1'
            if (condition(pdaTwraform.getPnd_Twraform_Tirf_Province_Code().greaterOrEqual("074") && pdaTwraform.getPnd_Twraform_Tirf_Province_Code().lessOrEqual("088")   //Natural: IF #TWRAFORM.TIRF-PROVINCE-CODE = '074' THRU '088' AND #TWRAFORM.TIRF-PROVINCE-CODE = MASK ( 999 )
                && DbsUtil.maskMatches(pdaTwraform.getPnd_Twraform_Tirf_Province_Code(),"999")))
            {
                pnd_Ws_Pnd_I.compute(new ComputeParameters(false, pnd_Ws_Pnd_I), pdaTwraform.getPnd_Twraform_Tirf_Province_Code().val());                                 //Natural: ASSIGN #WS.#I := VAL ( #TWRAFORM.TIRF-PROVINCE-CODE )
                pdaTwraform.getPnd_Twraform_Tirf_Province_Code().setValue(pnd_Prov_Table_Tircntl_State_Alpha_Code.getValue(pnd_Ws_Pnd_I));                                //Natural: ASSIGN #TWRAFORM.TIRF-PROVINCE-CODE := #PROV-TABLE.TIRCNTL-STATE-ALPHA-CODE ( #I )
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl3507.getIrs_B_Rec_Irs_B_City_State_Zip().setValue(DbsUtil.compress(pdaTwraform.getPnd_Twraform_Tirf_City(), pdaTwraform.getPnd_Twraform_Tirf_Province_Code(),  //Natural: COMPRESS #TWRAFORM.TIRF-CITY #TWRAFORM.TIRF-PROVINCE-CODE #TWRAFORM.TIRF-ZIP #TWRAFORM.TIRF-COUNTRY-NAME INTO IRS-B-CITY-STATE-ZIP
                pdaTwraform.getPnd_Twraform_Tirf_Zip(), pdaTwraform.getPnd_Twraform_Tirf_Country_Name()));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl3507.getIrs_B_Rec_Irs_B_City().setValue(pdaTwraform.getPnd_Twraform_Tirf_City());                                                                      //Natural: ASSIGN IRS-B-CITY := #TWRAFORM.TIRF-CITY
            ldaTwrl3507.getIrs_B_Rec_Irs_B_State().setValue(pnd_Case_Fields_Pnd_Geo);                                                                                     //Natural: ASSIGN IRS-B-STATE := #CASE-FIELDS.#GEO
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Zip().setValue(pdaTwraform.getPnd_Twraform_Tirf_Zip());                                                                        //Natural: ASSIGN IRS-B-ZIP := #TWRAFORM.TIRF-ZIP
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #TWRAFORM.TIRF-FORM-TYPE         = 1  /* 1099-R
        if (condition(pdaTwraform.getPnd_Twraform_Tirf_Ira_Distr().equals("Y")))                                                                                          //Natural: IF #TWRAFORM.TIRF-IRA-DISTR = 'Y'
        {
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Ira_Sep_Ind().setValue("1");                                                                                                   //Natural: ASSIGN IRS-B-IRA-SEP-IND := '1'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaTwradist.getPnd_Twradist_Pnd_In_Dist().notEquals(pdaTwraform.getPnd_Twraform_Tirf_Distribution_Cde())))                                          //Natural: IF #TWRADIST.#IN-DIST NE #TWRAFORM.TIRF-DISTRIBUTION-CDE
        {
            pdaTwradist.getPnd_Twradist_Pnd_In_Dist().setValue(pdaTwraform.getPnd_Twraform_Tirf_Distribution_Cde());                                                      //Natural: ASSIGN #TWRADIST.#IN-DIST := #TWRAFORM.TIRF-DISTRIBUTION-CDE
                                                                                                                                                                          //Natural: PERFORM PROCESS-DISTRIB
            sub_Process_Distrib();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl3507.getIrs_B_Rec_Irs_B_Dist_Code().setValue(pdaTwradist.getPnd_Twradist_Pnd_Out_Dist());                                                                  //Natural: ASSIGN IRS-B-DIST-CODE := #TWRADIST.#OUT-DIST
        ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_9().setValue(pdaTwraform.getPnd_Twraform_Tirf_Tot_Contractual_Ivc());                                                  //Natural: ASSIGN IRS-B-PAYMENT-AMT-9 := #TWRAFORM.TIRF-TOT-CONTRACTUAL-IVC
        pnd_Ws_Pnd_I.setValue(pnd_Case_Fields_Pnd_St_Occ_A);                                                                                                              //Natural: ASSIGN #I := #ST-OCC-A
        ldaTwrl3507.getIrs_B_Rec_Irs_B_Corr_Ind().setValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind());                                                                   //Natural: ASSIGN IRS-B-CORR-IND := #TWRL3508.#CORR-IND
        //*  WRITE '=' #TWRAFORM.TIRF-STATE-REPORTING(#I) '=' #I  '  s/b "Y" '
        //*  WRITE '#TWRAFORM:'
        //*    #TWRAFORM.TIRF-TIN #TWRAFORM.TIRF-CONTRACT-NBR
        //*  WRITE 'FORM-R:   '
        //*    FORM-R.TIRF-TIN FORM-R.TIRF-CONTRACT-NBR
        //*  WRITE 'FORM::    '
        //*    FORM.TIRF-TIN FORM.TIRF-CONTRACT-NBR
        if (condition(pdaTwraform.getPnd_Twraform_Tirf_State_Reporting().getValue(pnd_Ws_Pnd_I).equals("Y")))                                                             //Natural: IF #TWRAFORM.TIRF-STATE-REPORTING ( #I ) = 'Y'
        {
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_1().setValue(pdaTwraform.getPnd_Twraform_Tirf_Res_Gross_Amt().getValue(pnd_Ws_Pnd_I));                             //Natural: ASSIGN IRS-B-PAYMENT-AMT-1 := #TWRAFORM.TIRF-RES-GROSS-AMT ( #I )
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_B().setValue(pdaTwraform.getPnd_Twraform_Tirf_Res_Irr_Amt().getValue(pnd_Ws_Pnd_I));                               //Natural: ASSIGN IRS-B-PAYMENT-AMT-B := #TWRAFORM.TIRF-RES-IRR-AMT ( #I )
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_2().setValue(pdaTwraform.getPnd_Twraform_Tirf_Res_Taxable_Amt().getValue(pnd_Ws_Pnd_I));                           //Natural: ASSIGN IRS-B-PAYMENT-AMT-2 := #TWRAFORM.TIRF-RES-TAXABLE-AMT ( #I )
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_4().setValue(pdaTwraform.getPnd_Twraform_Tirf_Res_Fed_Tax_Wthld().getValue(pnd_Ws_Pnd_I));                         //Natural: ASSIGN IRS-B-PAYMENT-AMT-4 := #TWRAFORM.TIRF-RES-FED-TAX-WTHLD ( #I )
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_5().setValue(pdaTwraform.getPnd_Twraform_Tirf_Res_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                               //Natural: ASSIGN IRS-B-PAYMENT-AMT-5 := #TWRAFORM.TIRF-RES-IVC-AMT ( #I )
            if (condition(pdaTwraform.getPnd_Twraform_Tirf_Ira_Distr().equals("Y")))                                                                                      //Natural: IF #TWRAFORM.TIRF-IRA-DISTR = 'Y'
            {
                ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_A().setValue(pdaTwraform.getPnd_Twraform_Tirf_Res_Gross_Amt().getValue(pnd_Ws_Pnd_I));                         //Natural: ASSIGN IRS-B-PAYMENT-AMT-A := #TWRAFORM.TIRF-RES-GROSS-AMT ( #I )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTwraform.getPnd_Twraform_Tirf_Loc_Code().getValue(pnd_Ws_Pnd_I).equals(" ")))                                                                //Natural: IF #TWRAFORM.TIRF-LOC-CODE ( #I ) = ' '
            {
                setValueToSubstring(pdaTwraform.getPnd_Twraform_Tirf_State_Code().getValue(pnd_Ws_Pnd_I),ldaTwrl3507.getIrs_B_Rec_Irs_B_Contract_Payee(),                 //Natural: MOVE #TWRAFORM.TIRF-STATE-CODE ( #I ) TO SUBSTR ( IRS-B-CONTRACT-PAYEE,18,2 )
                    18,2);
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                setValueToSubstring(pdaTwraform.getPnd_Twraform_Tirf_Loc_Code().getValue(pnd_Ws_Pnd_I),ldaTwrl3507.getIrs_B_Rec_Irs_B_Contract_Payee(),                   //Natural: MOVE #TWRAFORM.TIRF-LOC-CODE ( #I ) TO SUBSTR ( IRS-B-CONTRACT-PAYEE,18,3 )
                    18,3);
            }                                                                                                                                                             //Natural: END-IF
            //*  UTAH
            short decideConditionsMet2380 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #STATE-TABLE.TIRCNTL-STATE-OLD-CODE;//Natural: VALUE '049'
            if (condition((pnd_State_Table_Tircntl_State_Old_Code.equals("049"))))
            {
                decideConditionsMet2380++;
                ldaTwrl3507.getIrs_B_Rec_Irs_B_Federal_State_Code().setValue(pnd_State_Table_Tircntl_State_Alpha_Code);                                                   //Natural: ASSIGN IRS-B-FEDERAL-STATE-CODE := #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ldaTwrl3507.getIrs_B_Rec_Irs_B_Federal_State_Code().setValueEdited(pnd_State_Table_Tircntl_Seq_Nbr,new ReportEditMask("99"));                             //Natural: MOVE EDITED #STATE-TABLE.TIRCNTL-SEQ-NBR ( EM = 99 ) TO IRS-B-FEDERAL-STATE-CODE
                ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().setValue(pnd_State_Table_Tircntl_Seq_Nbr);                                                                  //Natural: ASSIGN #TWRL3508.#IRS-B-STATE := #STATE-TABLE.TIRCNTL-SEQ-NBR
            }                                                                                                                                                             //Natural: END-DECIDE
            ldaTwrl3507.getIrs_B_Rec_Irs_B_State_Tax_Num().setValue(pdaTwraform.getPnd_Twraform_Tirf_State_Tax_Wthld().getValue(pnd_Ws_Pnd_I));                           //Natural: ASSIGN IRS-B-STATE-TAX-NUM := #TWRAFORM.TIRF-STATE-TAX-WTHLD ( #I )
            pnd_Case_Fields_Pnd_State_Dist.setValue(pdaTwraform.getPnd_Twraform_Tirf_State_Distr().getValue(pnd_Ws_Pnd_I));                                               //Natural: ASSIGN #CASE-FIELDS.#STATE-DIST := #TWRAFORM.TIRF-STATE-DISTR ( #I )
            if (condition(pdaTwraform.getPnd_Twraform_Tirf_Loc_Code().getValue(pnd_Ws_Pnd_I).notEquals(" ") || pdaTwraform.getPnd_Twraform_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Pnd_I).notEquals(new  //Natural: IF #TWRAFORM.TIRF-LOC-CODE ( #I ) NE ' ' OR #TWRAFORM.TIRF-LOC-TAX-WTHLD ( #I ) NE 0.00
                DbsDecimal("0.00"))))
            {
                ldaTwrl3507.getIrs_B_Rec_Irs_B_Local_Tax_Num().setValue(pdaTwraform.getPnd_Twraform_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Pnd_I));                         //Natural: ASSIGN IRS-B-LOCAL-TAX-NUM := #TWRAFORM.TIRF-LOC-TAX-WTHLD ( #I )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTwraform.getPnd_Twraform_Tirf_Res_Taxable_Not_Det().getValue(pnd_Ws_Pnd_I).equals("Y")))                                                     //Natural: IF #TWRAFORM.TIRF-RES-TAXABLE-NOT-DET ( #I ) = 'Y'
            {
                ldaTwrl3507.getIrs_B_Rec_Irs_B_Taxable_Not_Det().setValue("1");                                                                                           //Natural: ASSIGN IRS-B-TAXABLE-NOT-DET := '1'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTwraform.getPnd_Twraform_Tirf_Roth_Year().notEquals(getZero())))                                                                             //Natural: IF #TWRAFORM.TIRF-ROTH-YEAR NE 0
            {
                pnd_Ws_Pnd_Year_N4.setValue(pdaTwraform.getPnd_Twraform_Tirf_Roth_Year());                                                                                //Natural: ASSIGN #YEAR-N4 := #TWRAFORM.TIRF-ROTH-YEAR
                ldaTwrl3507.getIrs_B_Rec_Irs_B_1st_Year_Roth_Contrib().setValueEdited(pnd_Ws_Pnd_Year_N4,new ReportEditMask("9999"));                                     //Natural: MOVE EDITED #YEAR-N4 ( EM = 9999 ) TO IRS-B-1ST-YEAR-ROTH-CONTRIB
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl3507.getIrs_B_Rec_Irs_B_1st_Year_Roth_Contrib().reset();                                                                                           //Natural: RESET IRS-B-1ST-YEAR-ROTH-CONTRIB
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FORMAT-SPECIAL-STATE-DATA
            sub_Format_Special_State_Data();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-B-REC
            sub_Write_B_Rec();
            if (condition(Global.isEscape())) {return;}
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_1().reset();                                                                                                       //Natural: RESET IRS-B-PAYMENT-AMT-1 IRS-B-PAYMENT-AMT-2 IRS-B-PAYMENT-AMT-4 IRS-B-PAYMENT-AMT-5 IRS-B-PAYMENT-AMT-9 IRS-B-PAYMENT-AMT-A IRS-B-STATE-TAX-NUM IRS-B-LOCAL-TAX-NUM IRS-B-FEDERAL-STATE-CODE #IRS-B-STATE
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_2().reset();
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_4().reset();
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_5().reset();
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_9().reset();
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_A().reset();
            ldaTwrl3507.getIrs_B_Rec_Irs_B_State_Tax_Num().reset();
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Local_Tax_Num().reset();
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Federal_State_Code().reset();
            ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().reset();
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_Special_State_Data() throws Exception                                                                                                         //Natural: FORMAT-SPECIAL-STATE-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        pnd_Special_Data.reset();                                                                                                                                         //Natural: RESET #SPECIAL-DATA
        //*  UTAH
        short decideConditionsMet2412 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE;//Natural: VALUE 'UT'
        if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("UT"))))
        {
            decideConditionsMet2412++;
            pnd_Special_Data_Pnd_Ut_State_Distrib.setValue(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_1());                                                               //Natural: ASSIGN #UT-STATE-DISTRIB := IRS-B-PAYMENT-AMT-1
            pnd_Special_Data_Pnd_Ut_State_Id.setValue(pnd_State_Table_Tircntl_State_Alpha_Code);                                                                          //Natural: ASSIGN #UT-STATE-ID := #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE
            //*  EINCHG
            short decideConditionsMet2417 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2417++;
                pnd_Temp_State_Id_20.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                                 //Natural: ASSIGN #TEMP-STATE-ID-20 := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
                DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch("-"), new ExamineDelete());                                               //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                pnd_Special_Data_Pnd_Ut_State_Tax_Id.setValue(pnd_Temp_State_Id_20);                                                                                      //Natural: ASSIGN #UT-STATE-TAX-ID := #TEMP-STATE-ID-20
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2417++;
                pnd_Temp_State_Id_20.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                                //Natural: ASSIGN #TEMP-STATE-ID-20 := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
                DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch("-"), new ExamineDelete());                                               //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                pnd_Special_Data_Pnd_Ut_State_Tax_Id.setValue(pnd_Temp_State_Id_20);                                                                                      //Natural: ASSIGN #UT-STATE-TAX-ID := #TEMP-STATE-ID-20
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  VERMONT
            pnd_Temp_State_Id_20.reset();                                                                                                                                 //Natural: RESET #TEMP-STATE-ID-20
        }                                                                                                                                                                 //Natural: VALUE 'VT'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("VT"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2433 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2433++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2433++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - No Tax ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - No Tax ID' INTO #VT-STATE-TAX-ID
                //*  ALABAMA                     ////*DP STARTS
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'AL'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("AL"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2444 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2444++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2444++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  ALASKA
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'AK'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("AK"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2455 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2455++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2455++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  PENNSYLVANIA
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'PA'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("PA"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2466 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2466++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2466++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  ARIZONA
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'AZ'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("AZ"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2477 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2477++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2477++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  ARKANSAS
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'AR'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("AR"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2488 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2488++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2488++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  CALIFORNIA
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'CA'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("CA"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2499 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2499++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2499++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  COLORADO
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'CO'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("CO"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2510 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2510++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2510++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  DELAWARE
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'DE'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("DE"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2521 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2521++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2521++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Irs_B_Special_Entry_Temp_De.setValue(pnd_Special_Data);                                                                                                   //Natural: ASSIGN #IRS-B-SPECIAL-ENTRY-TEMP-DE := #SPECIAL-DATA
            //*  DISTRICT OF COLUMBIA
            DbsUtil.examine(new ExamineSource(pnd_Irs_B_Special_Entry_Temp_De,true), new ExamineSearch("-"), new ExamineDelete());                                        //Natural: EXAMINE FULL VALUE #IRS-B-SPECIAL-ENTRY-TEMP-DE '-' DELETE
            pnd_Special_Data_Pnd_De_State_Tax_Id.setValue(pnd_Irs_B_Special_Entry_Temp_De);                                                                               //Natural: ASSIGN #DE-STATE-TAX-ID := #IRS-B-SPECIAL-ENTRY-TEMP-DE
        }                                                                                                                                                                 //Natural: VALUE 'DC'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("DC"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2535 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2535++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2535++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //* DP
                //*  FLORIDA
            }                                                                                                                                                             //Natural: END-DECIDE
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Federal_State_Code().setValue(" ");                                                                                            //Natural: ASSIGN IRS-B-FEDERAL-STATE-CODE := ' '
        }                                                                                                                                                                 //Natural: VALUE 'FL'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("FL"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2548 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2548++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2548++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  GEORGIA
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'GA'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("GA"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2559 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2559++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2559++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Irs_B_Special_Entry_Temp_Ga.setValue(pnd_Special_Data);                                                                                                   //Natural: ASSIGN #IRS-B-SPECIAL-ENTRY-TEMP-GA := #SPECIAL-DATA
            //*  HAWAII
            DbsUtil.examine(new ExamineSource(pnd_Irs_B_Special_Entry_Temp_Ga,true), new ExamineSearch("-"), new ExamineDelete());                                        //Natural: EXAMINE FULL VALUE #IRS-B-SPECIAL-ENTRY-TEMP-GA '-' DELETE
            pnd_Special_Data_Pnd_Ga_State_Tax_Id.setValue(pnd_Irs_B_Special_Entry_Temp_Ga);                                                                               //Natural: ASSIGN #GA-STATE-TAX-ID := #IRS-B-SPECIAL-ENTRY-TEMP-GA
        }                                                                                                                                                                 //Natural: VALUE 'HI'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("HI"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2573 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2573++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2573++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  IDAHO
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'ID'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("ID"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2584 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2584++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2584++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  ILLINOIS
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'IL'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("IL"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2595 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2595++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2595++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  INDIANA
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'IN'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("IN"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2606 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2606++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2606++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  IOWA
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'IA'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("IA"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2617 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2617++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2617++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  KANSAS
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'KS'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("KS"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2628 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2628++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2628++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Irs_B_Special_Entry_Temp_Ks.setValue(pnd_Special_Data);                                                                                                   //Natural: ASSIGN #IRS-B-SPECIAL-ENTRY-TEMP-KS := #SPECIAL-DATA
            //*  KENTUCKY
            DbsUtil.examine(new ExamineSource(pnd_Irs_B_Special_Entry_Temp_Ks,true), new ExamineSearch("-"), new ExamineDelete());                                        //Natural: EXAMINE FULL VALUE #IRS-B-SPECIAL-ENTRY-TEMP-KS '-' DELETE
            pnd_Special_Data_Pnd_Ks_State_Tax_Id.setValue(pnd_Irs_B_Special_Entry_Temp_Ks);                                                                               //Natural: ASSIGN #KS-STATE-TAX-ID := #IRS-B-SPECIAL-ENTRY-TEMP-KS
        }                                                                                                                                                                 //Natural: VALUE 'KY'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("KY"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2642 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2642++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2642++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //* DP
                //* DP
                //* DP
                //*  LOUSIANA
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Special_Data_Pnd_Ky_State_Code.setValue("21");                                                                                                            //Natural: ASSIGN #KY-STATE-CODE := '21'
            pnd_Special_Data_Pnd_Ky_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                     //Natural: ASSIGN #KY-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Federal_State_Code().setValue(" ");                                                                                            //Natural: ASSIGN IRS-B-FEDERAL-STATE-CODE := ' '
        }                                                                                                                                                                 //Natural: VALUE 'LA'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("LA"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2659 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2659++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2659++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Irs_B_Special_Entry_Temp_La.setValue(pnd_Special_Data);                                                                                                   //Natural: ASSIGN #IRS-B-SPECIAL-ENTRY-TEMP-LA := #SPECIAL-DATA
            //*  MAINE
            DbsUtil.examine(new ExamineSource(pnd_Irs_B_Special_Entry_Temp_La,true), new ExamineSearch("-"), new ExamineDelete());                                        //Natural: EXAMINE FULL VALUE #IRS-B-SPECIAL-ENTRY-TEMP-LA '-' DELETE
            pnd_Special_Data_Pnd_La_State_Tax_Id.setValue(pnd_Irs_B_Special_Entry_Temp_La);                                                                               //Natural: ASSIGN #LA-STATE-TAX-ID := #IRS-B-SPECIAL-ENTRY-TEMP-LA
        }                                                                                                                                                                 //Natural: VALUE 'ME'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("ME"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2673 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2673++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2673++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  MARYLAND
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'MD'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("MD"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2684 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2684++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2684++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  MASSACHUSETTS
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'MA'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("MA"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2695 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2695++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2695++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  MICHIGAN
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'MI'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("MI"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2706 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2706++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2706++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  MINNESOTA
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'MN'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("MN"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2717 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2717++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2717++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  MISSISSIPPI
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'MS'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("MS"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2728 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2728++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2728++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  MISSOURI
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'MO'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("MO"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2739 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2739++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2739++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  MONTANA
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'MT'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("MT"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2750 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2750++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2750++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  NEBRASKA
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'NE'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("NE"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2761 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2761++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2761++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  NEVADA
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'NV'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("NV"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2772 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2772++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2772++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  NEW HAMPSHIRE
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'NH'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("NH"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2783 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2783++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2783++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  NEW JERSEY
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'NJ'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("NJ"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2794 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2794++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2794++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  NEW MEXICO
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'NM'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("NM"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2805 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2805++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2805++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  NORTH CAROLINA
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'NC'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("NC"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2816 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2816++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2816++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  NORTH DAKOTA
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Special_Data_Pnd_Nc_State_Code.setValue("NC");                                                                                                            //Natural: ASSIGN #NC-STATE-CODE := 'NC'
            pnd_Special_Data_Pnd_Nc_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                     //Natural: ASSIGN #NC-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
        }                                                                                                                                                                 //Natural: VALUE 'ND'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("ND"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2829 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2829++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2829++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Irs_B_Special_Entry_Temp_Nd.setValue(pnd_Special_Data);                                                                                                   //Natural: ASSIGN #IRS-B-SPECIAL-ENTRY-TEMP-ND := #SPECIAL-DATA
            DbsUtil.examine(new ExamineSource(pnd_Irs_B_Special_Entry_Temp_Nd,true), new ExamineSearch("-"), new ExamineDelete());                                        //Natural: EXAMINE FULL VALUE #IRS-B-SPECIAL-ENTRY-TEMP-ND '-' DELETE
            //*  OHIO
            DbsUtil.examine(new ExamineSource(pnd_Irs_B_Special_Entry_Temp_Nd,true), new ExamineSearch(" "), new ExamineDelete());                                        //Natural: EXAMINE FULL VALUE #IRS-B-SPECIAL-ENTRY-TEMP-ND ' ' DELETE
            pnd_Special_Data_Pnd_Nd_State_Tax_Id.setValue(pnd_Irs_B_Special_Entry_Temp_Nd);                                                                               //Natural: ASSIGN #ND-STATE-TAX-ID := #IRS-B-SPECIAL-ENTRY-TEMP-ND
        }                                                                                                                                                                 //Natural: VALUE 'OH'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("OH"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2844 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2844++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2844++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  OKLAHOMA
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'OK'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("OK"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2855 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2855++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2855++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  OREGON
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'OR'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("OR"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2866 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2866++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2866++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Irs_B_Special_Entry_Temp_Or.setValue(pnd_Special_Data);                                                                                                   //Natural: ASSIGN #IRS-B-SPECIAL-ENTRY-TEMP-OR := #SPECIAL-DATA
            //*  PUERTO-RICO
            DbsUtil.examine(new ExamineSource(pnd_Irs_B_Special_Entry_Temp_Or,true), new ExamineSearch("-"), new ExamineDelete());                                        //Natural: EXAMINE FULL VALUE #IRS-B-SPECIAL-ENTRY-TEMP-OR '-' DELETE
            pnd_Special_Data_Pnd_Or_State_Tax_Id.setValue(pnd_Irs_B_Special_Entry_Temp_Or);                                                                               //Natural: ASSIGN #OR-STATE-TAX-ID := #IRS-B-SPECIAL-ENTRY-TEMP-OR
            pnd_Special_Data_Pnd_Or_State_Code.setValue("OR");                                                                                                            //Natural: ASSIGN #OR-STATE-CODE := 'OR'
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Federal_State_Code().setValue(" ");                                                                                            //Natural: ASSIGN IRS-B-FEDERAL-STATE-CODE := ' '
        }                                                                                                                                                                 //Natural: VALUE 'PR'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("PR"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2882 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2882++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2882++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  RHODE-ISLAND
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'RI'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("RI"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2893 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2893++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2893++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  SOUTH-CAROLINA
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'SC'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("SC"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2904 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2904++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2904++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  SOUTH-DAKOTA
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'SD'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("SD"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2915 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2915++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2915++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  TENNESSEE
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'TN'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("TN"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2926 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2926++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2926++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  TEXAS
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'TX'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("TX"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2937 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2937++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2937++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  VIRGINIA
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'VA'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("VA"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2948 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2948++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2948++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Irs_B_Special_Entry_Temp_Va.setValue(pnd_Special_Data);                                                                                                   //Natural: ASSIGN #IRS-B-SPECIAL-ENTRY-TEMP-VA := #SPECIAL-DATA
            //*  VIRGIN ISLANDS
            DbsUtil.examine(new ExamineSource(pnd_Irs_B_Special_Entry_Temp_Va,true), new ExamineSearch("-"), new ExamineDelete());                                        //Natural: EXAMINE FULL VALUE #IRS-B-SPECIAL-ENTRY-TEMP-VA '-' DELETE
            pnd_Special_Data_Pnd_Va_State_Tax_Id.setValue(pnd_Irs_B_Special_Entry_Temp_Va);                                                                               //Natural: ASSIGN #VA-STATE-TAX-ID := #IRS-B-SPECIAL-ENTRY-TEMP-VA
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Federal_State_Code().setValue(" ");                                                                                            //Natural: ASSIGN IRS-B-FEDERAL-STATE-CODE := ' '
        }                                                                                                                                                                 //Natural: VALUE 'VI'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("VI"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2963 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2963++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2963++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  WASHINGTON
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'WA'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("WA"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2974 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2974++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2974++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  WEST VIRGINIA
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'WV'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("WV"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2985 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2985++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2985++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //* DP
                //*  WINCONSIN
            }                                                                                                                                                             //Natural: END-DECIDE
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Federal_State_Code().setValue(" ");                                                                                            //Natural: ASSIGN IRS-B-FEDERAL-STATE-CODE := ' '
        }                                                                                                                                                                 //Natural: VALUE 'WI'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("WI"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet2998 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet2998++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet2998++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Irs_B_Special_Entry_Temp_Wi.setValue(pnd_Special_Data);                                                                                                   //Natural: ASSIGN #IRS-B-SPECIAL-ENTRY-TEMP-WI := #SPECIAL-DATA
            //*  WYOMING
            DbsUtil.examine(new ExamineSource(pnd_Irs_B_Special_Entry_Temp_Wi,true), new ExamineSearch("-"), new ExamineDelete());                                        //Natural: EXAMINE FULL VALUE #IRS-B-SPECIAL-ENTRY-TEMP-WI '-' DELETE
            pnd_Special_Data_Pnd_Wi_State_Tax_Id.setValue(pnd_Irs_B_Special_Entry_Temp_Wi);                                                                               //Natural: ASSIGN #WI-STATE-TAX-ID := #IRS-B-SPECIAL-ENTRY-TEMP-WI
        }                                                                                                                                                                 //Natural: VALUE 'WY'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("WY"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet3012 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet3012++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                 //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet3012++;
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                //Natural: ASSIGN #VT-STATE-TAX-ID := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Vt_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - NO TAX ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID' INTO #VT-STATE-TAX-ID
                //*  DP ENDS
                //*  CONNECTICUT
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'CT'
        else if (condition((pnd_State_Table_Tircntl_State_Alpha_Code.equals("CT"))))
        {
            decideConditionsMet2412++;
            //*  EINCHG
            short decideConditionsMet3024 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #TWRAFORM.TIRF-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("T") || pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("A"))))
            {
                decideConditionsMet3024++;
                pnd_Temp_State_Id_20.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Tiaa);                                                                                 //Natural: ASSIGN #TEMP-STATE-ID-20 := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TIAA
                DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch("-"), new ExamineDelete());                                               //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                pnd_Special_Data_Pnd_Ct_State_Tax_Id.setValue(pnd_Temp_State_Id_20);                                                                                      //Natural: ASSIGN #CT-STATE-TAX-ID := #TEMP-STATE-ID-20
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Company_Cde().equals("X"))))
            {
                decideConditionsMet3024++;
                pnd_Temp_State_Id_20.setValue(pnd_State_Table_Tircntl_State_Tax_Id_Trust);                                                                                //Natural: ASSIGN #TEMP-STATE-ID-20 := #STATE-TABLE.TIRCNTL-STATE-TAX-ID-TRUST
                DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch("-"), new ExamineDelete());                                               //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                pnd_Special_Data_Pnd_Ct_State_Tax_Id.setValue(pnd_Temp_State_Id_20);                                                                                      //Natural: ASSIGN #CT-STATE-TAX-ID := #TEMP-STATE-ID-20
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Special_Data_Pnd_Ct_State_Tax_Id.setValue(DbsUtil.compress(pnd_State_Table_Tircntl_State_Alpha_Code, " - No Tax ID"));                                //Natural: COMPRESS #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ' - No Tax ID' INTO #CT-STATE-TAX-ID
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Temp_State_Id_20.reset();                                                                                                                                 //Natural: RESET #TEMP-STATE-ID-20
        }                                                                                                                                                                 //Natural: ANY VALUE
        if (condition(decideConditionsMet2412 > 0))
        {
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Special_Data_Entries().setValue(pnd_Special_Data);                                                                             //Natural: ASSIGN IRS-B-SPECIAL-DATA-ENTRIES := #SPECIAL-DATA
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Write_B_Rec() throws Exception                                                                                                                       //Natural: WRITE-B-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        //*  WRITE 'Begin Subroutine:  WRITE-B-REC'
        //* *IF #TWRL3508.#COMPANY-CODE= 'T'       /* TIAA          /* EINCHG
        //*  TIAA          /* EINCHG
        if (condition(ldaTwrl3508.getPnd_Twrl3508_Pnd_Company_Code().equals("T") || ldaTwrl3508.getPnd_Twrl3508_Pnd_Company_Code().equals("A")))                          //Natural: IF #TWRL3508.#COMPANY-CODE = 'T' OR = 'A'
        {
            pnd_Ws_Pnd_Seq_Num_Tiaa.nadd(1);                                                                                                                              //Natural: ADD 1 TO #WS.#SEQ-NUM-TIAA
            ldaTwrl3508.getPnd_Twrl3508_Pnd_Seq_Num().setValue(pnd_Ws_Pnd_Seq_Num_Tiaa);                                                                                  //Natural: ASSIGN #TWRL3508.#SEQ-NUM := #WS.#SEQ-NUM-TIAA
            getWorkFiles().write(1, false, ldaTwrl3507.getIrs_B_Rec(), ldaTwrl3508.getPnd_Twrl3508());                                                                    //Natural: WRITE WORK FILE 1 IRS-B-REC #TWRL3508
            //*  TRUST
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Seq_Num_Trst.nadd(1);                                                                                                                              //Natural: ADD 1 TO #WS.#SEQ-NUM-TRST
            ldaTwrl3508.getPnd_Twrl3508_Pnd_Seq_Num().setValue(pnd_Ws_Pnd_Seq_Num_Trst);                                                                                  //Natural: ASSIGN #TWRL3508.#SEQ-NUM := #WS.#SEQ-NUM-TRST
            getWorkFiles().write(2, false, ldaTwrl3507.getIrs_B_Rec(), ldaTwrl3508.getPnd_Twrl3508());                                                                    //Natural: WRITE WORK FILE 2 IRS-B-REC #TWRL3508
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().reset();                                                                                                            //Natural: RESET #TWRL3508.#IRS-B-STATE
        short decideConditionsMet3061 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #TWRL3508.#CORR-IND;//Natural: VALUE ' '
        if (condition((ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind().equals(" "))))
        {
            decideConditionsMet3061++;
            pnd_Ws_Pnd_Corr_Idx.setValue(1);                                                                                                                              //Natural: ASSIGN #WS.#CORR-IDX := 1
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind().equals("C"))))
        {
            decideConditionsMet3061++;
            pnd_Ws_Pnd_Corr_Idx.setValue(2);                                                                                                                              //Natural: ASSIGN #WS.#CORR-IDX := 2
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_A_Rec_Pnd_Generate.getValue(pnd_Ws_Pnd_Corr_Idx).setValue(true);                                                                                              //Natural: ASSIGN #A-REC.#GENERATE ( #CORR-IDX ) := #C-REC.#GENERATE ( #CORR-IDX ) := TRUE
        pnd_C_Rec_Pnd_Generate.getValue(pnd_Ws_Pnd_Corr_Idx).setValue(true);
        pnd_C_Rec_Pnd_Num_Payees.getValue(pnd_Ws_Pnd_Corr_Idx).nadd(1);                                                                                                   //Natural: ADD 1 TO #C-REC.#NUM-PAYEES ( #CORR-IDX )
        pnd_C_Rec_Pnd_Amt_1.getValue(pnd_Ws_Pnd_Corr_Idx).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_1());                                                           //Natural: ADD IRS-B-PAYMENT-AMT-1 TO #C-REC.#AMT-1 ( #CORR-IDX )
        pnd_C_Rec_Pnd_Amt_2.getValue(pnd_Ws_Pnd_Corr_Idx).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_2());                                                           //Natural: ADD IRS-B-PAYMENT-AMT-2 TO #C-REC.#AMT-2 ( #CORR-IDX )
        pnd_C_Rec_Pnd_Amt_4.getValue(pnd_Ws_Pnd_Corr_Idx).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_4());                                                           //Natural: ADD IRS-B-PAYMENT-AMT-4 TO #C-REC.#AMT-4 ( #CORR-IDX )
        pnd_C_Rec_Pnd_Amt_5.getValue(pnd_Ws_Pnd_Corr_Idx).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_5());                                                           //Natural: ADD IRS-B-PAYMENT-AMT-5 TO #C-REC.#AMT-5 ( #CORR-IDX )
        pnd_C_Rec_Pnd_Amt_9.getValue(pnd_Ws_Pnd_Corr_Idx).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_9());                                                           //Natural: ADD IRS-B-PAYMENT-AMT-9 TO #C-REC.#AMT-9 ( #CORR-IDX )
        pnd_C_Rec_Pnd_Amt_A.getValue(pnd_Ws_Pnd_Corr_Idx).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_A());                                                           //Natural: ADD IRS-B-PAYMENT-AMT-A TO #C-REC.#AMT-A ( #CORR-IDX )
        pnd_C_Rec_Pnd_Amt_B.getValue(pnd_Ws_Pnd_Corr_Idx).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_B());                                                           //Natural: ADD IRS-B-PAYMENT-AMT-B TO #C-REC.#AMT-B ( #CORR-IDX )
        //* *IF      #TWRL3508.#FORM            = 01            /* 1099-R
        if (condition(ldaTwrl3507.getIrs_B_Rec_Irs_B_Federal_State_Code().notEquals(" ")))                                                                                //Natural: IF IRS-B-FEDERAL-STATE-CODE NE ' '
        {
            pnd_K_Rec_Pnd_Generate.getValue(pnd_Ws_Pnd_Corr_Idx).setValue(true);                                                                                          //Natural: ASSIGN #K-REC.#GENERATE ( #CORR-IDX ) := TRUE
            if (condition(ldaTwrl3507.getIrs_B_Rec_Irs_B_Federal_State_Code().equals("UT")))                                                                              //Natural: IF IRS-B-FEDERAL-STATE-CODE = 'UT'
            {
                pnd_Ws_Pnd_J.setValue(49);                                                                                                                                //Natural: ASSIGN #WS.#J := 49
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_J.compute(new ComputeParameters(false, pnd_Ws_Pnd_J), ldaTwrl3507.getIrs_B_Rec_Irs_B_Federal_State_Code().val());                              //Natural: ASSIGN #WS.#J := VAL ( IRS-B-FEDERAL-STATE-CODE )
            }                                                                                                                                                             //Natural: END-IF
            pnd_K_Rec_Pnd_Num_Payees.getValue(pnd_Ws_Pnd_Corr_Idx,pnd_Ws_Pnd_J).nadd(1);                                                                                  //Natural: ADD 1 TO #K-REC.#NUM-PAYEES ( #CORR-IDX,#J )
            pnd_K_Rec_Pnd_Amt_1.getValue(pnd_Ws_Pnd_Corr_Idx,pnd_Ws_Pnd_J).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_1());                                          //Natural: ADD IRS-B-PAYMENT-AMT-1 TO #K-REC.#AMT-1 ( #CORR-IDX,#J )
            pnd_K_Rec_Pnd_Amt_2.getValue(pnd_Ws_Pnd_Corr_Idx,pnd_Ws_Pnd_J).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_2());                                          //Natural: ADD IRS-B-PAYMENT-AMT-2 TO #K-REC.#AMT-2 ( #CORR-IDX,#J )
            pnd_K_Rec_Pnd_Amt_4.getValue(pnd_Ws_Pnd_Corr_Idx,pnd_Ws_Pnd_J).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_4());                                          //Natural: ADD IRS-B-PAYMENT-AMT-4 TO #K-REC.#AMT-4 ( #CORR-IDX,#J )
            pnd_K_Rec_Pnd_Amt_5.getValue(pnd_Ws_Pnd_Corr_Idx,pnd_Ws_Pnd_J).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_5());                                          //Natural: ADD IRS-B-PAYMENT-AMT-5 TO #K-REC.#AMT-5 ( #CORR-IDX,#J )
            pnd_K_Rec_Pnd_Amt_9.getValue(pnd_Ws_Pnd_Corr_Idx,pnd_Ws_Pnd_J).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_9());                                          //Natural: ADD IRS-B-PAYMENT-AMT-9 TO #K-REC.#AMT-9 ( #CORR-IDX,#J )
            pnd_K_Rec_Pnd_Amt_A.getValue(pnd_Ws_Pnd_Corr_Idx,pnd_Ws_Pnd_J).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_A());                                          //Natural: ADD IRS-B-PAYMENT-AMT-A TO #K-REC.#AMT-A ( #CORR-IDX,#J )
            pnd_K_Rec_Pnd_Amt_B.getValue(pnd_Ws_Pnd_Corr_Idx,pnd_Ws_Pnd_J).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_B());                                          //Natural: ADD IRS-B-PAYMENT-AMT-B TO #K-REC.#AMT-B ( #CORR-IDX,#J )
            pnd_K_Rec_Pnd_State_Tax.getValue(pnd_Ws_Pnd_Corr_Idx,pnd_Ws_Pnd_J).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_State_Tax_Num());                                      //Natural: ADD IRS-B-STATE-TAX-NUM TO #K-REC.#STATE-TAX ( #CORR-IDX,#J )
            pnd_K_Rec_Pnd_Local_Tax.getValue(pnd_Ws_Pnd_Corr_Idx,pnd_Ws_Pnd_J).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Local_Tax_Num());                                      //Natural: ADD IRS-B-LOCAL-TAX-NUM TO #K-REC.#LOCAL-TAX ( #CORR-IDX,#J )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_A_Records() throws Exception                                                                                                                  //Natural: CREATE-A-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  WRITE 'Begin Subroutine:  CREATE-A-RECORDS'
        ldaTwrl3508.getPnd_Twrl3508_Pnd_Seq_Num().reset();                                                                                                                //Natural: RESET #TWRL3508.#SEQ-NUM
        pnd_Ws_Pnd_A_Rec_Sw.setValue(true);                                                                                                                               //Natural: MOVE TRUE TO #A-REC-SW
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMPANY
        sub_Process_Company();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_A_Rec_Sw.resetInitial();                                                                                                                               //Natural: RESET INITIAL #A-REC-SW
        FOR03:                                                                                                                                                            //Natural: FOR #WS.#I = 1 TO 3
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(3)); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(pnd_A_Rec_Pnd_Generate.getValue(pnd_Ws_Pnd_I).getBoolean()))                                                                                    //Natural: IF #A-REC.#GENERATE ( #I )
            {
                ldaTwrl3321.getIrs_A_Out_Tape().resetInitial();                                                                                                           //Natural: RESET INITIAL IRS-A-OUT-TAPE
                ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind().setValue(pnd_Corr_Ind2.getValue(pnd_Ws_Pnd_I));                                                                //Natural: ASSIGN #TWRL3508.#CORR-IND := #CORR-IND2 ( #I )
                ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payment_Year().setValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Tax_Year());                                                  //Natural: ASSIGN IRS-A-PAYMENT-YEAR := #TWRL3508.#TAX-YEAR
                ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Ein().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id());                                                       //Natural: ASSIGN IRS-A-PAYER-EIN := #TWRACOM2.#FED-ID
                ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Name_Control().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name());                                     //Natural: ASSIGN IRS-A-PAYER-NAME-CONTROL := #TWRACOM2.#COMP-SHORT-NAME
                //*  1099-R
                if (condition(ldaTwrl3508.getPnd_Twrl3508_Pnd_Form().equals(1)))                                                                                          //Natural: IF #TWRL3508.#FORM = 01
                {
                    ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Type_Of_Return().setValue("9");                                                                                   //Natural: ASSIGN IRS-A-TYPE-OF-RETURN := '9'
                    ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Amount_Indicators().setValue("12459AB");                                                                          //Natural: ASSIGN IRS-A-AMOUNT-INDICATORS := '12459AB'
                    //*  1099-INT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Type_Of_Return().setValue("6");                                                                                   //Natural: ASSIGN IRS-A-TYPE-OF-RETURN := '6'
                    ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Amount_Indicators().setValue("12345");                                                                            //Natural: ASSIGN IRS-A-AMOUNT-INDICATORS := '12345'
                }                                                                                                                                                         //Natural: END-IF
                ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Name_1().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Payer_A());                                              //Natural: ASSIGN IRS-A-PAYER-NAME-1 := #TWRACOM2.#COMP-PAYER-A
                ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Name_2().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Payer_B());                                              //Natural: ASSIGN IRS-A-PAYER-NAME-2 := #TWRACOM2.#COMP-PAYER-B
                ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Shipping_Address().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Address().getValue(1));                             //Natural: ASSIGN IRS-A-PAYER-SHIPPING-ADDRESS := #TWRACOM2.#ADDRESS ( 1 )
                ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Phone_No().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Phone());                                                   //Natural: ASSIGN IRS-A-PAYER-PHONE-NO := #TWRACOM2.#PHONE
                ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_City().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_City());                                                        //Natural: ASSIGN IRS-A-PAYER-CITY := #TWRACOM2.#CITY
                ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_State().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_State());                                                      //Natural: ASSIGN IRS-A-PAYER-STATE := #TWRACOM2.#STATE
                ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Zip().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Zip_9());                                                        //Natural: ASSIGN IRS-A-PAYER-ZIP := #TWRACOM2.#ZIP-9
                //* DP
                //* DP
                //* DP
                if (condition(pnd_State_Table_Tircntl_State_Alpha_Code.equals("NE")))                                                                                     //Natural: IF #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE = 'NE'
                {
                    pnd_Irs_A_Filler_4_Ne_Pnd_Irs_A_Filler_Sp2.setValue("13008692");                                                                                      //Natural: ASSIGN #IRS-A-FILLER-SP2 := '13008692'
                    ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Filler_4().setValue(pnd_Irs_A_Filler_4_Ne);                                                                       //Natural: ASSIGN IRS-A-FILLER-4 := #IRS-A-FILLER-4-NE
                    //* DP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Name_Control().equals("TIAA") || ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Name_Control().equals("ADMN"))) //Natural: IF IRS-A-PAYER-NAME-CONTROL = 'TIAA' OR = 'ADMN'
                {
                    getWorkFiles().write(1, false, ldaTwrl3321.getIrs_A_Out_Tape(), ldaTwrl3508.getPnd_Twrl3508());                                                       //Natural: WRITE WORK FILE 1 IRS-A-OUT-TAPE #TWRL3508
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getWorkFiles().write(2, false, ldaTwrl3321.getIrs_A_Out_Tape(), ldaTwrl3508.getPnd_Twrl3508());                                                       //Natural: WRITE WORK FILE 2 IRS-A-OUT-TAPE #TWRL3508
                }                                                                                                                                                         //Natural: END-IF
                //*    ADD   1                       TO IRS-F-NUMBER-OF-A-RECORDS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_A_Rec.getValue("*").reset();                                                                                                                                  //Natural: RESET #A-REC ( * )
    }
    private void sub_Create_C_Records() throws Exception                                                                                                                  //Natural: CREATE-C-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  WRITE 'Begin Subroutine:  CREATE-C-RECORDS'
        ldaTwrl3508.getPnd_Twrl3508_Pnd_Seq_Num().reset();                                                                                                                //Natural: RESET #TWRL3508.#SEQ-NUM
        FOR04:                                                                                                                                                            //Natural: FOR #WS.#I = 1 TO 3
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(3)); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(pnd_C_Rec_Pnd_Generate.getValue(pnd_Ws_Pnd_I).getBoolean()))                                                                                    //Natural: IF #C-REC.#GENERATE ( #I )
            {
                //*  RCC
                ldaTwrl3323.getIrs_C_Out_Tape().resetInitial();                                                                                                           //Natural: RESET INITIAL IRS-C-OUT-TAPE
                ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind().setValue(pnd_Corr_Ind2.getValue(pnd_Ws_Pnd_I));                                                                //Natural: ASSIGN #TWRL3508.#CORR-IND := #CORR-IND2 ( #I )
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Number_Of_Payees().setValue(pnd_C_Rec_Pnd_Num_Payees.getValue(pnd_Ws_Pnd_I));                                         //Natural: ASSIGN IRS-C-NUMBER-OF-PAYEES := #C-REC.#NUM-PAYEES ( #I )
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_1().setValue(pnd_C_Rec_Pnd_Amt_1.getValue(pnd_Ws_Pnd_I));                                               //Natural: ASSIGN IRS-C-CONTROL-TOTAL-1 := #C-REC.#AMT-1 ( #I )
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_2().setValue(pnd_C_Rec_Pnd_Amt_2.getValue(pnd_Ws_Pnd_I));                                               //Natural: ASSIGN IRS-C-CONTROL-TOTAL-2 := #C-REC.#AMT-2 ( #I )
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_4().setValue(pnd_C_Rec_Pnd_Amt_4.getValue(pnd_Ws_Pnd_I));                                               //Natural: ASSIGN IRS-C-CONTROL-TOTAL-4 := #C-REC.#AMT-4 ( #I )
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_5().setValue(pnd_C_Rec_Pnd_Amt_5.getValue(pnd_Ws_Pnd_I));                                               //Natural: ASSIGN IRS-C-CONTROL-TOTAL-5 := #C-REC.#AMT-5 ( #I )
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_9().setValue(pnd_C_Rec_Pnd_Amt_9.getValue(pnd_Ws_Pnd_I));                                               //Natural: ASSIGN IRS-C-CONTROL-TOTAL-9 := #C-REC.#AMT-9 ( #I )
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_A().setValue(pnd_C_Rec_Pnd_Amt_A.getValue(pnd_Ws_Pnd_I));                                               //Natural: ASSIGN IRS-C-CONTROL-TOTAL-A := #C-REC.#AMT-A ( #I )
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_B().setValue(pnd_C_Rec_Pnd_Amt_B.getValue(pnd_Ws_Pnd_I));                                               //Natural: ASSIGN IRS-C-CONTROL-TOTAL-B := #C-REC.#AMT-B ( #I )
                //* *  IF #HOLD-COMPANY = 'T'            /* TIAA                 /* EINCHG
                //*  TIAA                 /* EINCHG
                if (condition(pnd_Ws_Pnd_Hold_Company.equals("T") || pnd_Ws_Pnd_Hold_Company.equals("A")))                                                                //Natural: IF #HOLD-COMPANY = 'T' OR = 'A'
                {
                    getWorkFiles().write(1, false, ldaTwrl3323.getIrs_C_Out_Tape(), ldaTwrl3508.getPnd_Twrl3508());                                                       //Natural: WRITE WORK FILE 1 IRS-C-OUT-TAPE #TWRL3508
                    pnd_T_Rec_Pnd_Num_Payees_Tiaa.nadd(pnd_C_Rec_Pnd_Num_Payees.getValue(pnd_Ws_Pnd_I));                                                                  //Natural: ADD #C-REC.#NUM-PAYEES ( #I ) TO #T-REC.#NUM-PAYEES-TIAA
                    //*  TRUST
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getWorkFiles().write(2, false, ldaTwrl3323.getIrs_C_Out_Tape(), ldaTwrl3508.getPnd_Twrl3508());                                                       //Natural: WRITE WORK FILE 2 IRS-C-OUT-TAPE #TWRL3508
                    pnd_T_Rec_Pnd_Num_Payees_Trst.nadd(pnd_C_Rec_Pnd_Num_Payees.getValue(pnd_Ws_Pnd_I));                                                                  //Natural: ADD #C-REC.#NUM-PAYEES ( #I ) TO #T-REC.#NUM-PAYEES-TRST
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_C_Rec.getValue("*").reset();                                                                                                                                  //Natural: RESET #C-REC ( * )
    }
    private void sub_Create_K_Records() throws Exception                                                                                                                  //Natural: CREATE-K-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  WRITE 'Begin Subroutine:  CREATE-K-RECORDS'
        ldaTwrl3508.getPnd_Twrl3508_Pnd_Seq_Num().reset();                                                                                                                //Natural: RESET #TWRL3508.#SEQ-NUM
        FOR05:                                                                                                                                                            //Natural: FOR #WS.#I = 1 TO 3
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(3)); pnd_Ws_Pnd_I.nadd(1))
        {
            ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind().setValue(pnd_Corr_Ind2.getValue(pnd_Ws_Pnd_I));                                                                    //Natural: ASSIGN #TWRL3508.#CORR-IND := #CORR-IND2 ( #I )
            FOR06:                                                                                                                                                        //Natural: FOR #WS.#J = 1 TO #STATE-MAX
            for (pnd_Ws_Pnd_J.setValue(1); condition(pnd_Ws_Pnd_J.lessOrEqual(pnd_Ws_Const_Pnd_State_Max)); pnd_Ws_Pnd_J.nadd(1))
            {
                if (condition(pnd_K_Rec_Pnd_Num_Payees.getValue(pnd_Ws_Pnd_I,pnd_Ws_Pnd_J).notEquals(getZero())))                                                         //Natural: IF #K-REC.#NUM-PAYEES ( #I,#J ) NE 0
                {
                    ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_K_State().setValue(pnd_Ws_Pnd_J);                                                                                 //Natural: ASSIGN #TWRL3508.#IRS-K-STATE := #WS.#J
                    ldaTwrl3325.getIrs_K_Out_Tape().resetInitial();                                                                                                       //Natural: RESET INITIAL IRS-K-OUT-TAPE
                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_State_Code().setValueEdited(pnd_Ws_Pnd_J,new ReportEditMask("99"));                                               //Natural: MOVE EDITED #WS.#J ( EM = 99 ) TO IRS-K-STATE-CODE
                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Number_Of_Payees().setValue(pnd_K_Rec_Pnd_Num_Payees.getValue(pnd_Ws_Pnd_I,pnd_Ws_Pnd_J));                        //Natural: ASSIGN IRS-K-NUMBER-OF-PAYEES := #K-REC.#NUM-PAYEES ( #I,#J )
                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_1().setValue(pnd_K_Rec_Pnd_Amt_1.getValue(pnd_Ws_Pnd_I,pnd_Ws_Pnd_J));                              //Natural: ASSIGN IRS-K-CONTROL-TOTAL-1 := #K-REC.#AMT-1 ( #I,#J )
                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_2().setValue(pnd_K_Rec_Pnd_Amt_2.getValue(pnd_Ws_Pnd_I,pnd_Ws_Pnd_J));                              //Natural: ASSIGN IRS-K-CONTROL-TOTAL-2 := #K-REC.#AMT-2 ( #I,#J )
                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_4().setValue(pnd_K_Rec_Pnd_Amt_4.getValue(pnd_Ws_Pnd_I,pnd_Ws_Pnd_J));                              //Natural: ASSIGN IRS-K-CONTROL-TOTAL-4 := #K-REC.#AMT-4 ( #I,#J )
                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_5().setValue(pnd_K_Rec_Pnd_Amt_5.getValue(pnd_Ws_Pnd_I,pnd_Ws_Pnd_J));                              //Natural: ASSIGN IRS-K-CONTROL-TOTAL-5 := #K-REC.#AMT-5 ( #I,#J )
                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_9().setValue(pnd_K_Rec_Pnd_Amt_9.getValue(pnd_Ws_Pnd_I,pnd_Ws_Pnd_J));                              //Natural: ASSIGN IRS-K-CONTROL-TOTAL-9 := #K-REC.#AMT-9 ( #I,#J )
                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_A().setValue(pnd_K_Rec_Pnd_Amt_A.getValue(pnd_Ws_Pnd_I,pnd_Ws_Pnd_J));                              //Natural: ASSIGN IRS-K-CONTROL-TOTAL-A := #K-REC.#AMT-A ( #I,#J )
                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_B().setValue(pnd_K_Rec_Pnd_Amt_B.getValue(pnd_Ws_Pnd_I,pnd_Ws_Pnd_J));                              //Natural: ASSIGN IRS-K-CONTROL-TOTAL-B := #K-REC.#AMT-B ( #I,#J )
                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_St_Tax_Withheld().setValue(pnd_K_Rec_Pnd_State_Tax.getValue(pnd_Ws_Pnd_I,pnd_Ws_Pnd_J));                          //Natural: ASSIGN IRS-K-ST-TAX-WITHHELD := #K-REC.#STATE-TAX ( #I,#J )
                    if (condition(pnd_K_Rec_Pnd_Local_Tax.getValue(pnd_Ws_Pnd_Corr_Idx,pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00"))))                                  //Natural: IF #K-REC.#LOCAL-TAX ( #CORR-IDX,#I ) NE 0.00
                    {
                        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Lc_Tax_Withheld().setValue(pnd_K_Rec_Pnd_Local_Tax.getValue(pnd_Ws_Pnd_I,pnd_Ws_Pnd_J));                      //Natural: ASSIGN IRS-K-LC-TAX-WITHHELD := #K-REC.#LOCAL-TAX ( #I,#J )
                    }                                                                                                                                                     //Natural: END-IF
                    //* *    IF #HOLD-COMPANY = 'T'           /* TIAA               /* EINCHG
                    //*  TIAA               /* EINCHG
                    if (condition(pnd_Ws_Pnd_Hold_Company.equals("T") || pnd_Ws_Pnd_Hold_Company.equals("A")))                                                            //Natural: IF #HOLD-COMPANY = 'T' OR = 'A'
                    {
                        getWorkFiles().write(1, false, ldaTwrl3325.getIrs_K_Out_Tape(), ldaTwrl3508.getPnd_Twrl3508());                                                   //Natural: WRITE WORK FILE 1 IRS-K-OUT-TAPE #TWRL3508
                        //*  TRUST
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getWorkFiles().write(2, false, ldaTwrl3325.getIrs_K_Out_Tape(), ldaTwrl3508.getPnd_Twrl3508());                                                   //Natural: WRITE WORK FILE 2 IRS-K-OUT-TAPE #TWRL3508
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_K_Rec.getValue("*").reset();                                                                                                                                  //Natural: RESET #K-REC ( * ) #TWRL3508.#IRS-K-STATE
        ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_K_State().reset();
    }
    private void sub_Create_T_Record() throws Exception                                                                                                                   //Natural: CREATE-T-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        //*  WRITE 'Begin Subroutine:  CREATE-T-RECORDS'
        //*  EINCHG
        //*  EINCHG
        if (condition(pnd_Ws_Pnd_Tax_Year.greaterOrEqual(2018)))                                                                                                          //Natural: IF #WS.#TAX-YEAR GE 2018
        {
            pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue("A");                                                                                                    //Natural: ASSIGN #TWRACOM2.#COMP-CODE := 'A'
            //*  EINCHG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue("T");                                                                                                    //Natural: ASSIGN #TWRACOM2.#COMP-CODE := 'T'
            //*  EINCHG
        }                                                                                                                                                                 //Natural: END-IF
        pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(1);                                                                                                          //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 01
        pnd_Ws_Pnd_Final_Break.setValue(true);                                                                                                                            //Natural: ASSIGN #FINAL-BREAK := TRUE
        //*  MARINA
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMPANY
        sub_Process_Company();
        if (condition(Global.isEscape())) {return;}
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Transmitters_Tin().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id());                                                        //Natural: ASSIGN IRS-T-TRANSMITTERS-TIN := #TWRACOM2.#FED-ID
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Transmitter_Control_Code().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Tcc());                                              //Natural: ASSIGN IRS-T-TRANSMITTER-CONTROL-CODE := #TWRACOM2.#COMP-TCC
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Transmitter_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Trans_A());                                                  //Natural: ASSIGN IRS-T-TRANSMITTER-NAME := #TWRACOM2.#COMP-TRANS-A
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Transmitter_Name_Continued().reset();                                                                                         //Natural: RESET IRS-T-TRANSMITTER-NAME-CONTINUED IRS-T-MAGNETIC-TAPE-FILE-IND
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Magnetic_Tape_File_Ind().reset();
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Company_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Payer_A());                                                      //Natural: ASSIGN IRS-T-COMPANY-NAME := #TWRACOM2.#COMP-PAYER-A
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Company_Name_Continued().reset();                                                                                             //Natural: RESET IRS-T-COMPANY-NAME-CONTINUED
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Company_Mailing_Address().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Address().getValue(1));                                    //Natural: ASSIGN IRS-T-COMPANY-MAILING-ADDRESS := #TWRACOM2.#ADDRESS ( 1 )
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Company_City().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_City());                                                              //Natural: ASSIGN IRS-T-COMPANY-CITY := #TWRACOM2.#CITY
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Company_State().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_State());                                                            //Natural: ASSIGN IRS-T-COMPANY-STATE := #TWRACOM2.#STATE
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Company_Zip_Code().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Zip_9());                                                         //Natural: ASSIGN IRS-T-COMPANY-ZIP-CODE := #TWRACOM2.#ZIP-9
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Contact_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Contact_Name());                                                      //Natural: ASSIGN IRS-T-CONTACT-NAME := #TWRACOM2.#CONTACT-NAME
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Contact_Email_Address().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Contact_Email_Addr());                                       //Natural: ASSIGN IRS-T-CONTACT-EMAIL-ADDRESS := #TWRACOM2.#CONTACT-EMAIL-ADDR
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Contact_Phone_N_Extension().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Phone());                                                //Natural: ASSIGN IRS-T-CONTACT-PHONE-N-EXTENSION := #TWRACOM2.#PHONE
        ldaTwrl3508.getPnd_Twrl3508_Pnd_Company_Code().setValue(pnd_Ws_Const_Low_Values);                                                                                 //Natural: ASSIGN #TWRL3508.#COMPANY-CODE := LOW-VALUES
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Payment_Year().setValueEdited(pnd_Ws_Pnd_Tax_Year,new ReportEditMask("9999"));                                                //Natural: MOVE EDITED #WS.#TAX-YEAR ( EM = 9999 ) TO IRS-T-PAYMENT-YEAR
        if (condition(pnd_Ws_Pnd_System_Year.greater(ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Payment_Year())))                                                                //Natural: IF #WS.#SYSTEM-YEAR > IRS-T-PAYMENT-YEAR
        {
            ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Prior_Yr_Data_Ind().setValue("P");                                                                                        //Natural: ASSIGN IRS-T-PRIOR-YR-DATA-IND := 'P'
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Vendor_Ind().setValue("I");                                                                                                   //Natural: ASSIGN IRS-T-VENDOR-IND := 'I'
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Total_Number_Of_Payees().setValue(pnd_T_Rec_Pnd_Num_Payees_Tiaa);                                                             //Natural: ASSIGN IRS-T-TOTAL-NUMBER-OF-PAYEES := #T-REC.#NUM-PAYEES-TIAA
        getWorkFiles().write(1, false, ldaTwrl3326.getIrs_T_Out_Tape(), ldaTwrl3508.getPnd_Twrl3508());                                                                   //Natural: WRITE WORK FILE 1 IRS-T-OUT-TAPE #TWRL3508
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Total_Number_Of_Payees().setValue(pnd_T_Rec_Pnd_Num_Payees_Trst);                                                             //Natural: ASSIGN IRS-T-TOTAL-NUMBER-OF-PAYEES := #T-REC.#NUM-PAYEES-TRST
        getWorkFiles().write(2, false, ldaTwrl3326.getIrs_T_Out_Tape(), ldaTwrl3508.getPnd_Twrl3508());                                                                   //Natural: WRITE WORK FILE 2 IRS-T-OUT-TAPE #TWRL3508
    }
    private void sub_Create_F_Record() throws Exception                                                                                                                   //Natural: CREATE-F-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        ldaTwrl3508.getPnd_Twrl3508_Pnd_Company_Code().setValue(pnd_Ws_Const_High_Values);                                                                                //Natural: ASSIGN #TWRL3508.#COMPANY-CODE := HIGH-VALUES
        ldaTwrl3324.getIrs_F_Out_Tape_Irs_F_Number_Of_A_Records().setValue(1);                                                                                            //Natural: ASSIGN IRS-F-NUMBER-OF-A-RECORDS := 1
        ldaTwrl3324.getIrs_F_Out_Tape_Irs_F_Number_Of_B_Records().setValue(pnd_T_Rec_Pnd_Num_Payees_Tiaa);                                                                //Natural: ASSIGN IRS-F-NUMBER-OF-B-RECORDS := #T-REC.#NUM-PAYEES-TIAA
        getWorkFiles().write(1, false, ldaTwrl3324.getIrs_F_Out_Tape(), ldaTwrl3508.getPnd_Twrl3508());                                                                   //Natural: WRITE WORK FILE 1 IRS-F-OUT-TAPE #TWRL3508
        ldaTwrl3324.getIrs_F_Out_Tape_Irs_F_Number_Of_B_Records().setValue(pnd_T_Rec_Pnd_Num_Payees_Trst);                                                                //Natural: ASSIGN IRS-F-NUMBER-OF-B-RECORDS := #T-REC.#NUM-PAYEES-TRST
        getWorkFiles().write(2, false, ldaTwrl3324.getIrs_F_Out_Tape(), ldaTwrl3508.getPnd_Twrl3508());                                                                   //Natural: WRITE WORK FILE 2 IRS-F-OUT-TAPE #TWRL3508
    }
    private void sub_Str_Detail_Accept() throws Exception                                                                                                                 //Natural: STR-DETAIL-ACCEPT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(pdaTwratin.getTwratin_Pnd_I_Tin_Type().notEquals(pdaTwraform.getPnd_Twraform_Tirf_Tax_Id_Type()) || pdaTwratin.getTwratin_Pnd_I_Tin().notEquals(pdaTwraform.getPnd_Twraform_Tirf_Tin()))) //Natural: IF TWRATIN.#I-TIN-TYPE NE #TWRAFORM.TIRF-TAX-ID-TYPE OR TWRATIN.#I-TIN NE #TWRAFORM.TIRF-TIN
        {
            pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(pdaTwraform.getPnd_Twraform_Tirf_Tax_Id_Type());                                                              //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := #TWRAFORM.TIRF-TAX-ID-TYPE
            pdaTwratin.getTwratin_Pnd_I_Tin().setValue(pdaTwraform.getPnd_Twraform_Tirf_Tin());                                                                           //Natural: ASSIGN TWRATIN.#I-TIN := #TWRAFORM.TIRF-TIN
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIN
            sub_Process_Tin();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(4, new ReportEmptyLineSuppression(true),"///TIN",                                                                                            //Natural: DISPLAY ( 4 ) ( ES = ON ) '///TIN' TWRATIN.#O-TIN / '/' TWRATIN.#O-TIN-TYPE-DESC / '/' #WS.#DISP-REC '///Contract' #TWRAFORM.TIRF-CONTRACT-NBR 'Pa/y/e/e' #TWRAFORM.TIRF-PAYEE-CDE '//Name' #TWRAFORM.TIRF-PARTICIPANT-NAME ( HC = L ) / 'Address' #TWRAFORM.TIRF-ADDR-LN1 ( HC = L ) / '/' #TWRAFORM.TIRF-ADDR-LN2 ( HC = L ) / '/' #TWRAFORM.TIRF-ADDR-LN3 ( HC = L ) / '/' #TWRAFORM.TIRF-ADDR-LN4 ( HC = L ) / '/' #TWRAFORM.TIRF-ADDR-LN5 ( HC = L ) / '/' #TWRAFORM.TIRF-ADDR-LN6 ( HC = L ) '///State Distrib' #TWRAFORM.TIRF-STATE-DISTR ( #ST-OCC ) ( HC = R EM = -ZZZ,ZZZ,ZZ9.99 ) 3X '///State Tax' #TWRAFORM.TIRF-STATE-TAX-WTHLD ( #ST-OCC ) ( HC = R EM = -Z,ZZZ,ZZ9.99 ) 5X '///Local Distrib' #TWRAFORM.TIRF-LOC-DISTR ( #ST-OCC ) ( HC = R EM = -ZZZ,ZZZ,ZZ9.99 ) 2X '///Local Tax' #TWRAFORM.TIRF-LOC-TAX-WTHLD ( #ST-OCC ) ( HC = R EM = -Z,ZZZ,ZZ9.99 )
        		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),NEWLINE,"/",
        		pnd_Ws_Pnd_Disp_Rec,"///Contract",
        		pdaTwraform.getPnd_Twraform_Tirf_Contract_Nbr(),"Pa/y/e/e",
        		pdaTwraform.getPnd_Twraform_Tirf_Payee_Cde(),"//Name",
        		pdaTwraform.getPnd_Twraform_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"///State Distrib",
        		pdaTwraform.getPnd_Twraform_Tirf_State_Distr().getValue(pnd_Ws_Pnd_St_Occ), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),"///State Tax",
        		pdaTwraform.getPnd_Twraform_Tirf_State_Tax_Wthld().getValue(pnd_Ws_Pnd_St_Occ), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), 
            new ReportEditMask ("-Z,ZZZ,ZZ9.99"),new ColumnSpacing(5),"///Local Distrib",
        		pdaTwraform.getPnd_Twraform_Tirf_Loc_Distr().getValue(pnd_Ws_Pnd_St_Occ), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),"///Local Tax",
        		pdaTwraform.getPnd_Twraform_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Pnd_St_Occ), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), 
            new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
    }
    private void sub_Str_Detail_Reject() throws Exception                                                                                                                 //Natural: STR-DETAIL-REJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(pdaTwratin.getTwratin_Pnd_I_Tin_Type().notEquals(ldaTwrl9715.getForm_R_Tirf_Tax_Id_Type()) || pdaTwratin.getTwratin_Pnd_I_Tin().notEquals(ldaTwrl9715.getForm_R_Tirf_Tin()))) //Natural: IF TWRATIN.#I-TIN-TYPE NE FORM-R.TIRF-TAX-ID-TYPE OR TWRATIN.#I-TIN NE FORM-R.TIRF-TIN
        {
            pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(ldaTwrl9715.getForm_R_Tirf_Tax_Id_Type());                                                                    //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := FORM-R.TIRF-TAX-ID-TYPE
            pdaTwratin.getTwratin_Pnd_I_Tin().setValue(ldaTwrl9715.getForm_R_Tirf_Tin());                                                                                 //Natural: ASSIGN TWRATIN.#I-TIN := FORM-R.TIRF-TIN
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIN
            sub_Process_Tin();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(3, new ReportEmptyLineSuppression(true),"///TIN",                                                                                            //Natural: DISPLAY ( 3 ) ( ES = ON ) '///TIN' TWRATIN.#O-TIN / '/' TWRATIN.#O-TIN-TYPE-DESC '///Contract' FORM-R.TIRF-CONTRACT-NBR 'Pa/y/e/e' FORM-R.TIRF-PAYEE-CDE '//Name' FORM-R.TIRF-PARTICIPANT-NAME ( HC = L ) / 'Address' FORM-R.TIRF-ADDR-LN1 ( HC = L ) / '/' FORM-R.TIRF-ADDR-LN2 ( HC = L ) / '/' FORM-R.TIRF-ADDR-LN3 ( HC = L ) / '/' FORM-R.TIRF-ADDR-LN4 ( HC = L ) / '/' FORM-R.TIRF-ADDR-LN5 ( HC = L ) / '/' FORM-R.TIRF-ADDR-LN6 ( HC = L ) '#/of/Sta/tes' FORM-R.C*TIRF-1099-R-STATE-GRP ( EM = ZZ9 HC = R ) 'S/T/A/T' #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '/I/R/S' FORM-R.TIRF-STATE-IRS-RPT-IND ( #ST-OCC-A ) '///State Distrib' FORM-R.TIRF-STATE-DISTR ( #ST-OCC-A ) ( HC = R ) '///State Tax' FORM-R.TIRF-STATE-TAX-WTHLD ( #ST-OCC-A ) ( HC = R ) '///Local Distrib' FORM-R.TIRF-LOC-DISTR ( #ST-OCC-A ) ( HC = R ) '///Local Tax' FORM-R.TIRF-LOC-TAX-WTHLD ( #ST-OCC-A ) ( HC = R )
        		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),"///Contract",
        		ldaTwrl9715.getForm_R_Tirf_Contract_Nbr(),"Pa/y/e/e",
        		ldaTwrl9715.getForm_R_Tirf_Payee_Cde(),"//Name",
        		ldaTwrl9715.getForm_R_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"#/of/Sta/tes",
        		ldaTwrl9715.getForm_R_Count_Casttirf_1099_R_State_Grp(), new ReportEditMask ("ZZ9"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
            "S/T/A/T",
        		pnd_State_Table_Tircntl_State_Alpha_Code,"/I/R/S",
        		ldaTwrl9715.getForm_R_Tirf_State_Irs_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A),"///State Distrib",
        		ldaTwrl9715.getForm_R_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
            "///State Tax",
        		ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
            "///Local Distrib",
        		ldaTwrl9715.getForm_R_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
            "///Local Tax",
        		ldaTwrl9715.getForm_R_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 3 ) 1 LINES
    }
    private void sub_Stw_Detail_Accept() throws Exception                                                                                                                 //Natural: STW-DETAIL-ACCEPT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(pnd_Case_Fields_Pnd_Recon_Summary.getBoolean()))                                                                                                    //Natural: IF #CASE-FIELDS.#RECON-SUMMARY
        {
            pnd_Report_Indexes_Pnd_Stw1.reset();                                                                                                                          //Natural: RESET #REPORT-INDEXES.#STW1
                                                                                                                                                                          //Natural: PERFORM ACCUM-STW
            sub_Accum_Stw();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Recon_Detail.getBoolean()))                                                                                                     //Natural: IF #CASE-FIELDS.#RECON-DETAIL
        {
            Global.format("IS=OFF");                                                                                                                                      //Natural: SUSPEND IDENTICAL SUPPRESS ( 06 )
            FOR07:                                                                                                                                                        //Natural: FOR #WS.#K = 1 TO 3
            for (pnd_Ws_Pnd_K.setValue(1); condition(pnd_Ws_Pnd_K.lessOrEqual(3)); pnd_Ws_Pnd_K.nadd(1))
            {
                short decideConditionsMet3294 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #WS.#K;//Natural: VALUE 1
                if (condition((pnd_Ws_Pnd_K.equals(1))))
                {
                    decideConditionsMet3294++;
                    pdaTwraform.getPnd_Twraform_Pnd_Twraform_Det().setValuesByName(ldaTwrl9715.getVw_form_R());                                                           //Natural: MOVE BY NAME FORM-R TO #TWRAFORM-DET
                    pdaTwraform.getPnd_Twraform_C_1099_R().setValue(ldaTwrl9715.getForm_R_Count_Casttirf_1099_R_State_Grp());                                             //Natural: ASSIGN #TWRAFORM.C-1099-R := FORM-R.C*TIRF-1099-R-STATE-GRP
                    pnd_Ws_Pnd_St_Occ.setValue(pnd_Case_Fields_Pnd_St_Occ_A);                                                                                             //Natural: ASSIGN #WS.#ST-OCC := #CASE-FIELDS.#ST-OCC-A
                    pnd_Ws_Pnd_Disp_Rec.setValue("N E W");                                                                                                                //Natural: ASSIGN #WS.#DISP-REC := 'N E W'
                                                                                                                                                                          //Natural: PERFORM REPORT-4B
                    sub_Report_4b();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 2
                else if (condition((pnd_Ws_Pnd_K.equals(2))))
                {
                    decideConditionsMet3294++;
                    if (condition(! (pnd_Case_Fields_Pnd_Prior_Record.getBoolean())))                                                                                     //Natural: IF NOT #CASE-FIELDS.#PRIOR-RECORD
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pdaTwraform.getPnd_Twraform_Pnd_Twraform_Det().setValuesByName(ldaTwrl9710.getVw_form());                                                             //Natural: MOVE BY NAME FORM TO #TWRAFORM-DET
                    pdaTwraform.getPnd_Twraform_C_1099_R().setValue(ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp());                                               //Natural: ASSIGN #TWRAFORM.C-1099-R := FORM.C*TIRF-1099-R-STATE-GRP
                    pnd_Ws_Pnd_St_Occ.setValue(pnd_Case_Fields_Pnd_St_Occ_I);                                                                                             //Natural: ASSIGN #WS.#ST-OCC := #CASE-FIELDS.#ST-OCC-I
                    pnd_Ws_Pnd_Disp_Rec.setValue("O L D");                                                                                                                //Natural: ASSIGN #WS.#DISP-REC := 'O L D'
                                                                                                                                                                          //Natural: PERFORM REPORT-4B
                    sub_Report_4b();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                getReports().display(6, "/TIN",                                                                                                                           //Natural: DISPLAY ( 06 ) '/TIN' #RECON-CASE.#TIN ( IS = ON ) 'Contract/Payee' #RECON-CASE.#CNTRCT-PY ( IS = ON ) '/' #RECON-CASE.#HEADER ( #K ) 'Form Count' #RECON-CASE.#FORM-CNT ( #K ) ( HC = R ) 'State Distrib' #RECON-CASE.#STATE-DISTR ( #K ) ( HC = R ) 'State Tax' #RECON-CASE.#STATE-TAX ( #K ) ( HC = R ) 'Local Distrib' #RECON-CASE.#LOCAL-DISTR ( #K ) ( HC = R ) 'Local Tax' #RECON-CASE.#LOCAL-TAX ( #K ) ( HC = R )
                		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
                		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
                		pnd_Recon_Case_Pnd_Header.getValue(pnd_Ws_Pnd_K),"Form Count",
                		pnd_Recon_Case_Pnd_Form_Cnt.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Distrib",
                    
                		pnd_Recon_Case_Pnd_State_Distr.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Tax",
                    
                		pnd_Recon_Case_Pnd_State_Tax.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Distrib",
                    
                		pnd_Recon_Case_Pnd_Local_Distr.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Tax",
                    
                		pnd_Recon_Case_Pnd_Local_Tax.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            getReports().skip(6, 1);                                                                                                                                      //Natural: SKIP ( 06 ) 1 LINES
            getReports().skip(10, 1);                                                                                                                                     //Natural: SKIP ( 10 ) 1 LINES
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Report_4b() throws Exception                                                                                                                         //Natural: REPORT-4B
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        if (condition(pdaTwratin.getTwratin_Pnd_I_Tin_Type().notEquals(pdaTwraform.getPnd_Twraform_Tirf_Tax_Id_Type()) || pdaTwratin.getTwratin_Pnd_I_Tin().notEquals(pdaTwraform.getPnd_Twraform_Tirf_Tin()))) //Natural: IF TWRATIN.#I-TIN-TYPE NE #TWRAFORM.TIRF-TAX-ID-TYPE OR TWRATIN.#I-TIN NE #TWRAFORM.TIRF-TIN
        {
            pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(pdaTwraform.getPnd_Twraform_Tirf_Tax_Id_Type());                                                              //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := #TWRAFORM.TIRF-TAX-ID-TYPE
            pdaTwratin.getTwratin_Pnd_I_Tin().setValue(pdaTwraform.getPnd_Twraform_Tirf_Tin());                                                                           //Natural: ASSIGN TWRATIN.#I-TIN := #TWRAFORM.TIRF-TIN
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIN
            sub_Process_Tin();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(10, new ReportEmptyLineSuppression(true),"///TIN",                                                                                           //Natural: DISPLAY ( 10 ) ( ES = ON ) '///TIN' TWRATIN.#O-TIN / '/' TWRATIN.#O-TIN-TYPE-DESC / '/' #WS.#DISP-REC '///Contract' #TWRAFORM.TIRF-CONTRACT-NBR 'Pa/y/e/e' #TWRAFORM.TIRF-PAYEE-CDE '//Name' #TWRAFORM.TIRF-PARTICIPANT-NAME ( HC = L ) / 'Address' #TWRAFORM.TIRF-ADDR-LN1 ( HC = L ) / '/' #TWRAFORM.TIRF-ADDR-LN2 ( HC = L ) / '/' #TWRAFORM.TIRF-ADDR-LN3 ( HC = L ) / '/' #TWRAFORM.TIRF-ADDR-LN4 ( HC = L ) / '/' #TWRAFORM.TIRF-ADDR-LN5 ( HC = L ) / '/' #TWRAFORM.TIRF-ADDR-LN6 ( HC = L ) '///State Distrib' #TWRAFORM.TIRF-STATE-DISTR ( #ST-OCC ) ( HC = R EM = -ZZZ,ZZZ,ZZ9.99 ) 3X '///State Tax' #TWRAFORM.TIRF-STATE-TAX-WTHLD ( #ST-OCC ) ( HC = R EM = -Z,ZZZ,ZZ9.99 ) 5X '///Local Distrib' #TWRAFORM.TIRF-LOC-DISTR ( #ST-OCC ) ( HC = R EM = -ZZZ,ZZZ,ZZ9.99 ) 2X '///Local Tax' #TWRAFORM.TIRF-LOC-TAX-WTHLD ( #ST-OCC ) ( HC = R EM = -Z,ZZZ,ZZ9.99 )
        		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),NEWLINE,"/",
        		pnd_Ws_Pnd_Disp_Rec,"///Contract",
        		pdaTwraform.getPnd_Twraform_Tirf_Contract_Nbr(),"Pa/y/e/e",
        		pdaTwraform.getPnd_Twraform_Tirf_Payee_Cde(),"//Name",
        		pdaTwraform.getPnd_Twraform_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"///State Distrib",
        		pdaTwraform.getPnd_Twraform_Tirf_State_Distr().getValue(pnd_Ws_Pnd_St_Occ), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),"///State Tax",
        		pdaTwraform.getPnd_Twraform_Tirf_State_Tax_Wthld().getValue(pnd_Ws_Pnd_St_Occ), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), 
            new ReportEditMask ("-Z,ZZZ,ZZ9.99"),new ColumnSpacing(5),"///Local Distrib",
        		pdaTwraform.getPnd_Twraform_Tirf_Loc_Distr().getValue(pnd_Ws_Pnd_St_Occ), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),"///Local Tax",
        		pdaTwraform.getPnd_Twraform_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Pnd_St_Occ), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), 
            new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
    }
    private void sub_Stw_Detail_Reject() throws Exception                                                                                                                 //Natural: STW-DETAIL-REJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(pnd_Case_Fields_Pnd_Recon_Summary.getBoolean()))                                                                                                    //Natural: IF #CASE-FIELDS.#RECON-SUMMARY
        {
            pnd_Report_Indexes_Pnd_Stw1.setValue(3);                                                                                                                      //Natural: ASSIGN #REPORT-INDEXES.#STW1 := 3
                                                                                                                                                                          //Natural: PERFORM ACCUM-STW
            sub_Accum_Stw();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Recon_Detail.getBoolean()))                                                                                                     //Natural: IF #CASE-FIELDS.#RECON-DETAIL
        {
            Global.format("IS=OFF");                                                                                                                                      //Natural: SUSPEND IDENTICAL SUPPRESS ( 07 )
            FOR08:                                                                                                                                                        //Natural: FOR #WS.#K = 1 TO 3
            for (pnd_Ws_Pnd_K.setValue(1); condition(pnd_Ws_Pnd_K.lessOrEqual(3)); pnd_Ws_Pnd_K.nadd(1))
            {
                if (condition(pnd_Ws_Pnd_K.equals(2) && ! (pnd_Case_Fields_Pnd_Prior_Record.getBoolean())))                                                               //Natural: IF #WS.#K = 2 AND NOT #CASE-FIELDS.#PRIOR-RECORD
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(7, "/TIN",                                                                                                                           //Natural: DISPLAY ( 07 ) '/TIN' #RECON-CASE.#TIN ( IS = ON ) 'Contract/Payee' #RECON-CASE.#CNTRCT-PY ( IS = ON ) '/' #RECON-CASE.#HEADER ( #K ) 'Form Count' #RECON-CASE.#FORM-CNT ( #K ) ( HC = R ) 'State Distrib' #RECON-CASE.#STATE-DISTR ( #K ) ( HC = R ) 'State Tax' #RECON-CASE.#STATE-TAX ( #K ) ( HC = R ) 'Local Distrib' #RECON-CASE.#LOCAL-DISTR ( #K ) ( HC = R ) 'Local Tax' #RECON-CASE.#LOCAL-TAX ( #K ) ( HC = R )
                		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
                		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
                		pnd_Recon_Case_Pnd_Header.getValue(pnd_Ws_Pnd_K),"Form Count",
                		pnd_Recon_Case_Pnd_Form_Cnt.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Distrib",
                    
                		pnd_Recon_Case_Pnd_State_Distr.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Tax",
                    
                		pnd_Recon_Case_Pnd_State_Tax.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Distrib",
                    
                		pnd_Recon_Case_Pnd_Local_Distr.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Tax",
                    
                		pnd_Recon_Case_Pnd_Local_Tax.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_K.equals(3)))                                                                                                                    //Natural: IF #WS.#K = 3
                {
                    getReports().skip(7, 1);                                                                                                                              //Natural: SKIP ( 07 ) 1 LINES
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Stg_Detail() throws Exception                                                                                                                        //Natural: STG-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        if (condition(pnd_Case_Fields_Pnd_Recon_Summary.getBoolean()))                                                                                                    //Natural: IF #CASE-FIELDS.#RECON-SUMMARY
        {
                                                                                                                                                                          //Natural: PERFORM ACCUM-STG1
            sub_Accum_Stg1();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Recon_Detail.getBoolean()))                                                                                                     //Natural: IF #CASE-FIELDS.#RECON-DETAIL
        {
            Global.format("IS=OFF");                                                                                                                                      //Natural: SUSPEND IDENTICAL SUPPRESS ( 09 )
            FOR09:                                                                                                                                                        //Natural: FOR #WS.#K = 1 TO 3
            for (pnd_Ws_Pnd_K.setValue(1); condition(pnd_Ws_Pnd_K.lessOrEqual(3)); pnd_Ws_Pnd_K.nadd(1))
            {
                if (condition(pnd_Ws_Pnd_K.equals(2) && ! (pnd_Case_Fields_Pnd_Prior_Record.getBoolean())))                                                               //Natural: IF #WS.#K = 2 AND NOT #CASE-FIELDS.#PRIOR-RECORD
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(9, "/TIN",                                                                                                                           //Natural: DISPLAY ( 09 ) '/TIN' #RECON-CASE.#TIN ( IS = ON ) 'Contract/Payee' #RECON-CASE.#CNTRCT-PY ( IS = ON ) '/' #RECON-CASE.#HEADER ( #K ) 'Form Count' #RECON-CASE.#FORM-CNT ( #K ) ( HC = R ) 'State Distrib' #RECON-CASE.#STATE-DISTR ( #K ) ( HC = R ) 'State Tax' #RECON-CASE.#STATE-TAX ( #K ) ( HC = R ) 'Local Distrib' #RECON-CASE.#LOCAL-DISTR ( #K ) ( HC = R ) 'Local Tax' #RECON-CASE.#LOCAL-TAX ( #K ) ( HC = R )
                		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
                		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
                		pnd_Recon_Case_Pnd_Header.getValue(pnd_Ws_Pnd_K),"Form Count",
                		pnd_Recon_Case_Pnd_Form_Cnt.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Distrib",
                    
                		pnd_Recon_Case_Pnd_State_Distr.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Tax",
                    
                		pnd_Recon_Case_Pnd_State_Tax.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Distrib",
                    
                		pnd_Recon_Case_Pnd_Local_Distr.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Tax",
                    
                		pnd_Recon_Case_Pnd_Local_Tax.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_K.equals(3)))                                                                                                                    //Natural: IF #WS.#K = 3
                {
                    getReports().skip(9, 1);                                                                                                                              //Natural: SKIP ( 09 ) 1 LINES
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Sta_Report_4c() throws Exception                                                                                                                     //Natural: STA-REPORT-4C
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        if (condition(pdaTwratin.getTwratin_Pnd_I_Tin_Type().notEquals(ldaTwrl9715.getForm_R_Tirf_Tax_Id_Type()) || pdaTwratin.getTwratin_Pnd_I_Tin().notEquals(ldaTwrl9715.getForm_R_Tirf_Tin()))) //Natural: IF TWRATIN.#I-TIN-TYPE NE FORM-R.TIRF-TAX-ID-TYPE OR TWRATIN.#I-TIN NE FORM-R.TIRF-TIN
        {
            pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(ldaTwrl9715.getForm_R_Tirf_Tax_Id_Type());                                                                    //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := FORM-R.TIRF-TAX-ID-TYPE
            pdaTwratin.getTwratin_Pnd_I_Tin().setValue(ldaTwrl9715.getForm_R_Tirf_Tin());                                                                                 //Natural: ASSIGN TWRATIN.#I-TIN := FORM-R.TIRF-TIN
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIN
            sub_Process_Tin();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(11, new ReportEmptyLineSuppression(true),"///TIN",                                                                                           //Natural: DISPLAY ( 11 ) ( ES = ON ) '///TIN' TWRATIN.#O-TIN / '/' TWRATIN.#O-TIN-TYPE-DESC '///Contract' FORM-R.TIRF-CONTRACT-NBR 'Pa/y/e/e' FORM-R.TIRF-PAYEE-CDE '//Name' FORM-R.TIRF-PARTICIPANT-NAME ( HC = L ) / 'Address' FORM-R.TIRF-ADDR-LN1 ( HC = L ) / '/' FORM-R.TIRF-ADDR-LN2 ( HC = L ) / '/' FORM-R.TIRF-ADDR-LN3 ( HC = L ) / '/' FORM-R.TIRF-ADDR-LN4 ( HC = L ) / '/' FORM-R.TIRF-ADDR-LN5 ( HC = L ) / '/' FORM-R.TIRF-ADDR-LN6 ( HC = L ) '///State Distrib' FORM-R.TIRF-STATE-DISTR ( #ST-OCC-A ) ( HC = R EM = -ZZZ,ZZZ,ZZ9.99 ) 3X '///State Tax' FORM-R.TIRF-STATE-TAX-WTHLD ( #ST-OCC-A ) ( HC = R EM = -Z,ZZZ,ZZ9.99 ) 5X '///Local Distrib' FORM-R.TIRF-LOC-DISTR ( #ST-OCC-A ) ( HC = R EM = -ZZZ,ZZZ,ZZ9.99 ) 2X '///Local Tax' FORM-R.TIRF-LOC-TAX-WTHLD ( #ST-OCC-A ) ( HC = R EM = -Z,ZZZ,ZZ9.99 )
        		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),"///Contract",
        		ldaTwrl9715.getForm_R_Tirf_Contract_Nbr(),"Pa/y/e/e",
        		ldaTwrl9715.getForm_R_Tirf_Payee_Cde(),"//Name",
        		ldaTwrl9715.getForm_R_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"///State Distrib",
        		ldaTwrl9715.getForm_R_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),"///State Tax",
        		ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), 
            new ReportEditMask ("-Z,ZZZ,ZZ9.99"),new ColumnSpacing(5),"///Local Distrib",
        		ldaTwrl9715.getForm_R_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),"///Local Tax",
        		ldaTwrl9715.getForm_R_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), 
            new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(11, 1);                                                                                                                                         //Natural: SKIP ( 11 ) 1 LINES
    }
    private void sub_Summary_Reports() throws Exception                                                                                                                   //Natural: SUMMARY-REPORTS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        if (condition(pnd_Case_Fields2_Pnd_Recon_And_Reporting.getBoolean()))                                                                                             //Natural: IF #RECON-AND-REPORTING
        {
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORTS-C
            sub_Summary_Reports_C();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORTS-W
        sub_Summary_Reports_W();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORTS-G
        sub_Summary_Reports_G();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORTS-A
        sub_Summary_Reports_A();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Summary_Reports_C() throws Exception                                                                                                                 //Natural: SUMMARY-REPORTS-C
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(pnd_Ws_Pnd_S2.equals(1)))                                                                                                                           //Natural: IF #S2 = 1
        {
            getReports().skip(4, 4);                                                                                                                                      //Natural: SKIP ( 4 ) 4 LINES
            if (condition(getReports().getAstLinesLeft(4).less(4)))                                                                                                       //Natural: NEWPAGE ( 4 ) IF LESS 4 LINES
            {
                getReports().newPage(4);
                if (condition(Global.isEscape())){return;}
            }
            getReports().write(4, "=",new RepeatItem(126),NEWLINE,"Totals:",new ReportTAsterisk(pdaTwraform.getPnd_Twraform_Tirf_Participant_Name()),"Form Count:",pnd_Str_Pnd_Form_Cnt.getValue(7,pnd_Ws_Pnd_S2),  //Natural: WRITE ( 4 ) '=' ( 126 ) / 'Totals:' T*#TWRAFORM.TIRF-PARTICIPANT-NAME 'Form Count:' #STR.#FORM-CNT ( 7,#S2 ) 11X #STR.#STATE-DISTR ( 7,#S2 ) #STR.#STATE-TAX ( 7,#S2 ) #STR.#LOCAL-DISTR ( 7,#S2 ) #STR.#LOCAL-TAX ( 7,#S2 )
                new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(11),pnd_Str_Pnd_State_Distr.getValue(7,pnd_Ws_Pnd_S2), new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Str_Pnd_State_Tax.getValue(7,pnd_Ws_Pnd_S2), 
                new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),pnd_Str_Pnd_Local_Distr.getValue(7,pnd_Ws_Pnd_S2), new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Str_Pnd_Local_Tax.getValue(7,pnd_Ws_Pnd_S2), 
                new ReportEditMask ("-ZZZZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(3));                                                                                                                 //Natural: NEWPAGE ( 3 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(4));                                                                                                                 //Natural: NEWPAGE ( 4 )
        if (condition(Global.isEscape())){return;}
        FOR10:                                                                                                                                                            //Natural: FOR #STR1 = 1 TO #STR-MAX-LINES
        for (pnd_Report_Indexes_Pnd_Str1.setValue(1); condition(pnd_Report_Indexes_Pnd_Str1.lessOrEqual(pnd_Ws_Const_Pnd_Str_Max_Lines)); pnd_Report_Indexes_Pnd_Str1.nadd(1))
        {
            if (condition(pnd_Report_Indexes_Pnd_Str1.equals(7)))                                                                                                         //Natural: IF #STR1 = 7
            {
                getReports().write(2, "=",new RepeatItem(100));                                                                                                           //Natural: WRITE ( 2 ) '=' ( 100 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(2, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",                         //Natural: DISPLAY ( 2 ) ( ES = ON HC = R ) '/' #STR.#HEADER1 ( #STR1 ) / '/' #STR.#HEADER2 ( #STR1 ) 'Form/Count' #STR.#FORM-CNT ( #STR1,#S2 ) 'State/Distribution' #STR.#STATE-DISTR ( #STR1,#S2 ) 'State/Withholding' #STR.#STATE-TAX ( #STR1,#S2 ) 'Local/Distribution' #STR.#LOCAL-DISTR ( #STR1,#S2 ) 'Local/Withholding' #STR.#LOCAL-TAX ( #STR1,#S2 )
            		pnd_Str_Pnd_Header1.getValue(pnd_Report_Indexes_Pnd_Str1),NEWLINE,"/",
            		pnd_Str_Pnd_Header2.getValue(pnd_Report_Indexes_Pnd_Str1),"Form/Count",
            		pnd_Str_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Str1,pnd_Ws_Pnd_S2),"State/Distribution",
            		pnd_Str_Pnd_State_Distr.getValue(pnd_Report_Indexes_Pnd_Str1,pnd_Ws_Pnd_S2),"State/Withholding",
            		pnd_Str_Pnd_State_Tax.getValue(pnd_Report_Indexes_Pnd_Str1,pnd_Ws_Pnd_S2),"Local/Distribution",
            		pnd_Str_Pnd_Local_Distr.getValue(pnd_Report_Indexes_Pnd_Str1,pnd_Ws_Pnd_S2),"Local/Withholding",
            		pnd_Str_Pnd_Local_Tax.getValue(pnd_Report_Indexes_Pnd_Str1,pnd_Ws_Pnd_S2));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(2, "EMPTY FORMS           ",pnd_Ws_Pnd_Empty_Frm2);                                                                                            //Natural: WRITE ( 2 ) 'EMPTY FORMS           ' #EMPTY-FRM2
        if (Global.isEscape()) return;
    }
    private void sub_Summary_Reports_W() throws Exception                                                                                                                 //Natural: SUMMARY-REPORTS-W
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(pnd_Ws_Pnd_S2.equals(1)))                                                                                                                           //Natural: IF #S2 = 1
        {
            getReports().skip(10, 4);                                                                                                                                     //Natural: SKIP ( 10 ) 4 LINES
            if (condition(getReports().getAstLinesLeft(10).less(4)))                                                                                                      //Natural: NEWPAGE ( 10 ) IF LESS 4 LINES
            {
                getReports().newPage(10);
                if (condition(Global.isEscape())){return;}
            }
            getReports().write(10, "=",new RepeatItem(126),NEWLINE,"Totals:",new ReportTAsterisk(pdaTwraform.getPnd_Twraform_Tirf_Participant_Name()),"Form Count:",pnd_Stw_Pnd_Form_Cnt.getValue(1,pnd_Ws_Pnd_S2),  //Natural: WRITE ( 10 ) '=' ( 126 ) / 'Totals:' T*#TWRAFORM.TIRF-PARTICIPANT-NAME 'Form Count:' #STW.#FORM-CNT ( 1,#S2 ) 11X #STW.#STATE-DISTR ( 1,#S2 ) #STW.#STATE-TAX ( 1,#S2 ) #STW.#LOCAL-DISTR ( 1,#S2 ) #STW.#LOCAL-TAX ( 1,#S2 )
                new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(11),pnd_Stw_Pnd_State_Distr.getValue(1,pnd_Ws_Pnd_S2), new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Stw_Pnd_State_Tax.getValue(1,pnd_Ws_Pnd_S2), 
                new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),pnd_Stw_Pnd_Local_Distr.getValue(1,pnd_Ws_Pnd_S2), new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Stw_Pnd_Local_Tax.getValue(1,pnd_Ws_Pnd_S2), 
                new ReportEditMask ("-ZZZZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().newPage(new ReportSpecification(5));                                                                                                                 //Natural: NEWPAGE ( 05 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(6));                                                                                                                 //Natural: NEWPAGE ( 06 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(7));                                                                                                                 //Natural: NEWPAGE ( 07 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(10));                                                                                                                //Natural: NEWPAGE ( 10 )
        if (condition(Global.isEscape())){return;}
        FOR11:                                                                                                                                                            //Natural: FOR #STW1 = 1 TO #STW-MAX-LINES
        for (pnd_Report_Indexes_Pnd_Stw1.setValue(1); condition(pnd_Report_Indexes_Pnd_Stw1.lessOrEqual(pnd_Ws_Const_Pnd_Stw_Max_Lines)); pnd_Report_Indexes_Pnd_Stw1.nadd(1))
        {
            if (condition(pnd_Report_Indexes_Pnd_Stw1.equals(3) || pnd_Report_Indexes_Pnd_Stw1.equals(6)))                                                                //Natural: IF #STW1 = 3 OR = 6
            {
                getReports().write(5, "=",new RepeatItem(95));                                                                                                            //Natural: WRITE ( 5 ) '=' ( 95 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(5, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",                         //Natural: DISPLAY ( 5 ) ( ES = ON HC = R ) '/' #STW.#HEADER ( #STW1 ) 'Form/Count' #STW.#FORM-CNT ( #STW1,#S2 ) 'State/Distribution' #STW.#STATE-DISTR ( #STW1,#S2 ) 'State/Withholding' #STW.#STATE-TAX ( #STW1,#S2 ) 'Local/Distribution' #STW.#LOCAL-DISTR ( #STW1,#S2 ) 'Local/Withholding' #STW.#LOCAL-TAX ( #STW1,#S2 )
            		pnd_Stw_Pnd_Header.getValue(pnd_Report_Indexes_Pnd_Stw1),"Form/Count",
            		pnd_Stw_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Stw1,pnd_Ws_Pnd_S2),"State/Distribution",
            		pnd_Stw_Pnd_State_Distr.getValue(pnd_Report_Indexes_Pnd_Stw1,pnd_Ws_Pnd_S2),"State/Withholding",
            		pnd_Stw_Pnd_State_Tax.getValue(pnd_Report_Indexes_Pnd_Stw1,pnd_Ws_Pnd_S2),"Local/Distribution",
            		pnd_Stw_Pnd_Local_Distr.getValue(pnd_Report_Indexes_Pnd_Stw1,pnd_Ws_Pnd_S2),"Local/Withholding",
            		pnd_Stw_Pnd_Local_Tax.getValue(pnd_Report_Indexes_Pnd_Stw1,pnd_Ws_Pnd_S2));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Report_Indexes_Pnd_Stw1.equals(3)))                                                                                                         //Natural: IF #STW1 = 3
            {
                getReports().skip(5, 4);                                                                                                                                  //Natural: SKIP ( 5 ) 4 LINES
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Summary_Reports_G() throws Exception                                                                                                                 //Natural: SUMMARY-REPORTS-G
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(8));                                                                                                                 //Natural: NEWPAGE ( 8 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(9));                                                                                                                 //Natural: NEWPAGE ( 9 )
        if (condition(Global.isEscape())){return;}
        FOR12:                                                                                                                                                            //Natural: FOR #STG1 = 1 TO #STG-MAX-LINES
        for (pnd_Report_Indexes_Pnd_Stg1.setValue(1); condition(pnd_Report_Indexes_Pnd_Stg1.lessOrEqual(pnd_Ws_Const_Pnd_Stg_Max_Lines)); pnd_Report_Indexes_Pnd_Stg1.nadd(1))
        {
            if (condition(pnd_Report_Indexes_Pnd_Stg1.equals(3)))                                                                                                         //Natural: IF #STG1 = 3
            {
                getReports().write(8, "=",new RepeatItem(100));                                                                                                           //Natural: WRITE ( 8 ) '=' ( 100 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(8, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",                         //Natural: DISPLAY ( 8 ) ( ES = ON HC = R ) '/' #STG.#HEADER ( #STG1 ) 'Form/Count' #STG.#FORM-CNT ( #STG1,#S2 ) 'State/Distribution' #STG.#STATE-DISTR ( #STG1,#S2 ) 'State/Withholding' #STG.#STATE-TAX ( #STG1,#S2 ) 'Local/Distribution' #STG.#LOCAL-DISTR ( #STG1,#S2 ) 'Local/Withholding' #STG.#LOCAL-TAX ( #STG1,#S2 )
            		pnd_Stg_Pnd_Header.getValue(pnd_Report_Indexes_Pnd_Stg1),"Form/Count",
            		pnd_Stg_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Stg1,pnd_Ws_Pnd_S2),"State/Distribution",
            		pnd_Stg_Pnd_State_Distr.getValue(pnd_Report_Indexes_Pnd_Stg1,pnd_Ws_Pnd_S2),"State/Withholding",
            		pnd_Stg_Pnd_State_Tax.getValue(pnd_Report_Indexes_Pnd_Stg1,pnd_Ws_Pnd_S2),"Local/Distribution",
            		pnd_Stg_Pnd_Local_Distr.getValue(pnd_Report_Indexes_Pnd_Stg1,pnd_Ws_Pnd_S2),"Local/Withholding",
            		pnd_Stg_Pnd_Local_Tax.getValue(pnd_Report_Indexes_Pnd_Stg1,pnd_Ws_Pnd_S2));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Report_Indexes_Pnd_Stg1.equals(3)))                                                                                                         //Natural: IF #STG1 = 3
            {
                getReports().skip(8, 4);                                                                                                                                  //Natural: SKIP ( 8 ) 4 LINES
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(8, "EMPTY FORMS           ",pnd_Ws_Pnd_Empty_Cnt);                                                                                             //Natural: WRITE ( 8 ) 'EMPTY FORMS           ' #EMPTY-CNT
        if (Global.isEscape()) return;
    }
    private void sub_Summary_Reports_A() throws Exception                                                                                                                 //Natural: SUMMARY-REPORTS-A
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(pnd_Ws_Pnd_S2.equals(1)))                                                                                                                           //Natural: IF #S2 = 1
        {
            getReports().skip(11, 4);                                                                                                                                     //Natural: SKIP ( 11 ) 4 LINES
            if (condition(getReports().getAstLinesLeft(11).less(4)))                                                                                                      //Natural: NEWPAGE ( 11 ) IF LESS 4 LINES
            {
                getReports().newPage(11);
                if (condition(Global.isEscape())){return;}
            }
            getReports().write(11, "=",new RepeatItem(127),NEWLINE,"Totals:",new ReportTAsterisk(ldaTwrl9715.getForm_R_Tirf_Participant_Name()),"Form Count:",pnd_Sta_Pnd_Form_Cnt,  //Natural: WRITE ( 11 ) '=' ( 127 ) / 'Totals:' T*FORM-R.TIRF-PARTICIPANT-NAME 'Form Count:' #STA.#FORM-CNT 11X #STA.#STATE-DISTR #STA.#STATE-TAX #STA.#LOCAL-DISTR #STA.#LOCAL-TAX
                new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(11),pnd_Sta_Pnd_State_Distr, new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Sta_Pnd_State_Tax, 
                new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),pnd_Sta_Pnd_Local_Distr, new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Sta_Pnd_Local_Tax, new ReportEditMask 
                ("-ZZZZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().newPage(new ReportSpecification(11));                                                                                                                //Natural: NEWPAGE ( 11 )
        if (condition(Global.isEscape())){return;}
        pnd_Sta.reset();                                                                                                                                                  //Natural: RESET #STA
    }
    private void sub_Process_Tin() throws Exception                                                                                                                       //Natural: PROCESS-TIN
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        DbsUtil.callnat(Twrntin.class , getCurrentProcessState(), pdaTwratin.getTwratin_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratin.getTwratin_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTIN' USING TWRATIN.#INPUT-PARMS ( AD = O ) TWRATIN.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Process_Company() throws Exception                                                                                                                   //Natural: PROCESS-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  WRITE 'Begin Subroutine:  PROCESS-COMPANY'
        short decideConditionsMet3454 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #WS.#COMPANY-BREAK AND NOT #A-REC-SW
        if (condition(pnd_Ws_Pnd_Company_Break.getBoolean() && ! (pnd_Ws_Pnd_A_Rec_Sw.getBoolean())))
        {
            decideConditionsMet3454++;
            pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue(pnd_Sort_Rec_Tirf_Company_Cde);                                                                          //Natural: ASSIGN #TWRACOM2.#COMP-CODE := #SORT-REC.TIRF-COMPANY-CDE
        }                                                                                                                                                                 //Natural: WHEN #FINAL-BREAK
        else if (condition(pnd_Ws_Pnd_Final_Break.getBoolean()))
        {
            decideConditionsMet3454++;
            ignore();
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue(pnd_Ws_Pnd_Hold_Company);                                                                                //Natural: ASSIGN #TWRACOM2.#COMP-CODE := #HOLD-COMPANY
        }                                                                                                                                                                 //Natural: END-DECIDE
        pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(1);                                                                                                          //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 01
        pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                                         //Natural: ASSIGN #TWRACOM2.#TAX-YEAR := #WS.#TAX-YEAR
        DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOM2' USING #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Ws_Pnd_Company_Break.getBoolean() && ! (pnd_Ws_Pnd_A_Rec_Sw.getBoolean())))                                                                     //Natural: IF #WS.#COMPANY-BREAK AND NOT #A-REC-SW
        {
            pnd_Ws_Pnd_Company_Line.setValue("Company:");                                                                                                                 //Natural: ASSIGN #WS.#COMPANY-LINE := 'Company:'
            setValueToSubstring(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),pnd_Ws_Pnd_Company_Line,11,4);                                                          //Natural: MOVE #TWRACOM2.#COMP-SHORT-NAME TO SUBSTR ( #WS.#COMPANY-LINE,11,4 )
            setValueToSubstring("-",pnd_Ws_Pnd_Company_Line,16,1);                                                                                                        //Natural: MOVE '-' TO SUBSTR ( #WS.#COMPANY-LINE,16,1 )
            setValueToSubstring(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Name(),pnd_Ws_Pnd_Company_Line,18,55);                                                               //Natural: MOVE #TWRACOM2.#COMP-NAME TO SUBSTR ( #WS.#COMPANY-LINE,18,55 )
            pnd_Ws_Pnd_Company_Break.reset();                                                                                                                             //Natural: RESET #WS.#COMPANY-BREAK
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Input_Parms() throws Exception                                                                                                               //Natural: PROCESS-INPUT-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("TWRP3554|sub_Process_Input_Parms");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_Input_Parms);                                                                               //Natural: INPUT #WS.#INPUT-PARMS
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"State Correction Reporting parameters:",NEWLINE,new TabSetting(26),"Tax Year...........:",  //Natural: WRITE ( 1 ) 7T 'State Correction Reporting parameters:' / 26T 'Tax Year...........:' #WS.#TAX-YEAR
                    pnd_Ws_Pnd_Tax_Year);
                if (Global.isEscape()) return;
                if (condition(pnd_Ws_Pnd_State.greaterOrEqual("00") && pnd_Ws_Pnd_State.lessOrEqual(pnd_Ws_Const_Pnd_State_Max_A) && DbsUtil.maskMatches(pnd_Ws_Pnd_State, //Natural: IF #WS.#STATE = '00' THRU #WS-CONST.#STATE-MAX-A AND #WS.#STATE = MASK ( NN )
                    "NN")))
                {
                    pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                 //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
                    pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                                 //Natural: ASSIGN TWRATBL2.#TAX-YEAR := #WS.#TAX-YEAR
                    pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().setValue(false);                                                                                              //Natural: ASSIGN TWRATBL2.#ABEND-IND := TWRATBL2.#DISPLAY-IND := FALSE
                    pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().setValue(false);
                    pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue("0");                                                                                                //Natural: ASSIGN TWRATBL2.#STATE-CDE := '0'
                    setValueToSubstring(pnd_Ws_Pnd_State,pdaTwratbl2.getTwratbl2_Pnd_State_Cde(),2,2);                                                                    //Natural: MOVE #WS.#STATE TO SUBSTR ( TWRATBL2.#STATE-CDE,2,2 )
                    DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
                        new AttributeParameter("M"));
                    if (condition(Global.isEscape())) return;
                    if (condition(pdaTwratbl2.getTwratbl2_Pnd_Return_Cde().getBoolean()))                                                                                 //Natural: IF #RETURN-CDE
                    {
                        pnd_State_Table.setValuesByName(pdaTwratbl2.getTwratbl2());                                                                                       //Natural: MOVE BY NAME TWRATBL2 TO #STATE-TABLE
                        pnd_Ws_Pnd_Valid_State.setValue(true);                                                                                                            //Natural: ASSIGN #WS.#VALID-STATE := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(! (pnd_Ws_Pnd_Valid_State.getBoolean())))                                                                                                   //Natural: IF NOT #WS.#VALID-STATE
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Unknown STATE parmameter:",pnd_Ws_Pnd_State,new TabSetting(77),                  //Natural: WRITE ( 1 ) '***' 25T 'Unknown STATE parmameter:' #WS.#STATE 77T '***'
                        "***");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate(101);  if (true) return;                                                                                                            //Natural: TERMINATE 101
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_Datx.setValue(Global.getDATX());                                                                                                               //Natural: ASSIGN #WS.#DATX := *DATX
                pnd_Ws_Pnd_Timx.setValue(Global.getTIMX());                                                                                                               //Natural: ASSIGN #WS.#TIMX := *TIMX
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                                 //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #WS.#TAX-YEAR
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(false);                                                                                              //Natural: ASSIGN #TWRATBL4.#ABEND-IND := #TWRATBL4.#DISPLAY-IND := FALSE
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Display_Ind().setValue(false);
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Rec_Type().setValue("9ST0");                                                                                              //Natural: ASSIGN #TWRATBL4.#REC-TYPE := '9ST0'
                setValueToSubstring(pnd_Ws_Pnd_State,pdaTwratbl4.getPnd_Twratbl4_Pnd_Rec_Type(),5,2);                                                                     //Natural: MOVE #WS.#STATE TO SUBSTR ( #TWRATBL4.#REC-TYPE,5,2 )
                                                                                                                                                                          //Natural: PERFORM READ-CONTROL
                sub_Read_Control();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                short decideConditionsMet3503 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN TWRATBL2.TIRCNTL-CORR-MEDIA-CODE = 'M'
                if (condition(pdaTwratbl2.getTwratbl2_Tircntl_Corr_Media_Code().equals("M")))
                {
                    decideConditionsMet3503++;
                    ignore();
                }                                                                                                                                                         //Natural: WHEN #TWRATBL4.#TIRCNTL-IRS-ORIG AND TWRATBL2.TIRCNTL-CORR-MEDIA-CODE = 'P' OR = 'R' AND TWRATBL2.TIRCNTL-MEDIA-CODE = 'M' AND TWRATBL2.TIRCNTL-CORR-COMB-FED-IND = 'C'
                else if (condition((((pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Orig().getBoolean() && (pdaTwratbl2.getTwratbl2_Tircntl_Corr_Media_Code().equals("P") 
                    || pdaTwratbl2.getTwratbl2_Tircntl_Corr_Media_Code().equals("R"))) && pdaTwratbl2.getTwratbl2_Tircntl_Media_Code().equals("M")) && pdaTwratbl2.getTwratbl2_Tircntl_Corr_Comb_Fed_Ind().equals("C"))))
                {
                    decideConditionsMet3503++;
                    pnd_Case_Fields2_Pnd_Special_Processing.setValue(true);                                                                                               //Natural: ASSIGN #SPECIAL-PROCESSING := TRUE
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet3503 > 0))
                {
                    pnd_Case_Fields2_Pnd_Recon_And_Reporting.setValue(true);                                                                                              //Natural: ASSIGN #RECON-AND-REPORTING := TRUE
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    if (condition(pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Orig().getBoolean()))                                                                       //Natural: IF #TWRATBL4.#TIRCNTL-IRS-ORIG
                    {
                        pnd_Hold_Fields_Pnd_Hold_Comb_Fed_Ind.setValue(pdaTwratbl2.getTwratbl2_Tircntl_Corr_Comb_Fed_Ind());                                              //Natural: MOVE TIRCNTL-CORR-COMB-FED-IND TO #HOLD-COMB-FED-IND
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Hold_Fields_Pnd_Hold_Comb_Fed_Ind.setValue(pdaTwratbl2.getTwratbl2_Tircntl_Comb_Fed_Ind());                                                   //Natural: MOVE TWRATBL2.TIRCNTL-COMB-FED-IND TO #HOLD-COMB-FED-IND
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Hold_Fields_Pnd_Hold_Comb_Fed_Ind.notEquals("C") && pdaTwratbl2.getTwratbl2_Tircntl_Corr_Media_Code().notEquals("X")))              //Natural: IF #HOLD-COMB-FED-IND NE 'C' AND TIRCNTL-CORR-MEDIA-CODE NE 'X'
                    {
                        pnd_Case_Fields2_Pnd_Recon_And_Reporting.setValue(true);                                                                                          //Natural: ASSIGN #RECON-AND-REPORTING := TRUE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Case_Fields2_Pnd_Recon_Only.setValue(true);                                                                                                   //Natural: ASSIGN #RECON-ONLY := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-DECIDE
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(26),"State..............: ",pnd_State_Table_Tircntl_State_Alpha_Code,"-",pnd_State_Table_Tircntl_State_Full_Name,NEWLINE,new  //Natural: WRITE ( 1 ) / 26T 'State..............: ' #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME / 26T 'TIAA State Code....: ' #STATE-TABLE.TIRCNTL-STATE-OLD-CODE / 26T 'State Ind..........: ' TWRATBL2.TIRCNTL-STATE-IND / 26T 'CMB Ind............: ' #HOLD-COMB-FED-IND / 26T 'Media Ind..........: ' TWRATBL2.TIRCNTL-CORR-MEDIA-CODE / / 16T 'System DATE used for this run:' #WS.#DATX ( EM = MM/DD/YYYY ) / 16T 'System TIME used for this run:' #WS.#TIMX ( EM = MM/DD/YYYY�HH:II:SS.T )
                    TabSetting(26),"TIAA State Code....: ",pnd_State_Table_Tircntl_State_Old_Code,NEWLINE,new TabSetting(26),"State Ind..........: ",pdaTwratbl2.getTwratbl2_Tircntl_State_Ind(),NEWLINE,new 
                    TabSetting(26),"CMB Ind............: ",pnd_Hold_Fields_Pnd_Hold_Comb_Fed_Ind,NEWLINE,new TabSetting(26),"Media Ind..........: ",pdaTwratbl2.getTwratbl2_Tircntl_Corr_Media_Code(),NEWLINE,NEWLINE,new 
                    TabSetting(16),"System DATE used for this run:",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(16),"System TIME used for this run:",pnd_Ws_Pnd_Timx, 
                    new ReportEditMask ("MM/DD/YYYY HH:II:SS.T"));
                if (Global.isEscape()) return;
                if (condition(pdaTwratbl2.getTwratbl2_Tircntl_Corr_Media_Code().notEquals("M")))                                                                          //Natural: IF TWRATBL2.TIRCNTL-CORR-MEDIA-CODE NE 'M'
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Mag Media Processing Attempted for     ",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 1 ) '***' 25T 'Mag Media Processing Attempted for     ' 77T '***' / '***' 25T 'Non-Mag Media State (See State Code Table)' 77T '***'
                        TabSetting(25),"Non-Mag Media State (See State Code Table)",new TabSetting(77),"***");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate(105);  if (true) return;                                                                                                            //Natural: TERMINATE 105
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaTwratbl4.getPnd_Twratbl4_Pnd_Ret_Code().getBoolean() && pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Orig().getBoolean()))                //Natural: IF #TWRATBL4.#RET-CODE AND #TWRATBL4.#TIRCNTL-IRS-ORIG
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"State Original Reporting had not been run",new TabSetting(77),                   //Natural: WRITE ( 1 ) '***' 25T 'State Original Reporting had not been run' 77T '***'
                        "***");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate(102);  if (true) return;                                                                                                            //Natural: TERMINATE 102
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaTwratbl4.getPnd_Twratbl4_C_Tircntl_Rpt_Tbl_Pe().equals(12)))                                                                             //Natural: IF C-TIRCNTL-RPT-TBL-PE = 12
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"11 cycles of State Correction Reporting",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 1 ) '***' 25T '11 cycles of State Correction Reporting' 77T '***' / '***' 25T 'had been run' 77T '***'
                        TabSetting(25),"had been run",new TabSetting(77),"***");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate(103);  if (true) return;                                                                                                            //Natural: TERMINATE 103
                }                                                                                                                                                         //Natural: END-IF
                if (condition((pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(pdaTwratbl4.getPnd_Twratbl4_C_Tircntl_Rpt_Tbl_Pe()).add(25)).greater(pnd_Ws_Pnd_Datx))) //Natural: IF ( TIRCNTL-RPT-DTE ( C-TIRCNTL-RPT-TBL-PE ) + 25 ) > #WS.#DATX
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Last cycle of State Correction Reporting",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 1 ) '***' 25T 'Last cycle of State Correction Reporting' 77T '***' / '***' 25T 'had been run within previous 25 days on' TIRCNTL-RPT-DTE ( C-TIRCNTL-RPT-TBL-PE ) ( EM = MM/DD/YYYY ) 77T '***'
                        TabSetting(25),"had been run within previous 25 days on",pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(pdaTwratbl4.getPnd_Twratbl4_C_Tircntl_Rpt_Tbl_Pe()), 
                        new ReportEditMask ("MM/DD/YYYY"),new TabSetting(77),"***");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate(104);  if (true) return;                                                                                                            //Natural: TERMINATE 104
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_Prev_Rpt_Date.setValue(pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(pdaTwratbl4.getPnd_Twratbl4_C_Tircntl_Rpt_Tbl_Pe()));            //Natural: ASSIGN #WS.#PREV-RPT-DATE := TIRCNTL-RPT-DTE ( C-TIRCNTL-RPT-TBL-PE )
                pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(pdaTwratbl4.getPnd_Twratbl4_C_Tircntl_Rpt_Tbl_Pe().getDec().add(1)).setValue(pnd_Ws_Pnd_Datx);     //Natural: ASSIGN TIRCNTL-RPT-DTE ( C-TIRCNTL-RPT-TBL-PE + 1 ) := #WS.#DATX
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL
                sub_Update_Control();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Load_State_Table() throws Exception                                                                                                                  //Natural: LOAD-STATE-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        FOR13:                                                                                                                                                            //Natural: FOR #I = 0 TO #STATE-MAX
        for (pnd_Ws_Pnd_I.setValue(0); condition(pnd_Ws_Pnd_I.lessOrEqual(pnd_Ws_Const_Pnd_State_Max)); pnd_Ws_Pnd_I.nadd(1))
        {
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValueEdited(pnd_Ws_Pnd_I,new ReportEditMask("999"));                                                               //Natural: MOVE EDITED #I ( EM = 999 ) TO TWRATBL2.#STATE-CDE
            DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
            if (condition(pdaTwratbl2.getTwratbl2_Pnd_Return_Cde().getBoolean()))                                                                                         //Natural: IF #RETURN-CDE
            {
                pnd_State_Table_X.getValue(pnd_Ws_Pnd_I.getInt() + 2).setValuesByName(pdaTwratbl2.getTwratbl2());                                                         //Natural: MOVE BY NAME TWRATBL2 TO #STATE-TABLE-X ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Load_Province_Table() throws Exception                                                                                                               //Natural: LOAD-PROVINCE-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        FOR14:                                                                                                                                                            //Natural: FOR #I = #PROV-MIN TO #PROV-MAX
        for (pnd_Ws_Pnd_I.setValue(pnd_Prov_Min); condition(pnd_Ws_Pnd_I.lessOrEqual(pnd_Prov_Max)); pnd_Ws_Pnd_I.nadd(1))
        {
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValueEdited(pnd_Ws_Pnd_I,new ReportEditMask("999"));                                                               //Natural: MOVE EDITED #I ( EM = 999 ) TO TWRATBL2.#STATE-CDE
            DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
            if (condition(pdaTwratbl2.getTwratbl2_Pnd_Return_Cde().getBoolean()))                                                                                         //Natural: IF #RETURN-CDE
            {
                pnd_Prov_Table.getValue(pnd_Ws_Pnd_I).setValuesByName(pdaTwratbl2.getTwratbl2());                                                                         //Natural: MOVE BY NAME TWRATBL2 TO #PROV-TABLE ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Read_Control() throws Exception                                                                                                                      //Natural: READ-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4R' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Update_Control() throws Exception                                                                                                                    //Natural: UPDATE-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        DbsUtil.callnat(Twrntb4u.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4U' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Process_Distrib() throws Exception                                                                                                                   //Natural: PROCESS-DISTRIB
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        DbsUtil.callnat(Twrndist.class , getCurrentProcessState(), pdaTwradist.getPnd_Twradist_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwradist.getPnd_Twradist_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNDIST' USING #TWRADIST.#INPUT-PARMS ( AD = O ) #TWRADIST.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Sort_Rec_Tirf_Company_CdeIsBreak = pnd_Sort_Rec_Tirf_Company_Cde.isBreak(endOfData);
        boolean pnd_Sort_Rec_Tirf_Form_TypeIsBreak = pnd_Sort_Rec_Tirf_Form_Type.isBreak(endOfData);
        if (condition(pnd_Sort_Rec_Tirf_Company_CdeIsBreak || pnd_Sort_Rec_Tirf_Form_TypeIsBreak))
        {
            pnd_Ws_Pnd_Company_Break.setValue(true);                                                                                                                      //Natural: ASSIGN #WS.#COMPANY-BREAK := TRUE
            pnd_Ws_Pnd_S2.setValue(1);                                                                                                                                    //Natural: ASSIGN #WS.#S2 := 1
            pnd_Ws_Pnd_Hold_Company.setValue(sort01Tirf_Company_CdeOld);                                                                                                  //Natural: ASSIGN #HOLD-COMPANY := OLD ( #SORT-REC.TIRF-COMPANY-CDE )
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORTS
            sub_Summary_Reports();
            if (condition(Global.isEscape())) {return;}
            pnd_Str_Pnd_Form_Cnt.getValue("*",2).nadd(pnd_Str_Pnd_Form_Cnt.getValue("*",1));                                                                              //Natural: ADD #STR.#FORM-CNT ( *,1 ) TO #STR.#FORM-CNT ( *,2 )
            pnd_Str_Pnd_State_Distr.getValue("*",2).nadd(pnd_Str_Pnd_State_Distr.getValue("*",1));                                                                        //Natural: ADD #STR.#STATE-DISTR ( *,1 ) TO #STR.#STATE-DISTR ( *,2 )
            pnd_Str_Pnd_State_Tax.getValue("*",2).nadd(pnd_Str_Pnd_State_Tax.getValue("*",1));                                                                            //Natural: ADD #STR.#STATE-TAX ( *,1 ) TO #STR.#STATE-TAX ( *,2 )
            pnd_Str_Pnd_Local_Distr.getValue("*",2).nadd(pnd_Str_Pnd_Local_Distr.getValue("*",1));                                                                        //Natural: ADD #STR.#LOCAL-DISTR ( *,1 ) TO #STR.#LOCAL-DISTR ( *,2 )
            pnd_Str_Pnd_Local_Tax.getValue("*",2).nadd(pnd_Str_Pnd_Local_Tax.getValue("*",1));                                                                            //Natural: ADD #STR.#LOCAL-TAX ( *,1 ) TO #STR.#LOCAL-TAX ( *,2 )
            pnd_Str_Pnd_Totals.getValue("*",1).reset();                                                                                                                   //Natural: RESET #STR.#TOTALS ( *,1 )
            pnd_Stw_Pnd_Form_Cnt.getValue("*",2).nadd(pnd_Stw_Pnd_Form_Cnt.getValue("*",1));                                                                              //Natural: ADD #STW.#FORM-CNT ( *,1 ) TO #STW.#FORM-CNT ( *,2 )
            pnd_Stw_Pnd_State_Distr.getValue("*",2).nadd(pnd_Stw_Pnd_State_Distr.getValue("*",1));                                                                        //Natural: ADD #STW.#STATE-DISTR ( *,1 ) TO #STW.#STATE-DISTR ( *,2 )
            pnd_Stw_Pnd_State_Tax.getValue("*",2).nadd(pnd_Stw_Pnd_State_Tax.getValue("*",1));                                                                            //Natural: ADD #STW.#STATE-TAX ( *,1 ) TO #STW.#STATE-TAX ( *,2 )
            pnd_Stw_Pnd_Local_Distr.getValue("*",2).nadd(pnd_Stw_Pnd_Local_Distr.getValue("*",1));                                                                        //Natural: ADD #STW.#LOCAL-DISTR ( *,1 ) TO #STW.#LOCAL-DISTR ( *,2 )
            pnd_Stw_Pnd_Local_Tax.getValue("*",2).nadd(pnd_Stw_Pnd_Local_Tax.getValue("*",1));                                                                            //Natural: ADD #STW.#LOCAL-TAX ( *,1 ) TO #STW.#LOCAL-TAX ( *,2 )
            pnd_Stw_Pnd_Totals.getValue("*",1).reset();                                                                                                                   //Natural: RESET #STW.#TOTALS ( *,1 )
            pnd_Stg_Pnd_Form_Cnt.getValue("*",2).nadd(pnd_Stg_Pnd_Form_Cnt.getValue("*",1));                                                                              //Natural: ADD #STG.#FORM-CNT ( *,1 ) TO #STG.#FORM-CNT ( *,2 )
            pnd_Stg_Pnd_State_Distr.getValue("*",2).nadd(pnd_Stg_Pnd_State_Distr.getValue("*",1));                                                                        //Natural: ADD #STG.#STATE-DISTR ( *,1 ) TO #STG.#STATE-DISTR ( *,2 )
            pnd_Stg_Pnd_State_Tax.getValue("*",2).nadd(pnd_Stg_Pnd_State_Tax.getValue("*",1));                                                                            //Natural: ADD #STG.#STATE-TAX ( *,1 ) TO #STG.#STATE-TAX ( *,2 )
            pnd_Stg_Pnd_Local_Distr.getValue("*",2).nadd(pnd_Stg_Pnd_Local_Distr.getValue("*",1));                                                                        //Natural: ADD #STG.#LOCAL-DISTR ( *,1 ) TO #STG.#LOCAL-DISTR ( *,2 )
            pnd_Stg_Pnd_Local_Tax.getValue("*",2).nadd(pnd_Stg_Pnd_Local_Tax.getValue("*",1));                                                                            //Natural: ADD #STG.#LOCAL-TAX ( *,1 ) TO #STG.#LOCAL-TAX ( *,2 )
            pnd_Stg_Pnd_Totals.getValue("*",1).reset();                                                                                                                   //Natural: RESET #STG.#TOTALS ( *,1 )
            pnd_Ws_Pnd_Empty_Cnt.reset();                                                                                                                                 //Natural: RESET #EMPTY-CNT
            pnd_Ws_Pnd_Empty_Frm2.reset();                                                                                                                                //Natural: RESET #EMPTY-FRM2
                                                                                                                                                                          //Natural: PERFORM CREATE-A-RECORDS
            sub_Create_A_Records();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-C-RECORDS
            sub_Create_C_Records();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-K-RECORDS
            sub_Create_K_Records();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Sort_Rec_Tirf_Form_TypeIsBreak))
        {
            pnd_Ws_Pnd_S2.setValue(2);                                                                                                                                    //Natural: ASSIGN #WS.#S2 := 2
            pnd_Ws_Pnd_Company_Line.setValue("Grand Totals");                                                                                                             //Natural: ASSIGN #WS.#COMPANY-LINE := 'Grand Totals'
            pnd_Ws_Pnd_Empty_Frm2.setValue(pnd_Ws_Pnd_Empty_Tot_Frm2);                                                                                                    //Natural: MOVE #EMPTY-TOT-FRM2 TO #EMPTY-FRM2
            pnd_Ws_Pnd_Empty_Cnt.setValue(pnd_Ws_Pnd_Empty_Tot_Cnt);                                                                                                      //Natural: MOVE #EMPTY-TOT-CNT TO #EMPTY-CNT
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORTS
            sub_Summary_Reports();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Pnd_Accepted_Form_Cnt.nadd(pnd_Str_Pnd_Form_Cnt.getValue(7,2));                                                                                        //Natural: ADD #STR.#FORM-CNT ( 7,2 ) TO #WS.#ACCEPTED-FORM-CNT
            pnd_Str_Pnd_Totals.getValue("*",2).reset();                                                                                                                   //Natural: RESET #STR.#TOTALS ( *,2 )
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=80 ZP=ON");
        Global.format(2, "PS=58 LS=133 ZP=ON");
        Global.format(3, "PS=58 LS=133 ZP=ON");
        Global.format(4, "PS=58 LS=133 ZP=ON");
        Global.format(5, "PS=58 LS=133 ZP=ON");
        Global.format(6, "PS=58 LS=133 ZP=ON");
        Global.format(7, "PS=58 LS=133 ZP=ON");
        Global.format(8, "PS=58 LS=133 ZP=ON");
        Global.format(9, "PS=58 LS=133 ZP=ON");
        Global.format(10, "PS=58 LS=133 ZP=ON");
        Global.format(11, "PS=58 LS=133 ZP=ON");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(37),"TaxWaRS",new TabSetting(68),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(34),"Control Report",new TabSetting(68),"Report: RPT1",NEWLINE,NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(50),"State Correction Reporting Summary",new TabSetting(120),"Report: RPT2",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(60),pnd_State_Table_Tircntl_State_Alpha_Code,"-",pnd_State_Table_Tircntl_State_Full_Name,
            NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"State Correction Reporting Rejected Detail",new TabSetting(120),"Report: RPT3",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(60),pnd_State_Table_Tircntl_State_Alpha_Code,"-",pnd_State_Table_Tircntl_State_Full_Name,
            NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(4), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(47),"State Correction Return Accepted Detail",new TabSetting(120),"Report: RPT4a",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(60),pnd_State_Table_Tircntl_State_Alpha_Code,"-",pnd_State_Table_Tircntl_State_Full_Name,
            NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(5, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(5), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(47),"State Withholding Reconciliation Summary",new TabSetting(120),"Report: RPT5",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(60),pnd_State_Table_Tircntl_State_Alpha_Code,"-",pnd_State_Table_Tircntl_State_Full_Name,NEWLINE,new 
            TabSetting(44),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask 
            ("MM/DD/YYYY"),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(6, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(6), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(43),"State Withholding Reconciliation Accepted Detail",new TabSetting(120),"Report: RPT6",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(60),pnd_State_Table_Tircntl_State_Alpha_Code,"-",pnd_State_Table_Tircntl_State_Full_Name,NEWLINE,new 
            TabSetting(44),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask 
            ("MM/DD/YYYY"),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(7, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(7), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(43),"State Withholding Reconciliation Rejected Detail",new TabSetting(120),"Report: RPT7",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(60),pnd_State_Table_Tircntl_State_Alpha_Code,"-",pnd_State_Table_Tircntl_State_Full_Name,NEWLINE,new 
            TabSetting(44),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask 
            ("MM/DD/YYYY"),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(8, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(8), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"State General Ledger Reconciliation Summary",new TabSetting(120),"Report: RPT8",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(60),pnd_State_Table_Tircntl_State_Alpha_Code,"-",pnd_State_Table_Tircntl_State_Full_Name,NEWLINE,new 
            TabSetting(44),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask 
            ("MM/DD/YYYY"),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(9, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(9), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(45),"State General Ledger Reconciliation Detail",new TabSetting(120),"Report: RPT9",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(60),pnd_State_Table_Tircntl_State_Alpha_Code,"-",pnd_State_Table_Tircntl_State_Full_Name,NEWLINE,new 
            TabSetting(44),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask 
            ("MM/DD/YYYY"),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(10, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(10), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(47),"State Correction Return Accepted Detail",new TabSetting(120),"Report: RPT4b",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(60),pnd_State_Table_Tircntl_State_Alpha_Code,"-",pnd_State_Table_Tircntl_State_Full_Name,NEWLINE,new 
            TabSetting(44),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask 
            ("MM/DD/YYYY"),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(11, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(11), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(51),"State Correction Return Detail",new TabSetting(120),"Report: RPT4c",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(60),pnd_State_Table_Tircntl_State_Alpha_Code,"-",pnd_State_Table_Tircntl_State_Full_Name,
            NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);

        getReports().setDisplayColumns(4, new ReportEmptyLineSuppression(true),"///TIN",
        		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),NEWLINE,"/",
        		pnd_Ws_Pnd_Disp_Rec,"///Contract",
        		pdaTwraform.getPnd_Twraform_Tirf_Contract_Nbr(),"Pa/y/e/e",
        		pdaTwraform.getPnd_Twraform_Tirf_Payee_Cde(),"//Name",
        		pdaTwraform.getPnd_Twraform_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"///State Distrib",
        		pdaTwraform.getPnd_Twraform_Tirf_State_Distr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(3),"///State Tax",
        		pdaTwraform.getPnd_Twraform_Tirf_State_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask 
            ("-Z,ZZZ,ZZ9.99"),new ColumnSpacing(5),"///Local Distrib",
        		pdaTwraform.getPnd_Twraform_Tirf_Loc_Distr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(2),"///Local Tax",
        		pdaTwraform.getPnd_Twraform_Tirf_Loc_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(3, new ReportEmptyLineSuppression(true),"///TIN",
        		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),"///Contract",
        		ldaTwrl9715.getForm_R_Tirf_Contract_Nbr(),"Pa/y/e/e",
        		ldaTwrl9715.getForm_R_Tirf_Payee_Cde(),"//Name",
        		ldaTwrl9715.getForm_R_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"#/of/Sta/tes",
        		ldaTwrl9715.getForm_R_Count_Casttirf_1099_R_State_Grp(), new ReportEditMask ("ZZ9"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
            "S/T/A/T",
        		pnd_State_Table_Tircntl_State_Alpha_Code,"/I/R/S",
        		ldaTwrl9715.getForm_R_Tirf_State_Irs_Rpt_Ind(),"///State Distrib",
        		ldaTwrl9715.getForm_R_Tirf_State_Distr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"///State Tax",
        		ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"///Local Distrib",
        		ldaTwrl9715.getForm_R_Tirf_Loc_Distr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"///Local Tax",
        		ldaTwrl9715.getForm_R_Tirf_Loc_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(6, "/TIN",
        		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
        		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
        		pnd_Recon_Case_Pnd_Header,"Form Count",
        		pnd_Recon_Case_Pnd_Form_Cnt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Distrib",
        		pnd_Recon_Case_Pnd_State_Distr, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Tax",
        		pnd_Recon_Case_Pnd_State_Tax, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Distrib",
        		pnd_Recon_Case_Pnd_Local_Distr, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Tax",
        		pnd_Recon_Case_Pnd_Local_Tax, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(10, new ReportEmptyLineSuppression(true),"///TIN",
        		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),NEWLINE,"/",
        		pnd_Ws_Pnd_Disp_Rec,"///Contract",
        		pdaTwraform.getPnd_Twraform_Tirf_Contract_Nbr(),"Pa/y/e/e",
        		pdaTwraform.getPnd_Twraform_Tirf_Payee_Cde(),"//Name",
        		pdaTwraform.getPnd_Twraform_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"///State Distrib",
        		pdaTwraform.getPnd_Twraform_Tirf_State_Distr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(3),"///State Tax",
        		pdaTwraform.getPnd_Twraform_Tirf_State_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask 
            ("-Z,ZZZ,ZZ9.99"),new ColumnSpacing(5),"///Local Distrib",
        		pdaTwraform.getPnd_Twraform_Tirf_Loc_Distr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(2),"///Local Tax",
        		pdaTwraform.getPnd_Twraform_Tirf_Loc_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(7, "/TIN",
        		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
        		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
        		pnd_Recon_Case_Pnd_Header,"Form Count",
        		pnd_Recon_Case_Pnd_Form_Cnt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Distrib",
        		pnd_Recon_Case_Pnd_State_Distr, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Tax",
        		pnd_Recon_Case_Pnd_State_Tax, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Distrib",
        		pnd_Recon_Case_Pnd_Local_Distr, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Tax",
        		pnd_Recon_Case_Pnd_Local_Tax, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(9, "/TIN",
        		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
        		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
        		pnd_Recon_Case_Pnd_Header,"Form Count",
        		pnd_Recon_Case_Pnd_Form_Cnt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Distrib",
        		pnd_Recon_Case_Pnd_State_Distr, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Tax",
        		pnd_Recon_Case_Pnd_State_Tax, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Distrib",
        		pnd_Recon_Case_Pnd_Local_Distr, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Tax",
        		pnd_Recon_Case_Pnd_Local_Tax, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(11, new ReportEmptyLineSuppression(true),"///TIN",
        		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),"///Contract",
        		ldaTwrl9715.getForm_R_Tirf_Contract_Nbr(),"Pa/y/e/e",
        		ldaTwrl9715.getForm_R_Tirf_Payee_Cde(),"//Name",
        		ldaTwrl9715.getForm_R_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"///State Distrib",
        		ldaTwrl9715.getForm_R_Tirf_State_Distr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(3),"///State Tax",
        		ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-Z,ZZZ,ZZ9.99"),new 
            ColumnSpacing(5),"///Local Distrib",
        		ldaTwrl9715.getForm_R_Tirf_Loc_Distr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(2),"///Local Tax",
        		ldaTwrl9715.getForm_R_Tirf_Loc_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(2, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",
            
        		pnd_Str_Pnd_Header1,NEWLINE,"/",
        		pnd_Str_Pnd_Header2,"Form/Count",
        		pnd_Str_Pnd_Form_Cnt,"State/Distribution",
        		pnd_Str_Pnd_State_Distr,"State/Withholding",
        		pnd_Str_Pnd_State_Tax,"Local/Distribution",
        		pnd_Str_Pnd_Local_Distr,"Local/Withholding",
        		pnd_Str_Pnd_Local_Tax);
        getReports().setDisplayColumns(5, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",
            
        		pnd_Stw_Pnd_Header,"Form/Count",
        		pnd_Stw_Pnd_Form_Cnt,"State/Distribution",
        		pnd_Stw_Pnd_State_Distr,"State/Withholding",
        		pnd_Stw_Pnd_State_Tax,"Local/Distribution",
        		pnd_Stw_Pnd_Local_Distr,"Local/Withholding",
        		pnd_Stw_Pnd_Local_Tax);
        getReports().setDisplayColumns(8, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",
            
        		pnd_Stg_Pnd_Header,"Form/Count",
        		pnd_Stg_Pnd_Form_Cnt,"State/Distribution",
        		pnd_Stg_Pnd_State_Distr,"State/Withholding",
        		pnd_Stg_Pnd_State_Tax,"Local/Distribution",
        		pnd_Stg_Pnd_Local_Distr,"Local/Withholding",
        		pnd_Stg_Pnd_Local_Tax);
    }
}
