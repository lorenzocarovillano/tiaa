/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:44:37 PM
**        * FROM NATURAL PROGRAM : Twrp8140
************************************************************
**        * FILE NAME            : Twrp8140.java
**        * CLASS NAME           : Twrp8140
**        * INSTANCE NAME        : Twrp8140
************************************************************
************************************************************************
* PROGRAM     :  TWRP8140
* SYSTEM      :  TAX WITHHOLDINGS AND REPORTING SYSTEM.
* AUTHOR      :  RIAD A. LOUTFI
* PURPOSE     :  CREATE A TABLE OF NEW TAX YEAR STATE CODES USING
*                AN OLD TAX YEAR STATE CODES TABLE.
*                TABLE NUMBER 2
* DATE        :  09/15/2000
************************************************************************
*  CHANGE  LOG
* ----------------------------------------------------------------------
* 09/12/02 RM  CHANGED REPORT HEADING TO REFLECT TCII CHANGES
* 08/13/02 JR  CHANGED TIRCNTL-STATE-TAX-ID-ONE-MORE TO
*                      TIRCNTL-STATE-TAX-ID-TCII
* 10/18/11  JB NON-ERISA TDA - ADD COMPANY CODE 'F' SCAN JB1011
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp8140 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_rs;
    private DbsField rs_Tircntl_Tbl_Nbr;
    private DbsField rs_Tircntl_Tax_Year;
    private DbsField rs_Tircntl_Seq_Nbr;

    private DbsGroup rs_Tircntl_State_Code_Tbl;
    private DbsField rs_Tircntl_State_Old_Code;
    private DbsField rs_Tircntl_State_Alpha_Code;
    private DbsField rs_Tircntl_State_Full_Name;
    private DbsField rs_Tircntl_State_Tax_Id_Tiaa;
    private DbsField rs_Tircntl_State_Tax_Id_Cref;
    private DbsField rs_Tircntl_State_Tax_Id_Life;
    private DbsField rs_Tircntl_State_Tax_Id_Tcii;
    private DbsField rs_Tircntl_State_Tax_Id_Trust;
    private DbsField rs_Tircntl_State_Tax_Id_Fsb;
    private DbsField rs_Tircntl_Comb_Fed_Ind;
    private DbsField rs_Tircntl_Comb_Fed_Updte_Dte;
    private DbsField rs_Tircntl_State_Ind;
    private DbsField rs_Tircntl_Media_Code;
    private DbsField rs_Tircntl_State_Whhld_Ind;
    private DbsField rs_Tircntl_State_Updte_Dte;
    private DbsField rs_Tircntl_State_Entry_Dte;
    private DbsField rs_Tircntl_State_Comment;
    private DbsField rs_Tircntl_State_Updte_User;
    private DbsField rs_Tircntl_Fed_Ind;
    private DbsField rs_Tircntl_Corr_Comb_Fed_Ind;
    private DbsField rs_Tircntl_Corr_Fed_Ind;
    private DbsField rs_Tircntl_Corr_Media_Code;
    private DbsField rs_Tircntl_Corr_State_Ind;
    private DbsField rs_Tircntl_Threshold_Amt;

    private DataAccessProgramView vw_us;
    private DbsField us_Tircntl_Tbl_Nbr;
    private DbsField us_Tircntl_Tax_Year;
    private DbsField us_Tircntl_Seq_Nbr;

    private DbsGroup us_Tircntl_State_Code_Tbl;
    private DbsField us_Tircntl_State_Old_Code;
    private DbsField us_Tircntl_State_Alpha_Code;
    private DbsField us_Tircntl_State_Full_Name;
    private DbsField us_Tircntl_State_Tax_Id_Tiaa;
    private DbsField us_Tircntl_State_Tax_Id_Cref;
    private DbsField us_Tircntl_State_Tax_Id_Life;
    private DbsField us_Tircntl_State_Tax_Id_Tcii;
    private DbsField us_Tircntl_State_Tax_Id_Trust;
    private DbsField us_Tircntl_State_Tax_Id_Fsb;
    private DbsField us_Tircntl_Comb_Fed_Ind;
    private DbsField us_Tircntl_Comb_Fed_Updte_Dte;
    private DbsField us_Tircntl_State_Ind;
    private DbsField us_Tircntl_Media_Code;
    private DbsField us_Tircntl_State_Whhld_Ind;
    private DbsField us_Tircntl_State_Updte_Dte;
    private DbsField us_Tircntl_State_Entry_Dte;
    private DbsField us_Tircntl_State_Comment;
    private DbsField us_Tircntl_State_Updte_User;
    private DbsField us_Tircntl_Fed_Ind;
    private DbsField us_Tircntl_Corr_Comb_Fed_Ind;
    private DbsField us_Tircntl_Corr_Fed_Ind;
    private DbsField us_Tircntl_Corr_Media_Code;
    private DbsField us_Tircntl_Corr_State_Ind;
    private DbsField us_Tircntl_Threshold_Amt;

    private DataAccessProgramView vw_ss;
    private DbsField ss_Tircntl_Tbl_Nbr;
    private DbsField ss_Tircntl_Tax_Year;
    private DbsField ss_Tircntl_Seq_Nbr;

    private DbsGroup ss_Tircntl_State_Code_Tbl;
    private DbsField ss_Tircntl_State_Old_Code;
    private DbsField ss_Tircntl_State_Alpha_Code;
    private DbsField ss_Tircntl_State_Full_Name;
    private DbsField ss_Tircntl_State_Tax_Id_Tiaa;
    private DbsField ss_Tircntl_State_Tax_Id_Cref;
    private DbsField ss_Tircntl_State_Tax_Id_Life;
    private DbsField ss_Tircntl_State_Tax_Id_Tcii;
    private DbsField ss_Tircntl_State_Tax_Id_Trust;
    private DbsField ss_Tircntl_State_Tax_Id_Fsb;
    private DbsField ss_Tircntl_Comb_Fed_Ind;
    private DbsField ss_Tircntl_Comb_Fed_Updte_Dte;
    private DbsField ss_Tircntl_State_Ind;
    private DbsField ss_Tircntl_Media_Code;
    private DbsField ss_Tircntl_State_Whhld_Ind;
    private DbsField ss_Tircntl_State_Updte_Dte;
    private DbsField ss_Tircntl_State_Entry_Dte;
    private DbsField ss_Tircntl_State_Comment;
    private DbsField ss_Tircntl_State_Updte_User;
    private DbsField ss_Tircntl_Fed_Ind;
    private DbsField ss_Tircntl_Corr_Comb_Fed_Ind;
    private DbsField ss_Tircntl_Corr_Fed_Ind;
    private DbsField ss_Tircntl_Corr_Media_Code;
    private DbsField ss_Tircntl_Corr_State_Ind;
    private DbsField ss_Tircntl_Threshold_Amt;
    private DbsField pnd_Read1_Ctr;
    private DbsField pnd_Read2_Ctr;
    private DbsField pnd_Updt_Ctr;
    private DbsField pnd_Store_Ctr;
    private DbsField pnd_Old_Tax_Year;

    private DbsGroup pnd_Old_Tax_Year__R_Field_1;
    private DbsField pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N;
    private DbsField pnd_New_Tax_Year;

    private DbsGroup pnd_New_Tax_Year__R_Field_2;
    private DbsField pnd_New_Tax_Year_Pnd_New_Tax_Year_N;
    private DbsField pnd_Super1;

    private DbsGroup pnd_Super1__R_Field_3;
    private DbsField pnd_Super1_Pnd_S1_Tbl_Nbr;
    private DbsField pnd_Super1_Pnd_S1_Tax_Year;
    private DbsField pnd_Super1_Pnd_S1_Old_Code;
    private DbsField pnd_Super2;

    private DbsGroup pnd_Super2__R_Field_4;
    private DbsField pnd_Super2_Pnd_S2_Tbl_Nbr;
    private DbsField pnd_Super2_Pnd_S2_Tax_Year;
    private DbsField pnd_Super2_Pnd_S2_Old_Code;
    private DbsField pnd_New_Record_Found;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_rs = new DataAccessProgramView(new NameInfo("vw_rs", "RS"), "TIRCNTL_STATE_CODE_TBL_VIEW", "TIR_CONTROL");
        rs_Tircntl_Tbl_Nbr = vw_rs.getRecord().newFieldInGroup("rs_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        rs_Tircntl_Tax_Year = vw_rs.getRecord().newFieldInGroup("rs_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        rs_Tircntl_Seq_Nbr = vw_rs.getRecord().newFieldInGroup("rs_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        rs_Tircntl_State_Code_Tbl = vw_rs.getRecord().newGroupInGroup("RS_TIRCNTL_STATE_CODE_TBL", "TIRCNTL-STATE-CODE-TBL");
        rs_Tircntl_State_Old_Code = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_State_Old_Code", "TIRCNTL-STATE-OLD-CODE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TIRCNTL_STATE_OLD_CODE");
        rs_Tircntl_State_Alpha_Code = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_State_Alpha_Code", "TIRCNTL-STATE-ALPHA-CODE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TIRCNTL_STATE_ALPHA_CODE");
        rs_Tircntl_State_Full_Name = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_State_Full_Name", "TIRCNTL-STATE-FULL-NAME", FieldType.STRING, 
            19, RepeatingFieldStrategy.None, "TIRCNTL_STATE_FULL_NAME");
        rs_Tircntl_State_Tax_Id_Tiaa = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_State_Tax_Id_Tiaa", "TIRCNTL-STATE-TAX-ID-TIAA", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TIRCNTL_STATE_TAX_ID_TIAA");
        rs_Tircntl_State_Tax_Id_Cref = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_State_Tax_Id_Cref", "TIRCNTL-STATE-TAX-ID-CREF", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TIRCNTL_STATE_TAX_ID_CREF");
        rs_Tircntl_State_Tax_Id_Life = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_State_Tax_Id_Life", "TIRCNTL-STATE-TAX-ID-LIFE", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TIRCNTL_STATE_TAX_ID_LIFE");
        rs_Tircntl_State_Tax_Id_Tcii = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_State_Tax_Id_Tcii", "TIRCNTL-STATE-TAX-ID-TCII", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TIRCNTL_STATE_TAX_ID_TCII");
        rs_Tircntl_State_Tax_Id_Trust = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_State_Tax_Id_Trust", "TIRCNTL-STATE-TAX-ID-TRUST", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TIRCNTL_STATE_TAX_ID_TRUST");
        rs_Tircntl_State_Tax_Id_Fsb = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_State_Tax_Id_Fsb", "TIRCNTL-STATE-TAX-ID-FSB", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TIRCNTL_STATE_TAX_ID_FSB");
        rs_Tircntl_Comb_Fed_Ind = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_Comb_Fed_Ind", "TIRCNTL-COMB-FED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_COMB_FED_IND");
        rs_Tircntl_Comb_Fed_Updte_Dte = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_Comb_Fed_Updte_Dte", "TIRCNTL-COMB-FED-UPDTE-DTE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_COMB_FED_UPDTE_DTE");
        rs_Tircntl_State_Ind = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_State_Ind", "TIRCNTL-STATE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_STATE_IND");
        rs_Tircntl_Media_Code = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_Media_Code", "TIRCNTL-MEDIA-CODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_MEDIA_CODE");
        rs_Tircntl_State_Whhld_Ind = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_State_Whhld_Ind", "TIRCNTL-STATE-WHHLD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_STATE_WHHLD_IND");
        rs_Tircntl_State_Updte_Dte = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_State_Updte_Dte", "TIRCNTL-STATE-UPDTE-DTE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_STATE_UPDTE_DTE");
        rs_Tircntl_State_Entry_Dte = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_State_Entry_Dte", "TIRCNTL-STATE-ENTRY-DTE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_STATE_ENTRY_DTE");
        rs_Tircntl_State_Comment = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_State_Comment", "TIRCNTL-STATE-COMMENT", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "TIRCNTL_STATE_COMMENT");
        rs_Tircntl_State_Updte_User = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_State_Updte_User", "TIRCNTL-STATE-UPDTE-USER", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_STATE_UPDTE_USER");
        rs_Tircntl_Fed_Ind = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_Fed_Ind", "TIRCNTL-FED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_FED_IND");
        rs_Tircntl_Fed_Ind.setDdmHeader("ORIG/FED/METH");
        rs_Tircntl_Corr_Comb_Fed_Ind = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_Corr_Comb_Fed_Ind", "TIRCNTL-CORR-COMB-FED-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_CORR_COMB_FED_IND");
        rs_Tircntl_Corr_Comb_Fed_Ind.setDdmHeader("CORR/FED/CMB");
        rs_Tircntl_Corr_Fed_Ind = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_Corr_Fed_Ind", "TIRCNTL-CORR-FED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_CORR_FED_IND");
        rs_Tircntl_Corr_Fed_Ind.setDdmHeader("CORR/FED/METH");
        rs_Tircntl_Corr_Media_Code = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_Corr_Media_Code", "TIRCNTL-CORR-MEDIA-CODE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_CORR_MEDIA_CODE");
        rs_Tircntl_Corr_Media_Code.setDdmHeader("CORR/STATE/MEDIA");
        rs_Tircntl_Corr_State_Ind = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_Corr_State_Ind", "TIRCNTL-CORR-STATE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_CORR_STATE_IND");
        rs_Tircntl_Corr_State_Ind.setDdmHeader("CORR/STATE/METH");
        rs_Tircntl_Threshold_Amt = rs_Tircntl_State_Code_Tbl.newFieldInGroup("rs_Tircntl_Threshold_Amt", "TIRCNTL-THRESHOLD-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TIRCNTL_THRESHOLD_AMT");
        rs_Tircntl_Threshold_Amt.setDdmHeader("REPORTING/THRESHOLD/AMOUNT");
        registerRecord(vw_rs);

        vw_us = new DataAccessProgramView(new NameInfo("vw_us", "US"), "TIRCNTL_STATE_CODE_TBL_VIEW", "TIR_CONTROL");
        us_Tircntl_Tbl_Nbr = vw_us.getRecord().newFieldInGroup("us_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        us_Tircntl_Tax_Year = vw_us.getRecord().newFieldInGroup("us_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        us_Tircntl_Seq_Nbr = vw_us.getRecord().newFieldInGroup("us_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        us_Tircntl_State_Code_Tbl = vw_us.getRecord().newGroupInGroup("US_TIRCNTL_STATE_CODE_TBL", "TIRCNTL-STATE-CODE-TBL");
        us_Tircntl_State_Old_Code = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_State_Old_Code", "TIRCNTL-STATE-OLD-CODE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TIRCNTL_STATE_OLD_CODE");
        us_Tircntl_State_Alpha_Code = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_State_Alpha_Code", "TIRCNTL-STATE-ALPHA-CODE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TIRCNTL_STATE_ALPHA_CODE");
        us_Tircntl_State_Full_Name = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_State_Full_Name", "TIRCNTL-STATE-FULL-NAME", FieldType.STRING, 
            19, RepeatingFieldStrategy.None, "TIRCNTL_STATE_FULL_NAME");
        us_Tircntl_State_Tax_Id_Tiaa = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_State_Tax_Id_Tiaa", "TIRCNTL-STATE-TAX-ID-TIAA", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TIRCNTL_STATE_TAX_ID_TIAA");
        us_Tircntl_State_Tax_Id_Cref = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_State_Tax_Id_Cref", "TIRCNTL-STATE-TAX-ID-CREF", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TIRCNTL_STATE_TAX_ID_CREF");
        us_Tircntl_State_Tax_Id_Life = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_State_Tax_Id_Life", "TIRCNTL-STATE-TAX-ID-LIFE", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TIRCNTL_STATE_TAX_ID_LIFE");
        us_Tircntl_State_Tax_Id_Tcii = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_State_Tax_Id_Tcii", "TIRCNTL-STATE-TAX-ID-TCII", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TIRCNTL_STATE_TAX_ID_TCII");
        us_Tircntl_State_Tax_Id_Trust = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_State_Tax_Id_Trust", "TIRCNTL-STATE-TAX-ID-TRUST", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TIRCNTL_STATE_TAX_ID_TRUST");
        us_Tircntl_State_Tax_Id_Fsb = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_State_Tax_Id_Fsb", "TIRCNTL-STATE-TAX-ID-FSB", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TIRCNTL_STATE_TAX_ID_FSB");
        us_Tircntl_Comb_Fed_Ind = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_Comb_Fed_Ind", "TIRCNTL-COMB-FED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_COMB_FED_IND");
        us_Tircntl_Comb_Fed_Updte_Dte = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_Comb_Fed_Updte_Dte", "TIRCNTL-COMB-FED-UPDTE-DTE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_COMB_FED_UPDTE_DTE");
        us_Tircntl_State_Ind = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_State_Ind", "TIRCNTL-STATE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_STATE_IND");
        us_Tircntl_Media_Code = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_Media_Code", "TIRCNTL-MEDIA-CODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_MEDIA_CODE");
        us_Tircntl_State_Whhld_Ind = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_State_Whhld_Ind", "TIRCNTL-STATE-WHHLD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_STATE_WHHLD_IND");
        us_Tircntl_State_Updte_Dte = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_State_Updte_Dte", "TIRCNTL-STATE-UPDTE-DTE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_STATE_UPDTE_DTE");
        us_Tircntl_State_Entry_Dte = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_State_Entry_Dte", "TIRCNTL-STATE-ENTRY-DTE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_STATE_ENTRY_DTE");
        us_Tircntl_State_Comment = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_State_Comment", "TIRCNTL-STATE-COMMENT", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "TIRCNTL_STATE_COMMENT");
        us_Tircntl_State_Updte_User = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_State_Updte_User", "TIRCNTL-STATE-UPDTE-USER", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_STATE_UPDTE_USER");
        us_Tircntl_Fed_Ind = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_Fed_Ind", "TIRCNTL-FED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_FED_IND");
        us_Tircntl_Fed_Ind.setDdmHeader("ORIG/FED/METH");
        us_Tircntl_Corr_Comb_Fed_Ind = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_Corr_Comb_Fed_Ind", "TIRCNTL-CORR-COMB-FED-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_CORR_COMB_FED_IND");
        us_Tircntl_Corr_Comb_Fed_Ind.setDdmHeader("CORR/FED/CMB");
        us_Tircntl_Corr_Fed_Ind = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_Corr_Fed_Ind", "TIRCNTL-CORR-FED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_CORR_FED_IND");
        us_Tircntl_Corr_Fed_Ind.setDdmHeader("CORR/FED/METH");
        us_Tircntl_Corr_Media_Code = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_Corr_Media_Code", "TIRCNTL-CORR-MEDIA-CODE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_CORR_MEDIA_CODE");
        us_Tircntl_Corr_Media_Code.setDdmHeader("CORR/STATE/MEDIA");
        us_Tircntl_Corr_State_Ind = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_Corr_State_Ind", "TIRCNTL-CORR-STATE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_CORR_STATE_IND");
        us_Tircntl_Corr_State_Ind.setDdmHeader("CORR/STATE/METH");
        us_Tircntl_Threshold_Amt = us_Tircntl_State_Code_Tbl.newFieldInGroup("us_Tircntl_Threshold_Amt", "TIRCNTL-THRESHOLD-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TIRCNTL_THRESHOLD_AMT");
        us_Tircntl_Threshold_Amt.setDdmHeader("REPORTING/THRESHOLD/AMOUNT");
        registerRecord(vw_us);

        vw_ss = new DataAccessProgramView(new NameInfo("vw_ss", "SS"), "TIRCNTL_STATE_CODE_TBL_VIEW", "TIR_CONTROL");
        ss_Tircntl_Tbl_Nbr = vw_ss.getRecord().newFieldInGroup("ss_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        ss_Tircntl_Tax_Year = vw_ss.getRecord().newFieldInGroup("ss_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        ss_Tircntl_Seq_Nbr = vw_ss.getRecord().newFieldInGroup("ss_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        ss_Tircntl_State_Code_Tbl = vw_ss.getRecord().newGroupInGroup("SS_TIRCNTL_STATE_CODE_TBL", "TIRCNTL-STATE-CODE-TBL");
        ss_Tircntl_State_Old_Code = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_State_Old_Code", "TIRCNTL-STATE-OLD-CODE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TIRCNTL_STATE_OLD_CODE");
        ss_Tircntl_State_Alpha_Code = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_State_Alpha_Code", "TIRCNTL-STATE-ALPHA-CODE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TIRCNTL_STATE_ALPHA_CODE");
        ss_Tircntl_State_Full_Name = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_State_Full_Name", "TIRCNTL-STATE-FULL-NAME", FieldType.STRING, 
            19, RepeatingFieldStrategy.None, "TIRCNTL_STATE_FULL_NAME");
        ss_Tircntl_State_Tax_Id_Tiaa = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_State_Tax_Id_Tiaa", "TIRCNTL-STATE-TAX-ID-TIAA", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TIRCNTL_STATE_TAX_ID_TIAA");
        ss_Tircntl_State_Tax_Id_Cref = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_State_Tax_Id_Cref", "TIRCNTL-STATE-TAX-ID-CREF", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TIRCNTL_STATE_TAX_ID_CREF");
        ss_Tircntl_State_Tax_Id_Life = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_State_Tax_Id_Life", "TIRCNTL-STATE-TAX-ID-LIFE", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TIRCNTL_STATE_TAX_ID_LIFE");
        ss_Tircntl_State_Tax_Id_Tcii = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_State_Tax_Id_Tcii", "TIRCNTL-STATE-TAX-ID-TCII", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TIRCNTL_STATE_TAX_ID_TCII");
        ss_Tircntl_State_Tax_Id_Trust = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_State_Tax_Id_Trust", "TIRCNTL-STATE-TAX-ID-TRUST", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TIRCNTL_STATE_TAX_ID_TRUST");
        ss_Tircntl_State_Tax_Id_Fsb = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_State_Tax_Id_Fsb", "TIRCNTL-STATE-TAX-ID-FSB", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "TIRCNTL_STATE_TAX_ID_FSB");
        ss_Tircntl_Comb_Fed_Ind = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_Comb_Fed_Ind", "TIRCNTL-COMB-FED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_COMB_FED_IND");
        ss_Tircntl_Comb_Fed_Updte_Dte = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_Comb_Fed_Updte_Dte", "TIRCNTL-COMB-FED-UPDTE-DTE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_COMB_FED_UPDTE_DTE");
        ss_Tircntl_State_Ind = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_State_Ind", "TIRCNTL-STATE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_STATE_IND");
        ss_Tircntl_Media_Code = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_Media_Code", "TIRCNTL-MEDIA-CODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_MEDIA_CODE");
        ss_Tircntl_State_Whhld_Ind = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_State_Whhld_Ind", "TIRCNTL-STATE-WHHLD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_STATE_WHHLD_IND");
        ss_Tircntl_State_Updte_Dte = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_State_Updte_Dte", "TIRCNTL-STATE-UPDTE-DTE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_STATE_UPDTE_DTE");
        ss_Tircntl_State_Entry_Dte = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_State_Entry_Dte", "TIRCNTL-STATE-ENTRY-DTE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_STATE_ENTRY_DTE");
        ss_Tircntl_State_Comment = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_State_Comment", "TIRCNTL-STATE-COMMENT", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "TIRCNTL_STATE_COMMENT");
        ss_Tircntl_State_Updte_User = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_State_Updte_User", "TIRCNTL-STATE-UPDTE-USER", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_STATE_UPDTE_USER");
        ss_Tircntl_Fed_Ind = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_Fed_Ind", "TIRCNTL-FED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_FED_IND");
        ss_Tircntl_Fed_Ind.setDdmHeader("ORIG/FED/METH");
        ss_Tircntl_Corr_Comb_Fed_Ind = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_Corr_Comb_Fed_Ind", "TIRCNTL-CORR-COMB-FED-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_CORR_COMB_FED_IND");
        ss_Tircntl_Corr_Comb_Fed_Ind.setDdmHeader("CORR/FED/CMB");
        ss_Tircntl_Corr_Fed_Ind = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_Corr_Fed_Ind", "TIRCNTL-CORR-FED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_CORR_FED_IND");
        ss_Tircntl_Corr_Fed_Ind.setDdmHeader("CORR/FED/METH");
        ss_Tircntl_Corr_Media_Code = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_Corr_Media_Code", "TIRCNTL-CORR-MEDIA-CODE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_CORR_MEDIA_CODE");
        ss_Tircntl_Corr_Media_Code.setDdmHeader("CORR/STATE/MEDIA");
        ss_Tircntl_Corr_State_Ind = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_Corr_State_Ind", "TIRCNTL-CORR-STATE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_CORR_STATE_IND");
        ss_Tircntl_Corr_State_Ind.setDdmHeader("CORR/STATE/METH");
        ss_Tircntl_Threshold_Amt = ss_Tircntl_State_Code_Tbl.newFieldInGroup("ss_Tircntl_Threshold_Amt", "TIRCNTL-THRESHOLD-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TIRCNTL_THRESHOLD_AMT");
        ss_Tircntl_Threshold_Amt.setDdmHeader("REPORTING/THRESHOLD/AMOUNT");
        registerRecord(vw_ss);

        pnd_Read1_Ctr = localVariables.newFieldInRecord("pnd_Read1_Ctr", "#READ1-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Read2_Ctr = localVariables.newFieldInRecord("pnd_Read2_Ctr", "#READ2-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Updt_Ctr = localVariables.newFieldInRecord("pnd_Updt_Ctr", "#UPDT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Store_Ctr = localVariables.newFieldInRecord("pnd_Store_Ctr", "#STORE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Old_Tax_Year = localVariables.newFieldInRecord("pnd_Old_Tax_Year", "#OLD-TAX-YEAR", FieldType.STRING, 4);

        pnd_Old_Tax_Year__R_Field_1 = localVariables.newGroupInRecord("pnd_Old_Tax_Year__R_Field_1", "REDEFINE", pnd_Old_Tax_Year);
        pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N = pnd_Old_Tax_Year__R_Field_1.newFieldInGroup("pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N", "#OLD-TAX-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_New_Tax_Year = localVariables.newFieldInRecord("pnd_New_Tax_Year", "#NEW-TAX-YEAR", FieldType.STRING, 4);

        pnd_New_Tax_Year__R_Field_2 = localVariables.newGroupInRecord("pnd_New_Tax_Year__R_Field_2", "REDEFINE", pnd_New_Tax_Year);
        pnd_New_Tax_Year_Pnd_New_Tax_Year_N = pnd_New_Tax_Year__R_Field_2.newFieldInGroup("pnd_New_Tax_Year_Pnd_New_Tax_Year_N", "#NEW-TAX-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_Super1 = localVariables.newFieldInRecord("pnd_Super1", "#SUPER1", FieldType.STRING, 8);

        pnd_Super1__R_Field_3 = localVariables.newGroupInRecord("pnd_Super1__R_Field_3", "REDEFINE", pnd_Super1);
        pnd_Super1_Pnd_S1_Tbl_Nbr = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Tbl_Nbr", "#S1-TBL-NBR", FieldType.NUMERIC, 1);
        pnd_Super1_Pnd_S1_Tax_Year = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Tax_Year", "#S1-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Super1_Pnd_S1_Old_Code = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Old_Code", "#S1-OLD-CODE", FieldType.STRING, 3);
        pnd_Super2 = localVariables.newFieldInRecord("pnd_Super2", "#SUPER2", FieldType.STRING, 8);

        pnd_Super2__R_Field_4 = localVariables.newGroupInRecord("pnd_Super2__R_Field_4", "REDEFINE", pnd_Super2);
        pnd_Super2_Pnd_S2_Tbl_Nbr = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Tbl_Nbr", "#S2-TBL-NBR", FieldType.NUMERIC, 1);
        pnd_Super2_Pnd_S2_Tax_Year = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Tax_Year", "#S2-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Super2_Pnd_S2_Old_Code = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Old_Code", "#S2-OLD-CODE", FieldType.STRING, 3);
        pnd_New_Record_Found = localVariables.newFieldInRecord("pnd_New_Record_Found", "#NEW-RECORD-FOUND", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_rs.reset();
        vw_us.reset();
        vw_ss.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp8140() throws Exception
    {
        super("Twrp8140");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp8140|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Old_Tax_Year,pnd_New_Tax_Year);                                                                    //Natural: INPUT #OLD-TAX-YEAR #NEW-TAX-YEAR
                getReports().display(0, pnd_Old_Tax_Year,pnd_New_Tax_Year);                                                                                               //Natural: DISPLAY ( 00 ) #OLD-TAX-YEAR #NEW-TAX-YEAR
                if (Global.isEscape()) return;
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                pnd_Super1_Pnd_S1_Tbl_Nbr.setValue(2);                                                                                                                    //Natural: ASSIGN #S1-TBL-NBR := 2
                pnd_Super1_Pnd_S1_Tax_Year.setValue(pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N);                                                                                 //Natural: ASSIGN #S1-TAX-YEAR := #OLD-TAX-YEAR-N
                pnd_Super1_Pnd_S1_Old_Code.setValue(" ");                                                                                                                 //Natural: ASSIGN #S1-OLD-CODE := ' '
                vw_rs.startDatabaseRead                                                                                                                                   //Natural: READ RS WITH TIRCNTL-NBR-YEAR-OLD-STATE-CDE = #SUPER1
                (
                "READ01",
                new Wc[] { new Wc("TIRCNTL_NBR_YEAR_OLD_STATE_CDE", ">=", pnd_Super1, WcType.BY) },
                new Oc[] { new Oc("TIRCNTL_NBR_YEAR_OLD_STATE_CDE", "ASC") }
                );
                READ01:
                while (condition(vw_rs.readNextRow("READ01")))
                {
                    if (condition(rs_Tircntl_Tbl_Nbr.equals(pnd_Super1_Pnd_S1_Tbl_Nbr) && rs_Tircntl_Tax_Year.equals(pnd_Super1_Pnd_S1_Tax_Year)))                        //Natural: IF RS.TIRCNTL-TBL-NBR = #S1-TBL-NBR AND RS.TIRCNTL-TAX-YEAR = #S1-TAX-YEAR
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Read1_Ctr.nadd(1);                                                                                                                                //Natural: ADD 1 TO #READ1-CTR
                    pnd_New_Record_Found.setValue(false);                                                                                                                 //Natural: ASSIGN #NEW-RECORD-FOUND := FALSE
                    pnd_Super2_Pnd_S2_Tbl_Nbr.setValue(2);                                                                                                                //Natural: ASSIGN #S2-TBL-NBR := 2
                    pnd_Super2_Pnd_S2_Tax_Year.setValue(pnd_New_Tax_Year_Pnd_New_Tax_Year_N);                                                                             //Natural: ASSIGN #S2-TAX-YEAR := #NEW-TAX-YEAR-N
                    pnd_Super2_Pnd_S2_Old_Code.setValue(rs_Tircntl_State_Old_Code);                                                                                       //Natural: ASSIGN #S2-OLD-CODE := RS.TIRCNTL-STATE-OLD-CODE
                    vw_us.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) US WITH TIRCNTL-NBR-YEAR-OLD-STATE-CDE = #SUPER2
                    (
                    "RL2",
                    new Wc[] { new Wc("TIRCNTL_NBR_YEAR_OLD_STATE_CDE", ">=", pnd_Super2, WcType.BY) },
                    new Oc[] { new Oc("TIRCNTL_NBR_YEAR_OLD_STATE_CDE", "ASC") },
                    1
                    );
                    RL2:
                    while (condition(vw_us.readNextRow("RL2")))
                    {
                        if (condition(us_Tircntl_Tbl_Nbr.equals(pnd_Super2_Pnd_S2_Tbl_Nbr) && us_Tircntl_Tax_Year.equals(pnd_Super2_Pnd_S2_Tax_Year) &&                   //Natural: IF US.TIRCNTL-TBL-NBR = #S2-TBL-NBR AND US.TIRCNTL-TAX-YEAR = #S2-TAX-YEAR AND US.TIRCNTL-STATE-OLD-CODE = #S2-OLD-CODE
                            us_Tircntl_State_Old_Code.equals(pnd_Super2_Pnd_S2_Old_Code)))
                        {
                            pnd_New_Record_Found.setValue(true);                                                                                                          //Natural: ASSIGN #NEW-RECORD-FOUND := TRUE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Read2_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #READ2-CTR
                        if (condition(us_Tircntl_Seq_Nbr.equals(rs_Tircntl_Seq_Nbr) && us_Tircntl_State_Alpha_Code.equals(rs_Tircntl_State_Alpha_Code)                    //Natural: IF US.TIRCNTL-SEQ-NBR = RS.TIRCNTL-SEQ-NBR AND US.TIRCNTL-STATE-ALPHA-CODE = RS.TIRCNTL-STATE-ALPHA-CODE AND US.TIRCNTL-STATE-FULL-NAME = RS.TIRCNTL-STATE-FULL-NAME AND US.TIRCNTL-STATE-TAX-ID-TIAA = RS.TIRCNTL-STATE-TAX-ID-TIAA AND US.TIRCNTL-STATE-TAX-ID-CREF = RS.TIRCNTL-STATE-TAX-ID-CREF AND US.TIRCNTL-STATE-TAX-ID-LIFE = RS.TIRCNTL-STATE-TAX-ID-LIFE AND US.TIRCNTL-STATE-TAX-ID-TCII = RS.TIRCNTL-STATE-TAX-ID-TCII AND US.TIRCNTL-STATE-TAX-ID-TRUST = RS.TIRCNTL-STATE-TAX-ID-TRUST AND US.TIRCNTL-STATE-TAX-ID-FSB = RS.TIRCNTL-STATE-TAX-ID-FSB AND US.TIRCNTL-COMB-FED-IND = RS.TIRCNTL-COMB-FED-IND AND US.TIRCNTL-COMB-FED-UPDTE-DTE = RS.TIRCNTL-COMB-FED-UPDTE-DTE AND US.TIRCNTL-STATE-IND = RS.TIRCNTL-STATE-IND AND US.TIRCNTL-MEDIA-CODE = RS.TIRCNTL-MEDIA-CODE AND US.TIRCNTL-STATE-WHHLD-IND = RS.TIRCNTL-STATE-WHHLD-IND AND US.TIRCNTL-STATE-UPDTE-DTE = RS.TIRCNTL-STATE-UPDTE-DTE AND US.TIRCNTL-STATE-ENTRY-DTE = RS.TIRCNTL-STATE-ENTRY-DTE AND US.TIRCNTL-STATE-COMMENT = RS.TIRCNTL-STATE-COMMENT AND US.TIRCNTL-STATE-UPDTE-USER = RS.TIRCNTL-STATE-UPDTE-USER AND US.TIRCNTL-FED-IND = RS.TIRCNTL-FED-IND AND US.TIRCNTL-CORR-COMB-FED-IND = RS.TIRCNTL-CORR-COMB-FED-IND AND US.TIRCNTL-CORR-FED-IND = RS.TIRCNTL-CORR-FED-IND AND US.TIRCNTL-CORR-MEDIA-CODE = RS.TIRCNTL-CORR-MEDIA-CODE AND US.TIRCNTL-CORR-STATE-IND = RS.TIRCNTL-CORR-STATE-IND AND US.TIRCNTL-THRESHOLD-AMT = RS.TIRCNTL-THRESHOLD-AMT
                            && us_Tircntl_State_Full_Name.equals(rs_Tircntl_State_Full_Name) && us_Tircntl_State_Tax_Id_Tiaa.equals(rs_Tircntl_State_Tax_Id_Tiaa) 
                            && us_Tircntl_State_Tax_Id_Cref.equals(rs_Tircntl_State_Tax_Id_Cref) && us_Tircntl_State_Tax_Id_Life.equals(rs_Tircntl_State_Tax_Id_Life) 
                            && us_Tircntl_State_Tax_Id_Tcii.equals(rs_Tircntl_State_Tax_Id_Tcii) && us_Tircntl_State_Tax_Id_Trust.equals(rs_Tircntl_State_Tax_Id_Trust) 
                            && us_Tircntl_State_Tax_Id_Fsb.equals(rs_Tircntl_State_Tax_Id_Fsb) && us_Tircntl_Comb_Fed_Ind.equals(rs_Tircntl_Comb_Fed_Ind) 
                            && us_Tircntl_Comb_Fed_Updte_Dte.equals(rs_Tircntl_Comb_Fed_Updte_Dte) && us_Tircntl_State_Ind.equals(rs_Tircntl_State_Ind) 
                            && us_Tircntl_Media_Code.equals(rs_Tircntl_Media_Code) && us_Tircntl_State_Whhld_Ind.equals(rs_Tircntl_State_Whhld_Ind) && us_Tircntl_State_Updte_Dte.equals(rs_Tircntl_State_Updte_Dte) 
                            && us_Tircntl_State_Entry_Dte.equals(rs_Tircntl_State_Entry_Dte) && us_Tircntl_State_Comment.equals(rs_Tircntl_State_Comment) 
                            && us_Tircntl_State_Updte_User.equals(rs_Tircntl_State_Updte_User) && us_Tircntl_Fed_Ind.equals(rs_Tircntl_Fed_Ind) && us_Tircntl_Corr_Comb_Fed_Ind.equals(rs_Tircntl_Corr_Comb_Fed_Ind) 
                            && us_Tircntl_Corr_Fed_Ind.equals(rs_Tircntl_Corr_Fed_Ind) && us_Tircntl_Corr_Media_Code.equals(rs_Tircntl_Corr_Media_Code) 
                            && us_Tircntl_Corr_State_Ind.equals(rs_Tircntl_Corr_State_Ind) && us_Tircntl_Threshold_Amt.equals(rs_Tircntl_Threshold_Amt)))
                        {
                            //*                                    JB1011 ABOVE LINE
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            G1:                                                                                                                                           //Natural: GET US *ISN ( RL2. )
                            vw_us.readByID(vw_us.getAstISN("RL2"), "G1");
                            us_Tircntl_Seq_Nbr.setValue(rs_Tircntl_Seq_Nbr);                                                                                              //Natural: ASSIGN US.TIRCNTL-SEQ-NBR := RS.TIRCNTL-SEQ-NBR
                            us_Tircntl_State_Alpha_Code.setValue(rs_Tircntl_State_Alpha_Code);                                                                            //Natural: ASSIGN US.TIRCNTL-STATE-ALPHA-CODE := RS.TIRCNTL-STATE-ALPHA-CODE
                            us_Tircntl_State_Full_Name.setValue(rs_Tircntl_State_Full_Name);                                                                              //Natural: ASSIGN US.TIRCNTL-STATE-FULL-NAME := RS.TIRCNTL-STATE-FULL-NAME
                            us_Tircntl_State_Tax_Id_Tiaa.setValue(rs_Tircntl_State_Tax_Id_Tiaa);                                                                          //Natural: ASSIGN US.TIRCNTL-STATE-TAX-ID-TIAA := RS.TIRCNTL-STATE-TAX-ID-TIAA
                            us_Tircntl_State_Tax_Id_Cref.setValue(rs_Tircntl_State_Tax_Id_Cref);                                                                          //Natural: ASSIGN US.TIRCNTL-STATE-TAX-ID-CREF := RS.TIRCNTL-STATE-TAX-ID-CREF
                            us_Tircntl_State_Tax_Id_Life.setValue(rs_Tircntl_State_Tax_Id_Life);                                                                          //Natural: ASSIGN US.TIRCNTL-STATE-TAX-ID-LIFE := RS.TIRCNTL-STATE-TAX-ID-LIFE
                            us_Tircntl_State_Tax_Id_Tcii.setValue(rs_Tircntl_State_Tax_Id_Tcii);                                                                          //Natural: ASSIGN US.TIRCNTL-STATE-TAX-ID-TCII := RS.TIRCNTL-STATE-TAX-ID-TCII
                            us_Tircntl_State_Tax_Id_Trust.setValue(rs_Tircntl_State_Tax_Id_Trust);                                                                        //Natural: ASSIGN US.TIRCNTL-STATE-TAX-ID-TRUST := RS.TIRCNTL-STATE-TAX-ID-TRUST
                            us_Tircntl_State_Tax_Id_Fsb.setValue(rs_Tircntl_State_Tax_Id_Fsb);                                                                            //Natural: ASSIGN US.TIRCNTL-STATE-TAX-ID-FSB := RS.TIRCNTL-STATE-TAX-ID-FSB
                            //*                                     JB1011 ABOVE LINE
                            us_Tircntl_Comb_Fed_Ind.setValue(rs_Tircntl_Comb_Fed_Ind);                                                                                    //Natural: ASSIGN US.TIRCNTL-COMB-FED-IND := RS.TIRCNTL-COMB-FED-IND
                            us_Tircntl_Comb_Fed_Updte_Dte.setValue(rs_Tircntl_Comb_Fed_Updte_Dte);                                                                        //Natural: ASSIGN US.TIRCNTL-COMB-FED-UPDTE-DTE := RS.TIRCNTL-COMB-FED-UPDTE-DTE
                            us_Tircntl_State_Ind.setValue(rs_Tircntl_State_Ind);                                                                                          //Natural: ASSIGN US.TIRCNTL-STATE-IND := RS.TIRCNTL-STATE-IND
                            us_Tircntl_Media_Code.setValue(rs_Tircntl_Media_Code);                                                                                        //Natural: ASSIGN US.TIRCNTL-MEDIA-CODE := RS.TIRCNTL-MEDIA-CODE
                            us_Tircntl_State_Whhld_Ind.setValue(rs_Tircntl_State_Whhld_Ind);                                                                              //Natural: ASSIGN US.TIRCNTL-STATE-WHHLD-IND := RS.TIRCNTL-STATE-WHHLD-IND
                            us_Tircntl_State_Updte_Dte.setValue(rs_Tircntl_State_Updte_Dte);                                                                              //Natural: ASSIGN US.TIRCNTL-STATE-UPDTE-DTE := RS.TIRCNTL-STATE-UPDTE-DTE
                            us_Tircntl_State_Entry_Dte.setValue(rs_Tircntl_State_Entry_Dte);                                                                              //Natural: ASSIGN US.TIRCNTL-STATE-ENTRY-DTE := RS.TIRCNTL-STATE-ENTRY-DTE
                            us_Tircntl_State_Comment.setValue(rs_Tircntl_State_Comment);                                                                                  //Natural: ASSIGN US.TIRCNTL-STATE-COMMENT := RS.TIRCNTL-STATE-COMMENT
                            us_Tircntl_State_Updte_User.setValue(rs_Tircntl_State_Updte_User);                                                                            //Natural: ASSIGN US.TIRCNTL-STATE-UPDTE-USER := RS.TIRCNTL-STATE-UPDTE-USER
                            us_Tircntl_Fed_Ind.setValue(rs_Tircntl_Fed_Ind);                                                                                              //Natural: ASSIGN US.TIRCNTL-FED-IND := RS.TIRCNTL-FED-IND
                            us_Tircntl_Corr_Comb_Fed_Ind.setValue(rs_Tircntl_Corr_Comb_Fed_Ind);                                                                          //Natural: ASSIGN US.TIRCNTL-CORR-COMB-FED-IND := RS.TIRCNTL-CORR-COMB-FED-IND
                            us_Tircntl_Corr_Fed_Ind.setValue(rs_Tircntl_Corr_Fed_Ind);                                                                                    //Natural: ASSIGN US.TIRCNTL-CORR-FED-IND := RS.TIRCNTL-CORR-FED-IND
                            us_Tircntl_Corr_Media_Code.setValue(rs_Tircntl_Corr_Media_Code);                                                                              //Natural: ASSIGN US.TIRCNTL-CORR-MEDIA-CODE := RS.TIRCNTL-CORR-MEDIA-CODE
                            us_Tircntl_Corr_State_Ind.setValue(rs_Tircntl_Corr_State_Ind);                                                                                //Natural: ASSIGN US.TIRCNTL-CORR-STATE-IND := RS.TIRCNTL-CORR-STATE-IND
                            us_Tircntl_Threshold_Amt.setValue(rs_Tircntl_Threshold_Amt);                                                                                  //Natural: ASSIGN US.TIRCNTL-THRESHOLD-AMT := RS.TIRCNTL-THRESHOLD-AMT
                            vw_us.updateDBRow("G1");                                                                                                                      //Natural: UPDATE ( G1. )
                            getCurrentProcessState().getDbConv().dbCommit();                                                                                              //Natural: END TRANSACTION
                            pnd_Updt_Ctr.nadd(1);                                                                                                                         //Natural: ADD 1 TO #UPDT-CTR
                        }                                                                                                                                                 //Natural: END-IF
                        //*  (RL2.)
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_New_Record_Found.equals(false)))                                                                                                    //Natural: IF #NEW-RECORD-FOUND = FALSE
                    {
                        ss_Tircntl_Tbl_Nbr.setValue(rs_Tircntl_Tbl_Nbr);                                                                                                  //Natural: ASSIGN SS.TIRCNTL-TBL-NBR := RS.TIRCNTL-TBL-NBR
                        ss_Tircntl_Tax_Year.setValue(pnd_New_Tax_Year_Pnd_New_Tax_Year_N);                                                                                //Natural: ASSIGN SS.TIRCNTL-TAX-YEAR := #NEW-TAX-YEAR-N
                        ss_Tircntl_Seq_Nbr.setValue(rs_Tircntl_Seq_Nbr);                                                                                                  //Natural: ASSIGN SS.TIRCNTL-SEQ-NBR := RS.TIRCNTL-SEQ-NBR
                        ss_Tircntl_State_Old_Code.setValue(rs_Tircntl_State_Old_Code);                                                                                    //Natural: ASSIGN SS.TIRCNTL-STATE-OLD-CODE := RS.TIRCNTL-STATE-OLD-CODE
                        ss_Tircntl_State_Alpha_Code.setValue(rs_Tircntl_State_Alpha_Code);                                                                                //Natural: ASSIGN SS.TIRCNTL-STATE-ALPHA-CODE := RS.TIRCNTL-STATE-ALPHA-CODE
                        ss_Tircntl_State_Full_Name.setValue(rs_Tircntl_State_Full_Name);                                                                                  //Natural: ASSIGN SS.TIRCNTL-STATE-FULL-NAME := RS.TIRCNTL-STATE-FULL-NAME
                        ss_Tircntl_State_Tax_Id_Tiaa.setValue(rs_Tircntl_State_Tax_Id_Tiaa);                                                                              //Natural: ASSIGN SS.TIRCNTL-STATE-TAX-ID-TIAA := RS.TIRCNTL-STATE-TAX-ID-TIAA
                        ss_Tircntl_State_Tax_Id_Cref.setValue(rs_Tircntl_State_Tax_Id_Cref);                                                                              //Natural: ASSIGN SS.TIRCNTL-STATE-TAX-ID-CREF := RS.TIRCNTL-STATE-TAX-ID-CREF
                        ss_Tircntl_State_Tax_Id_Life.setValue(rs_Tircntl_State_Tax_Id_Life);                                                                              //Natural: ASSIGN SS.TIRCNTL-STATE-TAX-ID-LIFE := RS.TIRCNTL-STATE-TAX-ID-LIFE
                        ss_Tircntl_State_Tax_Id_Tcii.setValue(rs_Tircntl_State_Tax_Id_Tcii);                                                                              //Natural: ASSIGN SS.TIRCNTL-STATE-TAX-ID-TCII := RS.TIRCNTL-STATE-TAX-ID-TCII
                        ss_Tircntl_State_Tax_Id_Trust.setValue(rs_Tircntl_State_Tax_Id_Trust);                                                                            //Natural: ASSIGN SS.TIRCNTL-STATE-TAX-ID-TRUST := RS.TIRCNTL-STATE-TAX-ID-TRUST
                        ss_Tircntl_State_Tax_Id_Fsb.setValue(rs_Tircntl_State_Tax_Id_Fsb);                                                                                //Natural: ASSIGN SS.TIRCNTL-STATE-TAX-ID-FSB := RS.TIRCNTL-STATE-TAX-ID-FSB
                        //*                               JB1011  ABOVE LINE
                        ss_Tircntl_Comb_Fed_Ind.setValue(rs_Tircntl_Comb_Fed_Ind);                                                                                        //Natural: ASSIGN SS.TIRCNTL-COMB-FED-IND := RS.TIRCNTL-COMB-FED-IND
                        ss_Tircntl_Comb_Fed_Updte_Dte.setValue(rs_Tircntl_Comb_Fed_Updte_Dte);                                                                            //Natural: ASSIGN SS.TIRCNTL-COMB-FED-UPDTE-DTE := RS.TIRCNTL-COMB-FED-UPDTE-DTE
                        ss_Tircntl_State_Ind.setValue(rs_Tircntl_State_Ind);                                                                                              //Natural: ASSIGN SS.TIRCNTL-STATE-IND := RS.TIRCNTL-STATE-IND
                        ss_Tircntl_Media_Code.setValue(rs_Tircntl_Media_Code);                                                                                            //Natural: ASSIGN SS.TIRCNTL-MEDIA-CODE := RS.TIRCNTL-MEDIA-CODE
                        ss_Tircntl_State_Whhld_Ind.setValue(rs_Tircntl_State_Whhld_Ind);                                                                                  //Natural: ASSIGN SS.TIRCNTL-STATE-WHHLD-IND := RS.TIRCNTL-STATE-WHHLD-IND
                        ss_Tircntl_State_Updte_Dte.setValue(rs_Tircntl_State_Updte_Dte);                                                                                  //Natural: ASSIGN SS.TIRCNTL-STATE-UPDTE-DTE := RS.TIRCNTL-STATE-UPDTE-DTE
                        ss_Tircntl_State_Entry_Dte.setValue(rs_Tircntl_State_Entry_Dte);                                                                                  //Natural: ASSIGN SS.TIRCNTL-STATE-ENTRY-DTE := RS.TIRCNTL-STATE-ENTRY-DTE
                        ss_Tircntl_State_Comment.setValue(rs_Tircntl_State_Comment);                                                                                      //Natural: ASSIGN SS.TIRCNTL-STATE-COMMENT := RS.TIRCNTL-STATE-COMMENT
                        ss_Tircntl_State_Updte_User.setValue(rs_Tircntl_State_Updte_User);                                                                                //Natural: ASSIGN SS.TIRCNTL-STATE-UPDTE-USER := RS.TIRCNTL-STATE-UPDTE-USER
                        ss_Tircntl_Fed_Ind.setValue(rs_Tircntl_Fed_Ind);                                                                                                  //Natural: ASSIGN SS.TIRCNTL-FED-IND := RS.TIRCNTL-FED-IND
                        ss_Tircntl_Corr_Comb_Fed_Ind.setValue(rs_Tircntl_Corr_Comb_Fed_Ind);                                                                              //Natural: ASSIGN SS.TIRCNTL-CORR-COMB-FED-IND := RS.TIRCNTL-CORR-COMB-FED-IND
                        ss_Tircntl_Corr_Fed_Ind.setValue(rs_Tircntl_Corr_Fed_Ind);                                                                                        //Natural: ASSIGN SS.TIRCNTL-CORR-FED-IND := RS.TIRCNTL-CORR-FED-IND
                        ss_Tircntl_Corr_Media_Code.setValue(rs_Tircntl_Corr_Media_Code);                                                                                  //Natural: ASSIGN SS.TIRCNTL-CORR-MEDIA-CODE := RS.TIRCNTL-CORR-MEDIA-CODE
                        ss_Tircntl_Corr_State_Ind.setValue(rs_Tircntl_Corr_State_Ind);                                                                                    //Natural: ASSIGN SS.TIRCNTL-CORR-STATE-IND := RS.TIRCNTL-CORR-STATE-IND
                        ss_Tircntl_Threshold_Amt.setValue(rs_Tircntl_Threshold_Amt);                                                                                      //Natural: ASSIGN SS.TIRCNTL-THRESHOLD-AMT := RS.TIRCNTL-THRESHOLD-AMT
                        vw_ss.insertDBRow();                                                                                                                              //Natural: STORE SS
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                        pnd_Store_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STORE-CTR
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (RL1.)
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                pnd_Super1_Pnd_S1_Tbl_Nbr.setValue(2);                                                                                                                    //Natural: ASSIGN #S1-TBL-NBR := 2
                pnd_Super1_Pnd_S1_Tax_Year.setValue(0);                                                                                                                   //Natural: ASSIGN #S1-TAX-YEAR := 0
                pnd_Super1_Pnd_S1_Old_Code.setValue(" ");                                                                                                                 //Natural: ASSIGN #S1-OLD-CODE := ' '
                vw_rs.startDatabaseRead                                                                                                                                   //Natural: READ RS WITH TIRCNTL-NBR-YEAR-OLD-STATE-CDE = #SUPER1
                (
                "RL3",
                new Wc[] { new Wc("TIRCNTL_NBR_YEAR_OLD_STATE_CDE", ">=", pnd_Super1, WcType.BY) },
                new Oc[] { new Oc("TIRCNTL_NBR_YEAR_OLD_STATE_CDE", "ASC") }
                );
                RL3:
                while (condition(vw_rs.readNextRow("RL3")))
                {
                    if (condition(rs_Tircntl_Tbl_Nbr.equals(2)))                                                                                                          //Natural: IF RS.TIRCNTL-TBL-NBR = 2
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),rs_Tircntl_Tbl_Nbr,new TabSetting(4),rs_Tircntl_Tax_Year,new          //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T RS.TIRCNTL-TBL-NBR 04T RS.TIRCNTL-TAX-YEAR 10T RS.TIRCNTL-SEQ-NBR ( EM = 999 SG = OFF ) 14T RS.TIRCNTL-STATE-OLD-CODE 20T RS.TIRCNTL-STATE-ALPHA-CODE 25T RS.TIRCNTL-STATE-FULL-NAME 45T RS.TIRCNTL-STATE-TAX-ID-TIAA 68T RS.TIRCNTL-COMB-FED-IND 72T RS.TIRCNTL-COMB-FED-UPDTE-DTE 85T RS.TIRCNTL-STATE-IND 92T RS.TIRCNTL-MEDIA-CODE 99T RS.TIRCNTL-STATE-WHHLD-IND 103T RS.TIRCNTL-STATE-ENTRY-DTE 113T RS.TIRCNTL-STATE-UPDTE-DTE 124T RS.TIRCNTL-STATE-UPDTE-USER
                        TabSetting(10),rs_Tircntl_Seq_Nbr, new ReportEditMask ("999"), new SignPosition (false),new TabSetting(14),rs_Tircntl_State_Old_Code,new 
                        TabSetting(20),rs_Tircntl_State_Alpha_Code,new TabSetting(25),rs_Tircntl_State_Full_Name,new TabSetting(45),rs_Tircntl_State_Tax_Id_Tiaa,new 
                        TabSetting(68),rs_Tircntl_Comb_Fed_Ind,new TabSetting(72),rs_Tircntl_Comb_Fed_Updte_Dte,new TabSetting(85),rs_Tircntl_State_Ind,new 
                        TabSetting(92),rs_Tircntl_Media_Code,new TabSetting(99),rs_Tircntl_State_Whhld_Ind,new TabSetting(103),rs_Tircntl_State_Entry_Dte,new 
                        TabSetting(113),rs_Tircntl_State_Updte_Dte,new TabSetting(124),rs_Tircntl_State_Updte_User);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(16),"Threshold:",rs_Tircntl_Threshold_Amt, new ReportEditMask            //Natural: WRITE ( 01 ) NOTITLE NOHDR 16T 'Threshold:' RS.TIRCNTL-THRESHOLD-AMT ( EM = Z,ZZ9.99 ) 45T RS.TIRCNTL-STATE-TAX-ID-CREF 'Fed:' RS.TIRCNTL-FED-IND 1X 'Corr Fed:' RS.TIRCNTL-CORR-FED-IND 1X 'Corr Comb Fed:' RS.TIRCNTL-CORR-COMB-FED-IND 1X 'Corr Media:' RS.TIRCNTL-CORR-MEDIA-CODE 1X 'Corr State:' RS.TIRCNTL-CORR-STATE-IND
                        ("Z,ZZ9.99"),new TabSetting(45),rs_Tircntl_State_Tax_Id_Cref,"Fed:",rs_Tircntl_Fed_Ind,new ColumnSpacing(1),"Corr Fed:",rs_Tircntl_Corr_Fed_Ind,new 
                        ColumnSpacing(1),"Corr Comb Fed:",rs_Tircntl_Corr_Comb_Fed_Ind,new ColumnSpacing(1),"Corr Media:",rs_Tircntl_Corr_Media_Code,new 
                        ColumnSpacing(1),"Corr State:",rs_Tircntl_Corr_State_Ind);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(rs_Tircntl_State_Tax_Id_Trust.equals(" ")))                                                                                             //Natural: IF RS.TIRCNTL-STATE-TAX-ID-TRUST = ' '
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(45),rs_Tircntl_State_Tax_Id_Trust);                                  //Natural: WRITE ( 01 ) NOTITLE NOHDR 45T RS.TIRCNTL-STATE-TAX-ID-TRUST
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RL3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RL3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(rs_Tircntl_State_Tax_Id_Life.equals(" ")))                                                                                              //Natural: IF RS.TIRCNTL-STATE-TAX-ID-LIFE = ' '
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(45),rs_Tircntl_State_Tax_Id_Life);                                   //Natural: WRITE ( 01 ) NOTITLE NOHDR 45T RS.TIRCNTL-STATE-TAX-ID-LIFE
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RL3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RL3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(rs_Tircntl_State_Tax_Id_Tcii.equals(" ")))                                                                                              //Natural: IF RS.TIRCNTL-STATE-TAX-ID-TCII = ' '
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(45),rs_Tircntl_State_Tax_Id_Tcii);                                   //Natural: WRITE ( 01 ) NOTITLE NOHDR 45T RS.TIRCNTL-STATE-TAX-ID-TCII
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RL3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RL3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  JB1011 NEXT 6 LINES
                    if (condition(rs_Tircntl_State_Tax_Id_Fsb.equals(" ")))                                                                                               //Natural: IF RS.TIRCNTL-STATE-TAX-ID-FSB = ' '
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(45),rs_Tircntl_State_Tax_Id_Fsb);                                    //Natural: WRITE ( 01 ) NOTITLE NOHDR 45T RS.TIRCNTL-STATE-TAX-ID-FSB
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RL3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RL3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (RL3.)
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getReports().skip(0, 4);                                                                                                                                  //Natural: SKIP ( 00 ) 4
                getReports().print(0, "Old Tax Year Records Read................",pnd_Read1_Ctr);                                                                         //Natural: PRINT ( 00 ) 'Old Tax Year Records Read................' #READ1-CTR
                getReports().print(0, "New Tax Year Records Found...............",pnd_Read2_Ctr);                                                                         //Natural: PRINT ( 00 ) 'New Tax Year Records Found...............' #READ2-CTR
                getReports().print(0, "New Tax Year Records Updated.............",pnd_Updt_Ctr);                                                                          //Natural: PRINT ( 00 ) 'New Tax Year Records Updated.............' #UPDT-CTR
                getReports().print(0, "New Tax Year Records Stored..............",pnd_Store_Ctr);                                                                         //Natural: PRINT ( 00 ) 'New Tax Year Records Stored..............' #STORE-CTR
                getReports().skip(1, 4);                                                                                                                                  //Natural: SKIP ( 01 ) 4
                getReports().print(1, "Old Tax Year Records Read................",pnd_Read1_Ctr);                                                                         //Natural: PRINT ( 01 ) 'Old Tax Year Records Read................' #READ1-CTR
                getReports().print(1, "New Tax Year Records Found...............",pnd_Read2_Ctr);                                                                         //Natural: PRINT ( 01 ) 'New Tax Year Records Found...............' #READ2-CTR
                getReports().print(1, "New Tax Year Records Updated.............",pnd_Updt_Ctr);                                                                          //Natural: PRINT ( 01 ) 'New Tax Year Records Updated.............' #UPDT-CTR
                getReports().print(1, "New Tax Year Records Stored..............",pnd_Store_Ctr);                                                                         //Natural: PRINT ( 01 ) 'New Tax Year Records Stored..............' #STORE-CTR
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( 01 )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(54),"State Code Table Report",new             //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 54T 'State Code Table Report' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                    getReports().skip(1, 2);                                                                                                                              //Natural: SKIP ( 01 ) 2 LINES
                    //*  JB1011
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(45)," TIAA / CREF  ");                                                                      //Natural: WRITE ( 01 ) NOTITLE 45T ' TIAA / CREF  '
                    //*  JB1011
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(45)," TRST / LIFE  ");                                                                      //Natural: WRITE ( 01 ) NOTITLE 45T ' TRST / LIFE  '
                    //*  WRITE (01) NOTITLE                    /* JB1011
                    //*    45T ' TRST TAX ID  '
                    //*  JB1011
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"Tbl",new TabSetting(5),"Tax ",new TabSetting(10),"Seq",new TabSetting(14),"Old ",new    //Natural: WRITE ( 01 ) NOTITLE 01T 'Tbl' 05T 'Tax ' 10T 'Seq' 14T 'Old ' 19T 'Alpha' 25T '                   ' 45T ' TCII / FSB   ' 67T 'Comb' 73T 'Comb Ind' 83T 'State' 90T 'Media' 97T 'State' 104T ' State  ' 114T ' State  ' 124T ' Update '
                        TabSetting(19),"Alpha",new TabSetting(25),"                   ",new TabSetting(45)," TCII / FSB   ",new TabSetting(67),"Comb",new 
                        TabSetting(73),"Comb Ind",new TabSetting(83),"State",new TabSetting(90),"Media",new TabSetting(97),"State",new TabSetting(104)," State  ",new 
                        TabSetting(114)," State  ",new TabSetting(124)," Update ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"Nbr",new TabSetting(5),"Year",new TabSetting(10),"No.",new TabSetting(14),"Code",new    //Natural: WRITE ( 01 ) NOTITLE 01T 'Nbr' 05T 'Year' 10T 'No.' 14T 'Code' 19T 'Code ' 25T 'State Full Name    ' 45T ' Tax IDs ' 67T 'Ind ' 73T 'Fed. Ind' 83T ' Ind ' 90T 'Code ' 97T 'Whhld' 104T 'Entr Dte' 114T 'Updt Dte' 124T '  User  '
                        TabSetting(19),"Code ",new TabSetting(25),"State Full Name    ",new TabSetting(45)," Tax IDs ",new TabSetting(67),"Ind ",new TabSetting(73),"Fed. Ind",new 
                        TabSetting(83)," Ind ",new TabSetting(90),"Code ",new TabSetting(97),"Whhld",new TabSetting(104),"Entr Dte",new TabSetting(114),"Updt Dte",new 
                        TabSetting(124),"  User  ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"===",new TabSetting(5),"====",new TabSetting(10),"===",new TabSetting(14),"====",new    //Natural: WRITE ( 01 ) NOTITLE 01T '===' 05T '====' 10T '===' 14T '====' 19T '=====' 25T '===================' 45T '====================' 67T '====' 73T '========' 83T '=====' 90T '=====' 97T '=====' 104T '========' 114T '========' 124T '========'
                        TabSetting(19),"=====",new TabSetting(25),"===================",new TabSetting(45),"====================",new TabSetting(67),"====",new 
                        TabSetting(73),"========",new TabSetting(83),"=====",new TabSetting(90),"=====",new TabSetting(97),"=====",new TabSetting(104),"========",new 
                        TabSetting(114),"========",new TabSetting(124),"========");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");

        getReports().setDisplayColumns(0, pnd_Old_Tax_Year,pnd_New_Tax_Year);
    }
}
