/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:33:23 PM
**        * FROM NATURAL PROGRAM : Twrp1900
************************************************************
**        * FILE NAME            : Twrp1900.java
**        * CLASS NAME           : Twrp1900
**        * INSTANCE NAME        : Twrp1900
************************************************************
********************************************************************
* PROGRAM : TWRP1900
* FUNCTION: CREATES A FLAT FILE1  FOR A GIVEN TAX YEAR
* INPUT   : FILE 96 (TWRP-IRA) CONTRIBUTION FILE
* OUTPUT  : FLAT FILE1 AND CONTROL TOTAL
* AUTHOR  : EDITH  7/21/99
* UPDATES : 01/08/2000 EDITH
*           SET PRIOR TAX YEAR AND CREATE FLAT-FILE CONTAINING
*              TAX YEAR AND PROCESSING PERIOD (CREATE-DATE) TO BE
*              USED BY PRINTING PROGRAMS.
*
* 06/19/07  J.ROTHOLZ
*           ADDED DATAWAREHOUSE INTERFACE FILE OUTPUT
*           CHANGED READ TO USE S1 SUPERDESCRIPTOR
* 05/19/09  (M001) F.TWAHIR - ADD FIELDS TO DW FILE
* 10/19/2017 5498 BOX ADDITION RESTOW ONLY
* 11/12/2020 5498 TAX YEAR 2020 BOX CHABGES - RESTOW ONLY.
********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp1900 extends BLNatBase
{
    // Data Areas
    private LdaTwrl1900 ldaTwrl1900;
    private LdaTwrl1905 ldaTwrl1905;
    private LdaTwrl190a ldaTwrl190a;
    private LdaTwrl0600 ldaTwrl0600;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Rec_Ritten;
    private DbsField pnd_Rec_Read;
    private DbsField pnd_Cref_Cnt;
    private DbsField pnd_Tiaa_Cnt;
    private DbsField pnd_Life_Cnt;
    private DbsField pnd_Othr_Cnt;
    private DbsField pnd_Cnt;
    private DbsField pnd_I_Cnt;
    private DbsField pnd_A_Cnt;
    private DbsField pnd_Skip_Cnt;
    private DbsField pnd_Sign_Sw_Cnt;
    private DbsField pnd_All_Zero_Cnt;

    private DbsGroup pnd_Hold_Key_Values;
    private DbsField pnd_Hold_Key_Values_Pnd_H_Tax_Year;
    private DbsField pnd_Hold_Key_Values_Pnd_H_Tax_Id;
    private DbsField pnd_Hold_Key_Values_Pnd_H_Contract;
    private DbsField pnd_Hold_Key_Values_Pnd_H_Payee;
    private DbsField pnd_Skip_The_Rest;
    private DbsField pnd_T_Date;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl1900 = new LdaTwrl1900();
        registerRecord(ldaTwrl1900);
        registerRecord(ldaTwrl1900.getVw_f96_View());
        ldaTwrl1905 = new LdaTwrl1905();
        registerRecord(ldaTwrl1905);
        ldaTwrl190a = new LdaTwrl190a();
        registerRecord(ldaTwrl190a);
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Rec_Ritten = localVariables.newFieldInRecord("pnd_Rec_Ritten", "#REC-RITTEN", FieldType.NUMERIC, 9);
        pnd_Rec_Read = localVariables.newFieldInRecord("pnd_Rec_Read", "#REC-READ", FieldType.NUMERIC, 9);
        pnd_Cref_Cnt = localVariables.newFieldArrayInRecord("pnd_Cref_Cnt", "#CREF-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Tiaa_Cnt = localVariables.newFieldArrayInRecord("pnd_Tiaa_Cnt", "#TIAA-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Life_Cnt = localVariables.newFieldArrayInRecord("pnd_Life_Cnt", "#LIFE-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Othr_Cnt = localVariables.newFieldArrayInRecord("pnd_Othr_Cnt", "#OTHR-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 1);
        pnd_I_Cnt = localVariables.newFieldInRecord("pnd_I_Cnt", "#I-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_A_Cnt = localVariables.newFieldInRecord("pnd_A_Cnt", "#A-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Skip_Cnt = localVariables.newFieldInRecord("pnd_Skip_Cnt", "#SKIP-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Sign_Sw_Cnt = localVariables.newFieldInRecord("pnd_Sign_Sw_Cnt", "#SIGN-SW-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_All_Zero_Cnt = localVariables.newFieldInRecord("pnd_All_Zero_Cnt", "#ALL-ZERO-CNT", FieldType.PACKED_DECIMAL, 7);

        pnd_Hold_Key_Values = localVariables.newGroupInRecord("pnd_Hold_Key_Values", "#HOLD-KEY-VALUES");
        pnd_Hold_Key_Values_Pnd_H_Tax_Year = pnd_Hold_Key_Values.newFieldInGroup("pnd_Hold_Key_Values_Pnd_H_Tax_Year", "#H-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Hold_Key_Values_Pnd_H_Tax_Id = pnd_Hold_Key_Values.newFieldInGroup("pnd_Hold_Key_Values_Pnd_H_Tax_Id", "#H-TAX-ID", FieldType.STRING, 10);
        pnd_Hold_Key_Values_Pnd_H_Contract = pnd_Hold_Key_Values.newFieldInGroup("pnd_Hold_Key_Values_Pnd_H_Contract", "#H-CONTRACT", FieldType.STRING, 
            8);
        pnd_Hold_Key_Values_Pnd_H_Payee = pnd_Hold_Key_Values.newFieldInGroup("pnd_Hold_Key_Values_Pnd_H_Payee", "#H-PAYEE", FieldType.STRING, 2);
        pnd_Skip_The_Rest = localVariables.newFieldInRecord("pnd_Skip_The_Rest", "#SKIP-THE-REST", FieldType.BOOLEAN, 1);
        pnd_T_Date = localVariables.newFieldInRecord("pnd_T_Date", "#T-DATE", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl1900.initializeValues();
        ldaTwrl1905.initializeValues();
        ldaTwrl190a.initializeValues();
        ldaTwrl0600.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp1900() throws Exception
    {
        super("Twrp1900");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        getWorkFiles().read(1, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                             //Natural: READ WORK FILE 01 ONCE #TWRP0600-CONTROL-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            getReports().write(0, " ************************************ ",NEWLINE," ***                              *** ",NEWLINE," ***    CONTROL RECORD IS EMPTY   *** ", //Natural: WRITE ' ************************************ ' / ' ***                              *** ' / ' ***    CONTROL RECORD IS EMPTY   *** ' / ' ***    PLEASE INFORM SYSTEMS !!  *** ' / ' ***                              *** ' / ' ************************************ '
                NEWLINE," ***    PLEASE INFORM SYSTEMS !!  *** ",NEWLINE," ***                              *** ",NEWLINE," ************************************ ");
            if (Global.isEscape()) return;
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-ENDFILE
        ldaTwrl1900.getVw_f96_View().startDatabaseRead                                                                                                                    //Natural: READ F96-VIEW BY TWRC-S1-YR-TIN-CONTRACT = #TWRP0600-TAX-YEAR-CCYY
        (
        "READ01",
        new Wc[] { new Wc("TWRC_S1_YR_TIN_CONTRACT", ">=", ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy(), WcType.BY) },
        new Oc[] { new Oc("TWRC_S1_YR_TIN_CONTRACT", "ASC") }
        );
        READ01:
        while (condition(ldaTwrl1900.getVw_f96_View().readNextRow("READ01")))
        {
            if (condition(ldaTwrl1900.getF96_View_Twrc_Tax_Year().notEquals(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy())))                    //Natural: IF F96-VIEW.TWRC-TAX-YEAR NE #TWRP0600-TAX-YEAR-CCYY
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Rec_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-READ
            pnd_Cnt.setValue(1);                                                                                                                                          //Natural: ASSIGN #CNT := 1
                                                                                                                                                                          //Natural: PERFORM COMPANY-CNT
            sub_Company_Cnt();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaTwrl1900.getF96_View_Twrc_Source().equals("ML ")))                                                                                           //Natural: IF F96-VIEW.TWRC-SOURCE = 'ML '
            {
                ldaTwrl1900.getF96_View_Twrc_Source().setValue("ZZ ");                                                                                                    //Natural: ASSIGN F96-VIEW.TWRC-SOURCE := 'ZZ '
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl190a.getPnd_F96_Ff1_Pnd_Twrc_Orig_Sc().setValue(ldaTwrl1900.getF96_View_Twrc_Source());                                                                //Natural: ASSIGN #TWRC-ORIG-SC := F96-VIEW.TWRC-SOURCE
            ldaTwrl190a.getPnd_F96_Ff1_Pnd_Twrc_Updt_Sc().setValue(ldaTwrl1900.getF96_View_Twrc_Update_Source());                                                         //Natural: ASSIGN #TWRC-UPDT-SC := F96-VIEW.TWRC-UPDATE-SOURCE
            ldaTwrl190a.getPnd_F96_Ff1().setValuesByName(ldaTwrl1900.getVw_f96_View());                                                                                   //Natural: MOVE BY NAME F96-VIEW TO #F96-FF1
            getWorkFiles().write(2, false, ldaTwrl190a.getPnd_F96_Ff1());                                                                                                 //Natural: WRITE WORK FILE 02 #F96-FF1
            ldaTwrl190a.getPnd_F96_Ff1().reset();                                                                                                                         //Natural: RESET #F96-FF1
                                                                                                                                                                          //Natural: PERFORM DATA-WAREHOUSE-CONTRIBUTIONS
            sub_Data_Warehouse_Contributions();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Rec_Ritten.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #REC-RITTEN
            pnd_Cnt.setValue(2);                                                                                                                                          //Natural: ASSIGN #CNT := 2
                                                                                                                                                                          //Natural: PERFORM COMPANY-CNT
            sub_Company_Cnt();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: AT END OF DATA;//Natural: END-READ
        if (condition(ldaTwrl1900.getVw_f96_View().getAtEndOfData()))
        {
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getPROGRAM(),new TabSetting(34),"TAX WITHHOLDING AND REPORTING SYSTEM",new       //Natural: WRITE ( 1 ) NOTITLE NOHDR / *PROGRAM 34T 'TAX WITHHOLDING AND REPORTING SYSTEM' 119T 'PAGE' *PAGE-NUMBER ( 1 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 37T 'EXTRACTED CONTRIBUTION RECORDS' / 'RUNTIME : ' *TIMX 44T 'TAX YEAR ' #TWRP0600-TAX-YEAR-CCYY / 44T 'CONTROL TOTAL' // 'TOTAL RECORDS READ         : ' #REC-READ / '  1. CREF RECORDS          : ' #CREF-CNT ( 1 ) / '  2. TIAA RECORDS          : ' #TIAA-CNT ( 1 ) / '  3. TIAA-LIFE RECORDS     : ' #LIFE-CNT ( 1 ) / '  4. NON-CREF/TIAA RECORDS : ' #OTHR-CNT ( 1 ) /// 'TOTAL RECORDS WRITTEN      : ' #REC-RITTEN / '  1. CREF RECORDS          : ' #CREF-CNT ( 2 ) / '  2. TIAA RECORDS          : ' #TIAA-CNT ( 2 ) / '  3. TIAA-LIFE RECORDS     : ' #LIFE-CNT ( 2 ) / '  4. NON-CREF/TIAA RECORDS : ' #OTHR-CNT ( 2 )
                TabSetting(119),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new TabSetting(37),"EXTRACTED CONTRIBUTION RECORDS",NEWLINE,"RUNTIME : ",Global.getTIMX(),new 
                TabSetting(44),"TAX YEAR ",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy(),NEWLINE,new TabSetting(44),"CONTROL TOTAL",
                NEWLINE,NEWLINE,"TOTAL RECORDS READ         : ",pnd_Rec_Read,NEWLINE,"  1. CREF RECORDS          : ",pnd_Cref_Cnt.getValue(1),NEWLINE,"  2. TIAA RECORDS          : ",
                pnd_Tiaa_Cnt.getValue(1),NEWLINE,"  3. TIAA-LIFE RECORDS     : ",pnd_Life_Cnt.getValue(1),NEWLINE,"  4. NON-CREF/TIAA RECORDS : ",pnd_Othr_Cnt.getValue(1),
                NEWLINE,NEWLINE,NEWLINE,"TOTAL RECORDS WRITTEN      : ",pnd_Rec_Ritten,NEWLINE,"  1. CREF RECORDS          : ",pnd_Cref_Cnt.getValue(2),
                NEWLINE,"  2. TIAA RECORDS          : ",pnd_Tiaa_Cnt.getValue(2),NEWLINE,"  3. TIAA-LIFE RECORDS     : ",pnd_Life_Cnt.getValue(2),NEWLINE,
                "  4. NON-CREF/TIAA RECORDS : ",pnd_Othr_Cnt.getValue(2));
            if (condition(Global.isEscape())) return;
            getReports().write(0, "TAX-YEAR READ FROM CONTROL RECORD : ",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy(),NEWLINE,"TAX-YEAR USED IN THIS RUN       : ",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy(),NEWLINE,"From Date READ FROM CONTROL REC.: ",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd(),  //Natural: WRITE 'TAX-YEAR READ FROM CONTROL RECORD : ' #TWRP0600-TAX-YEAR-CCYY / 'TAX-YEAR USED IN THIS RUN       : ' #TWRP0600-TAX-YEAR-CCYY / 'From Date READ FROM CONTROL REC.: ' #TWRP0600-FROM-CCYYMMDD ( EM = XXXX/XX/XX ) / 'To   Date READ FROM CONTROL REC.: ' #TWRP0600-TO-CCYYMMDD ( EM = XXXX/XX/XX )
                new ReportEditMask ("XXXX/XX/XX"),NEWLINE,"To   Date READ FROM CONTROL REC.: ",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd(), 
                new ReportEditMask ("XXXX/XX/XX"));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,NEWLINE,NEWLINE,"TOTAL ACTIVE         :",pnd_A_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"TOTAL INACTIVE       :",pnd_I_Cnt,    //Natural: WRITE ( 0 ) /// 'TOTAL ACTIVE         :' #A-CNT / 'TOTAL INACTIVE       :' #I-CNT / 'TOTAL INACTIVE * -1  :' #SIGN-SW-CNT / 'TOTAL SKIPPED        :' #SKIP-CNT / 'TOTAL ALL ZEROS      :' #ALL-ZERO-CNT /
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"TOTAL INACTIVE * -1  :",pnd_Sign_Sw_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"TOTAL SKIPPED        :",pnd_Skip_Cnt, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"TOTAL ALL ZEROS      :",pnd_All_Zero_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE);
        if (Global.isEscape()) return;
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPANY-CNT
        //* *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DATA-WAREHOUSE-CONTRIBUTIONS
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-YTD-FILE
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-DAILY-FILE
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHANGE-AMOUNTS
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-AMOUNTS
    }
    private void sub_Company_Cnt() throws Exception                                                                                                                       //Natural: COMPANY-CNT
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        short decideConditionsMet253 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF F96-VIEW.TWRC-COMPANY-CDE;//Natural: VALUE 'C'
        if (condition((ldaTwrl1900.getF96_View_Twrc_Company_Cde().equals("C"))))
        {
            decideConditionsMet253++;
            pnd_Cref_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #CREF-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((ldaTwrl1900.getF96_View_Twrc_Company_Cde().equals("T"))))
        {
            decideConditionsMet253++;
            pnd_Tiaa_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #TIAA-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((ldaTwrl1900.getF96_View_Twrc_Company_Cde().equals("L"))))
        {
            decideConditionsMet253++;
            pnd_Life_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #LIFE-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Othr_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #OTHR-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Data_Warehouse_Contributions() throws Exception                                                                                                      //Natural: DATA-WAREHOUSE-CONTRIBUTIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        if (condition(ldaTwrl1900.getF96_View_Twrc_Tax_Year().lessOrEqual("2005")))                                                                                       //Natural: IF F96-VIEW.TWRC-TAX-YEAR LE '2005'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl1900.getF96_View_Twrc_Status().equals(" ")))                                                                                                 //Natural: IF F96-VIEW.TWRC-STATUS = ' '
        {
            pnd_A_Cnt.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #A-CNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_I_Cnt.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #I-CNT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl1900.getF96_View_Twrc_Tax_Year().equals(pnd_Hold_Key_Values_Pnd_H_Tax_Year) && ldaTwrl1900.getF96_View_Twrc_Tax_Id().equals(pnd_Hold_Key_Values_Pnd_H_Tax_Id)  //Natural: IF F96-VIEW.TWRC-TAX-YEAR = #H-TAX-YEAR AND F96-VIEW.TWRC-TAX-ID = #H-TAX-ID AND F96-VIEW.TWRC-CONTRACT = #H-CONTRACT AND F96-VIEW.TWRC-PAYEE = #H-PAYEE
            && ldaTwrl1900.getF96_View_Twrc_Contract().equals(pnd_Hold_Key_Values_Pnd_H_Contract) && ldaTwrl1900.getF96_View_Twrc_Payee().equals(pnd_Hold_Key_Values_Pnd_H_Payee)))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Hold_Key_Values_Pnd_H_Tax_Year.setValue(ldaTwrl1900.getF96_View_Twrc_Tax_Year());                                                                         //Natural: ASSIGN #H-TAX-YEAR := F96-VIEW.TWRC-TAX-YEAR
            pnd_Hold_Key_Values_Pnd_H_Tax_Id.setValue(ldaTwrl1900.getF96_View_Twrc_Tax_Id());                                                                             //Natural: ASSIGN #H-TAX-ID := F96-VIEW.TWRC-TAX-ID
            pnd_Hold_Key_Values_Pnd_H_Contract.setValue(ldaTwrl1900.getF96_View_Twrc_Contract());                                                                         //Natural: ASSIGN #H-CONTRACT := F96-VIEW.TWRC-CONTRACT
            pnd_Hold_Key_Values_Pnd_H_Payee.setValue(ldaTwrl1900.getF96_View_Twrc_Payee());                                                                               //Natural: ASSIGN #H-PAYEE := F96-VIEW.TWRC-PAYEE
            pnd_Skip_The_Rest.reset();                                                                                                                                    //Natural: RESET #SKIP-THE-REST
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Skip_The_Rest.getBoolean()))                                                                                                                    //Natural: IF #SKIP-THE-REST
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl1900.getF96_View_Twrc_Status().equals(" ") && ! (ldaTwrl1900.getF96_View_Twrc_Create_Date().greaterOrEqual(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd())  //Natural: IF F96-VIEW.TWRC-STATUS = ' ' AND NOT ( F96-VIEW.TWRC-CREATE-DATE EQ #TWRP0600-FROM-CCYYMMDD THRU #TWRP0600-TO-CCYYMMDD )
            && ldaTwrl1900.getF96_View_Twrc_Create_Date().lessOrEqual(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd()))))
        {
            pnd_Skip_The_Rest.setValue(true);                                                                                                                             //Natural: ASSIGN #SKIP-THE-REST := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Tax_Yr().setValue(ldaTwrl1900.getF96_View_Twrc_Tax_Year());                                                                    //Natural: ASSIGN #TX-DW-CONTR.#TAX-YR := F96-VIEW.TWRC-TAX-YEAR
        ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Company_Cd().setValue(ldaTwrl1900.getF96_View_Twrc_Company_Cde());                                                             //Natural: ASSIGN #TX-DW-CONTR.#COMPANY-CD := F96-VIEW.TWRC-COMPANY-CDE
        ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Tin().setValue(ldaTwrl1900.getF96_View_Twrc_Tax_Id());                                                                         //Natural: ASSIGN #TX-DW-CONTR.#TIN := F96-VIEW.TWRC-TAX-ID
        ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Tin_Type().setValue(ldaTwrl1900.getF96_View_Twrc_Tax_Id_Type());                                                               //Natural: ASSIGN #TX-DW-CONTR.#TIN-TYPE := F96-VIEW.TWRC-TAX-ID-TYPE
        ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Contract().setValue(ldaTwrl1900.getF96_View_Twrc_Contract());                                                                  //Natural: ASSIGN #TX-DW-CONTR.#CONTRACT := F96-VIEW.TWRC-CONTRACT
        ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Payee().setValue(ldaTwrl1900.getF96_View_Twrc_Payee());                                                                        //Natural: ASSIGN #TX-DW-CONTR.#PAYEE := F96-VIEW.TWRC-PAYEE
        if (condition(ldaTwrl1900.getF96_View_Twrc_Source().equals("ZZ")))                                                                                                //Natural: IF F96-VIEW.TWRC-SOURCE = 'ZZ'
        {
            ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Srce_Orig().setValue("ML");                                                                                                //Natural: ASSIGN #TX-DW-CONTR.#SRCE-ORIG := 'ML'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Srce_Orig().setValue(ldaTwrl1900.getF96_View_Twrc_Source());                                                               //Natural: ASSIGN #TX-DW-CONTR.#SRCE-ORIG := F96-VIEW.TWRC-SOURCE
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Srce_Updt().setValue(ldaTwrl1900.getF96_View_Twrc_Update_Source());                                                            //Natural: ASSIGN #TX-DW-CONTR.#SRCE-UPDT := F96-VIEW.TWRC-UPDATE-SOURCE
        ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Ira_Contract_Type().setValue(ldaTwrl1900.getF96_View_Twrc_Ira_Contract_Type());                                                //Natural: ASSIGN #TX-DW-CONTR.#IRA-CONTRACT-TYPE := F96-VIEW.TWRC-IRA-CONTRACT-TYPE
        if (condition(DbsUtil.maskMatches(ldaTwrl1900.getF96_View_Twrc_Create_Date(),"YYYYMMDD")))                                                                        //Natural: IF F96-VIEW.TWRC-CREATE-DATE = MASK ( YYYYMMDD )
        {
            pnd_T_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaTwrl1900.getF96_View_Twrc_Create_Date());                                                         //Natural: MOVE EDITED F96-VIEW.TWRC-CREATE-DATE TO #T-DATE ( EM = YYYYMMDD )
            ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Create_Dt().setValueEdited(pnd_T_Date,new ReportEditMask("MM/DD/YYYY"));                                                   //Natural: MOVE EDITED #T-DATE ( EM = MM/DD/YYYY ) TO #TX-DW-CONTR.#CREATE-DT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Create_Dt().setValue(ldaTwrl1900.getF96_View_Twrc_Create_Date());                                                          //Natural: ASSIGN #TX-DW-CONTR.#CREATE-DT := F96-VIEW.TWRC-CREATE-DATE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(ldaTwrl1900.getF96_View_Twrc_Update_Date(),"YYYYMMDD")))                                                                        //Natural: IF F96-VIEW.TWRC-UPDATE-DATE = MASK ( YYYYMMDD )
        {
            pnd_T_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaTwrl1900.getF96_View_Twrc_Update_Date());                                                         //Natural: MOVE EDITED F96-VIEW.TWRC-UPDATE-DATE TO #T-DATE ( EM = YYYYMMDD )
            ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Updt_Dt().setValueEdited(pnd_T_Date,new ReportEditMask("MM/DD/YYYY"));                                                     //Natural: MOVE EDITED #T-DATE ( EM = MM/DD/YYYY ) TO #TX-DW-CONTR.#UPDT-DT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Updt_Dt().setValue(ldaTwrl1900.getF96_View_Twrc_Update_Date());                                                            //Natural: ASSIGN #TX-DW-CONTR.#UPDT-DT := F96-VIEW.TWRC-UPDATE-DATE
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Create_User().setValue(ldaTwrl1900.getF96_View_Twrc_Create_User());                                                            //Natural: ASSIGN #TX-DW-CONTR.#CREATE-USER := F96-VIEW.TWRC-CREATE-USER
        ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Updt_User().setValue(ldaTwrl1900.getF96_View_Twrc_Update_User());                                                              //Natural: ASSIGN #TX-DW-CONTR.#UPDT-USER := F96-VIEW.TWRC-UPDATE-USER
        ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Status().setValue(ldaTwrl1900.getF96_View_Twrc_Status());                                                                      //Natural: ASSIGN #TX-DW-CONTR.#STATUS := F96-VIEW.TWRC-STATUS
        //*  (M001)
        ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Error_Reason_Code().setValueEdited(ldaTwrl1900.getF96_View_Twrc_Error_Reason(),new ReportEditMask("99"));                      //Natural: MOVE EDITED TWRC-ERROR-REASON ( EM = 99 ) TO #TX-DW-CONTR.#ERROR-REASON-CODE
        ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Origin_Area_Num().setValueEdited(ldaTwrl1900.getF96_View_Twrc_Orign_Area(),new ReportEditMask("99"));                          //Natural: MOVE EDITED TWRC-ORIGN-AREA ( EM = 99 ) TO #TX-DW-CONTR.#ORIGIN-AREA-NUM
        if (condition(ldaTwrl1900.getF96_View_Twrc_Status().equals("I") && ldaTwrl1900.getF96_View_Twrc_Create_Date().greaterOrEqual(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd())  //Natural: IF F96-VIEW.TWRC-STATUS = 'I' AND F96-VIEW.TWRC-CREATE-DATE EQ #TWRP0600-FROM-CCYYMMDD THRU #TWRP0600-TO-CCYYMMDD AND F96-VIEW.TWRC-UPDATE-DATE EQ #TWRP0600-FROM-CCYYMMDD THRU #TWRP0600-TO-CCYYMMDD
            && ldaTwrl1900.getF96_View_Twrc_Create_Date().lessOrEqual(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd()) && ldaTwrl1900.getF96_View_Twrc_Update_Date().greaterOrEqual(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd()) 
            && ldaTwrl1900.getF96_View_Twrc_Update_Date().lessOrEqual(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd())))
        {
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMOUNTS
            sub_Convert_Amounts();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-DAILY-FILE
            sub_Create_Daily_File();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl1900.getF96_View_Twrc_Status().equals("I")))                                                                                                 //Natural: IF F96-VIEW.TWRC-STATUS = 'I'
        {
                                                                                                                                                                          //Natural: PERFORM CHANGE-AMOUNTS
            sub_Change_Amounts();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMOUNTS
        sub_Convert_Amounts();
        if (condition(Global.isEscape())) {return;}
        short decideConditionsMet337 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN F96-VIEW.TWRC-CLASSIC-AMT = 0 AND F96-VIEW.TWRC-SEP-AMT = 0 AND F96-VIEW.TWRC-ROTH-AMT = 0 AND F96-VIEW.TWRC-ROLLOVER-AMT = 0 AND F96-VIEW.TWRC-RECHAR-AMT = 0 AND F96-VIEW.TWRC-ROTH-CONVERSION-AMT = 0 AND F96-VIEW.TWRC-FAIR-MKT-VAL-AMT = 0
        if (condition(ldaTwrl1900.getF96_View_Twrc_Classic_Amt().equals(getZero()) && ldaTwrl1900.getF96_View_Twrc_Sep_Amt().equals(getZero()) && ldaTwrl1900.getF96_View_Twrc_Roth_Amt().equals(getZero()) 
            && ldaTwrl1900.getF96_View_Twrc_Rollover_Amt().equals(getZero()) && ldaTwrl1900.getF96_View_Twrc_Rechar_Amt().equals(getZero()) && ldaTwrl1900.getF96_View_Twrc_Roth_Conversion_Amt().equals(getZero()) 
            && ldaTwrl1900.getF96_View_Twrc_Fair_Mkt_Val_Amt().equals(getZero())))
        {
            decideConditionsMet337++;
            //*  ACTIVE
            pnd_All_Zero_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #ALL-ZERO-CNT
        }                                                                                                                                                                 //Natural: WHEN F96-VIEW.TWRC-STATUS = ' '
        else if (condition(ldaTwrl1900.getF96_View_Twrc_Status().equals(" ")))
        {
            decideConditionsMet337++;
                                                                                                                                                                          //Natural: PERFORM CREATE-YTD-FILE
            sub_Create_Ytd_File();
            if (condition(Global.isEscape())) {return;}
            if (condition(! (pnd_Skip_The_Rest.getBoolean())))                                                                                                            //Natural: IF NOT #SKIP-THE-REST
            {
                                                                                                                                                                          //Natural: PERFORM CREATE-DAILY-FILE
                sub_Create_Daily_File();
                if (condition(Global.isEscape())) {return;}
                //*  INACTIVE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM CREATE-DAILY-FILE
            sub_Create_Daily_File();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaTwrl1900.getF96_View_Twrc_Status().equals("I")))                                                                                                 //Natural: IF F96-VIEW.TWRC-STATUS = 'I'
        {
            if (condition(ldaTwrl1900.getF96_View_Twrc_Create_Date().greaterOrEqual(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd())              //Natural: IF F96-VIEW.TWRC-CREATE-DATE = #TWRP0600-FROM-CCYYMMDD THRU #TWRP0600-TO-CCYYMMDD AND F96-VIEW.TWRC-UPDATE-DATE EQ #TWRP0600-FROM-CCYYMMDD THRU #TWRP0600-TO-CCYYMMDD
                && ldaTwrl1900.getF96_View_Twrc_Create_Date().lessOrEqual(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd()) && ldaTwrl1900.getF96_View_Twrc_Update_Date().greaterOrEqual(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd()) 
                && ldaTwrl1900.getF96_View_Twrc_Update_Date().lessOrEqual(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd())))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Skip_The_Rest.setValue(true);                                                                                                                         //Natural: ASSIGN #SKIP-THE-REST := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_Ytd_File() throws Exception                                                                                                                   //Natural: CREATE-YTD-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        //*  YTD   DATA WAREHOUSE FILE
        getWorkFiles().write(5, false, ldaTwrl1905.getPnd_Tx_Dw_Contr());                                                                                                 //Natural: WRITE WORK FILE 05 #TX-DW-CONTR
    }
    private void sub_Create_Daily_File() throws Exception                                                                                                                 //Natural: CREATE-DAILY-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        //*  DAILY DATA WAREHOUSE FILE
        getWorkFiles().write(4, false, ldaTwrl1905.getPnd_Tx_Dw_Contr());                                                                                                 //Natural: WRITE WORK FILE 04 #TX-DW-CONTR
    }
    private void sub_Change_Amounts() throws Exception                                                                                                                    //Natural: CHANGE-AMOUNTS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        if (condition(ldaTwrl1900.getF96_View_Twrc_Status().equals("I")))                                                                                                 //Natural: IF F96-VIEW.TWRC-STATUS = 'I'
        {
            ldaTwrl1900.getF96_View_Twrc_Classic_Amt().compute(new ComputeParameters(false, ldaTwrl1900.getF96_View_Twrc_Classic_Amt()), ldaTwrl1900.getF96_View_Twrc_Classic_Amt().multiply(-1)); //Natural: ASSIGN F96-VIEW.TWRC-CLASSIC-AMT := F96-VIEW.TWRC-CLASSIC-AMT * -1
            ldaTwrl1900.getF96_View_Twrc_Sep_Amt().compute(new ComputeParameters(false, ldaTwrl1900.getF96_View_Twrc_Sep_Amt()), ldaTwrl1900.getF96_View_Twrc_Sep_Amt().multiply(-1)); //Natural: ASSIGN F96-VIEW.TWRC-SEP-AMT := F96-VIEW.TWRC-SEP-AMT * -1
            ldaTwrl1900.getF96_View_Twrc_Roth_Amt().compute(new ComputeParameters(false, ldaTwrl1900.getF96_View_Twrc_Roth_Amt()), ldaTwrl1900.getF96_View_Twrc_Roth_Amt().multiply(-1)); //Natural: ASSIGN F96-VIEW.TWRC-ROTH-AMT := F96-VIEW.TWRC-ROTH-AMT * -1
            ldaTwrl1900.getF96_View_Twrc_Rollover_Amt().compute(new ComputeParameters(false, ldaTwrl1900.getF96_View_Twrc_Rollover_Amt()), ldaTwrl1900.getF96_View_Twrc_Rollover_Amt().multiply(-1)); //Natural: ASSIGN F96-VIEW.TWRC-ROLLOVER-AMT := F96-VIEW.TWRC-ROLLOVER-AMT * -1
            ldaTwrl1900.getF96_View_Twrc_Rechar_Amt().compute(new ComputeParameters(false, ldaTwrl1900.getF96_View_Twrc_Rechar_Amt()), ldaTwrl1900.getF96_View_Twrc_Rechar_Amt().multiply(-1)); //Natural: ASSIGN F96-VIEW.TWRC-RECHAR-AMT := F96-VIEW.TWRC-RECHAR-AMT * -1
            ldaTwrl1900.getF96_View_Twrc_Roth_Conversion_Amt().compute(new ComputeParameters(false, ldaTwrl1900.getF96_View_Twrc_Roth_Conversion_Amt()),                  //Natural: ASSIGN F96-VIEW.TWRC-ROTH-CONVERSION-AMT := F96-VIEW.TWRC-ROTH-CONVERSION-AMT * -1
                ldaTwrl1900.getF96_View_Twrc_Roth_Conversion_Amt().multiply(-1));
            ldaTwrl1900.getF96_View_Twrc_Fair_Mkt_Val_Amt().compute(new ComputeParameters(false, ldaTwrl1900.getF96_View_Twrc_Fair_Mkt_Val_Amt()), ldaTwrl1900.getF96_View_Twrc_Fair_Mkt_Val_Amt().multiply(-1)); //Natural: ASSIGN F96-VIEW.TWRC-FAIR-MKT-VAL-AMT := F96-VIEW.TWRC-FAIR-MKT-VAL-AMT * -1
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Convert_Amounts() throws Exception                                                                                                                   //Natural: CONVERT-AMOUNTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        //*  FORMAT DOLLAR VALUES AS ALPHA
        ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Classic_Amt().setValueEdited(ldaTwrl1900.getF96_View_Twrc_Classic_Amt(),new ReportEditMask("-99999999999.99"));                //Natural: MOVE EDITED F96-VIEW.TWRC-CLASSIC-AMT ( EM = -99999999999.99 ) TO #TX-DW-CONTR.#CLASSIC-AMT
        ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Sep_Amt().setValueEdited(ldaTwrl1900.getF96_View_Twrc_Sep_Amt(),new ReportEditMask("-99999999999.99"));                        //Natural: MOVE EDITED F96-VIEW.TWRC-SEP-AMT ( EM = -99999999999.99 ) TO #TX-DW-CONTR.#SEP-AMT
        ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Roth_Amt().setValueEdited(ldaTwrl1900.getF96_View_Twrc_Roth_Amt(),new ReportEditMask("-99999999999.99"));                      //Natural: MOVE EDITED F96-VIEW.TWRC-ROTH-AMT ( EM = -99999999999.99 ) TO #TX-DW-CONTR.#ROTH-AMT
        ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Rollover_Amt().setValueEdited(ldaTwrl1900.getF96_View_Twrc_Rollover_Amt(),new ReportEditMask("-99999999999.99"));              //Natural: MOVE EDITED F96-VIEW.TWRC-ROLLOVER-AMT ( EM = -99999999999.99 ) TO #TX-DW-CONTR.#ROLLOVER-AMT
        ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Rechar_Amt().setValueEdited(ldaTwrl1900.getF96_View_Twrc_Rechar_Amt(),new ReportEditMask("-99999999999.99"));                  //Natural: MOVE EDITED F96-VIEW.TWRC-RECHAR-AMT ( EM = -99999999999.99 ) TO #TX-DW-CONTR.#RECHAR-AMT
        ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Roth_Conv_Amt().setValueEdited(ldaTwrl1900.getF96_View_Twrc_Roth_Conversion_Amt(),new ReportEditMask("-99999999999.99"));      //Natural: MOVE EDITED F96-VIEW.TWRC-ROTH-CONVERSION-AMT ( EM = -99999999999.99 ) TO #TX-DW-CONTR.#ROTH-CONV-AMT
        ldaTwrl1905.getPnd_Tx_Dw_Contr_Pnd_Fmv_Amt().setValueEdited(ldaTwrl1900.getF96_View_Twrc_Fair_Mkt_Val_Amt(),new ReportEditMask("-99999999999.99"));               //Natural: MOVE EDITED F96-VIEW.TWRC-FAIR-MKT-VAL-AMT ( EM = -99999999999.99 ) TO #TX-DW-CONTR.#FMV-AMT
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");
    }
}
