/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:32:23 PM
**        * FROM NATURAL PROGRAM : Twrp0995
************************************************************
**        * FILE NAME            : Twrp0995.java
**        * CLASS NAME           : Twrp0995
**        * INSTANCE NAME        : Twrp0995
************************************************************
* PROGRAM  : TWRP0995
* FUNCTION : PRINTS RECORDS WITH UPDATE SOURCE CODES 'OL' OR 'ML'
* AUTHOR   : EDITH  6/2/99
* UPDATES  :
*          : INCLUSION OF NEW COMPANY , TIAA-LIFE (EDITH 10/18/99)
*          : ADDED NEW COMPANY - TCII (J.ROTHOLZ)
* 09/17/04 RM TOPS RELEASE 3 CHANGES
*             ADD NEW COMPANY CODE 'X' FOR TRUST COMPANY AND NEW SOURCE
*             CODES 'PL', 'NL'.
* 06/06/06: NAVISYS CHANGES   R. MA
*           SOURCE CODE 'NV' HAD BEEN ADDED TO ALL CONTROL REPORTS.
*           THIS PROGRAM IS ONE OF THE CONTROL REPORTS BUT DON't need
*           ANY CODE CHANGES THIS TIME.
* 10/18/11  JB NON-ERISA TDA - ADD COMPANY CODE 'F' SCAN JB1011
* 11/21/14  O SOTTO FATCA CHANGES MARKED 11/2014.
********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0995 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0901 ldaTwrl0901;
    private LdaTwrl0902 ldaTwrl0902;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_Area;
    private DbsField pnd_Work_Area_Pnd_Rec_Read;
    private DbsField pnd_Work_Area_Pnd_Process_Rec;

    private DbsGroup pnd_Work_Area_Pnd_Var_File1;
    private DbsField pnd_Work_Area_Pnd_Tax_Year;

    private DbsGroup pnd_Work_Area__R_Field_1;
    private DbsField pnd_Work_Area_Pnd_Tax_Year_A;
    private DbsField pnd_Work_Area_Pnd_Start_Date;
    private DbsField pnd_Work_Area_Pnd_End_Date;
    private DbsField pnd_Work_Area_Pnd_Freq;
    private DbsField pnd_Work_Area_Pnd_Sv_Company;
    private DbsField pnd_Work_Area_Pnd_Sv_Source;
    private DbsField pnd_Work_Area_Pnd_Sv_Payset;
    private DbsField pnd_Work_Area_Pnd_Print_Com;
    private DbsField pnd_Work_Area_Pnd_Trans_Desc;
    private DbsField pnd_Work_Area_Pnd_Old_Sc;
    private DbsField pnd_Work_Area_Pnd_Old_Co;
    private DbsField pnd_Work_Area_Pnd_Contract_Payee;
    private DbsField pnd_Work_Area_Pnd_I;
    private DbsField pnd_Work_Area_Pnd_Prt_Source_Code;
    private DbsField pnd_Work_Area_Pnd_New_Record;
    private DbsField pnd_Work_Area_Pnd_Fatca;
    private DbsField pnd_Work_Area_Pnd_Hdg1;

    private DbsGroup pnd_Totals;
    private DbsField pnd_Totals_Pnd_Total_Cnt;
    private DbsField pnd_Totals_Pnd_Total_Grs;
    private DbsField pnd_Totals_Pnd_Total_Ivc;
    private DbsField pnd_Totals_Pnd_Total_Int;
    private DbsField pnd_Totals_Pnd_Total_Fed;
    private DbsField pnd_Totals_Pnd_Total_Nra;
    private DbsField pnd_Totals_Pnd_Total_Sta;
    private DbsField pnd_Totals_Pnd_Total_Lcl;
    private DbsField pnd_Totals_Pnd_Total_Can;
    private DbsField pnd_Cmpy_Cnt;

    private DbsGroup pnd_Company_Totals;
    private DbsField pnd_Company_Totals_Pnd_Tot_Cmpny;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Cnt;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Grs;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Ivc;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Int;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Fed;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Nra;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Sta;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Lcl;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Can;
    private DbsField pnd_Tot_Ndx;
    private DbsField pnd_Dashes;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0901 = new LdaTwrl0901();
        registerRecord(ldaTwrl0901);
        ldaTwrl0902 = new LdaTwrl0902();
        registerRecord(ldaTwrl0902);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Work_Area = localVariables.newGroupInRecord("pnd_Work_Area", "#WORK-AREA");
        pnd_Work_Area_Pnd_Rec_Read = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Rec_Read", "#REC-READ", FieldType.NUMERIC, 7);
        pnd_Work_Area_Pnd_Process_Rec = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Process_Rec", "#PROCESS-REC", FieldType.NUMERIC, 7);

        pnd_Work_Area_Pnd_Var_File1 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area_Pnd_Var_File1", "#VAR-FILE1");
        pnd_Work_Area_Pnd_Tax_Year = pnd_Work_Area_Pnd_Var_File1.newFieldInGroup("pnd_Work_Area_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);

        pnd_Work_Area__R_Field_1 = pnd_Work_Area_Pnd_Var_File1.newGroupInGroup("pnd_Work_Area__R_Field_1", "REDEFINE", pnd_Work_Area_Pnd_Tax_Year);
        pnd_Work_Area_Pnd_Tax_Year_A = pnd_Work_Area__R_Field_1.newFieldInGroup("pnd_Work_Area_Pnd_Tax_Year_A", "#TAX-YEAR-A", FieldType.STRING, 4);
        pnd_Work_Area_Pnd_Start_Date = pnd_Work_Area_Pnd_Var_File1.newFieldInGroup("pnd_Work_Area_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);
        pnd_Work_Area_Pnd_End_Date = pnd_Work_Area_Pnd_Var_File1.newFieldInGroup("pnd_Work_Area_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);
        pnd_Work_Area_Pnd_Freq = pnd_Work_Area_Pnd_Var_File1.newFieldInGroup("pnd_Work_Area_Pnd_Freq", "#FREQ", FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Sv_Company = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Sv_Company", "#SV-COMPANY", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Sv_Source = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Sv_Source", "#SV-SOURCE", FieldType.STRING, 4);
        pnd_Work_Area_Pnd_Sv_Payset = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Sv_Payset", "#SV-PAYSET", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Print_Com = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Print_Com", "#PRINT-COM", FieldType.STRING, 4);
        pnd_Work_Area_Pnd_Trans_Desc = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Trans_Desc", "#TRANS-DESC", FieldType.STRING, 19);
        pnd_Work_Area_Pnd_Old_Sc = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Old_Sc", "#OLD-SC", FieldType.STRING, 4);
        pnd_Work_Area_Pnd_Old_Co = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Old_Co", "#OLD-CO", FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Contract_Payee = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Contract_Payee", "#CONTRACT-PAYEE", FieldType.STRING, 11);
        pnd_Work_Area_Pnd_I = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_I", "#I", FieldType.NUMERIC, 1);
        pnd_Work_Area_Pnd_Prt_Source_Code = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Prt_Source_Code", "#PRT-SOURCE-CODE", FieldType.STRING, 4);
        pnd_Work_Area_Pnd_New_Record = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_New_Record", "#NEW-RECORD", FieldType.BOOLEAN, 1);
        pnd_Work_Area_Pnd_Fatca = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Fatca", "#FATCA", FieldType.BOOLEAN, 1);
        pnd_Work_Area_Pnd_Hdg1 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Hdg1", "#HDG1", FieldType.STRING, 13);

        pnd_Totals = localVariables.newGroupArrayInRecord("pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Totals_Pnd_Total_Cnt = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Cnt", "#TOTAL-CNT", FieldType.NUMERIC, 7);
        pnd_Totals_Pnd_Total_Grs = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Grs", "#TOTAL-GRS", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Totals_Pnd_Total_Ivc = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Ivc", "#TOTAL-IVC", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Total_Int = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Int", "#TOTAL-INT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Total_Fed = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Fed", "#TOTAL-FED", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Total_Nra = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Nra", "#TOTAL-NRA", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Total_Sta = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Sta", "#TOTAL-STA", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Total_Lcl = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Lcl", "#TOTAL-LCL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Total_Can = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Can", "#TOTAL-CAN", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cmpy_Cnt = localVariables.newFieldInRecord("pnd_Cmpy_Cnt", "#CMPY-CNT", FieldType.PACKED_DECIMAL, 3);

        pnd_Company_Totals = localVariables.newGroupArrayInRecord("pnd_Company_Totals", "#COMPANY-TOTALS", new DbsArrayController(1, 7));
        pnd_Company_Totals_Pnd_Tot_Cmpny = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Cmpny", "#TOT-CMPNY", FieldType.STRING, 4);
        pnd_Company_Totals_Pnd_Tot_Total_Cnt = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Cnt", "#TOT-TOTAL-CNT", FieldType.NUMERIC, 
            7);
        pnd_Company_Totals_Pnd_Tot_Total_Grs = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Grs", "#TOT-TOTAL-GRS", FieldType.PACKED_DECIMAL, 
            14, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Ivc = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Ivc", "#TOT-TOTAL-IVC", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Int = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Int", "#TOT-TOTAL-INT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Fed = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Fed", "#TOT-TOTAL-FED", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Nra = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Nra", "#TOT-TOTAL-NRA", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Sta = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Sta", "#TOT-TOTAL-STA", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Lcl = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Lcl", "#TOT-TOTAL-LCL", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Can = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Can", "#TOT-TOTAL-CAN", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Tot_Ndx = localVariables.newFieldInRecord("pnd_Tot_Ndx", "#TOT-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Dashes = localVariables.newFieldInRecord("pnd_Dashes", "#DASHES", FieldType.STRING, 131);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0901.initializeValues();
        ldaTwrl0902.initializeValues();

        localVariables.reset();
        pnd_Work_Area_Pnd_New_Record.setInitialValue(true);
        pnd_Work_Area_Pnd_Fatca.setInitialValue(false);
        pnd_Work_Area_Pnd_Hdg1.setInitialValue("NON-FATCA TAX");
        pnd_Cmpy_Cnt.setInitialValue(7);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0995() throws Exception
    {
        super("Twrp0995");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  11/2014
        pnd_Dashes.moveAll("=");                                                                                                                                          //Natural: MOVE ALL '=' TO #DASHES
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #FLAT-FILE1
        while (condition(getWorkFiles().read(1, ldaTwrl0901.getPnd_Flat_File1())))
        {
            pnd_Work_Area_Pnd_Tax_Year.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Tax_Year());                                                                        //Natural: ASSIGN #TAX-YEAR := #FF1-TAX-YEAR
            pnd_Work_Area_Pnd_Start_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Start_Date());                                                                    //Natural: ASSIGN #START-DATE := #FF1-START-DATE
            pnd_Work_Area_Pnd_End_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_End_Date());                                                                        //Natural: ASSIGN #END-DATE := #FF1-END-DATE
            pnd_Work_Area_Pnd_Freq.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Frequency());                                                                           //Natural: ASSIGN #FREQ := #FF1-FREQUENCY
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* *READ WORK FILE 2  RECORD #FLAT-FILE3 /* 11/2014
        //*  11/2014
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 2 #FLAT-FILE3
        while (condition(getWorkFiles().read(2, ldaTwrl0902.getPnd_Flat_File3())))
        {
            pnd_Work_Area_Pnd_Rec_Read.nadd(1);                                                                                                                           //Natural: ADD 1 TO #REC-READ
            //*  9/17/04
            //*  11/2014
            if (condition(!(((((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("OL") || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("ML"))  //Natural: ACCEPT IF ( SUBSTRING ( #FF3-SOURCE-CODE,3,2 ) = 'OL' OR = 'ML' OR = 'PL' OR = 'NL' ) AND #FF3-TWRPYMNT-FATCA-IND NE 'Y'
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("PL")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("NL")) 
                && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Twrpymnt_Fatca_Ind().notEquals("Y")))))
            {
                continue;
            }
            pnd_Work_Area_Pnd_Process_Rec.nadd(1);                                                                                                                        //Natural: ADD 1 TO #PROCESS-REC
            if (condition(pnd_Work_Area_Pnd_New_Record.getBoolean()))                                                                                                     //Natural: IF #NEW-RECORD
            {
                pnd_Work_Area_Pnd_New_Record.setValue(false);                                                                                                             //Natural: ASSIGN #NEW-RECORD := FALSE
                pnd_Work_Area_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                              //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
                pnd_Work_Area_Pnd_Sv_Source.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                                //Natural: ASSIGN #SV-SOURCE := #FF3-SOURCE-CODE
                                                                                                                                                                          //Natural: PERFORM CHECK-TRANS-TYPE
                sub_Check_Trans_Type();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code().equals(pnd_Work_Area_Pnd_Sv_Company)))                                                     //Natural: IF #FF3-COMPANY-CODE = #SV-COMPANY
            {
                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().equals(pnd_Work_Area_Pnd_Sv_Source)))                                                   //Natural: IF #FF3-SOURCE-CODE = #SV-SOURCE
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM BREAK-SOURCE-CODE
                    sub_Break_Source_Code();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM BREAK-COMPANY
                sub_Break_Company();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL
            sub_Print_Detail();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: AT END OF DATA;//Natural: END-WORK
        READWORK02_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            //*  11/2014
            if (condition(pnd_Work_Area_Pnd_Process_Rec.equals(getZero())))                                                                                               //Natural: IF #PROCESS-REC = 0
            {
                //*      WRITE (1) 40T '*****  NO RECORDS PROCESSED ***** '
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM BREAK-COMPANY
                sub_Break_Company();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            getReports().eject(1, true);                                                                                                                                  //Natural: EJECT ( 1 )
            getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM()," - CONTROL TOTAL",NEWLINE,NEWLINE,NEWLINE,"RUNDATE    : ",Global.getDATX(),                   //Natural: WRITE ( 1 ) *PROGRAM ' - CONTROL TOTAL' // / 'RUNDATE    : ' *DATX ( EM = MM/DD/YYYY ) / 'RUNTIME    : ' *TIMX / 'TAX YEAR   : ' #TAX-YEAR / 'DATE RANGE : ' #START-DATE 'THRU' #END-DATE /// '1) NO. OF RECORDS READ      : ' #REC-READ / '2) NO. OF RECORDS PROCESSED : ' #PROCESS-REC
                new ReportEditMask ("MM/DD/YYYY"),NEWLINE,"RUNTIME    : ",Global.getTIMX(),NEWLINE,"TAX YEAR   : ",pnd_Work_Area_Pnd_Tax_Year,NEWLINE,"DATE RANGE : ",
                pnd_Work_Area_Pnd_Start_Date,"THRU",pnd_Work_Area_Pnd_End_Date,NEWLINE,NEWLINE,NEWLINE,"1) NO. OF RECORDS READ      : ",pnd_Work_Area_Pnd_Rec_Read,
                NEWLINE,"2) NO. OF RECORDS PROCESSED : ",pnd_Work_Area_Pnd_Process_Rec);
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //*  11/2014 START
        if (condition(pnd_Work_Area_Pnd_Process_Rec.equals(getZero())))                                                                                                   //Natural: IF #PROCESS-REC = 0
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(40),"*****  NO RECORDS PROCESSED ***** ");                                                          //Natural: WRITE ( 1 ) 40T '*****  NO RECORDS PROCESSED ***** '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Work_Area.resetInitial();                                                                                                                                     //Natural: RESET INITIAL #WORK-AREA #TOTALS ( * )
        pnd_Totals.getValue("*").resetInitial();
        pnd_Work_Area_Pnd_Fatca.setValue(true);                                                                                                                           //Natural: ASSIGN #FATCA := TRUE
        getWorkFiles().close(1);                                                                                                                                          //Natural: CLOSE WORK FILE 1
        getWorkFiles().close(2);                                                                                                                                          //Natural: CLOSE WORK FILE 2
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        READWORK03:                                                                                                                                                       //Natural: READ WORK FILE 1 #FLAT-FILE1
        while (condition(getWorkFiles().read(1, ldaTwrl0901.getPnd_Flat_File1())))
        {
            pnd_Work_Area_Pnd_Tax_Year.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Tax_Year());                                                                        //Natural: ASSIGN #TAX-YEAR := #FF1-TAX-YEAR
            pnd_Work_Area_Pnd_Start_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Start_Date());                                                                    //Natural: ASSIGN #START-DATE := #FF1-START-DATE
            pnd_Work_Area_Pnd_End_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_End_Date());                                                                        //Natural: ASSIGN #END-DATE := #FF1-END-DATE
            pnd_Work_Area_Pnd_Freq.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Frequency());                                                                           //Natural: ASSIGN #FREQ := #FF1-FREQUENCY
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK03_Exit:
        if (Global.isEscape()) return;
        READWORK04:                                                                                                                                                       //Natural: READ WORK FILE 2 #FLAT-FILE3
        while (condition(getWorkFiles().read(2, ldaTwrl0902.getPnd_Flat_File3())))
        {
            pnd_Work_Area_Pnd_Rec_Read.nadd(1);                                                                                                                           //Natural: ADD 1 TO #REC-READ
            //*  9/17/04
            //*  11/2014
            if (condition(!(((((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("OL") || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("ML"))  //Natural: ACCEPT IF ( SUBSTRING ( #FF3-SOURCE-CODE,3,2 ) = 'OL' OR = 'ML' OR = 'PL' OR = 'NL' ) AND #FF3-TWRPYMNT-FATCA-IND EQ 'Y'
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("PL")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("NL")) 
                && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Twrpymnt_Fatca_Ind().equals("Y")))))
            {
                continue;
            }
            pnd_Work_Area_Pnd_Process_Rec.nadd(1);                                                                                                                        //Natural: ADD 1 TO #PROCESS-REC
            if (condition(pnd_Work_Area_Pnd_New_Record.getBoolean()))                                                                                                     //Natural: IF #NEW-RECORD
            {
                pnd_Work_Area_Pnd_New_Record.setValue(false);                                                                                                             //Natural: ASSIGN #NEW-RECORD := FALSE
                pnd_Work_Area_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                              //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
                pnd_Work_Area_Pnd_Sv_Source.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                                //Natural: ASSIGN #SV-SOURCE := #FF3-SOURCE-CODE
                                                                                                                                                                          //Natural: PERFORM CHECK-TRANS-TYPE
                sub_Check_Trans_Type();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code().equals(pnd_Work_Area_Pnd_Sv_Company)))                                                     //Natural: IF #FF3-COMPANY-CODE = #SV-COMPANY
            {
                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().equals(pnd_Work_Area_Pnd_Sv_Source)))                                                   //Natural: IF #FF3-SOURCE-CODE = #SV-SOURCE
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM BREAK-SOURCE-CODE
                    sub_Break_Source_Code();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM BREAK-COMPANY
                sub_Break_Company();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL
            sub_Print_Detail();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: AT END OF DATA;//Natural: END-WORK
        READWORK04_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            if (condition(pnd_Work_Area_Pnd_Process_Rec.equals(getZero())))                                                                                               //Natural: IF #PROCESS-REC = 0
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM BREAK-COMPANY
                sub_Break_Company();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            getReports().eject(1, true);                                                                                                                                  //Natural: EJECT ( 1 )
            getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM()," - CONTROL TOTAL",NEWLINE,NEWLINE,NEWLINE,"RUNDATE    : ",Global.getDATX(),                   //Natural: WRITE ( 1 ) *PROGRAM ' - CONTROL TOTAL' // / 'RUNDATE    : ' *DATX ( EM = MM/DD/YYYY ) / 'RUNTIME    : ' *TIMX / 'TAX YEAR   : ' #TAX-YEAR / 'DATE RANGE : ' #START-DATE 'THRU' #END-DATE /// '1) NO. OF RECORDS READ      : ' #REC-READ / '2) NO. OF RECORDS PROCESSED : ' #PROCESS-REC
                new ReportEditMask ("MM/DD/YYYY"),NEWLINE,"RUNTIME    : ",Global.getTIMX(),NEWLINE,"TAX YEAR   : ",pnd_Work_Area_Pnd_Tax_Year,NEWLINE,"DATE RANGE : ",
                pnd_Work_Area_Pnd_Start_Date,"THRU",pnd_Work_Area_Pnd_End_Date,NEWLINE,NEWLINE,NEWLINE,"1) NO. OF RECORDS READ      : ",pnd_Work_Area_Pnd_Rec_Read,
                NEWLINE,"2) NO. OF RECORDS PROCESSED : ",pnd_Work_Area_Pnd_Process_Rec);
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        if (condition(pnd_Work_Area_Pnd_Process_Rec.equals(getZero())))                                                                                                   //Natural: IF #PROCESS-REC = 0
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(40),"*****  NO RECORDS PROCESSED ***** ");                                                          //Natural: WRITE ( 1 ) 40T '*****  NO RECORDS PROCESSED ***** '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-COMPANY-TOTALS
        sub_Print_Company_Totals();
        if (condition(Global.isEscape())) {return;}
        //*  11/2014 END
        //* ***********************************************************************
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CO-DESC
        //*  NONE IGNORE
    }
    private void sub_Break_Source_Code() throws Exception                                                                                                                 //Natural: BREAK-SOURCE-CODE
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(6),"TOTAL (",pnd_Work_Area_Pnd_Prt_Source_Code,")",new TabSetting(23),pnd_Totals_Pnd_Total_Cnt.getValue(1),  //Natural: WRITE ( 1 ) // 6T 'TOTAL (' #PRT-SOURCE-CODE ')' 23T #TOTAL-CNT ( 1 ) 57T #TOTAL-GRS ( 1 ) 77T #TOTAL-IVC ( 1 ) 93T #TOTAL-INT ( 1 ) 109T #TOTAL-FED ( 1 ) / 61T #TOTAL-NRA ( 1 ) 77T #TOTAL-STA ( 1 ) 93T #TOTAL-LCL ( 1 ) 109T #TOTAL-CAN ( 1 ) //
            new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(57),pnd_Totals_Pnd_Total_Grs.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(77),pnd_Totals_Pnd_Total_Ivc.getValue(1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(93),pnd_Totals_Pnd_Total_Int.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(109),pnd_Totals_Pnd_Total_Fed.getValue(1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(61),pnd_Totals_Pnd_Total_Nra.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new 
            TabSetting(77),pnd_Totals_Pnd_Total_Sta.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(93),pnd_Totals_Pnd_Total_Lcl.getValue(1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(109),pnd_Totals_Pnd_Total_Can.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,
            NEWLINE);
        if (Global.isEscape()) return;
        pnd_Totals.getValue(1).reset();                                                                                                                                   //Natural: RESET #TOTALS ( 1 )
                                                                                                                                                                          //Natural: PERFORM CHECK-TRANS-TYPE
        sub_Check_Trans_Type();
        if (condition(Global.isEscape())) {return;}
        pnd_Work_Area_Pnd_Sv_Source.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                                        //Natural: ASSIGN #SV-SOURCE := #FF3-SOURCE-CODE
    }
    private void sub_Break_Company() throws Exception                                                                                                                     //Natural: BREAK-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

                                                                                                                                                                          //Natural: PERFORM BREAK-SOURCE-CODE
        sub_Break_Source_Code();
        if (condition(Global.isEscape())) {return;}
        //*  11/2014 START
                                                                                                                                                                          //Natural: PERFORM CO-DESC
        sub_Co_Desc();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Company_Totals_Pnd_Tot_Cmpny.getValue("*").equals(pnd_Work_Area_Pnd_Print_Com)))                                                                //Natural: IF #TOT-CMPNY ( * ) = #PRINT-COM
        {
            DbsUtil.examine(new ExamineSource(pnd_Company_Totals_Pnd_Tot_Cmpny.getValue("*")), new ExamineSearch(pnd_Work_Area_Pnd_Print_Com), new ExamineGivingIndex(pnd_Tot_Ndx)); //Natural: EXAMINE #TOT-CMPNY ( * ) FOR #PRINT-COM GIVING INDEX #TOT-NDX
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR01:                                                                                                                                                        //Natural: FOR #TOT-NDX 1 #CMPY-CNT
            for (pnd_Tot_Ndx.setValue(1); condition(pnd_Tot_Ndx.lessOrEqual(pnd_Cmpy_Cnt)); pnd_Tot_Ndx.nadd(1))
            {
                if (condition(pnd_Company_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx).equals(" ")))                                                                        //Natural: IF #TOT-CMPNY ( #TOT-NDX ) = ' '
                {
                    pnd_Company_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx).setValue(pnd_Work_Area_Pnd_Print_Com);                                                         //Natural: ASSIGN #TOT-CMPNY ( #TOT-NDX ) := #PRINT-COM
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Company_Totals_Pnd_Tot_Total_Cnt.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Cnt.getValue(2));                                                            //Natural: ASSIGN #TOT-TOTAL-CNT ( #TOT-NDX ) := #TOT-TOTAL-CNT ( #TOT-NDX ) + #TOTAL-CNT ( 2 )
        pnd_Company_Totals_Pnd_Tot_Total_Grs.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Grs.getValue(2));                                                            //Natural: ASSIGN #TOT-TOTAL-GRS ( #TOT-NDX ) := #TOT-TOTAL-GRS ( #TOT-NDX ) + #TOTAL-GRS ( 2 )
        pnd_Company_Totals_Pnd_Tot_Total_Ivc.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Ivc.getValue(2));                                                            //Natural: ASSIGN #TOT-TOTAL-IVC ( #TOT-NDX ) := #TOT-TOTAL-IVC ( #TOT-NDX ) + #TOTAL-IVC ( 2 )
        pnd_Company_Totals_Pnd_Tot_Total_Int.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Int.getValue(2));                                                            //Natural: ASSIGN #TOT-TOTAL-INT ( #TOT-NDX ) := #TOT-TOTAL-INT ( #TOT-NDX ) + #TOTAL-INT ( 2 )
        pnd_Company_Totals_Pnd_Tot_Total_Fed.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Fed.getValue(2));                                                            //Natural: ASSIGN #TOT-TOTAL-FED ( #TOT-NDX ) := #TOT-TOTAL-FED ( #TOT-NDX ) + #TOTAL-FED ( 2 )
        pnd_Company_Totals_Pnd_Tot_Total_Nra.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Nra.getValue(2));                                                            //Natural: ASSIGN #TOT-TOTAL-NRA ( #TOT-NDX ) := #TOT-TOTAL-NRA ( #TOT-NDX ) + #TOTAL-NRA ( 2 )
        pnd_Company_Totals_Pnd_Tot_Total_Sta.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Sta.getValue(2));                                                            //Natural: ASSIGN #TOT-TOTAL-STA ( #TOT-NDX ) := #TOT-TOTAL-STA ( #TOT-NDX ) + #TOTAL-STA ( 2 )
        pnd_Company_Totals_Pnd_Tot_Total_Lcl.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Lcl.getValue(2));                                                            //Natural: ASSIGN #TOT-TOTAL-LCL ( #TOT-NDX ) := #TOT-TOTAL-LCL ( #TOT-NDX ) + #TOTAL-LCL ( 2 )
        pnd_Company_Totals_Pnd_Tot_Total_Can.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Can.getValue(2));                                                            //Natural: ASSIGN #TOT-TOTAL-CAN ( #TOT-NDX ) := #TOT-TOTAL-CAN ( #TOT-NDX ) + #TOTAL-CAN ( 2 )
        //*  11/2014 END
        getReports().write(1, ReportOption.NOTITLE,"GRANDTOTAL (",pnd_Work_Area_Pnd_Print_Com,")",new TabSetting(23),pnd_Totals_Pnd_Total_Cnt.getValue(2),                //Natural: WRITE ( 1 ) 'GRANDTOTAL (' #PRINT-COM ')' 23T #TOTAL-CNT ( 2 ) 57T #TOTAL-GRS ( 2 ) 77T #TOTAL-IVC ( 2 ) 93T #TOTAL-INT ( 2 ) 109T #TOTAL-FED ( 2 ) / 61T #TOTAL-NRA ( 2 ) 77T #TOTAL-STA ( 2 ) 93T #TOTAL-LCL ( 2 ) 109T #TOTAL-CAN ( 2 )
            new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(57),pnd_Totals_Pnd_Total_Grs.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(77),pnd_Totals_Pnd_Total_Ivc.getValue(2), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(93),pnd_Totals_Pnd_Total_Int.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(109),pnd_Totals_Pnd_Total_Fed.getValue(2), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(61),pnd_Totals_Pnd_Total_Nra.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new 
            TabSetting(77),pnd_Totals_Pnd_Total_Sta.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(93),pnd_Totals_Pnd_Total_Lcl.getValue(2), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(109),pnd_Totals_Pnd_Total_Can.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
        pnd_Totals.getValue(2).reset();                                                                                                                                   //Natural: RESET #TOTALS ( 2 )
        pnd_Work_Area_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                                      //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
    }
    private void sub_Print_Company_Totals() throws Exception                                                                                                              //Natural: PRINT-COMPANY-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(1, ReportOption.NOTITLE,pnd_Dashes);                                                                                                           //Natural: WRITE ( 1 ) #DASHES
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #TOT-NDX 1 #CMPY-CNT
        for (pnd_Tot_Ndx.setValue(1); condition(pnd_Tot_Ndx.lessOrEqual(pnd_Cmpy_Cnt)); pnd_Tot_Ndx.nadd(1))
        {
            if (condition(pnd_Company_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx).equals(" ")))                                                                            //Natural: IF #TOT-CMPNY ( #TOT-NDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,"OVERALLL TOTAL",pnd_Company_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx),new TabSetting(23),pnd_Company_Totals_Pnd_Tot_Total_Cnt.getValue(pnd_Tot_Ndx),  //Natural: WRITE ( 1 ) 'OVERALLL TOTAL' #TOT-CMPNY ( #TOT-NDX ) 23T #TOT-TOTAL-CNT ( #TOT-NDX ) 57T #TOT-TOTAL-GRS ( #TOT-NDX ) 77T #TOT-TOTAL-IVC ( #TOT-NDX ) 93T #TOT-TOTAL-INT ( #TOT-NDX ) 109T #TOT-TOTAL-FED ( #TOT-NDX ) / 61T #TOT-TOTAL-NRA ( #TOT-NDX ) 77T #TOT-TOTAL-STA ( #TOT-NDX ) 93T #TOT-TOTAL-LCL ( #TOT-NDX ) 109T #TOT-TOTAL-CAN ( #TOT-NDX )
                new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(57),pnd_Company_Totals_Pnd_Tot_Total_Grs.getValue(pnd_Tot_Ndx), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9.99-"),new 
                TabSetting(77),pnd_Company_Totals_Pnd_Tot_Total_Ivc.getValue(pnd_Tot_Ndx), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(93),pnd_Company_Totals_Pnd_Tot_Total_Int.getValue(pnd_Tot_Ndx), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(109),pnd_Company_Totals_Pnd_Tot_Total_Fed.getValue(pnd_Tot_Ndx), new ReportEditMask 
                ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(61),pnd_Company_Totals_Pnd_Tot_Total_Nra.getValue(pnd_Tot_Ndx), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new 
                TabSetting(77),pnd_Company_Totals_Pnd_Tot_Total_Sta.getValue(pnd_Tot_Ndx), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(93),pnd_Company_Totals_Pnd_Tot_Total_Lcl.getValue(pnd_Tot_Ndx), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(109),pnd_Company_Totals_Pnd_Tot_Total_Can.getValue(pnd_Tot_Ndx), new ReportEditMask 
                ("ZZZ,ZZZ,ZZ9.99-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Print_Detail() throws Exception                                                                                                                      //Natural: PRINT-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //*  9/17/04  RM
        short decideConditionsMet305 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #FF3-SOURCE-CODE;//Natural: VALUE 'ZZOL'
        if (condition((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().equals("ZZOL"))))
        {
            decideConditionsMet305++;
            pnd_Work_Area_Pnd_Prt_Source_Code.setValue("MLOL");                                                                                                           //Natural: ASSIGN #PRT-SOURCE-CODE := 'MLOL'
        }                                                                                                                                                                 //Natural: VALUE 'ZZML'
        else if (condition((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().equals("ZZML"))))
        {
            decideConditionsMet305++;
            pnd_Work_Area_Pnd_Prt_Source_Code.setValue("  ML");                                                                                                           //Natural: ASSIGN #PRT-SOURCE-CODE := '  ML'
        }                                                                                                                                                                 //Natural: VALUE 'YYPL'
        else if (condition((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().equals("YYPL"))))
        {
            decideConditionsMet305++;
            pnd_Work_Area_Pnd_Prt_Source_Code.setValue("NLPL");                                                                                                           //Natural: ASSIGN #PRT-SOURCE-CODE := 'NLPL'
        }                                                                                                                                                                 //Natural: VALUE 'YYNL'
        else if (condition((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().equals("YYNL"))))
        {
            decideConditionsMet305++;
            pnd_Work_Area_Pnd_Prt_Source_Code.setValue("  NL");                                                                                                           //Natural: ASSIGN #PRT-SOURCE-CODE := '  NL'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Work_Area_Pnd_Prt_Source_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                              //Natural: ASSIGN #PRT-SOURCE-CODE := #FF3-SOURCE-CODE
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Payset_Type().equals(pnd_Work_Area_Pnd_Sv_Payset)))                                                           //Natural: IF #FF3-PAYSET-TYPE = #SV-PAYSET
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM CHECK-TRANS-TYPE
            sub_Check_Trans_Type();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Work_Area_Pnd_Contract_Payee.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Contract_Nbr(),                   //Natural: COMPRESS #FF3-CONTRACT-NBR '-' #FF3-PAYEE-CDE TO #CONTRACT-PAYEE LEAVING NO SPACE
            "-", ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Payee_Cde()));
        //*  ( 1 - TOTAL   2 - GRANDTOTAL )
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,pnd_Work_Area_Pnd_Prt_Source_Code,new ColumnSpacing(1),pnd_Work_Area_Pnd_Trans_Desc,new             //Natural: WRITE ( 1 ) NOTITLE NOHDR #PRT-SOURCE-CODE 1X #TRANS-DESC 1X #CONTRACT-PAYEE 1X #FF3-TAX-ID-NBR 1X #FF3-PYMNT-DATE 2X #FF3-IVC-IND 1X #FF3-GROSS-AMT ( EM = ZZZ,ZZZ,ZZ9.99- ) 3X #FF3-IVC-AMT ( EM = Z,ZZZ,ZZ9.99- ) 3X #FF3-INT-AMT ( EM = Z,ZZZ,ZZ9.99- ) 3X #FF3-FED-WTHLD-AMT ( EM = Z,ZZZ,ZZ9.99- ) 3X #FF3-RESIDENCY-CODE / 63T #FF3-NRA-WTHLD-AMT ( EM = Z,ZZZ,ZZ9.99- ) 3X #FF3-STATE-WTHLD ( EM = Z,ZZZ,ZZ9.99- ) 3X #FF3-LOCAL-WTHLD ( EM = Z,ZZZ,ZZ9.99- ) 3X #FF3-CAN-WTHLD-AMT ( EM = Z,ZZZ,ZZ9.99- )
            ColumnSpacing(1),pnd_Work_Area_Pnd_Contract_Payee,new ColumnSpacing(1),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Id_Nbr(),new ColumnSpacing(1),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Pymnt_Date(),new 
            ColumnSpacing(2),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Ind(),new ColumnSpacing(1),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt(), new 
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(3),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new 
            ColumnSpacing(3),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Int_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(3),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Fed_Wthld_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(3),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code(),NEWLINE,new TabSetting(63),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Nra_Wthld_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(3),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new 
            ColumnSpacing(3),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Local_Wthld(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(3),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #I 1 2
        for (pnd_Work_Area_Pnd_I.setValue(1); condition(pnd_Work_Area_Pnd_I.lessOrEqual(2)); pnd_Work_Area_Pnd_I.nadd(1))
        {
            pnd_Totals_Pnd_Total_Cnt.getValue(pnd_Work_Area_Pnd_I).nadd(1);                                                                                               //Natural: ADD 1 TO #TOTAL-CNT ( #I )
            pnd_Totals_Pnd_Total_Grs.getValue(pnd_Work_Area_Pnd_I).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt());                                               //Natural: ADD #FF3-GROSS-AMT TO #TOTAL-GRS ( #I )
            pnd_Totals_Pnd_Total_Ivc.getValue(pnd_Work_Area_Pnd_I).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt());                                                 //Natural: ADD #FF3-IVC-AMT TO #TOTAL-IVC ( #I )
            pnd_Totals_Pnd_Total_Int.getValue(pnd_Work_Area_Pnd_I).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Int_Amt());                                                 //Natural: ADD #FF3-INT-AMT TO #TOTAL-INT ( #I )
            pnd_Totals_Pnd_Total_Fed.getValue(pnd_Work_Area_Pnd_I).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Fed_Wthld_Amt());                                           //Natural: ADD #FF3-FED-WTHLD-AMT TO #TOTAL-FED ( #I )
            pnd_Totals_Pnd_Total_Nra.getValue(pnd_Work_Area_Pnd_I).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Nra_Wthld_Amt());                                           //Natural: ADD #FF3-NRA-WTHLD-AMT TO #TOTAL-NRA ( #I )
            pnd_Totals_Pnd_Total_Sta.getValue(pnd_Work_Area_Pnd_I).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld());                                             //Natural: ADD #FF3-STATE-WTHLD TO #TOTAL-STA ( #I )
            pnd_Totals_Pnd_Total_Lcl.getValue(pnd_Work_Area_Pnd_I).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Local_Wthld());                                             //Natural: ADD #FF3-LOCAL-WTHLD TO #TOTAL-LCL ( #I )
            pnd_Totals_Pnd_Total_Can.getValue(pnd_Work_Area_Pnd_I).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt());                                           //Natural: ADD #FF3-CAN-WTHLD-AMT TO #TOTAL-CAN ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Check_Trans_Type() throws Exception                                                                                                                  //Natural: CHECK-TRANS-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Work_Area_Pnd_Trans_Desc.reset();                                                                                                                             //Natural: RESET #TRANS-DESC
        DbsUtil.callnat(Twrn0901.class , getCurrentProcessState(), pnd_Work_Area_Pnd_Tax_Year_A, ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Payset_Type(),                     //Natural: CALLNAT 'TWRN0901' #TAX-YEAR-A #FF3-PAYSET-TYPE #TRANS-DESC
            pnd_Work_Area_Pnd_Trans_Desc);
        if (condition(Global.isEscape())) return;
        pnd_Work_Area_Pnd_Sv_Payset.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Payset_Type());                                                                        //Natural: ASSIGN #SV-PAYSET := #FF3-PAYSET-TYPE
    }
    private void sub_Co_Desc() throws Exception                                                                                                                           //Natural: CO-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //*                  -------
        //*  ADD 'X'  9/17/04  RM
        //*  JB1011
        //*  11/2014 END
        short decideConditionsMet350 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SV-COMPANY;//Natural: VALUE 'C'
        if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("C"))))
        {
            decideConditionsMet350++;
            pnd_Work_Area_Pnd_Print_Com.setValue("CREF");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'CREF'
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("L"))))
        {
            decideConditionsMet350++;
            pnd_Work_Area_Pnd_Print_Com.setValue("LIFE");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'LIFE'
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("T"))))
        {
            decideConditionsMet350++;
            pnd_Work_Area_Pnd_Print_Com.setValue("TIAA");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'TIAA'
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("S"))))
        {
            decideConditionsMet350++;
            pnd_Work_Area_Pnd_Print_Com.setValue("TCII");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'TCII'
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("X"))))
        {
            decideConditionsMet350++;
            pnd_Work_Area_Pnd_Print_Com.setValue("TRST");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'TRST'
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("F"))))
        {
            decideConditionsMet350++;
            pnd_Work_Area_Pnd_Print_Com.setValue("FSB ");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'FSB '
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Work_Area_Pnd_Print_Com.setValue("OTHR");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'OTHR'
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM CO-DESC
                    sub_Co_Desc();
                    if (condition(Global.isEscape())) {return;}
                    //*  11/2014 START
                    if (condition(pnd_Work_Area_Pnd_Fatca.getBoolean()))                                                                                                  //Natural: IF #FATCA
                    {
                        pnd_Work_Area_Pnd_Hdg1.setValue("  FATCA TAX  ");                                                                                                 //Natural: ASSIGN #HDG1 := '  FATCA TAX  '
                        //*  11/2014 END
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR / *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 121T 'PAGE' *PAGE-NUMBER ( 1 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 43T 'DAILY ONLINE MAINTENANCE DETAIL REPORT' / 'RUNTIME : ' *TIMX 42T 'FINANCIAL TRANSACTIONS - TAX YEAR' #TAX-YEAR 97T 'DATE RANGE :' #START-DATE 'THRU' #END-DATE /// / 'COMPANY : ' #PRINT-COM // 'SYS.' 7X 'TYPE OF' 8X 'CONTRACT/' 13X 'PAYMENT' 2X 'IVC' 7X 'GROSS' 9X 'TAX FREE' 7X 'INTEREST' 7X 'FEDERAL TAX' 3X 'RES' / 'SRCE' 5X 'TRANSACTION' 5X 'PAYEE CODE' 2X 'TAX-ID-NO' 4X 'DATE' 3X 'IND' 7X 'AMOUNT' 10X 'IVC' 11X 'AMOUNT' 8X 'WITHHOLDING' 3X 'CDE' / 62T '--------------' 3X '-------------' 3X '-------------' 3X '-------------' 2X '---' / 63T #HDG1 8X 'STATE TAX' 7X 'LOCAL TAX' 7X 'CANADIAN' / 64T 'WITHHOLDING' 7X 'WITHHOLDING' 5X 'WITHHOLDING' 4X 'WITHHOLDING' //
                        TabSetting(121),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(43),"DAILY ONLINE MAINTENANCE DETAIL REPORT",NEWLINE,"RUNTIME : ",Global.getTIMX(),new TabSetting(42),"FINANCIAL TRANSACTIONS - TAX YEAR",pnd_Work_Area_Pnd_Tax_Year,new 
                        TabSetting(97),"DATE RANGE :",pnd_Work_Area_Pnd_Start_Date,"THRU",pnd_Work_Area_Pnd_End_Date,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"COMPANY : ",pnd_Work_Area_Pnd_Print_Com,NEWLINE,NEWLINE,"SYS.",new 
                        ColumnSpacing(7),"TYPE OF",new ColumnSpacing(8),"CONTRACT/",new ColumnSpacing(13),"PAYMENT",new ColumnSpacing(2),"IVC",new ColumnSpacing(7),"GROSS",new 
                        ColumnSpacing(9),"TAX FREE",new ColumnSpacing(7),"INTEREST",new ColumnSpacing(7),"FEDERAL TAX",new ColumnSpacing(3),"RES",NEWLINE,"SRCE",new 
                        ColumnSpacing(5),"TRANSACTION",new ColumnSpacing(5),"PAYEE CODE",new ColumnSpacing(2),"TAX-ID-NO",new ColumnSpacing(4),"DATE",new 
                        ColumnSpacing(3),"IND",new ColumnSpacing(7),"AMOUNT",new ColumnSpacing(10),"IVC",new ColumnSpacing(11),"AMOUNT",new ColumnSpacing(8),"WITHHOLDING",new 
                        ColumnSpacing(3),"CDE",NEWLINE,new TabSetting(62),"--------------",new ColumnSpacing(3),"-------------",new ColumnSpacing(3),"-------------",new 
                        ColumnSpacing(3),"-------------",new ColumnSpacing(2),"---",NEWLINE,new TabSetting(63),pnd_Work_Area_Pnd_Hdg1,new ColumnSpacing(8),"STATE TAX",new 
                        ColumnSpacing(7),"LOCAL TAX",new ColumnSpacing(7),"CANADIAN",NEWLINE,new TabSetting(64),"WITHHOLDING",new ColumnSpacing(7),"WITHHOLDING",new 
                        ColumnSpacing(5),"WITHHOLDING",new ColumnSpacing(4),"WITHHOLDING",NEWLINE,NEWLINE);
                    //* */ 66T 'NRA TAX'     8X 'STATE TAX'   7X 'LOCAL TAX'   7X 'CANADIAN'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");
    }
}
