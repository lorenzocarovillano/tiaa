/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:29:57 PM
**        * FROM NATURAL PROGRAM : Twrp0401
************************************************************
**        * FILE NAME            : Twrp0401.java
**        * CLASS NAME           : Twrp0401
**        * INSTANCE NAME        : Twrp0401
************************************************************
**--------------------------------------------------------------------**
* PROGRAM     : TWRP0401
* SYSTEM      : TAXWARS - TAX WITHHOLDING AND REPORTING SYSTEM.
*             :
* FUNCTION    : PROGRAM READS A PARAMETER CARD TO OBTAIN THE
*             : ONE-CHARACTER DELIMITER TO BE USED IN THE OUTPUT FILE.
*             : IF THERE IS NO PARAMETER CARD OR THE FIRST POSITION IS
*             : BLANK, A COMMA WILL BE USED AS THE DEFAULT DELIMITER.
*             : A SECURITY TABLE IS THEN READ IN ORDER TO PRINT A REPORT
*             : AND PRODUCE AN OUTPUT DATASET OF RACF IDS AUTHORIZED TO
*             : PERFORM 'TWRM' MAINTENANCE.  THE DATASET WILL
*             : SUBSEQUENTLY BE DOWNLOADED TO A SERVER.
*             :
* CREATED     : 09/11/2008
* AUTHOR      : ALTHEA A. YOUNG
**--------------------------------------------------------------------**
* HISTORY     :
* 11/21/2014  CTS      RESTOWED WITH NEW TWRL0400 (ADD RASMUJ)
* 11/06/2014  CTS      RESTOWED WITH NEW TWRL0400 (REMOVE ANDERRS
*                                                  MIMS & YOUNGT)
* 02/07/2014  JBREMER  RESTOWED WITH NEW TWRL0400 (ADD BOST)
* 12/17/2013  JBREMER  RESTOWED WITH NEW TWRL0400 (ADD TEWARI WEAVES
*                                                  REMOVE RIVERO)
* 12/15/2011  RSALGADO RESTOWED WITH NEW TWRL0400 (REMOVE LIM)
* 04-15-2009  : ALTHEA A. YOUNG - RE-COMPILED DUE TO TWRL0400, AS
*             :                   REQUESTED BY SECURITY ADMINISTRATION.
* 07/08/15 PAULS - RESTOWED WITH NEW TWRL0400 (REMOVE LEWISSH,
*                  SHRIVNA,TURNERK AND ADD MINUCCI)
* 04/19/2016 PAULS - RESTOWED WITH NEW TWRL0400 (ADD EDUTSEE)
* 01/31/2019  ARIVU    RESTOWED WITH NEW TWRL0400 (ADDED BAPTISD)
**--------------------------------------------------------------------**
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0401 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0400 ldaTwrl0400;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Parm_Card;

    private DbsGroup pnd_Parm_Card__R_Field_1;
    private DbsField pnd_Parm_Card_Pnd_Parm_Delimiter;
    private DbsField pnd_Delimiter;
    private DbsField pnd_I;
    private DbsField pnd_Rec_Count;
    private DbsField pnd_Out_Rec;

    private DbsGroup pnd_Out_Rec__R_Field_2;
    private DbsField pnd_Out_Rec_Pnd_Racf_Id;
    private DbsField pnd_Out_Rec__Filler1;
    private DbsField pnd_Out_Rec_Pnd_Active;
    private DbsField pnd_Out_Rec__Filler2;
    private DbsField pnd_Out_Rec_Pnd_Update;
    private DbsField pnd_Out_Rec__Filler3;

    private DbsGroup pnd_Out_Rec__R_Field_3;
    private DbsField pnd_Out_Rec_Pnd_Outrec_Footnote;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0400 = new LdaTwrl0400();
        registerRecord(ldaTwrl0400);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Parm_Card = localVariables.newFieldInRecord("pnd_Parm_Card", "#PARM-CARD", FieldType.STRING, 80);

        pnd_Parm_Card__R_Field_1 = localVariables.newGroupInRecord("pnd_Parm_Card__R_Field_1", "REDEFINE", pnd_Parm_Card);
        pnd_Parm_Card_Pnd_Parm_Delimiter = pnd_Parm_Card__R_Field_1.newFieldInGroup("pnd_Parm_Card_Pnd_Parm_Delimiter", "#PARM-DELIMITER", FieldType.STRING, 
            1);
        pnd_Delimiter = localVariables.newFieldInRecord("pnd_Delimiter", "#DELIMITER", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Rec_Count = localVariables.newFieldInRecord("pnd_Rec_Count", "#REC-COUNT", FieldType.NUMERIC, 5);
        pnd_Out_Rec = localVariables.newFieldInRecord("pnd_Out_Rec", "#OUT-REC", FieldType.STRING, 56);

        pnd_Out_Rec__R_Field_2 = localVariables.newGroupInRecord("pnd_Out_Rec__R_Field_2", "REDEFINE", pnd_Out_Rec);
        pnd_Out_Rec_Pnd_Racf_Id = pnd_Out_Rec__R_Field_2.newFieldInGroup("pnd_Out_Rec_Pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);
        pnd_Out_Rec__Filler1 = pnd_Out_Rec__R_Field_2.newFieldInGroup("pnd_Out_Rec__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Out_Rec_Pnd_Active = pnd_Out_Rec__R_Field_2.newFieldInGroup("pnd_Out_Rec_Pnd_Active", "#ACTIVE", FieldType.STRING, 6);
        pnd_Out_Rec__Filler2 = pnd_Out_Rec__R_Field_2.newFieldInGroup("pnd_Out_Rec__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_Out_Rec_Pnd_Update = pnd_Out_Rec__R_Field_2.newFieldInGroup("pnd_Out_Rec_Pnd_Update", "#UPDATE", FieldType.STRING, 39);
        pnd_Out_Rec__Filler3 = pnd_Out_Rec__R_Field_2.newFieldInGroup("pnd_Out_Rec__Filler3", "_FILLER3", FieldType.STRING, 1);

        pnd_Out_Rec__R_Field_3 = localVariables.newGroupInRecord("pnd_Out_Rec__R_Field_3", "REDEFINE", pnd_Out_Rec);
        pnd_Out_Rec_Pnd_Outrec_Footnote = pnd_Out_Rec__R_Field_3.newFieldInGroup("pnd_Out_Rec_Pnd_Outrec_Footnote", "#OUTREC-FOOTNOTE", FieldType.STRING, 
            56);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0400.initializeValues();

        localVariables.reset();
        pnd_I.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0401() throws Exception
    {
        super("Twrp0401");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *--------
        //*  JCL
        //*  DATA REPORT                                                                                                                                                  //Natural: FORMAT ( 00 ) PS = 60 LS = 133 ZP = ON
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) PS = 0 LS = 133 ZP = ON SF = 1
        //* *============**
        //*  MAIN PROGRAM *
        //* *============**
                                                                                                                                                                          //Natural: PERFORM GET-DELIMITER
        sub_Get_Delimiter();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM INITIALIZATION
        sub_Initialization();
        if (condition(Global.isEscape())) {return;}
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 #MAX-INDEX
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaTwrl0400.getPnd_Max_Index())); pnd_I.nadd(1))
        {
            pnd_Rec_Count.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #REC-COUNT
            pnd_Out_Rec_Pnd_Racf_Id.setValue(ldaTwrl0400.getPnd_Auth_Users().getValue(pnd_I));                                                                            //Natural: ASSIGN #RACF-ID := #AUTH-USERS ( #I )
            pnd_Out_Rec_Pnd_Active.setValue("Y");                                                                                                                         //Natural: ASSIGN #ACTIVE := 'Y'
            pnd_Out_Rec_Pnd_Update.setValue("Via Security Administration TWRM Access");                                                                                   //Natural: ASSIGN #UPDATE := 'Via Security Administration TWRM Access'
            getReports().display(1, "RACF-ID",                                                                                                                            //Natural: DISPLAY ( 01 ) 'RACF-ID' #RACF-ID 'Active' #ACTIVE 'Update' #UPDATE
            		pnd_Out_Rec_Pnd_Racf_Id,"Active",
            		pnd_Out_Rec_Pnd_Active,"Update",
            		pnd_Out_Rec_Pnd_Update);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD
            sub_Process_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,NEWLINE,"Total RACF-IDs On File =",pnd_Rec_Count);                                                                                  //Natural: WRITE ( 01 ) // 'Total RACF-IDs On File =' #REC-COUNT
        if (Global.isEscape()) return;
        DbsUtil.stop();  if (true) return;                                                                                                                                //Natural: STOP
        //* *---------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DELIMITER
        //* *----------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZATION
        //* *----------------------------**
        //* *----------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-RECORD
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'TaxWaRS Authorized RACF-IDs' 120T 'REPORT: RPT1' //
    }
    private void sub_Get_Delimiter() throws Exception                                                                                                                     //Natural: GET-DELIMITER
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------**
        getWorkFiles().read(1, pnd_Parm_Card);                                                                                                                            //Natural: READ WORK 01 ONCE #PARM-CARD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            pnd_Parm_Card_Pnd_Parm_Delimiter.setValue(",");                                                                                                               //Natural: ASSIGN #PARM-DELIMITER := ','
        }                                                                                                                                                                 //Natural: END-ENDFILE
        if (condition(pnd_Parm_Card_Pnd_Parm_Delimiter.equals(" ")))                                                                                                      //Natural: IF #PARM-DELIMITER = ' '
        {
            pnd_Parm_Card_Pnd_Parm_Delimiter.setValue(",");                                                                                                               //Natural: ASSIGN #PARM-DELIMITER := ','
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Delimiter.setValue(pnd_Parm_Card_Pnd_Parm_Delimiter);                                                                                                         //Natural: ASSIGN #DELIMITER := #PARM-DELIMITER
        if (condition(pnd_Delimiter.equals(",") || pnd_Delimiter.equals(";") || pnd_Delimiter.equals(":")))                                                               //Natural: IF #DELIMITER = ',' OR = ';' OR = ':'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "Invalid Delimiter Requested!  Must be comma, colon,","or semi-colon.  Default comma will be used.");                                   //Natural: WRITE 'Invalid Delimiter Requested!  Must be comma, colon,' 'or semi-colon.  Default comma will be used.'
            if (Global.isEscape()) return;
            pnd_Delimiter.setValue(",");                                                                                                                                  //Natural: ASSIGN #DELIMITER := ','
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, NEWLINE,"Delimiter used in output dataset is",pnd_Delimiter);                                                                               //Natural: WRITE / 'Delimiter used in output dataset is' #DELIMITER
        if (Global.isEscape()) return;
        //*  GET-DELIMITER
    }
    private void sub_Initialization() throws Exception                                                                                                                    //Natural: INITIALIZATION
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Out_Rec_Pnd_Racf_Id.setValue("RACF-ID");                                                                                                                      //Natural: ASSIGN #RACF-ID := 'RACF-ID'
        pnd_Out_Rec_Pnd_Active.setValue("Active");                                                                                                                        //Natural: ASSIGN #ACTIVE := 'Active'
        pnd_Out_Rec_Pnd_Update.setValue("Update");                                                                                                                        //Natural: ASSIGN #UPDATE := 'Update'
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD
        sub_Process_Record();
        if (condition(Global.isEscape())) {return;}
        //*  INITIALIZATION
    }
    private void sub_Process_Record() throws Exception                                                                                                                    //Natural: PROCESS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------**
        DbsUtil.examine(new ExamineSource(pnd_Out_Rec_Pnd_Racf_Id,true), new ExamineSearch(" "), new ExamineReplace("*"));                                                //Natural: EXAMINE FULL #RACF-ID FOR ' ' REPLACE '*'
        DbsUtil.examine(new ExamineSource(pnd_Out_Rec_Pnd_Active,true), new ExamineSearch(" "), new ExamineReplace("*"));                                                 //Natural: EXAMINE FULL #ACTIVE FOR ' ' REPLACE '*'
        DbsUtil.examine(new ExamineSource(pnd_Out_Rec_Pnd_Update,true), new ExamineSearch(" "), new ExamineReplace("*"));                                                 //Natural: EXAMINE FULL #UPDATE FOR ' ' REPLACE '*'
        pnd_Out_Rec.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Out_Rec_Pnd_Racf_Id, pnd_Delimiter, pnd_Out_Rec_Pnd_Active, pnd_Delimiter,               //Natural: COMPRESS #RACF-ID #DELIMITER #ACTIVE #DELIMITER #UPDATE #DELIMITER INTO #OUT-REC LEAVING NO
            pnd_Out_Rec_Pnd_Update, pnd_Delimiter));
        DbsUtil.examine(new ExamineSource(pnd_Out_Rec,true), new ExamineSearch("*"), new ExamineReplace(" "));                                                            //Natural: EXAMINE FULL #OUT-REC FOR '*' REPLACE ' '
        getWorkFiles().write(2, false, pnd_Out_Rec);                                                                                                                      //Natural: WRITE WORK FILE 02 #OUT-REC
        pnd_Out_Rec.reset();                                                                                                                                              //Natural: RESET #OUT-REC
        //*  PROCESS-RECORD
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133 ZP=ON");
        Global.format(1, "PS=0 LS=133 ZP=ON SF=1");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(53),"TaxWaRS Authorized RACF-IDs",new TabSetting(120),"REPORT: RPT1",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, "RACF-ID",
        		pnd_Out_Rec_Pnd_Racf_Id,"Active",
        		pnd_Out_Rec_Pnd_Active,"Update",
        		pnd_Out_Rec_Pnd_Update);
    }
}
