/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:35:57 PM
**        * FROM NATURAL PROGRAM : Twrp3533
************************************************************
**        * FILE NAME            : Twrp3533.java
**        * CLASS NAME           : Twrp3533
**        * INSTANCE NAME        : Twrp3533
************************************************************
************************************************************************
** PROGRAM : TWRP3533
** SYSTEM  : TAXWARS
** FUNCTION: 480.6 CORRECTION REPORTING
** HISTORY.....:
**
** 05/22/09 - MS - USED FOR TAX YEARS 2002 THRY 2007.
** 12/06/05 - BK. TAX YEAR ADDED TO TWRNCOMP PARMS
** 11/11/05    MS RECOMPILED - CHANGE IN TWRL5001
** 09/18/02    E.BOTTERI - THIS IS A NEW MODULE TO HANDLE PUERTO RICO
**             CORRECTION REPORTING FOR >= 2002.  THIS PROGRAM IS
**             FETCHED FROM DRIVER TWRP3530 BASED ON REPORTING YEAR.
**
** 11/19/02 - MN - RECOMPILED DUE TO FIELDS ADDED TO TWRL5001
** 01/30/08 - AY - REVISED TO ACCOMMODATE FORM 480.7C, WHICH REPLACES
**                 FORMS 480.6A AND 480.6B AS OF TAX YEAR 2007.
**                 REPLACED TWRL5000 WITH TWRAFRMN / TWRNFRMN.
**
** 11/24/2020 - PALDE - RESTOW FOR IRS REPORTING 2020 CHANGES
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3533 extends BLNatBase
{
    // Data Areas
    private PdaTwracomp pdaTwracomp;
    private PdaTwratin pdaTwratin;
    private PdaTwrafrmn pdaTwrafrmn;
    private LdaTwrl5001 ldaTwrl5001;
    private LdaTwrl9710 ldaTwrl9710;
    private LdaTwrl9715 ldaTwrl9715;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_form_U;
    private DbsField form_U_Tirf_Lu_User;
    private DbsField form_U_Tirf_Lu_Ts;
    private DbsField form_U_Tirf_Irs_Rpt_Ind;
    private DbsField form_U_Tirf_Irs_Rpt_Date;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;
    private DbsField pnd_Ws_Const_Pnd_Irsr_Max_Lines;
    private DbsField pnd_Ws_Const_Pnd_Irsw_Max_Lines;
    private DbsField pnd_Ws_Const_Pnd_Irsg_Max_Lines;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Input_Parms;
    private DbsField pnd_Ws_Pnd_Form;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_Datx;
    private DbsField pnd_Ws_Pnd_Timx;
    private DbsField pnd_Ws_Pnd_Prev_Rpt_Date;
    private DbsField pnd_Ws_Pnd_Company_Break;
    private DbsField pnd_Ws_Pnd_Form_Type;
    private DbsField pnd_Ws_Pnd_Terminate;
    private DbsField pnd_Ws_Pnd_Accepted_Form_Cnt;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_K;
    private DbsField pnd_Ws_Pnd_Err;
    private DbsField pnd_Ws_Pnd_S2;
    private DbsField pnd_Ws_Pnd_S3_Start;
    private DbsField pnd_Ws_Pnd_S3_End;
    private DbsField pnd_Ws_Pnd_S4_Start;
    private DbsField pnd_Ws_Pnd_S4_End;
    private DbsField pnd_Ws_Pnd_Company_Line;

    private DbsGroup pnd_Case_Fields;
    private DbsField pnd_Case_Fields_Pnd_Active_Isn;
    private DbsField pnd_Case_Fields_Pnd_Prior_Held;
    private DbsField pnd_Case_Fields_Pnd_Prior_Reported;
    private DbsField pnd_Case_Fields_Pnd_Prior_Record;
    private DbsField pnd_Case_Fields_Pnd_No_Change;
    private DbsField pnd_Case_Fields_Pnd_Recon_Summary;
    private DbsField pnd_Case_Fields_Pnd_Recon_Detail;
    private DbsField pnd_Case_Fields_Pnd_Update_Ind;

    private DbsGroup pnd_Report_Indexes;
    private DbsField pnd_Report_Indexes_Pnd_Irsr1;
    private DbsField pnd_Report_Indexes_Pnd_Irsw1;
    private DbsField pnd_Report_Indexes_Pnd_Irsw2;
    private DbsField pnd_Report_Indexes_Pnd_Irsg1;

    private DbsGroup pnd_Irsr;
    private DbsField pnd_Irsr_Pnd_Header1;
    private DbsField pnd_Irsr_Pnd_Header2;

    private DbsGroup pnd_Irsr_Pnd_Totals;
    private DbsField pnd_Irsr_Pnd_Form_Cnt;
    private DbsField pnd_Irsr_Pnd_Gross_Amt;
    private DbsField pnd_Irsr_Pnd_Int_Amt;
    private DbsField pnd_Irsr_Pnd_Tax_Wthld;

    private DbsGroup pnd_Irsw;
    private DbsField pnd_Irsw_Pnd_Header;

    private DbsGroup pnd_Irsw_Pnd_Totals;
    private DbsField pnd_Irsw_Pnd_Form_Cnt;
    private DbsField pnd_Irsw_Pnd_Gross_Amt;
    private DbsField pnd_Irsw_Pnd_Tax_Wthld;

    private DbsGroup pnd_Irsg;
    private DbsField pnd_Irsg_Pnd_Header;

    private DbsGroup pnd_Irsg_Pnd_Totals;
    private DbsField pnd_Irsg_Pnd_Form_Cnt;
    private DbsField pnd_Irsg_Pnd_Gross_Amt;
    private DbsField pnd_Irsg_Pnd_Tax_Wthld;

    private DbsGroup pnd_Recon_Case;
    private DbsField pnd_Recon_Case_Pnd_Tin;
    private DbsField pnd_Recon_Case_Pnd_Cntrct_Py;

    private DbsGroup pnd_Recon_Case_Pnd_Recon_Case_Detail;
    private DbsField pnd_Recon_Case_Pnd_Header;
    private DbsField pnd_Recon_Case_Pnd_Form_Cnt;
    private DbsField pnd_Recon_Case_Pnd_Gross_Amt;
    private DbsField pnd_Recon_Case_Pnd_Tax_Wthld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwracomp = new PdaTwracomp(localVariables);
        pdaTwratin = new PdaTwratin(localVariables);
        pdaTwrafrmn = new PdaTwrafrmn(localVariables);
        ldaTwrl5001 = new LdaTwrl5001();
        registerRecord(ldaTwrl5001);
        ldaTwrl9710 = new LdaTwrl9710();
        registerRecord(ldaTwrl9710);
        registerRecord(ldaTwrl9710.getVw_form());
        ldaTwrl9715 = new LdaTwrl9715();
        registerRecord(ldaTwrl9715);
        registerRecord(ldaTwrl9715.getVw_form_R());

        // Local Variables

        vw_form_U = new DataAccessProgramView(new NameInfo("vw_form_U", "FORM-U"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form_U_Tirf_Lu_User = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Lu_User", "TIRF-LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_LU_USER");
        form_U_Tirf_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        form_U_Tirf_Lu_Ts = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Lu_Ts", "TIRF-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "TIRF_LU_TS");
        form_U_Tirf_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        form_U_Tirf_Irs_Rpt_Ind = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Irs_Rpt_Ind", "TIRF-IRS-RPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_IND");
        form_U_Tirf_Irs_Rpt_Ind.setDdmHeader("IRS/RPT/IND");
        form_U_Tirf_Irs_Rpt_Date = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Irs_Rpt_Date", "TIRF-IRS-RPT-DATE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_DATE");
        form_U_Tirf_Irs_Rpt_Date.setDdmHeader("IRS/RPT/DATE");
        registerRecord(vw_form_U);

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_Pnd_Irsr_Max_Lines = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_Irsr_Max_Lines", "#IRSR-MAX-LINES", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Ws_Const_Pnd_Irsw_Max_Lines = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_Irsw_Max_Lines", "#IRSW-MAX-LINES", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Ws_Const_Pnd_Irsg_Max_Lines = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_Irsg_Max_Lines", "#IRSG-MAX-LINES", FieldType.PACKED_DECIMAL, 
            3);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Input_Parms = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Ws_Pnd_Form = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Form", "#FORM", FieldType.STRING, 6);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Datx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Datx", "#DATX", FieldType.DATE);
        pnd_Ws_Pnd_Timx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Timx", "#TIMX", FieldType.TIME);
        pnd_Ws_Pnd_Prev_Rpt_Date = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Prev_Rpt_Date", "#PREV-RPT-DATE", FieldType.DATE, new DbsArrayController(5, 
            6));
        pnd_Ws_Pnd_Company_Break = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Break", "#COMPANY-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Form_Type = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Form_Type", "#FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_Terminate = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Terminate", "#TERMINATE", FieldType.NUMERIC, 3);
        pnd_Ws_Pnd_Accepted_Form_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Accepted_Form_Cnt", "#ACCEPTED-FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_K = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Err = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Err", "#ERR", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S2", "#S2", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S3_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S3_Start", "#S3-START", FieldType.STRING, 8);
        pnd_Ws_Pnd_S3_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S3_End", "#S3-END", FieldType.STRING, 8);
        pnd_Ws_Pnd_S4_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S4_Start", "#S4-START", FieldType.STRING, 32);
        pnd_Ws_Pnd_S4_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S4_End", "#S4-END", FieldType.STRING, 32);
        pnd_Ws_Pnd_Company_Line = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Line", "#COMPANY-LINE", FieldType.STRING, 72);

        pnd_Case_Fields = localVariables.newGroupInRecord("pnd_Case_Fields", "#CASE-FIELDS");
        pnd_Case_Fields_Pnd_Active_Isn = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Active_Isn", "#ACTIVE-ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_Case_Fields_Pnd_Prior_Held = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Prior_Held", "#PRIOR-HELD", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Prior_Reported = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Prior_Reported", "#PRIOR-REPORTED", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Prior_Record = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Prior_Record", "#PRIOR-RECORD", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_No_Change = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_No_Change", "#NO-CHANGE", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Recon_Summary = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Recon_Summary", "#RECON-SUMMARY", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Recon_Detail = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Recon_Detail", "#RECON-DETAIL", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Update_Ind = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Update_Ind", "#UPDATE-IND", FieldType.STRING, 1);

        pnd_Report_Indexes = localVariables.newGroupInRecord("pnd_Report_Indexes", "#REPORT-INDEXES");
        pnd_Report_Indexes_Pnd_Irsr1 = pnd_Report_Indexes.newFieldInGroup("pnd_Report_Indexes_Pnd_Irsr1", "#IRSR1", FieldType.PACKED_DECIMAL, 7);
        pnd_Report_Indexes_Pnd_Irsw1 = pnd_Report_Indexes.newFieldInGroup("pnd_Report_Indexes_Pnd_Irsw1", "#IRSW1", FieldType.PACKED_DECIMAL, 7);
        pnd_Report_Indexes_Pnd_Irsw2 = pnd_Report_Indexes.newFieldInGroup("pnd_Report_Indexes_Pnd_Irsw2", "#IRSW2", FieldType.PACKED_DECIMAL, 7);
        pnd_Report_Indexes_Pnd_Irsg1 = pnd_Report_Indexes.newFieldInGroup("pnd_Report_Indexes_Pnd_Irsg1", "#IRSG1", FieldType.PACKED_DECIMAL, 7);

        pnd_Irsr = localVariables.newGroupArrayInRecord("pnd_Irsr", "#IRSR", new DbsArrayController(1, 7));
        pnd_Irsr_Pnd_Header1 = pnd_Irsr.newFieldInGroup("pnd_Irsr_Pnd_Header1", "#HEADER1", FieldType.STRING, 19);
        pnd_Irsr_Pnd_Header2 = pnd_Irsr.newFieldInGroup("pnd_Irsr_Pnd_Header2", "#HEADER2", FieldType.STRING, 19);

        pnd_Irsr_Pnd_Totals = pnd_Irsr.newGroupArrayInGroup("pnd_Irsr_Pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Irsr_Pnd_Form_Cnt = pnd_Irsr_Pnd_Totals.newFieldInGroup("pnd_Irsr_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Irsr_Pnd_Gross_Amt = pnd_Irsr_Pnd_Totals.newFieldInGroup("pnd_Irsr_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Irsr_Pnd_Int_Amt = pnd_Irsr_Pnd_Totals.newFieldInGroup("pnd_Irsr_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Irsr_Pnd_Tax_Wthld = pnd_Irsr_Pnd_Totals.newFieldInGroup("pnd_Irsr_Pnd_Tax_Wthld", "#TAX-WTHLD", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Irsw = localVariables.newGroupArrayInRecord("pnd_Irsw", "#IRSW", new DbsArrayController(1, 6));
        pnd_Irsw_Pnd_Header = pnd_Irsw.newFieldInGroup("pnd_Irsw_Pnd_Header", "#HEADER", FieldType.STRING, 14);

        pnd_Irsw_Pnd_Totals = pnd_Irsw.newGroupArrayInGroup("pnd_Irsw_Pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Irsw_Pnd_Form_Cnt = pnd_Irsw_Pnd_Totals.newFieldInGroup("pnd_Irsw_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Irsw_Pnd_Gross_Amt = pnd_Irsw_Pnd_Totals.newFieldInGroup("pnd_Irsw_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Irsw_Pnd_Tax_Wthld = pnd_Irsw_Pnd_Totals.newFieldInGroup("pnd_Irsw_Pnd_Tax_Wthld", "#TAX-WTHLD", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Irsg = localVariables.newGroupArrayInRecord("pnd_Irsg", "#IRSG", new DbsArrayController(1, 3));
        pnd_Irsg_Pnd_Header = pnd_Irsg.newFieldInGroup("pnd_Irsg_Pnd_Header", "#HEADER", FieldType.STRING, 19);

        pnd_Irsg_Pnd_Totals = pnd_Irsg.newGroupArrayInGroup("pnd_Irsg_Pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Irsg_Pnd_Form_Cnt = pnd_Irsg_Pnd_Totals.newFieldInGroup("pnd_Irsg_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Irsg_Pnd_Gross_Amt = pnd_Irsg_Pnd_Totals.newFieldInGroup("pnd_Irsg_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Irsg_Pnd_Tax_Wthld = pnd_Irsg_Pnd_Totals.newFieldInGroup("pnd_Irsg_Pnd_Tax_Wthld", "#TAX-WTHLD", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Recon_Case = localVariables.newGroupInRecord("pnd_Recon_Case", "#RECON-CASE");
        pnd_Recon_Case_Pnd_Tin = pnd_Recon_Case.newFieldInGroup("pnd_Recon_Case_Pnd_Tin", "#TIN", FieldType.STRING, 11);
        pnd_Recon_Case_Pnd_Cntrct_Py = pnd_Recon_Case.newFieldInGroup("pnd_Recon_Case_Pnd_Cntrct_Py", "#CNTRCT-PY", FieldType.STRING, 12);

        pnd_Recon_Case_Pnd_Recon_Case_Detail = pnd_Recon_Case.newGroupArrayInGroup("pnd_Recon_Case_Pnd_Recon_Case_Detail", "#RECON-CASE-DETAIL", new DbsArrayController(1, 
            3));
        pnd_Recon_Case_Pnd_Header = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_Header", "#HEADER", FieldType.STRING, 10);
        pnd_Recon_Case_Pnd_Form_Cnt = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Recon_Case_Pnd_Gross_Amt = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 
            14, 2);
        pnd_Recon_Case_Pnd_Tax_Wthld = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_Tax_Wthld", "#TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            11, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_form_U.reset();

        ldaTwrl5001.initializeValues();
        ldaTwrl9710.initializeValues();
        ldaTwrl9715.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
        pnd_Ws_Const_Pnd_Irsr_Max_Lines.setInitialValue(7);
        pnd_Ws_Const_Pnd_Irsw_Max_Lines.setInitialValue(6);
        pnd_Ws_Const_Pnd_Irsg_Max_Lines.setInitialValue(3);
        pnd_Ws_Pnd_Company_Break.setInitialValue(true);
        pnd_Irsr_Pnd_Header1.getValue(1).setInitialValue("Form Database Total");
        pnd_Irsr_Pnd_Header1.getValue(2).setInitialValue("Actve Records");
        pnd_Irsr_Pnd_Header1.getValue(3).setInitialValue("No Change");
        pnd_Irsr_Pnd_Header1.getValue(4).setInitialValue("Non Rept Zero Forms");
        pnd_Irsr_Pnd_Header1.getValue(5).setInitialValue("Prev Rejected Forms");
        pnd_Irsr_Pnd_Header1.getValue(6).setInitialValue("New  Rejected Forms");
        pnd_Irsr_Pnd_Header1.getValue(7).setInitialValue("Accepted Forms");
        pnd_Irsr_Pnd_Header2.getValue(2).setInitialValue(" (Prev. Reported)");
        pnd_Irsw_Pnd_Header.getValue(1).setInitialValue("Accepted");
        pnd_Irsw_Pnd_Header.getValue(2).setInitialValue("Prev. Reported");
        pnd_Irsw_Pnd_Header.getValue(3).setInitialValue("Net Change");
        pnd_Irsw_Pnd_Header.getValue(4).setInitialValue("Rejected");
        pnd_Irsw_Pnd_Header.getValue(5).setInitialValue("Prev. Reported");
        pnd_Irsw_Pnd_Header.getValue(6).setInitialValue("Net Change");
        pnd_Irsg_Pnd_Header.getValue(1).setInitialValue("Form Database Total");
        pnd_Irsg_Pnd_Header.getValue(2).setInitialValue("Prev. Reconciled");
        pnd_Irsg_Pnd_Header.getValue(3).setInitialValue("Net Change");
        pnd_Recon_Case_Pnd_Header.getValue(1).setInitialValue("New Form");
        pnd_Recon_Case_Pnd_Header.getValue(2).setInitialValue("Original");
        pnd_Recon_Case_Pnd_Header.getValue(3).setInitialValue("Net Change");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3533() throws Exception
    {
        super("Twrp3533");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp3533|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 80 ZP = ON;//Natural: FORMAT ( 02 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 03 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 04 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 05 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 06 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 07 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 08 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 09 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 10 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 11 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 12 ) PS = 58 LS = 133 ZP = ON
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: IF *DEVICE = 'BATCH'
                {
                    //*  SET DELIMITER MODE FOR BATCH INPUT
                    setControl("D");                                                                                                                                      //Natural: SET CONTROL 'D'
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  EB, FETCHED FROM TWRP3530
                }                                                                                                                                                         //Natural: END-IF
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 37T 'TaxWaRS' 68T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 34T 'Control Report' 68T 'Report: RPT1' / //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 02 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'Puerto Rico Correction Reporting Summary' 120T 'Report: RPT2' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 03 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'Puerto Rico Correction Reporting Summary' 120T 'Report: RPT3' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 04 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 43T 'Puerto Rico Correction Reporting Rejected Detail' 120T 'Report: RPT4' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 05 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 05 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 43T 'Puerto Rico Correction Reporting Rejected Detail' 120T 'Report: RPT5' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 06 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 06 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 43T 'Puerto Rico Correction Reporting Accepted Detail' 120T 'Report: RPT6' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 07 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 07 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 43T 'Puerto Rico Correction Reporting Accepted Detail' 120T 'Report: RPT7' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 08 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 08 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 38T 'Puerto Rico Correction Withholding Reconciliation Summary' 120T 'Report: RPT8' / 43T 'Reporting period' #WS.#PREV-RPT-DATE ( #FORM-TYPE ) ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 09 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 09 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 34T 'Puerto Rico Correction Withholding Reconciliation' 'Accepted Detail' 120T 'Report: RPT9' / 43T 'Reporting period' #WS.#PREV-RPT-DATE ( #FORM-TYPE ) ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 10 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 118T 'Page  :' *PAGE-NUMBER ( 10 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 34T 'Puerto Rico Correction Withholding Reconciliation' 'Rejected Detail' 118T 'Report:  RPT10' / 43T 'Reporting period' #WS.#PREV-RPT-DATE ( #FORM-TYPE ) ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 11 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 118T 'Page  :' *PAGE-NUMBER ( 11 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 42T 'Purto Rico General Ledger Reconciliation Summary' 118T 'Report:  RPT11' / 43T 'Reporting period' #WS.#PREV-RPT-DATE ( #FORM-TYPE ) ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 12 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 118T 'Page  :' *PAGE-NUMBER ( 12 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 43T 'Purto Rico General Ledger Reconciliation Detail' 118T 'Report:  RPT12' / 43T 'Reporting period' #WS.#PREV-RPT-DATE ( #FORM-TYPE ) ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) / #WS.#COMPANY-LINE //
                //* ***************
                //*  MAIN PROGRAM *
                //* ***************
                //*  PERFORM PROCESS-INPUT-PARMS
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_Form,pnd_Ws_Pnd_Tax_Year);                                                                  //Natural: INPUT #WS.#FORM #WS.#TAX-YEAR
                pnd_Ws_Pnd_S3_Start.setValueEdited(pnd_Ws_Pnd_Tax_Year,new ReportEditMask("9999"));                                                                       //Natural: MOVE EDITED #WS.#TAX-YEAR ( EM = 9999 ) TO #WS.#S3-START
                //*  01/30/08 - AY
                pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Tax_Year_A().setValueEdited(pnd_Ws_Pnd_Tax_Year,new ReportEditMask("9999"));                                              //Natural: MOVE EDITED #WS.#TAX-YEAR ( EM = 9999 ) TO #TWRAFRMN.#TAX-YEAR-A
                pnd_Ws_Pnd_S3_End.setValue(pnd_Ws_Pnd_S3_Start);                                                                                                          //Natural: ASSIGN #WS.#S3-END := #WS.#S3-START
                setValueToSubstring("A05",pnd_Ws_Pnd_S3_Start,5,3);                                                                                                       //Natural: MOVE 'A05' TO SUBSTR ( #WS.#S3-START,5,3 )
                setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Ws_Pnd_S3_Start,8,1);                                                                                     //Natural: MOVE LOW-VALUES TO SUBSTR ( #WS.#S3-START,8,1 )
                setValueToSubstring("A06",pnd_Ws_Pnd_S3_End,5,3);                                                                                                         //Natural: MOVE 'A06' TO SUBSTR ( #WS.#S3-END,5,3 )
                setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Ws_Pnd_S3_End,8,1);                                                                                      //Natural: MOVE HIGH-VALUES TO SUBSTR ( #WS.#S3-END,8,1 )
                //*  01/30/08 - AY
                DbsUtil.callnat(Twrnfrmn.class , getCurrentProcessState(), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNFRMN' USING #TWRAFRMN.#INPUT-PARMS ( AD = O ) #TWRAFRMN.#OUTPUT-DATA ( AD = M )
                    pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                ldaTwrl9710.getVw_form().startDatabaseRead                                                                                                                //Natural: READ FORM BY TIRF-SUPERDE-3 = #WS.#S3-START THRU #WS.#S3-END
                (
                "RD_FORM",
                new Wc[] { new Wc("TIRF_SUPERDE_3", ">=", pnd_Ws_Pnd_S3_Start, "And", WcType.BY) ,
                new Wc("TIRF_SUPERDE_3", "<=", pnd_Ws_Pnd_S3_End, WcType.BY) },
                new Oc[] { new Oc("TIRF_SUPERDE_3", "ASC") }
                );
                boolean endOfDataRdForm = true;
                boolean firstRdForm = true;
                RD_FORM:
                while (condition(ldaTwrl9710.getVw_form().readNextRow("RD_FORM")))
                {
                    CheckAtStartofData712();

                    if (condition(ldaTwrl9710.getVw_form().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventRd_Form();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataRdForm = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    if (condition(pnd_Ws_Pnd_Company_Break.getBoolean()))                                                                                                 //Natural: AT START OF DATA;//Natural: AT BREAK OF FORM.TIRF-COMPANY-CDE;//Natural: AT BREAK OF FORM.TIRF-FORM-TYPE;//Natural: IF #WS.#COMPANY-BREAK
                    {
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMPANY
                        sub_Process_Company();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Report_Indexes_Pnd_Irsr1.setValue(1);                                                                                                             //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 1
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                    sub_Accum_Irsr();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  ALREADY RECONCILED
                    if (condition(ldaTwrl9710.getForm_Tirf_Irs_Rpt_Ind().notEquals(" ")))                                                                                 //Natural: IF FORM.TIRF-IRS-RPT-IND NE ' '
                    {
                        //*  FOR GENERAL LEDGER
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSG
                        sub_Accum_Irsg();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ALREADY REPORTED
                    if (condition(! (ldaTwrl9710.getForm_Tirf_Irs_Rpt_Ind().equals(" ") || ldaTwrl9710.getForm_Tirf_Irs_Rpt_Ind().equals("H"))))                          //Natural: IF NOT FORM.TIRF-IRS-RPT-IND = ' ' OR = 'H'
                    {
                        pnd_Report_Indexes_Pnd_Irsr1.setValue(2);                                                                                                         //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 2
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                        sub_Accum_Irsr();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Case_Fields.resetInitial();                                                                                                                       //Natural: RESET INITIAL #CASE-FIELDS
                    pnd_Case_Fields_Pnd_Active_Isn.setValue(ldaTwrl9710.getVw_form().getAstISN("RD_FORM"));                                                               //Natural: ASSIGN #CASE-FIELDS.#ACTIVE-ISN := *ISN ( RD-FORM. )
                    pnd_Ws_Pnd_S4_End.setValue(ldaTwrl9710.getForm_Tirf_Superde_4());                                                                                     //Natural: ASSIGN #WS.#S4-END := #WS.#S4-START := FORM.TIRF-SUPERDE-4
                    pnd_Ws_Pnd_S4_Start.setValue(ldaTwrl9710.getForm_Tirf_Superde_4());
                                                                                                                                                                          //Natural: PERFORM PRIOR-REPORTED
                    sub_Prior_Reported();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    if (condition(ldaTwrl9710.getForm_Tirf_Empty_Form().getBoolean() && ! (pnd_Case_Fields_Pnd_Prior_Reported.getBoolean())))                             //Natural: IF FORM.TIRF-EMPTY-FORM AND NOT #CASE-FIELDS.#PRIOR-REPORTED
                    {
                        pnd_Report_Indexes_Pnd_Irsr1.setValue(4);                                                                                                         //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 4
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                        sub_Accum_Irsr();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Case_Fields_Pnd_Prior_Record.setValue(pnd_Case_Fields_Pnd_Prior_Reported.getBoolean());                                                           //Natural: ASSIGN #CASE-FIELDS.#PRIOR-RECORD := #CASE-FIELDS.#PRIOR-REPORTED
                                                                                                                                                                          //Natural: PERFORM SETUP-RECON-CASE
                    sub_Setup_Recon_Case();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    if (condition(! (pnd_Case_Fields_Pnd_Prior_Held.getBoolean())))                                                                                       //Natural: IF NOT #CASE-FIELDS.#PRIOR-HELD
                    {
                                                                                                                                                                          //Natural: PERFORM IRSG-DETAIL
                        sub_Irsg_Detail();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    //*  RECORDS IN ERROR
                    if (condition(ldaTwrl9710.getForm_Tirf_Irs_Reject_Ind().equals("Y")))                                                                                 //Natural: IF FORM.TIRF-IRS-REJECT-IND = 'Y'
                    {
                                                                                                                                                                          //Natural: PERFORM IRSR-DETAIL-REJECT
                        sub_Irsr_Detail_Reject();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM IRSW-DETAIL-REJECT
                        sub_Irsw_Detail_Reject();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //*  OLD REJECTS
                        if (condition(ldaTwrl9710.getForm_Tirf_Irs_Rpt_Ind().equals("H")))                                                                                //Natural: IF FORM.TIRF-IRS-RPT-IND = 'H'
                        {
                            pnd_Report_Indexes_Pnd_Irsr1.setValue(5);                                                                                                     //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 5
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                            sub_Accum_Irsr();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            //*  FIRST TIME REJECT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Report_Indexes_Pnd_Irsr1.setValue(6);                                                                                                     //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 6
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                            sub_Accum_Irsr();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            pnd_Case_Fields_Pnd_Update_Ind.setValue("H");                                                                                                 //Natural: ASSIGN #CASE-FIELDS.#UPDATE-IND := 'H'
                                                                                                                                                                          //Natural: PERFORM UPDATE-FORM
                            sub_Update_Form();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Case_Fields_Pnd_Prior_Reported.getBoolean()))                                                                                       //Natural: IF #CASE-FIELDS.#PRIOR-REPORTED
                    {
                                                                                                                                                                          //Natural: PERFORM COMPARE-IRSR-RECORDS
                        sub_Compare_Irsr_Records();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(pnd_Case_Fields_Pnd_No_Change.getBoolean()))                                                                                        //Natural: IF #CASE-FIELDS.#NO-CHANGE
                        {
                            pnd_Report_Indexes_Pnd_Irsr1.setValue(3);                                                                                                     //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 3
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                            sub_Accum_Irsr();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Report_Indexes_Pnd_Irsr1.setValue(7);                                                                                                             //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 7
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                    sub_Accum_Irsr();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM IRSR-DETAIL-ACCEPT
                    sub_Irsr_Detail_Accept();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    pnd_Case_Fields_Pnd_Update_Ind.setValue("C");                                                                                                         //Natural: ASSIGN #CASE-FIELDS.#UPDATE-IND := 'C'
                                                                                                                                                                          //Natural: PERFORM UPDATE-FORM
                    sub_Update_Form();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM IRSW-DETAIL-ACCEPT
                    sub_Irsw_Detail_Accept();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-READ
                if (condition(ldaTwrl9710.getVw_form().getAstCOUNTER().greater(0)))
                {
                    atBreakEventRd_Form(endOfDataRdForm);
                }
                if (Global.isEscape()) return;
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                if (condition(pnd_Ws_Pnd_Accepted_Form_Cnt.equals(getZero())))                                                                                            //Natural: IF #WS.#ACCEPTED-FORM-CNT = 0
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"No Forms were selected for the",new TabSetting(77),"***",NEWLINE,"***",new       //Natural: WRITE ( 1 ) '***' 25T 'No Forms were selected for the' 77T '***' / '***' 25T 'Correction Reporting' 77T '***'
                        TabSetting(25),"Correction Reporting",new TabSetting(77),"***");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                //* ************************
                //*  S U B R O U T I N E S
                //* ***************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-IRSR
                //* ***************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-IRSW
                //* ***************************
                //* ***************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-IRSG
                //* ****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-IRSG1
                //* ****************************
                //* *******************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRIOR-REPORTED
                //* *************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPARE-IRSR-RECORDS
                //* ****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-FORM
                //* ****************************
                //* *********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-RECON-CASE
                //* ********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-REPORTS
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-REPORTS-C
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-REPORTS-W
                //* **********************************
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-REPORTS-G
                //* **********************************
                //* ***********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IRSR-DETAIL-REJECT
                //* ***********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IRSR-DETAIL-ACCEPT
                //* ***********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IRSW-DETAIL-REJECT
                //* ***********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IRSW-DETAIL-ACCEPT
                //* ****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IRSG-DETAIL
                //* ****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TIN
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COMPANY
                //* ********************************
                //* *************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TERMINATE-PROCESSING
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Accum_Irsr() throws Exception                                                                                                                        //Natural: ACCUM-IRSR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        pnd_Irsr_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Irsr1,1).nadd(1);                                                                                           //Natural: ADD 1 TO #IRSR.#FORM-CNT ( #IRSR1,1 )
        pnd_Irsr_Pnd_Gross_Amt.getValue(pnd_Report_Indexes_Pnd_Irsr1,1).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                       //Natural: ADD FORM.TIRF-GROSS-AMT TO #IRSR.#GROSS-AMT ( #IRSR1,1 )
        pnd_Irsr_Pnd_Int_Amt.getValue(pnd_Report_Indexes_Pnd_Irsr1,1).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                           //Natural: ADD FORM.TIRF-INT-AMT TO #IRSR.#INT-AMT ( #IRSR1,1 )
        pnd_Irsr_Pnd_Tax_Wthld.getValue(pnd_Report_Indexes_Pnd_Irsr1,1).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                                   //Natural: ADD FORM.TIRF-FED-TAX-WTHLD TO #IRSR.#TAX-WTHLD ( #IRSR1,1 )
    }
    private void sub_Accum_Irsw() throws Exception                                                                                                                        //Natural: ACCUM-IRSW
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #WS.#K = 1 TO 3
        for (pnd_Ws_Pnd_K.setValue(1); condition(pnd_Ws_Pnd_K.lessOrEqual(3)); pnd_Ws_Pnd_K.nadd(1))
        {
            pnd_Report_Indexes_Pnd_Irsw2.compute(new ComputeParameters(false, pnd_Report_Indexes_Pnd_Irsw2), pnd_Report_Indexes_Pnd_Irsw1.add(pnd_Ws_Pnd_K));             //Natural: ASSIGN #REPORT-INDEXES.#IRSW2 := #REPORT-INDEXES.#IRSW1 + #WS.#K
            pnd_Irsw_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Irsw2,1).nadd(pnd_Recon_Case_Pnd_Form_Cnt.getValue(pnd_Ws_Pnd_K));                                      //Natural: ADD #RECON-CASE.#FORM-CNT ( #K ) TO #IRSW.#FORM-CNT ( #IRSW2,1 )
            pnd_Irsw_Pnd_Gross_Amt.getValue(pnd_Report_Indexes_Pnd_Irsw2,1).nadd(pnd_Recon_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_K));                                    //Natural: ADD #RECON-CASE.#GROSS-AMT ( #K ) TO #IRSW.#GROSS-AMT ( #IRSW2,1 )
            pnd_Irsw_Pnd_Tax_Wthld.getValue(pnd_Report_Indexes_Pnd_Irsw2,1).nadd(pnd_Recon_Case_Pnd_Tax_Wthld.getValue(pnd_Ws_Pnd_K));                                    //Natural: ADD #RECON-CASE.#TAX-WTHLD ( #K ) TO #IRSW.#TAX-WTHLD ( #IRSW2,1 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Accum_Irsg() throws Exception                                                                                                                        //Natural: ACCUM-IRSG
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        if (condition(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld().equals(new DbsDecimal("0.00"))))                                                                           //Natural: IF FORM.TIRF-FED-TAX-WTHLD = 0.00
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Irsg_Pnd_Form_Cnt.getValue(1,":",2,1).nadd(1);                                                                                                                //Natural: ADD 1 TO #IRSG.#FORM-CNT ( 1:2,1 )
        pnd_Irsg_Pnd_Gross_Amt.getValue(1,":",2,1).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                            //Natural: ADD FORM.TIRF-GROSS-AMT TO #IRSG.#GROSS-AMT ( 1:2,1 )
        pnd_Irsg_Pnd_Tax_Wthld.getValue(1,":",2,1).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                                                        //Natural: ADD FORM.TIRF-FED-TAX-WTHLD TO #IRSG.#TAX-WTHLD ( 1:2,1 )
    }
    private void sub_Accum_Irsg1() throws Exception                                                                                                                       //Natural: ACCUM-IRSG1
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #REPORT-INDEXES.#IRSG1 = 1 TO 3
        for (pnd_Report_Indexes_Pnd_Irsg1.setValue(1); condition(pnd_Report_Indexes_Pnd_Irsg1.lessOrEqual(3)); pnd_Report_Indexes_Pnd_Irsg1.nadd(1))
        {
            pnd_Irsg_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Irsg1,1).nadd(pnd_Recon_Case_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Irsg1));                      //Natural: ADD #RECON-CASE.#FORM-CNT ( #IRSG1 ) TO #IRSG.#FORM-CNT ( #IRSG1,1 )
            pnd_Irsg_Pnd_Gross_Amt.getValue(pnd_Report_Indexes_Pnd_Irsg1,1).nadd(pnd_Recon_Case_Pnd_Gross_Amt.getValue(pnd_Report_Indexes_Pnd_Irsg1));                    //Natural: ADD #RECON-CASE.#GROSS-AMT ( #IRSG1 ) TO #IRSG.#GROSS-AMT ( #IRSG1,1 )
            pnd_Irsg_Pnd_Tax_Wthld.getValue(pnd_Report_Indexes_Pnd_Irsg1,1).nadd(pnd_Recon_Case_Pnd_Tax_Wthld.getValue(pnd_Report_Indexes_Pnd_Irsg1));                    //Natural: ADD #RECON-CASE.#TAX-WTHLD ( #IRSG1 ) TO #IRSG.#TAX-WTHLD ( #IRSG1,1 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Prior_Reported() throws Exception                                                                                                                    //Natural: PRIOR-REPORTED
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Ws_Pnd_S4_Start,32,1);                                                                                            //Natural: MOVE LOW-VALUES TO SUBSTR ( #WS.#S4-START,32,1 )
        setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Ws_Pnd_S4_End,32,1);                                                                                             //Natural: MOVE HIGH-VALUES TO SUBSTR ( #WS.#S4-END,32,1 )
        //*  EB
        ldaTwrl9715.getVw_form_R().startDatabaseRead                                                                                                                      //Natural: READ FORM-R BY FORM-R.TIRF-SUPERDE-4 = #WS.#S4-START THRU #WS.#S4-END WHERE FORM-R.TIRF-COMPANY-CDE = FORM.TIRF-COMPANY-CDE
        (
        "RD1_FORM",
        new Wc[] { new Wc("TIRF_COMPANY_CDE", "=", ldaTwrl9710.getForm_Tirf_Company_Cde(), WcType.WHERE) ,
        new Wc("TIRF_SUPERDE_4", ">=", pnd_Ws_Pnd_S4_Start.getBinary(), "And", WcType.BY) ,
        new Wc("TIRF_SUPERDE_4", "<=", pnd_Ws_Pnd_S4_End.getBinary(), WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_4", "ASC") }
        );
        RD1_FORM:
        while (condition(ldaTwrl9715.getVw_form_R().readNextRow("RD1_FORM")))
        {
            if (condition(ldaTwrl9715.getForm_R_Tirf_Irs_Rpt_Ind().equals("H")))                                                                                          //Natural: IF FORM-R.TIRF-IRS-RPT-IND = 'H'
            {
                if (condition(! (pnd_Case_Fields_Pnd_Prior_Held.getBoolean())))                                                                                           //Natural: IF NOT #CASE-FIELDS.#PRIOR-HELD
                {
                    pnd_Case_Fields_Pnd_Prior_Held.setValue(true);                                                                                                        //Natural: ASSIGN #CASE-FIELDS.#PRIOR-HELD := TRUE
                    //*  INACTIVE
                    if (condition(ldaTwrl9715.getForm_R_Tirf_Active_Ind().equals(" ")))                                                                                   //Natural: IF FORM-R.TIRF-ACTIVE-IND = ' '
                    {
                        pnd_Case_Fields_Pnd_Prior_Record.setValue(true);                                                                                                  //Natural: ASSIGN #CASE-FIELDS.#PRIOR-RECORD := TRUE
                                                                                                                                                                          //Natural: PERFORM SETUP-RECON-CASE
                        sub_Setup_Recon_Case();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM IRSG-DETAIL
                        sub_Irsg_Detail();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(! (pnd_Case_Fields_Pnd_Prior_Reported.getBoolean())))                                                                                       //Natural: IF NOT #CASE-FIELDS.#PRIOR-REPORTED
                {
                    pnd_Case_Fields_Pnd_Prior_Reported.setValue(true);                                                                                                    //Natural: ASSIGN #CASE-FIELDS.#PRIOR-REPORTED := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Compare_Irsr_Records() throws Exception                                                                                                              //Natural: COMPARE-IRSR-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Id_Type().notEquals(ldaTwrl9715.getForm_R_Tirf_Tax_Id_Type()) || ldaTwrl9710.getForm_Tirf_Company_Cde().notEquals(ldaTwrl9715.getForm_R_Tirf_Company_Cde())  //Natural: IF FORM.TIRF-TAX-ID-TYPE NE FORM-R.TIRF-TAX-ID-TYPE OR FORM.TIRF-COMPANY-CDE NE FORM-R.TIRF-COMPANY-CDE OR FORM.TIRF-PARTICIPANT-NAME NE FORM-R.TIRF-PARTICIPANT-NAME OR FORM.TIRF-ADDR-LN1 NE FORM-R.TIRF-ADDR-LN1 OR FORM.TIRF-ADDR-LN2 NE FORM-R.TIRF-ADDR-LN2 OR FORM.TIRF-ADDR-LN3 NE FORM-R.TIRF-ADDR-LN3 OR FORM.TIRF-ADDR-LN4 NE FORM-R.TIRF-ADDR-LN4 OR FORM.TIRF-ADDR-LN5 NE FORM-R.TIRF-ADDR-LN5 OR FORM.TIRF-ADDR-LN6 NE FORM-R.TIRF-ADDR-LN6 OR FORM.TIRF-GROSS-AMT NE FORM-R.TIRF-GROSS-AMT OR FORM.TIRF-TAXABLE-AMT NE FORM-R.TIRF-TAXABLE-AMT OR FORM.TIRF-INT-AMT NE FORM-R.TIRF-INT-AMT OR FORM.TIRF-IVC-AMT NE FORM-R.TIRF-IVC-AMT OR FORM.TIRF-FED-TAX-WTHLD NE FORM-R.TIRF-FED-TAX-WTHLD
            || ldaTwrl9710.getForm_Tirf_Participant_Name().notEquals(ldaTwrl9715.getForm_R_Tirf_Participant_Name()) || ldaTwrl9710.getForm_Tirf_Addr_Ln1().notEquals(ldaTwrl9715.getForm_R_Tirf_Addr_Ln1()) 
            || ldaTwrl9710.getForm_Tirf_Addr_Ln2().notEquals(ldaTwrl9715.getForm_R_Tirf_Addr_Ln2()) || ldaTwrl9710.getForm_Tirf_Addr_Ln3().notEquals(ldaTwrl9715.getForm_R_Tirf_Addr_Ln3()) 
            || ldaTwrl9710.getForm_Tirf_Addr_Ln4().notEquals(ldaTwrl9715.getForm_R_Tirf_Addr_Ln4()) || ldaTwrl9710.getForm_Tirf_Addr_Ln5().notEquals(ldaTwrl9715.getForm_R_Tirf_Addr_Ln5()) 
            || ldaTwrl9710.getForm_Tirf_Addr_Ln6().notEquals(ldaTwrl9715.getForm_R_Tirf_Addr_Ln6()) || ldaTwrl9710.getForm_Tirf_Gross_Amt().notEquals(ldaTwrl9715.getForm_R_Tirf_Gross_Amt()) 
            || ldaTwrl9710.getForm_Tirf_Taxable_Amt().notEquals(ldaTwrl9715.getForm_R_Tirf_Taxable_Amt()) || ldaTwrl9710.getForm_Tirf_Int_Amt().notEquals(ldaTwrl9715.getForm_R_Tirf_Int_Amt()) 
            || ldaTwrl9710.getForm_Tirf_Ivc_Amt().notEquals(ldaTwrl9715.getForm_R_Tirf_Ivc_Amt()) || ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld().notEquals(ldaTwrl9715.getForm_R_Tirf_Fed_Tax_Wthld())))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Case_Fields_Pnd_No_Change.setValue(true);                                                                                                                 //Natural: ASSIGN #CASE-FIELDS.#NO-CHANGE := TRUE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_Form() throws Exception                                                                                                                       //Natural: UPDATE-FORM
    {
        if (BLNatReinput.isReinput()) return;

        G_FORM:                                                                                                                                                           //Natural: GET FORM-U #CASE-FIELDS.#ACTIVE-ISN
        vw_form_U.readByID(pnd_Case_Fields_Pnd_Active_Isn.getLong(), "G_FORM");
        form_U_Tirf_Irs_Rpt_Ind.setValue(pnd_Case_Fields_Pnd_Update_Ind);                                                                                                 //Natural: ASSIGN FORM-U.TIRF-IRS-RPT-IND := #CASE-FIELDS.#UPDATE-IND
        form_U_Tirf_Irs_Rpt_Date.setValue(pnd_Ws_Pnd_Datx);                                                                                                               //Natural: ASSIGN FORM-U.TIRF-IRS-RPT-DATE := #WS.#DATX
        form_U_Tirf_Lu_User.setValue(Global.getINIT_USER());                                                                                                              //Natural: ASSIGN FORM-U.TIRF-LU-USER := *INIT-USER
        form_U_Tirf_Lu_Ts.setValue(pnd_Ws_Pnd_Timx);                                                                                                                      //Natural: ASSIGN FORM-U.TIRF-LU-TS := #WS.#TIMX
        vw_form_U.updateDBRow("G_FORM");                                                                                                                                  //Natural: UPDATE ( G-FORM. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Setup_Recon_Case() throws Exception                                                                                                                  //Natural: SETUP-RECON-CASE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pnd_Case_Fields_Pnd_Recon_Summary.reset();                                                                                                                        //Natural: RESET #CASE-FIELDS.#RECON-SUMMARY #CASE-FIELDS.#RECON-DETAIL
        pnd_Case_Fields_Pnd_Recon_Detail.reset();
        //*  480.6A, 480.7C    /* 01/30/08
        if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().equals(5)))                                                                                                    //Natural: IF FORM.TIRF-FORM-TYPE = 05
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Prior_Record.getBoolean() && ldaTwrl9715.getForm_R_Tirf_Fed_Tax_Wthld().equals(new DbsDecimal("0.00"))))                        //Natural: IF #CASE-FIELDS.#PRIOR-RECORD AND FORM-R.TIRF-FED-TAX-WTHLD = 0.00
        {
            pnd_Case_Fields_Pnd_Prior_Record.reset();                                                                                                                     //Natural: RESET #CASE-FIELDS.#PRIOR-RECORD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld().notEquals(new DbsDecimal("0.00")) || pnd_Case_Fields_Pnd_Prior_Record.getBoolean() && ldaTwrl9715.getForm_R_Tirf_Fed_Tax_Wthld().notEquals(new  //Natural: IF FORM.TIRF-FED-TAX-WTHLD NE 0.00 OR #CASE-FIELDS.#PRIOR-RECORD AND FORM-R.TIRF-FED-TAX-WTHLD NE 0.00
            DbsDecimal("0.00"))))
        {
            pnd_Case_Fields_Pnd_Recon_Summary.setValue(true);                                                                                                             //Natural: ASSIGN #CASE-FIELDS.#RECON-SUMMARY := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Recon_Case.resetInitial();                                                                                                                                    //Natural: RESET INITIAL #RECON-CASE
        if (condition(pdaTwratin.getTwratin_Pnd_I_Tin_Type().notEquals(ldaTwrl9710.getForm_Tirf_Tax_Id_Type()) || pdaTwratin.getTwratin_Pnd_I_Tin().notEquals(ldaTwrl9710.getForm_Tirf_Tin()))) //Natural: IF TWRATIN.#I-TIN-TYPE NE FORM.TIRF-TAX-ID-TYPE OR TWRATIN.#I-TIN NE FORM.TIRF-TIN
        {
            pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(ldaTwrl9710.getForm_Tirf_Tax_Id_Type());                                                                      //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := FORM.TIRF-TAX-ID-TYPE
            pdaTwratin.getTwratin_Pnd_I_Tin().setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                                   //Natural: ASSIGN TWRATIN.#I-TIN := FORM.TIRF-TIN
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIN
            sub_Process_Tin();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Recon_Case_Pnd_Tin.setValue(pdaTwratin.getTwratin_Pnd_O_Tin());                                                                                               //Natural: ASSIGN #RECON-CASE.#TIN := TWRATIN.#O-TIN
        pnd_Recon_Case_Pnd_Cntrct_Py.setValueEdited(ldaTwrl9710.getForm_Tirf_Contract_Nbr(),new ReportEditMask("XXXXXXX-X/"));                                            //Natural: MOVE EDITED FORM.TIRF-CONTRACT-NBR ( EM = XXXXXXX-X/ ) TO #RECON-CASE.#CNTRCT-PY
        setValueToSubstring(ldaTwrl9710.getForm_Tirf_Payee_Cde(),pnd_Recon_Case_Pnd_Cntrct_Py,11,2);                                                                      //Natural: MOVE FORM.TIRF-PAYEE-CDE TO SUBSTR ( #RECON-CASE.#CNTRCT-PY,11,2 )
        pnd_Recon_Case_Pnd_Form_Cnt.getValue(1).nadd(1);                                                                                                                  //Natural: ADD 1 TO #RECON-CASE.#FORM-CNT ( 1 )
        pnd_Recon_Case_Pnd_Gross_Amt.getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                          //Natural: ASSIGN #RECON-CASE.#GROSS-AMT ( 1 ) := FORM.TIRF-GROSS-AMT
        pnd_Recon_Case_Pnd_Tax_Wthld.getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                                                      //Natural: ASSIGN #RECON-CASE.#TAX-WTHLD ( 1 ) := FORM.TIRF-FED-TAX-WTHLD
        if (condition(pnd_Case_Fields_Pnd_Prior_Record.getBoolean()))                                                                                                     //Natural: IF #CASE-FIELDS.#PRIOR-RECORD
        {
            pnd_Recon_Case_Pnd_Form_Cnt.getValue(2).nadd(1);                                                                                                              //Natural: ADD 1 TO #RECON-CASE.#FORM-CNT ( 2 )
            pnd_Recon_Case_Pnd_Gross_Amt.getValue(2).setValue(ldaTwrl9715.getForm_R_Tirf_Gross_Amt());                                                                    //Natural: ASSIGN #RECON-CASE.#GROSS-AMT ( 2 ) := FORM-R.TIRF-GROSS-AMT
            pnd_Recon_Case_Pnd_Tax_Wthld.getValue(2).setValue(ldaTwrl9715.getForm_R_Tirf_Fed_Tax_Wthld());                                                                //Natural: ASSIGN #RECON-CASE.#TAX-WTHLD ( 2 ) := FORM-R.TIRF-FED-TAX-WTHLD
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Recon_Case_Pnd_Form_Cnt.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Form_Cnt.getValue(3)), pnd_Recon_Case_Pnd_Form_Cnt.getValue(1).subtract(pnd_Recon_Case_Pnd_Form_Cnt.getValue(2))); //Natural: ASSIGN #RECON-CASE.#FORM-CNT ( 3 ) := #RECON-CASE.#FORM-CNT ( 1 ) - #RECON-CASE.#FORM-CNT ( 2 )
        if (condition(pnd_Recon_Case_Pnd_Tax_Wthld.getValue(1).equals(new DbsDecimal("0.00"))))                                                                           //Natural: IF #RECON-CASE.#TAX-WTHLD ( 1 ) = 0.00
        {
            pnd_Recon_Case_Pnd_Gross_Amt.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Gross_Amt.getValue(3)), pnd_Recon_Case_Pnd_Gross_Amt.getValue(2).multiply(-1)); //Natural: ASSIGN #RECON-CASE.#GROSS-AMT ( 3 ) := #RECON-CASE.#GROSS-AMT ( 2 ) * -1
            pnd_Recon_Case_Pnd_Tax_Wthld.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Tax_Wthld.getValue(3)), pnd_Recon_Case_Pnd_Tax_Wthld.getValue(2).multiply(-1)); //Natural: ASSIGN #RECON-CASE.#TAX-WTHLD ( 3 ) := #RECON-CASE.#TAX-WTHLD ( 2 ) * -1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Recon_Case_Pnd_Gross_Amt.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Gross_Amt.getValue(3)), pnd_Recon_Case_Pnd_Gross_Amt.getValue(1).subtract(pnd_Recon_Case_Pnd_Gross_Amt.getValue(2))); //Natural: ASSIGN #RECON-CASE.#GROSS-AMT ( 3 ) := #RECON-CASE.#GROSS-AMT ( 1 ) - #RECON-CASE.#GROSS-AMT ( 2 )
            pnd_Recon_Case_Pnd_Tax_Wthld.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Tax_Wthld.getValue(3)), pnd_Recon_Case_Pnd_Tax_Wthld.getValue(1).subtract(pnd_Recon_Case_Pnd_Tax_Wthld.getValue(2))); //Natural: ASSIGN #RECON-CASE.#TAX-WTHLD ( 3 ) := #RECON-CASE.#TAX-WTHLD ( 1 ) - #RECON-CASE.#TAX-WTHLD ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Recon_Case_Pnd_Form_Cnt.getValue(3).equals(new DbsDecimal("0.00")) && pnd_Recon_Case_Pnd_Gross_Amt.getValue(3).equals(new DbsDecimal("0.00"))   //Natural: IF #RECON-CASE.#FORM-CNT ( 3 ) = 0.00 AND #RECON-CASE.#GROSS-AMT ( 3 ) = 0.00 AND #RECON-CASE.#TAX-WTHLD ( 3 ) = 0.00
            && pnd_Recon_Case_Pnd_Tax_Wthld.getValue(3).equals(new DbsDecimal("0.00"))))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Case_Fields_Pnd_Recon_Detail.setValue(true);                                                                                                              //Natural: ASSIGN #CASE-FIELDS.#RECON-DETAIL := TRUE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Summary_Reports() throws Exception                                                                                                                   //Natural: SUMMARY-REPORTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORTS-C
        sub_Summary_Reports_C();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //*  480.6B
        if (condition(pnd_Ws_Pnd_Form_Type.equals(6)))                                                                                                                    //Natural: IF #WS.#FORM-TYPE = 06
        {
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORTS-W
            sub_Summary_Reports_W();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORTS-G
            sub_Summary_Reports_G();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Summary_Reports_C() throws Exception                                                                                                                 //Natural: SUMMARY-REPORTS-C
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        //*  480.6A, 480.7C  /* 01/30/08
        if (condition(pnd_Ws_Pnd_Form_Type.equals(5)))                                                                                                                    //Natural: IF #WS.#FORM-TYPE = 05
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(4));                                                                                                             //Natural: NEWPAGE ( 4 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(6));                                                                                                             //Natural: NEWPAGE ( 6 )
            if (condition(Global.isEscape())){return;}
            //*  480.6B
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().newPage(new ReportSpecification(3));                                                                                                             //Natural: NEWPAGE ( 3 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(5));                                                                                                             //Natural: NEWPAGE ( 5 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(7));                                                                                                             //Natural: NEWPAGE ( 7 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        FOR03:                                                                                                                                                            //Natural: FOR #IRSR1 = 1 TO #IRSR-MAX-LINES
        for (pnd_Report_Indexes_Pnd_Irsr1.setValue(1); condition(pnd_Report_Indexes_Pnd_Irsr1.lessOrEqual(pnd_Ws_Const_Pnd_Irsr_Max_Lines)); pnd_Report_Indexes_Pnd_Irsr1.nadd(1))
        {
            //*  480.6A, 480.7C  /* 01/30/08
            if (condition(pnd_Ws_Pnd_Form_Type.equals(5)))                                                                                                                //Natural: IF #WS.#FORM-TYPE = 05
            {
                if (condition(pnd_Report_Indexes_Pnd_Irsr1.equals(7)))                                                                                                    //Natural: IF #IRSR1 = 7
                {
                    getReports().write(2, "=",new RepeatItem(65));                                                                                                        //Natural: WRITE ( 2 ) '=' ( 65 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(2, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",                     //Natural: DISPLAY ( 2 ) ( ES = ON HC = R ) '/' #IRSR.#HEADER1 ( #IRSR1 ) / '/' #IRSR.#HEADER2 ( #IRSR1 ) 'Form/Count' #IRSR.#FORM-CNT ( #IRSR1,#S2 ) '/Gross Proceeds' #IRSR.#GROSS-AMT ( #IRSR1,#S2 ) '/Interest Amount' #IRSR.#INT-AMT ( #IRSR1,#S2 )
                		pnd_Irsr_Pnd_Header1.getValue(pnd_Report_Indexes_Pnd_Irsr1),NEWLINE,"/",
                		pnd_Irsr_Pnd_Header2.getValue(pnd_Report_Indexes_Pnd_Irsr1),"Form/Count",
                		pnd_Irsr_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Irsr1,pnd_Ws_Pnd_S2),"/Gross Proceeds",
                		pnd_Irsr_Pnd_Gross_Amt.getValue(pnd_Report_Indexes_Pnd_Irsr1,pnd_Ws_Pnd_S2),"/Interest Amount",
                		pnd_Irsr_Pnd_Int_Amt.getValue(pnd_Report_Indexes_Pnd_Irsr1,pnd_Ws_Pnd_S2));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  480.6B
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Report_Indexes_Pnd_Irsr1.equals(7)))                                                                                                    //Natural: IF #IRSR1 = 7
                {
                    getReports().write(3, "=",new RepeatItem(65));                                                                                                        //Natural: WRITE ( 3 ) '=' ( 65 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(3, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",                     //Natural: DISPLAY ( 3 ) ( ES = ON HC = R ) '/' #IRSR.#HEADER1 ( #IRSR1 ) / '/' #IRSR.#HEADER2 ( #IRSR1 ) 'Form/Count' #IRSR.#FORM-CNT ( #IRSR1,#S2 ) '/Amount Paid' #IRSR.#GROSS-AMT ( #IRSR1,#S2 ) '/Amount Withheld' #IRSR.#TAX-WTHLD ( #IRSR1,#S2 )
                		pnd_Irsr_Pnd_Header1.getValue(pnd_Report_Indexes_Pnd_Irsr1),NEWLINE,"/",
                		pnd_Irsr_Pnd_Header2.getValue(pnd_Report_Indexes_Pnd_Irsr1),"Form/Count",
                		pnd_Irsr_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Irsr1,pnd_Ws_Pnd_S2),"/Amount Paid",
                		pnd_Irsr_Pnd_Gross_Amt.getValue(pnd_Report_Indexes_Pnd_Irsr1,pnd_Ws_Pnd_S2),"/Amount Withheld",
                		pnd_Irsr_Pnd_Tax_Wthld.getValue(pnd_Report_Indexes_Pnd_Irsr1,pnd_Ws_Pnd_S2));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Summary_Reports_W() throws Exception                                                                                                                 //Natural: SUMMARY-REPORTS-W
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(8));                                                                                                                 //Natural: NEWPAGE ( 8 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(9));                                                                                                                 //Natural: NEWPAGE ( 9 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(10));                                                                                                                //Natural: NEWPAGE ( 10 )
        if (condition(Global.isEscape())){return;}
        FOR04:                                                                                                                                                            //Natural: FOR #IRSW1 = 1 TO #IRSW-MAX-LINES
        for (pnd_Report_Indexes_Pnd_Irsw1.setValue(1); condition(pnd_Report_Indexes_Pnd_Irsw1.lessOrEqual(pnd_Ws_Const_Pnd_Irsw_Max_Lines)); pnd_Report_Indexes_Pnd_Irsw1.nadd(1))
        {
            if (condition(pnd_Report_Indexes_Pnd_Irsw1.equals(3) || pnd_Report_Indexes_Pnd_Irsw1.equals(6)))                                                              //Natural: IF #IRSW1 = 3 OR = 6
            {
                getReports().write(8, "=",new RepeatItem(60));                                                                                                            //Natural: WRITE ( 8 ) '=' ( 60 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(8, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",                         //Natural: DISPLAY ( 8 ) ( ES = ON HC = R ) '/' #IRSW.#HEADER ( #IRSW1 ) 'Form/Count' #IRSW.#FORM-CNT ( #IRSW1,#S2 ) '/Amount Paid' #IRSW.#GROSS-AMT ( #IRSW1,#S2 ) '/Amount Withheld' #IRSW.#TAX-WTHLD ( #IRSW1,#S2 )
            		pnd_Irsw_Pnd_Header.getValue(pnd_Report_Indexes_Pnd_Irsw1),"Form/Count",
            		pnd_Irsw_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Irsw1,pnd_Ws_Pnd_S2),"/Amount Paid",
            		pnd_Irsw_Pnd_Gross_Amt.getValue(pnd_Report_Indexes_Pnd_Irsw1,pnd_Ws_Pnd_S2),"/Amount Withheld",
            		pnd_Irsw_Pnd_Tax_Wthld.getValue(pnd_Report_Indexes_Pnd_Irsw1,pnd_Ws_Pnd_S2));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Report_Indexes_Pnd_Irsw1.equals(3)))                                                                                                        //Natural: IF #IRSW1 = 3
            {
                getReports().skip(8, 4);                                                                                                                                  //Natural: SKIP ( 8 ) 4 LINES
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Summary_Reports_G() throws Exception                                                                                                                 //Natural: SUMMARY-REPORTS-G
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(11));                                                                                                                //Natural: NEWPAGE ( 11 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(12));                                                                                                                //Natural: NEWPAGE ( 12 )
        if (condition(Global.isEscape())){return;}
        FOR05:                                                                                                                                                            //Natural: FOR #IRSG1 = 1 TO #IRSG-MAX-LINES
        for (pnd_Report_Indexes_Pnd_Irsg1.setValue(1); condition(pnd_Report_Indexes_Pnd_Irsg1.lessOrEqual(pnd_Ws_Const_Pnd_Irsg_Max_Lines)); pnd_Report_Indexes_Pnd_Irsg1.nadd(1))
        {
            if (condition(pnd_Report_Indexes_Pnd_Irsg1.equals(3)))                                                                                                        //Natural: IF #IRSG1 = 3
            {
                getReports().write(11, "=",new RepeatItem(65));                                                                                                           //Natural: WRITE ( 11 ) '=' ( 65 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(11, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",                        //Natural: DISPLAY ( 11 ) ( ES = ON HC = R ) '/' #IRSG.#HEADER ( #IRSG1 ) 'Form/Count' #IRSG.#FORM-CNT ( #IRSG1,#S2 ) '/Amount Paid' #IRSG.#GROSS-AMT ( #IRSG1,#S2 ) '/Amount Withheld' #IRSG.#TAX-WTHLD ( #IRSG1,#S2 )
            		pnd_Irsg_Pnd_Header.getValue(pnd_Report_Indexes_Pnd_Irsg1),"Form/Count",
            		pnd_Irsg_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Irsg1,pnd_Ws_Pnd_S2),"/Amount Paid",
            		pnd_Irsg_Pnd_Gross_Amt.getValue(pnd_Report_Indexes_Pnd_Irsg1,pnd_Ws_Pnd_S2),"/Amount Withheld",
            		pnd_Irsg_Pnd_Tax_Wthld.getValue(pnd_Report_Indexes_Pnd_Irsg1,pnd_Ws_Pnd_S2));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Irsr_Detail_Reject() throws Exception                                                                                                                //Natural: IRSR-DETAIL-REJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(pdaTwratin.getTwratin_Pnd_I_Tin_Type().notEquals(ldaTwrl9710.getForm_Tirf_Tax_Id_Type()) || pdaTwratin.getTwratin_Pnd_I_Tin().notEquals(ldaTwrl9710.getForm_Tirf_Tin()))) //Natural: IF TWRATIN.#I-TIN-TYPE NE FORM.TIRF-TAX-ID-TYPE OR TWRATIN.#I-TIN NE FORM.TIRF-TIN
        {
            pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(ldaTwrl9710.getForm_Tirf_Tax_Id_Type());                                                                      //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := FORM.TIRF-TAX-ID-TYPE
            pdaTwratin.getTwratin_Pnd_I_Tin().setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                                   //Natural: ASSIGN TWRATIN.#I-TIN := FORM.TIRF-TIN
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIN
            sub_Process_Tin();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  480.6A, 480.7C    /* 01/30/08
        if (condition(pnd_Ws_Pnd_Form_Type.equals(5)))                                                                                                                    //Natural: IF #WS.#FORM-TYPE = 05
        {
            getReports().display(4, new ReportEmptyLineSuppression(true),"///TIN",                                                                                        //Natural: DISPLAY ( 4 ) ( ES = ON ) '///TIN' TWRATIN.#O-TIN / '/' TWRATIN.#O-TIN-TYPE-DESC '///Contract' FORM.TIRF-CONTRACT-NBR 'Pa/y/e/e' FORM.TIRF-PAYEE-CDE '//Name' FORM.TIRF-PARTICIPANT-NAME ( HC = L ) / 'Address' FORM.TIRF-ADDR-LN1 ( HC = L ) / '/' FORM.TIRF-ADDR-LN2 ( HC = L ) / '/' FORM.TIRF-ADDR-LN3 ( HC = L ) / '/' FORM.TIRF-ADDR-LN4 ( HC = L ) / '/' FORM.TIRF-ADDR-LN5 ( HC = L ) / '/' FORM.TIRF-ADDR-LN6 ( HC = L ) '///Gross Proceeds' FORM.TIRF-GROSS-AMT ( HC = R ) '//Interest/Amount' FORM.TIRF-INT-AMT ( HC = R )
            		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
            		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),"///Contract",
            		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"Pa/y/e/e",
            		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"//Name",
            		ldaTwrl9710.getForm_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"///Gross Proceeds",
            		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"//Interest/Amount",
            		ldaTwrl9710.getForm_Tirf_Int_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
            if (Global.isEscape()) return;
            FOR06:                                                                                                                                                        //Natural: FOR #WS.#I = 1 TO C*FORM.TIRF-SYS-ERR
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl9710.getForm_Count_Casttirf_Sys_Err())); pnd_Ws_Pnd_I.nadd(1))
            {
                pnd_Ws_Pnd_Err.setValue(ldaTwrl9710.getForm_Tirf_Sys_Err().getValue(pnd_Ws_Pnd_I));                                                                       //Natural: ASSIGN #WS.#ERR := FORM.TIRF-SYS-ERR ( #I )
                getReports().write(4, new ReportTAsterisk(ldaTwrl9710.getForm_Tirf_Participant_Name()),ldaTwrl5001.getPnd_Twrl5001_Pnd_Err_Desc().getValue(pnd_Ws_Pnd_Err.getInt() + 1)); //Natural: WRITE ( 4 ) T*FORM.TIRF-PARTICIPANT-NAME #TWRL5001.#ERR-DESC ( #ERR )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            getReports().skip(4, 1);                                                                                                                                      //Natural: SKIP ( 4 ) 1 LINES
            //*  480.6B
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().display(5, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),new ReportEmptyLineSuppression(true),"///TIN",                     //Natural: DISPLAY ( 5 ) ( HC = L ES = ON ) '///TIN' TWRATIN.#O-TIN / '/' TWRATIN.#O-TIN-TYPE-DESC '///Contract' FORM.TIRF-CONTRACT-NBR 'Pa/y/e/e' FORM.TIRF-PAYEE-CDE '//Name' FORM.TIRF-PARTICIPANT-NAME ( HC = L ) / 'Address' FORM.TIRF-ADDR-LN1 ( HC = L ) / '/' FORM.TIRF-ADDR-LN2 ( HC = L ) / '/' FORM.TIRF-ADDR-LN3 ( HC = L ) / '/' FORM.TIRF-ADDR-LN4 ( HC = L ) / '/' FORM.TIRF-ADDR-LN5 ( HC = L ) / '/' FORM.TIRF-ADDR-LN6 ( HC = L ) '///Amount Paid' FORM.TIRF-GROSS-AMT ( HC = R ) '//Amount/Withheld' FORM.TIRF-FED-TAX-WTHLD ( HC = R )
            		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
            		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),"///Contract",
            		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"Pa/y/e/e",
            		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"//Name",
            		ldaTwrl9710.getForm_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"///Amount Paid",
            		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"//Amount/Withheld",
            		ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
            if (Global.isEscape()) return;
            FOR07:                                                                                                                                                        //Natural: FOR #WS.#I = 1 TO C*FORM.TIRF-SYS-ERR
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl9710.getForm_Count_Casttirf_Sys_Err())); pnd_Ws_Pnd_I.nadd(1))
            {
                pnd_Ws_Pnd_Err.setValue(ldaTwrl9710.getForm_Tirf_Sys_Err().getValue(pnd_Ws_Pnd_I));                                                                       //Natural: ASSIGN #WS.#ERR := FORM.TIRF-SYS-ERR ( #I )
                getReports().write(5, new ReportTAsterisk(ldaTwrl9710.getForm_Tirf_Participant_Name()),ldaTwrl5001.getPnd_Twrl5001_Pnd_Err_Desc().getValue(pnd_Ws_Pnd_Err.getInt() + 1)); //Natural: WRITE ( 5 ) T*FORM.TIRF-PARTICIPANT-NAME #TWRL5001.#ERR-DESC ( #ERR )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            getReports().skip(5, 1);                                                                                                                                      //Natural: SKIP ( 5 ) 1 LINES
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Irsr_Detail_Accept() throws Exception                                                                                                                //Natural: IRSR-DETAIL-ACCEPT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(pdaTwratin.getTwratin_Pnd_I_Tin_Type().notEquals(ldaTwrl9710.getForm_Tirf_Tax_Id_Type()) || pdaTwratin.getTwratin_Pnd_I_Tin().notEquals(ldaTwrl9710.getForm_Tirf_Tin()))) //Natural: IF TWRATIN.#I-TIN-TYPE NE FORM.TIRF-TAX-ID-TYPE OR TWRATIN.#I-TIN NE FORM.TIRF-TIN
        {
            pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(ldaTwrl9710.getForm_Tirf_Tax_Id_Type());                                                                      //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := FORM.TIRF-TAX-ID-TYPE
            pdaTwratin.getTwratin_Pnd_I_Tin().setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                                   //Natural: ASSIGN TWRATIN.#I-TIN := FORM.TIRF-TIN
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIN
            sub_Process_Tin();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  480.6A, 480.7C    /* 01/30/08
        if (condition(pnd_Ws_Pnd_Form_Type.equals(5)))                                                                                                                    //Natural: IF #WS.#FORM-TYPE = 05
        {
            getReports().display(6, new ReportEmptyLineSuppression(true),"///TIN",                                                                                        //Natural: DISPLAY ( 6 ) ( ES = ON ) '///TIN' TWRATIN.#O-TIN / '/' TWRATIN.#O-TIN-TYPE-DESC '///Contract' FORM.TIRF-CONTRACT-NBR 'Pa/y/e/e' FORM.TIRF-PAYEE-CDE '//Name' FORM.TIRF-PARTICIPANT-NAME ( HC = L ) / 'Address' FORM.TIRF-ADDR-LN1 ( HC = L ) / '/' FORM.TIRF-ADDR-LN2 ( HC = L ) / '/' FORM.TIRF-ADDR-LN3 ( HC = L ) / '/' FORM.TIRF-ADDR-LN4 ( HC = L ) / '/' FORM.TIRF-ADDR-LN5 ( HC = L ) / '/' FORM.TIRF-ADDR-LN6 ( HC = L ) '///Gross Proceeds' FORM.TIRF-GROSS-AMT ( HC = R ) '//Interest/Amount' FORM.TIRF-INT-AMT ( HC = R )
            		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
            		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),"///Contract",
            		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"Pa/y/e/e",
            		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"//Name",
            		ldaTwrl9710.getForm_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"///Gross Proceeds",
            		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"//Interest/Amount",
            		ldaTwrl9710.getForm_Tirf_Int_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
            if (Global.isEscape()) return;
            getReports().skip(6, 1);                                                                                                                                      //Natural: SKIP ( 6 ) 1 LINES
            //*  480.6B
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().display(7, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),new ReportEmptyLineSuppression(true),"///TIN",                     //Natural: DISPLAY ( 7 ) ( HC = L ES = ON ) '///TIN' TWRATIN.#O-TIN / '/' TWRATIN.#O-TIN-TYPE-DESC '///Contract' FORM.TIRF-CONTRACT-NBR 'Pa/y/e/e' FORM.TIRF-PAYEE-CDE '//Name' FORM.TIRF-PARTICIPANT-NAME ( HC = L ) / 'Address' FORM.TIRF-ADDR-LN1 ( HC = L ) / '/' FORM.TIRF-ADDR-LN2 ( HC = L ) / '/' FORM.TIRF-ADDR-LN3 ( HC = L ) / '/' FORM.TIRF-ADDR-LN4 ( HC = L ) / '/' FORM.TIRF-ADDR-LN5 ( HC = L ) / '/' FORM.TIRF-ADDR-LN6 ( HC = L ) '///Amount Paid' FORM.TIRF-GROSS-AMT ( HC = R ) '//Amount/Withheld' FORM.TIRF-FED-TAX-WTHLD ( HC = R )
            		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
            		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),"///Contract",
            		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"Pa/y/e/e",
            		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"//Name",
            		ldaTwrl9710.getForm_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"///Amount Paid",
            		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"//Amount/Withheld",
            		ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
            if (Global.isEscape()) return;
            getReports().skip(7, 1);                                                                                                                                      //Natural: SKIP ( 7 ) 1 LINES
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Irsw_Detail_Reject() throws Exception                                                                                                                //Natural: IRSW-DETAIL-REJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(pnd_Case_Fields_Pnd_Recon_Summary.getBoolean()))                                                                                                    //Natural: IF #CASE-FIELDS.#RECON-SUMMARY
        {
            pnd_Report_Indexes_Pnd_Irsw1.setValue(3);                                                                                                                     //Natural: ASSIGN #REPORT-INDEXES.#IRSW1 := 3
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSW
            sub_Accum_Irsw();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Recon_Detail.getBoolean()))                                                                                                     //Natural: IF #CASE-FIELDS.#RECON-DETAIL
        {
            Global.format("IS=OFF");                                                                                                                                      //Natural: SUSPEND IDENTICAL SUPPRESS ( 10 )
            FOR08:                                                                                                                                                        //Natural: FOR #WS.#K = 1 TO 3
            for (pnd_Ws_Pnd_K.setValue(1); condition(pnd_Ws_Pnd_K.lessOrEqual(3)); pnd_Ws_Pnd_K.nadd(1))
            {
                if (condition(pnd_Ws_Pnd_K.equals(2) && ! (pnd_Case_Fields_Pnd_Prior_Record.getBoolean())))                                                               //Natural: IF #WS.#K = 2 AND NOT #CASE-FIELDS.#PRIOR-RECORD
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(10, "/TIN",                                                                                                                          //Natural: DISPLAY ( 10 ) '/TIN' #RECON-CASE.#TIN ( IS = ON ) 'Contract/Payee' #RECON-CASE.#CNTRCT-PY ( IS = ON ) '/' #RECON-CASE.#HEADER ( #K ) '/Form Count' #RECON-CASE.#FORM-CNT ( #K ) ( HC = R ) '/Amount Paid' #RECON-CASE.#GROSS-AMT ( #K ) ( HC = R ) 'Amount/Withheld' #RECON-CASE.#TAX-WTHLD ( #K ) ( HC = R )
                		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
                		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
                		pnd_Recon_Case_Pnd_Header.getValue(pnd_Ws_Pnd_K),"/Form Count",
                		pnd_Recon_Case_Pnd_Form_Cnt.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Amount Paid",
                    
                		pnd_Recon_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Amount/Withheld",
                    
                		pnd_Recon_Case_Pnd_Tax_Wthld.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_K.equals(3)))                                                                                                                    //Natural: IF #WS.#K = 3
                {
                    getReports().skip(10, 1);                                                                                                                             //Natural: SKIP ( 10 ) 1 LINES
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Irsw_Detail_Accept() throws Exception                                                                                                                //Natural: IRSW-DETAIL-ACCEPT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(pnd_Case_Fields_Pnd_Recon_Summary.getBoolean()))                                                                                                    //Natural: IF #CASE-FIELDS.#RECON-SUMMARY
        {
            pnd_Report_Indexes_Pnd_Irsw1.reset();                                                                                                                         //Natural: RESET #REPORT-INDEXES.#IRSW1
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSW
            sub_Accum_Irsw();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Recon_Detail.getBoolean()))                                                                                                     //Natural: IF #CASE-FIELDS.#RECON-DETAIL
        {
            Global.format("IS=OFF");                                                                                                                                      //Natural: SUSPEND IDENTICAL SUPPRESS ( 09 )
            FOR09:                                                                                                                                                        //Natural: FOR #WS.#K = 1 TO 3
            for (pnd_Ws_Pnd_K.setValue(1); condition(pnd_Ws_Pnd_K.lessOrEqual(3)); pnd_Ws_Pnd_K.nadd(1))
            {
                if (condition(pnd_Ws_Pnd_K.equals(2) && ! (pnd_Case_Fields_Pnd_Prior_Record.getBoolean())))                                                               //Natural: IF #WS.#K = 2 AND NOT #CASE-FIELDS.#PRIOR-RECORD
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(9, "/TIN",                                                                                                                           //Natural: DISPLAY ( 09 ) '/TIN' #RECON-CASE.#TIN ( IS = ON ) 'Contract/Payee' #RECON-CASE.#CNTRCT-PY ( IS = ON ) '/' #RECON-CASE.#HEADER ( #K ) '/Form Count' #RECON-CASE.#FORM-CNT ( #K ) ( HC = R ) '/Amount Paid' #RECON-CASE.#GROSS-AMT ( #K ) ( HC = R ) 'Amount/Withheld' #RECON-CASE.#TAX-WTHLD ( #K ) ( HC = R )
                		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
                		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
                		pnd_Recon_Case_Pnd_Header.getValue(pnd_Ws_Pnd_K),"/Form Count",
                		pnd_Recon_Case_Pnd_Form_Cnt.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Amount Paid",
                    
                		pnd_Recon_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Amount/Withheld",
                    
                		pnd_Recon_Case_Pnd_Tax_Wthld.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_K.equals(3)))                                                                                                                    //Natural: IF #WS.#K = 3
                {
                    getReports().skip(9, 1);                                                                                                                              //Natural: SKIP ( 09 ) 1 LINES
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Irsg_Detail() throws Exception                                                                                                                       //Natural: IRSG-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        if (condition(pnd_Case_Fields_Pnd_Recon_Summary.getBoolean()))                                                                                                    //Natural: IF #CASE-FIELDS.#RECON-SUMMARY
        {
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSG1
            sub_Accum_Irsg1();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Recon_Detail.getBoolean()))                                                                                                     //Natural: IF #CASE-FIELDS.#RECON-DETAIL
        {
            Global.format("IS=OFF");                                                                                                                                      //Natural: SUSPEND IDENTICAL SUPPRESS ( 12 )
            FOR10:                                                                                                                                                        //Natural: FOR #WS.#K = 1 TO 3
            for (pnd_Ws_Pnd_K.setValue(1); condition(pnd_Ws_Pnd_K.lessOrEqual(3)); pnd_Ws_Pnd_K.nadd(1))
            {
                if (condition(pnd_Ws_Pnd_K.equals(2) && ! (pnd_Case_Fields_Pnd_Prior_Record.getBoolean())))                                                               //Natural: IF #WS.#K = 2 AND NOT #CASE-FIELDS.#PRIOR-RECORD
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(12, "/TIN",                                                                                                                          //Natural: DISPLAY ( 12 ) '/TIN' #RECON-CASE.#TIN ( IS = ON ) 'Contract/Payee' #RECON-CASE.#CNTRCT-PY ( IS = ON ) '/' #RECON-CASE.#HEADER ( #K ) '/Form Count' #RECON-CASE.#FORM-CNT ( #K ) ( HC = R ) '/Amount Paid' #RECON-CASE.#GROSS-AMT ( #K ) ( HC = R ) 'Amount/Withheld' #RECON-CASE.#TAX-WTHLD ( #K ) ( HC = R )
                		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
                		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
                		pnd_Recon_Case_Pnd_Header.getValue(pnd_Ws_Pnd_K),"/Form Count",
                		pnd_Recon_Case_Pnd_Form_Cnt.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Amount Paid",
                    
                		pnd_Recon_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Amount/Withheld",
                    
                		pnd_Recon_Case_Pnd_Tax_Wthld.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_K.equals(3)))                                                                                                                    //Natural: IF #WS.#K = 3
                {
                    getReports().skip(12, 1);                                                                                                                             //Natural: SKIP ( 12 ) 1 LINES
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Tin() throws Exception                                                                                                                       //Natural: PROCESS-TIN
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        DbsUtil.callnat(Twrntin.class , getCurrentProcessState(), pdaTwratin.getTwratin_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratin.getTwratin_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTIN' USING TWRATIN.#INPUT-PARMS ( AD = O ) TWRATIN.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    //*  BK
    private void sub_Process_Company() throws Exception                                                                                                                   //Natural: PROCESS-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(ldaTwrl9710.getForm_Tirf_Company_Cde());                                                                     //Natural: ASSIGN #TWRACOMP.#COMP-CODE := FORM.TIRF-COMPANY-CDE
        pdaTwracomp.getPnd_Twracomp_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwracomp.getPnd_Twracomp_Pnd_Tax_Year()), ldaTwrl9710.getForm_Tirf_Tax_Year().val()); //Natural: ASSIGN #TWRACOMP.#TAX-YEAR := VAL ( FORM.TIRF-TAX-YEAR )
        DbsUtil.callnat(Twrncomp.class , getCurrentProcessState(), pdaTwracomp.getPnd_Twracomp_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracomp.getPnd_Twracomp_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOMP' USING #TWRACOMP.#INPUT-PARMS ( AD = O ) #TWRACOMP.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        pnd_Ws_Pnd_Company_Line.setValue("Company:");                                                                                                                     //Natural: ASSIGN #WS.#COMPANY-LINE := 'Company:'
        setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),pnd_Ws_Pnd_Company_Line,11,4);                                                              //Natural: MOVE #TWRACOMP.#COMP-SHORT-NAME TO SUBSTR ( #WS.#COMPANY-LINE,11,4 )
        setValueToSubstring("-",pnd_Ws_Pnd_Company_Line,16,1);                                                                                                            //Natural: MOVE '-' TO SUBSTR ( #WS.#COMPANY-LINE,16,1 )
        //*  EB
        setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Name(),pnd_Ws_Pnd_Company_Line,18,55);                                                                   //Natural: MOVE #TWRACOMP.#COMP-NAME TO SUBSTR ( #WS.#COMPANY-LINE,18,55 )
        pnd_Ws_Pnd_Company_Break.reset();                                                                                                                                 //Natural: RESET #WS.#COMPANY-BREAK
    }
    private void sub_Terminate_Processing() throws Exception                                                                                                              //Natural: TERMINATE-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        DbsUtil.terminate(pnd_Ws_Pnd_Terminate);  if (true) return;                                                                                                       //Natural: TERMINATE #WS.#TERMINATE
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventRd_Form() throws Exception {atBreakEventRd_Form(false);}
    private void atBreakEventRd_Form(boolean endOfData) throws Exception
    {
        boolean ldaTwrl9710_getForm_Tirf_Company_CdeIsBreak = ldaTwrl9710.getForm_Tirf_Company_Cde().isBreak(endOfData);
        boolean ldaTwrl9710_getForm_Tirf_Form_TypeIsBreak = ldaTwrl9710.getForm_Tirf_Form_Type().isBreak(endOfData);
        if (condition(ldaTwrl9710_getForm_Tirf_Company_CdeIsBreak || ldaTwrl9710_getForm_Tirf_Form_TypeIsBreak))
        {
            pnd_Ws_Pnd_Company_Break.setValue(true);                                                                                                                      //Natural: ASSIGN #WS.#COMPANY-BREAK := TRUE
            pnd_Ws_Pnd_S2.setValue(1);                                                                                                                                    //Natural: ASSIGN #WS.#S2 := 1
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORTS
            sub_Summary_Reports();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Irsr_Pnd_Form_Cnt.getValue("*",2).nadd(pnd_Irsr_Pnd_Form_Cnt.getValue("*",1));                                                                            //Natural: ADD #IRSR.#FORM-CNT ( *,1 ) TO #IRSR.#FORM-CNT ( *,2 )
            pnd_Irsr_Pnd_Gross_Amt.getValue("*",2).nadd(pnd_Irsr_Pnd_Gross_Amt.getValue("*",1));                                                                          //Natural: ADD #IRSR.#GROSS-AMT ( *,1 ) TO #IRSR.#GROSS-AMT ( *,2 )
            pnd_Irsr_Pnd_Int_Amt.getValue("*",2).nadd(pnd_Irsr_Pnd_Int_Amt.getValue("*",1));                                                                              //Natural: ADD #IRSR.#INT-AMT ( *,1 ) TO #IRSR.#INT-AMT ( *,2 )
            pnd_Irsr_Pnd_Tax_Wthld.getValue("*",2).nadd(pnd_Irsr_Pnd_Tax_Wthld.getValue("*",1));                                                                          //Natural: ADD #IRSR.#TAX-WTHLD ( *,1 ) TO #IRSR.#TAX-WTHLD ( *,2 )
            pnd_Irsr_Pnd_Totals.getValue("*",1).reset();                                                                                                                  //Natural: RESET #IRSR.#TOTALS ( *,1 )
            pnd_Irsw_Pnd_Form_Cnt.getValue("*",2).nadd(pnd_Irsw_Pnd_Form_Cnt.getValue("*",1));                                                                            //Natural: ADD #IRSW.#FORM-CNT ( *,1 ) TO #IRSW.#FORM-CNT ( *,2 )
            pnd_Irsw_Pnd_Gross_Amt.getValue("*",2).nadd(pnd_Irsw_Pnd_Gross_Amt.getValue("*",1));                                                                          //Natural: ADD #IRSW.#GROSS-AMT ( *,1 ) TO #IRSW.#GROSS-AMT ( *,2 )
            pnd_Irsw_Pnd_Tax_Wthld.getValue("*",2).nadd(pnd_Irsw_Pnd_Tax_Wthld.getValue("*",1));                                                                          //Natural: ADD #IRSW.#TAX-WTHLD ( *,1 ) TO #IRSW.#TAX-WTHLD ( *,2 )
            pnd_Irsw_Pnd_Totals.getValue("*",1).reset();                                                                                                                  //Natural: RESET #IRSW.#TOTALS ( *,1 )
            pnd_Irsg_Pnd_Form_Cnt.getValue("*",2).nadd(pnd_Irsg_Pnd_Form_Cnt.getValue("*",1));                                                                            //Natural: ADD #IRSG.#FORM-CNT ( *,1 ) TO #IRSG.#FORM-CNT ( *,2 )
            pnd_Irsg_Pnd_Gross_Amt.getValue("*",2).nadd(pnd_Irsg_Pnd_Gross_Amt.getValue("*",1));                                                                          //Natural: ADD #IRSG.#GROSS-AMT ( *,1 ) TO #IRSG.#GROSS-AMT ( *,2 )
            pnd_Irsg_Pnd_Tax_Wthld.getValue("*",2).nadd(pnd_Irsg_Pnd_Tax_Wthld.getValue("*",1));                                                                          //Natural: ADD #IRSG.#TAX-WTHLD ( *,1 ) TO #IRSG.#TAX-WTHLD ( *,2 )
            pnd_Irsg_Pnd_Totals.getValue("*",1).reset();                                                                                                                  //Natural: RESET #IRSG.#TOTALS ( *,1 )
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaTwrl9710_getForm_Tirf_Form_TypeIsBreak))
        {
            pnd_Ws_Pnd_S2.setValue(2);                                                                                                                                    //Natural: ASSIGN #WS.#S2 := 2
            pnd_Ws_Pnd_Company_Line.setValue("Grand Totals");                                                                                                             //Natural: ASSIGN #WS.#COMPANY-LINE := 'Grand Totals'
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORTS
            sub_Summary_Reports();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Ws_Pnd_Accepted_Form_Cnt.nadd(pnd_Irsr_Pnd_Form_Cnt.getValue(7,2));                                                                                       //Natural: ADD #IRSR.#FORM-CNT ( 7,2 ) TO #WS.#ACCEPTED-FORM-CNT
            pnd_Irsr_Pnd_Totals.getValue("*",2).reset();                                                                                                                  //Natural: RESET #IRSR.#TOTALS ( *,2 ) #IRSW.#TOTALS ( *,2 ) #IRSG.#TOTALS ( *,2 )
            pnd_Irsw_Pnd_Totals.getValue("*",2).reset();
            pnd_Irsg_Pnd_Totals.getValue("*",2).reset();
            pnd_Ws_Pnd_Form_Type.setValue(ldaTwrl9710.getForm_Tirf_Form_Type());                                                                                          //Natural: ASSIGN #WS.#FORM-TYPE := FORM.TIRF-FORM-TYPE
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=80 ZP=ON");
        Global.format(2, "PS=58 LS=133 ZP=ON");
        Global.format(3, "PS=58 LS=133 ZP=ON");
        Global.format(4, "PS=58 LS=133 ZP=ON");
        Global.format(5, "PS=58 LS=133 ZP=ON");
        Global.format(6, "PS=58 LS=133 ZP=ON");
        Global.format(7, "PS=58 LS=133 ZP=ON");
        Global.format(8, "PS=58 LS=133 ZP=ON");
        Global.format(9, "PS=58 LS=133 ZP=ON");
        Global.format(10, "PS=58 LS=133 ZP=ON");
        Global.format(11, "PS=58 LS=133 ZP=ON");
        Global.format(12, "PS=58 LS=133 ZP=ON");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(37),"TaxWaRS",new TabSetting(68),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(34),"Control Report",new TabSetting(68),"Report: RPT1",NEWLINE,NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"Puerto Rico Correction Reporting Summary",new TabSetting(120),"Report: RPT2",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"Puerto Rico Correction Reporting Summary",new TabSetting(120),"Report: RPT3",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(4), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(43),"Puerto Rico Correction Reporting Rejected Detail",new TabSetting(120),"Report: RPT4",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(5, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(5), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(43),"Puerto Rico Correction Reporting Rejected Detail",new TabSetting(120),"Report: RPT5",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(6, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(6), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(43),"Puerto Rico Correction Reporting Accepted Detail",new TabSetting(120),"Report: RPT6",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(7, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(7), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(43),"Puerto Rico Correction Reporting Accepted Detail",new TabSetting(120),"Report: RPT7",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(8, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(8), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(38),"Puerto Rico Correction Withholding Reconciliation Summary",new TabSetting(120),"Report: RPT8",NEWLINE,new TabSetting(43),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, 
            new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(9, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(9), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(34),"Puerto Rico Correction Withholding Reconciliation","Accepted Detail",new TabSetting(120),"Report: RPT9",NEWLINE,new TabSetting(43),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, 
            new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(10, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(118),"Page  :",getReports().getPageNumberDbs(10), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(34),"Puerto Rico Correction Withholding Reconciliation","Rejected Detail",new TabSetting(118),"Report:  RPT10",NEWLINE,new TabSetting(43),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, 
            new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(11, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(118),"Page  :",getReports().getPageNumberDbs(11), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(42),"Purto Rico General Ledger Reconciliation Summary",new TabSetting(118),"Report:  RPT11",NEWLINE,new TabSetting(43),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, 
            new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(12, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(118),"Page  :",getReports().getPageNumberDbs(12), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(43),"Purto Rico General Ledger Reconciliation Detail",new TabSetting(118),"Report:  RPT12",NEWLINE,new TabSetting(43),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, 
            new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);

        getReports().setDisplayColumns(2, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",
            
        		pnd_Irsr_Pnd_Header1,NEWLINE,"/",
        		pnd_Irsr_Pnd_Header2,"Form/Count",
        		pnd_Irsr_Pnd_Form_Cnt,"/Gross Proceeds",
        		pnd_Irsr_Pnd_Gross_Amt,"/Interest Amount",
        		pnd_Irsr_Pnd_Int_Amt);
        getReports().setDisplayColumns(3, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",
            
        		pnd_Irsr_Pnd_Header1,NEWLINE,"/",
        		pnd_Irsr_Pnd_Header2,"Form/Count",
        		pnd_Irsr_Pnd_Form_Cnt,"/Amount Paid",
        		pnd_Irsr_Pnd_Gross_Amt,"/Amount Withheld",
        		pnd_Irsr_Pnd_Tax_Wthld);
        getReports().setDisplayColumns(8, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",
            
        		pnd_Irsw_Pnd_Header,"Form/Count",
        		pnd_Irsw_Pnd_Form_Cnt,"/Amount Paid",
        		pnd_Irsw_Pnd_Gross_Amt,"/Amount Withheld",
        		pnd_Irsw_Pnd_Tax_Wthld);
        getReports().setDisplayColumns(11, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",
            
        		pnd_Irsg_Pnd_Header,"Form/Count",
        		pnd_Irsg_Pnd_Form_Cnt,"/Amount Paid",
        		pnd_Irsg_Pnd_Gross_Amt,"/Amount Withheld",
        		pnd_Irsg_Pnd_Tax_Wthld);
        getReports().setDisplayColumns(4, new ReportEmptyLineSuppression(true),"///TIN",
        		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),"///Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"Pa/y/e/e",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"//Name",
        		ldaTwrl9710.getForm_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"///Gross Proceeds",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"//Interest/Amount",
        		ldaTwrl9710.getForm_Tirf_Int_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(5, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),new ReportEmptyLineSuppression(true),"///TIN",
            
        		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),"///Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"Pa/y/e/e",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"//Name",
        		ldaTwrl9710.getForm_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"///Amount Paid",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"//Amount/Withheld",
        		ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(6, new ReportEmptyLineSuppression(true),"///TIN",
        		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),"///Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"Pa/y/e/e",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"//Name",
        		ldaTwrl9710.getForm_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"///Gross Proceeds",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"//Interest/Amount",
        		ldaTwrl9710.getForm_Tirf_Int_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(7, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),new ReportEmptyLineSuppression(true),"///TIN",
            
        		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),"///Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"Pa/y/e/e",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"//Name",
        		ldaTwrl9710.getForm_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"///Amount Paid",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"//Amount/Withheld",
        		ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(10, "/TIN",
        		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
        		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
        		pnd_Recon_Case_Pnd_Header,"/Form Count",
        		pnd_Recon_Case_Pnd_Form_Cnt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Amount Paid",
        		pnd_Recon_Case_Pnd_Gross_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Amount/Withheld",
        		pnd_Recon_Case_Pnd_Tax_Wthld, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(9, "/TIN",
        		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
        		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
        		pnd_Recon_Case_Pnd_Header,"/Form Count",
        		pnd_Recon_Case_Pnd_Form_Cnt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Amount Paid",
        		pnd_Recon_Case_Pnd_Gross_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Amount/Withheld",
        		pnd_Recon_Case_Pnd_Tax_Wthld, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(12, "/TIN",
        		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
        		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
        		pnd_Recon_Case_Pnd_Header,"/Form Count",
        		pnd_Recon_Case_Pnd_Form_Cnt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Amount Paid",
        		pnd_Recon_Case_Pnd_Gross_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Amount/Withheld",
        		pnd_Recon_Case_Pnd_Tax_Wthld, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
    }
    private void CheckAtStartofData712() throws Exception
    {
        if (condition(ldaTwrl9710.getVw_form().getAtStartOfData()))
        {
            pnd_Ws_Pnd_Form_Type.setValue(ldaTwrl9710.getForm_Tirf_Form_Type());                                                                                          //Natural: ASSIGN #WS.#FORM-TYPE := FORM.TIRF-FORM-TYPE
        }                                                                                                                                                                 //Natural: END-START
    }
}
