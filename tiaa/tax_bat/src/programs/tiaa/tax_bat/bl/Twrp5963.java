/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:43:06 PM
**        * FROM NATURAL PROGRAM : Twrp5963
************************************************************
**        * FILE NAME            : Twrp5963.java
**        * CLASS NAME           : Twrp5963
**        * INSTANCE NAME        : Twrp5963
************************************************************
************************************************************************
** PROGRAM : TWRP5963
** SYSTEM  : TAXWARS
** AUTHOR  : FELIX ORTIZ -
** FUNCTION: FORMS DATABASE CONTROL REPORTS
**           USED FOR TAX YEARS >= 2012
** HISTORY.....:
**  10/22/11  MS      - IRR AMOUNT FOR 1099-R
**  08/21/12  JB      - COPIED TWRP5959 FOR CUNY/SUNY P2
**  12/05/14  O SOTTO - FATCA CHANGES MARKED 11/2014.
**  02/16/2016 PAULS  - RECOMPILED  - TWRLCOMP CHANGES.
**  02/25/2016 RAHUL DAS - 1042-S REPORT ISSUE IN PENSION/ANNUITIES
**                         COLUMN - TAG: RAHUL
**  05/05/2016 RUPOLEENA - 1042-S REPORT ISSUE FOR PEN/ANNUITY COLUMN
**                         TAX YEAR 2013 & 2014 - MUKHERR
**  01/19/2017 SINHASN- RECOMPILED  - TWRLCOMP CHANGES.
**  12/03/2017 DASDH  - 5498 BOX CHANGES
**  10/05/2018 ARIVU  - RECOMPILED - TWRLCOMP CHANGES.
**  11/11/2020 SAIK   - 5498 BOX CHANGES(TAX YEAR - 2020)
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5963 extends BLNatBase
{
    // Data Areas
    private PdaTwraplsc pdaTwraplsc;
    private PdaTwratbl2 pdaTwratbl2;
    private LdaTwrlcomp ldaTwrlcomp;
    private PdaTwrafrmn pdaTwrafrmn;
    private LdaTwrl5001 ldaTwrl5001;
    private LdaTwrl5950 ldaTwrl5950;
    private LdaTwrl5951 ldaTwrl5951;
    private LdaTwrl9710 ldaTwrl9710;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_hist_Form;
    private DbsField hist_Form_Tirf_Superde_4;

    private DbsGroup hist_Form__R_Field_1;
    private DbsField hist_Form_Pnd_Tirf_Tax_Year;
    private DbsField hist_Form_Pnd_Tirf_Tin;
    private DbsField hist_Form_Pnd_Tirf_Form_Type;
    private DbsField hist_Form_Pnd_Tirf_Contract_Nbr;
    private DbsField hist_Form_Pnd_Tirf_Payee_Cde;
    private DbsField hist_Form_Pnd_Tirf_Key;
    private DbsField hist_Form_Pnd_Tirf_Invrse_Create_Ts;
    private DbsField hist_Form_Pnd_Tirf_Irs_Rpt_Ind;

    private DataAccessProgramView vw_form_1099_R;
    private DbsField form_1099_R_Tirf_Tax_Year;
    private DbsField form_1099_R_Tirf_Form_Type;
    private DbsField form_1099_R_Tirf_Company_Cde;
    private DbsField form_1099_R_Tirf_Tax_Id_Type;
    private DbsField form_1099_R_Tirf_Tin;
    private DbsField form_1099_R_Tirf_Contract_Nbr;
    private DbsField form_1099_R_Tirf_Payee_Cde;
    private DbsField form_1099_R_Tirf_Key;
    private DbsField form_1099_R_Count_Casttirf_1099_R_State_Grp;

    private DbsGroup form_1099_R_Tirf_1099_R_State_Grp;
    private DbsField form_1099_R_Tirf_State_Rpt_Ind;
    private DbsField form_1099_R_Tirf_State_Hardcopy_Ind;
    private DbsField form_1099_R_Tirf_State_Irs_Rpt_Ind;
    private DbsField form_1099_R_Tirf_State_Code;
    private DbsField form_1099_R_Tirf_State_Distr;
    private DbsField form_1099_R_Tirf_State_Tax_Wthld;
    private DbsField form_1099_R_Tirf_Loc_Code;
    private DbsField form_1099_R_Tirf_Loc_Distr;
    private DbsField form_1099_R_Tirf_Loc_Tax_Wthld;
    private DbsField form_1099_R_Tirf_State_Auth_Rpt_Ind;
    private DbsField form_1099_R_Tirf_State_Auth_Rpt_Date;
    private DbsField pnd_Input_Rec;

    private DbsGroup pnd_Input_Rec__R_Field_2;
    private DbsField pnd_Input_Rec_Pnd_Tax_Year;
    private DbsField pnd_Form_Text;

    private DbsGroup pnd_1099r_1099i_Control_Totals;
    private DbsField pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt;
    private DbsField pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt;
    private DbsField pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld;
    private DbsField pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt;
    private DbsField pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt;
    private DbsField pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Distr;
    private DbsField pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld;
    private DbsField pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Distr;
    private DbsField pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld;
    private DbsField pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt;

    private DbsGroup pnd_1099_Grand_Totals;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Form_Cnt;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Hold_Cnt;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Empty_Form_Cnt;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Gross_Amt;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Taxable_Amt;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Fed_Tax_Wthld;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Ivc_Amt;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Irr_Amt;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_State_Distr;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_State_Tax_Wthld;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Loc_Distr;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Loc_Tax_Wthld;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Int_Amt;

    private DbsGroup pnd_1042s_Control_Totals;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Trans_Count;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt;

    private DbsGroup pnd_1042s_Totals;
    private DbsField pnd_1042s_Totals_Pnd_1042s_Total_Gross;
    private DbsField pnd_1042s_Totals_Pnd_1042s_Total_Interest;
    private DbsField pnd_1042s_Totals_Pnd_1042s_Total_Pension;
    private DbsField pnd_1042s_Totals_Pnd_1042s_Total_Tax;
    private DbsField pnd_1042s_Totals_Pnd_1042s_Total_Refund;

    private DbsGroup pnd_5498_Control_Totals;
    private DbsField pnd_5498_Control_Totals_Pnd_5498_Form_Cnt;
    private DbsField pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Contrib;
    private DbsField pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Contrib;
    private DbsField pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rollover;
    private DbsField pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rechar;
    private DbsField pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Conversion;
    private DbsField pnd_5498_Control_Totals_Pnd_5498_Account_Fmv;
    private DbsField pnd_5498_Control_Totals_Pnd_5498_Sep_Amt;
    private DbsField pnd_5498_Control_Totals_Pnd_5498_Postpn_Amt;
    private DbsField pnd_5498_Control_Totals_Pnd_5498_Repayments_Amt;

    private DbsGroup pnd_4807c_Control_Totals;
    private DbsField pnd_4807c_Control_Totals_Pnd_4807c_Form_Cnt;
    private DbsField pnd_4807c_Control_Totals_Pnd_4807c_Empty_Form_Cnt;
    private DbsField pnd_4807c_Control_Totals_Pnd_4807c_Hold_Cnt;
    private DbsField pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt;
    private DbsField pnd_4807c_Control_Totals_Pnd_4807c_Int_Amt;
    private DbsField pnd_4807c_Control_Totals_Pnd_4807c_Distrib_Amt;
    private DbsField pnd_4807c_Control_Totals_Pnd_4807c_Taxable_Amt;
    private DbsField pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt;
    private DbsField pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt_Roll;
    private DbsField pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt_Roll;

    private DbsGroup pnd_Nr4_Control_Totals;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Hold_Cnt;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Income_Code;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld;

    private DbsGroup pnd_Accum_F10;
    private DbsField pnd_Accum_F10_Pnd_Cont_Cnt_F10;
    private DbsField pnd_Accum_F10_Pnd_Form_Cnt_F10;
    private DbsField pnd_Accum_F10_Pnd_Gross_Amt_F10;
    private DbsField pnd_Accum_F10_Pnd_Fed_Wthld_F10;
    private DbsField pnd_Accum_F10_Pnd_St_Wthld_F10;
    private DbsField pnd_Accum_F10_Pnd_Loc_Wthld_F10;

    private DbsGroup pnd_Vars_F10;
    private DbsField pnd_Vars_F10_Pnd_I_F10;
    private DbsField pnd_Vars_F10_Pnd_Ndx_F10;
    private DbsField pnd_Vars_F10_Pnd_All_Gross_F10;
    private DbsField pnd_Vars_F10_Pnd_Gross_Accum_F10;
    private DbsField pnd_Vars_F10_Pnd_Contract_Amt_Err_F10;
    private DbsField pnd_Form_Cnt;
    private DbsField pnd_Hold_Cnt;
    private DbsField pnd_Empty_Form_Cnt;
    private DbsField pnd_K;
    private DbsField pnd_I;
    private DbsField pnd_Company_Ndx;
    private DbsField pnd_Err_Ndx;
    private DbsField pnd_Nr4_Ndx;
    private DbsField pnd_Sys_Err_Ndx;
    private DbsField pnd_1099_State_Ndx;
    private DbsField pnd_State_Tbl_Ndx;

    private DbsGroup pnd_State_Tbl_Ndx__R_Field_3;
    private DbsField pnd_State_Tbl_Ndx_Pnd_State_Tbl_Ndx_A;
    private DbsField pnd_Reportable_Ndx;
    private DbsField pnd_State_Ndx;

    private DbsGroup pnd_1099_State_Tbl;
    private DbsField pnd_1099_State_Tbl_Pnd_State_Form_Cnt;
    private DbsField pnd_1099_State_Tbl_Pnd_State_Distr_Amt;
    private DbsField pnd_1099_State_Tbl_Pnd_State_Wthhld_Amt;
    private DbsField pnd_1099_State_Tbl_Pnd_Local_Distr_Amt;
    private DbsField pnd_1099_State_Tbl_Pnd_Local_Wthhld_Amt;
    private DbsField pnd_1099_State_Tbl_Pnd_Res_Gross_Amt;
    private DbsField pnd_1099_State_Tbl_Pnd_Res_Taxable_Amt;
    private DbsField pnd_1099_State_Tbl_Pnd_Res_Ivc_Amt;
    private DbsField pnd_1099_State_Tbl_Pnd_Res_Fed_Tax_Wthld;
    private DbsField pnd_1099_State_Tbl_Pnd_Res_Irr_Amt;

    private DbsGroup pnd_Summary_Amounts;
    private DbsField pnd_Summary_Amounts_Pnd_Summ_State_Form_Cnt;
    private DbsField pnd_Summary_Amounts_Pnd_Summ_State_Distr_Amt;
    private DbsField pnd_Summary_Amounts_Pnd_Summ_State_Wthhld_Amt;
    private DbsField pnd_Summary_Amounts_Pnd_Summ_Local_Distr_Amt;
    private DbsField pnd_Summary_Amounts_Pnd_Summ_Local_Wthhld_Amt;
    private DbsField pnd_Summary_Amounts_Pnd_Summ_Res_Gross_Amt;
    private DbsField pnd_Summary_Amounts_Pnd_Summ_Res_Taxable_Amt;
    private DbsField pnd_Summary_Amounts_Pnd_Summ_Res_Ivc_Amt;
    private DbsField pnd_Summary_Amounts_Pnd_Summ_Res_Fed_Tax_Wthld;
    private DbsField pnd_Summary_Amounts_Pnd_Summ_Res_Irr_Amt;
    private DbsField pnd_Sys_Err_Cnt;
    private DbsField pnd_Form_Key;

    private DbsGroup pnd_Form_Key__R_Field_4;
    private DbsField pnd_Form_Key_Pnd_Tax_Year;
    private DbsField pnd_Form_Key_Pnd_Status;
    private DbsField pnd_Hist_Key;

    private DbsGroup pnd_Hist_Key__R_Field_5;
    private DbsField pnd_Hist_Key_Pnd_Tax_Year;
    private DbsField pnd_Hist_Key_Pnd_Tin;
    private DbsField pnd_Hist_Key_Pnd_Form_Type;
    private DbsField pnd_Hist_Key_Pnd_Contract_Nbr;
    private DbsField pnd_Hist_Key_Pnd_Payee;
    private DbsField pnd_Hist_Key_Pnd_Key;
    private DbsField pnd_Hist_Key_Pnd_Ts;
    private DbsField pnd_Hist_Key_Pnd_Irs_Rpt_Ind;
    private DbsField pnd_Sys_Date;
    private DbsField pnd_Sys_Date_N;
    private DbsField pnd_Sys_Time;
    private DbsField pnd_Err_Msg;
    private DbsField pnd_Read_Cnt_In;
    private DbsField pnd_Error_Cnt;
    private DbsField pnd_Et_Cnt;
    private DbsField pnd_1042s_Max;
    private DbsField pnd_Old_Form_Type;
    private DbsField pnd_Old_Company_Cde;
    private DbsField pnd_C_1099_R;
    private DbsField pnd_C_1042_S;
    private DbsField pnd_Nr4_Income_Desc;
    private DbsField pnd_State_Ind_N;
    private DbsField pnd_Reported_To_Irs;

    private DbsRecord internalLoopRecord;
    private DbsField rEAD_FORMTirf_Tax_YearOld;
    private DbsField rEAD_FORMTirf_Company_CdeOld;
    private DbsField rEAD_FORMTirf_Form_TypeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwraplsc = new PdaTwraplsc(localVariables);
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        ldaTwrlcomp = new LdaTwrlcomp();
        registerRecord(ldaTwrlcomp);
        pdaTwrafrmn = new PdaTwrafrmn(localVariables);
        ldaTwrl5001 = new LdaTwrl5001();
        registerRecord(ldaTwrl5001);
        ldaTwrl5950 = new LdaTwrl5950();
        registerRecord(ldaTwrl5950);
        ldaTwrl5951 = new LdaTwrl5951();
        registerRecord(ldaTwrl5951);
        ldaTwrl9710 = new LdaTwrl9710();
        registerRecord(ldaTwrl9710);
        registerRecord(ldaTwrl9710.getVw_form());

        // Local Variables

        vw_hist_Form = new DataAccessProgramView(new NameInfo("vw_hist_Form", "HIST-FORM"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        hist_Form_Tirf_Superde_4 = vw_hist_Form.getRecord().newFieldInGroup("hist_Form_Tirf_Superde_4", "TIRF-SUPERDE-4", FieldType.BINARY, 39, RepeatingFieldStrategy.None, 
            "TIRF_SUPERDE_4");
        hist_Form_Tirf_Superde_4.setSuperDescriptor(true);

        hist_Form__R_Field_1 = vw_hist_Form.getRecord().newGroupInGroup("hist_Form__R_Field_1", "REDEFINE", hist_Form_Tirf_Superde_4);
        hist_Form_Pnd_Tirf_Tax_Year = hist_Form__R_Field_1.newFieldInGroup("hist_Form_Pnd_Tirf_Tax_Year", "#TIRF-TAX-YEAR", FieldType.STRING, 4);
        hist_Form_Pnd_Tirf_Tin = hist_Form__R_Field_1.newFieldInGroup("hist_Form_Pnd_Tirf_Tin", "#TIRF-TIN", FieldType.STRING, 10);
        hist_Form_Pnd_Tirf_Form_Type = hist_Form__R_Field_1.newFieldInGroup("hist_Form_Pnd_Tirf_Form_Type", "#TIRF-FORM-TYPE", FieldType.NUMERIC, 2);
        hist_Form_Pnd_Tirf_Contract_Nbr = hist_Form__R_Field_1.newFieldInGroup("hist_Form_Pnd_Tirf_Contract_Nbr", "#TIRF-CONTRACT-NBR", FieldType.STRING, 
            8);
        hist_Form_Pnd_Tirf_Payee_Cde = hist_Form__R_Field_1.newFieldInGroup("hist_Form_Pnd_Tirf_Payee_Cde", "#TIRF-PAYEE-CDE", FieldType.STRING, 2);
        hist_Form_Pnd_Tirf_Key = hist_Form__R_Field_1.newFieldInGroup("hist_Form_Pnd_Tirf_Key", "#TIRF-KEY", FieldType.STRING, 5);
        hist_Form_Pnd_Tirf_Invrse_Create_Ts = hist_Form__R_Field_1.newFieldInGroup("hist_Form_Pnd_Tirf_Invrse_Create_Ts", "#TIRF-INVRSE-CREATE-TS", FieldType.TIME);
        hist_Form_Pnd_Tirf_Irs_Rpt_Ind = hist_Form__R_Field_1.newFieldInGroup("hist_Form_Pnd_Tirf_Irs_Rpt_Ind", "#TIRF-IRS-RPT-IND", FieldType.STRING, 
            1);
        registerRecord(vw_hist_Form);

        vw_form_1099_R = new DataAccessProgramView(new NameInfo("vw_form_1099_R", "FORM-1099-R"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE", DdmPeriodicGroups.getInstance().getGroups("TWRFRM_FORM_FILE"));
        form_1099_R_Tirf_Tax_Year = vw_form_1099_R.getRecord().newFieldInGroup("form_1099_R_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_TAX_YEAR");
        form_1099_R_Tirf_Tax_Year.setDdmHeader("TAX/YEAR");
        form_1099_R_Tirf_Form_Type = vw_form_1099_R.getRecord().newFieldInGroup("form_1099_R_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "TIRF_FORM_TYPE");
        form_1099_R_Tirf_Form_Type.setDdmHeader("FORM");
        form_1099_R_Tirf_Company_Cde = vw_form_1099_R.getRecord().newFieldInGroup("form_1099_R_Tirf_Company_Cde", "TIRF-COMPANY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRF_COMPANY_CDE");
        form_1099_R_Tirf_Company_Cde.setDdmHeader("COM-/PANY/CODE");
        form_1099_R_Tirf_Tax_Id_Type = vw_form_1099_R.getRecord().newFieldInGroup("form_1099_R_Tirf_Tax_Id_Type", "TIRF-TAX-ID-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRF_TAX_ID_TYPE");
        form_1099_R_Tirf_Tin = vw_form_1099_R.getRecord().newFieldInGroup("form_1099_R_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TIRF_TIN");
        form_1099_R_Tirf_Tin.setDdmHeader("TAX/ID/NUMBER");
        form_1099_R_Tirf_Contract_Nbr = vw_form_1099_R.getRecord().newFieldInGroup("form_1099_R_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TIRF_CONTRACT_NBR");
        form_1099_R_Tirf_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        form_1099_R_Tirf_Payee_Cde = vw_form_1099_R.getRecord().newFieldInGroup("form_1099_R_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_PAYEE_CDE");
        form_1099_R_Tirf_Payee_Cde.setDdmHeader("PAYEE/CODE");
        form_1099_R_Tirf_Key = vw_form_1099_R.getRecord().newFieldInGroup("form_1099_R_Tirf_Key", "TIRF-KEY", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "TIRF_KEY");
        form_1099_R_Tirf_Key.setDdmHeader("KEY");
        form_1099_R_Count_Casttirf_1099_R_State_Grp = vw_form_1099_R.getRecord().newFieldInGroup("form_1099_R_Count_Casttirf_1099_R_State_Grp", "C*TIRF-1099-R-STATE-GRP", 
            RepeatingFieldStrategy.CAsteriskVariable, "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");

        form_1099_R_Tirf_1099_R_State_Grp = vw_form_1099_R.getRecord().newGroupInGroup("form_1099_R_Tirf_1099_R_State_Grp", "TIRF-1099-R-STATE-GRP", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_1099_R_State_Grp.setDdmHeader("1099-R/STATE/GROUP");
        form_1099_R_Tirf_State_Rpt_Ind = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_State_Rpt_Ind", "TIRF-STATE-RPT-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_RPT_IND", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_State_Rpt_Ind.setDdmHeader("STATE/RPT/IND");
        form_1099_R_Tirf_State_Hardcopy_Ind = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_State_Hardcopy_Ind", "TIRF-STATE-HARDCOPY-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_HARDCOPY_IND", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_State_Hardcopy_Ind.setDdmHeader("HARD-/COPY/IND");
        form_1099_R_Tirf_State_Irs_Rpt_Ind = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_State_Irs_Rpt_Ind", "TIRF-STATE-IRS-RPT-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_IRS_RPT_IND", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_State_Irs_Rpt_Ind.setDdmHeader("IRS/RPT/IND");
        form_1099_R_Tirf_State_Code = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_State_Code", "TIRF-STATE-CODE", FieldType.STRING, 
            2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_CODE", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_State_Code.setDdmHeader("RESI-/DENCY/CODE");
        form_1099_R_Tirf_State_Distr = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_State_Distr", "TIRF-STATE-DISTR", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_DISTR", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_State_Distr.setDdmHeader("STATE/DISTRI-/BUTION");
        form_1099_R_Tirf_State_Tax_Wthld = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_State_Tax_Wthld", "TIRF-STATE-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_State_Tax_Wthld.setDdmHeader("STATE/TAX/WITHHELD");
        form_1099_R_Tirf_Loc_Code = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_Loc_Code", "TIRF-LOC-CODE", FieldType.STRING, 
            5, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LOC_CODE", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_Loc_Code.setDdmHeader("LOCA-/LITY/CODE");
        form_1099_R_Tirf_Loc_Distr = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_Loc_Distr", "TIRF-LOC-DISTR", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LOC_DISTR", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_Loc_Distr.setDdmHeader("LOCALITY/DISTRI-/BUTION");
        form_1099_R_Tirf_Loc_Tax_Wthld = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_Loc_Tax_Wthld", "TIRF-LOC-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LOC_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_Loc_Tax_Wthld.setDdmHeader("LOCAL/TAX/WITHHELD");
        form_1099_R_Tirf_State_Auth_Rpt_Ind = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_State_Auth_Rpt_Ind", "TIRF-STATE-AUTH-RPT-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_AUTH_RPT_IND", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_State_Auth_Rpt_Ind.setDdmHeader("STATE/RPT/IND");
        form_1099_R_Tirf_State_Auth_Rpt_Date = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_State_Auth_Rpt_Date", "TIRF-STATE-AUTH-RPT-DATE", 
            FieldType.DATE, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_AUTH_RPT_DATE", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_State_Auth_Rpt_Date.setDdmHeader("STATE/AUTH RPT/DATE");
        registerRecord(vw_form_1099_R);

        pnd_Input_Rec = localVariables.newFieldInRecord("pnd_Input_Rec", "#INPUT-REC", FieldType.STRING, 80);

        pnd_Input_Rec__R_Field_2 = localVariables.newGroupInRecord("pnd_Input_Rec__R_Field_2", "REDEFINE", pnd_Input_Rec);
        pnd_Input_Rec_Pnd_Tax_Year = pnd_Input_Rec__R_Field_2.newFieldInGroup("pnd_Input_Rec_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Form_Text = localVariables.newFieldInRecord("pnd_Form_Text", "#FORM-TEXT", FieldType.STRING, 8);

        pnd_1099r_1099i_Control_Totals = localVariables.newGroupInRecord("pnd_1099r_1099i_Control_Totals", "#1099R-1099I-CONTROL-TOTALS");
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt = pnd_1099r_1099i_Control_Totals.newFieldInGroup("pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt", 
            "#1099R-GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt = pnd_1099r_1099i_Control_Totals.newFieldInGroup("pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt", 
            "#1099R-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld = pnd_1099r_1099i_Control_Totals.newFieldInGroup("pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld", 
            "#1099-FED-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt = pnd_1099r_1099i_Control_Totals.newFieldInGroup("pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt", 
            "#1099R-IVC-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt = pnd_1099r_1099i_Control_Totals.newFieldInGroup("pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt", 
            "#1099R-IRR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Distr = pnd_1099r_1099i_Control_Totals.newFieldInGroup("pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Distr", 
            "#1099R-STATE-DISTR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld = pnd_1099r_1099i_Control_Totals.newFieldInGroup("pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld", 
            "#1099R-STATE-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Distr = pnd_1099r_1099i_Control_Totals.newFieldInGroup("pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Distr", 
            "#1099R-LOC-DISTR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld = pnd_1099r_1099i_Control_Totals.newFieldInGroup("pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld", 
            "#1099R-LOC-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt = pnd_1099r_1099i_Control_Totals.newFieldInGroup("pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt", 
            "#1099I-INT-AMT", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_1099_Grand_Totals = localVariables.newGroupInRecord("pnd_1099_Grand_Totals", "#1099-GRAND-TOTALS");
        pnd_1099_Grand_Totals_Pnd_Total_1099_Form_Cnt = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Form_Cnt", "#TOTAL-1099-FORM-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_1099_Grand_Totals_Pnd_Total_1099_Hold_Cnt = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Hold_Cnt", "#TOTAL-1099-HOLD-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_1099_Grand_Totals_Pnd_Total_1099_Empty_Form_Cnt = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Empty_Form_Cnt", 
            "#TOTAL-1099-EMPTY-FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_1099_Grand_Totals_Pnd_Total_1099_Gross_Amt = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Gross_Amt", "#TOTAL-1099-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_Pnd_Total_1099_Taxable_Amt = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Taxable_Amt", "#TOTAL-1099-TAXABLE-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_Pnd_Total_1099_Fed_Tax_Wthld = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Fed_Tax_Wthld", 
            "#TOTAL-1099-FED-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_Pnd_Total_1099_Ivc_Amt = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Ivc_Amt", "#TOTAL-1099-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_Pnd_Total_1099_Irr_Amt = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Irr_Amt", "#TOTAL-1099-IRR-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_Pnd_Total_1099_State_Distr = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_State_Distr", "#TOTAL-1099-STATE-DISTR", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_Pnd_Total_1099_State_Tax_Wthld = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_State_Tax_Wthld", 
            "#TOTAL-1099-STATE-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_Pnd_Total_1099_Loc_Distr = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Loc_Distr", "#TOTAL-1099-LOC-DISTR", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_Pnd_Total_1099_Loc_Tax_Wthld = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Loc_Tax_Wthld", 
            "#TOTAL-1099-LOC-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_Pnd_Total_1099_Int_Amt = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Int_Amt", "#TOTAL-1099-INT-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);

        pnd_1042s_Control_Totals = localVariables.newGroupArrayInRecord("pnd_1042s_Control_Totals", "#1042S-CONTROL-TOTALS", new DbsArrayController(1, 
            2));
        pnd_1042s_Control_Totals_Pnd_1042s_Trans_Count = pnd_1042s_Control_Totals.newFieldInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Trans_Count", "#1042S-TRANS-COUNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income = pnd_1042s_Control_Totals.newFieldInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income", 
            "#1042S-GROSS-INCOME", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt = pnd_1042s_Control_Totals.newFieldInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt", 
            "#1042S-INTEREST-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt = pnd_1042s_Control_Totals.newFieldInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt", "#1042S-PENSION-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld = pnd_1042s_Control_Totals.newFieldInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld", 
            "#1042S-NRA-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt = pnd_1042s_Control_Totals.newFieldInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt", "#1042S-REFUND-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);

        pnd_1042s_Totals = localVariables.newGroupInRecord("pnd_1042s_Totals", "#1042S-TOTALS");
        pnd_1042s_Totals_Pnd_1042s_Total_Gross = pnd_1042s_Totals.newFieldInGroup("pnd_1042s_Totals_Pnd_1042s_Total_Gross", "#1042S-TOTAL-GROSS", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1042s_Totals_Pnd_1042s_Total_Interest = pnd_1042s_Totals.newFieldInGroup("pnd_1042s_Totals_Pnd_1042s_Total_Interest", "#1042S-TOTAL-INTEREST", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Totals_Pnd_1042s_Total_Pension = pnd_1042s_Totals.newFieldInGroup("pnd_1042s_Totals_Pnd_1042s_Total_Pension", "#1042S-TOTAL-PENSION", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Totals_Pnd_1042s_Total_Tax = pnd_1042s_Totals.newFieldInGroup("pnd_1042s_Totals_Pnd_1042s_Total_Tax", "#1042S-TOTAL-TAX", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1042s_Totals_Pnd_1042s_Total_Refund = pnd_1042s_Totals.newFieldInGroup("pnd_1042s_Totals_Pnd_1042s_Total_Refund", "#1042S-TOTAL-REFUND", FieldType.PACKED_DECIMAL, 
            13, 2);

        pnd_5498_Control_Totals = localVariables.newGroupInRecord("pnd_5498_Control_Totals", "#5498-CONTROL-TOTALS");
        pnd_5498_Control_Totals_Pnd_5498_Form_Cnt = pnd_5498_Control_Totals.newFieldInGroup("pnd_5498_Control_Totals_Pnd_5498_Form_Cnt", "#5498-FORM-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Contrib = pnd_5498_Control_Totals.newFieldInGroup("pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Contrib", 
            "#5498-TRAD-IRA-CONTRIB", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Contrib = pnd_5498_Control_Totals.newFieldInGroup("pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Contrib", 
            "#5498-ROTH-IRA-CONTRIB", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rollover = pnd_5498_Control_Totals.newFieldInGroup("pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rollover", 
            "#5498-TRAD-IRA-ROLLOVER", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rechar = pnd_5498_Control_Totals.newFieldInGroup("pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rechar", 
            "#5498-TRAD-IRA-RECHAR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Conversion = pnd_5498_Control_Totals.newFieldInGroup("pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Conversion", 
            "#5498-ROTH-IRA-CONVERSION", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Pnd_5498_Account_Fmv = pnd_5498_Control_Totals.newFieldInGroup("pnd_5498_Control_Totals_Pnd_5498_Account_Fmv", "#5498-ACCOUNT-FMV", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Pnd_5498_Sep_Amt = pnd_5498_Control_Totals.newFieldInGroup("pnd_5498_Control_Totals_Pnd_5498_Sep_Amt", "#5498-SEP-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Pnd_5498_Postpn_Amt = pnd_5498_Control_Totals.newFieldInGroup("pnd_5498_Control_Totals_Pnd_5498_Postpn_Amt", "#5498-POSTPN-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Pnd_5498_Repayments_Amt = pnd_5498_Control_Totals.newFieldInGroup("pnd_5498_Control_Totals_Pnd_5498_Repayments_Amt", "#5498-REPAYMENTS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);

        pnd_4807c_Control_Totals = localVariables.newGroupArrayInRecord("pnd_4807c_Control_Totals", "#4807C-CONTROL-TOTALS", new DbsArrayController(1, 
            2));
        pnd_4807c_Control_Totals_Pnd_4807c_Form_Cnt = pnd_4807c_Control_Totals.newFieldInGroup("pnd_4807c_Control_Totals_Pnd_4807c_Form_Cnt", "#4807C-FORM-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_4807c_Control_Totals_Pnd_4807c_Empty_Form_Cnt = pnd_4807c_Control_Totals.newFieldInGroup("pnd_4807c_Control_Totals_Pnd_4807c_Empty_Form_Cnt", 
            "#4807C-EMPTY-FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_4807c_Control_Totals_Pnd_4807c_Hold_Cnt = pnd_4807c_Control_Totals.newFieldInGroup("pnd_4807c_Control_Totals_Pnd_4807c_Hold_Cnt", "#4807C-HOLD-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt = pnd_4807c_Control_Totals.newFieldInGroup("pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt", "#4807C-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_Pnd_4807c_Int_Amt = pnd_4807c_Control_Totals.newFieldInGroup("pnd_4807c_Control_Totals_Pnd_4807c_Int_Amt", "#4807C-INT-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_Pnd_4807c_Distrib_Amt = pnd_4807c_Control_Totals.newFieldInGroup("pnd_4807c_Control_Totals_Pnd_4807c_Distrib_Amt", "#4807C-DISTRIB-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_Pnd_4807c_Taxable_Amt = pnd_4807c_Control_Totals.newFieldInGroup("pnd_4807c_Control_Totals_Pnd_4807c_Taxable_Amt", "#4807C-TAXABLE-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt = pnd_4807c_Control_Totals.newFieldInGroup("pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt", "#4807C-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt_Roll = pnd_4807c_Control_Totals.newFieldInGroup("pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt_Roll", 
            "#4807C-GROSS-AMT-ROLL", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt_Roll = pnd_4807c_Control_Totals.newFieldInGroup("pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt_Roll", 
            "#4807C-IVC-AMT-ROLL", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Nr4_Control_Totals = localVariables.newGroupArrayInRecord("pnd_Nr4_Control_Totals", "#NR4-CONTROL-TOTALS", new DbsArrayController(1, 4));
        pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt", "#NR4-FORM-CNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Nr4_Control_Totals_Pnd_Nr4_Hold_Cnt = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Hold_Cnt", "#NR4-HOLD-CNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Nr4_Control_Totals_Pnd_Nr4_Income_Code = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Income_Code", "#NR4-INCOME-CODE", 
            FieldType.STRING, 2);
        pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt", "#NR4-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt", "#NR4-INTEREST-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld", "#NR4-FED-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Accum_F10 = localVariables.newGroupArrayInRecord("pnd_Accum_F10", "#ACCUM-F10", new DbsArrayController(1, 3));
        pnd_Accum_F10_Pnd_Cont_Cnt_F10 = pnd_Accum_F10.newFieldInGroup("pnd_Accum_F10_Pnd_Cont_Cnt_F10", "#CONT-CNT-F10", FieldType.PACKED_DECIMAL, 7);
        pnd_Accum_F10_Pnd_Form_Cnt_F10 = pnd_Accum_F10.newFieldInGroup("pnd_Accum_F10_Pnd_Form_Cnt_F10", "#FORM-CNT-F10", FieldType.PACKED_DECIMAL, 7);
        pnd_Accum_F10_Pnd_Gross_Amt_F10 = pnd_Accum_F10.newFieldInGroup("pnd_Accum_F10_Pnd_Gross_Amt_F10", "#GROSS-AMT-F10", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Accum_F10_Pnd_Fed_Wthld_F10 = pnd_Accum_F10.newFieldInGroup("pnd_Accum_F10_Pnd_Fed_Wthld_F10", "#FED-WTHLD-F10", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Accum_F10_Pnd_St_Wthld_F10 = pnd_Accum_F10.newFieldInGroup("pnd_Accum_F10_Pnd_St_Wthld_F10", "#ST-WTHLD-F10", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Accum_F10_Pnd_Loc_Wthld_F10 = pnd_Accum_F10.newFieldInGroup("pnd_Accum_F10_Pnd_Loc_Wthld_F10", "#LOC-WTHLD-F10", FieldType.PACKED_DECIMAL, 
            13, 2);

        pnd_Vars_F10 = localVariables.newGroupInRecord("pnd_Vars_F10", "#VARS-F10");
        pnd_Vars_F10_Pnd_I_F10 = pnd_Vars_F10.newFieldInGroup("pnd_Vars_F10_Pnd_I_F10", "#I-F10", FieldType.PACKED_DECIMAL, 3);
        pnd_Vars_F10_Pnd_Ndx_F10 = pnd_Vars_F10.newFieldInGroup("pnd_Vars_F10_Pnd_Ndx_F10", "#NDX-F10", FieldType.PACKED_DECIMAL, 3);
        pnd_Vars_F10_Pnd_All_Gross_F10 = pnd_Vars_F10.newFieldInGroup("pnd_Vars_F10_Pnd_All_Gross_F10", "#ALL-GROSS-F10", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Vars_F10_Pnd_Gross_Accum_F10 = pnd_Vars_F10.newFieldInGroup("pnd_Vars_F10_Pnd_Gross_Accum_F10", "#GROSS-ACCUM-F10", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Vars_F10_Pnd_Contract_Amt_Err_F10 = pnd_Vars_F10.newFieldInGroup("pnd_Vars_F10_Pnd_Contract_Amt_Err_F10", "#CONTRACT-AMT-ERR-F10", FieldType.BOOLEAN, 
            1);
        pnd_Form_Cnt = localVariables.newFieldInRecord("pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Hold_Cnt = localVariables.newFieldInRecord("pnd_Hold_Cnt", "#HOLD-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Empty_Form_Cnt = localVariables.newFieldInRecord("pnd_Empty_Form_Cnt", "#EMPTY-FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Company_Ndx = localVariables.newFieldInRecord("pnd_Company_Ndx", "#COMPANY-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Err_Ndx = localVariables.newFieldInRecord("pnd_Err_Ndx", "#ERR-NDX", FieldType.NUMERIC, 3);
        pnd_Nr4_Ndx = localVariables.newFieldInRecord("pnd_Nr4_Ndx", "#NR4-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Sys_Err_Ndx = localVariables.newFieldInRecord("pnd_Sys_Err_Ndx", "#SYS-ERR-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_1099_State_Ndx = localVariables.newFieldInRecord("pnd_1099_State_Ndx", "#1099-STATE-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_State_Tbl_Ndx = localVariables.newFieldInRecord("pnd_State_Tbl_Ndx", "#STATE-TBL-NDX", FieldType.NUMERIC, 3);

        pnd_State_Tbl_Ndx__R_Field_3 = localVariables.newGroupInRecord("pnd_State_Tbl_Ndx__R_Field_3", "REDEFINE", pnd_State_Tbl_Ndx);
        pnd_State_Tbl_Ndx_Pnd_State_Tbl_Ndx_A = pnd_State_Tbl_Ndx__R_Field_3.newFieldInGroup("pnd_State_Tbl_Ndx_Pnd_State_Tbl_Ndx_A", "#STATE-TBL-NDX-A", 
            FieldType.STRING, 3);
        pnd_Reportable_Ndx = localVariables.newFieldInRecord("pnd_Reportable_Ndx", "#REPORTABLE-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_State_Ndx = localVariables.newFieldInRecord("pnd_State_Ndx", "#STATE-NDX", FieldType.PACKED_DECIMAL, 3);

        pnd_1099_State_Tbl = localVariables.newGroupArrayInRecord("pnd_1099_State_Tbl", "#1099-STATE-TBL", new DbsArrayController(1, 101, 1, 3));
        pnd_1099_State_Tbl_Pnd_State_Form_Cnt = pnd_1099_State_Tbl.newFieldInGroup("pnd_1099_State_Tbl_Pnd_State_Form_Cnt", "#STATE-FORM-CNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_1099_State_Tbl_Pnd_State_Distr_Amt = pnd_1099_State_Tbl.newFieldInGroup("pnd_1099_State_Tbl_Pnd_State_Distr_Amt", "#STATE-DISTR-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099_State_Tbl_Pnd_State_Wthhld_Amt = pnd_1099_State_Tbl.newFieldInGroup("pnd_1099_State_Tbl_Pnd_State_Wthhld_Amt", "#STATE-WTHHLD-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099_State_Tbl_Pnd_Local_Distr_Amt = pnd_1099_State_Tbl.newFieldInGroup("pnd_1099_State_Tbl_Pnd_Local_Distr_Amt", "#LOCAL-DISTR-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099_State_Tbl_Pnd_Local_Wthhld_Amt = pnd_1099_State_Tbl.newFieldInGroup("pnd_1099_State_Tbl_Pnd_Local_Wthhld_Amt", "#LOCAL-WTHHLD-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099_State_Tbl_Pnd_Res_Gross_Amt = pnd_1099_State_Tbl.newFieldInGroup("pnd_1099_State_Tbl_Pnd_Res_Gross_Amt", "#RES-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099_State_Tbl_Pnd_Res_Taxable_Amt = pnd_1099_State_Tbl.newFieldInGroup("pnd_1099_State_Tbl_Pnd_Res_Taxable_Amt", "#RES-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099_State_Tbl_Pnd_Res_Ivc_Amt = pnd_1099_State_Tbl.newFieldInGroup("pnd_1099_State_Tbl_Pnd_Res_Ivc_Amt", "#RES-IVC-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099_State_Tbl_Pnd_Res_Fed_Tax_Wthld = pnd_1099_State_Tbl.newFieldInGroup("pnd_1099_State_Tbl_Pnd_Res_Fed_Tax_Wthld", "#RES-FED-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_State_Tbl_Pnd_Res_Irr_Amt = pnd_1099_State_Tbl.newFieldInGroup("pnd_1099_State_Tbl_Pnd_Res_Irr_Amt", "#RES-IRR-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);

        pnd_Summary_Amounts = localVariables.newGroupInRecord("pnd_Summary_Amounts", "#SUMMARY-AMOUNTS");
        pnd_Summary_Amounts_Pnd_Summ_State_Form_Cnt = pnd_Summary_Amounts.newFieldInGroup("pnd_Summary_Amounts_Pnd_Summ_State_Form_Cnt", "#SUMM-STATE-FORM-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Summary_Amounts_Pnd_Summ_State_Distr_Amt = pnd_Summary_Amounts.newFieldInGroup("pnd_Summary_Amounts_Pnd_Summ_State_Distr_Amt", "#SUMM-STATE-DISTR-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Amounts_Pnd_Summ_State_Wthhld_Amt = pnd_Summary_Amounts.newFieldInGroup("pnd_Summary_Amounts_Pnd_Summ_State_Wthhld_Amt", "#SUMM-STATE-WTHHLD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Amounts_Pnd_Summ_Local_Distr_Amt = pnd_Summary_Amounts.newFieldInGroup("pnd_Summary_Amounts_Pnd_Summ_Local_Distr_Amt", "#SUMM-LOCAL-DISTR-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Amounts_Pnd_Summ_Local_Wthhld_Amt = pnd_Summary_Amounts.newFieldInGroup("pnd_Summary_Amounts_Pnd_Summ_Local_Wthhld_Amt", "#SUMM-LOCAL-WTHHLD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Amounts_Pnd_Summ_Res_Gross_Amt = pnd_Summary_Amounts.newFieldInGroup("pnd_Summary_Amounts_Pnd_Summ_Res_Gross_Amt", "#SUMM-RES-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Amounts_Pnd_Summ_Res_Taxable_Amt = pnd_Summary_Amounts.newFieldInGroup("pnd_Summary_Amounts_Pnd_Summ_Res_Taxable_Amt", "#SUMM-RES-TAXABLE-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Amounts_Pnd_Summ_Res_Ivc_Amt = pnd_Summary_Amounts.newFieldInGroup("pnd_Summary_Amounts_Pnd_Summ_Res_Ivc_Amt", "#SUMM-RES-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Amounts_Pnd_Summ_Res_Fed_Tax_Wthld = pnd_Summary_Amounts.newFieldInGroup("pnd_Summary_Amounts_Pnd_Summ_Res_Fed_Tax_Wthld", "#SUMM-RES-FED-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Amounts_Pnd_Summ_Res_Irr_Amt = pnd_Summary_Amounts.newFieldInGroup("pnd_Summary_Amounts_Pnd_Summ_Res_Irr_Amt", "#SUMM-RES-IRR-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Sys_Err_Cnt = localVariables.newFieldArrayInRecord("pnd_Sys_Err_Cnt", "#SYS-ERR-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            61));
        pnd_Form_Key = localVariables.newFieldInRecord("pnd_Form_Key", "#FORM-KEY", FieldType.STRING, 5);

        pnd_Form_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Form_Key__R_Field_4", "REDEFINE", pnd_Form_Key);
        pnd_Form_Key_Pnd_Tax_Year = pnd_Form_Key__R_Field_4.newFieldInGroup("pnd_Form_Key_Pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);
        pnd_Form_Key_Pnd_Status = pnd_Form_Key__R_Field_4.newFieldInGroup("pnd_Form_Key_Pnd_Status", "#STATUS", FieldType.STRING, 1);
        pnd_Hist_Key = localVariables.newFieldInRecord("pnd_Hist_Key", "#HIST-KEY", FieldType.STRING, 39);

        pnd_Hist_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Hist_Key__R_Field_5", "REDEFINE", pnd_Hist_Key);
        pnd_Hist_Key_Pnd_Tax_Year = pnd_Hist_Key__R_Field_5.newFieldInGroup("pnd_Hist_Key_Pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);
        pnd_Hist_Key_Pnd_Tin = pnd_Hist_Key__R_Field_5.newFieldInGroup("pnd_Hist_Key_Pnd_Tin", "#TIN", FieldType.STRING, 10);
        pnd_Hist_Key_Pnd_Form_Type = pnd_Hist_Key__R_Field_5.newFieldInGroup("pnd_Hist_Key_Pnd_Form_Type", "#FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_Hist_Key_Pnd_Contract_Nbr = pnd_Hist_Key__R_Field_5.newFieldInGroup("pnd_Hist_Key_Pnd_Contract_Nbr", "#CONTRACT-NBR", FieldType.STRING, 8);
        pnd_Hist_Key_Pnd_Payee = pnd_Hist_Key__R_Field_5.newFieldInGroup("pnd_Hist_Key_Pnd_Payee", "#PAYEE", FieldType.STRING, 2);
        pnd_Hist_Key_Pnd_Key = pnd_Hist_Key__R_Field_5.newFieldInGroup("pnd_Hist_Key_Pnd_Key", "#KEY", FieldType.STRING, 5);
        pnd_Hist_Key_Pnd_Ts = pnd_Hist_Key__R_Field_5.newFieldInGroup("pnd_Hist_Key_Pnd_Ts", "#TS", FieldType.TIME);
        pnd_Hist_Key_Pnd_Irs_Rpt_Ind = pnd_Hist_Key__R_Field_5.newFieldInGroup("pnd_Hist_Key_Pnd_Irs_Rpt_Ind", "#IRS-RPT-IND", FieldType.STRING, 1);
        pnd_Sys_Date = localVariables.newFieldInRecord("pnd_Sys_Date", "#SYS-DATE", FieldType.DATE);
        pnd_Sys_Date_N = localVariables.newFieldInRecord("pnd_Sys_Date_N", "#SYS-DATE-N", FieldType.NUMERIC, 8);
        pnd_Sys_Time = localVariables.newFieldInRecord("pnd_Sys_Time", "#SYS-TIME", FieldType.TIME);
        pnd_Err_Msg = localVariables.newFieldInRecord("pnd_Err_Msg", "#ERR-MSG", FieldType.STRING, 60);
        pnd_Read_Cnt_In = localVariables.newFieldInRecord("pnd_Read_Cnt_In", "#READ-CNT-IN", FieldType.PACKED_DECIMAL, 7);
        pnd_Error_Cnt = localVariables.newFieldInRecord("pnd_Error_Cnt", "#ERROR-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Et_Cnt = localVariables.newFieldInRecord("pnd_Et_Cnt", "#ET-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_1042s_Max = localVariables.newFieldInRecord("pnd_1042s_Max", "#1042S-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Old_Form_Type = localVariables.newFieldInRecord("pnd_Old_Form_Type", "#OLD-FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_Old_Company_Cde = localVariables.newFieldInRecord("pnd_Old_Company_Cde", "#OLD-COMPANY-CDE", FieldType.STRING, 1);
        pnd_C_1099_R = localVariables.newFieldInRecord("pnd_C_1099_R", "#C-1099-R", FieldType.PACKED_DECIMAL, 3);
        pnd_C_1042_S = localVariables.newFieldInRecord("pnd_C_1042_S", "#C-1042-S", FieldType.PACKED_DECIMAL, 3);
        pnd_Nr4_Income_Desc = localVariables.newFieldInRecord("pnd_Nr4_Income_Desc", "#NR4-INCOME-DESC", FieldType.STRING, 16);
        pnd_State_Ind_N = localVariables.newFieldInRecord("pnd_State_Ind_N", "#STATE-IND-N", FieldType.NUMERIC, 1);
        pnd_Reported_To_Irs = localVariables.newFieldInRecord("pnd_Reported_To_Irs", "#REPORTED-TO-IRS", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        rEAD_FORMTirf_Tax_YearOld = internalLoopRecord.newFieldInRecord("READ_FORM_Tirf_Tax_Year_OLD", "Tirf_Tax_Year_OLD", FieldType.STRING, 4);
        rEAD_FORMTirf_Company_CdeOld = internalLoopRecord.newFieldInRecord("READ_FORM_Tirf_Company_Cde_OLD", "Tirf_Company_Cde_OLD", FieldType.STRING, 
            1);
        rEAD_FORMTirf_Form_TypeOld = internalLoopRecord.newFieldInRecord("READ_FORM_Tirf_Form_Type_OLD", "Tirf_Form_Type_OLD", FieldType.NUMERIC, 2);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_hist_Form.reset();
        vw_form_1099_R.reset();
        internalLoopRecord.reset();

        ldaTwrlcomp.initializeValues();
        ldaTwrl5001.initializeValues();
        ldaTwrl5950.initializeValues();
        ldaTwrl5951.initializeValues();
        ldaTwrl9710.initializeValues();

        localVariables.reset();
        pnd_Vars_F10_Pnd_Contract_Amt_Err_F10.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5963() throws Exception
    {
        super("Twrp5963");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp5963|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT ( 0 ) PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 1 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 2 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 3 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 4 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 5 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 6 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 7 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 8 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 9 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 10 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 11 ) PS = 58 LS = 133 ZP = ON
                getReports().definePrinter(3, "'CMPRT10'");                                                                                                               //Natural: DEFINE PRINTER ( 2 ) OUTPUT 'CMPRT10'
                getReports().definePrinter(4, "'CMPRT10'");                                                                                                               //Natural: DEFINE PRINTER ( 3 ) OUTPUT 'CMPRT10'
                getReports().definePrinter(5, "'CMPRT10'");                                                                                                               //Natural: DEFINE PRINTER ( 4 ) OUTPUT 'CMPRT10'
                getReports().definePrinter(6, "'CMPRT10'");                                                                                                               //Natural: DEFINE PRINTER ( 5 ) OUTPUT 'CMPRT10'
                getReports().definePrinter(7, "'CMPRT10'");                                                                                                               //Natural: DEFINE PRINTER ( 6 ) OUTPUT 'CMPRT10'
                //*  ACTIVE
                getReports().definePrinter(12, "'CMPRT10'");                                                                                                              //Natural: DEFINE PRINTER ( 11 ) OUTPUT 'CMPRT10'
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                pnd_Sys_Date.setValue(Global.getDATX());                                                                                                                  //Natural: ASSIGN #SYS-DATE := *DATX
                pnd_Sys_Time.setValue(Global.getTIMX());                                                                                                                  //Natural: ASSIGN #SYS-TIME := *TIMX
                pnd_Sys_Date_N.setValue(Global.getDATN());                                                                                                                //Natural: ASSIGN #SYS-DATE-N := *DATN
                //*    REPORT HEADING
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 1 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 22T 'Tax Withholding and Reporting System' 68T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 34T 'Control Report' 68T 'Report: RPT1' // 'Tax Year: ' #INPUT-REC.#TAX-YEAR ( EM = 9999 ) //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 2 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'YTD Form Database Control' 110T 'Report: RPT2' / 59T 'Tax Year' #INPUT-REC.#TAX-YEAR ( EM = 9999 ) // 'Company: ' #TWRLCOMP.#COMP-SHORT-NAME ( #COMPANY-NDX ) //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 3 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 03 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'YTD Form Database Control' 110T 'Report: RPT3' / 59T 'Tax Year' #INPUT-REC.#TAX-YEAR ( EM = 9999 ) // 'Company: ' #TWRLCOMP.#COMP-SHORT-NAME ( #COMPANY-NDX ) //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 4 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 04 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'YTD Form Database Control' 110T 'Report: RPT4' / 59T 'Tax Year' #INPUT-REC.#TAX-YEAR ( EM = 9999 ) // 'Company: ' #TWRLCOMP.#COMP-SHORT-NAME ( #COMPANY-NDX ) //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 5 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 05 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'YTD Form Database Control' 110T 'Report: RPT5' / 59T 'Tax Year' #INPUT-REC.#TAX-YEAR ( EM = 9999 ) // 'Company: ' #TWRLCOMP.#COMP-SHORT-NAME ( #COMPANY-NDX ) //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 6 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 06 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'YTD Form Database Control' 110T 'Report: RPT6' / 59T 'Tax Year' #INPUT-REC.#TAX-YEAR ( EM = 9999 ) // 'Company: ' #TWRLCOMP.#COMP-SHORT-NAME ( #COMPANY-NDX ) //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 7 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 07 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'YTD Form Database Control' 110T 'Report: RPT7' / 59T 'Tax Year' #INPUT-REC.#TAX-YEAR ( EM = 9999 ) // 'Company:' #TWRLCOMP.#COMP-SHORT-NAME ( #COMPANY-NDX ) / 26T 'Reportable State Transactions' 90T 'Non-Reportable State Transactions' //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 8 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 08 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'YTD Form Database Control' 110T 'Report: RPT8' / 55T 'Hard Copy State Report' / 59T 'Tax Year' #INPUT-REC.#TAX-YEAR ( EM = 9999 ) // 'Company:' #TWRLCOMP.#COMP-SHORT-NAME ( #COMPANY-NDX ) //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 9 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 09 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'YTD Form Database Control' 110T 'Report: RPT9' / 59T 'Tax Year' #INPUT-REC.#TAX-YEAR ( EM = 9999 ) // 'Company:' #TWRLCOMP.#COMP-SHORT-NAME ( #COMPANY-NDX ) // //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 11 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 48T 'SUNY/CUNY YTD Form Database Control' 110T 'Report: RPT2' /
                //* ***************
                //*  MAIN PROGRAM *
                //* ***************
                //*    GET TAX YEAR FROM DRIVER PROGRAM
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Rec_Pnd_Tax_Year);                                                                           //Natural: INPUT #INPUT-REC.#TAX-YEAR
                //*    READ FORM FILE
                pnd_Form_Key_Pnd_Tax_Year.setValue(pnd_Input_Rec_Pnd_Tax_Year);                                                                                           //Natural: ASSIGN #FORM-KEY.#TAX-YEAR := #TWRAFRMN.#TAX-YEAR := #INPUT-REC.#TAX-YEAR
                pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Tax_Year().setValue(pnd_Input_Rec_Pnd_Tax_Year);
                pnd_Form_Key_Pnd_Status.setValue("A");                                                                                                                    //Natural: ASSIGN #FORM-KEY.#STATUS := 'A'
                //*  01/30/08 - AY
                DbsUtil.callnat(Twrnfrmn.class , getCurrentProcessState(), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNFRMN' USING #TWRAFRMN.#INPUT-PARMS ( AD = O ) #TWRAFRMN.#OUTPUT-DATA ( AD = M )
                    pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                DbsUtil.callnat(Twrnplsc.class , getCurrentProcessState(), pdaTwraplsc.getTwraplsc_Input_Parms(), new AttributeParameter("O"), pdaTwraplsc.getTwraplsc_Output_Data(),  //Natural: CALLNAT 'TWRNPLSC' USING TWRAPLSC.INPUT-PARMS ( AD = O ) TWRAPLSC.OUTPUT-DATA ( AD = M )
                    new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                ldaTwrl9710.getVw_form().startDatabaseRead                                                                                                                //Natural: READ FORM BY TIRF-SUPERDE-6 = #FORM-KEY
                (
                "READ_FORM",
                new Wc[] { new Wc("TIRF_SUPERDE_6", ">=", pnd_Form_Key, WcType.BY) },
                new Oc[] { new Oc("TIRF_SUPERDE_6", "ASC") }
                );
                boolean endOfDataReadForm = true;
                boolean firstReadForm = true;
                READ_FORM:
                while (condition(ldaTwrl9710.getVw_form().readNextRow("READ_FORM")))
                {
                    CheckAtStartofData753();

                    if (condition(ldaTwrl9710.getVw_form().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventRead_Form();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataReadForm = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().notEquals(pnd_Form_Key_Pnd_Tax_Year)))                                                              //Natural: IF TIRF-TAX-YEAR NE #FORM-KEY.#TAX-YEAR
                    {
                        if (true) break READ_FORM;                                                                                                                        //Natural: ESCAPE BOTTOM ( READ-FORM. )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().equals(10)))                                                                                       //Natural: IF TIRF-FORM-TYPE = 10
                    {
                                                                                                                                                                          //Natural: PERFORM SUNY-FORM-DB-CONTROL
                        sub_Suny_Form_Db_Control();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    //*                                                                                                                                                   //Natural: AT START OF DATA
                    //*  BEFORE BREAK PROCESSING
                    //*    ACCEPT IF #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE
                    //*      = 'J' OR = 'Q' OR = 'T'
                    //*    ACCEPT IF TIRF-TIN                       = '121620003'
                    //*      AND     #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR     = 'J8300004'
                    //*      AND     #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR     = 'K9110962'
                    //*      AND     #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE = '1'
                    //*  END-BEFORE
                    //*                                                                                                                                                   //Natural: AT BREAK OF TIRF-FORM-TYPE
                    //*                                                                                                                                                   //Natural: AT BREAK OF TIRF-COMPANY-CDE
                    //*    CHECK EMPTY FORM
                    if (condition(ldaTwrl9710.getForm_Tirf_Empty_Form().getBoolean()))                                                                                    //Natural: IF TIRF-EMPTY-FORM
                    {
                        pnd_Empty_Form_Cnt.nadd(1);                                                                                                                       //Natural: ADD 1 TO #EMPTY-FORM-CNT
                                                                                                                                                                          //Natural: PERFORM CHECK-REPORTED-TO-IRS
                        sub_Check_Reported_To_Irs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(pnd_Reported_To_Irs.getBoolean()))                                                                                                  //Natural: IF #REPORTED-TO-IRS
                        {
                                                                                                                                                                          //Natural: PERFORM CHECK-SYSTEM-HOLD-CODE
                            sub_Check_System_Hold_Code();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*    CHECK HOLD INDICATOR
                                                                                                                                                                          //Natural: PERFORM CHECK-SYSTEM-HOLD-CODE
                    sub_Check_System_Hold_Code();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*    ACCUMMULATE TOTALS BY FORM TYPE
                    pnd_Form_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #FORM-CNT
                    //*  1099-R
                    short decideConditionsMet861 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF TIRF-FORM-TYPE;//Natural: VALUE 1
                    if (condition((ldaTwrl9710.getForm_Tirf_Form_Type().equals(1))))
                    {
                        decideConditionsMet861++;
                        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt.nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                    //Natural: ADD TIRF-GROSS-AMT TO #1099R-GROSS-AMT
                        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt.nadd(ldaTwrl9710.getForm_Tirf_Taxable_Amt());                                                //Natural: ADD TIRF-TAXABLE-AMT TO #1099R-TAXABLE-AMT
                        pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld.nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                             //Natural: ADD TIRF-FED-TAX-WTHLD TO #1099-FED-TAX-WTHLD
                        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt.nadd(ldaTwrl9710.getForm_Tirf_Ivc_Amt());                                                        //Natural: ADD TIRF-IVC-AMT TO #1099R-IVC-AMT
                        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt.nadd(ldaTwrl9710.getForm_Tirf_Irr_Amt());                                                        //Natural: ADD TIRF-IRR-AMT TO #1099R-IRR-AMT
                        if (condition(ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp().notEquals(getZero())))                                                        //Natural: IF C*TIRF-1099-R-STATE-GRP NE 0
                        {
                            pnd_C_1099_R.setValue(ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp());                                                                 //Natural: ASSIGN #C-1099-R := C*TIRF-1099-R-STATE-GRP
                            pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld.nadd(ldaTwrl9710.getForm_Tirf_State_Tax_Wthld().getValue(1,":",pnd_C_1099_R));       //Natural: ADD TIRF-STATE-TAX-WTHLD ( 1:#C-1099-R ) TO #1099R-STATE-TAX-WTHLD
                            pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld.nadd(ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(1,":",pnd_C_1099_R));           //Natural: ADD TIRF-LOC-TAX-WTHLD ( 1:#C-1099-R ) TO #1099R-LOC-TAX-WTHLD
                                                                                                                                                                          //Natural: PERFORM POPULATE-1099-STATE-TABLE
                            sub_Populate_1099_State_Table();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            //*  1099-INT
                        }                                                                                                                                                 //Natural: END-IF
                        //*      FOR #I = 1 TO C*TIRF-1099-R-STATE-GRP
                        //*        IF FORM.TIRF-STATE-CODE(#I) NE '01' ESCAPE TOP END-IF
                        //*        DISPLAY
                        //*          '/TIN'             TIRF-TIN
                        //*          '/Contract'        TIRF-CONTRACT-NBR
                        //*          '/PY'              TIRF-PAYEE-CDE
                        //*          '/Key'             TIRF-KEY
                        //*          '/Gross'           TIRF-GROSS-AMT
                        //*          'IRA/SEP'          TIRF-IRA-DISTR
                        //*          '/TND'             TIRF-TAXABLE-NOT-DET
                        //*          '/IVC'             TIRF-IVC-AMT
                        //*          '/Taxable'         TIRF-TAXABLE-AMT
                        //*          '/OCC'             C*TIRF-1099-R-STATE-GRP(NL=2)
                        //*          'Res/Gross'        TIRF-RES-GROSS-AMT          (#I)
                        //*          'Res/TND'          TIRF-RES-TAXABLE-NOT-DET    (#I)
                        //*          'Res/IVC'          TIRF-RES-IVC-AMT            (#I)
                        //*          'Res/Taxable'      TIRF-RES-TAXABLE-AMT        (#I)
                        //*        WRITE WORK FILE 5
                        //*          TIRF-TIN
                        //*          TIRF-CONTRACT-NBR
                        //*          TIRF-PAYEE-CDE
                        //*          TIRF-DISTRIBUTION-CDE
                        //*          H'00'
                        //*          TIRF-RES-TAXABLE-AMT        (#I)
                        //*      END-FOR
                    }                                                                                                                                                     //Natural: VALUE 2
                    else if (condition((ldaTwrl9710.getForm_Tirf_Form_Type().equals(2))))
                    {
                        decideConditionsMet861++;
                        pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt.nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                        //Natural: ADD TIRF-INT-AMT TO #1099I-INT-AMT
                        //*  1042-S
                        pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld.nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                             //Natural: ADD TIRF-FED-TAX-WTHLD TO #1099-FED-TAX-WTHLD
                    }                                                                                                                                                     //Natural: VALUE 3
                    else if (condition((ldaTwrl9710.getForm_Tirf_Form_Type().equals(3))))
                    {
                        decideConditionsMet861++;
                        //*  11/2014 START
                        //*  FATCA IND PER ROX
                        if (condition(ldaTwrl9710.getForm_Tirf_Future_Use().getSubstring(20,1).equals("Y")))                                                              //Natural: IF SUBSTR ( TIRF-FUTURE-USE,20,1 ) = 'Y'
                        {
                            pnd_1042s_Control_Totals_Pnd_1042s_Trans_Count.getValue(2).nadd(1);                                                                           //Natural: ADD 1 TO #1042S-TRANS-COUNT ( 2 )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_1042s_Control_Totals_Pnd_1042s_Trans_Count.getValue(1).nadd(1);                                                                           //Natural: ADD 1 TO #1042S-TRANS-COUNT ( 1 )
                        }                                                                                                                                                 //Natural: END-IF
                        //*  11/2014 END
                        if (condition(ldaTwrl9710.getForm_Count_Casttirf_1042_S_Line_Grp().notEquals(getZero())))                                                         //Natural: IF C*TIRF-1042-S-LINE-GRP NE 0
                        {
                            FOR01:                                                                                                                                        //Natural: FOR #C-1042-S = 1 TO C*TIRF-1042-S-LINE-GRP
                            for (pnd_C_1042_S.setValue(1); condition(pnd_C_1042_S.lessOrEqual(ldaTwrl9710.getForm_Count_Casttirf_1042_S_Line_Grp())); 
                                pnd_C_1042_S.nadd(1))
                            {
                                //*  11/2014 START
                                if (condition(ldaTwrl9710.getForm_Tirf_Future_Use().getSubstring(20,1).equals("Y")))                                                      //Natural: IF SUBSTR ( TIRF-FUTURE-USE,20,1 ) = 'Y'
                                {
                                    pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));     //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-GROSS-INCOME ( 2 )
                                    pnd_1042s_Totals_Pnd_1042s_Total_Gross.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));                          //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-TOTAL-GROSS
                                    pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Nra_Tax_Wthld().getValue(pnd_C_1042_S));   //Natural: ADD TIRF-NRA-TAX-WTHLD ( #C-1042-S ) TO #1042S-NRA-TAX-WTHLD ( 2 )
                                    pnd_1042s_Totals_Pnd_1042s_Total_Tax.nadd(ldaTwrl9710.getForm_Tirf_Nra_Tax_Wthld().getValue(pnd_C_1042_S));                           //Natural: ADD TIRF-NRA-TAX-WTHLD ( #C-1042-S ) TO #1042S-TOTAL-TAX
                                    pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_1042_S_Refund_Amt().getValue(pnd_C_1042_S));  //Natural: ADD TIRF-1042-S-REFUND-AMT ( #C-1042-S ) TO #1042S-REFUND-AMT ( 2 )
                                    pnd_1042s_Totals_Pnd_1042s_Total_Refund.nadd(ldaTwrl9710.getForm_Tirf_1042_S_Refund_Amt().getValue(pnd_C_1042_S));                    //Natural: ADD TIRF-1042-S-REFUND-AMT ( #C-1042-S ) TO #1042S-TOTAL-REFUND
                                    //*  <<  START CHECK FOR TAX YEAR  >>  /* << MUKHERR STARTS
                                    if (condition(pnd_Form_Key_Pnd_Tax_Year.lessOrEqual("2013")))                                                                         //Natural: IF #FORM-KEY.#TAX-YEAR LE '2013'
                                    {
                                        //*  INTEREST
                                        short decideConditionsMet930 = 0;                                                                                                 //Natural: DECIDE ON FIRST VALUE TIRF-1042-S-INCOME-CODE ( #C-1042-S );//Natural: VALUE '01'
                                        if (condition((ldaTwrl9710.getForm_Tirf_1042_S_Income_Code().getValue(pnd_C_1042_S).equals("01"))))
                                        {
                                            decideConditionsMet930++;
                                            pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-INTEREST-AMT ( 2 )
                                            //*  PENSION AND ANNUITIES
                                            pnd_1042s_Totals_Pnd_1042s_Total_Interest.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));               //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-TOTAL-INTEREST
                                        }                                                                                                                                 //Natural: VALUE '14'
                                        else if (condition((ldaTwrl9710.getForm_Tirf_1042_S_Income_Code().getValue(pnd_C_1042_S).equals("14"))))
                                        {
                                            decideConditionsMet930++;
                                            pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-PENSION-AMT ( 2 )
                                            pnd_1042s_Totals_Pnd_1042s_Total_Pension.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));                //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-TOTAL-PENSION
                                        }                                                                                                                                 //Natural: NONE
                                        else if (condition())
                                        {
                                            ignore();
                                        }                                                                                                                                 //Natural: END-DECIDE
                                    }                                                                                                                                     //Natural: ELSE
                                    else if (condition())
                                    {
                                        if (condition(pnd_Form_Key_Pnd_Tax_Year.equals("2014")))                                                                          //Natural: IF #FORM-KEY.#TAX-YEAR EQ '2014'
                                        {
                                            //*  INTEREST
                                            short decideConditionsMet944 = 0;                                                                                             //Natural: DECIDE ON FIRST VALUE TIRF-CHPTR4-INCOME-CDE ( #C-1042-S );//Natural: VALUE '01'
                                            if (condition((ldaTwrl9710.getForm_Tirf_Chptr4_Income_Cde().getValue(pnd_C_1042_S).equals("01"))))
                                            {
                                                decideConditionsMet944++;
                                                pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-INTEREST-AMT ( 2 )
                                                //*  PENSION AND ANNUITIES
                                                pnd_1042s_Totals_Pnd_1042s_Total_Interest.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));           //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-TOTAL-INTEREST
                                            }                                                                                                                             //Natural: VALUE '14'
                                            else if (condition((ldaTwrl9710.getForm_Tirf_Chptr4_Income_Cde().getValue(pnd_C_1042_S).equals("14"))))
                                            {
                                                decideConditionsMet944++;
                                                pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-PENSION-AMT ( 2 )
                                                pnd_1042s_Totals_Pnd_1042s_Total_Pension.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));            //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-TOTAL-PENSION
                                            }                                                                                                                             //Natural: NONE
                                            else if (condition())
                                            {
                                                ignore();
                                            }                                                                                                                             //Natural: END-DECIDE
                                            //*  MUKHERR END >>
                                        }                                                                                                                                 //Natural: ELSE
                                        else if (condition())
                                        {
                                            if (condition(pnd_Form_Key_Pnd_Tax_Year.greaterOrEqual("2015")))                                                              //Natural: IF #FORM-KEY.#TAX-YEAR GE '2015'
                                            {
                                                //*  << RAHUL START
                                                //*            DECIDE ON FIRST VALUE TIRF-1042-S-INCOME-CODE(#C-1042-S)
                                                //*  INTEREST
                                                //*     RAHUL END >>                                                                                                      //Natural: DECIDE ON FIRST VALUE TIRF-CHPTR4-INCOME-CDE ( #C-1042-S )
                                                short decideConditionsMet961 = 0;                                                                                         //Natural: VALUE '01'
                                                if (condition((ldaTwrl9710.getForm_Tirf_Chptr4_Income_Cde().getValue(pnd_C_1042_S).equals("01"))))
                                                {
                                                    decideConditionsMet961++;
                                                    pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-INTEREST-AMT ( 2 )
                                                    //*  PENSION AND ANNUITIES   /* RAHUL
                                                    pnd_1042s_Totals_Pnd_1042s_Total_Interest.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));       //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-TOTAL-INTEREST
                                                    //*              VALUE '14'
                                                }                                                                                                                         //Natural: VALUE '15'
                                                else if (condition((ldaTwrl9710.getForm_Tirf_Chptr4_Income_Cde().getValue(pnd_C_1042_S).equals("15"))))
                                                {
                                                    decideConditionsMet961++;
                                                    pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-PENSION-AMT ( 2 )
                                                    pnd_1042s_Totals_Pnd_1042s_Total_Pension.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));        //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-TOTAL-PENSION
                                                }                                                                                                                         //Natural: NONE
                                                else if (condition())
                                                {
                                                    ignore();
                                                }                                                                                                                         //Natural: END-DECIDE
                                                //*  MUKHERR
                                            }                                                                                                                             //Natural: END-IF
                                            //*  MUKHERR
                                        }                                                                                                                                 //Natural: END-IF
                                        //*  MUKHERR
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));     //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-GROSS-INCOME ( 1 )
                                    pnd_1042s_Totals_Pnd_1042s_Total_Gross.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));                          //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-TOTAL-GROSS
                                    pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Nra_Tax_Wthld().getValue(pnd_C_1042_S));   //Natural: ADD TIRF-NRA-TAX-WTHLD ( #C-1042-S ) TO #1042S-NRA-TAX-WTHLD ( 1 )
                                    pnd_1042s_Totals_Pnd_1042s_Total_Tax.nadd(ldaTwrl9710.getForm_Tirf_Nra_Tax_Wthld().getValue(pnd_C_1042_S));                           //Natural: ADD TIRF-NRA-TAX-WTHLD ( #C-1042-S ) TO #1042S-TOTAL-TAX
                                    pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_1042_S_Refund_Amt().getValue(pnd_C_1042_S));  //Natural: ADD TIRF-1042-S-REFUND-AMT ( #C-1042-S ) TO #1042S-REFUND-AMT ( 1 )
                                    pnd_1042s_Totals_Pnd_1042s_Total_Refund.nadd(ldaTwrl9710.getForm_Tirf_1042_S_Refund_Amt().getValue(pnd_C_1042_S));                    //Natural: ADD TIRF-1042-S-REFUND-AMT ( #C-1042-S ) TO #1042S-TOTAL-REFUND
                                    //*  INTEREST
                                    short decideConditionsMet988 = 0;                                                                                                     //Natural: DECIDE ON FIRST VALUE TIRF-1042-S-INCOME-CODE ( #C-1042-S );//Natural: VALUE '01'
                                    if (condition((ldaTwrl9710.getForm_Tirf_1042_S_Income_Code().getValue(pnd_C_1042_S).equals("01"))))
                                    {
                                        decideConditionsMet988++;
                                        pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-INTEREST-AMT ( 1 )
                                        //*  PENSION AND ANNUITIES   /* MUKHERR
                                        pnd_1042s_Totals_Pnd_1042s_Total_Interest.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));                   //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-TOTAL-INTEREST
                                    }                                                                                                                                     //Natural: VALUE '14'
                                    else if (condition((ldaTwrl9710.getForm_Tirf_1042_S_Income_Code().getValue(pnd_C_1042_S).equals("14"))))
                                    {
                                        decideConditionsMet988++;
                                        //*  MUKHERR
                                        if (condition(pnd_Form_Key_Pnd_Tax_Year.lessOrEqual("2014")))                                                                     //Natural: IF #FORM-KEY.#TAX-YEAR LE '2014'
                                        {
                                            //*  MUKHERR
                                            //*  MUKHERR
                                            pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-PENSION-AMT ( 1 )
                                            //*  MUKHERR
                                            //*  MUKHERR
                                            pnd_1042s_Totals_Pnd_1042s_Total_Pension.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));                //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-TOTAL-PENSION
                                            //*  MUKHERR
                                            //*  PENSION AND ANNUITIES   /* RAHUL
                                        }                                                                                                                                 //Natural: END-IF
                                        //*            VALUE '14'
                                    }                                                                                                                                     //Natural: VALUE '15'
                                    else if (condition((ldaTwrl9710.getForm_Tirf_1042_S_Income_Code().getValue(pnd_C_1042_S).equals("15"))))
                                    {
                                        decideConditionsMet988++;
                                        //*  MUKHERR
                                        if (condition(pnd_Form_Key_Pnd_Tax_Year.greaterOrEqual("2015")))                                                                  //Natural: IF #FORM-KEY.#TAX-YEAR GE '2015'
                                        {
                                            pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-PENSION-AMT ( 1 )
                                            pnd_1042s_Totals_Pnd_1042s_Total_Pension.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));                //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-TOTAL-PENSION
                                            //*  MUKHERR
                                        }                                                                                                                                 //Natural: END-IF
                                    }                                                                                                                                     //Natural: NONE
                                    else if (condition())
                                    {
                                        ignore();
                                    }                                                                                                                                     //Natural: END-DECIDE
                                    //*  11/2014 END
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*  5498
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 4
                    else if (condition((ldaTwrl9710.getForm_Tirf_Form_Type().equals(4))))
                    {
                        decideConditionsMet861++;
                        pnd_5498_Control_Totals_Pnd_5498_Form_Cnt.nadd(1);                                                                                                //Natural: ADD 1 TO #5498-FORM-CNT
                        pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Contrib.nadd(ldaTwrl9710.getForm_Tirf_Trad_Ira_Contrib());                                              //Natural: ADD TIRF-TRAD-IRA-CONTRIB TO #5498-TRAD-IRA-CONTRIB
                        pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Contrib.nadd(ldaTwrl9710.getForm_Tirf_Roth_Ira_Contrib());                                              //Natural: ADD TIRF-ROTH-IRA-CONTRIB TO #5498-ROTH-IRA-CONTRIB
                        pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rollover.nadd(ldaTwrl9710.getForm_Tirf_Trad_Ira_Rollover());                                            //Natural: ADD TIRF-TRAD-IRA-ROLLOVER TO #5498-TRAD-IRA-ROLLOVER
                        pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rechar.nadd(ldaTwrl9710.getForm_Tirf_Ira_Rechar_Amt());                                                 //Natural: ADD TIRF-IRA-RECHAR-AMT TO #5498-TRAD-IRA-RECHAR
                        pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Conversion.nadd(ldaTwrl9710.getForm_Tirf_Roth_Ira_Conversion());                                        //Natural: ADD TIRF-ROTH-IRA-CONVERSION TO #5498-ROTH-IRA-CONVERSION
                        pnd_5498_Control_Totals_Pnd_5498_Account_Fmv.nadd(ldaTwrl9710.getForm_Tirf_Account_Fmv());                                                        //Natural: ADD TIRF-ACCOUNT-FMV TO #5498-ACCOUNT-FMV
                        //*  BK
                        pnd_5498_Control_Totals_Pnd_5498_Sep_Amt.nadd(ldaTwrl9710.getForm_Tirf_Sep_Amt());                                                                //Natural: ADD TIRF-SEP-AMT TO #5498-SEP-AMT
                        //*  DASDH
                        pnd_5498_Control_Totals_Pnd_5498_Postpn_Amt.nadd(ldaTwrl9710.getForm_Tirf_Postpn_Amt());                                                          //Natural: ADD TIRF-POSTPN-AMT TO #5498-POSTPN-AMT
                        //* SAIK
                        //*  480.7C
                        pnd_5498_Control_Totals_Pnd_5498_Repayments_Amt.nadd(ldaTwrl9710.getForm_Tirf_Repayments_Amt());                                                  //Natural: ADD TIRF-REPAYMENTS-AMT TO #5498-REPAYMENTS-AMT
                    }                                                                                                                                                     //Natural: VALUE 5
                    else if (condition((ldaTwrl9710.getForm_Tirf_Form_Type().equals(5))))
                    {
                        decideConditionsMet861++;
                        pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                              //Natural: ADD TIRF-GROSS-AMT TO #4807C-GROSS-AMT ( 1 )
                        pnd_4807c_Control_Totals_Pnd_4807c_Int_Amt.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                  //Natural: ADD TIRF-INT-AMT TO #4807C-INT-AMT ( 1 )
                        pnd_4807c_Control_Totals_Pnd_4807c_Taxable_Amt.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Taxable_Amt());                                          //Natural: ADD TIRF-TAXABLE-AMT TO #4807C-TAXABLE-AMT ( 1 )
                        pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Ivc_Amt());                                                  //Natural: ADD TIRF-IVC-AMT TO #4807C-IVC-AMT ( 1 )
                        pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt_Roll.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll());                                 //Natural: ADD TIRF-PR-GROSS-AMT-ROLL TO #4807C-GROSS-AMT-ROLL ( 1 )
                        //*  NR4 (CANADIAN)
                        pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt_Roll.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll());                                     //Natural: ADD TIRF-PR-IVC-AMT-ROLL TO #4807C-IVC-AMT-ROLL ( 1 )
                    }                                                                                                                                                     //Natural: VALUE 7
                    else if (condition((ldaTwrl9710.getForm_Tirf_Form_Type().equals(7))))
                    {
                        decideConditionsMet861++;
                        //*  PERIODIC
                        //*  LUMP SUM & RTB
                        //*  INTEREST
                        short decideConditionsMet1051 = 0;                                                                                                                //Natural: DECIDE ON FIRST VALUE TIRF-NR4-INCOME-CODE;//Natural: VALUE '02'
                        if (condition((ldaTwrl9710.getForm_Tirf_Nr4_Income_Code().equals("02"))))
                        {
                            decideConditionsMet1051++;
                            pnd_Nr4_Ndx.setValue(1);                                                                                                                      //Natural: ASSIGN #NR4-NDX := 1
                        }                                                                                                                                                 //Natural: VALUE '03'
                        else if (condition((ldaTwrl9710.getForm_Tirf_Nr4_Income_Code().equals("03"))))
                        {
                            decideConditionsMet1051++;
                            pnd_Nr4_Ndx.setValue(2);                                                                                                                      //Natural: ASSIGN #NR4-NDX := 2
                        }                                                                                                                                                 //Natural: VALUE '62'
                        else if (condition((ldaTwrl9710.getForm_Tirf_Nr4_Income_Code().equals("62"))))
                        {
                            decideConditionsMet1051++;
                            pnd_Nr4_Ndx.setValue(3);                                                                                                                      //Natural: ASSIGN #NR4-NDX := 3
                        }                                                                                                                                                 //Natural: NONE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-DECIDE
                        pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt.getValue(pnd_Nr4_Ndx).nadd(1);                                                                            //Natural: ADD 1 TO #NR4-FORM-CNT ( #NR4-NDX )
                        pnd_Nr4_Control_Totals_Pnd_Nr4_Income_Code.getValue(pnd_Nr4_Ndx).setValue(ldaTwrl9710.getForm_Tirf_Nr4_Income_Code());                            //Natural: MOVE TIRF-NR4-INCOME-CODE TO #NR4-INCOME-CODE ( #NR4-NDX )
                        pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt.getValue(pnd_Nr4_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                        //Natural: ADD TIRF-GROSS-AMT TO #NR4-GROSS-AMT ( #NR4-NDX )
                        pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt.getValue(pnd_Nr4_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                       //Natural: ADD TIRF-INT-AMT TO #NR4-INTEREST-AMT ( #NR4-NDX )
                        pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld.getValue(pnd_Nr4_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                //Natural: ADD TIRF-FED-TAX-WTHLD TO #NR4-FED-TAX-WTHLD ( #NR4-NDX )
                        //*      INCREMENT TOTALS
                        pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt.getValue(4).nadd(1);                                                                                      //Natural: ADD 1 TO #NR4-FORM-CNT ( 4 )
                        pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt.getValue(4).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                  //Natural: ADD TIRF-GROSS-AMT TO #NR4-GROSS-AMT ( 4 )
                        pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt.getValue(4).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                 //Natural: ADD TIRF-INT-AMT TO #NR4-INTEREST-AMT ( 4 )
                        pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld.getValue(4).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                          //Natural: ADD TIRF-FED-TAX-WTHLD TO #NR4-FED-TAX-WTHLD ( 4 )
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    rEAD_FORMTirf_Tax_YearOld.setValue(ldaTwrl9710.getForm_Tirf_Tax_Year());                                                                              //Natural: END-READ
                    rEAD_FORMTirf_Company_CdeOld.setValue(ldaTwrl9710.getForm_Tirf_Company_Cde());
                    rEAD_FORMTirf_Form_TypeOld.setValue(ldaTwrl9710.getForm_Tirf_Form_Type());
                }
                if (condition(ldaTwrl9710.getVw_form().getAstCOUNTER().greater(0)))
                {
                    atBreakEventRead_Form(endOfDataReadForm);
                }
                if (Global.isEscape()) return;
                //*  PRINT SUNY DB FORM CONTROL
                getReports().write(11, new TabSetting(22),"Number of",new TabSetting(32),"Number of",new TabSetting(43),new TabSetting(65),"Federal Tax",new              //Natural: WRITE ( 11 ) 22T 'Number of' 32T 'Number of' 43T 65T 'Federal Tax' 84T 'State Tax' 103T 'Local Tax' / 23T 'Letters' 32T 'Details' 48T 'Gross Amount' 66T 'Withheld' 84T 'Withheld' 103T 'Withheld' / 22T '-' ( 9 ) 32T '-' ( 9 ) 43T '-' ( 17 ) 62T '-' ( 17 ) 80T '-' ( 17 ) 99T '-' ( 17 ) / 'SUNY/CUNY Letters' 22T #FORM-CNT-F10 ( 1 ) ( EM = Z,ZZZ,ZZ9 SG = OFF ) 32T #CONT-CNT-F10 ( 1 ) ( EM = Z,ZZZ,ZZ9 SG = OFF ) 43T #GROSS-AMT-F10 ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 SG = OFF ) 62T #FED-WTHLD-F10 ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 SG = OFF ) 80T #ST-WTHLD-F10 ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 SG = OFF ) 99T #LOC-WTHLD-F10 ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 SG = OFF ) / 'Non-SUNY/CUNY Plans' 22T 22T #FORM-CNT-F10 ( 2 ) ( EM = Z,ZZZ,ZZ9 SG = OFF ) 32T #CONT-CNT-F10 ( 2 ) ( EM = Z,ZZZ,ZZ9 SG = OFF ) 43T #GROSS-AMT-F10 ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 SG = OFF ) 62T #FED-WTHLD-F10 ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 SG = OFF ) 80T #ST-WTHLD-F10 ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 SG = OFF ) 99T #LOC-WTHLD-F10 ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 SG = OFF ) / 'SUNY/CUNY Plans' 22T 22T #FORM-CNT-F10 ( 3 ) ( EM = Z,ZZZ,ZZ9 SG = OFF ) 32T #CONT-CNT-F10 ( 3 ) ( EM = Z,ZZZ,ZZ9 SG = OFF ) 43T #GROSS-AMT-F10 ( 3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 SG = OFF ) 62T #FED-WTHLD-F10 ( 3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 SG = OFF ) 80T #ST-WTHLD-F10 ( 3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 SG = OFF ) 99T #LOC-WTHLD-F10 ( 3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 SG = OFF ) /
                    TabSetting(84),"State Tax",new TabSetting(103),"Local Tax",NEWLINE,new TabSetting(23),"Letters",new TabSetting(32),"Details",new TabSetting(48),"Gross Amount",new 
                    TabSetting(66),"Withheld",new TabSetting(84),"Withheld",new TabSetting(103),"Withheld",NEWLINE,new TabSetting(22),"-",new RepeatItem(9),new 
                    TabSetting(32),"-",new RepeatItem(9),new TabSetting(43),"-",new RepeatItem(17),new TabSetting(62),"-",new RepeatItem(17),new TabSetting(80),"-",new 
                    RepeatItem(17),new TabSetting(99),"-",new RepeatItem(17),NEWLINE,"SUNY/CUNY Letters",new TabSetting(22),pnd_Accum_F10_Pnd_Form_Cnt_F10.getValue(1), 
                    new ReportEditMask ("Z,ZZZ,ZZ9"), new SignPosition (false),new TabSetting(32),pnd_Accum_F10_Pnd_Cont_Cnt_F10.getValue(1), new ReportEditMask 
                    ("Z,ZZZ,ZZ9"), new SignPosition (false),new TabSetting(43),pnd_Accum_F10_Pnd_Gross_Amt_F10.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"), 
                    new SignPosition (false),new TabSetting(62),pnd_Accum_F10_Pnd_Fed_Wthld_F10.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"), new 
                    SignPosition (false),new TabSetting(80),pnd_Accum_F10_Pnd_St_Wthld_F10.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"), new SignPosition 
                    (false),new TabSetting(99),pnd_Accum_F10_Pnd_Loc_Wthld_F10.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),NEWLINE,"Non-SUNY/CUNY Plans",new 
                    TabSetting(22),new TabSetting(22),pnd_Accum_F10_Pnd_Form_Cnt_F10.getValue(2), new ReportEditMask ("Z,ZZZ,ZZ9"), new SignPosition (false),new 
                    TabSetting(32),pnd_Accum_F10_Pnd_Cont_Cnt_F10.getValue(2), new ReportEditMask ("Z,ZZZ,ZZ9"), new SignPosition (false),new TabSetting(43),pnd_Accum_F10_Pnd_Gross_Amt_F10.getValue(2), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new TabSetting(62),pnd_Accum_F10_Pnd_Fed_Wthld_F10.getValue(2), new 
                    ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new TabSetting(80),pnd_Accum_F10_Pnd_St_Wthld_F10.getValue(2), new ReportEditMask 
                    ("ZZ,ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new TabSetting(99),pnd_Accum_F10_Pnd_Loc_Wthld_F10.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"), 
                    new SignPosition (false),NEWLINE,"SUNY/CUNY Plans",new TabSetting(22),new TabSetting(22),pnd_Accum_F10_Pnd_Form_Cnt_F10.getValue(3), 
                    new ReportEditMask ("Z,ZZZ,ZZ9"), new SignPosition (false),new TabSetting(32),pnd_Accum_F10_Pnd_Cont_Cnt_F10.getValue(3), new ReportEditMask 
                    ("Z,ZZZ,ZZ9"), new SignPosition (false),new TabSetting(43),pnd_Accum_F10_Pnd_Gross_Amt_F10.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"), 
                    new SignPosition (false),new TabSetting(62),pnd_Accum_F10_Pnd_Fed_Wthld_F10.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"), new 
                    SignPosition (false),new TabSetting(80),pnd_Accum_F10_Pnd_St_Wthld_F10.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"), new SignPosition 
                    (false),new TabSetting(99),pnd_Accum_F10_Pnd_Loc_Wthld_F10.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),
                    NEWLINE);
                if (Global.isEscape()) return;
                //* ***************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-1099
                //* *************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INCREMENT-1099-TOTAL
                //* ***************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-1099-GRAND-TOTAL
                //* ***************************************
                //* ******************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-1099-STATE-TABLE
                //* ******************************************
                //* ****************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-1099-STATE-REPORT
                //* ****************************************
                //* ***************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-5498
                //* ***************************
                //* ****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-1042S
                //*  'Form/Count'   #FORM-CNT (EM=Z,ZZZ,ZZ9)
                //* ****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-480-7
                //* **************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INCREMENT-4807-TOTAL
                //* ***************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-4807-GRAND-TOTAL
                //* ***************************************
                //* **************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-NR4
                //* **************************
                //* *******************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HOLD-RPT
                //*  MAILABLE-LOOP.
                //*  FOR #SYS-ERR-NDX = 1 TO C*TIRF-SYS-ERR
                //*   #ERR-NDX               := TIRF-SYS-ERR(#SYS-ERR-NDX)
                //*   IF #MOORE-HOLD(#ERR-NDX)
                //*     TWRL5951.#MOORE-MAIL := 'N'
                //*     ESCAPE BOTTOM (MAILABLE-LOOP.)
                //*   END-IF
                //*  END-FOR
                //* *********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-COMPANY-DESC
                //* **************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-REPORTED-TO-IRS
                //* ***************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-SYSTEM-HOLD-CODE
                //* ***************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUNY-FORM-DB-CONTROL
                //*  FOR #I-F10 1 20
                //*          ESCAPE TOP
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Print_1099() throws Exception                                                                                                                        //Natural: PRINT-1099
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        getReports().display(2, "///Type of/Form",                                                                                                                        //Natural: DISPLAY ( 2 ) '///Type of/Form' #FORM-TEXT '///Form/Count' #FORM-CNT ( EM = Z,ZZZ,ZZ9 ) '///Gross/Amount' #1099R-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '///Tax Free/IVC' #1099R-IVC-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) VERT AS 'Taxable/Amount/-----------/State Tax/Withholding' #1099R-TAXABLE-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #1099R-STATE-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) VERT AS 'Interest/Amount/-----------/Local Tax/Withholding' #1099I-INT-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #1099R-LOC-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) VERT AS 'Federal/Withholding/-----------/IRR Amount' #1099-FED-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #1099R-IRR-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
        		pnd_Form_Text,"///Form/Count",
        		pnd_Form_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"///Gross/Amount",
        		pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"///Tax Free/IVC",
        		pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, 
            "Taxable/Amount/-----------/State Tax/Withholding",
        		pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "Interest/Amount/-----------/Local Tax/Withholding",
            
        		pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "Federal/Withholding/-----------/IRR Amount",
            
        		pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
    }
    private void sub_Increment_1099_Total() throws Exception                                                                                                              //Natural: INCREMENT-1099-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        pnd_1099_Grand_Totals_Pnd_Total_1099_Form_Cnt.nadd(pnd_Form_Cnt);                                                                                                 //Natural: ADD #FORM-CNT TO #TOTAL-1099-FORM-CNT
        pnd_1099_Grand_Totals_Pnd_Total_1099_Hold_Cnt.nadd(pnd_Hold_Cnt);                                                                                                 //Natural: ADD #HOLD-CNT TO #TOTAL-1099-HOLD-CNT
        pnd_1099_Grand_Totals_Pnd_Total_1099_Empty_Form_Cnt.nadd(pnd_Empty_Form_Cnt);                                                                                     //Natural: ADD #EMPTY-FORM-CNT TO #TOTAL-1099-EMPTY-FORM-CNT
        pnd_1099_Grand_Totals_Pnd_Total_1099_Gross_Amt.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt);                                                          //Natural: ADD #1099R-GROSS-AMT TO #TOTAL-1099-GROSS-AMT
        pnd_1099_Grand_Totals_Pnd_Total_1099_Taxable_Amt.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt);                                                      //Natural: ADD #1099R-TAXABLE-AMT TO #TOTAL-1099-TAXABLE-AMT
        pnd_1099_Grand_Totals_Pnd_Total_1099_Fed_Tax_Wthld.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld);                                                   //Natural: ADD #1099-FED-TAX-WTHLD TO #TOTAL-1099-FED-TAX-WTHLD
        pnd_1099_Grand_Totals_Pnd_Total_1099_Ivc_Amt.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt);                                                              //Natural: ADD #1099R-IVC-AMT TO #TOTAL-1099-IVC-AMT
        pnd_1099_Grand_Totals_Pnd_Total_1099_State_Tax_Wthld.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld);                                              //Natural: ADD #1099R-STATE-TAX-WTHLD TO #TOTAL-1099-STATE-TAX-WTHLD
        pnd_1099_Grand_Totals_Pnd_Total_1099_Loc_Tax_Wthld.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld);                                                  //Natural: ADD #1099R-LOC-TAX-WTHLD TO #TOTAL-1099-LOC-TAX-WTHLD
        pnd_1099_Grand_Totals_Pnd_Total_1099_Int_Amt.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt);                                                              //Natural: ADD #1099I-INT-AMT TO #TOTAL-1099-INT-AMT
        pnd_1099_Grand_Totals_Pnd_Total_1099_Irr_Amt.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt);                                                              //Natural: ADD #1099R-IRR-AMT TO #TOTAL-1099-IRR-AMT
    }
    private void sub_Print_1099_Grand_Total() throws Exception                                                                                                            //Natural: PRINT-1099-GRAND-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Form_Text.setValue("TOTAL");                                                                                                                                  //Natural: ASSIGN #FORM-TEXT := 'TOTAL'
        pnd_Form_Cnt.setValue(pnd_1099_Grand_Totals_Pnd_Total_1099_Form_Cnt);                                                                                             //Natural: ASSIGN #FORM-CNT := #TOTAL-1099-FORM-CNT
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt.setValue(pnd_1099_Grand_Totals_Pnd_Total_1099_Gross_Amt);                                                      //Natural: ASSIGN #1099R-GROSS-AMT := #TOTAL-1099-GROSS-AMT
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt.setValue(pnd_1099_Grand_Totals_Pnd_Total_1099_Taxable_Amt);                                                  //Natural: ASSIGN #1099R-TAXABLE-AMT := #TOTAL-1099-TAXABLE-AMT
        pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld.setValue(pnd_1099_Grand_Totals_Pnd_Total_1099_Fed_Tax_Wthld);                                               //Natural: ASSIGN #1099-FED-TAX-WTHLD := #TOTAL-1099-FED-TAX-WTHLD
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt.setValue(pnd_1099_Grand_Totals_Pnd_Total_1099_Ivc_Amt);                                                          //Natural: ASSIGN #1099R-IVC-AMT := #TOTAL-1099-IVC-AMT
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld.setValue(pnd_1099_Grand_Totals_Pnd_Total_1099_State_Tax_Wthld);                                          //Natural: ASSIGN #1099R-STATE-TAX-WTHLD := #TOTAL-1099-STATE-TAX-WTHLD
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld.setValue(pnd_1099_Grand_Totals_Pnd_Total_1099_Loc_Tax_Wthld);                                              //Natural: ASSIGN #1099R-LOC-TAX-WTHLD := #TOTAL-1099-LOC-TAX-WTHLD
        pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt.setValue(pnd_1099_Grand_Totals_Pnd_Total_1099_Int_Amt);                                                          //Natural: ASSIGN #1099I-INT-AMT := #TOTAL-1099-INT-AMT
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt.setValue(pnd_1099_Grand_Totals_Pnd_Total_1099_Irr_Amt);                                                          //Natural: ASSIGN #1099R-IRR-AMT := #TOTAL-1099-IRR-AMT
                                                                                                                                                                          //Natural: PERFORM PRINT-1099
        sub_Print_1099();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        getReports().write(2, NEWLINE,NEWLINE,NEWLINE,"HELD CNT...:",pnd_1099_Grand_Totals_Pnd_Total_1099_Hold_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"EMPTY FORMS:",pnd_1099_Grand_Totals_Pnd_Total_1099_Empty_Form_Cnt,  //Natural: WRITE ( 2 ) // /'HELD CNT...:' #TOTAL-1099-HOLD-CNT ( EM = Z,ZZZ,ZZ9 ) /'EMPTY FORMS:' #TOTAL-1099-EMPTY-FORM-CNT ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Populate_1099_State_Table() throws Exception                                                                                                         //Natural: POPULATE-1099-STATE-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #1099-STATE-NDX = 1 TO #C-1099-R
        for (pnd_1099_State_Ndx.setValue(1); condition(pnd_1099_State_Ndx.lessOrEqual(pnd_C_1099_R)); pnd_1099_State_Ndx.nadd(1))
        {
            if (condition(ldaTwrl9710.getForm_Tirf_State_Rpt_Ind().getValue(pnd_1099_State_Ndx).equals("Y")))                                                             //Natural: IF FORM.TIRF-STATE-RPT-IND ( #1099-STATE-NDX ) = 'Y'
            {
                pnd_Reportable_Ndx.setValue(1);                                                                                                                           //Natural: ASSIGN #REPORTABLE-NDX := 1
                //*  NOT REPORTABLE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Reportable_Ndx.setValue(2);                                                                                                                           //Natural: ASSIGN #REPORTABLE-NDX := 2
            }                                                                                                                                                             //Natural: END-IF
            //*  UNTIL REPORTABLE AND HARDCOPY BUCKETS ARE POPULATED
            REPEAT:                                                                                                                                                       //Natural: REPEAT
            while (condition(whileTrue))
            {
                if (condition(DbsUtil.maskMatches(ldaTwrl9710.getForm_Tirf_State_Code().getValue(pnd_1099_State_Ndx),"99")))                                              //Natural: IF FORM.TIRF-STATE-CODE ( #1099-STATE-NDX ) = MASK ( 99 )
                {
                    pnd_State_Tbl_Ndx.compute(new ComputeParameters(false, pnd_State_Tbl_Ndx), ldaTwrl9710.getForm_Tirf_State_Code().getValue(pnd_1099_State_Ndx).val()); //Natural: ASSIGN #STATE-TBL-NDX := VAL ( FORM.TIRF-STATE-CODE ( #1099-STATE-NDX ) )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_State_Tbl_Ndx.setValue(96);                                                                                                                       //Natural: ASSIGN #STATE-TBL-NDX := 96
                }                                                                                                                                                         //Natural: END-IF
                pnd_1099_State_Tbl_Pnd_State_Form_Cnt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,pnd_Reportable_Ndx).nadd(1);                                                //Natural: ADD 1 TO #STATE-FORM-CNT ( #STATE-TBL-NDX,#REPORTABLE-NDX )
                pnd_1099_State_Tbl_Pnd_State_Distr_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,pnd_Reportable_Ndx).nadd(ldaTwrl9710.getForm_Tirf_State_Distr().getValue(pnd_1099_State_Ndx)); //Natural: ADD FORM.TIRF-STATE-DISTR ( #1099-STATE-NDX ) TO #STATE-DISTR-AMT ( #STATE-TBL-NDX,#REPORTABLE-NDX )
                pnd_1099_State_Tbl_Pnd_State_Wthhld_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,pnd_Reportable_Ndx).nadd(ldaTwrl9710.getForm_Tirf_State_Tax_Wthld().getValue(pnd_1099_State_Ndx)); //Natural: ADD FORM.TIRF-STATE-TAX-WTHLD ( #1099-STATE-NDX ) TO #STATE-WTHHLD-AMT ( #STATE-TBL-NDX,#REPORTABLE-NDX )
                pnd_1099_State_Tbl_Pnd_Local_Distr_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,pnd_Reportable_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Loc_Distr().getValue(pnd_1099_State_Ndx)); //Natural: ADD FORM.TIRF-LOC-DISTR ( #1099-STATE-NDX ) TO #LOCAL-DISTR-AMT ( #STATE-TBL-NDX,#REPORTABLE-NDX )
                pnd_1099_State_Tbl_Pnd_Local_Wthhld_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,pnd_Reportable_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(pnd_1099_State_Ndx)); //Natural: ADD FORM.TIRF-LOC-TAX-WTHLD ( #1099-STATE-NDX ) TO #LOCAL-WTHHLD-AMT ( #STATE-TBL-NDX,#REPORTABLE-NDX )
                pnd_1099_State_Tbl_Pnd_Res_Gross_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,pnd_Reportable_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Res_Gross_Amt().getValue(pnd_1099_State_Ndx)); //Natural: ADD FORM.TIRF-RES-GROSS-AMT ( #1099-STATE-NDX ) TO #RES-GROSS-AMT ( #STATE-TBL-NDX,#REPORTABLE-NDX )
                pnd_1099_State_Tbl_Pnd_Res_Taxable_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,pnd_Reportable_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Res_Taxable_Amt().getValue(pnd_1099_State_Ndx)); //Natural: ADD FORM.TIRF-RES-TAXABLE-AMT ( #1099-STATE-NDX ) TO #RES-TAXABLE-AMT ( #STATE-TBL-NDX,#REPORTABLE-NDX )
                pnd_1099_State_Tbl_Pnd_Res_Ivc_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,pnd_Reportable_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Res_Ivc_Amt().getValue(pnd_1099_State_Ndx)); //Natural: ADD FORM.TIRF-RES-IVC-AMT ( #1099-STATE-NDX ) TO #RES-IVC-AMT ( #STATE-TBL-NDX,#REPORTABLE-NDX )
                pnd_1099_State_Tbl_Pnd_Res_Fed_Tax_Wthld.getValue(pnd_State_Tbl_Ndx.getInt() + 1,pnd_Reportable_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Res_Fed_Tax_Wthld().getValue(pnd_1099_State_Ndx)); //Natural: ADD FORM.TIRF-RES-FED-TAX-WTHLD ( #1099-STATE-NDX ) TO #RES-FED-TAX-WTHLD ( #STATE-TBL-NDX,#REPORTABLE-NDX )
                pnd_1099_State_Tbl_Pnd_Res_Irr_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,pnd_Reportable_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Res_Irr_Amt().getValue(pnd_1099_State_Ndx)); //Natural: ADD FORM.TIRF-RES-IRR-AMT ( #1099-STATE-NDX ) TO #RES-IRR-AMT ( #STATE-TBL-NDX,#REPORTABLE-NDX )
                //*   GRAND TOTAL
                pnd_1099_State_Tbl_Pnd_State_Form_Cnt.getValue(100 + 1,pnd_Reportable_Ndx).nadd(1);                                                                       //Natural: ADD 1 TO #STATE-FORM-CNT ( 100,#REPORTABLE-NDX )
                pnd_1099_State_Tbl_Pnd_State_Distr_Amt.getValue(100 + 1,pnd_Reportable_Ndx).nadd(ldaTwrl9710.getForm_Tirf_State_Distr().getValue(pnd_1099_State_Ndx));    //Natural: ADD FORM.TIRF-STATE-DISTR ( #1099-STATE-NDX ) TO #STATE-DISTR-AMT ( 100,#REPORTABLE-NDX )
                pnd_1099_State_Tbl_Pnd_State_Wthhld_Amt.getValue(100 + 1,pnd_Reportable_Ndx).nadd(ldaTwrl9710.getForm_Tirf_State_Tax_Wthld().getValue(pnd_1099_State_Ndx)); //Natural: ADD FORM.TIRF-STATE-TAX-WTHLD ( #1099-STATE-NDX ) TO #STATE-WTHHLD-AMT ( 100,#REPORTABLE-NDX )
                pnd_1099_State_Tbl_Pnd_Local_Distr_Amt.getValue(100 + 1,pnd_Reportable_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Loc_Distr().getValue(pnd_1099_State_Ndx));      //Natural: ADD FORM.TIRF-LOC-DISTR ( #1099-STATE-NDX ) TO #LOCAL-DISTR-AMT ( 100,#REPORTABLE-NDX )
                pnd_1099_State_Tbl_Pnd_Local_Wthhld_Amt.getValue(100 + 1,pnd_Reportable_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(pnd_1099_State_Ndx)); //Natural: ADD FORM.TIRF-LOC-TAX-WTHLD ( #1099-STATE-NDX ) TO #LOCAL-WTHHLD-AMT ( 100,#REPORTABLE-NDX )
                pnd_1099_State_Tbl_Pnd_Res_Gross_Amt.getValue(100 + 1,pnd_Reportable_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Res_Gross_Amt().getValue(pnd_1099_State_Ndx));    //Natural: ADD FORM.TIRF-RES-GROSS-AMT ( #1099-STATE-NDX ) TO #RES-GROSS-AMT ( 100,#REPORTABLE-NDX )
                pnd_1099_State_Tbl_Pnd_Res_Taxable_Amt.getValue(100 + 1,pnd_Reportable_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Res_Taxable_Amt().getValue(pnd_1099_State_Ndx)); //Natural: ADD FORM.TIRF-RES-TAXABLE-AMT ( #1099-STATE-NDX ) TO #RES-TAXABLE-AMT ( 100,#REPORTABLE-NDX )
                pnd_1099_State_Tbl_Pnd_Res_Ivc_Amt.getValue(100 + 1,pnd_Reportable_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Res_Ivc_Amt().getValue(pnd_1099_State_Ndx));        //Natural: ADD FORM.TIRF-RES-IVC-AMT ( #1099-STATE-NDX ) TO #RES-IVC-AMT ( 100,#REPORTABLE-NDX )
                pnd_1099_State_Tbl_Pnd_Res_Fed_Tax_Wthld.getValue(100 + 1,pnd_Reportable_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Res_Fed_Tax_Wthld().getValue(pnd_1099_State_Ndx)); //Natural: ADD FORM.TIRF-RES-FED-TAX-WTHLD ( #1099-STATE-NDX ) TO #RES-FED-TAX-WTHLD ( 100,#REPORTABLE-NDX )
                pnd_1099_State_Tbl_Pnd_Res_Irr_Amt.getValue(100 + 1,pnd_Reportable_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Res_Irr_Amt().getValue(pnd_1099_State_Ndx));        //Natural: ADD FORM.TIRF-RES-IRR-AMT ( #1099-STATE-NDX ) TO #RES-IRR-AMT ( 100,#REPORTABLE-NDX )
                if (condition((ldaTwrl9710.getForm_Tirf_State_Hardcopy_Ind().getValue(pnd_1099_State_Ndx).notEquals(" ") || ldaTwrl9710.getForm_Tirf_State_Tax_Wthld().getValue(pnd_1099_State_Ndx).notEquals(new  //Natural: IF ( FORM.TIRF-STATE-HARDCOPY-IND ( #1099-STATE-NDX ) NE ' ' OR FORM.TIRF-STATE-TAX-WTHLD ( #1099-STATE-NDX ) NE 0.00 ) AND ( #REPORTABLE-NDX NE 3 )
                    DbsDecimal("0.00"))) && (pnd_Reportable_Ndx.notEquals(3))))
                {
                    pnd_Reportable_Ndx.setValue(3);                                                                                                                       //Natural: ASSIGN #REPORTABLE-NDX := 3
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (true) break REPEAT;                                                                                                                               //Natural: ESCAPE BOTTOM ( REPEAT. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-REPEAT
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Print_1099_State_Report() throws Exception                                                                                                           //Natural: PRINT-1099-STATE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #STATE-TBL-NDX = 0 TO 100
        for (pnd_State_Tbl_Ndx.setValue(0); condition(pnd_State_Tbl_Ndx.lessOrEqual(100)); pnd_State_Tbl_Ndx.nadd(1))
        {
            //*  PR
            short decideConditionsMet1223 = 0;                                                                                                                            //Natural: DECIDE ON EVERY VALUE #STATE-TBL-NDX;//Natural: VALUE 42
            if (condition(pnd_State_Tbl_Ndx.equals(42)))
            {
                decideConditionsMet1223++;
                //*  NOT USED
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: VALUE 58:95
            if (condition(pnd_State_Tbl_Ndx.greaterOrEqual(58) && pnd_State_Tbl_Ndx.lessOrEqual(95)))
            {
                decideConditionsMet1223++;
                //*  UNK TRTY
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: VALUE 98:99
            if (condition(pnd_State_Tbl_Ndx.greaterOrEqual(98) && pnd_State_Tbl_Ndx.lessOrEqual(99)))
            {
                decideConditionsMet1223++;
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: NONE
            if (condition(decideConditionsMet1223 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*   STATE TABLE LOOKUP
            //*  STATE CODE LOOKUP
            short decideConditionsMet1240 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #STATE-TBL-NDX;//Natural: VALUE 96
            if (condition((pnd_State_Tbl_Ndx.equals(96))))
            {
                decideConditionsMet1240++;
                pdaTwratbl2.getTwratbl2_Tircntl_State_Full_Name().setValue("Non State Residency");                                                                        //Natural: ASSIGN TWRATBL2.TIRCNTL-STATE-FULL-NAME := 'Non State Residency'
            }                                                                                                                                                             //Natural: VALUE 100
            else if (condition((pnd_State_Tbl_Ndx.equals(100))))
            {
                decideConditionsMet1240++;
                pdaTwratbl2.getTwratbl2_Tircntl_State_Full_Name().setValue("TOTAL");                                                                                      //Natural: ASSIGN TWRATBL2.TIRCNTL-STATE-FULL-NAME := 'TOTAL'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                     //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
                pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().setValue(false);                                                                                                  //Natural: ASSIGN TWRATBL2.#ABEND-IND := FALSE
                pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().setValue(false);                                                                                                //Natural: ASSIGN TWRATBL2.#DISPLAY-IND := FALSE
                pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(pnd_State_Tbl_Ndx_Pnd_State_Tbl_Ndx_A);                                                                  //Natural: ASSIGN TWRATBL2.#STATE-CDE := #STATE-TBL-NDX-A
                pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().setValue(pnd_Input_Rec_Pnd_Tax_Year);                                                                              //Natural: ASSIGN TWRATBL2.#TAX-YEAR := #INPUT-REC.#TAX-YEAR
                DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2());                                                                    //Natural: CALLNAT 'TWRNTBL2' TWRATBL2
                if (condition(Global.isEscape())) return;
                if (condition(! (pdaTwratbl2.getTwratbl2_Pnd_Return_Cde().getBoolean())))                                                                                 //Natural: IF NOT TWRATBL2.#RETURN-CDE
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(pnd_State_Tbl_Ndx.equals(100)))                                                                                                                 //Natural: IF #STATE-TBL-NDX = 100
            {
                getReports().skip(7, 1);                                                                                                                                  //Natural: SKIP ( 7 ) 1
                getReports().skip(9, 1);                                                                                                                                  //Natural: SKIP ( 9 ) 1
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTwratbl2.getTwratbl2_Tircntl_State_Ind().equals(" ")))                                                                                       //Natural: IF TIRCNTL-STATE-IND = ' '
            {
                pdaTwratbl2.getTwratbl2_Tircntl_State_Ind().setValue("9");                                                                                                //Natural: ASSIGN TIRCNTL-STATE-IND := '9'
            }                                                                                                                                                             //Natural: END-IF
            pnd_State_Ind_N.compute(new ComputeParameters(false, pnd_State_Ind_N), pdaTwratbl2.getTwratbl2_Tircntl_State_Ind().val());                                    //Natural: ASSIGN #STATE-IND-N := VAL ( TIRCNTL-STATE-IND )
            //*     REPORTABLE/NOT REPORTABLE STATE REPORT
            getReports().display(7, new ReportEmptyLineSuppression(true),"/State",                                                                                        //Natural: DISPLAY ( 7 ) ( ES = ON ) '/State' TWRATBL2.TIRCNTL-STATE-FULL-NAME 'Form/Count' #STATE-FORM-CNT ( #STATE-TBL-NDX,1 ) ( EM = ZZ,ZZ9 ) VERT AS 'State Distrib/Local Distrib' #STATE-DISTR-AMT ( #STATE-TBL-NDX,1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #LOCAL-DISTR-AMT ( #STATE-TBL-NDX,1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) VERT AS 'State Withheld/Local Withheld' #STATE-WTHHLD-AMT ( #STATE-TBL-NDX,1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #LOCAL-WTHHLD-AMT ( #STATE-TBL-NDX,1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) HORIZ 'Reporting/Indicator' #STATE-IND-DESC ( #STATE-IND-N ) 'Form/Count' #STATE-FORM-CNT ( #STATE-TBL-NDX,2 ) ( EM = ZZ,ZZ9 ) VERT AS 'State Distrib/Local Distrib' #STATE-DISTR-AMT ( #STATE-TBL-NDX,2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #LOCAL-DISTR-AMT ( #STATE-TBL-NDX,2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) VERT AS 'State Withheld/Local Withheld' #STATE-WTHHLD-AMT ( #STATE-TBL-NDX,2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #LOCAL-WTHHLD-AMT ( #STATE-TBL-NDX,2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            		pdaTwratbl2.getTwratbl2_Tircntl_State_Full_Name(),"Form/Count",
            		pnd_1099_State_Tbl_Pnd_State_Form_Cnt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,1), new ReportEditMask ("ZZ,ZZ9"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, 
                "State Distrib/Local Distrib",
            		pnd_1099_State_Tbl_Pnd_State_Distr_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,1), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099_State_Tbl_Pnd_Local_Distr_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,1), 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "State Withheld/Local Withheld",
            		pnd_1099_State_Tbl_Pnd_State_Wthhld_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,1), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099_State_Tbl_Pnd_Local_Wthhld_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,1), 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.HORIZONTAL,"Reporting/Indicator",
            		ldaTwrl5950.getTwrl5950_Pnd_State_Ind_Desc().getValue(pnd_State_Ind_N),"Form/Count",
            		pnd_1099_State_Tbl_Pnd_State_Form_Cnt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,2), new ReportEditMask ("ZZ,ZZ9"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, 
                "State Distrib/Local Distrib",
            		pnd_1099_State_Tbl_Pnd_State_Distr_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,2), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099_State_Tbl_Pnd_Local_Distr_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,2), 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "State Withheld/Local Withheld",
            		pnd_1099_State_Tbl_Pnd_State_Wthhld_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,2), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099_State_Tbl_Pnd_Local_Wthhld_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,2), 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(7, 1);                                                                                                                                      //Natural: SKIP ( 7 ) 1
            //*    HARDCOPY STATE REPORT
            getReports().display(8, new ReportEmptyLineSuppression(true),"/State",                                                                                        //Natural: DISPLAY ( 8 ) ( ES = ON ) '/State' TWRATBL2.TIRCNTL-STATE-FULL-NAME 'Form/Count' #STATE-FORM-CNT ( #STATE-TBL-NDX,3 ) ( EM = ZZ,ZZ9 ) VERT AS 'State Distrib/Local Distrib' #STATE-DISTR-AMT ( #STATE-TBL-NDX,3 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #LOCAL-DISTR-AMT ( #STATE-TBL-NDX,3 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) VERT AS 'State Withheld/Local Withheld' #STATE-WTHHLD-AMT ( #STATE-TBL-NDX,3 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #LOCAL-WTHHLD-AMT ( #STATE-TBL-NDX,3 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            		pdaTwratbl2.getTwratbl2_Tircntl_State_Full_Name(),"Form/Count",
            		pnd_1099_State_Tbl_Pnd_State_Form_Cnt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,3), new ReportEditMask ("ZZ,ZZ9"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, 
                "State Distrib/Local Distrib",
            		pnd_1099_State_Tbl_Pnd_State_Distr_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,3), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099_State_Tbl_Pnd_Local_Distr_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,3), 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "State Withheld/Local Withheld",
            		pnd_1099_State_Tbl_Pnd_State_Wthhld_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,3), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099_State_Tbl_Pnd_Local_Wthhld_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,3), 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*     SUMMARY STATE REPORT
            pnd_Summary_Amounts.reset();                                                                                                                                  //Natural: RESET #SUMMARY-AMOUNTS
            pnd_Summary_Amounts_Pnd_Summ_State_Distr_Amt.nadd(pnd_1099_State_Tbl_Pnd_State_Distr_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,1,":",2));                   //Natural: ADD #STATE-DISTR-AMT ( #STATE-TBL-NDX,1:2 ) TO #SUMM-STATE-DISTR-AMT
            pnd_Summary_Amounts_Pnd_Summ_Local_Distr_Amt.nadd(pnd_1099_State_Tbl_Pnd_Local_Distr_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,1,":",2));                   //Natural: ADD #LOCAL-DISTR-AMT ( #STATE-TBL-NDX,1:2 ) TO #SUMM-LOCAL-DISTR-AMT
            pnd_Summary_Amounts_Pnd_Summ_State_Wthhld_Amt.nadd(pnd_1099_State_Tbl_Pnd_State_Wthhld_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,1,":",2));                 //Natural: ADD #STATE-WTHHLD-AMT ( #STATE-TBL-NDX,1:2 ) TO #SUMM-STATE-WTHHLD-AMT
            pnd_Summary_Amounts_Pnd_Summ_Local_Wthhld_Amt.nadd(pnd_1099_State_Tbl_Pnd_Local_Wthhld_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,1,":",2));                 //Natural: ADD #LOCAL-WTHHLD-AMT ( #STATE-TBL-NDX,1:2 ) TO #SUMM-LOCAL-WTHHLD-AMT
            pnd_Summary_Amounts_Pnd_Summ_State_Form_Cnt.nadd(pnd_1099_State_Tbl_Pnd_State_Form_Cnt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,1,":",2));                     //Natural: ADD #STATE-FORM-CNT ( #STATE-TBL-NDX,1:2 ) TO #SUMM-STATE-FORM-CNT
            pnd_Summary_Amounts_Pnd_Summ_Res_Gross_Amt.nadd(pnd_1099_State_Tbl_Pnd_Res_Gross_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,1,":",2));                       //Natural: ADD #RES-GROSS-AMT ( #STATE-TBL-NDX,1:2 ) TO #SUMM-RES-GROSS-AMT
            pnd_Summary_Amounts_Pnd_Summ_Res_Taxable_Amt.nadd(pnd_1099_State_Tbl_Pnd_Res_Taxable_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,1,":",2));                   //Natural: ADD #RES-TAXABLE-AMT ( #STATE-TBL-NDX,1:2 ) TO #SUMM-RES-TAXABLE-AMT
            pnd_Summary_Amounts_Pnd_Summ_Res_Ivc_Amt.nadd(pnd_1099_State_Tbl_Pnd_Res_Ivc_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,1,":",2));                           //Natural: ADD #RES-IVC-AMT ( #STATE-TBL-NDX,1:2 ) TO #SUMM-RES-IVC-AMT
            pnd_Summary_Amounts_Pnd_Summ_Res_Fed_Tax_Wthld.nadd(pnd_1099_State_Tbl_Pnd_Res_Fed_Tax_Wthld.getValue(pnd_State_Tbl_Ndx.getInt() + 1,1,":",                   //Natural: ADD #RES-FED-TAX-WTHLD ( #STATE-TBL-NDX,1:2 ) TO #SUMM-RES-FED-TAX-WTHLD
                2));
            pnd_Summary_Amounts_Pnd_Summ_Res_Irr_Amt.nadd(pnd_1099_State_Tbl_Pnd_Res_Irr_Amt.getValue(pnd_State_Tbl_Ndx.getInt() + 1,1,":",2));                           //Natural: ADD #RES-IRR-AMT ( #STATE-TBL-NDX,1:2 ) TO #SUMM-RES-IRR-AMT
            getReports().display(9, new ReportEmptyLineSuppression(true),"/State",                                                                                        //Natural: DISPLAY ( 9 ) ( ES = ON ) '/State' TWRATBL2.TIRCNTL-STATE-FULL-NAME 'Form/Count' #SUMM-STATE-FORM-CNT ( EM = Z,ZZZ,ZZ9 ) VERT AS 'State Distrib/Local Distrib' #SUMM-STATE-DISTR-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #SUMM-LOCAL-DISTR-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) VERT AS 'State Withheld/Local Withheld' #SUMM-STATE-WTHHLD-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #SUMM-LOCAL-WTHHLD-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) VERT AS 'Res. Gross  /Res. Taxable' #SUMM-RES-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #SUMM-RES-TAXABLE-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) HORIZ 'Residency/IVC' #SUMM-RES-IVC-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'Res. Fed Tax' #SUMM-RES-FED-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'Res. IRR Amt' #SUMM-RES-IRR-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            		pdaTwratbl2.getTwratbl2_Tircntl_State_Full_Name(),"Form/Count",
            		pnd_Summary_Amounts_Pnd_Summ_State_Form_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "State Distrib/Local Distrib",
                
            		pnd_Summary_Amounts_Pnd_Summ_State_Distr_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Summary_Amounts_Pnd_Summ_Local_Distr_Amt, new 
                ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "State Withheld/Local Withheld",
            		pnd_Summary_Amounts_Pnd_Summ_State_Wthhld_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Summary_Amounts_Pnd_Summ_Local_Wthhld_Amt, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "Res. Gross  /Res. Taxable",
            		pnd_Summary_Amounts_Pnd_Summ_Res_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Summary_Amounts_Pnd_Summ_Res_Taxable_Amt, new 
                ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.HORIZONTAL,"Residency/IVC",
            		pnd_Summary_Amounts_Pnd_Summ_Res_Ivc_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Res. Fed Tax",
            		pnd_Summary_Amounts_Pnd_Summ_Res_Fed_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Res. IRR Amt",
            		pnd_Summary_Amounts_Pnd_Summ_Res_Irr_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(9, 1);                                                                                                                                      //Natural: SKIP ( 9 ) 1
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    //*  01/30/08 - AY
    private void sub_Print_5498() throws Exception                                                                                                                        //Natural: PRINT-5498
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Form_Text.setValue(pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Old_Form_Type.getInt() + 1));                                                     //Natural: ASSIGN #FORM-TEXT := #TWRAFRMN.#FORM-NAME ( #OLD-FORM-TYPE )
        //*  DASDH
        //*  BK
        //*  SAIK
        getReports().display(3, "Type of/Form",                                                                                                                           //Natural: DISPLAY ( 3 ) 'Type of/Form' #FORM-TEXT ( IS = ON ) 'Form/Count' #5498-FORM-CNT ( EM = Z,ZZZ,ZZ9 ) 'IRA Contribution/Classic Amount' #5498-TRAD-IRA-CONTRIB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'Late RO Amt' #5498-POSTPN-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'IRA Contribution/ROTH Amount' #5498-ROTH-IRA-CONTRIB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'SEP Amount' #5498-SEP-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'Rollover/Contribution' #5498-TRAD-IRA-ROLLOVER ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'Repayments' #5498-REPAYMENTS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'Recharactorization/Amount' #5498-TRAD-IRA-RECHAR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '/Conversion Amount' #5498-ROTH-IRA-CONVERSION ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '/Fair-Mkt-Val Amt' #5498-ACCOUNT-FMV ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
        		pnd_Form_Text, new IdenticalSuppress(true),"Form/Count",
        		pnd_5498_Control_Totals_Pnd_5498_Form_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"IRA Contribution/Classic Amount",
        		pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Contrib, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Late RO Amt",
        		pnd_5498_Control_Totals_Pnd_5498_Postpn_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"IRA Contribution/ROTH Amount",
        		pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Contrib, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"SEP Amount",
        		pnd_5498_Control_Totals_Pnd_5498_Sep_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Rollover/Contribution",
        		pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rollover, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Repayments",
        		pnd_5498_Control_Totals_Pnd_5498_Repayments_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Recharactorization/Amount",
        		pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rechar, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Conversion Amount",
        		pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Conversion, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Fair-Mkt-Val Amt",
        		pnd_5498_Control_Totals_Pnd_5498_Account_Fmv, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(3, NEWLINE,NEWLINE,NEWLINE,"Held Cnt...:",pnd_Hold_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Empty Forms:",pnd_Empty_Form_Cnt,            //Natural: WRITE ( 3 ) // /'Held Cnt...:' #HOLD-CNT ( EM = Z,ZZZ,ZZ9 ) /'Empty Forms:' #EMPTY-FORM-CNT ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Print_1042s() throws Exception                                                                                                                       //Natural: PRINT-1042S
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        //*  01/30/08 - AY
        //*  11/2014
        //*  11/2014
        getReports().display(4, "Type of/Form",                                                                                                                           //Natural: DISPLAY ( 4 ) 'Type of/Form' #TWRAFRMN.#FORM-NAME ( #OLD-FORM-TYPE ) 'Form/Count' #1042S-TRANS-COUNT ( * ) ( EM = Z,ZZZ,ZZ9 ) '/Gross Income' #1042S-GROSS-INCOME ( * ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'Pension/Annuities' #1042S-PENSION-AMT ( * ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '/Interest' #1042S-INTEREST-AMT ( * ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'NRA Tax/Withheld' #1042S-NRA-TAX-WTHLD ( * ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '/Amount Repaid' #1042S-REFUND-AMT ( * ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '(NON-FATCA)/(FATCA)'
        		pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Old_Form_Type.getInt() + 1),"Form/Count",
        		pnd_1042s_Control_Totals_Pnd_1042s_Trans_Count.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZ9"),"/Gross Income",
        		pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income.getValue("*"), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Pension/Annuities",
        		pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt.getValue("*"), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Interest",
        		pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt.getValue("*"), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"NRA Tax/Withheld",
        		pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld.getValue("*"), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Amount Repaid",
        		pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt.getValue("*"), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"(NON-FATCA)/(FATCA)");
        if (Global.isEscape()) return;
        //*  11/2014 START
        getReports().write(4, NEWLINE,NEWLINE,"Total   ",pnd_Form_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),pnd_1042s_Totals_Pnd_1042s_Total_Gross, new ReportEditMask        //Natural: WRITE ( 4 ) // 'Total   ' #FORM-CNT ( EM = Z,ZZZ,ZZ9 ) #1042S-TOTAL-GROSS ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #1042S-TOTAL-PENSION ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #1042S-TOTAL-INTEREST ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #1042S-TOTAL-TAX ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #1042S-TOTAL-REFUND ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1042s_Totals_Pnd_1042s_Total_Pension, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1042s_Totals_Pnd_1042s_Total_Interest, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1042s_Totals_Pnd_1042s_Total_Tax, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1042s_Totals_Pnd_1042s_Total_Refund, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  11/2014 END
        getReports().write(4, NEWLINE,NEWLINE,NEWLINE,"Held Cnt...:",pnd_Hold_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Empty Forms:",pnd_Empty_Form_Cnt,            //Natural: WRITE ( 4 ) // /'Held Cnt...:' #HOLD-CNT ( EM = Z,ZZZ,ZZ9 ) /'Empty Forms:' #EMPTY-FORM-CNT ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    //*  PR
    private void sub_Print_480_7() throws Exception                                                                                                                       //Natural: PRINT-480-7
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        getReports().display(5, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Type of/Form",                                                       //Natural: DISPLAY ( 5 ) ( HC = R ) 'Type of/Form' #FORM-TEXT ( HC = L ) 'Form/Count' #4807C-FORM-CNT ( #K ) 'Gross/Proceeds' #4807C-GROSS-AMT ( #K ) 'Interest/Amount' #4807C-INT-AMT ( #K ) 'Distribution Amount' #4807C-DISTRIB-AMT ( #K ) / 'Rollover Amount' #4807C-GROSS-AMT-ROLL ( #K ) 'Taxable/Amount' #4807C-TAXABLE-AMT ( #K ) 'IVC Amount' #4807C-IVC-AMT ( #K ) / 'Rollover IVC Amount' #4807C-IVC-AMT-ROLL ( #K )
        		pnd_Form_Text, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"Form/Count",
        		pnd_4807c_Control_Totals_Pnd_4807c_Form_Cnt.getValue(pnd_K),"Gross/Proceeds",
        		pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt.getValue(pnd_K),"Interest/Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Int_Amt.getValue(pnd_K),"Distribution Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Distrib_Amt.getValue(pnd_K),NEWLINE,"Rollover Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt_Roll.getValue(pnd_K),"Taxable/Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Taxable_Amt.getValue(pnd_K),"IVC Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt.getValue(pnd_K),NEWLINE,"Rollover IVC Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt_Roll.getValue(pnd_K));
        if (Global.isEscape()) return;
        getReports().skip(5, 1);                                                                                                                                          //Natural: SKIP ( 5 ) 1
    }
    private void sub_Increment_4807_Total() throws Exception                                                                                                              //Natural: INCREMENT-4807-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        pnd_4807c_Control_Totals_Pnd_4807c_Form_Cnt.getValue(2).nadd(pnd_4807c_Control_Totals_Pnd_4807c_Form_Cnt.getValue(1));                                            //Natural: ADD #4807C-FORM-CNT ( 1 ) TO #4807C-FORM-CNT ( 2 )
        pnd_4807c_Control_Totals_Pnd_4807c_Empty_Form_Cnt.getValue(2).nadd(pnd_4807c_Control_Totals_Pnd_4807c_Empty_Form_Cnt.getValue(1));                                //Natural: ADD #4807C-EMPTY-FORM-CNT ( 1 ) TO #4807C-EMPTY-FORM-CNT ( 2 )
        pnd_4807c_Control_Totals_Pnd_4807c_Hold_Cnt.getValue(2).nadd(pnd_4807c_Control_Totals_Pnd_4807c_Hold_Cnt.getValue(1));                                            //Natural: ADD #4807C-HOLD-CNT ( 1 ) TO #4807C-HOLD-CNT ( 2 )
        pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt.getValue(2).nadd(pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt.getValue(1));                                          //Natural: ADD #4807C-GROSS-AMT ( 1 ) TO #4807C-GROSS-AMT ( 2 )
        pnd_4807c_Control_Totals_Pnd_4807c_Int_Amt.getValue(2).nadd(pnd_4807c_Control_Totals_Pnd_4807c_Int_Amt.getValue(1));                                              //Natural: ADD #4807C-INT-AMT ( 1 ) TO #4807C-INT-AMT ( 2 )
        pnd_4807c_Control_Totals_Pnd_4807c_Distrib_Amt.getValue(2).nadd(pnd_4807c_Control_Totals_Pnd_4807c_Distrib_Amt.getValue(1));                                      //Natural: ADD #4807C-DISTRIB-AMT ( 1 ) TO #4807C-DISTRIB-AMT ( 2 )
        pnd_4807c_Control_Totals_Pnd_4807c_Taxable_Amt.getValue(2).nadd(pnd_4807c_Control_Totals_Pnd_4807c_Taxable_Amt.getValue(1));                                      //Natural: ADD #4807C-TAXABLE-AMT ( 1 ) TO #4807C-TAXABLE-AMT ( 2 )
        pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt.getValue(2).nadd(pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt.getValue(1));                                              //Natural: ADD #4807C-IVC-AMT ( 1 ) TO #4807C-IVC-AMT ( 2 )
        pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt_Roll.getValue(2).nadd(pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt_Roll.getValue(1));                                //Natural: ADD #4807C-GROSS-AMT-ROLL ( 1 ) TO #4807C-GROSS-AMT-ROLL ( 2 )
        pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt_Roll.getValue(2).nadd(pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt_Roll.getValue(1));                                    //Natural: ADD #4807C-IVC-AMT-ROLL ( 1 ) TO #4807C-IVC-AMT-ROLL ( 2 )
    }
    private void sub_Print_4807_Grand_Total() throws Exception                                                                                                            //Natural: PRINT-4807-GRAND-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Form_Text.setValue("Total");                                                                                                                                  //Natural: ASSIGN #FORM-TEXT := 'Total'
        pnd_K.setValue(2);                                                                                                                                                //Natural: ASSIGN #K := 2
        //*  PR
                                                                                                                                                                          //Natural: PERFORM PRINT-480-7
        sub_Print_480_7();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        getReports().write(5, NEWLINE,NEWLINE,NEWLINE,"HELD CNT...:",pnd_4807c_Control_Totals_Pnd_4807c_Hold_Cnt.getValue(pnd_K), new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Empty Forms:",pnd_4807c_Control_Totals_Pnd_4807c_Empty_Form_Cnt.getValue(pnd_K),  //Natural: WRITE ( 5 ) // /'HELD CNT...:' #4807C-HOLD-CNT ( #K ) /'Empty Forms:' #4807C-EMPTY-FORM-CNT ( #K )
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    //*  CANADIAN
    private void sub_Print_Nr4() throws Exception                                                                                                                         //Natural: PRINT-NR4
    {
        if (BLNatReinput.isReinput()) return;

        FOR04:                                                                                                                                                            //Natural: FOR #NR4-NDX = 1 TO 4
        for (pnd_Nr4_Ndx.setValue(1); condition(pnd_Nr4_Ndx.lessOrEqual(4)); pnd_Nr4_Ndx.nadd(1))
        {
            if (condition(pnd_Nr4_Ndx.equals(4)))                                                                                                                         //Natural: IF #NR4-NDX = 4
            {
                pnd_Form_Text.setValue("TOTAL");                                                                                                                          //Natural: ASSIGN #FORM-TEXT := 'TOTAL'
                //*  01/30/08 - AY
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Form_Text.setValue(pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Old_Form_Type.getInt() + 1));                                             //Natural: ASSIGN #FORM-TEXT := #TWRAFRMN.#FORM-NAME ( #OLD-FORM-TYPE )
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet1354 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #NR4-NDX;//Natural: VALUE 1
            if (condition((pnd_Nr4_Ndx.equals(1))))
            {
                decideConditionsMet1354++;
                pnd_Nr4_Income_Desc.setValue("Periodic Payment");                                                                                                         //Natural: ASSIGN #NR4-INCOME-DESC := 'Periodic Payment'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Nr4_Ndx.equals(2))))
            {
                decideConditionsMet1354++;
                pnd_Nr4_Income_Desc.setValue("Lump Sum/RTB");                                                                                                             //Natural: ASSIGN #NR4-INCOME-DESC := 'Lump Sum/RTB'
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Nr4_Ndx.equals(3))))
            {
                decideConditionsMet1354++;
                pnd_Nr4_Income_Desc.setValue("Interest");                                                                                                                 //Natural: ASSIGN #NR4-INCOME-DESC := 'Interest'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Nr4_Income_Desc.reset();                                                                                                                              //Natural: RESET #NR4-INCOME-DESC
            }                                                                                                                                                             //Natural: END-DECIDE
            getReports().display(6, "Type of/Form",                                                                                                                       //Natural: DISPLAY ( 6 ) 'Type of/Form' #FORM-TEXT ( IS = ON ) '/Income Desc' #NR4-INCOME-DESC 'Form/Count' #NR4-FORM-CNT ( #NR4-NDX ) ( EM = Z,ZZZ,ZZ9 ) '/Gross Amount' #NR4-GROSS-AMT ( #NR4-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '/Interest Amount' #NR4-INTEREST-AMT ( #NR4-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'Canadian Tax/Withheld' #NR4-FED-TAX-WTHLD ( #NR4-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            		pnd_Form_Text, new IdenticalSuppress(true),"/Income Desc",
            		pnd_Nr4_Income_Desc,"Form/Count",
            		pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt.getValue(pnd_Nr4_Ndx), new ReportEditMask ("Z,ZZZ,ZZ9"),"/Gross Amount",
            		pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt.getValue(pnd_Nr4_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Interest Amount",
            		pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt.getValue(pnd_Nr4_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Canadian Tax/Withheld",
            		pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld.getValue(pnd_Nr4_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(6, NEWLINE,NEWLINE,NEWLINE,"Held Cnt...:",pnd_Hold_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Empty Forms:",pnd_Empty_Form_Cnt,            //Natural: WRITE ( 6 ) // /'Held Cnt...:' #HOLD-CNT ( EM = Z,ZZZ,ZZ9 ) /'Empty Forms:' #EMPTY-FORM-CNT ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Write_Hold_Rpt() throws Exception                                                                                                                    //Natural: WRITE-HOLD-RPT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        if (condition(ldaTwrl9710.getForm_Tirf_Moore_Hold_Ind().equals(" ")))                                                                                             //Natural: IF TIRF-MOORE-HOLD-IND EQ ' '
        {
            ldaTwrl5951.getTwrl5951_Pnd_Moore_Mail().setValue("Y");                                                                                                       //Natural: ASSIGN TWRL5951.#MOORE-MAIL := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl5951.getTwrl5951_Pnd_Moore_Mail().setValue("N");                                                                                                       //Natural: ASSIGN TWRL5951.#MOORE-MAIL := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        //*  EB
        if (condition(ldaTwrl9710.getForm_Tirf_Paper_Print_Hold_Ind().equals(" ")))                                                                                       //Natural: IF TIRF-PAPER-PRINT-HOLD-IND EQ ' '
        {
            ldaTwrl5951.getTwrl5951_Pnd_In_House_Mail().setValue("Y");                                                                                                    //Natural: ASSIGN TWRL5951.#IN-HOUSE-MAIL := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl5951.getTwrl5951_Pnd_In_House_Mail().setValue("N");                                                                                                    //Natural: ASSIGN TWRL5951.#IN-HOUSE-MAIL := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Print_Hold_Ind().equals(" ")))                                                                                             //Natural: IF TIRF-PRINT-HOLD-IND EQ ' '
        {
            ldaTwrl5951.getTwrl5951_Pnd_Vol_Printable().setValue("Y");                                                                                                    //Natural: ASSIGN TWRL5951.#VOL-PRINTABLE := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl5951.getTwrl5951_Pnd_Vol_Printable().setValue("N");                                                                                                    //Natural: ASSIGN TWRL5951.#VOL-PRINTABLE := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Irs_Reject_Ind().equals(" ")))                                                                                             //Natural: IF TIRF-IRS-REJECT-IND EQ ' '
        {
            ldaTwrl5951.getTwrl5951_Pnd_Irs_Reportable().setValue("Y");                                                                                                   //Natural: ASSIGN TWRL5951.#IRS-REPORTABLE := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl5951.getTwrl5951_Pnd_Irs_Reportable().setValue("N");                                                                                                   //Natural: ASSIGN TWRL5951.#IRS-REPORTABLE := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        FOR05:                                                                                                                                                            //Natural: FOR #SYS-ERR-NDX = 1 TO C*TIRF-SYS-ERR
        for (pnd_Sys_Err_Ndx.setValue(1); condition(pnd_Sys_Err_Ndx.lessOrEqual(ldaTwrl9710.getForm_Count_Casttirf_Sys_Err())); pnd_Sys_Err_Ndx.nadd(1))
        {
            pnd_Err_Ndx.setValue(ldaTwrl9710.getForm_Tirf_Sys_Err().getValue(pnd_Sys_Err_Ndx));                                                                           //Natural: ASSIGN #ERR-NDX := TIRF-SYS-ERR ( #SYS-ERR-NDX )
            //*  J.ROTHOLZ 12/28/01
            if (condition(pnd_Err_Ndx.equals(24)))                                                                                                                        //Natural: IF #ERR-NDX = 24
            {
                //*  BYPASS MISSING
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  WALKROUTE ERRORS
            }                                                                                                                                                             //Natural: END-IF
            //*  SYS HOLD COUNT
            pnd_Sys_Err_Cnt.getValue(pnd_Err_Ndx.getInt() + 1).nadd(1);                                                                                                   //Natural: ADD 1 TO #SYS-ERR-CNT ( #ERR-NDX )
            //*  SYS HOLD TOTAL
            pnd_Sys_Err_Cnt.getValue(0 + 1).nadd(1);                                                                                                                      //Natural: ADD 1 TO #SYS-ERR-CNT ( 0 )
            ldaTwrl5951.getTwrl5951_Pnd_Tax_Year().setValue(ldaTwrl9710.getForm_Tirf_Tax_Year());                                                                         //Natural: ASSIGN TWRL5951.#TAX-YEAR := FORM.TIRF-TAX-YEAR
            ldaTwrl5951.getTwrl5951_Pnd_Form_Type().setValue(ldaTwrl9710.getForm_Tirf_Form_Type());                                                                       //Natural: ASSIGN TWRL5951.#FORM-TYPE := FORM.TIRF-FORM-TYPE
            ldaTwrl5951.getTwrl5951_Pnd_Comp_Short_Name().setValue(ldaTwrlcomp.getPnd_Twrlcomp_Pnd_Comp_Short_Name().getValue(pnd_Company_Ndx.getInt()                    //Natural: ASSIGN TWRL5951.#COMP-SHORT-NAME := #TWRLCOMP.#COMP-SHORT-NAME ( #COMPANY-NDX )
                + 1));
            ldaTwrl5951.getTwrl5951_Pnd_Tin().setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                                   //Natural: ASSIGN TWRL5951.#TIN := FORM.TIRF-TIN
            ldaTwrl5951.getTwrl5951_Pnd_Contract_Nbr().setValue(ldaTwrl9710.getForm_Tirf_Contract_Nbr());                                                                 //Natural: ASSIGN TWRL5951.#CONTRACT-NBR := FORM.TIRF-CONTRACT-NBR
            ldaTwrl5951.getTwrl5951_Pnd_Payee_Cde().setValue(ldaTwrl9710.getForm_Tirf_Payee_Cde());                                                                       //Natural: ASSIGN TWRL5951.#PAYEE-CDE := FORM.TIRF-PAYEE-CDE
            ldaTwrl5951.getTwrl5951_Pnd_Err_Cde().setValue(pnd_Err_Ndx);                                                                                                  //Natural: ASSIGN TWRL5951.#ERR-CDE := #ERR-NDX
            getWorkFiles().write(2, false, ldaTwrl5951.getTwrl5951());                                                                                                    //Natural: WRITE WORK FILE 2 TWRL5951
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Get_Company_Desc() throws Exception                                                                                                                  //Natural: GET-COMPANY-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        DbsUtil.examine(new ExamineSource(ldaTwrlcomp.getPnd_Twrlcomp_Pnd_Comp_Code().getValue("*")), new ExamineSearch(pnd_Old_Company_Cde), new ExamineGivingIndex(pnd_Company_Ndx)); //Natural: EXAMINE #COMP-CODE ( * ) FOR #OLD-COMPANY-CDE GIVING INDEX #COMPANY-NDX
    }
    private void sub_Check_Reported_To_Irs() throws Exception                                                                                                             //Natural: CHECK-REPORTED-TO-IRS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        //*   CHECK IF PREVIOUSLY REPORTED TO IRS
        pnd_Reported_To_Irs.reset();                                                                                                                                      //Natural: RESET #REPORTED-TO-IRS
        pnd_Hist_Key_Pnd_Tax_Year.setValue(ldaTwrl9710.getForm_Tirf_Tax_Year());                                                                                          //Natural: ASSIGN #HIST-KEY.#TAX-YEAR := FORM.TIRF-TAX-YEAR
        pnd_Hist_Key_Pnd_Tin.setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                                                    //Natural: ASSIGN #HIST-KEY.#TIN := FORM.TIRF-TIN
        pnd_Hist_Key_Pnd_Form_Type.setValue(ldaTwrl9710.getForm_Tirf_Form_Type());                                                                                        //Natural: ASSIGN #HIST-KEY.#FORM-TYPE := FORM.TIRF-FORM-TYPE
        pnd_Hist_Key_Pnd_Contract_Nbr.setValue(ldaTwrl9710.getForm_Tirf_Contract_Nbr());                                                                                  //Natural: ASSIGN #HIST-KEY.#CONTRACT-NBR := FORM.TIRF-CONTRACT-NBR
        pnd_Hist_Key_Pnd_Payee.setValue(ldaTwrl9710.getForm_Tirf_Payee_Cde());                                                                                            //Natural: ASSIGN #HIST-KEY.#PAYEE := FORM.TIRF-PAYEE-CDE
        pnd_Hist_Key_Pnd_Key.setValue(ldaTwrl9710.getForm_Tirf_Key());                                                                                                    //Natural: ASSIGN #HIST-KEY.#KEY := FORM.TIRF-KEY
        pnd_Hist_Key_Pnd_Ts.reset();                                                                                                                                      //Natural: RESET #HIST-KEY.#TS #HIST-KEY.#IRS-RPT-IND
        pnd_Hist_Key_Pnd_Irs_Rpt_Ind.reset();
        vw_hist_Form.createHistogram                                                                                                                                      //Natural: HISTOGRAM HIST-FORM TIRF-SUPERDE-4 STARTING FROM #HIST-KEY
        (
        "HIST_IRS",
        "TIRF_SUPERDE_4",
        new Wc[] { new Wc("TIRF_SUPERDE_4", ">=", pnd_Hist_Key.getBinary(), WcType.WITH) }
        );
        HIST_IRS:
        while (condition(vw_hist_Form.readNextRow("HIST_IRS")))
        {
            if (condition(hist_Form_Pnd_Tirf_Tax_Year.notEquals(pnd_Hist_Key_Pnd_Tax_Year) || hist_Form_Pnd_Tirf_Tin.notEquals(pnd_Hist_Key_Pnd_Tin) ||                   //Natural: IF #TIRF-TAX-YEAR NE #HIST-KEY.#TAX-YEAR OR #TIRF-TIN NE #HIST-KEY.#TIN OR #TIRF-FORM-TYPE NE #HIST-KEY.#FORM-TYPE OR #TIRF-CONTRACT-NBR NE #HIST-KEY.#CONTRACT-NBR OR #TIRF-PAYEE-CDE NE #HIST-KEY.#PAYEE OR #TIRF-KEY NE #HIST-KEY.#KEY
                hist_Form_Pnd_Tirf_Form_Type.notEquals(pnd_Hist_Key_Pnd_Form_Type) || hist_Form_Pnd_Tirf_Contract_Nbr.notEquals(pnd_Hist_Key_Pnd_Contract_Nbr) 
                || hist_Form_Pnd_Tirf_Payee_Cde.notEquals(pnd_Hist_Key_Pnd_Payee) || hist_Form_Pnd_Tirf_Key.notEquals(pnd_Hist_Key_Pnd_Key)))
            {
                if (true) break HIST_IRS;                                                                                                                                 //Natural: ESCAPE BOTTOM ( HIST-IRS. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(hist_Form_Pnd_Tirf_Irs_Rpt_Ind.equals("H")))                                                                                                    //Natural: IF #TIRF-IRS-RPT-IND = 'H'
            {
                //*  BYPASS HELD FORMS
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Reported_To_Irs.setValue(true);                                                                                                                       //Natural: ASSIGN #REPORTED-TO-IRS := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        //*   CHECK IF REPORTED TO STATE
        //*  1099-R
        if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().equals(1)))                                                                                                    //Natural: IF FORM.TIRF-FORM-TYPE = 01
        {
            vw_form_1099_R.startDatabaseRead                                                                                                                              //Natural: READ FORM-1099-R BY TIRF-SUPERDE-1 = #HIST-KEY
            (
            "F_1099_R",
            new Wc[] { new Wc("TIRF_SUPERDE_1", ">=", pnd_Hist_Key.getBinary(), WcType.BY) },
            new Oc[] { new Oc("TIRF_SUPERDE_1", "ASC") }
            );
            F_1099_R:
            while (condition(vw_form_1099_R.readNextRow("F_1099_R")))
            {
                if (condition(form_1099_R_Tirf_Tax_Year.notEquals(pnd_Hist_Key_Pnd_Tax_Year) || form_1099_R_Tirf_Tin.notEquals(pnd_Hist_Key_Pnd_Tin) ||                   //Natural: IF FORM-1099-R.TIRF-TAX-YEAR NE #HIST-KEY.#TAX-YEAR OR FORM-1099-R.TIRF-TIN NE #HIST-KEY.#TIN OR FORM-1099-R.TIRF-FORM-TYPE NE #HIST-KEY.#FORM-TYPE OR FORM-1099-R.TIRF-CONTRACT-NBR NE #HIST-KEY.#CONTRACT-NBR OR FORM-1099-R.TIRF-PAYEE-CDE NE #HIST-KEY.#PAYEE OR FORM-1099-R.TIRF-KEY NE #HIST-KEY.#KEY
                    form_1099_R_Tirf_Form_Type.notEquals(pnd_Hist_Key_Pnd_Form_Type) || form_1099_R_Tirf_Contract_Nbr.notEquals(pnd_Hist_Key_Pnd_Contract_Nbr) 
                    || form_1099_R_Tirf_Payee_Cde.notEquals(pnd_Hist_Key_Pnd_Payee) || form_1099_R_Tirf_Key.notEquals(pnd_Hist_Key_Pnd_Key)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                FOR06:                                                                                                                                                    //Natural: FOR #STATE-NDX = 1 TO FORM-1099-R.C*TIRF-1099-R-STATE-GRP
                for (pnd_State_Ndx.setValue(1); condition(pnd_State_Ndx.lessOrEqual(form_1099_R_Count_Casttirf_1099_R_State_Grp)); pnd_State_Ndx.nadd(1))
                {
                    if (condition(form_1099_R_Tirf_State_Auth_Rpt_Ind.getValue(pnd_State_Ndx).equals(" ") || form_1099_R_Tirf_State_Auth_Rpt_Ind.getValue(pnd_State_Ndx).equals("H"))) //Natural: IF FORM-1099-R.TIRF-STATE-AUTH-RPT-IND ( #STATE-NDX ) = ' ' OR = 'H'
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Reported_To_Irs.setValue(true);                                                                                                               //Natural: ASSIGN #REPORTED-TO-IRS := TRUE
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F_1099_R"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F_1099_R"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_System_Hold_Code() throws Exception                                                                                                            //Natural: CHECK-SYSTEM-HOLD-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        if (condition(ldaTwrl9710.getForm_Count_Casttirf_Sys_Err().notEquals(getZero())))                                                                                 //Natural: IF FORM.C*TIRF-SYS-ERR NE 0
        {
            pnd_Hold_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #HOLD-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-HOLD-RPT
            sub_Write_Hold_Rpt();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Suny_Form_Db_Control() throws Exception                                                                                                              //Natural: SUNY-FORM-DB-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        //*  HEADER RECORD
        if (condition(ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(" ")))                                                                                               //Natural: IF FORM.TIRF-CONTRACT-NBR = ' '
        {
            pnd_Accum_F10_Pnd_Form_Cnt_F10.getValue(1).nadd(1);                                                                                                           //Natural: ADD 1 TO #FORM-CNT-F10 ( 1 )
            pnd_Accum_F10_Pnd_Gross_Amt_F10.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                       //Natural: ADD TIRF-GROSS-AMT TO #GROSS-AMT-F10 ( 1 )
            pnd_Accum_F10_Pnd_Fed_Wthld_F10.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                                                   //Natural: ADD TIRF-FED-TAX-WTHLD TO #FED-WTHLD-F10 ( 1 )
            pnd_Accum_F10_Pnd_St_Wthld_F10.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Summ_State_Tax_Wthld());                                                             //Natural: ADD TIRF-SUMM-STATE-TAX-WTHLD TO #ST-WTHLD-F10 ( 1 )
            pnd_Accum_F10_Pnd_Loc_Wthld_F10.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Summ_Local_Tax_Wthld());                                                            //Natural: ADD TIRF-SUMM-LOCAL-TAX-WTHLD TO #LOC-WTHLD-F10 ( 1 )
            if (condition(ldaTwrl9710.getForm_Tirf_Multi_Plan().equals(true)))                                                                                            //Natural: IF TIRF-MULTI-PLAN = TRUE
            {
                pnd_Accum_F10_Pnd_Form_Cnt_F10.getValue(2).nadd(1);                                                                                                       //Natural: ADD 1 TO #FORM-CNT-F10 ( 2 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Accum_F10_Pnd_Form_Cnt_F10.getValue(3).nadd(1);                                                                                                       //Natural: ADD 1 TO #FORM-CNT-F10 ( 3 )
            }                                                                                                                                                             //Natural: END-IF
            //*  DETAIL RECORD
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  ADD 1                           TO #CONT-CNT-F10(1)
            //*    WRITE '=' TIRF-GROSS-AMT 3X 'from Detail Record'
            pnd_Vars_F10_Pnd_Gross_Accum_F10.reset();                                                                                                                     //Natural: RESET #GROSS-ACCUM-F10
            pnd_Vars_F10_Pnd_Contract_Amt_Err_F10.resetInitial();                                                                                                         //Natural: RESET INITIAL #CONTRACT-AMT-ERR-F10
            pnd_Vars_F10_Pnd_Gross_Accum_F10.nadd(ldaTwrl9710.getForm_Tirf_Ltr_Gross_Amt().getValue("*"));                                                                //Natural: ADD FORM.TIRF-LTR-GROSS-AMT ( * ) TO #GROSS-ACCUM-F10
            FOR07:                                                                                                                                                        //Natural: FOR #I-F10 1 TO C*FORM.TIRF-LTR-GRP
            for (pnd_Vars_F10_Pnd_I_F10.setValue(1); condition(pnd_Vars_F10_Pnd_I_F10.lessOrEqual(ldaTwrl9710.getForm_Count_Casttirf_Ltr_Grp())); pnd_Vars_F10_Pnd_I_F10.nadd(1))
            {
                //*     IF FORM.TIRF-GROSS-AMT = 0
                //*       ESCAPE BOTTOM
                //*     END-IF
                if (condition(pnd_Vars_F10_Pnd_Contract_Amt_Err_F10.getBoolean()))                                                                                        //Natural: IF #CONTRACT-AMT-ERR-F10
                {
                    getReports().write(0, ">>> Contract w/amt-error:",ldaTwrl9710.getForm_Tirf_Ltr_Gross_Amt().getValue(pnd_Vars_F10_Pnd_I_F10),new TabSetting(43),       //Natural: WRITE '>>> Contract w/amt-error:' FORM.TIRF-LTR-GROSS-AMT ( #I-F10 ) 43T '(Det)'
                        "(Det)");
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Vars_F10_Pnd_All_Gross_F10.nadd(ldaTwrl9710.getForm_Tirf_Ltr_Gross_Amt().getValue(pnd_Vars_F10_Pnd_I_F10));                                           //Natural: ADD FORM.TIRF-LTR-GROSS-AMT ( #I-F10 ) TO #ALL-GROSS-F10
                pnd_Accum_F10_Pnd_Cont_Cnt_F10.getValue(1).nadd(1);                                                                                                       //Natural: ADD 1 TO #CONT-CNT-F10 ( 1 )
                short decideConditionsMet1512 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN FORM.TIRF-PLAN-TYPE ( #I-F10 ) = 'C'
                if (condition(ldaTwrl9710.getForm_Tirf_Plan_Type().getValue(pnd_Vars_F10_Pnd_I_F10).equals("C")))
                {
                    decideConditionsMet1512++;
                    pnd_Accum_F10_Pnd_Gross_Amt_F10.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Ltr_Gross_Amt().getValue(pnd_Vars_F10_Pnd_I_F10));                          //Natural: ADD FORM.TIRF-LTR-GROSS-AMT ( #I-F10 ) TO #GROSS-AMT-F10 ( 2 )
                    pnd_Accum_F10_Pnd_Fed_Wthld_F10.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Ltr_Fed_Tax_Wthld().getValue(pnd_Vars_F10_Pnd_I_F10));                      //Natural: ADD FORM.TIRF-LTR-FED-TAX-WTHLD ( #I-F10 ) TO #FED-WTHLD-F10 ( 2 )
                    pnd_Accum_F10_Pnd_St_Wthld_F10.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Ltr_State_Tax_Wthld().getValue(pnd_Vars_F10_Pnd_I_F10));                     //Natural: ADD FORM.TIRF-LTR-STATE-TAX-WTHLD ( #I-F10 ) TO #ST-WTHLD-F10 ( 2 )
                    pnd_Accum_F10_Pnd_Loc_Wthld_F10.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Ltr_Loc_Tax_Wthld().getValue(pnd_Vars_F10_Pnd_I_F10));                      //Natural: ADD FORM.TIRF-LTR-LOC-TAX-WTHLD ( #I-F10 ) TO #LOC-WTHLD-F10 ( 2 )
                    pnd_Accum_F10_Pnd_Cont_Cnt_F10.getValue(2).nadd(1);                                                                                                   //Natural: ADD 1 TO #CONT-CNT-F10 ( 2 )
                }                                                                                                                                                         //Natural: WHEN FORM.TIRF-PLAN-TYPE ( #I-F10 ) = 'P'
                else if (condition(ldaTwrl9710.getForm_Tirf_Plan_Type().getValue(pnd_Vars_F10_Pnd_I_F10).equals("P")))
                {
                    decideConditionsMet1512++;
                    DbsUtil.examine(new ExamineSource(pdaTwraplsc.getTwraplsc_Rt_Plan().getValue(1,":",pdaTwraplsc.getTwraplsc_Rt_Plan_Cnt())), new ExamineSearch(ldaTwrl9710.getForm_Tirf_Plan().getValue(pnd_Vars_F10_Pnd_I_F10)),  //Natural: EXAMINE TWRAPLSC.RT-PLAN ( 1:RT-PLAN-CNT ) FOR FORM.TIRF-PLAN ( #I-F10 ) GIVING INDEX #NDX-F10
                        new ExamineGivingIndex(pnd_Vars_F10_Pnd_Ndx_F10));
                    if (condition(pnd_Vars_F10_Pnd_Ndx_F10.equals(getZero())))                                                                                            //Natural: IF #NDX-F10 = 0
                    {
                        pnd_Accum_F10_Pnd_Gross_Amt_F10.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Ltr_Gross_Amt().getValue(pnd_Vars_F10_Pnd_I_F10));                      //Natural: ADD FORM.TIRF-LTR-GROSS-AMT ( #I-F10 ) TO #GROSS-AMT-F10 ( 2 )
                        pnd_Accum_F10_Pnd_Fed_Wthld_F10.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Ltr_Fed_Tax_Wthld().getValue(pnd_Vars_F10_Pnd_I_F10));                  //Natural: ADD FORM.TIRF-LTR-FED-TAX-WTHLD ( #I-F10 ) TO #FED-WTHLD-F10 ( 2 )
                        pnd_Accum_F10_Pnd_St_Wthld_F10.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Ltr_State_Tax_Wthld().getValue(pnd_Vars_F10_Pnd_I_F10));                 //Natural: ADD FORM.TIRF-LTR-STATE-TAX-WTHLD ( #I-F10 ) TO #ST-WTHLD-F10 ( 2 )
                        pnd_Accum_F10_Pnd_Loc_Wthld_F10.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Ltr_Loc_Tax_Wthld().getValue(pnd_Vars_F10_Pnd_I_F10));                  //Natural: ADD FORM.TIRF-LTR-LOC-TAX-WTHLD ( #I-F10 ) TO #LOC-WTHLD-F10 ( 2 )
                        pnd_Accum_F10_Pnd_Cont_Cnt_F10.getValue(2).nadd(1);                                                                                               //Natural: ADD 1 TO #CONT-CNT-F10 ( 2 )
                        //*  #NDX > 0
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Accum_F10_Pnd_Gross_Amt_F10.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Ltr_Gross_Amt().getValue(pnd_Vars_F10_Pnd_I_F10));                      //Natural: ADD FORM.TIRF-LTR-GROSS-AMT ( #I-F10 ) TO #GROSS-AMT-F10 ( 3 )
                        pnd_Accum_F10_Pnd_Fed_Wthld_F10.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Ltr_Fed_Tax_Wthld().getValue(pnd_Vars_F10_Pnd_I_F10));                  //Natural: ADD FORM.TIRF-LTR-FED-TAX-WTHLD ( #I-F10 ) TO #FED-WTHLD-F10 ( 3 )
                        pnd_Accum_F10_Pnd_St_Wthld_F10.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Ltr_State_Tax_Wthld().getValue(pnd_Vars_F10_Pnd_I_F10));                 //Natural: ADD FORM.TIRF-LTR-STATE-TAX-WTHLD ( #I-F10 ) TO #ST-WTHLD-F10 ( 3 )
                        pnd_Accum_F10_Pnd_Loc_Wthld_F10.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Ltr_Loc_Tax_Wthld().getValue(pnd_Vars_F10_Pnd_I_F10));                  //Natural: ADD FORM.TIRF-LTR-LOC-TAX-WTHLD ( #I-F10 ) TO #LOC-WTHLD-F10 ( 3 )
                        pnd_Accum_F10_Pnd_Cont_Cnt_F10.getValue(3).nadd(1);                                                                                               //Natural: ADD 1 TO #CONT-CNT-F10 ( 3 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void atBreakEventRead_Form() throws Exception {atBreakEventRead_Form(false);}
    private void atBreakEventRead_Form(boolean endOfData) throws Exception
    {
        boolean ldaTwrl9710_getForm_Tirf_Form_TypeIsBreak = ldaTwrl9710.getForm_Tirf_Form_Type().isBreak(endOfData);
        boolean ldaTwrl9710_getForm_Tirf_Company_CdeIsBreak = ldaTwrl9710.getForm_Tirf_Company_Cde().isBreak(endOfData);
        if (condition(ldaTwrl9710_getForm_Tirf_Form_TypeIsBreak || ldaTwrl9710_getForm_Tirf_Company_CdeIsBreak))
        {
            if (condition(rEAD_FORMTirf_Tax_YearOld.notEquals(pnd_Form_Key_Pnd_Tax_Year)))                                                                                //Natural: IF OLD ( TIRF-TAX-YEAR ) NE #FORM-KEY.#TAX-YEAR
            {
                if (!endOfData)                                                                                                                                           //Natural: ESCAPE BOTTOM ( READ-FORM. )
                {
                    Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom, "READ_FORM");
                }
                if (condition(true)) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_Old_Company_Cde.setValue(rEAD_FORMTirf_Company_CdeOld);                                                                                                   //Natural: ASSIGN #OLD-COMPANY-CDE := OLD ( TIRF-COMPANY-CDE )
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-DESC
            sub_Get_Company_Desc();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Old_Form_Type.setValue(rEAD_FORMTirf_Form_TypeOld);                                                                                                       //Natural: ASSIGN #OLD-FORM-TYPE := OLD ( TIRF-FORM-TYPE )
            //*  01/30/08 - AY
            short decideConditionsMet782 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE #OLD-FORM-TYPE;//Natural: VALUE 1
            if (condition((pnd_Old_Form_Type.equals(1))))
            {
                decideConditionsMet782++;
                pnd_Form_Text.setValue(pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Old_Form_Type.getInt() + 1));                                             //Natural: ASSIGN #FORM-TEXT := #TWRAFRMN.#FORM-NAME ( #OLD-FORM-TYPE )
                                                                                                                                                                          //Natural: PERFORM PRINT-1099
                sub_Print_1099();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM INCREMENT-1099-TOTAL
                sub_Increment_1099_Total();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_1099r_1099i_Control_Totals.reset();                                                                                                                   //Natural: RESET #1099R-1099I-CONTROL-TOTALS
                if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().notEquals(2)))                                                                                         //Natural: IF TIRF-FORM-TYPE NE 2
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-1099-GRAND-TOTAL
                    sub_Print_1099_Grand_Total();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-1099-STATE-REPORT
                sub_Print_1099_State_Report();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  01/30/08 - AY
                pnd_1099_State_Tbl.getValue("*","*").reset();                                                                                                             //Natural: RESET #1099-STATE-TBL ( *,* )
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Old_Form_Type.equals(2))))
            {
                decideConditionsMet782++;
                pnd_Form_Text.setValue(pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Old_Form_Type.getInt() + 1));                                             //Natural: ASSIGN #FORM-TEXT := #TWRAFRMN.#FORM-NAME ( #OLD-FORM-TYPE )
                                                                                                                                                                          //Natural: PERFORM PRINT-1099
                sub_Print_1099();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM INCREMENT-1099-TOTAL
                sub_Increment_1099_Total();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-1099-GRAND-TOTAL
                sub_Print_1099_Grand_Total();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Old_Form_Type.equals(3))))
            {
                decideConditionsMet782++;
                                                                                                                                                                          //Natural: PERFORM PRINT-1042S
                sub_Print_1042s();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pnd_Old_Form_Type.equals(4))))
            {
                decideConditionsMet782++;
                                                                                                                                                                          //Natural: PERFORM PRINT-5498
                sub_Print_5498();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: VALUE 5
            else if (condition((pnd_Old_Form_Type.equals(5))))
            {
                decideConditionsMet782++;
                pnd_Form_Text.setValue(pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Old_Form_Type.getInt() + 1));                                             //Natural: ASSIGN #FORM-TEXT := #TWRAFRMN.#FORM-NAME ( #OLD-FORM-TYPE )
                pnd_4807c_Control_Totals_Pnd_4807c_Form_Cnt.getValue(1).setValue(pnd_Form_Cnt);                                                                           //Natural: ASSIGN #4807C-FORM-CNT ( 1 ) := #FORM-CNT
                pnd_4807c_Control_Totals_Pnd_4807c_Hold_Cnt.getValue(1).setValue(pnd_Hold_Cnt);                                                                           //Natural: ASSIGN #4807C-HOLD-CNT ( 1 ) := #HOLD-CNT
                pnd_4807c_Control_Totals_Pnd_4807c_Empty_Form_Cnt.getValue(1).setValue(pnd_Empty_Form_Cnt);                                                               //Natural: ASSIGN #4807C-EMPTY-FORM-CNT ( 1 ) := #EMPTY-FORM-CNT
                pnd_4807c_Control_Totals_Pnd_4807c_Distrib_Amt.getValue(1).compute(new ComputeParameters(false, pnd_4807c_Control_Totals_Pnd_4807c_Distrib_Amt.getValue(1)),  //Natural: ASSIGN #4807C-DISTRIB-AMT ( 1 ) := #4807C-GROSS-AMT ( 1 ) + #4807C-INT-AMT ( 1 )
                    pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt.getValue(1).add(pnd_4807c_Control_Totals_Pnd_4807c_Int_Amt.getValue(1)));
                pnd_K.setValue(1);                                                                                                                                        //Natural: ASSIGN #K := 1
                //*  PR
                                                                                                                                                                          //Natural: PERFORM PRINT-480-7
                sub_Print_480_7();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM INCREMENT-4807-TOTAL
                sub_Increment_4807_Total();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-4807-GRAND-TOTAL
                sub_Print_4807_Grand_Total();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: VALUE 7
            else if (condition((pnd_Old_Form_Type.equals(7))))
            {
                decideConditionsMet782++;
                //*  CANADIAN
                                                                                                                                                                          //Natural: PERFORM PRINT-NR4
                sub_Print_Nr4();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Form_Cnt.reset();                                                                                                                                         //Natural: RESET #FORM-CNT #HOLD-CNT #EMPTY-FORM-CNT #SYS-ERR-CNT ( * )
            pnd_Hold_Cnt.reset();
            pnd_Empty_Form_Cnt.reset();
            pnd_Sys_Err_Cnt.getValue("*").reset();
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaTwrl9710_getForm_Tirf_Company_CdeIsBreak))
        {
            pnd_Old_Company_Cde.setValue(ldaTwrl9710.getForm_Tirf_Company_Cde());                                                                                         //Natural: ASSIGN #OLD-COMPANY-CDE := TIRF-COMPANY-CDE
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-DESC
            sub_Get_Company_Desc();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            //*  11/2014
            //*  11/2014
            pnd_1099r_1099i_Control_Totals.reset();                                                                                                                       //Natural: RESET #1099R-1099I-CONTROL-TOTALS #1099-GRAND-TOTALS #1099-STATE-TBL ( *,* ) #1042S-CONTROL-TOTALS ( * ) #1042S-TOTALS #5498-CONTROL-TOTALS #4807C-CONTROL-TOTALS ( * ) #NR4-CONTROL-TOTALS ( * )
            pnd_1099_Grand_Totals.reset();
            pnd_1099_State_Tbl.getValue("*","*").reset();
            pnd_1042s_Control_Totals.getValue("*").reset();
            pnd_1042s_Totals.reset();
            pnd_5498_Control_Totals.reset();
            pnd_4807c_Control_Totals.getValue("*").reset();
            pnd_Nr4_Control_Totals.getValue("*").reset();
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(3));                                                                                                             //Natural: NEWPAGE ( 3 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(4));                                                                                                             //Natural: NEWPAGE ( 4 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(5));                                                                                                             //Natural: NEWPAGE ( 5 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(6));                                                                                                             //Natural: NEWPAGE ( 6 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(7));                                                                                                             //Natural: NEWPAGE ( 7 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(8));                                                                                                             //Natural: NEWPAGE ( 8 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(9));                                                                                                             //Natural: NEWPAGE ( 9 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=132 ZP=ON");
        Global.format(1, "PS=58 LS=133 ZP=ON");
        Global.format(2, "PS=58 LS=133 ZP=ON");
        Global.format(3, "PS=58 LS=133 ZP=ON");
        Global.format(4, "PS=58 LS=133 ZP=ON");
        Global.format(5, "PS=58 LS=133 ZP=ON");
        Global.format(6, "PS=58 LS=133 ZP=ON");
        Global.format(7, "PS=58 LS=133 ZP=ON");
        Global.format(8, "PS=58 LS=133 ZP=ON");
        Global.format(9, "PS=58 LS=133 ZP=ON");
        Global.format(10, "PS=58 LS=133 ZP=ON");
        Global.format(11, "PS=58 LS=133 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(22),"Tax Withholding and Reporting System",new TabSetting(68),"Page:",getReports().getPageNumberDbs(1), 
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(34),"Control Report",new TabSetting(68),"Report: RPT1",NEWLINE,NEWLINE,"Tax Year: ",pnd_Input_Rec_Pnd_Tax_Year, 
            new ReportEditMask ("9999"),NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(2), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"YTD Form Database Control",new TabSetting(110),"Report: RPT2",NEWLINE,new 
            TabSetting(59),"Tax Year",pnd_Input_Rec_Pnd_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE,"Company: ",ldaTwrlcomp.getPnd_Twrlcomp_Pnd_Comp_Short_Name(),
            NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(3), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"YTD Form Database Control",new TabSetting(110),"Report: RPT3",NEWLINE,new 
            TabSetting(59),"Tax Year",pnd_Input_Rec_Pnd_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE,"Company: ",ldaTwrlcomp.getPnd_Twrlcomp_Pnd_Comp_Short_Name(),
            NEWLINE,NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(4), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"YTD Form Database Control",new TabSetting(110),"Report: RPT4",NEWLINE,new 
            TabSetting(59),"Tax Year",pnd_Input_Rec_Pnd_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE,"Company: ",ldaTwrlcomp.getPnd_Twrlcomp_Pnd_Comp_Short_Name(),
            NEWLINE,NEWLINE);
        getReports().write(5, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(5), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"YTD Form Database Control",new TabSetting(110),"Report: RPT5",NEWLINE,new 
            TabSetting(59),"Tax Year",pnd_Input_Rec_Pnd_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE,"Company: ",ldaTwrlcomp.getPnd_Twrlcomp_Pnd_Comp_Short_Name(),
            NEWLINE,NEWLINE);
        getReports().write(6, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(6), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"YTD Form Database Control",new TabSetting(110),"Report: RPT6",NEWLINE,new 
            TabSetting(59),"Tax Year",pnd_Input_Rec_Pnd_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE,"Company: ",ldaTwrlcomp.getPnd_Twrlcomp_Pnd_Comp_Short_Name(),
            NEWLINE,NEWLINE);
        getReports().write(7, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(7), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"YTD Form Database Control",new TabSetting(110),"Report: RPT7",NEWLINE,new 
            TabSetting(59),"Tax Year",pnd_Input_Rec_Pnd_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE,"Company:",ldaTwrlcomp.getPnd_Twrlcomp_Pnd_Comp_Short_Name(),NEWLINE,new 
            TabSetting(26),"Reportable State Transactions",new TabSetting(90),"Non-Reportable State Transactions",NEWLINE,NEWLINE);
        getReports().write(8, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(8), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"YTD Form Database Control",new TabSetting(110),"Report: RPT8",NEWLINE,new 
            TabSetting(55),"Hard Copy State Report",NEWLINE,new TabSetting(59),"Tax Year",pnd_Input_Rec_Pnd_Tax_Year, new ReportEditMask ("9999"),NEWLINE,
            NEWLINE,"Company:",ldaTwrlcomp.getPnd_Twrlcomp_Pnd_Comp_Short_Name(),NEWLINE,NEWLINE);
        getReports().write(9, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(9), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"YTD Form Database Control",new TabSetting(110),"Report: RPT9",NEWLINE,new 
            TabSetting(59),"Tax Year",pnd_Input_Rec_Pnd_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE,"Company:",ldaTwrlcomp.getPnd_Twrlcomp_Pnd_Comp_Short_Name(),
            NEWLINE,NEWLINE,NEWLINE,NEWLINE);
        getReports().write(11, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(2), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(48),"SUNY/CUNY YTD Form Database Control",new 
            TabSetting(110),"Report: RPT2",NEWLINE);

        getReports().setDisplayColumns(2, "///Type of/Form",
        		pnd_Form_Text,"///Form/Count",
        		pnd_Form_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"///Gross/Amount",
        		pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"///Tax Free/IVC",
        		pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, 
            "Taxable/Amount/-----------/State Tax/Withholding",
        		pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "Interest/Amount/-----------/Local Tax/Withholding",
            
        		pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "Federal/Withholding/-----------/IRR Amount",
            
        		pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(7, new ReportEmptyLineSuppression(true),"/State",
        		pdaTwratbl2.getTwratbl2_Tircntl_State_Full_Name(),"Form/Count",
        		pnd_1099_State_Tbl_Pnd_State_Form_Cnt, new ReportEditMask ("ZZ,ZZ9"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "State Distrib/Local Distrib",
            
        		pnd_1099_State_Tbl_Pnd_State_Distr_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099_State_Tbl_Pnd_Local_Distr_Amt, new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "State Withheld/Local Withheld",
        		pnd_1099_State_Tbl_Pnd_State_Wthhld_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099_State_Tbl_Pnd_Local_Wthhld_Amt, new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.HORIZONTAL,"Reporting/Indicator",
        		ldaTwrl5950.getTwrl5950_Pnd_State_Ind_Desc(),"Form/Count",
        		pnd_1099_State_Tbl_Pnd_State_Form_Cnt, new ReportEditMask ("ZZ,ZZ9"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "State Distrib/Local Distrib",
            
        		pnd_1099_State_Tbl_Pnd_State_Distr_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099_State_Tbl_Pnd_Local_Distr_Amt, new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "State Withheld/Local Withheld",
        		pnd_1099_State_Tbl_Pnd_State_Wthhld_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099_State_Tbl_Pnd_Local_Wthhld_Amt, new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(8, new ReportEmptyLineSuppression(true),"/State",
        		pdaTwratbl2.getTwratbl2_Tircntl_State_Full_Name(),"Form/Count",
        		pnd_1099_State_Tbl_Pnd_State_Form_Cnt, new ReportEditMask ("ZZ,ZZ9"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "State Distrib/Local Distrib",
            
        		pnd_1099_State_Tbl_Pnd_State_Distr_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099_State_Tbl_Pnd_Local_Distr_Amt, new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "State Withheld/Local Withheld",
        		pnd_1099_State_Tbl_Pnd_State_Wthhld_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099_State_Tbl_Pnd_Local_Wthhld_Amt, new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(9, new ReportEmptyLineSuppression(true),"/State",
        		pdaTwratbl2.getTwratbl2_Tircntl_State_Full_Name(),"Form/Count",
        		pnd_Summary_Amounts_Pnd_Summ_State_Form_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "State Distrib/Local Distrib",
            
        		pnd_Summary_Amounts_Pnd_Summ_State_Distr_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Summary_Amounts_Pnd_Summ_Local_Distr_Amt, new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "State Withheld/Local Withheld",
        		pnd_Summary_Amounts_Pnd_Summ_State_Wthhld_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Summary_Amounts_Pnd_Summ_Local_Wthhld_Amt, new 
            ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "Res. Gross  /Res. Taxable",
        		pnd_Summary_Amounts_Pnd_Summ_Res_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Summary_Amounts_Pnd_Summ_Res_Taxable_Amt, new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.HORIZONTAL,"Residency/IVC",
        		pnd_Summary_Amounts_Pnd_Summ_Res_Ivc_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Res. Fed Tax",
        		pnd_Summary_Amounts_Pnd_Summ_Res_Fed_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Res. IRR Amt",
        		pnd_Summary_Amounts_Pnd_Summ_Res_Irr_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(3, "Type of/Form",
        		pnd_Form_Text, new IdenticalSuppress(true),"Form/Count",
        		pnd_5498_Control_Totals_Pnd_5498_Form_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"IRA Contribution/Classic Amount",
        		pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Contrib, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Late RO Amt",
        		pnd_5498_Control_Totals_Pnd_5498_Postpn_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"IRA Contribution/ROTH Amount",
        		pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Contrib, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"SEP Amount",
        		pnd_5498_Control_Totals_Pnd_5498_Sep_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Rollover/Contribution",
        		pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rollover, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Repayments",
        		pnd_5498_Control_Totals_Pnd_5498_Repayments_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Recharactorization/Amount",
        		pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rechar, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Conversion Amount",
        		pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Conversion, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Fair-Mkt-Val Amt",
        		pnd_5498_Control_Totals_Pnd_5498_Account_Fmv, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(4, "Type of/Form",
        		pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),"Form/Count",
        		pnd_1042s_Control_Totals_Pnd_1042s_Trans_Count, new ReportEditMask ("Z,ZZZ,ZZ9"),"/Gross Income",
        		pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Pension/Annuities",
        		pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Interest",
        		pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"NRA Tax/Withheld",
        		pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Amount Repaid",
        		pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"(NON-FATCA)/(FATCA)");
        getReports().setDisplayColumns(5, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Type of/Form",
        		pnd_Form_Text, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"Form/Count",
        		pnd_4807c_Control_Totals_Pnd_4807c_Form_Cnt,"Gross/Proceeds",
        		pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt,"Interest/Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Int_Amt,"Distribution Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Distrib_Amt,NEWLINE,"Rollover Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt_Roll,"Taxable/Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Taxable_Amt,"IVC Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt,NEWLINE,"Rollover IVC Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt_Roll);
        getReports().setDisplayColumns(6, "Type of/Form",
        		pnd_Form_Text, new IdenticalSuppress(true),"/Income Desc",
        		pnd_Nr4_Income_Desc,"Form/Count",
        		pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"/Gross Amount",
        		pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Interest Amount",
        		pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Canadian Tax/Withheld",
        		pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
    }
    private void CheckAtStartofData753() throws Exception
    {
        if (condition(ldaTwrl9710.getVw_form().getAtStartOfData()))
        {
            pnd_Old_Company_Cde.setValue(ldaTwrl9710.getForm_Tirf_Company_Cde());                                                                                         //Natural: ASSIGN #OLD-COMPANY-CDE := TIRF-COMPANY-CDE
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-DESC
            sub_Get_Company_Desc();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
