/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:45:33 PM
**        * FROM NATURAL PROGRAM : Twup1053
************************************************************
**        * FILE NAME            : Twup1053.java
**        * CLASS NAME           : Twup1053
**        * INSTANCE NAME        : Twup1053
************************************************************
**--------------------------------------------------------------------
** PROGRAM    :  TWUP1053
** APPLICATION:  TAXWARS SYSTEM
** DESCRIPTION:  TAXWARS FEED TO ODS. THIS FEED WILL BE USED TO
**               COMPARE CPS PAYMENT RECORD TO BE DISPLAYED IN
**               INCOME SUMMARY
**
** AUTHOR     :  COGNIZANT
** HISTORY    :
**   DATE       CHANGE BY      CHANGE DESCRIPTION
** 10/08/2020   S. VIKRAM    OMNI PAY FILE PROCESSING SHIFTED TO PREV.
**                           STEP AND TIAA-CONTRACT UPDATED FOR MATCHING
**                           INDEX-CONTRACT FROM REFERENCE TABLE WITH
**                           ID 2000 IN SUBROUTINE COMPARE-OMNIPAY-DATA
**                                              TAG:  INCOME-SUMMARY
**--------------------------------------------------------------------

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twup1053 extends BLNatBase
{
    // Data Areas
    private LdaTwrl9415 ldaTwrl9415;
    private LdaCpsl100 ldaCpsl100;
    private PdaTwradist pdaTwradist;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_rt_Find;

    private DbsGroup rt_Find_Rt_Record;
    private DbsField rt_Find_Rt_A_I_Ind;
    private DbsField rt_Find_Rt_Table_Id;
    private DbsField rt_Find_Rt_Short_Key;
    private DbsField rt_Find_Rt_Long_Key;
    private DbsField rt_Find_Rt_Desc1;
    private DbsField rt_Find_Rt_Desc2;
    private DbsField rt_Find_Rt_Desc3;
    private DbsField rt_Find_Rt_Desc4;
    private DbsField rt_Find_Rt_Desc5;
    private DbsField rt_Find_Rt_Eff_From_Ccyymmdd;
    private DbsField rt_Find_Rt_Eff_To_Ccyymmdd;
    private DbsField rt_Find_Rt_Upd_Source;
    private DbsField rt_Find_Rt_Upd_User;
    private DbsField rt_Find_Rt_Upd_Ccyymmdd;
    private DbsField rt_Find_Rt_Upd_Timn;

    private DataAccessProgramView vw_rt_Inst;

    private DbsGroup rt_Inst_Rt_Record;
    private DbsField rt_Inst_Rt_A_I_Ind;
    private DbsField rt_Inst_Rt_Table_Id;
    private DbsField rt_Inst_Rt_Short_Key;
    private DbsField rt_Inst_Rt_Long_Key;
    private DbsField rt_Inst_Rt_Desc1;
    private DbsField rt_Inst_Rt_Desc2;
    private DbsField rt_Inst_Rt_Desc3;
    private DbsField rt_Inst_Rt_Desc4;
    private DbsField rt_Inst_Rt_Desc5;
    private DbsField rt_Inst_Rt_Eff_From_Ccyymmdd;
    private DbsField rt_Inst_Rt_Eff_To_Ccyymmdd;
    private DbsField rt_Inst_Rt_Upd_Source;
    private DbsField rt_Inst_Rt_Upd_User;
    private DbsField rt_Inst_Rt_Upd_Ccyymmdd;
    private DbsField rt_Inst_Rt_Upd_Timn;

    private DataAccessProgramView vw_rt_Updt;

    private DbsGroup rt_Updt_Rt_Record;
    private DbsField rt_Updt_Rt_A_I_Ind;
    private DbsField rt_Updt_Rt_Table_Id;
    private DbsField rt_Updt_Rt_Short_Key;
    private DbsField rt_Updt_Rt_Long_Key;
    private DbsField rt_Updt_Rt_Desc1;
    private DbsField rt_Updt_Rt_Desc2;
    private DbsField rt_Updt_Rt_Desc3;
    private DbsField rt_Updt_Rt_Desc4;
    private DbsField rt_Updt_Rt_Desc5;
    private DbsField rt_Updt_Rt_Eff_From_Ccyymmdd;
    private DbsField rt_Updt_Rt_Eff_To_Ccyymmdd;
    private DbsField rt_Updt_Rt_Upd_Source;
    private DbsField rt_Updt_Rt_Upd_User;
    private DbsField rt_Updt_Rt_Upd_Ccyymmdd;
    private DbsField rt_Updt_Rt_Upd_Timn;
    private DbsField pnd_Control_Parm;

    private DbsGroup pnd_Control_Parm__R_Field_1;
    private DbsField pnd_Control_Parm_Pnd_Control_Parm_Date;
    private DbsField pnd_Control_Parm_Pnd_Control_Parm_Time;
    private DbsField pnd_Date_Start;

    private DbsGroup pnd_Date_Start__R_Field_2;
    private DbsField pnd_Date_Start_Pnd_Date_Strt_Ccyy;
    private DbsField pnd_Date_Start_Pnd_Date_Strt_Mm;
    private DbsField pnd_Date_Start_Pnd_Date_Strt_Dd;

    private DbsGroup pnd_Date_Start__R_Field_3;
    private DbsField pnd_Date_Start_Pnd_Date_Start_A;
    private DbsField pnd_Time_Start;

    private DbsGroup pnd_Time_Start__R_Field_4;
    private DbsField pnd_Time_Start_Pnd_Time_Start_Hh;
    private DbsField pnd_Time_Start_Pnd_Time_Start_Ii;
    private DbsField pnd_Time_Start_Pnd_Time_Start_Ss;
    private DbsField pnd_Date_End;

    private DbsGroup pnd_Date_End__R_Field_5;
    private DbsField pnd_Date_End_Pnd_Date_End_Ccyy;
    private DbsField pnd_Date_End_Pnd_Date_End_Mm;
    private DbsField pnd_Date_End_Pnd_Date_End_Dd;

    private DbsGroup pnd_Date_End__R_Field_6;
    private DbsField pnd_Date_End_Pnd_Date_End_A;
    private DbsField pnd_Time_End;

    private DbsGroup pnd_Time_End__R_Field_7;
    private DbsField pnd_Time_End_Pnd_Time_End_Hh;
    private DbsField pnd_Time_End_Pnd_Time_End_Ii;
    private DbsField pnd_Time_End_Pnd_Time_End_Ss;
    private DbsField pnd_Twrpymnt_Interface_Date_User_S;

    private DbsGroup pnd_Twrpymnt_Interface_Date_User_S__R_Field_8;
    private DbsField pnd_Twrpymnt_Interface_Date_User_S_Pnd_Twrpymnt_Dist_Updte_Dte_S;
    private DbsField pnd_Twrpymnt_Interface_Date_User_S_Pnd_Twrpymnt_Dist_Updte_User_S;
    private DbsField pnd_Twrpymnt_Interface_Date_User_E;

    private DbsGroup pnd_Twrpymnt_Interface_Date_User_E__R_Field_9;
    private DbsField pnd_Twrpymnt_Interface_Date_User_E_Pnd_Twrpymnt_Dist_Updte_Dte_E;
    private DbsField pnd_Twrpymnt_Interface_Date_User_E_Pnd_Twrpymnt_Dist_Updte_User_E;
    private DbsField pnd_Start_Invrse_Date;
    private DbsField pnd_End_Invrse_Date;
    private DbsField pnd_Date_Start_D;

    private DbsGroup pnd_Date_Start_D__R_Field_10;
    private DbsField pnd_Date_Start_D_Pnd_Date_Start_Ccyy;
    private DbsField pnd_Date_Start_D_Pnd_Date_Start_Mm;
    private DbsField pnd_Date_Start_D_Pnd_Date_Start_Dd;
    private DbsField pnd_Date_Start_D_2y;

    private DbsGroup pnd_Date_Start_D_2y__R_Field_11;
    private DbsField pnd_Date_Start_D_2y_Pnd_Date_St_2y_Ccyy;

    private DbsGroup pnd_Date_Start_D_2y__R_Field_12;
    private DbsField pnd_Date_Start_D_2y_Pnd_Date_St_2y_Ccyy_N;
    private DbsField pnd_Date_Start_D_2y_Pnd_Date_St_2y_Mm;
    private DbsField pnd_Date_Start_D_2y_Pnd_Date_St_2y_Dd;
    private DbsField pnd_Timex;

    private DbsGroup pnd_Timex__R_Field_13;
    private DbsField pnd_Timex_Pnd_Timex_Hh;
    private DbsField pnd_Timex_Pnd_Timex_Ii;
    private DbsField pnd_Timex_Pnd_Timex_Ss;

    private DbsGroup pnd_Timex__R_Field_14;
    private DbsField pnd_Timex_Pnd_Timen;
    private DbsField pnd_Insert_Or_Update;
    private DbsField pnd_Insert_Rcrd_Count;
    private DbsField pnd_Update_Rcrd_Count;
    private DbsField pnd_I;
    private DbsField pnd_Isn;
    private DbsField pnd_Rt_Super1;

    private DbsGroup pnd_Rt_Super1__R_Field_15;
    private DbsField pnd_Rt_Super1_Pnd_Rt_A_I_Ind;
    private DbsField pnd_Rt_Super1_Pnd_Rt_Table_Id;
    private DbsField pnd_Rt_Super1_Pnd_Rt_Short_Key;
    private DbsField pnd_Rt_Super1_Pnd_Rt_Long_Key;
    private DbsField pnd_Short_Key_Value;

    private DbsGroup pnd_Short_Key_Value__R_Field_16;

    private DbsGroup pnd_Short_Key_Value_Short_Key_Fields;
    private DbsField pnd_Short_Key_Value_Twrpymnt_Tax_Year;
    private DbsField pnd_Long_Key_Value;

    private DbsGroup pnd_Long_Key_Value__R_Field_17;

    private DbsGroup pnd_Long_Key_Value_Long_Key_Fields;
    private DbsField pnd_Long_Key_Value_Twrpymnt_Tax_Id_Nbr;
    private DbsField pnd_Long_Key_Value_Twrpymnt_Contract_Nbr;
    private DbsField pnd_Long_Key_Value_Twrpymnt_Payee_Cde;
    private DbsField pnd_Long_Key_Value_Twrpymnt_Tax_Citizenship;
    private DbsField pnd_Long_Key_Value_Twrpymnt_Citizen_Cde;
    private DbsField pnd_Long_Key_Value_Twrpymnt_Paymt_Category;
    private DbsField pnd_Long_Key_Value_Twrpymnt_Distribution_Cde;
    private DbsField pnd_Long_Key_Value_Twrpymnt_Company_Cde;
    private DbsField pnd_Long_Key_Value_Twrpymnt_Residency_Type;
    private DbsField pnd_Long_Key_Value_Twrpymnt_Residency_Code;
    private DbsField pnd_Long_Key_Value_Twrpymnt_Locality_Cde;
    private DbsField pnd_Long_Key_Value_Ref_Seq_Num;
    private DbsField pnd_Long_Key_Value_Twrpymnt_Contract_Seq_No;
    private DbsField pnd_Long_Key_Value_Seq_Num;

    private DbsGroup outfile;
    private DbsField outfile_Twrpymnt_Tax_Year;
    private DbsField outfile_Twrpymnt_Tax_Id_Nbr;
    private DbsField outfile_Twrpymnt_Contract_Nbr;
    private DbsField outfile_Twrpymnt_Payee_Cde;
    private DbsField outfile_Twrpymnt_Tax_Citizenship;
    private DbsField outfile_Twrpymnt_Citizen_Cde;
    private DbsField outfile_Twrpymnt_Paymt_Category;
    private DbsField outfile_Twrpymnt_Distribution_Cde;
    private DbsField outfile_Twrpymnt_Company_Cde;
    private DbsField outfile_Twrpymnt_Residency_Type;
    private DbsField outfile_Twrpymnt_Residency_Code;
    private DbsField outfile_Twrpymnt_Locality_Cde;
    private DbsField outfile_Ref_Seq_Num;

    private DbsGroup outfile__R_Field_18;
    private DbsField outfile_Ref_Seq_Num_N;

    private DbsGroup outfile__R_Field_19;
    private DbsField outfile_Ref_Seq_Num_B1;
    private DbsField outfile_Ref_Seq_Num_B2;
    private DbsField outfile_Ref_Seq_Num_B3;
    private DbsField outfile_Twrpymnt_Contract_Seq_No;
    private DbsField outfile_Seq_Num;
    private DbsField outfile_Ws_Tax_Year;
    private DbsField outfile_Ws_Tax_Id_Nbr;
    private DbsField outfile_Ws_Contract_Nbr;
    private DbsField outfile_Ws_Payee_Cde;
    private DbsField outfile_Twrpymnt_Pymnt_Dte;
    private DbsField outfile_Twrpymnt_Gross_Amt_Sign;
    private DbsField outfile_Twrpymnt_Gross_Amt;
    private DbsField outfile_Twrpymnt_Pymnt_Status;
    private DbsField outfile_Twrpymnt_Int_Amt_Sign;
    private DbsField outfile_Twrpymnt_Int_Amt;
    private DbsField outfile_Twrpymnt_Orgn_Srce_Cde;
    private DbsField outfile_Twrpymnt_Contract_Type;
    private DbsField outfile_Twrpymnt_Payment_Type;
    private DbsField outfile_Twrpymnt_Settle_Type;
    private DbsField outfile_Twrpymnt_Pymnt_Refer_Nbr;
    private DbsField outfile_Income_Ind;

    private DbsGroup cntlfile;
    private DbsField cntlfile_To_Date;
    private DbsField cntlfile_From_Date;
    private DbsField cntlfile_Num_Of_Records;
    private DbsField pnd_Mask_Date;
    private DbsField pnd_Cur_Year;

    private DbsGroup pnd_Cur_Year__R_Field_20;
    private DbsField pnd_Cur_Year_Pnd_Cur_Year_N;
    private DbsField pnd_Pre_Year;
    private DbsField temp_Distribution_Cde;

    private DbsGroup temp_Distribution_Cde__R_Field_21;
    private DbsField temp_Distribution_Cde_Temp_Distribution_Cde_1;
    private DbsField temp_Distribution_Cde_Temp_Distribution_Cde_2;
    private DbsField pnd_Rt_Super2;

    private DbsGroup pnd_Rt_Super2__R_Field_22;
    private DbsField pnd_Rt_Super2_Pnd_Rt_A_I_Ind2;
    private DbsField pnd_Rt_Super2_Pnd_Rt_Table_Id2;
    private DbsField pnd_Rt_Super2_Pnd_Rt_Short_Key2;
    private DbsField pnd_Rt_Super2_Pnd_Rt_Long_Key2;
    private DbsField pnd_Short_Key_Value2;

    private DbsGroup pnd_Short_Key_Value2__R_Field_23;

    private DbsGroup pnd_Short_Key_Value2_Short_Key_Fields2;
    private DbsField pnd_Short_Key_Value2_Twrt_Tax_Id;
    private DbsField pnd_Short_Key_Value2_Pnd_Filler1;
    private DbsField pnd_Short_Key_Value2_Twrt_Index_Cntrct_Nbr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl9415 = new LdaTwrl9415();
        registerRecord(ldaTwrl9415);
        registerRecord(ldaTwrl9415.getVw_pay());
        ldaCpsl100 = new LdaCpsl100();
        registerRecord(ldaCpsl100);
        registerRecord(ldaCpsl100.getVw_rt());
        localVariables = new DbsRecord();
        pdaTwradist = new PdaTwradist(localVariables);

        // Local Variables

        vw_rt_Find = new DataAccessProgramView(new NameInfo("vw_rt_Find", "RT-FIND"), "REFERENCE_TABLE", "REFERNCE_TABLE");

        rt_Find_Rt_Record = vw_rt_Find.getRecord().newGroupInGroup("RT_FIND_RT_RECORD", "RT-RECORD");
        rt_Find_Rt_A_I_Ind = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        rt_Find_Rt_Table_Id = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "RT_TABLE_ID");
        rt_Find_Rt_Short_Key = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "RT_SHORT_KEY");
        rt_Find_Rt_Long_Key = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "RT_LONG_KEY");
        rt_Find_Rt_Desc1 = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Desc1", "RT-DESC1", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC1");
        rt_Find_Rt_Desc2 = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Desc2", "RT-DESC2", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC2");
        rt_Find_Rt_Desc3 = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Desc3", "RT-DESC3", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC3");
        rt_Find_Rt_Desc4 = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Desc4", "RT-DESC4", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC4");
        rt_Find_Rt_Desc5 = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC5");
        rt_Find_Rt_Eff_From_Ccyymmdd = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Eff_From_Ccyymmdd", "RT-EFF-FROM-CCYYMMDD", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RT_EFF_FROM_CCYYMMDD");
        rt_Find_Rt_Eff_To_Ccyymmdd = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Eff_To_Ccyymmdd", "RT-EFF-TO-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_TO_CCYYMMDD");
        rt_Find_Rt_Upd_Source = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Upd_Source", "RT-UPD-SOURCE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_SOURCE");
        rt_Find_Rt_Upd_User = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Upd_User", "RT-UPD-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_USER");
        rt_Find_Rt_Upd_Ccyymmdd = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Upd_Ccyymmdd", "RT-UPD-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_CCYYMMDD");
        rt_Find_Rt_Upd_Timn = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Upd_Timn", "RT-UPD-TIMN", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "RT_UPD_TIMN");
        registerRecord(vw_rt_Find);

        vw_rt_Inst = new DataAccessProgramView(new NameInfo("vw_rt_Inst", "RT-INST"), "REFERENCE_TABLE", "REFERNCE_TABLE");

        rt_Inst_Rt_Record = vw_rt_Inst.getRecord().newGroupInGroup("RT_INST_RT_RECORD", "RT-RECORD");
        rt_Inst_Rt_A_I_Ind = rt_Inst_Rt_Record.newFieldInGroup("rt_Inst_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        rt_Inst_Rt_Table_Id = rt_Inst_Rt_Record.newFieldInGroup("rt_Inst_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "RT_TABLE_ID");
        rt_Inst_Rt_Short_Key = rt_Inst_Rt_Record.newFieldInGroup("rt_Inst_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "RT_SHORT_KEY");
        rt_Inst_Rt_Long_Key = rt_Inst_Rt_Record.newFieldInGroup("rt_Inst_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "RT_LONG_KEY");
        rt_Inst_Rt_Desc1 = rt_Inst_Rt_Record.newFieldInGroup("rt_Inst_Rt_Desc1", "RT-DESC1", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC1");
        rt_Inst_Rt_Desc2 = rt_Inst_Rt_Record.newFieldInGroup("rt_Inst_Rt_Desc2", "RT-DESC2", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC2");
        rt_Inst_Rt_Desc3 = rt_Inst_Rt_Record.newFieldInGroup("rt_Inst_Rt_Desc3", "RT-DESC3", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC3");
        rt_Inst_Rt_Desc4 = rt_Inst_Rt_Record.newFieldInGroup("rt_Inst_Rt_Desc4", "RT-DESC4", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC4");
        rt_Inst_Rt_Desc5 = rt_Inst_Rt_Record.newFieldInGroup("rt_Inst_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC5");
        rt_Inst_Rt_Eff_From_Ccyymmdd = rt_Inst_Rt_Record.newFieldInGroup("rt_Inst_Rt_Eff_From_Ccyymmdd", "RT-EFF-FROM-CCYYMMDD", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RT_EFF_FROM_CCYYMMDD");
        rt_Inst_Rt_Eff_To_Ccyymmdd = rt_Inst_Rt_Record.newFieldInGroup("rt_Inst_Rt_Eff_To_Ccyymmdd", "RT-EFF-TO-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_TO_CCYYMMDD");
        rt_Inst_Rt_Upd_Source = rt_Inst_Rt_Record.newFieldInGroup("rt_Inst_Rt_Upd_Source", "RT-UPD-SOURCE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_SOURCE");
        rt_Inst_Rt_Upd_User = rt_Inst_Rt_Record.newFieldInGroup("rt_Inst_Rt_Upd_User", "RT-UPD-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_USER");
        rt_Inst_Rt_Upd_Ccyymmdd = rt_Inst_Rt_Record.newFieldInGroup("rt_Inst_Rt_Upd_Ccyymmdd", "RT-UPD-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_CCYYMMDD");
        rt_Inst_Rt_Upd_Timn = rt_Inst_Rt_Record.newFieldInGroup("rt_Inst_Rt_Upd_Timn", "RT-UPD-TIMN", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "RT_UPD_TIMN");
        registerRecord(vw_rt_Inst);

        vw_rt_Updt = new DataAccessProgramView(new NameInfo("vw_rt_Updt", "RT-UPDT"), "REFERENCE_TABLE", "REFERNCE_TABLE");

        rt_Updt_Rt_Record = vw_rt_Updt.getRecord().newGroupInGroup("RT_UPDT_RT_RECORD", "RT-RECORD");
        rt_Updt_Rt_A_I_Ind = rt_Updt_Rt_Record.newFieldInGroup("rt_Updt_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        rt_Updt_Rt_Table_Id = rt_Updt_Rt_Record.newFieldInGroup("rt_Updt_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "RT_TABLE_ID");
        rt_Updt_Rt_Short_Key = rt_Updt_Rt_Record.newFieldInGroup("rt_Updt_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "RT_SHORT_KEY");
        rt_Updt_Rt_Long_Key = rt_Updt_Rt_Record.newFieldInGroup("rt_Updt_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "RT_LONG_KEY");
        rt_Updt_Rt_Desc1 = rt_Updt_Rt_Record.newFieldInGroup("rt_Updt_Rt_Desc1", "RT-DESC1", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC1");
        rt_Updt_Rt_Desc2 = rt_Updt_Rt_Record.newFieldInGroup("rt_Updt_Rt_Desc2", "RT-DESC2", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC2");
        rt_Updt_Rt_Desc3 = rt_Updt_Rt_Record.newFieldInGroup("rt_Updt_Rt_Desc3", "RT-DESC3", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC3");
        rt_Updt_Rt_Desc4 = rt_Updt_Rt_Record.newFieldInGroup("rt_Updt_Rt_Desc4", "RT-DESC4", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC4");
        rt_Updt_Rt_Desc5 = rt_Updt_Rt_Record.newFieldInGroup("rt_Updt_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC5");
        rt_Updt_Rt_Eff_From_Ccyymmdd = rt_Updt_Rt_Record.newFieldInGroup("rt_Updt_Rt_Eff_From_Ccyymmdd", "RT-EFF-FROM-CCYYMMDD", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RT_EFF_FROM_CCYYMMDD");
        rt_Updt_Rt_Eff_To_Ccyymmdd = rt_Updt_Rt_Record.newFieldInGroup("rt_Updt_Rt_Eff_To_Ccyymmdd", "RT-EFF-TO-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_TO_CCYYMMDD");
        rt_Updt_Rt_Upd_Source = rt_Updt_Rt_Record.newFieldInGroup("rt_Updt_Rt_Upd_Source", "RT-UPD-SOURCE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_SOURCE");
        rt_Updt_Rt_Upd_User = rt_Updt_Rt_Record.newFieldInGroup("rt_Updt_Rt_Upd_User", "RT-UPD-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_USER");
        rt_Updt_Rt_Upd_Ccyymmdd = rt_Updt_Rt_Record.newFieldInGroup("rt_Updt_Rt_Upd_Ccyymmdd", "RT-UPD-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_CCYYMMDD");
        rt_Updt_Rt_Upd_Timn = rt_Updt_Rt_Record.newFieldInGroup("rt_Updt_Rt_Upd_Timn", "RT-UPD-TIMN", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "RT_UPD_TIMN");
        registerRecord(vw_rt_Updt);

        pnd_Control_Parm = localVariables.newFieldInRecord("pnd_Control_Parm", "#CONTROL-PARM", FieldType.STRING, 14);

        pnd_Control_Parm__R_Field_1 = localVariables.newGroupInRecord("pnd_Control_Parm__R_Field_1", "REDEFINE", pnd_Control_Parm);
        pnd_Control_Parm_Pnd_Control_Parm_Date = pnd_Control_Parm__R_Field_1.newFieldInGroup("pnd_Control_Parm_Pnd_Control_Parm_Date", "#CONTROL-PARM-DATE", 
            FieldType.STRING, 8);
        pnd_Control_Parm_Pnd_Control_Parm_Time = pnd_Control_Parm__R_Field_1.newFieldInGroup("pnd_Control_Parm_Pnd_Control_Parm_Time", "#CONTROL-PARM-TIME", 
            FieldType.STRING, 6);
        pnd_Date_Start = localVariables.newFieldInRecord("pnd_Date_Start", "#DATE-START", FieldType.NUMERIC, 8);

        pnd_Date_Start__R_Field_2 = localVariables.newGroupInRecord("pnd_Date_Start__R_Field_2", "REDEFINE", pnd_Date_Start);
        pnd_Date_Start_Pnd_Date_Strt_Ccyy = pnd_Date_Start__R_Field_2.newFieldInGroup("pnd_Date_Start_Pnd_Date_Strt_Ccyy", "#DATE-STRT-CCYY", FieldType.NUMERIC, 
            4);
        pnd_Date_Start_Pnd_Date_Strt_Mm = pnd_Date_Start__R_Field_2.newFieldInGroup("pnd_Date_Start_Pnd_Date_Strt_Mm", "#DATE-STRT-MM", FieldType.NUMERIC, 
            2);
        pnd_Date_Start_Pnd_Date_Strt_Dd = pnd_Date_Start__R_Field_2.newFieldInGroup("pnd_Date_Start_Pnd_Date_Strt_Dd", "#DATE-STRT-DD", FieldType.NUMERIC, 
            2);

        pnd_Date_Start__R_Field_3 = localVariables.newGroupInRecord("pnd_Date_Start__R_Field_3", "REDEFINE", pnd_Date_Start);
        pnd_Date_Start_Pnd_Date_Start_A = pnd_Date_Start__R_Field_3.newFieldInGroup("pnd_Date_Start_Pnd_Date_Start_A", "#DATE-START-A", FieldType.STRING, 
            8);
        pnd_Time_Start = localVariables.newFieldInRecord("pnd_Time_Start", "#TIME-START", FieldType.STRING, 6);

        pnd_Time_Start__R_Field_4 = localVariables.newGroupInRecord("pnd_Time_Start__R_Field_4", "REDEFINE", pnd_Time_Start);
        pnd_Time_Start_Pnd_Time_Start_Hh = pnd_Time_Start__R_Field_4.newFieldInGroup("pnd_Time_Start_Pnd_Time_Start_Hh", "#TIME-START-HH", FieldType.STRING, 
            2);
        pnd_Time_Start_Pnd_Time_Start_Ii = pnd_Time_Start__R_Field_4.newFieldInGroup("pnd_Time_Start_Pnd_Time_Start_Ii", "#TIME-START-II", FieldType.STRING, 
            2);
        pnd_Time_Start_Pnd_Time_Start_Ss = pnd_Time_Start__R_Field_4.newFieldInGroup("pnd_Time_Start_Pnd_Time_Start_Ss", "#TIME-START-SS", FieldType.STRING, 
            2);
        pnd_Date_End = localVariables.newFieldInRecord("pnd_Date_End", "#DATE-END", FieldType.NUMERIC, 8);

        pnd_Date_End__R_Field_5 = localVariables.newGroupInRecord("pnd_Date_End__R_Field_5", "REDEFINE", pnd_Date_End);
        pnd_Date_End_Pnd_Date_End_Ccyy = pnd_Date_End__R_Field_5.newFieldInGroup("pnd_Date_End_Pnd_Date_End_Ccyy", "#DATE-END-CCYY", FieldType.NUMERIC, 
            4);
        pnd_Date_End_Pnd_Date_End_Mm = pnd_Date_End__R_Field_5.newFieldInGroup("pnd_Date_End_Pnd_Date_End_Mm", "#DATE-END-MM", FieldType.NUMERIC, 2);
        pnd_Date_End_Pnd_Date_End_Dd = pnd_Date_End__R_Field_5.newFieldInGroup("pnd_Date_End_Pnd_Date_End_Dd", "#DATE-END-DD", FieldType.NUMERIC, 2);

        pnd_Date_End__R_Field_6 = localVariables.newGroupInRecord("pnd_Date_End__R_Field_6", "REDEFINE", pnd_Date_End);
        pnd_Date_End_Pnd_Date_End_A = pnd_Date_End__R_Field_6.newFieldInGroup("pnd_Date_End_Pnd_Date_End_A", "#DATE-END-A", FieldType.STRING, 8);
        pnd_Time_End = localVariables.newFieldInRecord("pnd_Time_End", "#TIME-END", FieldType.STRING, 6);

        pnd_Time_End__R_Field_7 = localVariables.newGroupInRecord("pnd_Time_End__R_Field_7", "REDEFINE", pnd_Time_End);
        pnd_Time_End_Pnd_Time_End_Hh = pnd_Time_End__R_Field_7.newFieldInGroup("pnd_Time_End_Pnd_Time_End_Hh", "#TIME-END-HH", FieldType.STRING, 2);
        pnd_Time_End_Pnd_Time_End_Ii = pnd_Time_End__R_Field_7.newFieldInGroup("pnd_Time_End_Pnd_Time_End_Ii", "#TIME-END-II", FieldType.STRING, 2);
        pnd_Time_End_Pnd_Time_End_Ss = pnd_Time_End__R_Field_7.newFieldInGroup("pnd_Time_End_Pnd_Time_End_Ss", "#TIME-END-SS", FieldType.STRING, 2);
        pnd_Twrpymnt_Interface_Date_User_S = localVariables.newFieldInRecord("pnd_Twrpymnt_Interface_Date_User_S", "#TWRPYMNT-INTERFACE-DATE-USER-S", 
            FieldType.STRING, 16);

        pnd_Twrpymnt_Interface_Date_User_S__R_Field_8 = localVariables.newGroupInRecord("pnd_Twrpymnt_Interface_Date_User_S__R_Field_8", "REDEFINE", pnd_Twrpymnt_Interface_Date_User_S);
        pnd_Twrpymnt_Interface_Date_User_S_Pnd_Twrpymnt_Dist_Updte_Dte_S = pnd_Twrpymnt_Interface_Date_User_S__R_Field_8.newFieldInGroup("pnd_Twrpymnt_Interface_Date_User_S_Pnd_Twrpymnt_Dist_Updte_Dte_S", 
            "#TWRPYMNT-DIST-UPDTE-DTE-S", FieldType.STRING, 8);
        pnd_Twrpymnt_Interface_Date_User_S_Pnd_Twrpymnt_Dist_Updte_User_S = pnd_Twrpymnt_Interface_Date_User_S__R_Field_8.newFieldInGroup("pnd_Twrpymnt_Interface_Date_User_S_Pnd_Twrpymnt_Dist_Updte_User_S", 
            "#TWRPYMNT-DIST-UPDTE-USER-S", FieldType.STRING, 8);
        pnd_Twrpymnt_Interface_Date_User_E = localVariables.newFieldInRecord("pnd_Twrpymnt_Interface_Date_User_E", "#TWRPYMNT-INTERFACE-DATE-USER-E", 
            FieldType.STRING, 16);

        pnd_Twrpymnt_Interface_Date_User_E__R_Field_9 = localVariables.newGroupInRecord("pnd_Twrpymnt_Interface_Date_User_E__R_Field_9", "REDEFINE", pnd_Twrpymnt_Interface_Date_User_E);
        pnd_Twrpymnt_Interface_Date_User_E_Pnd_Twrpymnt_Dist_Updte_Dte_E = pnd_Twrpymnt_Interface_Date_User_E__R_Field_9.newFieldInGroup("pnd_Twrpymnt_Interface_Date_User_E_Pnd_Twrpymnt_Dist_Updte_Dte_E", 
            "#TWRPYMNT-DIST-UPDTE-DTE-E", FieldType.STRING, 8);
        pnd_Twrpymnt_Interface_Date_User_E_Pnd_Twrpymnt_Dist_Updte_User_E = pnd_Twrpymnt_Interface_Date_User_E__R_Field_9.newFieldInGroup("pnd_Twrpymnt_Interface_Date_User_E_Pnd_Twrpymnt_Dist_Updte_User_E", 
            "#TWRPYMNT-DIST-UPDTE-USER-E", FieldType.STRING, 8);
        pnd_Start_Invrse_Date = localVariables.newFieldInRecord("pnd_Start_Invrse_Date", "#START-INVRSE-DATE", FieldType.NUMERIC, 8);
        pnd_End_Invrse_Date = localVariables.newFieldInRecord("pnd_End_Invrse_Date", "#END-INVRSE-DATE", FieldType.NUMERIC, 8);
        pnd_Date_Start_D = localVariables.newFieldInRecord("pnd_Date_Start_D", "#DATE-START-D", FieldType.STRING, 8);

        pnd_Date_Start_D__R_Field_10 = localVariables.newGroupInRecord("pnd_Date_Start_D__R_Field_10", "REDEFINE", pnd_Date_Start_D);
        pnd_Date_Start_D_Pnd_Date_Start_Ccyy = pnd_Date_Start_D__R_Field_10.newFieldInGroup("pnd_Date_Start_D_Pnd_Date_Start_Ccyy", "#DATE-START-CCYY", 
            FieldType.STRING, 4);
        pnd_Date_Start_D_Pnd_Date_Start_Mm = pnd_Date_Start_D__R_Field_10.newFieldInGroup("pnd_Date_Start_D_Pnd_Date_Start_Mm", "#DATE-START-MM", FieldType.STRING, 
            2);
        pnd_Date_Start_D_Pnd_Date_Start_Dd = pnd_Date_Start_D__R_Field_10.newFieldInGroup("pnd_Date_Start_D_Pnd_Date_Start_Dd", "#DATE-START-DD", FieldType.STRING, 
            2);
        pnd_Date_Start_D_2y = localVariables.newFieldInRecord("pnd_Date_Start_D_2y", "#DATE-START-D-2Y", FieldType.STRING, 8);

        pnd_Date_Start_D_2y__R_Field_11 = localVariables.newGroupInRecord("pnd_Date_Start_D_2y__R_Field_11", "REDEFINE", pnd_Date_Start_D_2y);
        pnd_Date_Start_D_2y_Pnd_Date_St_2y_Ccyy = pnd_Date_Start_D_2y__R_Field_11.newFieldInGroup("pnd_Date_Start_D_2y_Pnd_Date_St_2y_Ccyy", "#DATE-ST-2Y-CCYY", 
            FieldType.STRING, 4);

        pnd_Date_Start_D_2y__R_Field_12 = pnd_Date_Start_D_2y__R_Field_11.newGroupInGroup("pnd_Date_Start_D_2y__R_Field_12", "REDEFINE", pnd_Date_Start_D_2y_Pnd_Date_St_2y_Ccyy);
        pnd_Date_Start_D_2y_Pnd_Date_St_2y_Ccyy_N = pnd_Date_Start_D_2y__R_Field_12.newFieldInGroup("pnd_Date_Start_D_2y_Pnd_Date_St_2y_Ccyy_N", "#DATE-ST-2Y-CCYY-N", 
            FieldType.NUMERIC, 4);
        pnd_Date_Start_D_2y_Pnd_Date_St_2y_Mm = pnd_Date_Start_D_2y__R_Field_11.newFieldInGroup("pnd_Date_Start_D_2y_Pnd_Date_St_2y_Mm", "#DATE-ST-2Y-MM", 
            FieldType.STRING, 2);
        pnd_Date_Start_D_2y_Pnd_Date_St_2y_Dd = pnd_Date_Start_D_2y__R_Field_11.newFieldInGroup("pnd_Date_Start_D_2y_Pnd_Date_St_2y_Dd", "#DATE-ST-2Y-DD", 
            FieldType.STRING, 2);
        pnd_Timex = localVariables.newFieldInRecord("pnd_Timex", "#TIMEX", FieldType.STRING, 6);

        pnd_Timex__R_Field_13 = localVariables.newGroupInRecord("pnd_Timex__R_Field_13", "REDEFINE", pnd_Timex);
        pnd_Timex_Pnd_Timex_Hh = pnd_Timex__R_Field_13.newFieldInGroup("pnd_Timex_Pnd_Timex_Hh", "#TIMEX-HH", FieldType.STRING, 2);
        pnd_Timex_Pnd_Timex_Ii = pnd_Timex__R_Field_13.newFieldInGroup("pnd_Timex_Pnd_Timex_Ii", "#TIMEX-II", FieldType.STRING, 2);
        pnd_Timex_Pnd_Timex_Ss = pnd_Timex__R_Field_13.newFieldInGroup("pnd_Timex_Pnd_Timex_Ss", "#TIMEX-SS", FieldType.STRING, 2);

        pnd_Timex__R_Field_14 = localVariables.newGroupInRecord("pnd_Timex__R_Field_14", "REDEFINE", pnd_Timex);
        pnd_Timex_Pnd_Timen = pnd_Timex__R_Field_14.newFieldInGroup("pnd_Timex_Pnd_Timen", "#TIMEN", FieldType.NUMERIC, 6);
        pnd_Insert_Or_Update = localVariables.newFieldInRecord("pnd_Insert_Or_Update", "#INSERT-OR-UPDATE", FieldType.STRING, 1);
        pnd_Insert_Rcrd_Count = localVariables.newFieldInRecord("pnd_Insert_Rcrd_Count", "#INSERT-RCRD-COUNT", FieldType.NUMERIC, 10);
        pnd_Update_Rcrd_Count = localVariables.newFieldInRecord("pnd_Update_Rcrd_Count", "#UPDATE-RCRD-COUNT", FieldType.NUMERIC, 10);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Rt_Super1 = localVariables.newFieldInRecord("pnd_Rt_Super1", "#RT-SUPER1", FieldType.STRING, 66);

        pnd_Rt_Super1__R_Field_15 = localVariables.newGroupInRecord("pnd_Rt_Super1__R_Field_15", "REDEFINE", pnd_Rt_Super1);
        pnd_Rt_Super1_Pnd_Rt_A_I_Ind = pnd_Rt_Super1__R_Field_15.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt_A_I_Ind", "#RT-A-I-IND", FieldType.STRING, 1);
        pnd_Rt_Super1_Pnd_Rt_Table_Id = pnd_Rt_Super1__R_Field_15.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt_Table_Id", "#RT-TABLE-ID", FieldType.STRING, 5);
        pnd_Rt_Super1_Pnd_Rt_Short_Key = pnd_Rt_Super1__R_Field_15.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt_Short_Key", "#RT-SHORT-KEY", FieldType.STRING, 
            20);
        pnd_Rt_Super1_Pnd_Rt_Long_Key = pnd_Rt_Super1__R_Field_15.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt_Long_Key", "#RT-LONG-KEY", FieldType.STRING, 40);
        pnd_Short_Key_Value = localVariables.newFieldInRecord("pnd_Short_Key_Value", "#SHORT-KEY-VALUE", FieldType.STRING, 20);

        pnd_Short_Key_Value__R_Field_16 = localVariables.newGroupInRecord("pnd_Short_Key_Value__R_Field_16", "REDEFINE", pnd_Short_Key_Value);

        pnd_Short_Key_Value_Short_Key_Fields = pnd_Short_Key_Value__R_Field_16.newGroupInGroup("pnd_Short_Key_Value_Short_Key_Fields", "SHORT-KEY-FIELDS");
        pnd_Short_Key_Value_Twrpymnt_Tax_Year = pnd_Short_Key_Value_Short_Key_Fields.newFieldInGroup("pnd_Short_Key_Value_Twrpymnt_Tax_Year", "TWRPYMNT-TAX-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Long_Key_Value = localVariables.newFieldInRecord("pnd_Long_Key_Value", "#LONG-KEY-VALUE", FieldType.STRING, 40);

        pnd_Long_Key_Value__R_Field_17 = localVariables.newGroupInRecord("pnd_Long_Key_Value__R_Field_17", "REDEFINE", pnd_Long_Key_Value);

        pnd_Long_Key_Value_Long_Key_Fields = pnd_Long_Key_Value__R_Field_17.newGroupInGroup("pnd_Long_Key_Value_Long_Key_Fields", "LONG-KEY-FIELDS");
        pnd_Long_Key_Value_Twrpymnt_Tax_Id_Nbr = pnd_Long_Key_Value_Long_Key_Fields.newFieldInGroup("pnd_Long_Key_Value_Twrpymnt_Tax_Id_Nbr", "TWRPYMNT-TAX-ID-NBR", 
            FieldType.STRING, 10);
        pnd_Long_Key_Value_Twrpymnt_Contract_Nbr = pnd_Long_Key_Value_Long_Key_Fields.newFieldInGroup("pnd_Long_Key_Value_Twrpymnt_Contract_Nbr", "TWRPYMNT-CONTRACT-NBR", 
            FieldType.STRING, 8);
        pnd_Long_Key_Value_Twrpymnt_Payee_Cde = pnd_Long_Key_Value_Long_Key_Fields.newFieldInGroup("pnd_Long_Key_Value_Twrpymnt_Payee_Cde", "TWRPYMNT-PAYEE-CDE", 
            FieldType.STRING, 2);
        pnd_Long_Key_Value_Twrpymnt_Tax_Citizenship = pnd_Long_Key_Value_Long_Key_Fields.newFieldInGroup("pnd_Long_Key_Value_Twrpymnt_Tax_Citizenship", 
            "TWRPYMNT-TAX-CITIZENSHIP", FieldType.STRING, 1);
        pnd_Long_Key_Value_Twrpymnt_Citizen_Cde = pnd_Long_Key_Value_Long_Key_Fields.newFieldInGroup("pnd_Long_Key_Value_Twrpymnt_Citizen_Cde", "TWRPYMNT-CITIZEN-CDE", 
            FieldType.STRING, 2);
        pnd_Long_Key_Value_Twrpymnt_Paymt_Category = pnd_Long_Key_Value_Long_Key_Fields.newFieldInGroup("pnd_Long_Key_Value_Twrpymnt_Paymt_Category", 
            "TWRPYMNT-PAYMT-CATEGORY", FieldType.STRING, 1);
        pnd_Long_Key_Value_Twrpymnt_Distribution_Cde = pnd_Long_Key_Value_Long_Key_Fields.newFieldInGroup("pnd_Long_Key_Value_Twrpymnt_Distribution_Cde", 
            "TWRPYMNT-DISTRIBUTION-CDE", FieldType.STRING, 2);
        pnd_Long_Key_Value_Twrpymnt_Company_Cde = pnd_Long_Key_Value_Long_Key_Fields.newFieldInGroup("pnd_Long_Key_Value_Twrpymnt_Company_Cde", "TWRPYMNT-COMPANY-CDE", 
            FieldType.STRING, 1);
        pnd_Long_Key_Value_Twrpymnt_Residency_Type = pnd_Long_Key_Value_Long_Key_Fields.newFieldInGroup("pnd_Long_Key_Value_Twrpymnt_Residency_Type", 
            "TWRPYMNT-RESIDENCY-TYPE", FieldType.STRING, 1);
        pnd_Long_Key_Value_Twrpymnt_Residency_Code = pnd_Long_Key_Value_Long_Key_Fields.newFieldInGroup("pnd_Long_Key_Value_Twrpymnt_Residency_Code", 
            "TWRPYMNT-RESIDENCY-CODE", FieldType.STRING, 2);
        pnd_Long_Key_Value_Twrpymnt_Locality_Cde = pnd_Long_Key_Value_Long_Key_Fields.newFieldInGroup("pnd_Long_Key_Value_Twrpymnt_Locality_Cde", "TWRPYMNT-LOCALITY-CDE", 
            FieldType.STRING, 3);
        pnd_Long_Key_Value_Ref_Seq_Num = pnd_Long_Key_Value_Long_Key_Fields.newFieldInGroup("pnd_Long_Key_Value_Ref_Seq_Num", "REF-SEQ-NUM", FieldType.STRING, 
            3);
        pnd_Long_Key_Value_Twrpymnt_Contract_Seq_No = pnd_Long_Key_Value_Long_Key_Fields.newFieldInGroup("pnd_Long_Key_Value_Twrpymnt_Contract_Seq_No", 
            "TWRPYMNT-CONTRACT-SEQ-NO", FieldType.NUMERIC, 2);
        pnd_Long_Key_Value_Seq_Num = pnd_Long_Key_Value_Long_Key_Fields.newFieldInGroup("pnd_Long_Key_Value_Seq_Num", "SEQ-NUM", FieldType.NUMERIC, 2);

        outfile = localVariables.newGroupInRecord("outfile", "OUTFILE");
        outfile_Twrpymnt_Tax_Year = outfile.newFieldInGroup("outfile_Twrpymnt_Tax_Year", "TWRPYMNT-TAX-YEAR", FieldType.NUMERIC, 4);
        outfile_Twrpymnt_Tax_Id_Nbr = outfile.newFieldInGroup("outfile_Twrpymnt_Tax_Id_Nbr", "TWRPYMNT-TAX-ID-NBR", FieldType.STRING, 10);
        outfile_Twrpymnt_Contract_Nbr = outfile.newFieldInGroup("outfile_Twrpymnt_Contract_Nbr", "TWRPYMNT-CONTRACT-NBR", FieldType.STRING, 8);
        outfile_Twrpymnt_Payee_Cde = outfile.newFieldInGroup("outfile_Twrpymnt_Payee_Cde", "TWRPYMNT-PAYEE-CDE", FieldType.STRING, 2);
        outfile_Twrpymnt_Tax_Citizenship = outfile.newFieldInGroup("outfile_Twrpymnt_Tax_Citizenship", "TWRPYMNT-TAX-CITIZENSHIP", FieldType.STRING, 1);
        outfile_Twrpymnt_Citizen_Cde = outfile.newFieldInGroup("outfile_Twrpymnt_Citizen_Cde", "TWRPYMNT-CITIZEN-CDE", FieldType.STRING, 2);
        outfile_Twrpymnt_Paymt_Category = outfile.newFieldInGroup("outfile_Twrpymnt_Paymt_Category", "TWRPYMNT-PAYMT-CATEGORY", FieldType.STRING, 1);
        outfile_Twrpymnt_Distribution_Cde = outfile.newFieldInGroup("outfile_Twrpymnt_Distribution_Cde", "TWRPYMNT-DISTRIBUTION-CDE", FieldType.STRING, 
            2);
        outfile_Twrpymnt_Company_Cde = outfile.newFieldInGroup("outfile_Twrpymnt_Company_Cde", "TWRPYMNT-COMPANY-CDE", FieldType.STRING, 1);
        outfile_Twrpymnt_Residency_Type = outfile.newFieldInGroup("outfile_Twrpymnt_Residency_Type", "TWRPYMNT-RESIDENCY-TYPE", FieldType.STRING, 1);
        outfile_Twrpymnt_Residency_Code = outfile.newFieldInGroup("outfile_Twrpymnt_Residency_Code", "TWRPYMNT-RESIDENCY-CODE", FieldType.STRING, 2);
        outfile_Twrpymnt_Locality_Cde = outfile.newFieldInGroup("outfile_Twrpymnt_Locality_Cde", "TWRPYMNT-LOCALITY-CDE", FieldType.STRING, 3);
        outfile_Ref_Seq_Num = outfile.newFieldInGroup("outfile_Ref_Seq_Num", "REF-SEQ-NUM", FieldType.STRING, 3);

        outfile__R_Field_18 = outfile.newGroupInGroup("outfile__R_Field_18", "REDEFINE", outfile_Ref_Seq_Num);
        outfile_Ref_Seq_Num_N = outfile__R_Field_18.newFieldInGroup("outfile_Ref_Seq_Num_N", "REF-SEQ-NUM-N", FieldType.NUMERIC, 3);

        outfile__R_Field_19 = outfile.newGroupInGroup("outfile__R_Field_19", "REDEFINE", outfile_Ref_Seq_Num);
        outfile_Ref_Seq_Num_B1 = outfile__R_Field_19.newFieldInGroup("outfile_Ref_Seq_Num_B1", "REF-SEQ-NUM-B1", FieldType.STRING, 1);
        outfile_Ref_Seq_Num_B2 = outfile__R_Field_19.newFieldInGroup("outfile_Ref_Seq_Num_B2", "REF-SEQ-NUM-B2", FieldType.STRING, 1);
        outfile_Ref_Seq_Num_B3 = outfile__R_Field_19.newFieldInGroup("outfile_Ref_Seq_Num_B3", "REF-SEQ-NUM-B3", FieldType.STRING, 1);
        outfile_Twrpymnt_Contract_Seq_No = outfile.newFieldInGroup("outfile_Twrpymnt_Contract_Seq_No", "TWRPYMNT-CONTRACT-SEQ-NO", FieldType.NUMERIC, 
            2);
        outfile_Seq_Num = outfile.newFieldInGroup("outfile_Seq_Num", "SEQ-NUM", FieldType.NUMERIC, 2);
        outfile_Ws_Tax_Year = outfile.newFieldInGroup("outfile_Ws_Tax_Year", "WS-TAX-YEAR", FieldType.NUMERIC, 4);
        outfile_Ws_Tax_Id_Nbr = outfile.newFieldInGroup("outfile_Ws_Tax_Id_Nbr", "WS-TAX-ID-NBR", FieldType.STRING, 10);
        outfile_Ws_Contract_Nbr = outfile.newFieldInGroup("outfile_Ws_Contract_Nbr", "WS-CONTRACT-NBR", FieldType.STRING, 8);
        outfile_Ws_Payee_Cde = outfile.newFieldInGroup("outfile_Ws_Payee_Cde", "WS-PAYEE-CDE", FieldType.STRING, 2);
        outfile_Twrpymnt_Pymnt_Dte = outfile.newFieldInGroup("outfile_Twrpymnt_Pymnt_Dte", "TWRPYMNT-PYMNT-DTE", FieldType.STRING, 8);
        outfile_Twrpymnt_Gross_Amt_Sign = outfile.newFieldInGroup("outfile_Twrpymnt_Gross_Amt_Sign", "TWRPYMNT-GROSS-AMT-SIGN", FieldType.STRING, 1);
        outfile_Twrpymnt_Gross_Amt = outfile.newFieldInGroup("outfile_Twrpymnt_Gross_Amt", "TWRPYMNT-GROSS-AMT", FieldType.NUMERIC, 11, 2);
        outfile_Twrpymnt_Pymnt_Status = outfile.newFieldInGroup("outfile_Twrpymnt_Pymnt_Status", "TWRPYMNT-PYMNT-STATUS", FieldType.STRING, 1);
        outfile_Twrpymnt_Int_Amt_Sign = outfile.newFieldInGroup("outfile_Twrpymnt_Int_Amt_Sign", "TWRPYMNT-INT-AMT-SIGN", FieldType.STRING, 1);
        outfile_Twrpymnt_Int_Amt = outfile.newFieldInGroup("outfile_Twrpymnt_Int_Amt", "TWRPYMNT-INT-AMT", FieldType.NUMERIC, 9, 2);
        outfile_Twrpymnt_Orgn_Srce_Cde = outfile.newFieldInGroup("outfile_Twrpymnt_Orgn_Srce_Cde", "TWRPYMNT-ORGN-SRCE-CDE", FieldType.STRING, 2);
        outfile_Twrpymnt_Contract_Type = outfile.newFieldInGroup("outfile_Twrpymnt_Contract_Type", "TWRPYMNT-CONTRACT-TYPE", FieldType.STRING, 5);
        outfile_Twrpymnt_Payment_Type = outfile.newFieldInGroup("outfile_Twrpymnt_Payment_Type", "TWRPYMNT-PAYMENT-TYPE", FieldType.STRING, 1);
        outfile_Twrpymnt_Settle_Type = outfile.newFieldInGroup("outfile_Twrpymnt_Settle_Type", "TWRPYMNT-SETTLE-TYPE", FieldType.STRING, 1);
        outfile_Twrpymnt_Pymnt_Refer_Nbr = outfile.newFieldInGroup("outfile_Twrpymnt_Pymnt_Refer_Nbr", "TWRPYMNT-PYMNT-REFER-NBR", FieldType.STRING, 2);
        outfile_Income_Ind = outfile.newFieldInGroup("outfile_Income_Ind", "INCOME-IND", FieldType.STRING, 1);

        cntlfile = localVariables.newGroupInRecord("cntlfile", "CNTLFILE");
        cntlfile_To_Date = cntlfile.newFieldInGroup("cntlfile_To_Date", "TO-DATE", FieldType.STRING, 8);
        cntlfile_From_Date = cntlfile.newFieldInGroup("cntlfile_From_Date", "FROM-DATE", FieldType.STRING, 8);
        cntlfile_Num_Of_Records = cntlfile.newFieldInGroup("cntlfile_Num_Of_Records", "NUM-OF-RECORDS", FieldType.NUMERIC, 10);
        pnd_Mask_Date = localVariables.newFieldInRecord("pnd_Mask_Date", "#MASK-DATE", FieldType.DATE);
        pnd_Cur_Year = localVariables.newFieldInRecord("pnd_Cur_Year", "#CUR-YEAR", FieldType.STRING, 4);

        pnd_Cur_Year__R_Field_20 = localVariables.newGroupInRecord("pnd_Cur_Year__R_Field_20", "REDEFINE", pnd_Cur_Year);
        pnd_Cur_Year_Pnd_Cur_Year_N = pnd_Cur_Year__R_Field_20.newFieldInGroup("pnd_Cur_Year_Pnd_Cur_Year_N", "#CUR-YEAR-N", FieldType.NUMERIC, 4);
        pnd_Pre_Year = localVariables.newFieldInRecord("pnd_Pre_Year", "#PRE-YEAR", FieldType.NUMERIC, 4);
        temp_Distribution_Cde = localVariables.newFieldInRecord("temp_Distribution_Cde", "TEMP-DISTRIBUTION-CDE", FieldType.STRING, 2);

        temp_Distribution_Cde__R_Field_21 = localVariables.newGroupInRecord("temp_Distribution_Cde__R_Field_21", "REDEFINE", temp_Distribution_Cde);
        temp_Distribution_Cde_Temp_Distribution_Cde_1 = temp_Distribution_Cde__R_Field_21.newFieldInGroup("temp_Distribution_Cde_Temp_Distribution_Cde_1", 
            "TEMP-DISTRIBUTION-CDE-1", FieldType.STRING, 1);
        temp_Distribution_Cde_Temp_Distribution_Cde_2 = temp_Distribution_Cde__R_Field_21.newFieldInGroup("temp_Distribution_Cde_Temp_Distribution_Cde_2", 
            "TEMP-DISTRIBUTION-CDE-2", FieldType.STRING, 1);
        pnd_Rt_Super2 = localVariables.newFieldInRecord("pnd_Rt_Super2", "#RT-SUPER2", FieldType.STRING, 66);

        pnd_Rt_Super2__R_Field_22 = localVariables.newGroupInRecord("pnd_Rt_Super2__R_Field_22", "REDEFINE", pnd_Rt_Super2);
        pnd_Rt_Super2_Pnd_Rt_A_I_Ind2 = pnd_Rt_Super2__R_Field_22.newFieldInGroup("pnd_Rt_Super2_Pnd_Rt_A_I_Ind2", "#RT-A-I-IND2", FieldType.STRING, 1);
        pnd_Rt_Super2_Pnd_Rt_Table_Id2 = pnd_Rt_Super2__R_Field_22.newFieldInGroup("pnd_Rt_Super2_Pnd_Rt_Table_Id2", "#RT-TABLE-ID2", FieldType.STRING, 
            5);
        pnd_Rt_Super2_Pnd_Rt_Short_Key2 = pnd_Rt_Super2__R_Field_22.newFieldInGroup("pnd_Rt_Super2_Pnd_Rt_Short_Key2", "#RT-SHORT-KEY2", FieldType.STRING, 
            20);
        pnd_Rt_Super2_Pnd_Rt_Long_Key2 = pnd_Rt_Super2__R_Field_22.newFieldInGroup("pnd_Rt_Super2_Pnd_Rt_Long_Key2", "#RT-LONG-KEY2", FieldType.STRING, 
            40);
        pnd_Short_Key_Value2 = localVariables.newFieldInRecord("pnd_Short_Key_Value2", "#SHORT-KEY-VALUE2", FieldType.STRING, 20);

        pnd_Short_Key_Value2__R_Field_23 = localVariables.newGroupInRecord("pnd_Short_Key_Value2__R_Field_23", "REDEFINE", pnd_Short_Key_Value2);

        pnd_Short_Key_Value2_Short_Key_Fields2 = pnd_Short_Key_Value2__R_Field_23.newGroupInGroup("pnd_Short_Key_Value2_Short_Key_Fields2", "SHORT-KEY-FIELDS2");
        pnd_Short_Key_Value2_Twrt_Tax_Id = pnd_Short_Key_Value2_Short_Key_Fields2.newFieldInGroup("pnd_Short_Key_Value2_Twrt_Tax_Id", "TWRT-TAX-ID", FieldType.STRING, 
            10);
        pnd_Short_Key_Value2_Pnd_Filler1 = pnd_Short_Key_Value2_Short_Key_Fields2.newFieldInGroup("pnd_Short_Key_Value2_Pnd_Filler1", "#FILLER1", FieldType.STRING, 
            1);
        pnd_Short_Key_Value2_Twrt_Index_Cntrct_Nbr = pnd_Short_Key_Value2_Short_Key_Fields2.newFieldInGroup("pnd_Short_Key_Value2_Twrt_Index_Cntrct_Nbr", 
            "TWRT-INDEX-CNTRCT-NBR", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_rt_Find.reset();
        vw_rt_Inst.reset();
        vw_rt_Updt.reset();

        ldaTwrl9415.initializeValues();
        ldaCpsl100.initializeValues();

        localVariables.reset();
        pnd_Rt_Super1.setInitialValue("  ");
        pnd_Rt_Super2.setInitialValue("  ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twup1053() throws Exception
    {
        super("Twup1053");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWUP1053", onError);
        setupReports();
        //*    START OF LOGIC   */                                                                                                                                        //Natural: ON ERROR
        pnd_Date_Start_D.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                                 //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #DATE-START-D
        pnd_Timex.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISS"));                                                                                          //Natural: MOVE EDITED *TIMX ( EM = HHIISS ) TO #TIMEX
        getReports().write(0, "$$$ = RUN START DATE: ",pnd_Date_Start_D_Pnd_Date_Start_Ccyy,"/",pnd_Date_Start_D_Pnd_Date_Start_Mm,"/",pnd_Date_Start_D_Pnd_Date_Start_Dd); //Natural: WRITE '$$$ = RUN START DATE: ' #DATE-START-CCYY '/' #DATE-START-MM '/' #DATE-START-DD
        if (Global.isEscape()) return;
        getReports().write(0, "$$$ = RUN START TIME: ",pnd_Timex_Pnd_Timex_Hh,":",pnd_Timex_Pnd_Timex_Ii,":",pnd_Timex_Pnd_Timex_Ss);                                     //Natural: WRITE '$$$ = RUN START TIME: ' #TIMEX-HH ':' #TIMEX-II ':' #TIMEX-SS
        if (Global.isEscape()) return;
        pnd_Cur_Year.setValue(pnd_Date_Start_D_Pnd_Date_Start_Ccyy);                                                                                                      //Natural: ASSIGN #CUR-YEAR := #DATE-START-CCYY
        pnd_Pre_Year.compute(new ComputeParameters(false, pnd_Pre_Year), pnd_Cur_Year_Pnd_Cur_Year_N.subtract(1));                                                        //Natural: ASSIGN #PRE-YEAR := #CUR-YEAR-N - 1
                                                                                                                                                                          //Natural: PERFORM READ-CONTROL-CARD
        sub_Read_Control_Card();
        if (condition(Global.isEscape())) {return;}
        pnd_Date_Start_Pnd_Date_Start_A.setValue(pnd_Control_Parm_Pnd_Control_Parm_Date);                                                                                 //Natural: ASSIGN #DATE-START-A := #CONTROL-PARM-DATE
        pnd_Time_Start.setValue(pnd_Control_Parm_Pnd_Control_Parm_Time);                                                                                                  //Natural: ASSIGN #TIME-START := #CONTROL-PARM-TIME
        pnd_Date_End_Pnd_Date_End_A.setValue(pnd_Date_Start_D);                                                                                                           //Natural: ASSIGN #DATE-END-A := #DATE-START-D
        pnd_Time_End.setValue(pnd_Timex);                                                                                                                                 //Natural: ASSIGN #TIME-END := #TIMEX
        getReports().write(0, "$$$ DATA WILL BE PROCESSED FROM: ",pnd_Date_Start_Pnd_Date_Strt_Ccyy,"/",pnd_Date_Start_Pnd_Date_Strt_Mm,"/",pnd_Date_Start_Pnd_Date_Strt_Dd, //Natural: WRITE '$$$ DATA WILL BE PROCESSED FROM: ' #DATE-STRT-CCYY '/' #DATE-STRT-MM '/' #DATE-STRT-DD #TIME-START-HH ':' #TIME-START-II ':' #TIME-START-SS
            pnd_Time_Start_Pnd_Time_Start_Hh,":",pnd_Time_Start_Pnd_Time_Start_Ii,":",pnd_Time_Start_Pnd_Time_Start_Ss);
        if (Global.isEscape()) return;
        getReports().write(0, "$$$                          TO: ",pnd_Date_End_Pnd_Date_End_Ccyy,"/",pnd_Date_End_Pnd_Date_End_Mm,"/",pnd_Date_End_Pnd_Date_End_Dd,       //Natural: WRITE '$$$                          TO: ' #DATE-END-CCYY '/' #DATE-END-MM '/' #DATE-END-DD #TIME-END-HH ':' #TIME-END-II ':' #TIME-END-SS
            pnd_Time_End_Pnd_Time_End_Hh,":",pnd_Time_End_Pnd_Time_End_Ii,":",pnd_Time_End_Pnd_Time_End_Ss);
        if (Global.isEscape()) return;
        //*  CHECK IF START AND END DATES ARE SAME */
        if (condition(pnd_Date_Start_Pnd_Date_Start_A.equals(pnd_Date_End_Pnd_Date_End_A)))                                                                               //Natural: IF #DATE-START-A = #DATE-END-A
        {
            getReports().write(0, "!!!               ESCAPE RUN            !!!");                                                                                         //Natural: WRITE '!!!               ESCAPE RUN            !!!'
            if (Global.isEscape()) return;
            getReports().write(0, "!!! JOB CANNOT RUN TWICE IN A SAME DATE !!!");                                                                                         //Natural: WRITE '!!! JOB CANNOT RUN TWICE IN A SAME DATE !!!'
            if (Global.isEscape()) return;
            getReports().write(0, "!!!               ESCAPE RUN (CC=30)    !!!");                                                                                         //Natural: WRITE '!!!               ESCAPE RUN (CC=30)    !!!'
            if (Global.isEscape()) return;
            DbsUtil.terminate(30);  if (true) return;                                                                                                                     //Natural: TERMINATE 30
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "!!! CONTROL CARD VALUE GOOD. START RUN. !!!");                                                                                         //Natural: WRITE '!!! CONTROL CARD VALUE GOOD. START RUN. !!!'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Twrpymnt_Interface_Date_User_S_Pnd_Twrpymnt_Dist_Updte_Dte_S.setValue(pnd_Date_Start_Pnd_Date_Start_A);                                                       //Natural: ASSIGN #TWRPYMNT-DIST-UPDTE-DTE-S := #DATE-START-A
        pnd_Twrpymnt_Interface_Date_User_E_Pnd_Twrpymnt_Dist_Updte_Dte_E.setValue(pnd_Date_End_Pnd_Date_End_A);                                                           //Natural: ASSIGN #TWRPYMNT-DIST-UPDTE-DTE-E := #DATE-END-A
        pnd_Twrpymnt_Interface_Date_User_S_Pnd_Twrpymnt_Dist_Updte_User_S.setValue(" ");                                                                                  //Natural: ASSIGN #TWRPYMNT-DIST-UPDTE-USER-S := ' '
        pnd_Twrpymnt_Interface_Date_User_E_Pnd_Twrpymnt_Dist_Updte_User_E.setValue(" ");                                                                                  //Natural: ASSIGN #TWRPYMNT-DIST-UPDTE-USER-E := ' '
        //* ************************************
        //*  READ PAYMENT FILE USING LU DATE
        //* ************************************
        ldaTwrl9415.getVw_pay().startDatabaseRead                                                                                                                         //Natural: READ PAY BY TWRPYMNT-INTERFACE-DATE-USER = #TWRPYMNT-INTERFACE-DATE-USER-S THRU #TWRPYMNT-INTERFACE-DATE-USER-E
        (
        "R1",
        new Wc[] { new Wc("TWRPYMNT_INTERFACE_DATE_USER", ">=", pnd_Twrpymnt_Interface_Date_User_S, "And", WcType.BY) ,
        new Wc("TWRPYMNT_INTERFACE_DATE_USER", "<=", pnd_Twrpymnt_Interface_Date_User_E, WcType.BY) },
        new Oc[] { new Oc("TWRPYMNT_INTERFACE_DATE_USER", "ASC") }
        );
        R1:
        while (condition(ldaTwrl9415.getVw_pay().readNextRow("R1")))
        {
            if (condition(ldaTwrl9415.getPay_Twrpymnt_Updt_Dte().equals(pnd_Date_Start_Pnd_Date_Start_A) && ldaTwrl9415.getPay_Twrpymnt_Updt_Time().less(pnd_Time_Start))) //Natural: IF TWRPYMNT-UPDT-DTE EQ #DATE-START-A AND PAY.TWRPYMNT-UPDT-TIME LT #TIME-START
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl9415.getPay_Twrpymnt_Updt_Dte().equals(pnd_Date_End_Pnd_Date_End_A) && ldaTwrl9415.getPay_Twrpymnt_Updt_Time().greaterOrEqual(pnd_Time_End))) //Natural: IF TWRPYMNT-UPDT-DTE EQ #DATE-END-A AND PAY.TWRPYMNT-UPDT-TIME GE #TIME-END
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            outfile.reset();                                                                                                                                              //Natural: RESET OUTFILE
            outfile_Twrpymnt_Tax_Year.setValue(ldaTwrl9415.getPay_Twrpymnt_Tax_Year());                                                                                   //Natural: ASSIGN OUTFILE.TWRPYMNT-TAX-YEAR := PAY.TWRPYMNT-TAX-YEAR
            outfile_Ws_Tax_Year.setValue(ldaTwrl9415.getPay_Twrpymnt_Tax_Year());                                                                                         //Natural: ASSIGN OUTFILE.WS-TAX-YEAR := PAY.TWRPYMNT-TAX-YEAR
            if (condition((outfile_Twrpymnt_Tax_Year.equals(pnd_Cur_Year_Pnd_Cur_Year_N) || outfile_Twrpymnt_Tax_Year.equals(pnd_Pre_Year))))                             //Natural: IF ( OUTFILE.TWRPYMNT-TAX-YEAR = #CUR-YEAR-N OR OUTFILE.TWRPYMNT-TAX-YEAR = #PRE-YEAR )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            outfile_Twrpymnt_Tax_Id_Nbr.setValue(ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr());                                                                               //Natural: ASSIGN OUTFILE.TWRPYMNT-TAX-ID-NBR := PAY.TWRPYMNT-TAX-ID-NBR
            outfile_Ws_Tax_Id_Nbr.setValue(ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr());                                                                                     //Natural: ASSIGN OUTFILE.WS-TAX-ID-NBR := PAY.TWRPYMNT-TAX-ID-NBR
            outfile_Twrpymnt_Contract_Nbr.setValue(ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr());                                                                           //Natural: ASSIGN OUTFILE.TWRPYMNT-CONTRACT-NBR := PAY.TWRPYMNT-CONTRACT-NBR
            outfile_Ws_Contract_Nbr.setValue(ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr());                                                                                 //Natural: ASSIGN OUTFILE.WS-CONTRACT-NBR := PAY.TWRPYMNT-CONTRACT-NBR
            //* * *** ADD OMNI COMPARE TO REPLACE INDEX WITH TIAA-CONTRACT **
            if (condition(ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde().getValue(1).equals("OP") || ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde().getValue(1).equals("  ")))  //Natural: IF PAY.TWRPYMNT-ORGN-SRCE-CDE ( 1 ) = 'OP' OR = '  '
            {
                //*  INCOME-SUMMARY
                                                                                                                                                                          //Natural: PERFORM COMPARE-OMNIPAY-DATA
                sub_Compare_Omnipay_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //* ** END ***************
            outfile_Twrpymnt_Payee_Cde.setValue(ldaTwrl9415.getPay_Twrpymnt_Payee_Cde());                                                                                 //Natural: ASSIGN OUTFILE.TWRPYMNT-PAYEE-CDE := PAY.TWRPYMNT-PAYEE-CDE
            outfile_Ws_Payee_Cde.setValue(ldaTwrl9415.getPay_Twrpymnt_Payee_Cde());                                                                                       //Natural: ASSIGN OUTFILE.WS-PAYEE-CDE := PAY.TWRPYMNT-PAYEE-CDE
            outfile_Twrpymnt_Tax_Citizenship.setValue(ldaTwrl9415.getPay_Twrpymnt_Tax_Citizenship());                                                                     //Natural: ASSIGN OUTFILE.TWRPYMNT-TAX-CITIZENSHIP := PAY.TWRPYMNT-TAX-CITIZENSHIP
            outfile_Twrpymnt_Citizen_Cde.setValue(ldaTwrl9415.getPay_Twrpymnt_Citizen_Cde());                                                                             //Natural: ASSIGN OUTFILE.TWRPYMNT-CITIZEN-CDE := PAY.TWRPYMNT-CITIZEN-CDE
            outfile_Twrpymnt_Paymt_Category.setValue(ldaTwrl9415.getPay_Twrpymnt_Paymt_Category());                                                                       //Natural: ASSIGN OUTFILE.TWRPYMNT-PAYMT-CATEGORY := PAY.TWRPYMNT-PAYMT-CATEGORY
            //* * USE NEW ROUTINE TO CONVERT DISTRIBUTION CODE
            temp_Distribution_Cde.setValue(ldaTwrl9415.getPay_Twrpymnt_Distribution_Cde());                                                                               //Natural: ASSIGN TEMP-DISTRIBUTION-CDE := PAY.TWRPYMNT-DISTRIBUTION-CDE
            if (condition(temp_Distribution_Cde_Temp_Distribution_Cde_2.equals(" ")))                                                                                     //Natural: IF TEMP-DISTRIBUTION-CDE-2 = ' '
            {
                pdaTwradist.getPnd_Twradist_Pnd_Abend_Ind().reset();                                                                                                      //Natural: RESET #TWRADIST.#ABEND-IND #TWRADIST.#DISPLAY-IND
                pdaTwradist.getPnd_Twradist_Pnd_Display_Ind().reset();
                pdaTwradist.getPnd_Twradist_Pnd_Function().setValue("1");                                                                                                 //Natural: ASSIGN #TWRADIST.#FUNCTION := '1'
                pdaTwradist.getPnd_Twradist_Pnd_Tax_Year().setValue(ldaTwrl9415.getPay_Twrpymnt_Tax_Year());                                                              //Natural: ASSIGN #TWRADIST.#TAX-YEAR := PAY.TWRPYMNT-TAX-YEAR
                pdaTwradist.getPnd_Twradist_Pnd_In_Dist().setValue(ldaTwrl9415.getPay_Twrpymnt_Distribution_Cde());                                                       //Natural: ASSIGN #TWRADIST.#IN-DIST := PAY.TWRPYMNT-DISTRIBUTION-CDE
                DbsUtil.callnat(Twrndist.class , getCurrentProcessState(), pdaTwradist.getPnd_Twradist());                                                                //Natural: CALLNAT 'TWRNDIST' USING #TWRADIST
                if (condition(Global.isEscape())) return;
                outfile_Twrpymnt_Distribution_Cde.setValue(pdaTwradist.getPnd_Twradist_Pnd_Out_Dist());                                                                   //Natural: MOVE #TWRADIST.#OUT-DIST TO OUTFILE.TWRPYMNT-DISTRIBUTION-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                outfile_Twrpymnt_Distribution_Cde.setValue(ldaTwrl9415.getPay_Twrpymnt_Distribution_Cde());                                                               //Natural: MOVE PAY.TWRPYMNT-DISTRIBUTION-CDE TO OUTFILE.TWRPYMNT-DISTRIBUTION-CDE
            }                                                                                                                                                             //Natural: END-IF
            outfile_Twrpymnt_Company_Cde.setValue(ldaTwrl9415.getPay_Twrpymnt_Company_Cde());                                                                             //Natural: ASSIGN OUTFILE.TWRPYMNT-COMPANY-CDE := PAY.TWRPYMNT-COMPANY-CDE
            outfile_Twrpymnt_Residency_Type.setValue(ldaTwrl9415.getPay_Twrpymnt_Residency_Type());                                                                       //Natural: ASSIGN OUTFILE.TWRPYMNT-RESIDENCY-TYPE := PAY.TWRPYMNT-RESIDENCY-TYPE
            outfile_Twrpymnt_Residency_Code.setValue(ldaTwrl9415.getPay_Twrpymnt_Residency_Code());                                                                       //Natural: ASSIGN OUTFILE.TWRPYMNT-RESIDENCY-CODE := PAY.TWRPYMNT-RESIDENCY-CODE
            outfile_Twrpymnt_Locality_Cde.setValue(ldaTwrl9415.getPay_Twrpymnt_Locality_Cde());                                                                           //Natural: ASSIGN OUTFILE.TWRPYMNT-LOCALITY-CDE := PAY.TWRPYMNT-LOCALITY-CDE
            outfile_Twrpymnt_Contract_Seq_No.setValue(ldaTwrl9415.getPay_Twrpymnt_Contract_Seq_No());                                                                     //Natural: ASSIGN OUTFILE.TWRPYMNT-CONTRACT-SEQ-NO := PAY.TWRPYMNT-CONTRACT-SEQ-NO
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 C*TWRPYMNT-PAYMENTS
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaTwrl9415.getPay_Count_Casttwrpymnt_Payments())); pnd_I.nadd(1))
            {
                outfile_Seq_Num.setValue(pnd_I);                                                                                                                          //Natural: ASSIGN OUTFILE.SEQ-NUM := #I
                outfile_Twrpymnt_Gross_Amt.setValue(ldaTwrl9415.getPay_Twrpymnt_Gross_Amt().getValue(pnd_I));                                                             //Natural: ASSIGN OUTFILE.TWRPYMNT-GROSS-AMT := PAY.TWRPYMNT-GROSS-AMT ( #I )
                if (condition(outfile_Twrpymnt_Gross_Amt.less(getZero())))                                                                                                //Natural: IF OUTFILE.TWRPYMNT-GROSS-AMT LT 0
                {
                    outfile_Twrpymnt_Gross_Amt_Sign.setValue("-");                                                                                                        //Natural: ASSIGN OUTFILE.TWRPYMNT-GROSS-AMT-SIGN := '-'
                    outfile_Twrpymnt_Gross_Amt.compute(new ComputeParameters(false, outfile_Twrpymnt_Gross_Amt), outfile_Twrpymnt_Gross_Amt.multiply(-1));                //Natural: ASSIGN OUTFILE.TWRPYMNT-GROSS-AMT := OUTFILE.TWRPYMNT-GROSS-AMT * -1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    outfile_Twrpymnt_Gross_Amt_Sign.setValue(" ");                                                                                                        //Natural: ASSIGN OUTFILE.TWRPYMNT-GROSS-AMT-SIGN := ' '
                }                                                                                                                                                         //Natural: END-IF
                pnd_Mask_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaTwrl9415.getPay_Twrpymnt_Pymnt_Dte().getValue(pnd_I));                                     //Natural: MOVE EDITED PAY.TWRPYMNT-PYMNT-DTE ( #I ) TO #MASK-DATE ( EM = YYYYMMDD )
                outfile_Twrpymnt_Pymnt_Dte.setValueEdited(pnd_Mask_Date,new ReportEditMask("MM/DD/YY"));                                                                  //Natural: MOVE EDITED #MASK-DATE ( EM = MM/DD/YY ) TO OUTFILE.TWRPYMNT-PYMNT-DTE
                outfile_Twrpymnt_Pymnt_Status.setValue(ldaTwrl9415.getPay_Twrpymnt_Pymnt_Status().getValue(pnd_I));                                                       //Natural: ASSIGN OUTFILE.TWRPYMNT-PYMNT-STATUS := PAY.TWRPYMNT-PYMNT-STATUS ( #I )
                outfile_Twrpymnt_Int_Amt.setValue(ldaTwrl9415.getPay_Twrpymnt_Int_Amt().getValue(pnd_I));                                                                 //Natural: ASSIGN OUTFILE.TWRPYMNT-INT-AMT := PAY.TWRPYMNT-INT-AMT ( #I )
                if (condition(outfile_Twrpymnt_Int_Amt.less(getZero())))                                                                                                  //Natural: IF OUTFILE.TWRPYMNT-INT-AMT LT 0
                {
                    outfile_Twrpymnt_Int_Amt_Sign.setValue("-");                                                                                                          //Natural: ASSIGN OUTFILE.TWRPYMNT-INT-AMT-SIGN := '-'
                    outfile_Twrpymnt_Int_Amt.compute(new ComputeParameters(false, outfile_Twrpymnt_Int_Amt), outfile_Twrpymnt_Int_Amt.multiply(-1));                      //Natural: ASSIGN OUTFILE.TWRPYMNT-INT-AMT := OUTFILE.TWRPYMNT-INT-AMT * -1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    outfile_Twrpymnt_Int_Amt_Sign.setValue(" ");                                                                                                          //Natural: ASSIGN OUTFILE.TWRPYMNT-INT-AMT-SIGN := ' '
                }                                                                                                                                                         //Natural: END-IF
                outfile_Twrpymnt_Orgn_Srce_Cde.setValue(ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I));                                                     //Natural: ASSIGN OUTFILE.TWRPYMNT-ORGN-SRCE-CDE := PAY.TWRPYMNT-ORGN-SRCE-CDE ( #I )
                outfile_Twrpymnt_Contract_Type.setValue(ldaTwrl9415.getPay_Twrpymnt_Contract_Type().getValue(pnd_I));                                                     //Natural: ASSIGN OUTFILE.TWRPYMNT-CONTRACT-TYPE := PAY.TWRPYMNT-CONTRACT-TYPE ( #I )
                outfile_Twrpymnt_Payment_Type.setValue(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I));                                                       //Natural: ASSIGN OUTFILE.TWRPYMNT-PAYMENT-TYPE := PAY.TWRPYMNT-PAYMENT-TYPE ( #I )
                outfile_Twrpymnt_Settle_Type.setValue(ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I));                                                         //Natural: ASSIGN OUTFILE.TWRPYMNT-SETTLE-TYPE := PAY.TWRPYMNT-SETTLE-TYPE ( #I )
                outfile_Twrpymnt_Pymnt_Refer_Nbr.setValue(ldaTwrl9415.getPay_Twrpymnt_Pymnt_Refer_Nbr().getValue(pnd_I));                                                 //Natural: ASSIGN OUTFILE.TWRPYMNT-PYMNT-REFER-NBR := PAY.TWRPYMNT-PYMNT-REFER-NBR ( #I )
                outfile_Ref_Seq_Num_N.setValue(ldaTwrl9415.getPay_Twrpymnt_Pymnt_Refer_Nbr_Num().getValue(pnd_I));                                                        //Natural: ASSIGN OUTFILE.REF-SEQ-NUM-N := PAY.TWRPYMNT-PYMNT-REFER-NBR-NUM ( #I )
                short decideConditionsMet574 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN OUTFILE.REF-SEQ-NUM-B3 = '}'
                if (condition(outfile_Ref_Seq_Num_B3.equals("}")))
                {
                    decideConditionsMet574++;
                    outfile_Ref_Seq_Num_B1.setValue("-");                                                                                                                 //Natural: ASSIGN OUTFILE.REF-SEQ-NUM-B1 := '-'
                    outfile_Ref_Seq_Num_B3.setValue("0");                                                                                                                 //Natural: ASSIGN OUTFILE.REF-SEQ-NUM-B3 := '0'
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    outfile_Ref_Seq_Num_B1.setValue(" ");                                                                                                                 //Natural: ASSIGN OUTFILE.REF-SEQ-NUM-B1 := ' '
                }                                                                                                                                                         //Natural: END-DECIDE
                short decideConditionsMet581 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'A' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'U'
                if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("A") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("U")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'D' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'U'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("D") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("U")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'R' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'U'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("R") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("U")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'R' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'G'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("R") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("G")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'D' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'L'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("D") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("L")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'R' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'B'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("R") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("B")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'R' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'Q'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("R") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("Q")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'R' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'I'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("R") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("I")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'B' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'L'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("B") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("L")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'R' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'W'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("R") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("W")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'B' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'B'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("B") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("B")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'B' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'Q'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("B") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("Q")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'B' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'I'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("B") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("I")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'A' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'L'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("A") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("L")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'R' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'W'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("R") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("W")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'A' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'B'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("A") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("B")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'A' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'Q'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("A") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("Q")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'A' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'I'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("A") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("I")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'D' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'I'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("D") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("I")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'R' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'R'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("R") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("R")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'A' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'I'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("A") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("I")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'R' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'L'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("R") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("L")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'R' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'I'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("R") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("I")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'C' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'X'
                else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_I).equals("C") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_I).equals("X")))
                {
                    decideConditionsMet581++;
                    outfile_Income_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'N'
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    outfile_Income_Ind.setValue("Y");                                                                                                                     //Natural: ASSIGN OUTFILE.INCOME-IND := 'Y'
                }                                                                                                                                                         //Natural: END-DECIDE
                //*    DECIDE WHETHER RECORD IS INSERT RECORD OR UPDATE RECORD */
                                                                                                                                                                          //Natural: PERFORM DECIDE-INSERT-OR-UPDATE
                sub_Decide_Insert_Or_Update();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    WRITE DATA  */
                                                                                                                                                                          //Natural: PERFORM WRITE-DATA
                sub_Write_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ***************************************
                                                                                                                                                                          //Natural: PERFORM CREATE-CONTROL-FILES
        sub_Create_Control_Files();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-PARM-CARD
        sub_Update_Control_Parm_Card();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "!!! END OF EXTRACT RUN, RUN END STATS.. !!!");                                                                                             //Natural: WRITE '!!! END OF EXTRACT RUN, RUN END STATS.. !!!'
        if (Global.isEscape()) return;
        getReports().write(0, "INSERT FILE RECORD COUNT: ",pnd_Insert_Rcrd_Count);                                                                                        //Natural: WRITE 'INSERT FILE RECORD COUNT: ' #INSERT-RCRD-COUNT
        if (Global.isEscape()) return;
        getReports().write(0, "UPDATE FILE RECORD COUNT: ",pnd_Update_Rcrd_Count);                                                                                        //Natural: WRITE 'UPDATE FILE RECORD COUNT: ' #UPDATE-RCRD-COUNT
        if (Global.isEscape()) return;
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CONTROL-CARD
        //* ************************************
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CONTROL-PARM-CARD
        //* ***************************************
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DATA
        //* ***************************************
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DECIDE-INSERT-OR-UPDATE
        //* ***************************************
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INSERT-REF-TABLE
        //* ***************************************
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-REF-TABLE
        //* ***************************************
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CONTROL-FILES
        //* ***************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPARE-OMNIPAY-DATA
        //* *READ WORK FILE 07 #OMNI-PAY-FILE
        //* *  IF TWRT-TAX-ID  GT     OUTFILE.TWRPYMNT-TAX-ID-NBR
        //* *    ESCAPE BOTTOM
        //* *  ELSE
        //* *    IF  TWRT-TAX-ID  EQ     OUTFILE.TWRPYMNT-TAX-ID-NBR
        //* *        AND OUTFILE.TWRPYMNT-CONTRACT-NBR EQ   TWRT-INDEX-CNTRCT-NBR
        //* *      OUTFILE.TWRPYMNT-CONTRACT-NBR := TWRT-TIAA-CNTRCT-NBR
        //* *      OUTFILE.WS-CONTRACT-NBR      := TWRT-TIAA-CNTRCT-NBR
        //* *      DISPLAY 'TAX-SSN'  OUTFILE.TWRPYMNT-TAX-ID-NBR
        //* *        'TIAA CNTRCT'  TWRT-TIAA-CNTRCT-NBR
        //* *        'INDEX CNTRCT' TWRT-INDEX-CNTRCT-NBR
        //* *        'PAY-UPDT-DATE' TWRPYMNT-UPDT-DTE
        //* *      ESCAPE BOTTOM
        //* *    END-IF
        //* *  END-IF
        //* *END-WORK
        //* *CLOSE WORK FILE 07
        //* *END-SUBROUTINE                        /* COMPARE-OMNIPAY-DATA */
        //* ***************************************
    }
    private void sub_Read_Control_Card() throws Exception                                                                                                                 //Natural: READ-CONTROL-CARD
    {
        if (BLNatReinput.isReinput()) return;

        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #CONTROL-PARM
        while (condition(getWorkFiles().read(1, pnd_Control_Parm)))
        {
            getReports().write(0, "*** CONTROL PARM VALUE: ",pnd_Control_Parm);                                                                                           //Natural: WRITE '*** CONTROL PARM VALUE: ' #CONTROL-PARM
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Control_Parm_Pnd_Control_Parm_Date.equals(" ")))                                                                                            //Natural: IF #CONTROL-PARM-DATE = ' '
            {
                getReports().write(0, "!!!        ERROR READING CONTROL PARM     !!!");                                                                                   //Natural: WRITE '!!!        ERROR READING CONTROL PARM     !!!'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "!!! CONTROL-PARM-DATE IS EMPTY, STOP RUN. !!!");                                                                                   //Natural: WRITE '!!! CONTROL-PARM-DATE IS EMPTY, STOP RUN. !!!'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "!!!     ERROR READING CONTROL PARM (CC=10)!!!");                                                                                   //Natural: WRITE '!!!     ERROR READING CONTROL PARM (CC=10)!!!'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(10);  if (true) return;                                                                                                                 //Natural: TERMINATE 10
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Control_Parm_Pnd_Control_Parm_Time.equals(" ")))                                                                                            //Natural: IF #CONTROL-PARM-TIME = ' '
            {
                getReports().write(0, "!!!        ERROR READING CONTROL PARM     !!!");                                                                                   //Natural: WRITE '!!!        ERROR READING CONTROL PARM     !!!'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "!!! CONTROL-PARM-TIME IS EMPTY, STOP RUN. !!!");                                                                                   //Natural: WRITE '!!! CONTROL-PARM-TIME IS EMPTY, STOP RUN. !!!'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "!!!     ERROR READING CONTROL PARM (CC=10)!!!");                                                                                   //Natural: WRITE '!!!     ERROR READING CONTROL PARM (CC=10)!!!'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(10);  if (true) return;                                                                                                                 //Natural: TERMINATE 10
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  READ-CONTROL-CARD */
    }
    private void sub_Update_Control_Parm_Card() throws Exception                                                                                                          //Natural: UPDATE-CONTROL-PARM-CARD
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(0, " *** UPDATE CONTROL CARD AT END OF RUN *** ");                                                                                             //Natural: WRITE ' *** UPDATE CONTROL CARD AT END OF RUN *** '
        if (Global.isEscape()) return;
        pnd_Control_Parm_Pnd_Control_Parm_Date.setValue(pnd_Date_Start_D);                                                                                                //Natural: ASSIGN #CONTROL-PARM-DATE := #DATE-START-D
        pnd_Control_Parm_Pnd_Control_Parm_Time.setValue(pnd_Timex);                                                                                                       //Natural: ASSIGN #CONTROL-PARM-TIME := #TIMEX
        getReports().write(0, " *** NEW CONTROL PARM VALUE: ",pnd_Control_Parm," *** ");                                                                                  //Natural: WRITE ' *** NEW CONTROL PARM VALUE: ' #CONTROL-PARM ' *** '
        if (Global.isEscape()) return;
        getWorkFiles().write(4, false, pnd_Control_Parm);                                                                                                                 //Natural: WRITE WORK FILE 4 #CONTROL-PARM
        //*  UPDATE-CONTROL-PARM-CARD */
    }
    private void sub_Write_Data() throws Exception                                                                                                                        //Natural: WRITE-DATA
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Insert_Or_Update.equals("I")))                                                                                                                  //Natural: IF #INSERT-OR-UPDATE = 'I'
        {
            getWorkFiles().write(2, false, outfile);                                                                                                                      //Natural: WRITE WORK FILE 2 OUTFILE
            pnd_Insert_Rcrd_Count.nadd(1);                                                                                                                                //Natural: ASSIGN #INSERT-RCRD-COUNT := #INSERT-RCRD-COUNT + 1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Insert_Or_Update.equals("U")))                                                                                                              //Natural: IF #INSERT-OR-UPDATE = 'U'
            {
                getWorkFiles().write(3, false, outfile);                                                                                                                  //Natural: WRITE WORK FILE 3 OUTFILE
                pnd_Update_Rcrd_Count.nadd(1);                                                                                                                            //Natural: ASSIGN #UPDATE-RCRD-COUNT := #UPDATE-RCRD-COUNT + 1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "!!!       RECORD WRITE ERROR       !!!");                                                                                          //Natural: WRITE '!!!       RECORD WRITE ERROR       !!!'
                if (Global.isEscape()) return;
                getReports().write(0, "!!! RECORD IS NOT UPDATE OR INSERT !!!");                                                                                          //Natural: WRITE '!!! RECORD IS NOT UPDATE OR INSERT !!!'
                if (Global.isEscape()) return;
                getReports().write(0, "!!!       RECORD WRITE ERROR(CC=40)!!!");                                                                                          //Natural: WRITE '!!!       RECORD WRITE ERROR(CC=40)!!!'
                if (Global.isEscape()) return;
                DbsUtil.terminate(40);  if (true) return;                                                                                                                 //Natural: TERMINATE 40
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-DATA */
    }
    private void sub_Decide_Insert_Or_Update() throws Exception                                                                                                           //Natural: DECIDE-INSERT-OR-UPDATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Short_Key_Value_Short_Key_Fields.setValuesByName(outfile);                                                                                                    //Natural: MOVE BY NAME OUTFILE TO SHORT-KEY-FIELDS
        pnd_Long_Key_Value_Long_Key_Fields.setValuesByName(outfile);                                                                                                      //Natural: MOVE BY NAME OUTFILE TO LONG-KEY-FIELDS
        pnd_Rt_Super1_Pnd_Rt_A_I_Ind.setValue("A");                                                                                                                       //Natural: ASSIGN #RT-A-I-IND := 'A'
        pnd_Rt_Super1_Pnd_Rt_Table_Id.setValue("10000");                                                                                                                  //Natural: ASSIGN #RT-TABLE-ID := '10000'
        pnd_Rt_Super1_Pnd_Rt_Short_Key.setValue(pnd_Short_Key_Value);                                                                                                     //Natural: ASSIGN #RT-SHORT-KEY := #SHORT-KEY-VALUE
        pnd_Rt_Super1_Pnd_Rt_Long_Key.setValue(pnd_Long_Key_Value);                                                                                                       //Natural: ASSIGN #RT-LONG-KEY := #LONG-KEY-VALUE
        vw_rt_Find.startDatabaseFind                                                                                                                                      //Natural: FIND RT-FIND WITH RT-SUPER1 = #RT-SUPER1
        (
        "F1",
        new Wc[] { new Wc("RT_SUPER1", "=", pnd_Rt_Super1, WcType.WITH) }
        );
        F1:
        while (condition(vw_rt_Find.readNextRow("F1", true)))
        {
            vw_rt_Find.setIfNotFoundControlFlag(false);
            if (condition(vw_rt_Find.getAstCOUNTER().equals(0)))                                                                                                          //Natural: IF NO RECORDS FOUND
            {
                pnd_Insert_Or_Update.setValue("I");                                                                                                                       //Natural: ASSIGN #INSERT-OR-UPDATE := 'I'
                                                                                                                                                                          //Natural: PERFORM INSERT-REF-TABLE
                sub_Insert_Ref_Table();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Insert_Or_Update.setValue("U");                                                                                                                           //Natural: ASSIGN #INSERT-OR-UPDATE := 'U'
            pnd_Isn.setValue(vw_rt_Find.getAstISN("F1"));                                                                                                                 //Natural: ASSIGN #ISN := *ISN ( F1. )
                                                                                                                                                                          //Natural: PERFORM UPDATE-REF-TABLE
            sub_Update_Ref_Table();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  DECIDE-INSERT-OR-UPDATE */
    }
    private void sub_Insert_Ref_Table() throws Exception                                                                                                                  //Natural: INSERT-REF-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        rt_Inst_Rt_A_I_Ind.setValue(pnd_Rt_Super1_Pnd_Rt_A_I_Ind);                                                                                                        //Natural: ASSIGN RT-INST.RT-A-I-IND := #RT-A-I-IND
        rt_Inst_Rt_Table_Id.setValue(pnd_Rt_Super1_Pnd_Rt_Table_Id);                                                                                                      //Natural: ASSIGN RT-INST.RT-TABLE-ID := #RT-TABLE-ID
        rt_Inst_Rt_Short_Key.setValue(pnd_Short_Key_Value);                                                                                                               //Natural: ASSIGN RT-INST.RT-SHORT-KEY := #SHORT-KEY-VALUE
        rt_Inst_Rt_Long_Key.setValue(pnd_Long_Key_Value);                                                                                                                 //Natural: ASSIGN RT-INST.RT-LONG-KEY := #LONG-KEY-VALUE
        rt_Inst_Rt_Upd_Source.setValue(Global.getPROGRAM());                                                                                                              //Natural: ASSIGN RT-INST.RT-UPD-SOURCE := *PROGRAM
        rt_Inst_Rt_Upd_User.setValue(Global.getPROGRAM());                                                                                                                //Natural: ASSIGN RT-INST.RT-UPD-USER := *PROGRAM
        rt_Inst_Rt_Upd_Ccyymmdd.setValue(pnd_Date_Start_D);                                                                                                               //Natural: ASSIGN RT-INST.RT-UPD-CCYYMMDD := #DATE-START-D
        rt_Inst_Rt_Upd_Timn.setValue(pnd_Timex_Pnd_Timen);                                                                                                                //Natural: ASSIGN RT-INST.RT-UPD-TIMN := #TIMEN
        pnd_Date_Start_D_2y_Pnd_Date_St_2y_Ccyy.setValue(pnd_Date_Start_D_Pnd_Date_Start_Ccyy);                                                                           //Natural: ASSIGN #DATE-ST-2Y-CCYY := #DATE-START-CCYY
        pnd_Date_Start_D_2y_Pnd_Date_St_2y_Ccyy_N.nadd(2);                                                                                                                //Natural: ASSIGN #DATE-ST-2Y-CCYY-N := #DATE-ST-2Y-CCYY-N + 2
        pnd_Date_Start_D_2y_Pnd_Date_St_2y_Mm.setValue(pnd_Date_Start_D_Pnd_Date_Start_Mm);                                                                               //Natural: ASSIGN #DATE-ST-2Y-MM := #DATE-START-MM
        pnd_Date_Start_D_2y_Pnd_Date_St_2y_Dd.setValue(pnd_Date_Start_D_Pnd_Date_Start_Dd);                                                                               //Natural: ASSIGN #DATE-ST-2Y-DD := #DATE-START-DD
        rt_Inst_Rt_Eff_From_Ccyymmdd.setValue(pnd_Date_Start_D);                                                                                                          //Natural: ASSIGN RT-INST.RT-EFF-FROM-CCYYMMDD := #DATE-START-D
        rt_Inst_Rt_Eff_To_Ccyymmdd.setValue(pnd_Date_Start_D_2y);                                                                                                         //Natural: ASSIGN RT-INST.RT-EFF-TO-CCYYMMDD := #DATE-START-D-2Y
        vw_rt_Inst.insertDBRow();                                                                                                                                         //Natural: STORE RT-INST
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  INSERT-REF-TABLE */
    }
    private void sub_Update_Ref_Table() throws Exception                                                                                                                  //Natural: UPDATE-REF-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        GET_FOR_MODIFY:                                                                                                                                                   //Natural: GET RT-UPDT #ISN
        vw_rt_Updt.readByID(pnd_Isn.getLong(), "GET_FOR_MODIFY");
        pnd_Date_Start_D_2y_Pnd_Date_St_2y_Ccyy.setValue(pnd_Date_Start_D_Pnd_Date_Start_Ccyy);                                                                           //Natural: ASSIGN #DATE-ST-2Y-CCYY := #DATE-START-CCYY
        pnd_Date_Start_D_2y_Pnd_Date_St_2y_Ccyy_N.nadd(2);                                                                                                                //Natural: ASSIGN #DATE-ST-2Y-CCYY-N := #DATE-ST-2Y-CCYY-N + 2
        pnd_Date_Start_D_2y_Pnd_Date_St_2y_Mm.setValue(pnd_Date_Start_D_Pnd_Date_Start_Mm);                                                                               //Natural: ASSIGN #DATE-ST-2Y-MM := #DATE-START-MM
        pnd_Date_Start_D_2y_Pnd_Date_St_2y_Dd.setValue(pnd_Date_Start_D_Pnd_Date_Start_Dd);                                                                               //Natural: ASSIGN #DATE-ST-2Y-DD := #DATE-START-DD
        rt_Updt_Rt_Eff_From_Ccyymmdd.setValue(pnd_Date_Start_D);                                                                                                          //Natural: ASSIGN RT-UPDT.RT-EFF-FROM-CCYYMMDD := #DATE-START-D
        rt_Updt_Rt_Eff_To_Ccyymmdd.setValue(pnd_Date_Start_D_2y);                                                                                                         //Natural: ASSIGN RT-UPDT.RT-EFF-TO-CCYYMMDD := #DATE-START-D-2Y
        vw_rt_Updt.updateDBRow("GET_FOR_MODIFY");                                                                                                                         //Natural: UPDATE ( GET-FOR-MODIFY. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  UPDATE-REF-TABLE */
    }
    private void sub_Create_Control_Files() throws Exception                                                                                                              //Natural: CREATE-CONTROL-FILES
    {
        if (BLNatReinput.isReinput()) return;

        cntlfile_To_Date.setValue(pnd_Date_End_Pnd_Date_End_A);                                                                                                           //Natural: ASSIGN TO-DATE := #DATE-END-A
        cntlfile_From_Date.setValue(pnd_Date_Start_Pnd_Date_Start_A);                                                                                                     //Natural: ASSIGN FROM-DATE := #DATE-START-A
        cntlfile_Num_Of_Records.setValue(pnd_Insert_Rcrd_Count);                                                                                                          //Natural: ASSIGN NUM-OF-RECORDS := #INSERT-RCRD-COUNT
        getWorkFiles().write(5, false, cntlfile);                                                                                                                         //Natural: WRITE WORK FILE 5 CNTLFILE
        cntlfile_To_Date.setValue(pnd_Date_End_Pnd_Date_End_A);                                                                                                           //Natural: ASSIGN TO-DATE := #DATE-END-A
        cntlfile_From_Date.setValue(pnd_Date_Start_Pnd_Date_Start_A);                                                                                                     //Natural: ASSIGN FROM-DATE := #DATE-START-A
        cntlfile_Num_Of_Records.setValue(pnd_Update_Rcrd_Count);                                                                                                          //Natural: ASSIGN NUM-OF-RECORDS := #UPDATE-RCRD-COUNT
        getWorkFiles().write(6, false, cntlfile);                                                                                                                         //Natural: WRITE WORK FILE 6 CNTLFILE
        //*  CREATE-CONTROL-FILES */
    }
    //*  INCOME-SUMMARY
    private void sub_Compare_Omnipay_Data() throws Exception                                                                                                              //Natural: COMPARE-OMNIPAY-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Rt_Super2.reset();                                                                                                                                            //Natural: RESET #RT-SUPER2 #SHORT-KEY-VALUE2
        pnd_Short_Key_Value2.reset();
        pnd_Short_Key_Value2_Twrt_Tax_Id.setValue(outfile_Twrpymnt_Tax_Id_Nbr);                                                                                           //Natural: ASSIGN #SHORT-KEY-VALUE2.TWRT-TAX-ID := OUTFILE.TWRPYMNT-TAX-ID-NBR
        pnd_Short_Key_Value2_Twrt_Index_Cntrct_Nbr.setValue(outfile_Twrpymnt_Contract_Nbr);                                                                               //Natural: ASSIGN #SHORT-KEY-VALUE2.TWRT-INDEX-CNTRCT-NBR := OUTFILE.TWRPYMNT-CONTRACT-NBR
        pnd_Rt_Super2_Pnd_Rt_A_I_Ind2.setValue("A");                                                                                                                      //Natural: ASSIGN #RT-A-I-IND2 := 'A'
        pnd_Rt_Super2_Pnd_Rt_Table_Id2.setValue("20000");                                                                                                                 //Natural: ASSIGN #RT-TABLE-ID2 := '20000'
        pnd_Rt_Super2_Pnd_Rt_Short_Key2.setValue(pnd_Short_Key_Value2);                                                                                                   //Natural: ASSIGN #RT-SHORT-KEY2 := #SHORT-KEY-VALUE2
        ldaCpsl100.getVw_rt().startDatabaseRead                                                                                                                           //Natural: READ RT WITH RT-SUPER1 STARTING FROM #RT-SUPER2
        (
        "SV1",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Rt_Super2, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") }
        );
        SV1:
        while (condition(ldaCpsl100.getVw_rt().readNextRow("SV1")))
        {
            if (condition(pnd_Rt_Super2_Pnd_Rt_A_I_Ind2.notEquals(ldaCpsl100.getRt_Rt_A_I_Ind()) || pnd_Rt_Super2_Pnd_Rt_Table_Id2.notEquals(ldaCpsl100.getRt_Rt_Table_Id())  //Natural: IF #RT-A-I-IND2 NE RT-A-I-IND OR #RT-TABLE-ID2 NE RT-TABLE-ID OR #RT-SHORT-KEY2 NE RT-SHORT-KEY
                || pnd_Rt_Super2_Pnd_Rt_Short_Key2.notEquals(ldaCpsl100.getRt_Rt_Short_Key())))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Long_Key_Value_Long_Key_Fields.reset();                                                                                                                   //Natural: RESET LONG-KEY-FIELDS
            outfile_Twrpymnt_Contract_Nbr.setValue(ldaCpsl100.getRt_Rt_Long_Key().getSubstring(1,8));                                                                     //Natural: ASSIGN OUTFILE.TWRPYMNT-CONTRACT-NBR := SUBSTR ( RT-LONG-KEY,1,8 )
            outfile_Ws_Contract_Nbr.setValue(ldaCpsl100.getRt_Rt_Long_Key().getSubstring(1,8));                                                                           //Natural: ASSIGN OUTFILE.WS-CONTRACT-NBR := SUBSTR ( RT-LONG-KEY,1,8 )
            getReports().display(0, "TAX-SSN",                                                                                                                            //Natural: DISPLAY 'TAX-SSN' OUTFILE.TWRPYMNT-TAX-ID-NBR 'TIAA CONTRACT' OUTFILE.TWRPYMNT-CONTRACT-NBR 'PAY CONTRACT' PAY.TWRPYMNT-CONTRACT-NBR 'PAY-UPDT-DATE' TWRPYMNT-UPDT-DTE
            		outfile_Twrpymnt_Tax_Id_Nbr,"TIAA CONTRACT",
            		outfile_Twrpymnt_Contract_Nbr,"PAY CONTRACT",
            		ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr(),"PAY-UPDT-DATE",
            		ldaTwrl9415.getPay_Twrpymnt_Updt_Dte());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("SV1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("SV1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  COMPARE-OMNIPAY-DATA */
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"**************************************************************",NEWLINE,"**************************************************************", //Natural: WRITE // '**************************************************************' / '**************************************************************' / '***'*PROGRAM '  ERROR:' *ERROR-NR 'LINE:' *ERROR-LINE / '**************************************************************' / '**************************************************************'
            NEWLINE,"***",Global.getPROGRAM(),"  ERROR:",Global.getERROR_NR(),"LINE:",Global.getERROR_LINE(),NEWLINE,"**************************************************************",
            NEWLINE,"**************************************************************");
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        getReports().setDisplayColumns(0, "TAX-SSN",
        		outfile_Twrpymnt_Tax_Id_Nbr,"TIAA CONTRACT",
        		outfile_Twrpymnt_Contract_Nbr,"PAY CONTRACT",
        		ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr(),"PAY-UPDT-DATE",
        		ldaTwrl9415.getPay_Twrpymnt_Updt_Dte());
    }
}
