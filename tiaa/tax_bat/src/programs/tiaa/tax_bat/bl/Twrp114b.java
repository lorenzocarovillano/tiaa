/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:32:58 PM
**        * FROM NATURAL PROGRAM : Twrp114b
************************************************************
**        * FILE NAME            : Twrp114b.java
**        * CLASS NAME           : Twrp114b
**        * INSTANCE NAME        : Twrp114b
************************************************************
************************************************************************
************************  T W R P 1 1 4 B  *****************************
************************************************************************
**                                                                    **
** PROGRAM NAME:  TWRP114B                                            **
** SYSTEM      :  TAX WITHHOLDING & REPORTING SYSTEM                  **
** AUTHOR      :  A.WILNER                                            **
** PURPOSE     :  VOLUME PRINT BATCH DRIVER                           **
** DESCRIPTION :  COMPLETE PROCESSING VOLUME PRINT CASES IN BATCH     **
**                THAT WERE NOT COMPLETED BY ONLINE FUNCTION.         **
**                VOLUME PRINT 00 RECORD REPRESENTS A VOLUME PRINT    **
**                REQUEST.                                            **
** HISTORY.....:                                                      **
**    --/--/---- -                                                    **
**                                                                    **
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp114b extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_notepad;
    private DbsField notepad_Np_Datn;
    private DbsField notepad_Np_Timn;
    private DbsField notepad_Np_Init_Id;
    private DbsField notepad_Np_Init_User;
    private DbsField notepad_Np_Program;
    private DbsField notepad_Np_Sort_Key1;

    private DbsGroup notepad__R_Field_1;
    private DbsField notepad_Pnd_Vol_Volume;
    private DbsField notepad_Pnd_Vol_Date;
    private DbsField notepad_Pnd_Vol_Time;
    private DbsField notepad_Pnd_Vol_Num;
    private DbsField notepad_Np_Data1;

    private DbsGroup notepad__R_Field_2;
    private DbsField notepad_Pnd_Ws_Vol_Contract_F;
    private DbsField notepad_Pnd_Ws_Vol_Contract_T;
    private DbsField notepad_Pnd_Ws_Vol_Creat_User;
    private DbsField notepad_Pnd_Ws_Vol_Form;
    private DbsField notepad_Pnd_Ws_Vol_Srce_Cde;
    private DbsField notepad_Pnd_Ws_Vol_Srce_Dte;
    private DbsField notepad_Pnd_Ws_Vol_Tin_F;
    private DbsField notepad_Pnd_Ws_Vol_Tin_T;
    private DbsField notepad_Pnd_Ws_Vol_Tax_Year;
    private DbsField notepad_Np_Data2;

    private DbsGroup notepad__R_Field_3;
    private DbsField notepad_Pnd_Np_Print_Primary;
    private DbsField notepad_Pnd_Np_Print_Alt;
    private DbsField notepad_Pnd_Np_Local_Map;
    private DbsField notepad_Pnd_Np_Printer;
    private DbsField notepad_Pnd_Np_Letter_Ind;
    private DbsField notepad_Pnd_Np_User_Name;
    private DbsField notepad_Pnd_Np_Hard_Copy;
    private DbsField pnd_Np_Super2;

    private DbsGroup pnd_Np_Super2__R_Field_4;
    private DbsField pnd_Np_Super2_Pnd_Np_Datn;
    private DbsField pnd_Np_Super2_Pnd_Np_Timn;
    private DbsField pnd_Np_Super2_Pnd_Np_Init_Id;
    private DbsField pnd_Np_Super2_Pnd_Np_Init_User;
    private DbsField pnd_Np_Super2_Pnd_Np_Program;
    private DbsField pnd_Np_Super2_Pnd_Np_Sort_Key1;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_notepad = new DataAccessProgramView(new NameInfo("vw_notepad", "NOTEPAD"), "TWR_NOTEPAD", "TWR_NOTEPAD");
        notepad_Np_Datn = vw_notepad.getRecord().newFieldInGroup("notepad_Np_Datn", "NP-DATN", FieldType.STRING, 8, RepeatingFieldStrategy.None, "NP_DATN");
        notepad_Np_Timn = vw_notepad.getRecord().newFieldInGroup("notepad_Np_Timn", "NP-TIMN", FieldType.STRING, 7, RepeatingFieldStrategy.None, "NP_TIMN");
        notepad_Np_Init_Id = vw_notepad.getRecord().newFieldInGroup("notepad_Np_Init_Id", "NP-INIT-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "NP_INIT_ID");
        notepad_Np_Init_User = vw_notepad.getRecord().newFieldInGroup("notepad_Np_Init_User", "NP-INIT-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "NP_INIT_USER");
        notepad_Np_Program = vw_notepad.getRecord().newFieldInGroup("notepad_Np_Program", "NP-PROGRAM", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "NP_PROGRAM");
        notepad_Np_Sort_Key1 = vw_notepad.getRecord().newFieldInGroup("notepad_Np_Sort_Key1", "NP-SORT-KEY1", FieldType.STRING, 50, RepeatingFieldStrategy.None, 
            "NP_SORT_KEY1");

        notepad__R_Field_1 = vw_notepad.getRecord().newGroupInGroup("notepad__R_Field_1", "REDEFINE", notepad_Np_Sort_Key1);
        notepad_Pnd_Vol_Volume = notepad__R_Field_1.newFieldInGroup("notepad_Pnd_Vol_Volume", "#VOL-VOLUME", FieldType.STRING, 7);
        notepad_Pnd_Vol_Date = notepad__R_Field_1.newFieldInGroup("notepad_Pnd_Vol_Date", "#VOL-DATE", FieldType.STRING, 8);
        notepad_Pnd_Vol_Time = notepad__R_Field_1.newFieldInGroup("notepad_Pnd_Vol_Time", "#VOL-TIME", FieldType.STRING, 12);
        notepad_Pnd_Vol_Num = notepad__R_Field_1.newFieldInGroup("notepad_Pnd_Vol_Num", "#VOL-NUM", FieldType.STRING, 2);
        notepad_Np_Data1 = vw_notepad.getRecord().newFieldInGroup("notepad_Np_Data1", "NP-DATA1", FieldType.STRING, 250, RepeatingFieldStrategy.None, 
            "NP_DATA1");

        notepad__R_Field_2 = vw_notepad.getRecord().newGroupInGroup("notepad__R_Field_2", "REDEFINE", notepad_Np_Data1);
        notepad_Pnd_Ws_Vol_Contract_F = notepad__R_Field_2.newFieldInGroup("notepad_Pnd_Ws_Vol_Contract_F", "#WS-VOL-CONTRACT-F", FieldType.STRING, 8);
        notepad_Pnd_Ws_Vol_Contract_T = notepad__R_Field_2.newFieldInGroup("notepad_Pnd_Ws_Vol_Contract_T", "#WS-VOL-CONTRACT-T", FieldType.STRING, 8);
        notepad_Pnd_Ws_Vol_Creat_User = notepad__R_Field_2.newFieldInGroup("notepad_Pnd_Ws_Vol_Creat_User", "#WS-VOL-CREAT-USER", FieldType.STRING, 8);
        notepad_Pnd_Ws_Vol_Form = notepad__R_Field_2.newFieldArrayInGroup("notepad_Pnd_Ws_Vol_Form", "#WS-VOL-FORM", FieldType.STRING, 6, new DbsArrayController(1, 
            5));
        notepad_Pnd_Ws_Vol_Srce_Cde = notepad__R_Field_2.newFieldArrayInGroup("notepad_Pnd_Ws_Vol_Srce_Cde", "#WS-VOL-SRCE-CDE", FieldType.STRING, 2, 
            new DbsArrayController(1, 5));
        notepad_Pnd_Ws_Vol_Srce_Dte = notepad__R_Field_2.newFieldInGroup("notepad_Pnd_Ws_Vol_Srce_Dte", "#WS-VOL-SRCE-DTE", FieldType.DATE);
        notepad_Pnd_Ws_Vol_Tin_F = notepad__R_Field_2.newFieldInGroup("notepad_Pnd_Ws_Vol_Tin_F", "#WS-VOL-TIN-F", FieldType.STRING, 10);
        notepad_Pnd_Ws_Vol_Tin_T = notepad__R_Field_2.newFieldInGroup("notepad_Pnd_Ws_Vol_Tin_T", "#WS-VOL-TIN-T", FieldType.STRING, 10);
        notepad_Pnd_Ws_Vol_Tax_Year = notepad__R_Field_2.newFieldInGroup("notepad_Pnd_Ws_Vol_Tax_Year", "#WS-VOL-TAX-YEAR", FieldType.NUMERIC, 4);
        notepad_Np_Data2 = vw_notepad.getRecord().newFieldInGroup("notepad_Np_Data2", "NP-DATA2", FieldType.STRING, 250, RepeatingFieldStrategy.None, 
            "NP_DATA2");

        notepad__R_Field_3 = vw_notepad.getRecord().newGroupInGroup("notepad__R_Field_3", "REDEFINE", notepad_Np_Data2);
        notepad_Pnd_Np_Print_Primary = notepad__R_Field_3.newFieldInGroup("notepad_Pnd_Np_Print_Primary", "#NP-PRINT-PRIMARY", FieldType.STRING, 1);
        notepad_Pnd_Np_Print_Alt = notepad__R_Field_3.newFieldInGroup("notepad_Pnd_Np_Print_Alt", "#NP-PRINT-ALT", FieldType.STRING, 1);
        notepad_Pnd_Np_Local_Map = notepad__R_Field_3.newFieldInGroup("notepad_Pnd_Np_Local_Map", "#NP-LOCAL-MAP", FieldType.STRING, 1);
        notepad_Pnd_Np_Printer = notepad__R_Field_3.newFieldInGroup("notepad_Pnd_Np_Printer", "#NP-PRINTER", FieldType.STRING, 10);
        notepad_Pnd_Np_Letter_Ind = notepad__R_Field_3.newFieldInGroup("notepad_Pnd_Np_Letter_Ind", "#NP-LETTER-IND", FieldType.STRING, 1);
        notepad_Pnd_Np_User_Name = notepad__R_Field_3.newFieldInGroup("notepad_Pnd_Np_User_Name", "#NP-USER-NAME", FieldType.STRING, 30);
        notepad_Pnd_Np_Hard_Copy = notepad__R_Field_3.newFieldInGroup("notepad_Pnd_Np_Hard_Copy", "#NP-HARD-COPY", FieldType.STRING, 1);
        registerRecord(vw_notepad);

        pnd_Np_Super2 = localVariables.newFieldInRecord("pnd_Np_Super2", "#NP-SUPER2", FieldType.STRING, 89);

        pnd_Np_Super2__R_Field_4 = localVariables.newGroupInRecord("pnd_Np_Super2__R_Field_4", "REDEFINE", pnd_Np_Super2);
        pnd_Np_Super2_Pnd_Np_Datn = pnd_Np_Super2__R_Field_4.newFieldInGroup("pnd_Np_Super2_Pnd_Np_Datn", "#NP-DATN", FieldType.STRING, 8);
        pnd_Np_Super2_Pnd_Np_Timn = pnd_Np_Super2__R_Field_4.newFieldInGroup("pnd_Np_Super2_Pnd_Np_Timn", "#NP-TIMN", FieldType.STRING, 7);
        pnd_Np_Super2_Pnd_Np_Init_Id = pnd_Np_Super2__R_Field_4.newFieldInGroup("pnd_Np_Super2_Pnd_Np_Init_Id", "#NP-INIT-ID", FieldType.STRING, 8);
        pnd_Np_Super2_Pnd_Np_Init_User = pnd_Np_Super2__R_Field_4.newFieldInGroup("pnd_Np_Super2_Pnd_Np_Init_User", "#NP-INIT-USER", FieldType.STRING, 
            8);
        pnd_Np_Super2_Pnd_Np_Program = pnd_Np_Super2__R_Field_4.newFieldInGroup("pnd_Np_Super2_Pnd_Np_Program", "#NP-PROGRAM", FieldType.STRING, 8);
        pnd_Np_Super2_Pnd_Np_Sort_Key1 = pnd_Np_Super2__R_Field_4.newFieldInGroup("pnd_Np_Super2_Pnd_Np_Sort_Key1", "#NP-SORT-KEY1", FieldType.STRING, 
            50);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_notepad.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp114b() throws Exception
    {
        super("Twrp114b");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* *
        //*  MAIN PROGRAM
        pnd_Np_Super2.reset();                                                                                                                                            //Natural: RESET #NP-SUPER2
        pnd_Np_Super2_Pnd_Np_Datn.setValue(Global.getDATN());                                                                                                             //Natural: MOVE *DATN TO #NP-DATN
        pnd_Np_Super2_Pnd_Np_Timn.setValue("9999999");                                                                                                                    //Natural: MOVE '9999999' TO #NP-TIMN
        pnd_Np_Super2_Pnd_Np_Init_Id.setValue("VOLUME");                                                                                                                  //Natural: MOVE 'VOLUME' TO #NP-INIT-ID
        vw_notepad.startDatabaseRead                                                                                                                                      //Natural: READ NOTEPAD WITH NP-SUPER2 = #NP-SUPER2
        (
        "READ01",
        new Wc[] { new Wc("NP_SUPER2", ">=", pnd_Np_Super2, WcType.BY) },
        new Oc[] { new Oc("NP_SUPER2", "ASC") }
        );
        READ01:
        while (condition(vw_notepad.readNextRow("READ01")))
        {
            if (condition(notepad_Pnd_Vol_Volume.notEquals("VOLUME")))                                                                                                    //Natural: IF #VOL-VOLUME NE 'VOLUME'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(notepad_Pnd_Vol_Num.notEquals("00")))                                                                                                           //Natural: IF #VOL-NUM NE '00'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            Global.getSTACK().pushData(StackOption.TOP, notepad_Np_Sort_Key1);                                                                                            //Natural: FETCH RETURN 'TWRP114A' NP-SORT-KEY1
            DbsUtil.invokeMain(DbsUtil.getBlType("TWRP114A"), getCurrentProcessState());
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
