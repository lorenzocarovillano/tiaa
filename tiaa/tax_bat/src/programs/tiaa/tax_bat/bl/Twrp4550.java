/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:39:42 PM
**        * FROM NATURAL PROGRAM : Twrp4550
************************************************************
**        * FILE NAME            : Twrp4550.java
**        * CLASS NAME           : Twrp4550
**        * INSTANCE NAME        : Twrp4550
************************************************************
************************************************************************
*
* PROGRAM  : TWRP4550
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : IRS 5498 CORRECTIONS FILE PRINT DUMP REPORT.
* CREATED  : 07 / 11 / 2000.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM READS IRS 5498 CORRECTIONS TAPE FILE, AND
*          : PRODUCES A PRINTED DUMP OF ALL THE RECORDS ON FILE.
* 11-17-09 : A. YOUNG        - RE-COMPILED TO ACCOMMODATE 2009 IRS
*          :                   CHANGES TO 'B', 'C', AND 'K' RECS.
* 10/03/07  RM TWRL444A UPDATED PER IRS 1220 SPEC.
*              UPDATE COMMENT FOR TWRL444A FROM MOORE TO BE IRS.
*              RESTOWED WITH UPDATED LDA.
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp4550 extends BLNatBase
{
    // Data Areas
    private LdaTwrl444a ldaTwrl444a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Columns;
    private DbsField pnd_Read_Ctr;
    private DbsField i;
    private DbsField j;
    private DbsField k;
    private DbsField l;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl444a = new LdaTwrl444a();
        registerRecord(ldaTwrl444a);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Columns = localVariables.newFieldInRecord("pnd_Columns", "#COLUMNS", FieldType.STRING, 100);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 8);
        i = localVariables.newFieldInRecord("i", "I", FieldType.PACKED_DECIMAL, 4);
        j = localVariables.newFieldInRecord("j", "J", FieldType.PACKED_DECIMAL, 4);
        k = localVariables.newFieldInRecord("k", "K", FieldType.PACKED_DECIMAL, 4);
        l = localVariables.newFieldInRecord("l", "L", FieldType.PACKED_DECIMAL, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl444a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp4550() throws Exception
    {
        super("Twrp4550");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP4550", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133;//Natural: FORMAT ( 02 ) PS = 60 LS = 133;//Natural: FORMAT ( 03 ) PS = 60 LS = 133;//Natural: FORMAT ( 04 ) PS = 60 LS = 133
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        pnd_Columns.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "----+----1----+----2----+----3----+----4----+----5", "----+----6----+----7----+----8----+----9----+----0")); //Natural: COMPRESS '----+----1----+----2----+----3----+----4----+----5' '----+----6----+----7----+----8----+----9----+----0' INTO #COLUMNS LEAVING NO SPACE
        RD1:                                                                                                                                                              //Natural: READ WORK FILE 01 RECORD #IRS
        while (condition(getWorkFiles().read(1, ldaTwrl444a.getPnd_Irs())))
        {
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            //*                                                                                                                                                           //Natural: DECIDE ON FIRST VALUE OF #T-RECORD-TYPE
            short decideConditionsMet378 = 0;                                                                                                                             //Natural: VALUES 'T'
            if (condition((ldaTwrl444a.getPnd_Irs_Pnd_T_Record_Type().equals("T"))))
            {
                decideConditionsMet378++;
                ldaTwrl444a.getPnd_Irs_Pnd_T_Filler_1().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #T-FILLER-1
                ldaTwrl444a.getPnd_Irs_Pnd_T_Filler_2().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #T-FILLER-2
                ldaTwrl444a.getPnd_Irs_Pnd_T_Filler_3().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #T-FILLER-3
                ldaTwrl444a.getPnd_Irs_Pnd_T_Filler_4().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #T-FILLER-4
                ldaTwrl444a.getPnd_Irs_Pnd_T_Blank_Or_Cr_Lf().moveAll(".");                                                                                               //Natural: MOVE ALL '.' TO #T-BLANK-OR-CR-LF
            }                                                                                                                                                             //Natural: VALUES 'A'
            else if (condition((ldaTwrl444a.getPnd_Irs_Pnd_T_Record_Type().equals("A"))))
            {
                decideConditionsMet378++;
                ldaTwrl444a.getPnd_Irs_Pnd_A_Filler_1().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #A-FILLER-1
                ldaTwrl444a.getPnd_Irs_Pnd_A_Filler_2().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #A-FILLER-2
                //*       MOVE ALL  '.'  TO  #A-FILLER-3                     10/03/07  RM
                ldaTwrl444a.getPnd_Irs_Pnd_A_Filler_4().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #A-FILLER-4
                ldaTwrl444a.getPnd_Irs_Pnd_A_Filler_5().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #A-FILLER-5
                ldaTwrl444a.getPnd_Irs_Pnd_A_Filler_6().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #A-FILLER-6
            }                                                                                                                                                             //Natural: VALUES 'B'
            else if (condition((ldaTwrl444a.getPnd_Irs_Pnd_T_Record_Type().equals("B"))))
            {
                decideConditionsMet378++;
                ldaTwrl444a.getPnd_Irs_Pnd_B_Filler_1().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #B-FILLER-1
                //*      MOVE ALL  '.'  TO  #B-FILLER-2                       /* 11-17-09
                ldaTwrl444a.getPnd_Irs_Pnd_B_Filler_3().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #B-FILLER-3
                ldaTwrl444a.getPnd_Irs_Pnd_B_Filler_4().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #B-FILLER-4
                ldaTwrl444a.getPnd_Irs_Pnd_B_Filler_5().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #B-FILLER-5
                ldaTwrl444a.getPnd_Irs_Pnd_B_Filler_6().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #B-FILLER-6
                //*      MOVE ALL  '.'  TO  #B-FILLER-7
                ldaTwrl444a.getPnd_Irs_Pnd_B_Filler_8().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #B-FILLER-8
            }                                                                                                                                                             //Natural: VALUES 'C'
            else if (condition((ldaTwrl444a.getPnd_Irs_Pnd_T_Record_Type().equals("C"))))
            {
                decideConditionsMet378++;
                ldaTwrl444a.getPnd_Irs_Pnd_C_Filler_1().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #C-FILLER-1
                ldaTwrl444a.getPnd_Irs_Pnd_C_Filler_2().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #C-FILLER-2
                //*      MOVE ALL  '.'  TO  #C-FILLER-3
                ldaTwrl444a.getPnd_Irs_Pnd_C_Filler_4().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #C-FILLER-4
                ldaTwrl444a.getPnd_Irs_Pnd_C_Blank_Cr_Lf().moveAll(".");                                                                                                  //Natural: MOVE ALL '.' TO #C-BLANK-CR-LF
            }                                                                                                                                                             //Natural: VALUES 'K'
            else if (condition((ldaTwrl444a.getPnd_Irs_Pnd_T_Record_Type().equals("K"))))
            {
                decideConditionsMet378++;
                ldaTwrl444a.getPnd_Irs_Pnd_K_Filler_1().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #K-FILLER-1
                ldaTwrl444a.getPnd_Irs_Pnd_K_Filler_2().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #K-FILLER-2
                //*      MOVE ALL  '.'  TO  #K-FILLER-3
                ldaTwrl444a.getPnd_Irs_Pnd_K_Filler_4().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #K-FILLER-4
                ldaTwrl444a.getPnd_Irs_Pnd_K_Blank_Cr_Lf().moveAll(".");                                                                                                  //Natural: MOVE ALL '.' TO #K-BLANK-CR-LF
            }                                                                                                                                                             //Natural: VALUES 'F'
            else if (condition((ldaTwrl444a.getPnd_Irs_Pnd_T_Record_Type().equals("F"))))
            {
                decideConditionsMet378++;
                ldaTwrl444a.getPnd_Irs_Pnd_F_Filler_2().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #F-FILLER-2
                ldaTwrl444a.getPnd_Irs_Pnd_F_Filler_3().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #F-FILLER-3
                ldaTwrl444a.getPnd_Irs_Pnd_F_Filler_4().moveAll(".");                                                                                                     //Natural: MOVE ALL '.' TO #F-FILLER-4
                ldaTwrl444a.getPnd_Irs_Pnd_F_Blank_Cr_Lf().moveAll(".");                                                                                                  //Natural: MOVE ALL '.' TO #F-BLANK-CR-LF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            ldaTwrl444a.getPnd_Dump_Pnd_D700().getValue("*").setValue(ldaTwrl444a.getPnd_Irs_Pnd_I700().getValue("*"));                                                   //Natural: ASSIGN #D700 ( * ) := #I700 ( * )
            ldaTwrl444a.getPnd_Dump_Pnd_D50().setValue(ldaTwrl444a.getPnd_Irs_Pnd_I50());                                                                                 //Natural: ASSIGN #D50 := #I50
            j.setValue(1);                                                                                                                                                //Natural: ASSIGN J := 1
            k.setValue(100);                                                                                                                                              //Natural: ASSIGN K := 100
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(7),pnd_Columns);                                                                 //Natural: WRITE ( 01 ) NOTITLE NOHDR 07T #COLUMNS
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR01:                                                                                                                                                        //Natural: FOR I = 1 TO 7
            for (i.setValue(1); condition(i.lessOrEqual(7)); i.nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),j, new ReportEditMask ("ZZZ9"),new TabSetting(7),ldaTwrl444a.getPnd_Dump_Pnd_D700().getValue(i),new  //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T J ( EM = ZZZ9 ) 07T #D700 ( I ) 109T K ( EM = ZZZ9 )
                    TabSetting(109),k, new ReportEditMask ("ZZZ9"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                j.nadd(100);                                                                                                                                              //Natural: ASSIGN J := J + 100
                k.nadd(100);                                                                                                                                              //Natural: ASSIGN K := K + 100
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            j.setValue(701);                                                                                                                                              //Natural: ASSIGN J := 701
            k.setValue(750);                                                                                                                                              //Natural: ASSIGN K := 750
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),j, new ReportEditMask ("ZZZ9"),new TabSetting(7),ldaTwrl444a.getPnd_Dump_Pnd_D50(),new  //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T J ( EM = ZZZ9 ) 07T #D50 109T K ( EM = ZZZ9 )
                TabSetting(109),k, new ReportEditMask ("ZZZ9"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        RD1_Exit:
        if (Global.isEscape()) return;
        //* *------
        if (condition(pnd_Read_Ctr.equals(getZero())))                                                                                                                    //Natural: IF #READ-CTR = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(6),"IRS 5498 Corrections Extract File (Work File 01)","Is Empty",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) '***' 06T 'IRS 5498 Corrections Extract File (Work File 01)' 'Is Empty' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *------------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 01 )
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
        //* *-------                                                                                                                                                      //Natural: ON ERROR
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),"IRS 5498 Correction Records Found........",pnd_Read_Ctr, new                     //Natural: WRITE ( 00 ) NOTITLE NOHDR 01T 'IRS 5498 Correction Records Found........' #READ-CTR
            ReportEditMask ("ZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),"IRS 5498 Correction Records Found........",pnd_Read_Ctr, new                     //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T 'IRS 5498 Correction Records Found........' #READ-CTR
            ReportEditMask ("ZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(41),"IRS 5498 CORRECTIONS TRANSMISSION/TAPE FILE DUMP",new  //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 41T 'IRS 5498 CORRECTIONS TRANSMISSION/TAPE FILE DUMP' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1 LINES
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
        sub_Error_Display_Start();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
        sub_Error_Display_End();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
        Global.format(2, "PS=60 LS=133");
        Global.format(3, "PS=60 LS=133");
        Global.format(4, "PS=60 LS=133");
    }
}
