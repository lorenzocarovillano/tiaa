/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:35:00 PM
**        * FROM NATURAL PROGRAM : Twrp3209
************************************************************
**        * FILE NAME            : Twrp3209.java
**        * CLASS NAME           : Twrp3209
**        * INSTANCE NAME        : Twrp3209
************************************************************
************************************************************************
** PROGRAM : TWRP3209
** SYSTEM  : TAXWARS
** AUTHOR  : COGNIZANT
** FUNCTION: TO UPDATE CITIZEN CODE IN FORM-RECEIPT-FILE
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3209 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Inp_Ny_Record;
    private DbsField pnd_Inp_Ny_Record_Pnd_Outw_Il_Name;
    private DbsField pnd_Inp_Ny_Record_Pnd_Outw_Il_Blank1;
    private DbsField pnd_Inp_Ny_Record_Pnd_Outw_Il_Tin;
    private DbsField pnd_Inp_Ny_Record_Pnd_Outw_Il_Blank2;
    private DbsField pnd_Inp_Ny_Record_Pnd_Outw_Il_Gross;
    private DbsField pnd_Inp_Ny_Record_Pnd_Outw_Il_Blank3;
    private DbsField pnd_Inp_Ny_Record_Pnd_Outw_Il_Tax_Wthld;
    private DbsField pnd_Outrec;

    private DbsGroup pnd_Outrec__R_Field_1;
    private DbsField pnd_Outrec_Pnd_Ssn;
    private DbsField pnd_Outrec_Pnd_Filler1;
    private DbsField pnd_Outrec_Pnd_Empname;
    private DbsField pnd_Outrec_Pnd_Filler2;
    private DbsField pnd_Outrec_Pnd_Gross_Amt;
    private DbsField pnd_Outrec_Pnd_Filler3;
    private DbsField pnd_Outrec_Pnd_Tax_Witheld;
    private DbsField pnd_Ws_Name;
    private DbsField pnd_Rec_Seq_Number;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Inp_Ny_Record = localVariables.newGroupInRecord("pnd_Inp_Ny_Record", "#INP-NY-RECORD");
        pnd_Inp_Ny_Record_Pnd_Outw_Il_Name = pnd_Inp_Ny_Record.newFieldInGroup("pnd_Inp_Ny_Record_Pnd_Outw_Il_Name", "#OUTW-IL-NAME", FieldType.STRING, 
            30);
        pnd_Inp_Ny_Record_Pnd_Outw_Il_Blank1 = pnd_Inp_Ny_Record.newFieldInGroup("pnd_Inp_Ny_Record_Pnd_Outw_Il_Blank1", "#OUTW-IL-BLANK1", FieldType.STRING, 
            1);
        pnd_Inp_Ny_Record_Pnd_Outw_Il_Tin = pnd_Inp_Ny_Record.newFieldInGroup("pnd_Inp_Ny_Record_Pnd_Outw_Il_Tin", "#OUTW-IL-TIN", FieldType.STRING, 9);
        pnd_Inp_Ny_Record_Pnd_Outw_Il_Blank2 = pnd_Inp_Ny_Record.newFieldInGroup("pnd_Inp_Ny_Record_Pnd_Outw_Il_Blank2", "#OUTW-IL-BLANK2", FieldType.STRING, 
            1);
        pnd_Inp_Ny_Record_Pnd_Outw_Il_Gross = pnd_Inp_Ny_Record.newFieldInGroup("pnd_Inp_Ny_Record_Pnd_Outw_Il_Gross", "#OUTW-IL-GROSS", FieldType.NUMERIC, 
            13, 2);
        pnd_Inp_Ny_Record_Pnd_Outw_Il_Blank3 = pnd_Inp_Ny_Record.newFieldInGroup("pnd_Inp_Ny_Record_Pnd_Outw_Il_Blank3", "#OUTW-IL-BLANK3", FieldType.STRING, 
            1);
        pnd_Inp_Ny_Record_Pnd_Outw_Il_Tax_Wthld = pnd_Inp_Ny_Record.newFieldInGroup("pnd_Inp_Ny_Record_Pnd_Outw_Il_Tax_Wthld", "#OUTW-IL-TAX-WTHLD", FieldType.NUMERIC, 
            13, 2);
        pnd_Outrec = localVariables.newFieldInRecord("pnd_Outrec", "#OUTREC", FieldType.STRING, 70);

        pnd_Outrec__R_Field_1 = localVariables.newGroupInRecord("pnd_Outrec__R_Field_1", "REDEFINE", pnd_Outrec);
        pnd_Outrec_Pnd_Ssn = pnd_Outrec__R_Field_1.newFieldInGroup("pnd_Outrec_Pnd_Ssn", "#SSN", FieldType.STRING, 9);
        pnd_Outrec_Pnd_Filler1 = pnd_Outrec__R_Field_1.newFieldInGroup("pnd_Outrec_Pnd_Filler1", "#FILLER1", FieldType.STRING, 1);
        pnd_Outrec_Pnd_Empname = pnd_Outrec__R_Field_1.newFieldInGroup("pnd_Outrec_Pnd_Empname", "#EMPNAME", FieldType.STRING, 30);
        pnd_Outrec_Pnd_Filler2 = pnd_Outrec__R_Field_1.newFieldInGroup("pnd_Outrec_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Outrec_Pnd_Gross_Amt = pnd_Outrec__R_Field_1.newFieldInGroup("pnd_Outrec_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.STRING, 14);
        pnd_Outrec_Pnd_Filler3 = pnd_Outrec__R_Field_1.newFieldInGroup("pnd_Outrec_Pnd_Filler3", "#FILLER3", FieldType.STRING, 1);
        pnd_Outrec_Pnd_Tax_Witheld = pnd_Outrec__R_Field_1.newFieldInGroup("pnd_Outrec_Pnd_Tax_Witheld", "#TAX-WITHELD", FieldType.STRING, 14);
        pnd_Ws_Name = localVariables.newFieldInRecord("pnd_Ws_Name", "#WS-NAME", FieldType.STRING, 30);
        pnd_Rec_Seq_Number = localVariables.newFieldInRecord("pnd_Rec_Seq_Number", "#REC-SEQ-NUMBER", FieldType.NUMERIC, 12);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3209() throws Exception
    {
        super("Twrp3209");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 #INP-NY-RECORD
        while (condition(getWorkFiles().read(1, pnd_Inp_Ny_Record)))
        {
            pnd_Rec_Seq_Number.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #REC-SEQ-NUMBER
                                                                                                                                                                          //Natural: PERFORM REFORMAT-CSV-REC
            sub_Reformat_Csv_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-CSV-OUTPUT
            sub_Write_Csv_Output();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REFORMAT-CSV-REC
        //* ***************************
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CSV-OUTPUT
    }
    private void sub_Reformat_Csv_Rec() throws Exception                                                                                                                  //Natural: REFORMAT-CSV-REC
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Outrec_Pnd_Empname.setValue(pnd_Inp_Ny_Record_Pnd_Outw_Il_Name);                                                                                              //Natural: ASSIGN #EMPNAME := #OUTW-IL-NAME
        pnd_Outrec_Pnd_Ssn.setValue(pnd_Inp_Ny_Record_Pnd_Outw_Il_Tin);                                                                                                   //Natural: ASSIGN #SSN := #OUTW-IL-TIN
        pnd_Outrec_Pnd_Gross_Amt.setValueEdited(pnd_Inp_Ny_Record_Pnd_Outw_Il_Gross,new ReportEditMask("ZZZZZZZZZZ9.99"));                                                //Natural: MOVE EDITED #OUTW-IL-GROSS ( EM = ZZZZZZZZZZ9.99 ) TO #GROSS-AMT
        pnd_Outrec_Pnd_Tax_Witheld.setValueEdited(pnd_Inp_Ny_Record_Pnd_Outw_Il_Tax_Wthld,new ReportEditMask("ZZZZZZZZZZ9.99"));                                          //Natural: MOVE EDITED #OUTW-IL-TAX-WTHLD ( EM = ZZZZZZZZZZ9.99 ) TO #TAX-WITHELD
        if (condition(pnd_Outrec_Pnd_Gross_Amt.getSubstring(13,2).equals("00")))                                                                                          //Natural: IF SUBSTRING ( #GROSS-AMT,13,2 ) EQ '00'
        {
            pnd_Outrec_Pnd_Gross_Amt.setValue(pnd_Outrec_Pnd_Gross_Amt.getSubstring(1,11));                                                                               //Natural: MOVE SUBSTRING ( #GROSS-AMT,1,11 ) TO #GROSS-AMT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Outrec_Pnd_Tax_Witheld.getSubstring(13,2).equals("00")))                                                                                        //Natural: IF SUBSTRING ( #TAX-WITHELD,13,2 ) EQ '00'
        {
            pnd_Outrec_Pnd_Tax_Witheld.setValue(pnd_Outrec_Pnd_Tax_Witheld.getSubstring(1,11));                                                                           //Natural: MOVE SUBSTRING ( #TAX-WITHELD,1,11 ) TO #TAX-WITHELD
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Outrec_Pnd_Filler1.setValue(",");                                                                                                                             //Natural: MOVE ',' TO #FILLER1 #FILLER2 #FILLER3
        pnd_Outrec_Pnd_Filler2.setValue(",");
        pnd_Outrec_Pnd_Filler3.setValue(",");
    }
    private void sub_Write_Csv_Output() throws Exception                                                                                                                  //Natural: WRITE-CSV-OUTPUT
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        getWorkFiles().write(2, false, pnd_Outrec);                                                                                                                       //Natural: WRITE WORK FILE 02 #OUTREC
    }

    //
}
