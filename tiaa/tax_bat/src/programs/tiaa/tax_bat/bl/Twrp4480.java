/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:39:25 PM
**        * FROM NATURAL PROGRAM : Twrp4480
************************************************************
**        * FILE NAME            : Twrp4480.java
**        * CLASS NAME           : Twrp4480
**        * INSTANCE NAME        : Twrp4480
************************************************************
************************************************************************
*
* PROGRAM  : TWRP4480  (IRS - 5498  ORIGINAL REPORTING).
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : PRODUCE FORM 5498 IRS ACCEPTED DETAIL REPORT.
* CREATED  : 06 / 14 / 2000.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM PRODUCES A REPORT OF ALL ACCEPTED 5498 RECORDS
*          : REPORTED TO THE 'IRS' (ORIGINAL REPORTING).
* 11/17/09 : AY        - RE-COMPILED TO ACCOMMODATE 2009 IRS CHANGES
*          :             TO 'B', 'C', AND 'K' RECS.
* 11/29/05 : BK ADDED SEP
* 12/02/17  DASDH 5498 BOX CHANGES
* 10/05/18  ARIVU EIN CHANGES. TAG: EINCHG
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp4480 extends BLNatBase
{
    // Data Areas
    private LdaTwrl448a ldaTwrl448a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_City_State_Zip_Code;
    private DbsField pnd_Total_B_Recs_Record;
    private DbsField pnd_Prev_A_Rec_Company;
    private DbsField pnd_Prev_C_Rec_Company;
    private DbsField pnd_Isn;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Number_Of_A_Recs;
    private DbsField pnd_T_Ctr;
    private DbsField pnd_A_Ctr;
    private DbsField pnd_B_Ctr;
    private DbsField pnd_C_Ctr;
    private DbsField pnd_K_Ctr;
    private DbsField pnd_F_Ctr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl448a = new LdaTwrl448a();
        registerRecord(ldaTwrl448a);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_City_State_Zip_Code = localVariables.newFieldInRecord("pnd_City_State_Zip_Code", "#CITY-STATE-ZIP-CODE", FieldType.STRING, 51);
        pnd_Total_B_Recs_Record = localVariables.newFieldInRecord("pnd_Total_B_Recs_Record", "#TOTAL-B-RECS-RECORD", FieldType.NUMERIC, 8);
        pnd_Prev_A_Rec_Company = localVariables.newFieldInRecord("pnd_Prev_A_Rec_Company", "#PREV-A-REC-COMPANY", FieldType.STRING, 1);
        pnd_Prev_C_Rec_Company = localVariables.newFieldInRecord("pnd_Prev_C_Rec_Company", "#PREV-C-REC-COMPANY", FieldType.STRING, 1);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Number_Of_A_Recs = localVariables.newFieldInRecord("pnd_Number_Of_A_Recs", "#NUMBER-OF-A-RECS", FieldType.NUMERIC, 8);
        pnd_T_Ctr = localVariables.newFieldInRecord("pnd_T_Ctr", "#T-CTR", FieldType.PACKED_DECIMAL, 8);
        pnd_A_Ctr = localVariables.newFieldInRecord("pnd_A_Ctr", "#A-CTR", FieldType.PACKED_DECIMAL, 8);
        pnd_B_Ctr = localVariables.newFieldInRecord("pnd_B_Ctr", "#B-CTR", FieldType.PACKED_DECIMAL, 8);
        pnd_C_Ctr = localVariables.newFieldInRecord("pnd_C_Ctr", "#C-CTR", FieldType.PACKED_DECIMAL, 8);
        pnd_K_Ctr = localVariables.newFieldInRecord("pnd_K_Ctr", "#K-CTR", FieldType.PACKED_DECIMAL, 8);
        pnd_F_Ctr = localVariables.newFieldInRecord("pnd_F_Ctr", "#F-CTR", FieldType.PACKED_DECIMAL, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl448a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp4480() throws Exception
    {
        super("Twrp4480");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP4480", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        RD1:                                                                                                                                                              //Natural: READ WORK FILE 01 RECORD #IRS-5498
        while (condition(getWorkFiles().read(1, ldaTwrl448a.getPnd_Irs_5498())))
        {
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            //*                                                                                                                                                           //Natural: DECIDE ON FIRST VALUE OF #T-RECORD-TYPE
            short decideConditionsMet416 = 0;                                                                                                                             //Natural: VALUE 'T'
            if (condition((ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Record_Type().equals("T"))))
            {
                decideConditionsMet416++;
                                                                                                                                                                          //Natural: PERFORM WRITE-T-RECORD
                sub_Write_T_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'A'
            else if (condition((ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Record_Type().equals("A"))))
            {
                decideConditionsMet416++;
                                                                                                                                                                          //Natural: PERFORM WRITE-A-RECORD
                sub_Write_A_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'B'
            else if (condition((ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Record_Type().equals("B"))))
            {
                decideConditionsMet416++;
                                                                                                                                                                          //Natural: PERFORM WRITE-B-RECORD
                sub_Write_B_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'C'
            else if (condition((ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Record_Type().equals("C"))))
            {
                decideConditionsMet416++;
                                                                                                                                                                          //Natural: PERFORM WRITE-C-RECORD
                sub_Write_C_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'K'
            else if (condition((ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Record_Type().equals("K"))))
            {
                decideConditionsMet416++;
                                                                                                                                                                          //Natural: PERFORM WRITE-K-RECORD
                sub_Write_K_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Record_Type().equals("F"))))
            {
                decideConditionsMet416++;
                                                                                                                                                                          //Natural: PERFORM WRITE-F-RECORD
                sub_Write_F_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        RD1_Exit:
        if (Global.isEscape()) return;
        //* *------
        if (condition(pnd_Read_Ctr.equals(getZero())))                                                                                                                    //Natural: IF #READ-CTR = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"Form (5498) Extract File (Work File 01) Is Empty",new TabSetting(77),"***",NEWLINE,"***",new                   //Natural: WRITE ( 00 ) '***' 06T 'Form (5498) Extract File (Work File 01) Is Empty' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-IF
        //*  PERFORM  END-OF-PROGRAM-PROCESSING
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //*  01T 'No. Of K Records...........................'  #K-CTR
        //*  01T 'No. Of K Records...........................'  #K-CTR
        //* *------------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 01 )
        //* *-------                                                                                                                                                      //Natural: ON ERROR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
    }
    private void sub_Write_B_Record() throws Exception                                                                                                                    //Natural: WRITE-B-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------
        //*  BK
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Taxpayer_Id_Numb(), new ReportEditMask          //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T #B-TAXPAYER-ID-NUMB ( EM = XXX-XX-XXXX ) 13T #B-TYPE-OF-TIN 16T #B-CONTRACT-PAYEE ( EM = XXXXXXXX-XX ) 28T #B-PAYMENT-AMOUNT-1 ( EM = ZZZZ,ZZZ,ZZ9.99- ) 45T #B-PAYMENT-AMOUNT-2 ( EM = ZZZZ,ZZZ,ZZ9.99- ) 62T #B-PAYMENT-AMOUNT-3 ( EM = ZZZZ,ZZZ,ZZ9.99- ) 79T #B-PAYMENT-AMOUNT-4 ( EM = ZZZZ,ZZZ,ZZ9.99- ) 96T #B-PAYMENT-AMOUNT-5 ( EM = ZZZZ,ZZZ,ZZ9.99- ) 113T #B-PAYMENT-AMOUNT-A ( EM = ZZZZ,ZZZ,ZZ9.99- ) / 28T #B-PAYMENT-AMOUNT-8 ( EM = ZZZZ,ZZZ,ZZ9.99- )
            ("XXX-XX-XXXX"),new TabSetting(13),ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Type_Of_Tin(),new TabSetting(16),ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Contract_Payee(), 
            new ReportEditMask ("XXXXXXXX-XX"),new TabSetting(28),ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Payment_Amount_1(), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99-"),new 
            TabSetting(45),ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Payment_Amount_2(), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99-"),new TabSetting(62),ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Payment_Amount_3(), 
            new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99-"),new TabSetting(79),ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Payment_Amount_4(), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99-"),new 
            TabSetting(96),ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Payment_Amount_5(), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99-"),new TabSetting(113),ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Payment_Amount_A(), 
            new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(28),ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Payment_Amount_8(), new ReportEditMask 
            ("ZZZZ,ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Name_Line_1());                                 //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T #B-NAME-LINE-1
        if (Global.isEscape()) return;
        //*  BK
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Address_Line(),new TabSetting(42),              //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T #B-ADDRESS-LINE 42T 'IRA Ind.:' #B-IRA-IND 'Foreign Ind.:' #B-FOREIGN-INDICATOR 'Roth Ind.:' #B-ROTH-IND 'SEP  Ind.:' #B-SEP-IND 'RQ M-Dist Ind.:' #B-RMD-IND 'Combined St.:' #B-COMBINED-STATE
            "IRA Ind.:",ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Ira_Ind(),"Foreign Ind.:",ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Foreign_Indicator(),"Roth Ind.:",ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Roth_Ind(),
            "SEP  Ind.:",ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Sep_Ind(),"RQ M-Dist Ind.:",ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Rmd_Ind(),"Combined St.:",ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Combined_State());
        if (Global.isEscape()) return;
        if (condition(ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Foreign_Indicator().equals(" ")))                                                                                 //Natural: IF #B-FOREIGN-INDICATOR = ' '
        {
            pnd_City_State_Zip_Code.setValue(DbsUtil.compress(ldaTwrl448a.getPnd_Irs_5498_Pnd_B_City(), ldaTwrl448a.getPnd_Irs_5498_Pnd_B_State(), ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Zip())); //Natural: COMPRESS #B-CITY #B-STATE #B-ZIP INTO #CITY-STATE-ZIP-CODE
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),pnd_City_State_Zip_Code);                                                     //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T #CITY-STATE-ZIP-CODE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Foreign_Address_Line());                    //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T #B-FOREIGN-ADDRESS-LINE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_T_Record() throws Exception                                                                                                                    //Natural: WRITE-T-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------
        //*  EINCHG - START
        if (condition(ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Payment_Year().greaterOrEqual("2018")))                                                                           //Natural: IF #T-PAYMENT-YEAR GE '2018'
        {
            ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Transmitter_Control_Code().setValue("91T39");                                                                               //Natural: MOVE '91T39' TO #T-TRANSMITTER-CONTROL-CODE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EINCHG - END
        getReports().skip(1, 5);                                                                                                                                          //Natural: SKIP ( 01 ) 5
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"           T Record            ",NEWLINE,"===============================",                //Natural: WRITE ( 01 ) NOTITLE NOHDR / '           T Record            ' / '===============================' / 'RECORD-TYPE....................' #T-RECORD-TYPE / 'PAYMENT-YEAR...................' #T-PAYMENT-YEAR / 'PRIOR-YR-DATA-IND..............' #T-PRIOR-YR-DATA-IND / 'TRANSMITTERS-TIN...............' #T-TRANSMITTERS-TIN / 'TRANSMITTER-CONTROL-CODE.......' #T-TRANSMITTER-CONTROL-CODE / 'REPLACEMENT-ALPHA-CHAR.........' #T-REPLACEMENT-ALPHA-CHAR / 'TEST-FILE-INDICATOR............' #T-TEST-FILE-INDICATOR / 'FOREIGN-ENTITY-IND.............' #T-FOREIGN-ENTITY-IND / 'TRANSMITTER-NAME...............' #T-TRANSMITTER-NAME / 'TRANSMITTER-NAME-CONTINUED.....' #T-TRANSMITTER-NAME-CONTINUED / 'COMPANY-NAME...................' #T-COMPANY-NAME / 'COMPANY-NAME-CONTINUED.........' #T-COMPANY-NAME-CONTINUED / 'COMPANY-MAILING-ADDRESS........' #T-COMPANY-MAILING-ADDRESS / 'COMPANY-CITY...................' #T-COMPANY-CITY / 'COMPANY-STATE..................' #T-COMPANY-STATE / 'COMPANY-ZIP-CODE...............' #T-COMPANY-ZIP-CODE / 'TOTAL-NUMBER-OF-PAYEES.........' #T-TOTAL-NUMBER-OF-PAYEES / 'CONTACT-NAME...................' #T-CONTACT-NAME / 'CONTACT-PHONE-N-EXTENSION......' #T-CONTACT-PHONE-N-EXTENSION / 'CONTACT-EMAIL-ADDRESS..........' #T-CONTACT-EMAIL-ADDRESS / 'MAGNETIC-TAPE-FILE-IND.........' #T-MAGNETIC-TAPE-FILE-IND / 'ELECTRONIC-FILE-NAME...........' #T-ELECTRONIC-FILE-NAME / 'MEDIA-NUMBER...................' #T-MEDIA-NUMBER / 'SEQUENCE-NUMBER................' #T-SEQUENCE-NUMBER / 'VENDOR INDICATOR...............' #T-VENDOR-INDICATOR
            NEWLINE,"RECORD-TYPE....................",ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Record_Type(),NEWLINE,"PAYMENT-YEAR...................",ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Payment_Year(),
            NEWLINE,"PRIOR-YR-DATA-IND..............",ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Prior_Yr_Data_Ind(),NEWLINE,"TRANSMITTERS-TIN...............",ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Transmitters_Tin(),
            NEWLINE,"TRANSMITTER-CONTROL-CODE.......",ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Transmitter_Control_Code(),NEWLINE,"REPLACEMENT-ALPHA-CHAR.........",
            ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Replacement_Alpha_Char(),NEWLINE,"TEST-FILE-INDICATOR............",ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Test_File_Indicator(),
            NEWLINE,"FOREIGN-ENTITY-IND.............",ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Foreign_Entity_Ind(),NEWLINE,"TRANSMITTER-NAME...............",ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Transmitter_Name(),
            NEWLINE,"TRANSMITTER-NAME-CONTINUED.....",ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Transmitter_Name_Continued(),NEWLINE,"COMPANY-NAME...................",
            ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Company_Name(),NEWLINE,"COMPANY-NAME-CONTINUED.........",ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Company_Name_Continued(),
            NEWLINE,"COMPANY-MAILING-ADDRESS........",ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Company_Mailing_Address(),NEWLINE,"COMPANY-CITY...................",
            ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Company_City(),NEWLINE,"COMPANY-STATE..................",ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Company_State(),
            NEWLINE,"COMPANY-ZIP-CODE...............",ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Company_Zip_Code(),NEWLINE,"TOTAL-NUMBER-OF-PAYEES.........",ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Total_Number_Of_Payees(),
            NEWLINE,"CONTACT-NAME...................",ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Contact_Name(),NEWLINE,"CONTACT-PHONE-N-EXTENSION......",ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Contact_Phone_N_Extension(),
            NEWLINE,"CONTACT-EMAIL-ADDRESS..........",ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Contact_Email_Address(),NEWLINE,"MAGNETIC-TAPE-FILE-IND.........",
            ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Magnetic_Tape_File_Ind(),NEWLINE,"ELECTRONIC-FILE-NAME...........",ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Electronic_File_Name(),
            NEWLINE,"MEDIA-NUMBER...................",ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Media_Number(),NEWLINE,"SEQUENCE-NUMBER................",ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Sequence_Number(),
            NEWLINE,"VENDOR INDICATOR...............",ldaTwrl448a.getPnd_Irs_5498_Pnd_T_Vendor_Indicator());
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
    }
    private void sub_Write_A_Record() throws Exception                                                                                                                    //Natural: WRITE-A-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------
        getReports().skip(1, 5);                                                                                                                                          //Natural: SKIP ( 01 ) 5
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"           A Record            ",NEWLINE,"===============================",                //Natural: WRITE ( 01 ) NOTITLE NOHDR / '           A Record            ' / '===============================' / 'RECORD-TYPE....................' #A-RECORD-TYPE / 'PAYMENT-YEAR...................' #A-PAYMENT-YEAR / 'PAYER-EIN......................' #A-PAYER-EIN / 'PAYER-NAME-CONTROL.............' #A-PAYER-NAME-CONTROL / 'LAST-FILING-IND................' #A-LAST-FILING-IND / 'COMBINED-FILER.................' #A-COMBINED-FILER / 'TYPE-OF-RETURN.................' #A-TYPE-OF-RETURN / 'AMOUNT-INDICATORS..............' #A-AMOUNT-INDICATORS / 'ORIGINAL-FILE-IND..............' #A-ORIGINAL-FILE-IND / 'REPLACEMENT-FILE-IND...........' #A-REPLACEMENT-FILE-IND / 'CORRECTION-FILE-IND............' #A-CORRECTION-FILE-IND / 'FORIEGN-CORP-INDICATOR.........' #A-FORIEGN-CORP-INDICATOR / 'PAYER-NAME-1...................' #A-PAYER-NAME-1 / 'PAYER-NAME-2...................' #A-PAYER-NAME-2 / 'TRANSFER-AGENT-INDICATOR.......' #A-TRANSFER-AGENT-INDICATOR / 'PAYER-SHIPPING-ADDRESS.........' #A-PAYER-SHIPPING-ADDRESS / 'PAYER-CITY.....................' #A-PAYER-CITY / 'PAYER-STATE....................' #A-PAYER-STATE / 'PAYER-ZIP......................' #A-PAYER-ZIP / 'PAYER-PHONE-NO.................' #A-PAYER-PHONE-NO / 'SEQUENCE-NUMBER................' #A-SEQUENCE-NUMBER
            NEWLINE,"RECORD-TYPE....................",ldaTwrl448a.getPnd_Irs_5498_Pnd_A_Record_Type(),NEWLINE,"PAYMENT-YEAR...................",ldaTwrl448a.getPnd_Irs_5498_Pnd_A_Payment_Year(),
            NEWLINE,"PAYER-EIN......................",ldaTwrl448a.getPnd_Irs_5498_Pnd_A_Payer_Ein(),NEWLINE,"PAYER-NAME-CONTROL.............",ldaTwrl448a.getPnd_Irs_5498_Pnd_A_Payer_Name_Control(),
            NEWLINE,"LAST-FILING-IND................",ldaTwrl448a.getPnd_Irs_5498_Pnd_A_Last_Filing_Ind(),NEWLINE,"COMBINED-FILER.................",ldaTwrl448a.getPnd_Irs_5498_Pnd_A_Combined_Filer(),
            NEWLINE,"TYPE-OF-RETURN.................",ldaTwrl448a.getPnd_Irs_5498_Pnd_A_Type_Of_Return(),NEWLINE,"AMOUNT-INDICATORS..............",ldaTwrl448a.getPnd_Irs_5498_Pnd_A_Amount_Indicators(),
            NEWLINE,"ORIGINAL-FILE-IND..............",ldaTwrl448a.getPnd_Irs_5498_Pnd_A_Original_File_Ind(),NEWLINE,"REPLACEMENT-FILE-IND...........",ldaTwrl448a.getPnd_Irs_5498_Pnd_A_Replacement_File_Ind(),
            NEWLINE,"CORRECTION-FILE-IND............",ldaTwrl448a.getPnd_Irs_5498_Pnd_A_Correction_File_Ind(),NEWLINE,"FORIEGN-CORP-INDICATOR.........",
            ldaTwrl448a.getPnd_Irs_5498_Pnd_A_Foriegn_Corp_Indicator(),NEWLINE,"PAYER-NAME-1...................",ldaTwrl448a.getPnd_Irs_5498_Pnd_A_Payer_Name_1(),
            NEWLINE,"PAYER-NAME-2...................",ldaTwrl448a.getPnd_Irs_5498_Pnd_A_Payer_Name_2(),NEWLINE,"TRANSFER-AGENT-INDICATOR.......",ldaTwrl448a.getPnd_Irs_5498_Pnd_A_Transfer_Agent_Indicator(),
            NEWLINE,"PAYER-SHIPPING-ADDRESS.........",ldaTwrl448a.getPnd_Irs_5498_Pnd_A_Payer_Shipping_Address(),NEWLINE,"PAYER-CITY.....................",
            ldaTwrl448a.getPnd_Irs_5498_Pnd_A_Payer_City(),NEWLINE,"PAYER-STATE....................",ldaTwrl448a.getPnd_Irs_5498_Pnd_A_Payer_State(),NEWLINE,
            "PAYER-ZIP......................",ldaTwrl448a.getPnd_Irs_5498_Pnd_A_Payer_Zip(),NEWLINE,"PAYER-PHONE-NO.................",ldaTwrl448a.getPnd_Irs_5498_Pnd_A_Payer_Phone_No(),
            NEWLINE,"SEQUENCE-NUMBER................",ldaTwrl448a.getPnd_Irs_5498_Pnd_A_Sequence_Number());
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
    }
    private void sub_Write_C_Record() throws Exception                                                                                                                    //Natural: WRITE-C-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------
        getReports().skip(1, 5);                                                                                                                                          //Natural: SKIP ( 01 ) 5
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"           C Record            ",NEWLINE,"===============================",                //Natural: WRITE ( 01 ) NOTITLE NOHDR / '           C Record            ' / '===============================' / 'RECORD-TYPE....................' #C-RECORD-TYPE / 'NUMBER-OF-PAYEES...............' #C-NUMBER-OF-PAYEES / 'TOTAL-1........................' #C-TOTAL-1 / 'TOTAL-2........................' #C-TOTAL-2 / 'TOTAL-3........................' #C-TOTAL-3 / 'TOTAL-4........................' #C-TOTAL-4 / 'TOTAL-5........................' #C-TOTAL-5 / 'TOTAL-6........................' #C-TOTAL-6 / 'TOTAL-7........................' #C-TOTAL-7 / 'TOTAL-8........................' #C-TOTAL-8 / 'TOTAL-9........................' #C-TOTAL-9 / 'TOTAL-A........................' #C-TOTAL-A / 'TOTAL-B........................' #C-TOTAL-B / 'TOTAL-C........................' #C-TOTAL-C / 'SEQUENCE-NUMBER................' #C-SEQUENCE-NUMBER
            NEWLINE,"RECORD-TYPE....................",ldaTwrl448a.getPnd_Irs_5498_Pnd_C_Record_Type(),NEWLINE,"NUMBER-OF-PAYEES...............",ldaTwrl448a.getPnd_Irs_5498_Pnd_C_Number_Of_Payees(),
            NEWLINE,"TOTAL-1........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_C_Total_1(),NEWLINE,"TOTAL-2........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_C_Total_2(),
            NEWLINE,"TOTAL-3........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_C_Total_3(),NEWLINE,"TOTAL-4........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_C_Total_4(),
            NEWLINE,"TOTAL-5........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_C_Total_5(),NEWLINE,"TOTAL-6........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_C_Total_6(),
            NEWLINE,"TOTAL-7........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_C_Total_7(),NEWLINE,"TOTAL-8........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_C_Total_8(),
            NEWLINE,"TOTAL-9........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_C_Total_9(),NEWLINE,"TOTAL-A........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_C_Total_A(),
            NEWLINE,"TOTAL-B........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_C_Total_B(),NEWLINE,"TOTAL-C........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_C_Total_C(),
            NEWLINE,"SEQUENCE-NUMBER................",ldaTwrl448a.getPnd_Irs_5498_Pnd_C_Sequence_Number());
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
    }
    private void sub_Write_K_Record() throws Exception                                                                                                                    //Natural: WRITE-K-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------
        getReports().skip(1, 5);                                                                                                                                          //Natural: SKIP ( 01 ) 5
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"           K Record            ",NEWLINE,"===============================",                //Natural: WRITE ( 01 ) NOTITLE NOHDR / '           K Record            ' / '===============================' / 'RECORD-TYPE....................' #K-RECORD-TYPE / 'NUMBER-OF-PAYEES...............' #K-NUMBER-OF-PAYEES / 'TOTAL-1........................' #K-TOTAL-1 / 'TOTAL-2........................' #K-TOTAL-2 / 'TOTAL-3........................' #K-TOTAL-3 / 'TOTAL-4........................' #K-TOTAL-4 / 'TOTAL-5........................' #K-TOTAL-5 / 'TOTAL-6........................' #K-TOTAL-6 / 'TOTAL-7........................' #K-TOTAL-7 / 'TOTAL-8........................' #K-TOTAL-8 / 'TOTAL-9........................' #K-TOTAL-9 / 'TOTAL-A........................' #K-TOTAL-A / 'TOTAL-B........................' #K-TOTAL-B / 'TOTAL-C........................' #K-TOTAL-C / 'SEQUENCE-NUMBER................' #K-SEQUENCE-NUMBER / 'STATE-TAX-WITHHELD.............' #K-ST-TAX-WITHHELD / 'LOCAL-TAX-WITHHELD.............' #K-LC-TAX-WITHHELD / 'LOCAL-TAX-WITHHELD.............' #K-LC-TAX-WITHHELD / 'STATE-CODE.....................' #K-STATE-CODE
            NEWLINE,"RECORD-TYPE....................",ldaTwrl448a.getPnd_Irs_5498_Pnd_K_Record_Type(),NEWLINE,"NUMBER-OF-PAYEES...............",ldaTwrl448a.getPnd_Irs_5498_Pnd_K_Number_Of_Payees(),
            NEWLINE,"TOTAL-1........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_K_Total_1(),NEWLINE,"TOTAL-2........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_K_Total_2(),
            NEWLINE,"TOTAL-3........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_K_Total_3(),NEWLINE,"TOTAL-4........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_K_Total_4(),
            NEWLINE,"TOTAL-5........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_K_Total_5(),NEWLINE,"TOTAL-6........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_K_Total_6(),
            NEWLINE,"TOTAL-7........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_K_Total_7(),NEWLINE,"TOTAL-8........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_K_Total_8(),
            NEWLINE,"TOTAL-9........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_K_Total_9(),NEWLINE,"TOTAL-A........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_K_Total_A(),
            NEWLINE,"TOTAL-B........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_K_Total_B(),NEWLINE,"TOTAL-C........................",ldaTwrl448a.getPnd_Irs_5498_Pnd_K_Total_C(),
            NEWLINE,"SEQUENCE-NUMBER................",ldaTwrl448a.getPnd_Irs_5498_Pnd_K_Sequence_Number(),NEWLINE,"STATE-TAX-WITHHELD.............",ldaTwrl448a.getPnd_Irs_5498_Pnd_K_St_Tax_Withheld(),
            NEWLINE,"LOCAL-TAX-WITHHELD.............",ldaTwrl448a.getPnd_Irs_5498_Pnd_K_Lc_Tax_Withheld(),NEWLINE,"LOCAL-TAX-WITHHELD.............",ldaTwrl448a.getPnd_Irs_5498_Pnd_K_Lc_Tax_Withheld(),
            NEWLINE,"STATE-CODE.....................",ldaTwrl448a.getPnd_Irs_5498_Pnd_K_State_Code());
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
    }
    private void sub_Write_F_Record() throws Exception                                                                                                                    //Natural: WRITE-F-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------
        getReports().skip(1, 5);                                                                                                                                          //Natural: SKIP ( 01 ) 5
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"           F Record            ",NEWLINE,"===============================",                //Natural: WRITE ( 01 ) NOTITLE NOHDR / '           F Record            ' / '===============================' / 'RECORD-TYPE....................' #F-RECORD-TYPE / 'NUMBER-OF "A" RECORDS..........' #F-NUMBER-OF-A-RECORDS / 'NUMBER-OF "B" RECORDS..........' #F-TOTAL-B-RECORDS / 'SEQUENCE-NUMBER................' #F-SEQUENCE-NUMBER
            NEWLINE,"RECORD-TYPE....................",ldaTwrl448a.getPnd_Irs_5498_Pnd_F_Record_Type(),NEWLINE,"NUMBER-OF 'A' RECORDS..........",ldaTwrl448a.getPnd_Irs_5498_Pnd_F_Number_Of_A_Records(),
            NEWLINE,"NUMBER-OF 'B' RECORDS..........",ldaTwrl448a.getPnd_Irs_5498_Pnd_F_Total_B_Records(),NEWLINE,"SEQUENCE-NUMBER................",ldaTwrl448a.getPnd_Irs_5498_Pnd_F_Sequence_Number());
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        getReports().write(0, NEWLINE,new TabSetting(1),"No. Of T Records...........................",pnd_T_Ctr,NEWLINE,new TabSetting(1),"No. Of A Records...........................",pnd_A_Ctr,NEWLINE,new  //Natural: WRITE ( 00 ) / 01T 'No. Of T Records...........................' #T-CTR / 01T 'No. Of A Records...........................' #A-CTR / 01T 'No. Of B Records...........................' #B-CTR / 01T 'No. Of C Records...........................' #C-CTR / 01T 'No. Of F Records...........................' #F-CTR
            TabSetting(1),"No. Of B Records...........................",pnd_B_Ctr,NEWLINE,new TabSetting(1),"No. Of C Records...........................",pnd_C_Ctr,NEWLINE,new 
            TabSetting(1),"No. Of F Records...........................",pnd_F_Ctr);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"No. Of T Records...........................",pnd_T_Ctr,NEWLINE,new TabSetting(1),"No. Of A Records...........................",pnd_A_Ctr,NEWLINE,new  //Natural: WRITE ( 01 ) / 01T 'No. Of T Records...........................' #T-CTR / 01T 'No. Of A Records...........................' #A-CTR / 01T 'No. Of B Records...........................' #B-CTR / 01T 'No. Of C Records...........................' #C-CTR / 01T 'No. Of F Records...........................' #F-CTR
            TabSetting(1),"No. Of B Records...........................",pnd_B_Ctr,NEWLINE,new TabSetting(1),"No. Of C Records...........................",pnd_C_Ctr,NEWLINE,new 
            TabSetting(1),"No. Of F Records...........................",pnd_F_Ctr);
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(48),"IRS 5498 Contributions Detail Report",new  //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 48T 'IRS 5498 Contributions Detail Report' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"Record Type...",ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Record_Type(),new                     //Natural: WRITE ( 01 ) NOTITLE 01T 'Record Type...' #B-RECORD-TYPE 20T 'Tax Year......' #B-PAYMENT-YEAR
                        TabSetting(20),"Tax Year......",ldaTwrl448a.getPnd_Irs_5498_Pnd_B_Payment_Year());
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1 LINES
                    //*  BK
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"Tax ID Num.",new TabSetting(13),"TY",new TabSetting(16),"Cntrct Paye",new               //Natural: WRITE ( 01 ) NOTITLE 01T 'Tax ID Num.' 13T 'TY' 16T 'Cntrct Paye' 28T ' Amt1    Classic' 45T ' Amt2   Rollover' 62T ' Amt3  Roth Conv' 79T ' AMT4 RECHARACT.' 96T ' AMT5     F.M.V.' 113T ' AMT10      ROTH' / 28T ' AMT8    SEP'
                        TabSetting(28)," Amt1    Classic",new TabSetting(45)," Amt2   Rollover",new TabSetting(62)," Amt3  Roth Conv",new TabSetting(79)," AMT4 RECHARACT.",new 
                        TabSetting(96)," AMT5     F.M.V.",new TabSetting(113)," AMT10      ROTH",NEWLINE,new TabSetting(28)," AMT8    SEP");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"===========",new TabSetting(13),"==",new TabSetting(16),"===========",new               //Natural: WRITE ( 01 ) NOTITLE 01T '===========' 13T '==' 16T '===========' 28T '================' 45T '================' 62T '================' 79T '================' 96T '================' 113T '================'
                        TabSetting(28),"================",new TabSetting(45),"================",new TabSetting(62),"================",new TabSetting(79),"================",new 
                        TabSetting(96),"================",new TabSetting(113),"================");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
        sub_Error_Display_Start();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
        sub_Error_Display_End();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
    }
}
