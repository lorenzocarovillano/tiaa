/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:44:29 PM
**        * FROM NATURAL PROGRAM : Twrp8110
************************************************************
**        * FILE NAME            : Twrp8110.java
**        * CLASS NAME           : Twrp8110
**        * INSTANCE NAME        : Twrp8110
************************************************************
************************************************************************
* PROGRAM     :  TWRP8110
* SYSTEM      :  TAX WITHHOLDINGS AND REPORTING SYSTEM.
* AUTHOR      :  RIAD A. LOUTFI
* PURPOSE     :  CREATE COMPANY CODES TABLE DATA BASE FILE (003/098).
*                TABLE NUMBER 6
* DATE        :  09/11/2000
*
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp8110 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_rcomp;
    private DbsField rcomp_Tircntl_Tbl_Nbr;
    private DbsField rcomp_Tircntl_Tax_Year;
    private DbsField rcomp_Tircntl_Seq_Nbr;

    private DbsGroup rcomp_Tircntl_Company_Info_Tbl;
    private DbsField rcomp_Tircntl_Company_Code;
    private DbsField rcomp_Tircntl_Company_Name;
    private DbsField rcomp_Tircntl_Company_Tax_Id;

    private DataAccessProgramView vw_ucomp;
    private DbsField ucomp_Tircntl_Tbl_Nbr;
    private DbsField ucomp_Tircntl_Tax_Year;
    private DbsField ucomp_Tircntl_Seq_Nbr;

    private DbsGroup ucomp_Tircntl_Company_Info_Tbl;
    private DbsField ucomp_Tircntl_Company_Code;
    private DbsField ucomp_Tircntl_Company_Name;
    private DbsField ucomp_Tircntl_Company_Tax_Id;

    private DataAccessProgramView vw_scomp;
    private DbsField scomp_Tircntl_Tbl_Nbr;
    private DbsField scomp_Tircntl_Tax_Year;
    private DbsField scomp_Tircntl_Seq_Nbr;

    private DbsGroup scomp_Tircntl_Company_Info_Tbl;
    private DbsField scomp_Tircntl_Company_Code;
    private DbsField scomp_Tircntl_Company_Name;
    private DbsField scomp_Tircntl_Company_Tax_Id;
    private DbsField pnd_Read1_Ctr;
    private DbsField pnd_Read2_Ctr;
    private DbsField pnd_Updt_Ctr;
    private DbsField pnd_Store_Ctr;
    private DbsField pnd_Old_Tax_Year;

    private DbsGroup pnd_Old_Tax_Year__R_Field_1;
    private DbsField pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N;
    private DbsField pnd_New_Tax_Year;

    private DbsGroup pnd_New_Tax_Year__R_Field_2;
    private DbsField pnd_New_Tax_Year_Pnd_New_Tax_Year_N;
    private DbsField pnd_Super1;

    private DbsGroup pnd_Super1__R_Field_3;
    private DbsField pnd_Super1_Pnd_S1_Tbl_Nbr;
    private DbsField pnd_Super1_Pnd_S1_Tax_Year;
    private DbsField pnd_Super1_Pnd_S1_Tbl_Comp_Name;
    private DbsField pnd_Super2;

    private DbsGroup pnd_Super2__R_Field_4;
    private DbsField pnd_Super2_Pnd_S2_Tbl_Nbr;
    private DbsField pnd_Super2_Pnd_S2_Tax_Year;
    private DbsField pnd_Super2_Pnd_S2_Tbl_Comp_Name;
    private DbsField pnd_New_Record_Found;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_rcomp = new DataAccessProgramView(new NameInfo("vw_rcomp", "RCOMP"), "TIRCNTL_COMPANY_INFO_TBL_VIEW", "TIR_CONTROL");
        rcomp_Tircntl_Tbl_Nbr = vw_rcomp.getRecord().newFieldInGroup("rcomp_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        rcomp_Tircntl_Tax_Year = vw_rcomp.getRecord().newFieldInGroup("rcomp_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        rcomp_Tircntl_Seq_Nbr = vw_rcomp.getRecord().newFieldInGroup("rcomp_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        rcomp_Tircntl_Company_Info_Tbl = vw_rcomp.getRecord().newGroupInGroup("RCOMP_TIRCNTL_COMPANY_INFO_TBL", "TIRCNTL-COMPANY-INFO-TBL");
        rcomp_Tircntl_Company_Code = rcomp_Tircntl_Company_Info_Tbl.newFieldInGroup("rcomp_Tircntl_Company_Code", "TIRCNTL-COMPANY-CODE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_COMPANY_CODE");
        rcomp_Tircntl_Company_Name = rcomp_Tircntl_Company_Info_Tbl.newFieldInGroup("rcomp_Tircntl_Company_Name", "TIRCNTL-COMPANY-NAME", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "TIRCNTL_COMPANY_NAME");
        rcomp_Tircntl_Company_Tax_Id = rcomp_Tircntl_Company_Info_Tbl.newFieldInGroup("rcomp_Tircntl_Company_Tax_Id", "TIRCNTL-COMPANY-TAX-ID", FieldType.STRING, 
            14, RepeatingFieldStrategy.None, "TIRCNTL_COMPANY_TAX_ID");
        registerRecord(vw_rcomp);

        vw_ucomp = new DataAccessProgramView(new NameInfo("vw_ucomp", "UCOMP"), "TIRCNTL_COMPANY_INFO_TBL_VIEW", "TIR_CONTROL");
        ucomp_Tircntl_Tbl_Nbr = vw_ucomp.getRecord().newFieldInGroup("ucomp_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        ucomp_Tircntl_Tax_Year = vw_ucomp.getRecord().newFieldInGroup("ucomp_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        ucomp_Tircntl_Seq_Nbr = vw_ucomp.getRecord().newFieldInGroup("ucomp_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        ucomp_Tircntl_Company_Info_Tbl = vw_ucomp.getRecord().newGroupInGroup("UCOMP_TIRCNTL_COMPANY_INFO_TBL", "TIRCNTL-COMPANY-INFO-TBL");
        ucomp_Tircntl_Company_Code = ucomp_Tircntl_Company_Info_Tbl.newFieldInGroup("ucomp_Tircntl_Company_Code", "TIRCNTL-COMPANY-CODE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_COMPANY_CODE");
        ucomp_Tircntl_Company_Name = ucomp_Tircntl_Company_Info_Tbl.newFieldInGroup("ucomp_Tircntl_Company_Name", "TIRCNTL-COMPANY-NAME", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "TIRCNTL_COMPANY_NAME");
        ucomp_Tircntl_Company_Tax_Id = ucomp_Tircntl_Company_Info_Tbl.newFieldInGroup("ucomp_Tircntl_Company_Tax_Id", "TIRCNTL-COMPANY-TAX-ID", FieldType.STRING, 
            14, RepeatingFieldStrategy.None, "TIRCNTL_COMPANY_TAX_ID");
        registerRecord(vw_ucomp);

        vw_scomp = new DataAccessProgramView(new NameInfo("vw_scomp", "SCOMP"), "TIRCNTL_COMPANY_INFO_TBL_VIEW", "TIR_CONTROL");
        scomp_Tircntl_Tbl_Nbr = vw_scomp.getRecord().newFieldInGroup("scomp_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        scomp_Tircntl_Tax_Year = vw_scomp.getRecord().newFieldInGroup("scomp_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        scomp_Tircntl_Seq_Nbr = vw_scomp.getRecord().newFieldInGroup("scomp_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        scomp_Tircntl_Company_Info_Tbl = vw_scomp.getRecord().newGroupInGroup("SCOMP_TIRCNTL_COMPANY_INFO_TBL", "TIRCNTL-COMPANY-INFO-TBL");
        scomp_Tircntl_Company_Code = scomp_Tircntl_Company_Info_Tbl.newFieldInGroup("scomp_Tircntl_Company_Code", "TIRCNTL-COMPANY-CODE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_COMPANY_CODE");
        scomp_Tircntl_Company_Name = scomp_Tircntl_Company_Info_Tbl.newFieldInGroup("scomp_Tircntl_Company_Name", "TIRCNTL-COMPANY-NAME", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "TIRCNTL_COMPANY_NAME");
        scomp_Tircntl_Company_Tax_Id = scomp_Tircntl_Company_Info_Tbl.newFieldInGroup("scomp_Tircntl_Company_Tax_Id", "TIRCNTL-COMPANY-TAX-ID", FieldType.STRING, 
            14, RepeatingFieldStrategy.None, "TIRCNTL_COMPANY_TAX_ID");
        registerRecord(vw_scomp);

        pnd_Read1_Ctr = localVariables.newFieldInRecord("pnd_Read1_Ctr", "#READ1-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Read2_Ctr = localVariables.newFieldInRecord("pnd_Read2_Ctr", "#READ2-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Updt_Ctr = localVariables.newFieldInRecord("pnd_Updt_Ctr", "#UPDT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Store_Ctr = localVariables.newFieldInRecord("pnd_Store_Ctr", "#STORE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Old_Tax_Year = localVariables.newFieldInRecord("pnd_Old_Tax_Year", "#OLD-TAX-YEAR", FieldType.STRING, 4);

        pnd_Old_Tax_Year__R_Field_1 = localVariables.newGroupInRecord("pnd_Old_Tax_Year__R_Field_1", "REDEFINE", pnd_Old_Tax_Year);
        pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N = pnd_Old_Tax_Year__R_Field_1.newFieldInGroup("pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N", "#OLD-TAX-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_New_Tax_Year = localVariables.newFieldInRecord("pnd_New_Tax_Year", "#NEW-TAX-YEAR", FieldType.STRING, 4);

        pnd_New_Tax_Year__R_Field_2 = localVariables.newGroupInRecord("pnd_New_Tax_Year__R_Field_2", "REDEFINE", pnd_New_Tax_Year);
        pnd_New_Tax_Year_Pnd_New_Tax_Year_N = pnd_New_Tax_Year__R_Field_2.newFieldInGroup("pnd_New_Tax_Year_Pnd_New_Tax_Year_N", "#NEW-TAX-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_Super1 = localVariables.newFieldInRecord("pnd_Super1", "#SUPER1", FieldType.STRING, 9);

        pnd_Super1__R_Field_3 = localVariables.newGroupInRecord("pnd_Super1__R_Field_3", "REDEFINE", pnd_Super1);
        pnd_Super1_Pnd_S1_Tbl_Nbr = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Tbl_Nbr", "#S1-TBL-NBR", FieldType.NUMERIC, 1);
        pnd_Super1_Pnd_S1_Tax_Year = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Tax_Year", "#S1-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Super1_Pnd_S1_Tbl_Comp_Name = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Tbl_Comp_Name", "#S1-TBL-COMP-NAME", FieldType.STRING, 
            4);
        pnd_Super2 = localVariables.newFieldInRecord("pnd_Super2", "#SUPER2", FieldType.STRING, 9);

        pnd_Super2__R_Field_4 = localVariables.newGroupInRecord("pnd_Super2__R_Field_4", "REDEFINE", pnd_Super2);
        pnd_Super2_Pnd_S2_Tbl_Nbr = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Tbl_Nbr", "#S2-TBL-NBR", FieldType.NUMERIC, 1);
        pnd_Super2_Pnd_S2_Tax_Year = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Tax_Year", "#S2-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Super2_Pnd_S2_Tbl_Comp_Name = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Tbl_Comp_Name", "#S2-TBL-COMP-NAME", FieldType.STRING, 
            4);
        pnd_New_Record_Found = localVariables.newFieldInRecord("pnd_New_Record_Found", "#NEW-RECORD-FOUND", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_rcomp.reset();
        vw_ucomp.reset();
        vw_scomp.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp8110() throws Exception
    {
        super("Twrp8110");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp8110|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Old_Tax_Year,pnd_New_Tax_Year);                                                                    //Natural: INPUT #OLD-TAX-YEAR #NEW-TAX-YEAR
                getReports().display(0, pnd_Old_Tax_Year,pnd_New_Tax_Year);                                                                                               //Natural: DISPLAY ( 00 ) #OLD-TAX-YEAR #NEW-TAX-YEAR
                if (Global.isEscape()) return;
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                pnd_Super1_Pnd_S1_Tbl_Nbr.setValue(6);                                                                                                                    //Natural: ASSIGN #S1-TBL-NBR := 6
                pnd_Super1_Pnd_S1_Tax_Year.setValue(pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N);                                                                                 //Natural: ASSIGN #S1-TAX-YEAR := #OLD-TAX-YEAR-N
                pnd_Super1_Pnd_S1_Tbl_Comp_Name.setValue(" ");                                                                                                            //Natural: ASSIGN #S1-TBL-COMP-NAME := ' '
                vw_rcomp.startDatabaseRead                                                                                                                                //Natural: READ RCOMP WITH TIRCNTL-NBR-YEAR-NAME = #SUPER1
                (
                "READ01",
                new Wc[] { new Wc("TIRCNTL_NBR_YEAR_NAME", ">=", pnd_Super1, WcType.BY) },
                new Oc[] { new Oc("TIRCNTL_NBR_YEAR_NAME", "ASC") }
                );
                READ01:
                while (condition(vw_rcomp.readNextRow("READ01")))
                {
                    if (condition(rcomp_Tircntl_Tbl_Nbr.equals(pnd_Super1_Pnd_S1_Tbl_Nbr) && rcomp_Tircntl_Tax_Year.equals(pnd_Super1_Pnd_S1_Tax_Year)))                  //Natural: IF RCOMP.TIRCNTL-TBL-NBR = #S1-TBL-NBR AND RCOMP.TIRCNTL-TAX-YEAR = #S1-TAX-YEAR
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Read1_Ctr.nadd(1);                                                                                                                                //Natural: ADD 1 TO #READ1-CTR
                    pnd_New_Record_Found.setValue(false);                                                                                                                 //Natural: ASSIGN #NEW-RECORD-FOUND := FALSE
                    pnd_Super2_Pnd_S2_Tbl_Nbr.setValue(6);                                                                                                                //Natural: ASSIGN #S2-TBL-NBR := 6
                    pnd_Super2_Pnd_S2_Tax_Year.setValue(pnd_New_Tax_Year_Pnd_New_Tax_Year_N);                                                                             //Natural: ASSIGN #S2-TAX-YEAR := #NEW-TAX-YEAR-N
                    pnd_Super2_Pnd_S2_Tbl_Comp_Name.setValue(rcomp_Tircntl_Company_Name);                                                                                 //Natural: ASSIGN #S2-TBL-COMP-NAME := RCOMP.TIRCNTL-COMPANY-NAME
                    vw_ucomp.startDatabaseRead                                                                                                                            //Natural: READ ( 1 ) UCOMP WITH TIRCNTL-NBR-YEAR-NAME = #SUPER2
                    (
                    "RL2",
                    new Wc[] { new Wc("TIRCNTL_NBR_YEAR_NAME", ">=", pnd_Super2, WcType.BY) },
                    new Oc[] { new Oc("TIRCNTL_NBR_YEAR_NAME", "ASC") },
                    1
                    );
                    RL2:
                    while (condition(vw_ucomp.readNextRow("RL2")))
                    {
                        if (condition(ucomp_Tircntl_Tbl_Nbr.equals(pnd_Super2_Pnd_S2_Tbl_Nbr) && ucomp_Tircntl_Tax_Year.equals(pnd_Super2_Pnd_S2_Tax_Year)                //Natural: IF UCOMP.TIRCNTL-TBL-NBR = #S2-TBL-NBR AND UCOMP.TIRCNTL-TAX-YEAR = #S2-TAX-YEAR AND UCOMP.TIRCNTL-COMPANY-NAME = #S2-TBL-COMP-NAME
                            && ucomp_Tircntl_Company_Name.equals(pnd_Super2_Pnd_S2_Tbl_Comp_Name)))
                        {
                            pnd_New_Record_Found.setValue(true);                                                                                                          //Natural: ASSIGN #NEW-RECORD-FOUND := TRUE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Read2_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #READ2-CTR
                        if (condition(ucomp_Tircntl_Seq_Nbr.equals(rcomp_Tircntl_Seq_Nbr) && ucomp_Tircntl_Company_Code.equals(rcomp_Tircntl_Company_Code)                //Natural: IF UCOMP.TIRCNTL-SEQ-NBR = RCOMP.TIRCNTL-SEQ-NBR AND UCOMP.TIRCNTL-COMPANY-CODE = RCOMP.TIRCNTL-COMPANY-CODE AND UCOMP.TIRCNTL-COMPANY-TAX-ID = RCOMP.TIRCNTL-COMPANY-TAX-ID
                            && ucomp_Tircntl_Company_Tax_Id.equals(rcomp_Tircntl_Company_Tax_Id)))
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            G1:                                                                                                                                           //Natural: GET UCOMP *ISN ( RL2. )
                            vw_ucomp.readByID(vw_ucomp.getAstISN("RL2"), "G1");
                            ucomp_Tircntl_Seq_Nbr.setValue(rcomp_Tircntl_Seq_Nbr);                                                                                        //Natural: ASSIGN UCOMP.TIRCNTL-SEQ-NBR := RCOMP.TIRCNTL-SEQ-NBR
                            ucomp_Tircntl_Company_Code.setValue(rcomp_Tircntl_Company_Code);                                                                              //Natural: ASSIGN UCOMP.TIRCNTL-COMPANY-CODE := RCOMP.TIRCNTL-COMPANY-CODE
                            ucomp_Tircntl_Company_Tax_Id.setValue(rcomp_Tircntl_Company_Tax_Id);                                                                          //Natural: ASSIGN UCOMP.TIRCNTL-COMPANY-TAX-ID := RCOMP.TIRCNTL-COMPANY-TAX-ID
                            vw_ucomp.updateDBRow("G1");                                                                                                                   //Natural: UPDATE ( G1. )
                            getCurrentProcessState().getDbConv().dbCommit();                                                                                              //Natural: END TRANSACTION
                            pnd_Updt_Ctr.nadd(1);                                                                                                                         //Natural: ADD 1 TO #UPDT-CTR
                        }                                                                                                                                                 //Natural: END-IF
                        //*  (RL2.)
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_New_Record_Found.equals(false)))                                                                                                    //Natural: IF #NEW-RECORD-FOUND = FALSE
                    {
                        scomp_Tircntl_Tbl_Nbr.setValue(rcomp_Tircntl_Tbl_Nbr);                                                                                            //Natural: ASSIGN SCOMP.TIRCNTL-TBL-NBR := RCOMP.TIRCNTL-TBL-NBR
                        scomp_Tircntl_Tax_Year.setValue(pnd_New_Tax_Year_Pnd_New_Tax_Year_N);                                                                             //Natural: ASSIGN SCOMP.TIRCNTL-TAX-YEAR := #NEW-TAX-YEAR-N
                        scomp_Tircntl_Seq_Nbr.setValue(rcomp_Tircntl_Seq_Nbr);                                                                                            //Natural: ASSIGN SCOMP.TIRCNTL-SEQ-NBR := RCOMP.TIRCNTL-SEQ-NBR
                        scomp_Tircntl_Company_Code.setValue(rcomp_Tircntl_Company_Code);                                                                                  //Natural: ASSIGN SCOMP.TIRCNTL-COMPANY-CODE := RCOMP.TIRCNTL-COMPANY-CODE
                        scomp_Tircntl_Company_Name.setValue(rcomp_Tircntl_Company_Name);                                                                                  //Natural: ASSIGN SCOMP.TIRCNTL-COMPANY-NAME := RCOMP.TIRCNTL-COMPANY-NAME
                        scomp_Tircntl_Company_Tax_Id.setValue(rcomp_Tircntl_Company_Tax_Id);                                                                              //Natural: ASSIGN SCOMP.TIRCNTL-COMPANY-TAX-ID := RCOMP.TIRCNTL-COMPANY-TAX-ID
                        vw_scomp.insertDBRow();                                                                                                                           //Natural: STORE SCOMP
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                        pnd_Store_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STORE-CTR
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (RL1.)
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                pnd_Super1_Pnd_S1_Tbl_Nbr.setValue(6);                                                                                                                    //Natural: ASSIGN #S1-TBL-NBR := 6
                pnd_Super1_Pnd_S1_Tax_Year.setValue(0);                                                                                                                   //Natural: ASSIGN #S1-TAX-YEAR := 0
                pnd_Super1_Pnd_S1_Tbl_Comp_Name.setValue(" ");                                                                                                            //Natural: ASSIGN #S1-TBL-COMP-NAME := ' '
                vw_rcomp.startDatabaseRead                                                                                                                                //Natural: READ RCOMP WITH TIRCNTL-NBR-YEAR-NAME = #SUPER1
                (
                "RL3",
                new Wc[] { new Wc("TIRCNTL_NBR_YEAR_NAME", ">=", pnd_Super1, WcType.BY) },
                new Oc[] { new Oc("TIRCNTL_NBR_YEAR_NAME", "ASC") }
                );
                RL3:
                while (condition(vw_rcomp.readNextRow("RL3")))
                {
                    if (condition(rcomp_Tircntl_Tbl_Nbr.equals(6)))                                                                                                       //Natural: IF RCOMP.TIRCNTL-TBL-NBR = 6
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),rcomp_Tircntl_Tbl_Nbr,new TabSetting(5),rcomp_Tircntl_Tax_Year,new    //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T RCOMP.TIRCNTL-TBL-NBR 05T RCOMP.TIRCNTL-TAX-YEAR 11T RCOMP.TIRCNTL-SEQ-NBR 20T RCOMP.TIRCNTL-COMPANY-CODE 27T RCOMP.TIRCNTL-COMPANY-NAME 35T RCOMP.TIRCNTL-COMPANY-TAX-ID
                        TabSetting(11),rcomp_Tircntl_Seq_Nbr,new TabSetting(20),rcomp_Tircntl_Company_Code,new TabSetting(27),rcomp_Tircntl_Company_Name,new 
                        TabSetting(35),rcomp_Tircntl_Company_Tax_Id);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*        *ISN (RL3.)
                    //*  (RL3.)
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getReports().skip(0, 4);                                                                                                                                  //Natural: SKIP ( 00 ) 4
                getReports().print(0, "Old Tax Year Records Read................",pnd_Read1_Ctr);                                                                         //Natural: PRINT ( 00 ) 'Old Tax Year Records Read................' #READ1-CTR
                getReports().print(0, "New Tax Year Records Found...............",pnd_Read2_Ctr);                                                                         //Natural: PRINT ( 00 ) 'New Tax Year Records Found...............' #READ2-CTR
                getReports().print(0, "New Tax Year Records Updated.............",pnd_Updt_Ctr);                                                                          //Natural: PRINT ( 00 ) 'New Tax Year Records Updated.............' #UPDT-CTR
                getReports().print(0, "New Tax Year Records Stored..............",pnd_Store_Ctr);                                                                         //Natural: PRINT ( 00 ) 'New Tax Year Records Stored..............' #STORE-CTR
                getReports().skip(1, 4);                                                                                                                                  //Natural: SKIP ( 01 ) 4
                getReports().print(1, "Old Tax Year Records Read................",pnd_Read1_Ctr);                                                                         //Natural: PRINT ( 01 ) 'Old Tax Year Records Read................' #READ1-CTR
                getReports().print(1, "New Tax Year Records Found...............",pnd_Read2_Ctr);                                                                         //Natural: PRINT ( 01 ) 'New Tax Year Records Found...............' #READ2-CTR
                getReports().print(1, "New Tax Year Records Updated.............",pnd_Updt_Ctr);                                                                          //Natural: PRINT ( 01 ) 'New Tax Year Records Updated.............' #UPDT-CTR
                getReports().print(1, "New Tax Year Records Stored..............",pnd_Store_Ctr);                                                                         //Natural: PRINT ( 01 ) 'New Tax Year Records Stored..............' #STORE-CTR
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( 01 )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(56),"Company Table Report",new                //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 56T 'Company Table Report' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                    getReports().skip(1, 2);                                                                                                                              //Natural: SKIP ( 01 ) 2 LINES
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TBL",new TabSetting(6),"Tax ",new TabSetting(12),"Seq",new TabSetting(17),"Company",new //Natural: WRITE ( 01 ) NOTITLE 01T 'TBL' 06T 'Tax ' 12T 'Seq' 17T 'Company' 26T 'Company' 35T '   Company    '
                        TabSetting(26),"Company",new TabSetting(35),"   Company    ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"NBR",new TabSetting(6),"Year",new TabSetting(12),"No.",new TabSetting(17)," Code  ",new //Natural: WRITE ( 01 ) NOTITLE 01T 'NBR' 06T 'Year' 12T 'No.' 17T ' Code  ' 26T ' Name  ' 35T '   Tax ID     '
                        TabSetting(26)," Name  ",new TabSetting(35),"   Tax ID     ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"===",new TabSetting(6),"====",new TabSetting(12),"===",new TabSetting(17),"=======",new //Natural: WRITE ( 01 ) NOTITLE 01T '===' 06T '====' 12T '===' 17T '=======' 26T '=======' 35T '=============='
                        TabSetting(26),"=======",new TabSetting(35),"==============");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");

        getReports().setDisplayColumns(0, pnd_Old_Tax_Year,pnd_New_Tax_Year);
    }
}
