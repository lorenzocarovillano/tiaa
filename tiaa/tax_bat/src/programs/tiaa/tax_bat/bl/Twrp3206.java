/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:34:55 PM
**        * FROM NATURAL PROGRAM : Twrp3206
************************************************************
**        * FILE NAME            : Twrp3206.java
**        * CLASS NAME           : Twrp3206
**        * INSTANCE NAME        : Twrp3206
************************************************************
************************************************************************
* PROGRAM   : TWRP3206 ILLINOIS ST TAX WITHHOLDING QTRLY REPORTING
*           : CLONED AND MODIFIED FROM TWRP3090 TO GENERATE THE FILE
*           : IN NEW FORMAT
* FUNCTION  : CREATES A QUARTERLY TAX FILING TAPE AND PRODUCES REPORTS:
*             1. INPUT RECORDS DETAIL LISTING BY COMP/SSN/PPCN-PAYEE
*             2. QTRLY REPORTED DETAIL LISTING BY COMP/SSN(OUTPUT)
*             3. QTRLY NON-REPORTED DETAIL LISTING BY COMP/SSN
*             4. SUMMARY REPORT
*             5. ADJUSTMENT REPORT BY SSN, PPCN-PAYEE
*
* DATE      : 06/25/2020 CTS
*
* HISTORY   :
*
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3206 extends BLNatBase
{
    // Data Areas
    private LdaTwrlpsg1 ldaTwrlpsg1;
    private PdaTwracom2 pdaTwracom2;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_part;
    private DbsField part_Twrparti_Tax_Id;

    private DbsGroup part__R_Field_1;
    private DbsField part_Twrparti_Tax_Id_N;
    private DbsField part_Twrparti_Status;
    private DbsField part_Twrparti_Part_Eff_End_Dte;
    private DbsField part_Twrparti_Participant_Name;
    private DbsField pnd_Cor_Super_Ssn_Rcdtype;

    private DbsGroup pnd_Cor_Super_Ssn_Rcdtype__R_Field_2;
    private DbsField pnd_Cor_Super_Ssn_Rcdtype_Pnd_Ph_Social_Security_No;
    private DbsField pnd_Cor_Super_Ssn_Rcdtype_Pnd_Ph_Rcd_Type_Cde;
    private DbsField pnd_Twrparti_Curr_Rec_Sd;

    private DbsGroup pnd_Twrparti_Curr_Rec_Sd__R_Field_3;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Part_Tax_Id;

    private DbsGroup pnd_Twrparti_Curr_Rec_Sd__R_Field_4;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Part_Tax_Id_N;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Part_Status;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Part_99999999;

    private DbsGroup pnd_Qtr;
    private DbsField pnd_Qtr_Pnd_Tiaa_Cref;
    private DbsField pnd_Qtr_Pnd_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Qtr_Form_Cntrct_Py_Nmbr;

    private DbsGroup pnd_Qtr__R_Field_5;
    private DbsField pnd_Qtr_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Qtr_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Qtr_Form_Srce_Cde;
    private DbsField pnd_Qtr_Ss_Employee_Name;
    private DbsField pnd_Qtr_Pnd_Qtr_St_Tax;
    private DbsField pnd_Qtr_Pnd_Ytd_St_Tax;
    private DbsField pnd_Qtr_Pnd_Adj_St_Tax;
    private DbsField pnd_Qtr_Pnd_Diff_St_Tax;
    private DbsField pnd_Qtr_Pnd_Qtr_Gross_Amt;
    private DbsField pnd_Qtr_Pnd_Ytd_Gross_Amt;
    private DbsField pnd_Qtr_Pnd_Adj_Gross_Amt;
    private DbsField pnd_Qtr_Pnd_Diff_Gross_Amt;
    private DbsField pnd_Qtr_Pnd_Qtr_Lc_Tax;
    private DbsField pnd_Qtr_Pnd_Ytd_Lc_Tax;
    private DbsField pnd_Qtr_Pnd_Adj_Lc_Tax;
    private DbsField pnd_Qtr_Pnd_Diff_Lc_Tax;

    private DbsGroup pnd_Ssn;
    private DbsField pnd_Ssn_Pnd_Qtr_St_Tax;
    private DbsField pnd_Ssn_Pnd_Ytd_St_Tax;
    private DbsField pnd_Ssn_Pnd_Adj_St_Tax;
    private DbsField pnd_Ssn_Pnd_Qtr_Lc_Tax;
    private DbsField pnd_Ssn_Pnd_Ytd_Lc_Tax;
    private DbsField pnd_Ssn_Pnd_Adj_Lc_Tax;

    private DbsGroup pnd_Rec;
    private DbsField pnd_Rec_Pnd_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Rec_Form_Cntrct_Py_Nmbr;

    private DbsGroup pnd_Rec__R_Field_6;
    private DbsField pnd_Rec_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Rec_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Rec_Ss_Employee_Name;
    private DbsField pnd_Rec_Pnd_Qtr_St_Tax;
    private DbsField pnd_Rec_Pnd_Ytd_St_Tax;
    private DbsField pnd_Rec_Pnd_Adj_St_Tax;
    private DbsField pnd_Rec_Pnd_Qtr_Lc_Tax;
    private DbsField pnd_Rec_Pnd_Ytd_Lc_Tax;
    private DbsField pnd_Rec_Pnd_Adj_Lc_Tax;
    private DbsField pnd_Temp_Participant_Name;
    private DbsField pnd_Name1;
    private DbsField pnd_Name2;
    private DbsField pnd_Name3;
    private DbsField pnd_Name4;
    private DbsField pnd_Name5;
    private DbsField pnd_Name6;
    private DbsField pnd_Name7;
    private DbsField pnd_Name8;
    private DbsField pnd_Name9;
    private DbsField pnd_Name10;
    private DbsField pnd_Name11;
    private DbsField pnd_Name12;
    private DbsField pnd_Name13;
    private DbsField pnd_Name14;
    private DbsField pnd_Adj_To_Make;
    private DbsField pnd_Rec_Seq;
    private DbsField pnd_Adj_Idx_Max;
    private DbsField pnd_I;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_S;
    private DbsField pnd_Break;
    private DbsField pnd_New_Record;
    private DbsField pnd_Per;
    private DbsField pnd_Run_Type;
    private DbsField pnd_Save_Tiaa_Cref;
    private DbsField pnd_Rs_Report_Prd;

    private DbsGroup pnd_Rs_Report_Prd__R_Field_7;
    private DbsField pnd_Rs_Report_Prd_Pnd_Rs_Report_Prd_Mm;
    private DbsField pnd_Rs_Report_Prd_Pnd_Rs_Report_Prd_Ccyy;
    private DbsField pnd_Input_Parm;

    private DbsGroup pnd_Input_Parm__R_Field_8;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To;

    private DbsGroup pnd_Input_Parm__R_Field_9;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy;

    private DbsGroup pnd_Input_Parm__R_Field_10;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Cc;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Yy;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd;
    private DbsField pnd_Input_Parm_Pnd_Filler2;
    private DbsField pnd_Input_Parm_Pnd_Year_End_Adj_Ind;
    private DbsField pnd_Old_Annt_Ssn;
    private DbsField pnd_Old_Annt_Name;

    private DbsGroup pnd_Totals;
    private DbsField pnd_Totals_Pnd_Tot_Cnt_In_Unq_Sspnd__Contractpnd;
    private DbsField pnd_Totals_Pnd_Tot_Cnt_In_Unq_Sspnd;
    private DbsField pnd_Totals_Pnd_Tot_Cnt_Adj_Sspnd__Contractpnd;
    private DbsField pnd_Totals_Pnd_Tot_Cnt_Adj_Sspnd;
    private DbsField pnd_Totals_Pnd_Tot_Amt_In;
    private DbsField pnd_Totals_Pnd_Tot_Cnt_Reported;
    private DbsField pnd_Totals_Pnd_Tot_Amt_Reported;
    private DbsField pnd_Totals_Pnd_Tot_Loc_Reported;
    private DbsField pnd_Totals_Pnd_Tot_Cnt_Not_Reported;
    private DbsField pnd_Totals_Pnd_Tot_Amt_Not_Reported;
    private DbsField pnd_Totals_Pnd_Tot_Amt_Adj_Qtr;
    private DbsField pnd_Totals_Pnd_Tot_Amt_Adj;
    private DbsField pnd_Totals_Pnd_Tot_Loc_Adj_Qtr;
    private DbsField pnd_Totals_Pnd_Tot_Loc_Adj;
    private DbsField pnd_Totals_Pnd_Tot_Qtr_Tax;
    private DbsField pnd_Totals_Pnd_Tot_Adj_Tax;
    private DbsField pnd_Totals_Pnd_Tot_Ytd_Tax;
    private DbsField pnd_Core_Found;
    private DbsField pnd_Part_Found;

    private DbsGroup re;
    private DbsField re_E_Filler_1;
    private DbsField re_E_Ret_Qtr;
    private DbsField re_E_Tax_Year;
    private DbsField re_E_State_Ein;
    private DbsField re_E_Buss_Name;
    private DbsField re_E_Add_Line_1;
    private DbsField re_E_Add_Line_2;
    private DbsField re_E_City;
    private DbsField re_E_Country;
    private DbsField re_E_Filler_2;
    private DbsField re_E_Zip_Code;
    private DbsField re_E_Filler_3;
    private DbsField re_E_Nbr_Employees;
    private DbsField re_E_Wh_Tot_Wages;
    private DbsField re_E_Wh_Tot_Income_Tax;
    private DbsField re_E_Wh_Taxable_Wages;
    private DbsField re_E_Month1_Employees;
    private DbsField re_E_Month2_Employees;
    private DbsField re_E_Month3_Employees;
    private DbsField re_E_Filler_4;
    private DbsField re_E_Transmitter;
    private DbsField re_E_Filler_5;
    private DbsField re_E_Sort_Ind;

    private DbsGroup rs;
    private DbsField rs_Rs_Soc_Sec_Nbr;
    private DbsField rs_Rs_First_Name;
    private DbsField rs_Rs_Middle_Name;
    private DbsField rs_Rs_Last_Name;
    private DbsField rs_Rs_Filler_1;
    private DbsField rs_Rs_Wage_Plan_Code;
    private DbsField rs_Rs_Filler_2;
    private DbsField rs_Rs_Rpt_Prd;
    private DbsField rs_Rs_Tot_Wages;
    private DbsField rs_Rs_Filler_3;
    private DbsField rs_E_State_Ein;
    private DbsField rs_Rs_Filler_A;
    private DbsField rs_Rs_Qtr_Gross_Amt;
    private DbsField rs_Rs_Filler_4;
    private DbsField rs_Rs_Tax_Withhold;
    private DbsField rs_Rs_Filler_5;
    private DbsField rs_Rs_Transmitter;
    private DbsField rs_Rs_Filler_6;
    private DbsField rs_Rs_Sort_Ind;
    private DbsField pnd_First_Name_Len;
    private DbsField pnd_Last_Name_Len;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pnd_Annt_Soc_Sec_NbrOld;
    private DbsField readWork01Ss_Employee_NameOld;
    private DbsField readWork01Pnd_Tiaa_CrefOld;
    private int psgm001ReturnCode;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrlpsg1 = new LdaTwrlpsg1();
        registerRecord(ldaTwrlpsg1);
        localVariables = new DbsRecord();
        pdaTwracom2 = new PdaTwracom2(localVariables);

        // Local Variables

        vw_part = new DataAccessProgramView(new NameInfo("vw_part", "PART"), "TWRPARTI_PARTICIPANT_FILE", "TWR_PARTICIPANT");
        part_Twrparti_Tax_Id = vw_part.getRecord().newFieldInGroup("part_Twrparti_Tax_Id", "TWRPARTI-TAX-ID", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TWRPARTI_TAX_ID");

        part__R_Field_1 = vw_part.getRecord().newGroupInGroup("part__R_Field_1", "REDEFINE", part_Twrparti_Tax_Id);
        part_Twrparti_Tax_Id_N = part__R_Field_1.newFieldInGroup("part_Twrparti_Tax_Id_N", "TWRPARTI-TAX-ID-N", FieldType.NUMERIC, 9);
        part_Twrparti_Status = vw_part.getRecord().newFieldInGroup("part_Twrparti_Status", "TWRPARTI-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_STATUS");
        part_Twrparti_Part_Eff_End_Dte = vw_part.getRecord().newFieldInGroup("part_Twrparti_Part_Eff_End_Dte", "TWRPARTI-PART-EFF-END-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_PART_EFF_END_DTE");
        part_Twrparti_Participant_Name = vw_part.getRecord().newFieldInGroup("part_Twrparti_Participant_Name", "TWRPARTI-PARTICIPANT-NAME", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "TWRPARTI_PARTICIPANT_NAME");
        registerRecord(vw_part);

        pnd_Cor_Super_Ssn_Rcdtype = localVariables.newFieldInRecord("pnd_Cor_Super_Ssn_Rcdtype", "#COR-SUPER-SSN-RCDTYPE", FieldType.STRING, 11);

        pnd_Cor_Super_Ssn_Rcdtype__R_Field_2 = localVariables.newGroupInRecord("pnd_Cor_Super_Ssn_Rcdtype__R_Field_2", "REDEFINE", pnd_Cor_Super_Ssn_Rcdtype);
        pnd_Cor_Super_Ssn_Rcdtype_Pnd_Ph_Social_Security_No = pnd_Cor_Super_Ssn_Rcdtype__R_Field_2.newFieldInGroup("pnd_Cor_Super_Ssn_Rcdtype_Pnd_Ph_Social_Security_No", 
            "#PH-SOCIAL-SECURITY-NO", FieldType.NUMERIC, 9);
        pnd_Cor_Super_Ssn_Rcdtype_Pnd_Ph_Rcd_Type_Cde = pnd_Cor_Super_Ssn_Rcdtype__R_Field_2.newFieldInGroup("pnd_Cor_Super_Ssn_Rcdtype_Pnd_Ph_Rcd_Type_Cde", 
            "#PH-RCD-TYPE-CDE", FieldType.NUMERIC, 2);
        pnd_Twrparti_Curr_Rec_Sd = localVariables.newFieldInRecord("pnd_Twrparti_Curr_Rec_Sd", "#TWRPARTI-CURR-REC-SD", FieldType.STRING, 19);

        pnd_Twrparti_Curr_Rec_Sd__R_Field_3 = localVariables.newGroupInRecord("pnd_Twrparti_Curr_Rec_Sd__R_Field_3", "REDEFINE", pnd_Twrparti_Curr_Rec_Sd);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Part_Tax_Id = pnd_Twrparti_Curr_Rec_Sd__R_Field_3.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Part_Tax_Id", "#PART-TAX-ID", 
            FieldType.STRING, 10);

        pnd_Twrparti_Curr_Rec_Sd__R_Field_4 = pnd_Twrparti_Curr_Rec_Sd__R_Field_3.newGroupInGroup("pnd_Twrparti_Curr_Rec_Sd__R_Field_4", "REDEFINE", pnd_Twrparti_Curr_Rec_Sd_Pnd_Part_Tax_Id);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Part_Tax_Id_N = pnd_Twrparti_Curr_Rec_Sd__R_Field_4.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Part_Tax_Id_N", 
            "#PART-TAX-ID-N", FieldType.NUMERIC, 9);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Part_Status = pnd_Twrparti_Curr_Rec_Sd__R_Field_3.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Part_Status", "#PART-STATUS", 
            FieldType.STRING, 1);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Part_99999999 = pnd_Twrparti_Curr_Rec_Sd__R_Field_3.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Part_99999999", 
            "#PART-99999999", FieldType.STRING, 8);

        pnd_Qtr = localVariables.newGroupInRecord("pnd_Qtr", "#QTR");
        pnd_Qtr_Pnd_Tiaa_Cref = pnd_Qtr.newFieldInGroup("pnd_Qtr_Pnd_Tiaa_Cref", "#TIAA-CREF", FieldType.STRING, 1);
        pnd_Qtr_Pnd_Annt_Soc_Sec_Nbr = pnd_Qtr.newFieldInGroup("pnd_Qtr_Pnd_Annt_Soc_Sec_Nbr", "#ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9);
        pnd_Qtr_Form_Cntrct_Py_Nmbr = pnd_Qtr.newFieldInGroup("pnd_Qtr_Form_Cntrct_Py_Nmbr", "FORM-CNTRCT-PY-NMBR", FieldType.STRING, 10);

        pnd_Qtr__R_Field_5 = pnd_Qtr.newGroupInGroup("pnd_Qtr__R_Field_5", "REDEFINE", pnd_Qtr_Form_Cntrct_Py_Nmbr);
        pnd_Qtr_Pnd_Cntrct_Ppcn_Nbr = pnd_Qtr__R_Field_5.newFieldInGroup("pnd_Qtr_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 8);
        pnd_Qtr_Pnd_Cntrct_Payee_Cde = pnd_Qtr__R_Field_5.newFieldInGroup("pnd_Qtr_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.STRING, 2);
        pnd_Qtr_Form_Srce_Cde = pnd_Qtr.newFieldInGroup("pnd_Qtr_Form_Srce_Cde", "FORM-SRCE-CDE", FieldType.STRING, 2);
        pnd_Qtr_Ss_Employee_Name = pnd_Qtr.newFieldInGroup("pnd_Qtr_Ss_Employee_Name", "SS-EMPLOYEE-NAME", FieldType.STRING, 27);
        pnd_Qtr_Pnd_Qtr_St_Tax = pnd_Qtr.newFieldArrayInGroup("pnd_Qtr_Pnd_Qtr_St_Tax", "#QTR-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            4));
        pnd_Qtr_Pnd_Ytd_St_Tax = pnd_Qtr.newFieldArrayInGroup("pnd_Qtr_Pnd_Ytd_St_Tax", "#YTD-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            5));
        pnd_Qtr_Pnd_Adj_St_Tax = pnd_Qtr.newFieldArrayInGroup("pnd_Qtr_Pnd_Adj_St_Tax", "#ADJ-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            4));
        pnd_Qtr_Pnd_Diff_St_Tax = pnd_Qtr.newFieldArrayInGroup("pnd_Qtr_Pnd_Diff_St_Tax", "#DIFF-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            5));
        pnd_Qtr_Pnd_Qtr_Gross_Amt = pnd_Qtr.newFieldArrayInGroup("pnd_Qtr_Pnd_Qtr_Gross_Amt", "#QTR-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            4));
        pnd_Qtr_Pnd_Ytd_Gross_Amt = pnd_Qtr.newFieldArrayInGroup("pnd_Qtr_Pnd_Ytd_Gross_Amt", "#YTD-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            5));
        pnd_Qtr_Pnd_Adj_Gross_Amt = pnd_Qtr.newFieldArrayInGroup("pnd_Qtr_Pnd_Adj_Gross_Amt", "#ADJ-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            4));
        pnd_Qtr_Pnd_Diff_Gross_Amt = pnd_Qtr.newFieldArrayInGroup("pnd_Qtr_Pnd_Diff_Gross_Amt", "#DIFF-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2, new 
            DbsArrayController(1, 5));
        pnd_Qtr_Pnd_Qtr_Lc_Tax = pnd_Qtr.newFieldArrayInGroup("pnd_Qtr_Pnd_Qtr_Lc_Tax", "#QTR-LC-TAX", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            4));
        pnd_Qtr_Pnd_Ytd_Lc_Tax = pnd_Qtr.newFieldArrayInGroup("pnd_Qtr_Pnd_Ytd_Lc_Tax", "#YTD-LC-TAX", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            5));
        pnd_Qtr_Pnd_Adj_Lc_Tax = pnd_Qtr.newFieldArrayInGroup("pnd_Qtr_Pnd_Adj_Lc_Tax", "#ADJ-LC-TAX", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            4));
        pnd_Qtr_Pnd_Diff_Lc_Tax = pnd_Qtr.newFieldArrayInGroup("pnd_Qtr_Pnd_Diff_Lc_Tax", "#DIFF-LC-TAX", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            5));

        pnd_Ssn = localVariables.newGroupInRecord("pnd_Ssn", "#SSN");
        pnd_Ssn_Pnd_Qtr_St_Tax = pnd_Ssn.newFieldArrayInGroup("pnd_Ssn_Pnd_Qtr_St_Tax", "#QTR-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            4));
        pnd_Ssn_Pnd_Ytd_St_Tax = pnd_Ssn.newFieldArrayInGroup("pnd_Ssn_Pnd_Ytd_St_Tax", "#YTD-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            5));
        pnd_Ssn_Pnd_Adj_St_Tax = pnd_Ssn.newFieldArrayInGroup("pnd_Ssn_Pnd_Adj_St_Tax", "#ADJ-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            4));
        pnd_Ssn_Pnd_Qtr_Lc_Tax = pnd_Ssn.newFieldArrayInGroup("pnd_Ssn_Pnd_Qtr_Lc_Tax", "#QTR-LC-TAX", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            4));
        pnd_Ssn_Pnd_Ytd_Lc_Tax = pnd_Ssn.newFieldArrayInGroup("pnd_Ssn_Pnd_Ytd_Lc_Tax", "#YTD-LC-TAX", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            5));
        pnd_Ssn_Pnd_Adj_Lc_Tax = pnd_Ssn.newFieldArrayInGroup("pnd_Ssn_Pnd_Adj_Lc_Tax", "#ADJ-LC-TAX", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            4));

        pnd_Rec = localVariables.newGroupArrayInRecord("pnd_Rec", "#REC", new DbsArrayController(1, 30));
        pnd_Rec_Pnd_Annt_Soc_Sec_Nbr = pnd_Rec.newFieldInGroup("pnd_Rec_Pnd_Annt_Soc_Sec_Nbr", "#ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9);
        pnd_Rec_Form_Cntrct_Py_Nmbr = pnd_Rec.newFieldInGroup("pnd_Rec_Form_Cntrct_Py_Nmbr", "FORM-CNTRCT-PY-NMBR", FieldType.STRING, 10);

        pnd_Rec__R_Field_6 = pnd_Rec.newGroupInGroup("pnd_Rec__R_Field_6", "REDEFINE", pnd_Rec_Form_Cntrct_Py_Nmbr);
        pnd_Rec_Pnd_Cntrct_Ppcn_Nbr = pnd_Rec__R_Field_6.newFieldInGroup("pnd_Rec_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 8);
        pnd_Rec_Pnd_Cntrct_Payee_Cde = pnd_Rec__R_Field_6.newFieldInGroup("pnd_Rec_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.STRING, 2);
        pnd_Rec_Ss_Employee_Name = pnd_Rec.newFieldInGroup("pnd_Rec_Ss_Employee_Name", "SS-EMPLOYEE-NAME", FieldType.STRING, 27);
        pnd_Rec_Pnd_Qtr_St_Tax = pnd_Rec.newFieldArrayInGroup("pnd_Rec_Pnd_Qtr_St_Tax", "#QTR-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            4));
        pnd_Rec_Pnd_Ytd_St_Tax = pnd_Rec.newFieldArrayInGroup("pnd_Rec_Pnd_Ytd_St_Tax", "#YTD-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            5));
        pnd_Rec_Pnd_Adj_St_Tax = pnd_Rec.newFieldArrayInGroup("pnd_Rec_Pnd_Adj_St_Tax", "#ADJ-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            4));
        pnd_Rec_Pnd_Qtr_Lc_Tax = pnd_Rec.newFieldArrayInGroup("pnd_Rec_Pnd_Qtr_Lc_Tax", "#QTR-LC-TAX", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            4));
        pnd_Rec_Pnd_Ytd_Lc_Tax = pnd_Rec.newFieldArrayInGroup("pnd_Rec_Pnd_Ytd_Lc_Tax", "#YTD-LC-TAX", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            5));
        pnd_Rec_Pnd_Adj_Lc_Tax = pnd_Rec.newFieldArrayInGroup("pnd_Rec_Pnd_Adj_Lc_Tax", "#ADJ-LC-TAX", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            4));
        pnd_Temp_Participant_Name = localVariables.newFieldInRecord("pnd_Temp_Participant_Name", "#TEMP-PARTICIPANT-NAME", FieldType.STRING, 35);
        pnd_Name1 = localVariables.newFieldInRecord("pnd_Name1", "#NAME1", FieldType.STRING, 35);
        pnd_Name2 = localVariables.newFieldInRecord("pnd_Name2", "#NAME2", FieldType.STRING, 35);
        pnd_Name3 = localVariables.newFieldInRecord("pnd_Name3", "#NAME3", FieldType.STRING, 35);
        pnd_Name4 = localVariables.newFieldInRecord("pnd_Name4", "#NAME4", FieldType.STRING, 35);
        pnd_Name5 = localVariables.newFieldInRecord("pnd_Name5", "#NAME5", FieldType.STRING, 35);
        pnd_Name6 = localVariables.newFieldInRecord("pnd_Name6", "#NAME6", FieldType.STRING, 35);
        pnd_Name7 = localVariables.newFieldInRecord("pnd_Name7", "#NAME7", FieldType.STRING, 35);
        pnd_Name8 = localVariables.newFieldInRecord("pnd_Name8", "#NAME8", FieldType.STRING, 35);
        pnd_Name9 = localVariables.newFieldInRecord("pnd_Name9", "#NAME9", FieldType.STRING, 35);
        pnd_Name10 = localVariables.newFieldInRecord("pnd_Name10", "#NAME10", FieldType.STRING, 35);
        pnd_Name11 = localVariables.newFieldInRecord("pnd_Name11", "#NAME11", FieldType.STRING, 35);
        pnd_Name12 = localVariables.newFieldInRecord("pnd_Name12", "#NAME12", FieldType.STRING, 35);
        pnd_Name13 = localVariables.newFieldInRecord("pnd_Name13", "#NAME13", FieldType.STRING, 35);
        pnd_Name14 = localVariables.newFieldInRecord("pnd_Name14", "#NAME14", FieldType.STRING, 35);
        pnd_Adj_To_Make = localVariables.newFieldInRecord("pnd_Adj_To_Make", "#ADJ-TO-MAKE", FieldType.BOOLEAN, 1);
        pnd_Rec_Seq = localVariables.newFieldInRecord("pnd_Rec_Seq", "#REC-SEQ", FieldType.NUMERIC, 2);
        pnd_Adj_Idx_Max = localVariables.newFieldInRecord("pnd_Adj_Idx_Max", "#ADJ-IDX-MAX", FieldType.INTEGER, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 2);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.NUMERIC, 2);
        pnd_S = localVariables.newFieldInRecord("pnd_S", "#S", FieldType.NUMERIC, 1);
        pnd_Break = localVariables.newFieldInRecord("pnd_Break", "#BREAK", FieldType.BOOLEAN, 1);
        pnd_New_Record = localVariables.newFieldInRecord("pnd_New_Record", "#NEW-RECORD", FieldType.BOOLEAN, 1);
        pnd_Per = localVariables.newFieldInRecord("pnd_Per", "#PER", FieldType.INTEGER, 1);
        pnd_Run_Type = localVariables.newFieldInRecord("pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 24);
        pnd_Save_Tiaa_Cref = localVariables.newFieldInRecord("pnd_Save_Tiaa_Cref", "#SAVE-TIAA-CREF", FieldType.STRING, 1);
        pnd_Rs_Report_Prd = localVariables.newFieldInRecord("pnd_Rs_Report_Prd", "#RS-REPORT-PRD", FieldType.NUMERIC, 6);

        pnd_Rs_Report_Prd__R_Field_7 = localVariables.newGroupInRecord("pnd_Rs_Report_Prd__R_Field_7", "REDEFINE", pnd_Rs_Report_Prd);
        pnd_Rs_Report_Prd_Pnd_Rs_Report_Prd_Mm = pnd_Rs_Report_Prd__R_Field_7.newFieldInGroup("pnd_Rs_Report_Prd_Pnd_Rs_Report_Prd_Mm", "#RS-REPORT-PRD-MM", 
            FieldType.NUMERIC, 2);
        pnd_Rs_Report_Prd_Pnd_Rs_Report_Prd_Ccyy = pnd_Rs_Report_Prd__R_Field_7.newFieldInGroup("pnd_Rs_Report_Prd_Pnd_Rs_Report_Prd_Ccyy", "#RS-REPORT-PRD-CCYY", 
            FieldType.NUMERIC, 4);
        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 10);

        pnd_Input_Parm__R_Field_8 = localVariables.newGroupInRecord("pnd_Input_Parm__R_Field_8", "REDEFINE", pnd_Input_Parm);
        pnd_Input_Parm_Pnd_Pymnt_Date_To = pnd_Input_Parm__R_Field_8.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To", "#PYMNT-DATE-TO", FieldType.STRING, 
            8);

        pnd_Input_Parm__R_Field_9 = pnd_Input_Parm__R_Field_8.newGroupInGroup("pnd_Input_Parm__R_Field_9", "REDEFINE", pnd_Input_Parm_Pnd_Pymnt_Date_To);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy = pnd_Input_Parm__R_Field_9.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy", "#PYMNT-DATE-TO-YYYY", 
            FieldType.NUMERIC, 4);

        pnd_Input_Parm__R_Field_10 = pnd_Input_Parm__R_Field_9.newGroupInGroup("pnd_Input_Parm__R_Field_10", "REDEFINE", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Cc = pnd_Input_Parm__R_Field_10.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Cc", "#PYMNT-DATE-TO-CC", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Yy = pnd_Input_Parm__R_Field_10.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Yy", "#PYMNT-DATE-TO-YY", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm = pnd_Input_Parm__R_Field_9.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm", "#PYMNT-DATE-TO-MM", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd = pnd_Input_Parm__R_Field_9.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd", "#PYMNT-DATE-TO-DD", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Filler2 = pnd_Input_Parm__R_Field_8.newFieldInGroup("pnd_Input_Parm_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Input_Parm_Pnd_Year_End_Adj_Ind = pnd_Input_Parm__R_Field_8.newFieldInGroup("pnd_Input_Parm_Pnd_Year_End_Adj_Ind", "#YEAR-END-ADJ-IND", FieldType.STRING, 
            1);
        pnd_Old_Annt_Ssn = localVariables.newFieldInRecord("pnd_Old_Annt_Ssn", "#OLD-ANNT-SSN", FieldType.NUMERIC, 9);
        pnd_Old_Annt_Name = localVariables.newFieldInRecord("pnd_Old_Annt_Name", "#OLD-ANNT-NAME", FieldType.STRING, 27);

        pnd_Totals = localVariables.newGroupInRecord("pnd_Totals", "#TOTALS");
        pnd_Totals_Pnd_Tot_Cnt_In_Unq_Sspnd__Contractpnd = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Tot_Cnt_In_Unq_Sspnd__Contractpnd", "#TOT-CNT-IN-UNQ-SS#-CONTRACT#", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Totals_Pnd_Tot_Cnt_In_Unq_Sspnd = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Tot_Cnt_In_Unq_Sspnd", "#TOT-CNT-IN-UNQ-SS#", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Totals_Pnd_Tot_Cnt_Adj_Sspnd__Contractpnd = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Tot_Cnt_Adj_Sspnd__Contractpnd", "#TOT-CNT-ADJ-SS#-CONTRACT#", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Totals_Pnd_Tot_Cnt_Adj_Sspnd = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Tot_Cnt_Adj_Sspnd", "#TOT-CNT-ADJ-SS#", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Totals_Pnd_Tot_Amt_In = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Tot_Amt_In", "#TOT-AMT-IN", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Totals_Pnd_Tot_Cnt_Reported = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Tot_Cnt_Reported", "#TOT-CNT-REPORTED", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Totals_Pnd_Tot_Amt_Reported = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Tot_Amt_Reported", "#TOT-AMT-REPORTED", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Totals_Pnd_Tot_Loc_Reported = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Tot_Loc_Reported", "#TOT-LOC-REPORTED", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Totals_Pnd_Tot_Cnt_Not_Reported = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Tot_Cnt_Not_Reported", "#TOT-CNT-NOT-REPORTED", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Totals_Pnd_Tot_Amt_Not_Reported = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Tot_Amt_Not_Reported", "#TOT-AMT-NOT-REPORTED", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Totals_Pnd_Tot_Amt_Adj_Qtr = pnd_Totals.newFieldArrayInGroup("pnd_Totals_Pnd_Tot_Amt_Adj_Qtr", "#TOT-AMT-ADJ-QTR", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 4));
        pnd_Totals_Pnd_Tot_Amt_Adj = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Tot_Amt_Adj", "#TOT-AMT-ADJ", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Totals_Pnd_Tot_Loc_Adj_Qtr = pnd_Totals.newFieldArrayInGroup("pnd_Totals_Pnd_Tot_Loc_Adj_Qtr", "#TOT-LOC-ADJ-QTR", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 4));
        pnd_Totals_Pnd_Tot_Loc_Adj = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Tot_Loc_Adj", "#TOT-LOC-ADJ", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Totals_Pnd_Tot_Qtr_Tax = pnd_Totals.newFieldArrayInGroup("pnd_Totals_Pnd_Tot_Qtr_Tax", "#TOT-QTR-TAX", FieldType.PACKED_DECIMAL, 11, 2, new 
            DbsArrayController(1, 4));
        pnd_Totals_Pnd_Tot_Adj_Tax = pnd_Totals.newFieldArrayInGroup("pnd_Totals_Pnd_Tot_Adj_Tax", "#TOT-ADJ-TAX", FieldType.PACKED_DECIMAL, 11, 2, new 
            DbsArrayController(1, 4));
        pnd_Totals_Pnd_Tot_Ytd_Tax = pnd_Totals.newFieldArrayInGroup("pnd_Totals_Pnd_Tot_Ytd_Tax", "#TOT-YTD-TAX", FieldType.PACKED_DECIMAL, 11, 2, new 
            DbsArrayController(1, 5));
        pnd_Core_Found = localVariables.newFieldInRecord("pnd_Core_Found", "#CORE-FOUND", FieldType.BOOLEAN, 1);
        pnd_Part_Found = localVariables.newFieldInRecord("pnd_Part_Found", "#PART-FOUND", FieldType.BOOLEAN, 1);

        re = localVariables.newGroupInRecord("re", "RE");
        re_E_Filler_1 = re.newFieldInGroup("re_E_Filler_1", "E-FILLER-1", FieldType.STRING, 12);
        re_E_Ret_Qtr = re.newFieldInGroup("re_E_Ret_Qtr", "E-RET-QTR", FieldType.STRING, 1);
        re_E_Tax_Year = re.newFieldInGroup("re_E_Tax_Year", "E-TAX-YEAR", FieldType.STRING, 4);
        re_E_State_Ein = re.newFieldInGroup("re_E_State_Ein", "E-STATE-EIN", FieldType.STRING, 8);
        re_E_Buss_Name = re.newFieldInGroup("re_E_Buss_Name", "E-BUSS-NAME", FieldType.STRING, 60);
        re_E_Add_Line_1 = re.newFieldInGroup("re_E_Add_Line_1", "E-ADD-LINE-1", FieldType.STRING, 30);
        re_E_Add_Line_2 = re.newFieldInGroup("re_E_Add_Line_2", "E-ADD-LINE-2", FieldType.STRING, 30);
        re_E_City = re.newFieldInGroup("re_E_City", "E-CITY", FieldType.STRING, 28);
        re_E_Country = re.newFieldInGroup("re_E_Country", "E-COUNTRY", FieldType.STRING, 3);
        re_E_Filler_2 = re.newFieldInGroup("re_E_Filler_2", "E-FILLER-2", FieldType.STRING, 2);
        re_E_Zip_Code = re.newFieldInGroup("re_E_Zip_Code", "E-ZIP-CODE", FieldType.STRING, 10);
        re_E_Filler_3 = re.newFieldInGroup("re_E_Filler_3", "E-FILLER-3", FieldType.STRING, 2);
        re_E_Nbr_Employees = re.newFieldInGroup("re_E_Nbr_Employees", "E-NBR-EMPLOYEES", FieldType.NUMERIC, 8);
        re_E_Wh_Tot_Wages = re.newFieldInGroup("re_E_Wh_Tot_Wages", "E-WH-TOT-WAGES", FieldType.NUMERIC, 13, 2);
        re_E_Wh_Tot_Income_Tax = re.newFieldInGroup("re_E_Wh_Tot_Income_Tax", "E-WH-TOT-INCOME-TAX", FieldType.NUMERIC, 13, 2);
        re_E_Wh_Taxable_Wages = re.newFieldInGroup("re_E_Wh_Taxable_Wages", "E-WH-TAXABLE-WAGES", FieldType.NUMERIC, 13, 2);
        re_E_Month1_Employees = re.newFieldInGroup("re_E_Month1_Employees", "E-MONTH1-EMPLOYEES", FieldType.NUMERIC, 7);
        re_E_Month2_Employees = re.newFieldInGroup("re_E_Month2_Employees", "E-MONTH2-EMPLOYEES", FieldType.NUMERIC, 7);
        re_E_Month3_Employees = re.newFieldInGroup("re_E_Month3_Employees", "E-MONTH3-EMPLOYEES", FieldType.NUMERIC, 7);
        re_E_Filler_4 = re.newFieldInGroup("re_E_Filler_4", "E-FILLER-4", FieldType.STRING, 93);
        re_E_Transmitter = re.newFieldInGroup("re_E_Transmitter", "E-TRANSMITTER", FieldType.STRING, 1);
        re_E_Filler_5 = re.newFieldInGroup("re_E_Filler_5", "E-FILLER-5", FieldType.STRING, 160);
        re_E_Sort_Ind = re.newFieldInGroup("re_E_Sort_Ind", "E-SORT-IND", FieldType.STRING, 2);

        rs = localVariables.newGroupInRecord("rs", "RS");
        rs_Rs_Soc_Sec_Nbr = rs.newFieldInGroup("rs_Rs_Soc_Sec_Nbr", "RS-SOC-SEC-NBR", FieldType.STRING, 9);
        rs_Rs_First_Name = rs.newFieldInGroup("rs_Rs_First_Name", "RS-FIRST-NAME", FieldType.STRING, 15);
        rs_Rs_Middle_Name = rs.newFieldInGroup("rs_Rs_Middle_Name", "RS-MIDDLE-NAME", FieldType.STRING, 15);
        rs_Rs_Last_Name = rs.newFieldInGroup("rs_Rs_Last_Name", "RS-LAST-NAME", FieldType.STRING, 23);
        rs_Rs_Filler_1 = rs.newFieldInGroup("rs_Rs_Filler_1", "RS-FILLER-1", FieldType.STRING, 123);
        rs_Rs_Wage_Plan_Code = rs.newFieldInGroup("rs_Rs_Wage_Plan_Code", "RS-WAGE-PLAN-CODE", FieldType.STRING, 1);
        rs_Rs_Filler_2 = rs.newFieldInGroup("rs_Rs_Filler_2", "RS-FILLER-2", FieldType.STRING, 1);
        rs_Rs_Rpt_Prd = rs.newFieldInGroup("rs_Rs_Rpt_Prd", "RS-RPT-PRD", FieldType.STRING, 6);
        rs_Rs_Tot_Wages = rs.newFieldInGroup("rs_Rs_Tot_Wages", "RS-TOT-WAGES", FieldType.NUMERIC, 11, 2);
        rs_Rs_Filler_3 = rs.newFieldInGroup("rs_Rs_Filler_3", "RS-FILLER-3", FieldType.STRING, 34);
        rs_E_State_Ein = rs.newFieldInGroup("rs_E_State_Ein", "E-STATE-EIN", FieldType.STRING, 8);
        rs_Rs_Filler_A = rs.newFieldInGroup("rs_Rs_Filler_A", "RS-FILLER-A", FieldType.STRING, 20);
        rs_Rs_Qtr_Gross_Amt = rs.newFieldInGroup("rs_Rs_Qtr_Gross_Amt", "RS-QTR-GROSS-AMT", FieldType.NUMERIC, 11, 2);
        rs_Rs_Filler_4 = rs.newFieldInGroup("rs_Rs_Filler_4", "RS-FILLER-4", FieldType.STRING, 1);
        rs_Rs_Tax_Withhold = rs.newFieldInGroup("rs_Rs_Tax_Withhold", "RS-TAX-WITHHOLD", FieldType.NUMERIC, 10, 2);
        rs_Rs_Filler_5 = rs.newFieldInGroup("rs_Rs_Filler_5", "RS-FILLER-5", FieldType.STRING, 63);
        rs_Rs_Transmitter = rs.newFieldInGroup("rs_Rs_Transmitter", "RS-TRANSMITTER", FieldType.STRING, 1);
        rs_Rs_Filler_6 = rs.newFieldInGroup("rs_Rs_Filler_6", "RS-FILLER-6", FieldType.STRING, 160);
        rs_Rs_Sort_Ind = rs.newFieldInGroup("rs_Rs_Sort_Ind", "RS-SORT-IND", FieldType.STRING, 2);
        pnd_First_Name_Len = localVariables.newFieldInRecord("pnd_First_Name_Len", "#FIRST-NAME-LEN", FieldType.NUMERIC, 2);
        pnd_Last_Name_Len = localVariables.newFieldInRecord("pnd_Last_Name_Len", "#LAST-NAME-LEN", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pnd_Annt_Soc_Sec_NbrOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Annt_Soc_Sec_Nbr_OLD", "Pnd_Annt_Soc_Sec_Nbr_OLD", FieldType.NUMERIC, 
            9);
        readWork01Ss_Employee_NameOld = internalLoopRecord.newFieldInRecord("ReadWork01_Ss_Employee_Name_OLD", "Ss_Employee_Name_OLD", FieldType.STRING, 
            27);
        readWork01Pnd_Tiaa_CrefOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Tiaa_Cref_OLD", "Pnd_Tiaa_Cref_OLD", FieldType.STRING, 1);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_part.reset();
        internalLoopRecord.reset();

        ldaTwrlpsg1.initializeValues();

        localVariables.reset();
        pnd_New_Record.setInitialValue(true);
        re_E_Country.setInitialValue("USA");
        re_E_Transmitter.setInitialValue("T");
        rs_Rs_Transmitter.setInitialValue("D");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3206() throws Exception
    {
        super("Twrp3206");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp3206|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        getReports().atTopOfPage(atTopEventRpt5, 5);
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                getReports().definePrinter(2, "INPUTRPT");                                                                                                                //Natural: DEFINE PRINTER ( INPUTRPT = 01 ) OUTPUT 'CMPRT01'
                getReports().definePrinter(3, "DTLRPTD");                                                                                                                 //Natural: DEFINE PRINTER ( DTLRPTD = 02 ) OUTPUT 'CMPRT02'
                getReports().definePrinter(4, "DTLNRPTD");                                                                                                                //Natural: DEFINE PRINTER ( DTLNRPTD = 03 ) OUTPUT 'CMPRT03'
                getReports().definePrinter(5, "SUMRPT");                                                                                                                  //Natural: DEFINE PRINTER ( SUMRPT = 04 ) OUTPUT 'CMPRT04'
                getReports().definePrinter(6, "ADJRPT");                                                                                                                  //Natural: DEFINE PRINTER ( ADJRPT = 05 ) OUTPUT 'CMPRT05'
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133;//Natural: FORMAT ( 02 ) PS = 60 LS = 133;//Natural: FORMAT ( 03 ) PS = 60 LS = 133;//Natural: FORMAT ( 04 ) PS = 60 LS = 133;//Natural: FORMAT ( 05 ) PS = 60 LS = 133
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parm);                                                                                       //Natural: INPUT #INPUT-PARM
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                                                                                                                                                                          //Natural: PERFORM DEFINE-RUN-TYPE
                sub_Define_Run_Type();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DEFINE-RUN-TYPE
                if (condition(pnd_Input_Parm_Pnd_Year_End_Adj_Ind.equals("Y")))                                                                                           //Natural: IF #YEAR-END-ADJ-IND = 'Y'
                {
                    pnd_Per.setValue(5);                                                                                                                                  //Natural: ASSIGN #PER := 5
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*                                                                                                                                                   //Natural: DECIDE ON FIRST VALUE OF #INPUT-PARM.#PYMNT-DATE-TO-MM
                    short decideConditionsMet370 = 0;                                                                                                                     //Natural: VALUE 03
                    if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(3))))
                    {
                        decideConditionsMet370++;
                        pnd_Per.setValue(1);                                                                                                                              //Natural: ASSIGN #PER := 1
                    }                                                                                                                                                     //Natural: VALUE 06
                    else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(6))))
                    {
                        decideConditionsMet370++;
                        pnd_Per.setValue(2);                                                                                                                              //Natural: ASSIGN #PER := 2
                    }                                                                                                                                                     //Natural: VALUE 09
                    else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(9))))
                    {
                        decideConditionsMet370++;
                        pnd_Per.setValue(3);                                                                                                                              //Natural: ASSIGN #PER := 3
                    }                                                                                                                                                     //Natural: VALUE 12
                    else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(12))))
                    {
                        decideConditionsMet370++;
                        pnd_Per.setValue(4);                                                                                                                              //Natural: ASSIGN #PER := 4
                    }                                                                                                                                                     //Natural: ANY VALUE
                    if (condition(decideConditionsMet370 > 0))
                    {
                        pnd_Rs_Report_Prd_Pnd_Rs_Report_Prd_Mm.setValue(pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm);                                                             //Natural: ASSIGN #RS-REPORT-PRD-MM := #INPUT-PARM.#PYMNT-DATE-TO-MM
                        pnd_Rs_Report_Prd_Pnd_Rs_Report_Prd_Ccyy.setValue(pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy);                                                         //Natural: ASSIGN #RS-REPORT-PRD-CCYY := #INPUT-PARM.#PYMNT-DATE-TO-YYYY
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        getReports().write(0, ReportOption.NOTITLE,"!!!!! ERROR FOUND, PROGRAM IS TERMINATED !!!!!",NEWLINE,"ERROR: #INPUT-PARM CONTAINS INVALID COMBINATION OF INPUT VALUES", //Natural: WRITE '!!!!! ERROR FOUND, PROGRAM IS TERMINATED !!!!!' / 'ERROR: #INPUT-PARM CONTAINS INVALID COMBINATION OF INPUT VALUES' / 'PLEASE CONTACT SYSTEM SUPPORT'
                            NEWLINE,"PLEASE CONTACT SYSTEM SUPPORT");
                        if (Global.isEscape()) return;
                        DbsUtil.terminate(90);  if (true) return;                                                                                                         //Natural: TERMINATE 90
                    }                                                                                                                                                     //Natural: END-DECIDE
                    //* CTS
                }                                                                                                                                                         //Natural: END-IF
                pnd_Adj_Idx_Max.compute(new ComputeParameters(false, pnd_Adj_Idx_Max), pnd_Per.subtract(1));                                                              //Natural: ASSIGN #ADJ-IDX-MAX := #PER -1
                rs_Rs_Wage_Plan_Code.setValue("P");                                                                                                                       //Natural: ASSIGN RS.RS-WAGE-PLAN-CODE := 'P'
                //*  RS.RS-TOT-WAGES        :=  000000000.00
                //*  RS.RS-TAXABLE-WAGES    :=  000000000.00
                rs_Rs_Rpt_Prd.setValue(pnd_Rs_Report_Prd);                                                                                                                //Natural: ASSIGN RS.RS-RPT-PRD := #RS-REPORT-PRD
                boolean endOfDataReadwork01 = true;                                                                                                                       //Natural: READ WORK FILE 01 RECORD #QTR
                boolean firstReadwork01 = true;
                READWORK01:
                while (condition(getWorkFiles().read(1, pnd_Qtr)))
                {
                    if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventReadwork01();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataReadwork01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    if (condition(pnd_New_Record.equals(true)))                                                                                                           //Natural: IF #NEW-RECORD = TRUE
                    {
                        pnd_New_Record.setValue(false);                                                                                                                   //Natural: ASSIGN #NEW-RECORD := FALSE
                        rs_Rs_First_Name.reset();                                                                                                                         //Natural: RESET RS.RS-FIRST-NAME RS.RS-MIDDLE-NAME RS.RS-LAST-NAME #NAME1 #NAME2 #NAME3 #NAME4 #NAME5
                        rs_Rs_Middle_Name.reset();
                        rs_Rs_Last_Name.reset();
                        pnd_Name1.reset();
                        pnd_Name2.reset();
                        pnd_Name3.reset();
                        pnd_Name4.reset();
                        pnd_Name5.reset();
                                                                                                                                                                          //Natural: PERFORM LOOKUP-NAME-IN-CORE
                        sub_Lookup_Name_In_Core();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(pnd_Core_Found.equals(false)))                                                                                                      //Natural: IF #CORE-FOUND = FALSE
                        {
                                                                                                                                                                          //Natural: PERFORM LOOKUP-NAME-IN-TAX-PARTICIPANT-FILE
                            sub_Lookup_Name_In_Tax_Participant_File();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            pnd_Temp_Participant_Name.separate(SeparateOption.WithAnyDelimiters, " ", pnd_Name1, pnd_Name2, pnd_Name3, pnd_Name4, pnd_Name5,              //Natural: SEPARATE #TEMP-PARTICIPANT-NAME INTO #NAME1 #NAME2 #NAME3 #NAME4 #NAME5 #NAME6 #NAME7 #NAME8 #NAME9 #NAME10 #NAME11 #NAME12 #NAME13 #NAME14 WITH DELIMITERS ' '
                                pnd_Name6, pnd_Name7, pnd_Name8, pnd_Name9, pnd_Name10, pnd_Name11, pnd_Name12, pnd_Name13, pnd_Name14);
                            if (condition(pnd_Name1.equals(" ")))                                                                                                         //Natural: IF #NAME1 = ' '
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                rs_Rs_First_Name.setValue(pnd_Name1);                                                                                                     //Natural: ASSIGN RS.RS-FIRST-NAME := #NAME1
                                if (condition(pnd_Name3.equals(" ")))                                                                                                     //Natural: IF #NAME3 = ' '
                                {
                                    rs_Rs_Last_Name.setValue(pnd_Name2);                                                                                                  //Natural: ASSIGN RS.RS-LAST-NAME := #NAME2
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    rs_Rs_Middle_Name.setValue(pnd_Name2);                                                                                                //Natural: ASSIGN RS.RS-MIDDLE-NAME := #NAME2
                                    rs_Rs_Last_Name.setValue(pnd_Name3);                                                                                                  //Natural: ASSIGN RS.RS-LAST-NAME := #NAME3
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        rs_Rs_Soc_Sec_Nbr.setValueEdited(pnd_Qtr_Pnd_Annt_Soc_Sec_Nbr,new ReportEditMask("999999999"));                                                   //Natural: MOVE EDITED #QTR.#ANNT-SOC-SEC-NBR ( EM = 999999999 ) TO RS.RS-SOC-SEC-NBR
                    }                                                                                                                                                     //Natural: END-IF
                    //*     CTS
                    //*     CTS
                    if (condition(pnd_Input_Parm_Pnd_Year_End_Adj_Ind.equals("N")))                                                                                       //Natural: IF #YEAR-END-ADJ-IND = 'N'
                    {
                        rs_Rs_Tax_Withhold.nadd(pnd_Qtr_Pnd_Qtr_St_Tax.getValue(pnd_Per));                                                                                //Natural: ASSIGN RS.RS-TAX-WITHHOLD := RS.RS-TAX-WITHHOLD + #QTR.#QTR-ST-TAX ( #PER )
                        //*    RS.RS-LOC-WITHHOLD  :=
                        //*      RS.RS-LOC-WITHHOLD  +  #QTR.#QTR-LC-TAX (#PER)
                        rs_Rs_Qtr_Gross_Amt.nadd(pnd_Qtr_Pnd_Qtr_Gross_Amt.getValue(pnd_Per));                                                                            //Natural: ASSIGN RS.RS-QTR-GROSS-AMT := RS.RS-QTR-GROSS-AMT + #QTR.#QTR-GROSS-AMT ( #PER )
                        rs_Rs_Tot_Wages.nadd(pnd_Qtr_Pnd_Ytd_Gross_Amt.getValue(pnd_Per));                                                                                //Natural: ASSIGN RS.RS-TOT-WAGES := RS.RS-TOT-WAGES + #QTR.#YTD-GROSS-AMT ( #PER )
                        re_E_Wh_Tot_Income_Tax.nadd(pnd_Qtr_Pnd_Qtr_St_Tax.getValue(pnd_Per));                                                                            //Natural: ASSIGN RE.E-WH-TOT-INCOME-TAX := RE.E-WH-TOT-INCOME-TAX + #QTR.#QTR-ST-TAX ( #PER )
                    }                                                                                                                                                     //Natural: END-IF
                    FOR01:                                                                                                                                                //Natural: FOR #I = 1 TO #PER
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Per)); pnd_I.nadd(1))
                    {
                        pnd_Ssn_Pnd_Ytd_St_Tax.getValue(pnd_I).nadd(pnd_Qtr_Pnd_Ytd_St_Tax.getValue(pnd_I));                                                              //Natural: ASSIGN #SSN.#YTD-ST-TAX ( #I ) := #SSN.#YTD-ST-TAX ( #I ) + #QTR.#YTD-ST-TAX ( #I )
                        pnd_Ssn_Pnd_Ytd_Lc_Tax.getValue(pnd_I).nadd(pnd_Qtr_Pnd_Ytd_Lc_Tax.getValue(pnd_I));                                                              //Natural: ASSIGN #SSN.#YTD-LC-TAX ( #I ) := #SSN.#YTD-LC-TAX ( #I ) + #QTR.#YTD-LC-TAX ( #I )
                        if (condition(pnd_I.equals(5)))                                                                                                                   //Natural: IF #I = 5
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Ssn_Pnd_Qtr_St_Tax.getValue(pnd_I).nadd(pnd_Qtr_Pnd_Qtr_St_Tax.getValue(pnd_I));                                                              //Natural: ASSIGN #SSN.#QTR-ST-TAX ( #I ) := #SSN.#QTR-ST-TAX ( #I ) + #QTR.#QTR-ST-TAX ( #I )
                        pnd_Ssn_Pnd_Adj_St_Tax.getValue(pnd_I).nadd(pnd_Qtr_Pnd_Adj_St_Tax.getValue(pnd_I));                                                              //Natural: ASSIGN #SSN.#ADJ-ST-TAX ( #I ) := #SSN.#ADJ-ST-TAX ( #I ) + #QTR.#ADJ-ST-TAX ( #I )
                        pnd_Ssn_Pnd_Qtr_Lc_Tax.getValue(pnd_I).nadd(pnd_Qtr_Pnd_Qtr_Lc_Tax.getValue(pnd_I));                                                              //Natural: ASSIGN #SSN.#QTR-LC-TAX ( #I ) := #SSN.#QTR-LC-TAX ( #I ) + #QTR.#QTR-LC-TAX ( #I )
                        pnd_Ssn_Pnd_Adj_Lc_Tax.getValue(pnd_I).nadd(pnd_Qtr_Pnd_Adj_Lc_Tax.getValue(pnd_I));                                                              //Natural: ASSIGN #SSN.#ADJ-LC-TAX ( #I ) := #SSN.#ADJ-LC-TAX ( #I ) + #QTR.#ADJ-LC-TAX ( #I )
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Totals_Pnd_Tot_Cnt_In_Unq_Sspnd__Contractpnd.nadd(1);                                                                                             //Natural: ADD 1 TO #TOT-CNT-IN-UNQ-SS#-CONTRACT#
                    if (condition(pnd_Input_Parm_Pnd_Year_End_Adj_Ind.equals("N")))                                                                                       //Natural: IF #YEAR-END-ADJ-IND = 'N'
                    {
                        pnd_Totals_Pnd_Tot_Amt_In.nadd(pnd_Qtr_Pnd_Qtr_St_Tax.getValue(pnd_Per));                                                                         //Natural: ADD #QTR.#QTR-ST-TAX ( #PER ) TO #TOT-AMT-IN
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM INPUT-RECORD-REPORT
                    sub_Input_Record_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*     KEEP CONTRACT LEVEL RECORDS FOR ADJUSTMENT REPORT
                    pnd_Rec_Seq.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REC-SEQ
                    pnd_Rec.getValue(pnd_Rec_Seq).setValuesByName(pnd_Qtr);                                                                                               //Natural: MOVE BY NAME #QTR TO #REC ( #REC-SEQ )
                    if (condition(pnd_Qtr_Pnd_Adj_St_Tax.getValue("*").notEquals(getZero()) || pnd_Qtr_Pnd_Adj_Lc_Tax.getValue("*").notEquals(getZero())))                //Natural: IF #QTR.#ADJ-ST-TAX ( * ) NE 0 OR #QTR.#ADJ-LC-TAX ( * ) NE 0
                    {
                        pnd_Adj_To_Make.setValue(true);                                                                                                                   //Natural: ASSIGN #ADJ-TO-MAKE := TRUE
                        FOR02:                                                                                                                                            //Natural: FOR #I = 1 TO #ADJ-IDX-MAX
                        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Adj_Idx_Max)); pnd_I.nadd(1))
                        {
                            if (condition(pnd_Qtr_Pnd_Adj_St_Tax.getValue(pnd_I).notEquals(getZero())))                                                                   //Natural: IF #QTR.#ADJ-ST-TAX ( #I ) NE 0
                            {
                                pnd_Totals_Pnd_Tot_Amt_Adj_Qtr.getValue(pnd_I).nadd(pnd_Qtr_Pnd_Adj_St_Tax.getValue(pnd_I));                                              //Natural: ADD #QTR.#ADJ-ST-TAX ( #I ) TO #TOT-AMT-ADJ-QTR ( #I )
                                pnd_Totals_Pnd_Tot_Amt_Adj.nadd(pnd_Qtr_Pnd_Adj_St_Tax.getValue(pnd_I));                                                                  //Natural: ADD #QTR.#ADJ-ST-TAX ( #I ) TO #TOT-AMT-ADJ
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(pnd_Qtr_Pnd_Adj_Lc_Tax.getValue(pnd_I).notEquals(getZero())))                                                                   //Natural: IF #QTR.#ADJ-LC-TAX ( #I ) NE 0
                            {
                                pnd_Totals_Pnd_Tot_Loc_Adj_Qtr.getValue(pnd_I).nadd(pnd_Qtr_Pnd_Adj_Lc_Tax.getValue(pnd_I));                                              //Natural: ADD #QTR.#ADJ-LC-TAX ( #I ) TO #TOT-LOC-ADJ-QTR ( #I )
                                pnd_Totals_Pnd_Tot_Loc_Adj.nadd(pnd_Qtr_Pnd_Adj_Lc_Tax.getValue(pnd_I));                                                                  //Natural: ADD #QTR.#ADJ-LC-TAX ( #I ) TO #TOT-LOC-ADJ
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*                                                                                                                                                   //Natural: AT BREAK OF #QTR.#ANNT-SOC-SEC-NBR
                    pnd_Save_Tiaa_Cref.setValue(pnd_Qtr_Pnd_Tiaa_Cref);                                                                                                   //Natural: AT BREAK OF #QTR.#TIAA-CREF;//Natural: ASSIGN #SAVE-TIAA-CREF := #QTR.#TIAA-CREF
                    readWork01Pnd_Annt_Soc_Sec_NbrOld.setValue(pnd_Qtr_Pnd_Annt_Soc_Sec_Nbr);                                                                             //Natural: END-WORK
                    readWork01Ss_Employee_NameOld.setValue(pnd_Qtr_Ss_Employee_Name);
                    readWork01Pnd_Tiaa_CrefOld.setValue(pnd_Qtr_Pnd_Tiaa_Cref);
                }
                READWORK01_Exit:
                if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                {
                    atBreakEventReadwork01(endOfDataReadwork01);
                }
                if (Global.isEscape()) return;
                //* *- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *------------------------------------
                //* *------------
                //* *----------------------------------------------------
                //* *------------
                //*        '    '
                //* *------------
                //* ***  EDS
                //* *------------
                //* *----------------------------------
                //* *------------
                //* *------------
                //* *--------------------------------------------
                //* *RE.E-STATE-CODE           :=  '06'
                //* *------------
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( INPUTRPT )
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( DTLRPTD )
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( DTLNRPTD )
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( SUMRPT )
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( ADJRPT )
                //* *---------
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Define_Run_Type() throws Exception                                                                                                                   //Natural: DEFINE-RUN-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ---------------
        //*         (DEFINE RUN-TYPE TO BE PRINTED IN THE HEADER)
        if (condition(pnd_Input_Parm_Pnd_Year_End_Adj_Ind.equals("Y")))                                                                                                   //Natural: IF #YEAR-END-ADJ-IND = 'Y'
        {
            pnd_Run_Type.setValue(DbsUtil.compress(pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy, " YEAD END ADJUSTMENT"));                                                       //Natural: COMPRESS #PYMNT-DATE-TO-YYYY ' YEAD END ADJUSTMENT' TO #RUN-TYPE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            short decideConditionsMet698 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #PYMNT-DATE-TO-MM;//Natural: VALUE 03
            if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(3))))
            {
                decideConditionsMet698++;
                pnd_Run_Type.setValue(DbsUtil.compress("1ST QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '1ST QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: VALUE 06
            else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(6))))
            {
                decideConditionsMet698++;
                pnd_Run_Type.setValue(DbsUtil.compress("2ND QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '2ND QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: VALUE 09
            else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(9))))
            {
                decideConditionsMet698++;
                pnd_Run_Type.setValue(DbsUtil.compress("3RD QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '3RD QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: VALUE 12
            else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(12))))
            {
                decideConditionsMet698++;
                pnd_Run_Type.setValue(DbsUtil.compress("4TH QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '4TH QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Lookup_Name_In_Core() throws Exception                                                                                                               //Natural: LOOKUP-NAME-IN-CORE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Core_Found.setValue(false);                                                                                                                                   //Natural: ASSIGN #CORE-FOUND := FALSE
        pnd_Cor_Super_Ssn_Rcdtype_Pnd_Ph_Social_Security_No.setValue(pnd_Qtr_Pnd_Annt_Soc_Sec_Nbr);                                                                       //Natural: ASSIGN #PH-SOCIAL-SECURITY-NO := #QTR.#ANNT-SOC-SEC-NBR
        pnd_Cor_Super_Ssn_Rcdtype_Pnd_Ph_Rcd_Type_Cde.setValue(1);                                                                                                        //Natural: ASSIGN #PH-RCD-TYPE-CDE := 01
        //*  FE201505 START
        ldaTwrlpsg1.getPnd_Psgm001().reset();                                                                                                                             //Natural: RESET #PSGM001
        ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Function().setValue("PR");                                                                                                      //Natural: ASSIGN #IN-FUNCTION := 'PR'
        ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Addr_Usg_Typ().setValue("CO");                                                                                                  //Natural: ASSIGN #IN-ADDR-USG-TYP := 'CO'
        ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Type().setValue("SSN");                                                                                                         //Natural: ASSIGN #IN-TYPE := 'SSN'
        ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Pin_Ssn().setValueEdited(pnd_Qtr_Pnd_Annt_Soc_Sec_Nbr,new ReportEditMask("999999999"));                                         //Natural: MOVE EDITED #QTR.#ANNT-SOC-SEC-NBR ( EM = 999999999 ) TO #IN-PIN-SSN
        psgm001ReturnCode = DbsUtil.callExternalProgram("PSGM001",ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Function(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Addr_Usg_Typ(),          //Natural: CALL 'PSGM001' #PSGM001
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Type(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Pin_Ssn(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Ret_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Input_String(),
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Out_Pin(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Out_Ssn(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Inact_Date(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Birth_Date(),
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Dec_Date(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Gender_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Last_Name(),
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_First_Name(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Second_Name(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Suffix_Desc(),
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Person_Org_Cd(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_1(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_2(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_3(),
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_4(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_5(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_City(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Postal_Code(),
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Iso_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Country(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Irs_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Irs_Country_Nm(),
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_State_Nm(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Geo_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_Type_Cd(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Rcrd_Status());
        if (condition(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Rcrd_Status().equals("Y") && (ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Last_Name().notEquals(" ") ||                     //Natural: IF #RT-RCRD-STATUS EQ 'Y' AND ( #RT-LAST-NAME NE ' ' OR #RT-ADDR-1 NE ' ' )
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_1().notEquals(" "))))
        {
            pnd_Core_Found.setValue(true);                                                                                                                                //Natural: ASSIGN #CORE-FOUND := TRUE
            rs_Rs_First_Name.setValue(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_First_Name());                                                                                    //Natural: ASSIGN RS.RS-FIRST-NAME := #RT-FIRST-NAME
            rs_Rs_Middle_Name.setValue(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Second_Name());                                                                                  //Natural: ASSIGN RS.RS-MIDDLE-NAME := #RT-SECOND-NAME
            rs_Rs_Last_Name.setValue(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Last_Name());                                                                                      //Natural: ASSIGN RS.RS-LAST-NAME := #RT-LAST-NAME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  FE201505 START
            ldaTwrlpsg1.getPnd_Psgm001().reset();                                                                                                                         //Natural: RESET #PSGM001
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Function().setValue("PR");                                                                                                  //Natural: ASSIGN #IN-FUNCTION := 'PR'
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Addr_Usg_Typ().setValue("RS");                                                                                              //Natural: ASSIGN #IN-ADDR-USG-TYP := 'RS'
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Type().setValue("SSN");                                                                                                     //Natural: ASSIGN #IN-TYPE := 'SSN'
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Pin_Ssn().setValueEdited(pnd_Qtr_Pnd_Annt_Soc_Sec_Nbr,new ReportEditMask("999999999"));                                     //Natural: MOVE EDITED #QTR.#ANNT-SOC-SEC-NBR ( EM = 999999999 ) TO #IN-PIN-SSN
            psgm001ReturnCode = DbsUtil.callExternalProgram("PSGM001",ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Function(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Addr_Usg_Typ(),      //Natural: CALL 'PSGM001' #PSGM001
                ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Type(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Pin_Ssn(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Ret_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Input_String(),
                ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Out_Pin(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Out_Ssn(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Inact_Date(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Birth_Date(),
                ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Dec_Date(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Gender_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Last_Name(),
                ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_First_Name(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Second_Name(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Suffix_Desc(),
                ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Person_Org_Cd(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_1(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_2(),
                ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_3(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_4(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_5(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_City(),
                ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Postal_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Iso_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Country(),
                ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Irs_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Irs_Country_Nm(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_State_Nm(),
                ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Geo_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_Type_Cd(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Rcrd_Status());
            if (condition(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Rcrd_Status().equals("Y") && (ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Last_Name().notEquals(" ")                    //Natural: IF #RT-RCRD-STATUS EQ 'Y' AND ( #RT-LAST-NAME NE ' ' OR #RT-ADDR-1 NE ' ' )
                || ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_1().notEquals(" "))))
            {
                pnd_Core_Found.setValue(true);                                                                                                                            //Natural: ASSIGN #CORE-FOUND := TRUE
                rs_Rs_First_Name.setValue(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_First_Name());                                                                                //Natural: ASSIGN RS.RS-FIRST-NAME := #RT-FIRST-NAME
                rs_Rs_Middle_Name.setValue(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Second_Name());                                                                              //Natural: ASSIGN RS.RS-MIDDLE-NAME := #RT-SECOND-NAME
                rs_Rs_Last_Name.setValue(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Last_Name());                                                                                  //Natural: ASSIGN RS.RS-LAST-NAME := #RT-LAST-NAME
                //*  FE201505 END
            }                                                                                                                                                             //Natural: END-IF
            //*  FE201505 END
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Lookup_Name_In_Tax_Participant_File() throws Exception                                                                                               //Natural: LOOKUP-NAME-IN-TAX-PARTICIPANT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Part_Found.setValue(false);                                                                                                                                   //Natural: ASSIGN #PART-FOUND := FALSE
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Part_Tax_Id_N.setValue(pnd_Qtr_Pnd_Annt_Soc_Sec_Nbr);                                                                                //Natural: ASSIGN #PART-TAX-ID-N := #QTR.#ANNT-SOC-SEC-NBR
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Part_Status.setValue(" ");                                                                                                           //Natural: ASSIGN #PART-STATUS := ' '
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Part_99999999.setValue("99999999");                                                                                                  //Natural: ASSIGN #PART-99999999 := '99999999'
        vw_part.startDatabaseRead                                                                                                                                         //Natural: READ ( 1 ) PART WITH TWRPARTI-CURR-REC-SD = #TWRPARTI-CURR-REC-SD
        (
        "PA1",
        new Wc[] { new Wc("TWRPARTI_CURR_REC_SD", ">=", pnd_Twrparti_Curr_Rec_Sd, WcType.BY) },
        new Oc[] { new Oc("TWRPARTI_CURR_REC_SD", "ASC") },
        1
        );
        PA1:
        while (condition(vw_part.readNextRow("PA1")))
        {
            if (condition(part_Twrparti_Tax_Id.equals(pnd_Twrparti_Curr_Rec_Sd_Pnd_Part_Tax_Id) && part_Twrparti_Status.equals(pnd_Twrparti_Curr_Rec_Sd_Pnd_Part_Status)  //Natural: IF PART.TWRPARTI-TAX-ID = #PART-TAX-ID AND PART.TWRPARTI-STATUS = #PART-STATUS AND PART.TWRPARTI-PART-EFF-END-DTE = #PART-99999999
                && part_Twrparti_Part_Eff_End_Dte.equals(pnd_Twrparti_Curr_Rec_Sd_Pnd_Part_99999999)))
            {
                pnd_Part_Found.setValue(true);                                                                                                                            //Natural: ASSIGN #PART-FOUND := TRUE
                pnd_Temp_Participant_Name.setValue(part_Twrparti_Participant_Name);                                                                                       //Natural: ASSIGN #TEMP-PARTICIPANT-NAME := PART.TWRPARTI-PARTICIPANT-NAME
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Write_Detail_Tax_Filing_And_Report() throws Exception                                                                                                //Natural: WRITE-DETAIL-TAX-FILING-AND-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------------------------
        if (condition(pnd_Input_Parm_Pnd_Year_End_Adj_Ind.equals("N")))                                                                                                   //Natural: IF #YEAR-END-ADJ-IND = 'N'
        {
            //*  << CTS
            if (condition(pnd_Save_Tiaa_Cref.equals("1")))                                                                                                                //Natural: IF #SAVE-TIAA-CREF EQ '1'
            {
                rs_Rs_Sort_Ind.setValue("12");                                                                                                                            //Natural: MOVE '12' TO RS.RS-SORT-IND
                rs_E_State_Ein.setValue("53421613");                                                                                                                      //Natural: ASSIGN RS.E-STATE-EIN := '53421613'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Save_Tiaa_Cref.equals("2")))                                                                                                            //Natural: IF #SAVE-TIAA-CREF EQ '2'
                {
                    rs_Rs_Sort_Ind.setValue("22");                                                                                                                        //Natural: MOVE '22' TO RS.RS-SORT-IND
                    rs_E_State_Ein.setValue("81508640");                                                                                                                  //Natural: ASSIGN RS.E-STATE-EIN := '81508640'
                }                                                                                                                                                         //Natural: END-IF
                //*  >> CTS
            }                                                                                                                                                             //Natural: END-IF
            //*                                                            /* CTS2
            pnd_First_Name_Len.reset();                                                                                                                                   //Natural: RESET #FIRST-NAME-LEN #LAST-NAME-LEN
            pnd_Last_Name_Len.reset();
            if (condition(rs_Rs_First_Name.equals(" ") || ! (DbsUtil.maskMatches(rs_Rs_First_Name,"A"))))                                                                 //Natural: IF RS.RS-FIRST-NAME EQ ' ' OR RS.RS-FIRST-NAME NE MASK ( A )
            {
                rs_Rs_First_Name.setValue("NONAME");                                                                                                                      //Natural: ASSIGN RS.RS-FIRST-NAME := 'NONAME'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(rs_Rs_Last_Name.equals(" ") || ! (DbsUtil.maskMatches(rs_Rs_Last_Name,"A"))))                                                                   //Natural: IF RS.RS-LAST-NAME EQ ' ' OR RS.RS-LAST-NAME NE MASK ( A )
            {
                rs_Rs_Last_Name.setValue("NONAME");                                                                                                                       //Natural: ASSIGN RS.RS-LAST-NAME := 'NONAME'
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(rs_Rs_First_Name), new ExamineSearch("X"), new ExamineGivingLength(pnd_First_Name_Len));                                    //Natural: EXAMINE RS.RS-FIRST-NAME FOR 'X' GIVING LENGTH IN #FIRST-NAME-LEN
            DbsUtil.examine(new ExamineSource(rs_Rs_Last_Name), new ExamineSearch("X"), new ExamineGivingLength(pnd_Last_Name_Len));                                      //Natural: EXAMINE RS.RS-LAST-NAME FOR 'X' GIVING LENGTH IN #LAST-NAME-LEN
            if (condition(rs_Rs_First_Name.getSubstring(pnd_First_Name_Len.getInt(),1).equals(",") || rs_Rs_First_Name.getSubstring(pnd_First_Name_Len.getInt(),          //Natural: IF SUBSTR ( RS.RS-FIRST-NAME,#FIRST-NAME-LEN,1 ) EQ ',' OR EQ '.'
                1).equals(".")))
            {
                setValueToSubstring(" ",rs_Rs_First_Name,pnd_First_Name_Len.getInt(),1);                                                                                  //Natural: MOVE ' ' TO SUBSTR ( RS.RS-FIRST-NAME,#FIRST-NAME-LEN,1 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(rs_Rs_Last_Name.getSubstring(pnd_Last_Name_Len.getInt(),1).equals(",") || rs_Rs_Last_Name.getSubstring(pnd_Last_Name_Len.getInt(),              //Natural: IF SUBSTR ( RS.RS-LAST-NAME,#LAST-NAME-LEN,1 ) EQ ',' OR EQ '.'
                1).equals(".")))
            {
                setValueToSubstring(" ",rs_Rs_Last_Name,pnd_Last_Name_Len.getInt(),1);                                                                                    //Natural: MOVE ' ' TO SUBSTR ( RS.RS-LAST-NAME,#LAST-NAME-LEN,1 )
            }                                                                                                                                                             //Natural: END-IF
            //*                                                            /* CTS2
            getWorkFiles().write(2, false, rs);                                                                                                                           //Natural: WRITE WORK FILE 02 RS
            //*  CTS
        }                                                                                                                                                                 //Natural: END-IF
        re_E_Nbr_Employees.nadd(1);                                                                                                                                       //Natural: ASSIGN RE.E-NBR-EMPLOYEES := RE.E-NBR-EMPLOYEES + 1
        getReports().display(0, "SSN",                                                                                                                                    //Natural: DISPLAY ( DTLRPTD ) 'SSN' RS.RS-SOC-SEC-NBR ( EM = XXX-XX-XXXX ) 'First/Name' RS.RS-FIRST-NAME 'Middle/Name' RS.RS-MIDDLE-NAME 'Last/Name' RS.RS-LAST-NAME 'Tax Withheld' RS.RS-TAX-WITHHOLD ( EM = Z,ZZZ,ZZ9.99- )
        		rs_Rs_Soc_Sec_Nbr, new ReportEditMask ("XXX-XX-XXXX"),"First/Name",
        		rs_Rs_First_Name,"Middle/Name",
        		rs_Rs_Middle_Name,"Last/Name",
        		rs_Rs_Last_Name,"Tax Withheld",
        		rs_Rs_Tax_Withhold, new ReportEditMask ("Z,ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
    }
    private void sub_Input_Record_Report() throws Exception                                                                                                               //Natural: INPUT-RECORD-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().display(0, "SSN",                                                                                                                                    //Natural: DISPLAY ( INPUTRPT ) 'SSN' #QTR.#ANNT-SOC-SEC-NBR ( EM = 999-99-9999 ) 'CONTRACT' #QTR.#CNTRCT-PPCN-NBR ( AL = 8 ) / 'PAYEE' #QTR.#CNTRCT-PAYEE-CDE 'TAX WITHHELD' #QTR.#QTR-ST-TAX ( 1 ) / 'ADJUSTMENT ' #QTR.#ADJ-ST-TAX ( 1 ) 'YEAR-TO-DATE' #QTR.#YTD-ST-TAX ( 1 ) 'TAX WITHHELD' #QTR.#QTR-ST-TAX ( 2 ) / 'ADJUSTMENT ' #QTR.#ADJ-ST-TAX ( 2 ) 'YEAR-TO-DATE' #QTR.#YTD-ST-TAX ( 2 ) 'TAX WITHHELD' #QTR.#QTR-ST-TAX ( 3 ) / 'ADJUSTMENT ' #QTR.#ADJ-ST-TAX ( 3 ) 'YEAR-TO-DATE' #QTR.#YTD-ST-TAX ( 3 ) 'TAX WITHHELD' #QTR.#QTR-ST-TAX ( 4 ) / 'ADJUSTMENT ' #QTR.#ADJ-ST-TAX ( 4 ) 'YEAR-TO-DATE' #QTR.#YTD-ST-TAX ( 4 ) / 'YTD-EOY-ADJ ' #QTR.#YTD-ST-TAX ( 5 )
        		pnd_Qtr_Pnd_Annt_Soc_Sec_Nbr, new ReportEditMask ("999-99-9999"),"CONTRACT",
        		pnd_Qtr_Pnd_Cntrct_Ppcn_Nbr, new AlphanumericLength (8),NEWLINE,"PAYEE",
        		pnd_Qtr_Pnd_Cntrct_Payee_Cde,"TAX WITHHELD",
        		pnd_Qtr_Pnd_Qtr_St_Tax.getValue(1),NEWLINE,"ADJUSTMENT ",
        		pnd_Qtr_Pnd_Adj_St_Tax.getValue(1),"YEAR-TO-DATE",
        		pnd_Qtr_Pnd_Ytd_St_Tax.getValue(1),"TAX WITHHELD",
        		pnd_Qtr_Pnd_Qtr_St_Tax.getValue(2),NEWLINE,"ADJUSTMENT ",
        		pnd_Qtr_Pnd_Adj_St_Tax.getValue(2),"YEAR-TO-DATE",
        		pnd_Qtr_Pnd_Ytd_St_Tax.getValue(2),"TAX WITHHELD",
        		pnd_Qtr_Pnd_Qtr_St_Tax.getValue(3),NEWLINE,"ADJUSTMENT ",
        		pnd_Qtr_Pnd_Adj_St_Tax.getValue(3),"YEAR-TO-DATE",
        		pnd_Qtr_Pnd_Ytd_St_Tax.getValue(3),"TAX WITHHELD",
        		pnd_Qtr_Pnd_Qtr_St_Tax.getValue(4),NEWLINE,"ADJUSTMENT ",
        		pnd_Qtr_Pnd_Adj_St_Tax.getValue(4),"YEAR-TO-DATE",
        		pnd_Qtr_Pnd_Ytd_St_Tax.getValue(4),NEWLINE,"YTD-EOY-ADJ ",
        		pnd_Qtr_Pnd_Ytd_St_Tax.getValue(5));
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #S 1 4
        for (pnd_S.setValue(1); condition(pnd_S.lessOrEqual(4)); pnd_S.nadd(1))
        {
            pnd_Totals_Pnd_Tot_Qtr_Tax.getValue(pnd_S).nadd(pnd_Qtr_Pnd_Qtr_St_Tax.getValue(pnd_S));                                                                      //Natural: ADD #QTR.#QTR-ST-TAX ( #S ) TO #TOT-QTR-TAX ( #S )
            pnd_Totals_Pnd_Tot_Adj_Tax.getValue(pnd_S).nadd(pnd_Qtr_Pnd_Adj_St_Tax.getValue(pnd_S));                                                                      //Natural: ADD #QTR.#ADJ-ST-TAX ( #S ) TO #TOT-ADJ-TAX ( #S )
            pnd_Totals_Pnd_Tot_Ytd_Tax.getValue(pnd_S).nadd(pnd_Qtr_Pnd_Ytd_St_Tax.getValue(pnd_S));                                                                      //Natural: ADD #QTR.#YTD-ST-TAX ( #S ) TO #TOT-YTD-TAX ( #S )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Totals_Pnd_Tot_Ytd_Tax.getValue(5).nadd(pnd_Qtr_Pnd_Ytd_St_Tax.getValue(5));                                                                                  //Natural: ADD #QTR.#YTD-ST-TAX ( 5 ) TO #TOT-YTD-TAX ( 5 )
    }
    private void sub_Adjustment_Report() throws Exception                                                                                                                 //Natural: ADJUSTMENT-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 TO #REC-SEQ
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Rec_Seq)); pnd_I.nadd(1))
        {
            getReports().display(0, "SSN",                                                                                                                                //Natural: DISPLAY ( ADJRPT ) 'SSN' #REC.#ANNT-SOC-SEC-NBR ( #I ) ( EM = 999-99-9999 ) / 'CONTRACT-PY OR NAME ' #REC.FORM-CNTRCT-PY-NMBR ( #I ) ( EM = XXXXXXXX-XX ) 'TAX WITHHELD' #REC.#QTR-ST-TAX ( #I,1 ) / 'ADJUSTMENT ' #REC.#ADJ-ST-TAX ( #I,1 ) 'YEAR-TO-DATE' #REC.#YTD-ST-TAX ( #I,1 ) 'TAX WITHHELD' #REC.#QTR-ST-TAX ( #I,2 ) / 'ADJUSTMENT ' #REC.#ADJ-ST-TAX ( #I,2 ) 'YEAR-TO-DATE' #REC.#YTD-ST-TAX ( #I,2 ) 'TAX WITHHELD' #REC.#QTR-ST-TAX ( #I,3 ) / 'ADJUSTMENT ' #REC.#ADJ-ST-TAX ( #I,3 ) 'YEAR-TO-DATE' #REC.#YTD-ST-TAX ( #I,3 ) 'TAX WITHHELD' #REC.#QTR-ST-TAX ( #I,4 ) / 'ADJUSTMENT ' #REC.#ADJ-ST-TAX ( #I,4 ) 'YEAR-TO-DATE' #REC.#YTD-ST-TAX ( #I,4 ) / 'YTD-EOY-ADJ ' #REC.#YTD-ST-TAX ( #I,5 ) /
            		pnd_Rec_Pnd_Annt_Soc_Sec_Nbr.getValue(pnd_I), new ReportEditMask ("999-99-9999"),NEWLINE,"CONTRACT-PY OR NAME ",
            		pnd_Rec_Form_Cntrct_Py_Nmbr.getValue(pnd_I), new ReportEditMask ("XXXXXXXX-XX"),"TAX WITHHELD",
            		pnd_Rec_Pnd_Qtr_St_Tax.getValue(pnd_I,1),NEWLINE,"ADJUSTMENT ",
            		pnd_Rec_Pnd_Adj_St_Tax.getValue(pnd_I,1),"YEAR-TO-DATE",
            		pnd_Rec_Pnd_Ytd_St_Tax.getValue(pnd_I,1),"TAX WITHHELD",
            		pnd_Rec_Pnd_Qtr_St_Tax.getValue(pnd_I,2),NEWLINE,"ADJUSTMENT ",
            		pnd_Rec_Pnd_Adj_St_Tax.getValue(pnd_I,2),"YEAR-TO-DATE",
            		pnd_Rec_Pnd_Ytd_St_Tax.getValue(pnd_I,2),"TAX WITHHELD",
            		pnd_Rec_Pnd_Qtr_St_Tax.getValue(pnd_I,3),NEWLINE,"ADJUSTMENT ",
            		pnd_Rec_Pnd_Adj_St_Tax.getValue(pnd_I,3),"YEAR-TO-DATE",
            		pnd_Rec_Pnd_Ytd_St_Tax.getValue(pnd_I,3),"TAX WITHHELD",
            		pnd_Rec_Pnd_Qtr_St_Tax.getValue(pnd_I,4),NEWLINE,"ADJUSTMENT ",
            		pnd_Rec_Pnd_Adj_St_Tax.getValue(pnd_I,4),"YEAR-TO-DATE",
            		pnd_Rec_Pnd_Ytd_St_Tax.getValue(pnd_I,4),NEWLINE,"YTD-EOY-ADJ ",
            		pnd_Rec_Pnd_Ytd_St_Tax.getValue(pnd_I,5),NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(6, "-",new RepeatItem(20),"-",new RepeatItem(13),"-",new RepeatItem(13),"-",new RepeatItem(13),"-",new RepeatItem(13),"-",new                  //Natural: WRITE ( ADJRPT ) '-' ( 20 ) '-' ( 13 ) '-' ( 13 ) '-' ( 13 ) '-' ( 13 ) '-' ( 13 ) '-' ( 13 ) '-' ( 13 ) '-' ( 13 ) / #OLD-ANNT-SSN ( EM = 999-99-9999 ) 22T #SSN.#QTR-ST-TAX ( 1 ) #SSN.#YTD-ST-TAX ( 1 ) #SSN.#QTR-ST-TAX ( 2 ) #SSN.#YTD-ST-TAX ( 2 ) #SSN.#QTR-ST-TAX ( 3 ) #SSN.#YTD-ST-TAX ( 3 ) #SSN.#QTR-ST-TAX ( 4 ) #SSN.#YTD-ST-TAX ( 4 ) / #OLD-ANNT-NAME ( AL = 20 ) 22T #SSN.#ADJ-ST-TAX ( 1 ) 15X #SSN.#ADJ-ST-TAX ( 2 ) 15X #SSN.#ADJ-ST-TAX ( 3 ) 15X #SSN.#ADJ-ST-TAX ( 4 ) #SSN.#YTD-ST-TAX ( 5 )
            RepeatItem(13),"-",new RepeatItem(13),"-",new RepeatItem(13),"-",new RepeatItem(13),NEWLINE,pnd_Old_Annt_Ssn, new ReportEditMask ("999-99-9999"),new 
            TabSetting(22),pnd_Ssn_Pnd_Qtr_St_Tax.getValue(1), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),pnd_Ssn_Pnd_Ytd_St_Tax.getValue(1), new ReportEditMask 
            ("Z,ZZZ,ZZ9.99-"),pnd_Ssn_Pnd_Qtr_St_Tax.getValue(2), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),pnd_Ssn_Pnd_Ytd_St_Tax.getValue(2), new ReportEditMask 
            ("Z,ZZZ,ZZ9.99-"),pnd_Ssn_Pnd_Qtr_St_Tax.getValue(3), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),pnd_Ssn_Pnd_Ytd_St_Tax.getValue(3), new ReportEditMask 
            ("Z,ZZZ,ZZ9.99-"),pnd_Ssn_Pnd_Qtr_St_Tax.getValue(4), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),pnd_Ssn_Pnd_Ytd_St_Tax.getValue(4), new ReportEditMask 
            ("Z,ZZZ,ZZ9.99-"),NEWLINE,pnd_Old_Annt_Name, new AlphanumericLength (20),new TabSetting(22),pnd_Ssn_Pnd_Adj_St_Tax.getValue(1), new ReportEditMask 
            ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(15),pnd_Ssn_Pnd_Adj_St_Tax.getValue(2), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(15),pnd_Ssn_Pnd_Adj_St_Tax.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(15),pnd_Ssn_Pnd_Adj_St_Tax.getValue(4), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),pnd_Ssn_Pnd_Ytd_St_Tax.getValue(5), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
        getReports().write(6, "*",new RepeatItem(132),NEWLINE);                                                                                                           //Natural: WRITE ( ADJRPT ) '*' ( 132 ) /
        if (Global.isEscape()) return;
    }
    private void sub_Zero_Amount_Detail_Report() throws Exception                                                                                                         //Natural: ZERO-AMOUNT-DETAIL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        getReports().display(0, "SSN",                                                                                                                                    //Natural: DISPLAY ( DTLNRPTD ) 'SSN' RS.RS-SOC-SEC-NBR ( EM = XXX-XX-XXXX ) 'First/Name' RS.RS-FIRST-NAME 'Middle/Name' RS.RS-MIDDLE-NAME 'Last/Name' RS.RS-LAST-NAME 'Tax Withheld' RS.RS-TAX-WITHHOLD ( EM = Z,ZZZ,ZZ9.99- )
        		rs_Rs_Soc_Sec_Nbr, new ReportEditMask ("XXX-XX-XXXX"),"First/Name",
        		rs_Rs_First_Name,"Middle/Name",
        		rs_Rs_Middle_Name,"Last/Name",
        		rs_Rs_Last_Name,"Tax Withheld",
        		rs_Rs_Tax_Withhold, new ReportEditMask ("Z,ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
    }
    //*  << CTS
    private void sub_Write_Header_Trailer_Record() throws Exception                                                                                                       //Natural: WRITE-HEADER-TRAILER-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        re_E_Tax_Year.setValue(pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy);                                                                                                    //Natural: ASSIGN RE.E-TAX-YEAR := #PYMNT-DATE-TO-YYYY
        re_E_Ret_Qtr.setValue(pnd_Per);                                                                                                                                   //Natural: ASSIGN RE.E-RET-QTR := #PER
        //* TIAA
        if (condition(pnd_Save_Tiaa_Cref.equals("1")))                                                                                                                    //Natural: IF #SAVE-TIAA-CREF = '1'
        {
            re_E_State_Ein.setValue("53421613");                                                                                                                          //Natural: ASSIGN RE.E-STATE-EIN := '53421613'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            re_E_State_Ein.setValue("81508640");                                                                                                                          //Natural: ASSIGN RE.E-STATE-EIN := '81508640'
            //*  >> CTS
            //*  06/16/10
            //*  06/16/10
        }                                                                                                                                                                 //Natural: END-IF
        re_E_Wh_Tot_Wages.setValue(0);                                                                                                                                    //Natural: ASSIGN RE.E-WH-TOT-WAGES := 00000000000000
        re_E_Month1_Employees.setValue(0);                                                                                                                                //Natural: ASSIGN RE.E-MONTH1-EMPLOYEES := 0000000
        re_E_Month2_Employees.setValue(0);                                                                                                                                //Natural: ASSIGN RE.E-MONTH2-EMPLOYEES := 0000000
        re_E_Month3_Employees.setValue(0);                                                                                                                                //Natural: ASSIGN RE.E-MONTH3-EMPLOYEES := 0000000
        re_E_Wh_Taxable_Wages.setValue(0);                                                                                                                                //Natural: ASSIGN RE.E-WH-TAXABLE-WAGES := 00000000000000
        pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().setValue(pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy);                                                                       //Natural: ASSIGN #TWRACOM2.#TAX-YEAR := #PYMNT-DATE-TO-YYYY
        pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(1);                                                                                                          //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 1
        //* CTS
        //* CTS
        if (condition(pnd_Save_Tiaa_Cref.equals("2")))                                                                                                                    //Natural: IF #SAVE-TIAA-CREF = '2'
        {
            pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue("X");                                                                                                    //Natural: ASSIGN #TWRACOM2.#COMP-CODE := 'X'
            //* CTS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  EINCHG START
            if (condition(pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().greaterOrEqual(2018)))                                                                               //Natural: IF #TWRACOM2.#TAX-YEAR GE 2018
            {
                pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue("A");                                                                                                //Natural: ASSIGN #TWRACOM2.#COMP-CODE := 'A'
                //* CTS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue("T");                                                                                                //Natural: ASSIGN #TWRACOM2.#COMP-CODE := 'T'
            }                                                                                                                                                             //Natural: END-IF
            //*  EINCHG END
            //* CTS
        }                                                                                                                                                                 //Natural: END-IF
        //*  06/16/10 - CALL COMPANY CODE TABLE
        //*  06/16/10
        DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOM2' #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        re_E_Buss_Name.setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Name());                                                                                             //Natural: ASSIGN RE.E-BUSS-NAME := #TWRACOM2.#COMP-NAME
        //*  06/16/10
        //*  06/16/10
        //*  06/16/10
        //*  06/16/10
        re_E_Add_Line_2.reset();                                                                                                                                          //Natural: RESET RE.E-ADD-LINE-2
        re_E_Add_Line_1.setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Address().getValue(1));                                                                                  //Natural: ASSIGN RE.E-ADD-LINE-1 := #TWRACOM2.#ADDRESS ( 1 )
        re_E_City.setValue(pdaTwracom2.getPnd_Twracom2_Pnd_City());                                                                                                       //Natural: ASSIGN RE.E-CITY := #TWRACOM2.#CITY
        re_E_Zip_Code.setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Zip_9());                                                                                                  //Natural: ASSIGN RE.E-ZIP-CODE := #TWRACOM2.#ZIP-9
        //*  << CTS
        if (condition(pnd_Save_Tiaa_Cref.equals("1")))                                                                                                                    //Natural: IF #SAVE-TIAA-CREF EQ '1'
        {
            re_E_Sort_Ind.setValue("11");                                                                                                                                 //Natural: MOVE '11' TO RE.E-SORT-IND
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Save_Tiaa_Cref.equals("2")))                                                                                                                //Natural: IF #SAVE-TIAA-CREF EQ '2'
            {
                re_E_Sort_Ind.setValue("21");                                                                                                                             //Natural: MOVE '21' TO RE.E-SORT-IND
            }                                                                                                                                                             //Natural: END-IF
            //*  >> CTS
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Parm_Pnd_Year_End_Adj_Ind.equals("N")))                                                                                                   //Natural: IF #YEAR-END-ADJ-IND = 'N'
        {
            //*    ONE HEADER RECORD PER COMPANY
            getWorkFiles().write(2, false, re);                                                                                                                           //Natural: WRITE WORK FILE 02 RE
        }                                                                                                                                                                 //Natural: END-IF
        //*  CTS
        //*  CTS
        re_E_Wh_Tot_Income_Tax.reset();                                                                                                                                   //Natural: RESET RE.E-WH-TOT-INCOME-TAX RE.E-NBR-EMPLOYEES
        re_E_Nbr_Employees.reset();
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------------
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(50),"ILLINOIS QUARTERLY TAX REPORTING",new  //Natural: WRITE ( INPUTRPT ) NOTITLE NOHDR *INIT-USER '-' *PROGRAM 50T 'ILLINOIS QUARTERLY TAX REPORTING' 110T *DATX ( EM = MM/DD/YYYY ) *TIMX / 'REPORT 01' 44T 'INPUT RECORDS DETAIL LISTING BY SSN, PPCN-PAYEE' 110T 'PAGE:' *PAGE-NUMBER ( INPUTRPT )
                        TabSetting(110),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),Global.getTIMX(),NEWLINE,"REPORT 01",new TabSetting(44),"INPUT RECORDS DETAIL LISTING BY SSN, PPCN-PAYEE",new 
                        TabSetting(110),"PAGE:",getReports().getPageNumberDbs(2));
                    if (condition(pnd_Qtr_Pnd_Tiaa_Cref.equals("1")))                                                                                                     //Natural: IF #QTR.#TIAA-CREF = '1'
                    {
                        getReports().write(2, new TabSetting(60),"TIAA CONTRACTS");                                                                                       //Natural: WRITE ( INPUTRPT ) 60T 'TIAA CONTRACTS'
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Qtr_Pnd_Tiaa_Cref.equals("2")))                                                                                                     //Natural: IF #QTR.#TIAA-CREF = '2'
                    {
                        getReports().write(2, new TabSetting(60),"TRUST CONTRACTS");                                                                                      //Natural: WRITE ( INPUTRPT ) 60T 'TRUST CONTRACTS'
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(2, new TabSetting(57),pnd_Run_Type,NEWLINE,NEWLINE,NEWLINE," ",new RepeatItem(26),"FIRST QUARTER"," ",new RepeatItem(13),"SECOND QUARTER"," ",new  //Natural: WRITE ( INPUTRPT ) 57T #RUN-TYPE /// ' ' ( 26 ) 'FIRST QUARTER' ' ' ( 13 ) 'SECOND QUARTER' ' ' ( 12 ) 'THIRD QUARTER' ' ' ( 09 ) 'FOURTH QUARTER/EOY-ADJ'
                        RepeatItem(12),"THIRD QUARTER"," ",new RepeatItem(9),"FOURTH QUARTER/EOY-ADJ");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *----------------------
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(50),"ILLINOIS QUARTERLY TAX REPORTING",new  //Natural: WRITE ( DTLRPTD ) NOTITLE NOHDR *INIT-USER '-' *PROGRAM 50T 'ILLINOIS QUARTERLY TAX REPORTING' 110T *DATX ( EM = MM/DD/YYYY ) *TIMX / 'REPORT 02' 47T 'STATE TAX REPORTED DETAIL LISTING BY SSN' 110T 'PAGE:' *PAGE-NUMBER ( DTLRPTD )
                        TabSetting(110),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),Global.getTIMX(),NEWLINE,"REPORT 02",new TabSetting(47),"STATE TAX REPORTED DETAIL LISTING BY SSN",new 
                        TabSetting(110),"PAGE:",getReports().getPageNumberDbs(3));
                    if (condition(pnd_Qtr_Pnd_Tiaa_Cref.equals("1")))                                                                                                     //Natural: IF #QTR.#TIAA-CREF = '1'
                    {
                        getReports().write(3, new TabSetting(60),"TIAA CONTRACTS");                                                                                       //Natural: WRITE ( DTLRPTD ) 60T 'TIAA CONTRACTS'
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Qtr_Pnd_Tiaa_Cref.equals("2")))                                                                                                     //Natural: IF #QTR.#TIAA-CREF = '2'
                    {
                        getReports().write(3, new TabSetting(60),"TRUST CONTRACTS");                                                                                      //Natural: WRITE ( DTLRPTD ) 60T 'TRUST CONTRACTS'
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(3, new TabSetting(57),pnd_Run_Type,NEWLINE,NEWLINE,NEWLINE);                                                                       //Natural: WRITE ( DTLRPTD ) 57T #RUN-TYPE ///
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------------
                    getReports().write(4, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(50),"ILLINOIS QUARTERLY TAX REPORTING",new  //Natural: WRITE ( DTLNRPTD ) NOTITLE NOHDR *INIT-USER '-' *PROGRAM 50T 'ILLINOIS QUARTERLY TAX REPORTING' 110T *DATX ( EM = MM/DD/YYYY ) *TIMX / 'REPORT 03' 45T 'STATE TAX NON-REPORTED DETAIL LISTING BY SSN' 110T 'PAGE:' *PAGE-NUMBER ( DTLNRPTD )
                        TabSetting(110),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),Global.getTIMX(),NEWLINE,"REPORT 03",new TabSetting(45),"STATE TAX NON-REPORTED DETAIL LISTING BY SSN",new 
                        TabSetting(110),"PAGE:",getReports().getPageNumberDbs(4));
                    if (condition(pnd_Qtr_Pnd_Tiaa_Cref.equals("1")))                                                                                                     //Natural: IF #QTR.#TIAA-CREF = '1'
                    {
                        getReports().write(4, new TabSetting(60),"TIAA CONTRACTS");                                                                                       //Natural: WRITE ( DTLNRPTD ) 60T 'TIAA CONTRACTS'
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Qtr_Pnd_Tiaa_Cref.equals("2")))                                                                                                     //Natural: IF #QTR.#TIAA-CREF = '2'
                    {
                        getReports().write(4, new TabSetting(60),"TRUST CONTRACTS");                                                                                      //Natural: WRITE ( DTLNRPTD ) 60T 'TRUST CONTRACTS'
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(4, new TabSetting(57),pnd_Run_Type,NEWLINE,NEWLINE,NEWLINE);                                                                       //Natural: WRITE ( DTLNRPTD ) 57T #RUN-TYPE ///
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *---------------------
                    getReports().write(5, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(50),"ILLINOIS QUARTERLY TAX REPORTING",new  //Natural: WRITE ( SUMRPT ) NOTITLE NOHDR *INIT-USER '-' *PROGRAM 50T 'ILLINOIS QUARTERLY TAX REPORTING' 110T *DATX ( EM = MM/DD/YYYY ) *TIMX / 'REPORT 04' 60T 'SUMMARY REPORT' 110T 'PAGE:' *PAGE-NUMBER ( SUMRPT )
                        TabSetting(110),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),Global.getTIMX(),NEWLINE,"REPORT 04",new TabSetting(60),"SUMMARY REPORT",new 
                        TabSetting(110),"PAGE:",getReports().getPageNumberDbs(5));
                    if (condition(pnd_Qtr_Pnd_Tiaa_Cref.equals("1")))                                                                                                     //Natural: IF #QTR.#TIAA-CREF = '1'
                    {
                        getReports().write(5, new TabSetting(60),"TIAA CONTRACTS");                                                                                       //Natural: WRITE ( SUMRPT ) 60T 'TIAA CONTRACTS'
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Qtr_Pnd_Tiaa_Cref.equals("2")))                                                                                                     //Natural: IF #QTR.#TIAA-CREF = '2'
                    {
                        getReports().write(5, new TabSetting(60),"TRUST CONTRACTS");                                                                                      //Natural: WRITE ( SUMRPT ) 60T 'TRUST CONTRACTS'
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(5, new TabSetting(57),pnd_Run_Type,NEWLINE,NEWLINE,NEWLINE);                                                                       //Natural: WRITE ( SUMRPT ) 57T #RUN-TYPE ///
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt5 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *---------------------
                    getReports().write(6, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(50),"ILLINOIS QUARTERLY TAX REPORTING",new  //Natural: WRITE ( ADJRPT ) NOTITLE NOHDR *INIT-USER '-' *PROGRAM 50T 'ILLINOIS QUARTERLY TAX REPORTING' 110T *DATX ( EM = MM/DD/YYYY ) *TIMX / 'REPORT 05' 49T 'ADJUSTMENT REPORT BY SSN, PPCN-PAYEE' 110T 'PAGE:' *PAGE-NUMBER ( ADJRPT )
                        TabSetting(110),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),Global.getTIMX(),NEWLINE,"REPORT 05",new TabSetting(49),"ADJUSTMENT REPORT BY SSN, PPCN-PAYEE",new 
                        TabSetting(110),"PAGE:",getReports().getPageNumberDbs(6));
                    if (condition(pnd_Qtr_Pnd_Tiaa_Cref.equals("1")))                                                                                                     //Natural: IF #QTR.#TIAA-CREF = '1'
                    {
                        getReports().write(6, new TabSetting(60),"TIAA CONTRACTS");                                                                                       //Natural: WRITE ( ADJRPT ) 60T 'TIAA CONTRACTS'
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Qtr_Pnd_Tiaa_Cref.equals("2")))                                                                                                     //Natural: IF #QTR.#TIAA-CREF = '2'
                    {
                        getReports().write(6, new TabSetting(60),"TRUST CONTRACTS");                                                                                      //Natural: WRITE ( ADJRPT ) 60T 'TRUST CONTRACTS'
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(6, new TabSetting(57),pnd_Run_Type,NEWLINE,NEWLINE,NEWLINE," ",new RepeatItem(26),"FIRST QUARTER"," ",new RepeatItem(13),"SECOND QUARTER"," ",new  //Natural: WRITE ( ADJRPT ) 57T #RUN-TYPE /// ' ' ( 26 ) 'FIRST QUARTER' ' ' ( 13 ) 'SECOND QUARTER' ' ' ( 12 ) 'THIRD QUARTER' ' ' ( 09 ) 'FOURTH QUARTER/EOY-ADJ'
                        RepeatItem(12),"THIRD QUARTER"," ",new RepeatItem(9),"FOURTH QUARTER/EOY-ADJ");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Qtr_Pnd_Annt_Soc_Sec_NbrIsBreak = pnd_Qtr_Pnd_Annt_Soc_Sec_Nbr.isBreak(endOfData);
        boolean pnd_Qtr_Pnd_Tiaa_CrefIsBreak = pnd_Qtr_Pnd_Tiaa_Cref.isBreak(endOfData);
        if (condition(pnd_Qtr_Pnd_Annt_Soc_Sec_NbrIsBreak || pnd_Qtr_Pnd_Tiaa_CrefIsBreak))
        {
            //* *----------------------------------
            pnd_Old_Annt_Ssn.setValue(readWork01Pnd_Annt_Soc_Sec_NbrOld);                                                                                                 //Natural: ASSIGN #OLD-ANNT-SSN := OLD ( #QTR.#ANNT-SOC-SEC-NBR )
            pnd_Old_Annt_Name.setValue(readWork01Ss_Employee_NameOld);                                                                                                    //Natural: ASSIGN #OLD-ANNT-NAME := OLD ( #QTR.SS-EMPLOYEE-NAME )
            if (condition(rs_Rs_Tax_Withhold.notEquals(getZero())))                                                                                                       //Natural: IF RS.RS-TAX-WITHHOLD NE 0
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL-TAX-FILING-AND-REPORT
                sub_Write_Detail_Tax_Filing_And_Report();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Totals_Pnd_Tot_Cnt_Reported.nadd(1);                                                                                                                  //Natural: ADD 1 TO #TOT-CNT-REPORTED
                pnd_Totals_Pnd_Tot_Amt_Reported.nadd(pnd_Ssn_Pnd_Qtr_St_Tax.getValue(pnd_Per));                                                                           //Natural: ADD #SSN.#QTR-ST-TAX ( #PER ) TO #TOT-AMT-REPORTED
                pnd_Totals_Pnd_Tot_Loc_Reported.nadd(pnd_Ssn_Pnd_Qtr_Lc_Tax.getValue(pnd_Per));                                                                           //Natural: ADD #SSN.#QTR-LC-TAX ( #PER ) TO #TOT-LOC-REPORTED
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM ZERO-AMOUNT-DETAIL-REPORT
                sub_Zero_Amount_Detail_Report();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Totals_Pnd_Tot_Cnt_Not_Reported.nadd(1);                                                                                                              //Natural: ADD 1 TO #TOT-CNT-NOT-REPORTED
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Adj_To_Make.getBoolean()))                                                                                                                  //Natural: IF #ADJ-TO-MAKE
            {
                                                                                                                                                                          //Natural: PERFORM ADJUSTMENT-REPORT
                sub_Adjustment_Report();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Totals_Pnd_Tot_Cnt_Adj_Sspnd.nadd(1);                                                                                                                 //Natural: ADD 1 TO #TOT-CNT-ADJ-SS#
                pnd_Totals_Pnd_Tot_Cnt_Adj_Sspnd__Contractpnd.nadd(pnd_Rec_Seq);                                                                                          //Natural: ADD #REC-SEQ TO #TOT-CNT-ADJ-SS#-CONTRACT#
            }                                                                                                                                                             //Natural: END-IF
            pnd_New_Record.setValue(true);                                                                                                                                //Natural: ASSIGN #NEW-RECORD := TRUE
            pnd_Totals_Pnd_Tot_Cnt_In_Unq_Sspnd.nadd(1);                                                                                                                  //Natural: ADD 1 TO #TOT-CNT-IN-UNQ-SS#
            rs_Rs_Tax_Withhold.reset();                                                                                                                                   //Natural: RESET RS.RS-TAX-WITHHOLD RS.RS-TOT-WAGES RS.RS-QTR-GROSS-AMT #SSN #REC ( * ) #REC-SEQ #ADJ-TO-MAKE
            rs_Rs_Tot_Wages.reset();
            rs_Rs_Qtr_Gross_Amt.reset();
            pnd_Ssn.reset();
            pnd_Rec.getValue("*").reset();
            pnd_Rec_Seq.reset();
            pnd_Adj_To_Make.reset();
            //*  TOPS R3 '1'=TIAA, '2'=TRUST  11/09/04
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Qtr_Pnd_Tiaa_CrefIsBreak))
        {
            //* *---------------------------
            //*  EDS
            getReports().write(2, NEWLINE,NEWLINE,"T O T A L - - >",new TabSetting(22),pnd_Totals_Pnd_Tot_Qtr_Tax.getValue(1), new ReportEditMask ("ZZZZZ,ZZ9.99-"),new   //Natural: WRITE ( INPUTRPT ) // 'T O T A L - - >' 22T #TOT-QTR-TAX ( 1 ) 1X #TOT-YTD-TAX ( 1 ) 1X #TOT-QTR-TAX ( 2 ) 1X #TOT-YTD-TAX ( 2 ) 1X #TOT-QTR-TAX ( 3 ) 1X #TOT-YTD-TAX ( 3 ) 1X #TOT-QTR-TAX ( 4 ) 1X #TOT-YTD-TAX ( 4 ) / 22T #TOT-ADJ-TAX ( 1 ) 50T #TOT-ADJ-TAX ( 2 ) 78T #TOT-ADJ-TAX ( 3 ) 106T #TOT-ADJ-TAX ( 4 ) 1X #TOT-YTD-TAX ( 5 ) /// 'TOTAL NUMBER OF RECORDS:                ' 6X #TOT-CNT-IN-UNQ-SS#-CONTRACT# / 'TOTAL NUMBER OF RECORDS WITH UNIQUE SS#:' 6X #TOT-CNT-IN-UNQ-SS# / 'TOTAL TAX WITHHELD AMOUNT FOR THIS QTR: ' #TOT-AMT-IN
                ColumnSpacing(1),pnd_Totals_Pnd_Tot_Ytd_Tax.getValue(1), new ReportEditMask ("ZZZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Totals_Pnd_Tot_Qtr_Tax.getValue(2), 
                new ReportEditMask ("ZZZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Totals_Pnd_Tot_Ytd_Tax.getValue(2), new ReportEditMask ("ZZZZZ,ZZ9.99-"),new 
                ColumnSpacing(1),pnd_Totals_Pnd_Tot_Qtr_Tax.getValue(3), new ReportEditMask ("ZZZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Totals_Pnd_Tot_Ytd_Tax.getValue(3), 
                new ReportEditMask ("ZZZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Totals_Pnd_Tot_Qtr_Tax.getValue(4), new ReportEditMask ("ZZZZZ,ZZ9.99-"),new 
                ColumnSpacing(1),pnd_Totals_Pnd_Tot_Ytd_Tax.getValue(4), new ReportEditMask ("ZZZZZ,ZZ9.99-"),NEWLINE,new TabSetting(22),pnd_Totals_Pnd_Tot_Adj_Tax.getValue(1), 
                new ReportEditMask ("ZZZZZ,ZZ9.99-"),new TabSetting(50),pnd_Totals_Pnd_Tot_Adj_Tax.getValue(2), new ReportEditMask ("ZZZZZ,ZZ9.99-"),new 
                TabSetting(78),pnd_Totals_Pnd_Tot_Adj_Tax.getValue(3), new ReportEditMask ("ZZZZZ,ZZ9.99-"),new TabSetting(106),pnd_Totals_Pnd_Tot_Adj_Tax.getValue(4), 
                new ReportEditMask ("ZZZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Totals_Pnd_Tot_Ytd_Tax.getValue(5), new ReportEditMask ("ZZZZZ,ZZ9.99-"),NEWLINE,NEWLINE,NEWLINE,"TOTAL NUMBER OF RECORDS:                ",new 
                ColumnSpacing(6),pnd_Totals_Pnd_Tot_Cnt_In_Unq_Sspnd__Contractpnd, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"TOTAL NUMBER OF RECORDS WITH UNIQUE SS#:",new 
                ColumnSpacing(6),pnd_Totals_Pnd_Tot_Cnt_In_Unq_Sspnd, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"TOTAL TAX WITHHELD AMOUNT FOR THIS QTR: ",pnd_Totals_Pnd_Tot_Amt_In, 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99-"));
            if (condition(Global.isEscape())) return;
            getReports().write(3, NEWLINE,"TOTAL TAX WITHHELD AMOUNT REPORTED :",new TabSetting(39),pnd_Totals_Pnd_Tot_Amt_Reported, new ReportEditMask                   //Natural: WRITE ( DTLRPTD ) / 'TOTAL TAX WITHHELD AMOUNT REPORTED :' 39T #TOT-AMT-REPORTED 87T #TOT-LOC-REPORTED / 'TOTAL NUMBER OF RECORDS WITH UNIQUE SS#:' 5X #TOT-CNT-REPORTED
                ("ZZ,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(87),pnd_Totals_Pnd_Tot_Loc_Reported, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99-"),NEWLINE,"TOTAL NUMBER OF RECORDS WITH UNIQUE SS#:",new 
                ColumnSpacing(5),pnd_Totals_Pnd_Tot_Cnt_Reported, new ReportEditMask ("Z,ZZZ,ZZ9"));
            if (condition(Global.isEscape())) return;
            getReports().write(4, NEWLINE,"TOTAL NUMBER OF RECORDS WITH UNIQUE SS#:",new ColumnSpacing(6),pnd_Totals_Pnd_Tot_Cnt_Not_Reported, new ReportEditMask         //Natural: WRITE ( DTLNRPTD ) / 'TOTAL NUMBER OF RECORDS WITH UNIQUE SS#:' 6X #TOT-CNT-NOT-REPORTED
                ("Z,ZZZ,ZZ9"));
            if (condition(Global.isEscape())) return;
            //*  MB
            //*  MB
            //*  MB
            //*  MB
            getReports().write(6, NEWLINE,"TOTAL ADJUSTMENT:",new TabSetting(19),pnd_Totals_Pnd_Tot_Amt_Adj_Qtr.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99-"),new  //Natural: WRITE ( ADJRPT ) / 'TOTAL ADJUSTMENT:' 19T #TOT-AMT-ADJ-QTR ( 1 ) 10X #TOT-AMT-ADJ-QTR ( 2 ) 10X #TOT-AMT-ADJ-QTR ( 3 ) 10X #TOT-AMT-ADJ-QTR ( 4 ) / 'YEAR-TO-DATE TOTAL ADJUSTMENT:' 5X #TOT-AMT-ADJ / 'TOTAL NUMBER OF RECORDS:                ' #TOT-CNT-ADJ-SS#-CONTRACT# / 'TOTAL NUMBER OF RECORDS WITH UNIQUE SS#:' #TOT-CNT-ADJ-SS#
                ColumnSpacing(10),pnd_Totals_Pnd_Tot_Amt_Adj_Qtr.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(10),pnd_Totals_Pnd_Tot_Amt_Adj_Qtr.getValue(3), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(10),pnd_Totals_Pnd_Tot_Amt_Adj_Qtr.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99-"),NEWLINE,"YEAR-TO-DATE TOTAL ADJUSTMENT:",new 
                ColumnSpacing(5),pnd_Totals_Pnd_Tot_Amt_Adj, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99-"),NEWLINE,"TOTAL NUMBER OF RECORDS:                ",pnd_Totals_Pnd_Tot_Cnt_Adj_Sspnd__Contractpnd, 
                new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"TOTAL NUMBER OF RECORDS WITH UNIQUE SS#:",pnd_Totals_Pnd_Tot_Cnt_Adj_Sspnd, new ReportEditMask 
                ("Z,ZZZ,ZZ9"));
            if (condition(Global.isEscape())) return;
            getReports().print(0, "=",readWork01Pnd_Tiaa_CrefOld);                                                                                                        //Natural: PRINT '=' OLD ( #QTR.#TIAA-CREF )
            if (condition(readWork01Pnd_Tiaa_CrefOld.equals("1")))                                                                                                        //Natural: IF OLD ( #QTR.#TIAA-CREF ) = '1'
            {
                getReports().write(5, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"*",new RepeatItem(3),"TOTAL TIAA INPUT RECORDS WITH UNIQUE SS#:",new ColumnSpacing(11),pnd_Totals_Pnd_Tot_Cnt_In_Unq_Sspnd,  //Natural: WRITE ( SUMRPT ) ///// '*' ( 3 ) 'TOTAL TIAA INPUT RECORDS WITH UNIQUE SS#:' 11X #TOT-CNT-IN-UNQ-SS# // '*' ( 3 ) 'TOTAL TIAA INPUT STATE TAX FOR THIS QTR :' 6X #TOT-AMT-IN // '*' ( 3 ) 'TOTAL TIAA STATE EMPLOYEES NOT-REPORTED :' 11X #TOT-CNT-NOT-REPORTED // '*' ( 3 ) 'TOTAL TIAA STATE EMPLOYEES REPORTED     :' 11X #TOT-CNT-REPORTED // '*' ( 3 ) 'TOTAL TIAA STATE TAX REPORTED           :' 6X #TOT-AMT-REPORTED
                    new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,"*",new RepeatItem(3),"TOTAL TIAA INPUT STATE TAX FOR THIS QTR :",new ColumnSpacing(6),pnd_Totals_Pnd_Tot_Amt_In, 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99-"),NEWLINE,NEWLINE,"*",new RepeatItem(3),"TOTAL TIAA STATE EMPLOYEES NOT-REPORTED :",new ColumnSpacing(11),pnd_Totals_Pnd_Tot_Cnt_Not_Reported, 
                    new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,"*",new RepeatItem(3),"TOTAL TIAA STATE EMPLOYEES REPORTED     :",new ColumnSpacing(11),pnd_Totals_Pnd_Tot_Cnt_Reported, 
                    new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,"*",new RepeatItem(3),"TOTAL TIAA STATE TAX REPORTED           :",new ColumnSpacing(6),pnd_Totals_Pnd_Tot_Amt_Reported, 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99-"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(readWork01Pnd_Tiaa_CrefOld.equals("2")))                                                                                                        //Natural: IF OLD ( #QTR.#TIAA-CREF ) = '2'
            {
                getReports().write(5, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"*",new RepeatItem(3),"TOTAL TRUST INPUT RECORDS WITH UNIQUE SS#:",new ColumnSpacing(11),pnd_Totals_Pnd_Tot_Cnt_In_Unq_Sspnd,  //Natural: WRITE ( SUMRPT ) ///// '*' ( 3 ) 'TOTAL TRUST INPUT RECORDS WITH UNIQUE SS#:' 11X #TOT-CNT-IN-UNQ-SS# // '*' ( 3 ) 'TOTAL TRUST INPUT STATE TAX FOR THIS QTR :' 6X #TOT-AMT-IN // '*' ( 3 ) 'TOTAL TRUST STATE EMPLOYEES NOT-REPORTED :' 11X #TOT-CNT-NOT-REPORTED // '*' ( 3 ) 'TOTAL TRUST STATE EMPLOYEES REPORTED     :' 11X #TOT-CNT-REPORTED // '*' ( 3 ) 'TOTAL TRUST STATE TAX REPORTED           :' 6X #TOT-AMT-REPORTED
                    new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,"*",new RepeatItem(3),"TOTAL TRUST INPUT STATE TAX FOR THIS QTR :",new ColumnSpacing(6),pnd_Totals_Pnd_Tot_Amt_In, 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99-"),NEWLINE,NEWLINE,"*",new RepeatItem(3),"TOTAL TRUST STATE EMPLOYEES NOT-REPORTED :",new ColumnSpacing(11),pnd_Totals_Pnd_Tot_Cnt_Not_Reported, 
                    new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,"*",new RepeatItem(3),"TOTAL TRUST STATE EMPLOYEES REPORTED     :",new ColumnSpacing(11),pnd_Totals_Pnd_Tot_Cnt_Reported, 
                    new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,"*",new RepeatItem(3),"TOTAL TRUST STATE TAX REPORTED           :",new ColumnSpacing(6),pnd_Totals_Pnd_Tot_Amt_Reported, 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99-"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_Totals.reset();                                                                                                                                           //Natural: RESET #TOTALS
            //*   CTS
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADER-TRAILER-RECORD
            sub_Write_Header_Trailer_Record();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( INPUTRPT )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( SUMRPT )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( DTLRPTD )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( DTLNRPTD )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( ADJRPT )
            if (condition(Global.isEscape())){return;}
            //* CTS
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
        Global.format(2, "PS=60 LS=133");
        Global.format(3, "PS=60 LS=133");
        Global.format(4, "PS=60 LS=133");
        Global.format(5, "PS=60 LS=133");

        getReports().setDisplayColumns(0, "SSN",
        		rs_Rs_Soc_Sec_Nbr, new ReportEditMask ("XXX-XX-XXXX"),"First/Name",
        		rs_Rs_First_Name,"Middle/Name",
        		rs_Rs_Middle_Name,"Last/Name",
        		rs_Rs_Last_Name,"Tax Withheld",
        		rs_Rs_Tax_Withhold, new ReportEditMask ("Z,ZZZ,ZZ9.99-"));
    }
}
