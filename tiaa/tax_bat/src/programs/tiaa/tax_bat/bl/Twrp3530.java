/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:35:44 PM
**        * FROM NATURAL PROGRAM : Twrp3530
************************************************************
**        * FILE NAME            : Twrp3530.java
**        * CLASS NAME           : Twrp3530
**        * INSTANCE NAME        : Twrp3530
************************************************************
************************************************************************
** PROGRAM : TWRP3530
** SYSTEM  : TAXWARS
** AUTHOR  : MICHAEL SUPONITSKY
** FUNCTION: 480.6 CORRECTION REPORTING
** HISTORY.....:
** 05/22/09    MS. - 2008 CHANGES.
** 01/30/08    A. YOUNG  - REVISED TO ACCOMMODATE FORM 480.7C, WHICH
**             REPLACES FORMS 480.6A AND 480.6B AS OF TAX YEAR 2007.
**             REPLACED TWRL5000 WITH TWRAFRMN / TWRNFRMN.
** 09/18/02    E.BOTTERI - THIS PRG. IS NOW A DRIVER FOR PUERTO RICO
**             CORRECTION REPORTING. IT FETCHES TWRP3532 FOR PROCESS
**             BEFORE 2002 & TWRP3533 FOR PROCESS STARTING 2002.
**             BOTH PRGMS. ARE COPIED FROM THE ORIGINAL TWRP3530.
**
** 07/11/14    CTS - ADDED THE AMENDED DATE FIELD AS PART OF THE INPUT
**             PARM AND THIS IS PASSED TO 'TWRP3553' SUB-PROGRAM.
**             CHANGES ARE TAGGED WITH CTS.
** 07/14/15    RAHUL - 480.7C PHASE 1 CHANGES - ACCEPT RESUBMIT IND FROM
**             JCL                            - TAG DASRAH
************************************************************************
**

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3530 extends BLNatBase
{
    // Data Areas
    private PdaTwratbl4 pdaTwratbl4;
    private PdaTwrafrmn pdaTwrafrmn;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_form_U;
    private DbsField form_U_Tirf_Lu_User;
    private DbsField form_U_Tirf_Lu_Ts;
    private DbsField form_U_Tirf_Irs_Rpt_Ind;
    private DbsField form_U_Tirf_Irs_Rpt_Date;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Input_Parms;
    private DbsField pnd_Ws_Pnd_Form;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_Amended_Date;
    private DbsField pnd_Ws_Pnd_Resubmit_Ind;
    private DbsField pnd_Ws_Pnd_Datx;
    private DbsField pnd_Ws_Pnd_Timx;
    private DbsField pnd_Ws_Pnd_Prev_Rpt_Date;
    private DbsField pnd_Ws_Pnd_Terminate;
    private DbsField pnd_Ws_Pnd_I;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwratbl4 = new PdaTwratbl4(localVariables);
        pdaTwrafrmn = new PdaTwrafrmn(localVariables);

        // Local Variables

        vw_form_U = new DataAccessProgramView(new NameInfo("vw_form_U", "FORM-U"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form_U_Tirf_Lu_User = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Lu_User", "TIRF-LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_LU_USER");
        form_U_Tirf_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        form_U_Tirf_Lu_Ts = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Lu_Ts", "TIRF-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "TIRF_LU_TS");
        form_U_Tirf_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        form_U_Tirf_Irs_Rpt_Ind = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Irs_Rpt_Ind", "TIRF-IRS-RPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_IND");
        form_U_Tirf_Irs_Rpt_Ind.setDdmHeader("IRS/RPT/IND");
        form_U_Tirf_Irs_Rpt_Date = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Irs_Rpt_Date", "TIRF-IRS-RPT-DATE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_DATE");
        form_U_Tirf_Irs_Rpt_Date.setDdmHeader("IRS/RPT/DATE");
        registerRecord(vw_form_U);

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Input_Parms = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Ws_Pnd_Form = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Form", "#FORM", FieldType.STRING, 6);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Amended_Date = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Amended_Date", "#AMENDED-DATE", FieldType.NUMERIC, 6);
        pnd_Ws_Pnd_Resubmit_Ind = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Resubmit_Ind", "#RESUBMIT-IND", FieldType.NUMERIC, 1);
        pnd_Ws_Pnd_Datx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Datx", "#DATX", FieldType.DATE);
        pnd_Ws_Pnd_Timx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Timx", "#TIMX", FieldType.TIME);
        pnd_Ws_Pnd_Prev_Rpt_Date = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Prev_Rpt_Date", "#PREV-RPT-DATE", FieldType.DATE, new DbsArrayController(5, 
            6));
        pnd_Ws_Pnd_Terminate = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Terminate", "#TERMINATE", FieldType.NUMERIC, 3);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_form_U.reset();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3530() throws Exception
    {
        super("Twrp3530");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 80 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 37T 'TaxWaRS' 68T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 34T 'Control Report' 68T 'Report: RPT1' / //
        //* ******************
        //*  MAIN PROGRAM
        //* ******************
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT-PARMS
        sub_Process_Input_Parms();
        if (condition(Global.isEscape())) {return;}
        short decideConditionsMet153 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #WS.#TAX-YEAR;//Natural: VALUE 1999 : 2001
        if (condition(((pnd_Ws_Pnd_Tax_Year.greaterOrEqual(1999) && pnd_Ws_Pnd_Tax_Year.lessOrEqual(2001)))))
        {
            decideConditionsMet153++;
            Global.getSTACK().pushData(StackOption.TOP, pnd_Ws_Pnd_Form, pnd_Ws_Pnd_Tax_Year);                                                                            //Natural: FETCH 'TWRP3532' #WS.#FORM #WS.#TAX-YEAR
            Global.setFetchProgram(DbsUtil.getBlType("TWRP3532"));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: VALUE 2002 : 2007
        else if (condition(((pnd_Ws_Pnd_Tax_Year.greaterOrEqual(2002) && pnd_Ws_Pnd_Tax_Year.lessOrEqual(2007)))))
        {
            decideConditionsMet153++;
            Global.getSTACK().pushData(StackOption.TOP, pnd_Ws_Pnd_Form, pnd_Ws_Pnd_Tax_Year);                                                                            //Natural: FETCH 'TWRP3533' #WS.#FORM #WS.#TAX-YEAR
            Global.setFetchProgram(DbsUtil.getBlType("TWRP3533"));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            //*  CTS
            //*  DASRAH
            Global.getSTACK().pushData(StackOption.TOP, pnd_Ws_Pnd_Form, pnd_Ws_Pnd_Tax_Year, pnd_Ws_Pnd_Amended_Date, pnd_Ws_Pnd_Resubmit_Ind, pnd_Ws_Pnd_Datx,          //Natural: FETCH 'TWRP3553' #WS.#FORM #WS.#TAX-YEAR #WS.#AMENDED-DATE #WS.#RESUBMIT-IND #WS.#DATX #WS.#TIMX #WS.#PREV-RPT-DATE ( * )
                pnd_Ws_Pnd_Timx, pnd_Ws_Pnd_Prev_Rpt_Date.getValue("*"));
            Global.setFetchProgram(DbsUtil.getBlType("TWRP3553"));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* ***************************
        //*  S U B R O U T I N E S
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INPUT-PARMS
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CONTROL
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CONTROL
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TERMINATE-PROCESSING
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Process_Input_Parms() throws Exception                                                                                                               //Natural: PROCESS-INPUT-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("TWRP3530|sub_Process_Input_Parms");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_Input_Parms);                                                                               //Natural: INPUT #WS.#INPUT-PARMS
                //*  01/30/08 - AY
                if (condition(pnd_Ws_Pnd_Form.equals("480.6") || pnd_Ws_Pnd_Form.equals("480.7")))                                                                        //Natural: IF #WS.#FORM = '480.6' OR = '480.7'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Unknown input parmameter:",pnd_Ws_Pnd_Form,new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 1 ) '***' 25T 'Unknown input parmameter:' #WS.#FORM 77T '***' / '***' 25T 'Valid values are ...: "480.6" (480.6a & 480.6b),' 77T '***' / '***' 25T 'or 480.7c depending on Tax Year.' 77T '***'
                        TabSetting(25),"Valid values are ...: '480.6' (480.6a & 480.6b),",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"or 480.7c depending on Tax Year.",new 
                        TabSetting(77),"***");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    pnd_Ws_Pnd_Terminate.setValue(101);                                                                                                                   //Natural: ASSIGN #WS.#TERMINATE := 101
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESSING
                    sub_Terminate_Processing();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Ws_Pnd_Tax_Year.equals(getZero())))                                                                                                     //Natural: IF #WS.#TAX-YEAR EQ 0
                {
                    pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), Global.getDATN().divide(10000).subtract(1));                           //Natural: ASSIGN #WS.#TAX-YEAR := *DATN / 10000 - 1
                    //*  01/30/08 - AY
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"Puerto Rico Correction Reporting parameters:",NEWLINE,new TabSetting(23),                   //Natural: WRITE ( 1 ) 7T 'Puerto Rico Correction Reporting parameters:' / 23T 'Tax Year...........:' #WS.#TAX-YEAR
                    "Tax Year...........:",pnd_Ws_Pnd_Tax_Year);
                if (Global.isEscape()) return;
                pnd_Ws_Pnd_Datx.setValue(Global.getDATX());                                                                                                               //Natural: ASSIGN #WS.#DATX := *DATX
                pnd_Ws_Pnd_Timx.setValue(Global.getTIMX());                                                                                                               //Natural: ASSIGN #WS.#TIMX := *TIMX
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                                 //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #TWRAFRMN.#TAX-YEAR := #WS.#TAX-YEAR
                pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(true);                                                                                               //Natural: ASSIGN #TWRATBL4.#ABEND-IND := TRUE
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Display_Ind().setValue(true);                                                                                             //Natural: ASSIGN #TWRATBL4.#DISPLAY-IND := TRUE
                //*  01/30/08 - AY
                DbsUtil.callnat(Twrnfrmn.class , getCurrentProcessState(), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNFRMN' USING #TWRAFRMN.#INPUT-PARMS ( AD = O ) #TWRAFRMN.#OUTPUT-DATA ( AD = M )
                    pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                FOR01:                                                                                                                                                    //Natural: FOR #I = 5 TO 6
                for (pnd_Ws_Pnd_I.setValue(5); condition(pnd_Ws_Pnd_I.lessOrEqual(6)); pnd_Ws_Pnd_I.nadd(1))
                {
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(pnd_Ws_Pnd_I);                                                                                    //Natural: ASSIGN #TWRATBL4.#FORM-IND := #I
                    //*  01/30/08
                                                                                                                                                                          //Natural: PERFORM READ-CONTROL
                    sub_Read_Control();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(23),"Form...............: ",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Ws_Pnd_I.getInt() + 1)); //Natural: WRITE ( 1 ) / 23T 'Form...............: ' #TWRAFRMN.#FORM-NAME ( #I )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(! (pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Orig().getBoolean())))                                                                   //Natural: IF NOT #TWRATBL4.#TIRCNTL-IRS-ORIG
                    {
                        //*  01/30/08 - AY
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                        sub_Error_Display_Start();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Original Reporting for",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Ws_Pnd_I.getInt() + 1),new  //Natural: WRITE ( 1 ) '***' 25T 'Original Reporting for' #TWRAFRMN.#FORM-NAME ( #I ) 77T '***' / '***' 25T 'had not been run.  IRS-ORIG:' #TWRATBL4.#TIRCNTL-IRS-ORIG ( EM = FALSE/TRUE ) 77T '***'
                            TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"had not been run.  IRS-ORIG:",pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Orig(), 
                            new ReportEditMask ("FALSE/TRUE"),new TabSetting(77),"***");
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                        sub_Error_Display_End();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Ws_Pnd_Terminate.setValue(102);                                                                                                               //Natural: ASSIGN #WS.#TERMINATE := 102
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESSING
                        sub_Terminate_Processing();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaTwratbl4.getPnd_Twratbl4_C_Tircntl_Rpt_Tbl_Pe().equals(12)))                                                                         //Natural: IF C-TIRCNTL-RPT-TBL-PE = 12
                    {
                        //*  01/30/08 - AY
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                        sub_Error_Display_Start();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"11 cycles of Correction Reporting had",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 1 ) '***' 25T '11 cycles of Correction Reporting had' 77T '***' / '***' 25T 'been run for' #TWRAFRMN.#FORM-NAME ( #I ) 77T '***'
                            TabSetting(25),"been run for",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Ws_Pnd_I.getInt() + 1),new TabSetting(77),
                            "***");
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                        sub_Error_Display_End();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Ws_Pnd_Terminate.setValue(103);                                                                                                               //Natural: ASSIGN #WS.#TERMINATE := 103
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESSING
                        sub_Terminate_Processing();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition((pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(pdaTwratbl4.getPnd_Twratbl4_C_Tircntl_Rpt_Tbl_Pe()).add(25)).greater(pnd_Ws_Pnd_Datx))) //Natural: IF ( TIRCNTL-RPT-DTE ( C-TIRCNTL-RPT-TBL-PE ) + 25 ) > #WS.#DATX
                    {
                        //*  01/30/08
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                        sub_Error_Display_Start();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Last cycle of Correction Reporting had",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 1 ) '***' 25T 'Last cycle of Correction Reporting had' 77T '***' / '***' 25T 'been run for' #TWRAFRMN.#FORM-NAME ( #I ) 77T '***' / '***' 25T 'within previous 25 days on' TIRCNTL-RPT-DTE ( C-TIRCNTL-RPT-TBL-PE ) ( EM = MM/DD/YYYY ) 77T '***'
                            TabSetting(25),"been run for",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Ws_Pnd_I.getInt() + 1),new TabSetting(77),"***",NEWLINE,"***",new 
                            TabSetting(25),"within previous 25 days on",pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(pdaTwratbl4.getPnd_Twratbl4_C_Tircntl_Rpt_Tbl_Pe()), 
                            new ReportEditMask ("MM/DD/YYYY"),new TabSetting(77),"***");
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                        sub_Error_Display_End();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Ws_Pnd_Terminate.setValue(104);                                                                                                               //Natural: ASSIGN #WS.#TERMINATE := 104
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESSING
                        sub_Terminate_Processing();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Pnd_Prev_Rpt_Date.getValue(pnd_Ws_Pnd_I).setValue(pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(pdaTwratbl4.getPnd_Twratbl4_C_Tircntl_Rpt_Tbl_Pe())); //Natural: ASSIGN #WS.#PREV-RPT-DATE ( #I ) := TIRCNTL-RPT-DTE ( C-TIRCNTL-RPT-TBL-PE )
                    pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(pdaTwratbl4.getPnd_Twratbl4_C_Tircntl_Rpt_Tbl_Pe().getDec().add(1)).setValue(pnd_Ws_Pnd_Datx); //Natural: ASSIGN TIRCNTL-RPT-DTE ( C-TIRCNTL-RPT-TBL-PE + 1 ) := #WS.#DATX
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL
                    sub_Update_Control();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(13),"System Date used for this run:",pnd_Ws_Pnd_Datx, new ReportEditMask                //Natural: WRITE ( 1 ) / 13T 'System Date used for this run:' #WS.#DATX ( EM = MM/DD/YYYY ) / 13T 'System Time used for this run:' #WS.#TIMX ( EM = MM/DD/YYYY�HH:II:SS.T )
                    ("MM/DD/YYYY"),NEWLINE,new TabSetting(13),"System Time used for this run:",pnd_Ws_Pnd_Timx, new ReportEditMask ("MM/DD/YYYY HH:II:SS.T"));
                if (Global.isEscape()) return;
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Read_Control() throws Exception                                                                                                                      //Natural: READ-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4R' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Update_Control() throws Exception                                                                                                                    //Natural: UPDATE-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        DbsUtil.callnat(Twrntb4u.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4U' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Terminate_Processing() throws Exception                                                                                                              //Natural: TERMINATE-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        DbsUtil.terminate(pnd_Ws_Pnd_Terminate);  if (true) return;                                                                                                       //Natural: TERMINATE #WS.#TERMINATE
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=80 ZP=ON");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(37),"TaxWaRS",new TabSetting(68),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(34),"Control Report",new TabSetting(68),"Report: RPT1",NEWLINE,NEWLINE,NEWLINE);
    }
}
