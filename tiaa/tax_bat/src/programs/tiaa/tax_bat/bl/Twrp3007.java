/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:34:17 PM
**        * FROM NATURAL PROGRAM : Twrp3007
************************************************************
**        * FILE NAME            : Twrp3007.java
**        * CLASS NAME           : Twrp3007
**        * INSTANCE NAME        : Twrp3007
************************************************************
***********************************************************************
* PROGRAM  : TWRP3007
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : PRODUCES MAG. MEDIA FILES FOR MAG. MEDIA STATE REPORTING.
* CREATED  : 03 / 15 / 2019.
*   BY     : ARIVU
* FUNCTION : PROGRAM PRODUCES NY QUARTERLY FILE
*
***********************************************************************
* MODIFICATION
* ARIVU   : 03/27/2019 - ADDED THE LOGIC TO ADD THE QTRLY GROSS AMOUNT
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3007 extends BLNatBase
{
    // Data Areas
    private PdaTwratbl2 pdaTwratbl2;
    private LdaTwrl3502 ldaTwrl3502;
    private LdaTwrl3513 ldaTwrl3513;
    private PdaTwracom2 pdaTwracom2;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Debug;
    private DbsField re_In;

    private DbsGroup re_In__R_Field_1;
    private DbsField re_In_E_Filler_1;
    private DbsField re_In_E_Ret_Qtr;
    private DbsField re_In_E_Tax_Year;
    private DbsField re_In_E_State_Ein;
    private DbsField re_In_E_Buss_Name;
    private DbsField re_In_E_Add_Line_1;
    private DbsField re_In_E_Add_Line_2;
    private DbsField re_In_E_City;
    private DbsField re_In_E_Country;
    private DbsField re_In_E_Filler_2;
    private DbsField re_In_E_Zip_Code;
    private DbsField re_In_E_Filler_3;
    private DbsField re_In_E_Nbr_Employees;
    private DbsField re_In_E_Wh_Tot_Wages;
    private DbsField re_In_E_Wh_Tot_Income_Tax;
    private DbsField re_In_E_Wh_Taxable_Wages;
    private DbsField re_In_E_Month1_Employees;
    private DbsField re_In_E_Month2_Employees;
    private DbsField re_In_E_Month3_Employees;
    private DbsField re_In_E_Filler_4;
    private DbsField re_In_E_Transmitter;
    private DbsField re_In_E_Filler_5;

    private DbsGroup re_In__R_Field_2;
    private DbsField re_In_Rs_Soc_Sec_Nbr;
    private DbsField re_In_Rs_First_Name;
    private DbsField re_In_Rs_Middle_Name;
    private DbsField re_In_Rs_Last_Name;
    private DbsField re_In_Rs_Filler_1;
    private DbsField re_In_Rs_Wage_Plan_Code;
    private DbsField re_In_Rs_Filler_2;
    private DbsField re_In_Rs_Rpt_Prd;
    private DbsField re_In_Rs_Tot_Wages;
    private DbsField re_In_Rs_Filler_3;
    private DbsField re_In_Rs_State_Ein;
    private DbsField re_In_Rs_Filler_A;
    private DbsField re_In_Rs_Qtr_Gross_Amt;
    private DbsField re_In_Rs_Filler_4;
    private DbsField re_In_Rs_Tax_Withhold;
    private DbsField re_In_Rs_Filler_5;
    private DbsField re_In_Rs_Loc_Withhold;
    private DbsField re_In_Rs_Filler_6;
    private DbsField re_In_Rs_Transmitter;
    private DbsField re_In_Rs_Filler_7;

    private DbsGroup pnd_Work_Area;
    private DbsField pnd_Work_Area_Pnd_Tot_1w;
    private DbsField pnd_Work_Area_Pnd_Grand_1w;
    private DbsField pnd_Work_Area_Pnd_Grand_1e;
    private DbsField pnd_Work_Area_Pnd_Ws_Tot_Tax;
    private DbsField pnd_Work_Area_Pnd_Tot_State_Distr;
    private DbsField pnd_Work_Area_Pnd_Tot_Yea_Gross;
    private DbsField pnd_Work_Area_Pnd_Tot_State_Tax_Wthld;
    private DbsField pnd_Work_Area_Pnd_Today;
    private DbsField pnd_Work_Area_Pnd_Trailer;
    private DbsField pnd_Work_Area_Pnd_I;
    private DbsField pnd_Work_Area_Pnd_Old_Company_Cde;
    private DbsField pnd_Work_Area_Pnd_Tot_Loc_Distr;
    private DbsField pnd_Work_Area_Pnd_Tot_Loc_Tax_Wthld;
    private DbsField pnd_Work_Area_Pnd_Fil1;
    private DbsField pnd_Work_Area_Pnd_Fil2;
    private DbsField pnd_Work_Area_Pnd_Fil3;
    private DbsField pnd_Work_Area_Pnd_Fil4;
    private DbsField pnd_Work_Area_Pnd_Daten;

    private DbsGroup pnd_Work_Area__R_Field_3;
    private DbsField pnd_Work_Area_Pnd_Daten_Cc;
    private DbsField pnd_Work_Area_Pnd_Daten_Yy;
    private DbsField pnd_Work_Area_Pnd_Daten_Mm;
    private DbsField pnd_Work_Area_Pnd_Daten_Dd;
    private DbsField pnd_Mid_Name;

    private DbsGroup pnd_Mid_Name__R_Field_4;
    private DbsField pnd_Mid_Name_Pnd_Mid_Name1;
    private DbsField pnd_Report_Per;
    private DbsField pnd_Report_Mm;
    private DbsField pnd_Tax_Yyyy;

    private DbsGroup pnd_Tax_Yyyy__R_Field_5;
    private DbsField pnd_Tax_Yyyy_Pnd_Tax_Cc;
    private DbsField pnd_Tax_Yyyy_Pnd_Tax_Yy;
    private DbsField pnd_Ws_Name;
    private DbsField pnd_Rec_Seq_Number;
    private DbsField pnd_Prev_A_Rec_Company;
    private DbsField pnd_Prev_C_Rec_Company;
    private DbsField pnd_Temp_State_Id;

    private DbsGroup pnd_Temp_State_Id__R_Field_6;
    private DbsField pnd_Temp_State_Id_Pnd_Temp_State_Id_Fill;
    private DbsField pnd_Temp_State_Id_Pnd_Temp_State_Id_7;
    private DbsField pnd_Temp_State_Id_20;

    private DbsGroup pnd_Work_Out_Ny_Record;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec1;

    private DbsGroup pnd_Work_Out_Ny_Record__R_Field_7;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Id;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Tin;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Name;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank1;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Wages_Ind;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank2;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Gross;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank3;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Taxable_Amt;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank4;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Tax_Wthld;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank5;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec2;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec3;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec4;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec5;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec6;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec7;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec8;

    private DbsGroup pnd_W_Work_File;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Company_Cde;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Sys_Err;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Empty_Form;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_State_Distr;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_State_Tax_Wthld;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Loc_Code;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Loc_Distr;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Loc_Tax_Wthld;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Isn;

    private DbsGroup pnd_W_Work_File__R_Field_8;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Isn_A;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_State_Rpt_Ind;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_State_Code;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Tax_Year;

    private DbsGroup pnd_W_Work_File__R_Field_9;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Tax_Year_Cc;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Tax_Year_Yy;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Name;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Tin;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Account;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Addr_Ln1;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Addr_Ln2;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Addr_Ln3;
    private DbsField pnd_Out_Ny_Record;

    private DbsGroup pnd_Out_Ny_Record__R_Field_10;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Id;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Tin;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Name;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank1;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Wages_Ind;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank2;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Gross;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank3;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Taxable_Amt;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank4;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Tax_Wthld;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank5;
    private DbsField pnd_W_Accum_Taxable_Amt;
    private DbsField pnd_W_Accum_Tax_Wthld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        ldaTwrl3502 = new LdaTwrl3502();
        registerRecord(ldaTwrl3502);
        ldaTwrl3513 = new LdaTwrl3513();
        registerRecord(ldaTwrl3513);
        pdaTwracom2 = new PdaTwracom2(localVariables);

        // Local Variables
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        re_In = localVariables.newFieldInRecord("re_In", "RE-IN", FieldType.STRING, 512);

        re_In__R_Field_1 = localVariables.newGroupInRecord("re_In__R_Field_1", "REDEFINE", re_In);
        re_In_E_Filler_1 = re_In__R_Field_1.newFieldInGroup("re_In_E_Filler_1", "E-FILLER-1", FieldType.STRING, 12);
        re_In_E_Ret_Qtr = re_In__R_Field_1.newFieldInGroup("re_In_E_Ret_Qtr", "E-RET-QTR", FieldType.STRING, 1);
        re_In_E_Tax_Year = re_In__R_Field_1.newFieldInGroup("re_In_E_Tax_Year", "E-TAX-YEAR", FieldType.STRING, 4);
        re_In_E_State_Ein = re_In__R_Field_1.newFieldInGroup("re_In_E_State_Ein", "E-STATE-EIN", FieldType.STRING, 8);
        re_In_E_Buss_Name = re_In__R_Field_1.newFieldInGroup("re_In_E_Buss_Name", "E-BUSS-NAME", FieldType.STRING, 60);
        re_In_E_Add_Line_1 = re_In__R_Field_1.newFieldInGroup("re_In_E_Add_Line_1", "E-ADD-LINE-1", FieldType.STRING, 30);
        re_In_E_Add_Line_2 = re_In__R_Field_1.newFieldInGroup("re_In_E_Add_Line_2", "E-ADD-LINE-2", FieldType.STRING, 30);
        re_In_E_City = re_In__R_Field_1.newFieldInGroup("re_In_E_City", "E-CITY", FieldType.STRING, 28);
        re_In_E_Country = re_In__R_Field_1.newFieldInGroup("re_In_E_Country", "E-COUNTRY", FieldType.STRING, 3);
        re_In_E_Filler_2 = re_In__R_Field_1.newFieldInGroup("re_In_E_Filler_2", "E-FILLER-2", FieldType.STRING, 2);
        re_In_E_Zip_Code = re_In__R_Field_1.newFieldInGroup("re_In_E_Zip_Code", "E-ZIP-CODE", FieldType.STRING, 10);
        re_In_E_Filler_3 = re_In__R_Field_1.newFieldInGroup("re_In_E_Filler_3", "E-FILLER-3", FieldType.STRING, 2);
        re_In_E_Nbr_Employees = re_In__R_Field_1.newFieldInGroup("re_In_E_Nbr_Employees", "E-NBR-EMPLOYEES", FieldType.NUMERIC, 8);
        re_In_E_Wh_Tot_Wages = re_In__R_Field_1.newFieldInGroup("re_In_E_Wh_Tot_Wages", "E-WH-TOT-WAGES", FieldType.NUMERIC, 13, 2);
        re_In_E_Wh_Tot_Income_Tax = re_In__R_Field_1.newFieldInGroup("re_In_E_Wh_Tot_Income_Tax", "E-WH-TOT-INCOME-TAX", FieldType.NUMERIC, 13, 2);
        re_In_E_Wh_Taxable_Wages = re_In__R_Field_1.newFieldInGroup("re_In_E_Wh_Taxable_Wages", "E-WH-TAXABLE-WAGES", FieldType.NUMERIC, 13, 2);
        re_In_E_Month1_Employees = re_In__R_Field_1.newFieldInGroup("re_In_E_Month1_Employees", "E-MONTH1-EMPLOYEES", FieldType.NUMERIC, 7);
        re_In_E_Month2_Employees = re_In__R_Field_1.newFieldInGroup("re_In_E_Month2_Employees", "E-MONTH2-EMPLOYEES", FieldType.NUMERIC, 7);
        re_In_E_Month3_Employees = re_In__R_Field_1.newFieldInGroup("re_In_E_Month3_Employees", "E-MONTH3-EMPLOYEES", FieldType.NUMERIC, 7);
        re_In_E_Filler_4 = re_In__R_Field_1.newFieldInGroup("re_In_E_Filler_4", "E-FILLER-4", FieldType.STRING, 93);
        re_In_E_Transmitter = re_In__R_Field_1.newFieldInGroup("re_In_E_Transmitter", "E-TRANSMITTER", FieldType.STRING, 1);
        re_In_E_Filler_5 = re_In__R_Field_1.newFieldInGroup("re_In_E_Filler_5", "E-FILLER-5", FieldType.STRING, 160);

        re_In__R_Field_2 = localVariables.newGroupInRecord("re_In__R_Field_2", "REDEFINE", re_In);
        re_In_Rs_Soc_Sec_Nbr = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Soc_Sec_Nbr", "RS-SOC-SEC-NBR", FieldType.STRING, 9);
        re_In_Rs_First_Name = re_In__R_Field_2.newFieldInGroup("re_In_Rs_First_Name", "RS-FIRST-NAME", FieldType.STRING, 15);
        re_In_Rs_Middle_Name = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Middle_Name", "RS-MIDDLE-NAME", FieldType.STRING, 15);
        re_In_Rs_Last_Name = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Last_Name", "RS-LAST-NAME", FieldType.STRING, 23);
        re_In_Rs_Filler_1 = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Filler_1", "RS-FILLER-1", FieldType.STRING, 123);
        re_In_Rs_Wage_Plan_Code = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Wage_Plan_Code", "RS-WAGE-PLAN-CODE", FieldType.STRING, 1);
        re_In_Rs_Filler_2 = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Filler_2", "RS-FILLER-2", FieldType.STRING, 1);
        re_In_Rs_Rpt_Prd = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Rpt_Prd", "RS-RPT-PRD", FieldType.STRING, 6);
        re_In_Rs_Tot_Wages = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Tot_Wages", "RS-TOT-WAGES", FieldType.NUMERIC, 11, 2);
        re_In_Rs_Filler_3 = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Filler_3", "RS-FILLER-3", FieldType.STRING, 34);
        re_In_Rs_State_Ein = re_In__R_Field_2.newFieldInGroup("re_In_Rs_State_Ein", "RS-STATE-EIN", FieldType.STRING, 8);
        re_In_Rs_Filler_A = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Filler_A", "RS-FILLER-A", FieldType.STRING, 20);
        re_In_Rs_Qtr_Gross_Amt = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Qtr_Gross_Amt", "RS-QTR-GROSS-AMT", FieldType.NUMERIC, 11, 2);
        re_In_Rs_Filler_4 = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Filler_4", "RS-FILLER-4", FieldType.STRING, 1);
        re_In_Rs_Tax_Withhold = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Tax_Withhold", "RS-TAX-WITHHOLD", FieldType.NUMERIC, 10, 2);
        re_In_Rs_Filler_5 = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Filler_5", "RS-FILLER-5", FieldType.STRING, 1);
        re_In_Rs_Loc_Withhold = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Loc_Withhold", "RS-LOC-WITHHOLD", FieldType.NUMERIC, 10, 2);
        re_In_Rs_Filler_6 = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Filler_6", "RS-FILLER-6", FieldType.STRING, 52);
        re_In_Rs_Transmitter = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Transmitter", "RS-TRANSMITTER", FieldType.STRING, 1);
        re_In_Rs_Filler_7 = re_In__R_Field_2.newFieldInGroup("re_In_Rs_Filler_7", "RS-FILLER-7", FieldType.STRING, 160);

        pnd_Work_Area = localVariables.newGroupInRecord("pnd_Work_Area", "#WORK-AREA");
        pnd_Work_Area_Pnd_Tot_1w = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_1w", "#TOT-1W", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Grand_1w = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Grand_1w", "#GRAND-1W", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Grand_1e = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Grand_1e", "#GRAND-1E", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Ws_Tot_Tax = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Tot_Tax", "#WS-TOT-TAX", FieldType.NUMERIC, 10, 2);
        pnd_Work_Area_Pnd_Tot_State_Distr = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_State_Distr", "#TOT-STATE-DISTR", FieldType.NUMERIC, 
            18, 2);
        pnd_Work_Area_Pnd_Tot_Yea_Gross = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Yea_Gross", "#TOT-YEA-GROSS", FieldType.NUMERIC, 18, 2);
        pnd_Work_Area_Pnd_Tot_State_Tax_Wthld = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_State_Tax_Wthld", "#TOT-STATE-TAX-WTHLD", FieldType.NUMERIC, 
            13, 2);
        pnd_Work_Area_Pnd_Today = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Today", "#TODAY", FieldType.DATE);
        pnd_Work_Area_Pnd_Trailer = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Trailer", "#TRAILER", FieldType.STRING, 20);
        pnd_Work_Area_Pnd_I = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_I", "#I", FieldType.NUMERIC, 5);
        pnd_Work_Area_Pnd_Old_Company_Cde = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Old_Company_Cde", "#OLD-COMPANY-CDE", FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Tot_Loc_Distr = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Loc_Distr", "#TOT-LOC-DISTR", FieldType.NUMERIC, 18, 2);
        pnd_Work_Area_Pnd_Tot_Loc_Tax_Wthld = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Loc_Tax_Wthld", "#TOT-LOC-TAX-WTHLD", FieldType.NUMERIC, 
            13, 2);
        pnd_Work_Area_Pnd_Fil1 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Fil1", "#FIL1", FieldType.STRING, 122);
        pnd_Work_Area_Pnd_Fil2 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Fil2", "#FIL2", FieldType.STRING, 250);
        pnd_Work_Area_Pnd_Fil3 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Fil3", "#FIL3", FieldType.STRING, 250);
        pnd_Work_Area_Pnd_Fil4 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Fil4", "#FIL4", FieldType.STRING, 225);
        pnd_Work_Area_Pnd_Daten = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Daten", "#DATEN", FieldType.NUMERIC, 8);

        pnd_Work_Area__R_Field_3 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area__R_Field_3", "REDEFINE", pnd_Work_Area_Pnd_Daten);
        pnd_Work_Area_Pnd_Daten_Cc = pnd_Work_Area__R_Field_3.newFieldInGroup("pnd_Work_Area_Pnd_Daten_Cc", "#DATEN-CC", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Daten_Yy = pnd_Work_Area__R_Field_3.newFieldInGroup("pnd_Work_Area_Pnd_Daten_Yy", "#DATEN-YY", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Daten_Mm = pnd_Work_Area__R_Field_3.newFieldInGroup("pnd_Work_Area_Pnd_Daten_Mm", "#DATEN-MM", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Daten_Dd = pnd_Work_Area__R_Field_3.newFieldInGroup("pnd_Work_Area_Pnd_Daten_Dd", "#DATEN-DD", FieldType.STRING, 2);
        pnd_Mid_Name = localVariables.newFieldInRecord("pnd_Mid_Name", "#MID-NAME", FieldType.STRING, 15);

        pnd_Mid_Name__R_Field_4 = localVariables.newGroupInRecord("pnd_Mid_Name__R_Field_4", "REDEFINE", pnd_Mid_Name);
        pnd_Mid_Name_Pnd_Mid_Name1 = pnd_Mid_Name__R_Field_4.newFieldInGroup("pnd_Mid_Name_Pnd_Mid_Name1", "#MID-NAME1", FieldType.STRING, 1);
        pnd_Report_Per = localVariables.newFieldInRecord("pnd_Report_Per", "#REPORT-PER", FieldType.STRING, 1);
        pnd_Report_Mm = localVariables.newFieldInRecord("pnd_Report_Mm", "#REPORT-MM", FieldType.STRING, 2);
        pnd_Tax_Yyyy = localVariables.newFieldInRecord("pnd_Tax_Yyyy", "#TAX-YYYY", FieldType.STRING, 4);

        pnd_Tax_Yyyy__R_Field_5 = localVariables.newGroupInRecord("pnd_Tax_Yyyy__R_Field_5", "REDEFINE", pnd_Tax_Yyyy);
        pnd_Tax_Yyyy_Pnd_Tax_Cc = pnd_Tax_Yyyy__R_Field_5.newFieldInGroup("pnd_Tax_Yyyy_Pnd_Tax_Cc", "#TAX-CC", FieldType.STRING, 2);
        pnd_Tax_Yyyy_Pnd_Tax_Yy = pnd_Tax_Yyyy__R_Field_5.newFieldInGroup("pnd_Tax_Yyyy_Pnd_Tax_Yy", "#TAX-YY", FieldType.STRING, 2);
        pnd_Ws_Name = localVariables.newFieldInRecord("pnd_Ws_Name", "#WS-NAME", FieldType.STRING, 53);
        pnd_Rec_Seq_Number = localVariables.newFieldInRecord("pnd_Rec_Seq_Number", "#REC-SEQ-NUMBER", FieldType.NUMERIC, 8);
        pnd_Prev_A_Rec_Company = localVariables.newFieldInRecord("pnd_Prev_A_Rec_Company", "#PREV-A-REC-COMPANY", FieldType.STRING, 1);
        pnd_Prev_C_Rec_Company = localVariables.newFieldInRecord("pnd_Prev_C_Rec_Company", "#PREV-C-REC-COMPANY", FieldType.STRING, 1);
        pnd_Temp_State_Id = localVariables.newFieldInRecord("pnd_Temp_State_Id", "#TEMP-STATE-ID", FieldType.STRING, 14);

        pnd_Temp_State_Id__R_Field_6 = localVariables.newGroupInRecord("pnd_Temp_State_Id__R_Field_6", "REDEFINE", pnd_Temp_State_Id);
        pnd_Temp_State_Id_Pnd_Temp_State_Id_Fill = pnd_Temp_State_Id__R_Field_6.newFieldInGroup("pnd_Temp_State_Id_Pnd_Temp_State_Id_Fill", "#TEMP-STATE-ID-FILL", 
            FieldType.STRING, 7);
        pnd_Temp_State_Id_Pnd_Temp_State_Id_7 = pnd_Temp_State_Id__R_Field_6.newFieldInGroup("pnd_Temp_State_Id_Pnd_Temp_State_Id_7", "#TEMP-STATE-ID-7", 
            FieldType.STRING, 7);
        pnd_Temp_State_Id_20 = localVariables.newFieldInRecord("pnd_Temp_State_Id_20", "#TEMP-STATE-ID-20", FieldType.STRING, 20);

        pnd_Work_Out_Ny_Record = localVariables.newGroupInRecord("pnd_Work_Out_Ny_Record", "#WORK-OUT-NY-RECORD");
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec1 = pnd_Work_Out_Ny_Record.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec1", "#W-OUT-IRS-REC1", 
            FieldType.STRING, 100);

        pnd_Work_Out_Ny_Record__R_Field_7 = pnd_Work_Out_Ny_Record.newGroupInGroup("pnd_Work_Out_Ny_Record__R_Field_7", "REDEFINE", pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec1);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Id = pnd_Work_Out_Ny_Record__R_Field_7.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Id", "#W-OUTW-NY-ID", 
            FieldType.STRING, 2);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Tin = pnd_Work_Out_Ny_Record__R_Field_7.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Tin", "#W-OUTW-NY-TIN", 
            FieldType.STRING, 9);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Name = pnd_Work_Out_Ny_Record__R_Field_7.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Name", "#W-OUTW-NY-NAME", 
            FieldType.STRING, 30);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank1 = pnd_Work_Out_Ny_Record__R_Field_7.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank1", 
            "#W-OUTW-NY-BLANK1", FieldType.STRING, 1);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Wages_Ind = pnd_Work_Out_Ny_Record__R_Field_7.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Wages_Ind", 
            "#W-OUTW-NY-WAGES-IND", FieldType.STRING, 1);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank2 = pnd_Work_Out_Ny_Record__R_Field_7.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank2", 
            "#W-OUTW-NY-BLANK2", FieldType.STRING, 1);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Gross = pnd_Work_Out_Ny_Record__R_Field_7.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Gross", "#W-OUTW-NY-GROSS", 
            FieldType.NUMERIC, 14, 2);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank3 = pnd_Work_Out_Ny_Record__R_Field_7.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank3", 
            "#W-OUTW-NY-BLANK3", FieldType.STRING, 1);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Taxable_Amt = pnd_Work_Out_Ny_Record__R_Field_7.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Taxable_Amt", 
            "#W-OUTW-NY-TAXABLE-AMT", FieldType.NUMERIC, 14, 2);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank4 = pnd_Work_Out_Ny_Record__R_Field_7.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank4", 
            "#W-OUTW-NY-BLANK4", FieldType.STRING, 1);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Tax_Wthld = pnd_Work_Out_Ny_Record__R_Field_7.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Tax_Wthld", 
            "#W-OUTW-NY-TAX-WTHLD", FieldType.NUMERIC, 14, 2);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank5 = pnd_Work_Out_Ny_Record__R_Field_7.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank5", 
            "#W-OUTW-NY-BLANK5", FieldType.STRING, 12);
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec2 = pnd_Work_Out_Ny_Record.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec2", "#W-OUT-IRS-REC2", 
            FieldType.STRING, 100);
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec3 = pnd_Work_Out_Ny_Record.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec3", "#W-OUT-IRS-REC3", 
            FieldType.STRING, 100);
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec4 = pnd_Work_Out_Ny_Record.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec4", "#W-OUT-IRS-REC4", 
            FieldType.STRING, 100);
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec5 = pnd_Work_Out_Ny_Record.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec5", "#W-OUT-IRS-REC5", 
            FieldType.STRING, 100);
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec6 = pnd_Work_Out_Ny_Record.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec6", "#W-OUT-IRS-REC6", 
            FieldType.STRING, 100);
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec7 = pnd_Work_Out_Ny_Record.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec7", "#W-OUT-IRS-REC7", 
            FieldType.STRING, 100);
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec8 = pnd_Work_Out_Ny_Record.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec8", "#W-OUT-IRS-REC8", 
            FieldType.STRING, 50);

        pnd_W_Work_File = localVariables.newGroupInRecord("pnd_W_Work_File", "#W-WORK-FILE");
        pnd_W_Work_File_Pnd_W_Tirf_Company_Cde = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Company_Cde", "#W-TIRF-COMPANY-CDE", FieldType.STRING, 
            1);
        pnd_W_Work_File_Pnd_W_Tirf_Sys_Err = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Sys_Err", "#W-TIRF-SYS-ERR", FieldType.STRING, 
            1);
        pnd_W_Work_File_Pnd_W_Tirf_Empty_Form = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Empty_Form", "#W-TIRF-EMPTY-FORM", FieldType.STRING, 
            1);
        pnd_W_Work_File_Pnd_W_Tirf_State_Distr = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_State_Distr", "#W-TIRF-STATE-DISTR", FieldType.NUMERIC, 
            11, 2);
        pnd_W_Work_File_Pnd_W_Tirf_State_Tax_Wthld = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_State_Tax_Wthld", "#W-TIRF-STATE-TAX-WTHLD", 
            FieldType.NUMERIC, 9, 2);
        pnd_W_Work_File_Pnd_W_Tirf_Loc_Code = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Loc_Code", "#W-TIRF-LOC-CODE", FieldType.STRING, 
            5);
        pnd_W_Work_File_Pnd_W_Tirf_Loc_Distr = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Loc_Distr", "#W-TIRF-LOC-DISTR", FieldType.NUMERIC, 
            11, 2);
        pnd_W_Work_File_Pnd_W_Tirf_Loc_Tax_Wthld = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Loc_Tax_Wthld", "#W-TIRF-LOC-TAX-WTHLD", 
            FieldType.NUMERIC, 9, 2);
        pnd_W_Work_File_Pnd_W_Tirf_Isn = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Isn", "#W-TIRF-ISN", FieldType.NUMERIC, 8);

        pnd_W_Work_File__R_Field_8 = pnd_W_Work_File.newGroupInGroup("pnd_W_Work_File__R_Field_8", "REDEFINE", pnd_W_Work_File_Pnd_W_Tirf_Isn);
        pnd_W_Work_File_Pnd_W_Tirf_Isn_A = pnd_W_Work_File__R_Field_8.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Isn_A", "#W-TIRF-ISN-A", FieldType.STRING, 
            8);
        pnd_W_Work_File_Pnd_W_Tirf_State_Rpt_Ind = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_State_Rpt_Ind", "#W-TIRF-STATE-RPT-IND", 
            FieldType.STRING, 1);
        pnd_W_Work_File_Pnd_W_Tirf_State_Code = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_State_Code", "#W-TIRF-STATE-CODE", FieldType.STRING, 
            2);
        pnd_W_Work_File_Pnd_W_Tirf_Tax_Year = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Tax_Year", "#W-TIRF-TAX-YEAR", FieldType.STRING, 
            4);

        pnd_W_Work_File__R_Field_9 = pnd_W_Work_File.newGroupInGroup("pnd_W_Work_File__R_Field_9", "REDEFINE", pnd_W_Work_File_Pnd_W_Tirf_Tax_Year);
        pnd_W_Work_File_Pnd_W_Tirf_Tax_Year_Cc = pnd_W_Work_File__R_Field_9.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Tax_Year_Cc", "#W-TIRF-TAX-YEAR-CC", 
            FieldType.STRING, 2);
        pnd_W_Work_File_Pnd_W_Tirf_Tax_Year_Yy = pnd_W_Work_File__R_Field_9.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Tax_Year_Yy", "#W-TIRF-TAX-YEAR-YY", 
            FieldType.STRING, 2);
        pnd_W_Work_File_Pnd_W_Tirf_Name = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Name", "#W-TIRF-NAME", FieldType.STRING, 40);
        pnd_W_Work_File_Pnd_W_Tirf_Tin = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Tin", "#W-TIRF-TIN", FieldType.STRING, 9);
        pnd_W_Work_File_Pnd_W_Tirf_Account = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Account", "#W-TIRF-ACCOUNT", FieldType.STRING, 
            10);
        pnd_W_Work_File_Pnd_W_Tirf_Addr_Ln1 = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Addr_Ln1", "#W-TIRF-ADDR-LN1", FieldType.STRING, 
            40);
        pnd_W_Work_File_Pnd_W_Tirf_Addr_Ln2 = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Addr_Ln2", "#W-TIRF-ADDR-LN2", FieldType.STRING, 
            40);
        pnd_W_Work_File_Pnd_W_Tirf_Addr_Ln3 = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Addr_Ln3", "#W-TIRF-ADDR-LN3", FieldType.STRING, 
            40);
        pnd_Out_Ny_Record = localVariables.newFieldInRecord("pnd_Out_Ny_Record", "#OUT-NY-RECORD", FieldType.STRING, 100);

        pnd_Out_Ny_Record__R_Field_10 = localVariables.newGroupInRecord("pnd_Out_Ny_Record__R_Field_10", "REDEFINE", pnd_Out_Ny_Record);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Id = pnd_Out_Ny_Record__R_Field_10.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Id", "#OUTW-NY-ID", FieldType.STRING, 
            2);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Tin = pnd_Out_Ny_Record__R_Field_10.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Tin", "#OUTW-NY-TIN", FieldType.STRING, 
            9);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Name = pnd_Out_Ny_Record__R_Field_10.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Name", "#OUTW-NY-NAME", FieldType.STRING, 
            30);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank1 = pnd_Out_Ny_Record__R_Field_10.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank1", "#OUTW-NY-BLANK1", 
            FieldType.STRING, 1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Wages_Ind = pnd_Out_Ny_Record__R_Field_10.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Wages_Ind", "#OUTW-NY-WAGES-IND", 
            FieldType.STRING, 1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank2 = pnd_Out_Ny_Record__R_Field_10.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank2", "#OUTW-NY-BLANK2", 
            FieldType.STRING, 1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Gross = pnd_Out_Ny_Record__R_Field_10.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Gross", "#OUTW-NY-GROSS", FieldType.NUMERIC, 
            14, 2);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank3 = pnd_Out_Ny_Record__R_Field_10.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank3", "#OUTW-NY-BLANK3", 
            FieldType.STRING, 1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Taxable_Amt = pnd_Out_Ny_Record__R_Field_10.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Taxable_Amt", "#OUTW-NY-TAXABLE-AMT", 
            FieldType.NUMERIC, 14, 2);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank4 = pnd_Out_Ny_Record__R_Field_10.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank4", "#OUTW-NY-BLANK4", 
            FieldType.STRING, 1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Tax_Wthld = pnd_Out_Ny_Record__R_Field_10.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Tax_Wthld", "#OUTW-NY-TAX-WTHLD", 
            FieldType.NUMERIC, 14, 2);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank5 = pnd_Out_Ny_Record__R_Field_10.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank5", "#OUTW-NY-BLANK5", 
            FieldType.STRING, 12);
        pnd_W_Accum_Taxable_Amt = localVariables.newFieldInRecord("pnd_W_Accum_Taxable_Amt", "#W-ACCUM-TAXABLE-AMT", FieldType.NUMERIC, 14, 2);
        pnd_W_Accum_Tax_Wthld = localVariables.newFieldInRecord("pnd_W_Accum_Tax_Wthld", "#W-ACCUM-TAX-WTHLD", FieldType.NUMERIC, 14, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl3502.initializeValues();
        ldaTwrl3513.initializeValues();

        localVariables.reset();
        pnd_Debug.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3007() throws Exception
    {
        super("Twrp3007");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) LS = 132
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        pnd_Work_Area_Pnd_Today.setValue(Global.getDATX());                                                                                                               //Natural: ASSIGN #TODAY := *DATX
        pnd_Work_Area_Pnd_Daten.setValue(Global.getDATN());                                                                                                               //Natural: ASSIGN #DATEN := *DATN
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 RE-IN
        while (condition(getWorkFiles().read(1, re_In)))
        {
            CheckAtStartofData932();

            //*                                                                                                                                                           //Natural: AT START OF DATA
            pnd_Rec_Seq_Number.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #REC-SEQ-NUMBER
            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Seq_Num().setValue(pnd_Rec_Seq_Number);                                                                            //Natural: ASSIGN #OUTB-SEQ-NUM := #REC-SEQ-NUMBER
            //*   COMPRESS  #OLD-COMPANY-CDE '3' #TIRF-ISN-A INTO #TRAILER
            //*  $$$$  - ROX
            if (condition(re_In_E_Transmitter.equals("D")))                                                                                                               //Natural: IF E-TRANSMITTER = 'D'
            {
                                                                                                                                                                          //Natural: PERFORM REFORMAT-REC
                sub_Reformat_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-RECORD
                sub_Write_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Work_Area_Pnd_Tot_State_Distr.nadd(re_In_Rs_Qtr_Gross_Amt);                                                                                           //Natural: ADD RS-QTR-GROSS-AMT TO #TOT-STATE-DISTR
                pnd_Work_Area_Pnd_Tot_Yea_Gross.nadd(re_In_Rs_Tot_Wages);                                                                                                 //Natural: ADD RS-TOT-WAGES TO #TOT-YEA-GROSS
                pnd_Work_Area_Pnd_Tot_State_Tax_Wthld.compute(new ComputeParameters(false, pnd_Work_Area_Pnd_Tot_State_Tax_Wthld), pnd_Work_Area_Pnd_Tot_State_Tax_Wthld.add(re_In_Rs_Tax_Withhold).add(re_In_Rs_Loc_Withhold)); //Natural: ADD RS-TAX-WITHHOLD RS-LOC-WITHHOLD TO #TOT-STATE-TAX-WTHLD
            }                                                                                                                                                             //Natural: END-IF
            //*  $$$$
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  $$$$  ROX
        if (condition(pnd_Rec_Seq_Number.notEquals(getZero())))                                                                                                           //Natural: IF #REC-SEQ-NUMBER NE 0
        {
                                                                                                                                                                          //Natural: PERFORM CREATE-C-RECORD
            sub_Create_C_Record();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM TRAILER-REC
            sub_Trailer_Rec();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-A-RECORD
        //* ********************************
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NY-A-RECORD
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-C-RECORD
        //* *****************************************
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: HEADER-RECORD
        //* ******************************
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NY-HEADER-REC
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRAILER-REC
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NY-TRAILER-REC
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-COMPANY-INFO
        //* *********************************
        //*  --------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RECORD
        //*  --------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REFORMAT-REC
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
    }
    private void sub_Create_A_Record() throws Exception                                                                                                                   //Natural: CREATE-A-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Work_Area_Pnd_Old_Company_Cde.setValue("A");                                                                                                                  //Natural: ASSIGN #OLD-COMPANY-CDE := 'A'
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-INFO
        sub_Get_Company_Info();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM NY-A-RECORD
        sub_Ny_A_Record();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Ny_A_Record() throws Exception                                                                                                                       //Natural: NY-A-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        short decideConditionsMet1016 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #REPORT-PER = '1'
        if (condition(pnd_Report_Per.equals("1")))
        {
            decideConditionsMet1016++;
            pnd_Report_Mm.setValue("03");                                                                                                                                 //Natural: MOVE '03' TO #REPORT-MM
        }                                                                                                                                                                 //Natural: WHEN #REPORT-PER = '2'
        else if (condition(pnd_Report_Per.equals("2")))
        {
            decideConditionsMet1016++;
            pnd_Report_Mm.setValue("06");                                                                                                                                 //Natural: MOVE '06' TO #REPORT-MM
        }                                                                                                                                                                 //Natural: WHEN #REPORT-PER = '3'
        else if (condition(pnd_Report_Per.equals("3")))
        {
            decideConditionsMet1016++;
            pnd_Report_Mm.setValue("09");                                                                                                                                 //Natural: MOVE '09' TO #REPORT-MM
        }                                                                                                                                                                 //Natural: WHEN #REPORT-PER = '4'
        else if (condition(pnd_Report_Per.equals("4")))
        {
            decideConditionsMet1016++;
            pnd_Report_Mm.setValue("12");                                                                                                                                 //Natural: MOVE '12' TO #REPORT-MM
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Report_Mm.setValue("12");                                                                                                                                 //Natural: MOVE '12' TO #REPORT-MM
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Rec_Seq_Number.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #REC-SEQ-NUMBER
        ldaTwrl3513.getPnd_Out1_Ny_Record().reset();                                                                                                                      //Natural: RESET #OUT1-NY-RECORD
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Report_Quarter().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Report_Mm, pnd_Tax_Yyyy_Pnd_Tax_Yy)); //Natural: COMPRESS #REPORT-MM #TAX-YY INTO #OUT1E-NY-REPORT-QUARTER LEAVING NO SPACE
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Id().setValue("1E");                                                                                               //Natural: ASSIGN #OUT1E-NY-ID := '1E'
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Tin().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id());                                                          //Natural: ASSIGN #OUT1E-NY-TIN := #TWRACOM2.#FED-ID
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank1().setValue(" ");                                                                                            //Natural: ASSIGN #OUT1E-NY-BLANK1 := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Trans_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Payer_A());                                             //Natural: ASSIGN #OUT1E-NY-TRANS-NAME := #TWRACOM2.#COMP-PAYER-A
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank2().setValue(" ");                                                                                            //Natural: ASSIGN #OUT1E-NY-BLANK2 := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Address().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Address().getValue(1));                                         //Natural: ASSIGN #OUT1E-NY-ADDRESS := #ADDRESS ( 1 )
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_City().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_City());                                                           //Natural: ASSIGN #OUT1E-NY-CITY := #CITY
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_State().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_State());                                                         //Natural: ASSIGN #OUT1E-NY-STATE := #STATE
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Zip().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Zip_9());                                                           //Natural: ASSIGN #OUT1E-NY-ZIP := #ZIP-9
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank().setValue(" ");                                                                                             //Natural: ASSIGN #OUT1E-NY-BLANK := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Ret_Type().setValue("O");                                                                                          //Natural: ASSIGN #OUT1E-NY-RET-TYPE := 'O'
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Emp_Ind().setValue(" ");                                                                                           //Natural: ASSIGN #OUT1E-NY-EMP-IND := ' '
        pnd_Work_Area_Pnd_Trailer.setValue(DbsUtil.compress(pnd_Work_Area_Pnd_Old_Company_Cde, "2"));                                                                     //Natural: COMPRESS #OLD-COMPANY-CDE '2' INTO #TRAILER
        getWorkFiles().write(2, false, ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1_Ny_Rec1());                                                                             //Natural: WRITE WORK FILE 02 #OUT1-NY-REC1
    }
    private void sub_Create_C_Record() throws Exception                                                                                                                   //Natural: CREATE-C-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pnd_Rec_Seq_Number.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #REC-SEQ-NUMBER
        pnd_Work_Area_Pnd_Grand_1e.nadd(1);                                                                                                                               //Natural: ADD 1 TO #GRAND-1E
        ldaTwrl3513.getPnd_Out1_Ny_Record().reset();                                                                                                                      //Natural: RESET #OUT1-NY-RECORD
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Id().setValue("1T");                                                                                               //Natural: ASSIGN #OUT1T-NY-ID := '1T'
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_1w().setValue(pnd_Work_Area_Pnd_Tot_1w);                                                                     //Natural: ASSIGN #OUT1T-NY-TOTAL-1W := #TOT-1W
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank().setValue(" ");                                                                                             //Natural: ASSIGN #OUT1T-NY-BLANK := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_Gross().setValue(pnd_Work_Area_Pnd_Tot_State_Distr);                                                         //Natural: ASSIGN #OUT1T-NY-TOTAL-GROSS := #TOT-STATE-DISTR
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank1().setValue(" ");                                                                                            //Natural: ASSIGN #OUT1T-NY-BLANK1 := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_Taxable_Amt().setValue(pnd_Work_Area_Pnd_Tot_Yea_Gross);                                                     //Natural: ASSIGN #OUT1T-NY-TOTAL-TAXABLE-AMT := #TOT-YEA-GROSS
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank2().setValue(" ");                                                                                            //Natural: ASSIGN #OUT1T-NY-BLANK2 := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Tax_Wthld().setValue(pnd_Work_Area_Pnd_Tot_State_Tax_Wthld);                                                       //Natural: ASSIGN #OUT1T-NY-TAX-WTHLD := #TOT-STATE-TAX-WTHLD
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank3().setValue(" ");                                                                                            //Natural: ASSIGN #OUT1T-NY-BLANK3 := ' '
        pnd_Work_Area_Pnd_Trailer.setValue(DbsUtil.compress(pnd_Work_Area_Pnd_Old_Company_Cde, "4"));                                                                     //Natural: COMPRESS #OLD-COMPANY-CDE '4' INTO #TRAILER
        getWorkFiles().write(2, false, ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1_Ny_Rec1());                                                                             //Natural: WRITE WORK FILE 02 #OUT1-NY-REC1
        pnd_Work_Area_Pnd_Tot_1w.reset();                                                                                                                                 //Natural: RESET #TOT-1W #TOT-STATE-DISTR #TOT-STATE-TAX-WTHLD #TOT-YEA-GROSS
        pnd_Work_Area_Pnd_Tot_State_Distr.reset();
        pnd_Work_Area_Pnd_Tot_State_Tax_Wthld.reset();
        pnd_Work_Area_Pnd_Tot_Yea_Gross.reset();
    }
    private void sub_Header_Record() throws Exception                                                                                                                     //Natural: HEADER-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Work_Area_Pnd_Old_Company_Cde.setValue("A");                                                                                                                  //Natural: ASSIGN #OLD-COMPANY-CDE := 'A'
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-INFO
        sub_Get_Company_Info();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM NY-HEADER-REC
        sub_Ny_Header_Rec();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Ny_Header_Rec() throws Exception                                                                                                                     //Natural: NY-HEADER-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        pnd_Rec_Seq_Number.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #REC-SEQ-NUMBER
        //*   #FED-ID
        //*   #COMP-NAME
        ldaTwrl3513.getPnd_Out1_Ny_Record().reset();                                                                                                                      //Natural: RESET #OUT1-NY-RECORD
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Id().setValue("1A");                                                                                               //Natural: ASSIGN #OUT1A-NY-ID := '1A'
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Mm().setValue(pnd_Work_Area_Pnd_Daten_Mm);                                                             //Natural: ASSIGN #OUT1A-NY-CREATE-DATE-MM := #DATEN-MM
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Dd().setValue(pnd_Work_Area_Pnd_Daten_Dd);                                                             //Natural: ASSIGN #OUT1A-NY-CREATE-DATE-DD := #DATEN-DD
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Yy().setValue(pnd_Work_Area_Pnd_Daten_Yy);                                                             //Natural: ASSIGN #OUT1A-NY-CREATE-DATE-YY := #DATEN-YY
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Id_Num().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id());                                                       //Natural: ASSIGN #OUT1A-NY-ID-NUM := #TWRACOM2.#FED-ID
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Trans_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Trans_A());                                             //Natural: ASSIGN #OUT1A-NY-TRANS-NAME := #TWRACOM2.#COMP-TRANS-A
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Address().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Address().getValue(1));                                         //Natural: ASSIGN #OUT1A-NY-ADDRESS := #ADDRESS ( 1 )
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_City().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_City());                                                           //Natural: ASSIGN #OUT1A-NY-CITY := #CITY
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_State().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_State());                                                         //Natural: ASSIGN #OUT1A-NY-STATE := #STATE
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Zip().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Zip_9());                                                           //Natural: ASSIGN #OUT1A-NY-ZIP := #ZIP-9
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Blank().setValue(" ");                                                                                             //Natural: ASSIGN #OUT1A-NY-BLANK := ' '
        pnd_Work_Area_Pnd_Trailer.setValue(DbsUtil.compress("A", "1"));                                                                                                   //Natural: COMPRESS 'A' '1' INTO #TRAILER
        getWorkFiles().write(2, false, ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1_Ny_Rec1());                                                                             //Natural: WRITE WORK FILE 02 #OUT1-NY-REC1
    }
    private void sub_Trailer_Rec() throws Exception                                                                                                                       //Natural: TRAILER-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        //*  IF #TIRF-STATE-CODE  =  'NY'
                                                                                                                                                                          //Natural: PERFORM NY-TRAILER-REC
        sub_Ny_Trailer_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  END-IF
    }
    private void sub_Ny_Trailer_Rec() throws Exception                                                                                                                    //Natural: NY-TRAILER-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        pnd_Rec_Seq_Number.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #REC-SEQ-NUMBER
        ldaTwrl3513.getPnd_Out1_Ny_Record().reset();                                                                                                                      //Natural: RESET #OUT1-NY-RECORD
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1f_Ny_Id().setValue("1F");                                                                                               //Natural: ASSIGN #OUT1F-NY-ID := '1F'
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1f_Ny_Total_1e().setValue(pnd_Work_Area_Pnd_Grand_1e);                                                                   //Natural: ASSIGN #OUT1F-NY-TOTAL-1E := #GRAND-1E
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1f_Ny_Total_1w().setValue(pnd_Work_Area_Pnd_Grand_1w);                                                                   //Natural: ASSIGN #OUT1F-NY-TOTAL-1W := #GRAND-1W
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1f_Ny_Blank().setValue(" ");                                                                                             //Natural: ASSIGN #OUT1F-NY-BLANK := ' '
        pnd_Work_Area_Pnd_Trailer.setValue(DbsUtil.compress("Z", "9"));                                                                                                   //Natural: COMPRESS 'Z' '9' INTO #TRAILER
        getWorkFiles().write(2, false, ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1_Ny_Rec1());                                                                             //Natural: WRITE WORK FILE 02 #OUT1-NY-REC1
    }
    private void sub_Get_Company_Info() throws Exception                                                                                                                  //Natural: GET-COMPANY-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue(pnd_Work_Area_Pnd_Old_Company_Cde);                                                                          //Natural: ASSIGN #TWRACOM2.#COMP-CODE := #OLD-COMPANY-CDE
        pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(1);                                                                                                          //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 01
        pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().setValueEdited(new ReportEditMask("9999"),re_In_E_Tax_Year);                                                           //Natural: MOVE EDITED E-TAX-YEAR TO #TWRACOM2.#TAX-YEAR ( EM = 9999 )
        pdaTwracom2.getPnd_Twracom2_Pnd_Abend_Ind().setValue(false);                                                                                                      //Natural: ASSIGN #TWRACOM2.#ABEND-IND := #TWRACOM2.#DISPLAY-IND := FALSE
        pdaTwracom2.getPnd_Twracom2_Pnd_Display_Ind().setValue(false);
        DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOM2' USING #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        if (condition(pdaTwracom2.getPnd_Twracom2_Pnd_Ret_Code().equals(false)))                                                                                          //Natural: IF #RET-CODE = FALSE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"State Original Return Code Company Inf.'TWRNCOM2'",new TabSetting(77),"***",NEWLINE,"***",new                  //Natural: WRITE ( 00 ) '***' 06T 'State Original Return Code Company Inf."TWRNCOM2"' 77T '***' / '***' 06T 'Return Message...:' #TWRACOM2.#RET-MSG 77T '***' / '***' 06T 'Program..........:' *PROGRAM 77T '***'
                TabSetting(6),"Return Message...:",pdaTwracom2.getPnd_Twracom2_Pnd_Ret_Msg(),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Program..........:",Global.getPROGRAM(),new 
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(94);  if (true) return;                                                                                                                     //Natural: TERMINATE 94
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Record() throws Exception                                                                                                                      //Natural: WRITE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------------------------------
        pnd_Work_Area_Pnd_Tot_1w.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #TOT-1W
        pnd_Work_Area_Pnd_Grand_1w.nadd(1);                                                                                                                               //Natural: ADD 1 TO #GRAND-1W
        getWorkFiles().write(2, false, ldaTwrl3513.getPnd_Out1_Ny_Record());                                                                                              //Natural: WRITE WORK FILE 02 #OUT1-NY-RECORD
        pnd_Work_Area_Pnd_Ws_Tot_Tax.reset();                                                                                                                             //Natural: RESET #WS-TOT-TAX
    }
    //*   $$$$  ROX
    private void sub_Reformat_Rec() throws Exception                                                                                                                      //Natural: REFORMAT-REC
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Mid_Name.setValue(re_In_Rs_Middle_Name);                                                                                                                      //Natural: ASSIGN #MID-NAME := RS-MIDDLE-NAME
        pnd_Ws_Name.setValue(DbsUtil.compress(re_In_Rs_Last_Name, re_In_Rs_First_Name, pnd_Mid_Name_Pnd_Mid_Name1));                                                      //Natural: COMPRESS RS-LAST-NAME RS-FIRST-NAME #MID-NAME1 INTO #WS-NAME
        pnd_Work_Area_Pnd_Ws_Tot_Tax.compute(new ComputeParameters(false, pnd_Work_Area_Pnd_Ws_Tot_Tax), pnd_Work_Area_Pnd_Ws_Tot_Tax.add(re_In_Rs_Tax_Withhold).add(re_In_Rs_Loc_Withhold)); //Natural: ADD RS-TAX-WITHHOLD RS-LOC-WITHHOLD TO #WS-TOT-TAX
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Id().setValue("1W");                                                                                               //Natural: ASSIGN #OUT1W-NY-ID := '1W'
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Tin().setValue(re_In_Rs_Soc_Sec_Nbr);                                                                              //Natural: ASSIGN #OUT1W-NY-TIN := RS-SOC-SEC-NBR
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Name().setValue(pnd_Ws_Name);                                                                                      //Natural: ASSIGN #OUT1W-NY-NAME := #WS-NAME
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank1().setValue(" ");                                                                                            //Natural: ASSIGN #OUT1W-NY-BLANK1 := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Wages_Ind().setValue("O");                                                                                         //Natural: ASSIGN #OUT1W-NY-WAGES-IND := 'O'
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank2().setValue(" ");                                                                                            //Natural: ASSIGN #OUT1W-NY-BLANK2 := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Gross().setValue(re_In_Rs_Qtr_Gross_Amt);                                                                          //Natural: ASSIGN #OUT1W-NY-GROSS := RS-QTR-GROSS-AMT
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank3().setValue(" ");                                                                                            //Natural: ASSIGN #OUT1W-NY-BLANK3 := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Taxable_Amt().setValue(re_In_Rs_Tot_Wages);                                                                        //Natural: ASSIGN #OUT1W-NY-TAXABLE-AMT := RS-TOT-WAGES
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank4().setValue(" ");                                                                                            //Natural: ASSIGN #OUT1W-NY-BLANK4 := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Tax_Wthld().setValue(pnd_Work_Area_Pnd_Ws_Tot_Tax);                                                                //Natural: ASSIGN #OUT1W-NY-TAX-WTHLD := #WS-TOT-TAX
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank5().setValue(" ");                                                                                            //Natural: ASSIGN #OUT1W-NY-BLANK5 := ' '
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132");
    }
    private void CheckAtStartofData932() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Report_Per.setValue(re_In_E_Ret_Qtr);                                                                                                                     //Natural: MOVE E-RET-QTR TO #REPORT-PER
            pnd_Tax_Yyyy.setValue(re_In_E_Tax_Year);                                                                                                                      //Natural: MOVE E-TAX-YEAR TO #TAX-YYYY
                                                                                                                                                                          //Natural: PERFORM HEADER-RECORD
            sub_Header_Record();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-A-RECORD
            sub_Create_A_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
