/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:37:58 PM
**        * FROM NATURAL PROGRAM : Twrp3556
************************************************************
**        * FILE NAME            : Twrp3556.java
**        * CLASS NAME           : Twrp3556
**        * INSTANCE NAME        : Twrp3556
************************************************************
************************************************************************
** PROGRAM : TWRP3556
** SYSTEM  : TAXWARS
** AUTHOR  : COGNIZANT
** FUNCTION: NEW YORK STATE CORRECTION REPORTING E-FILING
**           CHANGED LAYOUT TO INCORPORATE ORIGINAL REPORTING
**
** HISTORY.....:
** 08/04/14   CTS - THIS PROGRAM USES THE ORIGINAL AND CORRECTION FILE
**                  AS INPUT TO GENERATE TIAA AND TRUST FILES WITH 1A,
**                  1E, 1W, 1T AND 1F LAYOUTS.
** 10/05/2018 ARIVU - EIN CHANGES. TAG: EINCHG
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3556 extends BLNatBase
{
    // Data Areas
    private LdaTwrl3515 ldaTwrl3515;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Org_Ny_Record;
    private DbsField pnd_Org_Ny_Record_Pnd_Orga_Ny_Id;
    private DbsField pnd_Org_Ny_Record_Pnd_Orga_Ny_Create_Date;

    private DbsGroup pnd_Org_Ny_Record__R_Field_1;
    private DbsField pnd_Org_Ny_Record_Pnd_Orga_Ny_Create_Date_Mm;
    private DbsField pnd_Org_Ny_Record_Pnd_Orga_Ny_Create_Date_Dd;
    private DbsField pnd_Org_Ny_Record_Pnd_Orga_Ny_Create_Date_Yy;
    private DbsField pnd_Org_Ny_Record_Pnd_Orga_Ny_Id_Num;
    private DbsField pnd_Org_Ny_Record_Pnd_Orga_Ny_Trans_Name;
    private DbsField pnd_Org_Ny_Record_Pnd_Orga_Ny_Address;
    private DbsField pnd_Org_Ny_Record_Pnd_Orga_Ny_City;
    private DbsField pnd_Org_Ny_Record_Pnd_Orga_Ny_State;
    private DbsField pnd_Org_Ny_Record_Pnd_Orga_Ny_Zip;
    private DbsField pnd_Org_Ny_Record_Pnd_Orga_Ny_Blank;

    private DbsGroup pnd_Org_Ny_Record__R_Field_2;
    private DbsField pnd_Org_Ny_Record_Pnd_Orge_Ny_Id;
    private DbsField pnd_Org_Ny_Record_Pnd_Orge_Ny_Report_Quarter;
    private DbsField pnd_Org_Ny_Record_Pnd_Orge_Ny_Tin;
    private DbsField pnd_Org_Ny_Record_Pnd_Orge_Ny_Blank1;
    private DbsField pnd_Org_Ny_Record_Pnd_Orge_Ny_Trans_Name;
    private DbsField pnd_Org_Ny_Record_Pnd_Orge_Ny_Blank2;
    private DbsField pnd_Org_Ny_Record_Pnd_Orge_Ny_Address;
    private DbsField pnd_Org_Ny_Record_Pnd_Orge_Ny_City;
    private DbsField pnd_Org_Ny_Record_Pnd_Orge_Ny_State;
    private DbsField pnd_Org_Ny_Record_Pnd_Orge_Ny_Zip;
    private DbsField pnd_Org_Ny_Record_Pnd_Orge_Ny_Blank;
    private DbsField pnd_Org_Ny_Record_Pnd_Orge_Ny_Ret_Type;
    private DbsField pnd_Org_Ny_Record_Pnd_Orge_Ny_Emp_Ind;

    private DbsGroup pnd_Org_Ny_Record__R_Field_3;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgw_Ny_Id;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgw_Ny_Tin;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgw_Ny_Name;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgw_Ny_Blank1;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgw_Ny_Wages_Ind;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgw_Ny_Blank2;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgw_Ny_Gross;

    private DbsGroup pnd_Org_Ny_Record__R_Field_4;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgw_Ny_Gross_A;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgw_Ny_Blank3;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgw_Ny_Taxable_Amt;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgw_Ny_Blank4;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgw_Ny_Tax_Wthld;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgw_Ny_Blank5;

    private DbsGroup pnd_Org_Ny_Record__R_Field_5;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgt_Ny_Id;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgt_Ny_Total_1w;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgt_Ny_Blank;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgt_Ny_Total_Gross;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgt_Ny_Blank1;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgt_Ny_Total_Taxable_Amt;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgt_Ny_Blank2;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgt_Ny_Total_Withhld;

    private DbsGroup pnd_Org_Ny_Record__R_Field_6;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgf_Ny_Id;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgf_Ny_Total_1e;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgf_Ny_Total_1w;
    private DbsField pnd_Org_Ny_Record_Pnd_Orgf_Ny_Blank;

    private DbsGroup pnd_Org_Ny_Record__R_Field_7;
    private DbsField pnd_Org_Ny_Record_Pnd_Org_Ny_Rec1;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Input_Parms;
    private DbsField pnd_Ws_Pnd_State;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_K;
    private DbsField pnd_Ws_Pnd_St_Occ;
    private DbsField pnd_Ws_Pnd_S2;
    private DbsField pnd_Ws_Pnd_Datx;
    private DbsField pnd_Ws_Pnd_Timx;
    private DbsField pnd_Ws_Pnd_Prev_Rpt_Date;
    private DbsField pnd_Ws_Pnd_Valid_State;
    private DbsField pnd_Ws_Pnd_Company_Break;
    private DbsField pnd_Ws_Pnd_Accepted_Form_Cnt;
    private DbsField pnd_Ws_Pnd_Company_Line;
    private DbsField pnd_Ws_Pnd_Disp_Rec;
    private DbsField pnd_Ws_Pnd_S8;
    private DbsField pnd_Ws_Pnd_Corr_Read_Cnt;
    private DbsField pnd_Ws_Pnd_Org_Read_Cnt;
    private DbsField pnd_Ws_Pnd_Org_1t_Cnt;
    private DbsField pnd_Ws_Pnd_Out_1t_Cnt;
    private DbsField pnd_Ws_Pnd_Out_1t_Cnt2;
    private DbsField pnd_Ws_Pnd_Ws_Tiaa_Total_Gross;
    private DbsField pnd_Ws_Pnd_Ws_Tiaa_Total_Taxamt;
    private DbsField pnd_Ws_Pnd_Ws_Tiaa_Total_Withhld;
    private DbsField pnd_Ws_Pnd_Ws_Tiaa_1w_Cnt;
    private DbsField pnd_Ws_Pnd_Ws_Trust_Total_Gross;
    private DbsField pnd_Ws_Pnd_Ws_Trust_Total_Taxamt;
    private DbsField pnd_Ws_Pnd_Ws_Trust_Total_Withhld;
    private DbsField pnd_Ws_Pnd_Ws_Trust_1w_Cnt;
    private DbsField pnd_Ws_Pnd_Tiaa_Flag;
    private DbsField pnd_Ws_Pnd_Ssn_Match;
    private DbsField pnd_Sort_Rec;

    private DbsGroup pnd_Sort_Rec__R_Field_8;

    private DbsGroup pnd_Sort_Rec_Pnd_Sort_Rec_Detail;
    private DbsField pnd_Sort_Rec_Tirf_Tax_Year;
    private DbsField pnd_Sort_Rec_Tirf_Form_Type;
    private DbsField pnd_Sort_Rec_Tirf_Company_Cde;
    private DbsField pnd_Sort_Rec_Tirf_Tin;
    private DbsField pnd_Sort_Rec_Tirf_Contract_Nbr;
    private DbsField pnd_Sort_Rec_Tirf_Payee_Cde;
    private DbsField pnd_Sort_Rec_Tirf_Key;
    private DbsField pnd_J;
    private DbsField pnd_Tiaa_Array;
    private DbsField pnd_Trust_Array;
    private DbsField pnd_Idx;
    private DbsField pnd_Corr_1a_1e_Tiaa_Found;
    private DbsField pnd_Corr_1a_1e_Trust_Found;
    private DbsField pnd_Tiaa_Present;
    private DbsField pnd_Trust_Present;
    private DbsField pnd_Corr_1a_Trust_Found;
    private DbsField pnd_Corr_1e_Trust_Found;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl3515 = new LdaTwrl3515();
        registerRecord(ldaTwrl3515);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Org_Ny_Record = localVariables.newGroupInRecord("pnd_Org_Ny_Record", "#ORG-NY-RECORD");
        pnd_Org_Ny_Record_Pnd_Orga_Ny_Id = pnd_Org_Ny_Record.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orga_Ny_Id", "#ORGA-NY-ID", FieldType.STRING, 2);
        pnd_Org_Ny_Record_Pnd_Orga_Ny_Create_Date = pnd_Org_Ny_Record.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orga_Ny_Create_Date", "#ORGA-NY-CREATE-DATE", 
            FieldType.STRING, 6);

        pnd_Org_Ny_Record__R_Field_1 = pnd_Org_Ny_Record.newGroupInGroup("pnd_Org_Ny_Record__R_Field_1", "REDEFINE", pnd_Org_Ny_Record_Pnd_Orga_Ny_Create_Date);
        pnd_Org_Ny_Record_Pnd_Orga_Ny_Create_Date_Mm = pnd_Org_Ny_Record__R_Field_1.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orga_Ny_Create_Date_Mm", "#ORGA-NY-CREATE-DATE-MM", 
            FieldType.STRING, 2);
        pnd_Org_Ny_Record_Pnd_Orga_Ny_Create_Date_Dd = pnd_Org_Ny_Record__R_Field_1.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orga_Ny_Create_Date_Dd", "#ORGA-NY-CREATE-DATE-DD", 
            FieldType.STRING, 2);
        pnd_Org_Ny_Record_Pnd_Orga_Ny_Create_Date_Yy = pnd_Org_Ny_Record__R_Field_1.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orga_Ny_Create_Date_Yy", "#ORGA-NY-CREATE-DATE-YY", 
            FieldType.STRING, 2);
        pnd_Org_Ny_Record_Pnd_Orga_Ny_Id_Num = pnd_Org_Ny_Record.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orga_Ny_Id_Num", "#ORGA-NY-ID-NUM", FieldType.STRING, 
            11);
        pnd_Org_Ny_Record_Pnd_Orga_Ny_Trans_Name = pnd_Org_Ny_Record.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orga_Ny_Trans_Name", "#ORGA-NY-TRANS-NAME", 
            FieldType.STRING, 40);
        pnd_Org_Ny_Record_Pnd_Orga_Ny_Address = pnd_Org_Ny_Record.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orga_Ny_Address", "#ORGA-NY-ADDRESS", FieldType.STRING, 
            30);
        pnd_Org_Ny_Record_Pnd_Orga_Ny_City = pnd_Org_Ny_Record.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orga_Ny_City", "#ORGA-NY-CITY", FieldType.STRING, 
            25);
        pnd_Org_Ny_Record_Pnd_Orga_Ny_State = pnd_Org_Ny_Record.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orga_Ny_State", "#ORGA-NY-STATE", FieldType.STRING, 
            2);
        pnd_Org_Ny_Record_Pnd_Orga_Ny_Zip = pnd_Org_Ny_Record.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orga_Ny_Zip", "#ORGA-NY-ZIP", FieldType.STRING, 9);
        pnd_Org_Ny_Record_Pnd_Orga_Ny_Blank = pnd_Org_Ny_Record.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orga_Ny_Blank", "#ORGA-NY-BLANK", FieldType.STRING, 
            3);

        pnd_Org_Ny_Record__R_Field_2 = localVariables.newGroupInRecord("pnd_Org_Ny_Record__R_Field_2", "REDEFINE", pnd_Org_Ny_Record);
        pnd_Org_Ny_Record_Pnd_Orge_Ny_Id = pnd_Org_Ny_Record__R_Field_2.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orge_Ny_Id", "#ORGE-NY-ID", FieldType.STRING, 
            2);
        pnd_Org_Ny_Record_Pnd_Orge_Ny_Report_Quarter = pnd_Org_Ny_Record__R_Field_2.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orge_Ny_Report_Quarter", "#ORGE-NY-REPORT-QUARTER", 
            FieldType.STRING, 4);
        pnd_Org_Ny_Record_Pnd_Orge_Ny_Tin = pnd_Org_Ny_Record__R_Field_2.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orge_Ny_Tin", "#ORGE-NY-TIN", FieldType.STRING, 
            11);
        pnd_Org_Ny_Record_Pnd_Orge_Ny_Blank1 = pnd_Org_Ny_Record__R_Field_2.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orge_Ny_Blank1", "#ORGE-NY-BLANK1", 
            FieldType.STRING, 1);
        pnd_Org_Ny_Record_Pnd_Orge_Ny_Trans_Name = pnd_Org_Ny_Record__R_Field_2.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orge_Ny_Trans_Name", "#ORGE-NY-TRANS-NAME", 
            FieldType.STRING, 40);
        pnd_Org_Ny_Record_Pnd_Orge_Ny_Blank2 = pnd_Org_Ny_Record__R_Field_2.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orge_Ny_Blank2", "#ORGE-NY-BLANK2", 
            FieldType.STRING, 1);
        pnd_Org_Ny_Record_Pnd_Orge_Ny_Address = pnd_Org_Ny_Record__R_Field_2.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orge_Ny_Address", "#ORGE-NY-ADDRESS", 
            FieldType.STRING, 30);
        pnd_Org_Ny_Record_Pnd_Orge_Ny_City = pnd_Org_Ny_Record__R_Field_2.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orge_Ny_City", "#ORGE-NY-CITY", FieldType.STRING, 
            25);
        pnd_Org_Ny_Record_Pnd_Orge_Ny_State = pnd_Org_Ny_Record__R_Field_2.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orge_Ny_State", "#ORGE-NY-STATE", FieldType.STRING, 
            2);
        pnd_Org_Ny_Record_Pnd_Orge_Ny_Zip = pnd_Org_Ny_Record__R_Field_2.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orge_Ny_Zip", "#ORGE-NY-ZIP", FieldType.STRING, 
            9);
        pnd_Org_Ny_Record_Pnd_Orge_Ny_Blank = pnd_Org_Ny_Record__R_Field_2.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orge_Ny_Blank", "#ORGE-NY-BLANK", FieldType.STRING, 
            1);
        pnd_Org_Ny_Record_Pnd_Orge_Ny_Ret_Type = pnd_Org_Ny_Record__R_Field_2.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orge_Ny_Ret_Type", "#ORGE-NY-RET-TYPE", 
            FieldType.STRING, 1);
        pnd_Org_Ny_Record_Pnd_Orge_Ny_Emp_Ind = pnd_Org_Ny_Record__R_Field_2.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orge_Ny_Emp_Ind", "#ORGE-NY-EMP-IND", 
            FieldType.STRING, 1);

        pnd_Org_Ny_Record__R_Field_3 = localVariables.newGroupInRecord("pnd_Org_Ny_Record__R_Field_3", "REDEFINE", pnd_Org_Ny_Record);
        pnd_Org_Ny_Record_Pnd_Orgw_Ny_Id = pnd_Org_Ny_Record__R_Field_3.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgw_Ny_Id", "#ORGW-NY-ID", FieldType.STRING, 
            2);
        pnd_Org_Ny_Record_Pnd_Orgw_Ny_Tin = pnd_Org_Ny_Record__R_Field_3.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgw_Ny_Tin", "#ORGW-NY-TIN", FieldType.STRING, 
            9);
        pnd_Org_Ny_Record_Pnd_Orgw_Ny_Name = pnd_Org_Ny_Record__R_Field_3.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgw_Ny_Name", "#ORGW-NY-NAME", FieldType.STRING, 
            30);
        pnd_Org_Ny_Record_Pnd_Orgw_Ny_Blank1 = pnd_Org_Ny_Record__R_Field_3.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgw_Ny_Blank1", "#ORGW-NY-BLANK1", 
            FieldType.STRING, 1);
        pnd_Org_Ny_Record_Pnd_Orgw_Ny_Wages_Ind = pnd_Org_Ny_Record__R_Field_3.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgw_Ny_Wages_Ind", "#ORGW-NY-WAGES-IND", 
            FieldType.STRING, 1);
        pnd_Org_Ny_Record_Pnd_Orgw_Ny_Blank2 = pnd_Org_Ny_Record__R_Field_3.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgw_Ny_Blank2", "#ORGW-NY-BLANK2", 
            FieldType.STRING, 1);
        pnd_Org_Ny_Record_Pnd_Orgw_Ny_Gross = pnd_Org_Ny_Record__R_Field_3.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgw_Ny_Gross", "#ORGW-NY-GROSS", FieldType.NUMERIC, 
            14, 2);

        pnd_Org_Ny_Record__R_Field_4 = pnd_Org_Ny_Record__R_Field_3.newGroupInGroup("pnd_Org_Ny_Record__R_Field_4", "REDEFINE", pnd_Org_Ny_Record_Pnd_Orgw_Ny_Gross);
        pnd_Org_Ny_Record_Pnd_Orgw_Ny_Gross_A = pnd_Org_Ny_Record__R_Field_4.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgw_Ny_Gross_A", "#ORGW-NY-GROSS-A", 
            FieldType.STRING, 14);
        pnd_Org_Ny_Record_Pnd_Orgw_Ny_Blank3 = pnd_Org_Ny_Record__R_Field_3.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgw_Ny_Blank3", "#ORGW-NY-BLANK3", 
            FieldType.STRING, 1);
        pnd_Org_Ny_Record_Pnd_Orgw_Ny_Taxable_Amt = pnd_Org_Ny_Record__R_Field_3.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgw_Ny_Taxable_Amt", "#ORGW-NY-TAXABLE-AMT", 
            FieldType.NUMERIC, 14, 2);
        pnd_Org_Ny_Record_Pnd_Orgw_Ny_Blank4 = pnd_Org_Ny_Record__R_Field_3.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgw_Ny_Blank4", "#ORGW-NY-BLANK4", 
            FieldType.STRING, 1);
        pnd_Org_Ny_Record_Pnd_Orgw_Ny_Tax_Wthld = pnd_Org_Ny_Record__R_Field_3.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgw_Ny_Tax_Wthld", "#ORGW-NY-TAX-WTHLD", 
            FieldType.NUMERIC, 14, 2);
        pnd_Org_Ny_Record_Pnd_Orgw_Ny_Blank5 = pnd_Org_Ny_Record__R_Field_3.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgw_Ny_Blank5", "#ORGW-NY-BLANK5", 
            FieldType.STRING, 40);

        pnd_Org_Ny_Record__R_Field_5 = localVariables.newGroupInRecord("pnd_Org_Ny_Record__R_Field_5", "REDEFINE", pnd_Org_Ny_Record);
        pnd_Org_Ny_Record_Pnd_Orgt_Ny_Id = pnd_Org_Ny_Record__R_Field_5.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgt_Ny_Id", "#ORGT-NY-ID", FieldType.STRING, 
            2);
        pnd_Org_Ny_Record_Pnd_Orgt_Ny_Total_1w = pnd_Org_Ny_Record__R_Field_5.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgt_Ny_Total_1w", "#ORGT-NY-TOTAL-1W", 
            FieldType.NUMERIC, 7);
        pnd_Org_Ny_Record_Pnd_Orgt_Ny_Blank = pnd_Org_Ny_Record__R_Field_5.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgt_Ny_Blank", "#ORGT-NY-BLANK", FieldType.STRING, 
            35);
        pnd_Org_Ny_Record_Pnd_Orgt_Ny_Total_Gross = pnd_Org_Ny_Record__R_Field_5.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgt_Ny_Total_Gross", "#ORGT-NY-TOTAL-GROSS", 
            FieldType.NUMERIC, 14, 2);
        pnd_Org_Ny_Record_Pnd_Orgt_Ny_Blank1 = pnd_Org_Ny_Record__R_Field_5.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgt_Ny_Blank1", "#ORGT-NY-BLANK1", 
            FieldType.STRING, 1);
        pnd_Org_Ny_Record_Pnd_Orgt_Ny_Total_Taxable_Amt = pnd_Org_Ny_Record__R_Field_5.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgt_Ny_Total_Taxable_Amt", 
            "#ORGT-NY-TOTAL-TAXABLE-AMT", FieldType.NUMERIC, 14, 2);
        pnd_Org_Ny_Record_Pnd_Orgt_Ny_Blank2 = pnd_Org_Ny_Record__R_Field_5.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgt_Ny_Blank2", "#ORGT-NY-BLANK2", 
            FieldType.STRING, 1);
        pnd_Org_Ny_Record_Pnd_Orgt_Ny_Total_Withhld = pnd_Org_Ny_Record__R_Field_5.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgt_Ny_Total_Withhld", "#ORGT-NY-TOTAL-WITHHLD", 
            FieldType.NUMERIC, 14, 2);

        pnd_Org_Ny_Record__R_Field_6 = localVariables.newGroupInRecord("pnd_Org_Ny_Record__R_Field_6", "REDEFINE", pnd_Org_Ny_Record);
        pnd_Org_Ny_Record_Pnd_Orgf_Ny_Id = pnd_Org_Ny_Record__R_Field_6.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgf_Ny_Id", "#ORGF-NY-ID", FieldType.STRING, 
            2);
        pnd_Org_Ny_Record_Pnd_Orgf_Ny_Total_1e = pnd_Org_Ny_Record__R_Field_6.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgf_Ny_Total_1e", "#ORGF-NY-TOTAL-1E", 
            FieldType.NUMERIC, 10);
        pnd_Org_Ny_Record_Pnd_Orgf_Ny_Total_1w = pnd_Org_Ny_Record__R_Field_6.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgf_Ny_Total_1w", "#ORGF-NY-TOTAL-1W", 
            FieldType.NUMERIC, 10);
        pnd_Org_Ny_Record_Pnd_Orgf_Ny_Blank = pnd_Org_Ny_Record__R_Field_6.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Orgf_Ny_Blank", "#ORGF-NY-BLANK", FieldType.STRING, 
            106);

        pnd_Org_Ny_Record__R_Field_7 = localVariables.newGroupInRecord("pnd_Org_Ny_Record__R_Field_7", "REDEFINE", pnd_Org_Ny_Record);
        pnd_Org_Ny_Record_Pnd_Org_Ny_Rec1 = pnd_Org_Ny_Record__R_Field_7.newFieldInGroup("pnd_Org_Ny_Record_Pnd_Org_Ny_Rec1", "#ORG-NY-REC1", FieldType.STRING, 
            128);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Input_Parms = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Ws_Pnd_State = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_K = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_St_Occ = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_St_Occ", "#ST-OCC", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S2", "#S2", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Datx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Datx", "#DATX", FieldType.DATE);
        pnd_Ws_Pnd_Timx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Timx", "#TIMX", FieldType.TIME);
        pnd_Ws_Pnd_Prev_Rpt_Date = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Rpt_Date", "#PREV-RPT-DATE", FieldType.DATE);
        pnd_Ws_Pnd_Valid_State = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Valid_State", "#VALID-STATE", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Company_Break = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Break", "#COMPANY-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Accepted_Form_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Accepted_Form_Cnt", "#ACCEPTED-FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Company_Line = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Line", "#COMPANY-LINE", FieldType.STRING, 72);
        pnd_Ws_Pnd_Disp_Rec = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Disp_Rec", "#DISP-REC", FieldType.STRING, 5);
        pnd_Ws_Pnd_S8 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S8", "#S8", FieldType.STRING, 8);
        pnd_Ws_Pnd_Corr_Read_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Corr_Read_Cnt", "#CORR-READ-CNT", FieldType.NUMERIC, 5);
        pnd_Ws_Pnd_Org_Read_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Org_Read_Cnt", "#ORG-READ-CNT", FieldType.NUMERIC, 5);
        pnd_Ws_Pnd_Org_1t_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Org_1t_Cnt", "#ORG-1T-CNT", FieldType.NUMERIC, 5);
        pnd_Ws_Pnd_Out_1t_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Out_1t_Cnt", "#OUT-1T-CNT", FieldType.NUMERIC, 5);
        pnd_Ws_Pnd_Out_1t_Cnt2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Out_1t_Cnt2", "#OUT-1T-CNT2", FieldType.NUMERIC, 5);
        pnd_Ws_Pnd_Ws_Tiaa_Total_Gross = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Tiaa_Total_Gross", "#WS-TIAA-TOTAL-GROSS", FieldType.NUMERIC, 14, 2);
        pnd_Ws_Pnd_Ws_Tiaa_Total_Taxamt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Tiaa_Total_Taxamt", "#WS-TIAA-TOTAL-TAXAMT", FieldType.NUMERIC, 14, 2);
        pnd_Ws_Pnd_Ws_Tiaa_Total_Withhld = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Tiaa_Total_Withhld", "#WS-TIAA-TOTAL-WITHHLD", FieldType.NUMERIC, 14, 
            2);
        pnd_Ws_Pnd_Ws_Tiaa_1w_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Tiaa_1w_Cnt", "#WS-TIAA-1W-CNT", FieldType.NUMERIC, 7);
        pnd_Ws_Pnd_Ws_Trust_Total_Gross = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Trust_Total_Gross", "#WS-TRUST-TOTAL-GROSS", FieldType.NUMERIC, 14, 2);
        pnd_Ws_Pnd_Ws_Trust_Total_Taxamt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Trust_Total_Taxamt", "#WS-TRUST-TOTAL-TAXAMT", FieldType.NUMERIC, 14, 
            2);
        pnd_Ws_Pnd_Ws_Trust_Total_Withhld = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Trust_Total_Withhld", "#WS-TRUST-TOTAL-WITHHLD", FieldType.NUMERIC, 
            14, 2);
        pnd_Ws_Pnd_Ws_Trust_1w_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Trust_1w_Cnt", "#WS-TRUST-1W-CNT", FieldType.NUMERIC, 7);
        pnd_Ws_Pnd_Tiaa_Flag = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tiaa_Flag", "#TIAA-FLAG", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Ssn_Match = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ssn_Match", "#SSN-MATCH", FieldType.BOOLEAN, 1);
        pnd_Sort_Rec = localVariables.newFieldInRecord("pnd_Sort_Rec", "#SORT-REC", FieldType.STRING, 32);

        pnd_Sort_Rec__R_Field_8 = localVariables.newGroupInRecord("pnd_Sort_Rec__R_Field_8", "REDEFINE", pnd_Sort_Rec);

        pnd_Sort_Rec_Pnd_Sort_Rec_Detail = pnd_Sort_Rec__R_Field_8.newGroupInGroup("pnd_Sort_Rec_Pnd_Sort_Rec_Detail", "#SORT-REC-DETAIL");
        pnd_Sort_Rec_Tirf_Tax_Year = pnd_Sort_Rec_Pnd_Sort_Rec_Detail.newFieldInGroup("pnd_Sort_Rec_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Sort_Rec_Tirf_Form_Type = pnd_Sort_Rec_Pnd_Sort_Rec_Detail.newFieldInGroup("pnd_Sort_Rec_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 
            2);
        pnd_Sort_Rec_Tirf_Company_Cde = pnd_Sort_Rec_Pnd_Sort_Rec_Detail.newFieldInGroup("pnd_Sort_Rec_Tirf_Company_Cde", "TIRF-COMPANY-CDE", FieldType.STRING, 
            1);
        pnd_Sort_Rec_Tirf_Tin = pnd_Sort_Rec_Pnd_Sort_Rec_Detail.newFieldInGroup("pnd_Sort_Rec_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10);
        pnd_Sort_Rec_Tirf_Contract_Nbr = pnd_Sort_Rec_Pnd_Sort_Rec_Detail.newFieldInGroup("pnd_Sort_Rec_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 
            8);
        pnd_Sort_Rec_Tirf_Payee_Cde = pnd_Sort_Rec_Pnd_Sort_Rec_Detail.newFieldInGroup("pnd_Sort_Rec_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Sort_Rec_Tirf_Key = pnd_Sort_Rec_Pnd_Sort_Rec_Detail.newFieldInGroup("pnd_Sort_Rec_Tirf_Key", "TIRF-KEY", FieldType.STRING, 5);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 4);
        pnd_Tiaa_Array = localVariables.newFieldArrayInRecord("pnd_Tiaa_Array", "#TIAA-ARRAY", FieldType.STRING, 9, new DbsArrayController(1, 1000));
        pnd_Trust_Array = localVariables.newFieldArrayInRecord("pnd_Trust_Array", "#TRUST-ARRAY", FieldType.STRING, 9, new DbsArrayController(1, 1000));
        pnd_Idx = localVariables.newFieldInRecord("pnd_Idx", "#IDX", FieldType.NUMERIC, 3);
        pnd_Corr_1a_1e_Tiaa_Found = localVariables.newFieldInRecord("pnd_Corr_1a_1e_Tiaa_Found", "#CORR-1A-1E-TIAA-FOUND", FieldType.BOOLEAN, 1);
        pnd_Corr_1a_1e_Trust_Found = localVariables.newFieldInRecord("pnd_Corr_1a_1e_Trust_Found", "#CORR-1A-1E-TRUST-FOUND", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Present = localVariables.newFieldInRecord("pnd_Tiaa_Present", "#TIAA-PRESENT", FieldType.BOOLEAN, 1);
        pnd_Trust_Present = localVariables.newFieldInRecord("pnd_Trust_Present", "#TRUST-PRESENT", FieldType.BOOLEAN, 1);
        pnd_Corr_1a_Trust_Found = localVariables.newFieldInRecord("pnd_Corr_1a_Trust_Found", "#CORR-1A-TRUST-FOUND", FieldType.BOOLEAN, 1);
        pnd_Corr_1e_Trust_Found = localVariables.newFieldInRecord("pnd_Corr_1e_Trust_Found", "#CORR-1E-TRUST-FOUND", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl3515.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_Company_Break.setInitialValue(true);
        pnd_Ws_Pnd_Corr_Read_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Org_Read_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Org_1t_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Out_1t_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Out_1t_Cnt2.setInitialValue(0);
        pnd_Ws_Pnd_Ws_Tiaa_Total_Gross.setInitialValue(0);
        pnd_Ws_Pnd_Ws_Tiaa_Total_Taxamt.setInitialValue(0);
        pnd_Ws_Pnd_Ws_Tiaa_Total_Withhld.setInitialValue(0);
        pnd_Ws_Pnd_Ws_Tiaa_1w_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Ws_Trust_Total_Gross.setInitialValue(0);
        pnd_Ws_Pnd_Ws_Trust_Total_Taxamt.setInitialValue(0);
        pnd_Ws_Pnd_Ws_Trust_Total_Withhld.setInitialValue(0);
        pnd_Ws_Pnd_Ws_Trust_1w_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Tiaa_Flag.setInitialValue(false);
        pnd_Ws_Pnd_Ssn_Match.setInitialValue(false);
        pnd_J.setInitialValue(1);
        pnd_Corr_1a_1e_Tiaa_Found.setInitialValue(false);
        pnd_Corr_1a_1e_Trust_Found.setInitialValue(false);
        pnd_Tiaa_Present.setInitialValue(false);
        pnd_Trust_Present.setInitialValue(false);
        pnd_Corr_1a_Trust_Found.setInitialValue(false);
        pnd_Corr_1e_Trust_Found.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3556() throws Exception
    {
        super("Twrp3556");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
                                                                                                                                                                          //Natural: PERFORM INITIAL-PROCESS
        sub_Initial_Process();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADER-CORR-1W
        sub_Write_Header_Corr_1w();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-ORIG-1W
        sub_Process_Orig_1w();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-TRAILER
        sub_Process_Trailer();
        if (condition(Global.isEscape())) {return;}
        //* ************************
        //*  S U B R O U T I N E S *
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIAL-PROCESS
        //* ********************************
        //* * EINCHG END
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HEADER-CORR-1W
        //* *************************************
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ORIG-1W
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CORR-1W
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TRAILER
        //* ********************************
    }
    private void sub_Initial_Process() throws Exception                                                                                                                   //Natural: INITIAL-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        R0:                                                                                                                                                               //Natural: READ WORK FILE 1 #OUT-NY-RECORD
        while (condition(getWorkFiles().read(1, ldaTwrl3515.getPnd_Out_Ny_Record())))
        {
            if (condition(ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Oute_Ny_Id().equals("1E")))                                                                                //Natural: IF #OUTE-NY-ID = '1E'
            {
                //* * EINCHG START
                //*    IF #OUTE-NY-TIN EQ '131624203'
                if (condition(ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Oute_Ny_Tin().equals("131624203") || ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Oute_Ny_Tin().equals("822826183"))) //Natural: IF #OUTE-NY-TIN EQ '131624203' OR #OUTE-NY-TIN EQ '822826183'
                {
                    pnd_Tiaa_Present.setValue(true);                                                                                                                      //Natural: ASSIGN #TIAA-PRESENT := TRUE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Oute_Ny_Tin().equals("516559589")))                                                                //Natural: IF #OUTE-NY-TIN EQ '516559589'
                    {
                        pnd_Trust_Present.setValue(true);                                                                                                                 //Natural: ASSIGN #TRUST-PRESENT := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        R0_Exit:
        if (Global.isEscape()) return;
        if (condition(((pnd_Tiaa_Present.getBoolean() && pnd_Trust_Present.getBoolean()) || (! (pnd_Tiaa_Present.getBoolean()) && ! (pnd_Trust_Present.getBoolean())))))  //Natural: IF ( #TIAA-PRESENT AND #TRUST-PRESENT ) OR ( NOT #TIAA-PRESENT AND NOT #TRUST-PRESENT )
        {
            pnd_Ws_Pnd_Tiaa_Flag.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #TIAA-FLAG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Tiaa_Present.getBoolean() && ! (pnd_Trust_Present.getBoolean())))                                                                               //Natural: IF #TIAA-PRESENT AND NOT #TRUST-PRESENT
        {
            pnd_Ws_Pnd_Tiaa_Flag.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #TIAA-FLAG
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Tiaa_Present.getBoolean()) && pnd_Trust_Present.getBoolean()))                                                                               //Natural: IF NOT #TIAA-PRESENT AND #TRUST-PRESENT
        {
            pnd_Ws_Pnd_Tiaa_Flag.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #TIAA-FLAG
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Header_Corr_1w() throws Exception                                                                                                              //Natural: WRITE-HEADER-CORR-1W
    {
        if (BLNatReinput.isReinput()) return;

        R1:                                                                                                                                                               //Natural: READ WORK FILE 1 #OUT-NY-RECORD
        while (condition(getWorkFiles().read(1, ldaTwrl3515.getPnd_Out_Ny_Record())))
        {
            if (condition(pnd_Ws_Pnd_Tiaa_Flag.getBoolean()))                                                                                                             //Natural: IF #TIAA-FLAG
            {
                if (condition(ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Outa_Ny_Id().equals("1A") || ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Oute_Ny_Id().equals("1E")))          //Natural: IF #OUTA-NY-ID = '1A' OR #OUTE-NY-ID = '1E'
                {
                    getWorkFiles().write(3, false, ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Out_Ny_Rec1());                                                                   //Natural: WRITE WORK FILE 3 #OUT-NY-REC1
                    pnd_Corr_1a_1e_Tiaa_Found.setValue(true);                                                                                                             //Natural: ASSIGN #CORR-1A-1E-TIAA-FOUND := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Id().equals("1W")))                                                                            //Natural: IF #OUTW-NY-ID = '1W'
                {
                                                                                                                                                                          //Natural: PERFORM PROCESS-CORR-1W
                    sub_Process_Corr_1w();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Outt_Ny_Id().equals("1T") || ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Outf_Ny_Id().equals("1F")))          //Natural: IF #OUTT-NY-ID = '1T' OR #OUTF-NY-ID = '1F'
                {
                    pnd_J.setValue(1);                                                                                                                                    //Natural: ASSIGN #J := 1
                    pnd_Ws_Pnd_Tiaa_Flag.reset();                                                                                                                         //Natural: RESET #TIAA-FLAG
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Ws_Pnd_Tiaa_Flag.getBoolean())))                                                                                                         //Natural: IF NOT #TIAA-FLAG
            {
                if (condition(ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Outt_Ny_Id().equals("1T") || ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Outf_Ny_Id().equals("1F")))          //Natural: IF #OUTT-NY-ID = '1T' OR #OUTF-NY-ID = '1F'
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Outa_Ny_Id().equals("1A")))                                                                            //Natural: IF #OUTA-NY-ID = '1A'
                {
                    getWorkFiles().write(3, false, ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Out_Ny_Rec1());                                                                   //Natural: WRITE WORK FILE 3 #OUT-NY-REC1
                    pnd_Corr_1a_Trust_Found.setValue(true);                                                                                                               //Natural: ASSIGN #CORR-1A-TRUST-FOUND := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Oute_Ny_Id().equals("1E")))                                                                            //Natural: IF #OUTE-NY-ID = '1E'
                {
                    getWorkFiles().write(4, false, ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Out_Ny_Rec1());                                                                   //Natural: WRITE WORK FILE 4 #OUT-NY-REC1
                    pnd_Corr_1e_Trust_Found.setValue(true);                                                                                                               //Natural: ASSIGN #CORR-1E-TRUST-FOUND := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Id().equals("1W")))                                                                            //Natural: IF #OUTW-NY-ID = '1W'
                {
                                                                                                                                                                          //Natural: PERFORM PROCESS-CORR-1W
                    sub_Process_Corr_1w();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        R1_Exit:
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Tiaa_Flag.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #TIAA-FLAG
        //*  WRITE-HEADER-CORR-1W
    }
    private void sub_Process_Orig_1w() throws Exception                                                                                                                   //Natural: PROCESS-ORIG-1W
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        R2:                                                                                                                                                               //Natural: READ WORK FILE 2 #ORG-NY-REC1
        while (condition(getWorkFiles().read(2, pnd_Org_Ny_Record_Pnd_Org_Ny_Rec1)))
        {
            if (condition(pnd_Ws_Pnd_Tiaa_Flag.getBoolean()))                                                                                                             //Natural: IF #TIAA-FLAG
            {
                if (condition(pnd_Org_Ny_Record_Pnd_Orga_Ny_Id.equals("1A")))                                                                                             //Natural: IF #ORGA-NY-ID = '1A'
                {
                    if (condition(! (pnd_Corr_1a_1e_Tiaa_Found.getBoolean()) && ! (pnd_Corr_1a_Trust_Found.getBoolean())))                                                //Natural: IF NOT #CORR-1A-1E-TIAA-FOUND AND NOT #CORR-1A-TRUST-FOUND
                    {
                        getWorkFiles().write(3, false, pnd_Org_Ny_Record_Pnd_Org_Ny_Rec1);                                                                                //Natural: WRITE WORK FILE 3 #ORG-NY-REC1
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Org_Ny_Record_Pnd_Orge_Ny_Id.equals("1E")))                                                                                             //Natural: IF #ORGE-NY-ID = '1E'
                {
                    if (condition(! (pnd_Corr_1a_1e_Tiaa_Found.getBoolean())))                                                                                            //Natural: IF NOT #CORR-1A-1E-TIAA-FOUND
                    {
                        getWorkFiles().write(3, false, pnd_Org_Ny_Record_Pnd_Org_Ny_Rec1);                                                                                //Natural: WRITE WORK FILE 3 #ORG-NY-REC1
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Idx.reset();                                                                                                                                          //Natural: RESET #IDX
                if (condition(pnd_Org_Ny_Record_Pnd_Orgw_Ny_Id.equals("1W")))                                                                                             //Natural: IF #ORGW-NY-ID = '1W'
                {
                    DbsUtil.examine(new ExamineSource(pnd_Tiaa_Array.getValue("*")), new ExamineSearch(pnd_Org_Ny_Record_Pnd_Orgw_Ny_Tin), new ExamineGivingIndex(pnd_Idx)); //Natural: EXAMINE #TIAA-ARRAY ( * ) FOR #ORGW-NY-TIN GIVING INDEX IN #IDX
                    if (condition(pnd_Idx.equals(getZero())))                                                                                                             //Natural: IF #IDX = 0
                    {
                        getWorkFiles().write(3, false, pnd_Org_Ny_Record_Pnd_Org_Ny_Rec1);                                                                                //Natural: WRITE WORK FILE 3 #ORG-NY-REC1
                        pnd_Ws_Pnd_Ws_Tiaa_1w_Cnt.nadd(1);                                                                                                                //Natural: ADD 1 TO #WS-TIAA-1W-CNT
                        pnd_Ws_Pnd_Ws_Tiaa_Total_Gross.nadd(pnd_Org_Ny_Record_Pnd_Orgw_Ny_Gross);                                                                         //Natural: ASSIGN #WS-TIAA-TOTAL-GROSS := #WS-TIAA-TOTAL-GROSS + #ORGW-NY-GROSS
                        pnd_Ws_Pnd_Ws_Tiaa_Total_Taxamt.nadd(pnd_Org_Ny_Record_Pnd_Orgw_Ny_Taxable_Amt);                                                                  //Natural: ASSIGN #WS-TIAA-TOTAL-TAXAMT := #WS-TIAA-TOTAL-TAXAMT + #ORGW-NY-TAXABLE-AMT
                        pnd_Ws_Pnd_Ws_Tiaa_Total_Withhld.nadd(pnd_Org_Ny_Record_Pnd_Orgw_Ny_Tax_Wthld);                                                                   //Natural: ASSIGN #WS-TIAA-TOTAL-WITHHLD := #WS-TIAA-TOTAL-WITHHLD + #ORGW-NY-TAX-WTHLD
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Org_Ny_Record_Pnd_Orgt_Ny_Id.equals("1T")))                                                                                             //Natural: IF #ORGT-NY-ID = '1T'
                {
                    pnd_Ws_Pnd_Tiaa_Flag.reset();                                                                                                                         //Natural: RESET #TIAA-FLAG
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Ws_Pnd_Tiaa_Flag.getBoolean())))                                                                                                         //Natural: IF NOT #TIAA-FLAG
            {
                if (condition(pnd_Org_Ny_Record_Pnd_Orgt_Ny_Id.equals("1T") || pnd_Org_Ny_Record_Pnd_Orgf_Ny_Id.equals("1F")))                                            //Natural: IF #ORGT-NY-ID = '1T' OR #ORGF-NY-ID = '1F'
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Org_Ny_Record_Pnd_Orge_Ny_Id.equals("1E")))                                                                                             //Natural: IF #ORGE-NY-ID = '1E'
                {
                    if (condition(! (pnd_Corr_1e_Trust_Found.getBoolean())))                                                                                              //Natural: IF NOT #CORR-1E-TRUST-FOUND
                    {
                        getWorkFiles().write(4, false, pnd_Org_Ny_Record_Pnd_Org_Ny_Rec1);                                                                                //Natural: WRITE WORK FILE 4 #ORG-NY-REC1
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Idx.reset();                                                                                                                                          //Natural: RESET #IDX
                if (condition(pnd_Org_Ny_Record_Pnd_Orgw_Ny_Id.equals("1W")))                                                                                             //Natural: IF #ORGW-NY-ID = '1W'
                {
                    DbsUtil.examine(new ExamineSource(pnd_Trust_Array.getValue("*")), new ExamineSearch(pnd_Org_Ny_Record_Pnd_Orgw_Ny_Tin), new ExamineGivingIndex(pnd_Idx)); //Natural: EXAMINE #TRUST-ARRAY ( * ) FOR #ORGW-NY-TIN GIVING INDEX IN #IDX
                    if (condition(pnd_Idx.equals(getZero())))                                                                                                             //Natural: IF #IDX = 0
                    {
                        getWorkFiles().write(4, false, pnd_Org_Ny_Record_Pnd_Org_Ny_Rec1);                                                                                //Natural: WRITE WORK FILE 4 #ORG-NY-REC1
                        pnd_Ws_Pnd_Ws_Trust_1w_Cnt.nadd(1);                                                                                                               //Natural: ADD 1 TO #WS-TRUST-1W-CNT
                        pnd_Ws_Pnd_Ws_Trust_Total_Gross.nadd(pnd_Org_Ny_Record_Pnd_Orgw_Ny_Gross);                                                                        //Natural: ASSIGN #WS-TRUST-TOTAL-GROSS := #WS-TRUST-TOTAL-GROSS + #ORGW-NY-GROSS
                        pnd_Ws_Pnd_Ws_Trust_Total_Taxamt.nadd(pnd_Org_Ny_Record_Pnd_Orgw_Ny_Taxable_Amt);                                                                 //Natural: ASSIGN #WS-TRUST-TOTAL-TAXAMT := #WS-TRUST-TOTAL-TAXAMT + #ORGW-NY-TAXABLE-AMT
                        pnd_Ws_Pnd_Ws_Trust_Total_Withhld.nadd(pnd_Org_Ny_Record_Pnd_Orgw_Ny_Tax_Wthld);                                                                  //Natural: ASSIGN #WS-TRUST-TOTAL-WITHHLD := #WS-TRUST-TOTAL-WITHHLD + #ORGW-NY-TAX-WTHLD
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        R2_Exit:
        if (Global.isEscape()) return;
        //*  PROCESS-ORIG-1W
    }
    private void sub_Process_Corr_1w() throws Exception                                                                                                                   //Natural: PROCESS-CORR-1W
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        if (condition(pnd_Ws_Pnd_Tiaa_Flag.getBoolean()))                                                                                                                 //Natural: IF #TIAA-FLAG
        {
            if (condition(ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Id().equals("1W")))                                                                                //Natural: IF #OUTW-NY-ID = '1W'
            {
                //*  LOAD TIAA
                pnd_Tiaa_Array.getValue(pnd_J).setValue(ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Tin());                                                              //Natural: MOVE #OUT-NY-RECORD.#OUTW-NY-TIN TO #TIAA-ARRAY ( #J )
                pnd_J.nadd(1);                                                                                                                                            //Natural: ASSIGN #J := #J + 1
                getWorkFiles().write(3, false, ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Out_Ny_Rec1());                                                                       //Natural: WRITE WORK FILE 3 #OUT-NY-REC1
                pnd_Ws_Pnd_Ws_Tiaa_1w_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #WS-TIAA-1W-CNT
                pnd_Ws_Pnd_Ws_Tiaa_Total_Gross.nadd(ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Gross());                                                                //Natural: ASSIGN #WS-TIAA-TOTAL-GROSS := #WS-TIAA-TOTAL-GROSS + #OUTW-NY-GROSS
                pnd_Ws_Pnd_Ws_Tiaa_Total_Taxamt.nadd(ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Taxable_Amt());                                                         //Natural: ASSIGN #WS-TIAA-TOTAL-TAXAMT := #WS-TIAA-TOTAL-TAXAMT + #OUTW-NY-TAXABLE-AMT
                pnd_Ws_Pnd_Ws_Tiaa_Total_Withhld.nadd(ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Tax_Wthld());                                                          //Natural: ASSIGN #WS-TIAA-TOTAL-WITHHLD := #WS-TIAA-TOTAL-WITHHLD + #OUTW-NY-TAX-WTHLD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Ws_Pnd_Tiaa_Flag.getBoolean())))                                                                                                             //Natural: IF NOT #TIAA-FLAG
        {
            if (condition(ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Id().equals("1W")))                                                                                //Natural: IF #OUTW-NY-ID = '1W'
            {
                //*  LOAD TRUST
                pnd_Trust_Array.getValue(pnd_J).setValue(ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Tin());                                                             //Natural: MOVE #OUT-NY-RECORD.#OUTW-NY-TIN TO #TRUST-ARRAY ( #J )
                pnd_J.nadd(1);                                                                                                                                            //Natural: ASSIGN #J := #J + 1
                getWorkFiles().write(4, false, ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Out_Ny_Rec1());                                                                       //Natural: WRITE WORK FILE 4 #OUT-NY-REC1
                pnd_Ws_Pnd_Ws_Trust_1w_Cnt.nadd(1);                                                                                                                       //Natural: ADD 1 TO #WS-TRUST-1W-CNT
                pnd_Ws_Pnd_Ws_Trust_Total_Gross.nadd(ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Gross());                                                               //Natural: ASSIGN #WS-TRUST-TOTAL-GROSS := #WS-TRUST-TOTAL-GROSS + #OUTW-NY-GROSS
                pnd_Ws_Pnd_Ws_Trust_Total_Taxamt.nadd(ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Taxable_Amt());                                                        //Natural: ASSIGN #WS-TRUST-TOTAL-TAXAMT := #WS-TRUST-TOTAL-TAXAMT + #OUTW-NY-TAXABLE-AMT
                pnd_Ws_Pnd_Ws_Trust_Total_Withhld.nadd(ldaTwrl3515.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Tax_Wthld());                                                         //Natural: ASSIGN #WS-TRUST-TOTAL-WITHHLD := #WS-TRUST-TOTAL-WITHHLD + #OUTW-NY-TAX-WTHLD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  PROCESS-CORR-1W
    }
    private void sub_Process_Trailer() throws Exception                                                                                                                   //Natural: PROCESS-TRAILER
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pnd_Org_Ny_Record_Pnd_Org_Ny_Rec1.reset();                                                                                                                        //Natural: RESET #ORG-NY-REC1
        pnd_Org_Ny_Record_Pnd_Orgt_Ny_Id.setValue("1T");                                                                                                                  //Natural: MOVE '1T' TO #ORGT-NY-ID
        pnd_Org_Ny_Record_Pnd_Orgt_Ny_Total_1w.setValue(pnd_Ws_Pnd_Ws_Tiaa_1w_Cnt);                                                                                       //Natural: MOVE #WS-TIAA-1W-CNT TO #ORGT-NY-TOTAL-1W
        //* *MOVE #WS-TIAA-TOTAL-GROSS TO #ORGT-NY-TOTAL-GROSS
        pnd_Org_Ny_Record_Pnd_Orgt_Ny_Total_Gross.setValue(0);                                                                                                            //Natural: MOVE 0 TO #ORGT-NY-TOTAL-GROSS
        pnd_Org_Ny_Record_Pnd_Orgt_Ny_Total_Taxable_Amt.setValue(pnd_Ws_Pnd_Ws_Tiaa_Total_Taxamt);                                                                        //Natural: MOVE #WS-TIAA-TOTAL-TAXAMT TO #ORGT-NY-TOTAL-TAXABLE-AMT
        pnd_Org_Ny_Record_Pnd_Orgt_Ny_Total_Withhld.setValue(pnd_Ws_Pnd_Ws_Tiaa_Total_Withhld);                                                                           //Natural: MOVE #WS-TIAA-TOTAL-WITHHLD TO #ORGT-NY-TOTAL-WITHHLD
        //*  WRITE TIAA TRAILER
        getWorkFiles().write(3, false, pnd_Org_Ny_Record_Pnd_Org_Ny_Rec1);                                                                                                //Natural: WRITE WORK FILE 3 #ORG-NY-REC1
        pnd_Org_Ny_Record_Pnd_Org_Ny_Rec1.reset();                                                                                                                        //Natural: RESET #ORG-NY-REC1
        pnd_Org_Ny_Record_Pnd_Orgt_Ny_Id.setValue("1T");                                                                                                                  //Natural: MOVE '1T' TO #ORGT-NY-ID
        pnd_Org_Ny_Record_Pnd_Orgt_Ny_Total_1w.setValue(pnd_Ws_Pnd_Ws_Trust_1w_Cnt);                                                                                      //Natural: MOVE #WS-TRUST-1W-CNT TO #ORGT-NY-TOTAL-1W
        //* *MOVE #WS-TRUST-TOTAL-GROSS TO #ORGT-NY-TOTAL-GROSS
        pnd_Org_Ny_Record_Pnd_Orgt_Ny_Total_Gross.setValue(0);                                                                                                            //Natural: MOVE 0 TO #ORGT-NY-TOTAL-GROSS
        pnd_Org_Ny_Record_Pnd_Orgt_Ny_Total_Taxable_Amt.setValue(pnd_Ws_Pnd_Ws_Trust_Total_Taxamt);                                                                       //Natural: MOVE #WS-TRUST-TOTAL-TAXAMT TO #ORGT-NY-TOTAL-TAXABLE-AMT
        pnd_Org_Ny_Record_Pnd_Orgt_Ny_Total_Withhld.setValue(pnd_Ws_Pnd_Ws_Trust_Total_Withhld);                                                                          //Natural: MOVE #WS-TRUST-TOTAL-WITHHLD TO #ORGT-NY-TOTAL-WITHHLD
        //*  WRITE TRUST TRAILER
        getWorkFiles().write(4, false, pnd_Org_Ny_Record_Pnd_Org_Ny_Rec1);                                                                                                //Natural: WRITE WORK FILE 4 #ORG-NY-REC1
        pnd_Org_Ny_Record_Pnd_Org_Ny_Rec1.reset();                                                                                                                        //Natural: RESET #ORG-NY-REC1
        pnd_Org_Ny_Record_Pnd_Orgf_Ny_Id.setValue("1F");                                                                                                                  //Natural: ASSIGN #ORGF-NY-ID := '1F'
        pnd_Org_Ny_Record_Pnd_Orgf_Ny_Total_1e.setValue(2);                                                                                                               //Natural: ASSIGN #ORGF-NY-TOTAL-1E := 2
        pnd_Org_Ny_Record_Pnd_Orgf_Ny_Total_1w.compute(new ComputeParameters(false, pnd_Org_Ny_Record_Pnd_Orgf_Ny_Total_1w), pnd_Ws_Pnd_Ws_Tiaa_1w_Cnt.add(pnd_Ws_Pnd_Ws_Trust_1w_Cnt)); //Natural: ASSIGN #ORGF-NY-TOTAL-1W := #WS-TIAA-1W-CNT + #WS-TRUST-1W-CNT
        //*  WRITE GRAND TRAILER
        getWorkFiles().write(4, false, pnd_Org_Ny_Record_Pnd_Org_Ny_Rec1);                                                                                                //Natural: WRITE WORK FILE 4 #ORG-NY-REC1
        //*  PROCESS-TRAILER
    }

    //
}
