/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:38:00 PM
**        * FROM NATURAL PROGRAM : Twrp3600
************************************************************
**        * FILE NAME            : Twrp3600.java
**        * CLASS NAME           : Twrp3600
**        * INSTANCE NAME        : Twrp3600
************************************************************
***********************************************************************
*
* PROGRAM  : TWRP3600
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : CREATES FORMS EXTRACT FOR PAPER STATES.
* CREATED  : 11 / 17 / 2000.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM CREATES EXTRACT OF PAPER STATE FROM THE FORM
*            DATA BASE FILE.
*
*  04/04/17  DY  RESTOW MODULE - PIN EXPANSION PROJECT
*    10/13/2020 - RE-STOW COMPONENT FOR 5498           /* SECURE-ACT
* 12/04/2020 - RE-STOW COMPONENT FOR 5498 IRS REPORTING  2020
***********************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3600 extends BLNatBase
{
    // Data Areas
    private PdaTwratbl2 pdaTwratbl2;
    private PdaTwratbl4 pdaTwratbl4;
    private LdaTwrltb4r ldaTwrltb4r;
    private LdaTwrl3600 ldaTwrl3600;
    private LdaTwrl9705 ldaTwrl9705;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_First_Time_Run_Only;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_Combined_Fed_State;
    private DbsField pnd_Ws_Pnd_Rec_Type;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws__Filler1;
    private DbsField pnd_Ws_Pnd_State_Code;
    private DbsField pnd_Input_Parm;

    private DbsGroup pnd_Input_Parm__R_Field_2;
    private DbsField pnd_Input_Parm_Pnd_In_Form;
    private DbsField pnd_Input_Parm_Pnd_In_Form_Fill;
    private DbsField pnd_Input_Parm_Pnd_In_Tax_Year;
    private DbsField pnd_Input_Parm_Pnd_In_Not_First_Time_Run_Ok;
    private DbsField pnd_Input_Parm_Pnd_In_Input_State;
    private DbsField pnd_Super_8;

    private DbsGroup pnd_Super_8__R_Field_3;
    private DbsField pnd_Super_8_Pnd_Sup_Tax_Year;
    private DbsField pnd_Super_8_Pnd_Sup_Form_Type;
    private DbsField pnd_Super_8_Pnd_Sup_State_Code;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        pdaTwratbl4 = new PdaTwratbl4(localVariables);
        ldaTwrltb4r = new LdaTwrltb4r();
        registerRecord(ldaTwrltb4r);
        registerRecord(ldaTwrltb4r.getVw_tircntl_Rpt());
        ldaTwrl3600 = new LdaTwrl3600();
        registerRecord(ldaTwrl3600);
        ldaTwrl9705 = new LdaTwrl9705();
        registerRecord(ldaTwrl9705);
        registerRecord(ldaTwrl9705.getVw_form_U());

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_First_Time_Run_Only = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_First_Time_Run_Only", "#FIRST-TIME-RUN-ONLY", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Combined_Fed_State = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Combined_Fed_State", "#COMBINED-FED-STATE", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Rec_Type = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Type", "#REC-TYPE", FieldType.STRING, 7);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Rec_Type);
        pnd_Ws__Filler1 = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws__Filler1", "_FILLER1", FieldType.STRING, 4);
        pnd_Ws_Pnd_State_Code = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_State_Code", "#STATE-CODE", FieldType.STRING, 2);
        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 30);

        pnd_Input_Parm__R_Field_2 = localVariables.newGroupInRecord("pnd_Input_Parm__R_Field_2", "REDEFINE", pnd_Input_Parm);
        pnd_Input_Parm_Pnd_In_Form = pnd_Input_Parm__R_Field_2.newFieldInGroup("pnd_Input_Parm_Pnd_In_Form", "#IN-FORM", FieldType.STRING, 4);
        pnd_Input_Parm_Pnd_In_Form_Fill = pnd_Input_Parm__R_Field_2.newFieldInGroup("pnd_Input_Parm_Pnd_In_Form_Fill", "#IN-FORM-FILL", FieldType.STRING, 
            2);
        pnd_Input_Parm_Pnd_In_Tax_Year = pnd_Input_Parm__R_Field_2.newFieldInGroup("pnd_Input_Parm_Pnd_In_Tax_Year", "#IN-TAX-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Input_Parm_Pnd_In_Not_First_Time_Run_Ok = pnd_Input_Parm__R_Field_2.newFieldInGroup("pnd_Input_Parm_Pnd_In_Not_First_Time_Run_Ok", "#IN-NOT-FIRST-TIME-RUN-OK", 
            FieldType.STRING, 1);
        pnd_Input_Parm_Pnd_In_Input_State = pnd_Input_Parm__R_Field_2.newFieldInGroup("pnd_Input_Parm_Pnd_In_Input_State", "#IN-INPUT-STATE", FieldType.STRING, 
            2);
        pnd_Super_8 = localVariables.newFieldInRecord("pnd_Super_8", "#SUPER-8", FieldType.STRING, 8);

        pnd_Super_8__R_Field_3 = localVariables.newGroupInRecord("pnd_Super_8__R_Field_3", "REDEFINE", pnd_Super_8);
        pnd_Super_8_Pnd_Sup_Tax_Year = pnd_Super_8__R_Field_3.newFieldInGroup("pnd_Super_8_Pnd_Sup_Tax_Year", "#SUP-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Super_8_Pnd_Sup_Form_Type = pnd_Super_8__R_Field_3.newFieldInGroup("pnd_Super_8_Pnd_Sup_Form_Type", "#SUP-FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_Super_8_Pnd_Sup_State_Code = pnd_Super_8__R_Field_3.newFieldInGroup("pnd_Super_8_Pnd_Sup_State_Code", "#SUP-STATE-CODE", FieldType.STRING, 
            2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrltb4r.initializeValues();
        ldaTwrl3600.initializeValues();
        ldaTwrl9705.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_Rec_Type.setInitialValue("9ST000");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3600() throws Exception
    {
        super("Twrp3600");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 80 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 37T 'TaxWaRS' 68T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 31T 'State Control Report' 68T 'Report: RPT1' / //
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT-PARMS
        sub_Process_Input_Parms();
        if (condition(Global.isEscape())) {return;}
        pnd_Super_8_Pnd_Sup_Tax_Year.setValue(pnd_Ws_Pnd_Tax_Year);                                                                                                       //Natural: ASSIGN #SUPER-8.#SUP-TAX-YEAR := #WS.#TAX-YEAR
        pnd_Super_8_Pnd_Sup_Form_Type.setValue(1);                                                                                                                        //Natural: ASSIGN #SUPER-8.#SUP-FORM-TYPE := 01
        pnd_Super_8_Pnd_Sup_State_Code.setValue(pnd_Input_Parm_Pnd_In_Input_State);                                                                                       //Natural: ASSIGN #SUPER-8.#SUP-STATE-CODE := #IN-INPUT-STATE
        ldaTwrl9705.getVw_form_U().startDatabaseFind                                                                                                                      //Natural: FIND FORM-U WITH TIRF-SUPERDE-8 = #SUPER-8
        (
        "F_FORM",
        new Wc[] { new Wc("TIRF_SUPERDE_8", "=", pnd_Super_8, WcType.WITH) }
        );
        F_FORM:
        while (condition(ldaTwrl9705.getVw_form_U().readNextRow("F_FORM")))
        {
            ldaTwrl9705.getVw_form_U().setIfNotFoundControlFlag(false);
            if (condition(!(ldaTwrl9705.getForm_U_Tirf_Active_Ind().equals("A"))))                                                                                        //Natural: ACCEPT IF FORM-U.TIRF-ACTIVE-IND = 'A'
            {
                continue;
            }
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO C*TIRF-1099-R-STATE-GRP
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl9705.getForm_U_Count_Casttirf_1099_R_State_Grp())); pnd_Ws_Pnd_I.nadd(1))
            {
                if (condition(ldaTwrl9705.getForm_U_Tirf_State_Code().getValue(pnd_Ws_Pnd_I).equals(pnd_Super_8_Pnd_Sup_State_Code)))                                     //Natural: IF TIRF-STATE-CODE ( #I ) = #SUPER-8.#SUP-STATE-CODE
                {
                    //*  NO RECONCILIATION
                    //*  NO REPORTING
                    if (condition(ldaTwrl9705.getForm_U_Tirf_State_Hardcopy_Ind().getValue(pnd_Ws_Pnd_I).equals(" ") && ldaTwrl9705.getForm_U_Tirf_State_Reporting().getValue(pnd_Ws_Pnd_I).notEquals("Y"))) //Natural: IF TIRF-STATE-HARDCOPY-IND ( #I ) = ' ' AND TIRF-STATE-REPORTING ( #I ) NE 'Y'
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    ldaTwrl3600.getPnd_Twrl3600().reset();                                                                                                                //Natural: RESET #TWRL3600
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Isn().setValue(ldaTwrl9705.getVw_form_U().getAstISN("F_FORM"));                                                  //Natural: ASSIGN #TIRF-ISN := *ISN
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Tax_Year().setValue(ldaTwrl9705.getForm_U_Tirf_Tax_Year());                                                      //Natural: ASSIGN #TIRF-TAX-YEAR := TIRF-TAX-YEAR
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Company_Cde().setValue(ldaTwrl9705.getForm_U_Tirf_Company_Cde());                                                //Natural: ASSIGN #TIRF-COMPANY-CDE := TIRF-COMPANY-CDE
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Tax_Id_Type().setValue(ldaTwrl9705.getForm_U_Tirf_Tax_Id_Type());                                                //Natural: ASSIGN #TIRF-TAX-ID-TYPE := TIRF-TAX-ID-TYPE
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Tin().setValue(ldaTwrl9705.getForm_U_Tirf_Tin());                                                                //Natural: ASSIGN #TIRF-TIN := TIRF-TIN
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Contract_Nbr().setValue(ldaTwrl9705.getForm_U_Tirf_Contract_Nbr());                                              //Natural: ASSIGN #TIRF-CONTRACT-NBR := TIRF-CONTRACT-NBR
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Payee_Cde().setValue(ldaTwrl9705.getForm_U_Tirf_Payee_Cde());                                                    //Natural: ASSIGN #TIRF-PAYEE-CDE := TIRF-PAYEE-CDE
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Part_Last_Nme().setValue(ldaTwrl9705.getForm_U_Tirf_Part_Last_Nme());                                            //Natural: ASSIGN #TIRF-PART-LAST-NME := TIRF-PART-LAST-NME
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Part_First_Nme().setValue(ldaTwrl9705.getForm_U_Tirf_Part_First_Nme());                                          //Natural: ASSIGN #TIRF-PART-FIRST-NME := TIRF-PART-FIRST-NME
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Part_Mddle_Nme().setValue(ldaTwrl9705.getForm_U_Tirf_Part_Mddle_Nme());                                          //Natural: ASSIGN #TIRF-PART-MDDLE-NME := TIRF-PART-MDDLE-NME
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Addr_Ln1().setValue(ldaTwrl9705.getForm_U_Tirf_Addr_Ln1());                                                      //Natural: ASSIGN #TIRF-ADDR-LN1 := TIRF-ADDR-LN1
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Addr_Ln2().setValue(ldaTwrl9705.getForm_U_Tirf_Addr_Ln2());                                                      //Natural: ASSIGN #TIRF-ADDR-LN2 := TIRF-ADDR-LN2
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Addr_Ln3().setValue(ldaTwrl9705.getForm_U_Tirf_Addr_Ln3());                                                      //Natural: ASSIGN #TIRF-ADDR-LN3 := TIRF-ADDR-LN3
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Addr_Ln4().setValue(ldaTwrl9705.getForm_U_Tirf_Addr_Ln4());                                                      //Natural: ASSIGN #TIRF-ADDR-LN4 := TIRF-ADDR-LN4
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Addr_Ln5().setValue(ldaTwrl9705.getForm_U_Tirf_Addr_Ln5());                                                      //Natural: ASSIGN #TIRF-ADDR-LN5 := TIRF-ADDR-LN5
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Addr_Ln6().setValue(ldaTwrl9705.getForm_U_Tirf_Addr_Ln6());                                                      //Natural: ASSIGN #TIRF-ADDR-LN6 := TIRF-ADDR-LN6
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_State_Code().setValue(ldaTwrl9705.getForm_U_Tirf_State_Code().getValue(pnd_Ws_Pnd_I));                           //Natural: ASSIGN #TIRF-STATE-CODE := TIRF-STATE-CODE ( #I )
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_State_Distr().setValue(ldaTwrl9705.getForm_U_Tirf_State_Distr().getValue(pnd_Ws_Pnd_I));                         //Natural: ASSIGN #TIRF-STATE-DISTR := TIRF-STATE-DISTR ( #I )
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_State_Tax_Wthld().setValue(ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld().getValue(pnd_Ws_Pnd_I));                 //Natural: ASSIGN #TIRF-STATE-TAX-WTHLD := TIRF-STATE-TAX-WTHLD ( #I )
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Loc_Code().setValue(ldaTwrl9705.getForm_U_Tirf_Loc_Code().getValue(pnd_Ws_Pnd_I));                               //Natural: ASSIGN #TIRF-LOC-CODE := TIRF-LOC-CODE ( #I )
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Loc_Distr().setValue(ldaTwrl9705.getForm_U_Tirf_Loc_Distr().getValue(pnd_Ws_Pnd_I));                             //Natural: ASSIGN #TIRF-LOC-DISTR := TIRF-LOC-DISTR ( #I )
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Loc_Tax_Wthld().setValue(ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Pnd_I));                     //Natural: ASSIGN #TIRF-LOC-TAX-WTHLD := TIRF-LOC-TAX-WTHLD ( #I )
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_State_Rpt_Ind().setValue(ldaTwrl9705.getForm_U_Tirf_State_Rpt_Ind().getValue(pnd_Ws_Pnd_I));                     //Natural: ASSIGN #TIRF-STATE-RPT-IND := TIRF-STATE-RPT-IND ( #I )
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_State_Hardcopy_Ind().setValue(ldaTwrl9705.getForm_U_Tirf_State_Hardcopy_Ind().getValue(pnd_Ws_Pnd_I));           //Natural: ASSIGN #TIRF-STATE-HARDCOPY-IND := TIRF-STATE-HARDCOPY-IND ( #I )
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Res_Reject_Ind().setValue(ldaTwrl9705.getForm_U_Tirf_Res_Reject_Ind().getValue(pnd_Ws_Pnd_I));                   //Natural: ASSIGN #TIRF-RES-REJECT-IND := TIRF-RES-REJECT-IND ( #I )
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_State_Reporting().setValue(ldaTwrl9705.getForm_U_Tirf_State_Reporting().getValue(pnd_Ws_Pnd_I));                 //Natural: ASSIGN #TIRF-STATE-REPORTING := TIRF-STATE-REPORTING ( #I )
                    ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Res_Irr_Amt().setValue(ldaTwrl9705.getForm_U_Tirf_Res_Irr_Amt().getValue(pnd_Ws_Pnd_I));                         //Natural: ASSIGN #TIRF-RES-IRR-AMT := TIRF-RES-IRR-AMT ( #I )
                    if (condition(ldaTwrl9705.getForm_U_Tirf_Empty_Form().getBoolean()))                                                                                  //Natural: IF TIRF-EMPTY-FORM
                    {
                        ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Empty_Form().setValue("Y");                                                                                  //Natural: ASSIGN #TIRF-EMPTY-FORM := 'Y'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Empty_Form().setValue("N");                                                                                  //Natural: ASSIGN #TIRF-EMPTY-FORM := 'N'
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl9705.getForm_U_Tirf_Sys_Err().getValue("*").equals(5) || ldaTwrl9705.getForm_U_Tirf_Sys_Err().getValue("*").equals(20)           //Natural: IF TIRF-SYS-ERR ( * ) = 5 OR = 20 OR = 21
                        || ldaTwrl9705.getForm_U_Tirf_Sys_Err().getValue("*").equals(21)))
                    {
                        ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Sys_Err().setValue("Y");                                                                                     //Natural: ASSIGN #TIRF-SYS-ERR := 'Y'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Sys_Err().setValue("N");                                                                                     //Natural: ASSIGN #TIRF-SYS-ERR := 'N'
                    }                                                                                                                                                     //Natural: END-IF
                    //*   STATE REPORTABLE
                    //*   NOT CMB
                    if (condition(ldaTwrl9705.getForm_U_Tirf_State_Reporting().getValue(pnd_Ws_Pnd_I).equals("Y") && ! (pnd_Ws_Pnd_Combined_Fed_State.getBoolean())))     //Natural: IF TIRF-STATE-REPORTING ( #I ) = 'Y' AND NOT #COMBINED-FED-STATE
                    {
                        //*   REPRT. HELD
                        if (condition(ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Sys_Err().equals("Y") || ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Res_Reject_Ind().equals("Y")))   //Natural: IF #TIRF-SYS-ERR = 'Y' OR #TIRF-RES-REJECT-IND = 'Y'
                        {
                            ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Sort_Ind().setValue("3");                                                                                //Natural: ASSIGN #TIRF-SORT-IND := '3'
                            ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Process_Ind().setValue("H");                                                                             //Natural: ASSIGN #TIRF-PROCESS-IND := 'H'
                            //*   ORIGINAL
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Sort_Ind().setValue("1");                                                                                //Natural: ASSIGN #TIRF-SORT-IND := '1'
                            ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Process_Ind().setValue("O");                                                                             //Natural: ASSIGN #TIRF-PROCESS-IND := 'O'
                        }                                                                                                                                                 //Natural: END-IF
                        //*   NOT REPORT. STATE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*   RECON. HELD
                        if (condition(ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Sys_Err().equals("Y") || ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Res_Reject_Ind().equals("Y")))   //Natural: IF #TIRF-SYS-ERR = 'Y' OR #TIRF-RES-REJECT-IND = 'Y'
                        {
                            ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Sort_Ind().setValue("3");                                                                                //Natural: ASSIGN #TIRF-SORT-IND := '3'
                            ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Process_Ind().setValue("J");                                                                             //Natural: ASSIGN #TIRF-PROCESS-IND := 'J'
                            //*   RECONCILIATION
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Sort_Ind().setValue("2");                                                                                //Natural: ASSIGN #TIRF-SORT-IND := '2'
                            ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Process_Ind().setValue("X");                                                                             //Natural: ASSIGN #TIRF-PROCESS-IND := 'X'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    getWorkFiles().write(1, false, ldaTwrl3600.getPnd_Twrl3600());                                                                                        //Natural: WRITE WORK FILE 01 #TWRL3600
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F_FORM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F_FORM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //* **********************
        //*  S U B R O U T I N E S
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INPUT-PARMS
        //* ************************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Process_Input_Parms() throws Exception                                                                                                               //Natural: PROCESS-INPUT-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("TWRP3600|sub_Process_Input_Parms");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parm);                                                                                       //Natural: INPUT #INPUT-PARM
                getReports().print(1, "Input Parameter:",pnd_Input_Parm,NEWLINE,"Form...........:",pnd_Input_Parm_Pnd_In_Form,new ColumnSpacing(2),"(not used)",          //Natural: PRINT ( 01 ) 'Input Parameter:' #INPUT-PARM / 'Form...........:' #IN-FORM 2X '(not used)' / 'Tax Year.......:' #IN-TAX-YEAR / 'Not 1st run....:' #IN-NOT-FIRST-TIME-RUN-OK / 'State..........:' #IN-INPUT-STATE
                    NEWLINE,"Tax Year.......:",pnd_Input_Parm_Pnd_In_Tax_Year,NEWLINE,"Not 1st run....:",pnd_Input_Parm_Pnd_In_Not_First_Time_Run_Ok,NEWLINE,
                    "State..........:",pnd_Input_Parm_Pnd_In_Input_State);
                if (condition(pnd_Input_Parm_Pnd_In_Tax_Year.equals(getZero())))                                                                                          //Natural: IF #IN-TAX-YEAR = 0
                {
                    pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), Global.getDATN().divide(10000).subtract(1));                           //Natural: ASSIGN #WS.#TAX-YEAR := *DATN / 10000 - 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_Tax_Year.setValue(pnd_Input_Parm_Pnd_In_Tax_Year);                                                                                         //Natural: ASSIGN #WS.#TAX-YEAR := #IN-TAX-YEAR
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Parm_Pnd_In_Not_First_Time_Run_Ok.equals("Y")))                                                                                   //Natural: IF #IN-NOT-FIRST-TIME-RUN-OK = 'Y'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_First_Time_Run_Only.setValue(true);                                                                                                        //Natural: ASSIGN #WS.#FIRST-TIME-RUN-ONLY := TRUE
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"Information Used for this run:",NEWLINE,new TabSetting(23),"Tax Year...........:",pnd_Ws_Pnd_Tax_Year,  //Natural: WRITE ( 1 ) 7T 'Information Used for this run:' / 23T 'Tax Year...........:' #WS.#TAX-YEAR ( SG = OFF ) / 23T 'First time run only:' #WS.#FIRST-TIME-RUN-ONLY ( EM = N/Y )
                    new SignPosition (false),NEWLINE,new TabSetting(23),"First time run only:",pnd_Ws_Pnd_First_Time_Run_Only, new ReportEditMask ("N/Y"));
                if (Global.isEscape()) return;
                pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                     //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
                pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                                     //Natural: ASSIGN TWRATBL2.#TAX-YEAR := #WS.#TAX-YEAR
                pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().setValue(true);                                                                                                   //Natural: ASSIGN TWRATBL2.#ABEND-IND := TWRATBL2.#DISPLAY-IND := TRUE
                pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().setValue(true);
                pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue("0");                                                                                                    //Natural: ASSIGN TWRATBL2.#STATE-CDE := '0'
                setValueToSubstring(pnd_Input_Parm_Pnd_In_Input_State,pdaTwratbl2.getTwratbl2_Pnd_State_Cde(),2,2);                                                       //Natural: MOVE #IN-INPUT-STATE TO SUBSTR ( TWRATBL2.#STATE-CDE,2,2 )
                DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
                    new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(23),"State:.............:",pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code(),                  //Natural: WRITE ( 01 ) 23T 'State:.............:' TIRCNTL-STATE-ALPHA-CODE '-' TIRCNTL-STATE-FULL-NAME
                    "-",pdaTwratbl2.getTwratbl2_Tircntl_State_Full_Name());
                if (Global.isEscape()) return;
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(false);                                                                                              //Natural: ASSIGN #TWRATBL4.#ABEND-IND := #TWRATBL4.#DISPLAY-IND := FALSE
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Display_Ind().setValue(false);
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                                 //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #WS.#TAX-YEAR
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(0);                                                                                                   //Natural: ASSIGN #TWRATBL4.#FORM-IND := 0
                pnd_Ws_Pnd_State_Code.setValue(pnd_Input_Parm_Pnd_In_Input_State);                                                                                        //Natural: ASSIGN #WS.#STATE-CODE := #IN-INPUT-STATE
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Rec_Type().setValue(pnd_Ws_Pnd_Rec_Type);                                                                                 //Natural: ASSIGN #TWRATBL4.#REC-TYPE := #WS.#REC-TYPE
                if (condition(pdaTwratbl2.getTwratbl2_Tircntl_Comb_Fed_Ind().notEquals(" ")))                                                                             //Natural: IF TWRATBL2.TIRCNTL-COMB-FED-IND NE ' '
                {
                    pnd_Ws_Pnd_Combined_Fed_State.setValue(true);                                                                                                         //Natural: ASSIGN #COMBINED-FED-STATE := TRUE
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNTB4R' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                if (condition(pdaTwratbl4.getPnd_Twratbl4_Pnd_Ret_Code().getBoolean()))                                                                                   //Natural: IF #TWRATBL4.#RET-CODE
                {
                    if (condition(pnd_Ws_Pnd_First_Time_Run_Only.getBoolean()))                                                                                           //Natural: IF #WS.#FIRST-TIME-RUN-ONLY
                    {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                        sub_Error_Display_Start();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"State Original Reporting already run",new TabSetting(77),                    //Natural: WRITE ( 1 ) '***' 25T 'State Original Reporting already run' 77T '***'
                            "***");
                        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                        sub_Error_Display_End();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                        DbsUtil.terminate(102);  if (true) return;                                                                                                        //Natural: TERMINATE 102
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        DbsUtil.callnat(Twrntb4d.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"),            //Natural: CALLNAT 'TWRNTB4D' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
                            pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(), new AttributeParameter("M"));
                        if (condition(Global.isEscape())) return;
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=80 ZP=ON");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(37),"TaxWaRS",new TabSetting(68),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(31),"State Control Report",new TabSetting(68),"Report: RPT1",NEWLINE,NEWLINE,NEWLINE);
    }
}
