/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:32:24 PM
**        * FROM NATURAL PROGRAM : Twrp0996
************************************************************
**        * FILE NAME            : Twrp0996.java
**        * CLASS NAME           : Twrp0996
**        * INSTANCE NAME        : Twrp0996
************************************************************
* PROGRAM  : TWRP0996
* FUNCTION : PRINTS DAILY ONLINE MAINTENANCE DETAIL REPORT BY  COMPANY
*                    AND TIN ( SORTED BY COMPANY,TIN,TAX-CITIZENSHIP)
* AUTHOR   : EDITH  12/9/99
* 09/17/04 RM TOPS RELEASE 3 CHANGES
*             ADD NEW COMPANY CODE 'X' FOR TRUST COMPANY AND NEW SOURCE
*             CODES 'PL', 'NL'.
* 06/06/06: NAVISYS CHANGES   R. MA
*           SOURCE CODE 'NV' HAD BEEN ADDED TO ALL CONTROL REPORTS.
*           THIS PROGRAM IS ONE OF THE CONTROL REPORTS BUT DON't need
*           ANY CODE CHANGE THIS TIME.
* 10/18/11  JB NON-ERISA TDA - ADD COMPANY CODE 'F' SCAN JB1011
* 11/20/14  O SOTTO FATCA CHANGES MARKED 11/2014.
********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0996 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0901 ldaTwrl0901;
    private LdaTwrl0902 ldaTwrl0902;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_Area;
    private DbsField pnd_Work_Area_Pnd_Rec_Read;
    private DbsField pnd_Work_Area_Pnd_Process_Rec;

    private DbsGroup pnd_Work_Area_Pnd_Var_File1;
    private DbsField pnd_Work_Area_Pnd_Tax_Year;

    private DbsGroup pnd_Work_Area__R_Field_1;
    private DbsField pnd_Work_Area_Pnd_Tax_Year_A;
    private DbsField pnd_Work_Area_Pnd_Start_Date;
    private DbsField pnd_Work_Area_Pnd_End_Date;
    private DbsField pnd_Work_Area_Pnd_Freq;
    private DbsField pnd_Work_Area_Pnd_Sv_Company;
    private DbsField pnd_Work_Area_Pnd_Sv_Source;
    private DbsField pnd_Work_Area_Pnd_Sv_Payset;
    private DbsField pnd_Work_Area_Pnd_Print_Com;
    private DbsField pnd_Work_Area_Pnd_Trans_Desc;
    private DbsField pnd_Work_Area_Pnd_Old_Sc;
    private DbsField pnd_Work_Area_Pnd_Old_Co;
    private DbsField pnd_Work_Area_Pnd_Contract_Payee;
    private DbsField pnd_Work_Area_Pnd_I;
    private DbsField pnd_Work_Area_Pnd_Prt_Source_Code;
    private DbsField pnd_Work_Area_Pnd_New_Record;
    private DbsField pnd_Work_Area_Pnd_Temp_Pr_Wthld;
    private DbsField pnd_Work_Area_Pnd_Temp_State_Wthld;
    private DbsField pnd_Work_Area_Pnd_Fatca;
    private DbsField pnd_Work_Area_Pnd_Hdg1;

    private DbsGroup pnd_Totals;
    private DbsField pnd_Totals_Pnd_Total_Cnt;
    private DbsField pnd_Totals_Pnd_Total_Grs;
    private DbsField pnd_Totals_Pnd_Total_Ivc;
    private DbsField pnd_Totals_Pnd_Total_Int;
    private DbsField pnd_Totals_Pnd_Total_Fed;
    private DbsField pnd_Totals_Pnd_Total_Nra;
    private DbsField pnd_Totals_Pnd_Total_Sta;
    private DbsField pnd_Totals_Pnd_Total_Lcl;
    private DbsField pnd_Totals_Pnd_Total_Can;
    private DbsField pnd_Totals_Pnd_Total_Pr;
    private DbsField pnd_Cmpy_Cnt;

    private DbsGroup pnd_Company_Totals;
    private DbsField pnd_Company_Totals_Pnd_Tot_Cmpny;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Cnt;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Grs;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Ivc;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Int;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Fed;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Nra;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Sta;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Lcl;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Can;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Pr;
    private DbsField pnd_Tot_Ndx;
    private DbsField pnd_Dashes;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0901 = new LdaTwrl0901();
        registerRecord(ldaTwrl0901);
        ldaTwrl0902 = new LdaTwrl0902();
        registerRecord(ldaTwrl0902);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Work_Area = localVariables.newGroupInRecord("pnd_Work_Area", "#WORK-AREA");
        pnd_Work_Area_Pnd_Rec_Read = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Rec_Read", "#REC-READ", FieldType.NUMERIC, 7);
        pnd_Work_Area_Pnd_Process_Rec = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Process_Rec", "#PROCESS-REC", FieldType.NUMERIC, 7);

        pnd_Work_Area_Pnd_Var_File1 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area_Pnd_Var_File1", "#VAR-FILE1");
        pnd_Work_Area_Pnd_Tax_Year = pnd_Work_Area_Pnd_Var_File1.newFieldInGroup("pnd_Work_Area_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);

        pnd_Work_Area__R_Field_1 = pnd_Work_Area_Pnd_Var_File1.newGroupInGroup("pnd_Work_Area__R_Field_1", "REDEFINE", pnd_Work_Area_Pnd_Tax_Year);
        pnd_Work_Area_Pnd_Tax_Year_A = pnd_Work_Area__R_Field_1.newFieldInGroup("pnd_Work_Area_Pnd_Tax_Year_A", "#TAX-YEAR-A", FieldType.STRING, 4);
        pnd_Work_Area_Pnd_Start_Date = pnd_Work_Area_Pnd_Var_File1.newFieldInGroup("pnd_Work_Area_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);
        pnd_Work_Area_Pnd_End_Date = pnd_Work_Area_Pnd_Var_File1.newFieldInGroup("pnd_Work_Area_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);
        pnd_Work_Area_Pnd_Freq = pnd_Work_Area_Pnd_Var_File1.newFieldInGroup("pnd_Work_Area_Pnd_Freq", "#FREQ", FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Sv_Company = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Sv_Company", "#SV-COMPANY", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Sv_Source = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Sv_Source", "#SV-SOURCE", FieldType.STRING, 4);
        pnd_Work_Area_Pnd_Sv_Payset = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Sv_Payset", "#SV-PAYSET", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Print_Com = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Print_Com", "#PRINT-COM", FieldType.STRING, 4);
        pnd_Work_Area_Pnd_Trans_Desc = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Trans_Desc", "#TRANS-DESC", FieldType.STRING, 19);
        pnd_Work_Area_Pnd_Old_Sc = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Old_Sc", "#OLD-SC", FieldType.STRING, 4);
        pnd_Work_Area_Pnd_Old_Co = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Old_Co", "#OLD-CO", FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Contract_Payee = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Contract_Payee", "#CONTRACT-PAYEE", FieldType.STRING, 11);
        pnd_Work_Area_Pnd_I = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_I", "#I", FieldType.NUMERIC, 1);
        pnd_Work_Area_Pnd_Prt_Source_Code = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Prt_Source_Code", "#PRT-SOURCE-CODE", FieldType.STRING, 4);
        pnd_Work_Area_Pnd_New_Record = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_New_Record", "#NEW-RECORD", FieldType.BOOLEAN, 1);
        pnd_Work_Area_Pnd_Temp_Pr_Wthld = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Temp_Pr_Wthld", "#TEMP-PR-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Area_Pnd_Temp_State_Wthld = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Temp_State_Wthld", "#TEMP-STATE-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Area_Pnd_Fatca = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Fatca", "#FATCA", FieldType.BOOLEAN, 1);
        pnd_Work_Area_Pnd_Hdg1 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Hdg1", "#HDG1", FieldType.STRING, 13);

        pnd_Totals = localVariables.newGroupInRecord("pnd_Totals", "#TOTALS");
        pnd_Totals_Pnd_Total_Cnt = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Cnt", "#TOTAL-CNT", FieldType.NUMERIC, 7);
        pnd_Totals_Pnd_Total_Grs = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Grs", "#TOTAL-GRS", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Totals_Pnd_Total_Ivc = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Ivc", "#TOTAL-IVC", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Total_Int = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Int", "#TOTAL-INT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Total_Fed = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Fed", "#TOTAL-FED", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Total_Nra = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Nra", "#TOTAL-NRA", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Total_Sta = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Sta", "#TOTAL-STA", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Total_Lcl = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Lcl", "#TOTAL-LCL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Total_Can = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Can", "#TOTAL-CAN", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Total_Pr = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Pr", "#TOTAL-PR", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cmpy_Cnt = localVariables.newFieldInRecord("pnd_Cmpy_Cnt", "#CMPY-CNT", FieldType.PACKED_DECIMAL, 3);

        pnd_Company_Totals = localVariables.newGroupArrayInRecord("pnd_Company_Totals", "#COMPANY-TOTALS", new DbsArrayController(1, 7));
        pnd_Company_Totals_Pnd_Tot_Cmpny = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Cmpny", "#TOT-CMPNY", FieldType.STRING, 4);
        pnd_Company_Totals_Pnd_Tot_Total_Cnt = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Cnt", "#TOT-TOTAL-CNT", FieldType.NUMERIC, 
            7);
        pnd_Company_Totals_Pnd_Tot_Total_Grs = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Grs", "#TOT-TOTAL-GRS", FieldType.PACKED_DECIMAL, 
            14, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Ivc = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Ivc", "#TOT-TOTAL-IVC", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Int = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Int", "#TOT-TOTAL-INT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Fed = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Fed", "#TOT-TOTAL-FED", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Nra = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Nra", "#TOT-TOTAL-NRA", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Sta = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Sta", "#TOT-TOTAL-STA", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Lcl = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Lcl", "#TOT-TOTAL-LCL", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Can = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Can", "#TOT-TOTAL-CAN", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Pr = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Pr", "#TOT-TOTAL-PR", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Tot_Ndx = localVariables.newFieldInRecord("pnd_Tot_Ndx", "#TOT-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Dashes = localVariables.newFieldInRecord("pnd_Dashes", "#DASHES", FieldType.STRING, 131);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0901.initializeValues();
        ldaTwrl0902.initializeValues();

        localVariables.reset();
        pnd_Work_Area_Pnd_New_Record.setInitialValue(true);
        pnd_Work_Area_Pnd_Fatca.setInitialValue(false);
        pnd_Work_Area_Pnd_Hdg1.setInitialValue("NON-FATCA TAX");
        pnd_Cmpy_Cnt.setInitialValue(7);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0996() throws Exception
    {
        super("Twrp0996");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  11/2014
        pnd_Dashes.moveAll("=");                                                                                                                                          //Natural: MOVE ALL '=' TO #DASHES
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #FLAT-FILE1
        while (condition(getWorkFiles().read(1, ldaTwrl0901.getPnd_Flat_File1())))
        {
            pnd_Work_Area_Pnd_Tax_Year.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Tax_Year());                                                                        //Natural: ASSIGN #TAX-YEAR := #FF1-TAX-YEAR
            pnd_Work_Area_Pnd_Start_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Start_Date());                                                                    //Natural: ASSIGN #START-DATE := #FF1-START-DATE
            pnd_Work_Area_Pnd_End_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_End_Date());                                                                        //Natural: ASSIGN #END-DATE := #FF1-END-DATE
            pnd_Work_Area_Pnd_Freq.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Frequency());                                                                           //Natural: ASSIGN #FREQ := #FF1-FREQUENCY
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* *READ WORK FILE 2  RECORD #FLAT-FILE3 /* 11/2014
        //*  11/2014
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 2 #FLAT-FILE3
        while (condition(getWorkFiles().read(2, ldaTwrl0902.getPnd_Flat_File3())))
        {
            pnd_Work_Area_Pnd_Rec_Read.nadd(1);                                                                                                                           //Natural: ADD 1 TO #REC-READ
            //*  9/17/04 RM
            //*  11/2014
            if (condition(!(((((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("OL") || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("ML"))  //Natural: ACCEPT IF ( SUBSTRING ( #FF3-SOURCE-CODE,3,2 ) = 'OL' OR = 'ML' OR = 'PL' OR = 'NL' ) AND #FF3-TWRPYMNT-FATCA-IND NE 'Y'
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("PL")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("NL")) 
                && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Twrpymnt_Fatca_Ind().notEquals("Y")))))
            {
                continue;
            }
            pnd_Work_Area_Pnd_Process_Rec.nadd(1);                                                                                                                        //Natural: ADD 1 TO #PROCESS-REC
            if (condition(pnd_Work_Area_Pnd_New_Record.getBoolean()))                                                                                                     //Natural: IF #NEW-RECORD
            {
                pnd_Work_Area_Pnd_New_Record.setValue(false);                                                                                                             //Natural: ASSIGN #NEW-RECORD := FALSE
                pnd_Work_Area_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                              //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code().equals(pnd_Work_Area_Pnd_Sv_Company)))                                                     //Natural: IF #FF3-COMPANY-CODE = #SV-COMPANY
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM BREAK-COMPANY
                sub_Break_Company();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL
            sub_Print_Detail();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: AT END OF DATA;//Natural: END-WORK
        READWORK02_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            //*  11/2014
            if (condition(pnd_Work_Area_Pnd_Process_Rec.equals(getZero())))                                                                                               //Natural: IF #PROCESS-REC = 0
            {
                //*      WRITE (1) 40T '*****  NO RECORDS PROCESSED ***** '
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM BREAK-COMPANY
                sub_Break_Company();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            getReports().eject(1, true);                                                                                                                                  //Natural: EJECT ( 1 )
            getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM()," - CONTROL TOTAL",NEWLINE,NEWLINE,NEWLINE,"RUNDATE    : ",Global.getDATX(),                   //Natural: WRITE ( 1 ) *PROGRAM ' - CONTROL TOTAL' // / 'RUNDATE    : ' *DATX ( EM = MM/DD/YYYY ) / 'RUNTIME    : ' *TIMX / 'TAX YEAR   : ' #TAX-YEAR / 'DATE RANGE : ' #START-DATE 'THRU' #END-DATE /// '1) NO. OF RECORDS READ      : ' #REC-READ / '2) NO. OF RECORDS PROCESSED : ' #PROCESS-REC
                new ReportEditMask ("MM/DD/YYYY"),NEWLINE,"RUNTIME    : ",Global.getTIMX(),NEWLINE,"TAX YEAR   : ",pnd_Work_Area_Pnd_Tax_Year,NEWLINE,"DATE RANGE : ",
                pnd_Work_Area_Pnd_Start_Date,"THRU",pnd_Work_Area_Pnd_End_Date,NEWLINE,NEWLINE,NEWLINE,"1) NO. OF RECORDS READ      : ",pnd_Work_Area_Pnd_Rec_Read,
                NEWLINE,"2) NO. OF RECORDS PROCESSED : ",pnd_Work_Area_Pnd_Process_Rec);
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //*  11/2014 START
        if (condition(pnd_Work_Area_Pnd_Process_Rec.equals(getZero())))                                                                                                   //Natural: IF #PROCESS-REC = 0
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(40),"*****  NO RECORDS PROCESSED ***** ");                                                          //Natural: WRITE ( 1 ) 40T '*****  NO RECORDS PROCESSED ***** '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Work_Area.resetInitial();                                                                                                                                     //Natural: RESET INITIAL #WORK-AREA #TOTALS
        pnd_Totals.resetInitial();
        getWorkFiles().close(1);                                                                                                                                          //Natural: CLOSE WORK FILE 1
        getWorkFiles().close(2);                                                                                                                                          //Natural: CLOSE WORK FILE 2
        pnd_Work_Area_Pnd_Fatca.setValue(true);                                                                                                                           //Natural: ASSIGN #FATCA := TRUE
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        READWORK03:                                                                                                                                                       //Natural: READ WORK FILE 1 #FLAT-FILE1
        while (condition(getWorkFiles().read(1, ldaTwrl0901.getPnd_Flat_File1())))
        {
            pnd_Work_Area_Pnd_Tax_Year.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Tax_Year());                                                                        //Natural: ASSIGN #TAX-YEAR := #FF1-TAX-YEAR
            pnd_Work_Area_Pnd_Start_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Start_Date());                                                                    //Natural: ASSIGN #START-DATE := #FF1-START-DATE
            pnd_Work_Area_Pnd_End_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_End_Date());                                                                        //Natural: ASSIGN #END-DATE := #FF1-END-DATE
            pnd_Work_Area_Pnd_Freq.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Frequency());                                                                           //Natural: ASSIGN #FREQ := #FF1-FREQUENCY
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK03_Exit:
        if (Global.isEscape()) return;
        READWORK04:                                                                                                                                                       //Natural: READ WORK FILE 2 #FLAT-FILE3
        while (condition(getWorkFiles().read(2, ldaTwrl0902.getPnd_Flat_File3())))
        {
            pnd_Work_Area_Pnd_Rec_Read.nadd(1);                                                                                                                           //Natural: ADD 1 TO #REC-READ
            //*  9/17/04 RM
            if (condition(!(((((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("OL") || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("ML"))  //Natural: ACCEPT IF ( SUBSTRING ( #FF3-SOURCE-CODE,3,2 ) = 'OL' OR = 'ML' OR = 'PL' OR = 'NL' ) AND #FF3-TWRPYMNT-FATCA-IND EQ 'Y'
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("PL")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("NL")) 
                && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Twrpymnt_Fatca_Ind().equals("Y")))))
            {
                continue;
            }
            pnd_Work_Area_Pnd_Process_Rec.nadd(1);                                                                                                                        //Natural: ADD 1 TO #PROCESS-REC
            if (condition(pnd_Work_Area_Pnd_New_Record.getBoolean()))                                                                                                     //Natural: IF #NEW-RECORD
            {
                pnd_Work_Area_Pnd_New_Record.setValue(false);                                                                                                             //Natural: ASSIGN #NEW-RECORD := FALSE
                pnd_Work_Area_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                              //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code().equals(pnd_Work_Area_Pnd_Sv_Company)))                                                     //Natural: IF #FF3-COMPANY-CODE = #SV-COMPANY
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM BREAK-COMPANY
                sub_Break_Company();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL
            sub_Print_Detail();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: AT END OF DATA;//Natural: END-WORK
        READWORK04_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            if (condition(pnd_Work_Area_Pnd_Process_Rec.equals(getZero())))                                                                                               //Natural: IF #PROCESS-REC = 0
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM BREAK-COMPANY
                sub_Break_Company();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            getReports().eject(1, true);                                                                                                                                  //Natural: EJECT ( 1 )
            getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM()," - CONTROL TOTAL",NEWLINE,NEWLINE,NEWLINE,"RUNDATE    : ",Global.getDATX(),                   //Natural: WRITE ( 1 ) *PROGRAM ' - CONTROL TOTAL' // / 'RUNDATE    : ' *DATX ( EM = MM/DD/YYYY ) / 'RUNTIME    : ' *TIMX / 'TAX YEAR   : ' #TAX-YEAR / 'DATE RANGE : ' #START-DATE 'THRU' #END-DATE /// '1) NO. OF RECORDS READ      : ' #REC-READ / '2) NO. OF RECORDS PROCESSED : ' #PROCESS-REC
                new ReportEditMask ("MM/DD/YYYY"),NEWLINE,"RUNTIME    : ",Global.getTIMX(),NEWLINE,"TAX YEAR   : ",pnd_Work_Area_Pnd_Tax_Year,NEWLINE,"DATE RANGE : ",
                pnd_Work_Area_Pnd_Start_Date,"THRU",pnd_Work_Area_Pnd_End_Date,NEWLINE,NEWLINE,NEWLINE,"1) NO. OF RECORDS READ      : ",pnd_Work_Area_Pnd_Rec_Read,
                NEWLINE,"2) NO. OF RECORDS PROCESSED : ",pnd_Work_Area_Pnd_Process_Rec);
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        if (condition(pnd_Work_Area_Pnd_Process_Rec.equals(getZero())))                                                                                                   //Natural: IF #PROCESS-REC = 0
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(40),"*****  NO RECORDS PROCESSED ***** ");                                                          //Natural: WRITE ( 1 ) 40T '*****  NO RECORDS PROCESSED ***** '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-COMPANY-TOTALS
        sub_Print_Company_Totals();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************************
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CO-DESC
        //*  NONE IGNORE
    }
    private void sub_Break_Company() throws Exception                                                                                                                     //Natural: BREAK-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        //*  11/2014 START
                                                                                                                                                                          //Natural: PERFORM CO-DESC
        sub_Co_Desc();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Company_Totals_Pnd_Tot_Cmpny.getValue("*").equals(pnd_Work_Area_Pnd_Print_Com)))                                                                //Natural: IF #TOT-CMPNY ( * ) = #PRINT-COM
        {
            DbsUtil.examine(new ExamineSource(pnd_Company_Totals_Pnd_Tot_Cmpny.getValue("*")), new ExamineSearch(pnd_Work_Area_Pnd_Print_Com), new ExamineGivingIndex(pnd_Tot_Ndx)); //Natural: EXAMINE #TOT-CMPNY ( * ) FOR #PRINT-COM GIVING INDEX #TOT-NDX
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR01:                                                                                                                                                        //Natural: FOR #TOT-NDX 1 #CMPY-CNT
            for (pnd_Tot_Ndx.setValue(1); condition(pnd_Tot_Ndx.lessOrEqual(pnd_Cmpy_Cnt)); pnd_Tot_Ndx.nadd(1))
            {
                if (condition(pnd_Company_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx).equals(" ")))                                                                        //Natural: IF #TOT-CMPNY ( #TOT-NDX ) = ' '
                {
                    pnd_Company_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx).setValue(pnd_Work_Area_Pnd_Print_Com);                                                         //Natural: ASSIGN #TOT-CMPNY ( #TOT-NDX ) := #PRINT-COM
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Company_Totals_Pnd_Tot_Total_Cnt.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Cnt);                                                                        //Natural: ASSIGN #TOT-TOTAL-CNT ( #TOT-NDX ) := #TOT-TOTAL-CNT ( #TOT-NDX ) + #TOTAL-CNT
        pnd_Company_Totals_Pnd_Tot_Total_Grs.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Grs);                                                                        //Natural: ASSIGN #TOT-TOTAL-GRS ( #TOT-NDX ) := #TOT-TOTAL-GRS ( #TOT-NDX ) + #TOTAL-GRS
        pnd_Company_Totals_Pnd_Tot_Total_Ivc.getValue(pnd_Tot_Ndx).compute(new ComputeParameters(false, pnd_Company_Totals_Pnd_Tot_Total_Ivc.getValue(pnd_Tot_Ndx)),      //Natural: ASSIGN #TOT-TOTAL-IVC ( #TOT-NDX ) := #TOT-TOTAL-IVC ( #TOT-NDX ) + #TOTAL-IVC + #TOTAL-INT
            pnd_Company_Totals_Pnd_Tot_Total_Ivc.getValue(pnd_Tot_Ndx).add(pnd_Totals_Pnd_Total_Ivc).add(pnd_Totals_Pnd_Total_Int));
        pnd_Company_Totals_Pnd_Tot_Total_Fed.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Fed);                                                                        //Natural: ASSIGN #TOT-TOTAL-FED ( #TOT-NDX ) := #TOT-TOTAL-FED ( #TOT-NDX ) + #TOTAL-FED
        pnd_Company_Totals_Pnd_Tot_Total_Nra.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Nra);                                                                        //Natural: ASSIGN #TOT-TOTAL-NRA ( #TOT-NDX ) := #TOT-TOTAL-NRA ( #TOT-NDX ) + #TOTAL-NRA
        pnd_Company_Totals_Pnd_Tot_Total_Sta.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Sta);                                                                        //Natural: ASSIGN #TOT-TOTAL-STA ( #TOT-NDX ) := #TOT-TOTAL-STA ( #TOT-NDX ) + #TOTAL-STA
        pnd_Company_Totals_Pnd_Tot_Total_Lcl.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Lcl);                                                                        //Natural: ASSIGN #TOT-TOTAL-LCL ( #TOT-NDX ) := #TOT-TOTAL-LCL ( #TOT-NDX ) + #TOTAL-LCL
        pnd_Company_Totals_Pnd_Tot_Total_Can.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Can);                                                                        //Natural: ASSIGN #TOT-TOTAL-CAN ( #TOT-NDX ) := #TOT-TOTAL-CAN ( #TOT-NDX ) + #TOTAL-CAN
        pnd_Company_Totals_Pnd_Tot_Total_Pr.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Pr);                                                                          //Natural: ASSIGN #TOT-TOTAL-PR ( #TOT-NDX ) := #TOT-TOTAL-PR ( #TOT-NDX ) + #TOTAL-PR
        //*  11/2014 END
        getReports().write(1, ReportOption.NOTITLE,"GRANDTOTAL (",pnd_Work_Area_Pnd_Print_Com,")",new TabSetting(23),pnd_Totals_Pnd_Total_Cnt, new ReportEditMask         //Natural: WRITE ( 1 ) 'GRANDTOTAL (' #PRINT-COM ')' 23T #TOTAL-CNT 37T #TOTAL-GRS 58T #TOTAL-IVC 75T #TOTAL-FED 92T #TOTAL-NRA 109T #TOTAL-STA / 58T #TOTAL-LCL 75T #TOTAL-CAN 92T #TOTAL-PR 109T #TOTAL-INT
            ("Z,ZZZ,ZZ9"),new TabSetting(37),pnd_Totals_Pnd_Total_Grs, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(58),pnd_Totals_Pnd_Total_Ivc, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(75),pnd_Totals_Pnd_Total_Fed, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(92),pnd_Totals_Pnd_Total_Nra, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(109),pnd_Totals_Pnd_Total_Sta, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(58),pnd_Totals_Pnd_Total_Lcl, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(75),pnd_Totals_Pnd_Total_Can, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(92),pnd_Totals_Pnd_Total_Pr, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(109),pnd_Totals_Pnd_Total_Int, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
        pnd_Totals.reset();                                                                                                                                               //Natural: RESET #TOTALS
        pnd_Work_Area_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                                      //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
    }
    private void sub_Print_Company_Totals() throws Exception                                                                                                              //Natural: PRINT-COMPANY-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(1, ReportOption.NOTITLE,pnd_Dashes);                                                                                                           //Natural: WRITE ( 1 ) #DASHES
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #TOT-NDX 1 #CMPY-CNT
        for (pnd_Tot_Ndx.setValue(1); condition(pnd_Tot_Ndx.lessOrEqual(pnd_Cmpy_Cnt)); pnd_Tot_Ndx.nadd(1))
        {
            if (condition(pnd_Company_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx).equals(" ")))                                                                            //Natural: IF #TOT-CMPNY ( #TOT-NDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,"OVERALLL TOTAL",pnd_Company_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx),new TabSetting(23),pnd_Company_Totals_Pnd_Tot_Total_Cnt.getValue(pnd_Tot_Ndx),  //Natural: WRITE ( 1 ) 'OVERALLL TOTAL' #TOT-CMPNY ( #TOT-NDX ) 23T #TOT-TOTAL-CNT ( #TOT-NDX ) 37T #TOT-TOTAL-GRS ( #TOT-NDX ) 58T #TOT-TOTAL-IVC ( #TOT-NDX ) 75T #TOT-TOTAL-FED ( #TOT-NDX ) 92T #TOT-TOTAL-NRA ( #TOT-NDX ) 109T #TOT-TOTAL-STA ( #TOT-NDX ) / 58T #TOT-TOTAL-LCL ( #TOT-NDX ) 75T #TOT-TOTAL-CAN ( #TOT-NDX ) 92T #TOT-TOTAL-PR ( #TOT-NDX ) 109T #TOT-TOTAL-INT ( #TOT-NDX )
                new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(37),pnd_Company_Totals_Pnd_Tot_Total_Grs.getValue(pnd_Tot_Ndx), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9.99-"),new 
                TabSetting(58),pnd_Company_Totals_Pnd_Tot_Total_Ivc.getValue(pnd_Tot_Ndx), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(75),pnd_Company_Totals_Pnd_Tot_Total_Fed.getValue(pnd_Tot_Ndx), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(92),pnd_Company_Totals_Pnd_Tot_Total_Nra.getValue(pnd_Tot_Ndx), new ReportEditMask 
                ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(109),pnd_Company_Totals_Pnd_Tot_Total_Sta.getValue(pnd_Tot_Ndx), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new 
                TabSetting(58),pnd_Company_Totals_Pnd_Tot_Total_Lcl.getValue(pnd_Tot_Ndx), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(75),pnd_Company_Totals_Pnd_Tot_Total_Can.getValue(pnd_Tot_Ndx), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(92),pnd_Company_Totals_Pnd_Tot_Total_Pr.getValue(pnd_Tot_Ndx), new ReportEditMask 
                ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(109),pnd_Company_Totals_Pnd_Tot_Total_Int.getValue(pnd_Tot_Ndx), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Print_Detail() throws Exception                                                                                                                      //Natural: PRINT-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //*  9/17/04  RM
        short decideConditionsMet284 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #FF3-SOURCE-CODE;//Natural: VALUE 'ZZOL'
        if (condition((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().equals("ZZOL"))))
        {
            decideConditionsMet284++;
            pnd_Work_Area_Pnd_Prt_Source_Code.setValue("MLOL");                                                                                                           //Natural: ASSIGN #PRT-SOURCE-CODE := 'MLOL'
        }                                                                                                                                                                 //Natural: VALUE 'ZZML'
        else if (condition((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().equals("ZZML"))))
        {
            decideConditionsMet284++;
            pnd_Work_Area_Pnd_Prt_Source_Code.setValue("  ML");                                                                                                           //Natural: ASSIGN #PRT-SOURCE-CODE := '  ML'
        }                                                                                                                                                                 //Natural: VALUE 'YYPL'
        else if (condition((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().equals("YYPL"))))
        {
            decideConditionsMet284++;
            pnd_Work_Area_Pnd_Prt_Source_Code.setValue("NLPL");                                                                                                           //Natural: ASSIGN #PRT-SOURCE-CODE := 'NLPL'
        }                                                                                                                                                                 //Natural: VALUE 'YYNL'
        else if (condition((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().equals("YYNL"))))
        {
            decideConditionsMet284++;
            pnd_Work_Area_Pnd_Prt_Source_Code.setValue("  NL");                                                                                                           //Natural: ASSIGN #PRT-SOURCE-CODE := '  NL'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Work_Area_Pnd_Prt_Source_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                              //Natural: ASSIGN #PRT-SOURCE-CODE := #FF3-SOURCE-CODE
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Work_Area_Pnd_Temp_Pr_Wthld.reset();                                                                                                                          //Natural: RESET #TEMP-PR-WTHLD #TEMP-STATE-WTHLD
        pnd_Work_Area_Pnd_Temp_State_Wthld.reset();
        if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("PR") || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("RQ")          //Natural: IF #FF3-RESIDENCY-CODE = 'PR' OR = 'RQ' OR = '42'
            || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("42")))
        {
            pnd_Work_Area_Pnd_Temp_Pr_Wthld.nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld());                                                                    //Natural: ADD #FF3-STATE-WTHLD TO #TEMP-PR-WTHLD
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Work_Area_Pnd_Temp_State_Wthld.nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld());                                                                 //Natural: ADD #FF3-STATE-WTHLD TO #TEMP-STATE-WTHLD
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Work_Area_Pnd_Contract_Payee.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Contract_Nbr(),                   //Natural: COMPRESS #FF3-CONTRACT-NBR '-' #FF3-PAYEE-CDE TO #CONTRACT-PAYEE LEAVING NO SPACE
            "-", ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Payee_Cde()));
        //*  11/2014 START
        //*    DISPLAY (1)
        //*      '////TAX-ID-NO'             #FF3-TAX-ID-NBR
        //*      '///CONTRACT/PAYEE CODE'    #CONTRACT-PAYEE
        //*      '///PAYMENT/DATE'           #FF3-PYMNT-DATE
        //*      '///IVC/IND'                #FF3-IVC-IND
        //*      '////RC'                    #FF3-RESIDENCY-CODE
        //*     '///GROSS/AMOUNT'           #FF3-GROSS-AMT     (EM=ZZZ,ZZZ,ZZ9.99-)
        //*      '  '
        //*      'TAX FREE/IVC/-----------'  #FF3-IVC-AMT       (EM=Z,ZZZ,ZZ9.99-)
        //*      /  '  '
        //*      'LOCAL TAX/WITHHOLDING'     #FF3-LOCAL-WTHLD   (EM=Z,ZZZ,ZZ9.99-)
        //*      '  '
        //*      'FEDERAL TAX/WITHHOLDING/-----------'
        //*      #FF3-FED-WTHLD-AMT (EM=Z,ZZZ,ZZ9.99-)
        //*      /  '  '
        //*      'CANADIAN/WITHHOLDING'      #FF3-CAN-WTHLD-AMT (EM=Z,ZZZ,ZZ9.99-)
        //*      '  '
        //*      'NRA TAX/WITHHOLDING/-----------'
        //*      #FF3-NRA-WTHLD-AMT (EM=Z,ZZZ,ZZ9.99-)
        //*      /  '  '
        //*      'PUERTO RICO/WITHHOLDING'   #TEMP-PR-WTHLD     (EM=Z,ZZZ,ZZ9.99-)
        //*      '  '
        //*      'STATE TAX/WITHHOLDING/-----------'
        //*      #TEMP-STATE-WTHLD  (EM=Z,ZZZ,ZZ9.99-)
        //*      /  '  '
        //*      'INTEREST/AMOUNT'           #FF3-INT-AMT       (EM=Z,ZZZ,ZZ9.99-)
        //*    SKIP(1) 1
        getReports().write(1, ReportOption.NOTITLE,ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Id_Nbr(),pnd_Work_Area_Pnd_Contract_Payee,ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Pymnt_Date(),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Ind(),new  //Natural: WRITE ( 1 ) #FF3-TAX-ID-NBR #CONTRACT-PAYEE #FF3-PYMNT-DATE #FF3-IVC-IND 3X #FF3-RESIDENCY-CODE 2X #FF3-GROSS-AMT ( EM = ZZZ,ZZZ,ZZ9.99- ) 4X #FF3-IVC-AMT ( EM = Z,ZZZ,ZZ9.99- ) 4X #FF3-FED-WTHLD-AMT ( EM = Z,ZZZ,ZZ9.99- ) 4X #FF3-NRA-WTHLD-AMT ( EM = Z,ZZZ,ZZ9.99- ) 4X #TEMP-STATE-WTHLD ( EM = Z,ZZZ,ZZ9.99- ) / 60T #FF3-LOCAL-WTHLD ( EM = Z,ZZZ,ZZ9.99- ) 77T #FF3-CAN-WTHLD-AMT ( EM = Z,ZZZ,ZZ9.99- ) 94T #TEMP-PR-WTHLD ( EM = Z,ZZZ,ZZ9.99- ) 111T #FF3-INT-AMT ( EM = Z,ZZZ,ZZ9.99- )
            ColumnSpacing(3),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code(),new ColumnSpacing(2),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(4),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new 
            ColumnSpacing(4),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Fed_Wthld_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(4),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Nra_Wthld_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(4),pnd_Work_Area_Pnd_Temp_State_Wthld, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),NEWLINE,new 
            TabSetting(60),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Local_Wthld(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new TabSetting(77),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new TabSetting(94),pnd_Work_Area_Pnd_Temp_Pr_Wthld, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new TabSetting(111),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Int_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        //*  11/2014 END
        pnd_Totals_Pnd_Total_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #TOTAL-CNT
        pnd_Totals_Pnd_Total_Grs.nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt());                                                                                 //Natural: ADD #FF3-GROSS-AMT TO #TOTAL-GRS
        pnd_Totals_Pnd_Total_Ivc.nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt());                                                                                   //Natural: ADD #FF3-IVC-AMT TO #TOTAL-IVC
        pnd_Totals_Pnd_Total_Int.nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Int_Amt());                                                                                   //Natural: ADD #FF3-INT-AMT TO #TOTAL-INT
        pnd_Totals_Pnd_Total_Fed.nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Fed_Wthld_Amt());                                                                             //Natural: ADD #FF3-FED-WTHLD-AMT TO #TOTAL-FED
        pnd_Totals_Pnd_Total_Nra.nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Nra_Wthld_Amt());                                                                             //Natural: ADD #FF3-NRA-WTHLD-AMT TO #TOTAL-NRA
        pnd_Totals_Pnd_Total_Sta.nadd(pnd_Work_Area_Pnd_Temp_State_Wthld);                                                                                                //Natural: ADD #TEMP-STATE-WTHLD TO #TOTAL-STA
        pnd_Totals_Pnd_Total_Lcl.nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Local_Wthld());                                                                               //Natural: ADD #FF3-LOCAL-WTHLD TO #TOTAL-LCL
        pnd_Totals_Pnd_Total_Can.nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt());                                                                             //Natural: ADD #FF3-CAN-WTHLD-AMT TO #TOTAL-CAN
        pnd_Totals_Pnd_Total_Pr.nadd(pnd_Work_Area_Pnd_Temp_Pr_Wthld);                                                                                                    //Natural: ADD #TEMP-PR-WTHLD TO #TOTAL-PR
    }
    private void sub_Co_Desc() throws Exception                                                                                                                           //Natural: CO-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //*                  -------
        //*    9/17/04   RM
        //*    JB1011
        //*    11/2014 END
        short decideConditionsMet358 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SV-COMPANY;//Natural: VALUE 'C'
        if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("C"))))
        {
            decideConditionsMet358++;
            pnd_Work_Area_Pnd_Print_Com.setValue("CREF");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'CREF'
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("L"))))
        {
            decideConditionsMet358++;
            pnd_Work_Area_Pnd_Print_Com.setValue("LIFE");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'LIFE'
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("T"))))
        {
            decideConditionsMet358++;
            pnd_Work_Area_Pnd_Print_Com.setValue("TIAA");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'TIAA'
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("S"))))
        {
            decideConditionsMet358++;
            pnd_Work_Area_Pnd_Print_Com.setValue("TCII");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'TCII'
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("X"))))
        {
            decideConditionsMet358++;
            pnd_Work_Area_Pnd_Print_Com.setValue("TRST");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'TRST'
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("F"))))
        {
            decideConditionsMet358++;
            pnd_Work_Area_Pnd_Print_Com.setValue("FSB ");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'FSB '
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Work_Area_Pnd_Print_Com.setValue("OTHR");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'OTHR'
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM CO-DESC
                    sub_Co_Desc();
                    if (condition(Global.isEscape())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR / *INIT-USER '-' *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 121T 'PAGE' *PAGE-NUMBER ( 1 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 34T 'DAILY ONLINE MAINTENANCE DETAIL REPORT-BY COMPANY & TIN' / 'RUNTIME : ' *TIMX 42T 'FINANCIAL TRANSACTIONS - TAX YEAR' #TAX-YEAR 97T 'DATE RANGE :' #START-DATE 'THRU' #END-DATE /// / 'COMPANY : ' #PRINT-COM //
                        TabSetting(121),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(34),"DAILY ONLINE MAINTENANCE DETAIL REPORT-BY COMPANY & TIN",NEWLINE,"RUNTIME : ",Global.getTIMX(),new TabSetting(42),"FINANCIAL TRANSACTIONS - TAX YEAR",pnd_Work_Area_Pnd_Tax_Year,new 
                        TabSetting(97),"DATE RANGE :",pnd_Work_Area_Pnd_Start_Date,"THRU",pnd_Work_Area_Pnd_End_Date,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"COMPANY : ",
                        pnd_Work_Area_Pnd_Print_Com,NEWLINE,NEWLINE);
                    //*  11/2014 START
                    if (condition(pnd_Work_Area_Pnd_Fatca.getBoolean()))                                                                                                  //Natural: IF #FATCA
                    {
                        pnd_Work_Area_Pnd_Hdg1.setValue("  FATCA TAX  ");                                                                                                 //Natural: ASSIGN #HDG1 := '  FATCA TAX  '
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(60),"TAX FREE",new TabSetting(75),"FEDERAL TAX",new TabSetting(91),pnd_Work_Area_Pnd_Hdg1,new  //Natural: WRITE ( 1 ) 60T 'TAX FREE' 75T 'FEDERAL TAX' 91T #HDG1 110T 'STATE TAX' / 62T 'IVC' 75T 'WITHHOLDING' 92T 'WITHHOLDING' 109T 'WITHHODLING' / 58T '-----------' 75T '-----------' 92T '-----------' 109T '-----------' / 13T 'CONTRACT' 24T 'PAYMENT' 33T 'IVC' 45T 'GROSS' 59T 'LOCAL TAX' 77T 'CANADIAN' 92T 'PUERTO RICO' 111T 'INTEREST' / 'TAX-ID-NO' 12T 'PAYEE CODE' 26T 'DATE' 33T 'IND RC' 44T 'AMOUNT' 58T 'WITHHOLDING' 75T 'WITHHOLDING' 92T 'WITHHOLDING' 112T 'AMOUNT' / '---------- ----------- -------- --- -- --------------- ---------------- ---------------- ---------------- ----------------'
                        TabSetting(110),"STATE TAX",NEWLINE,new TabSetting(62),"IVC",new TabSetting(75),"WITHHOLDING",new TabSetting(92),"WITHHOLDING",new 
                        TabSetting(109),"WITHHODLING",NEWLINE,new TabSetting(58),"-----------",new TabSetting(75),"-----------",new TabSetting(92),"-----------",new 
                        TabSetting(109),"-----------",NEWLINE,new TabSetting(13),"CONTRACT",new TabSetting(24),"PAYMENT",new TabSetting(33),"IVC",new TabSetting(45),"GROSS",new 
                        TabSetting(59),"LOCAL TAX",new TabSetting(77),"CANADIAN",new TabSetting(92),"PUERTO RICO",new TabSetting(111),"INTEREST",NEWLINE,"TAX-ID-NO",new 
                        TabSetting(12),"PAYEE CODE",new TabSetting(26),"DATE",new TabSetting(33),"IND RC",new TabSetting(44),"AMOUNT",new TabSetting(58),"WITHHOLDING",new 
                        TabSetting(75),"WITHHOLDING",new TabSetting(92),"WITHHOLDING",new TabSetting(112),"AMOUNT",NEWLINE,"---------- ----------- -------- --- -- --------------- ---------------- ---------------- ---------------- ----------------");
                    //*  11/2014 END
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");
    }
}
