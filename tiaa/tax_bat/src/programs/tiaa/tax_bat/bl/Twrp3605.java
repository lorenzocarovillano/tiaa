/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:38:02 PM
**        * FROM NATURAL PROGRAM : Twrp3605
************************************************************
**        * FILE NAME            : Twrp3605.java
**        * CLASS NAME           : Twrp3605
**        * INSTANCE NAME        : Twrp3605
************************************************************
***********************************************************************
*
* PROGRAM  : TWRP3605
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : PRODUCE REPORTS USING THE FORMS EXTRACT FOR PAPER STATES.
* CREATED  : 11 / 17 / 2000.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM READS THE EXTRACT OF PAPER STATE FROM THE FORMS
*            DATA BASE FILE TO PRODUCE PAPER STATE REPORTS.
* HISTORY  :
*  06/16/2005 - MS - FIX PROBLEM WHEN ONLY ONE RECORD PER COMPANY.
*  01/20/2005 - MS - SUPPORT FOR MULTI-COMPANY PROCESSING.
*  12/12/2011 - RC - ADDED IRR-AMT
*  09/21/2012 - JB - CREATE CONTROL REC EVEN IF NO RECORDS TO PROCESS
*                    MARKED JB01
*  04/04/17  DY  RESTOW MODULE - PIN EXPANSION PROJECT
*    10/13/2020 - RE-STOW COMPONENT FOR 5498           /* SECURE-ACT
** 12/04/2020 - RE-STOW COMPONENT FOR 5498 IRS REPORTING  2020
***********************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3605 extends BLNatBase
{
    // Data Areas
    private PdaTwracom2 pdaTwracom2;
    private PdaTwradist pdaTwradist;
    private PdaTwratbl2 pdaTwratbl2;
    private PdaTwratbl4 pdaTwratbl4;
    private LdaTwrltb4r ldaTwrltb4r;
    private LdaTwrltb4u ldaTwrltb4u;
    private LdaTwrl3600 ldaTwrl3600;
    private LdaTwrl9705 ldaTwrl9705;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Input_Parm;

    private DbsGroup pnd_Input_Parm__R_Field_1;
    private DbsField pnd_Input_Parm_Pnd_In_Form;
    private DbsField pnd_Input_Parm_Pnd_In_Form_Fill;
    private DbsField pnd_Input_Parm_Pnd_In_Tax_Year;
    private DbsField pnd_Input_Parm_Pnd_In_Not_First_Time_Run_Ok;
    private DbsField pnd_Input_Parm_Pnd_In_Input_State;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Header_Tax_Year;
    private DbsField pnd_Ws_Pnd_Num_State_Code;
    private DbsField pnd_Ws_Pnd_Combined_Fed_State;
    private DbsField pnd_Ws_Pnd_State_Indicators;
    private DbsField pnd_Ws_Pnd_Form_Desc;
    private DbsField pnd_Ws_Pnd_Form_Desc2;
    private DbsField pnd_Ws_Pnd_Name;
    private DbsField pnd_Ws_Pnd_Address;
    private DbsField pnd_Ws_Pnd_Comp_Line;
    private DbsField pnd_Ws_Pnd_Et;
    private DbsField pnd_Ws_Pnd_Read_Cnt;
    private DbsField pnd_Ws_Pnd_Zero;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Ws_Pnd_S;
    private DbsField pnd_Ws_Pnd_W;
    private DbsField pnd_Ws_Pnd_W1;
    private DbsField pnd_Ws_Pnd_Det_Rpt_Break_Ind;
    private DbsField pnd_Ws_Pnd_Det_Rec_Break_Ind;
    private DbsField pnd_Ws_Pnd_Terminate;
    private DbsField pnd_Ws_Pnd_Datn;
    private DbsField pnd_Ws_Pnd_Datx;
    private DbsField pnd_Ws_Pnd_Timx;
    private DbsField pnd_Ws_Pnd_Process_Type;
    private DbsField pnd_Ws_Pnd_State_Name;
    private DbsField pnd_Ws_Pnd_Form_Page_Cnt;
    private DbsField pnd_Ws_Pnd_Ws_Isn;
    private DbsField pnd_Ws_Pnd_Rec_Type;

    private DbsGroup pnd_Ws__R_Field_2;
    private DbsField pnd_Ws__Filler1;
    private DbsField pnd_Ws_Pnd_State_Code;
    private DbsField pnd_Ws_Pnd_Det_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Det_Taxable_Amt;
    private DbsField pnd_Ws_Pnd_Det_Fed_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Det_Res_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Det_Res_Taxable_Amt;
    private DbsField pnd_Ws_Pnd_Det_State_Distr;
    private DbsField pnd_Ws_Pnd_Det_State_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Det_Loc_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Det_Loc_Distr;
    private DbsField pnd_Ws_Pnd_Det_Irr_Amt;
    private DbsField pnd_Ws_Pnd_Rec_Type_Desc;

    private DbsGroup pnd_Ws_Pnd_Totals;
    private DbsField pnd_Ws_Pnd_Rej_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Rej_Taxable_Amt;
    private DbsField pnd_Ws_Pnd_Rej_Fed_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Rej_Res_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Rej_Res_Taxable_Amt;
    private DbsField pnd_Ws_Pnd_Rej_State_Distr;
    private DbsField pnd_Ws_Pnd_Rej_State_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Rej_Loc_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Rej_Loc_Distr;
    private DbsField pnd_Ws_Pnd_Rej_Irr_Amt;
    private DbsField pnd_Ws_Pnd_Rco_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Rco_Taxable_Amt;
    private DbsField pnd_Ws_Pnd_Rco_Fed_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Rco_Res_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Rco_Res_Taxable_Amt;
    private DbsField pnd_Ws_Pnd_Rco_State_Distr;
    private DbsField pnd_Ws_Pnd_Rco_State_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Rco_Loc_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Rco_Loc_Distr;
    private DbsField pnd_Ws_Pnd_Rco_Irr_Amt;
    private DbsField pnd_Ws_Pnd_Tot_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Tot_Taxable_Amt;
    private DbsField pnd_Ws_Pnd_Tot_Fed_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Tot_Res_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Tot_Res_Taxable_Amt;
    private DbsField pnd_Ws_Pnd_Tot_State_Distr;
    private DbsField pnd_Ws_Pnd_Tot_State_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Tot_Loc_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Tot_Loc_Distr;
    private DbsField pnd_Ws_Pnd_Tot_Irr_Amt;
    private DbsField pnd_Ws_Pnd_Rec_State_Distr;
    private DbsField pnd_Ws_Pnd_Rec_State_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Rec_Loc_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Rec_Loc_Distr;
    private DbsField pnd_Ws_Pnd_Rec_Counter;
    private DbsField pnd_Ws_Pnd_Accept_Form_Count;
    private DbsField pnd_Ws_Pnd_Recon_Only;
    private DbsField pnd_Ws_Pnd_Empty_Form_Count;
    private DbsField pnd_Ws_Pnd_Reject_Form_Count;

    private DbsRecord internalLoopRecord;
    private DbsField rEAD_WORKPnd_Tirf_Company_CdeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwracom2 = new PdaTwracom2(localVariables);
        pdaTwradist = new PdaTwradist(localVariables);
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        pdaTwratbl4 = new PdaTwratbl4(localVariables);
        ldaTwrltb4r = new LdaTwrltb4r();
        registerRecord(ldaTwrltb4r);
        registerRecord(ldaTwrltb4r.getVw_tircntl_Rpt());
        ldaTwrltb4u = new LdaTwrltb4u();
        registerRecord(ldaTwrltb4u);
        registerRecord(ldaTwrltb4u.getVw_tircntl_Rpt_U());
        ldaTwrl3600 = new LdaTwrl3600();
        registerRecord(ldaTwrl3600);
        ldaTwrl9705 = new LdaTwrl9705();
        registerRecord(ldaTwrl9705);
        registerRecord(ldaTwrl9705.getVw_form_U());

        // Local Variables
        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 30);

        pnd_Input_Parm__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Parm__R_Field_1", "REDEFINE", pnd_Input_Parm);
        pnd_Input_Parm_Pnd_In_Form = pnd_Input_Parm__R_Field_1.newFieldInGroup("pnd_Input_Parm_Pnd_In_Form", "#IN-FORM", FieldType.STRING, 4);
        pnd_Input_Parm_Pnd_In_Form_Fill = pnd_Input_Parm__R_Field_1.newFieldInGroup("pnd_Input_Parm_Pnd_In_Form_Fill", "#IN-FORM-FILL", FieldType.STRING, 
            2);
        pnd_Input_Parm_Pnd_In_Tax_Year = pnd_Input_Parm__R_Field_1.newFieldInGroup("pnd_Input_Parm_Pnd_In_Tax_Year", "#IN-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Input_Parm_Pnd_In_Not_First_Time_Run_Ok = pnd_Input_Parm__R_Field_1.newFieldInGroup("pnd_Input_Parm_Pnd_In_Not_First_Time_Run_Ok", "#IN-NOT-FIRST-TIME-RUN-OK", 
            FieldType.STRING, 1);
        pnd_Input_Parm_Pnd_In_Input_State = pnd_Input_Parm__R_Field_1.newFieldInGroup("pnd_Input_Parm_Pnd_In_Input_State", "#IN-INPUT-STATE", FieldType.STRING, 
            2);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Header_Tax_Year = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Header_Tax_Year", "#HEADER-TAX-YEAR", FieldType.STRING, 4);
        pnd_Ws_Pnd_Num_State_Code = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Num_State_Code", "#NUM-STATE-CODE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Combined_Fed_State = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Combined_Fed_State", "#COMBINED-FED-STATE", FieldType.STRING, 1);
        pnd_Ws_Pnd_State_Indicators = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_State_Indicators", "#STATE-INDICATORS", FieldType.STRING, 20);
        pnd_Ws_Pnd_Form_Desc = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Form_Desc", "#FORM-DESC", FieldType.STRING, 30);
        pnd_Ws_Pnd_Form_Desc2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Form_Desc2", "#FORM-DESC2", FieldType.STRING, 30);
        pnd_Ws_Pnd_Name = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Name", "#NAME", FieldType.STRING, 30);
        pnd_Ws_Pnd_Address = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Address", "#ADDRESS", FieldType.STRING, 126);
        pnd_Ws_Pnd_Comp_Line = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Comp_Line", "#COMP-LINE", FieldType.STRING, 12);
        pnd_Ws_Pnd_Et = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Et", "#ET", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Read_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Read_Cnt", "#READ-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Ws_Pnd_Zero = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Zero", "#ZERO", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S", "#S", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_W = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_W", "#W", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_W1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_W1", "#W1", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Det_Rpt_Break_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_Rpt_Break_Ind", "#DET-RPT-BREAK-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Det_Rec_Break_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_Rec_Break_Ind", "#DET-REC-BREAK-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Terminate = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Terminate", "#TERMINATE", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Datn = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Datn", "#DATN", FieldType.STRING, 8);
        pnd_Ws_Pnd_Datx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Datx", "#DATX", FieldType.DATE);
        pnd_Ws_Pnd_Timx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Timx", "#TIMX", FieldType.TIME);
        pnd_Ws_Pnd_Process_Type = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Process_Type", "#PROCESS-TYPE", FieldType.STRING, 1);
        pnd_Ws_Pnd_State_Name = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_State_Name", "#STATE-NAME", FieldType.STRING, 19);
        pnd_Ws_Pnd_Form_Page_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Form_Page_Cnt", "#FORM-PAGE-CNT", FieldType.PACKED_DECIMAL, 2);
        pnd_Ws_Pnd_Ws_Isn = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Isn", "#WS-ISN", FieldType.NUMERIC, 8);
        pnd_Ws_Pnd_Rec_Type = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Type", "#REC-TYPE", FieldType.STRING, 7);

        pnd_Ws__R_Field_2 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_2", "REDEFINE", pnd_Ws_Pnd_Rec_Type);
        pnd_Ws__Filler1 = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws__Filler1", "_FILLER1", FieldType.STRING, 4);
        pnd_Ws_Pnd_State_Code = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_State_Code", "#STATE-CODE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Det_Gross_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_Gross_Amt", "#DET-GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Det_Taxable_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_Taxable_Amt", "#DET-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Det_Fed_Tax_Wthld = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_Fed_Tax_Wthld", "#DET-FED-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Det_Res_Gross_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_Res_Gross_Amt", "#DET-RES-GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Det_Res_Taxable_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_Res_Taxable_Amt", "#DET-RES-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Ws_Pnd_Det_State_Distr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_State_Distr", "#DET-STATE-DISTR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Det_State_Tax_Wthld = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_State_Tax_Wthld", "#DET-STATE-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Ws_Pnd_Det_Loc_Tax_Wthld = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_Loc_Tax_Wthld", "#DET-LOC-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Det_Loc_Distr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_Loc_Distr", "#DET-LOC-DISTR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Det_Irr_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_Irr_Amt", "#DET-IRR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Rec_Type_Desc = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Rec_Type_Desc", "#REC-TYPE-DESC", FieldType.STRING, 14, new DbsArrayController(1, 
            3));

        pnd_Ws_Pnd_Totals = pnd_Ws.newGroupArrayInGroup("pnd_Ws_Pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Ws_Pnd_Rej_Gross_Amt = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Rej_Gross_Amt", "#REJ-GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Rej_Taxable_Amt = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Rej_Taxable_Amt", "#REJ-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Ws_Pnd_Rej_Fed_Tax_Wthld = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Rej_Fed_Tax_Wthld", "#REJ-FED-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Ws_Pnd_Rej_Res_Gross_Amt = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Rej_Res_Gross_Amt", "#REJ-RES-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Ws_Pnd_Rej_Res_Taxable_Amt = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Rej_Res_Taxable_Amt", "#REJ-RES-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Ws_Pnd_Rej_State_Distr = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Rej_State_Distr", "#REJ-STATE-DISTR", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Ws_Pnd_Rej_State_Tax_Wthld = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Rej_State_Tax_Wthld", "#REJ-STATE-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Ws_Pnd_Rej_Loc_Tax_Wthld = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Rej_Loc_Tax_Wthld", "#REJ-LOC-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Ws_Pnd_Rej_Loc_Distr = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Rej_Loc_Distr", "#REJ-LOC-DISTR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Rej_Irr_Amt = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Rej_Irr_Amt", "#REJ-IRR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Rco_Gross_Amt = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Rco_Gross_Amt", "#RCO-GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Rco_Taxable_Amt = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Rco_Taxable_Amt", "#RCO-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Ws_Pnd_Rco_Fed_Tax_Wthld = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Rco_Fed_Tax_Wthld", "#RCO-FED-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Ws_Pnd_Rco_Res_Gross_Amt = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Rco_Res_Gross_Amt", "#RCO-RES-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Ws_Pnd_Rco_Res_Taxable_Amt = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Rco_Res_Taxable_Amt", "#RCO-RES-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Ws_Pnd_Rco_State_Distr = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Rco_State_Distr", "#RCO-STATE-DISTR", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Ws_Pnd_Rco_State_Tax_Wthld = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Rco_State_Tax_Wthld", "#RCO-STATE-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Ws_Pnd_Rco_Loc_Tax_Wthld = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Rco_Loc_Tax_Wthld", "#RCO-LOC-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Ws_Pnd_Rco_Loc_Distr = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Rco_Loc_Distr", "#RCO-LOC-DISTR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Rco_Irr_Amt = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Rco_Irr_Amt", "#RCO-IRR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Tot_Gross_Amt = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Tot_Gross_Amt", "#TOT-GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Tot_Taxable_Amt = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Tot_Taxable_Amt", "#TOT-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Ws_Pnd_Tot_Fed_Tax_Wthld = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Tot_Fed_Tax_Wthld", "#TOT-FED-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Ws_Pnd_Tot_Res_Gross_Amt = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Tot_Res_Gross_Amt", "#TOT-RES-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Ws_Pnd_Tot_Res_Taxable_Amt = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Tot_Res_Taxable_Amt", "#TOT-RES-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Ws_Pnd_Tot_State_Distr = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Tot_State_Distr", "#TOT-STATE-DISTR", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Ws_Pnd_Tot_State_Tax_Wthld = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Tot_State_Tax_Wthld", "#TOT-STATE-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Ws_Pnd_Tot_Loc_Tax_Wthld = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Tot_Loc_Tax_Wthld", "#TOT-LOC-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Ws_Pnd_Tot_Loc_Distr = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Tot_Loc_Distr", "#TOT-LOC-DISTR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Tot_Irr_Amt = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Tot_Irr_Amt", "#TOT-IRR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Rec_State_Distr = pnd_Ws_Pnd_Totals.newFieldArrayInGroup("pnd_Ws_Pnd_Rec_State_Distr", "#REC-STATE-DISTR", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 3));
        pnd_Ws_Pnd_Rec_State_Tax_Wthld = pnd_Ws_Pnd_Totals.newFieldArrayInGroup("pnd_Ws_Pnd_Rec_State_Tax_Wthld", "#REC-STATE-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 3));
        pnd_Ws_Pnd_Rec_Loc_Tax_Wthld = pnd_Ws_Pnd_Totals.newFieldArrayInGroup("pnd_Ws_Pnd_Rec_Loc_Tax_Wthld", "#REC-LOC-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 3));
        pnd_Ws_Pnd_Rec_Loc_Distr = pnd_Ws_Pnd_Totals.newFieldArrayInGroup("pnd_Ws_Pnd_Rec_Loc_Distr", "#REC-LOC-DISTR", FieldType.PACKED_DECIMAL, 13, 2, 
            new DbsArrayController(1, 3));
        pnd_Ws_Pnd_Rec_Counter = pnd_Ws_Pnd_Totals.newFieldArrayInGroup("pnd_Ws_Pnd_Rec_Counter", "#REC-COUNTER", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 
            3));
        pnd_Ws_Pnd_Accept_Form_Count = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Accept_Form_Count", "#ACCEPT-FORM-COUNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Recon_Only = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Recon_Only", "#RECON-ONLY", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Empty_Form_Count = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Empty_Form_Count", "#EMPTY-FORM-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Reject_Form_Count = pnd_Ws_Pnd_Totals.newFieldInGroup("pnd_Ws_Pnd_Reject_Form_Count", "#REJECT-FORM-COUNT", FieldType.PACKED_DECIMAL, 
            7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        rEAD_WORKPnd_Tirf_Company_CdeOld = internalLoopRecord.newFieldInRecord("READ_WORK_Pnd_Tirf_Company_Cde_OLD", "Pnd_Tirf_Company_Cde_OLD", FieldType.STRING, 
            1);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaTwrltb4r.initializeValues();
        ldaTwrltb4u.initializeValues();
        ldaTwrl3600.initializeValues();
        ldaTwrl9705.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_Rec_Type.setInitialValue("9ST000");
        pnd_Ws_Pnd_Rec_Type_Desc.getValue(1).setInitialValue("Accepted Forms");
        pnd_Ws_Pnd_Rec_Type_Desc.getValue(2).setInitialValue("Rejected Forms");
        pnd_Ws_Pnd_Rec_Type_Desc.getValue(3).setInitialValue("Total");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3605() throws Exception
    {
        super("Twrp3605");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp3605|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 02 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 03 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 04 ) PS = 60 LS = 133 ZP = ON
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: IF *DEVICE = 'BATCH'
                {
                    //*  SET DELIMITER MODE FOR BATCH INPUT
                    setControl("D");                                                                                                                                      //Natural: SET CONTROL 'D'
                }                                                                                                                                                         //Natural: END-IF
                //* ***************
                //*  MAIN PROGRAM *
                //* ***************
                pdaTwradist.getPnd_Twradist_Pnd_Abend_Ind().setValue(true);                                                                                               //Natural: ASSIGN #TWRADIST.#ABEND-IND := #TWRADIST.#DISPLAY-IND := #TWRACOM2.#ABEND-IND := #TWRACOM2.#DISPLAY-IND := TRUE
                pdaTwradist.getPnd_Twradist_Pnd_Display_Ind().setValue(true);
                pdaTwracom2.getPnd_Twracom2_Pnd_Abend_Ind().setValue(true);
                pdaTwracom2.getPnd_Twracom2_Pnd_Display_Ind().setValue(true);
                pdaTwradist.getPnd_Twradist_Pnd_Function().setValue("1");                                                                                                 //Natural: ASSIGN #TWRADIST.#FUNCTION := '1'
                pnd_Ws_Pnd_S.setValue(1);                                                                                                                                 //Natural: ASSIGN #WS.#S := 1
                pnd_Ws_Pnd_Datn.setValue(Global.getDATN());                                                                                                               //Natural: ASSIGN #DATN := *DATN
                pnd_Ws_Pnd_Datx.setValue(Global.getDATX());                                                                                                               //Natural: ASSIGN #DATX := *DATX
                pnd_Ws_Pnd_Timx.setValue(Global.getTIMX());                                                                                                               //Natural: ASSIGN #TIMX := *TIMX
                boolean endOfDataReadWork = true;                                                                                                                         //Natural: READ WORK FILE 01 RECORD #TWRL3600
                boolean firstReadWork = true;
                READ_WORK:
                while (condition(getWorkFiles().read(1, ldaTwrl3600.getPnd_Twrl3600())))
                {
                    CheckAtStartofData765();

                    if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventRead_Work();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataReadWork = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    pnd_Ws_Pnd_Read_Cnt.nadd(1);                                                                                                                          //Natural: AT START OF DATA;//Natural: ADD 1 TO #READ-CNT
                    //*  UPDATE FORMS                                                                                                                                     //Natural: AT BREAK OF #TWRL3600.#TIRF-COMPANY-CDE
                    G_FR:                                                                                                                                                 //Natural: GET FORM-U #TIRF-ISN
                    ldaTwrl9705.getVw_form_U().readByID(ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Isn().getLong(), "G_FR");
                    pnd_Ws_Pnd_J.reset();                                                                                                                                 //Natural: RESET #J
                    FOR01:                                                                                                                                                //Natural: FOR #I = 1 TO FORM-U.C*TIRF-1099-R-STATE-GRP
                    for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl9705.getForm_U_Count_Casttirf_1099_R_State_Grp())); pnd_Ws_Pnd_I.nadd(1))
                    {
                        if (condition(ldaTwrl9705.getForm_U_Tirf_State_Code().getValue(pnd_Ws_Pnd_I).notEquals(ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_State_Code())))       //Natural: IF FORM-U.TIRF-STATE-CODE ( #I ) NE #TIRF-STATE-CODE
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Ws_Pnd_J.setValue(pnd_Ws_Pnd_I);                                                                                                              //Natural: ASSIGN #J := #I
                        ldaTwrl9705.getForm_U_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Ws_Pnd_I).setValue(ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Process_Ind());              //Natural: ASSIGN FORM-U.TIRF-STATE-AUTH-RPT-IND ( #I ) := #TIRF-PROCESS-IND
                        ldaTwrl9705.getForm_U_Tirf_State_Auth_Rpt_Date().getValue(pnd_Ws_Pnd_I).setValue(pnd_Ws_Pnd_Datx);                                                //Natural: ASSIGN FORM-U.TIRF-STATE-AUTH-RPT-DATE ( #I ) := #DATX
                        ldaTwrl9705.getForm_U_Tirf_Lu_User().setValue(Global.getINIT_PROGRAM());                                                                          //Natural: ASSIGN FORM-U.TIRF-LU-USER := *INIT-PROGRAM
                        ldaTwrl9705.getForm_U_Tirf_Lu_Ts().setValue(pnd_Ws_Pnd_Timx);                                                                                     //Natural: ASSIGN FORM-U.TIRF-LU-TS := #TIMX
                        ldaTwrl9705.getVw_form_U().updateDBRow("G_FR");                                                                                                   //Natural: UPDATE ( G-FR. )
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_WORK"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_WORK"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Ws_Pnd_J.equals(getZero())))                                                                                                        //Natural: IF #J = 0
                    {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                        sub_Error_Display_Start();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_WORK"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_WORK"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"State is not found on form record",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 0 ) '***' 25T 'State is not found on form record' 77T '***' / '***' 25T 'Tax Year..:' #TIRF-TAX-YEAR 77T '***' / '***' 25T 'TIN.......:' #TIRF-TIN 77T '***' / '***' 25T 'Contract..:' #TIRF-CONTRACT-NBR 77T '***' / '***' 25T 'Payee.....: ' #TIRF-PAYEE-CDE 77T '***'
                            TabSetting(25),"Tax Year..:",ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Tax_Year(),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"TIN.......:",ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Tin(),new 
                            TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Contract..:",ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Contract_Nbr(),new 
                            TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Payee.....: ",ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Payee_Cde(),new TabSetting(77),
                            "***");
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_WORK"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_WORK"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                        sub_Error_Display_End();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_WORK"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_WORK"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Ws_Pnd_Terminate.setValue(true);                                                                                                              //Natural: ASSIGN #TERMINATE := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Pnd_Et.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ET
                    if (condition(pnd_Ws_Pnd_Et.greaterOrEqual(200)))                                                                                                     //Natural: IF #ET GE 200
                    {
                        pnd_Ws_Pnd_Et.reset();                                                                                                                            //Natural: RESET #ET
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Empty_Form().equals("Y")))                                                                         //Natural: IF #TIRF-EMPTY-FORM = 'Y'
                    {
                        pnd_Ws_Pnd_Empty_Form_Count.getValue(pnd_Ws_Pnd_S).nadd(1);                                                                                       //Natural: ADD 1 TO #EMPTY-FORM-COUNT ( #S )
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Pnd_W.setValue(pnd_Ws_Pnd_W1);                                                                                                                 //Natural: ASSIGN #W := #W1
                    short decideConditionsMet817 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF #TIRF-PROCESS-IND;//Natural: VALUE 'H', 'J'
                    if (condition((ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Process_Ind().equals("H") || ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Process_Ind().equals("J"))))
                    {
                        decideConditionsMet817++;
                        pnd_Ws_Pnd_Form_Desc.setValue("Rejected Forms");                                                                                                  //Natural: ASSIGN #FORM-DESC := #FORM-DESC2 := 'Rejected Forms'
                        pnd_Ws_Pnd_Form_Desc2.setValue("Rejected Forms");
                        pnd_Ws_Pnd_Reject_Form_Count.getValue(pnd_Ws_Pnd_S).nadd(1);                                                                                      //Natural: ADD 1 TO #REJECT-FORM-COUNT ( #S )
                        pnd_Ws_Pnd_W1.setValue(2);                                                                                                                        //Natural: ASSIGN #W1 := 2
                        pnd_Ws_Pnd_Rej_Gross_Amt.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Gross_Amt());                                                     //Natural: ADD TIRF-GROSS-AMT TO #REJ-GROSS-AMT ( #S )
                        pnd_Ws_Pnd_Rej_Taxable_Amt.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Taxable_Amt());                                                 //Natural: ADD TIRF-TAXABLE-AMT TO #REJ-TAXABLE-AMT ( #S )
                        pnd_Ws_Pnd_Rej_Fed_Tax_Wthld.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                             //Natural: ADD TIRF-FED-TAX-WTHLD TO #REJ-FED-TAX-WTHLD ( #S )
                        pnd_Ws_Pnd_Rej_Res_Gross_Amt.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Res_Gross_Amt().getValue(pnd_Ws_Pnd_J));                      //Natural: ADD TIRF-RES-GROSS-AMT ( #J ) TO #REJ-RES-GROSS-AMT ( #S )
                        pnd_Ws_Pnd_Rej_Res_Taxable_Amt.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Res_Taxable_Amt().getValue(pnd_Ws_Pnd_J));                  //Natural: ADD TIRF-RES-TAXABLE-AMT ( #J ) TO #REJ-RES-TAXABLE-AMT ( #S )
                        pnd_Ws_Pnd_Rej_State_Distr.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_State_Distr().getValue(pnd_Ws_Pnd_J));                          //Natural: ADD TIRF-STATE-DISTR ( #J ) TO #REJ-STATE-DISTR ( #S )
                        pnd_Ws_Pnd_Rej_State_Tax_Wthld.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld().getValue(pnd_Ws_Pnd_J));                  //Natural: ADD TIRF-STATE-TAX-WTHLD ( #J ) TO #REJ-STATE-TAX-WTHLD ( #S )
                        pnd_Ws_Pnd_Rej_Loc_Tax_Wthld.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Pnd_J));                      //Natural: ADD TIRF-LOC-TAX-WTHLD ( #J ) TO #REJ-LOC-TAX-WTHLD ( #S )
                        pnd_Ws_Pnd_Rej_Loc_Distr.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Distr().getValue(pnd_Ws_Pnd_J));                              //Natural: ADD TIRF-LOC-DISTR ( #J ) TO #REJ-LOC-DISTR ( #S )
                        pnd_Ws_Pnd_Rej_Irr_Amt.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Res_Irr_Amt().getValue(pnd_Ws_Pnd_J));                              //Natural: ADD TIRF-RES-IRR-AMT ( #J ) TO #REJ-IRR-AMT ( #S )
                    }                                                                                                                                                     //Natural: VALUE 'X'
                    else if (condition((ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Process_Ind().equals("X"))))
                    {
                        decideConditionsMet817++;
                        pnd_Ws_Pnd_Form_Desc.setValue("Not Reportable Forms");                                                                                            //Natural: ASSIGN #FORM-DESC := 'Not Reportable Forms'
                        pnd_Ws_Pnd_Form_Desc2.setValue("Accepted Forms");                                                                                                 //Natural: ASSIGN #FORM-DESC2 := 'Accepted Forms'
                        if (condition(ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Empty_Form().notEquals("Y")))                                                                  //Natural: IF #TIRF-EMPTY-FORM NE 'Y'
                        {
                            pnd_Ws_Pnd_Recon_Only.getValue(pnd_Ws_Pnd_S).nadd(1);                                                                                         //Natural: ADD 1 TO #RECON-ONLY ( #S )
                            pnd_Ws_Pnd_W1.setValue(1);                                                                                                                    //Natural: ASSIGN #W1 := 1
                            pnd_Ws_Pnd_Rco_Gross_Amt.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Gross_Amt());                                                 //Natural: ADD TIRF-GROSS-AMT TO #RCO-GROSS-AMT ( #S )
                            pnd_Ws_Pnd_Rco_Taxable_Amt.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Taxable_Amt());                                             //Natural: ADD TIRF-TAXABLE-AMT TO #RCO-TAXABLE-AMT ( #S )
                            pnd_Ws_Pnd_Rco_Fed_Tax_Wthld.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                         //Natural: ADD TIRF-FED-TAX-WTHLD TO #RCO-FED-TAX-WTHLD ( #S )
                            pnd_Ws_Pnd_Rco_Res_Gross_Amt.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Res_Gross_Amt().getValue(pnd_Ws_Pnd_J));                  //Natural: ADD TIRF-RES-GROSS-AMT ( #J ) TO #RCO-RES-GROSS-AMT ( #S )
                            pnd_Ws_Pnd_Rco_Res_Taxable_Amt.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Res_Taxable_Amt().getValue(pnd_Ws_Pnd_J));              //Natural: ADD TIRF-RES-TAXABLE-AMT ( #J ) TO #RCO-RES-TAXABLE-AMT ( #S )
                            pnd_Ws_Pnd_Rco_State_Distr.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_State_Distr().getValue(pnd_Ws_Pnd_J));                      //Natural: ADD TIRF-STATE-DISTR ( #J ) TO #RCO-STATE-DISTR ( #S )
                            pnd_Ws_Pnd_Rco_State_Tax_Wthld.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld().getValue(pnd_Ws_Pnd_J));              //Natural: ADD TIRF-STATE-TAX-WTHLD ( #J ) TO #RCO-STATE-TAX-WTHLD ( #S )
                            pnd_Ws_Pnd_Rco_Loc_Tax_Wthld.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Pnd_J));                  //Natural: ADD TIRF-LOC-TAX-WTHLD ( #J ) TO #RCO-LOC-TAX-WTHLD ( #S )
                            pnd_Ws_Pnd_Rco_Loc_Distr.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Distr().getValue(pnd_Ws_Pnd_J));                          //Natural: ADD TIRF-LOC-DISTR ( #J ) TO #RCO-LOC-DISTR ( #S )
                            pnd_Ws_Pnd_Rco_Irr_Amt.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Res_Irr_Amt().getValue(pnd_Ws_Pnd_J));                          //Natural: ADD TIRF-RES-IRR-AMT ( #J ) TO #RCO-IRR-AMT ( #S )
                                                                                                                                                                          //Natural: PERFORM PRINT-HARDCOPY
                            sub_Print_Hardcopy();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_WORK"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_WORK"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 'O'
                    else if (condition((ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Process_Ind().equals("O"))))
                    {
                        decideConditionsMet817++;
                        pnd_Ws_Pnd_Form_Desc.setValue("Accepted Forms");                                                                                                  //Natural: ASSIGN #FORM-DESC := #FORM-DESC2 := 'Accepted Forms'
                        pnd_Ws_Pnd_Form_Desc2.setValue("Accepted Forms");
                        if (condition(ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Empty_Form().notEquals("Y")))                                                                  //Natural: IF #TIRF-EMPTY-FORM NE 'Y'
                        {
                            pnd_Ws_Pnd_Accept_Form_Count.getValue(pnd_Ws_Pnd_S).nadd(1);                                                                                  //Natural: ADD 1 TO #ACCEPT-FORM-COUNT ( #S )
                            pnd_Ws_Pnd_W1.setValue(1);                                                                                                                    //Natural: ASSIGN #W1 := 1
                            pnd_Ws_Pnd_Tot_Gross_Amt.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Gross_Amt());                                                 //Natural: ADD TIRF-GROSS-AMT TO #TOT-GROSS-AMT ( #S )
                            pnd_Ws_Pnd_Tot_Taxable_Amt.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Taxable_Amt());                                             //Natural: ADD TIRF-TAXABLE-AMT TO #TOT-TAXABLE-AMT ( #S )
                            pnd_Ws_Pnd_Tot_Fed_Tax_Wthld.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                         //Natural: ADD TIRF-FED-TAX-WTHLD TO #TOT-FED-TAX-WTHLD ( #S )
                            pnd_Ws_Pnd_Tot_Res_Gross_Amt.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Res_Gross_Amt().getValue(pnd_Ws_Pnd_J));                  //Natural: ADD TIRF-RES-GROSS-AMT ( #J ) TO #TOT-RES-GROSS-AMT ( #S )
                            pnd_Ws_Pnd_Tot_Res_Taxable_Amt.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Res_Taxable_Amt().getValue(pnd_Ws_Pnd_J));              //Natural: ADD TIRF-RES-TAXABLE-AMT ( #J ) TO #TOT-RES-TAXABLE-AMT ( #S )
                            pnd_Ws_Pnd_Tot_State_Distr.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_State_Distr().getValue(pnd_Ws_Pnd_J));                      //Natural: ADD TIRF-STATE-DISTR ( #J ) TO #TOT-STATE-DISTR ( #S )
                            pnd_Ws_Pnd_Tot_State_Tax_Wthld.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld().getValue(pnd_Ws_Pnd_J));              //Natural: ADD TIRF-STATE-TAX-WTHLD ( #J ) TO #TOT-STATE-TAX-WTHLD ( #S )
                            pnd_Ws_Pnd_Tot_Loc_Tax_Wthld.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Pnd_J));                  //Natural: ADD TIRF-LOC-TAX-WTHLD ( #J ) TO #TOT-LOC-TAX-WTHLD ( #S )
                            pnd_Ws_Pnd_Tot_Loc_Distr.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Distr().getValue(pnd_Ws_Pnd_J));                          //Natural: ADD TIRF-LOC-DISTR ( #J ) TO #TOT-LOC-DISTR ( #S )
                            pnd_Ws_Pnd_Tot_Irr_Amt.getValue(pnd_Ws_Pnd_S).nadd(ldaTwrl9705.getForm_U_Tirf_Res_Irr_Amt().getValue(pnd_Ws_Pnd_J));                          //Natural: ADD TIRF-RES-IRR-AMT ( #J ) TO #TOT-IRR-AMT ( #S )
                                                                                                                                                                          //Natural: PERFORM PRINT-HARDCOPY
                            sub_Print_Hardcopy();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_WORK"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_WORK"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    if (condition(pnd_Ws_Pnd_Form_Desc.isBreak()))                                                                                                        //Natural: IF BREAK OF #FORM-DESC
                    {
                        getReports().newPage(new ReportSpecification(2));                                                                                                 //Natural: NEWPAGE ( 02 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_WORK"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_WORK"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM DETAIL-BREAK
                        sub_Detail_Break();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_WORK"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_WORK"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Pnd_Det_Gross_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Gross_Amt());                                                                                //Natural: ADD TIRF-GROSS-AMT TO #DET-GROSS-AMT
                    pnd_Ws_Pnd_Det_Taxable_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Taxable_Amt());                                                                            //Natural: ADD TIRF-TAXABLE-AMT TO #DET-TAXABLE-AMT
                    pnd_Ws_Pnd_Det_Fed_Tax_Wthld.nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                                                        //Natural: ADD TIRF-FED-TAX-WTHLD TO #DET-FED-TAX-WTHLD
                    pnd_Ws_Pnd_Det_Res_Gross_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Gross_Amt().getValue(pnd_Ws_Pnd_J));                                                 //Natural: ADD TIRF-RES-GROSS-AMT ( #J ) TO #DET-RES-GROSS-AMT
                    pnd_Ws_Pnd_Det_Res_Taxable_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Taxable_Amt().getValue(pnd_Ws_Pnd_J));                                             //Natural: ADD TIRF-RES-TAXABLE-AMT ( #J ) TO #DET-RES-TAXABLE-AMT
                    pnd_Ws_Pnd_Det_State_Distr.nadd(ldaTwrl9705.getForm_U_Tirf_State_Distr().getValue(pnd_Ws_Pnd_J));                                                     //Natural: ADD TIRF-STATE-DISTR ( #J ) TO #DET-STATE-DISTR
                    pnd_Ws_Pnd_Det_State_Tax_Wthld.nadd(ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld().getValue(pnd_Ws_Pnd_J));                                             //Natural: ADD TIRF-STATE-TAX-WTHLD ( #J ) TO #DET-STATE-TAX-WTHLD
                    pnd_Ws_Pnd_Det_Loc_Tax_Wthld.nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Pnd_J));                                                 //Natural: ADD TIRF-LOC-TAX-WTHLD ( #J ) TO #DET-LOC-TAX-WTHLD
                    pnd_Ws_Pnd_Det_Loc_Distr.nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Distr().getValue(pnd_Ws_Pnd_J));                                                         //Natural: ADD TIRF-LOC-DISTR ( #J ) TO #DET-LOC-DISTR
                    pnd_Ws_Pnd_Det_Irr_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Irr_Amt().getValue(pnd_Ws_Pnd_J));                                                         //Natural: ADD TIRF-RES-IRR-AMT ( #J ) TO #DET-IRR-AMT
                    if (condition(ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Empty_Form().equals("Y")))                                                                         //Natural: IF #TIRF-EMPTY-FORM = 'Y'
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Pnd_Name.setValue(DbsUtil.compress(ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Part_First_Nme(), ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Part_Mddle_Nme(),  //Natural: COMPRESS #TIRF-PART-FIRST-NME #TIRF-PART-MDDLE-NME #TIRF-PART-LAST-NME INTO #NAME
                        ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Part_Last_Nme()));
                    pnd_Ws_Pnd_Address.setValue(DbsUtil.compress(ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Addr_Ln1(), ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Addr_Ln2(),        //Natural: COMPRESS #TIRF-ADDR-LN1 #TIRF-ADDR-LN2 #TIRF-ADDR-LN3 #TIRF-ADDR-LN4 #TIRF-ADDR-LN5 #TIRF-ADDR-LN6 INTO #WS.#ADDRESS
                        ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Addr_Ln3(), ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Addr_Ln4(), ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Addr_Ln5(), 
                        ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Addr_Ln6()));
                    if (condition(ldaTwrl9705.getForm_U_Tirf_Distribution_Cde().notEquals(pdaTwradist.getPnd_Twradist_Pnd_In_Dist())))                                    //Natural: IF FORM-U.TIRF-DISTRIBUTION-CDE NE #TWRADIST.#IN-DIST
                    {
                        pdaTwradist.getPnd_Twradist_Pnd_In_Dist().setValue(ldaTwrl9705.getForm_U_Tirf_Distribution_Cde());                                                //Natural: ASSIGN #TWRADIST.#IN-DIST := FORM-U.TIRF-DISTRIBUTION-CDE
                        DbsUtil.callnat(Twrndist.class , getCurrentProcessState(), pdaTwradist.getPnd_Twradist_Pnd_Input_Parms(), new AttributeParameter("O"),            //Natural: CALLNAT 'TWRNDIST' USING #TWRADIST.#INPUT-PARMS ( AD = O ) #TWRADIST.#OUTPUT-DATA ( AD = M )
                            pdaTwradist.getPnd_Twradist_Pnd_Output_Data(), new AttributeParameter("M"));
                        if (condition(Global.isEscape())) return;
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_State_Tax_Wthld().notEquals(new DbsDecimal("0.00")) || ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Loc_Tax_Wthld().notEquals(new  //Natural: IF #TIRF-STATE-TAX-WTHLD NE 0.00 OR #TIRF-LOC-TAX-WTHLD NE 0.00
                        DbsDecimal("0.00"))))
                    {
                        if (condition(pnd_Ws_Pnd_Form_Desc2.isBreak()))                                                                                                   //Natural: IF BREAK OF #FORM-DESC2
                        {
                                                                                                                                                                          //Natural: PERFORM DETAIL-RECO-BREAK
                            sub_Detail_Reco_Break();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_WORK"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_WORK"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Ws_Pnd_Rec_State_Distr.getValue(pnd_Ws_Pnd_S,pnd_Ws_Pnd_W1).nadd(ldaTwrl9705.getForm_U_Tirf_State_Distr().getValue(pnd_Ws_Pnd_J));            //Natural: ADD TIRF-STATE-DISTR ( #J ) TO #REC-STATE-DISTR ( #S,#W1 )
                        pnd_Ws_Pnd_Rec_State_Tax_Wthld.getValue(pnd_Ws_Pnd_S,pnd_Ws_Pnd_W1).nadd(ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld().getValue(pnd_Ws_Pnd_J));    //Natural: ADD TIRF-STATE-TAX-WTHLD ( #J ) TO #REC-STATE-TAX-WTHLD ( #S,#W1 )
                        pnd_Ws_Pnd_Rec_Loc_Distr.getValue(pnd_Ws_Pnd_S,pnd_Ws_Pnd_W1).nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Distr().getValue(pnd_Ws_Pnd_J));                //Natural: ADD TIRF-LOC-DISTR ( #J ) TO #REC-LOC-DISTR ( #S,#W1 )
                        pnd_Ws_Pnd_Rec_Loc_Tax_Wthld.getValue(pnd_Ws_Pnd_S,pnd_Ws_Pnd_W1).nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Pnd_J));        //Natural: ADD TIRF-LOC-TAX-WTHLD ( #J ) TO #REC-LOC-TAX-WTHLD ( #S,#W1 )
                        pnd_Ws_Pnd_Rec_Counter.getValue(pnd_Ws_Pnd_S,pnd_Ws_Pnd_W1).nadd(1);                                                                              //Natural: ADD 1 TO #REC-COUNTER ( #S,#W1 )
                        getReports().print(4, "Name:",pnd_Ws_Pnd_Name,NEWLINE,"Addr:",pnd_Ws_Pnd_Address);                                                                //Natural: PRINT ( 04 ) 'Name:' #NAME / 'Addr:' #WS.#ADDRESS
                        getReports().display(4, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/TIN",                                               //Natural: DISPLAY ( 04 ) ( HC = R ) '/TIN' #TIRF-TIN ( HC = L ) '/Comp' #TWRACOM2.#COMP-SHORT-NAME '/Contract' #TIRF-CONTRACT-NBR ( HC = L ) 'State/Dist' #TIRF-STATE-DISTR ( EM = -ZZZ,ZZZ,ZZ9.99 ) 'State/WTHLD' #TIRF-STATE-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZ9.99 ) 'Local/Dist' #TIRF-LOC-DISTR ( EM = -ZZ,ZZZ,ZZ9.99 ) 'Local/WTHLD' #TIRF-LOC-TAX-WTHLD ( EM = -Z,ZZZ,ZZ9.99 )
                        		ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Tin(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/Comp",
                        		pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),"/Contract",
                        		ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Contract_Nbr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"State/Dist",
                            
                        		ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_State_Distr(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"State/WTHLD",
                        		ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_State_Tax_Wthld(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),"Local/Dist",
                        		ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Loc_Distr(), new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),"Local/WTHLD",
                        		ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Loc_Tax_Wthld(), new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_WORK"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_WORK"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().print(2, "Name:",pnd_Ws_Pnd_Name,NEWLINE,"Addr:",pnd_Ws_Pnd_Address);                                                                    //Natural: PRINT ( 02 ) 'Name:' #NAME / 'Addr:' #WS.#ADDRESS
                    getReports().display(2, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"//TIN",                                                  //Natural: DISPLAY ( 02 ) ( HC = R ) '//TIN' #TIRF-TIN ( HC = L ) '//Comp' #TWRACOM2.#COMP-SHORT-NAME '//Contract' #TIRF-CONTRACT-NBR ( HC = L ) '/Dist/Code' #TWRADIST.#OUT-DIST ( LC = � ) '/Not/Dtrm' FORM-U.TIRF-TAXABLE-NOT-DET ( LC = � ) '/Federal/Gross' FORM-U.TIRF-GROSS-AMT ( EM = -ZZZZ,ZZZ.99 ) '/Federal/Taxable' FORM-U.TIRF-TAXABLE-AMT ( EM = -ZZZZ,ZZZ.99 ) '/Federal/Tax' FORM-U.TIRF-FED-TAX-WTHLD ( EM = -ZZZZ,ZZZ.99 ) 'State/Gross/Taxable' FORM-U.TIRF-RES-GROSS-AMT ( #J ) ( EM = -ZZZZ,ZZZ.99 ) / '/' FORM-U.TIRF-RES-TAXABLE-AMT ( #J ) ( EM = -ZZZZ,ZZZ.99 ) '/State/Distrib' FORM-U.TIRF-STATE-DISTR ( #J ) ( EM = -ZZZZ,ZZZ.99 ) 'State/Local/Tax' FORM-U.TIRF-STATE-TAX-WTHLD ( #J ) ( EM = -ZZZ,ZZ.99 ) / '/' FORM-U.TIRF-LOC-TAX-WTHLD ( #J ) ( EM = -ZZZ,ZZ.99 ) '//IRR Amt' FORM-U.TIRF-RES-IRR-AMT ( #J ) ( EM = -ZZZZ,ZZZ.99 )
                    		ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Tin(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"//Comp",
                    		pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),"//Contract",
                    		ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Contract_Nbr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/Dist/Code",
                        
                    		pdaTwradist.getPnd_Twradist_Pnd_Out_Dist(), new FieldAttributes("LC=�"),"/Not/Dtrm",
                    		ldaTwrl9705.getForm_U_Tirf_Taxable_Not_Det(), new FieldAttributes("LC=�"),"/Federal/Gross",
                    		ldaTwrl9705.getForm_U_Tirf_Gross_Amt(), new ReportEditMask ("-ZZZZ,ZZZ.99"),"/Federal/Taxable",
                    		ldaTwrl9705.getForm_U_Tirf_Taxable_Amt(), new ReportEditMask ("-ZZZZ,ZZZ.99"),"/Federal/Tax",
                    		ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld(), new ReportEditMask ("-ZZZZ,ZZZ.99"),"State/Gross/Taxable",
                    		ldaTwrl9705.getForm_U_Tirf_Res_Gross_Amt().getValue(pnd_Ws_Pnd_J), new ReportEditMask ("-ZZZZ,ZZZ.99"),NEWLINE,"/",
                    		ldaTwrl9705.getForm_U_Tirf_Res_Taxable_Amt().getValue(pnd_Ws_Pnd_J), new ReportEditMask ("-ZZZZ,ZZZ.99"),"/State/Distrib",
                    		ldaTwrl9705.getForm_U_Tirf_State_Distr().getValue(pnd_Ws_Pnd_J), new ReportEditMask ("-ZZZZ,ZZZ.99"),"State/Local/Tax",
                    		ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld().getValue(pnd_Ws_Pnd_J), new ReportEditMask ("-ZZZ,ZZ.99"),NEWLINE,"/",
                    		ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Pnd_J), new ReportEditMask ("-ZZZ,ZZ.99"),"//IRR Amt",
                    		ldaTwrl9705.getForm_U_Tirf_Res_Irr_Amt().getValue(pnd_Ws_Pnd_J), new ReportEditMask ("-ZZZZ,ZZZ.99"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_WORK"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_WORK"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 02 ) 1
                    pnd_Ws_Pnd_Det_Rpt_Break_Ind.setValue(true);                                                                                                          //Natural: ASSIGN #WS.#DET-RPT-BREAK-IND := TRUE
                    pnd_Ws_Pnd_Det_Rec_Break_Ind.setValue(true);                                                                                                          //Natural: ASSIGN #WS.#DET-REC-BREAK-IND := TRUE
                    rEAD_WORKPnd_Tirf_Company_CdeOld.setValue(ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Company_Cde());                                                        //Natural: END-WORK
                }
                READ_WORK_Exit:
                if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                {
                    atBreakEventRead_Work(endOfDataReadWork);
                }
                if (Global.isEscape()) return;
                pnd_Ws_Pnd_Comp_Line.setValue("Grand Totals");                                                                                                            //Natural: ASSIGN #WS.#COMP-LINE := 'Grand Totals'
                pnd_Ws_Pnd_S.setValue(2);                                                                                                                                 //Natural: ASSIGN #WS.#S := 2
                pnd_Ws_Pnd_Det_Rec_Break_Ind.setValue(true);                                                                                                              //Natural: ASSIGN #WS.#DET-REC-BREAK-IND := TRUE
                //*  NOT EMPTY INPUT FILE
                if (condition(pnd_Ws_Pnd_W.notEquals(getZero())))                                                                                                         //Natural: IF #W NE 0
                {
                                                                                                                                                                          //Natural: PERFORM DETAIL-RECO-BREAK
                    sub_Detail_Reco_Break();
                    if (condition(Global.isEscape())) {return;}
                    //*  PERFORM UPDATE-CONTROL-REC     /* JB01 REMOVE FROM IF SO IT WILL
                }                                                                                                                                                         //Natural: END-IF
                //*  IF #READ-CNT  = 0
                //*                                   CASE IT WAS AN EMPTY STATE FILE
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parm);                                                                                       //Natural: INPUT #INPUT-PARM
                pnd_Ws_Pnd_Header_Tax_Year.setValue(pnd_Input_Parm_Pnd_In_Tax_Year);                                                                                      //Natural: ASSIGN #HEADER-TAX-YEAR := #IN-TAX-YEAR
                pnd_Ws_Pnd_Num_State_Code.setValue(pnd_Input_Parm_Pnd_In_Input_State);                                                                                    //Natural: ASSIGN #NUM-STATE-CODE := #IN-INPUT-STATE
                //*  END-IF
                //*  ALWAYS CREATE CONTROL REC
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-REC
                sub_Update_Control_Rec();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-DATA
                sub_End_Of_Data();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"=",new RepeatItem(131),NEWLINE,new TabSetting(44),"end of report",NEWLINE,new               //Natural: WRITE ( 01 ) 01T '=' ( 131 ) / 44T 'end of report' / 01T '=' ( 131 )
                    TabSetting(1),"=",new RepeatItem(131));
                if (Global.isEscape()) return;
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"=",new RepeatItem(131),NEWLINE,new TabSetting(44),"end of report",NEWLINE,new               //Natural: WRITE ( 02 ) 01T '=' ( 131 ) / 44T 'end of report' / 01T '=' ( 131 )
                    TabSetting(1),"=",new RepeatItem(131));
                if (Global.isEscape()) return;
                getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),"=",new RepeatItem(131),NEWLINE,new TabSetting(44),"end of report",NEWLINE,new               //Natural: WRITE ( 03 ) 01T '=' ( 131 ) / 44T 'end of report' / 01T '=' ( 131 )
                    TabSetting(1),"=",new RepeatItem(131));
                if (Global.isEscape()) return;
                getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),"=",new RepeatItem(131),NEWLINE,new TabSetting(44),"end of report",NEWLINE,new               //Natural: WRITE ( 04 ) 01T '=' ( 131 ) / 44T 'end of report' / 01T '=' ( 131 )
                    TabSetting(1),"=",new RepeatItem(131));
                if (Global.isEscape()) return;
                if (condition(pnd_Ws_Pnd_Terminate.getBoolean()))                                                                                                         //Natural: IF #TERMINATE
                {
                    DbsUtil.terminate(101);  if (true) return;                                                                                                            //Natural: TERMINATE 101
                }                                                                                                                                                         //Natural: END-IF
                //* **********************
                //*  S U B R O U T I N E S
                //* *********************************
                //* *********************************
                //* ********************************
                //* ********************************
                //* ***********************************
                //* ***********************************
                //* ****************************
                //* *****************************
                //* **********************************
                //* ********************
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 01 );//Natural: AT TOP OF PAGE ( 02 );//Natural: AT TOP OF PAGE ( 03 );//Natural: AT TOP OF PAGE ( 04 )
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Get_Company_Info() throws Exception                                                                                                                  //Natural: GET-COMPANY-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue(ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Company_Cde());                                                         //Natural: ASSIGN #TWRACOM2.#COMP-CODE := #TWRL3600.#TIRF-COMPANY-CDE
        pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(1);                                                                                                          //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 01
        pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().setValueEdited(new ReportEditMask("9999"),pnd_Ws_Pnd_Header_Tax_Year);                                                 //Natural: MOVE EDITED #HEADER-TAX-YEAR TO #TWRACOM2.#TAX-YEAR ( EM = 9999 )
        DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOM2' USING #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        pnd_Ws_Pnd_Comp_Line.setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name());                                                                                 //Natural: ASSIGN #WS.#COMP-LINE := #TWRACOM2.#COMP-SHORT-NAME
    }
    private void sub_Get_State_Info() throws Exception                                                                                                                    //Natural: GET-STATE-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                             //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
        pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().setValueEdited(new ReportEditMask("9999"),pnd_Ws_Pnd_Header_Tax_Year);                                                     //Natural: MOVE EDITED #HEADER-TAX-YEAR TO TWRATBL2.#TAX-YEAR ( EM = 9999 )
        pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().setValue(true);                                                                                                           //Natural: ASSIGN TWRATBL2.#ABEND-IND := TWRATBL2.#DISPLAY-IND := TRUE
        pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().setValue(true);
        pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "0", pnd_Ws_Pnd_Num_State_Code));                                //Natural: COMPRESS '0' #NUM-STATE-CODE INTO TWRATBL2.#STATE-CDE LEAVING NO SPACE
        DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        if (condition(pdaTwratbl2.getTwratbl2_Tircntl_Comb_Fed_Ind().notEquals(" ")))                                                                                     //Natural: IF TIRCNTL-COMB-FED-IND NE ' '
        {
            pnd_Ws_Pnd_Combined_Fed_State.setValue("Y");                                                                                                                  //Natural: ASSIGN #COMBINED-FED-STATE := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1009 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF TIRCNTL-STATE-IND;//Natural: VALUE '1'
        if (condition((pdaTwratbl2.getTwratbl2_Tircntl_State_Ind().equals("1"))))
        {
            decideConditionsMet1009++;
            pnd_Ws_Pnd_State_Indicators.setValue("Txbl, All");                                                                                                            //Natural: ASSIGN #STATE-INDICATORS := 'Txbl, All'
        }                                                                                                                                                                 //Natural: VALUE '2'
        else if (condition((pdaTwratbl2.getTwratbl2_Tircntl_State_Ind().equals("2"))))
        {
            decideConditionsMet1009++;
            pnd_Ws_Pnd_State_Indicators.setValue("Txbl, Withhold > 0");                                                                                                   //Natural: ASSIGN #STATE-INDICATORS := 'Txbl, Withhold > 0'
        }                                                                                                                                                                 //Natural: VALUE '3'
        else if (condition((pdaTwratbl2.getTwratbl2_Tircntl_State_Ind().equals("3"))))
        {
            decideConditionsMet1009++;
            pnd_Ws_Pnd_State_Indicators.setValue("Gross, All");                                                                                                           //Natural: ASSIGN #STATE-INDICATORS := 'Gross, All'
        }                                                                                                                                                                 //Natural: VALUE '4'
        else if (condition((pdaTwratbl2.getTwratbl2_Tircntl_State_Ind().equals("4"))))
        {
            decideConditionsMet1009++;
            pnd_Ws_Pnd_State_Indicators.setValue("Gross, Withhold > 0");                                                                                                  //Natural: ASSIGN #STATE-INDICATORS := 'Gross, Withhold > 0'
        }                                                                                                                                                                 //Natural: VALUE '9'
        else if (condition((pdaTwratbl2.getTwratbl2_Tircntl_State_Ind().equals("9"))))
        {
            decideConditionsMet1009++;
            pnd_Ws_Pnd_State_Indicators.setValue("Non Reportable");                                                                                                       //Natural: ASSIGN #STATE-INDICATORS := 'Non Reportable'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Ws_Pnd_State_Name.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Full_Name());                                                                                //Natural: ASSIGN #WS.#STATE-NAME := TIRCNTL-STATE-FULL-NAME
    }
    private void sub_Update_Control_Rec() throws Exception                                                                                                                //Natural: UPDATE-CONTROL-REC
    {
        if (BLNatReinput.isReinput()) return;

        ldaTwrltb4u.getTircntl_Rpt_U_Tircntl_Tbl_Nbr().setValue(4);                                                                                                       //Natural: ASSIGN TIRCNTL-RPT-U.TIRCNTL-TBL-NBR := 4
        ldaTwrltb4u.getTircntl_Rpt_U_Tircntl_Tax_Year().setValueEdited(new ReportEditMask("9999"),pnd_Ws_Pnd_Header_Tax_Year);                                            //Natural: MOVE EDITED #HEADER-TAX-YEAR TO TIRCNTL-RPT-U.TIRCNTL-TAX-YEAR ( EM = 9999 )
        ldaTwrltb4u.getTircntl_Rpt_U_Tircntl_Seq_Nbr().setValue(1);                                                                                                       //Natural: ASSIGN TIRCNTL-RPT-U.TIRCNTL-SEQ-NBR := 1
        pnd_Ws_Pnd_State_Code.setValue(pnd_Ws_Pnd_Num_State_Code);                                                                                                        //Natural: ASSIGN #WS.#STATE-CODE := #NUM-STATE-CODE
        ldaTwrltb4u.getTircntl_Rpt_U_Tircntl_Rpt_Form_Name().setValue(pnd_Ws_Pnd_Rec_Type);                                                                               //Natural: ASSIGN TIRCNTL-RPT-U.TIRCNTL-RPT-FORM-NAME := #WS.#REC-TYPE
        ldaTwrltb4u.getTircntl_Rpt_U_Tircntl_Rpt_Dte().getValue(1).setValue(pnd_Ws_Pnd_Datx);                                                                             //Natural: ASSIGN TIRCNTL-RPT-U.TIRCNTL-RPT-DTE ( 1 ) := #DATX
        ldaTwrltb4u.getVw_tircntl_Rpt_U().insertDBRow();                                                                                                                  //Natural: STORE TIRCNTL-RPT-U
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_End_Of_Data() throws Exception                                                                                                                       //Natural: END-OF-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        pnd_Ws_Pnd_Tot_Gross_Amt.getValue(2).nadd(pnd_Ws_Pnd_Tot_Gross_Amt.getValue(1));                                                                                  //Natural: ADD #TOT-GROSS-AMT ( 1 ) TO #TOT-GROSS-AMT ( 2 )
        pnd_Ws_Pnd_Tot_Taxable_Amt.getValue(2).nadd(pnd_Ws_Pnd_Tot_Taxable_Amt.getValue(1));                                                                              //Natural: ADD #TOT-TAXABLE-AMT ( 1 ) TO #TOT-TAXABLE-AMT ( 2 )
        pnd_Ws_Pnd_Tot_Fed_Tax_Wthld.getValue(2).nadd(pnd_Ws_Pnd_Tot_Fed_Tax_Wthld.getValue(1));                                                                          //Natural: ADD #TOT-FED-TAX-WTHLD ( 1 ) TO #TOT-FED-TAX-WTHLD ( 2 )
        pnd_Ws_Pnd_Tot_Res_Gross_Amt.getValue(2).nadd(pnd_Ws_Pnd_Tot_Res_Gross_Amt.getValue(1));                                                                          //Natural: ADD #TOT-RES-GROSS-AMT ( 1 ) TO #TOT-RES-GROSS-AMT ( 2 )
        pnd_Ws_Pnd_Tot_Res_Taxable_Amt.getValue(2).nadd(pnd_Ws_Pnd_Tot_Res_Taxable_Amt.getValue(1));                                                                      //Natural: ADD #TOT-RES-TAXABLE-AMT ( 1 ) TO #TOT-RES-TAXABLE-AMT ( 2 )
        pnd_Ws_Pnd_Tot_State_Distr.getValue(2).nadd(pnd_Ws_Pnd_Tot_State_Distr.getValue(1));                                                                              //Natural: ADD #TOT-STATE-DISTR ( 1 ) TO #TOT-STATE-DISTR ( 2 )
        pnd_Ws_Pnd_Tot_State_Tax_Wthld.getValue(2).nadd(pnd_Ws_Pnd_Tot_State_Tax_Wthld.getValue(1));                                                                      //Natural: ADD #TOT-STATE-TAX-WTHLD ( 1 ) TO #TOT-STATE-TAX-WTHLD ( 2 )
        pnd_Ws_Pnd_Tot_Loc_Distr.getValue(2).nadd(pnd_Ws_Pnd_Tot_Loc_Distr.getValue(1));                                                                                  //Natural: ADD #TOT-LOC-DISTR ( 1 ) TO #TOT-LOC-DISTR ( 2 )
        pnd_Ws_Pnd_Tot_Loc_Tax_Wthld.getValue(2).nadd(pnd_Ws_Pnd_Tot_Loc_Tax_Wthld.getValue(1));                                                                          //Natural: ADD #TOT-LOC-TAX-WTHLD ( 1 ) TO #TOT-LOC-TAX-WTHLD ( 2 )
        pnd_Ws_Pnd_Tot_Irr_Amt.getValue(2).nadd(pnd_Ws_Pnd_Tot_Irr_Amt.getValue(1));                                                                                      //Natural: ADD #TOT-IRR-AMT ( 1 ) TO #TOT-IRR-AMT ( 2 )
        pnd_Ws_Pnd_Rej_Gross_Amt.getValue(2).nadd(pnd_Ws_Pnd_Rej_Gross_Amt.getValue(1));                                                                                  //Natural: ADD #REJ-GROSS-AMT ( 1 ) TO #REJ-GROSS-AMT ( 2 )
        pnd_Ws_Pnd_Rej_Taxable_Amt.getValue(2).nadd(pnd_Ws_Pnd_Rej_Taxable_Amt.getValue(1));                                                                              //Natural: ADD #REJ-TAXABLE-AMT ( 1 ) TO #REJ-TAXABLE-AMT ( 2 )
        pnd_Ws_Pnd_Rej_Fed_Tax_Wthld.getValue(2).nadd(pnd_Ws_Pnd_Rej_Fed_Tax_Wthld.getValue(1));                                                                          //Natural: ADD #REJ-FED-TAX-WTHLD ( 1 ) TO #REJ-FED-TAX-WTHLD ( 2 )
        pnd_Ws_Pnd_Rej_Res_Gross_Amt.getValue(2).nadd(pnd_Ws_Pnd_Rej_Res_Gross_Amt.getValue(1));                                                                          //Natural: ADD #REJ-RES-GROSS-AMT ( 1 ) TO #REJ-RES-GROSS-AMT ( 2 )
        pnd_Ws_Pnd_Rej_Res_Taxable_Amt.getValue(2).nadd(pnd_Ws_Pnd_Rej_Res_Taxable_Amt.getValue(1));                                                                      //Natural: ADD #REJ-RES-TAXABLE-AMT ( 1 ) TO #REJ-RES-TAXABLE-AMT ( 2 )
        pnd_Ws_Pnd_Rej_State_Distr.getValue(2).nadd(pnd_Ws_Pnd_Rej_State_Distr.getValue(1));                                                                              //Natural: ADD #REJ-STATE-DISTR ( 1 ) TO #REJ-STATE-DISTR ( 2 )
        pnd_Ws_Pnd_Rej_State_Tax_Wthld.getValue(2).nadd(pnd_Ws_Pnd_Rej_State_Tax_Wthld.getValue(1));                                                                      //Natural: ADD #REJ-STATE-TAX-WTHLD ( 1 ) TO #REJ-STATE-TAX-WTHLD ( 2 )
        pnd_Ws_Pnd_Rej_Loc_Distr.getValue(2).nadd(pnd_Ws_Pnd_Rej_Loc_Distr.getValue(1));                                                                                  //Natural: ADD #REJ-LOC-DISTR ( 1 ) TO #REJ-LOC-DISTR ( 2 )
        pnd_Ws_Pnd_Rej_Loc_Tax_Wthld.getValue(2).nadd(pnd_Ws_Pnd_Rej_Loc_Tax_Wthld.getValue(1));                                                                          //Natural: ADD #REJ-LOC-TAX-WTHLD ( 1 ) TO #REJ-LOC-TAX-WTHLD ( 2 )
        pnd_Ws_Pnd_Rej_Irr_Amt.getValue(2).nadd(pnd_Ws_Pnd_Rej_Irr_Amt.getValue(1));                                                                                      //Natural: ADD #REJ-IRR-AMT ( 1 ) TO #REJ-IRR-AMT ( 2 )
        pnd_Ws_Pnd_Rco_Gross_Amt.getValue(2).nadd(pnd_Ws_Pnd_Rco_Gross_Amt.getValue(1));                                                                                  //Natural: ADD #RCO-GROSS-AMT ( 1 ) TO #RCO-GROSS-AMT ( 2 )
        pnd_Ws_Pnd_Rco_Taxable_Amt.getValue(2).nadd(pnd_Ws_Pnd_Rco_Taxable_Amt.getValue(1));                                                                              //Natural: ADD #RCO-TAXABLE-AMT ( 1 ) TO #RCO-TAXABLE-AMT ( 2 )
        pnd_Ws_Pnd_Rco_Fed_Tax_Wthld.getValue(2).nadd(pnd_Ws_Pnd_Rco_Fed_Tax_Wthld.getValue(1));                                                                          //Natural: ADD #RCO-FED-TAX-WTHLD ( 1 ) TO #RCO-FED-TAX-WTHLD ( 2 )
        pnd_Ws_Pnd_Rco_Res_Gross_Amt.getValue(2).nadd(pnd_Ws_Pnd_Rco_Res_Gross_Amt.getValue(1));                                                                          //Natural: ADD #RCO-RES-GROSS-AMT ( 1 ) TO #RCO-RES-GROSS-AMT ( 2 )
        pnd_Ws_Pnd_Rco_Res_Taxable_Amt.getValue(2).nadd(pnd_Ws_Pnd_Rco_Res_Taxable_Amt.getValue(1));                                                                      //Natural: ADD #RCO-RES-TAXABLE-AMT ( 1 ) TO #RCO-RES-TAXABLE-AMT ( 2 )
        pnd_Ws_Pnd_Rco_State_Distr.getValue(2).nadd(pnd_Ws_Pnd_Rco_State_Distr.getValue(1));                                                                              //Natural: ADD #RCO-STATE-DISTR ( 1 ) TO #RCO-STATE-DISTR ( 2 )
        pnd_Ws_Pnd_Rco_State_Tax_Wthld.getValue(2).nadd(pnd_Ws_Pnd_Rco_State_Tax_Wthld.getValue(1));                                                                      //Natural: ADD #RCO-STATE-TAX-WTHLD ( 1 ) TO #RCO-STATE-TAX-WTHLD ( 2 )
        pnd_Ws_Pnd_Rco_Loc_Distr.getValue(2).nadd(pnd_Ws_Pnd_Rco_Loc_Distr.getValue(1));                                                                                  //Natural: ADD #RCO-LOC-DISTR ( 1 ) TO #RCO-LOC-DISTR ( 2 )
        pnd_Ws_Pnd_Rco_Loc_Tax_Wthld.getValue(2).nadd(pnd_Ws_Pnd_Rco_Loc_Tax_Wthld.getValue(1));                                                                          //Natural: ADD #RCO-LOC-TAX-WTHLD ( 1 ) TO #RCO-LOC-TAX-WTHLD ( 2 )
        pnd_Ws_Pnd_Rco_Irr_Amt.getValue(2).nadd(pnd_Ws_Pnd_Rco_Irr_Amt.getValue(1));                                                                                      //Natural: ADD #RCO-IRR-AMT ( 1 ) TO #RCO-IRR-AMT ( 2 )
        pnd_Ws_Pnd_Empty_Form_Count.getValue(2).nadd(pnd_Ws_Pnd_Empty_Form_Count.getValue(1));                                                                            //Natural: ADD #EMPTY-FORM-COUNT ( 1 ) TO #EMPTY-FORM-COUNT ( 2 )
        pnd_Ws_Pnd_Reject_Form_Count.getValue(2).nadd(pnd_Ws_Pnd_Reject_Form_Count.getValue(1));                                                                          //Natural: ADD #REJECT-FORM-COUNT ( 1 ) TO #REJECT-FORM-COUNT ( 2 )
        pnd_Ws_Pnd_Recon_Only.getValue(2).nadd(pnd_Ws_Pnd_Recon_Only.getValue(1));                                                                                        //Natural: ADD #RECON-ONLY ( 1 ) TO #RECON-ONLY ( 2 )
        pnd_Ws_Pnd_Accept_Form_Count.getValue(2).nadd(pnd_Ws_Pnd_Accept_Form_Count.getValue(1));                                                                          //Natural: ADD #ACCEPT-FORM-COUNT ( 1 ) TO #ACCEPT-FORM-COUNT ( 2 )
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"STATE REPORTABLE   ",NEWLINE,"Company................",pnd_Ws_Pnd_Comp_Line,NEWLINE,"Federal Gross..........",pnd_Ws_Pnd_Tot_Gross_Amt.getValue(pnd_Ws_Pnd_S),  //Natural: WRITE ( 01 ) NOTITLE NOHDR / 'STATE REPORTABLE   ' / 'Company................' #WS.#COMP-LINE / 'Federal Gross..........' #TOT-GROSS-AMT ( #S ) / 'Federal Taxable........' #TOT-TAXABLE-AMT ( #S ) / 'Federal Withholding....' #TOT-FED-TAX-WTHLD ( #S ) / 'State Gross............' #TOT-RES-GROSS-AMT ( #S ) / 'State Taxable..........' #TOT-RES-TAXABLE-AMT ( #S ) / 'State Distribution.....' #TOT-STATE-DISTR ( #S ) / 'State Withholding......' #TOT-STATE-TAX-WTHLD ( #S ) / 'Local Distribution.....' #TOT-LOC-DISTR ( #S ) / 'Local Withholding......' #TOT-LOC-TAX-WTHLD ( #S ) / 'IRR Amount.............' #TOT-IRR-AMT ( #S ) / 'State Indicators.......' #STATE-INDICATORS / 'Empty..................' #ZERO / 'Rejected...............' #ZERO / 'Not Reportable.........' #ZERO / 'Reportable.............' #ACCEPT-FORM-COUNT ( #S )
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Federal Taxable........",pnd_Ws_Pnd_Tot_Taxable_Amt.getValue(pnd_Ws_Pnd_S), new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Federal Withholding....",pnd_Ws_Pnd_Tot_Fed_Tax_Wthld.getValue(pnd_Ws_Pnd_S), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Gross............",pnd_Ws_Pnd_Tot_Res_Gross_Amt.getValue(pnd_Ws_Pnd_S), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Taxable..........",pnd_Ws_Pnd_Tot_Res_Taxable_Amt.getValue(pnd_Ws_Pnd_S), new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Distribution.....",pnd_Ws_Pnd_Tot_State_Distr.getValue(pnd_Ws_Pnd_S), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Withholding......",pnd_Ws_Pnd_Tot_State_Tax_Wthld.getValue(pnd_Ws_Pnd_S), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Local Distribution.....",pnd_Ws_Pnd_Tot_Loc_Distr.getValue(pnd_Ws_Pnd_S), new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Local Withholding......",pnd_Ws_Pnd_Tot_Loc_Tax_Wthld.getValue(pnd_Ws_Pnd_S), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IRR Amount.............",pnd_Ws_Pnd_Tot_Irr_Amt.getValue(pnd_Ws_Pnd_S), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Indicators.......",pnd_Ws_Pnd_State_Indicators,NEWLINE,"Empty..................",pnd_Ws_Pnd_Zero, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"Rejected...............",pnd_Ws_Pnd_Zero, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"Not Reportable.........",pnd_Ws_Pnd_Zero, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"Reportable.............",pnd_Ws_Pnd_Accept_Form_Count.getValue(pnd_Ws_Pnd_S), new ReportEditMask 
            ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 01 ) 2
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"NOT REPORTABLE     ",NEWLINE,"Company................",pnd_Ws_Pnd_Comp_Line,NEWLINE,"Federal Gross..........",pnd_Ws_Pnd_Rco_Gross_Amt.getValue(pnd_Ws_Pnd_S),  //Natural: WRITE ( 01 ) NOTITLE NOHDR / 'NOT REPORTABLE     ' / 'Company................' #WS.#COMP-LINE / 'Federal Gross..........' #RCO-GROSS-AMT ( #S ) / 'Federal Taxable........' #RCO-TAXABLE-AMT ( #S ) / 'Federal Withholding....' #RCO-FED-TAX-WTHLD ( #S ) / 'State Gross............' #RCO-RES-GROSS-AMT ( #S ) / 'State Taxable..........' #RCO-RES-TAXABLE-AMT ( #S ) / 'State Distribution.....' #RCO-STATE-DISTR ( #S ) / 'State Withholding......' #RCO-STATE-TAX-WTHLD ( #S ) / 'Local Distribution.....' #RCO-LOC-DISTR ( #S ) / 'Local Withholding......' #RCO-LOC-TAX-WTHLD ( #S ) / 'IRR Amount.............' #RCO-IRR-AMT ( #S ) / 'State Indicators.......' #STATE-INDICATORS / 'Empty..................' #ZERO / 'Rejected...............' #ZERO / 'Not Reportable.........' #RECON-ONLY ( #S ) / 'Reportable.............' #ZERO
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Federal Taxable........",pnd_Ws_Pnd_Rco_Taxable_Amt.getValue(pnd_Ws_Pnd_S), new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Federal Withholding....",pnd_Ws_Pnd_Rco_Fed_Tax_Wthld.getValue(pnd_Ws_Pnd_S), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Gross............",pnd_Ws_Pnd_Rco_Res_Gross_Amt.getValue(pnd_Ws_Pnd_S), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Taxable..........",pnd_Ws_Pnd_Rco_Res_Taxable_Amt.getValue(pnd_Ws_Pnd_S), new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Distribution.....",pnd_Ws_Pnd_Rco_State_Distr.getValue(pnd_Ws_Pnd_S), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Withholding......",pnd_Ws_Pnd_Rco_State_Tax_Wthld.getValue(pnd_Ws_Pnd_S), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Local Distribution.....",pnd_Ws_Pnd_Rco_Loc_Distr.getValue(pnd_Ws_Pnd_S), new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Local Withholding......",pnd_Ws_Pnd_Rco_Loc_Tax_Wthld.getValue(pnd_Ws_Pnd_S), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IRR Amount.............",pnd_Ws_Pnd_Rco_Irr_Amt.getValue(pnd_Ws_Pnd_S), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Indicators.......",pnd_Ws_Pnd_State_Indicators,NEWLINE,"Empty..................",pnd_Ws_Pnd_Zero, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"Rejected...............",pnd_Ws_Pnd_Zero, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"Not Reportable.........",pnd_Ws_Pnd_Recon_Only.getValue(pnd_Ws_Pnd_S), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"Reportable.............",pnd_Ws_Pnd_Zero, new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 01 ) 2
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"REJECTED  ",NEWLINE,"Company................",pnd_Ws_Pnd_Comp_Line,NEWLINE,"Federal Gross..........",pnd_Ws_Pnd_Rej_Gross_Amt.getValue(pnd_Ws_Pnd_S),  //Natural: WRITE ( 01 ) NOTITLE NOHDR / 'REJECTED  ' / 'Company................' #WS.#COMP-LINE / 'Federal Gross..........' #REJ-GROSS-AMT ( #S ) / 'Federal Taxable........' #REJ-TAXABLE-AMT ( #S ) / 'Federal Withholding....' #REJ-FED-TAX-WTHLD ( #S ) / 'State Gross............' #REJ-RES-GROSS-AMT ( #S ) / 'State Taxable..........' #REJ-RES-TAXABLE-AMT ( #S ) / 'State Distribution.....' #REJ-STATE-DISTR ( #S ) / 'State Withholding......' #REJ-STATE-TAX-WTHLD ( #S ) / 'Local Distribution.....' #REJ-LOC-DISTR ( #S ) / 'Local Withholding......' #REJ-LOC-TAX-WTHLD ( #S ) / 'IRR Amount.............' #REJ-IRR-AMT ( #S ) / 'State Indicators.......' #STATE-INDICATORS / 'Empty..................' #EMPTY-FORM-COUNT ( #S ) / 'Rejected...............' #REJECT-FORM-COUNT ( #S ) / 'Not Reportable.........' #ZERO / 'Reportable.............' #ZERO
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Federal Taxable........",pnd_Ws_Pnd_Rej_Taxable_Amt.getValue(pnd_Ws_Pnd_S), new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Federal Withholding....",pnd_Ws_Pnd_Rej_Fed_Tax_Wthld.getValue(pnd_Ws_Pnd_S), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Gross............",pnd_Ws_Pnd_Rej_Res_Gross_Amt.getValue(pnd_Ws_Pnd_S), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Taxable..........",pnd_Ws_Pnd_Rej_Res_Taxable_Amt.getValue(pnd_Ws_Pnd_S), new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Distribution.....",pnd_Ws_Pnd_Rej_State_Distr.getValue(pnd_Ws_Pnd_S), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Withholding......",pnd_Ws_Pnd_Rej_State_Tax_Wthld.getValue(pnd_Ws_Pnd_S), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Local Distribution.....",pnd_Ws_Pnd_Rej_Loc_Distr.getValue(pnd_Ws_Pnd_S), new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Local Withholding......",pnd_Ws_Pnd_Rej_Loc_Tax_Wthld.getValue(pnd_Ws_Pnd_S), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IRR Amount.............",pnd_Ws_Pnd_Rej_Irr_Amt.getValue(pnd_Ws_Pnd_S), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Indicators.......",pnd_Ws_Pnd_State_Indicators,NEWLINE,"Empty..................",pnd_Ws_Pnd_Empty_Form_Count.getValue(pnd_Ws_Pnd_S), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"Rejected...............",pnd_Ws_Pnd_Reject_Form_Count.getValue(pnd_Ws_Pnd_S), new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,"Not Reportable.........",pnd_Ws_Pnd_Zero, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"Reportable.............",pnd_Ws_Pnd_Zero, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 01 ) 2
        pnd_Ws_Pnd_Rco_Gross_Amt.getValue(pnd_Ws_Pnd_S).nadd(pnd_Ws_Pnd_Tot_Gross_Amt.getValue(pnd_Ws_Pnd_S));                                                            //Natural: ADD #TOT-GROSS-AMT ( #S ) TO #RCO-GROSS-AMT ( #S )
        pnd_Ws_Pnd_Rco_Taxable_Amt.getValue(pnd_Ws_Pnd_S).nadd(pnd_Ws_Pnd_Tot_Taxable_Amt.getValue(pnd_Ws_Pnd_S));                                                        //Natural: ADD #TOT-TAXABLE-AMT ( #S ) TO #RCO-TAXABLE-AMT ( #S )
        pnd_Ws_Pnd_Rco_Fed_Tax_Wthld.getValue(pnd_Ws_Pnd_S).nadd(pnd_Ws_Pnd_Tot_Fed_Tax_Wthld.getValue(pnd_Ws_Pnd_S));                                                    //Natural: ADD #TOT-FED-TAX-WTHLD ( #S ) TO #RCO-FED-TAX-WTHLD ( #S )
        pnd_Ws_Pnd_Rco_Res_Gross_Amt.getValue(pnd_Ws_Pnd_S).nadd(pnd_Ws_Pnd_Tot_Res_Gross_Amt.getValue(pnd_Ws_Pnd_S));                                                    //Natural: ADD #TOT-RES-GROSS-AMT ( #S ) TO #RCO-RES-GROSS-AMT ( #S )
        pnd_Ws_Pnd_Rco_Res_Taxable_Amt.getValue(pnd_Ws_Pnd_S).nadd(pnd_Ws_Pnd_Tot_Res_Taxable_Amt.getValue(pnd_Ws_Pnd_S));                                                //Natural: ADD #TOT-RES-TAXABLE-AMT ( #S ) TO #RCO-RES-TAXABLE-AMT ( #S )
        pnd_Ws_Pnd_Rco_State_Distr.getValue(pnd_Ws_Pnd_S).nadd(pnd_Ws_Pnd_Tot_State_Distr.getValue(pnd_Ws_Pnd_S));                                                        //Natural: ADD #TOT-STATE-DISTR ( #S ) TO #RCO-STATE-DISTR ( #S )
        pnd_Ws_Pnd_Rco_State_Tax_Wthld.getValue(pnd_Ws_Pnd_S).nadd(pnd_Ws_Pnd_Tot_State_Tax_Wthld.getValue(pnd_Ws_Pnd_S));                                                //Natural: ADD #TOT-STATE-TAX-WTHLD ( #S ) TO #RCO-STATE-TAX-WTHLD ( #S )
        pnd_Ws_Pnd_Rco_Loc_Distr.getValue(pnd_Ws_Pnd_S).nadd(pnd_Ws_Pnd_Tot_Loc_Distr.getValue(pnd_Ws_Pnd_S));                                                            //Natural: ADD #TOT-LOC-DISTR ( #S ) TO #RCO-LOC-DISTR ( #S )
        pnd_Ws_Pnd_Rco_Loc_Tax_Wthld.getValue(pnd_Ws_Pnd_S).nadd(pnd_Ws_Pnd_Tot_Loc_Tax_Wthld.getValue(pnd_Ws_Pnd_S));                                                    //Natural: ADD #TOT-LOC-TAX-WTHLD ( #S ) TO #RCO-LOC-TAX-WTHLD ( #S )
        pnd_Ws_Pnd_Rco_Irr_Amt.getValue(pnd_Ws_Pnd_S).nadd(pnd_Ws_Pnd_Tot_Irr_Amt.getValue(pnd_Ws_Pnd_S));                                                                //Natural: ADD #TOT-IRR-AMT ( #S ) TO #RCO-IRR-AMT ( #S )
        pnd_Ws_Pnd_Rco_Gross_Amt.getValue(pnd_Ws_Pnd_S).nadd(pnd_Ws_Pnd_Rej_Gross_Amt.getValue(pnd_Ws_Pnd_S));                                                            //Natural: ADD #REJ-GROSS-AMT ( #S ) TO #RCO-GROSS-AMT ( #S )
        pnd_Ws_Pnd_Rco_Taxable_Amt.getValue(pnd_Ws_Pnd_S).nadd(pnd_Ws_Pnd_Rej_Taxable_Amt.getValue(pnd_Ws_Pnd_S));                                                        //Natural: ADD #REJ-TAXABLE-AMT ( #S ) TO #RCO-TAXABLE-AMT ( #S )
        pnd_Ws_Pnd_Rco_Fed_Tax_Wthld.getValue(pnd_Ws_Pnd_S).nadd(pnd_Ws_Pnd_Rej_Fed_Tax_Wthld.getValue(pnd_Ws_Pnd_S));                                                    //Natural: ADD #REJ-FED-TAX-WTHLD ( #S ) TO #RCO-FED-TAX-WTHLD ( #S )
        pnd_Ws_Pnd_Rco_Res_Gross_Amt.getValue(pnd_Ws_Pnd_S).nadd(pnd_Ws_Pnd_Rej_Res_Gross_Amt.getValue(pnd_Ws_Pnd_S));                                                    //Natural: ADD #REJ-RES-GROSS-AMT ( #S ) TO #RCO-RES-GROSS-AMT ( #S )
        pnd_Ws_Pnd_Rco_Res_Taxable_Amt.getValue(pnd_Ws_Pnd_S).nadd(pnd_Ws_Pnd_Rej_Res_Taxable_Amt.getValue(pnd_Ws_Pnd_S));                                                //Natural: ADD #REJ-RES-TAXABLE-AMT ( #S ) TO #RCO-RES-TAXABLE-AMT ( #S )
        pnd_Ws_Pnd_Rco_State_Distr.getValue(pnd_Ws_Pnd_S).nadd(pnd_Ws_Pnd_Rej_State_Distr.getValue(pnd_Ws_Pnd_S));                                                        //Natural: ADD #REJ-STATE-DISTR ( #S ) TO #RCO-STATE-DISTR ( #S )
        pnd_Ws_Pnd_Rco_State_Tax_Wthld.getValue(pnd_Ws_Pnd_S).nadd(pnd_Ws_Pnd_Rej_State_Tax_Wthld.getValue(pnd_Ws_Pnd_S));                                                //Natural: ADD #REJ-STATE-TAX-WTHLD ( #S ) TO #RCO-STATE-TAX-WTHLD ( #S )
        pnd_Ws_Pnd_Rco_Loc_Distr.getValue(pnd_Ws_Pnd_S).nadd(pnd_Ws_Pnd_Rej_Loc_Distr.getValue(pnd_Ws_Pnd_S));                                                            //Natural: ADD #REJ-LOC-DISTR ( #S ) TO #RCO-LOC-DISTR ( #S )
        pnd_Ws_Pnd_Rco_Loc_Tax_Wthld.getValue(pnd_Ws_Pnd_S).nadd(pnd_Ws_Pnd_Rej_Loc_Tax_Wthld.getValue(pnd_Ws_Pnd_S));                                                    //Natural: ADD #REJ-LOC-TAX-WTHLD ( #S ) TO #RCO-LOC-TAX-WTHLD ( #S )
        pnd_Ws_Pnd_Rco_Irr_Amt.getValue(pnd_Ws_Pnd_S).nadd(pnd_Ws_Pnd_Rej_Irr_Amt.getValue(pnd_Ws_Pnd_S));                                                                //Natural: ADD #REJ-IRR-AMT ( #S ) TO #RCO-IRR-AMT ( #S )
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTALS:",NEWLINE,"Company................",pnd_Ws_Pnd_Comp_Line,NEWLINE,"Federal Gross..........",pnd_Ws_Pnd_Rco_Gross_Amt.getValue(pnd_Ws_Pnd_S),  //Natural: WRITE ( 01 ) / 'TOTALS:' / 'Company................' #WS.#COMP-LINE / 'Federal Gross..........' #RCO-GROSS-AMT ( #S ) / 'Federal Taxable........' #RCO-TAXABLE-AMT ( #S ) / 'Federal Withholding....' #RCO-FED-TAX-WTHLD ( #S ) / 'State Gross............' #RCO-RES-GROSS-AMT ( #S ) / 'State Taxable..........' #RCO-RES-TAXABLE-AMT ( #S ) / 'State Distribution.....' #RCO-STATE-DISTR ( #S ) / 'State Withholding......' #RCO-STATE-TAX-WTHLD ( #S ) / 'Local Distribution.....' #RCO-LOC-DISTR ( #S ) / 'Local Withholding......' #RCO-LOC-TAX-WTHLD ( #S ) / 'IRR Amount.............' #RCO-IRR-AMT ( #S ) / 'State Indicators.......' #STATE-INDICATORS / 'Empty..................' #EMPTY-FORM-COUNT ( #S ) / 'Rejected...............' #REJECT-FORM-COUNT ( #S ) / 'Not Reportable.........' #RECON-ONLY ( #S ) / 'Reportable.............' #ACCEPT-FORM-COUNT ( #S )
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Federal Taxable........",pnd_Ws_Pnd_Rco_Taxable_Amt.getValue(pnd_Ws_Pnd_S), new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Federal Withholding....",pnd_Ws_Pnd_Rco_Fed_Tax_Wthld.getValue(pnd_Ws_Pnd_S), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Gross............",pnd_Ws_Pnd_Rco_Res_Gross_Amt.getValue(pnd_Ws_Pnd_S), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Taxable..........",pnd_Ws_Pnd_Rco_Res_Taxable_Amt.getValue(pnd_Ws_Pnd_S), new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Distribution.....",pnd_Ws_Pnd_Rco_State_Distr.getValue(pnd_Ws_Pnd_S), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Withholding......",pnd_Ws_Pnd_Rco_State_Tax_Wthld.getValue(pnd_Ws_Pnd_S), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Local Distribution.....",pnd_Ws_Pnd_Rco_Loc_Distr.getValue(pnd_Ws_Pnd_S), new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Local Withholding......",pnd_Ws_Pnd_Rco_Loc_Tax_Wthld.getValue(pnd_Ws_Pnd_S), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IRR Amount.............",pnd_Ws_Pnd_Rco_Irr_Amt.getValue(pnd_Ws_Pnd_S), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Indicators.......",pnd_Ws_Pnd_State_Indicators,NEWLINE,"Empty..................",pnd_Ws_Pnd_Empty_Form_Count.getValue(pnd_Ws_Pnd_S), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"Rejected...............",pnd_Ws_Pnd_Reject_Form_Count.getValue(pnd_Ws_Pnd_S), new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,"Not Reportable.........",pnd_Ws_Pnd_Recon_Only.getValue(pnd_Ws_Pnd_S), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"Reportable.............",pnd_Ws_Pnd_Accept_Form_Count.getValue(pnd_Ws_Pnd_S), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Tot_Gross_Amt.getValue(1).reset();                                                                                                                     //Natural: RESET #TOT-GROSS-AMT ( 1 ) #TOT-TAXABLE-AMT ( 1 ) #TOT-FED-TAX-WTHLD ( 1 ) #TOT-RES-GROSS-AMT ( 1 ) #TOT-RES-TAXABLE-AMT ( 1 ) #TOT-STATE-DISTR ( 1 ) #TOT-STATE-TAX-WTHLD ( 1 ) #TOT-LOC-DISTR ( 1 ) #TOT-LOC-TAX-WTHLD ( 1 ) #TOT-IRR-AMT ( 1 ) #REJ-GROSS-AMT ( 1 ) #REJ-TAXABLE-AMT ( 1 ) #REJ-FED-TAX-WTHLD ( 1 ) #REJ-RES-GROSS-AMT ( 1 ) #REJ-RES-TAXABLE-AMT ( 1 ) #REJ-STATE-DISTR ( 1 ) #REJ-STATE-TAX-WTHLD ( 1 ) #REJ-LOC-DISTR ( 1 ) #REJ-LOC-TAX-WTHLD ( 1 ) #RCO-GROSS-AMT ( 1 ) #RCO-TAXABLE-AMT ( 1 ) #RCO-FED-TAX-WTHLD ( 1 ) #RCO-RES-GROSS-AMT ( 1 ) #RCO-RES-TAXABLE-AMT ( 1 ) #RCO-STATE-DISTR ( 1 ) #RCO-STATE-TAX-WTHLD ( 1 ) #RCO-LOC-DISTR ( 1 ) #RCO-LOC-TAX-WTHLD ( 1 ) #RCO-IRR-AMT ( 1 ) #EMPTY-FORM-COUNT ( 1 ) #REJECT-FORM-COUNT ( 1 ) #RECON-ONLY ( 1 ) #ACCEPT-FORM-COUNT ( 1 )
        pnd_Ws_Pnd_Tot_Taxable_Amt.getValue(1).reset();
        pnd_Ws_Pnd_Tot_Fed_Tax_Wthld.getValue(1).reset();
        pnd_Ws_Pnd_Tot_Res_Gross_Amt.getValue(1).reset();
        pnd_Ws_Pnd_Tot_Res_Taxable_Amt.getValue(1).reset();
        pnd_Ws_Pnd_Tot_State_Distr.getValue(1).reset();
        pnd_Ws_Pnd_Tot_State_Tax_Wthld.getValue(1).reset();
        pnd_Ws_Pnd_Tot_Loc_Distr.getValue(1).reset();
        pnd_Ws_Pnd_Tot_Loc_Tax_Wthld.getValue(1).reset();
        pnd_Ws_Pnd_Tot_Irr_Amt.getValue(1).reset();
        pnd_Ws_Pnd_Rej_Gross_Amt.getValue(1).reset();
        pnd_Ws_Pnd_Rej_Taxable_Amt.getValue(1).reset();
        pnd_Ws_Pnd_Rej_Fed_Tax_Wthld.getValue(1).reset();
        pnd_Ws_Pnd_Rej_Res_Gross_Amt.getValue(1).reset();
        pnd_Ws_Pnd_Rej_Res_Taxable_Amt.getValue(1).reset();
        pnd_Ws_Pnd_Rej_State_Distr.getValue(1).reset();
        pnd_Ws_Pnd_Rej_State_Tax_Wthld.getValue(1).reset();
        pnd_Ws_Pnd_Rej_Loc_Distr.getValue(1).reset();
        pnd_Ws_Pnd_Rej_Loc_Tax_Wthld.getValue(1).reset();
        pnd_Ws_Pnd_Rco_Gross_Amt.getValue(1).reset();
        pnd_Ws_Pnd_Rco_Taxable_Amt.getValue(1).reset();
        pnd_Ws_Pnd_Rco_Fed_Tax_Wthld.getValue(1).reset();
        pnd_Ws_Pnd_Rco_Res_Gross_Amt.getValue(1).reset();
        pnd_Ws_Pnd_Rco_Res_Taxable_Amt.getValue(1).reset();
        pnd_Ws_Pnd_Rco_State_Distr.getValue(1).reset();
        pnd_Ws_Pnd_Rco_State_Tax_Wthld.getValue(1).reset();
        pnd_Ws_Pnd_Rco_Loc_Distr.getValue(1).reset();
        pnd_Ws_Pnd_Rco_Loc_Tax_Wthld.getValue(1).reset();
        pnd_Ws_Pnd_Rco_Irr_Amt.getValue(1).reset();
        pnd_Ws_Pnd_Empty_Form_Count.getValue(1).reset();
        pnd_Ws_Pnd_Reject_Form_Count.getValue(1).reset();
        pnd_Ws_Pnd_Recon_Only.getValue(1).reset();
        pnd_Ws_Pnd_Accept_Form_Count.getValue(1).reset();
    }
    private void sub_Detail_Break() throws Exception                                                                                                                      //Natural: DETAIL-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        if (condition(pnd_Ws_Pnd_Det_Rpt_Break_Ind.getBoolean()))                                                                                                         //Natural: IF #WS.#DET-RPT-BREAK-IND
        {
            getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(131),NEWLINE,"TOTALS:",NEWLINE,"Federal Gross..........",pnd_Ws_Pnd_Det_Gross_Amt,              //Natural: WRITE ( 02 ) '-' ( 131 ) / 'TOTALS:' / 'Federal Gross..........' #DET-GROSS-AMT / 'Federal Taxable........' #DET-TAXABLE-AMT / 'Federal Withholding....' #DET-FED-TAX-WTHLD / 'State Gross............' #DET-RES-GROSS-AMT / 'State Taxable..........' #DET-RES-TAXABLE-AMT / 'State Distribution.....' #DET-STATE-DISTR / 'State Withholding......' #DET-STATE-TAX-WTHLD / 'Local Distribution.....' #DET-LOC-DISTR / 'State IRR Amount.......' #DET-IRR-AMT / 'Local Withholding......' #DET-LOC-TAX-WTHLD
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Federal Taxable........",pnd_Ws_Pnd_Det_Taxable_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Federal Withholding....",pnd_Ws_Pnd_Det_Fed_Tax_Wthld, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Gross............",pnd_Ws_Pnd_Det_Res_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Taxable..........",pnd_Ws_Pnd_Det_Res_Taxable_Amt, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Distribution.....",pnd_Ws_Pnd_Det_State_Distr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Withholding......",pnd_Ws_Pnd_Det_State_Tax_Wthld, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Local Distribution.....",pnd_Ws_Pnd_Det_Loc_Distr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State IRR Amount.......",pnd_Ws_Pnd_Det_Irr_Amt, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Local Withholding......",pnd_Ws_Pnd_Det_Loc_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Ws_Pnd_Det_Gross_Amt.reset();                                                                                                                             //Natural: RESET #DET-GROSS-AMT #DET-TAXABLE-AMT #DET-FED-TAX-WTHLD #DET-RES-GROSS-AMT #DET-RES-TAXABLE-AMT #DET-STATE-DISTR #DET-STATE-TAX-WTHLD #DET-LOC-DISTR #DET-IRR-AMT #DET-LOC-TAX-WTHLD #WS.#DET-RPT-BREAK-IND
            pnd_Ws_Pnd_Det_Taxable_Amt.reset();
            pnd_Ws_Pnd_Det_Fed_Tax_Wthld.reset();
            pnd_Ws_Pnd_Det_Res_Gross_Amt.reset();
            pnd_Ws_Pnd_Det_Res_Taxable_Amt.reset();
            pnd_Ws_Pnd_Det_State_Distr.reset();
            pnd_Ws_Pnd_Det_State_Tax_Wthld.reset();
            pnd_Ws_Pnd_Det_Loc_Distr.reset();
            pnd_Ws_Pnd_Det_Irr_Amt.reset();
            pnd_Ws_Pnd_Det_Loc_Tax_Wthld.reset();
            pnd_Ws_Pnd_Det_Rpt_Break_Ind.reset();
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Detail_Reco_Break() throws Exception                                                                                                                 //Natural: DETAIL-RECO-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        if (condition(pnd_Ws_Pnd_Det_Rec_Break_Ind.getBoolean()))                                                                                                         //Natural: IF #WS.#DET-REC-BREAK-IND
        {
            //*  MS 06/16/05
            if (condition(pnd_Ws_Pnd_W.equals(getZero())))                                                                                                                //Natural: IF #W = 0
            {
                pnd_Ws_Pnd_W.setValue(pnd_Ws_Pnd_W1);                                                                                                                     //Natural: ASSIGN #W := #W1
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(4, ReportOption.NOTITLE,"-",new RepeatItem(131),NEWLINE,"Totals for:",pnd_Ws_Pnd_Comp_Line,new TabSetting(26),pnd_Ws_Pnd_Rec_State_Distr.getValue(pnd_Ws_Pnd_S,pnd_Ws_Pnd_W),  //Natural: WRITE ( 04 ) '-' ( 131 ) / 'Totals for:' #WS.#COMP-LINE 26T #REC-STATE-DISTR ( #S,#W ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) #REC-STATE-TAX-WTHLD ( #S,#W ) ( EM = -ZZZZZ,ZZ9.99 ) #REC-LOC-DISTR ( #S,#W ) ( EM = -ZZ,ZZZ,ZZ9.99 ) #REC-LOC-TAX-WTHLD ( #S,#W ) ( EM = -Z,ZZZ,ZZ9.99 )
                new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),pnd_Ws_Pnd_Rec_State_Tax_Wthld.getValue(pnd_Ws_Pnd_S,pnd_Ws_Pnd_W), new ReportEditMask ("-ZZZZZ,ZZ9.99"),pnd_Ws_Pnd_Rec_Loc_Distr.getValue(pnd_Ws_Pnd_S,pnd_Ws_Pnd_W), 
                new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),pnd_Ws_Pnd_Rec_Loc_Tax_Wthld.getValue(pnd_Ws_Pnd_S,pnd_Ws_Pnd_W), new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            if (condition(pnd_Ws_Pnd_S.equals(1)))                                                                                                                        //Natural: IF #S = 1
            {
                pnd_Ws_Pnd_Rec_State_Distr.getValue(pnd_Ws_Pnd_S,3).nadd(pnd_Ws_Pnd_Rec_State_Distr.getValue(pnd_Ws_Pnd_S,pnd_Ws_Pnd_W));                                 //Natural: ADD #REC-STATE-DISTR ( #S,#W ) TO #REC-STATE-DISTR ( #S,3 )
                pnd_Ws_Pnd_Rec_State_Tax_Wthld.getValue(pnd_Ws_Pnd_S,3).nadd(pnd_Ws_Pnd_Rec_State_Tax_Wthld.getValue(pnd_Ws_Pnd_S,pnd_Ws_Pnd_W));                         //Natural: ADD #REC-STATE-TAX-WTHLD ( #S,#W ) TO #REC-STATE-TAX-WTHLD ( #S,3 )
                pnd_Ws_Pnd_Rec_Loc_Distr.getValue(pnd_Ws_Pnd_S,3).nadd(pnd_Ws_Pnd_Rec_Loc_Distr.getValue(pnd_Ws_Pnd_S,pnd_Ws_Pnd_W));                                     //Natural: ADD #REC-LOC-DISTR ( #S,#W ) TO #REC-LOC-DISTR ( #S,3 )
                pnd_Ws_Pnd_Rec_Loc_Tax_Wthld.getValue(pnd_Ws_Pnd_S,3).nadd(pnd_Ws_Pnd_Rec_Loc_Tax_Wthld.getValue(pnd_Ws_Pnd_S,pnd_Ws_Pnd_W));                             //Natural: ADD #REC-LOC-TAX-WTHLD ( #S,#W ) TO #REC-LOC-TAX-WTHLD ( #S,3 )
                pnd_Ws_Pnd_Rec_Counter.getValue(pnd_Ws_Pnd_S,3).nadd(pnd_Ws_Pnd_Rec_Counter.getValue(pnd_Ws_Pnd_S,pnd_Ws_Pnd_W));                                         //Natural: ADD #REC-COUNTER ( #S,#W ) TO #REC-COUNTER ( #S,3 )
                getReports().newPage(new ReportSpecification(4));                                                                                                         //Natural: NEWPAGE ( 04 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"Withholding Information -",pnd_Ws_Pnd_Comp_Line,NEWLINE);                         //Natural: WRITE ( 03 ) /// / 'Withholding Information -' #WS.#COMP-LINE /
            if (Global.isEscape()) return;
            FOR02:                                                                                                                                                        //Natural: FOR #I = 1 TO 3
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(3)); pnd_Ws_Pnd_I.nadd(1))
            {
                getReports().display(3, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",                                                          //Natural: DISPLAY ( 03 ) ( HC = R ) '/' #REC-TYPE-DESC ( #I ) 'State/Dist' #REC-STATE-DISTR ( #S,#I ) ( EM = -Z,ZZZ,ZZZ,ZZ9.99 ) 'State/Wthld' #REC-STATE-TAX-WTHLD ( #S,#I ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 'Local/Dist' #REC-LOC-DISTR ( #S,#I ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 'Local/Wthld' #REC-LOC-TAX-WTHLD ( #S,#I ) ( EM = -ZZ,ZZZ,ZZ9.99 ) '/Counter' #REC-COUNTER ( #S,#I ) ( EM = -Z,ZZZ,ZZ9 )
                		pnd_Ws_Pnd_Rec_Type_Desc.getValue(pnd_Ws_Pnd_I),"State/Dist",
                		pnd_Ws_Pnd_Rec_State_Distr.getValue(pnd_Ws_Pnd_S,pnd_Ws_Pnd_I), new ReportEditMask ("-Z,ZZZ,ZZZ,ZZ9.99"),"State/Wthld",
                		pnd_Ws_Pnd_Rec_State_Tax_Wthld.getValue(pnd_Ws_Pnd_S,pnd_Ws_Pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"Local/Dist",
                		pnd_Ws_Pnd_Rec_Loc_Distr.getValue(pnd_Ws_Pnd_S,pnd_Ws_Pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"Local/Wthld",
                		pnd_Ws_Pnd_Rec_Loc_Tax_Wthld.getValue(pnd_Ws_Pnd_S,pnd_Ws_Pnd_I), new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),"/Counter",
                		pnd_Ws_Pnd_Rec_Counter.getValue(pnd_Ws_Pnd_S,pnd_Ws_Pnd_I), new ReportEditMask ("-Z,ZZZ,ZZ9"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Ws_Pnd_Rec_State_Distr.getValue(2,"*").nadd(pnd_Ws_Pnd_Rec_State_Distr.getValue(1,"*"));                                                                  //Natural: ADD #REC-STATE-DISTR ( 1,* ) TO #REC-STATE-DISTR ( 2,* )
            pnd_Ws_Pnd_Rec_State_Tax_Wthld.getValue(2,"*").nadd(pnd_Ws_Pnd_Rec_State_Tax_Wthld.getValue(1,"*"));                                                          //Natural: ADD #REC-STATE-TAX-WTHLD ( 1,* ) TO #REC-STATE-TAX-WTHLD ( 2,* )
            pnd_Ws_Pnd_Rec_Loc_Distr.getValue(2,"*").nadd(pnd_Ws_Pnd_Rec_Loc_Distr.getValue(1,"*"));                                                                      //Natural: ADD #REC-LOC-DISTR ( 1,* ) TO #REC-LOC-DISTR ( 2,* )
            pnd_Ws_Pnd_Rec_Loc_Tax_Wthld.getValue(2,"*").nadd(pnd_Ws_Pnd_Rec_Loc_Tax_Wthld.getValue(1,"*"));                                                              //Natural: ADD #REC-LOC-TAX-WTHLD ( 1,* ) TO #REC-LOC-TAX-WTHLD ( 2,* )
            pnd_Ws_Pnd_Rec_Counter.getValue(2,"*").nadd(pnd_Ws_Pnd_Rec_Counter.getValue(1,"*"));                                                                          //Natural: ADD #REC-COUNTER ( 1,* ) TO #REC-COUNTER ( 2,* )
            pnd_Ws_Pnd_Det_Rec_Break_Ind.reset();                                                                                                                         //Natural: RESET #WS.#DET-REC-BREAK-IND #REC-STATE-DISTR ( 1,* ) #REC-STATE-TAX-WTHLD ( 1,* ) #REC-LOC-DISTR ( 1,* ) #REC-LOC-TAX-WTHLD ( 1,* ) #REC-COUNTER ( 1,* )
            pnd_Ws_Pnd_Rec_State_Distr.getValue(1,"*").reset();
            pnd_Ws_Pnd_Rec_State_Tax_Wthld.getValue(1,"*").reset();
            pnd_Ws_Pnd_Rec_Loc_Distr.getValue(1,"*").reset();
            pnd_Ws_Pnd_Rec_Loc_Tax_Wthld.getValue(1,"*").reset();
            pnd_Ws_Pnd_Rec_Counter.getValue(1,"*").reset();
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Hardcopy() throws Exception                                                                                                                    //Natural: PRINT-HARDCOPY
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************
        if (condition(ldaTwrl9705.getForm_U_Tirf_State_Hardcopy_Ind().getValue(pnd_Ws_Pnd_J).equals("P")))                                                                //Natural: IF TIRF-STATE-HARDCOPY-IND ( #J ) = 'P'
        {
            pnd_Ws_Pnd_Process_Type.setValue("B");                                                                                                                        //Natural: ASSIGN #PROCESS-TYPE := 'B'
            pnd_Ws_Pnd_Form_Page_Cnt.setValue(pnd_Ws_Pnd_J);                                                                                                              //Natural: ASSIGN #FORM-PAGE-CNT := #J
            pnd_Ws_Pnd_Ws_Isn.setValue(ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Isn());                                                                                       //Natural: ASSIGN #WS-ISN := #TIRF-ISN
            getWorkFiles().write(2, false, pnd_Ws_Pnd_State_Name, pnd_Ws_Pnd_Form_Page_Cnt, pnd_Ws_Pnd_Ws_Isn);                                                           //Natural: WRITE WORK FILE 02 #STATE-NAME #FORM-PAGE-CNT #WS-ISN
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 0 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 0 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 0 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new               //Natural: WRITE ( 01 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'Tax Withholding Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 49T 'Original State Summary Reporting For:' TIRCNTL-STATE-ALPHA-CODE 120T 'Report: RPT1' / 62T 'Tax year:' #HEADER-TAX-YEAR /
                        TabSetting(53),"Tax Withholding Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask 
                        ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(49),"Original State Summary Reporting For:",pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code(),new 
                        TabSetting(120),"Report: RPT1",NEWLINE,new TabSetting(62),"Tax year:",pnd_Ws_Pnd_Header_Tax_Year,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new               //Natural: WRITE ( 02 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'Tax Withholding Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 50T 'Original State Detail Reporting For:' TIRCNTL-STATE-ALPHA-CODE 120T 'Report: RPT2' / 62T 'Tax year:' #HEADER-TAX-YEAR / / #FORM-DESC /
                        TabSetting(53),"Tax Withholding Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask 
                        ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(50),"Original State Detail Reporting For:",pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code(),new 
                        TabSetting(120),"Report: RPT2",NEWLINE,new TabSetting(62),"Tax year:",pnd_Ws_Pnd_Header_Tax_Year,NEWLINE,NEWLINE,pnd_Ws_Pnd_Form_Desc,
                        NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new               //Natural: WRITE ( 03 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'Tax Withholding Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'Original State Reconciliation Summary For:' TIRCNTL-STATE-ALPHA-CODE 120T 'Report: RPT3' / 62T 'Tax year:' #HEADER-TAX-YEAR /
                        TabSetting(53),"Tax Withholding Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(3), new ReportEditMask 
                        ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(46),"Original State Reconciliation Summary For:",pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code(),new 
                        TabSetting(120),"Report: RPT3",NEWLINE,new TabSetting(62),"Tax year:",pnd_Ws_Pnd_Header_Tax_Year,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(4, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new               //Natural: WRITE ( 04 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'Tax Withholding Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T 'Original State Reconciliation Detail For:' TIRCNTL-STATE-ALPHA-CODE 120T 'Report: RPT4' / 62T 'Tax year:' #HEADER-TAX-YEAR / / #FORM-DESC2 /
                        TabSetting(53),"Tax Withholding Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(4), new ReportEditMask 
                        ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(47),"Original State Reconciliation Detail For:",pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code(),new 
                        TabSetting(120),"Report: RPT4",NEWLINE,new TabSetting(62),"Tax year:",pnd_Ws_Pnd_Header_Tax_Year,NEWLINE,NEWLINE,pnd_Ws_Pnd_Form_Desc2,
                        NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventRead_Work() throws Exception {atBreakEventRead_Work(false);}
    private void atBreakEventRead_Work(boolean endOfData) throws Exception
    {
        boolean ldaTwrl3600_getPnd_Twrl3600_Pnd_Tirf_Company_CdeIsBreak = ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Company_Cde().isBreak(endOfData);
        if (condition(ldaTwrl3600_getPnd_Twrl3600_Pnd_Tirf_Company_CdeIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM DETAIL-BREAK
            sub_Detail_Break();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DETAIL-RECO-BREAK
            sub_Detail_Reco_Break();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-DATA
            sub_End_Of_Data();
            if (condition(Global.isEscape())) {return;}
            //*  EOF
            if (condition(rEAD_WORKPnd_Tirf_Company_CdeOld.notEquals(" ") && ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Company_Cde().equals(" ")))                             //Natural: IF OLD ( #TWRL3600.#TIRF-COMPANY-CDE ) NE ' ' AND #TWRL3600.#TIRF-COMPANY-CDE = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 02 )
                if (condition(Global.isEscape())){return;}
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-INFO
                sub_Get_Company_Info();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133 ZP=ON");
        Global.format(1, "PS=60 LS=133 ZP=ON");
        Global.format(2, "PS=60 LS=133 ZP=ON");
        Global.format(3, "PS=60 LS=133 ZP=ON");
        Global.format(4, "PS=60 LS=133 ZP=ON");

        getReports().setDisplayColumns(4, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/TIN",
        		ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Tin(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/Comp",
        		pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),"/Contract",
        		ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Contract_Nbr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"State/Dist",
        		ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_State_Distr(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"State/WTHLD",
        		ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_State_Tax_Wthld(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),"Local/Dist",
        		ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Loc_Distr(), new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),"Local/WTHLD",
        		ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Loc_Tax_Wthld(), new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(2, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"//TIN",
        		ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Tin(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"//Comp",
        		pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),"//Contract",
        		ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Contract_Nbr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/Dist/Code",
        		pdaTwradist.getPnd_Twradist_Pnd_Out_Dist(), new FieldAttributes("LC=�"),"/Not/Dtrm",
        		ldaTwrl9705.getForm_U_Tirf_Taxable_Not_Det(), new FieldAttributes("LC=�"),"/Federal/Gross",
        		ldaTwrl9705.getForm_U_Tirf_Gross_Amt(), new ReportEditMask ("-ZZZZ,ZZZ.99"),"/Federal/Taxable",
        		ldaTwrl9705.getForm_U_Tirf_Taxable_Amt(), new ReportEditMask ("-ZZZZ,ZZZ.99"),"/Federal/Tax",
        		ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld(), new ReportEditMask ("-ZZZZ,ZZZ.99"),"State/Gross/Taxable",
        		ldaTwrl9705.getForm_U_Tirf_Res_Gross_Amt(), new ReportEditMask ("-ZZZZ,ZZZ.99"),NEWLINE,"/",
        		ldaTwrl9705.getForm_U_Tirf_Res_Taxable_Amt(), new ReportEditMask ("-ZZZZ,ZZZ.99"),"/State/Distrib",
        		ldaTwrl9705.getForm_U_Tirf_State_Distr(), new ReportEditMask ("-ZZZZ,ZZZ.99"),"State/Local/Tax",
        		ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld(), new ReportEditMask ("-ZZZ,ZZ.99"),NEWLINE,"/",
        		ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld(), new ReportEditMask ("-ZZZ,ZZ.99"),"//IRR Amt",
        		ldaTwrl9705.getForm_U_Tirf_Res_Irr_Amt(), new ReportEditMask ("-ZZZZ,ZZZ.99"));
        getReports().setDisplayColumns(3, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",
        		pnd_Ws_Pnd_Rec_Type_Desc,"State/Dist",
        		pnd_Ws_Pnd_Rec_State_Distr, new ReportEditMask ("-Z,ZZZ,ZZZ,ZZ9.99"),"State/Wthld",
        		pnd_Ws_Pnd_Rec_State_Tax_Wthld, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"Local/Dist",
        		pnd_Ws_Pnd_Rec_Loc_Distr, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"Local/Wthld",
        		pnd_Ws_Pnd_Rec_Loc_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),"/Counter",
        		pnd_Ws_Pnd_Rec_Counter, new ReportEditMask ("-Z,ZZZ,ZZ9"));
    }
    private void CheckAtStartofData765() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pdaTwradist.getPnd_Twradist_Pnd_Tax_Year_A().setValue(ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Tax_Year());                                                       //Natural: ASSIGN #TWRADIST.#TAX-YEAR-A := #HEADER-TAX-YEAR := #TIRF-TAX-YEAR
            pnd_Ws_Pnd_Header_Tax_Year.setValue(ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_Tax_Year());
            pnd_Ws_Pnd_Num_State_Code.setValue(ldaTwrl3600.getPnd_Twrl3600_Pnd_Tirf_State_Code());                                                                        //Natural: ASSIGN #NUM-STATE-CODE := #TIRF-STATE-CODE
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-INFO
            sub_Get_Company_Info();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-STATE-INFO
            sub_Get_State_Info();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
