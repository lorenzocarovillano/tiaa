/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:35:13 PM
**        * FROM NATURAL PROGRAM : Twrp3320
************************************************************
**        * FILE NAME            : Twrp3320.java
**        * CLASS NAME           : Twrp3320
**        * INSTANCE NAME        : Twrp3320
************************************************************
********************************************************************
*                                                                  *
*      PROJECT  - TWARS                                            *
*                                                                  *
*      TWRP3320 - THIS PROGRAM WILL READ THE IRS TAPE AND          *
*                 WILL COMPARE THE SUMS OF DETAIL AMOUNTS          *
*                 WITH THE DATA ON THE TRAILER RECORDS.            *
*                                                                  *
* 08/13/02    J.ROTHOLZ - RECOMPILED DUE TO INCREASE IN #COMP-NAME
*             LENGTH IN TWRLCOMP & TWRACOMP
* 12/15/06    RM  RECOMPILED DUE TO LDA CHANGES
* 11/17/09    A. YOUNG  - RE-COMPILED TO ACCOMMODATE 2009 IRS      *
*                         CHANGES TO 'B', 'C', AND 'K' RECS.       *
* 10/01/10    A. YOUNG  - RE-COMPILED TO ACCOMMODATE 2010 IRS      *
*                         CHANGES TO 'A' AND 'B' RECORDS.          *
* 10/30/11    R. CARREON - COMPLIAN CHANGES
* 10/05/18    ARIVU     - EIN CHANGES - TAG: EINCHG
********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3320 extends BLNatBase
{
    // Data Areas
    private PdaTwratbl2 pdaTwratbl2;
    private LdaTwrl3321 ldaTwrl3321;
    private LdaTwrl3322 ldaTwrl3322;
    private LdaTwrl3323 ldaTwrl3323;
    private LdaTwrl3324 ldaTwrl3324;
    private LdaTwrl3325 ldaTwrl3325;
    private LdaTwrl3326 ldaTwrl3326;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField contr_Tot_1;
    private DbsField contr_Tot_2;
    private DbsField contr_Tot_3;
    private DbsField contr_Tot_4;
    private DbsField contr_Tot_5;
    private DbsField contr_Tot_6;
    private DbsField contr_Tot_7;
    private DbsField contr_Tot_8;
    private DbsField contr_Tot_9;
    private DbsField contr_Tot_10;
    private DbsField contr_Tot_11;
    private DbsField contr_Tot_B;
    private DbsField b_Record_Ctr;
    private DbsField pnd_Max_States;

    private DbsGroup state_Totals;
    private DbsField state_Totals_State_Code;
    private DbsField state_Totals_State_Name;
    private DbsField state_Totals_St_B_Rec_Ctr;
    private DbsField state_Totals_State_Tot_1;
    private DbsField state_Totals_State_Tot_2;
    private DbsField state_Totals_State_Tot_3;
    private DbsField state_Totals_State_Tot_4;
    private DbsField state_Totals_State_Tot_5;
    private DbsField state_Totals_State_Tot_6;
    private DbsField state_Totals_State_Tot_7;
    private DbsField state_Totals_State_Tot_8;
    private DbsField state_Totals_State_Tot_9;
    private DbsField state_Totals_State_Tot_10;
    private DbsField state_Totals_State_Tot_11;
    private DbsField state_Totals_State_Tot_B;
    private DbsField pnd_K;
    private DbsField pnd_Comment_C;
    private DbsField pnd_Comment_1;
    private DbsField pnd_Comment_2;
    private DbsField pnd_Comment_3;
    private DbsField pnd_Comment_4;
    private DbsField pnd_Comment_5;
    private DbsField pnd_Comment_6;
    private DbsField pnd_Comment_7;
    private DbsField pnd_Comment_8;
    private DbsField pnd_Comment_9;
    private DbsField pnd_Comment_10;
    private DbsField pnd_Comment_11;
    private DbsField pnd_Comment_B;
    private DbsField pnd_Header_1;

    private DbsGroup pnd_Header_1__R_Field_1;
    private DbsField pnd_Header_1_Pnd_Company;
    private DbsField pnd_Header_1_Pnd_Filler;
    private DbsField pnd_Header_1_Pnd_Category;
    private DbsField pnd_Ws_Irs_B_Contract_Payee;

    private DbsGroup pnd_Ws_Irs_B_Contract_Payee__R_Field_2;
    private DbsField pnd_Ws_Irs_B_Contract_Payee_Pnd_Ws_Irs_B_Tin;
    private DbsField pnd_Ws_Irs_B_Contract_Payee_Pnd_Ws_Irs_B_Cont_Py;
    private DbsField pnd_Page_Ctr_1;
    private DbsField pnd_Rec_B_Ctr;
    private DbsField pnd_Rec_A_Ctr;
    private DbsField pnd_Year_X;

    private DbsGroup pnd_Year_X__R_Field_3;
    private DbsField pnd_Year_X_Pnd_Year;
    private DbsField pnd_Parm_Year;

    private DbsGroup pnd_Parm_Year__R_Field_4;
    private DbsField pnd_Parm_Year_Pnd_Parm_Year_N;
    private DbsField pnd_Ctr_1000;

    private DbsGroup pnd_Ctr_1000__R_Field_5;
    private DbsField pnd_Ctr_1000_Pnd_Ctr_High;
    private DbsField pnd_Ctr_1000_Pnd_Ctr_Low;
    private DbsField pnd_Ws_Tax_Year;
    private DbsField pnd_I;
    private DbsField pnd_Y;
    private DbsField pnd_Z;
    private DbsField state_Alpha;
    private DbsField state_Index;
    private DbsField state_Comb_Fed_Ind;
    private DbsField pnd_Ws_State_Code_2;
    private DbsField pnd_Ws_State_Name;
    private DbsField pnd_Contr_Card;

    private DbsGroup pnd_Contr_Card__R_Field_6;
    private DbsField pnd_Contr_Card_Pnd_Filler_1;
    private DbsField pnd_Contr_Card_Pnd_Contr_Sw;
    private DbsField pnd_Contr_Card_Pnd_Filler_2;

    private DbsGroup pnd_State_Table;
    private DbsField pnd_State_Table_Pnd_Ws_State_Seq_Nbr;
    private DbsField pnd_State_Table_Pnd_Ws_State_Alpha;
    private DbsField pnd_State_Table_Pnd_Ws_State_Code;
    private DbsField pnd_State_Table_Pnd_Ws_State_Full_Name;
    private DbsField pnd_State_Table_Pnd_Ws_State_Tax_Id_Tiaa;
    private DbsField pnd_State_Table_Pnd_Ws_State_Tax_Id_Cref;
    private DbsField pnd_State_Table_Pnd_Ws_State_Tax_Id_Life;
    private DbsField pnd_State_Table_Pnd_Ws_State_Tax_Id_Trst;
    private DbsField pnd_State_Table_Pnd_Ws_State_Comb_Fed_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        ldaTwrl3321 = new LdaTwrl3321();
        registerRecord(ldaTwrl3321);
        ldaTwrl3322 = new LdaTwrl3322();
        registerRecord(ldaTwrl3322);
        ldaTwrl3323 = new LdaTwrl3323();
        registerRecord(ldaTwrl3323);
        ldaTwrl3324 = new LdaTwrl3324();
        registerRecord(ldaTwrl3324);
        ldaTwrl3325 = new LdaTwrl3325();
        registerRecord(ldaTwrl3325);
        ldaTwrl3326 = new LdaTwrl3326();
        registerRecord(ldaTwrl3326);

        // Local Variables
        contr_Tot_1 = localVariables.newFieldInRecord("contr_Tot_1", "CONTR-TOT-1", FieldType.NUMERIC, 15, 2);
        contr_Tot_2 = localVariables.newFieldInRecord("contr_Tot_2", "CONTR-TOT-2", FieldType.NUMERIC, 15, 2);
        contr_Tot_3 = localVariables.newFieldInRecord("contr_Tot_3", "CONTR-TOT-3", FieldType.NUMERIC, 15, 2);
        contr_Tot_4 = localVariables.newFieldInRecord("contr_Tot_4", "CONTR-TOT-4", FieldType.NUMERIC, 15, 2);
        contr_Tot_5 = localVariables.newFieldInRecord("contr_Tot_5", "CONTR-TOT-5", FieldType.NUMERIC, 15, 2);
        contr_Tot_6 = localVariables.newFieldInRecord("contr_Tot_6", "CONTR-TOT-6", FieldType.NUMERIC, 15, 2);
        contr_Tot_7 = localVariables.newFieldInRecord("contr_Tot_7", "CONTR-TOT-7", FieldType.NUMERIC, 15, 2);
        contr_Tot_8 = localVariables.newFieldInRecord("contr_Tot_8", "CONTR-TOT-8", FieldType.NUMERIC, 15, 2);
        contr_Tot_9 = localVariables.newFieldInRecord("contr_Tot_9", "CONTR-TOT-9", FieldType.NUMERIC, 15, 2);
        contr_Tot_10 = localVariables.newFieldInRecord("contr_Tot_10", "CONTR-TOT-10", FieldType.NUMERIC, 15, 2);
        contr_Tot_11 = localVariables.newFieldInRecord("contr_Tot_11", "CONTR-TOT-11", FieldType.NUMERIC, 15, 2);
        contr_Tot_B = localVariables.newFieldInRecord("contr_Tot_B", "CONTR-TOT-B", FieldType.NUMERIC, 15, 2);
        b_Record_Ctr = localVariables.newFieldInRecord("b_Record_Ctr", "B-RECORD-CTR", FieldType.NUMERIC, 8);
        pnd_Max_States = localVariables.newFieldInRecord("pnd_Max_States", "#MAX-STATES", FieldType.NUMERIC, 2);

        state_Totals = localVariables.newGroupArrayInRecord("state_Totals", "STATE-TOTALS", new DbsArrayController(1, 50));
        state_Totals_State_Code = state_Totals.newFieldInGroup("state_Totals_State_Code", "STATE-CODE", FieldType.STRING, 2);
        state_Totals_State_Name = state_Totals.newFieldInGroup("state_Totals_State_Name", "STATE-NAME", FieldType.STRING, 16);
        state_Totals_St_B_Rec_Ctr = state_Totals.newFieldInGroup("state_Totals_St_B_Rec_Ctr", "ST-B-REC-CTR", FieldType.NUMERIC, 6);
        state_Totals_State_Tot_1 = state_Totals.newFieldInGroup("state_Totals_State_Tot_1", "STATE-TOT-1", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_2 = state_Totals.newFieldInGroup("state_Totals_State_Tot_2", "STATE-TOT-2", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_3 = state_Totals.newFieldInGroup("state_Totals_State_Tot_3", "STATE-TOT-3", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_4 = state_Totals.newFieldInGroup("state_Totals_State_Tot_4", "STATE-TOT-4", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_5 = state_Totals.newFieldInGroup("state_Totals_State_Tot_5", "STATE-TOT-5", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_6 = state_Totals.newFieldInGroup("state_Totals_State_Tot_6", "STATE-TOT-6", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_7 = state_Totals.newFieldInGroup("state_Totals_State_Tot_7", "STATE-TOT-7", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_8 = state_Totals.newFieldInGroup("state_Totals_State_Tot_8", "STATE-TOT-8", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_9 = state_Totals.newFieldInGroup("state_Totals_State_Tot_9", "STATE-TOT-9", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_10 = state_Totals.newFieldInGroup("state_Totals_State_Tot_10", "STATE-TOT-10", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_11 = state_Totals.newFieldInGroup("state_Totals_State_Tot_11", "STATE-TOT-11", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_B = state_Totals.newFieldInGroup("state_Totals_State_Tot_B", "STATE-TOT-B", FieldType.NUMERIC, 15, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 5);
        pnd_Comment_C = localVariables.newFieldInRecord("pnd_Comment_C", "#COMMENT-C", FieldType.STRING, 8);
        pnd_Comment_1 = localVariables.newFieldInRecord("pnd_Comment_1", "#COMMENT-1", FieldType.STRING, 8);
        pnd_Comment_2 = localVariables.newFieldInRecord("pnd_Comment_2", "#COMMENT-2", FieldType.STRING, 8);
        pnd_Comment_3 = localVariables.newFieldInRecord("pnd_Comment_3", "#COMMENT-3", FieldType.STRING, 8);
        pnd_Comment_4 = localVariables.newFieldInRecord("pnd_Comment_4", "#COMMENT-4", FieldType.STRING, 8);
        pnd_Comment_5 = localVariables.newFieldInRecord("pnd_Comment_5", "#COMMENT-5", FieldType.STRING, 8);
        pnd_Comment_6 = localVariables.newFieldInRecord("pnd_Comment_6", "#COMMENT-6", FieldType.STRING, 8);
        pnd_Comment_7 = localVariables.newFieldInRecord("pnd_Comment_7", "#COMMENT-7", FieldType.STRING, 8);
        pnd_Comment_8 = localVariables.newFieldInRecord("pnd_Comment_8", "#COMMENT-8", FieldType.STRING, 8);
        pnd_Comment_9 = localVariables.newFieldInRecord("pnd_Comment_9", "#COMMENT-9", FieldType.STRING, 8);
        pnd_Comment_10 = localVariables.newFieldInRecord("pnd_Comment_10", "#COMMENT-10", FieldType.STRING, 8);
        pnd_Comment_11 = localVariables.newFieldInRecord("pnd_Comment_11", "#COMMENT-11", FieldType.STRING, 8);
        pnd_Comment_B = localVariables.newFieldInRecord("pnd_Comment_B", "#COMMENT-B", FieldType.STRING, 8);
        pnd_Header_1 = localVariables.newFieldInRecord("pnd_Header_1", "#HEADER-1", FieldType.STRING, 13);

        pnd_Header_1__R_Field_1 = localVariables.newGroupInRecord("pnd_Header_1__R_Field_1", "REDEFINE", pnd_Header_1);
        pnd_Header_1_Pnd_Company = pnd_Header_1__R_Field_1.newFieldInGroup("pnd_Header_1_Pnd_Company", "#COMPANY", FieldType.STRING, 4);
        pnd_Header_1_Pnd_Filler = pnd_Header_1__R_Field_1.newFieldInGroup("pnd_Header_1_Pnd_Filler", "#FILLER", FieldType.STRING, 6);
        pnd_Header_1_Pnd_Category = pnd_Header_1__R_Field_1.newFieldInGroup("pnd_Header_1_Pnd_Category", "#CATEGORY", FieldType.STRING, 3);
        pnd_Ws_Irs_B_Contract_Payee = localVariables.newFieldInRecord("pnd_Ws_Irs_B_Contract_Payee", "#WS-IRS-B-CONTRACT-PAYEE", FieldType.STRING, 20);

        pnd_Ws_Irs_B_Contract_Payee__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Irs_B_Contract_Payee__R_Field_2", "REDEFINE", pnd_Ws_Irs_B_Contract_Payee);
        pnd_Ws_Irs_B_Contract_Payee_Pnd_Ws_Irs_B_Tin = pnd_Ws_Irs_B_Contract_Payee__R_Field_2.newFieldInGroup("pnd_Ws_Irs_B_Contract_Payee_Pnd_Ws_Irs_B_Tin", 
            "#WS-IRS-B-TIN", FieldType.STRING, 4);
        pnd_Ws_Irs_B_Contract_Payee_Pnd_Ws_Irs_B_Cont_Py = pnd_Ws_Irs_B_Contract_Payee__R_Field_2.newFieldInGroup("pnd_Ws_Irs_B_Contract_Payee_Pnd_Ws_Irs_B_Cont_Py", 
            "#WS-IRS-B-CONT-PY", FieldType.STRING, 10);
        pnd_Page_Ctr_1 = localVariables.newFieldInRecord("pnd_Page_Ctr_1", "#PAGE-CTR-1", FieldType.NUMERIC, 6);
        pnd_Rec_B_Ctr = localVariables.newFieldInRecord("pnd_Rec_B_Ctr", "#REC-B-CTR", FieldType.NUMERIC, 1);
        pnd_Rec_A_Ctr = localVariables.newFieldInRecord("pnd_Rec_A_Ctr", "#REC-A-CTR", FieldType.NUMERIC, 4);
        pnd_Year_X = localVariables.newFieldInRecord("pnd_Year_X", "#YEAR-X", FieldType.NUMERIC, 4);

        pnd_Year_X__R_Field_3 = localVariables.newGroupInRecord("pnd_Year_X__R_Field_3", "REDEFINE", pnd_Year_X);
        pnd_Year_X_Pnd_Year = pnd_Year_X__R_Field_3.newFieldInGroup("pnd_Year_X_Pnd_Year", "#YEAR", FieldType.STRING, 4);
        pnd_Parm_Year = localVariables.newFieldInRecord("pnd_Parm_Year", "#PARM-YEAR", FieldType.STRING, 4);

        pnd_Parm_Year__R_Field_4 = localVariables.newGroupInRecord("pnd_Parm_Year__R_Field_4", "REDEFINE", pnd_Parm_Year);
        pnd_Parm_Year_Pnd_Parm_Year_N = pnd_Parm_Year__R_Field_4.newFieldInGroup("pnd_Parm_Year_Pnd_Parm_Year_N", "#PARM-YEAR-N", FieldType.NUMERIC, 4);
        pnd_Ctr_1000 = localVariables.newFieldInRecord("pnd_Ctr_1000", "#CTR-1000", FieldType.NUMERIC, 7);

        pnd_Ctr_1000__R_Field_5 = localVariables.newGroupInRecord("pnd_Ctr_1000__R_Field_5", "REDEFINE", pnd_Ctr_1000);
        pnd_Ctr_1000_Pnd_Ctr_High = pnd_Ctr_1000__R_Field_5.newFieldInGroup("pnd_Ctr_1000_Pnd_Ctr_High", "#CTR-HIGH", FieldType.NUMERIC, 4);
        pnd_Ctr_1000_Pnd_Ctr_Low = pnd_Ctr_1000__R_Field_5.newFieldInGroup("pnd_Ctr_1000_Pnd_Ctr_Low", "#CTR-LOW", FieldType.NUMERIC, 3);
        pnd_Ws_Tax_Year = localVariables.newFieldInRecord("pnd_Ws_Tax_Year", "#WS-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.NUMERIC, 6);
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.NUMERIC, 6);
        state_Alpha = localVariables.newFieldArrayInRecord("state_Alpha", "STATE-ALPHA", FieldType.STRING, 3, new DbsArrayController(1, 50));
        state_Index = localVariables.newFieldArrayInRecord("state_Index", "STATE-INDEX", FieldType.NUMERIC, 2, new DbsArrayController(1, 50));
        state_Comb_Fed_Ind = localVariables.newFieldArrayInRecord("state_Comb_Fed_Ind", "STATE-COMB-FED-IND", FieldType.STRING, 1, new DbsArrayController(1, 
            50));
        pnd_Ws_State_Code_2 = localVariables.newFieldArrayInRecord("pnd_Ws_State_Code_2", "#WS-STATE-CODE-2", FieldType.STRING, 2, new DbsArrayController(1, 
            50));
        pnd_Ws_State_Name = localVariables.newFieldArrayInRecord("pnd_Ws_State_Name", "#WS-STATE-NAME", FieldType.STRING, 19, new DbsArrayController(1, 
            50));
        pnd_Contr_Card = localVariables.newFieldInRecord("pnd_Contr_Card", "#CONTR-CARD", FieldType.STRING, 40);

        pnd_Contr_Card__R_Field_6 = localVariables.newGroupInRecord("pnd_Contr_Card__R_Field_6", "REDEFINE", pnd_Contr_Card);
        pnd_Contr_Card_Pnd_Filler_1 = pnd_Contr_Card__R_Field_6.newFieldInGroup("pnd_Contr_Card_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 11);
        pnd_Contr_Card_Pnd_Contr_Sw = pnd_Contr_Card__R_Field_6.newFieldInGroup("pnd_Contr_Card_Pnd_Contr_Sw", "#CONTR-SW", FieldType.STRING, 1);
        pnd_Contr_Card_Pnd_Filler_2 = pnd_Contr_Card__R_Field_6.newFieldInGroup("pnd_Contr_Card_Pnd_Filler_2", "#FILLER-2", FieldType.STRING, 28);

        pnd_State_Table = localVariables.newGroupArrayInRecord("pnd_State_Table", "#STATE-TABLE", new DbsArrayController(1, 100));
        pnd_State_Table_Pnd_Ws_State_Seq_Nbr = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_Ws_State_Seq_Nbr", "#WS-STATE-SEQ-NBR", FieldType.NUMERIC, 
            3);
        pnd_State_Table_Pnd_Ws_State_Alpha = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_Ws_State_Alpha", "#WS-STATE-ALPHA", FieldType.STRING, 
            2);
        pnd_State_Table_Pnd_Ws_State_Code = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_Ws_State_Code", "#WS-STATE-CODE", FieldType.STRING, 3);
        pnd_State_Table_Pnd_Ws_State_Full_Name = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_Ws_State_Full_Name", "#WS-STATE-FULL-NAME", FieldType.STRING, 
            19);
        pnd_State_Table_Pnd_Ws_State_Tax_Id_Tiaa = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_Ws_State_Tax_Id_Tiaa", "#WS-STATE-TAX-ID-TIAA", 
            FieldType.STRING, 15);
        pnd_State_Table_Pnd_Ws_State_Tax_Id_Cref = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_Ws_State_Tax_Id_Cref", "#WS-STATE-TAX-ID-CREF", 
            FieldType.STRING, 14);
        pnd_State_Table_Pnd_Ws_State_Tax_Id_Life = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_Ws_State_Tax_Id_Life", "#WS-STATE-TAX-ID-LIFE", 
            FieldType.STRING, 14);
        pnd_State_Table_Pnd_Ws_State_Tax_Id_Trst = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_Ws_State_Tax_Id_Trst", "#WS-STATE-TAX-ID-TRST", 
            FieldType.STRING, 14);
        pnd_State_Table_Pnd_Ws_State_Comb_Fed_Ind = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_Ws_State_Comb_Fed_Ind", "#WS-STATE-COMB-FED-IND", 
            FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl3321.initializeValues();
        ldaTwrl3322.initializeValues();
        ldaTwrl3323.initializeValues();
        ldaTwrl3324.initializeValues();
        ldaTwrl3325.initializeValues();
        ldaTwrl3326.initializeValues();

        localVariables.reset();
        contr_Tot_1.setInitialValue(0);
        contr_Tot_2.setInitialValue(0);
        contr_Tot_3.setInitialValue(0);
        contr_Tot_4.setInitialValue(0);
        contr_Tot_5.setInitialValue(0);
        contr_Tot_6.setInitialValue(0);
        contr_Tot_7.setInitialValue(0);
        contr_Tot_8.setInitialValue(0);
        contr_Tot_9.setInitialValue(0);
        contr_Tot_10.setInitialValue(0);
        contr_Tot_11.setInitialValue(0);
        contr_Tot_B.setInitialValue(0);
        b_Record_Ctr.setInitialValue(0);
        state_Totals_State_Code.getValue(1).setInitialValue(" ");
        state_Totals_State_Name.getValue(1).setInitialValue(" ");
        state_Totals_St_B_Rec_Ctr.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_1.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_2.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_3.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_4.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_5.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_6.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_7.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_8.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_9.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_10.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_11.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_B.getValue(1).setInitialValue(0);
        pnd_K.setInitialValue(0);
        pnd_Comment_C.setInitialValue(" ");
        pnd_Comment_1.setInitialValue(" ");
        pnd_Comment_2.setInitialValue(" ");
        pnd_Comment_3.setInitialValue(" ");
        pnd_Comment_4.setInitialValue(" ");
        pnd_Comment_5.setInitialValue(" ");
        pnd_Comment_6.setInitialValue(" ");
        pnd_Comment_7.setInitialValue(" ");
        pnd_Comment_8.setInitialValue(" ");
        pnd_Comment_9.setInitialValue(" ");
        pnd_Comment_10.setInitialValue(" ");
        pnd_Comment_11.setInitialValue(" ");
        pnd_Comment_B.setInitialValue(" ");
        pnd_Header_1.setInitialValue("     1099-   ");
        pnd_Page_Ctr_1.setInitialValue(0);
        pnd_Rec_B_Ctr.setInitialValue(0);
        pnd_Rec_A_Ctr.setInitialValue(0);
        pnd_Year_X.setInitialValue(0);
        pnd_Ctr_1000.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3320() throws Exception
    {
        super("Twrp3320");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp3320|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT ( 5 ) LS = 132 PS = 60;//Natural: FORMAT ( 0 ) LS = 132 PS = 60
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 5 ) TITLE 'TWRP3320-01' 41X 'TAX INFORMATION AND REPORTING' 41X 'PAGE' #PAGE-CTR-1 ( EM = ZZZ9 )
                //* **********************************************************************
                //*    2X IRS-B-CONTRACT-PAYEE   (EM=X(11))
                //* **********************************************************************
                //* **********************************************************************
                //* **********************************************************************
                //* **********************************************************************
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Contr_Card,pnd_Parm_Year);                                                                         //Natural: INPUT #CONTR-CARD #PARM-YEAR
                if (condition(pnd_Parm_Year.equals(" ")))                                                                                                                 //Natural: IF #PARM-YEAR = ' '
                {
                    pnd_Ws_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Tax_Year), Global.getDATN().divide(10000).subtract(1));                                   //Natural: ASSIGN #WS-TAX-YEAR := *DATN / 10000 - 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Tax_Year.setValue(pnd_Parm_Year_Pnd_Parm_Year_N);                                                                                              //Natural: ASSIGN #WS-TAX-YEAR := #PARM-YEAR-N
                }                                                                                                                                                         //Natural: END-IF
                //*  12-09-00 FRANK
                                                                                                                                                                          //Natural: PERFORM LOAD-STATE-TABLE
                sub_Load_State_Table();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  12-09-00 FRANK
                pnd_Y.reset();                                                                                                                                            //Natural: RESET #Y
                FOR01:                                                                                                                                                    //Natural: FOR #Z = 00 TO 99
                for (pnd_Z.setValue(0); condition(pnd_Z.lessOrEqual(99)); pnd_Z.nadd(1))
                {
                    if (condition(pnd_State_Table_Pnd_Ws_State_Comb_Fed_Ind.getValue(pnd_Z.getInt() + 1).equals("C")))                                                    //Natural: IF #WS-STATE-COMB-FED-IND ( #Z ) = 'C'
                    {
                        pnd_Y.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #Y
                        //*  12-29-00 FRANK
                        pnd_Ws_State_Code_2.getValue(pnd_Y).setValueEdited(pnd_State_Table_Pnd_Ws_State_Seq_Nbr.getValue(pnd_Z.getInt() + 1),new ReportEditMask("99"));   //Natural: MOVE EDITED #WS-STATE-SEQ-NBR ( #Z ) ( EM = 99 ) TO #WS-STATE-CODE-2 ( #Y )
                        state_Totals_State_Name.getValue(pnd_Y).setValue(pnd_State_Table_Pnd_Ws_State_Full_Name.getValue(pnd_Z.getInt() + 1));                            //Natural: ASSIGN STATE-NAME ( #Y ) := #WS-STATE-FULL-NAME ( #Z )
                        state_Alpha.getValue(pnd_Y).setValue(pnd_State_Table_Pnd_Ws_State_Alpha.getValue(pnd_Z.getInt() + 1));                                            //Natural: ASSIGN STATE-ALPHA ( #Y ) := #WS-STATE-ALPHA ( #Z )
                        state_Index.getValue(pnd_Y).setValue(pnd_Y);                                                                                                      //Natural: ASSIGN STATE-INDEX ( #Y ) := #Y
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                pnd_Max_States.setValue(pnd_Y);                                                                                                                           //Natural: ASSIGN #MAX-STATES := #Y
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 2 RECORD IRS-A-OUT-TAPE
                while (condition(getWorkFiles().read(2, ldaTwrl3321.getIrs_A_Out_Tape())))
                {
                    short decideConditionsMet684 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF IRS-A-RECORD-TYPE;//Natural: VALUE 'A'
                    if (condition((ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Record_Type().equals("A"))))
                    {
                        decideConditionsMet684++;
                                                                                                                                                                          //Natural: PERFORM ROUTINE-A
                        sub_Routine_A();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: VALUE 'B'
                    else if (condition((ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Record_Type().equals("B"))))
                    {
                        decideConditionsMet684++;
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Move_1().setValue(ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Move_1());                                              //Natural: MOVE IRS-A-MOVE-1 TO IRS-B-MOVE-1
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Move_2().setValue(ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Move_2());                                              //Natural: MOVE IRS-A-MOVE-2 TO IRS-B-MOVE-2
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Move_3().setValue(ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Move_3());                                              //Natural: MOVE IRS-A-MOVE-3 TO IRS-B-MOVE-3
                                                                                                                                                                          //Natural: PERFORM ROUTINE-B
                        sub_Routine_B();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: VALUE 'C'
                    else if (condition((ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Record_Type().equals("C"))))
                    {
                        decideConditionsMet684++;
                        ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Move_1().setValue(ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Move_1());                                              //Natural: MOVE IRS-A-MOVE-1 TO IRS-C-MOVE-1
                        ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Move_2().setValue(ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Move_2());                                              //Natural: MOVE IRS-A-MOVE-2 TO IRS-C-MOVE-2
                        ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Move_3().setValue(ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Move_3());                                              //Natural: MOVE IRS-A-MOVE-3 TO IRS-C-MOVE-3
                                                                                                                                                                          //Natural: PERFORM ROUTINE-C
                        sub_Routine_C();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: VALUE 'K'
                    else if (condition((ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Record_Type().equals("K"))))
                    {
                        decideConditionsMet684++;
                        //*  02-11-99 FC
                        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Move_1().setValue(ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Move_1());                                              //Natural: MOVE IRS-A-MOVE-1 TO IRS-K-MOVE-1
                        //*  02-11-99 FC
                        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Move_2().setValue(ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Move_2());                                              //Natural: MOVE IRS-A-MOVE-2 TO IRS-K-MOVE-2
                        //*  02-11-99 FC
                        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Move_3().setValue(ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Move_3());                                              //Natural: MOVE IRS-A-MOVE-3 TO IRS-K-MOVE-3
                                                                                                                                                                          //Natural: PERFORM ROUTINE-K
                        sub_Routine_K();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: VALUE 'F'
                    else if (condition((ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Record_Type().equals("F"))))
                    {
                        decideConditionsMet684++;
                        ldaTwrl3324.getIrs_F_Out_Tape_Irs_F_Move_1().setValue(ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Move_1());                                              //Natural: MOVE IRS-A-MOVE-1 TO IRS-F-MOVE-1
                        ldaTwrl3324.getIrs_F_Out_Tape_Irs_F_Move_2().setValue(ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Move_2());                                              //Natural: MOVE IRS-A-MOVE-2 TO IRS-F-MOVE-2
                        ldaTwrl3324.getIrs_F_Out_Tape_Irs_F_Move_3().setValue(ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Move_3());                                              //Natural: MOVE IRS-A-MOVE-3 TO IRS-F-MOVE-3
                                                                                                                                                                          //Natural: PERFORM ROUTINE-F
                        sub_Routine_F();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: VALUE 'T'
                    else if (condition((ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Record_Type().equals("T"))))
                    {
                        decideConditionsMet684++;
                        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Move_1().setValue(ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Move_1());                                              //Natural: MOVE IRS-A-MOVE-1 TO IRS-T-MOVE-1
                        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Move_2().setValue(ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Move_2());                                              //Natural: MOVE IRS-A-MOVE-2 TO IRS-T-MOVE-2
                        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Move_3().setValue(ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Move_3());                                              //Natural: MOVE IRS-A-MOVE-3 TO IRS-T-MOVE-3
                        ignore();
                    }                                                                                                                                                     //Natural: NONE VALUE
                    else if (condition())
                    {
                        getReports().skip(5, 2);                                                                                                                          //Natural: SKIP ( 5 ) 2
                        getReports().write(5, "INVALID RECORD-TYPE",ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Record_Type());                                                   //Natural: WRITE ( 5 ) 'INVALID RECORD-TYPE' IRS-A-RECORD-TYPE
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Routine_A() throws Exception                                                                                                                         //Natural: ROUTINE-A
    {
        if (BLNatReinput.isReinput()) return;

        //*  1991
        //*  RCC
        //*  11-18-99 FRANK
        pnd_Rec_B_Ctr.reset();                                                                                                                                            //Natural: RESET #REC-B-CTR B-RECORD-CTR ST-B-REC-CTR ( * ) CONTR-TOT-1 STATE-TOT-1 ( * ) CONTR-TOT-2 STATE-TOT-2 ( * ) CONTR-TOT-3 STATE-TOT-3 ( * ) CONTR-TOT-4 STATE-TOT-4 ( * ) CONTR-TOT-5 STATE-TOT-5 ( * ) CONTR-TOT-6 STATE-TOT-6 ( * ) CONTR-TOT-7 STATE-TOT-7 ( * ) CONTR-TOT-8 STATE-TOT-8 ( * ) CONTR-TOT-9 STATE-TOT-9 ( * ) CONTR-TOT-10 STATE-TOT-10 ( * ) CONTR-TOT-11 CONTR-TOT-B
        b_Record_Ctr.reset();
        state_Totals_St_B_Rec_Ctr.getValue("*").reset();
        contr_Tot_1.reset();
        state_Totals_State_Tot_1.getValue("*").reset();
        contr_Tot_2.reset();
        state_Totals_State_Tot_2.getValue("*").reset();
        contr_Tot_3.reset();
        state_Totals_State_Tot_3.getValue("*").reset();
        contr_Tot_4.reset();
        state_Totals_State_Tot_4.getValue("*").reset();
        contr_Tot_5.reset();
        state_Totals_State_Tot_5.getValue("*").reset();
        contr_Tot_6.reset();
        state_Totals_State_Tot_6.getValue("*").reset();
        contr_Tot_7.reset();
        state_Totals_State_Tot_7.getValue("*").reset();
        contr_Tot_8.reset();
        state_Totals_State_Tot_8.getValue("*").reset();
        contr_Tot_9.reset();
        state_Totals_State_Tot_9.getValue("*").reset();
        contr_Tot_10.reset();
        state_Totals_State_Tot_10.getValue("*").reset();
        contr_Tot_11.reset();
        contr_Tot_B.reset();
        pnd_Year_X_Pnd_Year.setValue(ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payment_Year());                                                                                 //Natural: ASSIGN #YEAR := IRS-A-PAYMENT-YEAR
        //*  09-24-02 FRANK
        //*  EINCHG
        //*  EINCHG
        short decideConditionsMet734 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF IRS-A-PAYER-EIN;//Natural: VALUE '136022042'
        if (condition((ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Ein().equals("136022042"))))
        {
            decideConditionsMet734++;
            pnd_Header_1_Pnd_Company.setValue("CREF");                                                                                                                    //Natural: ASSIGN #COMPANY := 'CREF'
        }                                                                                                                                                                 //Natural: VALUE '131624203'
        else if (condition((ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Ein().equals("131624203"))))
        {
            decideConditionsMet734++;
            pnd_Header_1_Pnd_Company.setValue("TIAA");                                                                                                                    //Natural: ASSIGN #COMPANY := 'TIAA'
        }                                                                                                                                                                 //Natural: VALUE '516559589'
        else if (condition((ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Ein().equals("516559589"))))
        {
            decideConditionsMet734++;
            pnd_Header_1_Pnd_Company.setValue("TRST");                                                                                                                    //Natural: ASSIGN #COMPANY := 'TRST'
        }                                                                                                                                                                 //Natural: VALUE '133917848'
        else if (condition((ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Ein().equals("133917848"))))
        {
            decideConditionsMet734++;
            pnd_Header_1_Pnd_Company.setValue("LIFE");                                                                                                                    //Natural: ASSIGN #COMPANY := 'LIFE'
        }                                                                                                                                                                 //Natural: VALUE '133586143'
        else if (condition((ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Ein().equals("133586143"))))
        {
            decideConditionsMet734++;
            pnd_Header_1_Pnd_Company.setValue("TCII");                                                                                                                    //Natural: ASSIGN #COMPANY := 'TCII'
        }                                                                                                                                                                 //Natural: VALUE '822826183'
        else if (condition((ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Ein().equals("822826183"))))
        {
            decideConditionsMet734++;
            pnd_Header_1_Pnd_Company.setValue("ADMN");                                                                                                                    //Natural: ASSIGN #COMPANY := 'ADMN'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Header_1_Pnd_Company.reset();                                                                                                                             //Natural: RESET #COMPANY
            getReports().skip(5, 2);                                                                                                                                      //Natural: SKIP ( 5 ) 2
            getReports().write(5, "INVALID A-PAYER-EIN",ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Ein());                                                                 //Natural: WRITE ( 5 ) 'INVALID A-PAYER-EIN' IRS-A-PAYER-EIN
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Type_Of_Return().equals("6")))                                                                                  //Natural: IF IRS-A-TYPE-OF-RETURN = '6'
        {
            pnd_Header_1_Pnd_Category.setValue("INT");                                                                                                                    //Natural: MOVE 'INT' TO #CATEGORY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Type_Of_Return().equals("9")))                                                                              //Natural: IF IRS-A-TYPE-OF-RETURN = '9'
            {
                pnd_Header_1_Pnd_Category.setValue("R  ");                                                                                                                //Natural: MOVE 'R  ' TO #CATEGORY
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Header_1_Pnd_Category.reset();                                                                                                                        //Natural: RESET #CATEGORY
                getReports().skip(5, 2);                                                                                                                                  //Natural: SKIP ( 5 ) 2
                getReports().write(5, "INVALID A-TYPE-OF-RETURN",ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Type_Of_Return());                                                   //Natural: WRITE ( 5 ) 'INVALID A-TYPE-OF-RETURN' IRS-A-TYPE-OF-RETURN
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        getReports().newPage(new ReportSpecification(5));                                                                                                                 //Natural: NEWPAGE ( 5 )
        if (condition(Global.isEscape())){return;}
        pnd_Page_Ctr_1.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #PAGE-CTR-1
        getReports().write(5, new ColumnSpacing(50),"NON-PERIODIC CONTROL REPORT",pnd_Year_X_Pnd_Year,new ColumnSpacing(41),Global.getDATU(),NEWLINE,new                  //Natural: WRITE ( 5 ) 50X 'NON-PERIODIC CONTROL REPORT' #YEAR 41X *DATU /55X 'IRS ORIGINAL REPORTING' 46X *TIME ( EM = XXXXXXXX ) //60X #HEADER-1 /44X '   T F   D ' /44X '   X O I I  ' / 'REC  PM  DOC RET TIN TAXPAYER    CONTRACT   ' '  N R R S  ST        NAME-ADDRESS LINES' 26X 'AMOUNTS' / 'TYP YEAR CDE IND TYP    ID         PAYEE     ' ' D N A T  CD' / '--- ---- --- --- --- ---------  -------------' ' - - - -- --' 1X '-----------------------------------' 1X '-----------------------------------'
            ColumnSpacing(55),"IRS ORIGINAL REPORTING",new ColumnSpacing(46),Global.getTIME(), new ReportEditMask ("XXXXXXXX"),NEWLINE,NEWLINE,new ColumnSpacing(60),pnd_Header_1,NEWLINE,new 
            ColumnSpacing(44),"   T F   D ",NEWLINE,new ColumnSpacing(44),"   X O I I  ",NEWLINE,"REC  PM  DOC RET TIN TAXPAYER    CONTRACT   ","  N R R S  ST        NAME-ADDRESS LINES",new 
            ColumnSpacing(26),"AMOUNTS",NEWLINE,"TYP YEAR CDE IND TYP    ID         PAYEE     "," D N A T  CD",NEWLINE,"--- ---- --- --- --- ---------  -------------"," - - - -- --",new 
            ColumnSpacing(1),"-----------------------------------",new ColumnSpacing(1),"-----------------------------------");
        if (Global.isEscape()) return;
        pnd_Rec_A_Ctr.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #REC-A-CTR
    }
    private void sub_Routine_B() throws Exception                                                                                                                         //Natural: ROUTINE-B
    {
        if (BLNatReinput.isReinput()) return;

        b_Record_Ctr.nadd(1);                                                                                                                                             //Natural: ADD 1 TO B-RECORD-CTR
        pnd_Ctr_1000.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CTR-1000
        contr_Tot_1.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                                                                         //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO CONTR-TOT-1
        contr_Tot_2.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_2());                                                                                         //Natural: ADD IRS-B-PAYMENT-AMOUNT-2 TO CONTR-TOT-2
        contr_Tot_3.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_3());                                                                                         //Natural: ADD IRS-B-PAYMENT-AMOUNT-3 TO CONTR-TOT-3
        contr_Tot_4.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_4());                                                                                         //Natural: ADD IRS-B-PAYMENT-AMOUNT-4 TO CONTR-TOT-4
        contr_Tot_5.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_5());                                                                                         //Natural: ADD IRS-B-PAYMENT-AMOUNT-5 TO CONTR-TOT-5
        contr_Tot_6.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_6());                                                                                         //Natural: ADD IRS-B-PAYMENT-AMOUNT-6 TO CONTR-TOT-6
        contr_Tot_7.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_7());                                                                                         //Natural: ADD IRS-B-PAYMENT-AMOUNT-7 TO CONTR-TOT-7
        contr_Tot_8.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_8());                                                                                         //Natural: ADD IRS-B-PAYMENT-AMOUNT-8 TO CONTR-TOT-8
        contr_Tot_9.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_9());                                                                                         //Natural: ADD IRS-B-PAYMENT-AMOUNT-9 TO CONTR-TOT-9
        contr_Tot_10.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld());                                                                                      //Natural: ADD IRS-B-ME-ST-TAX-WITHHELD TO CONTR-TOT-10
        contr_Tot_11.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld());                                                                                      //Natural: ADD IRS-B-ME-LC-TAX-WITHHELD TO CONTR-TOT-11
        //*  RCC
        contr_Tot_B.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_B());                                                                                         //Natural: ADD IRS-B-PAYMENT-AMOUNT-B TO CONTR-TOT-B
        //*  12-09-00 FRANK
        pnd_K.reset();                                                                                                                                                    //Natural: RESET #K
        FOR02:                                                                                                                                                            //Natural: FOR #Z = 01 TO #MAX-STATES
        for (pnd_Z.setValue(1); condition(pnd_Z.lessOrEqual(pnd_Max_States)); pnd_Z.nadd(1))
        {
            if (condition(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Federal_State_Code().equals(pnd_Ws_State_Code_2.getValue(pnd_Z))))                                          //Natural: IF IRS-B-FEDERAL-STATE-CODE = #WS-STATE-CODE-2 ( #Z )
            {
                pnd_K.setValue(state_Index.getValue(pnd_Z));                                                                                                              //Natural: ASSIGN #K := STATE-INDEX ( #Z )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  1991
        if (condition(pnd_K.notEquals(getZero())))                                                                                                                        //Natural: IF #K NE 0
        {
            state_Totals_St_B_Rec_Ctr.getValue(pnd_K).nadd(1);                                                                                                            //Natural: ADD 1 TO ST-B-REC-CTR ( #K )
            state_Totals_State_Tot_1.getValue(pnd_K).nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                                        //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO STATE-TOT-1 ( #K )
            state_Totals_State_Tot_2.getValue(pnd_K).nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_2());                                                        //Natural: ADD IRS-B-PAYMENT-AMOUNT-2 TO STATE-TOT-2 ( #K )
            state_Totals_State_Tot_4.getValue(pnd_K).nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_4());                                                        //Natural: ADD IRS-B-PAYMENT-AMOUNT-4 TO STATE-TOT-4 ( #K )
            state_Totals_State_Tot_5.getValue(pnd_K).nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_5());                                                        //Natural: ADD IRS-B-PAYMENT-AMOUNT-5 TO STATE-TOT-5 ( #K )
            //*  1991
            state_Totals_State_Tot_9.getValue(pnd_K).nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_9());                                                        //Natural: ADD IRS-B-PAYMENT-AMOUNT-9 TO STATE-TOT-9 ( #K )
            //* 03-03-99 FC
            state_Totals_State_Tot_10.getValue(pnd_K).nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_A());                                                       //Natural: ADD IRS-B-PAYMENT-AMOUNT-A TO STATE-TOT-10 ( #K )
            //* *ADD   IRS-B-ME-LC-TAX-WITHHELD TO STATE-TOT-11 (#K) /* 1991
            //*  RCC
            state_Totals_State_Tot_B.getValue(pnd_K).nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_B());                                                        //Natural: ADD IRS-B-PAYMENT-AMOUNT-B TO STATE-TOT-B ( #K )
        }                                                                                                                                                                 //Natural: END-IF
        //*   L-LOCAL DISTRIB'
        if (condition(pnd_Rec_B_Ctr.equals(6)))                                                                                                                           //Natural: IF #REC-B-CTR = 6
        {
            getReports().skip(5, 3);                                                                                                                                      //Natural: SKIP ( 5 ) 3
            getReports().write(5, "1-Gross Amt  2-Tax Amt  3-Capital Gains  4-Fed Tax  5-IVC ","6-Net Unreal Apprec in Emp Securities  8-Other ","9-Tot Emp Contrib",     //Natural: WRITE ( 5 ) '1-Gross Amt  2-Tax Amt  3-Capital Gains  4-Fed Tax  5-IVC ' '6-Net Unreal Apprec in Emp Securities  8-Other ' '9-Tot Emp Contrib' / 'A-State Tax  S-State Distrib  C-Local Tax'
                NEWLINE,"A-State Tax  S-State Distrib  C-Local Tax");
            if (Global.isEscape()) return;
            pnd_Page_Ctr_1.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #PAGE-CTR-1
            getReports().newPage(new ReportSpecification(5));                                                                                                             //Natural: NEWPAGE ( 5 )
            if (condition(Global.isEscape())){return;}
            getReports().write(5, new ColumnSpacing(50),"NON-PERIODIC CONTROL REPORT",pnd_Year_X_Pnd_Year,new ColumnSpacing(41),Global.getDATU(),NEWLINE,new              //Natural: WRITE ( 5 ) 50X 'NON-PERIODIC CONTROL REPORT' #YEAR 41X *DATU /55X 'IRS ORIGINAL REPORTING' 46X *TIME ( EM = XXXXXXXX ) //60X #HEADER-1 /44X '   T F   D   ' /44X '   X O I I   ' / 'REC  PM  DOC RET TIN TAXPAYER    CONTRACT   ' '  N R R S  ST        NAME-ADDRESS LINES' 26X 'AMOUNTS' / 'TYP YEAR CDE IND TYP    ID         PAYEE     ' ' D N A T  CD' / '--- ---- --- --- --- ---------  -------------' ' - - - -- --' 1X '-----------------------------------' 1X '-----------------------------------'
                ColumnSpacing(55),"IRS ORIGINAL REPORTING",new ColumnSpacing(46),Global.getTIME(), new ReportEditMask ("XXXXXXXX"),NEWLINE,NEWLINE,new ColumnSpacing(60),pnd_Header_1,NEWLINE,new 
                ColumnSpacing(44),"   T F   D   ",NEWLINE,new ColumnSpacing(44),"   X O I I   ",NEWLINE,"REC  PM  DOC RET TIN TAXPAYER    CONTRACT   ","  N R R S  ST        NAME-ADDRESS LINES",new 
                ColumnSpacing(26),"AMOUNTS",NEWLINE,"TYP YEAR CDE IND TYP    ID         PAYEE     "," D N A T  CD",NEWLINE,"--- ---- --- --- --- ---------  -------------"," - - - -- --",new 
                ColumnSpacing(1),"-----------------------------------",new ColumnSpacing(1),"-----------------------------------");
            if (Global.isEscape()) return;
            pnd_Rec_B_Ctr.setValue(0);                                                                                                                                    //Natural: MOVE 0 TO #REC-B-CTR
        }                                                                                                                                                                 //Natural: END-IF
        //*  11-23-99 FRANK
        if (condition(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld_A().equals(" ")))                                                                            //Natural: IF IRS-B-ME-ST-TAX-WITHHELD-A = ' '
        {
            ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld().setValue(0);                                                                                         //Natural: ASSIGN IRS-B-ME-ST-TAX-WITHHELD := 0
        }                                                                                                                                                                 //Natural: END-IF
        //*  11-23-99 FRANK
        if (condition(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld_A().equals(" ")))                                                                            //Natural: IF IRS-B-ME-LC-TAX-WITHHELD-A = ' '
        {
            ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld().setValue(0);                                                                                         //Natural: ASSIGN IRS-B-ME-LC-TAX-WITHHELD := 0
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Irs_B_Contract_Payee.setValue(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Contract_Payee());                                                                       //Natural: ASSIGN #WS-IRS-B-CONTRACT-PAYEE := IRS-B-CONTRACT-PAYEE
        if (condition(pnd_Ctr_1000_Pnd_Ctr_Low.equals(1)))                                                                                                                //Natural: IF #CTR-LOW = 1
        {
            pnd_Ws_Irs_B_Contract_Payee.setValue(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Contract_Payee());                                                                   //Natural: ASSIGN #WS-IRS-B-CONTRACT-PAYEE := IRS-B-CONTRACT-PAYEE
            getReports().skip(5, 1);                                                                                                                                      //Natural: SKIP ( 5 ) 1
            //*  ST DIST
            //*  LOC TAX
            getReports().write(5, new ColumnSpacing(1),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Record_Type(),new ColumnSpacing(2),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Year(),new  //Natural: WRITE ( 05 ) 1X IRS-B-RECORD-TYPE 2X IRS-B-PAYMENT-YEAR 2X IRS-B-DOCUMENT-CODE 2X IRS-B-RETURN-INDICATOR 3X IRS-B-TYPE-OF-TIN 2X IRS-B-TAXPAYER-ID-NUMB 2X #WS-IRS-B-CONT-PY ( EM = X ( 11 ) ) 4X IRS-B-TXBLE-NOT-DET-IND IRS-B-FOREIGN-INDICATOR 1X IRS-B-IRA-SEP-INDICATOR 1X IRS-B-DOCUMENT-CODE 1X IRS-B-FEDERAL-STATE-CODE 1X IRS-B-NAME-LINE-1 ( EM = X ( 35 ) ) 1X IRS-B-PAYMENT-AMOUNT-1 ( EM = ZZ,ZZZ,ZZ9.99 ) '(1)' 1X IRS-B-PAYMENT-AMOUNT-8 ( EM = ZZ,ZZZ,ZZ9.99 ) '(8)' / 59X IRS-B-NAME-LINE-2 ( EM = X ( 35 ) ) 1X IRS-B-PAYMENT-AMOUNT-2 ( EM = ZZ,ZZZ,ZZ9.99 ) '(2)' 1X IRS-B-PAYMENT-AMOUNT-9 ( EM = ZZ,ZZZ,ZZ9.99 ) '(9)' / 59X IRS-B-ADDRESS-LINE ( EM = X ( 35 ) ) 1X IRS-B-PAYMENT-AMOUNT-3 ( EM = ZZ,ZZZ,ZZ9.99 ) '(3)' 1X IRS-B-PAYMENT-AMOUNT-A ( EM = ZZ,ZZZ,ZZ9.99 ) '(A)' / 59X IRS-B-CITY ( EM = X ( 26 ) ) IRS-B-STATE IRS-B-ZIP ( EM = X ( 5 ) ) 01X IRS-B-PAYMENT-AMOUNT-4 ( EM = ZZ,ZZZ,ZZ9.99 ) '(4)' 01X IRS-B-PAYMENT-AMOUNT-1 ( EM = ZZ,ZZZ,ZZ9.99 ) '(S) ' / 95X IRS-B-PAYMENT-AMOUNT-5 ( EM = ZZ,ZZZ,ZZ9.99 ) '(5)' 01X IRS-B-ME-LC-TAX-WITHHELD ( EM = ZZ,ZZZ,ZZ9.99 ) '(C) ' / 95X IRS-B-PAYMENT-AMOUNT-6 ( EM = ZZ,ZZZ,ZZ9.99 ) '(6)' / 95X IRS-B-PAYMENT-AMOUNT-B ( EM = ZZ,ZZZ,ZZ9.99 ) '(B)'
                ColumnSpacing(2),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Document_Code(),new ColumnSpacing(2),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Return_Indicator(),new 
                ColumnSpacing(3),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Type_Of_Tin(),new ColumnSpacing(2),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Taxpayer_Id_Numb(),new 
                ColumnSpacing(2),pnd_Ws_Irs_B_Contract_Payee_Pnd_Ws_Irs_B_Cont_Py, new ReportEditMask ("XXXXXXXXXXX"),new ColumnSpacing(4),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Txble_Not_Det_Ind(),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Foreign_Indicator(),new 
                ColumnSpacing(1),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Ira_Sep_Indicator(),new ColumnSpacing(1),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Document_Code(),new 
                ColumnSpacing(1),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Federal_State_Code(),new ColumnSpacing(1),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Name_Line_1(), 
                new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new ColumnSpacing(1),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1(), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"(1)",new ColumnSpacing(1),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_8(), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.99"),"(8)",NEWLINE,new ColumnSpacing(59),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Name_Line_2(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new 
                ColumnSpacing(1),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_2(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"(2)",new ColumnSpacing(1),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_9(), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"(9)",NEWLINE,new ColumnSpacing(59),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Address_Line(), new ReportEditMask 
                ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new ColumnSpacing(1),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_3(), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.99"),"(3)",new ColumnSpacing(1),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_A(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"(A)",NEWLINE,new 
                ColumnSpacing(59),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_City(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXX"),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_State(),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Zip(), 
                new ReportEditMask ("XXXXX"),new ColumnSpacing(1),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_4(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"(4)",new 
                ColumnSpacing(1),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"(S) ",NEWLINE,new ColumnSpacing(95),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_5(), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"(5)",new ColumnSpacing(1),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld(), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.99"),"(C) ",NEWLINE,new ColumnSpacing(95),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_6(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"(6)",NEWLINE,new 
                ColumnSpacing(95),ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_B(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"(B)");
            if (Global.isEscape()) return;
            pnd_Rec_B_Ctr.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #REC-B-CTR
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Routine_C() throws Exception                                                                                                                         //Natural: ROUTINE-C
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Number_Of_Payees().equals(b_Record_Ctr)))                                                                       //Natural: IF IRS-C-NUMBER-OF-PAYEES = B-RECORD-CTR
        {
            pnd_Comment_C.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-C
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_C.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-C
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_1().equals(contr_Tot_1)))                                                                         //Natural: IF IRS-C-CONTROL-TOTAL-1 = CONTR-TOT-1
        {
            pnd_Comment_1.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_1.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_2().equals(contr_Tot_2)))                                                                         //Natural: IF IRS-C-CONTROL-TOTAL-2 = CONTR-TOT-2
        {
            pnd_Comment_2.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_2.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-2
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_3().equals(contr_Tot_3)))                                                                         //Natural: IF IRS-C-CONTROL-TOTAL-3 = CONTR-TOT-3
        {
            pnd_Comment_3.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-3
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_3.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-3
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_4().equals(contr_Tot_4)))                                                                         //Natural: IF IRS-C-CONTROL-TOTAL-4 = CONTR-TOT-4
        {
            pnd_Comment_4.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-4
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_4.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-4
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_5().equals(contr_Tot_5)))                                                                         //Natural: IF IRS-C-CONTROL-TOTAL-5 = CONTR-TOT-5
        {
            pnd_Comment_5.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-5
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_5.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-5
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_6().equals(contr_Tot_6)))                                                                         //Natural: IF IRS-C-CONTROL-TOTAL-6 = CONTR-TOT-6
        {
            pnd_Comment_6.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-6
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_6.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-6
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_7().equals(contr_Tot_7)))                                                                         //Natural: IF IRS-C-CONTROL-TOTAL-7 = CONTR-TOT-7
        {
            pnd_Comment_7.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-7
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_7.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-7
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_8().equals(contr_Tot_8)))                                                                         //Natural: IF IRS-C-CONTROL-TOTAL-8 = CONTR-TOT-8
        {
            pnd_Comment_8.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-8
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_8.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-8
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_9().equals(contr_Tot_9)))                                                                         //Natural: IF IRS-C-CONTROL-TOTAL-9 = CONTR-TOT-9
        {
            pnd_Comment_9.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-9
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_9.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-9
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(contr_Tot_B.equals(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_B())))                                                                         //Natural: IF CONTR-TOT-B = IRS-C-CONTROL-TOTAL-B
        {
            pnd_Comment_B.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-B
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_B.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-B
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Page_Ctr_1.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #PAGE-CTR-1
        getReports().newPage(new ReportSpecification(5));                                                                                                                 //Natural: NEWPAGE ( 5 )
        if (condition(Global.isEscape())){return;}
        //*  04-23-04 FRANK
        //*  04-23-04 FRANK
        //*  RCC
        getReports().write(5, new ColumnSpacing(50),"NON-PERIODIC CONTROL REPORT",pnd_Year_X_Pnd_Year,new ColumnSpacing(41),Global.getDATU(),NEWLINE,new                  //Natural: WRITE ( 5 ) 50X 'NON-PERIODIC CONTROL REPORT' #YEAR 41X *DATU / 55X 'IRS ORIGINAL REPORTING' 46X *TIME ( EM = XXXXXXXX ) // 60X #HEADER-1 // 60X 'U.S. FEDERAL' //// 7X 'TOTAL DETAIL AMOUNTS       TOTAL TRAILER AMOUNTS' 7X 'BALANCED' / 7X '--------------------       ---------------------' 7X '--------' /// 'COUNT ' 11X B-RECORD-CTR ( EM = ZZ,ZZZ,ZZ9 ) 18X IRS-C-NUMBER-OF-PAYEES ( EM = ZZ,ZZZ,ZZ9 ) 7X #COMMENT-C /// '(1)   ' CONTR-TOT-1 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 8X IRS-C-CONTROL-TOTAL-1 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #COMMENT-1 // '(2)   ' CONTR-TOT-2 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 8X IRS-C-CONTROL-TOTAL-2 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #COMMENT-2 // '(3)   ' CONTR-TOT-3 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 8X IRS-C-CONTROL-TOTAL-3 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #COMMENT-3 // '(4)   ' CONTR-TOT-4 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 8X IRS-C-CONTROL-TOTAL-4 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #COMMENT-4 // '(5)   ' CONTR-TOT-5 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 8X IRS-C-CONTROL-TOTAL-5 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #COMMENT-5 // '(6)   ' CONTR-TOT-6 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 8X IRS-C-CONTROL-TOTAL-6 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #COMMENT-6 // '(7)   ' CONTR-TOT-7 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 8X IRS-C-CONTROL-TOTAL-7 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #COMMENT-7 // '(8)   ' CONTR-TOT-8 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 8X IRS-C-CONTROL-TOTAL-8 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #COMMENT-8 // '(9)   ' CONTR-TOT-9 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 8X IRS-C-CONTROL-TOTAL-9 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #COMMENT-9 // '(B)   ' CONTR-TOT-B ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 8X IRS-C-CONTROL-TOTAL-B ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #COMMENT-B
            ColumnSpacing(55),"IRS ORIGINAL REPORTING",new ColumnSpacing(46),Global.getTIME(), new ReportEditMask ("XXXXXXXX"),NEWLINE,NEWLINE,new ColumnSpacing(60),pnd_Header_1,NEWLINE,NEWLINE,new 
            ColumnSpacing(60),"U.S. FEDERAL",NEWLINE,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(7),"TOTAL DETAIL AMOUNTS       TOTAL TRAILER AMOUNTS",new 
            ColumnSpacing(7),"BALANCED",NEWLINE,new ColumnSpacing(7),"--------------------       ---------------------",new ColumnSpacing(7),"--------",NEWLINE,NEWLINE,NEWLINE,"COUNT ",new 
            ColumnSpacing(11),b_Record_Ctr, new ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(18),ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Number_Of_Payees(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(7),pnd_Comment_C,NEWLINE,NEWLINE,NEWLINE,"(1)   ",contr_Tot_1, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(8),ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_1(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Comment_1,NEWLINE,NEWLINE,"(2)   ",contr_Tot_2, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_2(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(7),pnd_Comment_2,NEWLINE,NEWLINE,"(3)   ",contr_Tot_3, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_3(), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Comment_3,NEWLINE,NEWLINE,"(4)   ",contr_Tot_4, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(8),ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_4(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Comment_4,NEWLINE,NEWLINE,"(5)   ",contr_Tot_5, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_5(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(7),pnd_Comment_5,NEWLINE,NEWLINE,"(6)   ",contr_Tot_6, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_6(), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Comment_6,NEWLINE,NEWLINE,"(7)   ",contr_Tot_7, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(8),ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_7(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Comment_7,NEWLINE,NEWLINE,"(8)   ",contr_Tot_8, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_8(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(7),pnd_Comment_8,NEWLINE,NEWLINE,"(9)   ",contr_Tot_9, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_9(), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Comment_9,NEWLINE,NEWLINE,"(B)   ",contr_Tot_B, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(8),ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_B(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Comment_B);
        if (Global.isEscape()) return;
    }
    //*  NEW FOR 1991
    private void sub_Routine_K() throws Exception                                                                                                                         //Natural: ROUTINE-K
    {
        if (BLNatReinput.isReinput()) return;

        //*  12-09-00 FRANK
        pnd_K.reset();                                                                                                                                                    //Natural: RESET #K
        FOR03:                                                                                                                                                            //Natural: FOR #Z = 01 TO #MAX-STATES
        for (pnd_Z.setValue(1); condition(pnd_Z.lessOrEqual(pnd_Max_States)); pnd_Z.nadd(1))
        {
            if (condition(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_State_Code().equals(pnd_Ws_State_Code_2.getValue(pnd_Z))))                                                  //Natural: IF IRS-K-STATE-CODE = #WS-STATE-CODE-2 ( #Z )
            {
                pnd_K.setValue(state_Index.getValue(pnd_Z));                                                                                                              //Natural: ASSIGN #K := STATE-INDEX ( #Z )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Number_Of_Payees().equals(state_Totals_St_B_Rec_Ctr.getValue(pnd_K))))                                          //Natural: IF IRS-K-NUMBER-OF-PAYEES = ST-B-REC-CTR ( #K )
        {
            pnd_Comment_C.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-C
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_C.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-C
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_1().equals(state_Totals_State_Tot_1.getValue(pnd_K))))                                            //Natural: IF IRS-K-CONTROL-TOTAL-1 = STATE-TOT-1 ( #K )
        {
            pnd_Comment_1.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_1.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_2().equals(state_Totals_State_Tot_2.getValue(pnd_K))))                                            //Natural: IF IRS-K-CONTROL-TOTAL-2 = STATE-TOT-2 ( #K )
        {
            pnd_Comment_2.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_2.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-2
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_3().equals(state_Totals_State_Tot_3.getValue(pnd_K))))                                            //Natural: IF IRS-K-CONTROL-TOTAL-3 = STATE-TOT-3 ( #K )
        {
            pnd_Comment_3.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-3
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_3.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-3
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_4().equals(state_Totals_State_Tot_4.getValue(pnd_K))))                                            //Natural: IF IRS-K-CONTROL-TOTAL-4 = STATE-TOT-4 ( #K )
        {
            pnd_Comment_4.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-4
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_4.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-4
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_5().equals(state_Totals_State_Tot_5.getValue(pnd_K))))                                            //Natural: IF IRS-K-CONTROL-TOTAL-5 = STATE-TOT-5 ( #K )
        {
            pnd_Comment_5.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-5
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_5.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-5
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_6().equals(state_Totals_State_Tot_6.getValue(pnd_K))))                                            //Natural: IF IRS-K-CONTROL-TOTAL-6 = STATE-TOT-6 ( #K )
        {
            pnd_Comment_6.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-6
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_6.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-6
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_7().equals(state_Totals_State_Tot_7.getValue(pnd_K))))                                            //Natural: IF IRS-K-CONTROL-TOTAL-7 = STATE-TOT-7 ( #K )
        {
            pnd_Comment_7.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-7
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_7.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-7
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_8().equals(state_Totals_State_Tot_8.getValue(pnd_K))))                                            //Natural: IF IRS-K-CONTROL-TOTAL-8 = STATE-TOT-8 ( #K )
        {
            pnd_Comment_8.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-8
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_8.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-8
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_9().equals(state_Totals_State_Tot_9.getValue(pnd_K))))                                            //Natural: IF IRS-K-CONTROL-TOTAL-9 = STATE-TOT-9 ( #K )
        {
            pnd_Comment_9.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-9
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_9.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-9
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_A().equals(state_Totals_State_Tot_10.getValue(pnd_K))))                                           //Natural: IF IRS-K-CONTROL-TOTAL-A = STATE-TOT-10 ( #K )
        {
            pnd_Comment_10.setValue("  YES   ");                                                                                                                          //Natural: MOVE '  YES   ' TO #COMMENT-10
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_10.setValue("** NO **");                                                                                                                          //Natural: MOVE '** NO **' TO #COMMENT-10
        }                                                                                                                                                                 //Natural: END-IF
        //*   IRS-K-CONTROL-TOTAL-B IS NOW IRR-AMT TOTAL            /*  RCC
        //*   STATE-TOT-11(#K)  IS ALWAYS ZERO                      /*
        //*                                                         /*
        //*   IF   IRS-K-CONTROL-TOTAL-B = STATE-TOT-11 (#K)        /*
        //*   MOVE '  YES   ' TO #COMMENT-11                        /*
        //*  ELSE MOVE '** NO **' TO #COMMENT-11                    /*
        //*  END-IF                                                 /*
        if (condition(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_B().equals(state_Totals_State_Tot_B.getValue(pnd_K))))                                            //Natural: IF IRS-K-CONTROL-TOTAL-B = STATE-TOT-B ( #K )
        {
            pnd_Comment_B.setValue("  YES   ");                                                                                                                           //Natural: MOVE '  YES   ' TO #COMMENT-B
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comment_B.setValue("** NO **");                                                                                                                           //Natural: MOVE '** NO **' TO #COMMENT-B
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Page_Ctr_1.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #PAGE-CTR-1
        getReports().newPage(new ReportSpecification(5));                                                                                                                 //Natural: NEWPAGE ( 5 )
        if (condition(Global.isEscape())){return;}
        getReports().write(5, new ColumnSpacing(50),"NON-PERIODIC CONTROL REPORT",pnd_Year_X_Pnd_Year,new ColumnSpacing(41),Global.getDATU(),NEWLINE,new                  //Natural: WRITE ( 5 ) 50X 'NON-PERIODIC CONTROL REPORT' #YEAR 41X *DATU / 55X 'IRS ORIGINAL REPORTING' 46X *TIME ( EM = XXXXXXXX ) // 60X #HEADER-1 // 60X IRS-K-STATE-CODE STATE-NAME ( #K ) //// 7X 'TOTAL DETAIL AMOUNTS       TOTAL TRAILER AMOUNTS' 7X 'BALANCED' / 7X '--------------------       ---------------------' 7X '--------' /// 'COUNT ' 11X ST-B-REC-CTR ( #K ) ( EM = ZZZ,ZZ9 ) 21X IRS-K-NUMBER-OF-PAYEES ( EM = ZZZ,ZZ9 ) 10X #COMMENT-C /// '(1)   ' STATE-TOT-1 ( #K ) ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 8X IRS-K-CONTROL-TOTAL-1 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #COMMENT-1 // '(2)   ' STATE-TOT-2 ( #K ) ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 8X IRS-K-CONTROL-TOTAL-2 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #COMMENT-2 // '(3)   ' STATE-TOT-3 ( #K ) ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 8X IRS-K-CONTROL-TOTAL-3 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #COMMENT-3 // '(4)   ' STATE-TOT-4 ( #K ) ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 8X IRS-K-CONTROL-TOTAL-4 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #COMMENT-4 // '(5)   ' STATE-TOT-5 ( #K ) ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 8X IRS-K-CONTROL-TOTAL-5 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #COMMENT-5 // '(6)   ' STATE-TOT-6 ( #K ) ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 8X IRS-K-CONTROL-TOTAL-6 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #COMMENT-6 // '(7)   ' STATE-TOT-7 ( #K ) ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 8X IRS-K-CONTROL-TOTAL-7 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #COMMENT-7 // '(8)   ' STATE-TOT-8 ( #K ) ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 8X IRS-K-CONTROL-TOTAL-8 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #COMMENT-8 // '(9)   ' STATE-TOT-9 ( #K ) ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 8X IRS-K-CONTROL-TOTAL-9 ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #COMMENT-9 // '(10)  ' STATE-TOT-10 ( #K ) ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 8X IRS-K-CONTROL-TOTAL-A ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #COMMENT-10 // '(11)  ' STATE-TOT-11 ( #K ) ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) // '(B)  ' STATE-TOT-B ( #K ) ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 8X IRS-K-CONTROL-TOTAL-B ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #COMMENT-B
            ColumnSpacing(55),"IRS ORIGINAL REPORTING",new ColumnSpacing(46),Global.getTIME(), new ReportEditMask ("XXXXXXXX"),NEWLINE,NEWLINE,new ColumnSpacing(60),pnd_Header_1,NEWLINE,NEWLINE,new 
            ColumnSpacing(60),ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_State_Code(),state_Totals_State_Name.getValue(pnd_K),NEWLINE,NEWLINE,NEWLINE,NEWLINE,new 
            ColumnSpacing(7),"TOTAL DETAIL AMOUNTS       TOTAL TRAILER AMOUNTS",new ColumnSpacing(7),"BALANCED",NEWLINE,new ColumnSpacing(7),"--------------------       ---------------------",new 
            ColumnSpacing(7),"--------",NEWLINE,NEWLINE,NEWLINE,"COUNT ",new ColumnSpacing(11),state_Totals_St_B_Rec_Ctr.getValue(pnd_K), new ReportEditMask 
            ("ZZZ,ZZ9"),new ColumnSpacing(21),ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Number_Of_Payees(), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(10),pnd_Comment_C,NEWLINE,NEWLINE,NEWLINE,"(1)   ",state_Totals_State_Tot_1.getValue(pnd_K), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_1(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(7),pnd_Comment_1,NEWLINE,NEWLINE,"(2)   ",state_Totals_State_Tot_2.getValue(pnd_K), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(8),ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_2(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Comment_2,NEWLINE,NEWLINE,"(3)   ",state_Totals_State_Tot_3.getValue(pnd_K), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_3(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(7),pnd_Comment_3,NEWLINE,NEWLINE,"(4)   ",state_Totals_State_Tot_4.getValue(pnd_K), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(8),ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_4(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Comment_4,NEWLINE,NEWLINE,"(5)   ",state_Totals_State_Tot_5.getValue(pnd_K), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_5(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(7),pnd_Comment_5,NEWLINE,NEWLINE,"(6)   ",state_Totals_State_Tot_6.getValue(pnd_K), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(8),ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_6(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Comment_6,NEWLINE,NEWLINE,"(7)   ",state_Totals_State_Tot_7.getValue(pnd_K), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_7(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(7),pnd_Comment_7,NEWLINE,NEWLINE,"(8)   ",state_Totals_State_Tot_8.getValue(pnd_K), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(8),ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_8(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Comment_8,NEWLINE,NEWLINE,"(9)   ",state_Totals_State_Tot_9.getValue(pnd_K), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_9(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(7),pnd_Comment_9,NEWLINE,NEWLINE,"(10)  ",state_Totals_State_Tot_10.getValue(pnd_K), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(8),ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_A(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Comment_10,NEWLINE,NEWLINE,"(11)  ",state_Totals_State_Tot_11.getValue(pnd_K), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,"(B)  ",state_Totals_State_Tot_B.getValue(pnd_K), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(8),ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_B(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Comment_B);
        if (Global.isEscape()) return;
    }
    private void sub_Routine_F() throws Exception                                                                                                                         //Natural: ROUTINE-F
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Page_Ctr_1.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #PAGE-CTR-1
        getReports().newPage(new ReportSpecification(5));                                                                                                                 //Natural: NEWPAGE ( 5 )
        if (condition(Global.isEscape())){return;}
        getReports().write(5, new ColumnSpacing(50),"NON-PERIODIC CONTROL REPORT",pnd_Year_X_Pnd_Year,new ColumnSpacing(41),Global.getDATU(),NEWLINE,new                  //Natural: WRITE ( 5 ) 50X 'NON-PERIODIC CONTROL REPORT' #YEAR 41X *DATU /55X 'IRS ORIGINAL REPORTING' 46X *TIME ( EM = XXXXXXXX ) ///// 7X 'TOTAL NUMBER OF A-TYPE RECORDS ON THE TAPE: ' #REC-A-CTR ( EM = Z,ZZ9 ) /// 7X 'TOTAL NUMBER OF A-TYPE RECORDS ON TRAILER : ' IRS-F-NUMBER-OF-A-RECORDS ( EM = Z,ZZ9 )
            ColumnSpacing(55),"IRS ORIGINAL REPORTING",new ColumnSpacing(46),Global.getTIME(), new ReportEditMask ("XXXXXXXX"),NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new 
            ColumnSpacing(7),"TOTAL NUMBER OF A-TYPE RECORDS ON THE TAPE: ",pnd_Rec_A_Ctr, new ReportEditMask ("Z,ZZ9"),NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(7),"TOTAL NUMBER OF A-TYPE RECORDS ON TRAILER : ",ldaTwrl3324.getIrs_F_Out_Tape_Irs_F_Number_Of_A_Records(), 
            new ReportEditMask ("Z,ZZ9"));
        if (Global.isEscape()) return;
    }
    //*  12-09-00 FRANK
    private void sub_Load_State_Table() throws Exception                                                                                                                  //Natural: LOAD-STATE-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                             //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
        pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().setValue(pnd_Ws_Tax_Year);                                                                                                 //Natural: ASSIGN TWRATBL2.#TAX-YEAR := #WS-TAX-YEAR
        pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().setValue(false);                                                                                                          //Natural: ASSIGN TWRATBL2.#ABEND-IND := FALSE
        pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().setValue(false);                                                                                                        //Natural: ASSIGN TWRATBL2.#DISPLAY-IND := FALSE
        FOR04:                                                                                                                                                            //Natural: FOR #I = 0 TO 99
        for (pnd_I.setValue(0); condition(pnd_I.lessOrEqual(99)); pnd_I.nadd(1))
        {
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValueEdited(pnd_I,new ReportEditMask("999"));                                                                      //Natural: MOVE EDITED #I ( EM = 999 ) TO TWRATBL2.#STATE-CDE
            DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
            if (condition(pdaTwratbl2.getTwratbl2_Pnd_Return_Cde().equals(true) || ! (pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().getBoolean())))                           //Natural: IF #RETURN-CDE = TRUE OR NOT TWRATBL2.#DISPLAY-IND
            {
                pnd_State_Table_Pnd_Ws_State_Seq_Nbr.getValue(pnd_I.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_Seq_Nbr());                                    //Natural: ASSIGN #WS-STATE-SEQ-NBR ( #I ) := TIRCNTL-SEQ-NBR
                pnd_State_Table_Pnd_Ws_State_Alpha.getValue(pnd_I.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code());                             //Natural: ASSIGN #WS-STATE-ALPHA ( #I ) := TWRATBL2.TIRCNTL-STATE-ALPHA-CODE
                pnd_State_Table_Pnd_Ws_State_Full_Name.getValue(pnd_I.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Full_Name());                          //Natural: ASSIGN #WS-STATE-FULL-NAME ( #I ) := TIRCNTL-STATE-FULL-NAME
                pnd_State_Table_Pnd_Ws_State_Tax_Id_Tiaa.getValue(pnd_I.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                      //Natural: ASSIGN #WS-STATE-TAX-ID-TIAA ( #I ) := TIRCNTL-STATE-TAX-ID-TIAA
                pnd_State_Table_Pnd_Ws_State_Tax_Id_Cref.getValue(pnd_I.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Cref());                      //Natural: ASSIGN #WS-STATE-TAX-ID-CREF ( #I ) := TIRCNTL-STATE-TAX-ID-CREF
                pnd_State_Table_Pnd_Ws_State_Tax_Id_Life.getValue(pnd_I.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Life());                      //Natural: ASSIGN #WS-STATE-TAX-ID-LIFE ( #I ) := TIRCNTL-STATE-TAX-ID-LIFE
                pnd_State_Table_Pnd_Ws_State_Tax_Id_Trst.getValue(pnd_I.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Trust());                     //Natural: ASSIGN #WS-STATE-TAX-ID-TRST ( #I ) := TIRCNTL-STATE-TAX-ID-TRUST
                pnd_State_Table_Pnd_Ws_State_Comb_Fed_Ind.getValue(pnd_I.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_Comb_Fed_Ind());                          //Natural: ASSIGN #WS-STATE-COMB-FED-IND ( #I ) := TWRATBL2.TIRCNTL-COMB-FED-IND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(5, "LS=132 PS=60");
        Global.format(0, "LS=132 PS=60");

        getReports().write(5, ReportOption.TITLE,"TWRP3320-01",new ColumnSpacing(41),"TAX INFORMATION AND REPORTING",new ColumnSpacing(41),"PAGE",pnd_Page_Ctr_1, 
            new ReportEditMask ("ZZZ9"));
    }
}
