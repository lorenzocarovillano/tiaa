/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:35:30 PM
**        * FROM NATURAL PROGRAM : Twrp3521
************************************************************
**        * FILE NAME            : Twrp3521.java
**        * CLASS NAME           : Twrp3521
**        * INSTANCE NAME        : Twrp3521
************************************************************
************************************************************************
** PROGRAM : TWRP3521
** SYSTEM  : TAXWARS
** AUTHOR  : JON ROTHOLZ
** FUNCTION: DRIVER PROGRAM FOR MAG MEDIA STATE CORRECTION REPORTING
** HISTORY.....:
**    06/15/2012 - J.ROTHOLZ - CLONED FROM TWRP3520 FOR MAG MEDIA
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3521 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input_Parms;
    private DbsField pnd_Input_Parms_Pnd_State;
    private DbsField pnd_Input_Parms_Pnd_Tax_Year;

    private DbsGroup pnd_Input_Parms__R_Field_1;
    private DbsField pnd_Input_Parms_Pnd_Tax_Year_A;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Input_Parms = localVariables.newGroupInRecord("pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Input_Parms_Pnd_State = pnd_Input_Parms.newFieldInGroup("pnd_Input_Parms_Pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_Input_Parms_Pnd_Tax_Year = pnd_Input_Parms.newFieldInGroup("pnd_Input_Parms_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);

        pnd_Input_Parms__R_Field_1 = pnd_Input_Parms.newGroupInGroup("pnd_Input_Parms__R_Field_1", "REDEFINE", pnd_Input_Parms_Pnd_Tax_Year);
        pnd_Input_Parms_Pnd_Tax_Year_A = pnd_Input_Parms__R_Field_1.newFieldInGroup("pnd_Input_Parms_Pnd_Tax_Year_A", "#TAX-YEAR-A", FieldType.STRING, 
            4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3521() throws Exception
    {
        super("Twrp3521");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT-PARMS
        sub_Process_Input_Parms();
        if (condition(Global.isEscape())) {return;}
        //*  FIRST YEAR OF MAG MEDIA STATE CORRECTION
        short decideConditionsMet25 = 0;                                                                                                                                  //Natural: DECIDE ON FIRST VALUE #TAX-YEAR;//Natural: VALUE 2011:9999
        if (condition(((pnd_Input_Parms_Pnd_Tax_Year.greaterOrEqual(2011) && pnd_Input_Parms_Pnd_Tax_Year.lessOrEqual(9999)))))
        {
            decideConditionsMet25++;
            //*  CHANGE YEAR VALUE AND FETCHED PROGRAM
            //*  IN FUTURE YEARS IF NEW MODULES ARE NEEDED
            Global.getSTACK().pushData(StackOption.TOP, pnd_Input_Parms_Pnd_State, pnd_Input_Parms_Pnd_Tax_Year);                                                         //Natural: FETCH 'TWRP3554' #STATE #TAX-YEAR
            Global.setFetchProgram(DbsUtil.getBlType("TWRP3554"));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            getReports().write(0, " ************************************** ",NEWLINE," ***                                *** ",NEWLINE," ***   INVALID TAX YEAR PARAMETER   *** ", //Natural: WRITE ' ************************************** ' / ' ***                                *** ' / ' ***   INVALID TAX YEAR PARAMETER   *** ' / ' ***     Value Must be >= 2011      *** ' / ' ***                                *** ' / ' ************************************** '
                NEWLINE," ***     Value Must be >= 2011      *** ",NEWLINE," ***                                *** ",NEWLINE," ************************************** ");
            if (Global.isEscape()) return;
            DbsUtil.terminate(92);  if (true) return;                                                                                                                     //Natural: TERMINATE 92
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INPUT-PARMS
        //* ************************************
        //* **
    }
    private void sub_Process_Input_Parms() throws Exception                                                                                                               //Natural: PROCESS-INPUT-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("TWRP3521|sub_Process_Input_Parms");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parms);                                                                                      //Natural: INPUT #INPUT-PARMS
                if (condition(! (DbsUtil.maskMatches(pnd_Input_Parms_Pnd_State,"NN")) || ! (DbsUtil.maskMatches(pnd_Input_Parms_Pnd_Tax_Year_A,"NNNN"))))                 //Natural: IF #STATE NE MASK ( NN ) OR #TAX-YEAR-A NE MASK ( NNNN )
                {
                    getReports().write(0, " ************************************ ",NEWLINE," ***                              *** ",NEWLINE," ***  INPUT PARAMETER(S) INVALID  *** ", //Natural: WRITE ' ************************************ ' / ' ***                              *** ' / ' ***  INPUT PARAMETER(S) INVALID  *** ' / ' ***    PLEASE INFORM SYSTEMS !!  *** ' / ' ***                              *** ' / ' ************************************ '
                        NEWLINE," ***    PLEASE INFORM SYSTEMS !!  *** ",NEWLINE," ***                              *** ",NEWLINE," ************************************ ");
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(90);  if (true) return;                                                                                                             //Natural: TERMINATE 90
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Parms_Pnd_Tax_Year.less(2011)))                                                                                                   //Natural: IF #TAX-YEAR < 2011
                {
                    getReports().write(0, " ************************************* ",NEWLINE," ***                               *** ",NEWLINE," ***       INVALID PARAMETER       *** ", //Natural: WRITE ' ************************************* ' / ' ***                               *** ' / ' ***       INVALID PARAMETER       *** ' / ' ***   Mag Media State Correction  *** ' / ' ***         Began in 2011         *** ' / ' ***                               *** ' / ' ************************************* '
                        NEWLINE," ***   Mag Media State Correction  *** ",NEWLINE," ***         Began in 2011         *** ",NEWLINE," ***                               *** ",
                        NEWLINE," ************************************* ");
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(91);  if (true) return;                                                                                                             //Natural: TERMINATE 91
                }                                                                                                                                                         //Natural: END-IF
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //
}
