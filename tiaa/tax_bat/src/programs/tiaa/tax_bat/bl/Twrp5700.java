/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:41:39 PM
**        * FROM NATURAL PROGRAM : Twrp5700
************************************************************
**        * FILE NAME            : Twrp5700.java
**        * CLASS NAME           : Twrp5700
**        * INSTANCE NAME        : Twrp5700
************************************************************
************************************************************************
* PROGRAM : TWRP5700
* SYSTEM  : TAXWARS
* FUNCTION: NEW REPORT TO DISPLAY W2 DATA ONLY. OUTPUT FROM TWRP5501
*           IS USED AS INPUT
* HISTORY.....:
* 02/18/15: OS - RECOMPILED FOR UPDATED TWRL0900
*
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5700 extends BLNatBase
{
    // Data Areas
    private PdaTwracomp pdaTwracomp;
    private LdaTwrl0600 ldaTwrl0600;
    private LdaTwrl0900 ldaTwrl0900;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Pnd_Cntl_Max;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_Company_Line;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Ws_Pnd_S1;
    private DbsField pnd_Ws_Pnd_S2;
    private DbsField pnd_Ws_Pnd_Tran_Cv;
    private DbsField pnd_Ws_Pnd_Gross_Cv;
    private DbsField pnd_Ws_Pnd_Fed_Cv;
    private DbsField pnd_Ws_Pnd_Sta_Cv;
    private DbsField pnd_Ws_Pnd_Loc_Cv;

    private DbsGroup pnd_Cntl;
    private DbsField pnd_Cntl_Pnd_Cntl_Text;
    private DbsField pnd_Cntl_Pnd_Tran_Cnt_Cv;
    private DbsField pnd_Cntl_Pnd_Gross_Amt_Cv;
    private DbsField pnd_Cntl_Pnd_Fed_Tax_Cv;
    private DbsField pnd_Cntl_Pnd_Sta_Tax_Cv;
    private DbsField pnd_Cntl_Pnd_Loc_Tax_Cv;

    private DbsGroup pnd_Cntl_Pnd_Totals;
    private DbsField pnd_Cntl_Pnd_Tran_Cnt;
    private DbsField pnd_Cntl_Pnd_Gross_Amt;
    private DbsField pnd_Cntl_Pnd_Fed_Tax;
    private DbsField pnd_Cntl_Pnd_Sta_Tax;
    private DbsField pnd_Cntl_Pnd_Loc_Tax;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Twrpymnt_Company_Cde_FormOld;
    private DbsField readWork01Twrpymnt_Tax_YearOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwracomp = new PdaTwracomp(localVariables);
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);
        ldaTwrl0900 = new LdaTwrl0900();
        registerRecord(ldaTwrl0900);

        // Local Variables

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Pnd_Cntl_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_Cntl_Max", "#CNTL-MAX", FieldType.PACKED_DECIMAL, 3);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Tax_Year = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Company_Line = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Line", "#COMPANY-LINE", FieldType.STRING, 72);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S1", "#S1", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S2", "#S2", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Tran_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tran_Cv", "#TRAN-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Gross_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Gross_Cv", "#GROSS-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Fed_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Fed_Cv", "#FED-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Sta_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Sta_Cv", "#STA-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Loc_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Loc_Cv", "#LOC-CV", FieldType.ATTRIBUTE_CONTROL, 2);

        pnd_Cntl = localVariables.newGroupArrayInRecord("pnd_Cntl", "#CNTL", new DbsArrayController(1, 6));
        pnd_Cntl_Pnd_Cntl_Text = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Cntl_Text", "#CNTL-TEXT", FieldType.STRING, 17);
        pnd_Cntl_Pnd_Tran_Cnt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Tran_Cnt_Cv", "#TRAN-CNT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Gross_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Gross_Amt_Cv", "#GROSS-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Fed_Tax_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Fed_Tax_Cv", "#FED-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Sta_Tax_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Sta_Tax_Cv", "#STA-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Loc_Tax_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Loc_Tax_Cv", "#LOC-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 2);

        pnd_Cntl_Pnd_Totals = pnd_Cntl.newGroupArrayInGroup("pnd_Cntl_Pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Cntl_Pnd_Tran_Cnt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Tran_Cnt", "#TRAN-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Cntl_Pnd_Gross_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Cntl_Pnd_Fed_Tax = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Fed_Tax", "#FED-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Sta_Tax = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Sta_Tax", "#STA-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Loc_Tax = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Loc_Tax", "#LOC-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Twrpymnt_Company_Cde_FormOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrpymnt_Company_Cde_Form_OLD", "Twrpymnt_Company_Cde_Form_OLD", 
            FieldType.STRING, 1);
        readWork01Twrpymnt_Tax_YearOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrpymnt_Tax_Year_OLD", "Twrpymnt_Tax_Year_OLD", FieldType.NUMERIC, 
            4);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaTwrl0600.initializeValues();
        ldaTwrl0900.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Pnd_Cntl_Max.setInitialValue(6);
        pnd_Cntl_Pnd_Cntl_Text.getValue(1).setInitialValue("All Payments");
        pnd_Cntl_Pnd_Cntl_Text.getValue(2).setInitialValue("Active Payments");
        pnd_Cntl_Pnd_Cntl_Text.getValue(3).setInitialValue("Periodic Payments");
        pnd_Cntl_Pnd_Cntl_Text.getValue(4).setInitialValue("Lump Sum / RTB");
        pnd_Cntl_Pnd_Cntl_Text.getValue(5).setInitialValue("Interest");
        pnd_Cntl_Pnd_Cntl_Text.getValue(6).setInitialValue("Form Total");
        pnd_Cntl_Pnd_Tran_Cnt_Cv.setInitialAttributeValue("AD=I)#CNTL.#TRAN-CNT-CV(2)	(AD=I)#CNTL.#TRAN-CNT-CV(3)	(AD=N)#CNTL.#TRAN-CNT-CV(4)	(AD=N)#CNTL.#TRAN-CNT-CV(5)	(AD=N)#CNTL.#TRAN-CNT-CV(6)	(AD=N");
        pnd_Cntl_Pnd_Gross_Amt_Cv.setInitialAttributeValue("AD=I)#CNTL.#GROSS-AMT-CV(2)	(AD=I)#CNTL.#GROSS-AMT-CV(3)	(AD=I)#CNTL.#GROSS-AMT-CV(4)	(AD=I)#CNTL.#GROSS-AMT-CV(5)	(AD=I)#CNTL.#GROSS-AMT-CV(6)	(AD=I");
        pnd_Cntl_Pnd_Fed_Tax_Cv.setInitialAttributeValue("AD=I)#CNTL.#FED-TAX-CV(2)	(AD=I)#CNTL.#FED-TAX-CV(3)	(AD=I)#CNTL.#FED-TAX-CV(4)	(AD=I)#CNTL.#FED-TAX-CV(5)	(AD=N)#CNTL.#FED-TAX-CV(6)	(AD=I");
        pnd_Cntl_Pnd_Sta_Tax_Cv.setInitialAttributeValue("AD=I)#CNTL.#STA-TAX-CV(2)	(AD=I)#CNTL.#STA-TAX-CV(3)	(AD=I)#CNTL.#STA-TAX-CV(4)	(AD=I)#CNTL.#STA-TAX-CV(5)	(AD=N)#CNTL.#STA-TAX-CV(6)	(AD=I");
        pnd_Cntl_Pnd_Loc_Tax_Cv.setInitialAttributeValue("AD=I)#CNTL.#LOC-TAX-CV(2)	(AD=I)#CNTL.#LOC-TAX-CV(3)	(AD=I)#CNTL.#LOC-TAX-CV(4)	(AD=I)#CNTL.#LOC-TAX-CV(5)	(AD=N)#CNTL.#LOC-TAX-CV(6)	(AD=I");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5700() throws Exception
    {
        super("Twrp5700");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 1 ) PS = 58 LS = 133 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 56T 'Summary of W2 Transactions' 120T 'Report: RPT1' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / #WS.#COMPANY-LINE //
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
                                                                                                                                                                          //Natural: PERFORM PROCESS-CONTROL-RECORD
        sub_Process_Control_Record();
        if (condition(Global.isEscape())) {return;}
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 RECORD #XTAXYR-F94
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaTwrl0900.getPnd_Xtaxyr_F94())))
        {
            CheckAtStartofData204();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*  PP                                                                                                                                                       //Natural: AT START OF DATA;//Natural: AT BREAK OF #XTAXYR-F94.TWRPYMNT-COMPANY-CDE-FORM
            if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Paymt_Category().equals("1")))                                                                           //Natural: IF #XTAXYR-F94.TWRPYMNT-PAYMT-CATEGORY = '1'
            {
                pnd_Ws_Pnd_J.setValue(3);                                                                                                                                 //Natural: ASSIGN #WS.#J := 3
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_J.setValue(4);                                                                                                                                 //Natural: ASSIGN #WS.#J := 4
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO #XTAXYR-F94.#C-TWRPYMNT-PAYMENTS
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments())); pnd_Ws_Pnd_I.nadd(1))
            {
                //*                                                     ALL PAYMENTS
                if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("7") && (ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payment_Type().getValue(pnd_Ws_Pnd_I).equals("M")  //Natural: IF #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE = '7' AND ( #XTAXYR-F94.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'M' AND ( #XTAXYR-F94.TWRPYMNT-SETTLE-TYPE ( #I ) = 'S' OR = 'T' OR = 'E' OR = 'M' OR = 'U' ) )
                    && ((((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("S") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("T")) 
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("E")) || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("M")) 
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("U"))))))
                {
                    //*                                                     ALL PAYMENTS
                    pnd_Cntl_Pnd_Tran_Cnt.getValue(1,1).nadd(1);                                                                                                          //Natural: ADD 1 TO #CNTL.#TRAN-CNT ( 1,1 )
                    pnd_Cntl_Pnd_Gross_Amt.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 1,1 )
                    pnd_Cntl_Pnd_Fed_Tax.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                               //Natural: ADD #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #CNTL.#FED-TAX ( 1,1 )
                    pnd_Cntl_Pnd_Sta_Tax.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) TO #CNTL.#STA-TAX ( 1,1 )
                    pnd_Cntl_Pnd_Loc_Tax.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Local_Wthld().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-LOCAL-WTHLD ( #I ) TO #CNTL.#LOC-TAX ( 1,1 )
                    pnd_Cntl_Pnd_Gross_Amt.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                   //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 1,1 )
                    if (condition(! (ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals(" ") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals("C")))) //Natural: IF NOT #XTAXYR-F94.TWRPYMNT-PYMNT-STATUS ( #I ) = ' ' OR = 'C'
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*                                                   ACTIVE PAYMENTS
                    pnd_Cntl_Pnd_Tran_Cnt.getValue(2,1).nadd(1);                                                                                                          //Natural: ADD 1 TO #CNTL.#TRAN-CNT ( 2,1 )
                    pnd_Cntl_Pnd_Gross_Amt.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 2,1 )
                    pnd_Cntl_Pnd_Fed_Tax.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                               //Natural: ADD #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #CNTL.#FED-TAX ( 2,1 )
                    pnd_Cntl_Pnd_Sta_Tax.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) TO #CNTL.#STA-TAX ( 2,1 )
                    pnd_Cntl_Pnd_Loc_Tax.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Local_Wthld().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-LOCAL-WTHLD ( #I ) TO #CNTL.#LOC-TAX ( 2,1 )
                    pnd_Cntl_Pnd_Gross_Amt.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                   //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 2,1 )
                    pnd_Cntl_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_J,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                      //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT ( #J,1 )
                    pnd_Cntl_Pnd_Fed_Tax.getValue(pnd_Ws_Pnd_J,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                    //Natural: ADD #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #CNTL.#FED-TAX ( #J,1 )
                    pnd_Cntl_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_J,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I));                      //Natural: ADD #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) TO #CNTL.#STA-TAX ( #J,1 )
                    pnd_Cntl_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_J,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Local_Wthld().getValue(pnd_Ws_Pnd_I));                      //Natural: ADD #XTAXYR-F94.TWRPYMNT-LOCAL-WTHLD ( #I ) TO #CNTL.#LOC-TAX ( #J,1 )
                    pnd_Cntl_Pnd_Gross_Amt.getValue(5,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                   //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 5,1 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            readWork01Twrpymnt_Company_Cde_FormOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form());                                                   //Natural: END-WORK
            readWork01Twrpymnt_Tax_YearOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Company_Line.setValue("Grand Totals");                                                                                                                 //Natural: ASSIGN #WS.#COMPANY-LINE := 'Grand Totals'
        pnd_Ws_Pnd_S2.setValue(2);                                                                                                                                        //Natural: ASSIGN #WS.#S2 := 2
                                                                                                                                                                          //Natural: PERFORM WRITE-W2-REPORT
        sub_Write_W2_Report();
        if (condition(Global.isEscape())) {return;}
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-W2-REPORT
        //* **************************************
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COMPANY
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CONTROL-RECORD
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Write_W2_Report() throws Exception                                                                                                                   //Natural: WRITE-W2-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Cntl_Pnd_Gross_Amt.getValue(6,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Gross_Amt.getValue(6,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Gross_Amt.getValue(3,":",5,pnd_Ws_Pnd_S2).add(new  //Natural: ASSIGN #CNTL.#GROSS-AMT ( 6,#S2 ) := #CNTL.#GROSS-AMT ( 3:5,#S2 ) + 0.00
            DbsDecimal("0.00")));
        pnd_Cntl_Pnd_Fed_Tax.getValue(6,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Fed_Tax.getValue(6,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Fed_Tax.getValue(3,":",5,pnd_Ws_Pnd_S2).add(new  //Natural: ASSIGN #CNTL.#FED-TAX ( 6,#S2 ) := #CNTL.#FED-TAX ( 3:5,#S2 ) + 0.00
            DbsDecimal("0.00")));
        pnd_Cntl_Pnd_Sta_Tax.getValue(6,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Sta_Tax.getValue(6,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Sta_Tax.getValue(3,":",5,pnd_Ws_Pnd_S2).add(new  //Natural: ASSIGN #CNTL.#STA-TAX ( 6,#S2 ) := #CNTL.#STA-TAX ( 3:5,#S2 ) + 0.00
            DbsDecimal("0.00")));
        pnd_Cntl_Pnd_Loc_Tax.getValue(6,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Loc_Tax.getValue(6,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Loc_Tax.getValue(3,":",5,pnd_Ws_Pnd_S2).add(new  //Natural: ASSIGN #CNTL.#LOC-TAX ( 6,#S2 ) := #CNTL.#LOC-TAX ( 3:5,#S2 ) + 0.00
            DbsDecimal("0.00")));
        FOR02:                                                                                                                                                            //Natural: FOR #WS.#S1 = 1 TO #CNTL-MAX
        for (pnd_Ws_Pnd_S1.setValue(1); condition(pnd_Ws_Pnd_S1.lessOrEqual(pnd_Ws_Const_Pnd_Cntl_Max)); pnd_Ws_Pnd_S1.nadd(1))
        {
            pnd_Ws_Pnd_Tran_Cv.setValue(pnd_Cntl_Pnd_Tran_Cnt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                //Natural: ASSIGN #WS.#TRAN-CV := #CNTL.#TRAN-CNT-CV ( #S1 )
            pnd_Ws_Pnd_Gross_Cv.setValue(pnd_Cntl_Pnd_Gross_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                              //Natural: ASSIGN #WS.#GROSS-CV := #CNTL.#GROSS-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Fed_Cv.setValue(pnd_Cntl_Pnd_Fed_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#FED-CV := #CNTL.#FED-TAX-CV ( #S1 )
            pnd_Ws_Pnd_Sta_Cv.setValue(pnd_Cntl_Pnd_Sta_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#STA-CV := #CNTL.#STA-TAX-CV ( #S1 )
            pnd_Ws_Pnd_Loc_Cv.setValue(pnd_Cntl_Pnd_Loc_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#LOC-CV := #CNTL.#LOC-TAX-CV ( #S1 )
            getReports().display(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",                         //Natural: DISPLAY ( 1 ) ( HC = R ES = ON ) '/' #CNTL-TEXT ( #S1 ) '//Trans Count' #CNTL.#TRAN-CNT ( #S1,#S2 ) ( CV = #TRAN-CV ) '//Gross Amount' #CNTL.#GROSS-AMT ( #S1,#S2 ) ( CV = #GROSS-CV ) 3X '//Federal Tax' #CNTL.#FED-TAX ( #S1,#S2 ) ( CV = #FED-CV ) 3X '//State Tax' #CNTL.#STA-TAX ( #S1,#S2 ) ( CV = #STA-CV ) 3X '//Local Tax' #CNTL.#LOC-TAX ( #S1,#S2 ) ( CV = #LOC-CV )
            		pnd_Cntl_Pnd_Cntl_Text.getValue(pnd_Ws_Pnd_S1),"//Trans Count",
            		pnd_Cntl_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Tran_Cv,"//Gross Amount",
            		pnd_Cntl_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Gross_Cv,new ColumnSpacing(3),"//Federal Tax",
            		pnd_Cntl_Pnd_Fed_Tax.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Fed_Cv,new ColumnSpacing(3),"//State Tax",
            		pnd_Cntl_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Sta_Cv,new ColumnSpacing(3),"//Local Tax",
            		pnd_Cntl_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Loc_Cv);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1 LINES
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Process_Company() throws Exception                                                                                                                   //Natural: PROCESS-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        DbsUtil.callnat(Twrncomp.class , getCurrentProcessState(), pdaTwracomp.getPnd_Twracomp_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracomp.getPnd_Twracomp_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOMP' USING #TWRACOMP.#INPUT-PARMS ( AD = O ) #TWRACOMP.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Process_Control_Record() throws Exception                                                                                                            //Natural: PROCESS-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        getWorkFiles().read(3, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                             //Natural: READ WORK FILE 03 ONCE #TWRP0600-CONTROL-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"CONTROL RECORD IS EMPTY",new TabSetting(77),"***");                                      //Natural: WRITE ( 1 ) '***' 25T 'CONTROL RECORD IS EMPTY' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(101);  if (true) return;                                                                                                                    //Natural: TERMINATE 101
        }                                                                                                                                                                 //Natural: END-ENDFILE
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaTwrl0900_getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_FormIsBreak = ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form().isBreak(endOfData);
        if (condition(ldaTwrl0900_getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_FormIsBreak))
        {
            pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(readWork01Twrpymnt_Company_Cde_FormOld);                                                                 //Natural: ASSIGN #TWRACOMP.#COMP-CODE := OLD ( #XTAXYR-F94.TWRPYMNT-COMPANY-CDE-FORM )
            pdaTwracomp.getPnd_Twracomp_Pnd_Tax_Year().setValue(readWork01Twrpymnt_Tax_YearOld);                                                                          //Natural: ASSIGN #TWRACOMP.#TAX-YEAR := OLD ( #XTAXYR-F94.TWRPYMNT-TAX-YEAR )
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMPANY
            sub_Process_Company();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Pnd_Company_Line.setValue("Company:");                                                                                                                 //Natural: ASSIGN #WS.#COMPANY-LINE := 'Company:'
            setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),pnd_Ws_Pnd_Company_Line,11,4);                                                          //Natural: MOVE #TWRACOMP.#COMP-SHORT-NAME TO SUBSTR ( #WS.#COMPANY-LINE,11,4 )
            setValueToSubstring("-",pnd_Ws_Pnd_Company_Line,16,1);                                                                                                        //Natural: MOVE '-' TO SUBSTR ( #WS.#COMPANY-LINE,16,1 )
            setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Name(),pnd_Ws_Pnd_Company_Line,18,55);                                                               //Natural: MOVE #TWRACOMP.#COMP-NAME TO SUBSTR ( #WS.#COMPANY-LINE,18,55 )
            pnd_Ws_Pnd_S2.setValue(1);                                                                                                                                    //Natural: ASSIGN #WS.#S2 := 1
                                                                                                                                                                          //Natural: PERFORM WRITE-W2-REPORT
            sub_Write_W2_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Cntl_Pnd_Tran_Cnt.getValue("*",2).nadd(pnd_Cntl_Pnd_Tran_Cnt.getValue("*",1));                                                                            //Natural: ADD #CNTL.#TRAN-CNT ( *,1 ) TO #CNTL.#TRAN-CNT ( *,2 )
            pnd_Cntl_Pnd_Gross_Amt.getValue("*",2).nadd(pnd_Cntl_Pnd_Gross_Amt.getValue("*",1));                                                                          //Natural: ADD #CNTL.#GROSS-AMT ( *,1 ) TO #CNTL.#GROSS-AMT ( *,2 )
            pnd_Cntl_Pnd_Fed_Tax.getValue("*",2).nadd(pnd_Cntl_Pnd_Fed_Tax.getValue("*",1));                                                                              //Natural: ADD #CNTL.#FED-TAX ( *,1 ) TO #CNTL.#FED-TAX ( *,2 )
            pnd_Cntl_Pnd_Sta_Tax.getValue("*",2).nadd(pnd_Cntl_Pnd_Sta_Tax.getValue("*",1));                                                                              //Natural: ADD #CNTL.#STA-TAX ( *,1 ) TO #CNTL.#STA-TAX ( *,2 )
            pnd_Cntl_Pnd_Loc_Tax.getValue("*",2).nadd(pnd_Cntl_Pnd_Loc_Tax.getValue("*",1));                                                                              //Natural: ADD #CNTL.#LOC-TAX ( *,1 ) TO #CNTL.#LOC-TAX ( *,2 )
            pnd_Cntl_Pnd_Totals.getValue("*",1).reset();                                                                                                                  //Natural: RESET #CNTL.#TOTALS ( *,1 )
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=133 ZP=ON");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(56),"Summary of W2 Transactions",new TabSetting(120),"Report: RPT1",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, new 
            ReportEditMask ("9999"), new SignPosition (false),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",
            
        		pnd_Cntl_Pnd_Cntl_Text,"//Trans Count",
        		pnd_Cntl_Pnd_Tran_Cnt, pnd_Ws_Pnd_Tran_Cv,"//Gross Amount",
        		pnd_Cntl_Pnd_Gross_Amt, pnd_Ws_Pnd_Gross_Cv,new ColumnSpacing(3),"//Federal Tax",
        		pnd_Cntl_Pnd_Fed_Tax, pnd_Ws_Pnd_Fed_Cv,new ColumnSpacing(3),"//State Tax",
        		pnd_Cntl_Pnd_Sta_Tax, pnd_Ws_Pnd_Sta_Cv,new ColumnSpacing(3),"//Local Tax",
        		pnd_Cntl_Pnd_Loc_Tax, pnd_Ws_Pnd_Loc_Cv);
    }
    private void CheckAtStartofData204() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Ws_Pnd_Tax_Year.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());                                                                              //Natural: ASSIGN #WS.#TAX-YEAR := #XTAXYR-F94.TWRPYMNT-TAX-YEAR
            //*  BK
        }                                                                                                                                                                 //Natural: END-START
    }
}
