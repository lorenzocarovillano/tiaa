/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:40:00 PM
**        * FROM NATURAL PROGRAM : Twrp4620
************************************************************
**        * FILE NAME            : Twrp4620.java
**        * CLASS NAME           : Twrp4620
**        * INSTANCE NAME        : Twrp4620
************************************************************
************************************************************************
* PROGRAM  : TWRP4620
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : EDITS AND VALIDATION PROGRAM.
* CREATED  : 07 / 24 / 1999.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM READS IRA CONTRIBUTION DATA, AND PRODUCES CONTROL
*            TOTAL REPORTS FOR EACH ONE OF THE 'IRA' FORM TYPES
*           (I.E. 10, 20, AND 30).
* MODIFIED : 08/23/05 J.ROTHOLZ - MODIFIED CODE TO HANDLE SEP
*            IRA DATA IN FEED FROM OMNIPLUS (TOPS 6.5)
*
* 04/27/2017  WEBBJ - PIN EXPANSION SCAN JW0427 ..STOW ONLY
* 11/24/2020 - PALDE - RESTOW FOR IRS REPORTING 2020 - 5498 CHANGES
*
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp4620 extends BLNatBase
{
    // Data Areas
    private LdaTwrl462a ldaTwrl462a;
    private LdaTwrl462b ldaTwrl462b;
    private LdaTwrl462c ldaTwrl462c;
    private LdaTwrl462d ldaTwrl462d;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Pin_Base_Key;

    private DbsGroup pnd_Pin_Base_Key__R_Field_1;
    private DbsField pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr;

    private DbsGroup pnd_Pin_Base_Key__R_Field_2;
    private DbsField pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr_N;
    private DbsField pnd_Pin_Base_Key_Pnd_Ph_Bse_Addrss_Ind;
    private DbsField pnd_Twrc_S2_Forms;

    private DbsGroup pnd_Twrc_S2_Forms__R_Field_3;
    private DbsField pnd_Twrc_S2_Forms_Pnd_S2_Tax_Year;
    private DbsField pnd_Twrc_S2_Forms_Pnd_S2_Status;
    private DbsField pnd_Twrc_S2_Forms_Pnd_S2_Tax_Id;
    private DbsField pnd_Twrc_S2_Forms_Pnd_S2_Contract;
    private DbsField pnd_Twrc_S2_Forms_Pnd_S2_Payee;
    private DbsField pnd_Isn;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Accept_Ctr;
    private DbsField pnd_Reject_Ctr;
    private DbsField pnd_Bypass_Ctr;
    private DbsField pnd_Dob_Ctr;
    private DbsField pnd_No_Of_Errors;
    private DbsField pnd_No_Of_Warnings;
    private DbsField pnd_Error_Msg;
    private DbsField pnd_Warning_Msg;
    private DbsField pnd_Hdr_Tax_Year;
    private DbsField pnd_Source_Total_Header;
    private DbsField pnd_Prev_Form_Type;
    private DbsField pnd_Header_Date;
    private DbsField pnd_Interface_Date;
    private DbsField pnd_Hdr_Simulation_Date;
    private DbsField pnd_T_Trans_Count;
    private DbsField pnd_T_Classic_Amt;
    private DbsField pnd_T_Roth_Amt;
    private DbsField pnd_T_Rollover_Amt;
    private DbsField pnd_T_Rechar_Amt;
    private DbsField pnd_T_Sep_Amt;
    private DbsField pnd_T_Fmv_Amt;
    private DbsField pnd_T_Roth_Conv_Amt;
    private DbsField pnd_C_Trans_Count;
    private DbsField pnd_C_Classic_Amt;
    private DbsField pnd_C_Roth_Amt;
    private DbsField pnd_C_Rollover_Amt;
    private DbsField pnd_C_Rechar_Amt;
    private DbsField pnd_C_Sep_Amt;
    private DbsField pnd_C_Fmv_Amt;
    private DbsField pnd_C_Roth_Conv_Amt;
    private DbsField pnd_L_Trans_Count;
    private DbsField pnd_L_Classic_Amt;
    private DbsField pnd_L_Roth_Amt;
    private DbsField pnd_L_Rollover_Amt;
    private DbsField pnd_L_Rechar_Amt;
    private DbsField pnd_L_Sep_Amt;
    private DbsField pnd_L_Fmv_Amt;
    private DbsField pnd_L_Roth_Conv_Amt;
    private DbsField pnd_S_Trans_Count;
    private DbsField pnd_S_Classic_Amt;
    private DbsField pnd_S_Roth_Amt;
    private DbsField pnd_S_Rollover_Amt;
    private DbsField pnd_S_Rechar_Amt;
    private DbsField pnd_S_Sep_Amt;
    private DbsField pnd_S_Fmv_Amt;
    private DbsField pnd_S_Roth_Conv_Amt;
    private DbsField pnd_Rejected;
    private DbsField pnd_Dup_Found;
    private DbsField pnd_Naa_Found;
    private DbsField i1;
    private DbsField i2;
    private DbsField i3;
    private DbsField i4;
    private DbsField i;
    private DbsField j;
    private DbsField k;
    private DbsField l;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl462a = new LdaTwrl462a();
        registerRecord(ldaTwrl462a);
        ldaTwrl462b = new LdaTwrl462b();
        registerRecord(ldaTwrl462b);
        ldaTwrl462c = new LdaTwrl462c();
        registerRecord(ldaTwrl462c);
        registerRecord(ldaTwrl462c.getVw_ira());
        ldaTwrl462d = new LdaTwrl462d();
        registerRecord(ldaTwrl462d);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Pin_Base_Key = localVariables.newFieldInRecord("pnd_Pin_Base_Key", "#PIN-BASE-KEY", FieldType.STRING, 13);

        pnd_Pin_Base_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Pin_Base_Key__R_Field_1", "REDEFINE", pnd_Pin_Base_Key);
        pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr = pnd_Pin_Base_Key__R_Field_1.newFieldInGroup("pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr", "#PH-UNQUE-ID-NMBR", 
            FieldType.STRING, 12);

        pnd_Pin_Base_Key__R_Field_2 = pnd_Pin_Base_Key__R_Field_1.newGroupInGroup("pnd_Pin_Base_Key__R_Field_2", "REDEFINE", pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr);
        pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr_N = pnd_Pin_Base_Key__R_Field_2.newFieldInGroup("pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr_N", "#PH-UNQUE-ID-NMBR-N", 
            FieldType.NUMERIC, 12);
        pnd_Pin_Base_Key_Pnd_Ph_Bse_Addrss_Ind = pnd_Pin_Base_Key__R_Field_1.newFieldInGroup("pnd_Pin_Base_Key_Pnd_Ph_Bse_Addrss_Ind", "#PH-BSE-ADDRSS-IND", 
            FieldType.STRING, 1);
        pnd_Twrc_S2_Forms = localVariables.newFieldInRecord("pnd_Twrc_S2_Forms", "#TWRC-S2-FORMS", FieldType.STRING, 25);

        pnd_Twrc_S2_Forms__R_Field_3 = localVariables.newGroupInRecord("pnd_Twrc_S2_Forms__R_Field_3", "REDEFINE", pnd_Twrc_S2_Forms);
        pnd_Twrc_S2_Forms_Pnd_S2_Tax_Year = pnd_Twrc_S2_Forms__R_Field_3.newFieldInGroup("pnd_Twrc_S2_Forms_Pnd_S2_Tax_Year", "#S2-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Twrc_S2_Forms_Pnd_S2_Status = pnd_Twrc_S2_Forms__R_Field_3.newFieldInGroup("pnd_Twrc_S2_Forms_Pnd_S2_Status", "#S2-STATUS", FieldType.STRING, 
            1);
        pnd_Twrc_S2_Forms_Pnd_S2_Tax_Id = pnd_Twrc_S2_Forms__R_Field_3.newFieldInGroup("pnd_Twrc_S2_Forms_Pnd_S2_Tax_Id", "#S2-TAX-ID", FieldType.STRING, 
            10);
        pnd_Twrc_S2_Forms_Pnd_S2_Contract = pnd_Twrc_S2_Forms__R_Field_3.newFieldInGroup("pnd_Twrc_S2_Forms_Pnd_S2_Contract", "#S2-CONTRACT", FieldType.STRING, 
            8);
        pnd_Twrc_S2_Forms_Pnd_S2_Payee = pnd_Twrc_S2_Forms__R_Field_3.newFieldInGroup("pnd_Twrc_S2_Forms_Pnd_S2_Payee", "#S2-PAYEE", FieldType.STRING, 
            2);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Accept_Ctr = localVariables.newFieldInRecord("pnd_Accept_Ctr", "#ACCEPT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Reject_Ctr = localVariables.newFieldInRecord("pnd_Reject_Ctr", "#REJECT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Bypass_Ctr = localVariables.newFieldInRecord("pnd_Bypass_Ctr", "#BYPASS-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Dob_Ctr = localVariables.newFieldInRecord("pnd_Dob_Ctr", "#DOB-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_No_Of_Errors = localVariables.newFieldInRecord("pnd_No_Of_Errors", "#NO-OF-ERRORS", FieldType.PACKED_DECIMAL, 7);
        pnd_No_Of_Warnings = localVariables.newFieldInRecord("pnd_No_Of_Warnings", "#NO-OF-WARNINGS", FieldType.PACKED_DECIMAL, 7);
        pnd_Error_Msg = localVariables.newFieldArrayInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 80, new DbsArrayController(1, 50));
        pnd_Warning_Msg = localVariables.newFieldArrayInRecord("pnd_Warning_Msg", "#WARNING-MSG", FieldType.STRING, 80, new DbsArrayController(1, 50));
        pnd_Hdr_Tax_Year = localVariables.newFieldInRecord("pnd_Hdr_Tax_Year", "#HDR-TAX-YEAR", FieldType.STRING, 4);
        pnd_Source_Total_Header = localVariables.newFieldInRecord("pnd_Source_Total_Header", "#SOURCE-TOTAL-HEADER", FieldType.STRING, 17);
        pnd_Prev_Form_Type = localVariables.newFieldInRecord("pnd_Prev_Form_Type", "#PREV-FORM-TYPE", FieldType.STRING, 2);
        pnd_Header_Date = localVariables.newFieldInRecord("pnd_Header_Date", "#HEADER-DATE", FieldType.DATE);
        pnd_Interface_Date = localVariables.newFieldInRecord("pnd_Interface_Date", "#INTERFACE-DATE", FieldType.DATE);
        pnd_Hdr_Simulation_Date = localVariables.newFieldInRecord("pnd_Hdr_Simulation_Date", "#HDR-SIMULATION-DATE", FieldType.DATE);
        pnd_T_Trans_Count = localVariables.newFieldArrayInRecord("pnd_T_Trans_Count", "#T-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            8));
        pnd_T_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_T_Classic_Amt", "#T-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_T_Roth_Amt", "#T-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_T_Rollover_Amt", "#T-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_T_Rechar_Amt", "#T-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_T_Sep_Amt", "#T-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_T_Fmv_Amt", "#T-FMV-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            8));
        pnd_T_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_T_Roth_Conv_Amt", "#T-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Trans_Count = localVariables.newFieldArrayInRecord("pnd_C_Trans_Count", "#C-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            8));
        pnd_C_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_C_Classic_Amt", "#C-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_C_Roth_Amt", "#C-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_C_Rollover_Amt", "#C-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_C_Rechar_Amt", "#C-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_C_Sep_Amt", "#C-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_C_Fmv_Amt", "#C-FMV-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            8));
        pnd_C_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_C_Roth_Conv_Amt", "#C-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Trans_Count = localVariables.newFieldArrayInRecord("pnd_L_Trans_Count", "#L-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            8));
        pnd_L_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_L_Classic_Amt", "#L-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_L_Roth_Amt", "#L-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_L_Rollover_Amt", "#L-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_L_Rechar_Amt", "#L-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_L_Sep_Amt", "#L-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_L_Fmv_Amt", "#L-FMV-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            8));
        pnd_L_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_L_Roth_Conv_Amt", "#L-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_S_Trans_Count = localVariables.newFieldArrayInRecord("pnd_S_Trans_Count", "#S-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            8));
        pnd_S_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_S_Classic_Amt", "#S-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_S_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_S_Roth_Amt", "#S-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_S_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_S_Rollover_Amt", "#S-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_S_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_S_Rechar_Amt", "#S-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_S_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_S_Sep_Amt", "#S-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_S_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_S_Fmv_Amt", "#S-FMV-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            8));
        pnd_S_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_S_Roth_Conv_Amt", "#S-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_Rejected = localVariables.newFieldInRecord("pnd_Rejected", "#REJECTED", FieldType.BOOLEAN, 1);
        pnd_Dup_Found = localVariables.newFieldInRecord("pnd_Dup_Found", "#DUP-FOUND", FieldType.BOOLEAN, 1);
        pnd_Naa_Found = localVariables.newFieldInRecord("pnd_Naa_Found", "#NAA-FOUND", FieldType.BOOLEAN, 1);
        i1 = localVariables.newFieldInRecord("i1", "I1", FieldType.PACKED_DECIMAL, 4);
        i2 = localVariables.newFieldInRecord("i2", "I2", FieldType.PACKED_DECIMAL, 4);
        i3 = localVariables.newFieldInRecord("i3", "I3", FieldType.PACKED_DECIMAL, 4);
        i4 = localVariables.newFieldInRecord("i4", "I4", FieldType.PACKED_DECIMAL, 4);
        i = localVariables.newFieldInRecord("i", "I", FieldType.PACKED_DECIMAL, 4);
        j = localVariables.newFieldInRecord("j", "J", FieldType.PACKED_DECIMAL, 4);
        k = localVariables.newFieldInRecord("k", "K", FieldType.PACKED_DECIMAL, 4);
        l = localVariables.newFieldInRecord("l", "L", FieldType.PACKED_DECIMAL, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl462a.initializeValues();
        ldaTwrl462b.initializeValues();
        ldaTwrl462c.initializeValues();
        ldaTwrl462d.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp4620() throws Exception
    {
        super("Twrp4620");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP4620", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133;//Natural: FORMAT ( 02 ) PS = 60 LS = 133;//Natural: FORMAT ( 03 ) PS = 60 LS = 133;//Natural: FORMAT ( 04 ) PS = 60 LS = 133;//Natural: FORMAT ( 05 ) PS = 60 LS = 133;//Natural: FORMAT ( 06 ) PS = 60 LS = 133;//Natural: FORMAT ( 07 ) PS = 60 LS = 133;//Natural: FORMAT ( 08 ) PS = 60 LS = 133
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        pnd_Interface_Date.setValue(Global.getDATX());                                                                                                                    //Natural: ASSIGN #INTERFACE-DATE := *DATX
        RD1:                                                                                                                                                              //Natural: READ WORK FILE 01 RECORD IRA-RECORD
        while (condition(getWorkFiles().read(1, ldaTwrl462a.getIra_Record())))
        {
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            if (condition(pnd_Read_Ctr.equals(1)))                                                                                                                        //Natural: IF #READ-CTR = 1
            {
                pnd_Hdr_Tax_Year.setValue(ldaTwrl462a.getIra_Record_Ira_Tax_Year());                                                                                      //Natural: ASSIGN #HDR-TAX-YEAR := IRA-TAX-YEAR
                pnd_Prev_Form_Type.setValue(ldaTwrl462a.getIra_Record_Ira_Form_Type());                                                                                   //Natural: ASSIGN #PREV-FORM-TYPE := IRA-FORM-TYPE
                pnd_Header_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaTwrl462a.getIra_Record_Ira_Simulation_Date());                                           //Natural: MOVE EDITED IRA-SIMULATION-DATE TO #HEADER-DATE ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Prev_Form_Type.equals(ldaTwrl462a.getIra_Record_Ira_Form_Type())))                                                                          //Natural: IF #PREV-FORM-TYPE = IRA-FORM-TYPE
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Prev_Form_Type.equals("10")))                                                                                                           //Natural: IF #PREV-FORM-TYPE = '10'
                {
                    pnd_Source_Total_Header.setValue("     Classic     ");                                                                                                //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '     Classic     '
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Prev_Form_Type.equals("20")))                                                                                                       //Natural: IF #PREV-FORM-TYPE = '20'
                    {
                        pnd_Source_Total_Header.setValue("      Roth       ");                                                                                            //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '      Roth       '
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Prev_Form_Type.equals("30")))                                                                                                   //Natural: IF #PREV-FORM-TYPE = '30'
                        {
                            pnd_Source_Total_Header.setValue("    Rollovers    ");                                                                                        //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '    Rollovers    '
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Source_Total_Header.setValue("      SEP        ");                                                                                        //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '      SEP        '
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                i1.setValue(1);                                                                                                                                           //Natural: ASSIGN I1 := 1
                i2.setValue(2);                                                                                                                                           //Natural: ASSIGN I2 := 2
                i3.setValue(3);                                                                                                                                           //Natural: ASSIGN I3 := 3
                i4.setValue(4);                                                                                                                                           //Natural: ASSIGN I4 := 4
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
                sub_Display_Control_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM UPDATE-GENERAL-TOTALS
                sub_Update_General_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM RESET-CONTROL-TOTALS
                sub_Reset_Control_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Prev_Form_Type.setValue(ldaTwrl462a.getIra_Record_Ira_Form_Type());                                                                                   //Natural: ASSIGN #PREV-FORM-TYPE := IRA-FORM-TYPE
            }                                                                                                                                                             //Natural: END-IF
            j.setValue(1);                                                                                                                                                //Natural: ASSIGN J := 1
            if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("T")))                                                                                      //Natural: IF IRA-COMPANY-CODE = 'T'
            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-T-CONTROL-TOTALS
                sub_Update_T_Control_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("C")))                                                                                  //Natural: IF IRA-COMPANY-CODE = 'C'
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-C-CONTROL-TOTALS
                    sub_Update_C_Control_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("L")))                                                                              //Natural: IF IRA-COMPANY-CODE = 'L'
                    {
                                                                                                                                                                          //Natural: PERFORM UPDATE-L-CONTROL-TOTALS
                        sub_Update_L_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  09-24-03 FRANK
                        if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("S")))                                                                          //Natural: IF IRA-COMPANY-CODE = 'S'
                        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-S-CONTROL-TOTALS
                            sub_Update_S_Control_Totals();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Dup_Found.setValue(false);                                                                                                                                //Natural: ASSIGN #DUP-FOUND := FALSE
            pnd_Twrc_S2_Forms_Pnd_S2_Tax_Year.setValue(ldaTwrl462a.getIra_Record_Ira_Tax_Year());                                                                         //Natural: ASSIGN #S2-TAX-YEAR := IRA-TAX-YEAR
            pnd_Twrc_S2_Forms_Pnd_S2_Status.setValue(" ");                                                                                                                //Natural: ASSIGN #S2-STATUS := ' '
            pnd_Twrc_S2_Forms_Pnd_S2_Tax_Id.setValue(ldaTwrl462a.getIra_Record_Ira_Tax_Id());                                                                             //Natural: ASSIGN #S2-TAX-ID := IRA-TAX-ID
            pnd_Twrc_S2_Forms_Pnd_S2_Contract.setValue(ldaTwrl462a.getIra_Record_Ira_Contract());                                                                         //Natural: ASSIGN #S2-CONTRACT := IRA-CONTRACT
            pnd_Twrc_S2_Forms_Pnd_S2_Payee.setValue(ldaTwrl462a.getIra_Record_Ira_Payee());                                                                               //Natural: ASSIGN #S2-PAYEE := IRA-PAYEE
            ldaTwrl462c.getVw_ira().startDatabaseRead                                                                                                                     //Natural: READ IRA WITH TWRC-S2-FORMS = #TWRC-S2-FORMS
            (
            "READ01",
            new Wc[] { new Wc("TWRC_S2_FORMS", ">=", pnd_Twrc_S2_Forms, WcType.BY) },
            new Oc[] { new Oc("TWRC_S2_FORMS", "ASC") }
            );
            READ01:
            while (condition(ldaTwrl462c.getVw_ira().readNextRow("READ01")))
            {
                if (condition(ldaTwrl462c.getIra_Twrc_Tax_Year().equals(ldaTwrl462a.getIra_Record_Ira_Tax_Year()) && ldaTwrl462c.getIra_Twrc_Status().equals(" ")         //Natural: IF TWRC-TAX-YEAR = IRA-TAX-YEAR AND TWRC-STATUS = ' ' AND TWRC-TAX-ID = IRA-TAX-ID AND TWRC-CONTRACT = IRA-CONTRACT AND TWRC-PAYEE = IRA-PAYEE
                    && ldaTwrl462c.getIra_Twrc_Tax_Id().equals(ldaTwrl462a.getIra_Record_Ira_Tax_Id()) && ldaTwrl462c.getIra_Twrc_Contract().equals(ldaTwrl462a.getIra_Record_Ira_Contract()) 
                    && ldaTwrl462c.getIra_Twrc_Payee().equals(ldaTwrl462a.getIra_Record_Ira_Payee())))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl462c.getIra_Twrc_Company_Cde().equals(ldaTwrl462a.getIra_Record_Ira_Company_Code())))                                                //Natural: IF TWRC-COMPANY-CDE = IRA-COMPANY-CODE
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl462c.getIra_Twrc_Classic_Amt().equals(ldaTwrl462a.getIra_Record_Ira_Classic_Amt()) && ldaTwrl462c.getIra_Twrc_Sep_Amt().equals(ldaTwrl462a.getIra_Record_Ira_Sep_Amt())  //Natural: IF TWRC-CLASSIC-AMT = IRA-CLASSIC-AMT AND TWRC-SEP-AMT = IRA-SEP-AMT AND TWRC-ROTH-AMT = IRA-ROTH-AMT AND TWRC-ROLLOVER-AMT = IRA-ROLLOVER-AMT AND TWRC-RECHAR-AMT = IRA-RECHARACTER-AMT AND TWRC-FAIR-MKT-VAL-AMT = IRA-FMV-AMT AND TWRC-ROTH-CONVERSION-AMT = IRA-ROTH-CONV-AMT
                    && ldaTwrl462c.getIra_Twrc_Roth_Amt().equals(ldaTwrl462a.getIra_Record_Ira_Roth_Amt()) && ldaTwrl462c.getIra_Twrc_Rollover_Amt().equals(ldaTwrl462a.getIra_Record_Ira_Rollover_Amt()) 
                    && ldaTwrl462c.getIra_Twrc_Rechar_Amt().equals(ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt()) && ldaTwrl462c.getIra_Twrc_Fair_Mkt_Val_Amt().equals(ldaTwrl462a.getIra_Record_Ira_Fmv_Amt()) 
                    && ldaTwrl462c.getIra_Twrc_Roth_Conversion_Amt().equals(ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt())))
                {
                                                                                                                                                                          //Natural: PERFORM EDITS
                    sub_Edits();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Rejected.equals(false)))                                                                                                            //Natural: IF #REJECTED = FALSE
                    {
                        j.setValue(2);                                                                                                                                    //Natural: ASSIGN J := 2
                        if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("T")))                                                                          //Natural: IF IRA-COMPANY-CODE = 'T'
                        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-T-CONTROL-TOTALS
                            sub_Update_T_Control_Totals();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("C")))                                                                      //Natural: IF IRA-COMPANY-CODE = 'C'
                            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-C-CONTROL-TOTALS
                                sub_Update_C_Control_Totals();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("L")))                                                                  //Natural: IF IRA-COMPANY-CODE = 'L'
                                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-L-CONTROL-TOTALS
                                    sub_Update_L_Control_Totals();
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    //*  09-24-03 FRANK
                                    if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("S")))                                                              //Natural: IF IRA-COMPANY-CODE = 'S'
                                    {
                                                                                                                                                                          //Natural: PERFORM UPDATE-S-CONTROL-TOTALS
                                        sub_Update_S_Control_Totals();
                                        if (condition(Global.isEscape()))
                                        {
                                            if (condition(Global.isEscapeBottom())) break;
                                            else if (condition(Global.isEscapeBottomImmediate())) break;
                                            else if (condition(Global.isEscapeTop())) continue;
                                            else if (condition(Global.isEscapeRoutine())) return;
                                            else break;
                                        }
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Bypass_Ctr.nadd(1);                                                                                                                           //Natural: ADD 1 TO #BYPASS-CTR
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        j.setValue(3);                                                                                                                                    //Natural: ASSIGN J := 3
                        if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("T")))                                                                          //Natural: IF IRA-COMPANY-CODE = 'T'
                        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-T-CONTROL-TOTALS
                            sub_Update_T_Control_Totals();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("C")))                                                                      //Natural: IF IRA-COMPANY-CODE = 'C'
                            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-C-CONTROL-TOTALS
                                sub_Update_C_Control_Totals();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("L")))                                                                  //Natural: IF IRA-COMPANY-CODE = 'L'
                                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-L-CONTROL-TOTALS
                                    sub_Update_L_Control_Totals();
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    //*  09-24-03 FRANK
                                    if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("S")))                                                              //Natural: IF IRA-COMPANY-CODE = 'S'
                                    {
                                                                                                                                                                          //Natural: PERFORM UPDATE-S-CONTROL-TOTALS
                                        sub_Update_S_Control_Totals();
                                        if (condition(Global.isEscape()))
                                        {
                                            if (condition(Global.isEscapeBottom())) break;
                                            else if (condition(Global.isEscapeBottomImmediate())) break;
                                            else if (condition(Global.isEscapeTop())) continue;
                                            else if (condition(Global.isEscapeRoutine())) return;
                                            else break;
                                        }
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Reject_Ctr.nadd(1);                                                                                                                           //Natural: ADD 1 TO #REJECT-CTR
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Dup_Found.setValue(true);                                                                                                                         //Natural: ASSIGN #DUP-FOUND := TRUE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Dup_Found.equals(true)))                                                                                                                    //Natural: IF #DUP-FOUND = TRUE
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM EDITS
            sub_Edits();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Rejected.equals(false)))                                                                                                                    //Natural: IF #REJECTED = FALSE
            {
                j.setValue(4);                                                                                                                                            //Natural: ASSIGN J := 4
                if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("T")))                                                                                  //Natural: IF IRA-COMPANY-CODE = 'T'
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-T-CONTROL-TOTALS
                    sub_Update_T_Control_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("C")))                                                                              //Natural: IF IRA-COMPANY-CODE = 'C'
                    {
                                                                                                                                                                          //Natural: PERFORM UPDATE-C-CONTROL-TOTALS
                        sub_Update_C_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("L")))                                                                          //Natural: IF IRA-COMPANY-CODE = 'L'
                        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-L-CONTROL-TOTALS
                            sub_Update_L_Control_Totals();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  09-24-03 FRANK
                            if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("S")))                                                                      //Natural: IF IRA-COMPANY-CODE = 'S'
                            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-S-CONTROL-TOTALS
                                sub_Update_S_Control_Totals();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("RD1"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Accept_Ctr.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #ACCEPT-CTR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                j.setValue(3);                                                                                                                                            //Natural: ASSIGN J := 3
                if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("T")))                                                                                  //Natural: IF IRA-COMPANY-CODE = 'T'
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-T-CONTROL-TOTALS
                    sub_Update_T_Control_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("C")))                                                                              //Natural: IF IRA-COMPANY-CODE = 'C'
                    {
                                                                                                                                                                          //Natural: PERFORM UPDATE-C-CONTROL-TOTALS
                        sub_Update_C_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("L")))                                                                          //Natural: IF IRA-COMPANY-CODE = 'L'
                        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-L-CONTROL-TOTALS
                            sub_Update_L_Control_Totals();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  09-24-03 FRANK
                            if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("S")))                                                                      //Natural: IF IRA-COMPANY-CODE = 'S'
                            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-S-CONTROL-TOTALS
                                sub_Update_S_Control_Totals();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("RD1"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Reject_Ctr.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #REJECT-CTR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        RD1_Exit:
        if (Global.isEscape()) return;
        //* *------
        if (condition(pnd_Read_Ctr.equals(getZero())))                                                                                                                    //Natural: IF #READ-CTR = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"Tax Transaction File (Work File 01) Is Empty",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new  //Natural: WRITE ( 00 ) '***' 06T 'Tax Transaction File (Work File 01) Is Empty' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Source_Total_Header.setValue(DbsUtil.compress("      SEP        "));                                                                                          //Natural: COMPRESS '      SEP        ' INTO #SOURCE-TOTAL-HEADER
        i1.setValue(1);                                                                                                                                                   //Natural: ASSIGN I1 := 1
        i2.setValue(2);                                                                                                                                                   //Natural: ASSIGN I2 := 2
        i3.setValue(3);                                                                                                                                                   //Natural: ASSIGN I3 := 3
        i4.setValue(4);                                                                                                                                                   //Natural: ASSIGN I4 := 4
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
        sub_Display_Control_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-GENERAL-TOTALS
        sub_Update_General_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM RESET-CONTROL-TOTALS
        sub_Reset_Control_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //* * 09-24-03 ADDED SERVICES BY FRANK
        //* *------------
        //* *------------
        //* *------------
        //* * 09-24-03 ADDED SERVICES BY FRANK
        //* *------------
        //* *------------------------------------------
        //* *------------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 01 )
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
        //* *-------                                                                                                                                                      //Natural: ON ERROR
    }
    private void sub_Edits() throws Exception                                                                                                                             //Natural: EDITS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------
        i.reset();                                                                                                                                                        //Natural: RESET I L #NO-OF-ERRORS
        l.reset();
        pnd_No_Of_Errors.reset();
        pnd_Rejected.setValue(false);                                                                                                                                     //Natural: ASSIGN #REJECTED := FALSE
        //*  EDIT TAX YEAR.
        if (condition(DbsUtil.maskMatches(ldaTwrl462a.getIra_Record_Ira_Tax_Year(),"YYYY") && ldaTwrl462a.getIra_Record_Ira_Tax_Year().greaterOrEqual("1998")))           //Natural: IF IRA-TAX-YEAR = MASK ( YYYY ) AND IRA-TAX-YEAR >= '1998'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT COMPANY CODE.
        //* *IF IRA-COMPANY-CODE  =  'C'  OR =  'T'  OR = 'L'
        //*  09-24-03 FC
        if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("C") || ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("T") || ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("L")  //Natural: IF IRA-COMPANY-CODE = 'C' OR = 'T' OR = 'L' OR = 'S'
            || ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("S")))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT SIMULATION DATE.
        if (condition(DbsUtil.maskMatches(ldaTwrl462a.getIra_Record_Ira_Simulation_Date(),"YYYYMMDD")))                                                                   //Natural: IF IRA-SIMULATION-DATE = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT TAX ID.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Tax_Id().equals(" ")))                                                                                                //Natural: IF IRA-TAX-ID = ' '
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT TAX ID TYPE.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Tax_Id_Type().equals("1") || ldaTwrl462a.getIra_Record_Ira_Tax_Id_Type().equals("2") || ldaTwrl462a.getIra_Record_Ira_Tax_Id_Type().equals("3")  //Natural: IF IRA-TAX-ID-TYPE = '1' OR = '2' OR = '3' OR = '4' OR = '5'
            || ldaTwrl462a.getIra_Record_Ira_Tax_Id_Type().equals("4") || ldaTwrl462a.getIra_Record_Ira_Tax_Id_Type().equals("5")))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT CONTRACT NUMBER.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Contract().equals(" ")))                                                                                              //Natural: IF IRA-CONTRACT = ' '
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT CITIZENSHIP CODE.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Citizenship_Code().equals("01") || ldaTwrl462a.getIra_Record_Ira_Citizenship_Code().equals("02") ||                   //Natural: IF IRA-CITIZENSHIP-CODE = '01' OR = '02' OR = '03'
            ldaTwrl462a.getIra_Record_Ira_Citizenship_Code().equals("03")))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT RESIDENCY CODE.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Residency_Code().equals(" ")))                                                                                        //Natural: IF IRA-RESIDENCY-CODE = ' '
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT RESIDENCY TYPE.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Residency_Type().equals("1") || ldaTwrl462a.getIra_Record_Ira_Residency_Type().equals("2") || ldaTwrl462a.getIra_Record_Ira_Residency_Type().equals("3")  //Natural: IF IRA-RESIDENCY-TYPE = '1' OR = '2' OR = '3' OR = '4' OR = '5'
            || ldaTwrl462a.getIra_Record_Ira_Residency_Type().equals("4") || ldaTwrl462a.getIra_Record_Ira_Residency_Type().equals("5")))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT DATE OF BIRTH.
        if (condition(DbsUtil.maskMatches(ldaTwrl462a.getIra_Record_Ira_Dob(),"YYYYMMDD")))                                                                               //Natural: IF IRA-DOB = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT FORM TYPE.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Form_Type().equals("10") || ldaTwrl462a.getIra_Record_Ira_Form_Type().equals("20") || ldaTwrl462a.getIra_Record_Ira_Form_Type().equals("30")  //Natural: IF IRA-FORM-TYPE = '10' OR = '20' OR = '30' OR = '40' OR = '50'
            || ldaTwrl462a.getIra_Record_Ira_Form_Type().equals("40") || ldaTwrl462a.getIra_Record_Ira_Form_Type().equals("50")))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT ROLLOVER INDICATOR.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Rollover().equals(" ") || ldaTwrl462a.getIra_Record_Ira_Rollover().equals("Y")))                                      //Natural: IF IRA-ROLLOVER = ' ' OR = 'Y'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT RECHARACTERIZATION INDICATOR.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Recharacter().equals(" ") || ldaTwrl462a.getIra_Record_Ira_Recharacter().equals("Y")))                                //Natural: IF IRA-RECHARACTER = ' ' OR = 'Y'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT FORM PERIOD.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Form_Period().equals("P1 ") || ldaTwrl462a.getIra_Record_Ira_Form_Period().equals("PP1") || ldaTwrl462a.getIra_Record_Ira_Form_Period().equals("P2")  //Natural: IF IRA-FORM-PERIOD = 'P1 ' OR = 'PP1' OR = 'P2' OR = 'PP2'
            || ldaTwrl462a.getIra_Record_Ira_Form_Period().equals("PP2")))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT CLASSIC AMOUNT.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Classic_Amt().less(new DbsDecimal("0.00"))))                                                                          //Natural: IF IRA-CLASSIC-AMT < 0.00
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT ROTH AMOUNT.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Roth_Amt().less(new DbsDecimal("0.00"))))                                                                             //Natural: IF IRA-ROTH-AMT < 0.00
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT SEP AMOUNT
        if (condition(ldaTwrl462a.getIra_Record_Ira_Sep_Amt().less(new DbsDecimal("0.00"))))                                                                              //Natural: IF IRA-SEP-AMT < 0.00
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT ROLLOVER AMOUNT.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Rollover_Amt().less(new DbsDecimal("0.00"))))                                                                         //Natural: IF IRA-ROLLOVER-AMT < 0.00
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT RECHARACTERIZATION AMOUNT.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt().less(new DbsDecimal("0.00"))))                                                                      //Natural: IF IRA-RECHARACTER-AMT < 0.00
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT EDUCATION AMOUNT.
        //*  IF IRA-EDUCATIONAL-AMT  <  0.00
        //*    #REJECTED  :=  TRUE
        //*    ESCAPE ROUTINE
        //*  END-IF
        //*  EDIT ROTH CONVERSION AMOUNT.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt().less(new DbsDecimal("0.00"))))                                                                        //Natural: IF IRA-ROTH-CONV-AMT < 0.00
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT FAIR MARKET VALUE AMOUNT
        if (condition(ldaTwrl462a.getIra_Record_Ira_Fmv_Amt().less(new DbsDecimal("0.00"))))                                                                              //Natural: IF IRA-FMV-AMT < 0.00
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT DATE OF DEATH
        if (condition(ldaTwrl462a.getIra_Record_Ira_Dod().equals(" ") || DbsUtil.maskMatches(ldaTwrl462a.getIra_Record_Ira_Dod(),"YYYYMMDD") || DbsUtil.maskMatches(ldaTwrl462a.getIra_Record_Ira_Dod(), //Natural: IF IRA-DOD = ' ' OR IRA-DOD = MASK ( YYYYMMDD ) OR IRA-DOD = MASK ( YYYYMM00 )
            "YYYYMM00")))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_T_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-T-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pnd_T_Trans_Count.getValue(j).nadd(1);                                                                                                                            //Natural: ADD 1 TO #T-TRANS-COUNT ( J )
        pnd_T_Classic_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Classic_Amt());                                                                                  //Natural: ADD IRA-CLASSIC-AMT TO #T-CLASSIC-AMT ( J )
        pnd_T_Sep_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Sep_Amt());                                                                                          //Natural: ADD IRA-SEP-AMT TO #T-SEP-AMT ( J )
        pnd_T_Roth_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Amt());                                                                                        //Natural: ADD IRA-ROTH-AMT TO #T-ROTH-AMT ( J )
        pnd_T_Rollover_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Rollover_Amt());                                                                                //Natural: ADD IRA-ROLLOVER-AMT TO #T-ROLLOVER-AMT ( J )
        pnd_T_Rechar_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt());                                                                               //Natural: ADD IRA-RECHARACTER-AMT TO #T-RECHAR-AMT ( J )
        pnd_T_Fmv_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Fmv_Amt());                                                                                          //Natural: ADD IRA-FMV-AMT TO #T-FMV-AMT ( J )
        pnd_T_Roth_Conv_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt());                                                                              //Natural: ADD IRA-ROTH-CONV-AMT TO #T-ROTH-CONV-AMT ( J )
    }
    private void sub_Update_C_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-C-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pnd_C_Trans_Count.getValue(j).nadd(1);                                                                                                                            //Natural: ADD 1 TO #C-TRANS-COUNT ( J )
        pnd_C_Classic_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Classic_Amt());                                                                                  //Natural: ADD IRA-CLASSIC-AMT TO #C-CLASSIC-AMT ( J )
        pnd_C_Sep_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Sep_Amt());                                                                                          //Natural: ADD IRA-SEP-AMT TO #C-SEP-AMT ( J )
        pnd_C_Roth_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Amt());                                                                                        //Natural: ADD IRA-ROTH-AMT TO #C-ROTH-AMT ( J )
        pnd_C_Rollover_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Rollover_Amt());                                                                                //Natural: ADD IRA-ROLLOVER-AMT TO #C-ROLLOVER-AMT ( J )
        pnd_C_Rechar_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt());                                                                               //Natural: ADD IRA-RECHARACTER-AMT TO #C-RECHAR-AMT ( J )
        pnd_C_Fmv_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Fmv_Amt());                                                                                          //Natural: ADD IRA-FMV-AMT TO #C-FMV-AMT ( J )
        pnd_C_Roth_Conv_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt());                                                                              //Natural: ADD IRA-ROTH-CONV-AMT TO #C-ROTH-CONV-AMT ( J )
    }
    private void sub_Update_L_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-L-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pnd_L_Trans_Count.getValue(j).nadd(1);                                                                                                                            //Natural: ADD 1 TO #L-TRANS-COUNT ( J )
        pnd_L_Classic_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Classic_Amt());                                                                                  //Natural: ADD IRA-CLASSIC-AMT TO #L-CLASSIC-AMT ( J )
        pnd_L_Sep_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Sep_Amt());                                                                                          //Natural: ADD IRA-SEP-AMT TO #L-SEP-AMT ( J )
        pnd_L_Roth_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Amt());                                                                                        //Natural: ADD IRA-ROTH-AMT TO #L-ROTH-AMT ( J )
        pnd_L_Rollover_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Rollover_Amt());                                                                                //Natural: ADD IRA-ROLLOVER-AMT TO #L-ROLLOVER-AMT ( J )
        pnd_L_Rechar_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt());                                                                               //Natural: ADD IRA-RECHARACTER-AMT TO #L-RECHAR-AMT ( J )
        pnd_L_Fmv_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Fmv_Amt());                                                                                          //Natural: ADD IRA-FMV-AMT TO #L-FMV-AMT ( J )
        pnd_L_Roth_Conv_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt());                                                                              //Natural: ADD IRA-ROTH-CONV-AMT TO #L-ROTH-CONV-AMT ( J )
    }
    private void sub_Update_S_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-S-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pnd_S_Trans_Count.getValue(j).nadd(1);                                                                                                                            //Natural: ADD 1 TO #S-TRANS-COUNT ( J )
        pnd_S_Classic_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Classic_Amt());                                                                                  //Natural: ADD IRA-CLASSIC-AMT TO #S-CLASSIC-AMT ( J )
        pnd_S_Sep_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Sep_Amt());                                                                                          //Natural: ADD IRA-SEP-AMT TO #S-SEP-AMT ( J )
        pnd_S_Roth_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Amt());                                                                                        //Natural: ADD IRA-ROTH-AMT TO #S-ROTH-AMT ( J )
        pnd_S_Rollover_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Rollover_Amt());                                                                                //Natural: ADD IRA-ROLLOVER-AMT TO #S-ROLLOVER-AMT ( J )
        pnd_S_Rechar_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt());                                                                               //Natural: ADD IRA-RECHARACTER-AMT TO #S-RECHAR-AMT ( J )
        pnd_S_Fmv_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Fmv_Amt());                                                                                          //Natural: ADD IRA-FMV-AMT TO #S-FMV-AMT ( J )
        pnd_S_Roth_Conv_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt());                                                                              //Natural: ADD IRA-ROTH-CONV-AMT TO #S-ROTH-CONV-AMT ( J )
    }
    private void sub_Display_Control_Totals() throws Exception                                                                                                            //Natural: DISPLAY-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------------
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"       (TIAA)       ",NEWLINE);                                                                     //Natural: WRITE ( 01 ) NOTITLE 01T '       (TIAA)       ' /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_T_Trans_Count.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZ9"),new         //Natural: WRITE ( 01 ) NOTITLE 'Transaction Count...' 30T #T-TRANS-COUNT ( I1 ) 54T #T-TRANS-COUNT ( I2 ) 75T #T-TRANS-COUNT ( I3 ) 96T #T-TRANS-COUNT ( I4 )
            TabSetting(54),pnd_T_Trans_Count.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(75),pnd_T_Trans_Count.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZ9"),new TabSetting(96),pnd_T_Trans_Count.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_T_Classic_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 01 ) NOTITLE 'Classic Contrib.....' 26T #T-CLASSIC-AMT ( I1 ) 47T #T-CLASSIC-AMT ( I2 ) 68T #T-CLASSIC-AMT ( I3 ) 89T #T-CLASSIC-AMT ( I4 )
            TabSetting(47),pnd_T_Classic_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_T_Classic_Amt.getValue(i3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_T_Classic_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_T_Roth_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new     //Natural: WRITE ( 01 ) NOTITLE 'Roth Contrib........' 26T #T-ROTH-AMT ( I1 ) 47T #T-ROTH-AMT ( I2 ) 68T #T-ROTH-AMT ( I3 ) 89T #T-ROTH-AMT ( I4 )
            TabSetting(47),pnd_T_Roth_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_T_Roth_Amt.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_T_Roth_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_T_Rollover_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new //Natural: WRITE ( 01 ) NOTITLE 'Rollover............' 26T #T-ROLLOVER-AMT ( I1 ) 47T #T-ROLLOVER-AMT ( I2 ) 68T #T-ROLLOVER-AMT ( I3 ) 89T #T-ROLLOVER-AMT ( I4 )
            TabSetting(47),pnd_T_Rollover_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_T_Rollover_Amt.getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_T_Rollover_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_T_Roth_Conv_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 01 ) NOTITLE 'Roth Conversion.....' 26T #T-ROTH-CONV-AMT ( I1 ) 47T #T-ROTH-CONV-AMT ( I2 ) 68T #T-ROTH-CONV-AMT ( I3 ) 89T #T-ROTH-CONV-AMT ( I4 )
            TabSetting(47),pnd_T_Roth_Conv_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_T_Roth_Conv_Amt.getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_T_Roth_Conv_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_T_Rechar_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new   //Natural: WRITE ( 01 ) NOTITLE 'Recharacter.........' 26T #T-RECHAR-AMT ( I1 ) 47T #T-RECHAR-AMT ( I2 ) 68T #T-RECHAR-AMT ( I3 ) 89T #T-RECHAR-AMT ( I4 )
            TabSetting(47),pnd_T_Rechar_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_T_Rechar_Amt.getValue(i3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_T_Rechar_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_T_Sep_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new      //Natural: WRITE ( 01 ) NOTITLE 'SEP.................' 26T #T-SEP-AMT ( I1 ) 47T #T-SEP-AMT ( I2 ) 68T #T-SEP-AMT ( I3 ) 89T #T-SEP-AMT ( I4 )
            TabSetting(47),pnd_T_Sep_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_T_Sep_Amt.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_T_Sep_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(25),pnd_T_Fmv_Amt.getValue(i1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new     //Natural: WRITE ( 01 ) NOTITLE 'Fair Market.........' 25T #T-FMV-AMT ( I1 ) 46T #T-FMV-AMT ( I2 ) 67T #T-FMV-AMT ( I3 ) 88T #T-FMV-AMT ( I4 )
            TabSetting(46),pnd_T_Fmv_Amt.getValue(i2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(67),pnd_T_Fmv_Amt.getValue(i3), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(88),pnd_T_Fmv_Amt.getValue(i4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 3);                                                                                                                                          //Natural: SKIP ( 04 ) 3
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"       (CREF)       ",NEWLINE);                                                                     //Natural: WRITE ( 01 ) NOTITLE 01T '       (CREF)       ' /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_C_Trans_Count.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZ9"),new         //Natural: WRITE ( 01 ) NOTITLE 'Transaction Count...' 30T #C-TRANS-COUNT ( I1 ) 54T #C-TRANS-COUNT ( I2 ) 75T #C-TRANS-COUNT ( I3 ) 96T #C-TRANS-COUNT ( I4 )
            TabSetting(54),pnd_C_Trans_Count.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(75),pnd_C_Trans_Count.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZ9"),new TabSetting(96),pnd_C_Trans_Count.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_C_Classic_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 01 ) NOTITLE 'Classic Contrib.....' 26T #C-CLASSIC-AMT ( I1 ) 47T #C-CLASSIC-AMT ( I2 ) 68T #C-CLASSIC-AMT ( I3 ) 89T #C-CLASSIC-AMT ( I4 )
            TabSetting(47),pnd_C_Classic_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_C_Classic_Amt.getValue(i3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_C_Classic_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_C_Roth_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new     //Natural: WRITE ( 01 ) NOTITLE 'Roth Contrib........' 26T #C-ROTH-AMT ( I1 ) 47T #C-ROTH-AMT ( I2 ) 68T #C-ROTH-AMT ( I3 ) 89T #C-ROTH-AMT ( I4 )
            TabSetting(47),pnd_C_Roth_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_C_Roth_Amt.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_C_Roth_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_C_Rollover_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new //Natural: WRITE ( 01 ) NOTITLE 'Rollover............' 26T #C-ROLLOVER-AMT ( I1 ) 47T #C-ROLLOVER-AMT ( I2 ) 68T #C-ROLLOVER-AMT ( I3 ) 89T #C-ROLLOVER-AMT ( I4 )
            TabSetting(47),pnd_C_Rollover_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_C_Rollover_Amt.getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_C_Rollover_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_C_Roth_Conv_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 01 ) NOTITLE 'Roth Conversion.....' 26T #C-ROTH-CONV-AMT ( I1 ) 47T #C-ROTH-CONV-AMT ( I2 ) 68T #C-ROTH-CONV-AMT ( I3 ) 89T #C-ROTH-CONV-AMT ( I4 )
            TabSetting(47),pnd_C_Roth_Conv_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_C_Roth_Conv_Amt.getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_C_Roth_Conv_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_C_Rechar_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new   //Natural: WRITE ( 01 ) NOTITLE 'Recharacter.........' 26T #C-RECHAR-AMT ( I1 ) 47T #C-RECHAR-AMT ( I2 ) 68T #C-RECHAR-AMT ( I3 ) 89T #C-RECHAR-AMT ( I4 )
            TabSetting(47),pnd_C_Rechar_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_C_Rechar_Amt.getValue(i3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_C_Rechar_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_C_Sep_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new      //Natural: WRITE ( 01 ) NOTITLE 'SEP.................' 26T #C-SEP-AMT ( I1 ) 47T #C-SEP-AMT ( I2 ) 68T #C-SEP-AMT ( I3 ) 89T #C-SEP-AMT ( I4 )
            TabSetting(47),pnd_C_Sep_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_C_Sep_Amt.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_C_Sep_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(25),pnd_C_Fmv_Amt.getValue(i1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new     //Natural: WRITE ( 01 ) NOTITLE 'Fair Market.........' 25T #C-FMV-AMT ( I1 ) 46T #C-FMV-AMT ( I2 ) 67T #C-FMV-AMT ( I3 ) 88T #C-FMV-AMT ( I4 )
            TabSetting(46),pnd_C_Fmv_Amt.getValue(i2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(67),pnd_C_Fmv_Amt.getValue(i3), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(88),pnd_C_Fmv_Amt.getValue(i4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 3);                                                                                                                                          //Natural: SKIP ( 04 ) 3
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"       (LIFE)       ",NEWLINE);                                                                     //Natural: WRITE ( 01 ) NOTITLE 01T '       (LIFE)       ' /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_L_Trans_Count.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZ9"),new         //Natural: WRITE ( 01 ) NOTITLE 'Transaction Count...' 30T #L-TRANS-COUNT ( I1 ) 54T #L-TRANS-COUNT ( I2 ) 75T #L-TRANS-COUNT ( I3 ) 96T #L-TRANS-COUNT ( I4 )
            TabSetting(54),pnd_L_Trans_Count.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(75),pnd_L_Trans_Count.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZ9"),new TabSetting(96),pnd_L_Trans_Count.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_L_Classic_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 01 ) NOTITLE 'Classic Contrib.....' 26T #L-CLASSIC-AMT ( I1 ) 47T #L-CLASSIC-AMT ( I2 ) 68T #L-CLASSIC-AMT ( I3 ) 89T #L-CLASSIC-AMT ( I4 )
            TabSetting(47),pnd_L_Classic_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_L_Classic_Amt.getValue(i3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_L_Classic_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_L_Roth_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new     //Natural: WRITE ( 01 ) NOTITLE 'Roth Contrib........' 26T #L-ROTH-AMT ( I1 ) 47T #L-ROTH-AMT ( I2 ) 68T #L-ROTH-AMT ( I3 ) 89T #L-ROTH-AMT ( I4 )
            TabSetting(47),pnd_L_Roth_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_L_Roth_Amt.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_L_Roth_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_L_Rollover_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new //Natural: WRITE ( 01 ) NOTITLE 'Rollover............' 26T #L-ROLLOVER-AMT ( I1 ) 47T #L-ROLLOVER-AMT ( I2 ) 68T #L-ROLLOVER-AMT ( I3 ) 89T #L-ROLLOVER-AMT ( I4 )
            TabSetting(47),pnd_L_Rollover_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_L_Rollover_Amt.getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_L_Rollover_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_L_Roth_Conv_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 01 ) NOTITLE 'Roth Conversion.....' 26T #L-ROTH-CONV-AMT ( I1 ) 47T #L-ROTH-CONV-AMT ( I2 ) 68T #L-ROTH-CONV-AMT ( I3 ) 89T #L-ROTH-CONV-AMT ( I4 )
            TabSetting(47),pnd_L_Roth_Conv_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_L_Roth_Conv_Amt.getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_L_Roth_Conv_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_L_Rechar_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new   //Natural: WRITE ( 01 ) NOTITLE 'Recharacter.........' 26T #L-RECHAR-AMT ( I1 ) 47T #L-RECHAR-AMT ( I2 ) 68T #L-RECHAR-AMT ( I3 ) 89T #L-RECHAR-AMT ( I4 )
            TabSetting(47),pnd_L_Rechar_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_L_Rechar_Amt.getValue(i3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_L_Rechar_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_L_Sep_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new      //Natural: WRITE ( 01 ) NOTITLE 'SEP.................' 26T #L-SEP-AMT ( I1 ) 47T #L-SEP-AMT ( I2 ) 68T #L-SEP-AMT ( I3 ) 89T #L-SEP-AMT ( I4 )
            TabSetting(47),pnd_L_Sep_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_L_Sep_Amt.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_L_Sep_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(25),pnd_L_Fmv_Amt.getValue(i1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new     //Natural: WRITE ( 01 ) NOTITLE 'Fair Market.........' 25T #L-FMV-AMT ( I1 ) 46T #L-FMV-AMT ( I2 ) 67T #L-FMV-AMT ( I3 ) 88T #L-FMV-AMT ( I4 )
            TabSetting(46),pnd_L_Fmv_Amt.getValue(i2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(67),pnd_L_Fmv_Amt.getValue(i3), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(88),pnd_L_Fmv_Amt.getValue(i4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        //* * 09-24-03 ADDED TCII BY FRANK
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"       (TCII)       ",NEWLINE);                                                                     //Natural: WRITE ( 01 ) NOTITLE 01T '       (TCII)       ' /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_S_Trans_Count.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZ9"),new         //Natural: WRITE ( 01 ) NOTITLE 'Transaction Count...' 30T #S-TRANS-COUNT ( I1 ) 54T #S-TRANS-COUNT ( I2 ) 75T #S-TRANS-COUNT ( I3 ) 96T #S-TRANS-COUNT ( I4 )
            TabSetting(54),pnd_S_Trans_Count.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(75),pnd_S_Trans_Count.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZ9"),new TabSetting(96),pnd_S_Trans_Count.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_S_Classic_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 01 ) NOTITLE 'Classic Contrib.....' 26T #S-CLASSIC-AMT ( I1 ) 47T #S-CLASSIC-AMT ( I2 ) 68T #S-CLASSIC-AMT ( I3 ) 89T #S-CLASSIC-AMT ( I4 )
            TabSetting(47),pnd_S_Classic_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_S_Classic_Amt.getValue(i3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_S_Classic_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_S_Roth_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new     //Natural: WRITE ( 01 ) NOTITLE 'Roth Contrib........' 26T #S-ROTH-AMT ( I1 ) 47T #S-ROTH-AMT ( I2 ) 68T #S-ROTH-AMT ( I3 ) 89T #S-ROTH-AMT ( I4 )
            TabSetting(47),pnd_S_Roth_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_S_Roth_Amt.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_S_Roth_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_S_Rollover_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new //Natural: WRITE ( 01 ) NOTITLE 'Rollover............' 26T #S-ROLLOVER-AMT ( I1 ) 47T #S-ROLLOVER-AMT ( I2 ) 68T #S-ROLLOVER-AMT ( I3 ) 89T #S-ROLLOVER-AMT ( I4 )
            TabSetting(47),pnd_S_Rollover_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_S_Rollover_Amt.getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_S_Rollover_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_S_Roth_Conv_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 01 ) NOTITLE 'Roth Conversion.....' 26T #S-ROTH-CONV-AMT ( I1 ) 47T #S-ROTH-CONV-AMT ( I2 ) 68T #S-ROTH-CONV-AMT ( I3 ) 89T #S-ROTH-CONV-AMT ( I4 )
            TabSetting(47),pnd_S_Roth_Conv_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_S_Roth_Conv_Amt.getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_S_Roth_Conv_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_S_Rechar_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new   //Natural: WRITE ( 01 ) NOTITLE 'Recharacter.........' 26T #S-RECHAR-AMT ( I1 ) 47T #S-RECHAR-AMT ( I2 ) 68T #S-RECHAR-AMT ( I3 ) 89T #S-RECHAR-AMT ( I4 )
            TabSetting(47),pnd_S_Rechar_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_S_Rechar_Amt.getValue(i3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_S_Rechar_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_S_Sep_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new      //Natural: WRITE ( 01 ) NOTITLE 'SEP.................' 26T #S-SEP-AMT ( I1 ) 47T #S-SEP-AMT ( I2 ) 68T #S-SEP-AMT ( I3 ) 89T #S-SEP-AMT ( I4 )
            TabSetting(47),pnd_S_Sep_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_S_Sep_Amt.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_S_Sep_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(25),pnd_S_Fmv_Amt.getValue(i1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new     //Natural: WRITE ( 01 ) NOTITLE 'Fair Market.........' 25T #S-FMV-AMT ( I1 ) 46T #S-FMV-AMT ( I2 ) 67T #S-FMV-AMT ( I3 ) 88T #S-FMV-AMT ( I4 )
            TabSetting(46),pnd_S_Fmv_Amt.getValue(i2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(67),pnd_S_Fmv_Amt.getValue(i3), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(88),pnd_S_Fmv_Amt.getValue(i4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 3);                                                                                                                                          //Natural: SKIP ( 04 ) 3
    }
    private void sub_Update_General_Totals() throws Exception                                                                                                             //Natural: UPDATE-GENERAL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------
        pnd_T_Trans_Count.getValue(5,":",8).nadd(pnd_T_Trans_Count.getValue(1,":",4));                                                                                    //Natural: ADD #T-TRANS-COUNT ( 1:4 ) TO #T-TRANS-COUNT ( 5:8 )
        pnd_T_Classic_Amt.getValue(5,":",8).nadd(pnd_T_Classic_Amt.getValue(1,":",4));                                                                                    //Natural: ADD #T-CLASSIC-AMT ( 1:4 ) TO #T-CLASSIC-AMT ( 5:8 )
        pnd_T_Roth_Amt.getValue(5,":",8).nadd(pnd_T_Roth_Amt.getValue(1,":",4));                                                                                          //Natural: ADD #T-ROTH-AMT ( 1:4 ) TO #T-ROTH-AMT ( 5:8 )
        pnd_T_Rollover_Amt.getValue(5,":",8).nadd(pnd_T_Rollover_Amt.getValue(1,":",4));                                                                                  //Natural: ADD #T-ROLLOVER-AMT ( 1:4 ) TO #T-ROLLOVER-AMT ( 5:8 )
        pnd_T_Rechar_Amt.getValue(5,":",8).nadd(pnd_T_Rechar_Amt.getValue(1,":",4));                                                                                      //Natural: ADD #T-RECHAR-AMT ( 1:4 ) TO #T-RECHAR-AMT ( 5:8 )
        pnd_T_Sep_Amt.getValue(5,":",8).nadd(pnd_T_Sep_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #T-SEP-AMT ( 1:4 ) TO #T-SEP-AMT ( 5:8 )
        pnd_T_Fmv_Amt.getValue(5,":",8).nadd(pnd_T_Fmv_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #T-FMV-AMT ( 1:4 ) TO #T-FMV-AMT ( 5:8 )
        pnd_T_Roth_Conv_Amt.getValue(5,":",8).nadd(pnd_T_Roth_Conv_Amt.getValue(1,":",4));                                                                                //Natural: ADD #T-ROTH-CONV-AMT ( 1:4 ) TO #T-ROTH-CONV-AMT ( 5:8 )
        pnd_C_Trans_Count.getValue(5,":",8).nadd(pnd_C_Trans_Count.getValue(1,":",4));                                                                                    //Natural: ADD #C-TRANS-COUNT ( 1:4 ) TO #C-TRANS-COUNT ( 5:8 )
        pnd_C_Classic_Amt.getValue(5,":",8).nadd(pnd_C_Classic_Amt.getValue(1,":",4));                                                                                    //Natural: ADD #C-CLASSIC-AMT ( 1:4 ) TO #C-CLASSIC-AMT ( 5:8 )
        pnd_C_Roth_Amt.getValue(5,":",8).nadd(pnd_C_Roth_Amt.getValue(1,":",4));                                                                                          //Natural: ADD #C-ROTH-AMT ( 1:4 ) TO #C-ROTH-AMT ( 5:8 )
        pnd_C_Rollover_Amt.getValue(5,":",8).nadd(pnd_C_Rollover_Amt.getValue(1,":",4));                                                                                  //Natural: ADD #C-ROLLOVER-AMT ( 1:4 ) TO #C-ROLLOVER-AMT ( 5:8 )
        pnd_C_Rechar_Amt.getValue(5,":",8).nadd(pnd_C_Rechar_Amt.getValue(1,":",4));                                                                                      //Natural: ADD #C-RECHAR-AMT ( 1:4 ) TO #C-RECHAR-AMT ( 5:8 )
        pnd_C_Sep_Amt.getValue(5,":",8).nadd(pnd_C_Sep_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #C-SEP-AMT ( 1:4 ) TO #C-SEP-AMT ( 5:8 )
        pnd_C_Fmv_Amt.getValue(5,":",8).nadd(pnd_C_Fmv_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #C-FMV-AMT ( 1:4 ) TO #C-FMV-AMT ( 5:8 )
        pnd_C_Roth_Conv_Amt.getValue(5,":",8).nadd(pnd_C_Roth_Conv_Amt.getValue(1,":",4));                                                                                //Natural: ADD #C-ROTH-CONV-AMT ( 1:4 ) TO #C-ROTH-CONV-AMT ( 5:8 )
        pnd_L_Trans_Count.getValue(5,":",8).nadd(pnd_L_Trans_Count.getValue(1,":",4));                                                                                    //Natural: ADD #L-TRANS-COUNT ( 1:4 ) TO #L-TRANS-COUNT ( 5:8 )
        pnd_L_Classic_Amt.getValue(5,":",8).nadd(pnd_L_Classic_Amt.getValue(1,":",4));                                                                                    //Natural: ADD #L-CLASSIC-AMT ( 1:4 ) TO #L-CLASSIC-AMT ( 5:8 )
        pnd_L_Roth_Amt.getValue(5,":",8).nadd(pnd_L_Roth_Amt.getValue(1,":",4));                                                                                          //Natural: ADD #L-ROTH-AMT ( 1:4 ) TO #L-ROTH-AMT ( 5:8 )
        pnd_L_Rollover_Amt.getValue(5,":",8).nadd(pnd_L_Rollover_Amt.getValue(1,":",4));                                                                                  //Natural: ADD #L-ROLLOVER-AMT ( 1:4 ) TO #L-ROLLOVER-AMT ( 5:8 )
        pnd_L_Rechar_Amt.getValue(5,":",8).nadd(pnd_L_Rechar_Amt.getValue(1,":",4));                                                                                      //Natural: ADD #L-RECHAR-AMT ( 1:4 ) TO #L-RECHAR-AMT ( 5:8 )
        pnd_L_Sep_Amt.getValue(5,":",8).nadd(pnd_L_Sep_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #L-SEP-AMT ( 1:4 ) TO #L-SEP-AMT ( 5:8 )
        pnd_L_Fmv_Amt.getValue(5,":",8).nadd(pnd_L_Fmv_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #L-FMV-AMT ( 1:4 ) TO #L-FMV-AMT ( 5:8 )
        pnd_L_Roth_Conv_Amt.getValue(5,":",8).nadd(pnd_L_Roth_Conv_Amt.getValue(1,":",4));                                                                                //Natural: ADD #L-ROTH-CONV-AMT ( 1:4 ) TO #L-ROTH-CONV-AMT ( 5:8 )
        //* * 09-24-03 ADDED SERVICES BY FRANK
        pnd_S_Trans_Count.getValue(5,":",8).nadd(pnd_S_Trans_Count.getValue(1,":",4));                                                                                    //Natural: ADD #S-TRANS-COUNT ( 1:4 ) TO #S-TRANS-COUNT ( 5:8 )
        pnd_S_Classic_Amt.getValue(5,":",8).nadd(pnd_S_Classic_Amt.getValue(1,":",4));                                                                                    //Natural: ADD #S-CLASSIC-AMT ( 1:4 ) TO #S-CLASSIC-AMT ( 5:8 )
        pnd_S_Roth_Amt.getValue(5,":",8).nadd(pnd_S_Roth_Amt.getValue(1,":",4));                                                                                          //Natural: ADD #S-ROTH-AMT ( 1:4 ) TO #S-ROTH-AMT ( 5:8 )
        pnd_S_Rollover_Amt.getValue(5,":",8).nadd(pnd_S_Rollover_Amt.getValue(1,":",4));                                                                                  //Natural: ADD #S-ROLLOVER-AMT ( 1:4 ) TO #S-ROLLOVER-AMT ( 5:8 )
        pnd_S_Rechar_Amt.getValue(5,":",8).nadd(pnd_S_Rechar_Amt.getValue(1,":",4));                                                                                      //Natural: ADD #S-RECHAR-AMT ( 1:4 ) TO #S-RECHAR-AMT ( 5:8 )
        pnd_S_Sep_Amt.getValue(5,":",8).nadd(pnd_S_Sep_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #S-SEP-AMT ( 1:4 ) TO #S-SEP-AMT ( 5:8 )
        pnd_S_Fmv_Amt.getValue(5,":",8).nadd(pnd_S_Fmv_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #S-FMV-AMT ( 1:4 ) TO #S-FMV-AMT ( 5:8 )
        pnd_S_Roth_Conv_Amt.getValue(5,":",8).nadd(pnd_S_Roth_Conv_Amt.getValue(1,":",4));                                                                                //Natural: ADD #S-ROTH-CONV-AMT ( 1:4 ) TO #S-ROTH-CONV-AMT ( 5:8 )
    }
    private void sub_Reset_Control_Totals() throws Exception                                                                                                              //Natural: RESET-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------
        pnd_T_Trans_Count.getValue(1,":",4).reset();                                                                                                                      //Natural: RESET #T-TRANS-COUNT ( 1:4 ) #C-TRANS-COUNT ( 1:4 ) #L-TRANS-COUNT ( 1:4 ) #T-CLASSIC-AMT ( 1:4 ) #C-CLASSIC-AMT ( 1:4 ) #L-CLASSIC-AMT ( 1:4 ) #T-ROTH-AMT ( 1:4 ) #C-ROTH-AMT ( 1:4 ) #L-ROTH-AMT ( 1:4 ) #T-ROLLOVER-AMT ( 1:4 ) #C-ROLLOVER-AMT ( 1:4 ) #L-ROLLOVER-AMT ( 1:4 ) #T-RECHAR-AMT ( 1:4 ) #C-RECHAR-AMT ( 1:4 ) #L-RECHAR-AMT ( 1:4 ) #T-SEP-AMT ( 1:4 ) #C-SEP-AMT ( 1:4 ) #L-SEP-AMT ( 1:4 ) #T-FMV-AMT ( 1:4 ) #C-FMV-AMT ( 1:4 ) #L-FMV-AMT ( 1:4 ) #T-ROTH-CONV-AMT ( 1:4 ) #C-ROTH-CONV-AMT ( 1:4 ) #L-ROTH-CONV-AMT ( 1:4 ) #S-TRANS-COUNT ( 1:4 ) #S-CLASSIC-AMT ( 1:4 ) #S-ROTH-AMT ( 1:4 ) #S-ROLLOVER-AMT ( 1:4 ) #S-RECHAR-AMT ( 1:4 ) #S-SEP-AMT ( 1:4 ) #S-FMV-AMT ( 1:4 ) #S-ROTH-CONV-AMT ( 1:4 )
        pnd_C_Trans_Count.getValue(1,":",4).reset();
        pnd_L_Trans_Count.getValue(1,":",4).reset();
        pnd_T_Classic_Amt.getValue(1,":",4).reset();
        pnd_C_Classic_Amt.getValue(1,":",4).reset();
        pnd_L_Classic_Amt.getValue(1,":",4).reset();
        pnd_T_Roth_Amt.getValue(1,":",4).reset();
        pnd_C_Roth_Amt.getValue(1,":",4).reset();
        pnd_L_Roth_Amt.getValue(1,":",4).reset();
        pnd_T_Rollover_Amt.getValue(1,":",4).reset();
        pnd_C_Rollover_Amt.getValue(1,":",4).reset();
        pnd_L_Rollover_Amt.getValue(1,":",4).reset();
        pnd_T_Rechar_Amt.getValue(1,":",4).reset();
        pnd_C_Rechar_Amt.getValue(1,":",4).reset();
        pnd_L_Rechar_Amt.getValue(1,":",4).reset();
        pnd_T_Sep_Amt.getValue(1,":",4).reset();
        pnd_C_Sep_Amt.getValue(1,":",4).reset();
        pnd_L_Sep_Amt.getValue(1,":",4).reset();
        pnd_T_Fmv_Amt.getValue(1,":",4).reset();
        pnd_C_Fmv_Amt.getValue(1,":",4).reset();
        pnd_L_Fmv_Amt.getValue(1,":",4).reset();
        pnd_T_Roth_Conv_Amt.getValue(1,":",4).reset();
        pnd_C_Roth_Conv_Amt.getValue(1,":",4).reset();
        pnd_L_Roth_Conv_Amt.getValue(1,":",4).reset();
        pnd_S_Trans_Count.getValue(1,":",4).reset();
        pnd_S_Classic_Amt.getValue(1,":",4).reset();
        pnd_S_Roth_Amt.getValue(1,":",4).reset();
        pnd_S_Rollover_Amt.getValue(1,":",4).reset();
        pnd_S_Rechar_Amt.getValue(1,":",4).reset();
        pnd_S_Sep_Amt.getValue(1,":",4).reset();
        pnd_S_Fmv_Amt.getValue(1,":",4).reset();
        pnd_S_Roth_Conv_Amt.getValue(1,":",4).reset();
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Source_Total_Header.setValue("  Report Totals  ");                                                                                                            //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '  Report Totals  '
        i1.setValue(5);                                                                                                                                                   //Natural: ASSIGN I1 := 5
        i2.setValue(6);                                                                                                                                                   //Natural: ASSIGN I2 := 6
        i3.setValue(7);                                                                                                                                                   //Natural: ASSIGN I3 := 7
        i4.setValue(8);                                                                                                                                                   //Natural: ASSIGN I4 := 8
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
        sub_Display_Control_Totals();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, new TabSetting(1),"Records Read...............................",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new                  //Natural: WRITE ( 00 ) 01T 'Records Read...............................' #READ-CTR / 01T 'Records Accepted...........................' #ACCEPT-CTR / 01T 'Records Rejected...........................' #REJECT-CTR / 01T 'Records Bypassed (Already On File).........' #BYPASS-CTR
            TabSetting(1),"Records Accepted...........................",pnd_Accept_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"Records Rejected...........................",pnd_Reject_Ctr, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"Records Bypassed (Already On File).........",pnd_Bypass_Ctr, new ReportEditMask 
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"Records Read...............................",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new  //Natural: WRITE ( 01 ) 01T 'Records Read...............................' #READ-CTR / 01T 'Records Accepted...........................' #ACCEPT-CTR / 01T 'Records Rejected...........................' #REJECT-CTR / 01T 'Records Bypassed (Already On File).........' #BYPASS-CTR
            TabSetting(1),"Records Accepted...........................",pnd_Accept_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"Records Rejected...........................",pnd_Reject_Ctr, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"Records Bypassed (Already On File).........",pnd_Bypass_Ctr, new ReportEditMask 
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(48),"IRA Contributions Edit Control Report",new  //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 48T 'IRA Contributions Edit Control Report' 120T 'REPORT: RPT4'
                        TabSetting(120),"REPORT: RPT4");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),pnd_Header_Date, new ReportEditMask ("MM/DD/YYYY"),new TabSetting(58),pnd_Source_Total_Header,new  //Natural: WRITE ( 01 ) NOTITLE 25T #HEADER-DATE ( EM = MM/DD/YYYY ) 58T #SOURCE-TOTAL-HEADER 97T #INTERFACE-DATE ( EM = MM/DD/YYYY )
                        TabSetting(97),pnd_Interface_Date, new ReportEditMask ("MM/DD/YYYY"));
                    getReports().skip(1, 2);                                                                                                                              //Natural: SKIP ( 01 ) 2 LINES
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(22),"       Input        ",new TabSetting(43),"      Bypassed      ",new                    //Natural: WRITE ( 01 ) NOTITLE 22T '       Input        ' 43T '      Bypassed      ' 64T '      Rejected      ' 85T '      Accepted      '
                        TabSetting(64),"      Rejected      ",new TabSetting(85),"      Accepted      ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"                    ",new TabSetting(22),"====================",new                     //Natural: WRITE ( 01 ) NOTITLE 01T '                    ' 22T '====================' 43T '====================' 64T '====================' 85T '===================='
                        TabSetting(43),"====================",new TabSetting(64),"====================",new TabSetting(85),"====================");
                    //*   106T '===================='
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
        pnd_Source_Total_Header.setValue(DbsUtil.compress("Source System: IR"));                                                                                          //Natural: COMPRESS 'Source System: IR' INTO #SOURCE-TOTAL-HEADER
        i1.setValue(1);                                                                                                                                                   //Natural: ASSIGN I1 := 1
        i2.setValue(2);                                                                                                                                                   //Natural: ASSIGN I2 := 2
        i3.setValue(3);                                                                                                                                                   //Natural: ASSIGN I3 := 3
        i4.setValue(4);                                                                                                                                                   //Natural: ASSIGN I4 := 4
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
        sub_Display_Control_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-GENERAL-TOTALS
        sub_Update_General_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
        Global.format(2, "PS=60 LS=133");
        Global.format(3, "PS=60 LS=133");
        Global.format(4, "PS=60 LS=133");
        Global.format(5, "PS=60 LS=133");
        Global.format(6, "PS=60 LS=133");
        Global.format(7, "PS=60 LS=133");
        Global.format(8, "PS=60 LS=133");
    }
}
