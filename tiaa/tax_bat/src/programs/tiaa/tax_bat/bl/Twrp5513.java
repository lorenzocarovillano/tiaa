/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:41:04 PM
**        * FROM NATURAL PROGRAM : Twrp5513
************************************************************
**        * FILE NAME            : Twrp5513.java
**        * CLASS NAME           : Twrp5513
**        * INSTANCE NAME        : Twrp5513
************************************************************
************************************************************************
** PROGRAM : TWRP5513
** SYSTEM  : TAXWARS
** AUTHOR  : MICHAEL SUPONITSKY
** FUNCTION: RECONCILIATION REPORT FOR 480.6A AND 480.6B.
**           (RESIDENCY CODE = '42' OR 'RQ'.
** HISTORY.....:
** 08/13/02  J.ROTHOLZ - RECOMPILED DUE TO INCREASE IN #COMP-NAME
**           LENGTH IN TWRLCOMP & TWRACOMP
* 10/14/03:STOWED DUE TO UPDATE ON TWRL0900-ADDED CONTRACT TYPE & IRC-TO
** 11/12/2003 - M. SUPONITSKY - COPY OF TWRP5503. USED FOR TAX YEARS
**                              LESS THAN 2003.
** 12/06/05 - BK. TAX YEAR ADDED TO TWRNCOMP PARMS
** 02/18/15: OS - RECOMPILED FOR UPDATED TWRL0900
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5513 extends BLNatBase
{
    // Data Areas
    private PdaTwracomp pdaTwracomp;
    private LdaTwrl0900 ldaTwrl0900;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Pnd_Cntl_Max;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_Company_Line;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_S1;
    private DbsField pnd_Ws_Pnd_S2;
    private DbsField pnd_Ws_Pnd_Tran_Cv;
    private DbsField pnd_Ws_Pnd_Gross_Cv;
    private DbsField pnd_Ws_Pnd_Sta_Cv;
    private DbsField pnd_Ws_Pnd_Int_Cv;

    private DbsGroup pnd_Cntl;
    private DbsField pnd_Cntl_Pnd_Cntl_Text;
    private DbsField pnd_Cntl_Pnd_Tran_Cnt_Cv;
    private DbsField pnd_Cntl_Pnd_Gross_Amt_Cv;
    private DbsField pnd_Cntl_Pnd_Sta_Tax_Cv;
    private DbsField pnd_Cntl_Pnd_Int_Amt_Cv;

    private DbsGroup pnd_Cntl_Pnd_Totals;
    private DbsField pnd_Cntl_Pnd_Tran_Cnt;
    private DbsField pnd_Cntl_Pnd_Gross_Amt;
    private DbsField pnd_Cntl_Pnd_Sta_Tax;
    private DbsField pnd_Cntl_Pnd_Int_Amt;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Twrpymnt_Company_Cde_FormOld;
    private DbsField readWork01Twrpymnt_Tax_YearOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwracomp = new PdaTwracomp(localVariables);
        ldaTwrl0900 = new LdaTwrl0900();
        registerRecord(ldaTwrl0900);

        // Local Variables

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Pnd_Cntl_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_Cntl_Max", "#CNTL-MAX", FieldType.PACKED_DECIMAL, 3);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Tax_Year = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Company_Line = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Line", "#COMPANY-LINE", FieldType.STRING, 59);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S1", "#S1", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S2", "#S2", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Tran_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tran_Cv", "#TRAN-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Gross_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Gross_Cv", "#GROSS-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Sta_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Sta_Cv", "#STA-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Int_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Int_Cv", "#INT-CV", FieldType.ATTRIBUTE_CONTROL, 2);

        pnd_Cntl = localVariables.newGroupArrayInRecord("pnd_Cntl", "#CNTL", new DbsArrayController(1, 5));
        pnd_Cntl_Pnd_Cntl_Text = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Cntl_Text", "#CNTL-TEXT", FieldType.STRING, 15);
        pnd_Cntl_Pnd_Tran_Cnt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Tran_Cnt_Cv", "#TRAN-CNT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Gross_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Gross_Amt_Cv", "#GROSS-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Sta_Tax_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Sta_Tax_Cv", "#STA-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Int_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Int_Amt_Cv", "#INT-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);

        pnd_Cntl_Pnd_Totals = pnd_Cntl.newGroupArrayInGroup("pnd_Cntl_Pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Cntl_Pnd_Tran_Cnt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Tran_Cnt", "#TRAN-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Cntl_Pnd_Gross_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Cntl_Pnd_Sta_Tax = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Sta_Tax", "#STA-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Int_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Twrpymnt_Company_Cde_FormOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrpymnt_Company_Cde_Form_OLD", "Twrpymnt_Company_Cde_Form_OLD", 
            FieldType.STRING, 1);
        readWork01Twrpymnt_Tax_YearOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrpymnt_Tax_Year_OLD", "Twrpymnt_Tax_Year_OLD", FieldType.NUMERIC, 
            4);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaTwrl0900.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Pnd_Cntl_Max.setInitialValue(5);
        pnd_Cntl_Pnd_Cntl_Text.getValue(1).setInitialValue("All Payments");
        pnd_Cntl_Pnd_Cntl_Text.getValue(2).setInitialValue("Active Payments");
        pnd_Cntl_Pnd_Cntl_Text.getValue(3).setInitialValue("480.6A");
        pnd_Cntl_Pnd_Cntl_Text.getValue(4).setInitialValue("480.6B");
        pnd_Cntl_Pnd_Cntl_Text.getValue(5).setInitialValue("Form Total");
        pnd_Cntl_Pnd_Tran_Cnt_Cv.setInitialAttributeValue("AD=I)#CNTL.#TRAN-CNT-CV(2)	(AD=I)#CNTL.#TRAN-CNT-CV(3)	(AD=N)#CNTL.#TRAN-CNT-CV(4)	(AD=N)#CNTL.#TRAN-CNT-CV(5)	(AD=N");
        pnd_Cntl_Pnd_Gross_Amt_Cv.setInitialAttributeValue("AD=I)#CNTL.#GROSS-AMT-CV(2)	(AD=I)#CNTL.#GROSS-AMT-CV(3)	(AD=I)#CNTL.#GROSS-AMT-CV(4)	(AD=I)#CNTL.#GROSS-AMT-CV(5)	(AD=I");
        pnd_Cntl_Pnd_Sta_Tax_Cv.setInitialAttributeValue("AD=I)#CNTL.#STA-TAX-CV(2)	(AD=I)#CNTL.#STA-TAX-CV(3)	(AD=N)#CNTL.#STA-TAX-CV(4)	(AD=I)#CNTL.#STA-TAX-CV(5)	(AD=I");
        pnd_Cntl_Pnd_Int_Amt_Cv.setInitialAttributeValue("AD=I)#CNTL.#INT-AMT-CV(2)	(AD=I)#CNTL.#INT-AMT-CV(3)	(AD=I)#CNTL.#INT-AMT-CV(4)	(AD=N)#CNTL.#INT-AMT-CV(5)	(AD=I");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5513() throws Exception
    {
        super("Twrp5513");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 133 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 45T 'Summary of Puerto Rico Payment Transactions' 120T 'Report: RPT1' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / #WS.#COMPANY-LINE //
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 RECORD #XTAXYR-F94
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaTwrl0900.getPnd_Xtaxyr_F94())))
        {
            CheckAtStartofData185();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            FOR01:                                                                                                                                                        //Natural: AT START OF DATA;//Natural: AT BREAK OF #XTAXYR-F94.TWRPYMNT-COMPANY-CDE-FORM;//Natural: AT END OF DATA;//Natural: FOR #I = 1 TO #XTAXYR-F94.#C-TWRPYMNT-PAYMENTS
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments())); pnd_Ws_Pnd_I.nadd(1))
            {
                //*                                                   ALL PAYMENTS
                pnd_Cntl_Pnd_Tran_Cnt.getValue(1,1).nadd(1);                                                                                                              //Natural: ADD 1 TO #CNTL.#TRAN-CNT ( 1,1 )
                pnd_Cntl_Pnd_Gross_Amt.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 1,1 )
                pnd_Cntl_Pnd_Sta_Tax.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) TO #CNTL.#STA-TAX ( 1,1 )
                pnd_Cntl_Pnd_Int_Amt.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                         //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#INT-AMT ( 1,1 )
                if (condition(! (ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals(" ") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals("C")))) //Natural: IF NOT #XTAXYR-F94.TWRPYMNT-PYMNT-STATUS ( #I ) = ' ' OR = 'C'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*                                                   ACTIVE PAYMENTS
                pnd_Cntl_Pnd_Tran_Cnt.getValue(2,1).nadd(1);                                                                                                              //Natural: ADD 1 TO #CNTL.#TRAN-CNT ( 2,1 )
                pnd_Cntl_Pnd_Gross_Amt.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 2,1 )
                pnd_Cntl_Pnd_Sta_Tax.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) TO #CNTL.#STA-TAX ( 2,1 )
                pnd_Cntl_Pnd_Int_Amt.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                         //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#INT-AMT ( 2,1 )
                if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I).equals(new DbsDecimal("0.00"))))                                //Natural: IF #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) = 0.00
                {
                    pnd_Cntl_Pnd_Gross_Amt.getValue(3,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 3,1 )
                    pnd_Cntl_Pnd_Int_Amt.getValue(3,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#INT-AMT ( 3,1 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cntl_Pnd_Gross_Amt.getValue(4,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 4,1 )
                    pnd_Cntl_Pnd_Sta_Tax.getValue(4,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) TO #CNTL.#STA-TAX ( 4,1 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            readWork01Twrpymnt_Company_Cde_FormOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form());                                                   //Natural: END-WORK
            readWork01Twrpymnt_Tax_YearOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            pnd_Ws_Pnd_Company_Line.setValue("Grand Totals");                                                                                                             //Natural: ASSIGN #WS.#COMPANY-LINE := 'Grand Totals'
            pnd_Ws_Pnd_S2.setValue(2);                                                                                                                                    //Natural: ASSIGN #WS.#S2 := 2
                                                                                                                                                                          //Natural: PERFORM WRITE-PUERTO-RICO-REPORT
            sub_Write_Puerto_Rico_Report();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //* **********************
        //*  S U B R O U T I N E S
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PUERTO-RICO-REPORT
        //* *****************************************
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COMPANY
    }
    private void sub_Write_Puerto_Rico_Report() throws Exception                                                                                                          //Natural: WRITE-PUERTO-RICO-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntl_Pnd_Gross_Amt.getValue(5,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Gross_Amt.getValue(5,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Gross_Amt.getValue(3, //Natural: ASSIGN #CNTL.#GROSS-AMT ( 5,#S2 ) := #CNTL.#GROSS-AMT ( 3,#S2 ) + #CNTL.#GROSS-AMT ( 4,#S2 )
            pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Gross_Amt.getValue(4,pnd_Ws_Pnd_S2)));
        pnd_Cntl_Pnd_Sta_Tax.getValue(5,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Sta_Tax.getValue(4,pnd_Ws_Pnd_S2));                                                          //Natural: ASSIGN #CNTL.#STA-TAX ( 5,#S2 ) := #CNTL.#STA-TAX ( 4,#S2 )
        pnd_Cntl_Pnd_Int_Amt.getValue(5,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Int_Amt.getValue(3,pnd_Ws_Pnd_S2));                                                          //Natural: ASSIGN #CNTL.#INT-AMT ( 5,#S2 ) := #CNTL.#INT-AMT ( 3,#S2 )
        FOR02:                                                                                                                                                            //Natural: FOR #WS.#S1 = 1 TO #CNTL-MAX
        for (pnd_Ws_Pnd_S1.setValue(1); condition(pnd_Ws_Pnd_S1.lessOrEqual(pnd_Ws_Const_Pnd_Cntl_Max)); pnd_Ws_Pnd_S1.nadd(1))
        {
            pnd_Ws_Pnd_Tran_Cv.setValue(pnd_Cntl_Pnd_Tran_Cnt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                //Natural: ASSIGN #WS.#TRAN-CV := #CNTL.#TRAN-CNT-CV ( #S1 )
            pnd_Ws_Pnd_Gross_Cv.setValue(pnd_Cntl_Pnd_Gross_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                              //Natural: ASSIGN #WS.#GROSS-CV := #CNTL.#GROSS-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Int_Cv.setValue(pnd_Cntl_Pnd_Int_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#INT-CV := #CNTL.#INT-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Sta_Cv.setValue(pnd_Cntl_Pnd_Sta_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#STA-CV := #CNTL.#STA-TAX-CV ( #S1 )
            getReports().display(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",                         //Natural: DISPLAY ( 1 ) ( HC = R ES = ON ) '/' #CNTL-TEXT ( #S1 ) 'Trans Count' #CNTL.#TRAN-CNT ( #S1,#S2 ) ( CV = #TRAN-CV ) 'Gross Amount' #CNTL.#GROSS-AMT ( #S1,#S2 ) ( CV = #GROSS-CV ) 3X 'Puerto Rico Tax' #CNTL.#STA-TAX ( #S1,#S2 ) ( CV = #STA-CV ) 3X 'Interest' #CNTL.#INT-AMT ( #S1,#S2 ) ( CV = #INT-CV )
            		pnd_Cntl_Pnd_Cntl_Text.getValue(pnd_Ws_Pnd_S1),"Trans Count",
            		pnd_Cntl_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Tran_Cv,"Gross Amount",
            		pnd_Cntl_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Gross_Cv,new ColumnSpacing(3),"Puerto Rico Tax",
            		pnd_Cntl_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Sta_Cv,new ColumnSpacing(3),"Interest",
            		pnd_Cntl_Pnd_Int_Amt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Int_Cv);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1 LINES
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Process_Company() throws Exception                                                                                                                   //Natural: PROCESS-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        DbsUtil.callnat(Twrncomp.class , getCurrentProcessState(), pdaTwracomp.getPnd_Twracomp_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracomp.getPnd_Twracomp_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOMP' USING #TWRACOMP.#INPUT-PARMS ( AD = O ) #TWRACOMP.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaTwrl0900_getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_FormIsBreak = ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form().isBreak(endOfData);
        if (condition(ldaTwrl0900_getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_FormIsBreak))
        {
            pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(readWork01Twrpymnt_Company_Cde_FormOld);                                                                 //Natural: ASSIGN #TWRACOMP.#COMP-CODE := OLD ( #XTAXYR-F94.TWRPYMNT-COMPANY-CDE-FORM )
            pdaTwracomp.getPnd_Twracomp_Pnd_Tax_Year().setValue(readWork01Twrpymnt_Tax_YearOld);                                                                          //Natural: ASSIGN #TWRACOMP.#TAX-YEAR := OLD ( #XTAXYR-F94.TWRPYMNT-TAX-YEAR )
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMPANY
            sub_Process_Company();
            if (condition(Global.isEscape())) {return;}
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            pnd_Ws_Pnd_Company_Line.setValue("Company:");                                                                                                                 //Natural: ASSIGN #WS.#COMPANY-LINE := 'Company:'
            setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),pnd_Ws_Pnd_Company_Line,11,4);                                                          //Natural: MOVE #TWRACOMP.#COMP-SHORT-NAME TO SUBSTR ( #WS.#COMPANY-LINE,11,4 )
            setValueToSubstring("-",pnd_Ws_Pnd_Company_Line,16,1);                                                                                                        //Natural: MOVE '-' TO SUBSTR ( #WS.#COMPANY-LINE,16,1 )
            setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Name(),pnd_Ws_Pnd_Company_Line,18,37);                                                               //Natural: MOVE #TWRACOMP.#COMP-NAME TO SUBSTR ( #WS.#COMPANY-LINE,18,37 )
            pnd_Ws_Pnd_S2.setValue(1);                                                                                                                                    //Natural: ASSIGN #WS.#S2 := 1
                                                                                                                                                                          //Natural: PERFORM WRITE-PUERTO-RICO-REPORT
            sub_Write_Puerto_Rico_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Cntl_Pnd_Tran_Cnt.getValue("*",2).nadd(pnd_Cntl_Pnd_Tran_Cnt.getValue("*",1));                                                                            //Natural: ADD #CNTL.#TRAN-CNT ( *,1 ) TO #CNTL.#TRAN-CNT ( *,2 )
            pnd_Cntl_Pnd_Gross_Amt.getValue("*",2).nadd(pnd_Cntl_Pnd_Gross_Amt.getValue("*",1));                                                                          //Natural: ADD #CNTL.#GROSS-AMT ( *,1 ) TO #CNTL.#GROSS-AMT ( *,2 )
            pnd_Cntl_Pnd_Sta_Tax.getValue("*",2).nadd(pnd_Cntl_Pnd_Sta_Tax.getValue("*",1));                                                                              //Natural: ADD #CNTL.#STA-TAX ( *,1 ) TO #CNTL.#STA-TAX ( *,2 )
            pnd_Cntl_Pnd_Int_Amt.getValue("*",2).nadd(pnd_Cntl_Pnd_Int_Amt.getValue("*",1));                                                                              //Natural: ADD #CNTL.#INT-AMT ( *,1 ) TO #CNTL.#INT-AMT ( *,2 )
            pnd_Cntl_Pnd_Totals.getValue("*",1).reset();                                                                                                                  //Natural: RESET #CNTL.#TOTALS ( *,1 )
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=133 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(45),"Summary of Puerto Rico Payment Transactions",new TabSetting(120),"Report: RPT1",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",
            
        		pnd_Cntl_Pnd_Cntl_Text,"Trans Count",
        		pnd_Cntl_Pnd_Tran_Cnt, pnd_Ws_Pnd_Tran_Cv,"Gross Amount",
        		pnd_Cntl_Pnd_Gross_Amt, pnd_Ws_Pnd_Gross_Cv,new ColumnSpacing(3),"Puerto Rico Tax",
        		pnd_Cntl_Pnd_Sta_Tax, pnd_Ws_Pnd_Sta_Cv,new ColumnSpacing(3),"Interest",
        		pnd_Cntl_Pnd_Int_Amt, pnd_Ws_Pnd_Int_Cv);
    }
    private void CheckAtStartofData185() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Ws_Pnd_Tax_Year.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());                                                                              //Natural: ASSIGN #WS.#TAX-YEAR := #XTAXYR-F94.TWRPYMNT-TAX-YEAR
            //*  BK
        }                                                                                                                                                                 //Natural: END-START
    }
}
