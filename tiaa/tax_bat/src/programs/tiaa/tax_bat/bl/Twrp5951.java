/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:42:30 PM
**        * FROM NATURAL PROGRAM : Twrp5951
************************************************************
**        * FILE NAME            : Twrp5951.java
**        * CLASS NAME           : Twrp5951
**        * INSTANCE NAME        : Twrp5951
************************************************************
************************************************************************
** PROGRAM : TWRP5951
** SYSTEM  : TAXWARS
** AUTHOR  : FELIX ORTIZ
** FUNCTION: FORMS SYSTEM HOLD REPORTS
** HISTORY.....:
**    11/04/1999 - FELIX ORTIZ
**                 INCLUDE COMPANY SHORT NAME IN SORT STATEMENT.
**    02/16/2000 - FELIX ORTIZ
**                 INCLUDE FORM TYPE IN SORT STATEMENT FOR HOLD CODE RPT
**    09/26/2002 - E.BOTTERI
**                 GET NEW PAPER PRINT HOLD FROM LDA, ADD IT TO REPORT,
**                 MODIFY LOGIC TO DISPLAY 'Moore Mailable' FLAG AND
**                 INCLUDE SERVICES (TCII) IN THE BREAK DOWN.
**    11/19/2002 - MN - RECOMPILED DUE TO FIELDS ADDED TO TWRL5001
**    11/10/2005 - MS RECOMPILED - CHANGE IN TWRL5001
**    01/30/2008 - AY REPLACED TWRL5000 WITH TWRAFRMN / TWRNFRMN.
**    01/07/2011 - JR REMOVED MOORE MAILABLE ERROR FROM REPORTS.
**    08/15/2012 - JB ADD REPORTS FOR CUNY PHASE 2
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5951 extends BLNatBase
{
    // Data Areas
    private PdaTwrafrmn pdaTwrafrmn;
    private LdaTwrl5001 ldaTwrl5001;
    private LdaTwrl5951 ldaTwrl5951;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Key;
    private DbsField pnd_Sys_Date;
    private DbsField pnd_Sys_Date_N;
    private DbsField pnd_Sys_Time;
    private DbsField pnd_Read_Cnt;
    private DbsField pnd_Ndx;
    private DbsField pnd_Dtf_Reject_Ind;
    private DbsField pnd_Rrd_Hold_Ind;
    private DbsField pnd_Letter_Hold;
    private DbsField pnd_Err_Cnt;
    private DbsField pnd_Err_Cnt_T10;
    private DbsField pnd_Old_Comp_Short_Name;
    private DbsField pnd_Old_Form_Type;
    private DbsField pnd_S3_Key;

    private DbsGroup pnd_S3_Key__R_Field_1;
    private DbsField pnd_S3_Key_Pnd_S3_Tax_Year;
    private DbsField pnd_S3_Key_Pnd_S3_Status;
    private DbsField pnd_S3_Key_Pnd_S3_Form_Type;
    private DbsField pnd_S3_Key_Pnd_S3_Form_Comp;
    private DbsField pnd_S3_Key_Pnd_S3_Form_Tin;

    private DbsRecord internalLoopRecord;
    private DbsField rEAD_SYSHOLDPnd_Form_TypeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwrafrmn = new PdaTwrafrmn(localVariables);
        ldaTwrl5001 = new LdaTwrl5001();
        registerRecord(ldaTwrl5001);
        ldaTwrl5951 = new LdaTwrl5951();
        registerRecord(ldaTwrl5951);

        // Local Variables
        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 20);
        pnd_Sys_Date = localVariables.newFieldInRecord("pnd_Sys_Date", "#SYS-DATE", FieldType.DATE);
        pnd_Sys_Date_N = localVariables.newFieldInRecord("pnd_Sys_Date_N", "#SYS-DATE-N", FieldType.NUMERIC, 8);
        pnd_Sys_Time = localVariables.newFieldInRecord("pnd_Sys_Time", "#SYS-TIME", FieldType.TIME);
        pnd_Read_Cnt = localVariables.newFieldInRecord("pnd_Read_Cnt", "#READ-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ndx = localVariables.newFieldInRecord("pnd_Ndx", "#NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Dtf_Reject_Ind = localVariables.newFieldInRecord("pnd_Dtf_Reject_Ind", "#DTF-REJECT-IND", FieldType.STRING, 1);
        pnd_Rrd_Hold_Ind = localVariables.newFieldInRecord("pnd_Rrd_Hold_Ind", "#RRD-HOLD-IND", FieldType.STRING, 1);
        pnd_Letter_Hold = localVariables.newFieldInRecord("pnd_Letter_Hold", "#LETTER-HOLD", FieldType.STRING, 1);
        pnd_Err_Cnt = localVariables.newFieldArrayInRecord("pnd_Err_Cnt", "#ERR-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 61));
        pnd_Err_Cnt_T10 = localVariables.newFieldArrayInRecord("pnd_Err_Cnt_T10", "#ERR-CNT-T10", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            61));
        pnd_Old_Comp_Short_Name = localVariables.newFieldInRecord("pnd_Old_Comp_Short_Name", "#OLD-COMP-SHORT-NAME", FieldType.STRING, 4);
        pnd_Old_Form_Type = localVariables.newFieldInRecord("pnd_Old_Form_Type", "#OLD-FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_S3_Key = localVariables.newFieldInRecord("pnd_S3_Key", "#S3-KEY", FieldType.STRING, 18);

        pnd_S3_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_S3_Key__R_Field_1", "REDEFINE", pnd_S3_Key);
        pnd_S3_Key_Pnd_S3_Tax_Year = pnd_S3_Key__R_Field_1.newFieldInGroup("pnd_S3_Key_Pnd_S3_Tax_Year", "#S3-TAX-YEAR", FieldType.STRING, 4);
        pnd_S3_Key_Pnd_S3_Status = pnd_S3_Key__R_Field_1.newFieldInGroup("pnd_S3_Key_Pnd_S3_Status", "#S3-STATUS", FieldType.STRING, 1);
        pnd_S3_Key_Pnd_S3_Form_Type = pnd_S3_Key__R_Field_1.newFieldInGroup("pnd_S3_Key_Pnd_S3_Form_Type", "#S3-FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_S3_Key_Pnd_S3_Form_Comp = pnd_S3_Key__R_Field_1.newFieldInGroup("pnd_S3_Key_Pnd_S3_Form_Comp", "#S3-FORM-COMP", FieldType.STRING, 1);
        pnd_S3_Key_Pnd_S3_Form_Tin = pnd_S3_Key__R_Field_1.newFieldInGroup("pnd_S3_Key_Pnd_S3_Form_Tin", "#S3-FORM-TIN", FieldType.STRING, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        rEAD_SYSHOLDPnd_Form_TypeOld = internalLoopRecord.newFieldInRecord("READ_SYSHOLD_Pnd_Form_Type_OLD", "Pnd_Form_Type_OLD", FieldType.NUMERIC, 2);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaTwrl5001.initializeValues();
        ldaTwrl5951.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Twrp5951() throws Exception
    {
        super("Twrp5951");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 1 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 2 ) PS = 58 LS = 133 ZP = ON SF = 2;//Natural: FORMAT ( 3 ) PS = 58 LS = 133 ZP = ON SF = 2;//Natural: FORMAT ( 4 ) PS = 58 LS = 133 ZP = ON SF = 3;//Natural: FORMAT ( 5 ) PS = 58 LS = 133 ZP = ON SF = 3;//Natural: FORMAT ( 6 ) PS = 58 LS = 133 ZP = ON SF = 3;//Natural: FORMAT ( 7 ) PS = 58 LS = 133 ZP = ON SF = 3
        getReports().definePrinter(6, "'CMPRT02'");                                                                                                                       //Natural: DEFINE PRINTER ( 5 ) OUTPUT 'CMPRT02'
        getReports().definePrinter(7, "'CMPRT03'");                                                                                                                       //Natural: DEFINE PRINTER ( 6 ) OUTPUT 'CMPRT03'
        getReports().definePrinter(5, "'CMPRT03'");                                                                                                                       //Natural: DEFINE PRINTER ( 4 ) OUTPUT 'CMPRT03'
        //*  01/30/2008 - AY
        //*  01/30/2008 - AY
        //*  01/30/2008 - AY
        //*  01/30/2008 - AY
        //*  01/30/2008 - AY
        //*  01/30/2008 - AY
        //*  01/30/2008 - AY
        //*  01/30/2008 - AY
        //*  01/30/2008 - AY
        //*  01/30/2008 - AY
        //*  01/30/2008 - AY
        //*  01/30/2008 - AY
        //*  01/30/2008 - AY
        getReports().definePrinter(8, "'CMPRT03'");                                                                                                                       //Natural: DEFINE PRINTER ( 7 ) OUTPUT 'CMPRT03'
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        pnd_Sys_Date.setValue(Global.getDATX());                                                                                                                          //Natural: ASSIGN #SYS-DATE := *DATX
        pnd_Sys_Time.setValue(Global.getTIMX());                                                                                                                          //Natural: ASSIGN #SYS-TIME := *TIMX
        pnd_Sys_Date_N.setValue(Global.getDATN());                                                                                                                        //Natural: ASSIGN #SYS-DATE-N := *DATN
        //*    REPORT HEADING
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 22T 'Tax Withholding and Reporting System' 68T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 34T 'Control Report' 68T 'Report: RPT1' // 'Tax Year: ' TWRL5951.#TAX-YEAR //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 52T 'System Hold Report by Hold Code' 110T 'Report: RPT2' / 59T 'Tax Year' TWRL5951.#TAX-YEAR // 'Company  : ' #COMP-SHORT-NAME / 'Form Type: ' #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 5 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 05 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 52T 'SUNY Hold Report by Hold Code' 110T 'Report: RPT5' / 59T 'Tax Year' TWRL5951.#TAX-YEAR // 'Form Type: ' #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) //
        //*  'COMPANY  : ' #COMP-SHORT-NAME /
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 3 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 03 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 50T 'System Hold Report by Participant' 110T 'Report: RPT3' / 59T 'Tax Year' TWRL5951.#TAX-YEAR // 'Company  : ' #COMP-SHORT-NAME / 'Form Type: ' #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 6 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 06 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 50T 'SUNY Hold Report by Participant' 110T 'Report: RPT6' / 59T 'Tax Year' TWRL5951.#TAX-YEAR // 'Form Type: ' #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) //
        //*  'COMPANY  : ' #COMP-SHORT-NAME /
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 4 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 04 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 50T 'System Hold Report by Participant' 110T 'Report: RPT4' / 59T 'Tax Year' TWRL5951.#TAX-YEAR // 'Company  : ' #OLD-COMP-SHORT-NAME / 'Form Type: ' #TWRAFRMN.#FORM-NAME ( #OLD-FORM-TYPE ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 7 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 07 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 50T 'SUNY Hold Report by Participant' 110T 'Report: RPT7' / 59T 'Tax Year' TWRL5951.#TAX-YEAR // 'Company  : ' #COMP-SHORT-NAME / 'Form Type: ' #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) //
        //*  'Company  : ' #OLD-COMP-SHORT-NAME /
        //*  (#OLD-FORM-TYPE)      //               /* 01/30/2008 - AY
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        boolean endOfDataReadSyshold = true;                                                                                                                              //Natural: READ WORK FILE 1 TWRL5951
        boolean firstReadSyshold = true;
        READ_SYSHOLD:
        while (condition(getWorkFiles().read(1, ldaTwrl5951.getTwrl5951())))
        {
            CheckAtStartofData171();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventRead_Syshold();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadSyshold = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            if (condition(ldaTwrl5951.getTwrl5951_Pnd_Form_Type().equals(10)))                                                                                            //Natural: IF #FORM-TYPE = 10
            {
                ldaTwrl5951.getTwrl5951_Pnd_Comp_Short_Name().setValue("H'FF'");                                                                                          //Natural: ASSIGN #COMP-SHORT-NAME := H'FF'
                if (condition(ldaTwrl5951.getTwrl5951_Pnd_Contract_Nbr().greater(" ")))                                                                                   //Natural: IF #CONTRACT-NBR > ' '
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Dtf_Reject_Ind.setValue("N");                                                                                                                     //Natural: ASSIGN #DTF-REJECT-IND := 'N'
                    pnd_Rrd_Hold_Ind.setValue("N");                                                                                                                       //Natural: ASSIGN #RRD-HOLD-IND := 'N'
                    pnd_Letter_Hold.setValue("N");                                                                                                                        //Natural: ASSIGN #LETTER-HOLD := 'N'
                    if (condition(ldaTwrl5001.getPnd_Twrl5001_Pnd_Suny_Rrd_Hold().getValue(ldaTwrl5951.getTwrl5951_Pnd_Err_Cde().getInt() + 1).getBoolean()))             //Natural: IF #SUNY-RRD-HOLD ( #ERR-CDE )
                    {
                        pnd_Rrd_Hold_Ind.setValue("Y");                                                                                                                   //Natural: ASSIGN #RRD-HOLD-IND := 'Y'
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl5001.getPnd_Twrl5001_Pnd_Suny_Dtf_Reject().getValue(ldaTwrl5951.getTwrl5951_Pnd_Err_Cde().getInt() + 1).getBoolean()))           //Natural: IF #SUNY-DTF-REJECT ( #ERR-CDE )
                    {
                        pnd_Dtf_Reject_Ind.setValue("Y");                                                                                                                 //Natural: ASSIGN #DTF-REJECT-IND := 'Y'
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl5001.getPnd_Twrl5001_Pnd_Suny_Ltr_Hold().getValue(ldaTwrl5951.getTwrl5951_Pnd_Err_Cde().getInt() + 1).getBoolean()))             //Natural: IF #SUNY-LTR-HOLD ( #ERR-CDE )
                    {
                        pnd_Letter_Hold.setValue("Y");                                                                                                                    //Natural: ASSIGN #LETTER-HOLD := 'Y'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  01/30/2008 - AY
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: BEFORE BREAK PROCESSING
            pnd_Key.setValue(ldaTwrl5951.getTwrl5951_Pnd_Tin());                                                                                                          //Natural: ASSIGN #KEY := #TIN
            setValueToSubstring(ldaTwrl5951.getTwrl5951_Pnd_Contract_Nbr(),pnd_Key,11,8);                                                                                 //Natural: MOVE #CONTRACT-NBR TO SUBSTR ( #KEY,11,8 )
            setValueToSubstring(ldaTwrl5951.getTwrl5951_Pnd_Payee_Cde(),pnd_Key,19,2);                                                                                    //Natural: MOVE #PAYEE-CDE TO SUBSTR ( #KEY,19,2 )
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //*                                                                                                                                                           //Natural: AT BREAK OF #KEY
            //*                                                                                                                                                           //Natural: AT BREAK OF #FORM-TYPE
            //*                                                                                                                                                           //Natural: AT BREAK OF #COMP-SHORT-NAME
            pnd_Read_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CNT
            if (condition(ldaTwrl5951.getTwrl5951_Pnd_Form_Type().notEquals(10)))                                                                                         //Natural: IF #FORM-TYPE NE 10
            {
                getReports().display(3, "/Tax ID/Number",                                                                                                                 //Natural: DISPLAY ( 3 ) '/Tax ID/Number' #TIN ( IS = ON ) '/Contract/Number' #CONTRACT-NBR ( IS = ON ) '//Payee' #PAYEE-CDE ( IS = ON LC = �� ) '/Moore/Mailable' #MOORE-MAIL ( LC = ��� ) '/In-House/Mailable' #IN-HOUSE-MAIL ( LC = ��� ) '/Volume/Printable' #VOL-PRINTABLE ( LC = ���� ) '/IRS/Reportable' #IRS-REPORTABLE ( LC = ���� ) '/Error/Code' #ERR-CDE '//Error Description' #TWRL5001.#ERR-DESC ( #ERR-CDE )
                		ldaTwrl5951.getTwrl5951_Pnd_Tin(), new IdenticalSuppress(true),"/Contract/Number",
                		ldaTwrl5951.getTwrl5951_Pnd_Contract_Nbr(), new IdenticalSuppress(true),"//Payee",
                		ldaTwrl5951.getTwrl5951_Pnd_Payee_Cde(), new IdenticalSuppress(true), new FieldAttributes("LC=��"),"/Moore/Mailable",
                		ldaTwrl5951.getTwrl5951_Pnd_Moore_Mail(), new FieldAttributes("LC=���"),"/In-House/Mailable",
                		ldaTwrl5951.getTwrl5951_Pnd_In_House_Mail(), new FieldAttributes("LC=���"),"/Volume/Printable",
                		ldaTwrl5951.getTwrl5951_Pnd_Vol_Printable(), new FieldAttributes("LC=����"),"/IRS/Reportable",
                		ldaTwrl5951.getTwrl5951_Pnd_Irs_Reportable(), new FieldAttributes("LC=����"),"/Error/Code",
                		ldaTwrl5951.getTwrl5951_Pnd_Err_Cde(),"//Error Description",
                		ldaTwrl5001.getPnd_Twrl5001_Pnd_Err_Desc().getValue(ldaTwrl5951.getTwrl5951_Pnd_Err_Cde().getInt() + 1));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_SYSHOLD"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_SYSHOLD"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    'Moore/Mailable/Error' #TWRL5001.#MOORE-HOLD (#ERR-CDE)
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().display(6, "/Tax ID/Number",                                                                                                                 //Natural: DISPLAY ( 6 ) '/Tax ID/Number' #TIN ( IS = ON ) '/RRD/Hold' #RRD-HOLD-IND ( LC = ��� ) '/DTF/Reject' #DTF-REJECT-IND ( LC = ��� ) '/Letter/Hold' #LETTER-HOLD ( LC = ���� ) '/Error/Code' #ERR-CDE '//Error Description' #TWRL5001.#ERR-DESC ( #ERR-CDE )
                		ldaTwrl5951.getTwrl5951_Pnd_Tin(), new IdenticalSuppress(true),"/RRD/Hold",
                		pnd_Rrd_Hold_Ind, new FieldAttributes("LC=���"),"/DTF/Reject",
                		pnd_Dtf_Reject_Ind, new FieldAttributes("LC=���"),"/Letter/Hold",
                		pnd_Letter_Hold, new FieldAttributes("LC=����"),"/Error/Code",
                		ldaTwrl5951.getTwrl5951_Pnd_Err_Cde(),"//Error Description",
                		ldaTwrl5001.getPnd_Twrl5001_Pnd_Err_Desc().getValue(ldaTwrl5951.getTwrl5951_Pnd_Err_Cde().getInt() + 1));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_SYSHOLD"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_SYSHOLD"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*      '/Contract/Number'     #CONTRACT-NBR(IS=ON)
                //*      '//Payee'              #PAYEE-CDE (IS=ON LC=��)
                //*      '/IRS/Reportable'      #IRS-REPORTABLE (LC=����)
            }                                                                                                                                                             //Natural: END-IF
            //*                                                 (EM=N/Y LC=���)
            if (condition(ldaTwrl5951.getTwrl5951_Pnd_Form_Type().notEquals(10)))                                                                                         //Natural: IF #FORM-TYPE NE 10
            {
                pnd_Err_Cnt.getValue(ldaTwrl5951.getTwrl5951_Pnd_Err_Cde().getInt() + 1).nadd(1);                                                                         //Natural: ADD 1 TO #ERR-CNT ( #ERR-CDE )
                //*  TOTAL
                pnd_Err_Cnt.getValue(0 + 1).nadd(1);                                                                                                                      //Natural: ADD 1 TO #ERR-CNT ( 0 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Err_Cnt_T10.getValue(ldaTwrl5951.getTwrl5951_Pnd_Err_Cde().getInt() + 1).nadd(1);                                                                     //Natural: ADD 1 TO #ERR-CNT-T10 ( #ERR-CDE )
                //*  TOTAL
                pnd_Err_Cnt_T10.getValue(0 + 1).nadd(1);                                                                                                                  //Natural: ADD 1 TO #ERR-CNT-T10 ( 0 )
            }                                                                                                                                                             //Natural: END-IF
            getSort().writeSortInData(ldaTwrl5951.getTwrl5951_Pnd_Comp_Short_Name(), ldaTwrl5951.getTwrl5951_Pnd_Form_Type(), ldaTwrl5951.getTwrl5951_Pnd_Err_Cde(),      //Natural: END-ALL
                ldaTwrl5951.getTwrl5951_Pnd_Tax_Year(), ldaTwrl5951.getTwrl5951_Pnd_Tin(), ldaTwrl5951.getTwrl5951_Pnd_Contract_Nbr(), ldaTwrl5951.getTwrl5951_Pnd_Payee_Cde(), 
                ldaTwrl5951.getTwrl5951_Pnd_Moore_Mail(), ldaTwrl5951.getTwrl5951_Pnd_Vol_Printable(), ldaTwrl5951.getTwrl5951_Pnd_Irs_Reportable(), pnd_Rrd_Hold_Ind, 
                pnd_Dtf_Reject_Ind, pnd_Letter_Hold);
            rEAD_SYSHOLDPnd_Form_TypeOld.setValue(ldaTwrl5951.getTwrl5951_Pnd_Form_Type());                                                                               //Natural: END-WORK
        }
        READ_SYSHOLD_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventRead_Syshold(endOfDataReadSyshold);
        }
        if (Global.isEscape()) return;
        //*  01/30/2008 - AY
        getSort().sortData(ldaTwrl5951.getTwrl5951_Pnd_Comp_Short_Name(), ldaTwrl5951.getTwrl5951_Pnd_Form_Type(), ldaTwrl5951.getTwrl5951_Pnd_Err_Cde(),                 //Natural: SORT BY #COMP-SHORT-NAME #FORM-TYPE #ERR-CDE TWRL5951.#TAX-YEAR #TIN #CONTRACT-NBR #PAYEE-CDE USING #MOORE-MAIL #VOL-PRINTABLE #IRS-REPORTABLE #RRD-HOLD-IND #DTF-REJECT-IND #LETTER-HOLD
            ldaTwrl5951.getTwrl5951_Pnd_Tax_Year(), ldaTwrl5951.getTwrl5951_Pnd_Tin(), ldaTwrl5951.getTwrl5951_Pnd_Contract_Nbr(), ldaTwrl5951.getTwrl5951_Pnd_Payee_Cde());
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(ldaTwrl5951.getTwrl5951_Pnd_Comp_Short_Name(), ldaTwrl5951.getTwrl5951_Pnd_Form_Type(), ldaTwrl5951.getTwrl5951_Pnd_Err_Cde(), 
            ldaTwrl5951.getTwrl5951_Pnd_Tax_Year(), ldaTwrl5951.getTwrl5951_Pnd_Tin(), ldaTwrl5951.getTwrl5951_Pnd_Contract_Nbr(), ldaTwrl5951.getTwrl5951_Pnd_Payee_Cde(), 
            ldaTwrl5951.getTwrl5951_Pnd_Moore_Mail(), ldaTwrl5951.getTwrl5951_Pnd_Vol_Printable(), ldaTwrl5951.getTwrl5951_Pnd_Irs_Reportable(), pnd_Rrd_Hold_Ind, 
            pnd_Dtf_Reject_Ind, pnd_Letter_Hold)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            ldaTwrl5951.getTwrl5951_Pnd_Comp_Short_Name().setValue("    ");                                                                                               //Natural: ASSIGN #COMP-SHORT-NAME := '    '
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: BEFORE BREAK PROCESSING
            pnd_Key.setValue(ldaTwrl5951.getTwrl5951_Pnd_Tin());                                                                                                          //Natural: ASSIGN #KEY := #TIN
            setValueToSubstring(ldaTwrl5951.getTwrl5951_Pnd_Contract_Nbr(),pnd_Key,11,8);                                                                                 //Natural: MOVE #CONTRACT-NBR TO SUBSTR ( #KEY,11,8 )
            setValueToSubstring(ldaTwrl5951.getTwrl5951_Pnd_Payee_Cde(),pnd_Key,19,2);                                                                                    //Natural: MOVE #PAYEE-CDE TO SUBSTR ( #KEY,19,2 )
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //*                                                                                                                                                           //Natural: AT BREAK OF #KEY
            //*                                                                                                                                                           //Natural: AT BREAK #ERR-CDE
            //*                                                                                                                                                           //Natural: AT BREAK #FORM-TYPE
            //*                                                                                                                                                           //Natural: AT BREAK #COMP-SHORT-NAME
            if (condition(ldaTwrl5951.getTwrl5951_Pnd_Form_Type().notEquals(10)))                                                                                         //Natural: IF #FORM-TYPE NE 10
            {
                //*  TOTAL
                pnd_Err_Cnt.getValue(0 + 1).nadd(1);                                                                                                                      //Natural: ADD 1 TO #ERR-CNT ( 0 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  TOTAL
                pnd_Err_Cnt_T10.getValue(0 + 1).nadd(1);                                                                                                                  //Natural: ADD 1 TO #ERR-CNT-T10 ( 0 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl5951.getTwrl5951_Pnd_Form_Type().notEquals(10)))                                                                                         //Natural: IF #FORM-TYPE NE 10
            {
                getReports().display(2, "/Tax ID/Number",                                                                                                                 //Natural: DISPLAY ( 2 ) '/Tax ID/Number' #TIN ( IS = ON ) '/Contract/Number' #CONTRACT-NBR ( IS = ON ) '//Payee' #PAYEE-CDE ( IS = ON LC = �� ) '/Moore/Mailable' #MOORE-MAIL ( LC = ��� ) '/In-House/Mailable' #IN-HOUSE-MAIL ( LC = ��� ) '/Volume/Printable' #VOL-PRINTABLE ( LC = ���� ) '/IRS/Reportable' #IRS-REPORTABLE ( LC = ���� ) '/Error/Code' #ERR-CDE '//Error Description' #TWRL5001.#ERR-DESC ( #ERR-CDE )
                		ldaTwrl5951.getTwrl5951_Pnd_Tin(), new IdenticalSuppress(true),"/Contract/Number",
                		ldaTwrl5951.getTwrl5951_Pnd_Contract_Nbr(), new IdenticalSuppress(true),"//Payee",
                		ldaTwrl5951.getTwrl5951_Pnd_Payee_Cde(), new IdenticalSuppress(true), new FieldAttributes("LC=��"),"/Moore/Mailable",
                		ldaTwrl5951.getTwrl5951_Pnd_Moore_Mail(), new FieldAttributes("LC=���"),"/In-House/Mailable",
                		ldaTwrl5951.getTwrl5951_Pnd_In_House_Mail(), new FieldAttributes("LC=���"),"/Volume/Printable",
                		ldaTwrl5951.getTwrl5951_Pnd_Vol_Printable(), new FieldAttributes("LC=����"),"/IRS/Reportable",
                		ldaTwrl5951.getTwrl5951_Pnd_Irs_Reportable(), new FieldAttributes("LC=����"),"/Error/Code",
                		ldaTwrl5951.getTwrl5951_Pnd_Err_Cde(),"//Error Description",
                		ldaTwrl5001.getPnd_Twrl5001_Pnd_Err_Desc().getValue(ldaTwrl5951.getTwrl5951_Pnd_Err_Cde().getInt() + 1));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    'Moore/Mailable/Error' #TWRL5001.#MOORE-HOLD (#ERR-CDE)
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().display(5, "/Tax ID/Number",                                                                                                                 //Natural: DISPLAY ( 5 ) '/Tax ID/Number' #TIN ( IS = ON ) '/RRD/Hold' #RRD-HOLD-IND ( LC = ��� ) '/DTF/Reject' #DTF-REJECT-IND ( LC = ��� ) '/Letter/Hold' #LETTER-HOLD ( LC = ���� ) '/Error/Code' #ERR-CDE '//Error Description' #TWRL5001.#ERR-DESC ( #ERR-CDE )
                		ldaTwrl5951.getTwrl5951_Pnd_Tin(), new IdenticalSuppress(true),"/RRD/Hold",
                		pnd_Rrd_Hold_Ind, new FieldAttributes("LC=���"),"/DTF/Reject",
                		pnd_Dtf_Reject_Ind, new FieldAttributes("LC=���"),"/Letter/Hold",
                		pnd_Letter_Hold, new FieldAttributes("LC=����"),"/Error/Code",
                		ldaTwrl5951.getTwrl5951_Pnd_Err_Cde(),"//Error Description",
                		ldaTwrl5001.getPnd_Twrl5001_Pnd_Err_Desc().getValue(ldaTwrl5951.getTwrl5951_Pnd_Err_Cde().getInt() + 1));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*       '/Contract/Number'     #CONTRACT-NBR(IS=ON)
                //*       '//Payee'              #PAYEE-CDE (IS=ON LC=��)
                //*    '/IRS/Reportable'      #IRS-REPORTABLE (LC=����)
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
    }

    //

    // Support Methods

    private void atBreakEventRead_Syshold() throws Exception {atBreakEventRead_Syshold(false);}
    private void atBreakEventRead_Syshold(boolean endOfData) throws Exception
    {
        boolean pnd_KeyIsBreak = pnd_Key.isBreak(endOfData);
        boolean ldaTwrl5951_getTwrl5951_Pnd_Form_TypeIsBreak = ldaTwrl5951.getTwrl5951_Pnd_Form_Type().isBreak(endOfData);
        boolean ldaTwrl5951_getTwrl5951_Pnd_Comp_Short_NameIsBreak = ldaTwrl5951.getTwrl5951_Pnd_Comp_Short_Name().isBreak(endOfData);
        if (condition(pnd_KeyIsBreak || ldaTwrl5951_getTwrl5951_Pnd_Form_TypeIsBreak || ldaTwrl5951_getTwrl5951_Pnd_Comp_Short_NameIsBreak))
        {
            Global.format("IS=OFF");                                                                                                                                      //Natural: SUSPEND IDENTICAL SUPPRESS ( 3 )
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaTwrl5951_getTwrl5951_Pnd_Form_TypeIsBreak || ldaTwrl5951_getTwrl5951_Pnd_Comp_Short_NameIsBreak))
        {
            pnd_Old_Form_Type.setValue(rEAD_SYSHOLDPnd_Form_TypeOld);                                                                                                     //Natural: ASSIGN #OLD-FORM-TYPE := OLD ( #FORM-TYPE )
            if (condition(ldaTwrl5951.getTwrl5951_Pnd_Form_Type().notEquals(10)))                                                                                         //Natural: IF #FORM-TYPE NE 10
            {
                getReports().newPage(new ReportSpecification(4));                                                                                                         //Natural: NEWPAGE ( 4 )
                if (condition(Global.isEscape())){return;}
                getReports().getPageNumberDbs(3).nadd(1);                                                                                                                 //Natural: ADD 1 TO *PAGE-NUMBER ( 3 )
                FOR01:                                                                                                                                                    //Natural: FOR #NDX = 1 TO #TWRL5001-MAX
                for (pnd_Ndx.setValue(1); condition(pnd_Ndx.lessOrEqual(ldaTwrl5001.getPnd_Twrl5001_Max())); pnd_Ndx.nadd(1))
                {
                    if (condition(pnd_Err_Cnt.getValue(pnd_Ndx.getInt() + 1).greater(getZero())))                                                                         //Natural: IF #ERR-CNT ( #NDX ) GT 0
                    {
                        getReports().display(4, "Error Code",                                                                                                             //Natural: DISPLAY ( 4 ) 'Error Code' #NDX 'Error Description' #TWRL5001.#ERR-DESC ( #NDX ) 'Total' #ERR-CNT ( #NDX ) ( EM = Z,ZZZ,ZZ9 )
                        		pnd_Ndx,"Error Description",
                        		ldaTwrl5001.getPnd_Twrl5001_Pnd_Err_Desc().getValue(pnd_Ndx.getInt() + 1),"Total",
                        		pnd_Err_Cnt.getValue(pnd_Ndx.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZ9"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape())) return;
                getReports().write(4, NEWLINE,NEWLINE,new ReportTAsterisk(pnd_Ndx),"Total",new ReportTAsterisk(pnd_Err_Cnt),pnd_Err_Cnt.getValue(0 + 1),                  //Natural: WRITE ( 4 ) // T*#NDX 'Total' T*#ERR-CNT #ERR-CNT ( 0 ) ( EM = Z,ZZZ,ZZ9 )
                    new ReportEditMask ("Z,ZZZ,ZZ9"));
                if (condition(Global.isEscape())) return;
                pnd_Err_Cnt.getValue("*").reset();                                                                                                                        //Natural: RESET #ERR-CNT ( * )
                getReports().newPage(new ReportSpecification(3));                                                                                                         //Natural: NEWPAGE ( 3 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().newPage(new ReportSpecification(7));                                                                                                         //Natural: NEWPAGE ( 7 )
                if (condition(Global.isEscape())){return;}
                getReports().getPageNumberDbs(6).nadd(1);                                                                                                                 //Natural: ADD 1 TO *PAGE-NUMBER ( 6 )
                FOR02:                                                                                                                                                    //Natural: FOR #NDX = 1 TO #TWRL5001-MAX
                for (pnd_Ndx.setValue(1); condition(pnd_Ndx.lessOrEqual(ldaTwrl5001.getPnd_Twrl5001_Max())); pnd_Ndx.nadd(1))
                {
                    if (condition(pnd_Err_Cnt_T10.getValue(pnd_Ndx.getInt() + 1).greater(getZero())))                                                                     //Natural: IF #ERR-CNT-T10 ( #NDX ) GT 0
                    {
                        getReports().display(7, "Error Code",                                                                                                             //Natural: DISPLAY ( 7 ) 'Error Code' #NDX 'Error Description' #TWRL5001.#ERR-DESC ( #NDX ) 'Total' #ERR-CNT-T10 ( #NDX ) ( EM = Z,ZZZ,ZZ9 )
                        		pnd_Ndx,"Error Description",
                        		ldaTwrl5001.getPnd_Twrl5001_Pnd_Err_Desc().getValue(pnd_Ndx.getInt() + 1),"Total",
                        		pnd_Err_Cnt_T10.getValue(pnd_Ndx.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZ9"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape())) return;
                getReports().write(7, NEWLINE,NEWLINE,new ReportTAsterisk(pnd_Ndx),"Total",new ReportTAsterisk(pnd_Err_Cnt_T10),pnd_Err_Cnt_T10.getValue(0 + 1),          //Natural: WRITE ( 7 ) // T*#NDX 'Total' T*#ERR-CNT-T10 #ERR-CNT-T10 ( 0 ) ( EM = Z,ZZZ,ZZ9 )
                    new ReportEditMask ("Z,ZZZ,ZZ9"));
                if (condition(Global.isEscape())) return;
                pnd_Err_Cnt_T10.getValue("*").reset();                                                                                                                    //Natural: RESET #ERR-CNT-T10 ( * )
                getReports().newPage(new ReportSpecification(6));                                                                                                         //Natural: NEWPAGE ( 6 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaTwrl5951_getTwrl5951_Pnd_Comp_Short_NameIsBreak))
        {
            pnd_Old_Comp_Short_Name.setValue(ldaTwrl5951.getTwrl5951_Pnd_Comp_Short_Name());                                                                              //Natural: ASSIGN #OLD-COMP-SHORT-NAME := #COMP-SHORT-NAME
        }                                                                                                                                                                 //Natural: END-BREAK
    }

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_KeyIsBreak = pnd_Key.isBreak(endOfData);
        boolean ldaTwrl5951_getTwrl5951_Pnd_Err_CdeIsBreak = ldaTwrl5951.getTwrl5951_Pnd_Err_Cde().isBreak(endOfData);
        boolean ldaTwrl5951_getTwrl5951_Pnd_Form_TypeIsBreak = ldaTwrl5951.getTwrl5951_Pnd_Form_Type().isBreak(endOfData);
        boolean ldaTwrl5951_getTwrl5951_Pnd_Comp_Short_NameIsBreak = ldaTwrl5951.getTwrl5951_Pnd_Comp_Short_Name().isBreak(endOfData);
        if (condition(pnd_KeyIsBreak || ldaTwrl5951_getTwrl5951_Pnd_Err_CdeIsBreak || ldaTwrl5951_getTwrl5951_Pnd_Form_TypeIsBreak || ldaTwrl5951_getTwrl5951_Pnd_Comp_Short_NameIsBreak))
        {
            Global.format("IS=OFF");                                                                                                                                      //Natural: SUSPEND IDENTICAL SUPPRESS ( 2 )
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaTwrl5951_getTwrl5951_Pnd_Err_CdeIsBreak || ldaTwrl5951_getTwrl5951_Pnd_Form_TypeIsBreak || ldaTwrl5951_getTwrl5951_Pnd_Comp_Short_NameIsBreak))
        {
            if (condition(ldaTwrl5951.getTwrl5951_Pnd_Form_Type().notEquals(10)))                                                                                         //Natural: IF #FORM-TYPE NE 10
            {
                getReports().write(2, NEWLINE,NEWLINE,"Total",new ColumnSpacing(10),pnd_Err_Cnt.getValue(0 + 1), new ReportEditMask ("Z,ZZZ,ZZ9"));                       //Natural: WRITE ( 2 ) // 'Total' 10X #ERR-CNT ( 0 ) ( EM = Z,ZZZ,ZZ9 )
                if (condition(Global.isEscape())) return;
                pnd_Err_Cnt.getValue("*").reset();                                                                                                                        //Natural: RESET #ERR-CNT ( * )
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 2 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Err_Cnt_T10.getValue(0 + 1).greater(getZero())))                                                                                        //Natural: IF #ERR-CNT-T10 ( 0 ) > 0
                {
                    getReports().write(5, NEWLINE,NEWLINE,"Total",new ColumnSpacing(10),pnd_Err_Cnt_T10.getValue(0 + 1), new ReportEditMask ("Z,ZZZ,ZZ9"));               //Natural: WRITE ( 5 ) // 'Total' 10X #ERR-CNT-T10 ( 0 ) ( EM = Z,ZZZ,ZZ9 )
                    if (condition(Global.isEscape())) return;
                    pnd_Err_Cnt_T10.getValue("*").reset();                                                                                                                //Natural: RESET #ERR-CNT-T10 ( * )
                    getReports().newPage(new ReportSpecification(5));                                                                                                     //Natural: NEWPAGE ( 5 )
                    if (condition(Global.isEscape())){return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaTwrl5951_getTwrl5951_Pnd_Form_TypeIsBreak || ldaTwrl5951_getTwrl5951_Pnd_Comp_Short_NameIsBreak))
        {
            ignore();
            //*    #OLD-FORM-TYPE := OLD(#FORM-TYPE)
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaTwrl5951_getTwrl5951_Pnd_Comp_Short_NameIsBreak))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=133 ZP=ON");
        Global.format(2, "PS=58 LS=133 ZP=ON SF=2");
        Global.format(3, "PS=58 LS=133 ZP=ON SF=2");
        Global.format(4, "PS=58 LS=133 ZP=ON SF=3");
        Global.format(5, "PS=58 LS=133 ZP=ON SF=3");
        Global.format(6, "PS=58 LS=133 ZP=ON SF=3");
        Global.format(7, "PS=58 LS=133 ZP=ON SF=3");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(22),"Tax Withholding and Reporting System",new TabSetting(68),"Page:",getReports().getPageNumberDbs(1), 
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(34),"Control Report",new TabSetting(68),"Report: RPT1",
            NEWLINE,NEWLINE,"Tax Year: ",ldaTwrl5951.getTwrl5951_Pnd_Tax_Year(),NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(2), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(52),"System Hold Report by Hold Code",new TabSetting(110),"Report: RPT2",NEWLINE,new 
            TabSetting(59),"Tax Year",ldaTwrl5951.getTwrl5951_Pnd_Tax_Year(),NEWLINE,NEWLINE,"Company  : ",ldaTwrl5951.getTwrl5951_Pnd_Comp_Short_Name(),
            NEWLINE,"Form Type: ",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,NEWLINE);
        getReports().write(5, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(5), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(52),"SUNY Hold Report by Hold Code",new TabSetting(110),"Report: RPT5",NEWLINE,new 
            TabSetting(59),"Tax Year",ldaTwrl5951.getTwrl5951_Pnd_Tax_Year(),NEWLINE,NEWLINE,"Form Type: ",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,
            NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(3), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(50),"System Hold Report by Participant",new 
            TabSetting(110),"Report: RPT3",NEWLINE,new TabSetting(59),"Tax Year",ldaTwrl5951.getTwrl5951_Pnd_Tax_Year(),NEWLINE,NEWLINE,"Company  : ",ldaTwrl5951.getTwrl5951_Pnd_Comp_Short_Name(),
            NEWLINE,"Form Type: ",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,NEWLINE);
        getReports().write(6, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(6), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(50),"SUNY Hold Report by Participant",new TabSetting(110),"Report: RPT6",NEWLINE,new 
            TabSetting(59),"Tax Year",ldaTwrl5951.getTwrl5951_Pnd_Tax_Year(),NEWLINE,NEWLINE,"Form Type: ",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,
            NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(4), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(50),"System Hold Report by Participant",new 
            TabSetting(110),"Report: RPT4",NEWLINE,new TabSetting(59),"Tax Year",ldaTwrl5951.getTwrl5951_Pnd_Tax_Year(),NEWLINE,NEWLINE,"Company  : ",pnd_Old_Comp_Short_Name,
            NEWLINE,"Form Type: ",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,NEWLINE);
        getReports().write(7, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(7), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(50),"SUNY Hold Report by Participant",new TabSetting(110),"Report: RPT7",NEWLINE,new 
            TabSetting(59),"Tax Year",ldaTwrl5951.getTwrl5951_Pnd_Tax_Year(),NEWLINE,NEWLINE,"Company  : ",ldaTwrl5951.getTwrl5951_Pnd_Comp_Short_Name(),
            NEWLINE,"Form Type: ",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,NEWLINE);

        getReports().setDisplayColumns(4, "Error Code",
        		pnd_Ndx,"Error Description",
        		ldaTwrl5001.getPnd_Twrl5001_Pnd_Err_Desc(),"Total",
        		pnd_Err_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"));
        getReports().setDisplayColumns(7, "Error Code",
        		pnd_Ndx,"Error Description",
        		ldaTwrl5001.getPnd_Twrl5001_Pnd_Err_Desc(),"Total",
        		pnd_Err_Cnt_T10, new ReportEditMask ("Z,ZZZ,ZZ9"));
        getReports().setDisplayColumns(3, "/Tax ID/Number",
        		ldaTwrl5951.getTwrl5951_Pnd_Tin(), new IdenticalSuppress(true),"/Contract/Number",
        		ldaTwrl5951.getTwrl5951_Pnd_Contract_Nbr(), new IdenticalSuppress(true),"//Payee",
        		ldaTwrl5951.getTwrl5951_Pnd_Payee_Cde(), new IdenticalSuppress(true), new FieldAttributes("LC=��"),"/Moore/Mailable",
        		ldaTwrl5951.getTwrl5951_Pnd_Moore_Mail(), new FieldAttributes("LC=���"),"/In-House/Mailable",
        		ldaTwrl5951.getTwrl5951_Pnd_In_House_Mail(), new FieldAttributes("LC=���"),"/Volume/Printable",
        		ldaTwrl5951.getTwrl5951_Pnd_Vol_Printable(), new FieldAttributes("LC=����"),"/IRS/Reportable",
        		ldaTwrl5951.getTwrl5951_Pnd_Irs_Reportable(), new FieldAttributes("LC=����"),"/Error/Code",
        		ldaTwrl5951.getTwrl5951_Pnd_Err_Cde(),"//Error Description",
        		ldaTwrl5001.getPnd_Twrl5001_Pnd_Err_Desc());
        getReports().setDisplayColumns(6, "/Tax ID/Number",
        		ldaTwrl5951.getTwrl5951_Pnd_Tin(), new IdenticalSuppress(true),"/RRD/Hold",
        		pnd_Rrd_Hold_Ind, new FieldAttributes("LC=���"),"/DTF/Reject",
        		pnd_Dtf_Reject_Ind, new FieldAttributes("LC=���"),"/Letter/Hold",
        		pnd_Letter_Hold, new FieldAttributes("LC=����"),"/Error/Code",
        		ldaTwrl5951.getTwrl5951_Pnd_Err_Cde(),"//Error Description",
        		ldaTwrl5001.getPnd_Twrl5001_Pnd_Err_Desc());
        getReports().setDisplayColumns(2, "/Tax ID/Number",
        		ldaTwrl5951.getTwrl5951_Pnd_Tin(), new IdenticalSuppress(true),"/Contract/Number",
        		ldaTwrl5951.getTwrl5951_Pnd_Contract_Nbr(), new IdenticalSuppress(true),"//Payee",
        		ldaTwrl5951.getTwrl5951_Pnd_Payee_Cde(), new IdenticalSuppress(true), new FieldAttributes("LC=��"),"/Moore/Mailable",
        		ldaTwrl5951.getTwrl5951_Pnd_Moore_Mail(), new FieldAttributes("LC=���"),"/In-House/Mailable",
        		ldaTwrl5951.getTwrl5951_Pnd_In_House_Mail(), new FieldAttributes("LC=���"),"/Volume/Printable",
        		ldaTwrl5951.getTwrl5951_Pnd_Vol_Printable(), new FieldAttributes("LC=����"),"/IRS/Reportable",
        		ldaTwrl5951.getTwrl5951_Pnd_Irs_Reportable(), new FieldAttributes("LC=����"),"/Error/Code",
        		ldaTwrl5951.getTwrl5951_Pnd_Err_Cde(),"//Error Description",
        		ldaTwrl5001.getPnd_Twrl5001_Pnd_Err_Desc());
        getReports().setDisplayColumns(5, "/Tax ID/Number",
        		ldaTwrl5951.getTwrl5951_Pnd_Tin(), new IdenticalSuppress(true),"/RRD/Hold",
        		pnd_Rrd_Hold_Ind, new FieldAttributes("LC=���"),"/DTF/Reject",
        		pnd_Dtf_Reject_Ind, new FieldAttributes("LC=���"),"/Letter/Hold",
        		pnd_Letter_Hold, new FieldAttributes("LC=����"),"/Error/Code",
        		ldaTwrl5951.getTwrl5951_Pnd_Err_Cde(),"//Error Description",
        		ldaTwrl5001.getPnd_Twrl5001_Pnd_Err_Desc());
    }
    private void CheckAtStartofData171() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Old_Comp_Short_Name.setValue(ldaTwrl5951.getTwrl5951_Pnd_Comp_Short_Name());                                                                              //Natural: ASSIGN #OLD-COMP-SHORT-NAME := #COMP-SHORT-NAME
            pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Tax_Year()), ldaTwrl5951.getTwrl5951_Pnd_Tax_Year().val()); //Natural: ASSIGN #TWRAFRMN.#TAX-YEAR := VAL ( TWRL5951.#TAX-YEAR )
            //*  01/30/2008 - AY
            DbsUtil.callnat(Twrnfrmn.class , getCurrentProcessState(), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNFRMN' USING #TWRAFRMN.#INPUT-PARMS ( AD = O ) #TWRAFRMN.#OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-START
    }
}
