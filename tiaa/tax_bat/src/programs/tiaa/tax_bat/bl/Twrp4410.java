/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:39:13 PM
**        * FROM NATURAL PROGRAM : Twrp4410
************************************************************
**        * FILE NAME            : Twrp4410.java
**        * CLASS NAME           : Twrp4410
**        * INSTANCE NAME        : Twrp4410
************************************************************
************************************************************************
* PROGRAM  : TWRP4410
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : CREATES BATCH EXTRACT OF ALL "5498" FROM THE FORM FILE.
* CREATED  : 09 / 23 / 1999.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM READS THE FORM DATA BASE FILE TO PRODUCE AN
*            EXTRACT OF ALL 5498 FORM RECORDS FOR IRS ORIGINAL
*            REPORTING.
* HISTORY
* ------------------------------------------
* 05/08/2001 JH  TWRL4001 CHANGED: EMBEDDED ALPHAS IN CONTRACT NBR
* 05/25/2005 BK  TWRL441A CHANGED: TIRF-SEP-AMT ADDED
*
* 11/29/06  J.ROTHOLZ
*           REMOVE LOGIC TO DETERMINE IRA TYPE BY CONTRACT RANGE AND
*           INSTEAD TAKE IRA TYPE FROM DATA STORED ON FORM FILE
* 04/09/07  J.ROTHOLZ
*           INSERT PARM LOGIC TO USE TAX YEAR FROM PARM OR DEFAULT
*           TO USE CURRENT YEAR MINUS 1
* 02/09/17  STOW FOR TWRL4001 NEW ONEIRA CONTRACT RANGES
* 12/03/17  DASDH STOW FOR NEW FIELDS ADDITION IN FORM FILE
** 11/24/2020 - PALDE - RESTOW FOR IRS REPORTING 2020 CHANGES
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp4410 extends BLNatBase
{
    // Data Areas
    private LdaTwrl441a ldaTwrl441a;
    private LdaTwrl4001 ldaTwrl4001;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input_Parm;
    private DbsField pnd_Input_Parm_Pnd_Form;
    private DbsField pnd_Input_Parm_Pnd_Tax_Year;
    private DbsField pnd_In_Tax_Year;

    private DbsGroup pnd_In_Tax_Year__R_Field_1;
    private DbsField pnd_In_Tax_Year_Pnd_In_Tax_Year_A;

    private DataAccessProgramView vw_cntl;
    private DbsField cntl_Tircntl_Tbl_Nbr;
    private DbsField cntl_Tircntl_Tax_Year;
    private DbsField cntl_Tircntl_Rpt_Form_Name;
    private DbsField cntl_Count_Casttircntl_Rpt_Tbl_Pe;

    private DbsGroup cntl_Tircntl_Rpt_Tbl_Pe;
    private DbsField cntl_Tircntl_Rpt_Dte;
    private DbsField pnd_Tirf_Superde_3;

    private DbsGroup pnd_Tirf_Superde_3__R_Field_2;
    private DbsField pnd_Tirf_Superde_3_Pnd_S3_Tax_Year;
    private DbsField pnd_Tirf_Superde_3_Pnd_S3_Active_Ind;
    private DbsField pnd_Tirf_Superde_3_Pnd_S3_Form_Type;
    private DbsField pnd_Super_Cntl;

    private DbsGroup pnd_Super_Cntl__R_Field_3;
    private DbsField pnd_Super_Cntl_Pnd_S_Tbl;
    private DbsField pnd_Super_Cntl_Pnd_S_Tax_Year;

    private DbsGroup pnd_Super_Cntl__R_Field_4;
    private DbsField pnd_Super_Cntl_Pnd_S_Tax_Year_N;
    private DbsField pnd_Super_Cntl_Pnd_S_Form;
    private DbsField pnd_Todays_Yyyymmdd;

    private DbsGroup pnd_Todays_Yyyymmdd__R_Field_5;
    private DbsField pnd_Todays_Yyyymmdd_Pnd_Todays_Yyyymmdd_N;

    private DbsGroup pnd_Todays_Yyyymmdd__R_Field_6;
    private DbsField pnd_Todays_Yyyymmdd_Pnd_Todays_Yyyy_N;
    private DbsField pnd_Todays_Yyyymmdd_Pnd_Todays_Mm_N;
    private DbsField pnd_5498_Type;

    private DbsGroup pnd_5498_Type__R_Field_7;
    private DbsField pnd_5498_Type_Pnd_5498_Type_N;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Ok_Ctr;
    private DbsField pnd_Empty_Ctr;
    private DbsField pnd_Reject_Ctr;
    private DbsField pnd_No_Contract_Ctr;
    private DbsField i;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl441a = new LdaTwrl441a();
        registerRecord(ldaTwrl441a);
        registerRecord(ldaTwrl441a.getVw_form());
        ldaTwrl4001 = new LdaTwrl4001();
        registerRecord(ldaTwrl4001);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Input_Parm = localVariables.newGroupInRecord("pnd_Input_Parm", "#INPUT-PARM");
        pnd_Input_Parm_Pnd_Form = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Form", "#FORM", FieldType.STRING, 6);
        pnd_Input_Parm_Pnd_Tax_Year = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_In_Tax_Year = localVariables.newFieldInRecord("pnd_In_Tax_Year", "#IN-TAX-YEAR", FieldType.NUMERIC, 4);

        pnd_In_Tax_Year__R_Field_1 = localVariables.newGroupInRecord("pnd_In_Tax_Year__R_Field_1", "REDEFINE", pnd_In_Tax_Year);
        pnd_In_Tax_Year_Pnd_In_Tax_Year_A = pnd_In_Tax_Year__R_Field_1.newFieldInGroup("pnd_In_Tax_Year_Pnd_In_Tax_Year_A", "#IN-TAX-YEAR-A", FieldType.STRING, 
            4);

        vw_cntl = new DataAccessProgramView(new NameInfo("vw_cntl", "CNTL"), "TIRCNTL_REPORTING_TBL_VIEW", "TIR_CONTROL", DdmPeriodicGroups.getInstance().getGroups("TIRCNTL_REPORTING_TBL_VIEW"));
        cntl_Tircntl_Tbl_Nbr = vw_cntl.getRecord().newFieldInGroup("cntl_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        cntl_Tircntl_Tax_Year = vw_cntl.getRecord().newFieldInGroup("cntl_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        cntl_Tircntl_Rpt_Form_Name = vw_cntl.getRecord().newFieldInGroup("cntl_Tircntl_Rpt_Form_Name", "TIRCNTL-RPT-FORM-NAME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TIRCNTL_RPT_FORM_NAME");
        cntl_Count_Casttircntl_Rpt_Tbl_Pe = vw_cntl.getRecord().newFieldInGroup("cntl_Count_Casttircntl_Rpt_Tbl_Pe", "C*TIRCNTL-RPT-TBL-PE", RepeatingFieldStrategy.CAsteriskVariable, 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");

        cntl_Tircntl_Rpt_Tbl_Pe = vw_cntl.getRecord().newGroupInGroup("cntl_Tircntl_Rpt_Tbl_Pe", "TIRCNTL-RPT-TBL-PE", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        cntl_Tircntl_Rpt_Dte = cntl_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("cntl_Tircntl_Rpt_Dte", "TIRCNTL-RPT-DTE", FieldType.DATE, new DbsArrayController(1, 
            12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_DTE", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        cntl_Tircntl_Rpt_Dte.setDdmHeader("REPOR-/TING/DATE");
        registerRecord(vw_cntl);

        pnd_Tirf_Superde_3 = localVariables.newFieldInRecord("pnd_Tirf_Superde_3", "#TIRF-SUPERDE-3", FieldType.STRING, 7);

        pnd_Tirf_Superde_3__R_Field_2 = localVariables.newGroupInRecord("pnd_Tirf_Superde_3__R_Field_2", "REDEFINE", pnd_Tirf_Superde_3);
        pnd_Tirf_Superde_3_Pnd_S3_Tax_Year = pnd_Tirf_Superde_3__R_Field_2.newFieldInGroup("pnd_Tirf_Superde_3_Pnd_S3_Tax_Year", "#S3-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Tirf_Superde_3_Pnd_S3_Active_Ind = pnd_Tirf_Superde_3__R_Field_2.newFieldInGroup("pnd_Tirf_Superde_3_Pnd_S3_Active_Ind", "#S3-ACTIVE-IND", 
            FieldType.STRING, 1);
        pnd_Tirf_Superde_3_Pnd_S3_Form_Type = pnd_Tirf_Superde_3__R_Field_2.newFieldInGroup("pnd_Tirf_Superde_3_Pnd_S3_Form_Type", "#S3-FORM-TYPE", FieldType.NUMERIC, 
            2);
        pnd_Super_Cntl = localVariables.newFieldInRecord("pnd_Super_Cntl", "#SUPER-CNTL", FieldType.STRING, 12);

        pnd_Super_Cntl__R_Field_3 = localVariables.newGroupInRecord("pnd_Super_Cntl__R_Field_3", "REDEFINE", pnd_Super_Cntl);
        pnd_Super_Cntl_Pnd_S_Tbl = pnd_Super_Cntl__R_Field_3.newFieldInGroup("pnd_Super_Cntl_Pnd_S_Tbl", "#S-TBL", FieldType.NUMERIC, 1);
        pnd_Super_Cntl_Pnd_S_Tax_Year = pnd_Super_Cntl__R_Field_3.newFieldInGroup("pnd_Super_Cntl_Pnd_S_Tax_Year", "#S-TAX-YEAR", FieldType.STRING, 4);

        pnd_Super_Cntl__R_Field_4 = pnd_Super_Cntl__R_Field_3.newGroupInGroup("pnd_Super_Cntl__R_Field_4", "REDEFINE", pnd_Super_Cntl_Pnd_S_Tax_Year);
        pnd_Super_Cntl_Pnd_S_Tax_Year_N = pnd_Super_Cntl__R_Field_4.newFieldInGroup("pnd_Super_Cntl_Pnd_S_Tax_Year_N", "#S-TAX-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_Super_Cntl_Pnd_S_Form = pnd_Super_Cntl__R_Field_3.newFieldInGroup("pnd_Super_Cntl_Pnd_S_Form", "#S-FORM", FieldType.STRING, 7);
        pnd_Todays_Yyyymmdd = localVariables.newFieldInRecord("pnd_Todays_Yyyymmdd", "#TODAYS-YYYYMMDD", FieldType.STRING, 8);

        pnd_Todays_Yyyymmdd__R_Field_5 = localVariables.newGroupInRecord("pnd_Todays_Yyyymmdd__R_Field_5", "REDEFINE", pnd_Todays_Yyyymmdd);
        pnd_Todays_Yyyymmdd_Pnd_Todays_Yyyymmdd_N = pnd_Todays_Yyyymmdd__R_Field_5.newFieldInGroup("pnd_Todays_Yyyymmdd_Pnd_Todays_Yyyymmdd_N", "#TODAYS-YYYYMMDD-N", 
            FieldType.NUMERIC, 8);

        pnd_Todays_Yyyymmdd__R_Field_6 = localVariables.newGroupInRecord("pnd_Todays_Yyyymmdd__R_Field_6", "REDEFINE", pnd_Todays_Yyyymmdd);
        pnd_Todays_Yyyymmdd_Pnd_Todays_Yyyy_N = pnd_Todays_Yyyymmdd__R_Field_6.newFieldInGroup("pnd_Todays_Yyyymmdd_Pnd_Todays_Yyyy_N", "#TODAYS-YYYY-N", 
            FieldType.NUMERIC, 4);
        pnd_Todays_Yyyymmdd_Pnd_Todays_Mm_N = pnd_Todays_Yyyymmdd__R_Field_6.newFieldInGroup("pnd_Todays_Yyyymmdd_Pnd_Todays_Mm_N", "#TODAYS-MM-N", FieldType.NUMERIC, 
            2);
        pnd_5498_Type = localVariables.newFieldInRecord("pnd_5498_Type", "#5498-TYPE", FieldType.STRING, 2);

        pnd_5498_Type__R_Field_7 = localVariables.newGroupInRecord("pnd_5498_Type__R_Field_7", "REDEFINE", pnd_5498_Type);
        pnd_5498_Type_Pnd_5498_Type_N = pnd_5498_Type__R_Field_7.newFieldInGroup("pnd_5498_Type_Pnd_5498_Type_N", "#5498-TYPE-N", FieldType.NUMERIC, 2);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ok_Ctr = localVariables.newFieldInRecord("pnd_Ok_Ctr", "#OK-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Empty_Ctr = localVariables.newFieldInRecord("pnd_Empty_Ctr", "#EMPTY-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Reject_Ctr = localVariables.newFieldInRecord("pnd_Reject_Ctr", "#REJECT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_No_Contract_Ctr = localVariables.newFieldInRecord("pnd_No_Contract_Ctr", "#NO-CONTRACT-CTR", FieldType.PACKED_DECIMAL, 7);
        i = localVariables.newFieldInRecord("i", "I", FieldType.PACKED_DECIMAL, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cntl.reset();

        ldaTwrl441a.initializeValues();
        ldaTwrl4001.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp4410() throws Exception
    {
        super("Twrp4410");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp4410|Main");
        OnErrorManager.pushEvent("TWRP4410", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parm);                                                                                       //Natural: INPUT #INPUT-PARM
                getReports().display(0, pnd_Input_Parm_Pnd_Form,pnd_Input_Parm_Pnd_Tax_Year);                                                                             //Natural: DISPLAY #INPUT-PARM.#FORM #INPUT-PARM.#TAX-YEAR
                if (Global.isEscape()) return;
                if (condition(pnd_Input_Parm_Pnd_Tax_Year.equals(getZero())))                                                                                             //Natural: IF #INPUT-PARM.#TAX-YEAR = 0
                {
                    pnd_In_Tax_Year.compute(new ComputeParameters(false, pnd_In_Tax_Year), Global.getDATN().divide(10000).subtract(1));                                   //Natural: ASSIGN #IN-TAX-YEAR := *DATN / 10000 - 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_In_Tax_Year.setValue(pnd_Input_Parm_Pnd_Tax_Year);                                                                                                //Natural: ASSIGN #IN-TAX-YEAR := #INPUT-PARM.#TAX-YEAR
                }                                                                                                                                                         //Natural: END-IF
                //*  #TODAYS-YYYYMMDD-N  :=  *DATN
                //*  *
                //*  WRITE *APPLIC-ID
                //*  IF *APPLIC-ID NE 'PROJNTAX'
                //*    #S3-TAX-YEAR    :=  #TODAYS-YYYY-N  -  1
                //*  ELSE
                //*    #S3-TAX-YEAR    :=  #TODAYS-YYYY-N
                //*  END-IF
                pnd_Tirf_Superde_3_Pnd_S3_Tax_Year.setValue(pnd_In_Tax_Year);                                                                                             //Natural: ASSIGN #S3-TAX-YEAR := #IN-TAX-YEAR
                pnd_Tirf_Superde_3_Pnd_S3_Active_Ind.setValue("A");                                                                                                       //Natural: ASSIGN #S3-ACTIVE-IND := 'A'
                pnd_Tirf_Superde_3_Pnd_S3_Form_Type.setValue(4);                                                                                                          //Natural: ASSIGN #S3-FORM-TYPE := 04
                ldaTwrl441a.getVw_form().startDatabaseRead                                                                                                                //Natural: READ FORM WITH TIRF-SUPERDE-3 = #TIRF-SUPERDE-3
                (
                "RD1",
                new Wc[] { new Wc("TIRF_SUPERDE_3", ">=", pnd_Tirf_Superde_3, WcType.BY) },
                new Oc[] { new Oc("TIRF_SUPERDE_3", "ASC") }
                );
                RD1:
                while (condition(ldaTwrl441a.getVw_form().readNextRow("RD1")))
                {
                    if (condition(ldaTwrl441a.getForm_Tirf_Tax_Year().equals(pnd_Tirf_Superde_3_Pnd_S3_Tax_Year) && ldaTwrl441a.getForm_Tirf_Active_Ind().equals("A")     //Natural: IF TIRF-TAX-YEAR = #S3-TAX-YEAR AND TIRF-ACTIVE-IND = 'A' AND TIRF-FORM-TYPE = 04
                        && ldaTwrl441a.getForm_Tirf_Form_Type().equals(4)))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Read_Ctr.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #READ-CTR
                    //*  PERFORM  LOOKUP-IRA-TYPE
                    if (condition(ldaTwrl441a.getForm_Tirf_Ira_Acct_Type().greater(" ")))                                                                                 //Natural: IF TIRF-IRA-ACCT-TYPE > ' '
                    {
                        pnd_5498_Type_Pnd_5498_Type_N.compute(new ComputeParameters(false, pnd_5498_Type_Pnd_5498_Type_N), ldaTwrl441a.getForm_Tirf_Ira_Acct_Type().val().multiply(10)); //Natural: ASSIGN #5498-TYPE-N := VAL ( TIRF-IRA-ACCT-TYPE ) * 10
                    }                                                                                                                                                     //Natural: END-IF
                    //*  BK
                    if (condition(pnd_5498_Type.equals(" ")))                                                                                                             //Natural: IF #5498-TYPE = ' '
                    {
                        pnd_No_Contract_Ctr.nadd(1);                                                                                                                      //Natural: ADD 1 TO #NO-CONTRACT-CTR
                        getReports().write(0, pnd_No_Contract_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"),ldaTwrl441a.getForm_Tirf_Contract_Nbr(),pnd_Read_Ctr,                 //Natural: WRITE #NO-CONTRACT-CTR FORM.TIRF-CONTRACT-NBR #READ-CTR
                            new ReportEditMask ("Z,ZZZ,ZZ9"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                        //*  BK
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl441a.getForm_Tirf_Empty_Form().equals(true)))                                                                                    //Natural: IF TIRF-EMPTY-FORM = TRUE
                    {
                        pnd_Empty_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #EMPTY-CTR
                        getWorkFiles().write(2, false, pnd_5498_Type, ldaTwrl441a.getVw_form(), ldaTwrl441a.getVw_form().getAstISN("RD1"));                               //Natural: WRITE WORK FILE 02 #5498-TYPE FORM *ISN ( RD1. )
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl441a.getForm_Tirf_Irs_Reject_Ind().equals("Y")))                                                                                 //Natural: IF TIRF-IRS-REJECT-IND = 'Y'
                    {
                        pnd_Reject_Ctr.nadd(1);                                                                                                                           //Natural: ADD 1 TO #REJECT-CTR
                        getWorkFiles().write(3, false, pnd_5498_Type, ldaTwrl441a.getVw_form(), ldaTwrl441a.getVw_form().getAstISN("RD1"));                               //Natural: WRITE WORK FILE 03 #5498-TYPE FORM *ISN ( RD1. )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  BK
                        pnd_Ok_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #OK-CTR
                        getWorkFiles().write(1, false, pnd_5498_Type, ldaTwrl441a.getVw_form(), ldaTwrl441a.getVw_form().getAstISN("RD1"));                               //Natural: WRITE WORK FILE 01 #5498-TYPE FORM *ISN ( RD1. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                //* *------
                if (condition(pnd_Read_Ctr.equals(getZero())))                                                                                                            //Natural: IF #READ-CTR = 0
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(0, "***",new TabSetting(6),"No (5498) Forms Found For IRS Reporting",new TabSetting(77),"***",NEWLINE,"***",new                    //Natural: WRITE ( 00 ) '***' 06T 'No (5498) Forms Found For IRS Reporting' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                        TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new TabSetting(77),"***");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate(90);  if (true) return;                                                                                                             //Natural: TERMINATE 90
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
                sub_End_Of_Program_Processing();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *------------
                //* *------------
                //* *------------
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( 01 )
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //* *------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
                //* *------------
                //* *-------                                                                                                                                              //Natural: ON ERROR
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Lookup_Ira_Type() throws Exception                                                                                                                   //Natural: LOOKUP-IRA-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------
        //* *
        //* *  FIND IRA-TYPE BY CONTRACT RANGE
        //* *
        pnd_5498_Type.reset();                                                                                                                                            //Natural: RESET #5498-TYPE
        FOR01:                                                                                                                                                            //Natural: FOR I = 1 TO #IRA-CNTRCT-TBL-MAX
        for (i.setValue(1); condition(i.lessOrEqual(ldaTwrl4001.getPnd_Ira_Cntrct_Tbl_Max())); i.nadd(1))
        {
            if (condition(ldaTwrl441a.getForm_Tirf_Contract_Nbr().greaterOrEqual(ldaTwrl4001.getPnd_Ira_Contract_Tbl_Pnd_Ira_Cntrct_Begin_Range().getValue(i))            //Natural: IF FORM.TIRF-CONTRACT-NBR >= #IRA-CNTRCT-BEGIN-RANGE ( I ) AND FORM.TIRF-CONTRACT-NBR <= #IRA-CNTRCT-END-RANGE ( I )
                && ldaTwrl441a.getForm_Tirf_Contract_Nbr().lessOrEqual(ldaTwrl4001.getPnd_Ira_Contract_Tbl_Pnd_Ira_Cntrct_End_Range().getValue(i))))
            {
                pnd_5498_Type.setValue(ldaTwrl4001.getPnd_Ira_Contract_Tbl_Pnd_Ira_Type().getValue(i));                                                                   //Natural: ASSIGN #5498-TYPE := #IRA-CONTRACT-TBL.#IRA-TYPE ( I )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        getReports().write(0, new TabSetting(1),"5498 Forms Extracted For IRS Reporting......",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new                 //Natural: WRITE ( 00 ) 01T '5498 Forms Extracted For IRS Reporting......' #READ-CTR / 01T '5498 OK FORMS OUTPUT .......................' #OK-CTR / 01T '5498 Empty Forms Not Reported...............' #EMPTY-CTR / 01T '5498 Rejected Forms ........................' #REJECT-CTR / 01T '5498 WRONG CONTRACT FORMS...................' #NO-CONTRACT-CTR
            TabSetting(1),"5498 OK FORMS OUTPUT .......................",pnd_Ok_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"5498 Empty Forms Not Reported...............",pnd_Empty_Ctr, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"5498 Rejected Forms ........................",pnd_Reject_Ctr, new ReportEditMask 
            ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"5498 WRONG CONTRACT FORMS...................",pnd_No_Contract_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"5498 Forms Extracted For IRS Reporting......",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new  //Natural: WRITE ( 01 ) 01T '5498 Forms Extracted For IRS Reporting......' #READ-CTR / 01T '5498 OK FORMS OUTPUT .......................' #OK-CTR / 01T '5498 Empty Forms Not Reported...............' #EMPTY-CTR / 01T '5498 Rejected Forms ........................' #REJECT-CTR / 01T '5498 WRONG CONTRACT FORMS...................' #NO-CONTRACT-CTR
            TabSetting(1),"5498 OK FORMS OUTPUT .......................",pnd_Ok_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"5498 Empty Forms Not Reported...............",pnd_Empty_Ctr, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"5498 Rejected Forms ........................",pnd_Reject_Ctr, new ReportEditMask 
            ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"5498 WRONG CONTRACT FORMS...................",pnd_No_Contract_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(45),"IRA Contributions Form (5498) IRS Extract",new  //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 45T 'IRA Contributions Form (5498) IRS Extract' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");

        getReports().setDisplayColumns(0, pnd_Input_Parm_Pnd_Form,pnd_Input_Parm_Pnd_Tax_Year);
    }
}
