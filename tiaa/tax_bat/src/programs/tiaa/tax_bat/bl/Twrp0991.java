/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:32:21 PM
**        * FROM NATURAL PROGRAM : Twrp0991
************************************************************
**        * FILE NAME            : Twrp0991.java
**        * CLASS NAME           : Twrp0991
**        * INSTANCE NAME        : Twrp0991
************************************************************
***********************************************************************
*
* PROGRAM  : TWRP0991
*            CLONE OF TWRP0997 TO READ SORTED FLAT FILE, NOT DB
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : REPORT DUPLICATE CONTRACT PAYEE RECORDS CONTROL FILE.
* CREATED  : 01 / 03 / 2000.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM HISTOGRAMS THE TAX PAYMENT FILE LOOKING FOR
*            DUPLICATE (TAX YEAR, CONTRACT, AND PAYEE) RECORDS
*            TO REPORT.
* HISTORY:
*          11/21/2002 - MARINA NACHBER
*          - ADDED INPUT PARAMETER WITH YEAR & CHANGED EXTRACTING RECS
*            FOR GIVEN YEAR (INSTEAD OF READING TROUGH THE WHOLE FILE);
*          - CHANGED NOT TO PUT ON THE REPORT SERVICES/CREF DUPLICATE;
*          - CHANGED STARTING 2002 TO EXTRACT RECORDS WITH PAYMENT STAT.
*            ' ' OR 'C'
* 06/20/07 : A. YOUNG - REVISED FOR TAXWARS DATA WAREHOUSE.
* 02/18/15: OS - RECOMPILED FOR UPDATED TWRL0900
***********************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0991 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0900 ldaTwrl0900;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_payh;
    private DbsField payh_Twrpymnt_Con_Pay_Sd;

    private DbsGroup payh__R_Field_1;
    private DbsField payh_Pnd_Twrpymnt_Tax_Year;
    private DbsField payh_Pnd_Twrpymnt_Contract_Nbr;
    private DbsField payh_Pnd_Twrpymnt_Payee_Cde;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Tax_Year_A;

    private DbsGroup pnd_Ws__R_Field_2;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Prev_Contract;
    private DbsField pnd_Prev_Payee;
    private DbsField pnd_Prev_Comp;
    private DbsField pnd_Prev_Tin;
    private DbsField pnd_Diff_Comp_Found;
    private DbsField pnd_Act_Paymts_Found;
    private DbsField pnd_Save_Company_Code;
    private DbsField pnd_Save_Tax_Id_Nbr;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_I;
    private DbsField pnd_I1;

    private DbsGroup pnd_Table;
    private DbsField pnd_Table_Pnd_Contract;
    private DbsField pnd_Table_Pnd_Payee;
    private DbsField pnd_Table_Pnd_Company;
    private DbsField pnd_Table_Pnd_Tax_Id;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0900 = new LdaTwrl0900();
        registerRecord(ldaTwrl0900);

        // Local Variables
        localVariables = new DbsRecord();

        vw_payh = new DataAccessProgramView(new NameInfo("vw_payh", "PAYH"), "TWRPYMNT_PAYMENT_FILE", "TIR_PAYMENT");
        payh_Twrpymnt_Con_Pay_Sd = vw_payh.getRecord().newFieldInGroup("payh_Twrpymnt_Con_Pay_Sd", "TWRPYMNT-CON-PAY-SD", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "TWRPYMNT_CON_PAY_SD");
        payh_Twrpymnt_Con_Pay_Sd.setSuperDescriptor(true);

        payh__R_Field_1 = vw_payh.getRecord().newGroupInGroup("payh__R_Field_1", "REDEFINE", payh_Twrpymnt_Con_Pay_Sd);
        payh_Pnd_Twrpymnt_Tax_Year = payh__R_Field_1.newFieldInGroup("payh_Pnd_Twrpymnt_Tax_Year", "#TWRPYMNT-TAX-YEAR", FieldType.NUMERIC, 4);
        payh_Pnd_Twrpymnt_Contract_Nbr = payh__R_Field_1.newFieldInGroup("payh_Pnd_Twrpymnt_Contract_Nbr", "#TWRPYMNT-CONTRACT-NBR", FieldType.STRING, 
            8);
        payh_Pnd_Twrpymnt_Payee_Cde = payh__R_Field_1.newFieldInGroup("payh_Pnd_Twrpymnt_Payee_Cde", "#TWRPYMNT-PAYEE-CDE", FieldType.STRING, 2);
        registerRecord(vw_payh);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Tax_Year_A = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tax_Year_A", "#TAX-YEAR-A", FieldType.STRING, 4);

        pnd_Ws__R_Field_2 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_2", "REDEFINE", pnd_Ws_Pnd_Tax_Year_A);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Prev_Contract = localVariables.newFieldInRecord("pnd_Prev_Contract", "#PREV-CONTRACT", FieldType.STRING, 8);
        pnd_Prev_Payee = localVariables.newFieldInRecord("pnd_Prev_Payee", "#PREV-PAYEE", FieldType.STRING, 2);
        pnd_Prev_Comp = localVariables.newFieldInRecord("pnd_Prev_Comp", "#PREV-COMP", FieldType.STRING, 1);
        pnd_Prev_Tin = localVariables.newFieldInRecord("pnd_Prev_Tin", "#PREV-TIN", FieldType.STRING, 10);
        pnd_Diff_Comp_Found = localVariables.newFieldInRecord("pnd_Diff_Comp_Found", "#DIFF-COMP-FOUND", FieldType.BOOLEAN, 1);
        pnd_Act_Paymts_Found = localVariables.newFieldInRecord("pnd_Act_Paymts_Found", "#ACT-PAYMTS-FOUND", FieldType.BOOLEAN, 1);
        pnd_Save_Company_Code = localVariables.newFieldInRecord("pnd_Save_Company_Code", "#SAVE-COMPANY-CODE", FieldType.STRING, 1);
        pnd_Save_Tax_Id_Nbr = localVariables.newFieldInRecord("pnd_Save_Tax_Id_Nbr", "#SAVE-TAX-ID-NBR", FieldType.STRING, 10);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.NUMERIC, 3);

        pnd_Table = localVariables.newGroupInRecord("pnd_Table", "#TABLE");
        pnd_Table_Pnd_Contract = pnd_Table.newFieldArrayInGroup("pnd_Table_Pnd_Contract", "#CONTRACT", FieldType.STRING, 8, new DbsArrayController(1, 
            100));
        pnd_Table_Pnd_Payee = pnd_Table.newFieldArrayInGroup("pnd_Table_Pnd_Payee", "#PAYEE", FieldType.STRING, 2, new DbsArrayController(1, 100));
        pnd_Table_Pnd_Company = pnd_Table.newFieldArrayInGroup("pnd_Table_Pnd_Company", "#COMPANY", FieldType.STRING, 1, new DbsArrayController(1, 100));
        pnd_Table_Pnd_Tax_Id = pnd_Table.newFieldArrayInGroup("pnd_Table_Pnd_Tax_Id", "#TAX-ID", FieldType.STRING, 10, new DbsArrayController(1, 100));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_payh.reset();

        ldaTwrl0900.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0991() throws Exception
    {
        super("Twrp0991");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP0991", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) SG = OFF PS = 60 LS = 133;//Natural: FORMAT ( 01 ) SG = OFF PS = 60 LS = 133
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT-PARMS
        sub_Process_Input_Parms();
        if (condition(Global.isEscape())) {return;}
        //*  HS1.
        //*  HISTOGRAM   PAYH  TWRPYMNT-CON-PAY-SD STARTING FROM #WS.#TAX-YEAR
        //*  WRITE 'TWRPYMNT-CON-PAY-SD AFTER HISTORGRAM '   /* TEST
        //*        TWRPYMNT-CON-PAY-SD
        //*   IF #TWRPYMNT-TAX-YEAR > #WS.#TAX-YEAR
        //*   WRITE '#TWRPYMNT-TAX-YEAR ' #TWRPYMNT-TAX-YEAR   /* TEST
        //*     ESCAPE BOTTOM
        //*   END-IF
        //*   IF *NUMBER (HS1.)  =  1
        //*     WRITE  TWRPYMNT-CON-PAY-SD  '*NUMBER' *NUMBER (HS1.) /* TEST
        //*     IGNORE
        //*   ELSE
        //*     #READ-CTR  :=  0
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-TABLE
        sub_Initialize_Table();
        if (condition(Global.isEscape())) {return;}
        pnd_Diff_Comp_Found.reset();                                                                                                                                      //Natural: RESET #DIFF-COMP-FOUND
        pnd_I.reset();                                                                                                                                                    //Natural: RESET #I
        //*  WRITE 'TWRPYMNT-CON-PAY-SD before READ'  TWRPYMNT-CON-PAY-SD  /* TEST
        //*     READ  PAY   WITH  TWRPYMNT-CON-PAY-SD  =  TWRPYMNT-CON-PAY-SD
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 02 #XTAXYR-F94.PYMNT-GROUP #XTAXYR-F94.#PYMNT-OCCUR ( * )
        while (condition(getWorkFiles().read(2, ldaTwrl0900.getPnd_Xtaxyr_F94_Pymnt_Group(), ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Pymnt_Occur().getValue("*"))))
        {
            //*      IF TWRPYMNT-TAX-YEAR      =  #TWRPYMNT-TAX-YEAR      AND
            if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr().notEquals(pnd_Prev_Contract) || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payee_Cde().notEquals(pnd_Prev_Payee))) //Natural: IF TWRPYMNT-CONTRACT-NBR NE #PREV-CONTRACT OR TWRPYMNT-PAYEE-CDE NE #PREV-PAYEE
            {
                if (condition(pnd_Diff_Comp_Found.getBoolean()))                                                                                                          //Natural: IF #DIFF-COMP-FOUND
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-REPORT
                    sub_Print_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-TABLE
                sub_Initialize_Table();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Diff_Comp_Found.reset();                                                                                                                              //Natural: RESET #DIFF-COMP-FOUND
                pnd_I.reset();                                                                                                                                            //Natural: RESET #I
                pnd_Read_Ctr.setValue(0);                                                                                                                                 //Natural: ASSIGN #READ-CTR := 0
                pnd_Prev_Contract.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr());                                                                        //Natural: ASSIGN #PREV-CONTRACT := TWRPYMNT-CONTRACT-NBR
                pnd_Prev_Payee.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payee_Cde());                                                                              //Natural: ASSIGN #PREV-PAYEE := TWRPYMNT-PAYEE-CDE
                //*    ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Prev_Contract.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr());                                                                            //Natural: ASSIGN #PREV-CONTRACT := TWRPYMNT-CONTRACT-NBR
            pnd_Prev_Payee.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payee_Cde());                                                                                  //Natural: ASSIGN #PREV-PAYEE := TWRPYMNT-PAYEE-CDE
            pnd_Prev_Comp.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde());                                                                                 //Natural: ASSIGN #PREV-COMP := TWRPYMNT-COMPANY-CDE
            pnd_Prev_Tin.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr());                                                                                   //Natural: ASSIGN #PREV-TIN := TWRPYMNT-TAX-ID-NBR
            //*        TWRPYMNT-CONTRACT-NBR  =  #TWRPYMNT-CONTRACT-NBR  AND
            //*        TWRPYMNT-PAYEE-CDE     =  #TWRPYMNT-PAYEE-CDE
            //*        IGNORE
            //*      ELSE
            //*        IF #DIFF-COMP-FOUND
            //*          PERFORM PRINT-REPORT
            //* *      END-IF
            //*        ESCAPE BOTTOM
            //*      END-IF
            pnd_Act_Paymts_Found.setValue(false);                                                                                                                         //Natural: ASSIGN #ACT-PAYMTS-FOUND := FALSE
            if (condition(pnd_Ws_Pnd_Tax_Year.greaterOrEqual(2002)))                                                                                                      //Natural: IF #WS.#TAX-YEAR GE 2002
            {
                FOR01:                                                                                                                                                    //Natural: FOR #I1 1 TO #C-TWRPYMNT-PAYMENTS
                for (pnd_I1.setValue(1); condition(pnd_I1.lessOrEqual(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments())); pnd_I1.nadd(1))
                {
                    if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Status().getValue(pnd_I1).equals(" ") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Status().getValue(pnd_I1).equals("C"))) //Natural: IF TWRPYMNT-PYMNT-STATUS ( #I1 ) = ' ' OR = 'C'
                    {
                        pnd_Act_Paymts_Found.setValue(true);                                                                                                              //Natural: ASSIGN #ACT-PAYMTS-FOUND := TRUE
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Act_Paymts_Found.getBoolean()))                                                                                                         //Natural: IF #ACT-PAYMTS-FOUND
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            if (condition(pnd_Read_Ctr.equals(1)))                                                                                                                        //Natural: IF #READ-CTR = 1
            {
                pnd_Save_Company_Code.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde());                                                                     //Natural: ASSIGN #SAVE-COMPANY-CODE := TWRPYMNT-COMPANY-CDE
            }                                                                                                                                                             //Natural: END-IF
            //*       WRITE ' #I' #I                       /* TEST
            pnd_I.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #I
            pnd_Table_Pnd_Contract.getValue(pnd_I).setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr());                                                       //Natural: ASSIGN #CONTRACT ( #I ) := TWRPYMNT-CONTRACT-NBR
            pnd_Table_Pnd_Payee.getValue(pnd_I).setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payee_Cde());                                                             //Natural: ASSIGN #PAYEE ( #I ) := TWRPYMNT-PAYEE-CDE
            pnd_Table_Pnd_Company.getValue(pnd_I).setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde());                                                         //Natural: ASSIGN #COMPANY ( #I ) := TWRPYMNT-COMPANY-CDE
            pnd_Table_Pnd_Tax_Id.getValue(pnd_I).setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr());                                                           //Natural: ASSIGN #TAX-ID ( #I ) := TWRPYMNT-TAX-ID-NBR
            if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().notEquals(pnd_Save_Company_Code)))                                                         //Natural: IF TWRPYMNT-COMPANY-CDE NE #SAVE-COMPANY-CODE
            {
                if (condition(((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("S") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("C"))     //Natural: IF TWRPYMNT-COMPANY-CDE = 'S' OR = 'C' AND #SAVE-COMPANY-CODE = 'S' OR = 'C'
                    && (pnd_Save_Company_Code.equals("S") || pnd_Save_Company_Code.equals("C")))))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Diff_Comp_Found.setValue(true);                                                                                                                   //Natural: ASSIGN #DIFF-COMP-FOUND := TRUE
                    pnd_Save_Company_Code.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde());                                                                 //Natural: ASSIGN #SAVE-COMPANY-CODE := TWRPYMNT-COMPANY-CDE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  END-IF
        //*  END-HISTOGRAM
        //* *-----------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 01 )
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INPUT-PARMS
        //* ************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-TABLE
        //* ************************
        //* ************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-REPORT
        //* ************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
        //* *-------                                                                                                                                                      //Natural: ON ERROR
    }
    private void sub_Process_Input_Parms() throws Exception                                                                                                               //Natural: PROCESS-INPUT-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getWorkFiles().read(1, pnd_Ws_Pnd_Tax_Year_A);                                                                                                                    //Natural: READ WORK FILE 1 ONCE #WS.#TAX-YEAR-A
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            getReports().write(0, " ************************************ ",NEWLINE," ***                              *** ",NEWLINE," ***   INPUT PARAMETER IS EMPTY   *** ", //Natural: WRITE ' ************************************ ' / ' ***                              *** ' / ' ***   INPUT PARAMETER IS EMPTY   *** ' / ' ***    PLEASE INFORM SYSTEMS !!  *** ' / ' ***                              *** ' / ' ************************************ '
                NEWLINE," ***    PLEASE INFORM SYSTEMS !!  *** ",NEWLINE," ***                              *** ",NEWLINE," ************************************ ");
            if (Global.isEscape()) return;
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-ENDFILE
        //*  06/20/07
        //*  NEXT YEAR
        //*  CURRENT YR
        //*  CURRENT YR - 1
        //*  CURRENT YR - 2
        //*  CURRENT YR - 3
        short decideConditionsMet353 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST #WS.#TAX-YEAR-A;//Natural: VALUE '+1'
        if (condition((pnd_Ws_Pnd_Tax_Year_A.equals("+1"))))
        {
            decideConditionsMet353++;
            pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), (Global.getDATN().divide(10000)).add(1));                                      //Natural: ASSIGN #WS.#TAX-YEAR := ( *DATN / 10000 ) + 1
        }                                                                                                                                                                 //Natural: VALUE ' ', '0'
        else if (condition((pnd_Ws_Pnd_Tax_Year_A.equals(" ") || pnd_Ws_Pnd_Tax_Year_A.equals("0"))))
        {
            decideConditionsMet353++;
            pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), Global.getDATN().divide(10000));                                               //Natural: ASSIGN #WS.#TAX-YEAR := *DATN / 10000
        }                                                                                                                                                                 //Natural: VALUE '-1'
        else if (condition((pnd_Ws_Pnd_Tax_Year_A.equals("-1"))))
        {
            decideConditionsMet353++;
            pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), (Global.getDATN().divide(10000)).subtract(1));                                 //Natural: ASSIGN #WS.#TAX-YEAR := ( *DATN / 10000 ) - 1
        }                                                                                                                                                                 //Natural: VALUE '-2'
        else if (condition((pnd_Ws_Pnd_Tax_Year_A.equals("-2"))))
        {
            decideConditionsMet353++;
            pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), (Global.getDATN().divide(10000)).subtract(2));                                 //Natural: ASSIGN #WS.#TAX-YEAR := ( *DATN / 10000 ) - 2
        }                                                                                                                                                                 //Natural: VALUE '-3'
        else if (condition((pnd_Ws_Pnd_Tax_Year_A.equals("-3"))))
        {
            decideConditionsMet353++;
            pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), (Global.getDATN().divide(10000)).subtract(3));                                 //Natural: ASSIGN #WS.#TAX-YEAR := ( *DATN / 10000 ) - 3
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
            //*  06/20/07
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Initialize_Table() throws Exception                                                                                                                  //Natural: INITIALIZE-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #I FROM 1 TO #READ-CTR
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Read_Ctr)); pnd_I.nadd(1))
        {
            pnd_Table_Pnd_Contract.getValue(pnd_I).reset();                                                                                                               //Natural: RESET #CONTRACT ( #I ) #PAYEE ( #I ) #COMPANY ( #I ) #TAX-ID ( #I )
            pnd_Table_Pnd_Payee.getValue(pnd_I).reset();
            pnd_Table_Pnd_Company.getValue(pnd_I).reset();
            pnd_Table_Pnd_Tax_Id.getValue(pnd_I).reset();
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Print_Report() throws Exception                                                                                                                      //Natural: PRINT-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #I FROM 1 TO #READ-CTR
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Read_Ctr)); pnd_I.nadd(1))
        {
            if (condition(pnd_I.equals(1)))                                                                                                                               //Natural: IF #I = 1
            {
                Global.format("IS=OFF");                                                                                                                                  //Natural: SUSPEND IDENTICAL SUPPRESS ( 01 )
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(8),pnd_Table_Pnd_Contract.getValue(pnd_I), new IdenticalSuppress(true),new       //Natural: WRITE ( 01 ) NOTITLE NOHDR 08T #CONTRACT ( #I ) ( IS = ON ) 20T #PAYEE ( #I ) ( IS = ON ) 27T #TAX-ID ( #I ) 43T #COMPANY ( #I )
                TabSetting(20),pnd_Table_Pnd_Payee.getValue(pnd_I), new IdenticalSuppress(true),new TabSetting(27),pnd_Table_Pnd_Tax_Id.getValue(pnd_I),new 
                TabSetting(43),pnd_Table_Pnd_Company.getValue(pnd_I));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(48),"Tax Multiple Contracts Control Report",new  //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 48T 'Tax Multiple Contracts Control Report' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(59),"Tax Year ",pnd_Ws_Pnd_Tax_Year, new ReportEditMask ("9999"));                          //Natural: WRITE ( 01 ) NOTITLE 59T 'Tax Year ' #WS.#TAX-YEAR
                    getReports().skip(1, 2);                                                                                                                              //Natural: SKIP ( 01 ) 2 LINES
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(8),"        ",new TabSetting(19),"Payee",new TabSetting(27),"  Tax ID  ",new                //Natural: WRITE ( 01 ) NOTITLE 08T '        ' 19T 'Payee' 27T '  Tax ID  ' 40T 'Company'
                        TabSetting(40),"Company");
                    //*    01T 'Tax '
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(8),"Contract",new TabSetting(19),"Code ",new TabSetting(27),"  Number  ",new                //Natural: WRITE ( 01 ) NOTITLE 08T 'Contract' 19T 'Code ' 27T '  Number  ' 40T ' Code  '
                        TabSetting(40)," Code  ");
                    //*    01T 'Year'
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(8),"========",new TabSetting(19),"=====",new TabSetting(27),"==========",new                //Natural: WRITE ( 01 ) NOTITLE 08T '========' 19T '=====' 27T '==========' 40T '======='
                        TabSetting(40),"=======");
                    //*    01T '===='
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
        ignore();
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "SG=OFF PS=60 LS=133");
        Global.format(1, "SG=OFF PS=60 LS=133");
    }
}
