/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:38:35 PM
**        * FROM NATURAL PROGRAM : Twrp3646
************************************************************
**        * FILE NAME            : Twrp3646.java
**        * CLASS NAME           : Twrp3646
**        * INSTANCE NAME        : Twrp3646
************************************************************
************************************************************************
** PROGRAM:  TWRP3646 - IRS TO AL LAYOUT CONVERSION (1099-R)
** SYSTEM :  TAX WITHHOLDING & REPORTING SYSTEM
** AUTHOR :  J.BREMER
** PURPOSE:  CONVERT ALABAMA 1099-R DATA FROM IRS LAYOUT
**           ("B" RECORDS) TO NEW LAYOUT REQUIRED BY STATE
**
** INPUTS :  TPFX.TWR.TAX.P180ATWA.AL.ORIG.FILE
**
** HISTORY.....:
**   11/26/12 - J.BREMER  - CLONED TWRP3645 (PENNSYLVANIA)  AND
**                          APPLIED ALABAMA LAYOUT
**   01/10/14 - R. CARREON - ACCUMULATE AMOUNTS FOR SAME TIN
** 11/28/17 RM - RESTOW MODULE - TWRL3502 MODIFIED 2017 LAYOUT CHANGE
** 10/05/18 ARIVU - EIN CHANGES - CHANGE TAG: EINCHG
** 01/09/19 VIKRAM - RECORD IDENTIFIER ADDED IN STATE REPORTING -VIKRAM
***********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3646 extends BLNatBase
{
    // Data Areas
    private LdaTwrl3502 ldaTwrl3502;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Tab;
    private DbsField pnd_Comma;
    private DbsField pnd_Out;

    private DbsGroup pnd_Out__R_Field_1;
    private DbsField pnd_Out_Pnd_Record_Identifier;
    private DbsField pnd_Out_Pnd_Dlm07;
    private DbsField pnd_Out_Pnd_Emp_Ssn;
    private DbsField pnd_Out_Pnd_Dlm01;
    private DbsField pnd_Out_Pnd_Emp_First_Name;
    private DbsField pnd_Out_Pnd_Dlm02;
    private DbsField pnd_Out_Pnd_Emp_Middle_Name;
    private DbsField pnd_Out_Pnd_Dlm03;
    private DbsField pnd_Out_Pnd_Emp_Last_Name;
    private DbsField pnd_Out_Pnd_Dlm04;
    private DbsField pnd_Out_Pnd_Emp_Tax_Acct;
    private DbsField pnd_Out_Pnd_Dlm05;
    private DbsField pnd_Out_Pnd_Entity_Id;
    private DbsField pnd_Out_Pnd_Dlm06;
    private DbsField pnd_Out_Pnd_Emp_State;
    private DbsField pnd_Out_Pnd_Dlm08;
    private DbsField pnd_Out_Pnd_Taxable_Compensation;
    private DbsField pnd_Out_Pnd_Dlm09;
    private DbsField pnd_Out_Pnd_Al_Income_Tax_Withheld;
    private DbsField pnd_Out_Pnd_Dlm10;
    private DbsField pnd_Out_Pnd_Fed_Income_Tax_Withheld;
    private DbsField pnd_Out_Pnd_Dlm11;
    private DbsField pnd_Out_Pnd_Other_Income;
    private DbsField pnd_Out_Pnd_Dlm12;
    private DbsField pnd_Out_Pnd_Tax_Yr;
    private DbsField pnd_Out_Pnd_Dlm13;
    private DbsField pnd_Out_Hdr;

    private DbsGroup pnd_Out_Hdr__R_Field_2;
    private DbsField pnd_Out_Hdr_Pnd_Hdr04;
    private DbsField pnd_Out_Hdr_Pnd_Dlm05h;
    private DbsField pnd_Out_Hdr_Pnd_Hdr03;
    private DbsField pnd_Out_Hdr_Pnd_Dlm01h;
    private DbsField pnd_Out_Hdr_Pnd_Hdr06;
    private DbsField pnd_Out_Hdr_Pnd_Dlm02h;
    private DbsField pnd_Out_Hdr_Pnd_Hdr07;
    private DbsField pnd_Out_Hdr_Pnd_Dlm03h;
    private DbsField pnd_Out_Hdr_Pnd_Hdr05;
    private DbsField pnd_Out_Hdr_Pnd_Dlm04h;
    private DbsField pnd_Out_Hdr_Pnd_Hdr01;
    private DbsField pnd_Out_Hdr_Pnd_Dlm06h;
    private DbsField pnd_Out_Hdr_Pnd_Hdr20;
    private DbsField pnd_Out_Hdr_Pnd_Dlm07h;
    private DbsField pnd_Out_Hdr_Pnd_Hdr12;
    private DbsField pnd_Out_Hdr_Pnd_Dlm08h;
    private DbsField pnd_Out_Hdr_Pnd_Hdr15;
    private DbsField pnd_Out_Hdr_Pnd_Dlm09h;
    private DbsField pnd_Out_Hdr_Pnd_Hdr14;
    private DbsField pnd_Out_Hdr_Pnd_Dlm10h;
    private DbsField pnd_Out_Hdr_Pnd_Hdr21;
    private DbsField pnd_Out_Hdr_Pnd_Dlm11h;
    private DbsField pnd_Out_Hdr_Pnd_Hdr22;
    private DbsField pnd_Out_Hdr_Pnd_Dlm12h;
    private DbsField pnd_Out_Hdr_Pnd_Hdr02;
    private DbsField pnd_Out_Hdr_Pnd_Dlm13h;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Read_Cnt;
    private DbsField pnd_Counters_Pnd_Accept_Cnt;
    private DbsField pnd_Counters_Pnd_Write_Tiaa_Cnt;
    private DbsField pnd_Counters_Pnd_Write_Trust_Cnt;
    private DbsField pnd_Name_Seg1;
    private DbsField pnd_Name_Seg2;
    private DbsField pnd_Name_Seg3;
    private DbsField pnd_Name_Seg4;
    private DbsField pnd_Name_Seg5;
    private DbsField pnd_Temp_Co_Tin;
    private DbsField pnd_Temp_Co_Acct;
    private DbsField pnd_Temp_Long_Addr;

    private DbsGroup pnd_Temp_Long_Addr__R_Field_3;
    private DbsField pnd_Temp_Long_Addr_Pnd_Temp_Addr_Part1;
    private DbsField pnd_Temp_Long_Addr_Pnd_Temp_Addr_Part2;
    private DbsField pnd_Save_Outb_Tin;
    private DbsField pnd_First_Pass_A;
    private DbsField pnd_First_Pass;
    private DbsField pnd_Write_Rec;
    private DbsField pnd_W_Outb_Pay_Amt1;
    private DbsField pnd_W_Outb_Pay_Amt2;
    private DbsField pnd_W_Outb_Pay_Amt4;
    private DbsField pnd_W_Outb_State_Tax;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl3502 = new LdaTwrl3502();
        registerRecord(ldaTwrl3502);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Tab = localVariables.newFieldInRecord("pnd_Tab", "#TAB", FieldType.STRING, 1);
        pnd_Comma = localVariables.newFieldInRecord("pnd_Comma", "#COMMA", FieldType.STRING, 1);
        pnd_Out = localVariables.newFieldInRecord("pnd_Out", "#OUT", FieldType.STRING, 376);

        pnd_Out__R_Field_1 = localVariables.newGroupInRecord("pnd_Out__R_Field_1", "REDEFINE", pnd_Out);
        pnd_Out_Pnd_Record_Identifier = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Record_Identifier", "#RECORD-IDENTIFIER", FieldType.STRING, 2);
        pnd_Out_Pnd_Dlm07 = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Dlm07", "#DLM07", FieldType.STRING, 1);
        pnd_Out_Pnd_Emp_Ssn = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Emp_Ssn", "#EMP-SSN", FieldType.STRING, 9);
        pnd_Out_Pnd_Dlm01 = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Dlm01", "#DLM01", FieldType.STRING, 1);
        pnd_Out_Pnd_Emp_First_Name = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Emp_First_Name", "#EMP-FIRST-NAME", FieldType.STRING, 15);
        pnd_Out_Pnd_Dlm02 = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Dlm02", "#DLM02", FieldType.STRING, 1);
        pnd_Out_Pnd_Emp_Middle_Name = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Emp_Middle_Name", "#EMP-MIDDLE-NAME", FieldType.STRING, 15);
        pnd_Out_Pnd_Dlm03 = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Dlm03", "#DLM03", FieldType.STRING, 1);
        pnd_Out_Pnd_Emp_Last_Name = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Emp_Last_Name", "#EMP-LAST-NAME", FieldType.STRING, 20);
        pnd_Out_Pnd_Dlm04 = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Dlm04", "#DLM04", FieldType.STRING, 1);
        pnd_Out_Pnd_Emp_Tax_Acct = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Emp_Tax_Acct", "#EMP-TAX-ACCT", FieldType.STRING, 10);
        pnd_Out_Pnd_Dlm05 = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Dlm05", "#DLM05", FieldType.STRING, 1);
        pnd_Out_Pnd_Entity_Id = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Entity_Id", "#ENTITY-ID", FieldType.STRING, 9);
        pnd_Out_Pnd_Dlm06 = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Dlm06", "#DLM06", FieldType.STRING, 1);
        pnd_Out_Pnd_Emp_State = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Emp_State", "#EMP-STATE", FieldType.STRING, 2);
        pnd_Out_Pnd_Dlm08 = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Dlm08", "#DLM08", FieldType.STRING, 1);
        pnd_Out_Pnd_Taxable_Compensation = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Taxable_Compensation", "#TAXABLE-COMPENSATION", FieldType.STRING, 
            13);
        pnd_Out_Pnd_Dlm09 = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Dlm09", "#DLM09", FieldType.STRING, 1);
        pnd_Out_Pnd_Al_Income_Tax_Withheld = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Al_Income_Tax_Withheld", "#AL-INCOME-TAX-WITHHELD", FieldType.STRING, 
            13);
        pnd_Out_Pnd_Dlm10 = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Dlm10", "#DLM10", FieldType.STRING, 1);
        pnd_Out_Pnd_Fed_Income_Tax_Withheld = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Fed_Income_Tax_Withheld", "#FED-INCOME-TAX-WITHHELD", FieldType.STRING, 
            13);
        pnd_Out_Pnd_Dlm11 = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Dlm11", "#DLM11", FieldType.STRING, 1);
        pnd_Out_Pnd_Other_Income = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Other_Income", "#OTHER-INCOME", FieldType.STRING, 13);
        pnd_Out_Pnd_Dlm12 = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Dlm12", "#DLM12", FieldType.STRING, 1);
        pnd_Out_Pnd_Tax_Yr = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Tax_Yr", "#TAX-YR", FieldType.NUMERIC, 4);
        pnd_Out_Pnd_Dlm13 = pnd_Out__R_Field_1.newFieldInGroup("pnd_Out_Pnd_Dlm13", "#DLM13", FieldType.STRING, 1);
        pnd_Out_Hdr = localVariables.newFieldInRecord("pnd_Out_Hdr", "#OUT-HDR", FieldType.STRING, 376);

        pnd_Out_Hdr__R_Field_2 = localVariables.newGroupInRecord("pnd_Out_Hdr__R_Field_2", "REDEFINE", pnd_Out_Hdr);
        pnd_Out_Hdr_Pnd_Hdr04 = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Hdr04", "#HDR04", FieldType.STRING, 17);
        pnd_Out_Hdr_Pnd_Dlm05h = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Dlm05h", "#DLM05H", FieldType.STRING, 1);
        pnd_Out_Hdr_Pnd_Hdr03 = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Hdr03", "#HDR03", FieldType.STRING, 12);
        pnd_Out_Hdr_Pnd_Dlm01h = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Dlm01h", "#DLM01H", FieldType.STRING, 1);
        pnd_Out_Hdr_Pnd_Hdr06 = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Hdr06", "#HDR06", FieldType.STRING, 19);
        pnd_Out_Hdr_Pnd_Dlm02h = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Dlm02h", "#DLM02H", FieldType.STRING, 1);
        pnd_Out_Hdr_Pnd_Hdr07 = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Hdr07", "#HDR07", FieldType.STRING, 20);
        pnd_Out_Hdr_Pnd_Dlm03h = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Dlm03h", "#DLM03H", FieldType.STRING, 1);
        pnd_Out_Hdr_Pnd_Hdr05 = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Hdr05", "#HDR05", FieldType.STRING, 18);
        pnd_Out_Hdr_Pnd_Dlm04h = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Dlm04h", "#DLM04H", FieldType.STRING, 1);
        pnd_Out_Hdr_Pnd_Hdr01 = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Hdr01", "#HDR01", FieldType.STRING, 27);
        pnd_Out_Hdr_Pnd_Dlm06h = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Dlm06h", "#DLM06H", FieldType.STRING, 1);
        pnd_Out_Hdr_Pnd_Hdr20 = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Hdr20", "#HDR20", FieldType.STRING, 9);
        pnd_Out_Hdr_Pnd_Dlm07h = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Dlm07h", "#DLM07H", FieldType.STRING, 1);
        pnd_Out_Hdr_Pnd_Hdr12 = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Hdr12", "#HDR12", FieldType.STRING, 10);
        pnd_Out_Hdr_Pnd_Dlm08h = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Dlm08h", "#DLM08H", FieldType.STRING, 1);
        pnd_Out_Hdr_Pnd_Hdr15 = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Hdr15", "#HDR15", FieldType.STRING, 13);
        pnd_Out_Hdr_Pnd_Dlm09h = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Dlm09h", "#DLM09H", FieldType.STRING, 1);
        pnd_Out_Hdr_Pnd_Hdr14 = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Hdr14", "#HDR14", FieldType.STRING, 22);
        pnd_Out_Hdr_Pnd_Dlm10h = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Dlm10h", "#DLM10H", FieldType.STRING, 1);
        pnd_Out_Hdr_Pnd_Hdr21 = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Hdr21", "#HDR21", FieldType.STRING, 23);
        pnd_Out_Hdr_Pnd_Dlm11h = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Dlm11h", "#DLM11H", FieldType.STRING, 1);
        pnd_Out_Hdr_Pnd_Hdr22 = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Hdr22", "#HDR22", FieldType.STRING, 12);
        pnd_Out_Hdr_Pnd_Dlm12h = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Dlm12h", "#DLM12H", FieldType.STRING, 1);
        pnd_Out_Hdr_Pnd_Hdr02 = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Hdr02", "#HDR02", FieldType.STRING, 8);
        pnd_Out_Hdr_Pnd_Dlm13h = pnd_Out_Hdr__R_Field_2.newFieldInGroup("pnd_Out_Hdr_Pnd_Dlm13h", "#DLM13H", FieldType.STRING, 1);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Read_Cnt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Read_Cnt", "#READ-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Counters_Pnd_Accept_Cnt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Accept_Cnt", "#ACCEPT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Counters_Pnd_Write_Tiaa_Cnt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Write_Tiaa_Cnt", "#WRITE-TIAA-CNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Counters_Pnd_Write_Trust_Cnt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Write_Trust_Cnt", "#WRITE-TRUST-CNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Name_Seg1 = localVariables.newFieldInRecord("pnd_Name_Seg1", "#NAME-SEG1", FieldType.STRING, 30);
        pnd_Name_Seg2 = localVariables.newFieldInRecord("pnd_Name_Seg2", "#NAME-SEG2", FieldType.STRING, 30);
        pnd_Name_Seg3 = localVariables.newFieldInRecord("pnd_Name_Seg3", "#NAME-SEG3", FieldType.STRING, 30);
        pnd_Name_Seg4 = localVariables.newFieldInRecord("pnd_Name_Seg4", "#NAME-SEG4", FieldType.STRING, 30);
        pnd_Name_Seg5 = localVariables.newFieldInRecord("pnd_Name_Seg5", "#NAME-SEG5", FieldType.STRING, 30);
        pnd_Temp_Co_Tin = localVariables.newFieldInRecord("pnd_Temp_Co_Tin", "#TEMP-CO-TIN", FieldType.STRING, 9);
        pnd_Temp_Co_Acct = localVariables.newFieldInRecord("pnd_Temp_Co_Acct", "#TEMP-CO-ACCT", FieldType.STRING, 10);
        pnd_Temp_Long_Addr = localVariables.newFieldInRecord("pnd_Temp_Long_Addr", "#TEMP-LONG-ADDR", FieldType.STRING, 40);

        pnd_Temp_Long_Addr__R_Field_3 = localVariables.newGroupInRecord("pnd_Temp_Long_Addr__R_Field_3", "REDEFINE", pnd_Temp_Long_Addr);
        pnd_Temp_Long_Addr_Pnd_Temp_Addr_Part1 = pnd_Temp_Long_Addr__R_Field_3.newFieldInGroup("pnd_Temp_Long_Addr_Pnd_Temp_Addr_Part1", "#TEMP-ADDR-PART1", 
            FieldType.STRING, 22);
        pnd_Temp_Long_Addr_Pnd_Temp_Addr_Part2 = pnd_Temp_Long_Addr__R_Field_3.newFieldInGroup("pnd_Temp_Long_Addr_Pnd_Temp_Addr_Part2", "#TEMP-ADDR-PART2", 
            FieldType.STRING, 18);
        pnd_Save_Outb_Tin = localVariables.newFieldInRecord("pnd_Save_Outb_Tin", "#SAVE-OUTB-TIN", FieldType.STRING, 9);
        pnd_First_Pass_A = localVariables.newFieldInRecord("pnd_First_Pass_A", "#FIRST-PASS-A", FieldType.BOOLEAN, 1);
        pnd_First_Pass = localVariables.newFieldInRecord("pnd_First_Pass", "#FIRST-PASS", FieldType.BOOLEAN, 1);
        pnd_Write_Rec = localVariables.newFieldInRecord("pnd_Write_Rec", "#WRITE-REC", FieldType.BOOLEAN, 1);
        pnd_W_Outb_Pay_Amt1 = localVariables.newFieldInRecord("pnd_W_Outb_Pay_Amt1", "#W-OUTB-PAY-AMT1", FieldType.NUMERIC, 12, 2);
        pnd_W_Outb_Pay_Amt2 = localVariables.newFieldInRecord("pnd_W_Outb_Pay_Amt2", "#W-OUTB-PAY-AMT2", FieldType.NUMERIC, 12, 2);
        pnd_W_Outb_Pay_Amt4 = localVariables.newFieldInRecord("pnd_W_Outb_Pay_Amt4", "#W-OUTB-PAY-AMT4", FieldType.NUMERIC, 12, 2);
        pnd_W_Outb_State_Tax = localVariables.newFieldInRecord("pnd_W_Outb_State_Tax", "#W-OUTB-STATE-TAX", FieldType.NUMERIC, 12, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl3502.initializeValues();

        localVariables.reset();
        pnd_Tab.setInitialValue("H'05'");
        pnd_Comma.setInitialValue(",");
        pnd_First_Pass_A.setInitialValue(true);
        pnd_First_Pass.setInitialValue(true);
        pnd_Write_Rec.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3646() throws Exception
    {
        super("Twrp3646");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) PS = 58 LS = 80 ZP = ON
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 37T 'TAXWARS' 68T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 34T 'CONTROL REPORT' 68T 'REPORT: RPT9' / //
        //* **
                                                                                                                                                                          //Natural: PERFORM PROCESS-HEADER-RECORD
        sub_Process_Header_Record();
        if (condition(Global.isEscape())) {return;}
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #OUT-IRS-RECORD
        while (condition(getWorkFiles().read(1, ldaTwrl3502.getPnd_Out_Irs_Record())))
        {
            pnd_Counters_Pnd_Read_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #READ-CNT
            if (condition(!(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Rec_Id().equals("A") || ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Rec_Id().equals("B"))))         //Natural: ACCEPT IF #OUTB-REC-ID = 'A' OR = 'B'
            {
                continue;
            }
            if (condition(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Rec_Id().equals("A")))                                                                               //Natural: IF #OUTB-REC-ID = 'A'
            {
                pnd_First_Pass.setValue(true);                                                                                                                            //Natural: ASSIGN #FIRST-PASS := TRUE
                if (condition(pnd_First_Pass_A.getBoolean()))                                                                                                             //Natural: IF #FIRST-PASS-A
                {
                    pnd_First_Pass_A.setValue(false);                                                                                                                     //Natural: ASSIGN #FIRST-PASS-A := FALSE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-RECORD
                    sub_Write_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Temp_Co_Tin.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outa_Tin());                                                                               //Natural: ASSIGN #TEMP-CO-TIN := #OUTA-TIN
                if (condition(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outa_Tin().equals("131624203")))                                                                      //Natural: IF #OUTA-TIN = '131624203'
                {
                    pnd_Temp_Co_Acct.setValue("0000243537");                                                                                                              //Natural: ASSIGN #TEMP-CO-ACCT := '0000243537'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  EINCHG
                    //*  EINCHG
                    if (condition(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outa_Tin().equals("822826183")))                                                                  //Natural: IF #OUTA-TIN = '822826183'
                    {
                        pnd_Temp_Co_Acct.setValue("R009975021");                                                                                                          //Natural: ASSIGN #TEMP-CO-ACCT := 'R009975021'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Temp_Co_Acct.setValue("0000430011");                                                                                                          //Natural: ASSIGN #TEMP-CO-ACCT := '0000430011'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  $$$$ ROX
                pnd_Save_Outb_Tin.reset();                                                                                                                                //Natural: RESET #SAVE-OUTB-TIN
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  IF B RECORD PROCESS THE RECORD
            pnd_Counters_Pnd_Accept_Cnt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #ACCEPT-CNT
            //*  $$$$ ROX
            if (condition(pnd_First_Pass.getBoolean()))                                                                                                                   //Natural: IF #FIRST-PASS
            {
                pnd_First_Pass.setValue(false);                                                                                                                           //Natural: ASSIGN #FIRST-PASS := FALSE
                pnd_Save_Outb_Tin.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Tin());                                                                             //Natural: ASSIGN #SAVE-OUTB-TIN := #OUTB-TIN
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Save_Outb_Tin.equals(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Tin())))                                                                    //Natural: IF #SAVE-OUTB-TIN = #OUTB-TIN
            {
                                                                                                                                                                          //Natural: PERFORM ACCUM-AMOUNTS
                sub_Accum_Amounts();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM REFORMAT-RECORD
                sub_Reformat_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-RECORD
                sub_Write_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Save_Outb_Tin.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Tin());                                                                             //Natural: ASSIGN #SAVE-OUTB-TIN := #OUTB-TIN
                                                                                                                                                                          //Natural: PERFORM REFORMAT-RECORD
                sub_Reformat_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*   PERFORM REFORMAT-RECORD
            //*  $$$$
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-RECORD
        sub_Write_Record();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, "Records Read:          ",pnd_Counters_Pnd_Read_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"), new SignPosition (false),NEWLINE,"Records Reformatted:   ",pnd_Counters_Pnd_Accept_Cnt,  //Natural: WRITE ( 1 ) 'Records Read:          ' #READ-CNT ( EM = Z,ZZZ,ZZ9 SG = OFF ) / 'Records Reformatted:   ' #ACCEPT-CNT ( EM = Z,ZZZ,ZZ9 SG = OFF ) / 'TIAA Records Written:  ' #WRITE-TIAA-CNT ( EM = Z,ZZZ,ZZ9 SG = OFF ) / 'TRUST Records Written: ' #WRITE-TRUST-CNT ( EM = Z,ZZZ,ZZ9 SG = OFF ) /
            new ReportEditMask ("Z,ZZZ,ZZ9"), new SignPosition (false),NEWLINE,"TIAA Records Written:  ",pnd_Counters_Pnd_Write_Tiaa_Cnt, new ReportEditMask 
            ("Z,ZZZ,ZZ9"), new SignPosition (false),NEWLINE,"TRUST Records Written: ",pnd_Counters_Pnd_Write_Trust_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"), 
            new SignPosition (false),NEWLINE);
        if (Global.isEscape()) return;
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REFORMAT-RECORD
        //* *********************************
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-HEADER-RECORD
        //* ******************************************
        //*  --------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-AMOUNTS
        //* *#W-OUTB-PAY-AMT1   := #W-OUTB-PAY-AMT1  + #OUTB-PAY-AMT1
        //*  --------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RECORD
        //* *MOVE EDITED #W-OUTB-PAY-AMT2  (EM=9999999999.99)
        //* *  TO #TAXABLE-COMPENSATION
        //* **
    }
    private void sub_Reformat_Record() throws Exception                                                                                                                   //Natural: REFORMAT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Temp_Long_Addr.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Payee_Address());                                                                          //Natural: ASSIGN #TEMP-LONG-ADDR := #OUTB-PAYEE-ADDRESS
        //*  KS
        getReports().write(0, "#OUTB-FIRST-PAYEE-NAME-->",ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_First_Payee_Name());                                                 //Natural: WRITE '#OUTB-FIRST-PAYEE-NAME-->' #OUTB-FIRST-PAYEE-NAME
        if (Global.isEscape()) return;
        //*  ??? TENISE/KWAME
        //*  VIKRAM
        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_First_Payee_Name().separate(SeparateOption.WithAnyDelimiters, " ", pnd_Name_Seg1, pnd_Name_Seg2, pnd_Name_Seg3,        //Natural: SEPARATE #OUTB-FIRST-PAYEE-NAME INTO #NAME-SEG1 #NAME-SEG2 #NAME-SEG3 #NAME-SEG4 #NAME-SEG5 WITH DELIMITERS ' '
            pnd_Name_Seg4, pnd_Name_Seg5);
        pnd_Out_Pnd_Emp_Tax_Acct.setValue(pnd_Temp_Co_Acct);                                                                                                              //Natural: ASSIGN #EMP-TAX-ACCT := #TEMP-CO-ACCT
        pnd_Out_Pnd_Tax_Yr.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Year());                                                                               //Natural: ASSIGN #TAX-YR := #OUTB-PAY-YEAR
        pnd_Out_Pnd_Emp_Ssn.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Tin());                                                                                   //Natural: ASSIGN #EMP-SSN := #OUTB-TIN
        pnd_Out_Pnd_Emp_Last_Name.setValue(pnd_Name_Seg1);                                                                                                                //Natural: ASSIGN #EMP-LAST-NAME := #NAME-SEG1
        pnd_Out_Pnd_Emp_First_Name.setValue(pnd_Name_Seg2);                                                                                                               //Natural: ASSIGN #EMP-FIRST-NAME := #NAME-SEG2
        pnd_Out_Pnd_Emp_Middle_Name.setValue(" ");                                                                                                                        //Natural: ASSIGN #EMP-MIDDLE-NAME := ' '
        pnd_Out_Pnd_Emp_State.setValue("01");                                                                                                                             //Natural: ASSIGN #EMP-STATE := '01'
        pnd_Out_Pnd_Entity_Id.setValue(pnd_Temp_Co_Tin);                                                                                                                  //Natural: ASSIGN #ENTITY-ID := #TEMP-CO-TIN
        pnd_Out_Pnd_Record_Identifier.setValue("RS");                                                                                                                     //Natural: ASSIGN #RECORD-IDENTIFIER := 'RS'
    }
    //*  A27
    //*  A8
    //*  A12
    //*  A18
    //*  A19
    //*  A20
    //*  A10
    //*  A22
    //*  A22
    //*  A12
    //*  A13
    //*  A9
    //*  A17             VIKRAM
    private void sub_Process_Header_Record() throws Exception                                                                                                             //Natural: PROCESS-HEADER-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Out_Hdr_Pnd_Hdr01.setValue("Employer Tax Account Number");                                                                                                    //Natural: ASSIGN #HDR01 := 'Employer Tax Account Number'
        pnd_Out_Hdr_Pnd_Hdr02.setValue("Tax Year");                                                                                                                       //Natural: ASSIGN #HDR02 := 'Tax Year'
        pnd_Out_Hdr_Pnd_Hdr03.setValue("Employee SSN");                                                                                                                   //Natural: ASSIGN #HDR03 := 'Employee SSN'
        pnd_Out_Hdr_Pnd_Hdr05.setValue("Employee Last Name");                                                                                                             //Natural: ASSIGN #HDR05 := 'Employee Last Name'
        pnd_Out_Hdr_Pnd_Hdr06.setValue("Employee First Name");                                                                                                            //Natural: ASSIGN #HDR06 := 'Employee First Name'
        pnd_Out_Hdr_Pnd_Hdr07.setValue("Employee Middle Name");                                                                                                           //Natural: ASSIGN #HDR07 := 'Employee Middle Name'
        pnd_Out_Hdr_Pnd_Hdr12.setValue("State Code");                                                                                                                     //Natural: ASSIGN #HDR12 := 'State Code'
        pnd_Out_Hdr_Pnd_Hdr14.setValue("AL Income Tax Withheld");                                                                                                         //Natural: ASSIGN #HDR14 := 'AL Income Tax Withheld'
        pnd_Out_Hdr_Pnd_Hdr21.setValue("Fed Income Tax Withheld");                                                                                                        //Natural: ASSIGN #HDR21 := 'Fed Income Tax Withheld'
        pnd_Out_Hdr_Pnd_Hdr22.setValue("Other Income");                                                                                                                   //Natural: ASSIGN #HDR22 := 'Other Income'
        pnd_Out_Hdr_Pnd_Hdr15.setValue("Taxable Wages");                                                                                                                  //Natural: ASSIGN #HDR15 := 'Taxable Wages'
        pnd_Out_Hdr_Pnd_Hdr20.setValue("Entity ID");                                                                                                                      //Natural: ASSIGN #HDR20 := 'Entity ID'
        pnd_Out_Hdr_Pnd_Hdr04.setValue("Record Identifier");                                                                                                              //Natural: ASSIGN #HDR04 := 'Record Identifier'
        //*  VIKRAM
        pnd_Out_Hdr_Pnd_Dlm01h.setValue(pnd_Comma);                                                                                                                       //Natural: MOVE #COMMA TO #DLM01H #DLM02H #DLM03H #DLM04H #DLM06H #DLM07H #DLM08H #DLM09H #DLM10H #DLM11H #DLM12H #DLM13H #DLM05H
        pnd_Out_Hdr_Pnd_Dlm02h.setValue(pnd_Comma);
        pnd_Out_Hdr_Pnd_Dlm03h.setValue(pnd_Comma);
        pnd_Out_Hdr_Pnd_Dlm04h.setValue(pnd_Comma);
        pnd_Out_Hdr_Pnd_Dlm06h.setValue(pnd_Comma);
        pnd_Out_Hdr_Pnd_Dlm07h.setValue(pnd_Comma);
        pnd_Out_Hdr_Pnd_Dlm08h.setValue(pnd_Comma);
        pnd_Out_Hdr_Pnd_Dlm09h.setValue(pnd_Comma);
        pnd_Out_Hdr_Pnd_Dlm10h.setValue(pnd_Comma);
        pnd_Out_Hdr_Pnd_Dlm11h.setValue(pnd_Comma);
        pnd_Out_Hdr_Pnd_Dlm12h.setValue(pnd_Comma);
        pnd_Out_Hdr_Pnd_Dlm13h.setValue(pnd_Comma);
        pnd_Out_Hdr_Pnd_Dlm05h.setValue(pnd_Comma);
        getWorkFiles().write(2, false, pnd_Out_Hdr);                                                                                                                      //Natural: WRITE WORK FILE 2 #OUT-HDR
        getWorkFiles().write(3, false, pnd_Out_Hdr);                                                                                                                      //Natural: WRITE WORK FILE 3 #OUT-HDR
    }
    //*  $$$$ ROX
    private void sub_Accum_Amounts() throws Exception                                                                                                                     //Natural: ACCUM-AMOUNTS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_W_Outb_State_Tax.nadd(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_State_Tax());                                                                                //Natural: ASSIGN #W-OUTB-STATE-TAX := #W-OUTB-STATE-TAX + #OUTB-STATE-TAX
        pnd_W_Outb_Pay_Amt2.nadd(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt2());                                                                                  //Natural: ASSIGN #W-OUTB-PAY-AMT2 := #W-OUTB-PAY-AMT2 + #OUTB-PAY-AMT2
        pnd_W_Outb_Pay_Amt4.nadd(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt4());                                                                                  //Natural: ASSIGN #W-OUTB-PAY-AMT4 := #W-OUTB-PAY-AMT4 + #OUTB-PAY-AMT4
    }
    //*  $$$$ ROX
    private void sub_Write_Record() throws Exception                                                                                                                      //Natural: WRITE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------------------------------
        pnd_Out_Pnd_Al_Income_Tax_Withheld.setValueEdited(pnd_W_Outb_State_Tax,new ReportEditMask("9999999999.99"));                                                      //Natural: MOVE EDITED #W-OUTB-STATE-TAX ( EM = 9999999999.99 ) TO #AL-INCOME-TAX-WITHHELD
        //*  VIKRAM STARTS
        //* *MOVE EDITED #W-OUTB-PAY-AMT1  (EM=9999999999.99) TO #OTHER-INCOME
        //*  VIK
        //*  VIKRAM ENDS
        pnd_Out_Pnd_Other_Income.setValueEdited(pnd_W_Outb_Pay_Amt2,new ReportEditMask("9999999999.99"));                                                                 //Natural: MOVE EDITED #W-OUTB-PAY-AMT2 ( EM = 9999999999.99 ) TO #OTHER-INCOME
        pnd_Out_Pnd_Taxable_Compensation.setValue("0000000000000");                                                                                                       //Natural: ASSIGN #TAXABLE-COMPENSATION := '0000000000000'
        pnd_Out_Pnd_Fed_Income_Tax_Withheld.setValueEdited(pnd_W_Outb_Pay_Amt4,new ReportEditMask("9999999999.99"));                                                      //Natural: MOVE EDITED #W-OUTB-PAY-AMT4 ( EM = 9999999999.99 ) TO #FED-INCOME-TAX-WITHHELD
        //*  VIKRAM
        pnd_Out_Pnd_Dlm01.setValue(pnd_Comma);                                                                                                                            //Natural: MOVE #COMMA TO #DLM01 #DLM02 #DLM03 #DLM04 #DLM05 #DLM06 #DLM08 #DLM09 #DLM10 #DLM11 #DLM12 #DLM13 #DLM07
        pnd_Out_Pnd_Dlm02.setValue(pnd_Comma);
        pnd_Out_Pnd_Dlm03.setValue(pnd_Comma);
        pnd_Out_Pnd_Dlm04.setValue(pnd_Comma);
        pnd_Out_Pnd_Dlm05.setValue(pnd_Comma);
        pnd_Out_Pnd_Dlm06.setValue(pnd_Comma);
        pnd_Out_Pnd_Dlm08.setValue(pnd_Comma);
        pnd_Out_Pnd_Dlm09.setValue(pnd_Comma);
        pnd_Out_Pnd_Dlm10.setValue(pnd_Comma);
        pnd_Out_Pnd_Dlm11.setValue(pnd_Comma);
        pnd_Out_Pnd_Dlm12.setValue(pnd_Comma);
        pnd_Out_Pnd_Dlm13.setValue(pnd_Comma);
        pnd_Out_Pnd_Dlm07.setValue(pnd_Comma);
        //* EINCHG
        if (condition(pnd_Temp_Co_Acct.equals("0000243537") || pnd_Temp_Co_Acct.equals("R009975021")))                                                                    //Natural: IF #TEMP-CO-ACCT = '0000243537' OR #TEMP-CO-ACCT = 'R009975021'
        {
            getWorkFiles().write(2, false, pnd_Out);                                                                                                                      //Natural: WRITE WORK FILE 2 #OUT
            pnd_Counters_Pnd_Write_Tiaa_Cnt.nadd(1);                                                                                                                      //Natural: ADD 1 TO #WRITE-TIAA-CNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Counters_Pnd_Write_Trust_Cnt.nadd(1);                                                                                                                     //Natural: ADD 1 TO #WRITE-TRUST-CNT
            getWorkFiles().write(3, false, pnd_Out);                                                                                                                      //Natural: WRITE WORK FILE 3 #OUT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_W_Outb_State_Tax.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_State_Tax());                                                                            //Natural: ASSIGN #W-OUTB-STATE-TAX := #OUTB-STATE-TAX
        pnd_W_Outb_Pay_Amt1.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt1());                                                                              //Natural: ASSIGN #W-OUTB-PAY-AMT1 := #OUTB-PAY-AMT1
        pnd_W_Outb_Pay_Amt2.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt2());                                                                              //Natural: ASSIGN #W-OUTB-PAY-AMT2 := #OUTB-PAY-AMT2
        pnd_W_Outb_Pay_Amt4.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt4());                                                                              //Natural: ASSIGN #W-OUTB-PAY-AMT4 := #OUTB-PAY-AMT4
        pnd_Out.reset();                                                                                                                                                  //Natural: RESET #OUT
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=58 LS=80 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(37),"TAXWARS",new TabSetting(68),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(34),"CONTROL REPORT",new TabSetting(68),"REPORT: RPT9",NEWLINE,NEWLINE,NEWLINE);
    }
}
