/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:41:48 PM
**        * FROM NATURAL PROGRAM : Twrp5715
************************************************************
**        * FILE NAME            : Twrp5715.java
**        * CLASS NAME           : Twrp5715
**        * INSTANCE NAME        : Twrp5715
************************************************************
************************************************************************
* PROGRAM : TWRP5715
* SYSTEM  : TAXWARS
* FUNCTION: NEW REPORTS TO DISPLAY W2 TRANSACTION FOR CURRENT MONTH ONLY
*           INPUT IS OUTPUT OF PROGRAM TWRP5706.
*           RPT1 -  MONTHLY W2 TRANSACTIONS FOR MONTH BY RESIDENCY
*           RPT2 -  MONTHLY W2 TRANSACTIONS FOR MONTH BY PARTICIPANT
* HISTORY : 04/01/10  J.ROTHOLZ - CLEANED UP REPORT LAYOUT
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5715 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0600 ldaTwrl0600;
    private PdaTwracomp pdaTwracomp;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Hold_Tax_Year;

    private DbsGroup pnd_W2_Record;
    private DbsField pnd_W2_Record_Pnd_Tax_Year;

    private DbsGroup pnd_W2_Record__R_Field_1;
    private DbsField pnd_W2_Record_Pnd_Tax_Year_N;
    private DbsField pnd_W2_Record_Pnd_Tax_Id_Type;
    private DbsField pnd_W2_Record_Pnd_Tax_Id_Nbr;
    private DbsField pnd_W2_Record_Pnd_Pin_Nbr;
    private DbsField pnd_W2_Record_Pnd_Company_Code;
    private DbsField pnd_W2_Record_Pnd_Contract_Nbr;
    private DbsField pnd_W2_Record_Pnd_Payee_Cde;
    private DbsField pnd_W2_Record_Pnd_Residency_Type;
    private DbsField pnd_W2_Record_Pnd_Residency_Code;
    private DbsField pnd_W2_Record_Pnd_Gross_Amount;
    private DbsField pnd_W2_Record_Pnd_Federal_Tax;
    private DbsField pnd_W2_Record_Pnd_State_Tax;
    private DbsField pnd_W2_Record_Pnd_Name;
    private DbsField pnd_W2_Record_Pnd_Address_1;
    private DbsField pnd_W2_Record_Pnd_Address_2;
    private DbsField pnd_W2_Record_Pnd_Address_3;
    private DbsField pnd_W2_Record_Pnd_Address_4;
    private DbsField pnd_W2_Record_Pnd_Address_5;
    private DbsField pnd_W2_Record_Pnd_State_Id;
    private DbsField pnd_W2_Record_Pnd_Transaction_Dt;
    private DbsField pnd_W2_Record_Pnd_W2_Op_Ia_Ind;
    private DbsField pnd_W2_Record_Pnd_Record_Type;
    private DbsField pnd_Old_Tax_Id;
    private DbsField pnd_Old_State_Id;
    private DbsField pnd_Old_Name;
    private DbsField pnd_Old_Addr_1;
    private DbsField pnd_Old_Addr_2;
    private DbsField pnd_Old_Addr_3;
    private DbsField pnd_Old_Addr_4;
    private DbsField pnd_Old_Addr_5;

    private DbsGroup pnd_Part_Totals;
    private DbsField pnd_Part_Totals_Pnd_Part_Count;
    private DbsField pnd_Part_Totals_Pnd_Gross_Amount_Pt;
    private DbsField pnd_Part_Totals_Pnd_Federal_Tax_Pt;
    private DbsField pnd_Part_Totals_Pnd_State_Tax_Pt;

    private DbsGroup pnd_State_Totals;
    private DbsField pnd_State_Totals_Pnd_State_Count;
    private DbsField pnd_State_Totals_Pnd_Gross_Amount_St;
    private DbsField pnd_State_Totals_Pnd_Federal_Tax_St;
    private DbsField pnd_State_Totals_Pnd_State_Tax_St;

    private DbsGroup pnd_Comp_Totals;
    private DbsField pnd_Comp_Totals_Pnd_Comp_Count;
    private DbsField pnd_Comp_Totals_Pnd_Gross_Amount_Ct;
    private DbsField pnd_Comp_Totals_Pnd_Federal_Tax_Ct;
    private DbsField pnd_Comp_Totals_Pnd_State_Tax_Ct;

    private DbsGroup pnd_Grand_Totals;
    private DbsField pnd_Grand_Totals_Pnd_Grand_Count;
    private DbsField pnd_Grand_Totals_Pnd_Gross_Amount_Gt;
    private DbsField pnd_Grand_Totals_Pnd_Federal_Tax_Gt;
    private DbsField pnd_Grand_Totals_Pnd_State_Tax_Gt;
    private DbsField pnd_I;
    private DbsField pnd_Type;

    private DbsGroup pnd_Type__R_Field_2;
    private DbsField pnd_Type_Pnd_Pmnt;
    private DbsField pnd_Hold_Ndx;
    private DbsField pnd_Opia_Count_Ct;
    private DbsField pnd_Gross_Amount_Opia_Ct;
    private DbsField pnd_Federal_Tax_Opia_Ct;
    private DbsField pnd_State_Tax_Opia_Ct;
    private DbsField pnd_Opia_Count_Gt;
    private DbsField pnd_Gross_Amount_Opia_Gt;
    private DbsField pnd_Federal_Tax_Opia_Gt;
    private DbsField pnd_State_Tax_Opia_Gt;
    private DbsField pnd_Company_Line;

    private DbsGroup pnd_Company_Line__R_Field_3;
    private DbsField pnd_Company_Line_Pnd_Skip;
    private DbsField pnd_Company_Line_Pnd_Company;
    private DbsField pnd_Cntrct_Nbr;

    private DbsGroup pnd_Cntrct_Nbr__R_Field_4;
    private DbsField pnd_Cntrct_Nbr_Pnd_Xx;
    private DbsField pnd_Cntrct_Nbr_Pnd_Ppcn;
    private DbsField pnd_Monthly_Date;

    private DbsGroup pnd_Monthly_Date__R_Field_5;
    private DbsField pnd_Monthly_Date_Pnd_Yyyy;
    private DbsField pnd_Monthly_Date_Pnd_Mm;
    private DbsField pnd_Monthly_Date_Pnd_Dd;
    private DbsField pnd_Month;
    private DbsField pnd_Report_Title_01;
    private DbsField pnd_Report_Title_02;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pnd_Tax_Id_NbrOld;
    private DbsField readWork01Pnd_State_IdOld;
    private DbsField readWork01Pnd_NameOld;
    private DbsField readWork01Pnd_Address_1Old;
    private DbsField readWork01Pnd_Address_2Old;
    private DbsField readWork01Pnd_Address_3Old;
    private DbsField readWork01Pnd_Address_4Old;
    private DbsField readWork01Pnd_Address_5Old;
    private DbsField readWork01Pnd_Company_CodeOld;
    private DbsField readWork01Pnd_Tax_Year_NOld;
    private DbsField readWork01Pnd_CompanyOld;
    private DbsField readWork01Pnd_W2_Op_Ia_IndOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);
        localVariables = new DbsRecord();
        pdaTwracomp = new PdaTwracomp(localVariables);

        // Local Variables
        pnd_Hold_Tax_Year = localVariables.newFieldInRecord("pnd_Hold_Tax_Year", "#HOLD-TAX-YEAR", FieldType.STRING, 4);

        pnd_W2_Record = localVariables.newGroupInRecord("pnd_W2_Record", "#W2-RECORD");
        pnd_W2_Record_Pnd_Tax_Year = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);

        pnd_W2_Record__R_Field_1 = pnd_W2_Record.newGroupInGroup("pnd_W2_Record__R_Field_1", "REDEFINE", pnd_W2_Record_Pnd_Tax_Year);
        pnd_W2_Record_Pnd_Tax_Year_N = pnd_W2_Record__R_Field_1.newFieldInGroup("pnd_W2_Record_Pnd_Tax_Year_N", "#TAX-YEAR-N", FieldType.NUMERIC, 4);
        pnd_W2_Record_Pnd_Tax_Id_Type = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Tax_Id_Type", "#TAX-ID-TYPE", FieldType.STRING, 1);
        pnd_W2_Record_Pnd_Tax_Id_Nbr = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.STRING, 10);
        pnd_W2_Record_Pnd_Pin_Nbr = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 12);
        pnd_W2_Record_Pnd_Company_Code = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Company_Code", "#COMPANY-CODE", FieldType.STRING, 1);
        pnd_W2_Record_Pnd_Contract_Nbr = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Contract_Nbr", "#CONTRACT-NBR", FieldType.STRING, 8);
        pnd_W2_Record_Pnd_Payee_Cde = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.STRING, 2);
        pnd_W2_Record_Pnd_Residency_Type = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Residency_Type", "#RESIDENCY-TYPE", FieldType.STRING, 1);
        pnd_W2_Record_Pnd_Residency_Code = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Residency_Code", "#RESIDENCY-CODE", FieldType.STRING, 2);
        pnd_W2_Record_Pnd_Gross_Amount = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Gross_Amount", "#GROSS-AMOUNT", FieldType.PACKED_DECIMAL, 11, 
            2);
        pnd_W2_Record_Pnd_Federal_Tax = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Federal_Tax", "#FEDERAL-TAX", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W2_Record_Pnd_State_Tax = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_State_Tax", "#STATE-TAX", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W2_Record_Pnd_Name = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Name", "#NAME", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Address_1 = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Address_1", "#ADDRESS-1", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Address_2 = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Address_2", "#ADDRESS-2", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Address_3 = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Address_3", "#ADDRESS-3", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Address_4 = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Address_4", "#ADDRESS-4", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Address_5 = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Address_5", "#ADDRESS-5", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_State_Id = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_State_Id", "#STATE-ID", FieldType.STRING, 16);
        pnd_W2_Record_Pnd_Transaction_Dt = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Transaction_Dt", "#TRANSACTION-DT", FieldType.STRING, 8);
        pnd_W2_Record_Pnd_W2_Op_Ia_Ind = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_W2_Op_Ia_Ind", "#W2-OP-IA-IND", FieldType.NUMERIC, 1);
        pnd_W2_Record_Pnd_Record_Type = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Record_Type", "#RECORD-TYPE", FieldType.NUMERIC, 1);
        pnd_Old_Tax_Id = localVariables.newFieldInRecord("pnd_Old_Tax_Id", "#OLD-TAX-ID", FieldType.STRING, 10);
        pnd_Old_State_Id = localVariables.newFieldInRecord("pnd_Old_State_Id", "#OLD-STATE-ID", FieldType.STRING, 2);
        pnd_Old_Name = localVariables.newFieldInRecord("pnd_Old_Name", "#OLD-NAME", FieldType.STRING, 35);
        pnd_Old_Addr_1 = localVariables.newFieldInRecord("pnd_Old_Addr_1", "#OLD-ADDR-1", FieldType.STRING, 35);
        pnd_Old_Addr_2 = localVariables.newFieldInRecord("pnd_Old_Addr_2", "#OLD-ADDR-2", FieldType.STRING, 35);
        pnd_Old_Addr_3 = localVariables.newFieldInRecord("pnd_Old_Addr_3", "#OLD-ADDR-3", FieldType.STRING, 35);
        pnd_Old_Addr_4 = localVariables.newFieldInRecord("pnd_Old_Addr_4", "#OLD-ADDR-4", FieldType.STRING, 35);
        pnd_Old_Addr_5 = localVariables.newFieldInRecord("pnd_Old_Addr_5", "#OLD-ADDR-5", FieldType.STRING, 35);

        pnd_Part_Totals = localVariables.newGroupInRecord("pnd_Part_Totals", "#PART-TOTALS");
        pnd_Part_Totals_Pnd_Part_Count = pnd_Part_Totals.newFieldInGroup("pnd_Part_Totals_Pnd_Part_Count", "#PART-COUNT", FieldType.PACKED_DECIMAL, 8);
        pnd_Part_Totals_Pnd_Gross_Amount_Pt = pnd_Part_Totals.newFieldInGroup("pnd_Part_Totals_Pnd_Gross_Amount_Pt", "#GROSS-AMOUNT-PT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Part_Totals_Pnd_Federal_Tax_Pt = pnd_Part_Totals.newFieldInGroup("pnd_Part_Totals_Pnd_Federal_Tax_Pt", "#FEDERAL-TAX-PT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Part_Totals_Pnd_State_Tax_Pt = pnd_Part_Totals.newFieldInGroup("pnd_Part_Totals_Pnd_State_Tax_Pt", "#STATE-TAX-PT", FieldType.PACKED_DECIMAL, 
            11, 2);

        pnd_State_Totals = localVariables.newGroupInRecord("pnd_State_Totals", "#STATE-TOTALS");
        pnd_State_Totals_Pnd_State_Count = pnd_State_Totals.newFieldInGroup("pnd_State_Totals_Pnd_State_Count", "#STATE-COUNT", FieldType.PACKED_DECIMAL, 
            8);
        pnd_State_Totals_Pnd_Gross_Amount_St = pnd_State_Totals.newFieldInGroup("pnd_State_Totals_Pnd_Gross_Amount_St", "#GROSS-AMOUNT-ST", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_State_Totals_Pnd_Federal_Tax_St = pnd_State_Totals.newFieldInGroup("pnd_State_Totals_Pnd_Federal_Tax_St", "#FEDERAL-TAX-ST", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_State_Totals_Pnd_State_Tax_St = pnd_State_Totals.newFieldInGroup("pnd_State_Totals_Pnd_State_Tax_St", "#STATE-TAX-ST", FieldType.PACKED_DECIMAL, 
            11, 2);

        pnd_Comp_Totals = localVariables.newGroupInRecord("pnd_Comp_Totals", "#COMP-TOTALS");
        pnd_Comp_Totals_Pnd_Comp_Count = pnd_Comp_Totals.newFieldInGroup("pnd_Comp_Totals_Pnd_Comp_Count", "#COMP-COUNT", FieldType.PACKED_DECIMAL, 8);
        pnd_Comp_Totals_Pnd_Gross_Amount_Ct = pnd_Comp_Totals.newFieldInGroup("pnd_Comp_Totals_Pnd_Gross_Amount_Ct", "#GROSS-AMOUNT-CT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Comp_Totals_Pnd_Federal_Tax_Ct = pnd_Comp_Totals.newFieldInGroup("pnd_Comp_Totals_Pnd_Federal_Tax_Ct", "#FEDERAL-TAX-CT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Comp_Totals_Pnd_State_Tax_Ct = pnd_Comp_Totals.newFieldInGroup("pnd_Comp_Totals_Pnd_State_Tax_Ct", "#STATE-TAX-CT", FieldType.PACKED_DECIMAL, 
            11, 2);

        pnd_Grand_Totals = localVariables.newGroupInRecord("pnd_Grand_Totals", "#GRAND-TOTALS");
        pnd_Grand_Totals_Pnd_Grand_Count = pnd_Grand_Totals.newFieldInGroup("pnd_Grand_Totals_Pnd_Grand_Count", "#GRAND-COUNT", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Grand_Totals_Pnd_Gross_Amount_Gt = pnd_Grand_Totals.newFieldInGroup("pnd_Grand_Totals_Pnd_Gross_Amount_Gt", "#GROSS-AMOUNT-GT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Grand_Totals_Pnd_Federal_Tax_Gt = pnd_Grand_Totals.newFieldInGroup("pnd_Grand_Totals_Pnd_Federal_Tax_Gt", "#FEDERAL-TAX-GT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Grand_Totals_Pnd_State_Tax_Gt = pnd_Grand_Totals.newFieldInGroup("pnd_Grand_Totals_Pnd_State_Tax_Gt", "#STATE-TAX-GT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 4);
        pnd_Type = localVariables.newFieldInRecord("pnd_Type", "#TYPE", FieldType.STRING, 25);

        pnd_Type__R_Field_2 = localVariables.newGroupInRecord("pnd_Type__R_Field_2", "REDEFINE", pnd_Type);
        pnd_Type_Pnd_Pmnt = pnd_Type__R_Field_2.newFieldArrayInGroup("pnd_Type_Pnd_Pmnt", "#PMNT", FieldType.STRING, 5, new DbsArrayController(1, 5));
        pnd_Hold_Ndx = localVariables.newFieldInRecord("pnd_Hold_Ndx", "#HOLD-NDX", FieldType.NUMERIC, 1);
        pnd_Opia_Count_Ct = localVariables.newFieldArrayInRecord("pnd_Opia_Count_Ct", "#OPIA-COUNT-CT", FieldType.PACKED_DECIMAL, 8, new DbsArrayController(1, 
            5));
        pnd_Gross_Amount_Opia_Ct = localVariables.newFieldArrayInRecord("pnd_Gross_Amount_Opia_Ct", "#GROSS-AMOUNT-OPIA-CT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 5));
        pnd_Federal_Tax_Opia_Ct = localVariables.newFieldArrayInRecord("pnd_Federal_Tax_Opia_Ct", "#FEDERAL-TAX-OPIA-CT", FieldType.PACKED_DECIMAL, 11, 
            2, new DbsArrayController(1, 5));
        pnd_State_Tax_Opia_Ct = localVariables.newFieldArrayInRecord("pnd_State_Tax_Opia_Ct", "#STATE-TAX-OPIA-CT", FieldType.PACKED_DECIMAL, 11, 2, new 
            DbsArrayController(1, 5));
        pnd_Opia_Count_Gt = localVariables.newFieldArrayInRecord("pnd_Opia_Count_Gt", "#OPIA-COUNT-GT", FieldType.PACKED_DECIMAL, 8, new DbsArrayController(1, 
            5));
        pnd_Gross_Amount_Opia_Gt = localVariables.newFieldArrayInRecord("pnd_Gross_Amount_Opia_Gt", "#GROSS-AMOUNT-OPIA-GT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 5));
        pnd_Federal_Tax_Opia_Gt = localVariables.newFieldArrayInRecord("pnd_Federal_Tax_Opia_Gt", "#FEDERAL-TAX-OPIA-GT", FieldType.PACKED_DECIMAL, 11, 
            2, new DbsArrayController(1, 5));
        pnd_State_Tax_Opia_Gt = localVariables.newFieldArrayInRecord("pnd_State_Tax_Opia_Gt", "#STATE-TAX-OPIA-GT", FieldType.PACKED_DECIMAL, 11, 2, new 
            DbsArrayController(1, 5));
        pnd_Company_Line = localVariables.newFieldInRecord("pnd_Company_Line", "#COMPANY-LINE", FieldType.STRING, 72);

        pnd_Company_Line__R_Field_3 = localVariables.newGroupInRecord("pnd_Company_Line__R_Field_3", "REDEFINE", pnd_Company_Line);
        pnd_Company_Line_Pnd_Skip = pnd_Company_Line__R_Field_3.newFieldInGroup("pnd_Company_Line_Pnd_Skip", "#SKIP", FieldType.STRING, 10);
        pnd_Company_Line_Pnd_Company = pnd_Company_Line__R_Field_3.newFieldInGroup("pnd_Company_Line_Pnd_Company", "#COMPANY", FieldType.STRING, 4);
        pnd_Cntrct_Nbr = localVariables.newFieldInRecord("pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 9);

        pnd_Cntrct_Nbr__R_Field_4 = localVariables.newGroupInRecord("pnd_Cntrct_Nbr__R_Field_4", "REDEFINE", pnd_Cntrct_Nbr);
        pnd_Cntrct_Nbr_Pnd_Xx = pnd_Cntrct_Nbr__R_Field_4.newFieldInGroup("pnd_Cntrct_Nbr_Pnd_Xx", "#XX", FieldType.STRING, 1);
        pnd_Cntrct_Nbr_Pnd_Ppcn = pnd_Cntrct_Nbr__R_Field_4.newFieldInGroup("pnd_Cntrct_Nbr_Pnd_Ppcn", "#PPCN", FieldType.STRING, 8);
        pnd_Monthly_Date = localVariables.newFieldInRecord("pnd_Monthly_Date", "#MONTHLY-DATE", FieldType.STRING, 8);

        pnd_Monthly_Date__R_Field_5 = localVariables.newGroupInRecord("pnd_Monthly_Date__R_Field_5", "REDEFINE", pnd_Monthly_Date);
        pnd_Monthly_Date_Pnd_Yyyy = pnd_Monthly_Date__R_Field_5.newFieldInGroup("pnd_Monthly_Date_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 4);
        pnd_Monthly_Date_Pnd_Mm = pnd_Monthly_Date__R_Field_5.newFieldInGroup("pnd_Monthly_Date_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Monthly_Date_Pnd_Dd = pnd_Monthly_Date__R_Field_5.newFieldInGroup("pnd_Monthly_Date_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);
        pnd_Month = localVariables.newFieldArrayInRecord("pnd_Month", "#MONTH", FieldType.STRING, 9, new DbsArrayController(1, 12));
        pnd_Report_Title_01 = localVariables.newFieldInRecord("pnd_Report_Title_01", "#REPORT-TITLE-01", FieldType.STRING, 55);
        pnd_Report_Title_02 = localVariables.newFieldInRecord("pnd_Report_Title_02", "#REPORT-TITLE-02", FieldType.STRING, 57);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pnd_Tax_Id_NbrOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Tax_Id_Nbr_OLD", "Pnd_Tax_Id_Nbr_OLD", FieldType.STRING, 10);
        readWork01Pnd_State_IdOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_State_Id_OLD", "Pnd_State_Id_OLD", FieldType.STRING, 16);
        readWork01Pnd_NameOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Name_OLD", "Pnd_Name_OLD", FieldType.STRING, 35);
        readWork01Pnd_Address_1Old = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Address_1_OLD", "Pnd_Address_1_OLD", FieldType.STRING, 35);
        readWork01Pnd_Address_2Old = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Address_2_OLD", "Pnd_Address_2_OLD", FieldType.STRING, 35);
        readWork01Pnd_Address_3Old = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Address_3_OLD", "Pnd_Address_3_OLD", FieldType.STRING, 35);
        readWork01Pnd_Address_4Old = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Address_4_OLD", "Pnd_Address_4_OLD", FieldType.STRING, 35);
        readWork01Pnd_Address_5Old = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Address_5_OLD", "Pnd_Address_5_OLD", FieldType.STRING, 35);
        readWork01Pnd_Company_CodeOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Company_Code_OLD", "Pnd_Company_Code_OLD", FieldType.STRING, 
            1);
        readWork01Pnd_Tax_Year_NOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Tax_Year_N_OLD", "Pnd_Tax_Year_N_OLD", FieldType.NUMERIC, 4);
        readWork01Pnd_CompanyOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Company_OLD", "Pnd_Company_OLD", FieldType.STRING, 4);
        readWork01Pnd_W2_Op_Ia_IndOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_W2_Op_Ia_Ind_OLD", "Pnd_W2_Op_Ia_Ind_OLD", FieldType.NUMERIC, 
            1);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaTwrl0600.initializeValues();

        localVariables.reset();
        pnd_Type.setInitialValue("OP   IA   NL   ML   OP/IA");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5715() throws Exception
    {
        super("Twrp5715");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atEndOfPage(atEndEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 2 ) PS = 58 LS = 133 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                                                                                                                                                                          //Natural: PERFORM PROCESS-CONTROL-RECORD
        sub_Process_Control_Record();
        if (condition(Global.isEscape())) {return;}
        StringExtension.separate("January February March April May June July August September October November December", SeparateOption.WithAnyDelimiters,               //Natural: SEPARATE 'January February March April May June July August September October November December' INTO #MONTH ( * ) WITH DELIMITERS ' '
            " ", pnd_Month.getValue("*"));
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Report_Title_01.setValue(DbsUtil.compress("Monthly W2 Transactions for", pnd_Month.getValue(pnd_Monthly_Date_Pnd_Mm), "by Residency"));                       //Natural: COMPRESS 'Monthly W2 Transactions for' #MONTH ( #MM ) 'by Residency' INTO #REPORT-TITLE-01
        pnd_Report_Title_02.setValue(DbsUtil.compress("Monthly W2 Transactions for", pnd_Month.getValue(pnd_Monthly_Date_Pnd_Mm), "by Participant"));                     //Natural: COMPRESS 'Monthly W2 Transactions for' #MONTH ( #MM ) 'by Participant' INTO #REPORT-TITLE-02
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 01 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 02 )
        //*                                                                                                                                                               //Natural: AT END OF PAGE ( 01 )
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 RECORD #W2-RECORD
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pnd_W2_Record)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            if (condition(pnd_W2_Record_Pnd_Record_Type.equals(1)))                                                                                                       //Natural: IF #RECORD-TYPE = 1
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Hold_Tax_Year.equals(" ")))                                                                                                                 //Natural: IF #HOLD-TAX-YEAR = ' '
            {
                pnd_Hold_Tax_Year.setValue(pnd_W2_Record_Pnd_Tax_Year);                                                                                                   //Natural: ASSIGN #HOLD-TAX-YEAR := #W2-RECORD.#TAX-YEAR
            }                                                                                                                                                             //Natural: END-IF
            pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(pnd_W2_Record_Pnd_Company_Code);                                                                         //Natural: ASSIGN #TWRACOMP.#COMP-CODE := #W2-RECORD.#COMPANY-CODE
            pdaTwracomp.getPnd_Twracomp_Pnd_Tax_Year().setValue(pnd_W2_Record_Pnd_Tax_Year_N);                                                                            //Natural: ASSIGN #TWRACOMP.#TAX-YEAR := #W2-RECORD.#TAX-YEAR-N
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMPANY
            sub_Process_Company();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *****************************************
            //*  PARTICIPANT BREAK - LOWEST LEVEL BREAK
            //* *****************************************
            //* ************************************                                                                                                                      //Natural: AT BREAK OF #W2-RECORD.#TAX-ID-NBR
            //*  RESIDENCY BREAK - MID LEVEL BREAK
            //* ************************************
            //* **************************************                                                                                                                    //Natural: AT BREAK OF #RESIDENCY-CODE
            //*  COMPANY BREAK - HIGHEST LEVEL BREAK
            //* **************************************
            //*                                                                                                                                                           //Natural: AT BREAK OF #W2-RECORD.#COMPANY-CODE
                                                                                                                                                                          //Natural: PERFORM WRITE-W2-REPORT-01
            sub_Write_W2_Report_01();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Part_Totals_Pnd_Gross_Amount_Pt.nadd(pnd_W2_Record_Pnd_Gross_Amount);                                                                                     //Natural: ADD #GROSS-AMOUNT TO #GROSS-AMOUNT-PT
            pnd_State_Totals_Pnd_Gross_Amount_St.nadd(pnd_W2_Record_Pnd_Gross_Amount);                                                                                    //Natural: ADD #GROSS-AMOUNT TO #GROSS-AMOUNT-ST
            pnd_Gross_Amount_Opia_Ct.getValue(pnd_W2_Record_Pnd_W2_Op_Ia_Ind).nadd(pnd_W2_Record_Pnd_Gross_Amount);                                                       //Natural: ADD #GROSS-AMOUNT TO #GROSS-AMOUNT-OPIA-CT ( #W2-OP-IA-IND )
            pnd_Gross_Amount_Opia_Gt.getValue(pnd_W2_Record_Pnd_W2_Op_Ia_Ind).nadd(pnd_W2_Record_Pnd_Gross_Amount);                                                       //Natural: ADD #GROSS-AMOUNT TO #GROSS-AMOUNT-OPIA-GT ( #W2-OP-IA-IND )
            pnd_Part_Totals_Pnd_Federal_Tax_Pt.nadd(pnd_W2_Record_Pnd_Federal_Tax);                                                                                       //Natural: ADD #FEDERAL-TAX TO #FEDERAL-TAX-PT
            pnd_State_Totals_Pnd_Federal_Tax_St.nadd(pnd_W2_Record_Pnd_Federal_Tax);                                                                                      //Natural: ADD #FEDERAL-TAX TO #FEDERAL-TAX-ST
            pnd_Federal_Tax_Opia_Ct.getValue(pnd_W2_Record_Pnd_W2_Op_Ia_Ind).nadd(pnd_W2_Record_Pnd_Federal_Tax);                                                         //Natural: ADD #FEDERAL-TAX TO #FEDERAL-TAX-OPIA-CT ( #W2-OP-IA-IND )
            pnd_Federal_Tax_Opia_Gt.getValue(pnd_W2_Record_Pnd_W2_Op_Ia_Ind).nadd(pnd_W2_Record_Pnd_Federal_Tax);                                                         //Natural: ADD #FEDERAL-TAX TO #FEDERAL-TAX-OPIA-GT ( #W2-OP-IA-IND )
            pnd_Part_Totals_Pnd_State_Tax_Pt.nadd(pnd_W2_Record_Pnd_State_Tax);                                                                                           //Natural: ADD #STATE-TAX TO #STATE-TAX-PT
            pnd_State_Totals_Pnd_State_Tax_St.nadd(pnd_W2_Record_Pnd_State_Tax);                                                                                          //Natural: ADD #STATE-TAX TO #STATE-TAX-ST
            pnd_State_Tax_Opia_Ct.getValue(pnd_W2_Record_Pnd_W2_Op_Ia_Ind).nadd(pnd_W2_Record_Pnd_State_Tax);                                                             //Natural: ADD #STATE-TAX TO #STATE-TAX-OPIA-CT ( #W2-OP-IA-IND )
            pnd_State_Tax_Opia_Gt.getValue(pnd_W2_Record_Pnd_W2_Op_Ia_Ind).nadd(pnd_W2_Record_Pnd_State_Tax);                                                             //Natural: ADD #STATE-TAX TO #STATE-TAX-OPIA-GT ( #W2-OP-IA-IND )
            readWork01Pnd_Tax_Id_NbrOld.setValue(pnd_W2_Record_Pnd_Tax_Id_Nbr);                                                                                           //Natural: END-WORK
            readWork01Pnd_State_IdOld.setValue(pnd_W2_Record_Pnd_State_Id);
            readWork01Pnd_NameOld.setValue(pnd_W2_Record_Pnd_Name);
            readWork01Pnd_Address_1Old.setValue(pnd_W2_Record_Pnd_Address_1);
            readWork01Pnd_Address_2Old.setValue(pnd_W2_Record_Pnd_Address_2);
            readWork01Pnd_Address_3Old.setValue(pnd_W2_Record_Pnd_Address_3);
            readWork01Pnd_Address_4Old.setValue(pnd_W2_Record_Pnd_Address_4);
            readWork01Pnd_Address_5Old.setValue(pnd_W2_Record_Pnd_Address_5);
            readWork01Pnd_Company_CodeOld.setValue(pnd_W2_Record_Pnd_Company_Code);
            readWork01Pnd_Tax_Year_NOld.setValue(pnd_W2_Record_Pnd_Tax_Year_N);
            readWork01Pnd_CompanyOld.setValue(pnd_Company_Line_Pnd_Company);
            readWork01Pnd_W2_Op_Ia_IndOld.setValue(pnd_W2_Record_Pnd_W2_Op_Ia_Ind);
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-GRAND-TOTALS
        sub_Write_Grand_Totals();
        if (condition(Global.isEscape())) {return;}
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-W2-REPORT-01
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-W2-REPORT-02
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COMPANY
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-GRAND-TOTALS
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CONTROL-RECORD
        //* **
    }
    private void sub_Write_W2_Report_01() throws Exception                                                                                                                //Natural: WRITE-W2-REPORT-01
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        if (condition(getReports().getAstLineCount(1).greater(54)))                                                                                                       //Natural: IF *LINE-COUNT ( 01 ) > 54
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 01 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cntrct_Nbr.reset();                                                                                                                                           //Natural: RESET #CNTRCT-NBR
        pnd_Cntrct_Nbr_Pnd_Ppcn.setValue(pnd_W2_Record_Pnd_Contract_Nbr);                                                                                                 //Natural: MOVE #CONTRACT-NBR TO #PPCN
        if (condition(pnd_W2_Record_Pnd_W2_Op_Ia_Ind.equals(2)))                                                                                                          //Natural: IF #W2-OP-IA-IND = 2
        {
            pnd_Cntrct_Nbr_Pnd_Xx.setValue("*");                                                                                                                          //Natural: MOVE '*' TO #XX
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_W2_Record_Pnd_W2_Op_Ia_Ind.equals(5)))                                                                                                          //Natural: IF #W2-OP-IA-IND = 5
        {
            pnd_Cntrct_Nbr_Pnd_Xx.setValue("@");                                                                                                                          //Natural: MOVE '@' TO #XX
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_W2_Record_Pnd_Tax_Id_Nbr, new ReportEditMask ("XXX-XX-XXXX"),new TabSetting(13),pnd_W2_Record_Pnd_Name,  //Natural: WRITE ( 01 ) / 1T #TAX-ID-NBR ( EM = XXX-XX-XXXX ) 13T #NAME ( AL = 30 ) 44T #CNTRCT-NBR #PAYEE-CDE 58T #GROSS-AMOUNT ( EM = -ZZZ,ZZZ,ZZ9.99 ) 76T #FEDERAL-TAX ( EM = -Z,ZZZ,ZZ9.99 ) 90T #GROSS-AMOUNT ( EM = -ZZZ,ZZZ,ZZ9.99 ) 109T #STATE-ID ( EM = XX ) 118T #GROSS-AMOUNT ( EM = -ZZZ,ZZZ,ZZ9.99 )
            new AlphanumericLength (30),new TabSetting(44),pnd_Cntrct_Nbr,pnd_W2_Record_Pnd_Payee_Cde,new TabSetting(58),pnd_W2_Record_Pnd_Gross_Amount, 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(76),pnd_W2_Record_Pnd_Federal_Tax, new ReportEditMask ("-Z,ZZZ,ZZ9.99"),new TabSetting(90),pnd_W2_Record_Pnd_Gross_Amount, 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(109),pnd_W2_Record_Pnd_State_Id, new ReportEditMask ("XX"),new TabSetting(118),pnd_W2_Record_Pnd_Gross_Amount, 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        short decideConditionsMet305 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #ADDRESS-1 NE ' '
        if (condition(pnd_W2_Record_Pnd_Address_1.notEquals(" ")))
        {
            decideConditionsMet305++;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(13),pnd_W2_Record_Pnd_Address_1,new TabSetting(120),pnd_W2_Record_Pnd_State_Tax,                    //Natural: WRITE ( 01 ) 13T #ADDRESS-1 120T #STATE-TAX ( EM = -Z,ZZZ,ZZ9.99 )
                new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #ADDRESS-2 NE ' '
        if (condition(pnd_W2_Record_Pnd_Address_2.notEquals(" ")))
        {
            decideConditionsMet305++;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(13),pnd_W2_Record_Pnd_Address_2);                                                                   //Natural: WRITE ( 01 ) 13T #ADDRESS-2
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #ADDRESS-3 NE ' '
        if (condition(pnd_W2_Record_Pnd_Address_3.notEquals(" ")))
        {
            decideConditionsMet305++;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(13),pnd_W2_Record_Pnd_Address_3);                                                                   //Natural: WRITE ( 01 ) 13T #ADDRESS-3
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #ADDRESS-4 NE ' '
        if (condition(pnd_W2_Record_Pnd_Address_4.notEquals(" ")))
        {
            decideConditionsMet305++;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(13),pnd_W2_Record_Pnd_Address_4);                                                                   //Natural: WRITE ( 01 ) 13T #ADDRESS-4
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #ADDRESS-5 NE ' '
        if (condition(pnd_W2_Record_Pnd_Address_5.notEquals(" ")))
        {
            decideConditionsMet305++;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(13),pnd_W2_Record_Pnd_Address_5);                                                                   //Natural: WRITE ( 01 ) 13T #ADDRESS-5
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet305 > 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet305 == 0))
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(120),pnd_W2_Record_Pnd_State_Tax, new ReportEditMask ("-Z,ZZZ,ZZ9.99"));                            //Natural: WRITE ( 01 ) 120T #STATE-TAX ( EM = -Z,ZZZ,ZZ9.99 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  ADD 1 TO #OPIA-COUNT-CT (#W2-OP-IA-IND)
        //*  ADD 1 TO #OPIA-COUNT-GT (#W2-OP-IA-IND)
        //*  ADD 1 TO #STATE-COUNT
        //*  ADD 1 TO #COMP-COUNT
        //*  ADD 1 TO #GRAND-COUNT
    }
    private void sub_Write_W2_Report_02() throws Exception                                                                                                                //Natural: WRITE-W2-REPORT-02
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        if (condition(getReports().getAstLineCount(2).greater(54)))                                                                                                       //Natural: IF *LINE-COUNT ( 02 ) > 54
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 02 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Part_Totals_Pnd_Gross_Amount_Pt.notEquals(getZero())))                                                                                          //Natural: IF #GROSS-AMOUNT-PT NE 0
        {
            pnd_Comp_Totals_Pnd_Comp_Count.nadd(1);                                                                                                                       //Natural: ADD 1 TO #COMP-COUNT
            pnd_Grand_Totals_Pnd_Grand_Count.nadd(1);                                                                                                                     //Natural: ADD 1 TO #GRAND-COUNT
            pnd_State_Totals_Pnd_State_Count.nadd(1);                                                                                                                     //Natural: ADD 1 TO #STATE-COUNT
            if (condition(pnd_W2_Record_Pnd_W2_Op_Ia_Ind.greater(getZero())))                                                                                             //Natural: IF #W2-OP-IA-IND > 0
            {
                pnd_Opia_Count_Ct.getValue(pnd_W2_Record_Pnd_W2_Op_Ia_Ind).nadd(1);                                                                                       //Natural: ADD 1 TO #OPIA-COUNT-CT ( #W2-OP-IA-IND )
                pnd_Opia_Count_Gt.getValue(pnd_W2_Record_Pnd_W2_Op_Ia_Ind).nadd(1);                                                                                       //Natural: ADD 1 TO #OPIA-COUNT-GT ( #W2-OP-IA-IND )
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_Old_Tax_Id, new ReportEditMask ("XXX-XX-XXXX"),new TabSetting(13),pnd_Old_Name,      //Natural: WRITE ( 02 ) / 1T #OLD-TAX-ID ( EM = XXX-XX-XXXX ) 13T #OLD-NAME ( AL = 30 ) 44T #GROSS-AMOUNT-PT ( EM = -ZZZ,ZZZ,ZZ9.99 ) 62T #FEDERAL-TAX-PT ( EM = -ZZ,ZZZ,ZZ9.99 ) 77T #GROSS-AMOUNT-PT ( EM = -ZZZ,ZZZ,ZZ9.99 ) 95T #OLD-STATE-ID ( EM = XX ) 98T #GROSS-AMOUNT-PT ( EM = -ZZZ,ZZZ,ZZ9.99 ) 118T #STATE-TAX-PT ( EM = -Z,ZZZ,ZZ9.99 )
                new AlphanumericLength (30),new TabSetting(44),pnd_Part_Totals_Pnd_Gross_Amount_Pt, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(62),pnd_Part_Totals_Pnd_Federal_Tax_Pt, 
                new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_Part_Totals_Pnd_Gross_Amount_Pt, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(95),pnd_Old_State_Id, new ReportEditMask ("XX"),new TabSetting(98),pnd_Part_Totals_Pnd_Gross_Amount_Pt, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(118),pnd_Part_Totals_Pnd_State_Tax_Pt, new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            short decideConditionsMet346 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #OLD-ADDR-1 NE ' '
            if (condition(pnd_Old_Addr_1.notEquals(" ")))
            {
                decideConditionsMet346++;
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(13),pnd_Old_Addr_1);                                                                            //Natural: WRITE ( 02 ) 13T #OLD-ADDR-1
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: WHEN #OLD-ADDR-2 NE ' '
            if (condition(pnd_Old_Addr_2.notEquals(" ")))
            {
                decideConditionsMet346++;
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(13),pnd_Old_Addr_2);                                                                            //Natural: WRITE ( 02 ) 13T #OLD-ADDR-2
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: WHEN #OLD-ADDR-3 NE ' '
            if (condition(pnd_Old_Addr_3.notEquals(" ")))
            {
                decideConditionsMet346++;
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(13),pnd_Old_Addr_3);                                                                            //Natural: WRITE ( 02 ) 13T #OLD-ADDR-3
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: WHEN #OLD-ADDR-4 NE ' '
            if (condition(pnd_Old_Addr_4.notEquals(" ")))
            {
                decideConditionsMet346++;
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(13),pnd_Old_Addr_4);                                                                            //Natural: WRITE ( 02 ) 13T #OLD-ADDR-4
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: WHEN #OLD-ADDR-5 NE ' '
            if (condition(pnd_Old_Addr_5.notEquals(" ")))
            {
                decideConditionsMet346++;
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(13),pnd_Old_Addr_5);                                                                            //Natural: WRITE ( 02 ) 13T #OLD-ADDR-5
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet346 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Part_Totals.reset();                                                                                                                                          //Natural: RESET #PART-TOTALS
    }
    private void sub_Process_Company() throws Exception                                                                                                                   //Natural: PROCESS-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        DbsUtil.callnat(Twrncomp.class , getCurrentProcessState(), pdaTwracomp.getPnd_Twracomp_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracomp.getPnd_Twracomp_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOMP' USING #TWRACOMP.#INPUT-PARMS ( AD = O ) #TWRACOMP.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        pnd_Company_Line.setValue("Company:");                                                                                                                            //Natural: ASSIGN #COMPANY-LINE := 'Company:'
        setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),pnd_Company_Line,11,4);                                                                     //Natural: MOVE #TWRACOMP.#COMP-SHORT-NAME TO SUBSTR ( #COMPANY-LINE,11,4 )
        setValueToSubstring("-",pnd_Company_Line,16,1);                                                                                                                   //Natural: MOVE '-' TO SUBSTR ( #COMPANY-LINE,16,1 )
        setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Name(),pnd_Company_Line,18,55);                                                                          //Natural: MOVE #TWRACOMP.#COMP-NAME TO SUBSTR ( #COMPANY-LINE,18,55 )
    }
    private void sub_Write_Grand_Totals() throws Exception                                                                                                                //Natural: WRITE-GRAND-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        pnd_Company_Line.reset();                                                                                                                                         //Natural: RESET #COMPANY-LINE
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(13),"Total Form Count Overall  ",new TabSetting(43),pnd_Grand_Totals_Pnd_Grand_Count,           //Natural: WRITE ( 01 ) / 13T 'Total Form Count Overall  ' 43T #GRAND-COUNT ( EM = ZZZ,ZZ9 ) 58T #GROSS-AMOUNT-GT ( EM = -ZZZ,ZZZ,ZZ9.99 ) 74T #FEDERAL-TAX-GT ( EM = -ZZZ,ZZZ,ZZ9.99 ) 90T #GROSS-AMOUNT-GT ( EM = -ZZZ,ZZZ,ZZ9.99 ) 118T #GROSS-AMOUNT-GT ( EM = -ZZZ,ZZZ,ZZ9.99 ) / 118T #STATE-TAX-GT ( EM = -ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(58),pnd_Grand_Totals_Pnd_Gross_Amount_Gt, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(74),pnd_Grand_Totals_Pnd_Federal_Tax_Gt, 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Grand_Totals_Pnd_Gross_Amount_Gt, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(118),pnd_Grand_Totals_Pnd_Gross_Amount_Gt, 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(118),pnd_Grand_Totals_Pnd_State_Tax_Gt, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"Total Form Count Overall  ",new TabSetting(31),pnd_Grand_Totals_Pnd_Grand_Count,            //Natural: WRITE ( 02 ) / 1T 'Total Form Count Overall  ' 31T #GRAND-COUNT ( EM = ZZZ,ZZ9 ) 44T #GROSS-AMOUNT-GT ( EM = -ZZZ,ZZZ,ZZ9.99 ) 62T #FEDERAL-TAX-GT ( EM = -ZZ,ZZZ,ZZ9.99 ) 77T #GROSS-AMOUNT-GT ( EM = -ZZZ,ZZZ,ZZ9.99 ) 98T #GROSS-AMOUNT-GT ( EM = -ZZZ,ZZZ,ZZ9.99 ) 116T #STATE-TAX-GT ( EM = -ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(44),pnd_Grand_Totals_Pnd_Gross_Amount_Gt, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(62),pnd_Grand_Totals_Pnd_Federal_Tax_Gt, 
            new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_Grand_Totals_Pnd_Gross_Amount_Gt, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(98),pnd_Grand_Totals_Pnd_Gross_Amount_Gt, 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(116),pnd_Grand_Totals_Pnd_State_Tax_Gt, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 5
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
        {
            if (condition(pnd_Opia_Count_Gt.getValue(pnd_I).greater(getZero())))                                                                                          //Natural: IF #OPIA-COUNT-GT ( #I ) > 0
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(13),"Total Form Count for",pnd_Type_Pnd_Pmnt.getValue(pnd_I),new TabSetting(43),pnd_Opia_Count_Gt.getValue(pnd_I),  //Natural: WRITE ( 01 ) 13T 'Total Form Count for' #PMNT ( #I ) 43T #OPIA-COUNT-GT ( #I ) ( EM = ZZZ,ZZ9 ) 58T #GROSS-AMOUNT-OPIA-GT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 74T #FEDERAL-TAX-OPIA-GT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 90T #GROSS-AMOUNT-OPIA-GT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 118T #GROSS-AMOUNT-OPIA-GT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) / 118T #STATE-TAX-OPIA-GT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(58),pnd_Gross_Amount_Opia_Gt.getValue(pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
                    TabSetting(74),pnd_Federal_Tax_Opia_Gt.getValue(pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Gross_Amount_Opia_Gt.getValue(pnd_I), 
                    new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(118),pnd_Gross_Amount_Opia_Gt.getValue(pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
                    TabSetting(118),pnd_State_Tax_Opia_Gt.getValue(pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"Total Form Count for",pnd_Type_Pnd_Pmnt.getValue(pnd_I),new TabSetting(31),pnd_Opia_Count_Gt.getValue(pnd_I),  //Natural: WRITE ( 02 ) 1T 'Total Form Count for' #PMNT ( #I ) 31T #OPIA-COUNT-GT ( #I ) ( EM = ZZZ,ZZ9 ) 44T #GROSS-AMOUNT-OPIA-GT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 62T #FEDERAL-TAX-OPIA-GT ( #I ) ( EM = -ZZ,ZZZ,ZZ9.99 ) 77T #GROSS-AMOUNT-OPIA-GT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 98T #GROSS-AMOUNT-OPIA-GT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 116T #STATE-TAX-OPIA-GT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(44),pnd_Gross_Amount_Opia_Gt.getValue(pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
                    TabSetting(62),pnd_Federal_Tax_Opia_Gt.getValue(pnd_I), new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_Gross_Amount_Opia_Gt.getValue(pnd_I), 
                    new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(98),pnd_Gross_Amount_Opia_Gt.getValue(pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
                    TabSetting(116),pnd_State_Tax_Opia_Gt.getValue(pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Process_Control_Record() throws Exception                                                                                                            //Natural: PROCESS-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        getWorkFiles().read(2, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                             //Natural: READ WORK FILE 2 ONCE #TWRP0600-CONTROL-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            DbsUtil.terminateApplication("External Subroutine ERROR_DISPLAY_START is missing from the collection!");                                                      //Natural: PERFORM ERROR-DISPLAY-START
            getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"CONTROL RECORD IS EMPTY",new TabSetting(77),"***");                                      //Natural: WRITE ( 01 ) '***' 25T 'CONTROL RECORD IS EMPTY' 77T '***'
            if (Global.isEscape()) return;
            DbsUtil.terminateApplication("External Subroutine ERROR_DISPLAY_END is missing from the collection!");                                                        //Natural: PERFORM ERROR-DISPLAY-END
            DbsUtil.terminate(101);  if (true) return;                                                                                                                    //Natural: TERMINATE 101
        }                                                                                                                                                                 //Natural: END-ENDFILE
        getReports().write(0, "FROM DATE",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd());                                                       //Natural: WRITE 'FROM DATE' #TWRP0600-FROM-CCYYMMDD
        if (Global.isEscape()) return;
        getReports().write(0, "TO DATE",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd());                                                           //Natural: WRITE 'TO DATE' #TWRP0600-TO-CCYYMMDD
        if (Global.isEscape()) return;
        getReports().write(0, "INTF DATE",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd());                                                  //Natural: WRITE 'INTF DATE' #TWRP0600-INTERFACE-CCYYMMDD
        if (Global.isEscape()) return;
        pnd_Monthly_Date.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd());                                                          //Natural: MOVE #TWRP0600-INTERFACE-CCYYMMDD TO #MONTHLY-DATE
        if (condition(pnd_Monthly_Date_Pnd_Dd.less(26)))                                                                                                                  //Natural: IF #DD < 26
        {
            if (condition(pnd_Monthly_Date_Pnd_Mm.equals(1)))                                                                                                             //Natural: IF #MM = 1
            {
                pnd_Monthly_Date_Pnd_Yyyy.nsubtract(1);                                                                                                                   //Natural: COMPUTE #YYYY = #YYYY - 1
                pnd_Monthly_Date_Pnd_Mm.setValue(12);                                                                                                                     //Natural: MOVE 12 TO #MM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Monthly_Date_Pnd_Mm.nsubtract(1);                                                                                                                     //Natural: COMPUTE #MM = #MM - 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Monthly_Date_Pnd_Dd.setValue(1);                                                                                                                              //Natural: MOVE 1 TO #DD
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd().setValue(pnd_Monthly_Date);                                                               //Natural: MOVE #MONTHLY-DATE TO #TWRP0600-FROM-CCYYMMDD
        pnd_Monthly_Date_Pnd_Dd.setValue(31);                                                                                                                             //Natural: MOVE 31 TO #DD
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd().setValue(pnd_Monthly_Date);                                                                 //Natural: MOVE #MONTHLY-DATE TO #TWRP0600-TO-CCYYMMDD
        //*  #HOLD-TAX-YEAR := #TWRP0600-TAX-YEAR-CCYY
        getReports().write(0, "NEW FROM DATE",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd());                                                   //Natural: WRITE 'NEW FROM DATE' #TWRP0600-FROM-CCYYMMDD
        if (Global.isEscape()) return;
        getReports().write(0, "NEW TO   DATE",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd());                                                     //Natural: WRITE 'NEW TO   DATE' #TWRP0600-TO-CCYYMMDD
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,NEWLINE,"TO/FROM DATE",pnd_Monthly_Date,NEWLINE,NEWLINE);                                                                           //Natural: WRITE // 'TO/FROM DATE' #MONTHLY-DATE //
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(63),"TaxWaRS",new     //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 45T #REPORT-TITLE-01 120T 'Report: RPT1' / 60T 'Tax Year:' #HOLD-TAX-YEAR / #COMPANY-LINE // 3T 'Social' 15T 'Name and Address' 45T 'Account' 66T 'Box 1' 80T 'Box 2' 97T 'Box 11' 107T 'Box 15' 117T 'Box 16/Gross Amt' / 1T 'Security No' 45T 'Number' 64T 'Gross Amt' 78T 'Federal Tax' 96T 'Gross Amt' 106T '--State--' 117T 'Box 17/State Tax'
                        TabSetting(120),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                        TabSetting(45),pnd_Report_Title_01,new TabSetting(120),"Report: RPT1",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Hold_Tax_Year,NEWLINE,pnd_Company_Line,NEWLINE,NEWLINE,new 
                        TabSetting(3),"Social",new TabSetting(15),"Name and Address",new TabSetting(45),"Account",new TabSetting(66),"Box 1",new TabSetting(80),"Box 2",new 
                        TabSetting(97),"Box 11",new TabSetting(107),"Box 15",new TabSetting(117),"Box 16/Gross Amt",NEWLINE,new TabSetting(1),"Security No",new 
                        TabSetting(45),"Number",new TabSetting(64),"Gross Amt",new TabSetting(78),"Federal Tax",new TabSetting(96),"Gross Amt",new TabSetting(106),"--State--",new 
                        TabSetting(117),"Box 17/State Tax");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(63),"TaxWaRS",new     //Natural: WRITE ( 02 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 41T #REPORT-TITLE-02 120T 'Report: RPT2' / 60T 'Tax Year:' #HOLD-TAX-YEAR / #COMPANY-LINE // 3T 'Social' 15T 'Name and Address' 52T 'Box 1' 68T 'Box 2' 84T 'Box 11' 94T 'Box 15' 105T 'Box 16' 123T 'Box 17' / 1T 'Security No' 50T 'Gross Amt' 65T 'Federal Tax' 83T 'Gross Amt' 94T 'State' 104T 'Gross Amt' 122T 'State Tax'
                        TabSetting(120),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                        TabSetting(41),pnd_Report_Title_02,new TabSetting(120),"Report: RPT2",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Hold_Tax_Year,NEWLINE,pnd_Company_Line,NEWLINE,NEWLINE,new 
                        TabSetting(3),"Social",new TabSetting(15),"Name and Address",new TabSetting(52),"Box 1",new TabSetting(68),"Box 2",new TabSetting(84),"Box 11",new 
                        TabSetting(94),"Box 15",new TabSetting(105),"Box 16",new TabSetting(123),"Box 17",NEWLINE,new TabSetting(1),"Security No",new TabSetting(50),"Gross Amt",new 
                        TabSetting(65),"Federal Tax",new TabSetting(83),"Gross Amt",new TabSetting(94),"State",new TabSetting(104),"Gross Amt",new TabSetting(122),
                        "State Tax");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atEndEventRpt1 = (Object sender, EventArgs e) ->
    {
        String localMethod = "TWRP5715|atEndEventRpt1";
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,"* - IA Contract,  @ - Both OP & IA payments");                                                            //Natural: WRITE ( 01 ) '* - IA Contract,  @ - Both OP & IA payments'
                }                                                                                                                                                         //Natural: END-ENDPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_W2_Record_Pnd_Tax_Id_NbrIsBreak = pnd_W2_Record_Pnd_Tax_Id_Nbr.isBreak(endOfData);
        boolean pnd_W2_Record_Pnd_Residency_CodeIsBreak = pnd_W2_Record_Pnd_Residency_Code.isBreak(endOfData);
        boolean pnd_W2_Record_Pnd_Company_CodeIsBreak = pnd_W2_Record_Pnd_Company_Code.isBreak(endOfData);
        if (condition(pnd_W2_Record_Pnd_Tax_Id_NbrIsBreak || pnd_W2_Record_Pnd_Residency_CodeIsBreak || pnd_W2_Record_Pnd_Company_CodeIsBreak))
        {
            pnd_Old_Tax_Id.setValue(readWork01Pnd_Tax_Id_NbrOld);                                                                                                         //Natural: ASSIGN #OLD-TAX-ID := OLD ( #TAX-ID-NBR )
            pnd_Old_State_Id.setValue(readWork01Pnd_State_IdOld);                                                                                                         //Natural: ASSIGN #OLD-STATE-ID := OLD ( #STATE-ID )
            pnd_Old_Name.setValue(readWork01Pnd_NameOld);                                                                                                                 //Natural: ASSIGN #OLD-NAME := OLD ( #NAME )
            pnd_Old_Addr_1.setValue(readWork01Pnd_Address_1Old);                                                                                                          //Natural: ASSIGN #OLD-ADDR-1 := OLD ( #ADDRESS-1 )
            pnd_Old_Addr_2.setValue(readWork01Pnd_Address_2Old);                                                                                                          //Natural: ASSIGN #OLD-ADDR-2 := OLD ( #ADDRESS-2 )
            pnd_Old_Addr_3.setValue(readWork01Pnd_Address_3Old);                                                                                                          //Natural: ASSIGN #OLD-ADDR-3 := OLD ( #ADDRESS-3 )
            pnd_Old_Addr_4.setValue(readWork01Pnd_Address_4Old);                                                                                                          //Natural: ASSIGN #OLD-ADDR-4 := OLD ( #ADDRESS-4 )
            pnd_Old_Addr_5.setValue(readWork01Pnd_Address_5Old);                                                                                                          //Natural: ASSIGN #OLD-ADDR-5 := OLD ( #ADDRESS-5 )
                                                                                                                                                                          //Natural: PERFORM WRITE-W2-REPORT-02
            sub_Write_W2_Report_02();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_W2_Record_Pnd_Residency_CodeIsBreak || pnd_W2_Record_Pnd_Company_CodeIsBreak))
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(13),"Total Form Count for State",readWork01Pnd_State_IdOld, new AlphanumericLength          //Natural: WRITE ( 01 ) / 13T 'Total Form Count for State' OLD ( #STATE-ID ) ( AL = 2 ) 43T #STATE-COUNT ( EM = ZZZ,ZZ9 ) 58T #GROSS-AMOUNT-ST ( EM = -ZZZ,ZZZ,ZZ9.99 ) 74T #FEDERAL-TAX-ST ( EM = -ZZZ,ZZZ,ZZ9.99 ) 90T #GROSS-AMOUNT-ST ( EM = -ZZZ,ZZZ,ZZ9.99 ) 118T #GROSS-AMOUNT-ST ( EM = -ZZZ,ZZZ,ZZ9.99 ) / 118T #STATE-TAX-ST ( EM = -ZZZ,ZZZ,ZZ9.99 )
                (2),new TabSetting(43),pnd_State_Totals_Pnd_State_Count, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(58),pnd_State_Totals_Pnd_Gross_Amount_St, 
                new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(74),pnd_State_Totals_Pnd_Federal_Tax_St, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(90),pnd_State_Totals_Pnd_Gross_Amount_St, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(118),pnd_State_Totals_Pnd_Gross_Amount_St, 
                new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(118),pnd_State_Totals_Pnd_State_Tax_St, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape())) return;
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"Total Form Count for State",readWork01Pnd_State_IdOld, new AlphanumericLength           //Natural: WRITE ( 02 ) / 1T 'Total Form Count for State' OLD ( #STATE-ID ) ( AL = 2 ) 31T #STATE-COUNT ( EM = ZZZ,ZZ9 ) 44T #GROSS-AMOUNT-ST ( EM = -ZZZ,ZZZ,ZZ9.99 ) 62T #FEDERAL-TAX-ST ( EM = -ZZ,ZZZ,ZZ9.99 ) 77T #GROSS-AMOUNT-ST ( EM = -ZZZ,ZZZ,ZZ9.99 ) 98T #GROSS-AMOUNT-ST ( EM = -ZZZ,ZZZ,ZZ9.99 ) 116T #STATE-TAX-ST ( EM = -ZZZ,ZZZ,ZZ9.99 )
                (2),new TabSetting(31),pnd_State_Totals_Pnd_State_Count, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(44),pnd_State_Totals_Pnd_Gross_Amount_St, 
                new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(62),pnd_State_Totals_Pnd_Federal_Tax_St, new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),new 
                TabSetting(77),pnd_State_Totals_Pnd_Gross_Amount_St, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(98),pnd_State_Totals_Pnd_Gross_Amount_St, 
                new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(116),pnd_State_Totals_Pnd_State_Tax_St, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape())) return;
            pnd_Comp_Totals_Pnd_Gross_Amount_Ct.nadd(pnd_State_Totals_Pnd_Gross_Amount_St);                                                                               //Natural: ADD #GROSS-AMOUNT-ST TO #GROSS-AMOUNT-CT
            pnd_Comp_Totals_Pnd_Gross_Amount_Ct.nadd(pnd_State_Totals_Pnd_Gross_Amount_St);                                                                               //Natural: ADD #GROSS-AMOUNT-ST TO #GROSS-AMOUNT-CT
            pnd_Comp_Totals_Pnd_Federal_Tax_Ct.nadd(pnd_State_Totals_Pnd_Federal_Tax_St);                                                                                 //Natural: ADD #FEDERAL-TAX-ST TO #FEDERAL-TAX-CT
            pnd_Comp_Totals_Pnd_State_Tax_Ct.nadd(pnd_State_Totals_Pnd_State_Tax_St);                                                                                     //Natural: ADD #STATE-TAX-ST TO #STATE-TAX-CT
            pnd_State_Totals.reset();                                                                                                                                     //Natural: RESET #STATE-TOTALS
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 01 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 02 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_W2_Record_Pnd_Company_CodeIsBreak))
        {
            pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(readWork01Pnd_Company_CodeOld);                                                                          //Natural: ASSIGN #TWRACOMP.#COMP-CODE := OLD ( #W2-RECORD.#COMPANY-CODE )
            pdaTwracomp.getPnd_Twracomp_Pnd_Tax_Year().setValue(readWork01Pnd_Tax_Year_NOld);                                                                             //Natural: ASSIGN #TWRACOMP.#TAX-YEAR := OLD ( #W2-RECORD.#TAX-YEAR-N )
            if (condition(pnd_Grand_Totals_Pnd_Grand_Count.equals(getZero())))                                                                                            //Natural: IF #GRAND-COUNT = 0
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMPANY
                sub_Process_Company();
                if (condition(Global.isEscape())) {return;}
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(13),"Total Form Count for",readWork01Pnd_CompanyOld,new ColumnSpacing(5),new            //Natural: WRITE ( 01 ) / 13T 'Total Form Count for' OLD ( #COMPANY ) 5X 43T #COMP-COUNT ( EM = ZZZ,ZZ9 ) 58T #GROSS-AMOUNT-CT ( EM = -ZZZ,ZZZ,ZZ9.99 ) 74T #FEDERAL-TAX-CT ( EM = -ZZZ,ZZZ,ZZ9.99 ) 90T #GROSS-AMOUNT-CT ( EM = -ZZZ,ZZZ,ZZ9.99 ) 118T #GROSS-AMOUNT-CT ( EM = -ZZZ,ZZZ,ZZ9.99 ) / 118T #STATE-TAX-CT ( EM = -ZZZ,ZZZ,ZZ9.99 )
                    TabSetting(43),pnd_Comp_Totals_Pnd_Comp_Count, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(58),pnd_Comp_Totals_Pnd_Gross_Amount_Ct, 
                    new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(74),pnd_Comp_Totals_Pnd_Federal_Tax_Ct, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
                    TabSetting(90),pnd_Comp_Totals_Pnd_Gross_Amount_Ct, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(118),pnd_Comp_Totals_Pnd_Gross_Amount_Ct, 
                    new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(118),pnd_Comp_Totals_Pnd_State_Tax_Ct, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape())) return;
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"Total Part/Res Count: ",readWork01Pnd_CompanyOld,new TabSetting(31),pnd_Comp_Totals_Pnd_Comp_Count,  //Natural: WRITE ( 02 ) / 1T 'Total Part/Res Count: ' OLD ( #COMPANY ) 31T #COMP-COUNT ( EM = ZZZ,ZZ9 ) 44T #GROSS-AMOUNT-CT ( EM = -ZZZ,ZZZ,ZZ9.99 ) 62T #FEDERAL-TAX-CT ( EM = -ZZ,ZZZ,ZZ9.99 ) 77T #GROSS-AMOUNT-CT ( EM = -ZZZ,ZZZ,ZZ9.99 ) 98T #GROSS-AMOUNT-CT ( EM = -ZZZ,ZZZ,ZZ9.99 ) 116T #STATE-TAX-CT ( EM = -ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(44),pnd_Comp_Totals_Pnd_Gross_Amount_Ct, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(62),pnd_Comp_Totals_Pnd_Federal_Tax_Ct, 
                    new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_Comp_Totals_Pnd_Gross_Amount_Ct, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
                    TabSetting(98),pnd_Comp_Totals_Pnd_Gross_Amount_Ct, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(116),pnd_Comp_Totals_Pnd_State_Tax_Ct, 
                    new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape())) return;
                if (condition(pnd_W2_Record_Pnd_W2_Op_Ia_Ind.equals(getZero())))                                                                                          //Natural: IF #W2-OP-IA-IND = 0
                {
                    pnd_Hold_Ndx.setValue(readWork01Pnd_W2_Op_Ia_IndOld);                                                                                                 //Natural: ASSIGN #HOLD-NDX := OLD ( #W2-OP-IA-IND )
                    pnd_Opia_Count_Ct.getValue(pnd_Hold_Ndx).nadd(1);                                                                                                     //Natural: ADD 1 TO #OPIA-COUNT-CT ( #HOLD-NDX )
                    pnd_Opia_Count_Gt.getValue(pnd_Hold_Ndx).nadd(1);                                                                                                     //Natural: ADD 1 TO #OPIA-COUNT-GT ( #HOLD-NDX )
                }                                                                                                                                                         //Natural: END-IF
                FOR01:                                                                                                                                                    //Natural: FOR #I 1 5
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
                {
                    if (condition(pnd_Opia_Count_Ct.getValue(pnd_I).greater(getZero())))                                                                                  //Natural: IF #OPIA-COUNT-CT ( #I ) > 0
                    {
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(13),"Total Form Count for",pnd_Type_Pnd_Pmnt.getValue(pnd_I),new ColumnSpacing(4),new   //Natural: WRITE ( 01 ) 13T 'Total Form Count for' #PMNT ( #I ) 4X 43T #OPIA-COUNT-CT ( #I ) ( EM = ZZZ,ZZ9 ) 58T #GROSS-AMOUNT-OPIA-CT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 74T #FEDERAL-TAX-OPIA-CT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 90T #GROSS-AMOUNT-OPIA-CT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 118T #GROSS-AMOUNT-OPIA-CT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) / 118T #STATE-TAX-OPIA-CT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9.99 )
                            TabSetting(43),pnd_Opia_Count_Ct.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(58),pnd_Gross_Amount_Opia_Ct.getValue(pnd_I), 
                            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(74),pnd_Federal_Tax_Opia_Ct.getValue(pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
                            TabSetting(90),pnd_Gross_Amount_Opia_Ct.getValue(pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(118),pnd_Gross_Amount_Opia_Ct.getValue(pnd_I), 
                            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(118),pnd_State_Tax_Opia_Ct.getValue(pnd_I), new ReportEditMask 
                            ("-ZZZ,ZZZ,ZZ9.99"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"Total Part/Res Count: ",pnd_Type_Pnd_Pmnt.getValue(pnd_I),new TabSetting(31),pnd_Opia_Count_Ct.getValue(pnd_I),  //Natural: WRITE ( 02 ) 1T 'Total Part/Res Count: ' #PMNT ( #I ) 31T #OPIA-COUNT-CT ( #I ) ( EM = ZZZ,ZZ9 ) 44T #GROSS-AMOUNT-OPIA-CT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 62T #FEDERAL-TAX-OPIA-CT ( #I ) ( EM = -ZZ,ZZZ,ZZ9.99 ) 77T #GROSS-AMOUNT-OPIA-CT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 98T #GROSS-AMOUNT-OPIA-CT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 116T #STATE-TAX-OPIA-CT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9.99 )
                            new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(44),pnd_Gross_Amount_Opia_Ct.getValue(pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
                            TabSetting(62),pnd_Federal_Tax_Opia_Ct.getValue(pnd_I), new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_Gross_Amount_Opia_Ct.getValue(pnd_I), 
                            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(98),pnd_Gross_Amount_Opia_Ct.getValue(pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
                            TabSetting(116),pnd_State_Tax_Opia_Ct.getValue(pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_Grand_Totals_Pnd_Gross_Amount_Gt.nadd(pnd_Comp_Totals_Pnd_Gross_Amount_Ct);                                                                               //Natural: ADD #GROSS-AMOUNT-CT TO #GROSS-AMOUNT-GT
            pnd_Grand_Totals_Pnd_Federal_Tax_Gt.nadd(pnd_Comp_Totals_Pnd_Federal_Tax_Ct);                                                                                 //Natural: ADD #FEDERAL-TAX-CT TO #FEDERAL-TAX-GT
            pnd_Grand_Totals_Pnd_State_Tax_Gt.nadd(pnd_Comp_Totals_Pnd_State_Tax_Ct);                                                                                     //Natural: ADD #STATE-TAX-CT TO #STATE-TAX-GT
            pnd_Comp_Totals.reset();                                                                                                                                      //Natural: RESET #COMP-TOTALS #OPIA-COUNT-CT ( * ) #GROSS-AMOUNT-OPIA-CT ( * ) #FEDERAL-TAX-OPIA-CT ( * ) #STATE-TAX-OPIA-CT ( * )
            pnd_Opia_Count_Ct.getValue("*").reset();
            pnd_Gross_Amount_Opia_Ct.getValue("*").reset();
            pnd_Federal_Tax_Opia_Ct.getValue("*").reset();
            pnd_State_Tax_Opia_Ct.getValue("*").reset();
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 01 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 02 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=58 LS=133 ZP=ON");
        Global.format(2, "PS=58 LS=133 ZP=ON");
    }
}
