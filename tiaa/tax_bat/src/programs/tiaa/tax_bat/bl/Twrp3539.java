/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:36:27 PM
**        * FROM NATURAL PROGRAM : Twrp3539
************************************************************
**        * FILE NAME            : Twrp3539.java
**        * CLASS NAME           : Twrp3539
**        * INSTANCE NAME        : Twrp3539
************************************************************
************************************************************************
** PROGRAM : TWRP3539
** SYSTEM  : TAXWARS
** AUTHOR  : TED PROIMOS
** FUNCTION: STATES CORRECTION REPORTING / RECONCILIATION
**
**
** HISTORY.....:
** 08/13/02  J.ROTHOLZ - RECOMPILED DUE TO INCREASE IN #COMP-NAME
**           LENGTH IN TWRLCOMP & TWRACOMP
** 08/16/02  J.ROTHOLZ - RE-STOWED TO PICK UP CHANGES IN TWRAFORM
** 12/06/05 - BK. TAX YEAR ADDED TO TWRNCOMP PARMS
** 12/04/2020 - RE-STOW COMPONENT FOR 5498 IRS REPORTING  2020
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3539 extends BLNatBase
{
    // Data Areas
    private PdaTwracomp pdaTwracomp;
    private PdaTwraform pdaTwraform;
    private PdaTwratbl2 pdaTwratbl2;
    private PdaTwratbl4 pdaTwratbl4;
    private PdaTwratin pdaTwratin;
    private LdaTwrl9710 ldaTwrl9710;
    private LdaTwrl9715 ldaTwrl9715;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_form_U;
    private DbsField form_U_Tirf_Lu_User;
    private DbsField form_U_Tirf_Lu_Ts;
    private DbsField form_U_Count_Casttirf_1099_R_State_Grp;

    private DbsGroup form_U_Tirf_1099_R_State_Grp;
    private DbsField form_U_Tirf_State_Hardcopy_Ind;
    private DbsField form_U_Tirf_State_Code;
    private DbsField form_U_Tirf_State_Auth_Rpt_Ind;
    private DbsField form_U_Tirf_State_Auth_Rpt_Date;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;
    private DbsField pnd_Ws_Const_Pnd_State_Max;
    private DbsField pnd_Ws_Const_Pnd_Str_Max_Lines;
    private DbsField pnd_Ws_Const_Pnd_Stw_Max_Lines;
    private DbsField pnd_Ws_Const_Pnd_Stg_Max_Lines;

    private DbsGroup pnd_Hardcopy_Ws;
    private DbsField pnd_Hardcopy_Ws_Pnd_Process_Type;
    private DbsField pnd_Hardcopy_Ws_Pnd_State_Name;
    private DbsField pnd_Hardcopy_Ws_Pnd_Form_Page_Cnt;
    private DbsField pnd_Hardcopy_Ws_Pnd_Ws_Isn;
    private DbsField pnd_Hardcopy_Ws_Pnd_M1_Add1;
    private DbsField pnd_Hardcopy_Ws_Pnd_M1_Add2;
    private DbsField pnd_Hardcopy_Ws_Pnd_M1_Add3;
    private DbsField pnd_Hardcopy_Ws_Pnd_M1_Add4;
    private DbsField pnd_Hardcopy_Ws_Pnd_M1_Add5;
    private DbsField pnd_Hardcopy_Ws_Pnd_M1_Add6;
    private DbsField pnd_Hardcopy_Ws_Pnd_M1_Fullname;
    private DbsField pnd_Hardcopy_Ws_Pnd_M1_Firstname;
    private DbsField pnd_Hardcopy_Ws_Pnd_M1_Lasname;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Input_Parms;
    private DbsField pnd_Ws_Pnd_State;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_Terminate;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_K;
    private DbsField pnd_Ws_Pnd_St_Occ;
    private DbsField pnd_Ws_Pnd_S2;
    private DbsField pnd_Ws_Pnd_Datx;
    private DbsField pnd_Ws_Pnd_Timx;
    private DbsField pnd_Ws_Pnd_Prev_Rpt_Date;
    private DbsField pnd_Ws_Pnd_Valid_State;
    private DbsField pnd_Ws_Pnd_Company_Break;
    private DbsField pnd_Ws_Pnd_Accepted_Form_Cnt;
    private DbsField pnd_Ws_Pnd_Company_Line;
    private DbsField pnd_Ws_Pnd_Disp_Rec;
    private DbsField pnd_Ws_Pnd_S8;
    private DbsField pnd_Ws_Pnd_Empty_Cnt;
    private DbsField pnd_Ws_Pnd_Empty_Tot_Cnt;
    private DbsField pnd_Ws_Pnd_Empty_Frm2;
    private DbsField pnd_Ws_Pnd_Empty_Tot_Frm2;

    private DbsGroup pnd_State_Table;
    private DbsField pnd_State_Table_Tircntl_State_Old_Code;
    private DbsField pnd_State_Table_Tircntl_State_Alpha_Code;
    private DbsField pnd_State_Table_Tircntl_State_Full_Name;
    private DbsField pnd_State_Table_Tircntl_Comb_Fed_Ind;
    private DbsField pnd_State_Table_Tircntl_State_Ind;
    private DbsField pnd_State_Table_Tircntl_Media_Code;
    private DbsField pnd_Sort_Rec;

    private DbsGroup pnd_Sort_Rec__R_Field_1;

    private DbsGroup pnd_Sort_Rec_Pnd_Sort_Rec_Detail;
    private DbsField pnd_Sort_Rec_Tirf_Tax_Year;
    private DbsField pnd_Sort_Rec_Tirf_Form_Type;
    private DbsField pnd_Sort_Rec_Tirf_Company_Cde;
    private DbsField pnd_Sort_Rec_Tirf_Tin;
    private DbsField pnd_Sort_Rec_Tirf_Contract_Nbr;
    private DbsField pnd_Sort_Rec_Tirf_Payee_Cde;
    private DbsField pnd_Sort_Rec_Tirf_Key;
    private DbsField pnd_S1_Start;

    private DbsGroup pnd_S1_Start__R_Field_2;

    private DbsGroup pnd_S1_Start_Pnd_S1_Detail;
    private DbsField pnd_S1_Start_Tirf_Tax_Year;
    private DbsField pnd_S1_Start_Tirf_Tin;
    private DbsField pnd_S1_Start_Tirf_Form_Type;
    private DbsField pnd_S1_Start_Tirf_Contract_Nbr;
    private DbsField pnd_S1_Start_Tirf_Payee_Cde;
    private DbsField pnd_S1_Start_Tirf_Key;
    private DbsField pnd_S1_End;

    private DbsGroup pnd_Case_Fields2;
    private DbsField pnd_Case_Fields2_Pnd_Recon_Only;
    private DbsField pnd_Case_Fields2_Pnd_Recon_And_Reporting;

    private DbsGroup pnd_Case_Fields;
    private DbsField pnd_Case_Fields_Pnd_St_Occ_A;
    private DbsField pnd_Case_Fields_Pnd_St_Occ_I;
    private DbsField pnd_Case_Fields_Pnd_St_Max;
    private DbsField pnd_Case_Fields_Pnd_Active_Isn;
    private DbsField pnd_Case_Fields_Pnd_State_Added;
    private DbsField pnd_Case_Fields_Pnd_Prior_Reported;
    private DbsField pnd_Case_Fields_Pnd_Prior_Held;
    private DbsField pnd_Case_Fields_Pnd_Prior_Record;
    private DbsField pnd_Case_Fields_Pnd_No_Change;
    private DbsField pnd_Case_Fields_Pnd_Recon_Summary;
    private DbsField pnd_Case_Fields_Pnd_Recon_Detail;
    private DbsField pnd_Case_Fields_Pnd_Process_Recon_Reporting;
    private DbsField pnd_Case_Fields_Pnd_Process_Recon;
    private DbsField pnd_Case_Fields_Pnd_Prior_Reported_For_Reporting;
    private DbsField pnd_Case_Fields_Pnd_Prior_Reported_For_Recon;
    private DbsField pnd_Case_Fields_Pnd_Prior_Recon;
    private DbsField pnd_Case_Fields_Pnd_Process_Recon_Reporting_Only;
    private DbsField pnd_Case_Fields_Pnd_Process_Reporting;
    private DbsField pnd_Case_Fields_Pnd_Process_Recon_Active_Rec;
    private DbsField pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec;
    private DbsField pnd_Case_Fields_Pnd_Legder_Not_Done;
    private DbsField pnd_Case_Fields_Pnd_Update_With_Reporting;
    private DbsField pnd_Case_Fields_Pnd_Already_Reported;
    private DbsField pnd_Case_Fields_Pnd_Already_Recon;
    private DbsField pnd_Case_Fields_Pnd_Update_Ind;

    private DbsGroup pnd_Report_Indexes;
    private DbsField pnd_Report_Indexes_Pnd_Str1;
    private DbsField pnd_Report_Indexes_Pnd_Stw1;
    private DbsField pnd_Report_Indexes_Pnd_Stw2;
    private DbsField pnd_Report_Indexes_Pnd_Stg1;

    private DbsGroup pnd_Str;
    private DbsField pnd_Str_Pnd_Header1;
    private DbsField pnd_Str_Pnd_Header2;

    private DbsGroup pnd_Str_Pnd_Totals;
    private DbsField pnd_Str_Pnd_Form_Cnt;
    private DbsField pnd_Str_Pnd_State_Distr;
    private DbsField pnd_Str_Pnd_State_Tax;
    private DbsField pnd_Str_Pnd_Local_Distr;
    private DbsField pnd_Str_Pnd_Local_Tax;

    private DbsGroup pnd_Stw;
    private DbsField pnd_Stw_Pnd_Header;

    private DbsGroup pnd_Stw_Pnd_Totals;
    private DbsField pnd_Stw_Pnd_Form_Cnt;
    private DbsField pnd_Stw_Pnd_State_Distr;
    private DbsField pnd_Stw_Pnd_State_Tax;
    private DbsField pnd_Stw_Pnd_Local_Distr;
    private DbsField pnd_Stw_Pnd_Local_Tax;

    private DbsGroup pnd_Stg;
    private DbsField pnd_Stg_Pnd_Header;

    private DbsGroup pnd_Stg_Pnd_Totals;
    private DbsField pnd_Stg_Pnd_Form_Cnt;
    private DbsField pnd_Stg_Pnd_State_Distr;
    private DbsField pnd_Stg_Pnd_State_Tax;
    private DbsField pnd_Stg_Pnd_Local_Distr;
    private DbsField pnd_Stg_Pnd_Local_Tax;

    private DbsGroup pnd_Recon_Case;
    private DbsField pnd_Recon_Case_Pnd_Tin;
    private DbsField pnd_Recon_Case_Pnd_Cntrct_Py;

    private DbsGroup pnd_Recon_Case_Pnd_Recon_Case_Detail;
    private DbsField pnd_Recon_Case_Pnd_Header;
    private DbsField pnd_Recon_Case_Pnd_Form_Cnt;
    private DbsField pnd_Recon_Case_Pnd_State_Distr;
    private DbsField pnd_Recon_Case_Pnd_State_Tax;
    private DbsField pnd_Recon_Case_Pnd_Local_Distr;
    private DbsField pnd_Recon_Case_Pnd_Local_Tax;

    private DbsGroup pnd_Hold_Fields;
    private DbsField pnd_Hold_Fields_Pnd_Hold_Comb_Fed_Ind;
    private DbsField pnd_Hold_Fields_Pnd_Hold_Rej_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwracomp = new PdaTwracomp(localVariables);
        pdaTwraform = new PdaTwraform(localVariables);
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        pdaTwratbl4 = new PdaTwratbl4(localVariables);
        pdaTwratin = new PdaTwratin(localVariables);
        ldaTwrl9710 = new LdaTwrl9710();
        registerRecord(ldaTwrl9710);
        registerRecord(ldaTwrl9710.getVw_form());
        ldaTwrl9715 = new LdaTwrl9715();
        registerRecord(ldaTwrl9715);
        registerRecord(ldaTwrl9715.getVw_form_R());

        // Local Variables

        vw_form_U = new DataAccessProgramView(new NameInfo("vw_form_U", "FORM-U"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE", DdmPeriodicGroups.getInstance().getGroups("TWRFRM_FORM_FILE"));
        form_U_Tirf_Lu_User = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Lu_User", "TIRF-LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_LU_USER");
        form_U_Tirf_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        form_U_Tirf_Lu_Ts = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Lu_Ts", "TIRF-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "TIRF_LU_TS");
        form_U_Tirf_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        form_U_Count_Casttirf_1099_R_State_Grp = vw_form_U.getRecord().newFieldInGroup("form_U_Count_Casttirf_1099_R_State_Grp", "C*TIRF-1099-R-STATE-GRP", 
            RepeatingFieldStrategy.CAsteriskVariable, "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");

        form_U_Tirf_1099_R_State_Grp = vw_form_U.getRecord().newGroupInGroup("form_U_Tirf_1099_R_State_Grp", "TIRF-1099-R-STATE-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_U_Tirf_1099_R_State_Grp.setDdmHeader("1099-R/STATE/GROUP");
        form_U_Tirf_State_Hardcopy_Ind = form_U_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_U_Tirf_State_Hardcopy_Ind", "TIRF-STATE-HARDCOPY-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_HARDCOPY_IND", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_U_Tirf_State_Hardcopy_Ind.setDdmHeader("HARD-/COPY/IND");
        form_U_Tirf_State_Code = form_U_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_U_Tirf_State_Code", "TIRF-STATE-CODE", FieldType.STRING, 2, new 
            DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_CODE", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_U_Tirf_State_Code.setDdmHeader("RESI-/DENCY/CODE");
        form_U_Tirf_State_Auth_Rpt_Ind = form_U_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_U_Tirf_State_Auth_Rpt_Ind", "TIRF-STATE-AUTH-RPT-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_AUTH_RPT_IND", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_U_Tirf_State_Auth_Rpt_Ind.setDdmHeader("STATE/RPT/IND");
        form_U_Tirf_State_Auth_Rpt_Date = form_U_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_U_Tirf_State_Auth_Rpt_Date", "TIRF-STATE-AUTH-RPT-DATE", 
            FieldType.DATE, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_AUTH_RPT_DATE", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_U_Tirf_State_Auth_Rpt_Date.setDdmHeader("STATE/AUTH RPT/DATE");
        registerRecord(vw_form_U);

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_Pnd_State_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_State_Max", "#STATE-MAX", FieldType.STRING, 2);
        pnd_Ws_Const_Pnd_Str_Max_Lines = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_Str_Max_Lines", "#STR-MAX-LINES", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Const_Pnd_Stw_Max_Lines = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_Stw_Max_Lines", "#STW-MAX-LINES", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Const_Pnd_Stg_Max_Lines = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_Stg_Max_Lines", "#STG-MAX-LINES", FieldType.PACKED_DECIMAL, 3);

        pnd_Hardcopy_Ws = localVariables.newGroupInRecord("pnd_Hardcopy_Ws", "#HARDCOPY-WS");
        pnd_Hardcopy_Ws_Pnd_Process_Type = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_Process_Type", "#PROCESS-TYPE", FieldType.STRING, 1);
        pnd_Hardcopy_Ws_Pnd_State_Name = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_State_Name", "#STATE-NAME", FieldType.STRING, 19);
        pnd_Hardcopy_Ws_Pnd_Form_Page_Cnt = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_Form_Page_Cnt", "#FORM-PAGE-CNT", FieldType.PACKED_DECIMAL, 
            2);
        pnd_Hardcopy_Ws_Pnd_Ws_Isn = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_Ws_Isn", "#WS-ISN", FieldType.NUMERIC, 8);
        pnd_Hardcopy_Ws_Pnd_M1_Add1 = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_M1_Add1", "#M1-ADD1", FieldType.STRING, 35);
        pnd_Hardcopy_Ws_Pnd_M1_Add2 = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_M1_Add2", "#M1-ADD2", FieldType.STRING, 35);
        pnd_Hardcopy_Ws_Pnd_M1_Add3 = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_M1_Add3", "#M1-ADD3", FieldType.STRING, 35);
        pnd_Hardcopy_Ws_Pnd_M1_Add4 = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_M1_Add4", "#M1-ADD4", FieldType.STRING, 35);
        pnd_Hardcopy_Ws_Pnd_M1_Add5 = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_M1_Add5", "#M1-ADD5", FieldType.STRING, 35);
        pnd_Hardcopy_Ws_Pnd_M1_Add6 = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_M1_Add6", "#M1-ADD6", FieldType.STRING, 35);
        pnd_Hardcopy_Ws_Pnd_M1_Fullname = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_M1_Fullname", "#M1-FULLNAME", FieldType.STRING, 35);
        pnd_Hardcopy_Ws_Pnd_M1_Firstname = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_M1_Firstname", "#M1-FIRSTNAME", FieldType.STRING, 35);
        pnd_Hardcopy_Ws_Pnd_M1_Lasname = pnd_Hardcopy_Ws.newFieldInGroup("pnd_Hardcopy_Ws_Pnd_M1_Lasname", "#M1-LASNAME", FieldType.STRING, 35);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Input_Parms = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Ws_Pnd_State = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Terminate = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Terminate", "#TERMINATE", FieldType.NUMERIC, 3);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_K = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_St_Occ = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_St_Occ", "#ST-OCC", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S2", "#S2", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Datx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Datx", "#DATX", FieldType.DATE);
        pnd_Ws_Pnd_Timx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Timx", "#TIMX", FieldType.TIME);
        pnd_Ws_Pnd_Prev_Rpt_Date = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Rpt_Date", "#PREV-RPT-DATE", FieldType.DATE);
        pnd_Ws_Pnd_Valid_State = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Valid_State", "#VALID-STATE", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Company_Break = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Break", "#COMPANY-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Accepted_Form_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Accepted_Form_Cnt", "#ACCEPTED-FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Company_Line = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Line", "#COMPANY-LINE", FieldType.STRING, 59);
        pnd_Ws_Pnd_Disp_Rec = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Disp_Rec", "#DISP-REC", FieldType.STRING, 5);
        pnd_Ws_Pnd_S8 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S8", "#S8", FieldType.STRING, 8);
        pnd_Ws_Pnd_Empty_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Empty_Cnt", "#EMPTY-CNT", FieldType.NUMERIC, 5);
        pnd_Ws_Pnd_Empty_Tot_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Empty_Tot_Cnt", "#EMPTY-TOT-CNT", FieldType.NUMERIC, 5);
        pnd_Ws_Pnd_Empty_Frm2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Empty_Frm2", "#EMPTY-FRM2", FieldType.NUMERIC, 5);
        pnd_Ws_Pnd_Empty_Tot_Frm2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Empty_Tot_Frm2", "#EMPTY-TOT-FRM2", FieldType.NUMERIC, 5);

        pnd_State_Table = localVariables.newGroupInRecord("pnd_State_Table", "#STATE-TABLE");
        pnd_State_Table_Tircntl_State_Old_Code = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_State_Old_Code", "TIRCNTL-STATE-OLD-CODE", FieldType.STRING, 
            3);
        pnd_State_Table_Tircntl_State_Alpha_Code = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_State_Alpha_Code", "TIRCNTL-STATE-ALPHA-CODE", 
            FieldType.STRING, 3);
        pnd_State_Table_Tircntl_State_Full_Name = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_State_Full_Name", "TIRCNTL-STATE-FULL-NAME", 
            FieldType.STRING, 19);
        pnd_State_Table_Tircntl_Comb_Fed_Ind = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_Comb_Fed_Ind", "TIRCNTL-COMB-FED-IND", FieldType.STRING, 
            1);
        pnd_State_Table_Tircntl_State_Ind = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_State_Ind", "TIRCNTL-STATE-IND", FieldType.STRING, 
            1);
        pnd_State_Table_Tircntl_Media_Code = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_Media_Code", "TIRCNTL-MEDIA-CODE", FieldType.STRING, 
            1);
        pnd_Sort_Rec = localVariables.newFieldInRecord("pnd_Sort_Rec", "#SORT-REC", FieldType.STRING, 32);

        pnd_Sort_Rec__R_Field_1 = localVariables.newGroupInRecord("pnd_Sort_Rec__R_Field_1", "REDEFINE", pnd_Sort_Rec);

        pnd_Sort_Rec_Pnd_Sort_Rec_Detail = pnd_Sort_Rec__R_Field_1.newGroupInGroup("pnd_Sort_Rec_Pnd_Sort_Rec_Detail", "#SORT-REC-DETAIL");
        pnd_Sort_Rec_Tirf_Tax_Year = pnd_Sort_Rec_Pnd_Sort_Rec_Detail.newFieldInGroup("pnd_Sort_Rec_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Sort_Rec_Tirf_Form_Type = pnd_Sort_Rec_Pnd_Sort_Rec_Detail.newFieldInGroup("pnd_Sort_Rec_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 
            2);
        pnd_Sort_Rec_Tirf_Company_Cde = pnd_Sort_Rec_Pnd_Sort_Rec_Detail.newFieldInGroup("pnd_Sort_Rec_Tirf_Company_Cde", "TIRF-COMPANY-CDE", FieldType.STRING, 
            1);
        pnd_Sort_Rec_Tirf_Tin = pnd_Sort_Rec_Pnd_Sort_Rec_Detail.newFieldInGroup("pnd_Sort_Rec_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10);
        pnd_Sort_Rec_Tirf_Contract_Nbr = pnd_Sort_Rec_Pnd_Sort_Rec_Detail.newFieldInGroup("pnd_Sort_Rec_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 
            8);
        pnd_Sort_Rec_Tirf_Payee_Cde = pnd_Sort_Rec_Pnd_Sort_Rec_Detail.newFieldInGroup("pnd_Sort_Rec_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Sort_Rec_Tirf_Key = pnd_Sort_Rec_Pnd_Sort_Rec_Detail.newFieldInGroup("pnd_Sort_Rec_Tirf_Key", "TIRF-KEY", FieldType.STRING, 5);
        pnd_S1_Start = localVariables.newFieldInRecord("pnd_S1_Start", "#S1-START", FieldType.STRING, 32);

        pnd_S1_Start__R_Field_2 = localVariables.newGroupInRecord("pnd_S1_Start__R_Field_2", "REDEFINE", pnd_S1_Start);

        pnd_S1_Start_Pnd_S1_Detail = pnd_S1_Start__R_Field_2.newGroupInGroup("pnd_S1_Start_Pnd_S1_Detail", "#S1-DETAIL");
        pnd_S1_Start_Tirf_Tax_Year = pnd_S1_Start_Pnd_S1_Detail.newFieldInGroup("pnd_S1_Start_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 4);
        pnd_S1_Start_Tirf_Tin = pnd_S1_Start_Pnd_S1_Detail.newFieldInGroup("pnd_S1_Start_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10);
        pnd_S1_Start_Tirf_Form_Type = pnd_S1_Start_Pnd_S1_Detail.newFieldInGroup("pnd_S1_Start_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_S1_Start_Tirf_Contract_Nbr = pnd_S1_Start_Pnd_S1_Detail.newFieldInGroup("pnd_S1_Start_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 
            8);
        pnd_S1_Start_Tirf_Payee_Cde = pnd_S1_Start_Pnd_S1_Detail.newFieldInGroup("pnd_S1_Start_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 2);
        pnd_S1_Start_Tirf_Key = pnd_S1_Start_Pnd_S1_Detail.newFieldInGroup("pnd_S1_Start_Tirf_Key", "TIRF-KEY", FieldType.STRING, 5);
        pnd_S1_End = localVariables.newFieldInRecord("pnd_S1_End", "#S1-END", FieldType.STRING, 32);

        pnd_Case_Fields2 = localVariables.newGroupInRecord("pnd_Case_Fields2", "#CASE-FIELDS2");
        pnd_Case_Fields2_Pnd_Recon_Only = pnd_Case_Fields2.newFieldInGroup("pnd_Case_Fields2_Pnd_Recon_Only", "#RECON-ONLY", FieldType.BOOLEAN, 1);
        pnd_Case_Fields2_Pnd_Recon_And_Reporting = pnd_Case_Fields2.newFieldInGroup("pnd_Case_Fields2_Pnd_Recon_And_Reporting", "#RECON-AND-REPORTING", 
            FieldType.BOOLEAN, 1);

        pnd_Case_Fields = localVariables.newGroupInRecord("pnd_Case_Fields", "#CASE-FIELDS");
        pnd_Case_Fields_Pnd_St_Occ_A = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_St_Occ_A", "#ST-OCC-A", FieldType.PACKED_DECIMAL, 3);
        pnd_Case_Fields_Pnd_St_Occ_I = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_St_Occ_I", "#ST-OCC-I", FieldType.PACKED_DECIMAL, 3);
        pnd_Case_Fields_Pnd_St_Max = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_St_Max", "#ST-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Case_Fields_Pnd_Active_Isn = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Active_Isn", "#ACTIVE-ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_Case_Fields_Pnd_State_Added = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_State_Added", "#STATE-ADDED", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Prior_Reported = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Prior_Reported", "#PRIOR-REPORTED", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Prior_Held = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Prior_Held", "#PRIOR-HELD", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Prior_Record = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Prior_Record", "#PRIOR-RECORD", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_No_Change = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_No_Change", "#NO-CHANGE", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Recon_Summary = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Recon_Summary", "#RECON-SUMMARY", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Recon_Detail = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Recon_Detail", "#RECON-DETAIL", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Process_Recon_Reporting = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Process_Recon_Reporting", "#PROCESS-RECON-REPORTING", 
            FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Process_Recon = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Process_Recon", "#PROCESS-RECON", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Prior_Reported_For_Reporting = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Prior_Reported_For_Reporting", "#PRIOR-REPORTED-FOR-REPORTING", 
            FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Prior_Reported_For_Recon = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Prior_Reported_For_Recon", "#PRIOR-REPORTED-FOR-RECON", 
            FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Prior_Recon = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Prior_Recon", "#PRIOR-RECON", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Process_Recon_Reporting_Only = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Process_Recon_Reporting_Only", "#PROCESS-RECON-REPORTING-ONLY", 
            FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Process_Reporting = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Process_Reporting", "#PROCESS-REPORTING", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Process_Recon_Active_Rec = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Process_Recon_Active_Rec", "#PROCESS-RECON-ACTIVE-REC", 
            FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec", "#PROCESS-REPORTING-ACTIVE-REC", 
            FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Legder_Not_Done = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Legder_Not_Done", "#LEGDER-NOT-DONE", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Update_With_Reporting = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Update_With_Reporting", "#UPDATE-WITH-REPORTING", 
            FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Already_Reported = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Already_Reported", "#ALREADY-REPORTED", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Already_Recon = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Already_Recon", "#ALREADY-RECON", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Update_Ind = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Update_Ind", "#UPDATE-IND", FieldType.STRING, 1);

        pnd_Report_Indexes = localVariables.newGroupInRecord("pnd_Report_Indexes", "#REPORT-INDEXES");
        pnd_Report_Indexes_Pnd_Str1 = pnd_Report_Indexes.newFieldInGroup("pnd_Report_Indexes_Pnd_Str1", "#STR1", FieldType.PACKED_DECIMAL, 7);
        pnd_Report_Indexes_Pnd_Stw1 = pnd_Report_Indexes.newFieldInGroup("pnd_Report_Indexes_Pnd_Stw1", "#STW1", FieldType.PACKED_DECIMAL, 7);
        pnd_Report_Indexes_Pnd_Stw2 = pnd_Report_Indexes.newFieldInGroup("pnd_Report_Indexes_Pnd_Stw2", "#STW2", FieldType.PACKED_DECIMAL, 7);
        pnd_Report_Indexes_Pnd_Stg1 = pnd_Report_Indexes.newFieldInGroup("pnd_Report_Indexes_Pnd_Stg1", "#STG1", FieldType.PACKED_DECIMAL, 7);

        pnd_Str = localVariables.newGroupArrayInRecord("pnd_Str", "#STR", new DbsArrayController(1, 7));
        pnd_Str_Pnd_Header1 = pnd_Str.newFieldInGroup("pnd_Str_Pnd_Header1", "#HEADER1", FieldType.STRING, 19);
        pnd_Str_Pnd_Header2 = pnd_Str.newFieldInGroup("pnd_Str_Pnd_Header2", "#HEADER2", FieldType.STRING, 19);

        pnd_Str_Pnd_Totals = pnd_Str.newGroupArrayInGroup("pnd_Str_Pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Str_Pnd_Form_Cnt = pnd_Str_Pnd_Totals.newFieldInGroup("pnd_Str_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Str_Pnd_State_Distr = pnd_Str_Pnd_Totals.newFieldInGroup("pnd_Str_Pnd_State_Distr", "#STATE-DISTR", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Str_Pnd_State_Tax = pnd_Str_Pnd_Totals.newFieldInGroup("pnd_Str_Pnd_State_Tax", "#STATE-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Str_Pnd_Local_Distr = pnd_Str_Pnd_Totals.newFieldInGroup("pnd_Str_Pnd_Local_Distr", "#LOCAL-DISTR", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Str_Pnd_Local_Tax = pnd_Str_Pnd_Totals.newFieldInGroup("pnd_Str_Pnd_Local_Tax", "#LOCAL-TAX", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Stw = localVariables.newGroupArrayInRecord("pnd_Stw", "#STW", new DbsArrayController(1, 6));
        pnd_Stw_Pnd_Header = pnd_Stw.newFieldInGroup("pnd_Stw_Pnd_Header", "#HEADER", FieldType.STRING, 14);

        pnd_Stw_Pnd_Totals = pnd_Stw.newGroupArrayInGroup("pnd_Stw_Pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Stw_Pnd_Form_Cnt = pnd_Stw_Pnd_Totals.newFieldInGroup("pnd_Stw_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Stw_Pnd_State_Distr = pnd_Stw_Pnd_Totals.newFieldInGroup("pnd_Stw_Pnd_State_Distr", "#STATE-DISTR", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Stw_Pnd_State_Tax = pnd_Stw_Pnd_Totals.newFieldInGroup("pnd_Stw_Pnd_State_Tax", "#STATE-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Stw_Pnd_Local_Distr = pnd_Stw_Pnd_Totals.newFieldInGroup("pnd_Stw_Pnd_Local_Distr", "#LOCAL-DISTR", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Stw_Pnd_Local_Tax = pnd_Stw_Pnd_Totals.newFieldInGroup("pnd_Stw_Pnd_Local_Tax", "#LOCAL-TAX", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Stg = localVariables.newGroupArrayInRecord("pnd_Stg", "#STG", new DbsArrayController(1, 3));
        pnd_Stg_Pnd_Header = pnd_Stg.newFieldInGroup("pnd_Stg_Pnd_Header", "#HEADER", FieldType.STRING, 19);

        pnd_Stg_Pnd_Totals = pnd_Stg.newGroupArrayInGroup("pnd_Stg_Pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Stg_Pnd_Form_Cnt = pnd_Stg_Pnd_Totals.newFieldInGroup("pnd_Stg_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Stg_Pnd_State_Distr = pnd_Stg_Pnd_Totals.newFieldInGroup("pnd_Stg_Pnd_State_Distr", "#STATE-DISTR", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Stg_Pnd_State_Tax = pnd_Stg_Pnd_Totals.newFieldInGroup("pnd_Stg_Pnd_State_Tax", "#STATE-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Stg_Pnd_Local_Distr = pnd_Stg_Pnd_Totals.newFieldInGroup("pnd_Stg_Pnd_Local_Distr", "#LOCAL-DISTR", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Stg_Pnd_Local_Tax = pnd_Stg_Pnd_Totals.newFieldInGroup("pnd_Stg_Pnd_Local_Tax", "#LOCAL-TAX", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Recon_Case = localVariables.newGroupInRecord("pnd_Recon_Case", "#RECON-CASE");
        pnd_Recon_Case_Pnd_Tin = pnd_Recon_Case.newFieldInGroup("pnd_Recon_Case_Pnd_Tin", "#TIN", FieldType.STRING, 11);
        pnd_Recon_Case_Pnd_Cntrct_Py = pnd_Recon_Case.newFieldInGroup("pnd_Recon_Case_Pnd_Cntrct_Py", "#CNTRCT-PY", FieldType.STRING, 12);

        pnd_Recon_Case_Pnd_Recon_Case_Detail = pnd_Recon_Case.newGroupArrayInGroup("pnd_Recon_Case_Pnd_Recon_Case_Detail", "#RECON-CASE-DETAIL", new DbsArrayController(1, 
            3));
        pnd_Recon_Case_Pnd_Header = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_Header", "#HEADER", FieldType.STRING, 10);
        pnd_Recon_Case_Pnd_Form_Cnt = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Recon_Case_Pnd_State_Distr = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_State_Distr", "#STATE-DISTR", FieldType.PACKED_DECIMAL, 
            14, 2);
        pnd_Recon_Case_Pnd_State_Tax = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_State_Tax", "#STATE-TAX", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Recon_Case_Pnd_Local_Distr = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_Local_Distr", "#LOCAL-DISTR", FieldType.PACKED_DECIMAL, 
            14, 2);
        pnd_Recon_Case_Pnd_Local_Tax = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_Local_Tax", "#LOCAL-TAX", FieldType.PACKED_DECIMAL, 
            11, 2);

        pnd_Hold_Fields = localVariables.newGroupInRecord("pnd_Hold_Fields", "#HOLD-FIELDS");
        pnd_Hold_Fields_Pnd_Hold_Comb_Fed_Ind = pnd_Hold_Fields.newFieldInGroup("pnd_Hold_Fields_Pnd_Hold_Comb_Fed_Ind", "#HOLD-COMB-FED-IND", FieldType.STRING, 
            1);
        pnd_Hold_Fields_Pnd_Hold_Rej_Ind = pnd_Hold_Fields.newFieldInGroup("pnd_Hold_Fields_Pnd_Hold_Rej_Ind", "#HOLD-REJ-IND", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_form_U.reset();

        ldaTwrl9710.initializeValues();
        ldaTwrl9715.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
        pnd_Ws_Const_Pnd_State_Max.setInitialValue("57");
        pnd_Ws_Const_Pnd_Str_Max_Lines.setInitialValue(7);
        pnd_Ws_Const_Pnd_Stw_Max_Lines.setInitialValue(6);
        pnd_Ws_Const_Pnd_Stg_Max_Lines.setInitialValue(3);
        pnd_Ws_Pnd_Company_Break.setInitialValue(true);
        pnd_Str_Pnd_Header1.getValue(1).setInitialValue("Form Database Total");
        pnd_Str_Pnd_Header1.getValue(2).setInitialValue("Actve Records");
        pnd_Str_Pnd_Header1.getValue(3).setInitialValue("Actve Rec Not Rept");
        pnd_Str_Pnd_Header1.getValue(4).setInitialValue("Non Rept Zero Forms");
        pnd_Str_Pnd_Header1.getValue(5).setInitialValue("Prev Rejected Forms");
        pnd_Str_Pnd_Header1.getValue(6).setInitialValue("New  Rejected Forms");
        pnd_Str_Pnd_Header1.getValue(7).setInitialValue("Accepted Forms");
        pnd_Str_Pnd_Header2.getValue(2).setInitialValue(" (Prev. Reported)");
        pnd_Stw_Pnd_Header.getValue(1).setInitialValue("Accepted");
        pnd_Stw_Pnd_Header.getValue(2).setInitialValue("Prev. Reported");
        pnd_Stw_Pnd_Header.getValue(3).setInitialValue("Net Change");
        pnd_Stw_Pnd_Header.getValue(4).setInitialValue("Rejected");
        pnd_Stw_Pnd_Header.getValue(5).setInitialValue("Prev. Reported");
        pnd_Stw_Pnd_Header.getValue(6).setInitialValue("Net Change");
        pnd_Stg_Pnd_Header.getValue(1).setInitialValue("Form Database Total");
        pnd_Stg_Pnd_Header.getValue(2).setInitialValue("Prev. Reconciled");
        pnd_Stg_Pnd_Header.getValue(3).setInitialValue("Net Change");
        pnd_Recon_Case_Pnd_Header.getValue(1).setInitialValue("New Form");
        pnd_Recon_Case_Pnd_Header.getValue(2).setInitialValue("Original");
        pnd_Recon_Case_Pnd_Header.getValue(3).setInitialValue("Net Change");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Twrp3539() throws Exception
    {
        super("Twrp3539");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp3539|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 80 ZP = ON;//Natural: FORMAT ( 02 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 03 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 04 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 05 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 06 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 07 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 08 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 09 ) PS = 58 LS = 133 ZP = ON
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: IF *DEVICE = 'BATCH'
                {
                    //*  SET DELIMITER MODE FOR BATCH INPUT
                    setControl("D");                                                                                                                                      //Natural: SET CONTROL 'D'
                }                                                                                                                                                         //Natural: END-IF
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 37T 'TaxWaRS' 68T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 34T 'Control Report' 68T 'Report: RPT1' / //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 02 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 50T 'State Correction Reporting Summary' 120T 'Report: RPT2' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 60T #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 03 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'State Correction Reporting Rejected Detail' 120T 'Report: RPT3' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 60T #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 04 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'State Correction Reporting Accepted Detail' 120T 'Report: RPT4' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 60T #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 05 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 05 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T 'State Withholding Reconciliation Summary' 120T 'Report: RPT5' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 60T #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME / 44T 'Reporting period' #WS.#PREV-RPT-DATE ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 06 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 06 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 43T 'State Withholding Reconciliation Accepted Detail' 120T 'Report: RPT6' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 60T #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME / 44T 'Reporting period' #WS.#PREV-RPT-DATE ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 07 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 07 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 43T 'State Withholding Reconciliation Rejected Detail' 120T 'Report: RPT7' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 60T #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME / 44T 'Reporting period' #WS.#PREV-RPT-DATE ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 08 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 08 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'State General Ledger Reconciliation Summary' 120T 'Report: RPT8' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 60T #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME / 44T 'Reporting period' #WS.#PREV-RPT-DATE ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 09 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 09 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 45T 'State General Ledger Reconciliation Detail' 120T 'Report: RPT9' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 60T #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME / 44T 'Reporting period' #WS.#PREV-RPT-DATE ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / #WS.#COMPANY-LINE //
                //* ***************
                //*  MAIN PROGRAM *
                //* ***************
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_State,pnd_Ws_Pnd_Tax_Year);                                                                 //Natural: INPUT #WS.#STATE #WS.#TAX-YEAR
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT-PARMS
                sub_Process_Input_Parms();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Ws_Pnd_S8.setValueEdited(pnd_Ws_Pnd_Tax_Year,new ReportEditMask("9999"));                                                                             //Natural: MOVE EDITED #WS.#TAX-YEAR ( EM = 9999 ) TO #WS.#S8
                setValueToSubstring("01",pnd_Ws_Pnd_S8,5,2);                                                                                                              //Natural: MOVE '01' TO SUBSTR ( #WS.#S8,5,2 )
                setValueToSubstring(pnd_Ws_Pnd_State,pnd_Ws_Pnd_S8,7,2);                                                                                                  //Natural: MOVE #WS.#STATE TO SUBSTR ( #WS.#S8,7,2 )
                ldaTwrl9710.getVw_form().startDatabaseFind                                                                                                                //Natural: FIND FORM WITH FORM.TIRF-SUPERDE-8 = #WS.#S8
                (
                "F_STATE",
                new Wc[] { new Wc("TIRF_SUPERDE_8", "=", pnd_Ws_Pnd_S8, WcType.WITH) }
                );
                F_STATE:
                while (condition(ldaTwrl9710.getVw_form().readNextRow("F_STATE")))
                {
                    ldaTwrl9710.getVw_form().setIfNotFoundControlFlag(false);
                    if (condition(!(ldaTwrl9710.getForm_Tirf_Active_Ind().equals("A"))))                                                                                  //Natural: ACCEPT IF FORM.TIRF-ACTIVE-IND = 'A'
                    {
                        continue;
                    }
                    pnd_Sort_Rec_Pnd_Sort_Rec_Detail.setValuesByName(ldaTwrl9710.getVw_form());                                                                           //Natural: MOVE BY NAME FORM TO #SORT-REC-DETAIL
                    getSort().writeSortInData(pnd_Sort_Rec);                                                                                                              //Natural: END-ALL
                }                                                                                                                                                         //Natural: END-FIND
                if (Global.isEscape()) return;
                getSort().sortData(pnd_Sort_Rec);                                                                                                                         //Natural: SORT BY #SORT-REC USING KEYS
                boolean endOfDataSort01 = true;
                boolean firstSort01 = true;
                SORT01:
                while (condition(getSort().readSortOutData(pnd_Sort_Rec)))
                {
                    if (condition(getSort().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventSort01(false);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataSort01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    if (condition(pnd_Ws_Pnd_Company_Break.getBoolean()))                                                                                                 //Natural: AT BREAK OF #SORT-REC.TIRF-COMPANY-CDE;//Natural: AT BREAK OF #SORT-REC.TIRF-FORM-TYPE;//Natural: IF #WS.#COMPANY-BREAK
                    {
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMPANY
                        sub_Process_Company();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_S1_Start_Tirf_Tax_Year.setValue(pnd_Sort_Rec_Tirf_Tax_Year);                                                                                      //Natural: ASSIGN #S1-START.TIRF-TAX-YEAR := #SORT-REC.TIRF-TAX-YEAR
                    pnd_S1_Start_Tirf_Tin.setValue(pnd_Sort_Rec_Tirf_Tin);                                                                                                //Natural: ASSIGN #S1-START.TIRF-TIN := #SORT-REC.TIRF-TIN
                    pnd_S1_Start_Tirf_Form_Type.setValue(pnd_Sort_Rec_Tirf_Form_Type);                                                                                    //Natural: ASSIGN #S1-START.TIRF-FORM-TYPE := #SORT-REC.TIRF-FORM-TYPE
                    pnd_S1_Start_Tirf_Contract_Nbr.setValue(pnd_Sort_Rec_Tirf_Contract_Nbr);                                                                              //Natural: ASSIGN #S1-START.TIRF-CONTRACT-NBR := #SORT-REC.TIRF-CONTRACT-NBR
                    pnd_S1_Start_Tirf_Payee_Cde.setValue(pnd_Sort_Rec_Tirf_Payee_Cde);                                                                                    //Natural: ASSIGN #S1-START.TIRF-PAYEE-CDE := #SORT-REC.TIRF-PAYEE-CDE
                    pnd_S1_Start_Tirf_Key.setValue(pnd_Sort_Rec_Tirf_Key);                                                                                                //Natural: ASSIGN #S1-START.TIRF-KEY := #SORT-REC.TIRF-KEY
                    pnd_S1_End.setValue(pnd_S1_Start);                                                                                                                    //Natural: ASSIGN #S1-END := #S1-START
                    setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_S1_Start,32,1);                                                                                       //Natural: MOVE LOW-VALUES TO SUBSTR ( #S1-START,32,1 )
                    setValueToSubstring(pnd_Ws_Const_High_Values,pnd_S1_End,32,1);                                                                                        //Natural: MOVE HIGH-VALUES TO SUBSTR ( #S1-END,32,1 )
                                                                                                                                                                          //Natural: PERFORM PROCESS-CASE
                    sub_Process_Case();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-SORT
                if (condition(getSort().getAstCOUNTER().greater(0)))
                {
                    atBreakEventSort01(endOfDataSort01);
                }
                endSort();
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                if (condition(pnd_Ws_Pnd_Accepted_Form_Cnt.equals(getZero())))                                                                                            //Natural: IF #WS.#ACCEPTED-FORM-CNT = 0
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"No Forms were selected for the",new TabSetting(77),"***",NEWLINE,"***",new       //Natural: WRITE ( 1 ) '***' 25T 'No Forms were selected for the' 77T '***' / '***' 25T 'State Correction Reporting' 77T '***'
                        TabSetting(25),"State Correction Reporting",new TabSetting(77),"***");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                //* ************************
                //*  S U B R O U T I N E S
                //* *****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CASE
                //*                                         IN CASE OF NO INACTIVE RECORDS
                //*          #CASE-FIELDS.#PROCESS-REPORTING             := FALSE
                //*          #CASE-FIELDS.#PROCESS-REPORTING             := FALSE
                //* ***    #CASE-FIELDS.#PROCESS-RECON-REPORTING      := FALSE
                //* ***    #CASE-FIELDS.#PROCESS-RECON-REPORTING-ONLY := FALSE
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ACTIVE-REC
                //* * #REPORT-INDEXES.#STR1               := 5
                //*       #REPORT-INDEXES.#STR1     := 6
                //*       #REPORT-INDEXES.#STR1     := 7
                //*  #REPORT-INDEXES.#STR1       := 8
                //* **************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-STR
                //* **************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-STW
                //* **************************
                //* **************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-STG
                //* ***************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-STG1
                //* ***************************
                //* ************************************
                //* ****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-FORM
                //* ****************************
                //* *********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-RECON-CASE
                //* ********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-REPORTS
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-REPORTS-C
                //* **********************************
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-REPORTS-W
                //* **********************************
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-REPORTS-G
                //* **********************************
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STR-DETAIL-ACCEPT
                //*  'H/A/R/D'          #TWRAFORM.TIRF-STATE-HARDCOPY-IND(#ST-OCC)
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STR-DETAIL-REJECT
                //*  'H/A/R/D'          FORM-R.TIRF-STATE-HARDCOPY-IND   (#ST-OCC-A)
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STW-DETAIL-ACCEPT
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STW-DETAIL-REJECT
                //* ***************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STG-DETAIL
                //* ****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TIN
                //* ********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COMPANY
                //* ********************************
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INPUT-PARMS
                //* *****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CONTROL
                //* *******************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CONTROL
                //* ******************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-HARDCOPY
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Process_Case() throws Exception                                                                                                                      //Natural: PROCESS-CASE
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        pnd_Case_Fields.reset();                                                                                                                                          //Natural: RESET #CASE-FIELDS
        ldaTwrl9710.getVw_form().startDatabaseRead                                                                                                                        //Natural: READ FORM BY TIRF-SUPERDE-1 = #S1-START THRU #S1-END
        (
        "RD_FORM",
        new Wc[] { new Wc("TIRF_SUPERDE_1", ">=", pnd_S1_Start.getBinary(), "And", WcType.BY) ,
        new Wc("TIRF_SUPERDE_1", "<=", pnd_S1_End.getBinary(), WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_1", "ASC") }
        );
        RD_FORM:
        while (condition(ldaTwrl9710.getVw_form().readNextRow("RD_FORM")))
        {
            pnd_Case_Fields_Pnd_St_Max.setValue(ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp());                                                                   //Natural: ASSIGN #CASE-FIELDS.#ST-MAX := FORM.C*TIRF-1099-R-STATE-GRP
            if (condition(pnd_Case_Fields_Pnd_St_Max.equals(getZero())))                                                                                                  //Natural: IF #CASE-FIELDS.#ST-MAX = 0
            {
                pnd_Ws_Pnd_St_Occ.reset();                                                                                                                                //Natural: RESET #WS.#ST-OCC
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                DbsUtil.examine(new ExamineSource(ldaTwrl9710.getForm_Tirf_State_Code().getValue(1,":",pnd_Case_Fields_Pnd_St_Max),true), new ExamineSearch(pnd_Ws_Pnd_State,  //Natural: EXAMINE FULL FORM.TIRF-STATE-CODE ( 1:#ST-MAX ) FOR FULL #WS.#STATE GIVING INDEX IN #WS.#ST-OCC
                    true), new ExamineGivingIndex(pnd_Ws_Pnd_St_Occ));
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl9710.getForm_Tirf_Active_Ind().notEquals("A") && pnd_Ws_Pnd_St_Occ.equals(getZero())))                                                   //Natural: REJECT IF FORM.TIRF-ACTIVE-IND NE 'A' AND #WS.#ST-OCC = 0
            {
                continue;
            }
            //*  ACTIVE RECORD
            if (condition(ldaTwrl9710.getForm_Tirf_Active_Ind().equals("A")))                                                                                             //Natural: IF FORM.TIRF-ACTIVE-IND = 'A'
            {
                ldaTwrl9715.getVw_form_R().setValuesByName(ldaTwrl9710.getVw_form());                                                                                     //Natural: MOVE BY NAME FORM TO FORM-R
                pnd_Case_Fields_Pnd_St_Occ_A.setValue(pnd_Ws_Pnd_St_Occ);                                                                                                 //Natural: ASSIGN #CASE-FIELDS.#ST-OCC-A := #WS.#ST-OCC
                pnd_Report_Indexes_Pnd_Str1.setValue(1);                                                                                                                  //Natural: ASSIGN #REPORT-INDEXES.#STR1 := 1
                if (condition(ldaTwrl9715.getForm_R_Tirf_Empty_Form().getBoolean()))                                                                                      //Natural: IF FORM-R.TIRF-EMPTY-FORM
                {
                    pnd_Ws_Pnd_Empty_Frm2.nadd(1);                                                                                                                        //Natural: ADD 1 TO #EMPTY-FRM2
                    pnd_Ws_Pnd_Empty_Tot_Frm2.nadd(1);                                                                                                                    //Natural: ADD 1 TO #EMPTY-TOT-FRM2
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ACCUM-STR
                sub_Accum_Str();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                if (condition(ldaTwrl9715.getForm_R_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A).notEquals(" ")))                                     //Natural: IF FORM-R.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-A ) NE ' '
                {
                                                                                                                                                                          //Natural: PERFORM ACCUM-STG
                    sub_Accum_Stg();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                //*  ALREADY REPORTED
                if (condition(! (ldaTwrl9715.getForm_R_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(" ") || ldaTwrl9715.getForm_R_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals("H")  //Natural: IF NOT FORM-R.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-A ) = ' ' OR = 'H' OR = 'J'
                    || ldaTwrl9715.getForm_R_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals("J"))))
                {
                    pnd_Report_Indexes_Pnd_Str1.setValue(2);                                                                                                              //Natural: ASSIGN #REPORT-INDEXES.#STR1 := 2
                                                                                                                                                                          //Natural: PERFORM ACCUM-STR
                    sub_Accum_Str();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                //*    RESET #PROCESS-RECON-REPORTING-ONLY
                //*    RESET #PROCESS-RECON-REPORTING
                //*    RESET #PROCESS-REPORTING
                //*    RESET #UPDATE-WITH-REPORTING
                if (condition(pnd_Case_Fields2_Pnd_Recon_And_Reporting.getBoolean()))                                                                                     //Natural: IF #RECON-AND-REPORTING
                {
                    if (condition(ldaTwrl9715.getForm_R_Tirf_State_Reporting().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals("Y")))                                       //Natural: IF FORM-R.TIRF-STATE-REPORTING ( #ST-OCC-A ) = 'Y'
                    {
                        pnd_Case_Fields_Pnd_Process_Reporting.setValue(true);                                                                                             //Natural: ASSIGN #PROCESS-REPORTING := TRUE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Report_Indexes_Pnd_Str1.setValue(3);                                                                                                          //Natural: ASSIGN #REPORT-INDEXES.#STR1 := 3
                                                                                                                                                                          //Natural: PERFORM ACCUM-STR
                        sub_Accum_Str();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    //* **   IF (FORM-R.TIRF-STATE-HARDCOPY-IND(#ST-OCC-A)  NE ' ') AND
                    //* **    ( FORM-R.TIRF-STATE-TAX-WTHLD(#ST-OCC-A)  NE 0 OR
                    //* **     FORM-R.TIRF-LOC-TAX-WTHLD(#ST-OCC-A)    NE 0)
                    //* **      #PROCESS-RECON-REPORTING  := TRUE
                    //* **   END-IF
                    if (condition(ldaTwrl9715.getForm_R_Tirf_State_Hardcopy_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A).notEquals(" ")))                                 //Natural: IF FORM-R.TIRF-STATE-HARDCOPY-IND ( #ST-OCC-A ) NE ' '
                    {
                        pnd_Case_Fields_Pnd_Process_Recon.setValue(true);                                                                                                 //Natural: ASSIGN #PROCESS-RECON := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Case_Fields_Pnd_Process_Reporting.getBoolean() || pnd_Case_Fields_Pnd_Process_Recon.getBoolean()))                                  //Natural: IF #PROCESS-REPORTING OR #PROCESS-RECON
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Case_Fields2_Pnd_Recon_Only.getBoolean()))                                                                                              //Natural: IF #RECON-ONLY
                {
                    if (condition((ldaTwrl9715.getForm_R_Tirf_State_Hardcopy_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A).notEquals(" "))))                               //Natural: IF ( FORM-R.TIRF-STATE-HARDCOPY-IND ( #ST-OCC-A ) NE ' ' )
                    {
                        pnd_Case_Fields_Pnd_Process_Recon.setValue(true);                                                                                                 //Natural: ASSIGN #PROCESS-RECON := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Case_Fields_Pnd_Process_Recon.getBoolean()))                                                                                        //Natural: IF #PROCESS-RECON
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Case_Fields_Pnd_Active_Isn.setValue(ldaTwrl9710.getVw_form().getAstISN("RD_FORM"));                                                                   //Natural: ASSIGN #CASE-FIELDS.#ACTIVE-ISN := *ISN ( RD-FORM. )
                pnd_Case_Fields_Pnd_St_Occ_I.setValue(pnd_Ws_Pnd_St_Occ);                                                                                                 //Natural: ASSIGN #CASE-FIELDS.#ST-OCC-I := #WS.#ST-OCC
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Case_Fields_Pnd_St_Occ_I.setValue(pnd_Ws_Pnd_St_Occ);                                                                                                     //Natural: ASSIGN #CASE-FIELDS.#ST-OCC-I := #WS.#ST-OCC
            //*  IF #PROCESS-REPORTING               OR
            //*     #PROCESS-RECON
            //*      IGNORE
            //*  ELSE
            //*     ESCAPE ROUTINE
            //*  END-IF
            //* **********************************************************
            //* * THE FOLLOWING CODE SEARCHES FOR INACTIVE PRIOR RECORD
            //* **********************************************************
            pnd_Case_Fields_Pnd_Prior_Reported_For_Reporting.reset();                                                                                                     //Natural: RESET #CASE-FIELDS.#PRIOR-REPORTED-FOR-REPORTING
            pnd_Case_Fields_Pnd_Prior_Recon.reset();                                                                                                                      //Natural: RESET #CASE-FIELDS.#PRIOR-RECON
            pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.reset();                                                                                                     //Natural: RESET #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC
            pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.reset();                                                                                                         //Natural: RESET #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC
            if (condition(pnd_Case_Fields_Pnd_Process_Reporting.getBoolean() && ! (pnd_Case_Fields_Pnd_Already_Reported.getBoolean())))                                   //Natural: IF #PROCESS-REPORTING AND NOT #ALREADY-REPORTED
            {
                //*   IF #WS.#STATE = '12' OR #WS.#STATE = '17'
                if (condition(pnd_Ws_Pnd_State.equals("12") || pnd_Ws_Pnd_State.equals("08")))                                                                            //Natural: IF #WS.#STATE = '12' OR #WS.#STATE = '08'
                {
                    if (condition(ldaTwrl9710.getForm_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals("O") || ldaTwrl9710.getForm_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals("C"))) //Natural: IF FORM.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-I ) = 'O' OR = 'C'
                    {
                        pnd_Case_Fields_Pnd_Already_Reported.setValue(true);                                                                                              //Natural: ASSIGN #CASE-FIELDS.#ALREADY-REPORTED := TRUE
                        pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.setValue(true);                                                                                  //Natural: ASSIGN #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC := TRUE
                        pnd_Case_Fields_Pnd_Prior_Reported_For_Reporting.setValue(true);                                                                                  //Natural: ASSIGN #CASE-FIELDS.#PRIOR-REPORTED-FOR-REPORTING := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition((((ldaTwrl9710.getForm_Tirf_State_Irs_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals("Y") && ldaTwrl9710.getForm_Tirf_Res_Cmb_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals("Y"))  //Natural: IF ( FORM.TIRF-STATE-IRS-RPT-IND ( #ST-OCC-I ) = 'Y' AND FORM.TIRF-RES-CMB-IND ( #ST-OCC-I ) = 'Y' AND ( FORM.TIRF-IRS-RPT-IND = 'O' OR = 'C' ) ) OR ( FORM.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-I ) = 'O' OR = 'C' )
                        && (ldaTwrl9710.getForm_Tirf_Irs_Rpt_Ind().equals("O") || ldaTwrl9710.getForm_Tirf_Irs_Rpt_Ind().equals("C"))) || (ldaTwrl9710.getForm_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals("O") 
                        || ldaTwrl9710.getForm_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals("C")))))
                    {
                        pnd_Case_Fields_Pnd_Already_Reported.setValue(true);                                                                                              //Natural: ASSIGN #CASE-FIELDS.#ALREADY-REPORTED := TRUE
                        pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.setValue(true);                                                                                  //Natural: ASSIGN #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC := TRUE
                        pnd_Case_Fields_Pnd_Prior_Reported_For_Reporting.setValue(true);                                                                                  //Natural: ASSIGN #CASE-FIELDS.#PRIOR-REPORTED-FOR-REPORTING := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Case_Fields_Pnd_Process_Recon.getBoolean() && ! (pnd_Case_Fields_Pnd_Already_Recon.getBoolean())))                                          //Natural: IF #PROCESS-RECON AND NOT #ALREADY-RECON
            {
                if (condition(ldaTwrl9710.getForm_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals("O") || ldaTwrl9710.getForm_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals("C")  //Natural: IF FORM.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-I ) = 'O' OR = 'C' OR = 'X'
                    || ldaTwrl9710.getForm_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals("X")))
                {
                    pnd_Case_Fields_Pnd_Already_Recon.setValue(true);                                                                                                     //Natural: ASSIGN #CASE-FIELDS.#ALREADY-RECON := TRUE
                    pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.setValue(true);                                                                                          //Natural: ASSIGN #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC := TRUE
                    pnd_Case_Fields_Pnd_Prior_Recon.setValue(true);                                                                                                       //Natural: ASSIGN #CASE-FIELDS.#PRIOR-RECON := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.getBoolean() || pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.getBoolean()))                    //Natural: IF #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC OR #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ACTIVE-REC
                sub_Process_Active_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.reset();                                                                                                         //Natural: RESET #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC
        pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.reset();                                                                                                             //Natural: RESET #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC
        if (condition(pnd_Case_Fields_Pnd_Process_Reporting.getBoolean() && ! (pnd_Case_Fields_Pnd_Already_Reported.getBoolean())))                                       //Natural: IF #CASE-FIELDS.#PROCESS-REPORTING AND NOT #ALREADY-REPORTED
        {
            pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.setValue(true);                                                                                              //Natural: ASSIGN #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Process_Recon.getBoolean() && ! (pnd_Case_Fields_Pnd_Already_Recon.getBoolean())))                                              //Natural: IF #CASE-FIELDS.#PROCESS-RECON AND NOT #ALREADY-RECON
        {
            pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.setValue(true);                                                                                                  //Natural: ASSIGN #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.getBoolean() || pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.getBoolean()))                        //Natural: IF #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC OR #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ACTIVE-REC
            sub_Process_Active_Rec();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Active_Rec() throws Exception                                                                                                                //Natural: PROCESS-ACTIVE-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        if (condition(! (pnd_Case_Fields_Pnd_Prior_Reported_For_Reporting.getBoolean()) && pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.getBoolean()                  //Natural: IF NOT #CASE-FIELDS.#PRIOR-REPORTED-FOR-REPORTING AND #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC AND FORM-R.TIRF-STATE-DISTR ( #ST-OCC-A ) = 0.00 AND FORM-R.TIRF-STATE-TAX-WTHLD ( #ST-OCC-A ) = 0.00 AND FORM-R.TIRF-LOC-DISTR ( #ST-OCC-A ) = 0.00 AND FORM-R.TIRF-LOC-TAX-WTHLD ( #ST-OCC-A ) = 0.00
            && ldaTwrl9715.getForm_R_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(new DbsDecimal("0.00")) && ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(new 
            DbsDecimal("0.00")) && ldaTwrl9715.getForm_R_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(new DbsDecimal("0.00")) && ldaTwrl9715.getForm_R_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(new 
            DbsDecimal("0.00"))))
        {
            pnd_Report_Indexes_Pnd_Str1.setValue(4);                                                                                                                      //Natural: ASSIGN #REPORT-INDEXES.#STR1 := 4
                                                                                                                                                                          //Natural: PERFORM ACCUM-STR
            sub_Accum_Str();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            if (condition(pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.getBoolean()))                                                                                     //Natural: IF #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC
            {
                pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.reset();                                                                                                 //Natural: RESET #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Prior_Recon.getBoolean() && pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.getBoolean()))                                         //Natural: IF #CASE-FIELDS.#PRIOR-RECON AND #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC
        {
            pnd_Case_Fields_Pnd_Prior_Record.setValue(true);                                                                                                              //Natural: ASSIGN #CASE-FIELDS.#PRIOR-RECORD := TRUE
                                                                                                                                                                          //Natural: PERFORM SETUP-RECON-CASE
            sub_Setup_Recon_Case();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            if (condition(! (pnd_Case_Fields_Pnd_Recon_Summary.getBoolean())))                                                                                            //Natural: IF NOT #CASE-FIELDS.#RECON-SUMMARY
            {
                if (condition(pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.getBoolean()))                                                                             //Natural: IF #PROCESS-REPORTING-ACTIVE-REC
                {
                    pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.reset();                                                                                                 //Natural: RESET #PROCESS-RECON-ACTIVE-REC
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.getBoolean() && ! (pnd_Case_Fields_Pnd_Prior_Recon.getBoolean())))                                     //Natural: IF #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC AND NOT #CASE-FIELDS.#PRIOR-RECON
        {
            pnd_Case_Fields_Pnd_Prior_Record.setValue(false);                                                                                                             //Natural: ASSIGN #CASE-FIELDS.#PRIOR-RECORD := FALSE
                                                                                                                                                                          //Natural: PERFORM SETUP-RECON-CASE
            sub_Setup_Recon_Case();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            if (condition(! (pnd_Case_Fields_Pnd_Recon_Summary.getBoolean())))                                                                                            //Natural: IF NOT #CASE-FIELDS.#RECON-SUMMARY
            {
                if (condition(pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.getBoolean()))                                                                             //Natural: IF #PROCESS-REPORTING-ACTIVE-REC
                {
                    pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.reset();                                                                                                 //Natural: RESET #PROCESS-RECON-ACTIVE-REC
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.getBoolean()))                                                                                         //Natural: IF #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC
        {
                                                                                                                                                                          //Natural: PERFORM STG-DETAIL
            sub_Stg_Detail();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaTwratbl2.getTwratbl2_Tircntl_Corr_Media_Code().equals("M")))                                                                                     //Natural: IF TWRATBL2.TIRCNTL-CORR-MEDIA-CODE = 'M'
        {
            pnd_Hold_Fields_Pnd_Hold_Rej_Ind.setValue(ldaTwrl9715.getForm_R_Tirf_Irs_Reject_Ind());                                                                       //Natural: ASSIGN #HOLD-REJ-IND := FORM-R.TIRF-IRS-REJECT-IND
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Hold_Fields_Pnd_Hold_Rej_Ind.setValue(ldaTwrl9715.getForm_R_Tirf_Res_Reject_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                                //Natural: ASSIGN #HOLD-REJ-IND := FORM-R.TIRF-RES-REJECT-IND ( #ST-OCC-A )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Hold_Fields_Pnd_Hold_Rej_Ind.equals("Y")))                                                                                                      //Natural: IF #HOLD-REJ-IND = 'Y'
        {
            if (condition(pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.getBoolean()))                                                                                 //Natural: IF #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC
            {
                                                                                                                                                                          //Natural: PERFORM STR-DETAIL-REJECT
                sub_Str_Detail_Reject();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  OLD REJECTS
                if (condition(ldaTwrl9715.getForm_R_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals("H")))                                        //Natural: IF FORM-R.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-A ) = 'H'
                {
                    pnd_Report_Indexes_Pnd_Str1.setValue(5);                                                                                                              //Natural: ASSIGN #REPORT-INDEXES.#STR1 := 5
                                                                                                                                                                          //Natural: PERFORM ACCUM-STR
                    sub_Accum_Str();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    //* ***   ESCAPE ROUTINE
                    //*  FIRST TIME REJECT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Report_Indexes_Pnd_Str1.setValue(6);                                                                                                              //Natural: ASSIGN #REPORT-INDEXES.#STR1 := 6
                                                                                                                                                                          //Natural: PERFORM ACCUM-STR
                    sub_Accum_Str();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.getBoolean()))                                                                                     //Natural: IF #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC
            {
                                                                                                                                                                          //Natural: PERFORM STW-DETAIL-REJECT
                sub_Stw_Detail_Reject();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  OLD REJECTS
            if (condition(ldaTwrl9715.getForm_R_Tirf_State_Auth_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals("H")))                                            //Natural: IF FORM-R.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-A ) = 'H'
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM UPDATE-FORM
            sub_Update_Form();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.getBoolean()))                                                                                     //Natural: IF #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC
        {
            pnd_Report_Indexes_Pnd_Str1.setValue(7);                                                                                                                      //Natural: ASSIGN #REPORT-INDEXES.#STR1 := 7
                                                                                                                                                                          //Natural: PERFORM ACCUM-STR
            sub_Accum_Str();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #CASE-FIELDS.#PRIOR-REPORTED-FOR-REPORTING
        if (condition(pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.getBoolean()))                                                                                     //Natural: IF #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC
        {
            pdaTwraform.getPnd_Twraform_Pnd_Twraform_Det().setValuesByName(ldaTwrl9715.getVw_form_R());                                                                   //Natural: MOVE BY NAME FORM-R TO #TWRAFORM-DET
            pdaTwraform.getPnd_Twraform_C_1099_R().setValue(ldaTwrl9715.getForm_R_Count_Casttirf_1099_R_State_Grp());                                                     //Natural: ASSIGN #TWRAFORM.C-1099-R := FORM-R.C*TIRF-1099-R-STATE-GRP
            pnd_Ws_Pnd_St_Occ.setValue(pnd_Case_Fields_Pnd_St_Occ_A);                                                                                                     //Natural: ASSIGN #WS.#ST-OCC := #CASE-FIELDS.#ST-OCC-A
            pnd_Ws_Pnd_Disp_Rec.setValue("N E W");                                                                                                                        //Natural: ASSIGN #WS.#DISP-REC := 'N E W'
                                                                                                                                                                          //Natural: PERFORM STR-DETAIL-ACCEPT
            sub_Str_Detail_Accept();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Prior_Reported_For_Reporting.getBoolean() && pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.getBoolean()))                    //Natural: IF #CASE-FIELDS.#PRIOR-REPORTED-FOR-REPORTING AND #CASE-FIELDS.#PROCESS-REPORTING-ACTIVE-REC
        {
            pdaTwraform.getPnd_Twraform_Pnd_Twraform_Det().setValuesByName(ldaTwrl9710.getVw_form());                                                                     //Natural: MOVE BY NAME FORM TO #TWRAFORM-DET
            pdaTwraform.getPnd_Twraform_C_1099_R().setValue(ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp());                                                       //Natural: ASSIGN #TWRAFORM.C-1099-R := FORM.C*TIRF-1099-R-STATE-GRP
            pnd_Ws_Pnd_St_Occ.setValue(pnd_Case_Fields_Pnd_St_Occ_I);                                                                                                     //Natural: ASSIGN #WS.#ST-OCC := #CASE-FIELDS.#ST-OCC-I
            pnd_Ws_Pnd_Disp_Rec.setValue("O L D");                                                                                                                        //Natural: ASSIGN #WS.#DISP-REC := 'O L D'
                                                                                                                                                                          //Natural: PERFORM STR-DETAIL-ACCEPT
            sub_Str_Detail_Accept();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 4 ) 1 LINES
        if (condition(pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.getBoolean()))                                                                                         //Natural: IF #CASE-FIELDS.#PROCESS-RECON-ACTIVE-REC
        {
                                                                                                                                                                          //Natural: PERFORM STW-DETAIL-ACCEPT
            sub_Stw_Detail_Accept();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM UPDATE-FORM
        sub_Update_Form();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    }
    private void sub_Accum_Str() throws Exception                                                                                                                         //Natural: ACCUM-STR
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        pnd_Str_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Str1,1).nadd(1);                                                                                             //Natural: ADD 1 TO #STR.#FORM-CNT ( #STR1,1 )
        pnd_Str_Pnd_State_Distr.getValue(pnd_Report_Indexes_Pnd_Str1,1).nadd(ldaTwrl9715.getForm_R_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A));            //Natural: ADD FORM-R.TIRF-STATE-DISTR ( #ST-OCC-A ) TO #STR.#STATE-DISTR ( #STR1,1 )
        pnd_Str_Pnd_State_Tax.getValue(pnd_Report_Indexes_Pnd_Str1,1).nadd(ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A));          //Natural: ADD FORM-R.TIRF-STATE-TAX-WTHLD ( #ST-OCC-A ) TO #STR.#STATE-TAX ( #STR1,1 )
        pnd_Str_Pnd_Local_Distr.getValue(pnd_Report_Indexes_Pnd_Str1,1).nadd(ldaTwrl9715.getForm_R_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A));              //Natural: ADD FORM-R.TIRF-LOC-DISTR ( #ST-OCC-A ) TO #STR.#LOCAL-DISTR ( #STR1,1 )
        pnd_Str_Pnd_Local_Tax.getValue(pnd_Report_Indexes_Pnd_Str1,1).nadd(ldaTwrl9715.getForm_R_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A));            //Natural: ADD FORM-R.TIRF-LOC-TAX-WTHLD ( #ST-OCC-A ) TO #STR.#LOCAL-TAX ( #STR1,1 )
    }
    private void sub_Accum_Stw() throws Exception                                                                                                                         //Natural: ACCUM-STW
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #WS.#K = 1 TO 3
        for (pnd_Ws_Pnd_K.setValue(1); condition(pnd_Ws_Pnd_K.lessOrEqual(3)); pnd_Ws_Pnd_K.nadd(1))
        {
            pnd_Report_Indexes_Pnd_Stw2.compute(new ComputeParameters(false, pnd_Report_Indexes_Pnd_Stw2), pnd_Report_Indexes_Pnd_Stw1.add(pnd_Ws_Pnd_K));                //Natural: ASSIGN #REPORT-INDEXES.#STW2 := #REPORT-INDEXES.#STW1 + #WS.#K
            pnd_Stw_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Stw2,1).nadd(pnd_Recon_Case_Pnd_Form_Cnt.getValue(pnd_Ws_Pnd_K));                                        //Natural: ADD #RECON-CASE.#FORM-CNT ( #K ) TO #STW.#FORM-CNT ( #STW2,1 )
            pnd_Stw_Pnd_State_Distr.getValue(pnd_Report_Indexes_Pnd_Stw2,1).nadd(pnd_Recon_Case_Pnd_State_Distr.getValue(pnd_Ws_Pnd_K));                                  //Natural: ADD #RECON-CASE.#STATE-DISTR ( #K ) TO #STW.#STATE-DISTR ( #STW2,1 )
            pnd_Stw_Pnd_State_Tax.getValue(pnd_Report_Indexes_Pnd_Stw2,1).nadd(pnd_Recon_Case_Pnd_State_Tax.getValue(pnd_Ws_Pnd_K));                                      //Natural: ADD #RECON-CASE.#STATE-TAX ( #K ) TO #STW.#STATE-TAX ( #STW2,1 )
            pnd_Stw_Pnd_Local_Distr.getValue(pnd_Report_Indexes_Pnd_Stw2,1).nadd(pnd_Recon_Case_Pnd_Local_Distr.getValue(pnd_Ws_Pnd_K));                                  //Natural: ADD #RECON-CASE.#LOCAL-DISTR ( #K ) TO #STW.#LOCAL-DISTR ( #STW2,1 )
            pnd_Stw_Pnd_Local_Tax.getValue(pnd_Report_Indexes_Pnd_Stw2,1).nadd(pnd_Recon_Case_Pnd_Local_Tax.getValue(pnd_Ws_Pnd_K));                                      //Natural: ADD #RECON-CASE.#LOCAL-TAX ( #K ) TO #STW.#LOCAL-TAX ( #STW2,1 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Accum_Stg() throws Exception                                                                                                                         //Natural: ACCUM-STG
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        if (condition(ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(new DbsDecimal("0.00")) && ldaTwrl9715.getForm_R_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(new  //Natural: IF FORM-R.TIRF-STATE-TAX-WTHLD ( #ST-OCC-A ) = 0.00 AND FORM-R.TIRF-LOC-TAX-WTHLD ( #ST-OCC-A ) = 0.00 AND FORM-R.TIRF-STATE-DISTR ( #ST-OCC-A ) = 0.00 AND FORM-R.TIRF-LOC-DISTR ( #ST-OCC-A ) = 0.00
            DbsDecimal("0.00")) && ldaTwrl9715.getForm_R_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(new DbsDecimal("0.00")) && ldaTwrl9715.getForm_R_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(new 
            DbsDecimal("0.00"))))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Stg_Pnd_Form_Cnt.getValue(1,":",2,1).nadd(1);                                                                                                                 //Natural: ADD 1 TO #STG.#FORM-CNT ( 1:2,1 )
        pnd_Stg_Pnd_State_Distr.getValue(1,":",2,1).nadd(ldaTwrl9715.getForm_R_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                                //Natural: ADD FORM-R.TIRF-STATE-DISTR ( #ST-OCC-A ) TO #STG.#STATE-DISTR ( 1:2,1 )
        pnd_Stg_Pnd_State_Tax.getValue(1,":",2,1).nadd(ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                              //Natural: ADD FORM-R.TIRF-STATE-TAX-WTHLD ( #ST-OCC-A ) TO #STG.#STATE-TAX ( 1:2,1 )
        pnd_Stg_Pnd_Local_Distr.getValue(1,":",2,1).nadd(ldaTwrl9715.getForm_R_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                                  //Natural: ADD FORM-R.TIRF-LOC-DISTR ( #ST-OCC-A ) TO #STG.#LOCAL-DISTR ( 1:2,1 )
        pnd_Stg_Pnd_Local_Tax.getValue(1,":",2,1).nadd(ldaTwrl9715.getForm_R_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                                //Natural: ADD FORM-R.TIRF-LOC-TAX-WTHLD ( #ST-OCC-A ) TO #STG.#LOCAL-TAX ( 1:2,1 )
    }
    private void sub_Accum_Stg1() throws Exception                                                                                                                        //Natural: ACCUM-STG1
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #REPORT-INDEXES.#STG1 = 1 TO 3
        for (pnd_Report_Indexes_Pnd_Stg1.setValue(1); condition(pnd_Report_Indexes_Pnd_Stg1.lessOrEqual(3)); pnd_Report_Indexes_Pnd_Stg1.nadd(1))
        {
            pnd_Stg_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Stg1,1).nadd(pnd_Recon_Case_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Stg1));                         //Natural: ADD #RECON-CASE.#FORM-CNT ( #STG1 ) TO #STG.#FORM-CNT ( #STG1,1 )
            pnd_Stg_Pnd_State_Distr.getValue(pnd_Report_Indexes_Pnd_Stg1,1).nadd(pnd_Recon_Case_Pnd_State_Distr.getValue(pnd_Report_Indexes_Pnd_Stg1));                   //Natural: ADD #RECON-CASE.#STATE-DISTR ( #STG1 ) TO #STG.#STATE-DISTR ( #STG1,1 )
            pnd_Stg_Pnd_State_Tax.getValue(pnd_Report_Indexes_Pnd_Stg1,1).nadd(pnd_Recon_Case_Pnd_State_Tax.getValue(pnd_Report_Indexes_Pnd_Stg1));                       //Natural: ADD #RECON-CASE.#STATE-TAX ( #STG1 ) TO #STG.#STATE-TAX ( #STG1,1 )
            pnd_Stg_Pnd_Local_Distr.getValue(pnd_Report_Indexes_Pnd_Stg1,1).nadd(pnd_Recon_Case_Pnd_Local_Distr.getValue(pnd_Report_Indexes_Pnd_Stg1));                   //Natural: ADD #RECON-CASE.#LOCAL-DISTR ( #STG1 ) TO #STG.#LOCAL-DISTR ( #STG1,1 )
            pnd_Stg_Pnd_Local_Tax.getValue(pnd_Report_Indexes_Pnd_Stg1,1).nadd(pnd_Recon_Case_Pnd_Local_Tax.getValue(pnd_Report_Indexes_Pnd_Stg1));                       //Natural: ADD #RECON-CASE.#LOCAL-TAX ( #STG1 ) TO #STG.#LOCAL-TAX ( #STG1,1 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Update_Form() throws Exception                                                                                                                       //Natural: UPDATE-FORM
    {
        if (BLNatReinput.isReinput()) return;

        G_FORM:                                                                                                                                                           //Natural: GET FORM-U #CASE-FIELDS.#ACTIVE-ISN
        vw_form_U.readByID(pnd_Case_Fields_Pnd_Active_Isn.getLong(), "G_FORM");
        if (condition(pnd_Case_Fields_Pnd_State_Added.getBoolean()))                                                                                                      //Natural: IF #CASE-FIELDS.#STATE-ADDED
        {
            pnd_Case_Fields_Pnd_St_Occ_A.compute(new ComputeParameters(false, pnd_Case_Fields_Pnd_St_Occ_A), form_U_Count_Casttirf_1099_R_State_Grp.add(1));              //Natural: ASSIGN #CASE-FIELDS.#ST-OCC-A := FORM-U.C*TIRF-1099-R-STATE-GRP + 1
            form_U_Tirf_State_Code.getValue(pnd_Case_Fields_Pnd_St_Occ_A).setValue(pnd_Ws_Pnd_State);                                                                     //Natural: ASSIGN FORM-U.TIRF-STATE-CODE ( #ST-OCC-A ) := #WS.#STATE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Process_Reporting_Active_Rec.getBoolean()))                                                                                     //Natural: IF #PROCESS-REPORTING-ACTIVE-REC
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-HARDCOPY
            sub_Print_Hardcopy();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Case_Fields_Pnd_Update_With_Reporting.setValue(true);                                                                                                     //Natural: ASSIGN #UPDATE-WITH-REPORTING := TRUE
            if (condition(pnd_Hold_Fields_Pnd_Hold_Rej_Ind.equals("Y")))                                                                                                  //Natural: IF #HOLD-REJ-IND = 'Y'
            {
                form_U_Tirf_State_Auth_Rpt_Ind.getValue(pnd_Case_Fields_Pnd_St_Occ_A).setValue("H");                                                                      //Natural: ASSIGN FORM-U.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-A ) := 'H'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                form_U_Tirf_State_Auth_Rpt_Ind.getValue(pnd_Case_Fields_Pnd_St_Occ_A).setValue("C");                                                                      //Natural: ASSIGN FORM-U.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-A ) := 'C'
            }                                                                                                                                                             //Natural: END-IF
            form_U_Tirf_State_Auth_Rpt_Date.getValue(pnd_Case_Fields_Pnd_St_Occ_A).setValue(pnd_Ws_Pnd_Datx);                                                             //Natural: ASSIGN FORM-U.TIRF-STATE-AUTH-RPT-DATE ( #ST-OCC-A ) := #WS.#DATX
            form_U_Tirf_Lu_User.setValue(Global.getINIT_USER());                                                                                                          //Natural: ASSIGN FORM-U.TIRF-LU-USER := *INIT-USER
            form_U_Tirf_Lu_Ts.setValue(pnd_Ws_Pnd_Timx);                                                                                                                  //Natural: ASSIGN FORM-U.TIRF-LU-TS := #WS.#TIMX
            vw_form_U.updateDBRow("G_FORM");                                                                                                                              //Natural: UPDATE ( G-FORM. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Case_Fields_Pnd_Process_Recon_Active_Rec.getBoolean() && ! (pnd_Case_Fields_Pnd_Update_With_Reporting.getBoolean())))                       //Natural: IF #PROCESS-RECON-ACTIVE-REC AND NOT #UPDATE-WITH-REPORTING
            {
                if (condition(ldaTwrl9715.getForm_R_Tirf_State_Hardcopy_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals(" ")))                                        //Natural: IF FORM-R.TIRF-STATE-HARDCOPY-IND ( #ST-OCC-A ) EQ ' '
                {
                    form_U_Tirf_State_Auth_Rpt_Ind.getValue(pnd_Case_Fields_Pnd_St_Occ_A).setValue(" ");                                                                  //Natural: ASSIGN FORM-U.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-A ) := ' '
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Hold_Fields_Pnd_Hold_Rej_Ind.equals("Y")))                                                                                          //Natural: IF #HOLD-REJ-IND = 'Y'
                    {
                        form_U_Tirf_State_Auth_Rpt_Ind.getValue(pnd_Case_Fields_Pnd_St_Occ_A).setValue("J");                                                              //Natural: ASSIGN FORM-U.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-A ) := 'J'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        form_U_Tirf_State_Auth_Rpt_Ind.getValue(pnd_Case_Fields_Pnd_St_Occ_A).setValue("X");                                                              //Natural: ASSIGN FORM-U.TIRF-STATE-AUTH-RPT-IND ( #ST-OCC-A ) := 'X'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                form_U_Tirf_State_Auth_Rpt_Date.getValue(pnd_Case_Fields_Pnd_St_Occ_A).setValue(pnd_Ws_Pnd_Datx);                                                         //Natural: ASSIGN FORM-U.TIRF-STATE-AUTH-RPT-DATE ( #ST-OCC-A ) := #WS.#DATX
                form_U_Tirf_Lu_User.setValue(Global.getINIT_USER());                                                                                                      //Natural: ASSIGN FORM-U.TIRF-LU-USER := *INIT-USER
                form_U_Tirf_Lu_Ts.setValue(pnd_Ws_Pnd_Timx);                                                                                                              //Natural: ASSIGN FORM-U.TIRF-LU-TS := #WS.#TIMX
                vw_form_U.updateDBRow("G_FORM");                                                                                                                          //Natural: UPDATE ( G-FORM. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Setup_Recon_Case() throws Exception                                                                                                                  //Natural: SETUP-RECON-CASE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pnd_Case_Fields_Pnd_Recon_Summary.reset();                                                                                                                        //Natural: RESET #CASE-FIELDS.#RECON-SUMMARY #CASE-FIELDS.#RECON-DETAIL
        pnd_Case_Fields_Pnd_Recon_Detail.reset();
        if (condition(pnd_Case_Fields_Pnd_Prior_Record.getBoolean() && ldaTwrl9710.getForm_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals(new       //Natural: IF #CASE-FIELDS.#PRIOR-RECORD AND FORM.TIRF-STATE-TAX-WTHLD ( #ST-OCC-I ) = 0.00 AND FORM.TIRF-LOC-TAX-WTHLD ( #ST-OCC-I ) = 0.00 AND FORM.TIRF-STATE-DISTR ( #ST-OCC-I ) = 0.00 AND FORM.TIRF-LOC-DISTR ( #ST-OCC-I ) = 0.00
            DbsDecimal("0.00")) && ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals(new DbsDecimal("0.00")) && ldaTwrl9710.getForm_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals(new 
            DbsDecimal("0.00")) && ldaTwrl9710.getForm_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_I).equals(new DbsDecimal("0.00"))))
        {
            pnd_Case_Fields_Pnd_Prior_Record.reset();                                                                                                                     //Natural: RESET #CASE-FIELDS.#PRIOR-RECORD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A).notEquals(new DbsDecimal("0.00")) || ldaTwrl9715.getForm_R_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A).notEquals(new  //Natural: IF ( FORM-R.TIRF-STATE-TAX-WTHLD ( #ST-OCC-A ) NE 0.00 OR FORM-R.TIRF-LOC-TAX-WTHLD ( #ST-OCC-A ) NE 0.00 OR FORM-R.TIRF-STATE-DISTR ( #ST-OCC-A ) NE 0.00 OR FORM-R.TIRF-LOC-DISTR ( #ST-OCC-A ) NE 0.00 ) OR #CASE-FIELDS.#PRIOR-RECORD AND ( FORM.TIRF-STATE-TAX-WTHLD ( #ST-OCC-I ) NE 0.00 OR FORM.TIRF-LOC-TAX-WTHLD ( #ST-OCC-I ) NE 0.00 OR FORM.TIRF-STATE-DISTR ( #ST-OCC-I ) NE 0.00 OR FORM.TIRF-LOC-DISTR ( #ST-OCC-I ) NE 0.00 )
            DbsDecimal("0.00")) || ldaTwrl9715.getForm_R_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A).notEquals(new DbsDecimal("0.00")) || 
            ldaTwrl9715.getForm_R_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A).notEquals(new DbsDecimal("0.00"))) || pnd_Case_Fields_Pnd_Prior_Record.getBoolean() 
            && (ldaTwrl9710.getForm_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_I).notEquals(new DbsDecimal("0.00")) || ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_I).notEquals(new 
            DbsDecimal("0.00")) || ldaTwrl9710.getForm_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_I).notEquals(new DbsDecimal("0.00")) || ldaTwrl9710.getForm_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_I).notEquals(new 
            DbsDecimal("0.00")))))
        {
            pnd_Case_Fields_Pnd_Recon_Summary.setValue(true);                                                                                                             //Natural: ASSIGN #CASE-FIELDS.#RECON-SUMMARY := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* ** ADD CODE FOR GERALDINE
        if (condition(ldaTwrl9715.getForm_R_Tirf_Empty_Form().getBoolean()))                                                                                              //Natural: IF FORM-R.TIRF-EMPTY-FORM
        {
            pnd_Ws_Pnd_Empty_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #EMPTY-CNT
            pnd_Ws_Pnd_Empty_Tot_Cnt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #EMPTY-TOT-CNT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Recon_Case.resetInitial();                                                                                                                                    //Natural: RESET INITIAL #RECON-CASE
        if (condition(pdaTwratin.getTwratin_Pnd_I_Tin_Type().notEquals(ldaTwrl9715.getForm_R_Tirf_Tax_Id_Type()) || pdaTwratin.getTwratin_Pnd_I_Tin().notEquals(ldaTwrl9715.getForm_R_Tirf_Tin()))) //Natural: IF TWRATIN.#I-TIN-TYPE NE FORM-R.TIRF-TAX-ID-TYPE OR TWRATIN.#I-TIN NE FORM-R.TIRF-TIN
        {
            pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(ldaTwrl9715.getForm_R_Tirf_Tax_Id_Type());                                                                    //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := FORM-R.TIRF-TAX-ID-TYPE
            pdaTwratin.getTwratin_Pnd_I_Tin().setValue(ldaTwrl9715.getForm_R_Tirf_Tin());                                                                                 //Natural: ASSIGN TWRATIN.#I-TIN := FORM-R.TIRF-TIN
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIN
            sub_Process_Tin();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Recon_Case_Pnd_Tin.setValue(pdaTwratin.getTwratin_Pnd_O_Tin());                                                                                               //Natural: ASSIGN #RECON-CASE.#TIN := TWRATIN.#O-TIN
        pnd_Recon_Case_Pnd_Cntrct_Py.setValueEdited(ldaTwrl9715.getForm_R_Tirf_Contract_Nbr(),new ReportEditMask("XXXXXXX-X/"));                                          //Natural: MOVE EDITED FORM-R.TIRF-CONTRACT-NBR ( EM = XXXXXXX-X/ ) TO #RECON-CASE.#CNTRCT-PY
        setValueToSubstring(ldaTwrl9715.getForm_R_Tirf_Payee_Cde(),pnd_Recon_Case_Pnd_Cntrct_Py,11,2);                                                                    //Natural: MOVE FORM-R.TIRF-PAYEE-CDE TO SUBSTR ( #RECON-CASE.#CNTRCT-PY,11,2 )
        pnd_Recon_Case_Pnd_Form_Cnt.getValue(1).nadd(1);                                                                                                                  //Natural: ADD 1 TO #RECON-CASE.#FORM-CNT ( 1 )
        pnd_Recon_Case_Pnd_State_Distr.getValue(1).setValue(ldaTwrl9715.getForm_R_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                             //Natural: ASSIGN #RECON-CASE.#STATE-DISTR ( 1 ) := FORM-R.TIRF-STATE-DISTR ( #ST-OCC-A )
        pnd_Recon_Case_Pnd_State_Tax.getValue(1).setValue(ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                           //Natural: ASSIGN #RECON-CASE.#STATE-TAX ( 1 ) := FORM-R.TIRF-STATE-TAX-WTHLD ( #ST-OCC-A )
        pnd_Recon_Case_Pnd_Local_Distr.getValue(1).setValue(ldaTwrl9715.getForm_R_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                               //Natural: ASSIGN #RECON-CASE.#LOCAL-DISTR ( 1 ) := FORM-R.TIRF-LOC-DISTR ( #ST-OCC-A )
        pnd_Recon_Case_Pnd_Local_Tax.getValue(1).setValue(ldaTwrl9715.getForm_R_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A));                             //Natural: ASSIGN #RECON-CASE.#LOCAL-TAX ( 1 ) := FORM-R.TIRF-LOC-TAX-WTHLD ( #ST-OCC-A )
        if (condition(pnd_Case_Fields_Pnd_Prior_Record.getBoolean()))                                                                                                     //Natural: IF #CASE-FIELDS.#PRIOR-RECORD
        {
            pnd_Recon_Case_Pnd_Form_Cnt.getValue(2).nadd(1);                                                                                                              //Natural: ADD 1 TO #RECON-CASE.#FORM-CNT ( 2 )
            pnd_Recon_Case_Pnd_State_Distr.getValue(2).setValue(ldaTwrl9710.getForm_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_I));                           //Natural: ASSIGN #RECON-CASE.#STATE-DISTR ( 2 ) := FORM.TIRF-STATE-DISTR ( #ST-OCC-I )
            pnd_Recon_Case_Pnd_State_Tax.getValue(2).setValue(ldaTwrl9710.getForm_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_I));                         //Natural: ASSIGN #RECON-CASE.#STATE-TAX ( 2 ) := FORM.TIRF-STATE-TAX-WTHLD ( #ST-OCC-I )
            pnd_Recon_Case_Pnd_Local_Distr.getValue(2).setValue(ldaTwrl9710.getForm_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_I));                             //Natural: ASSIGN #RECON-CASE.#LOCAL-DISTR ( 2 ) := FORM.TIRF-LOC-DISTR ( #ST-OCC-I )
            pnd_Recon_Case_Pnd_Local_Tax.getValue(2).setValue(ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_I));                           //Natural: ASSIGN #RECON-CASE.#LOCAL-TAX ( 2 ) := FORM.TIRF-LOC-TAX-WTHLD ( #ST-OCC-I )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Recon_Case_Pnd_Form_Cnt.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Form_Cnt.getValue(3)), pnd_Recon_Case_Pnd_Form_Cnt.getValue(1).subtract(pnd_Recon_Case_Pnd_Form_Cnt.getValue(2))); //Natural: ASSIGN #RECON-CASE.#FORM-CNT ( 3 ) := #RECON-CASE.#FORM-CNT ( 1 ) - #RECON-CASE.#FORM-CNT ( 2 )
        if (condition(pnd_Recon_Case_Pnd_State_Tax.getValue(1).equals(new DbsDecimal("0.00")) && pnd_Recon_Case_Pnd_Local_Tax.getValue(1).equals(new DbsDecimal("0.00"))  //Natural: IF #RECON-CASE.#STATE-TAX ( 1 ) = 0.00 AND #RECON-CASE.#LOCAL-TAX ( 1 ) = 0.00 AND #RECON-CASE.#STATE-DISTR ( 1 ) = 0.00 AND #RECON-CASE.#LOCAL-DISTR ( 1 ) = 0.00
            && pnd_Recon_Case_Pnd_State_Distr.getValue(1).equals(new DbsDecimal("0.00")) && pnd_Recon_Case_Pnd_Local_Distr.getValue(1).equals(new DbsDecimal("0.00"))))
        {
            pnd_Recon_Case_Pnd_State_Distr.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_State_Distr.getValue(3)), pnd_Recon_Case_Pnd_State_Distr.getValue(2).multiply(-1)); //Natural: ASSIGN #RECON-CASE.#STATE-DISTR ( 3 ) := #RECON-CASE.#STATE-DISTR ( 2 ) * -1
            pnd_Recon_Case_Pnd_State_Tax.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_State_Tax.getValue(3)), pnd_Recon_Case_Pnd_State_Tax.getValue(2).multiply(-1)); //Natural: ASSIGN #RECON-CASE.#STATE-TAX ( 3 ) := #RECON-CASE.#STATE-TAX ( 2 ) * -1
            pnd_Recon_Case_Pnd_Local_Distr.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Local_Distr.getValue(3)), pnd_Recon_Case_Pnd_Local_Distr.getValue(2).multiply(-1)); //Natural: ASSIGN #RECON-CASE.#LOCAL-DISTR ( 3 ) := #RECON-CASE.#LOCAL-DISTR ( 2 ) * -1
            pnd_Recon_Case_Pnd_Local_Tax.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Local_Tax.getValue(3)), pnd_Recon_Case_Pnd_Local_Tax.getValue(2).multiply(-1)); //Natural: ASSIGN #RECON-CASE.#LOCAL-TAX ( 3 ) := #RECON-CASE.#LOCAL-TAX ( 2 ) * -1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Recon_Case_Pnd_State_Distr.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_State_Distr.getValue(3)), pnd_Recon_Case_Pnd_State_Distr.getValue(1).subtract(pnd_Recon_Case_Pnd_State_Distr.getValue(2))); //Natural: ASSIGN #RECON-CASE.#STATE-DISTR ( 3 ) := #RECON-CASE.#STATE-DISTR ( 1 ) - #RECON-CASE.#STATE-DISTR ( 2 )
            pnd_Recon_Case_Pnd_State_Tax.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_State_Tax.getValue(3)), pnd_Recon_Case_Pnd_State_Tax.getValue(1).subtract(pnd_Recon_Case_Pnd_State_Tax.getValue(2))); //Natural: ASSIGN #RECON-CASE.#STATE-TAX ( 3 ) := #RECON-CASE.#STATE-TAX ( 1 ) - #RECON-CASE.#STATE-TAX ( 2 )
            pnd_Recon_Case_Pnd_Local_Distr.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Local_Distr.getValue(3)), pnd_Recon_Case_Pnd_Local_Distr.getValue(1).subtract(pnd_Recon_Case_Pnd_Local_Distr.getValue(2))); //Natural: ASSIGN #RECON-CASE.#LOCAL-DISTR ( 3 ) := #RECON-CASE.#LOCAL-DISTR ( 1 ) - #RECON-CASE.#LOCAL-DISTR ( 2 )
            pnd_Recon_Case_Pnd_Local_Tax.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Local_Tax.getValue(3)), pnd_Recon_Case_Pnd_Local_Tax.getValue(1).subtract(pnd_Recon_Case_Pnd_Local_Tax.getValue(2))); //Natural: ASSIGN #RECON-CASE.#LOCAL-TAX ( 3 ) := #RECON-CASE.#LOCAL-TAX ( 1 ) - #RECON-CASE.#LOCAL-TAX ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Recon_Case_Pnd_Form_Cnt.getValue(3).equals(new DbsDecimal("0.00")) && pnd_Recon_Case_Pnd_State_Distr.getValue(3).equals(new                     //Natural: IF #RECON-CASE.#FORM-CNT ( 3 ) = 0.00 AND #RECON-CASE.#STATE-DISTR ( 3 ) = 0.00 AND #RECON-CASE.#STATE-TAX ( 3 ) = 0.00 AND #RECON-CASE.#LOCAL-DISTR ( 3 ) = 0.00 AND #RECON-CASE.#LOCAL-TAX ( 3 ) = 0.00
            DbsDecimal("0.00")) && pnd_Recon_Case_Pnd_State_Tax.getValue(3).equals(new DbsDecimal("0.00")) && pnd_Recon_Case_Pnd_Local_Distr.getValue(3).equals(new 
            DbsDecimal("0.00")) && pnd_Recon_Case_Pnd_Local_Tax.getValue(3).equals(new DbsDecimal("0.00"))))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Case_Fields_Pnd_Recon_Detail.setValue(true);                                                                                                              //Natural: ASSIGN #CASE-FIELDS.#RECON-DETAIL := TRUE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Summary_Reports() throws Exception                                                                                                                   //Natural: SUMMARY-REPORTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        if (condition(pnd_Case_Fields2_Pnd_Recon_And_Reporting.getBoolean()))                                                                                             //Natural: IF #RECON-AND-REPORTING
        {
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORTS-C
            sub_Summary_Reports_C();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORTS-W
        sub_Summary_Reports_W();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORTS-G
        sub_Summary_Reports_G();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    }
    private void sub_Summary_Reports_C() throws Exception                                                                                                                 //Natural: SUMMARY-REPORTS-C
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(3));                                                                                                                 //Natural: NEWPAGE ( 3 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(4));                                                                                                                 //Natural: NEWPAGE ( 4 )
        if (condition(Global.isEscape())){return;}
        FOR03:                                                                                                                                                            //Natural: FOR #STR1 = 1 TO #STR-MAX-LINES
        for (pnd_Report_Indexes_Pnd_Str1.setValue(1); condition(pnd_Report_Indexes_Pnd_Str1.lessOrEqual(pnd_Ws_Const_Pnd_Str_Max_Lines)); pnd_Report_Indexes_Pnd_Str1.nadd(1))
        {
            //*  IF #STR1 = 8
            if (condition(pnd_Report_Indexes_Pnd_Str1.equals(7)))                                                                                                         //Natural: IF #STR1 = 7
            {
                getReports().write(2, "=",new RepeatItem(100));                                                                                                           //Natural: WRITE ( 2 ) '=' ( 100 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(2, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",                         //Natural: DISPLAY ( 2 ) ( ES = ON HC = R ) '/' #STR.#HEADER1 ( #STR1 ) / '/' #STR.#HEADER2 ( #STR1 ) 'Form/Count' #STR.#FORM-CNT ( #STR1,#S2 ) 'State/Distribution' #STR.#STATE-DISTR ( #STR1,#S2 ) 'State/Withholding' #STR.#STATE-TAX ( #STR1,#S2 ) 'Local/Distribution' #STR.#LOCAL-DISTR ( #STR1,#S2 ) 'Local/Withholding' #STR.#LOCAL-TAX ( #STR1,#S2 )
            		pnd_Str_Pnd_Header1.getValue(pnd_Report_Indexes_Pnd_Str1),NEWLINE,"/",
            		pnd_Str_Pnd_Header2.getValue(pnd_Report_Indexes_Pnd_Str1),"Form/Count",
            		pnd_Str_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Str1,pnd_Ws_Pnd_S2),"State/Distribution",
            		pnd_Str_Pnd_State_Distr.getValue(pnd_Report_Indexes_Pnd_Str1,pnd_Ws_Pnd_S2),"State/Withholding",
            		pnd_Str_Pnd_State_Tax.getValue(pnd_Report_Indexes_Pnd_Str1,pnd_Ws_Pnd_S2),"Local/Distribution",
            		pnd_Str_Pnd_Local_Distr.getValue(pnd_Report_Indexes_Pnd_Str1,pnd_Ws_Pnd_S2),"Local/Withholding",
            		pnd_Str_Pnd_Local_Tax.getValue(pnd_Report_Indexes_Pnd_Str1,pnd_Ws_Pnd_S2));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(2, "EMPTY FORMS           ",pnd_Ws_Pnd_Empty_Frm2);                                                                                            //Natural: WRITE ( 2 ) 'EMPTY FORMS           ' #EMPTY-FRM2
        if (Global.isEscape()) return;
    }
    private void sub_Summary_Reports_W() throws Exception                                                                                                                 //Natural: SUMMARY-REPORTS-W
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(5));                                                                                                                 //Natural: NEWPAGE ( 5 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(6));                                                                                                                 //Natural: NEWPAGE ( 6 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(7));                                                                                                                 //Natural: NEWPAGE ( 7 )
        if (condition(Global.isEscape())){return;}
        FOR04:                                                                                                                                                            //Natural: FOR #STW1 = 1 TO #STW-MAX-LINES
        for (pnd_Report_Indexes_Pnd_Stw1.setValue(1); condition(pnd_Report_Indexes_Pnd_Stw1.lessOrEqual(pnd_Ws_Const_Pnd_Stw_Max_Lines)); pnd_Report_Indexes_Pnd_Stw1.nadd(1))
        {
            if (condition(pnd_Report_Indexes_Pnd_Stw1.equals(3) || pnd_Report_Indexes_Pnd_Stw1.equals(6)))                                                                //Natural: IF #STW1 = 3 OR = 6
            {
                getReports().write(5, "=",new RepeatItem(95));                                                                                                            //Natural: WRITE ( 5 ) '=' ( 95 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(5, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",                         //Natural: DISPLAY ( 5 ) ( ES = ON HC = R ) '/' #STW.#HEADER ( #STW1 ) 'Form/Count' #STW.#FORM-CNT ( #STW1,#S2 ) 'State/Distribution' #STW.#STATE-DISTR ( #STW1,#S2 ) 'State/Withholding' #STW.#STATE-TAX ( #STW1,#S2 ) 'Local/Distribution' #STW.#LOCAL-DISTR ( #STW1,#S2 ) 'Local/Withholding' #STW.#LOCAL-TAX ( #STW1,#S2 )
            		pnd_Stw_Pnd_Header.getValue(pnd_Report_Indexes_Pnd_Stw1),"Form/Count",
            		pnd_Stw_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Stw1,pnd_Ws_Pnd_S2),"State/Distribution",
            		pnd_Stw_Pnd_State_Distr.getValue(pnd_Report_Indexes_Pnd_Stw1,pnd_Ws_Pnd_S2),"State/Withholding",
            		pnd_Stw_Pnd_State_Tax.getValue(pnd_Report_Indexes_Pnd_Stw1,pnd_Ws_Pnd_S2),"Local/Distribution",
            		pnd_Stw_Pnd_Local_Distr.getValue(pnd_Report_Indexes_Pnd_Stw1,pnd_Ws_Pnd_S2),"Local/Withholding",
            		pnd_Stw_Pnd_Local_Tax.getValue(pnd_Report_Indexes_Pnd_Stw1,pnd_Ws_Pnd_S2));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Report_Indexes_Pnd_Stw1.equals(3)))                                                                                                         //Natural: IF #STW1 = 3
            {
                getReports().skip(5, 4);                                                                                                                                  //Natural: SKIP ( 5 ) 4 LINES
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Summary_Reports_G() throws Exception                                                                                                                 //Natural: SUMMARY-REPORTS-G
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(8));                                                                                                                 //Natural: NEWPAGE ( 8 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(9));                                                                                                                 //Natural: NEWPAGE ( 9 )
        if (condition(Global.isEscape())){return;}
        FOR05:                                                                                                                                                            //Natural: FOR #STG1 = 1 TO #STG-MAX-LINES
        for (pnd_Report_Indexes_Pnd_Stg1.setValue(1); condition(pnd_Report_Indexes_Pnd_Stg1.lessOrEqual(pnd_Ws_Const_Pnd_Stg_Max_Lines)); pnd_Report_Indexes_Pnd_Stg1.nadd(1))
        {
            if (condition(pnd_Report_Indexes_Pnd_Stg1.equals(3)))                                                                                                         //Natural: IF #STG1 = 3
            {
                getReports().write(8, "=",new RepeatItem(100));                                                                                                           //Natural: WRITE ( 8 ) '=' ( 100 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(8, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",                         //Natural: DISPLAY ( 8 ) ( ES = ON HC = R ) '/' #STG.#HEADER ( #STG1 ) 'Form/Count' #STG.#FORM-CNT ( #STG1,#S2 ) 'State/Distribution' #STG.#STATE-DISTR ( #STG1,#S2 ) 'State/Withholding' #STG.#STATE-TAX ( #STG1,#S2 ) 'Local/Distribution' #STG.#LOCAL-DISTR ( #STG1,#S2 ) 'Local/Withholding' #STG.#LOCAL-TAX ( #STG1,#S2 )
            		pnd_Stg_Pnd_Header.getValue(pnd_Report_Indexes_Pnd_Stg1),"Form/Count",
            		pnd_Stg_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Stg1,pnd_Ws_Pnd_S2),"State/Distribution",
            		pnd_Stg_Pnd_State_Distr.getValue(pnd_Report_Indexes_Pnd_Stg1,pnd_Ws_Pnd_S2),"State/Withholding",
            		pnd_Stg_Pnd_State_Tax.getValue(pnd_Report_Indexes_Pnd_Stg1,pnd_Ws_Pnd_S2),"Local/Distribution",
            		pnd_Stg_Pnd_Local_Distr.getValue(pnd_Report_Indexes_Pnd_Stg1,pnd_Ws_Pnd_S2),"Local/Withholding",
            		pnd_Stg_Pnd_Local_Tax.getValue(pnd_Report_Indexes_Pnd_Stg1,pnd_Ws_Pnd_S2));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  WRITE  (8)
            //*    'EMPTY FORMS               '         #EMPTY-CNT
            if (condition(pnd_Report_Indexes_Pnd_Stg1.equals(3)))                                                                                                         //Natural: IF #STG1 = 3
            {
                getReports().skip(8, 4);                                                                                                                                  //Natural: SKIP ( 8 ) 4 LINES
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE  (8)
        getReports().write(8, "EMPTY FORMS           ",pnd_Ws_Pnd_Empty_Cnt);                                                                                             //Natural: WRITE ( 8 ) 'EMPTY FORMS           ' #EMPTY-CNT
        if (Global.isEscape()) return;
    }
    private void sub_Str_Detail_Accept() throws Exception                                                                                                                 //Natural: STR-DETAIL-ACCEPT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        if (condition(pdaTwratin.getTwratin_Pnd_I_Tin_Type().notEquals(pdaTwraform.getPnd_Twraform_Tirf_Tax_Id_Type()) || pdaTwratin.getTwratin_Pnd_I_Tin().notEquals(pdaTwraform.getPnd_Twraform_Tirf_Tin()))) //Natural: IF TWRATIN.#I-TIN-TYPE NE #TWRAFORM.TIRF-TAX-ID-TYPE OR TWRATIN.#I-TIN NE #TWRAFORM.TIRF-TIN
        {
            pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(pdaTwraform.getPnd_Twraform_Tirf_Tax_Id_Type());                                                              //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := #TWRAFORM.TIRF-TAX-ID-TYPE
            pdaTwratin.getTwratin_Pnd_I_Tin().setValue(pdaTwraform.getPnd_Twraform_Tirf_Tin());                                                                           //Natural: ASSIGN TWRATIN.#I-TIN := #TWRAFORM.TIRF-TIN
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIN
            sub_Process_Tin();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(4, new ReportEmptyLineSuppression(true),"///TIN",                                                                                            //Natural: DISPLAY ( 4 ) ( ES = ON ) '///TIN' TWRATIN.#O-TIN / '/' TWRATIN.#O-TIN-TYPE-DESC / '/' #WS.#DISP-REC '///Contract' #TWRAFORM.TIRF-CONTRACT-NBR 'Pa/y/e/e' #TWRAFORM.TIRF-PAYEE-CDE '//Name' #TWRAFORM.TIRF-PARTICIPANT-NAME ( HC = L ) / 'Address' #TWRAFORM.TIRF-ADDR-LN1 ( HC = L ) / '/' #TWRAFORM.TIRF-ADDR-LN2 ( HC = L ) / '/' #TWRAFORM.TIRF-ADDR-LN3 ( HC = L ) / '/' #TWRAFORM.TIRF-ADDR-LN4 ( HC = L ) / '/' #TWRAFORM.TIRF-ADDR-LN5 ( HC = L ) / '/' #TWRAFORM.TIRF-ADDR-LN6 ( HC = L ) '#/of/Sta/tes' #TWRAFORM.C-1099-R ( EM = ZZ9 HC = R ) 'S/T/A/T' #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '/I/R/S' #TWRAFORM.TIRF-STATE-IRS-RPT-IND ( #ST-OCC ) '///State Distrib' #TWRAFORM.TIRF-STATE-DISTR ( #ST-OCC ) ( HC = R EM = -ZZZ,ZZZ,ZZ9.99 ) '///State Tax' #TWRAFORM.TIRF-STATE-TAX-WTHLD ( #ST-OCC ) ( HC = R EM = -Z,ZZZ,ZZ9.99 ) '///Local Distrib' #TWRAFORM.TIRF-LOC-DISTR ( #ST-OCC ) ( HC = R EM = -ZZZ,ZZZ,ZZ9.99 ) '///Local Tax' #TWRAFORM.TIRF-LOC-TAX-WTHLD ( #ST-OCC ) ( HC = R EM = -Z,ZZZ,ZZ9.99 )
        		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),NEWLINE,"/",
        		pnd_Ws_Pnd_Disp_Rec,"///Contract",
        		pdaTwraform.getPnd_Twraform_Tirf_Contract_Nbr(),"Pa/y/e/e",
        		pdaTwraform.getPnd_Twraform_Tirf_Payee_Cde(),"//Name",
        		pdaTwraform.getPnd_Twraform_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"#/of/Sta/tes",
        		pdaTwraform.getPnd_Twraform_C_1099_R(), new ReportEditMask ("ZZ9"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"S/T/A/T",
            
        		pnd_State_Table_Tircntl_State_Alpha_Code,"/I/R/S",
        		pdaTwraform.getPnd_Twraform_Tirf_State_Irs_Rpt_Ind().getValue(pnd_Ws_Pnd_St_Occ),"///State Distrib",
        		pdaTwraform.getPnd_Twraform_Tirf_State_Distr().getValue(pnd_Ws_Pnd_St_Occ), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"///State Tax",
        		pdaTwraform.getPnd_Twraform_Tirf_State_Tax_Wthld().getValue(pnd_Ws_Pnd_St_Occ), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), 
            new ReportEditMask ("-Z,ZZZ,ZZ9.99"),"///Local Distrib",
        		pdaTwraform.getPnd_Twraform_Tirf_Loc_Distr().getValue(pnd_Ws_Pnd_St_Occ), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"///Local Tax",
        		pdaTwraform.getPnd_Twraform_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Pnd_St_Occ), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), 
            new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
    }
    private void sub_Str_Detail_Reject() throws Exception                                                                                                                 //Natural: STR-DETAIL-REJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        if (condition(pdaTwratin.getTwratin_Pnd_I_Tin_Type().notEquals(ldaTwrl9715.getForm_R_Tirf_Tax_Id_Type()) || pdaTwratin.getTwratin_Pnd_I_Tin().notEquals(ldaTwrl9715.getForm_R_Tirf_Tin()))) //Natural: IF TWRATIN.#I-TIN-TYPE NE FORM-R.TIRF-TAX-ID-TYPE OR TWRATIN.#I-TIN NE FORM-R.TIRF-TIN
        {
            pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(ldaTwrl9715.getForm_R_Tirf_Tax_Id_Type());                                                                    //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := FORM-R.TIRF-TAX-ID-TYPE
            pdaTwratin.getTwratin_Pnd_I_Tin().setValue(ldaTwrl9715.getForm_R_Tirf_Tin());                                                                                 //Natural: ASSIGN TWRATIN.#I-TIN := FORM-R.TIRF-TIN
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIN
            sub_Process_Tin();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(3, new ReportEmptyLineSuppression(true),"///TIN",                                                                                            //Natural: DISPLAY ( 3 ) ( ES = ON ) '///TIN' TWRATIN.#O-TIN / '/' TWRATIN.#O-TIN-TYPE-DESC '///Contract' FORM-R.TIRF-CONTRACT-NBR 'Pa/y/e/e' FORM-R.TIRF-PAYEE-CDE '//Name' FORM-R.TIRF-PARTICIPANT-NAME ( HC = L ) / 'Address' FORM-R.TIRF-ADDR-LN1 ( HC = L ) / '/' FORM-R.TIRF-ADDR-LN2 ( HC = L ) / '/' FORM-R.TIRF-ADDR-LN3 ( HC = L ) / '/' FORM-R.TIRF-ADDR-LN4 ( HC = L ) / '/' FORM-R.TIRF-ADDR-LN5 ( HC = L ) / '/' FORM-R.TIRF-ADDR-LN6 ( HC = L ) '#/of/Sta/tes' FORM-R.C*TIRF-1099-R-STATE-GRP ( EM = ZZ9 HC = R ) 'S/T/A/T' #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '/I/R/S' FORM-R.TIRF-STATE-IRS-RPT-IND ( #ST-OCC-A ) '///State Distrib' FORM-R.TIRF-STATE-DISTR ( #ST-OCC-A ) ( HC = R ) '///State Tax' FORM-R.TIRF-STATE-TAX-WTHLD ( #ST-OCC-A ) ( HC = R ) '///Local Distrib' FORM-R.TIRF-LOC-DISTR ( #ST-OCC-A ) ( HC = R ) '///Local Tax' FORM-R.TIRF-LOC-TAX-WTHLD ( #ST-OCC-A ) ( HC = R )
        		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),"///Contract",
        		ldaTwrl9715.getForm_R_Tirf_Contract_Nbr(),"Pa/y/e/e",
        		ldaTwrl9715.getForm_R_Tirf_Payee_Cde(),"//Name",
        		ldaTwrl9715.getForm_R_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"#/of/Sta/tes",
        		ldaTwrl9715.getForm_R_Count_Casttirf_1099_R_State_Grp(), new ReportEditMask ("ZZ9"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
            "S/T/A/T",
        		pnd_State_Table_Tircntl_State_Alpha_Code,"/I/R/S",
        		ldaTwrl9715.getForm_R_Tirf_State_Irs_Rpt_Ind().getValue(pnd_Case_Fields_Pnd_St_Occ_A),"///State Distrib",
        		ldaTwrl9715.getForm_R_Tirf_State_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
            "///State Tax",
        		ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
            "///Local Distrib",
        		ldaTwrl9715.getForm_R_Tirf_Loc_Distr().getValue(pnd_Case_Fields_Pnd_St_Occ_A), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
            "///Local Tax",
        		ldaTwrl9715.getForm_R_Tirf_Loc_Tax_Wthld().getValue(pnd_Case_Fields_Pnd_St_Occ_A), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 3 ) 1 LINES
    }
    private void sub_Stw_Detail_Accept() throws Exception                                                                                                                 //Natural: STW-DETAIL-ACCEPT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        if (condition(pnd_Case_Fields_Pnd_Recon_Summary.getBoolean()))                                                                                                    //Natural: IF #CASE-FIELDS.#RECON-SUMMARY
        {
            pnd_Report_Indexes_Pnd_Stw1.reset();                                                                                                                          //Natural: RESET #REPORT-INDEXES.#STW1
                                                                                                                                                                          //Natural: PERFORM ACCUM-STW
            sub_Accum_Stw();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Recon_Detail.getBoolean()))                                                                                                     //Natural: IF #CASE-FIELDS.#RECON-DETAIL
        {
            Global.format("IS=OFF");                                                                                                                                      //Natural: SUSPEND IDENTICAL SUPPRESS ( 06 )
            FOR06:                                                                                                                                                        //Natural: FOR #WS.#K = 1 TO 3
            for (pnd_Ws_Pnd_K.setValue(1); condition(pnd_Ws_Pnd_K.lessOrEqual(3)); pnd_Ws_Pnd_K.nadd(1))
            {
                if (condition(pnd_Ws_Pnd_K.equals(2) && ! (pnd_Case_Fields_Pnd_Prior_Record.getBoolean())))                                                               //Natural: IF #WS.#K = 2 AND NOT #CASE-FIELDS.#PRIOR-RECORD
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(6, "/TIN",                                                                                                                           //Natural: DISPLAY ( 06 ) '/TIN' #RECON-CASE.#TIN ( IS = ON ) 'Contract/Payee' #RECON-CASE.#CNTRCT-PY ( IS = ON ) '/' #RECON-CASE.#HEADER ( #K ) 'Form Count' #RECON-CASE.#FORM-CNT ( #K ) ( HC = R ) 'State Distrib' #RECON-CASE.#STATE-DISTR ( #K ) ( HC = R ) 'State Tax' #RECON-CASE.#STATE-TAX ( #K ) ( HC = R ) 'Local Distrib' #RECON-CASE.#LOCAL-DISTR ( #K ) ( HC = R ) 'Local Tax' #RECON-CASE.#LOCAL-TAX ( #K ) ( HC = R )
                		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
                		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
                		pnd_Recon_Case_Pnd_Header.getValue(pnd_Ws_Pnd_K),"Form Count",
                		pnd_Recon_Case_Pnd_Form_Cnt.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Distrib",
                    
                		pnd_Recon_Case_Pnd_State_Distr.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Tax",
                    
                		pnd_Recon_Case_Pnd_State_Tax.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Distrib",
                    
                		pnd_Recon_Case_Pnd_Local_Distr.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Tax",
                    
                		pnd_Recon_Case_Pnd_Local_Tax.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_K.equals(3)))                                                                                                                    //Natural: IF #WS.#K = 3
                {
                    getReports().skip(6, 1);                                                                                                                              //Natural: SKIP ( 06 ) 1 LINES
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Stw_Detail_Reject() throws Exception                                                                                                                 //Natural: STW-DETAIL-REJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        if (condition(pnd_Case_Fields_Pnd_Recon_Summary.getBoolean()))                                                                                                    //Natural: IF #CASE-FIELDS.#RECON-SUMMARY
        {
            pnd_Report_Indexes_Pnd_Stw1.setValue(3);                                                                                                                      //Natural: ASSIGN #REPORT-INDEXES.#STW1 := 3
                                                                                                                                                                          //Natural: PERFORM ACCUM-STW
            sub_Accum_Stw();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Recon_Detail.getBoolean()))                                                                                                     //Natural: IF #CASE-FIELDS.#RECON-DETAIL
        {
            Global.format("IS=OFF");                                                                                                                                      //Natural: SUSPEND IDENTICAL SUPPRESS ( 07 )
            FOR07:                                                                                                                                                        //Natural: FOR #WS.#K = 1 TO 3
            for (pnd_Ws_Pnd_K.setValue(1); condition(pnd_Ws_Pnd_K.lessOrEqual(3)); pnd_Ws_Pnd_K.nadd(1))
            {
                if (condition(pnd_Ws_Pnd_K.equals(2) && ! (pnd_Case_Fields_Pnd_Prior_Record.getBoolean())))                                                               //Natural: IF #WS.#K = 2 AND NOT #CASE-FIELDS.#PRIOR-RECORD
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(7, "/TIN",                                                                                                                           //Natural: DISPLAY ( 07 ) '/TIN' #RECON-CASE.#TIN ( IS = ON ) 'Contract/Payee' #RECON-CASE.#CNTRCT-PY ( IS = ON ) '/' #RECON-CASE.#HEADER ( #K ) 'Form Count' #RECON-CASE.#FORM-CNT ( #K ) ( HC = R ) 'State Distrib' #RECON-CASE.#STATE-DISTR ( #K ) ( HC = R ) 'State Tax' #RECON-CASE.#STATE-TAX ( #K ) ( HC = R ) 'Local Distrib' #RECON-CASE.#LOCAL-DISTR ( #K ) ( HC = R ) 'Local Tax' #RECON-CASE.#LOCAL-TAX ( #K ) ( HC = R )
                		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
                		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
                		pnd_Recon_Case_Pnd_Header.getValue(pnd_Ws_Pnd_K),"Form Count",
                		pnd_Recon_Case_Pnd_Form_Cnt.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Distrib",
                    
                		pnd_Recon_Case_Pnd_State_Distr.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Tax",
                    
                		pnd_Recon_Case_Pnd_State_Tax.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Distrib",
                    
                		pnd_Recon_Case_Pnd_Local_Distr.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Tax",
                    
                		pnd_Recon_Case_Pnd_Local_Tax.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_K.equals(3)))                                                                                                                    //Natural: IF #WS.#K = 3
                {
                    getReports().skip(7, 1);                                                                                                                              //Natural: SKIP ( 07 ) 1 LINES
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Stg_Detail() throws Exception                                                                                                                        //Natural: STG-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        if (condition(pnd_Case_Fields_Pnd_Recon_Summary.getBoolean()))                                                                                                    //Natural: IF #CASE-FIELDS.#RECON-SUMMARY
        {
                                                                                                                                                                          //Natural: PERFORM ACCUM-STG1
            sub_Accum_Stg1();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Recon_Detail.getBoolean()))                                                                                                     //Natural: IF #CASE-FIELDS.#RECON-DETAIL
        {
            Global.format("IS=OFF");                                                                                                                                      //Natural: SUSPEND IDENTICAL SUPPRESS ( 09 )
            FOR08:                                                                                                                                                        //Natural: FOR #WS.#K = 1 TO 3
            for (pnd_Ws_Pnd_K.setValue(1); condition(pnd_Ws_Pnd_K.lessOrEqual(3)); pnd_Ws_Pnd_K.nadd(1))
            {
                if (condition(pnd_Ws_Pnd_K.equals(2) && ! (pnd_Case_Fields_Pnd_Prior_Record.getBoolean())))                                                               //Natural: IF #WS.#K = 2 AND NOT #CASE-FIELDS.#PRIOR-RECORD
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(9, "/TIN",                                                                                                                           //Natural: DISPLAY ( 09 ) '/TIN' #RECON-CASE.#TIN ( IS = ON ) 'Contract/Payee' #RECON-CASE.#CNTRCT-PY ( IS = ON ) '/' #RECON-CASE.#HEADER ( #K ) 'Form Count' #RECON-CASE.#FORM-CNT ( #K ) ( HC = R ) 'State Distrib' #RECON-CASE.#STATE-DISTR ( #K ) ( HC = R ) 'State Tax' #RECON-CASE.#STATE-TAX ( #K ) ( HC = R ) 'Local Distrib' #RECON-CASE.#LOCAL-DISTR ( #K ) ( HC = R ) 'Local Tax' #RECON-CASE.#LOCAL-TAX ( #K ) ( HC = R )
                		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
                		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
                		pnd_Recon_Case_Pnd_Header.getValue(pnd_Ws_Pnd_K),"Form Count",
                		pnd_Recon_Case_Pnd_Form_Cnt.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Distrib",
                    
                		pnd_Recon_Case_Pnd_State_Distr.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Tax",
                    
                		pnd_Recon_Case_Pnd_State_Tax.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Distrib",
                    
                		pnd_Recon_Case_Pnd_Local_Distr.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Tax",
                    
                		pnd_Recon_Case_Pnd_Local_Tax.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_K.equals(3)))                                                                                                                    //Natural: IF #WS.#K = 3
                {
                    getReports().skip(9, 1);                                                                                                                              //Natural: SKIP ( 09 ) 1 LINES
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Tin() throws Exception                                                                                                                       //Natural: PROCESS-TIN
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        DbsUtil.callnat(Twrntin.class , getCurrentProcessState(), pdaTwratin.getTwratin_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratin.getTwratin_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTIN' USING TWRATIN.#INPUT-PARMS ( AD = O ) TWRATIN.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    //*  BK
    private void sub_Process_Company() throws Exception                                                                                                                   //Natural: PROCESS-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(pnd_Sort_Rec_Tirf_Company_Cde);                                                                              //Natural: ASSIGN #TWRACOMP.#COMP-CODE := #SORT-REC.TIRF-COMPANY-CDE
        pdaTwracomp.getPnd_Twracomp_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwracomp.getPnd_Twracomp_Pnd_Tax_Year()), pnd_Sort_Rec_Tirf_Tax_Year.val());   //Natural: ASSIGN #TWRACOMP.#TAX-YEAR := VAL ( #SORT-REC.TIRF-TAX-YEAR )
        DbsUtil.callnat(Twrncomp.class , getCurrentProcessState(), pdaTwracomp.getPnd_Twracomp_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracomp.getPnd_Twracomp_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOMP' USING #TWRACOMP.#INPUT-PARMS ( AD = O ) #TWRACOMP.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        pnd_Ws_Pnd_Company_Line.setValue("Company:");                                                                                                                     //Natural: ASSIGN #WS.#COMPANY-LINE := 'Company:'
        setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),pnd_Ws_Pnd_Company_Line,11,4);                                                              //Natural: MOVE #TWRACOMP.#COMP-SHORT-NAME TO SUBSTR ( #WS.#COMPANY-LINE,11,4 )
        setValueToSubstring("-",pnd_Ws_Pnd_Company_Line,16,1);                                                                                                            //Natural: MOVE '-' TO SUBSTR ( #WS.#COMPANY-LINE,16,1 )
        setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Name(),pnd_Ws_Pnd_Company_Line,18,37);                                                                   //Natural: MOVE #TWRACOMP.#COMP-NAME TO SUBSTR ( #WS.#COMPANY-LINE,18,37 )
        pnd_Ws_Pnd_Company_Break.reset();                                                                                                                                 //Natural: RESET #WS.#COMPANY-BREAK
    }
    private void sub_Process_Input_Parms() throws Exception                                                                                                               //Natural: PROCESS-INPUT-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        //*  INPUT #WS.#INPUT-PARMS
        if (condition(pnd_Ws_Pnd_Tax_Year.equals(getZero())))                                                                                                             //Natural: IF #WS.#TAX-YEAR = 0
        {
            pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), Global.getDATN().divide(10000).subtract(1));                                   //Natural: ASSIGN #WS.#TAX-YEAR := *DATN / 10000 - 1
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"State Correctrion Reporting parameters:",NEWLINE,new TabSetting(26),"Tax Year...........:",         //Natural: WRITE ( 1 ) 7T 'State Correctrion Reporting parameters:' / 26T 'Tax Year...........:' #WS.#TAX-YEAR
            pnd_Ws_Pnd_Tax_Year);
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_State.greaterOrEqual("00") && pnd_Ws_Pnd_State.lessOrEqual(pnd_Ws_Const_Pnd_State_Max) && DbsUtil.maskMatches(pnd_Ws_Pnd_State,          //Natural: IF #WS.#STATE = '00' THRU #WS-CONST.#STATE-MAX AND #WS.#STATE = MASK ( NN )
            "NN")))
        {
            pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                         //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
            pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                                         //Natural: ASSIGN TWRATBL2.#TAX-YEAR := #WS.#TAX-YEAR
            pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().setValue(false);                                                                                                      //Natural: ASSIGN TWRATBL2.#ABEND-IND := TWRATBL2.#DISPLAY-IND := FALSE
            pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().setValue(false);
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue("0");                                                                                                        //Natural: ASSIGN TWRATBL2.#STATE-CDE := '0'
            setValueToSubstring(pnd_Ws_Pnd_State,pdaTwratbl2.getTwratbl2_Pnd_State_Cde(),2,2);                                                                            //Natural: MOVE #WS.#STATE TO SUBSTR ( TWRATBL2.#STATE-CDE,2,2 )
            DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
            if (condition(pdaTwratbl2.getTwratbl2_Pnd_Return_Cde().getBoolean()))                                                                                         //Natural: IF #RETURN-CDE
            {
                pnd_State_Table.setValuesByName(pdaTwratbl2.getTwratbl2());                                                                                               //Natural: MOVE BY NAME TWRATBL2 TO #STATE-TABLE
                pnd_Ws_Pnd_Valid_State.setValue(true);                                                                                                                    //Natural: ASSIGN #WS.#VALID-STATE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Ws_Pnd_Valid_State.getBoolean())))                                                                                                           //Natural: IF NOT #WS.#VALID-STATE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Unknown STATE parmameter:",pnd_Ws_Pnd_State,new TabSetting(77),"***");                   //Natural: WRITE ( 1 ) '***' 25T 'Unknown STATE parmameter:' #WS.#STATE 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(101);  if (true) return;                                                                                                                    //Natural: TERMINATE 101
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Datx.setValue(Global.getDATX());                                                                                                                       //Natural: ASSIGN #WS.#DATX := *DATX
        pnd_Ws_Pnd_Timx.setValue(Global.getTIMX());                                                                                                                       //Natural: ASSIGN #WS.#TIMX := *TIMX
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                                         //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #WS.#TAX-YEAR
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(false);                                                                                                      //Natural: ASSIGN #TWRATBL4.#ABEND-IND := #TWRATBL4.#DISPLAY-IND := FALSE
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Display_Ind().setValue(false);
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Rec_Type().setValue("9ST0");                                                                                                      //Natural: ASSIGN #TWRATBL4.#REC-TYPE := '9ST0'
        setValueToSubstring(pnd_Ws_Pnd_State,pdaTwratbl4.getPnd_Twratbl4_Pnd_Rec_Type(),5,2);                                                                             //Natural: MOVE #WS.#STATE TO SUBSTR ( #TWRATBL4.#REC-TYPE,5,2 )
                                                                                                                                                                          //Natural: PERFORM READ-CONTROL
        sub_Read_Control();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (condition(pdaTwratbl4.getPnd_Twratbl4_Pnd_Ret_Code().getBoolean() && pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Orig().getBoolean()))                        //Natural: IF #TWRATBL4.#RET-CODE AND #TWRATBL4.#TIRCNTL-IRS-ORIG
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"State Original Reporting had not been run",new TabSetting(77),"***");                    //Natural: WRITE ( 1 ) '***' 25T 'State Original Reporting had not been run' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Ws_Pnd_Terminate.setValue(102);                                                                                                                           //Natural: ASSIGN #WS.#TERMINATE := 102
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaTwratbl4.getPnd_Twratbl4_C_Tircntl_Rpt_Tbl_Pe().equals(12)))                                                                                     //Natural: IF C-TIRCNTL-RPT-TBL-PE = 12
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"11 cycles of State Correction Reporting",new TabSetting(77),"***",NEWLINE,"***",new      //Natural: WRITE ( 1 ) '***' 25T '11 cycles of State Correction Reporting' 77T '***' / '***' 25T 'had been run' 77T '***'
                TabSetting(25),"had been run",new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Ws_Pnd_Terminate.setValue(103);                                                                                                                           //Natural: ASSIGN #WS.#TERMINATE := 103
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(pdaTwratbl4.getPnd_Twratbl4_C_Tircntl_Rpt_Tbl_Pe()).add(25)).greater(pnd_Ws_Pnd_Datx)))     //Natural: IF ( TIRCNTL-RPT-DTE ( C-TIRCNTL-RPT-TBL-PE ) + 25 ) > #WS.#DATX
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Last cycle of State Corrcetion Reporting",new TabSetting(77),"***",NEWLINE,"***",new     //Natural: WRITE ( 1 ) '***' 25T 'Last cycle of State Corrcetion Reporting' 77T '***' / '***' 25T 'had been run within previous 25 days on' TIRCNTL-RPT-DTE ( C-TIRCNTL-RPT-TBL-PE ) ( EM = MM/DD/YYYY ) 77T '***'
                TabSetting(25),"had been run within previous 25 days on",pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(pdaTwratbl4.getPnd_Twratbl4_C_Tircntl_Rpt_Tbl_Pe()), 
                new ReportEditMask ("MM/DD/YYYY"),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Ws_Pnd_Terminate.setValue(104);                                                                                                                           //Natural: ASSIGN #WS.#TERMINATE := 104
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Pnd_State.equals("12") || pnd_Ws_Pnd_State.equals("08")))                                                                                    //Natural: IF #WS.#STATE = '12' OR #WS.#STATE = '08'
        {
            pnd_Case_Fields2_Pnd_Recon_And_Reporting.setValue(true);                                                                                                      //Natural: ASSIGN #RECON-AND-REPORTING := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                                     //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #WS.#TAX-YEAR
            pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(true);                                                                                                   //Natural: ASSIGN #TWRATBL4.#ABEND-IND := TRUE
            pdaTwratbl4.getPnd_Twratbl4_Pnd_Display_Ind().setValue(true);                                                                                                 //Natural: ASSIGN #TWRATBL4.#DISPLAY-IND := TRUE
            pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(1);                                                                                                       //Natural: ASSIGN #TWRATBL4.#FORM-IND := 1
                                                                                                                                                                          //Natural: PERFORM READ-CONTROL
            sub_Read_Control();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            if (condition(pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Orig().getBoolean()))                                                                               //Natural: IF #TWRATBL4.#TIRCNTL-IRS-ORIG
            {
                pnd_Hold_Fields_Pnd_Hold_Comb_Fed_Ind.setValue(pdaTwratbl2.getTwratbl2_Tircntl_Corr_Comb_Fed_Ind());                                                      //Natural: MOVE TIRCNTL-CORR-COMB-FED-IND TO #HOLD-COMB-FED-IND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Hold_Fields_Pnd_Hold_Comb_Fed_Ind.setValue(pdaTwratbl2.getTwratbl2_Tircntl_Comb_Fed_Ind());                                                           //Natural: MOVE TWRATBL2.TIRCNTL-COMB-FED-IND TO #HOLD-COMB-FED-IND
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTwratbl2.getTwratbl2_Tircntl_Corr_Media_Code().equals("M")))                                                                                 //Natural: IF TWRATBL2.TIRCNTL-CORR-MEDIA-CODE = 'M'
            {
                pnd_Case_Fields2_Pnd_Recon_And_Reporting.setValue(true);                                                                                                  //Natural: ASSIGN #RECON-AND-REPORTING := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Hold_Fields_Pnd_Hold_Comb_Fed_Ind.notEquals("C") && pdaTwratbl2.getTwratbl2_Tircntl_Corr_Media_Code().notEquals("X")))                  //Natural: IF #HOLD-COMB-FED-IND NE 'C' AND TIRCNTL-CORR-MEDIA-CODE NE 'X'
                {
                    pnd_Case_Fields2_Pnd_Recon_And_Reporting.setValue(true);                                                                                              //Natural: ASSIGN #RECON-AND-REPORTING := TRUE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Case_Fields2_Pnd_Recon_Only.setValue(true);                                                                                                       //Natural: ASSIGN #RECON-ONLY := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(26),"State..............: ",pnd_State_Table_Tircntl_State_Alpha_Code,"-",pnd_State_Table_Tircntl_State_Full_Name,NEWLINE,new  //Natural: WRITE ( 1 ) / 26T 'State..............: ' #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME / 26T 'TIAA State Code....: ' #STATE-TABLE.TIRCNTL-STATE-OLD-CODE / 26T 'State Ind..........: ' TWRATBL2.TIRCNTL-STATE-IND / 26T 'CMB Ind............: ' #HOLD-COMB-FED-IND / 26T 'Media Ind..........: ' TWRATBL2.TIRCNTL-CORR-MEDIA-CODE / / 16T 'System DATE used for this run:' #WS.#DATX ( EM = MM/DD/YYYY ) / 16T 'System TIME used for this run:' #WS.#TIMX ( EM = MM/DD/YYYY�HH:II:SS.T )
            TabSetting(26),"TIAA State Code....: ",pnd_State_Table_Tircntl_State_Old_Code,NEWLINE,new TabSetting(26),"State Ind..........: ",pdaTwratbl2.getTwratbl2_Tircntl_State_Ind(),NEWLINE,new 
            TabSetting(26),"CMB Ind............: ",pnd_Hold_Fields_Pnd_Hold_Comb_Fed_Ind,NEWLINE,new TabSetting(26),"Media Ind..........: ",pdaTwratbl2.getTwratbl2_Tircntl_Corr_Media_Code(),NEWLINE,NEWLINE,new 
            TabSetting(16),"System DATE used for this run:",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(16),"System TIME used for this run:",pnd_Ws_Pnd_Timx, 
            new ReportEditMask ("MM/DD/YYYY HH:II:SS.T"));
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_Terminate.notEquals(getZero())))                                                                                                         //Natural: IF #WS.#TERMINATE NE 0
        {
            DbsUtil.terminate(pnd_Ws_Pnd_Terminate);  if (true) return;                                                                                                   //Natural: TERMINATE #WS.#TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Prev_Rpt_Date.setValue(pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(pdaTwratbl4.getPnd_Twratbl4_C_Tircntl_Rpt_Tbl_Pe()));                    //Natural: ASSIGN #WS.#PREV-RPT-DATE := TIRCNTL-RPT-DTE ( C-TIRCNTL-RPT-TBL-PE )
        pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(pdaTwratbl4.getPnd_Twratbl4_C_Tircntl_Rpt_Tbl_Pe().getDec().add(1)).setValue(pnd_Ws_Pnd_Datx);             //Natural: ASSIGN TIRCNTL-RPT-DTE ( C-TIRCNTL-RPT-TBL-PE + 1 ) := #WS.#DATX
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL
        sub_Update_Control();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Read_Control() throws Exception                                                                                                                      //Natural: READ-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4R' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Update_Control() throws Exception                                                                                                                    //Natural: UPDATE-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        DbsUtil.callnat(Twrntb4u.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4U' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Print_Hardcopy() throws Exception                                                                                                                    //Natural: PRINT-HARDCOPY
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        if (condition(form_U_Tirf_State_Hardcopy_Ind.getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals("P") || form_U_Tirf_State_Hardcopy_Ind.getValue(pnd_Case_Fields_Pnd_St_Occ_A).equals("R"))) //Natural: IF FORM-U.TIRF-STATE-HARDCOPY-IND ( #ST-OCC-A ) = 'P' OR = 'R'
        {
            pnd_Hardcopy_Ws_Pnd_Process_Type.setValue("B");                                                                                                               //Natural: MOVE 'B' TO #PROCESS-TYPE
            pnd_Hardcopy_Ws_Pnd_State_Name.setValue(pnd_State_Table_Tircntl_State_Full_Name);                                                                             //Natural: MOVE #STATE-TABLE.TIRCNTL-STATE-FULL-NAME TO #STATE-NAME
            pnd_Hardcopy_Ws_Pnd_Form_Page_Cnt.setValue(pnd_Case_Fields_Pnd_St_Occ_A);                                                                                     //Natural: MOVE #ST-OCC-A TO #FORM-PAGE-CNT
            pnd_Hardcopy_Ws_Pnd_Ws_Isn.setValue(pnd_Case_Fields_Pnd_Active_Isn);                                                                                          //Natural: MOVE #ACTIVE-ISN TO #WS-ISN
            getWorkFiles().write(1, false, pnd_Hardcopy_Ws_Pnd_State_Name, pnd_Hardcopy_Ws_Pnd_Form_Page_Cnt, pnd_Hardcopy_Ws_Pnd_Ws_Isn);                                //Natural: WRITE WORK FILE 01 #STATE-NAME #FORM-PAGE-CNT #WS-ISN
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Sort_Rec_Tirf_Company_CdeIsBreak = pnd_Sort_Rec_Tirf_Company_Cde.isBreak(endOfData);
        boolean pnd_Sort_Rec_Tirf_Form_TypeIsBreak = pnd_Sort_Rec_Tirf_Form_Type.isBreak(endOfData);
        if (condition(pnd_Sort_Rec_Tirf_Company_CdeIsBreak || pnd_Sort_Rec_Tirf_Form_TypeIsBreak))
        {
            pnd_Ws_Pnd_Company_Break.setValue(true);                                                                                                                      //Natural: ASSIGN #WS.#COMPANY-BREAK := TRUE
            pnd_Ws_Pnd_S2.setValue(1);                                                                                                                                    //Natural: ASSIGN #WS.#S2 := 1
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORTS
            sub_Summary_Reports();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Str_Pnd_Form_Cnt.getValue("*",2).nadd(pnd_Str_Pnd_Form_Cnt.getValue("*",1));                                                                              //Natural: ADD #STR.#FORM-CNT ( *,1 ) TO #STR.#FORM-CNT ( *,2 )
            pnd_Str_Pnd_State_Distr.getValue("*",2).nadd(pnd_Str_Pnd_State_Distr.getValue("*",1));                                                                        //Natural: ADD #STR.#STATE-DISTR ( *,1 ) TO #STR.#STATE-DISTR ( *,2 )
            pnd_Str_Pnd_State_Tax.getValue("*",2).nadd(pnd_Str_Pnd_State_Tax.getValue("*",1));                                                                            //Natural: ADD #STR.#STATE-TAX ( *,1 ) TO #STR.#STATE-TAX ( *,2 )
            pnd_Str_Pnd_Local_Distr.getValue("*",2).nadd(pnd_Str_Pnd_Local_Distr.getValue("*",1));                                                                        //Natural: ADD #STR.#LOCAL-DISTR ( *,1 ) TO #STR.#LOCAL-DISTR ( *,2 )
            pnd_Str_Pnd_Local_Tax.getValue("*",2).nadd(pnd_Str_Pnd_Local_Tax.getValue("*",1));                                                                            //Natural: ADD #STR.#LOCAL-TAX ( *,1 ) TO #STR.#LOCAL-TAX ( *,2 )
            pnd_Str_Pnd_Totals.getValue("*",1).reset();                                                                                                                   //Natural: RESET #STR.#TOTALS ( *,1 )
            pnd_Stw_Pnd_Form_Cnt.getValue("*",2).nadd(pnd_Stw_Pnd_Form_Cnt.getValue("*",1));                                                                              //Natural: ADD #STW.#FORM-CNT ( *,1 ) TO #STW.#FORM-CNT ( *,2 )
            pnd_Stw_Pnd_State_Distr.getValue("*",2).nadd(pnd_Stw_Pnd_State_Distr.getValue("*",1));                                                                        //Natural: ADD #STW.#STATE-DISTR ( *,1 ) TO #STW.#STATE-DISTR ( *,2 )
            pnd_Stw_Pnd_State_Tax.getValue("*",2).nadd(pnd_Stw_Pnd_State_Tax.getValue("*",1));                                                                            //Natural: ADD #STW.#STATE-TAX ( *,1 ) TO #STW.#STATE-TAX ( *,2 )
            pnd_Stw_Pnd_Local_Distr.getValue("*",2).nadd(pnd_Stw_Pnd_Local_Distr.getValue("*",1));                                                                        //Natural: ADD #STW.#LOCAL-DISTR ( *,1 ) TO #STW.#LOCAL-DISTR ( *,2 )
            pnd_Stw_Pnd_Local_Tax.getValue("*",2).nadd(pnd_Stw_Pnd_Local_Tax.getValue("*",1));                                                                            //Natural: ADD #STW.#LOCAL-TAX ( *,1 ) TO #STW.#LOCAL-TAX ( *,2 )
            pnd_Stw_Pnd_Totals.getValue("*",1).reset();                                                                                                                   //Natural: RESET #STW.#TOTALS ( *,1 )
            pnd_Stg_Pnd_Form_Cnt.getValue("*",2).nadd(pnd_Stg_Pnd_Form_Cnt.getValue("*",1));                                                                              //Natural: ADD #STG.#FORM-CNT ( *,1 ) TO #STG.#FORM-CNT ( *,2 )
            pnd_Stg_Pnd_State_Distr.getValue("*",2).nadd(pnd_Stg_Pnd_State_Distr.getValue("*",1));                                                                        //Natural: ADD #STG.#STATE-DISTR ( *,1 ) TO #STG.#STATE-DISTR ( *,2 )
            pnd_Stg_Pnd_State_Tax.getValue("*",2).nadd(pnd_Stg_Pnd_State_Tax.getValue("*",1));                                                                            //Natural: ADD #STG.#STATE-TAX ( *,1 ) TO #STG.#STATE-TAX ( *,2 )
            pnd_Stg_Pnd_Local_Distr.getValue("*",2).nadd(pnd_Stg_Pnd_Local_Distr.getValue("*",1));                                                                        //Natural: ADD #STG.#LOCAL-DISTR ( *,1 ) TO #STG.#LOCAL-DISTR ( *,2 )
            pnd_Stg_Pnd_Local_Tax.getValue("*",2).nadd(pnd_Stg_Pnd_Local_Tax.getValue("*",1));                                                                            //Natural: ADD #STG.#LOCAL-TAX ( *,1 ) TO #STG.#LOCAL-TAX ( *,2 )
            pnd_Stg_Pnd_Totals.getValue("*",1).reset();                                                                                                                   //Natural: RESET #STG.#TOTALS ( *,1 )
            pnd_Ws_Pnd_Empty_Cnt.reset();                                                                                                                                 //Natural: RESET #EMPTY-CNT
            pnd_Ws_Pnd_Empty_Frm2.reset();                                                                                                                                //Natural: RESET #EMPTY-FRM2
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Sort_Rec_Tirf_Form_TypeIsBreak))
        {
            pnd_Ws_Pnd_S2.setValue(2);                                                                                                                                    //Natural: ASSIGN #WS.#S2 := 2
            pnd_Ws_Pnd_Company_Line.setValue("Grand Totals");                                                                                                             //Natural: ASSIGN #WS.#COMPANY-LINE := 'Grand Totals'
            pnd_Ws_Pnd_Empty_Frm2.setValue(pnd_Ws_Pnd_Empty_Tot_Frm2);                                                                                                    //Natural: MOVE #EMPTY-TOT-FRM2 TO #EMPTY-FRM2
            pnd_Ws_Pnd_Empty_Cnt.setValue(pnd_Ws_Pnd_Empty_Tot_Cnt);                                                                                                      //Natural: MOVE #EMPTY-TOT-CNT TO #EMPTY-CNT
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORTS
            sub_Summary_Reports();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Ws_Pnd_Accepted_Form_Cnt.nadd(pnd_Str_Pnd_Form_Cnt.getValue(7,2));                                                                                        //Natural: ADD #STR.#FORM-CNT ( 7,2 ) TO #WS.#ACCEPTED-FORM-CNT
            pnd_Str_Pnd_Totals.getValue("*",2).reset();                                                                                                                   //Natural: RESET #STR.#TOTALS ( *,2 )
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=80 ZP=ON");
        Global.format(2, "PS=58 LS=133 ZP=ON");
        Global.format(3, "PS=58 LS=133 ZP=ON");
        Global.format(4, "PS=58 LS=133 ZP=ON");
        Global.format(5, "PS=58 LS=133 ZP=ON");
        Global.format(6, "PS=58 LS=133 ZP=ON");
        Global.format(7, "PS=58 LS=133 ZP=ON");
        Global.format(8, "PS=58 LS=133 ZP=ON");
        Global.format(9, "PS=58 LS=133 ZP=ON");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(37),"TaxWaRS",new TabSetting(68),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(34),"Control Report",new TabSetting(68),"Report: RPT1",NEWLINE,NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(50),"State Correction Reporting Summary",new TabSetting(120),"Report: RPT2",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(60),pnd_State_Table_Tircntl_State_Alpha_Code,"-",pnd_State_Table_Tircntl_State_Full_Name,
            NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"State Correction Reporting Rejected Detail",new TabSetting(120),"Report: RPT3",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(60),pnd_State_Table_Tircntl_State_Alpha_Code,"-",pnd_State_Table_Tircntl_State_Full_Name,
            NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(4), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"State Correction Reporting Accepted Detail",new TabSetting(120),"Report: RPT4",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(60),pnd_State_Table_Tircntl_State_Alpha_Code,"-",pnd_State_Table_Tircntl_State_Full_Name,
            NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(5, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(5), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(47),"State Withholding Reconciliation Summary",new TabSetting(120),"Report: RPT5",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(60),pnd_State_Table_Tircntl_State_Alpha_Code,"-",pnd_State_Table_Tircntl_State_Full_Name,NEWLINE,new 
            TabSetting(44),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask 
            ("MM/DD/YYYY"),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(6, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(6), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(43),"State Withholding Reconciliation Accepted Detail",new TabSetting(120),"Report: RPT6",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(60),pnd_State_Table_Tircntl_State_Alpha_Code,"-",pnd_State_Table_Tircntl_State_Full_Name,NEWLINE,new 
            TabSetting(44),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask 
            ("MM/DD/YYYY"),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(7, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(7), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(43),"State Withholding Reconciliation Rejected Detail",new TabSetting(120),"Report: RPT7",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(60),pnd_State_Table_Tircntl_State_Alpha_Code,"-",pnd_State_Table_Tircntl_State_Full_Name,NEWLINE,new 
            TabSetting(44),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask 
            ("MM/DD/YYYY"),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(8, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(8), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"State General Ledger Reconciliation Summary",new TabSetting(120),"Report: RPT8",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(60),pnd_State_Table_Tircntl_State_Alpha_Code,"-",pnd_State_Table_Tircntl_State_Full_Name,NEWLINE,new 
            TabSetting(44),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask 
            ("MM/DD/YYYY"),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(9, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(9), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(45),"State General Ledger Reconciliation Detail",new TabSetting(120),"Report: RPT9",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(60),pnd_State_Table_Tircntl_State_Alpha_Code,"-",pnd_State_Table_Tircntl_State_Full_Name,NEWLINE,new 
            TabSetting(44),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask 
            ("MM/DD/YYYY"),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);

        getReports().setDisplayColumns(2, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",
            
        		pnd_Str_Pnd_Header1,NEWLINE,"/",
        		pnd_Str_Pnd_Header2,"Form/Count",
        		pnd_Str_Pnd_Form_Cnt,"State/Distribution",
        		pnd_Str_Pnd_State_Distr,"State/Withholding",
        		pnd_Str_Pnd_State_Tax,"Local/Distribution",
        		pnd_Str_Pnd_Local_Distr,"Local/Withholding",
        		pnd_Str_Pnd_Local_Tax);
        getReports().setDisplayColumns(5, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",
            
        		pnd_Stw_Pnd_Header,"Form/Count",
        		pnd_Stw_Pnd_Form_Cnt,"State/Distribution",
        		pnd_Stw_Pnd_State_Distr,"State/Withholding",
        		pnd_Stw_Pnd_State_Tax,"Local/Distribution",
        		pnd_Stw_Pnd_Local_Distr,"Local/Withholding",
        		pnd_Stw_Pnd_Local_Tax);
        getReports().setDisplayColumns(8, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",
            
        		pnd_Stg_Pnd_Header,"Form/Count",
        		pnd_Stg_Pnd_Form_Cnt,"State/Distribution",
        		pnd_Stg_Pnd_State_Distr,"State/Withholding",
        		pnd_Stg_Pnd_State_Tax,"Local/Distribution",
        		pnd_Stg_Pnd_Local_Distr,"Local/Withholding",
        		pnd_Stg_Pnd_Local_Tax);
        getReports().setDisplayColumns(4, new ReportEmptyLineSuppression(true),"///TIN",
        		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),NEWLINE,"/",
        		pnd_Ws_Pnd_Disp_Rec,"///Contract",
        		pdaTwraform.getPnd_Twraform_Tirf_Contract_Nbr(),"Pa/y/e/e",
        		pdaTwraform.getPnd_Twraform_Tirf_Payee_Cde(),"//Name",
        		pdaTwraform.getPnd_Twraform_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pdaTwraform.getPnd_Twraform_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"#/of/Sta/tes",
        		pdaTwraform.getPnd_Twraform_C_1099_R(), new ReportEditMask ("ZZ9"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"S/T/A/T",
            
        		pnd_State_Table_Tircntl_State_Alpha_Code,"/I/R/S",
        		pdaTwraform.getPnd_Twraform_Tirf_State_Irs_Rpt_Ind(),"///State Distrib",
        		pdaTwraform.getPnd_Twraform_Tirf_State_Distr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),
            "///State Tax",
        		pdaTwraform.getPnd_Twraform_Tirf_State_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask 
            ("-Z,ZZZ,ZZ9.99"),"///Local Distrib",
        		pdaTwraform.getPnd_Twraform_Tirf_Loc_Distr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),
            "///Local Tax",
        		pdaTwraform.getPnd_Twraform_Tirf_Loc_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(3, new ReportEmptyLineSuppression(true),"///TIN",
        		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),"///Contract",
        		ldaTwrl9715.getForm_R_Tirf_Contract_Nbr(),"Pa/y/e/e",
        		ldaTwrl9715.getForm_R_Tirf_Payee_Cde(),"//Name",
        		ldaTwrl9715.getForm_R_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9715.getForm_R_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"#/of/Sta/tes",
        		ldaTwrl9715.getForm_R_Count_Casttirf_1099_R_State_Grp(), new ReportEditMask ("ZZ9"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
            "S/T/A/T",
        		pnd_State_Table_Tircntl_State_Alpha_Code,"/I/R/S",
        		ldaTwrl9715.getForm_R_Tirf_State_Irs_Rpt_Ind(),"///State Distrib",
        		ldaTwrl9715.getForm_R_Tirf_State_Distr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"///State Tax",
        		ldaTwrl9715.getForm_R_Tirf_State_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"///Local Distrib",
        		ldaTwrl9715.getForm_R_Tirf_Loc_Distr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"///Local Tax",
        		ldaTwrl9715.getForm_R_Tirf_Loc_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(6, "/TIN",
        		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
        		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
        		pnd_Recon_Case_Pnd_Header,"Form Count",
        		pnd_Recon_Case_Pnd_Form_Cnt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Distrib",
        		pnd_Recon_Case_Pnd_State_Distr, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Tax",
        		pnd_Recon_Case_Pnd_State_Tax, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Distrib",
        		pnd_Recon_Case_Pnd_Local_Distr, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Tax",
        		pnd_Recon_Case_Pnd_Local_Tax, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(7, "/TIN",
        		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
        		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
        		pnd_Recon_Case_Pnd_Header,"Form Count",
        		pnd_Recon_Case_Pnd_Form_Cnt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Distrib",
        		pnd_Recon_Case_Pnd_State_Distr, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Tax",
        		pnd_Recon_Case_Pnd_State_Tax, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Distrib",
        		pnd_Recon_Case_Pnd_Local_Distr, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Tax",
        		pnd_Recon_Case_Pnd_Local_Tax, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(9, "/TIN",
        		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
        		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
        		pnd_Recon_Case_Pnd_Header,"Form Count",
        		pnd_Recon_Case_Pnd_Form_Cnt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Distrib",
        		pnd_Recon_Case_Pnd_State_Distr, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"State Tax",
        		pnd_Recon_Case_Pnd_State_Tax, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Distrib",
        		pnd_Recon_Case_Pnd_Local_Distr, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Local Tax",
        		pnd_Recon_Case_Pnd_Local_Tax, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
    }
}
