/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:44:09 PM
**        * FROM NATURAL PROGRAM : Twrp6892
************************************************************
**        * FILE NAME            : Twrp6892.java
**        * CLASS NAME           : Twrp6892
**        * INSTANCE NAME        : Twrp6892
************************************************************
************************************************************************
** PROGRAM.: TWRP6892
** SYSTEM..: TAXWARS
** AUTHOR..: SNEHA SINHA
** FUNCTION: MOBIUS MASS MIGRATION FOR TAX REFACTOR PROJECT
** DATE....: JAN 05, 2015
** DESC....: THIS PROGRAM IS A CLONE OF TWRP6890. THIS IS DESIGNED TO
**           PROCESS 1099-R WITH SUNY. THE CONTROL WILL BE PASSED FROM
**           TWRP6890 PROGRAM. FOR SUNY FORMS IT HAS TO CHECK IF 1099R
**           IS PRESENT. IF NOT, THEN DO NOT PROCESS THE SUNY FORMS.
**
** HISTORY.:
**    10/13/2020 - RE-STOW COMPONENT FOR 5498           /* SECURE-ACT
** 12/04/2020 - RE-STOW COMPONENT FOR 5498 IRS REPORTING  2020
**
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp6892 extends BLNatBase
{
    // Data Areas
    private PdaTwrafrmn pdaTwrafrmn;
    private PdaTwra0214 pdaTwra0214;
    private PdaPsta9610 pdaPsta9610;
    private LdaTwrl0600 ldaTwrl0600;
    private LdaTwrl2001 ldaTwrl2001;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;

    private DataAccessProgramView vw_form;
    private DbsField form_Tirf_Tax_Year;
    private DbsField form_Tirf_Form_Type;
    private DbsField form_Tirf_Company_Cde;
    private DbsField form_Tirf_Tin;
    private DbsField form_Tirf_Contract_Nbr;
    private DbsField form_Tirf_Payee_Cde;
    private DbsField form_Tirf_Key;
    private DbsField form_Tirf_Tax_Id_Type;
    private DbsField form_Tirf_Active_Ind;
    private DbsField form_Tirf_Deceased_Ind;
    private DbsField form_Tirf_Empty_Form;
    private DbsField form_Tirf_Participant_Name;
    private DbsField form_Tirf_Moore_Hold_Ind;
    private DbsField form_Tirf_Paper_Print_Hold_Ind;
    private DbsField form_Tirf_Mobius_Ind;
    private DbsField form_Tirf_Pin;
    private DbsField form_Tirf_Prev_Rpt_Ind;
    private DbsField form_Count_Casttirf_Ltr_Grp;

    private DbsGroup form_Tirf_Ltr_Grp;
    private DbsField form_Tirf_Ltr_Gross_Amt;
    private DbsField form_Tirf_Ltr_Fed_Tax_Wthld;
    private DbsField form_Tirf_Ltr_State_Tax_Wthld;
    private DbsField form_Tirf_Ltr_Loc_Tax_Wthld;
    private DbsField form_Tirf_Ltr_Per;
    private DbsField form_Tirf_Gross_Amt;
    private DbsField form_Tirf_Fed_Tax_Wthld;
    private DbsField form_Tirf_Trad_Ira_Contrib;
    private DbsField form_Tirf_Roth_Ira_Contrib;
    private DbsField form_Tirf_Superde_3;
    private DbsField form_Tirf_Superde_10;
    private DbsField form_Tirf_Superde_12;

    private DataAccessProgramView vw_form_01;
    private DbsField form_01_Tirf_Tax_Year;
    private DbsField form_01_Tirf_Form_Type;
    private DbsField form_01_Tirf_Company_Cde;
    private DbsField form_01_Tirf_Tin;
    private DbsField form_01_Tirf_Contract_Nbr;
    private DbsField form_01_Tirf_Payee_Cde;
    private DbsField form_01_Tirf_Key;
    private DbsField form_01_Tirf_Tax_Id_Type;
    private DbsField form_01_Tirf_Active_Ind;
    private DbsField form_01_Tirf_Deceased_Ind;
    private DbsField form_01_Tirf_Empty_Form;
    private DbsField form_01_Tirf_Moore_Hold_Ind;
    private DbsField form_01_Tirf_Paper_Print_Hold_Ind;
    private DbsField form_01_Tirf_Mobius_Ind;
    private DbsField form_01_Tirf_Pin;
    private DbsField form_01_Tirf_Prev_Rpt_Ind;
    private DbsField form_01_Count_Casttirf_Ltr_Grp;

    private DbsGroup form_01_Tirf_Ltr_Grp;
    private DbsField form_01_Tirf_Ltr_Gross_Amt;
    private DbsField form_01_Tirf_Ltr_Fed_Tax_Wthld;
    private DbsField form_01_Tirf_Ltr_State_Tax_Wthld;
    private DbsField form_01_Tirf_Ltr_Loc_Tax_Wthld;
    private DbsField form_01_Tirf_Ltr_Per;
    private DbsField form_01_Tirf_Gross_Amt;
    private DbsField form_01_Tirf_Fed_Tax_Wthld;
    private DbsField form_01_Tirf_Trad_Ira_Contrib;
    private DbsField form_01_Tirf_Roth_Ira_Contrib;
    private DbsField form_01_Tirf_Taxable_Amt;
    private DbsField form_01_Tirf_Ivc_Amt;
    private DbsField form_01_Tirf_Distribution_Cde;
    private DbsField form_01_Count_Casttirf_1099_R_State_Grp;

    private DbsGroup form_01_Tirf_1099_R_State_Grp;
    private DbsField form_01_Tirf_State_Tax_Wthld;
    private DbsField form_01_Tirf_Loc_Tax_Wthld;
    private DbsField form_01_Tirf_State_Rpt_Ind;
    private DbsField form_01_Tirf_State_Code;
    private DbsField form_01_Tirf_State_Distr;
    private DbsField form_01_Tirf_Loc_Distr;
    private DbsField form_01_Tirf_State_Hardcopy_Ind;
    private DbsField form_01_Tirf_Roth_Year;
    private DbsField form_01_Tirf_Taxable_Not_Det;
    private DbsField form_01_Tirf_Tot_Contractual_Ivc;
    private DbsField form_01_Tirf_Ira_Distr;
    private DbsField form_01_Tirf_Pct_Of_Tot;
    private DbsField form_01_Tirf_Tot_Distr;
    private DbsField form_01_Tirf_Moore_Test_Ind;
    private DbsField form_01_Tirf_Participant_Name;
    private DbsField form_01_Tirf_Addr_Ln1;
    private DbsField form_01_Tirf_Addr_Ln2;
    private DbsField form_01_Tirf_Addr_Ln3;
    private DbsField form_01_Tirf_Addr_Ln4;
    private DbsField form_01_Tirf_Addr_Ln5;
    private DbsField form_01_Tirf_Addr_Ln6;
    private DbsField form_01_Tirf_Walk_Rte;
    private DbsField form_01_Tirf_Geo_Cde;
    private DbsField form_01_Tirf_Country_Name;
    private DbsField form_01_Tirf_Foreign_Addr;
    private DbsField form_01_Tirf_Zip;
    private DbsField form_01_Tirf_Trad_Ira_Rollover;

    private DbsGroup form_01__R_Field_1;
    private DbsField form_01_Tirf_Irr_Amt;
    private DbsField form_01_Tirf_Ltr_Data;
    private DbsField form_01_Tirf_Superde_1;
    private DbsField form_01_Tirf_Superde_3;
    private DbsField form_01_Tirf_Superde_10;
    private DbsField form_01_Tirf_Superde_12;

    private DataAccessProgramView vw_twrparti_Read;
    private DbsField twrparti_Read_Twrparti_Tax_Id;
    private DbsField twrparti_Read_Twrparti_Rlup_Email_Pref;
    private DbsField twrparti_Read_Twrparti_Rlup_Email_Pref_Dte;
    private DbsField twrparti_Read_Twrparti_Rlup_Email_Addr;
    private DbsField twrparti_Read_Twrparti_Rlup_Email_Addr_Dte;
    private DbsField pnd_Twrparti_Curr_Rec_Sd;

    private DbsGroup pnd_Twrparti_Curr_Rec_Sd__R_Field_2;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Tax_Id;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Status;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Part_Eff_End_Dte;

    private DataAccessProgramView vw_form_Upd;
    private DbsField form_Upd_Tirf_Tax_Year;
    private DbsField form_Upd_Tirf_Form_Type;
    private DbsField form_Upd_Tirf_Tin;
    private DbsField form_Upd_Tirf_Payee_Cde;
    private DbsField form_Upd_Tirf_Mobius_Stat;
    private DbsField form_Upd_Tirf_Mobius_Ind;
    private DbsField form_Upd_Tirf_Mobius_Stat_Date;
    private DbsField form_Upd_Tirf_Part_Rpt_Date;
    private DbsField form_Upd_Tirf_Part_Rpt_Ind;
    private DbsField form_Upd_Tirf_Lu_User;
    private DbsField form_Upd_Tirf_Lu_Ts;
    private DbsField form_Upd_Tirf_Company_Cde;
    private DbsField pnd_Wkfle_Recd_Cnt;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Input;
    private DbsField pnd_Ws_Pnd_Form_Type;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_File_Limit;
    private DbsField pnd_Ws_Pnd_Email_File;
    private DbsField pnd_Ws_Pnd_Reset_Et_Data;
    private DbsField pnd_Ws_Pnd_Bypass_E_Deliv;
    private DbsField pnd_Ws_Pnd_Mobius_Limit;
    private DbsField pnd_Ws_Pnd_Stored_Tin;
    private DbsField pnd_Ws_Pnd_Restart_Text;
    private DbsField pnd_Ws_Pnd_Mobius_Status;
    private DbsField pnd_Ws_Pnd_Mobius_Status_S;
    private DbsField pnd_Ws_Pnd_Mobius_Status_01;
    private DbsField pnd_Ws_Pnd_Customer_Id;

    private DbsGroup pnd_Ws__R_Field_3;
    private DbsField pnd_Ws_Pnd_Pin;
    private DbsField pnd_Ws_Pnd_Hold_Customer_Id;
    private DbsField pnd_Ws_Pnd_Link_Let_Type;
    private DbsField pnd_Ws_Pnd_Form_Page_Cnt;
    private DbsField pnd_Ws_Pnd_Zero_Isn;
    private DbsField pnd_Ws_Pnd_Sys_Date;
    private DbsField pnd_Ws_Pnd_Sys_Time;
    private DbsField pnd_Ws_Pnd_Message;
    private DbsField pnd_Ws_Pnd_Message_S;
    private DbsField pnd_Ws_Pnd_Message_01;

    private DbsGroup pnd_Ws_Counters;
    private DbsField pnd_Ws_Pnd_Read_Cnt_Suny;
    private DbsField pnd_Ws_Pnd_Read_Cnt_1099r;
    private DbsField pnd_Ws_Pnd_Form_Cnt;
    private DbsField pnd_Ws_Pnd_Part_Cnt;
    private DbsField pnd_Ws_Pnd_Empty_Form_Cnt;
    private DbsField pnd_Ws_Pnd_Cpm_Lookup_Cnt;
    private DbsField pnd_Ws_Pnd_Tax_Email_Cnt;
    private DbsField pnd_Ws_Pnd_No_Tax_Email_Cnt;
    private DbsField pnd_Ws_Pnd_Email_Non_Email_Bypassed;
    private DbsField pnd_Ws_Pnd_Missing_Pin_Cnt_Suny;
    private DbsField pnd_Ws_Pnd_Missing_Pin_Cnt_1099r;
    private DbsField pnd_Ws_Pnd_Missing_Pin_Cnt_S_Good;
    private DbsField pnd_Ws_Pnd_Missing_Pin_Cnt_R_Good;
    private DbsField pnd_Ws_Pnd_Missing_Ssn_Cnt_R;
    private DbsField pnd_Ws_Pnd_Missing_Ssn_Cnt_S;
    private DbsField pnd_Ws_Pnd_Paper_Print_Cnt;
    private DbsField pnd_Ws_Pnd_Bypass_Cnt;
    private DbsField pnd_Ws_Pnd_5498_Bypass_Cnt;
    private DbsField pnd_Ws_Pnd_Letr_Bypass_Cnt;
    private DbsField pnd_Ws_Pnd_Form_Pages_Cnt;
    private DbsField pnd_Ws_Pnd_Multi_Page_Form_Cnt;
    private DbsField pnd_Ws_Pnd_Total_Form_Cnt;
    private DbsField pnd_Ws_Pnd_Non_Suny_Part_Cnt;
    private DbsField pnd_Ws_Pnd_Suny_Det_Rec_Cnt;
    private DbsField pnd_Ws_Pnd_Tirf_Paper_Print_Hold_Ind;
    private DbsField pnd_Ws_Pnd_Tirf_Moore_Hold_Ind;
    private DbsField pnd_Ws_Pnd_Debug;
    private DbsField pnd_Ws_Pnd_Read_Cnt1;
    private DbsField pnd_Ws_Pnd_Read_Cnt_01;
    private DbsField pnd_Ws_Pnd_Update_Cnt_Suny;
    private DbsField pnd_Ws_Pnd_Update_Cnt_1099r;
    private DbsField pnd_Ws_Pnd_Et_Cnt;
    private DbsField et_Limit;
    private DbsField pnd_Contract_Txt1;
    private DbsField pnd_Contract_Hdr_Lit1;
    private DbsField pnd_Test_Ind_Cnt;
    private DbsField pnd_Ws_Form_Type;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_In_Form_Type;
    private DbsField pnd_Bypass_Reason;
    private DbsField pnd_Tin_Hold;
    private DbsField pnd_Cnt;
    private DbsField pnd_Ws_Progname;

    private DbsGroup pnd_Db_Read_Fields;
    private DbsField pnd_Db_Read_Fields_Pnd_Letter_Cnt_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Detl_Cnt_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Suny_Gross_Amt_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Suny_Fed_Tax_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Suny_Summ_State_Tax_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Suny_Summ_Lcl_Tax_Db;

    private DbsGroup pnd_E_Del_Bypassed_Fields;
    private DbsField pnd_E_Del_Bypassed_Fields_Pnd_Letter_Cnt_B;
    private DbsField pnd_E_Del_Bypassed_Fields_Pnd_Detl_Cnt_B;
    private DbsField pnd_E_Del_Bypassed_Fields_Pnd_Suny_Gross_Amt_B;
    private DbsField pnd_E_Del_Bypassed_Fields_Pnd_Suny_Fed_Tax_B;
    private DbsField pnd_E_Del_Bypassed_Fields_Pnd_Suny_Summ_State_Tax_B;
    private DbsField pnd_E_Del_Bypassed_Fields_Pnd_Suny_Summ_Lcl_Tax_B;

    private DbsGroup pnd_E_Del_Accepted_Fields;
    private DbsField pnd_E_Del_Accepted_Fields_Pnd_Letter_Cnt_A;
    private DbsField pnd_E_Del_Accepted_Fields_Pnd_Detl_Cnt_A;
    private DbsField pnd_E_Del_Accepted_Fields_Pnd_Suny_Gross_Amt_A;
    private DbsField pnd_E_Del_Accepted_Fields_Pnd_Suny_Fed_Tax_A;
    private DbsField pnd_E_Del_Accepted_Fields_Pnd_Suny_Summ_State_Tax_A;
    private DbsField pnd_E_Del_Accepted_Fields_Pnd_Suny_Summ_Lcl_Tax_A;
    private DbsField pnd_Ws_Tax_Year;

    private DbsGroup pnd_Ws_Tax_Year__R_Field_4;
    private DbsField pnd_Ws_Tax_Year_Pnd_Ws_Tax_Year_A;
    private DbsField pnd_Detl_Temp_Db;
    private DbsField pnd_Detl_Temp_B;
    private DbsField pnd_Detl_Temp_A;
    private DbsField pnd_Detl_Temp_P_B;
    private DbsField pnd_Detl_Temp_P_A;
    private DbsField pnd_Superde_12;

    private DbsGroup pnd_Superde_12__R_Field_5;
    private DbsField pnd_Superde_12_Pnd_Sd12_Tax_Year;
    private DbsField pnd_Superde_12_Pnd_Sd12_Form_Type;
    private DbsField pnd_Superde_12_Pnd_Sd12_Active_Ind;
    private DbsField pnd_Superde_12_Pnd_Sd12_Mobius_Ind;
    private DbsField pnd_Superde_12_Pnd_Sd12_Tin;
    private DbsField pnd_Superde_12_End;
    private DbsField pnd_Superde_12_Form_01;

    private DbsGroup pnd_Superde_12_Form_01__R_Field_6;
    private DbsField pnd_Superde_12_Form_01_Pnd_Sd12_Tax_Year;
    private DbsField pnd_Superde_12_Form_01_Pnd_Sd12_Form_Type;
    private DbsField pnd_Superde_12_Form_01_Pnd_Sd12_Active_Ind;
    private DbsField pnd_Superde_12_Form_01_Pnd_Sd12_Mobius_Ind;
    private DbsField pnd_Superde_12_Form_01_Pnd_Sd12_Tin;
    private DbsField pnd_Superde_12_Form_01_End;

    private DbsGroup pnd_Superde_12_Form_01_End__R_Field_7;
    private DbsField pnd_Superde_12_Form_01_End_Pnd_Sd12_Tax_Year_End;
    private DbsField pnd_Superde_12_Form_01_End_Pnd_Sd12_Form_Type_End;
    private DbsField pnd_Superde_12_Form_01_End_Pnd_Sd12_Active_Ind_End;
    private DbsField pnd_Superde_12_Form_01_End_Pnd_Sd12_Mobius_Ind_End;
    private DbsField pnd_Superde_12_Form_01_End_Pnd_Sd12_Tin;
    private DbsField pnd_Form_Type_10_01_Found;
    private DbsField pnd_First_Time_Form_Process;
    private DbsField pnd_Old_Tin;
    private DbsField pnd_C;
    private DbsField pnd_Y_Cnt;
    private DbsField pnd_Z;
    private DbsField pnd_I2;
    private DbsField pnd_Tin_On_Hold;
    private DbsField pnd_M;
    private DbsField pnd_Mob_Prev_Pin;
    private DbsField pnd_Mob_Prev_Tin;
    private DbsField pnd_Mob_Suny_Ind;
    private DbsField pnd_Mob_Hold_Pin;
    private DbsField pnd_Mob_Hold_Tin;
    private DbsField pnd_Hold_Mobdata;
    private DbsField pnd_Contract_Txt;
    private DbsField pnd_Contract_Hdr_Lit;
    private DbsField pnd_Test_Ind;
    private DbsField pnd_Test_Ind_01;
    private DbsField pnd_Coupon_Ind;
    private DbsField pnd_Coupon_Ind_01;
    private DbsField pnd_Suny_Bypassed;
    private DbsField pnd_1099r_Bypassed;
    private DbsField pnd_Ny_Ltr_Bypass;
    private DbsField pnd_Ny_Ltr_Value_I;

    private DbsGroup pnd_Ltr_Array;
    private DbsField pnd_Ltr_Array_Pnd_Ltr_Tin;
    private DbsField pnd_Ltr_Array_Pnd_Ltr_Cntrct;
    private DbsField pnd_Ltr_Array_Pnd_Ltr_Data;
    private DbsField pnd_Additional_Forms;
    private DbsField pnd_I;

    private DbsGroup pnd_Tiaa_Totals;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_R;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Gross_Amt;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Taxable;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Fed;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_State;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Local;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Ivc;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Irr;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Int;

    private DbsGroup pnd_Trust_Totals;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_R;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_R_Gross_Amt;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_R_Taxable;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_R_Fed;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_R_State;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_R_Local;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_R_Ivc;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_R_Irr;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_R_Int;

    private DbsGroup pnd_Tiaa_Email_Bypassed;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Gross_Amt_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Taxable_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Ivc_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Fed_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Local_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Int_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_State_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Irr_B;

    private DbsGroup pnd_Tiaa_Email_Accepted;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Gross_Amt_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Taxable_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Ivc_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Fed_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Local_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Int_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_State_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Irr_A;

    private DbsGroup pnd_Trust_Email_Bypassed;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Gross_Amt_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Taxable_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Ivc_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Fed_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Local_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Int_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_State_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Irr_B;

    private DbsGroup pnd_Trust_Email_Accepted;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Gross_Amt_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Taxable_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Ivc_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Fed_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Local_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Int_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_State_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Irr_A;

    private DbsGroup pnd_Trust_Paper_Bypassed;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Gross_Amt_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Taxable_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Ivc_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Fed_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Local_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Int_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Irr_P_B;

    private DbsGroup pnd_Tiaa_Trust_1099_R_Total;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_P_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_P_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_P_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_P_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_P_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_P_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_P_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_P_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_P_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_P_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_P_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_P_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_P_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_P_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_P_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_P_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_P_A;
    private DbsField pnd_Total_Unq_Tin;
    private DbsField pnd_Unq_Tin_Eb;
    private DbsField pnd_Unq_Tin_Ea;
    private DbsField pnd_Unq_Tin_Pb;
    private DbsField pnd_Unq_Tin_Pa;
    private DbsField pnd_Hold_Rpt_Tin;
    private DbsField pls_Email_Address;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwrafrmn = new PdaTwrafrmn(localVariables);
        pdaTwra0214 = new PdaTwra0214(localVariables);
        pdaPsta9610 = new PdaPsta9610(localVariables);
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);
        ldaTwrl2001 = new LdaTwrl2001();
        registerRecord(ldaTwrl2001);

        // Local Variables

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);

        vw_form = new DataAccessProgramView(new NameInfo("vw_form", "FORM"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE", DdmPeriodicGroups.getInstance().getGroups("TWRFRM_FORM_FILE"));
        form_Tirf_Tax_Year = vw_form.getRecord().newFieldInGroup("form_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_TAX_YEAR");
        form_Tirf_Tax_Year.setDdmHeader("TAX/YEAR");
        form_Tirf_Form_Type = vw_form.getRecord().newFieldInGroup("form_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TIRF_FORM_TYPE");
        form_Tirf_Form_Type.setDdmHeader("FORM");
        form_Tirf_Company_Cde = vw_form.getRecord().newFieldInGroup("form_Tirf_Company_Cde", "TIRF-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_COMPANY_CDE");
        form_Tirf_Company_Cde.setDdmHeader("COM-/PANY/CODE");
        form_Tirf_Tin = vw_form.getRecord().newFieldInGroup("form_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIRF_TIN");
        form_Tirf_Tin.setDdmHeader("TAX/ID/NUMBER");
        form_Tirf_Contract_Nbr = vw_form.getRecord().newFieldInGroup("form_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_CONTRACT_NBR");
        form_Tirf_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        form_Tirf_Payee_Cde = vw_form.getRecord().newFieldInGroup("form_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_PAYEE_CDE");
        form_Tirf_Payee_Cde.setDdmHeader("PAYEE/CODE");
        form_Tirf_Key = vw_form.getRecord().newFieldInGroup("form_Tirf_Key", "TIRF-KEY", FieldType.STRING, 5, RepeatingFieldStrategy.None, "TIRF_KEY");
        form_Tirf_Key.setDdmHeader("KEY");
        form_Tirf_Tax_Id_Type = vw_form.getRecord().newFieldInGroup("form_Tirf_Tax_Id_Type", "TIRF-TAX-ID-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_TAX_ID_TYPE");
        form_Tirf_Active_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Active_Ind", "TIRF-ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_ACTIVE_IND");
        form_Tirf_Active_Ind.setDdmHeader("ACT/IND");
        form_Tirf_Deceased_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Deceased_Ind", "TIRF-DECEASED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_DECEASED_IND");
        form_Tirf_Deceased_Ind.setDdmHeader("DECE-/ASED/IND");
        form_Tirf_Empty_Form = vw_form.getRecord().newFieldInGroup("form_Tirf_Empty_Form", "TIRF-EMPTY-FORM", FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, 
            "TIRF_EMPTY_FORM");
        form_Tirf_Empty_Form.setDdmHeader("EMPTY/FORM");
        form_Tirf_Participant_Name = vw_form.getRecord().newFieldInGroup("form_Tirf_Participant_Name", "TIRF-PARTICIPANT-NAME", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "TIRF_PARTICIPANT_NAME");
        form_Tirf_Participant_Name.setDdmHeader("PARTICIPANT/NAME");
        form_Tirf_Moore_Hold_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Moore_Hold_Ind", "TIRF-MOORE-HOLD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_MOORE_HOLD_IND");
        form_Tirf_Moore_Hold_Ind.setDdmHeader("MOORE/HOLD/IND");
        form_Tirf_Paper_Print_Hold_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Paper_Print_Hold_Ind", "TIRF-PAPER-PRINT-HOLD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRF_PAPER_PRINT_HOLD_IND");
        form_Tirf_Paper_Print_Hold_Ind.setDdmHeader("PAPER/PRINT/HOLD");
        form_Tirf_Mobius_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Mobius_Ind", "TIRF-MOBIUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_MOBIUS_IND");
        form_Tirf_Mobius_Ind.setDdmHeader("INCLUDE/IN/MOBIUS");
        form_Tirf_Pin = vw_form.getRecord().newFieldInGroup("form_Tirf_Pin", "TIRF-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "TIRF_PIN");
        form_Tirf_Pin.setDdmHeader("PIN");
        form_Tirf_Prev_Rpt_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Prev_Rpt_Ind", "TIRF-PREV-RPT-IND", FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, 
            "TIRF_PREV_RPT_IND");
        form_Tirf_Prev_Rpt_Ind.setDdmHeader("PREV/RPT/IND");
        form_Count_Casttirf_Ltr_Grp = vw_form.getRecord().newFieldInGroup("form_Count_Casttirf_Ltr_Grp", "C*TIRF-LTR-GRP", RepeatingFieldStrategy.CAsteriskVariable, 
            "TWRFRM_FORM_FILE_TIRF_LTR_GRP");

        form_Tirf_Ltr_Grp = vw_form.getRecord().newGroupInGroup("form_Tirf_Ltr_Grp", "TIRF-LTR-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        form_Tirf_Ltr_Gross_Amt = form_Tirf_Ltr_Grp.newFieldArrayInGroup("form_Tirf_Ltr_Gross_Amt", "TIRF-LTR-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 
            2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_GROSS_AMT", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        form_Tirf_Ltr_Gross_Amt.setDdmHeader("LETTER/GROSS/AMOUNT");
        form_Tirf_Ltr_Fed_Tax_Wthld = form_Tirf_Ltr_Grp.newFieldArrayInGroup("form_Tirf_Ltr_Fed_Tax_Wthld", "TIRF-LTR-FED-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_FED_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        form_Tirf_Ltr_Fed_Tax_Wthld.setDdmHeader("LTR/FED/TAX");
        form_Tirf_Ltr_State_Tax_Wthld = form_Tirf_Ltr_Grp.newFieldArrayInGroup("form_Tirf_Ltr_State_Tax_Wthld", "TIRF-LTR-STATE-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_STATE_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        form_Tirf_Ltr_State_Tax_Wthld.setDdmHeader("LTR/STATE/TAX");
        form_Tirf_Ltr_Loc_Tax_Wthld = form_Tirf_Ltr_Grp.newFieldArrayInGroup("form_Tirf_Ltr_Loc_Tax_Wthld", "TIRF-LTR-LOC-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_LOC_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        form_Tirf_Ltr_Loc_Tax_Wthld.setDdmHeader("LTR/LOCAL/TAX");
        form_Tirf_Ltr_Per = form_Tirf_Ltr_Grp.newFieldArrayInGroup("form_Tirf_Ltr_Per", "TIRF-LTR-PER", FieldType.PACKED_DECIMAL, 8, 5, new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_PER", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        form_Tirf_Ltr_Per.setDdmHeader("LTR/PCT");
        form_Tirf_Gross_Amt = vw_form.getRecord().newFieldInGroup("form_Tirf_Gross_Amt", "TIRF-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "TIRF_GROSS_AMT");
        form_Tirf_Gross_Amt.setDdmHeader("GROSS/DISTRI-/BUTION");
        form_Tirf_Fed_Tax_Wthld = vw_form.getRecord().newFieldInGroup("form_Tirf_Fed_Tax_Wthld", "TIRF-FED-TAX-WTHLD", FieldType.PACKED_DECIMAL, 9, 2, 
            RepeatingFieldStrategy.None, "TIRF_FED_TAX_WTHLD");
        form_Tirf_Fed_Tax_Wthld.setDdmHeader("FEDERAL/TAX/WITHHELD");
        form_Tirf_Trad_Ira_Contrib = vw_form.getRecord().newFieldInGroup("form_Tirf_Trad_Ira_Contrib", "TIRF-TRAD-IRA-CONTRIB", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TIRF_TRAD_IRA_CONTRIB");
        form_Tirf_Trad_Ira_Contrib.setDdmHeader("TRADITIONAL/IRA/CONTRIBUTION");
        form_Tirf_Roth_Ira_Contrib = vw_form.getRecord().newFieldInGroup("form_Tirf_Roth_Ira_Contrib", "TIRF-ROTH-IRA-CONTRIB", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TIRF_ROTH_IRA_CONTRIB");
        form_Tirf_Roth_Ira_Contrib.setDdmHeader("ROTH/IRA/CONTRIBUTION");
        form_Tirf_Superde_3 = vw_form.getRecord().newFieldInGroup("form_Tirf_Superde_3", "TIRF-SUPERDE-3", FieldType.STRING, 33, RepeatingFieldStrategy.None, 
            "TIRF_SUPERDE_3");
        form_Tirf_Superde_3.setSuperDescriptor(true);
        form_Tirf_Superde_10 = vw_form.getRecord().newFieldInGroup("form_Tirf_Superde_10", "TIRF-SUPERDE-10", FieldType.STRING, 31, RepeatingFieldStrategy.None, 
            "TIRF_SUPERDE_10");
        form_Tirf_Superde_10.setSuperDescriptor(true);
        form_Tirf_Superde_12 = vw_form.getRecord().newFieldInGroup("form_Tirf_Superde_12", "TIRF-SUPERDE-12", FieldType.STRING, 33, RepeatingFieldStrategy.None, 
            "TIRF_SUPERDE_12");
        form_Tirf_Superde_12.setSuperDescriptor(true);
        registerRecord(vw_form);

        vw_form_01 = new DataAccessProgramView(new NameInfo("vw_form_01", "FORM-01"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE", DdmPeriodicGroups.getInstance().getGroups("TWRFRM_FORM_FILE"));
        form_01_Tirf_Tax_Year = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_TAX_YEAR");
        form_01_Tirf_Tax_Year.setDdmHeader("TAX/YEAR");
        form_01_Tirf_Form_Type = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TIRF_FORM_TYPE");
        form_01_Tirf_Form_Type.setDdmHeader("FORM");
        form_01_Tirf_Company_Cde = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Company_Cde", "TIRF-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_COMPANY_CDE");
        form_01_Tirf_Company_Cde.setDdmHeader("COM-/PANY/CODE");
        form_01_Tirf_Tin = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIRF_TIN");
        form_01_Tirf_Tin.setDdmHeader("TAX/ID/NUMBER");
        form_01_Tirf_Contract_Nbr = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_CONTRACT_NBR");
        form_01_Tirf_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        form_01_Tirf_Payee_Cde = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_PAYEE_CDE");
        form_01_Tirf_Payee_Cde.setDdmHeader("PAYEE/CODE");
        form_01_Tirf_Key = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Key", "TIRF-KEY", FieldType.STRING, 5, RepeatingFieldStrategy.None, "TIRF_KEY");
        form_01_Tirf_Key.setDdmHeader("KEY");
        form_01_Tirf_Tax_Id_Type = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Tax_Id_Type", "TIRF-TAX-ID-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_TAX_ID_TYPE");
        form_01_Tirf_Active_Ind = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Active_Ind", "TIRF-ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_ACTIVE_IND");
        form_01_Tirf_Active_Ind.setDdmHeader("ACT/IND");
        form_01_Tirf_Deceased_Ind = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Deceased_Ind", "TIRF-DECEASED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_DECEASED_IND");
        form_01_Tirf_Deceased_Ind.setDdmHeader("DECE-/ASED/IND");
        form_01_Tirf_Empty_Form = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Empty_Form", "TIRF-EMPTY-FORM", FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, 
            "TIRF_EMPTY_FORM");
        form_01_Tirf_Empty_Form.setDdmHeader("EMPTY/FORM");
        form_01_Tirf_Moore_Hold_Ind = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Moore_Hold_Ind", "TIRF-MOORE-HOLD-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TIRF_MOORE_HOLD_IND");
        form_01_Tirf_Moore_Hold_Ind.setDdmHeader("MOORE/HOLD/IND");
        form_01_Tirf_Paper_Print_Hold_Ind = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Paper_Print_Hold_Ind", "TIRF-PAPER-PRINT-HOLD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRF_PAPER_PRINT_HOLD_IND");
        form_01_Tirf_Paper_Print_Hold_Ind.setDdmHeader("PAPER/PRINT/HOLD");
        form_01_Tirf_Mobius_Ind = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Mobius_Ind", "TIRF-MOBIUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_MOBIUS_IND");
        form_01_Tirf_Mobius_Ind.setDdmHeader("INCLUDE/IN/MOBIUS");
        form_01_Tirf_Pin = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Pin", "TIRF-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "TIRF_PIN");
        form_01_Tirf_Pin.setDdmHeader("PIN");
        form_01_Tirf_Prev_Rpt_Ind = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Prev_Rpt_Ind", "TIRF-PREV-RPT-IND", FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, 
            "TIRF_PREV_RPT_IND");
        form_01_Tirf_Prev_Rpt_Ind.setDdmHeader("PREV/RPT/IND");
        form_01_Count_Casttirf_Ltr_Grp = vw_form_01.getRecord().newFieldInGroup("form_01_Count_Casttirf_Ltr_Grp", "C*TIRF-LTR-GRP", RepeatingFieldStrategy.CAsteriskVariable, 
            "TWRFRM_FORM_FILE_TIRF_LTR_GRP");

        form_01_Tirf_Ltr_Grp = vw_form_01.getRecord().newGroupInGroup("form_01_Tirf_Ltr_Grp", "TIRF-LTR-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        form_01_Tirf_Ltr_Gross_Amt = form_01_Tirf_Ltr_Grp.newFieldArrayInGroup("form_01_Tirf_Ltr_Gross_Amt", "TIRF-LTR-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_GROSS_AMT", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        form_01_Tirf_Ltr_Gross_Amt.setDdmHeader("LETTER/GROSS/AMOUNT");
        form_01_Tirf_Ltr_Fed_Tax_Wthld = form_01_Tirf_Ltr_Grp.newFieldArrayInGroup("form_01_Tirf_Ltr_Fed_Tax_Wthld", "TIRF-LTR-FED-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_FED_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        form_01_Tirf_Ltr_Fed_Tax_Wthld.setDdmHeader("LTR/FED/TAX");
        form_01_Tirf_Ltr_State_Tax_Wthld = form_01_Tirf_Ltr_Grp.newFieldArrayInGroup("form_01_Tirf_Ltr_State_Tax_Wthld", "TIRF-LTR-STATE-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_STATE_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        form_01_Tirf_Ltr_State_Tax_Wthld.setDdmHeader("LTR/STATE/TAX");
        form_01_Tirf_Ltr_Loc_Tax_Wthld = form_01_Tirf_Ltr_Grp.newFieldArrayInGroup("form_01_Tirf_Ltr_Loc_Tax_Wthld", "TIRF-LTR-LOC-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_LOC_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        form_01_Tirf_Ltr_Loc_Tax_Wthld.setDdmHeader("LTR/LOCAL/TAX");
        form_01_Tirf_Ltr_Per = form_01_Tirf_Ltr_Grp.newFieldArrayInGroup("form_01_Tirf_Ltr_Per", "TIRF-LTR-PER", FieldType.PACKED_DECIMAL, 8, 5, new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_PER", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        form_01_Tirf_Ltr_Per.setDdmHeader("LTR/PCT");
        form_01_Tirf_Gross_Amt = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Gross_Amt", "TIRF-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "TIRF_GROSS_AMT");
        form_01_Tirf_Gross_Amt.setDdmHeader("GROSS/DISTRI-/BUTION");
        form_01_Tirf_Fed_Tax_Wthld = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Fed_Tax_Wthld", "TIRF-FED-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TIRF_FED_TAX_WTHLD");
        form_01_Tirf_Fed_Tax_Wthld.setDdmHeader("FEDERAL/TAX/WITHHELD");
        form_01_Tirf_Trad_Ira_Contrib = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Trad_Ira_Contrib", "TIRF-TRAD-IRA-CONTRIB", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TIRF_TRAD_IRA_CONTRIB");
        form_01_Tirf_Trad_Ira_Contrib.setDdmHeader("TRADITIONAL/IRA/CONTRIBUTION");
        form_01_Tirf_Roth_Ira_Contrib = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Roth_Ira_Contrib", "TIRF-ROTH-IRA-CONTRIB", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TIRF_ROTH_IRA_CONTRIB");
        form_01_Tirf_Roth_Ira_Contrib.setDdmHeader("ROTH/IRA/CONTRIBUTION");
        form_01_Tirf_Taxable_Amt = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Taxable_Amt", "TIRF-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 11, 
            2, RepeatingFieldStrategy.None, "TIRF_TAXABLE_AMT");
        form_01_Tirf_Taxable_Amt.setDdmHeader("TAXABLE/AMOUNT");
        form_01_Tirf_Ivc_Amt = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Ivc_Amt", "TIRF-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "TIRF_IVC_AMT");
        form_01_Tirf_Ivc_Amt.setDdmHeader("IVC/AMOUNT");
        form_01_Tirf_Distribution_Cde = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Distribution_Cde", "TIRF-DISTRIBUTION-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TIRF_DISTRIBUTION_CDE");
        form_01_Tirf_Distribution_Cde.setDdmHeader("DISTRI-/BUTION/CODE");
        form_01_Count_Casttirf_1099_R_State_Grp = vw_form_01.getRecord().newFieldInGroup("form_01_Count_Casttirf_1099_R_State_Grp", "C*TIRF-1099-R-STATE-GRP", 
            RepeatingFieldStrategy.CAsteriskVariable, "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");

        form_01_Tirf_1099_R_State_Grp = vw_form_01.getRecord().newGroupInGroup("form_01_Tirf_1099_R_State_Grp", "TIRF-1099-R-STATE-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_01_Tirf_1099_R_State_Grp.setDdmHeader("1099-R/STATE/GROUP");
        form_01_Tirf_State_Tax_Wthld = form_01_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_01_Tirf_State_Tax_Wthld", "TIRF-STATE-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_01_Tirf_State_Tax_Wthld.setDdmHeader("STATE/TAX/WITHHELD");
        form_01_Tirf_Loc_Tax_Wthld = form_01_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_01_Tirf_Loc_Tax_Wthld", "TIRF-LOC-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LOC_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_01_Tirf_Loc_Tax_Wthld.setDdmHeader("LOCAL/TAX/WITHHELD");
        form_01_Tirf_State_Rpt_Ind = form_01_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_01_Tirf_State_Rpt_Ind", "TIRF-STATE-RPT-IND", FieldType.STRING, 
            1, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_RPT_IND", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_01_Tirf_State_Rpt_Ind.setDdmHeader("STATE/RPT/IND");
        form_01_Tirf_State_Code = form_01_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_01_Tirf_State_Code", "TIRF-STATE-CODE", FieldType.STRING, 2, 
            new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_CODE", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_01_Tirf_State_Code.setDdmHeader("RESI-/DENCY/CODE");
        form_01_Tirf_State_Distr = form_01_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_01_Tirf_State_Distr", "TIRF-STATE-DISTR", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_DISTR", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_01_Tirf_State_Distr.setDdmHeader("STATE/DISTRI-/BUTION");
        form_01_Tirf_Loc_Distr = form_01_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_01_Tirf_Loc_Distr", "TIRF-LOC-DISTR", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LOC_DISTR", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_01_Tirf_Loc_Distr.setDdmHeader("LOCALITY/DISTRI-/BUTION");
        form_01_Tirf_State_Hardcopy_Ind = form_01_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_01_Tirf_State_Hardcopy_Ind", "TIRF-STATE-HARDCOPY-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_HARDCOPY_IND", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_01_Tirf_State_Hardcopy_Ind.setDdmHeader("HARD-/COPY/IND");
        form_01_Tirf_Roth_Year = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Roth_Year", "TIRF-ROTH-YEAR", FieldType.PACKED_DECIMAL, 7, RepeatingFieldStrategy.None, 
            "TIRF_ROTH_YEAR");
        form_01_Tirf_Roth_Year.setDdmHeader("1 YEAR/DESIGNATED/ROTH CONTR.");
        form_01_Tirf_Taxable_Not_Det = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Taxable_Not_Det", "TIRF-TAXABLE-NOT-DET", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRF_TAXABLE_NOT_DET");
        form_01_Tirf_Taxable_Not_Det.setDdmHeader("TAXABLE/AMOUNT NOT/DETERMINED");
        form_01_Tirf_Tot_Contractual_Ivc = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Tot_Contractual_Ivc", "TIRF-TOT-CONTRACTUAL-IVC", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TIRF_TOT_CONTRACTUAL_IVC");
        form_01_Tirf_Tot_Contractual_Ivc.setDdmHeader("TOTAL/CONTRACTUAL/IVC");
        form_01_Tirf_Ira_Distr = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Ira_Distr", "TIRF-IRA-DISTR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_IRA_DISTR");
        form_01_Tirf_Ira_Distr.setDdmHeader("IRA/DISTRI-/BUTION");
        form_01_Tirf_Pct_Of_Tot = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Pct_Of_Tot", "TIRF-PCT-OF-TOT", FieldType.PACKED_DECIMAL, 3, RepeatingFieldStrategy.None, 
            "TIRF_PCT_OF_TOT");
        form_01_Tirf_Pct_Of_Tot.setDdmHeader("PERCENT/OF TOTAL/DISTRIB.");
        form_01_Tirf_Tot_Distr = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Tot_Distr", "TIRF-TOT-DISTR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_TOT_DISTR");
        form_01_Tirf_Tot_Distr.setDdmHeader("TOTAL/DISTRI-/BUTION");
        form_01_Tirf_Moore_Test_Ind = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Moore_Test_Ind", "TIRF-MOORE-TEST-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TIRF_MOORE_TEST_IND");
        form_01_Tirf_Moore_Test_Ind.setDdmHeader("MOORE/TEST/IND");
        form_01_Tirf_Participant_Name = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Participant_Name", "TIRF-PARTICIPANT-NAME", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "TIRF_PARTICIPANT_NAME");
        form_01_Tirf_Participant_Name.setDdmHeader("PARTICIPANT/NAME");
        form_01_Tirf_Addr_Ln1 = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Addr_Ln1", "TIRF-ADDR-LN1", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TIRF_ADDR_LN1");
        form_01_Tirf_Addr_Ln1.setDdmHeader("ADDRESS/LINE 1");
        form_01_Tirf_Addr_Ln2 = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Addr_Ln2", "TIRF-ADDR-LN2", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TIRF_ADDR_LN2");
        form_01_Tirf_Addr_Ln2.setDdmHeader("ADDRESS/LINE 2");
        form_01_Tirf_Addr_Ln3 = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Addr_Ln3", "TIRF-ADDR-LN3", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TIRF_ADDR_LN3");
        form_01_Tirf_Addr_Ln3.setDdmHeader("ADDRESS/LINE 3");
        form_01_Tirf_Addr_Ln4 = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Addr_Ln4", "TIRF-ADDR-LN4", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TIRF_ADDR_LN4");
        form_01_Tirf_Addr_Ln4.setDdmHeader("ADDRESS/LINE 4");
        form_01_Tirf_Addr_Ln5 = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Addr_Ln5", "TIRF-ADDR-LN5", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TIRF_ADDR_LN5");
        form_01_Tirf_Addr_Ln5.setDdmHeader("ADDRESS/LINE 5");
        form_01_Tirf_Addr_Ln6 = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Addr_Ln6", "TIRF-ADDR-LN6", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TIRF_ADDR_LN6");
        form_01_Tirf_Addr_Ln6.setDdmHeader("ADDRESS/LINE 6");
        form_01_Tirf_Walk_Rte = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Walk_Rte", "TIRF-WALK-RTE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_WALK_RTE");
        form_01_Tirf_Walk_Rte.setDdmHeader("WALK/ROUTE");
        form_01_Tirf_Geo_Cde = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Geo_Cde", "TIRF-GEO-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_GEO_CDE");
        form_01_Tirf_Geo_Cde.setDdmHeader("GEO/CODE");
        form_01_Tirf_Country_Name = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Country_Name", "TIRF-COUNTRY-NAME", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "TIRF_COUNTRY_NAME");
        form_01_Tirf_Country_Name.setDdmHeader("COUNTRY NAME");
        form_01_Tirf_Foreign_Addr = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Foreign_Addr", "TIRF-FOREIGN-ADDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_FOREIGN_ADDR");
        form_01_Tirf_Foreign_Addr.setDdmHeader("FORE-/IGN/ADDR");
        form_01_Tirf_Zip = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Zip", "TIRF-ZIP", FieldType.STRING, 9, RepeatingFieldStrategy.None, "TIRF_ZIP");
        form_01_Tirf_Zip.setDdmHeader("ZIP");
        form_01_Tirf_Trad_Ira_Rollover = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Trad_Ira_Rollover", "TIRF-TRAD-IRA-ROLLOVER", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TIRF_TRAD_IRA_ROLLOVER");
        form_01_Tirf_Trad_Ira_Rollover.setDdmHeader("TRADITIONAL/IRA/ROLLOVERS");

        form_01__R_Field_1 = vw_form_01.getRecord().newGroupInGroup("form_01__R_Field_1", "REDEFINE", form_01_Tirf_Trad_Ira_Rollover);
        form_01_Tirf_Irr_Amt = form_01__R_Field_1.newFieldInGroup("form_01_Tirf_Irr_Amt", "TIRF-IRR-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        form_01_Tirf_Ltr_Data = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Ltr_Data", "TIRF-LTR-DATA", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_LTR_DATA");
        form_01_Tirf_Ltr_Data.setDdmHeader("LTR/DATA");
        form_01_Tirf_Superde_1 = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Superde_1", "TIRF-SUPERDE-1", FieldType.BINARY, 38, RepeatingFieldStrategy.None, 
            "TIRF_SUPERDE_1");
        form_01_Tirf_Superde_1.setSuperDescriptor(true);
        form_01_Tirf_Superde_3 = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Superde_3", "TIRF-SUPERDE-3", FieldType.STRING, 33, RepeatingFieldStrategy.None, 
            "TIRF_SUPERDE_3");
        form_01_Tirf_Superde_3.setSuperDescriptor(true);
        form_01_Tirf_Superde_10 = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Superde_10", "TIRF-SUPERDE-10", FieldType.STRING, 31, RepeatingFieldStrategy.None, 
            "TIRF_SUPERDE_10");
        form_01_Tirf_Superde_10.setSuperDescriptor(true);
        form_01_Tirf_Superde_12 = vw_form_01.getRecord().newFieldInGroup("form_01_Tirf_Superde_12", "TIRF-SUPERDE-12", FieldType.STRING, 33, RepeatingFieldStrategy.None, 
            "TIRF_SUPERDE_12");
        form_01_Tirf_Superde_12.setSuperDescriptor(true);
        registerRecord(vw_form_01);

        vw_twrparti_Read = new DataAccessProgramView(new NameInfo("vw_twrparti_Read", "TWRPARTI-READ"), "TWRPARTI_PARTICIPANT_FILE", "TWR_PARTICIPANT");
        twrparti_Read_Twrparti_Tax_Id = vw_twrparti_Read.getRecord().newFieldInGroup("twrparti_Read_Twrparti_Tax_Id", "TWRPARTI-TAX-ID", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TWRPARTI_TAX_ID");
        twrparti_Read_Twrparti_Rlup_Email_Pref = vw_twrparti_Read.getRecord().newFieldInGroup("twrparti_Read_Twrparti_Rlup_Email_Pref", "TWRPARTI-RLUP-EMAIL-PREF", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_EMAIL_PREF");
        twrparti_Read_Twrparti_Rlup_Email_Pref_Dte = vw_twrparti_Read.getRecord().newFieldInGroup("twrparti_Read_Twrparti_Rlup_Email_Pref_Dte", "TWRPARTI-RLUP-EMAIL-PREF-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_EMAIL_PREF_DTE");
        twrparti_Read_Twrparti_Rlup_Email_Addr = vw_twrparti_Read.getRecord().newFieldInGroup("twrparti_Read_Twrparti_Rlup_Email_Addr", "TWRPARTI-RLUP-EMAIL-ADDR", 
            FieldType.STRING, 70, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_EMAIL_ADDR");
        twrparti_Read_Twrparti_Rlup_Email_Addr_Dte = vw_twrparti_Read.getRecord().newFieldInGroup("twrparti_Read_Twrparti_Rlup_Email_Addr_Dte", "TWRPARTI-RLUP-EMAIL-ADDR-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_EMAIL_ADDR_DTE");
        registerRecord(vw_twrparti_Read);

        pnd_Twrparti_Curr_Rec_Sd = localVariables.newFieldInRecord("pnd_Twrparti_Curr_Rec_Sd", "#TWRPARTI-CURR-REC-SD", FieldType.STRING, 19);

        pnd_Twrparti_Curr_Rec_Sd__R_Field_2 = localVariables.newGroupInRecord("pnd_Twrparti_Curr_Rec_Sd__R_Field_2", "REDEFINE", pnd_Twrparti_Curr_Rec_Sd);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Tax_Id = pnd_Twrparti_Curr_Rec_Sd__R_Field_2.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Tax_Id", 
            "#KEY-TWRPARTI-TAX-ID", FieldType.STRING, 10);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Status = pnd_Twrparti_Curr_Rec_Sd__R_Field_2.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Status", 
            "#KEY-TWRPARTI-STATUS", FieldType.STRING, 1);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Part_Eff_End_Dte = pnd_Twrparti_Curr_Rec_Sd__R_Field_2.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Part_Eff_End_Dte", 
            "#KEY-TWRPARTI-PART-EFF-END-DTE", FieldType.STRING, 8);

        vw_form_Upd = new DataAccessProgramView(new NameInfo("vw_form_Upd", "FORM-UPD"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form_Upd_Tirf_Tax_Year = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_TAX_YEAR");
        form_Upd_Tirf_Tax_Year.setDdmHeader("TAX/YEAR");
        form_Upd_Tirf_Form_Type = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TIRF_FORM_TYPE");
        form_Upd_Tirf_Form_Type.setDdmHeader("FORM");
        form_Upd_Tirf_Tin = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TIRF_TIN");
        form_Upd_Tirf_Tin.setDdmHeader("TAX/ID/NUMBER");
        form_Upd_Tirf_Payee_Cde = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_PAYEE_CDE");
        form_Upd_Tirf_Payee_Cde.setDdmHeader("PAYEE/CODE");
        form_Upd_Tirf_Mobius_Stat = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Mobius_Stat", "TIRF-MOBIUS-STAT", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_MOBIUS_STAT");
        form_Upd_Tirf_Mobius_Stat.setDdmHeader("MOBIUS/STATUS");
        form_Upd_Tirf_Mobius_Ind = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Mobius_Ind", "TIRF-MOBIUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_MOBIUS_IND");
        form_Upd_Tirf_Mobius_Ind.setDdmHeader("INCLUDE/IN/MOBIUS");
        form_Upd_Tirf_Mobius_Stat_Date = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Mobius_Stat_Date", "TIRF-MOBIUS-STAT-DATE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TIRF_MOBIUS_STAT_DATE");
        form_Upd_Tirf_Mobius_Stat_Date.setDdmHeader("MOBIUS/STATUS/DATE");
        form_Upd_Tirf_Part_Rpt_Date = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Part_Rpt_Date", "TIRF-PART-RPT-DATE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIRF_PART_RPT_DATE");
        form_Upd_Tirf_Part_Rpt_Date.setDdmHeader("PART/RPT/DATE");
        form_Upd_Tirf_Part_Rpt_Ind = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Part_Rpt_Ind", "TIRF-PART-RPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_PART_RPT_IND");
        form_Upd_Tirf_Part_Rpt_Ind.setDdmHeader("PART/RPT/IND");
        form_Upd_Tirf_Lu_User = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Lu_User", "TIRF-LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_LU_USER");
        form_Upd_Tirf_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        form_Upd_Tirf_Lu_Ts = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Lu_Ts", "TIRF-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TIRF_LU_TS");
        form_Upd_Tirf_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        form_Upd_Tirf_Company_Cde = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Company_Cde", "TIRF-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_COMPANY_CDE");
        form_Upd_Tirf_Company_Cde.setDdmHeader("COM-/PANY/CODE");
        registerRecord(vw_form_Upd);

        pnd_Wkfle_Recd_Cnt = localVariables.newFieldInRecord("pnd_Wkfle_Recd_Cnt", "#WKFLE-RECD-CNT", FieldType.NUMERIC, 20);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Input = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input", "#INPUT");
        pnd_Ws_Pnd_Form_Type = pnd_Ws_Pnd_Input.newFieldInGroup("pnd_Ws_Pnd_Form_Type", "#FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws_Pnd_Input.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_File_Limit = pnd_Ws_Pnd_Input.newFieldInGroup("pnd_Ws_Pnd_File_Limit", "#FILE-LIMIT", FieldType.NUMERIC, 7);
        pnd_Ws_Pnd_Email_File = pnd_Ws_Pnd_Input.newFieldInGroup("pnd_Ws_Pnd_Email_File", "#EMAIL-FILE", FieldType.STRING, 1);
        pnd_Ws_Pnd_Reset_Et_Data = pnd_Ws_Pnd_Input.newFieldInGroup("pnd_Ws_Pnd_Reset_Et_Data", "#RESET-ET-DATA", FieldType.STRING, 1);
        pnd_Ws_Pnd_Bypass_E_Deliv = pnd_Ws_Pnd_Input.newFieldInGroup("pnd_Ws_Pnd_Bypass_E_Deliv", "#BYPASS-E-DELIV", FieldType.STRING, 1);
        pnd_Ws_Pnd_Mobius_Limit = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Mobius_Limit", "#MOBIUS-LIMIT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Stored_Tin = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Stored_Tin", "#STORED-TIN", FieldType.STRING, 10);
        pnd_Ws_Pnd_Restart_Text = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Restart_Text", "#RESTART-TEXT", FieldType.STRING, 1);
        pnd_Ws_Pnd_Mobius_Status = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Mobius_Status", "#MOBIUS-STATUS", FieldType.STRING, 2);
        pnd_Ws_Pnd_Mobius_Status_S = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Mobius_Status_S", "#MOBIUS-STATUS-S", FieldType.STRING, 2);
        pnd_Ws_Pnd_Mobius_Status_01 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Mobius_Status_01", "#MOBIUS-STATUS-01", FieldType.STRING, 2);
        pnd_Ws_Pnd_Customer_Id = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Customer_Id", "#CUSTOMER-ID", FieldType.STRING, 15);

        pnd_Ws__R_Field_3 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_3", "REDEFINE", pnd_Ws_Pnd_Customer_Id);
        pnd_Ws_Pnd_Pin = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_Ws_Pnd_Hold_Customer_Id = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Hold_Customer_Id", "#HOLD-CUSTOMER-ID", FieldType.STRING, 15);
        pnd_Ws_Pnd_Link_Let_Type = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Link_Let_Type", "#LINK-LET-TYPE", FieldType.STRING, 1, new DbsArrayController(1, 
            7));
        pnd_Ws_Pnd_Form_Page_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Form_Page_Cnt", "#FORM-PAGE-CNT", FieldType.PACKED_DECIMAL, 2);
        pnd_Ws_Pnd_Zero_Isn = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Zero_Isn", "#ZERO-ISN", FieldType.NUMERIC, 8);
        pnd_Ws_Pnd_Sys_Date = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Sys_Date", "#SYS-DATE", FieldType.DATE);
        pnd_Ws_Pnd_Sys_Time = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Sys_Time", "#SYS-TIME", FieldType.TIME);
        pnd_Ws_Pnd_Message = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Message", "#MESSAGE", FieldType.STRING, 30);
        pnd_Ws_Pnd_Message_S = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Message_S", "#MESSAGE-S", FieldType.STRING, 30);
        pnd_Ws_Pnd_Message_01 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Message_01", "#MESSAGE-01", FieldType.STRING, 30);

        pnd_Ws_Counters = pnd_Ws.newGroupInGroup("pnd_Ws_Counters", "COUNTERS");
        pnd_Ws_Pnd_Read_Cnt_Suny = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Read_Cnt_Suny", "#READ-CNT-SUNY", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Read_Cnt_1099r = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Read_Cnt_1099r", "#READ-CNT-1099R", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Form_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Part_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Part_Cnt", "#PART-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Empty_Form_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Empty_Form_Cnt", "#EMPTY-FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Cpm_Lookup_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Cpm_Lookup_Cnt", "#CPM-LOOKUP-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Tax_Email_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Tax_Email_Cnt", "#TAX-EMAIL-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_No_Tax_Email_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_No_Tax_Email_Cnt", "#NO-TAX-EMAIL-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Email_Non_Email_Bypassed = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Email_Non_Email_Bypassed", "#EMAIL-NON-EMAIL-BYPASSED", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Missing_Pin_Cnt_Suny = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Missing_Pin_Cnt_Suny", "#MISSING-PIN-CNT-SUNY", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Missing_Pin_Cnt_1099r = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Missing_Pin_Cnt_1099r", "#MISSING-PIN-CNT-1099R", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Missing_Pin_Cnt_S_Good = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Missing_Pin_Cnt_S_Good", "#MISSING-PIN-CNT-S-GOOD", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Missing_Pin_Cnt_R_Good = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Missing_Pin_Cnt_R_Good", "#MISSING-PIN-CNT-R-GOOD", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Missing_Ssn_Cnt_R = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Missing_Ssn_Cnt_R", "#MISSING-SSN-CNT-R", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Missing_Ssn_Cnt_S = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Missing_Ssn_Cnt_S", "#MISSING-SSN-CNT-S", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Paper_Print_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Paper_Print_Cnt", "#PAPER-PRINT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Bypass_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Bypass_Cnt", "#BYPASS-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_5498_Bypass_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_5498_Bypass_Cnt", "#5498-BYPASS-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Letr_Bypass_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Letr_Bypass_Cnt", "#LETR-BYPASS-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Form_Pages_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Form_Pages_Cnt", "#FORM-PAGES-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Multi_Page_Form_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Multi_Page_Form_Cnt", "#MULTI-PAGE-FORM-CNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Total_Form_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Total_Form_Cnt", "#TOTAL-FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Non_Suny_Part_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Non_Suny_Part_Cnt", "#NON-SUNY-PART-CNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Suny_Det_Rec_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Suny_Det_Rec_Cnt", "#SUNY-DET-REC-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Tirf_Paper_Print_Hold_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tirf_Paper_Print_Hold_Ind", "#TIRF-PAPER-PRINT-HOLD-IND", FieldType.STRING, 
            1);
        pnd_Ws_Pnd_Tirf_Moore_Hold_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tirf_Moore_Hold_Ind", "#TIRF-MOORE-HOLD-IND", FieldType.STRING, 1);
        pnd_Ws_Pnd_Debug = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Read_Cnt1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Read_Cnt1", "#READ-CNT1", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Read_Cnt_01 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Read_Cnt_01", "#READ-CNT-01", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Update_Cnt_Suny = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Update_Cnt_Suny", "#UPDATE-CNT-SUNY", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Update_Cnt_1099r = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Update_Cnt_1099r", "#UPDATE-CNT-1099R", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Et_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Et_Cnt", "#ET-CNT", FieldType.PACKED_DECIMAL, 3);
        et_Limit = localVariables.newFieldInRecord("et_Limit", "ET-LIMIT", FieldType.PACKED_DECIMAL, 3);
        pnd_Contract_Txt1 = localVariables.newFieldInRecord("pnd_Contract_Txt1", "#CONTRACT-TXT1", FieldType.STRING, 9);
        pnd_Contract_Hdr_Lit1 = localVariables.newFieldInRecord("pnd_Contract_Hdr_Lit1", "#CONTRACT-HDR-LIT1", FieldType.STRING, 9);
        pnd_Test_Ind_Cnt = localVariables.newFieldInRecord("pnd_Test_Ind_Cnt", "#TEST-IND-CNT", FieldType.NUMERIC, 10);
        pnd_Ws_Form_Type = localVariables.newFieldInRecord("pnd_Ws_Form_Type", "#WS-FORM-TYPE", FieldType.STRING, 20);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_In_Form_Type = localVariables.newFieldInRecord("pnd_In_Form_Type", "#IN-FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_Bypass_Reason = localVariables.newFieldInRecord("pnd_Bypass_Reason", "#BYPASS-REASON", FieldType.STRING, 50);
        pnd_Tin_Hold = localVariables.newFieldInRecord("pnd_Tin_Hold", "#TIN-HOLD", FieldType.STRING, 10);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 10);
        pnd_Ws_Progname = localVariables.newFieldInRecord("pnd_Ws_Progname", "#WS-PROGNAME", FieldType.STRING, 11);

        pnd_Db_Read_Fields = localVariables.newGroupInRecord("pnd_Db_Read_Fields", "#DB-READ-FIELDS");
        pnd_Db_Read_Fields_Pnd_Letter_Cnt_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Letter_Cnt_Db", "#LETTER-CNT-DB", FieldType.NUMERIC, 
            10);
        pnd_Db_Read_Fields_Pnd_Detl_Cnt_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Detl_Cnt_Db", "#DETL-CNT-DB", FieldType.NUMERIC, 
            10);
        pnd_Db_Read_Fields_Pnd_Suny_Gross_Amt_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Suny_Gross_Amt_Db", "#SUNY-GROSS-AMT-DB", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Db_Read_Fields_Pnd_Suny_Fed_Tax_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Suny_Fed_Tax_Db", "#SUNY-FED-TAX-DB", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Db_Read_Fields_Pnd_Suny_Summ_State_Tax_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Suny_Summ_State_Tax_Db", "#SUNY-SUMM-STATE-TAX-DB", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Db_Read_Fields_Pnd_Suny_Summ_Lcl_Tax_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Suny_Summ_Lcl_Tax_Db", "#SUNY-SUMM-LCL-TAX-DB", 
            FieldType.PACKED_DECIMAL, 13, 2);

        pnd_E_Del_Bypassed_Fields = localVariables.newGroupInRecord("pnd_E_Del_Bypassed_Fields", "#E-DEL-BYPASSED-FIELDS");
        pnd_E_Del_Bypassed_Fields_Pnd_Letter_Cnt_B = pnd_E_Del_Bypassed_Fields.newFieldInGroup("pnd_E_Del_Bypassed_Fields_Pnd_Letter_Cnt_B", "#LETTER-CNT-B", 
            FieldType.NUMERIC, 10);
        pnd_E_Del_Bypassed_Fields_Pnd_Detl_Cnt_B = pnd_E_Del_Bypassed_Fields.newFieldInGroup("pnd_E_Del_Bypassed_Fields_Pnd_Detl_Cnt_B", "#DETL-CNT-B", 
            FieldType.NUMERIC, 10);
        pnd_E_Del_Bypassed_Fields_Pnd_Suny_Gross_Amt_B = pnd_E_Del_Bypassed_Fields.newFieldInGroup("pnd_E_Del_Bypassed_Fields_Pnd_Suny_Gross_Amt_B", "#SUNY-GROSS-AMT-B", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_E_Del_Bypassed_Fields_Pnd_Suny_Fed_Tax_B = pnd_E_Del_Bypassed_Fields.newFieldInGroup("pnd_E_Del_Bypassed_Fields_Pnd_Suny_Fed_Tax_B", "#SUNY-FED-TAX-B", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_E_Del_Bypassed_Fields_Pnd_Suny_Summ_State_Tax_B = pnd_E_Del_Bypassed_Fields.newFieldInGroup("pnd_E_Del_Bypassed_Fields_Pnd_Suny_Summ_State_Tax_B", 
            "#SUNY-SUMM-STATE-TAX-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_E_Del_Bypassed_Fields_Pnd_Suny_Summ_Lcl_Tax_B = pnd_E_Del_Bypassed_Fields.newFieldInGroup("pnd_E_Del_Bypassed_Fields_Pnd_Suny_Summ_Lcl_Tax_B", 
            "#SUNY-SUMM-LCL-TAX-B", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_E_Del_Accepted_Fields = localVariables.newGroupInRecord("pnd_E_Del_Accepted_Fields", "#E-DEL-ACCEPTED-FIELDS");
        pnd_E_Del_Accepted_Fields_Pnd_Letter_Cnt_A = pnd_E_Del_Accepted_Fields.newFieldInGroup("pnd_E_Del_Accepted_Fields_Pnd_Letter_Cnt_A", "#LETTER-CNT-A", 
            FieldType.NUMERIC, 10);
        pnd_E_Del_Accepted_Fields_Pnd_Detl_Cnt_A = pnd_E_Del_Accepted_Fields.newFieldInGroup("pnd_E_Del_Accepted_Fields_Pnd_Detl_Cnt_A", "#DETL-CNT-A", 
            FieldType.NUMERIC, 10);
        pnd_E_Del_Accepted_Fields_Pnd_Suny_Gross_Amt_A = pnd_E_Del_Accepted_Fields.newFieldInGroup("pnd_E_Del_Accepted_Fields_Pnd_Suny_Gross_Amt_A", "#SUNY-GROSS-AMT-A", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_E_Del_Accepted_Fields_Pnd_Suny_Fed_Tax_A = pnd_E_Del_Accepted_Fields.newFieldInGroup("pnd_E_Del_Accepted_Fields_Pnd_Suny_Fed_Tax_A", "#SUNY-FED-TAX-A", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_E_Del_Accepted_Fields_Pnd_Suny_Summ_State_Tax_A = pnd_E_Del_Accepted_Fields.newFieldInGroup("pnd_E_Del_Accepted_Fields_Pnd_Suny_Summ_State_Tax_A", 
            "#SUNY-SUMM-STATE-TAX-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_E_Del_Accepted_Fields_Pnd_Suny_Summ_Lcl_Tax_A = pnd_E_Del_Accepted_Fields.newFieldInGroup("pnd_E_Del_Accepted_Fields_Pnd_Suny_Summ_Lcl_Tax_A", 
            "#SUNY-SUMM-LCL-TAX-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Tax_Year = localVariables.newFieldInRecord("pnd_Ws_Tax_Year", "#WS-TAX-YEAR", FieldType.NUMERIC, 4);

        pnd_Ws_Tax_Year__R_Field_4 = localVariables.newGroupInRecord("pnd_Ws_Tax_Year__R_Field_4", "REDEFINE", pnd_Ws_Tax_Year);
        pnd_Ws_Tax_Year_Pnd_Ws_Tax_Year_A = pnd_Ws_Tax_Year__R_Field_4.newFieldInGroup("pnd_Ws_Tax_Year_Pnd_Ws_Tax_Year_A", "#WS-TAX-YEAR-A", FieldType.STRING, 
            4);
        pnd_Detl_Temp_Db = localVariables.newFieldInRecord("pnd_Detl_Temp_Db", "#DETL-TEMP-DB", FieldType.NUMERIC, 10);
        pnd_Detl_Temp_B = localVariables.newFieldInRecord("pnd_Detl_Temp_B", "#DETL-TEMP-B", FieldType.NUMERIC, 10);
        pnd_Detl_Temp_A = localVariables.newFieldInRecord("pnd_Detl_Temp_A", "#DETL-TEMP-A", FieldType.NUMERIC, 10);
        pnd_Detl_Temp_P_B = localVariables.newFieldInRecord("pnd_Detl_Temp_P_B", "#DETL-TEMP-P-B", FieldType.NUMERIC, 10);
        pnd_Detl_Temp_P_A = localVariables.newFieldInRecord("pnd_Detl_Temp_P_A", "#DETL-TEMP-P-A", FieldType.NUMERIC, 10);
        pnd_Superde_12 = localVariables.newFieldInRecord("pnd_Superde_12", "#SUPERDE-12", FieldType.STRING, 19);

        pnd_Superde_12__R_Field_5 = localVariables.newGroupInRecord("pnd_Superde_12__R_Field_5", "REDEFINE", pnd_Superde_12);
        pnd_Superde_12_Pnd_Sd12_Tax_Year = pnd_Superde_12__R_Field_5.newFieldInGroup("pnd_Superde_12_Pnd_Sd12_Tax_Year", "#SD12-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Superde_12_Pnd_Sd12_Form_Type = pnd_Superde_12__R_Field_5.newFieldInGroup("pnd_Superde_12_Pnd_Sd12_Form_Type", "#SD12-FORM-TYPE", FieldType.NUMERIC, 
            2);
        pnd_Superde_12_Pnd_Sd12_Active_Ind = pnd_Superde_12__R_Field_5.newFieldInGroup("pnd_Superde_12_Pnd_Sd12_Active_Ind", "#SD12-ACTIVE-IND", FieldType.STRING, 
            1);
        pnd_Superde_12_Pnd_Sd12_Mobius_Ind = pnd_Superde_12__R_Field_5.newFieldInGroup("pnd_Superde_12_Pnd_Sd12_Mobius_Ind", "#SD12-MOBIUS-IND", FieldType.STRING, 
            1);
        pnd_Superde_12_Pnd_Sd12_Tin = pnd_Superde_12__R_Field_5.newFieldInGroup("pnd_Superde_12_Pnd_Sd12_Tin", "#SD12-TIN", FieldType.STRING, 10);
        pnd_Superde_12_End = localVariables.newFieldInRecord("pnd_Superde_12_End", "#SUPERDE-12-END", FieldType.STRING, 9);
        pnd_Superde_12_Form_01 = localVariables.newFieldInRecord("pnd_Superde_12_Form_01", "#SUPERDE-12-FORM-01", FieldType.STRING, 19);

        pnd_Superde_12_Form_01__R_Field_6 = localVariables.newGroupInRecord("pnd_Superde_12_Form_01__R_Field_6", "REDEFINE", pnd_Superde_12_Form_01);
        pnd_Superde_12_Form_01_Pnd_Sd12_Tax_Year = pnd_Superde_12_Form_01__R_Field_6.newFieldInGroup("pnd_Superde_12_Form_01_Pnd_Sd12_Tax_Year", "#SD12-TAX-YEAR", 
            FieldType.STRING, 4);
        pnd_Superde_12_Form_01_Pnd_Sd12_Form_Type = pnd_Superde_12_Form_01__R_Field_6.newFieldInGroup("pnd_Superde_12_Form_01_Pnd_Sd12_Form_Type", "#SD12-FORM-TYPE", 
            FieldType.NUMERIC, 2);
        pnd_Superde_12_Form_01_Pnd_Sd12_Active_Ind = pnd_Superde_12_Form_01__R_Field_6.newFieldInGroup("pnd_Superde_12_Form_01_Pnd_Sd12_Active_Ind", "#SD12-ACTIVE-IND", 
            FieldType.STRING, 1);
        pnd_Superde_12_Form_01_Pnd_Sd12_Mobius_Ind = pnd_Superde_12_Form_01__R_Field_6.newFieldInGroup("pnd_Superde_12_Form_01_Pnd_Sd12_Mobius_Ind", "#SD12-MOBIUS-IND", 
            FieldType.STRING, 1);
        pnd_Superde_12_Form_01_Pnd_Sd12_Tin = pnd_Superde_12_Form_01__R_Field_6.newFieldInGroup("pnd_Superde_12_Form_01_Pnd_Sd12_Tin", "#SD12-TIN", FieldType.STRING, 
            10);
        pnd_Superde_12_Form_01_End = localVariables.newFieldInRecord("pnd_Superde_12_Form_01_End", "#SUPERDE-12-FORM-01-END", FieldType.STRING, 19);

        pnd_Superde_12_Form_01_End__R_Field_7 = localVariables.newGroupInRecord("pnd_Superde_12_Form_01_End__R_Field_7", "REDEFINE", pnd_Superde_12_Form_01_End);
        pnd_Superde_12_Form_01_End_Pnd_Sd12_Tax_Year_End = pnd_Superde_12_Form_01_End__R_Field_7.newFieldInGroup("pnd_Superde_12_Form_01_End_Pnd_Sd12_Tax_Year_End", 
            "#SD12-TAX-YEAR-END", FieldType.STRING, 4);
        pnd_Superde_12_Form_01_End_Pnd_Sd12_Form_Type_End = pnd_Superde_12_Form_01_End__R_Field_7.newFieldInGroup("pnd_Superde_12_Form_01_End_Pnd_Sd12_Form_Type_End", 
            "#SD12-FORM-TYPE-END", FieldType.NUMERIC, 2);
        pnd_Superde_12_Form_01_End_Pnd_Sd12_Active_Ind_End = pnd_Superde_12_Form_01_End__R_Field_7.newFieldInGroup("pnd_Superde_12_Form_01_End_Pnd_Sd12_Active_Ind_End", 
            "#SD12-ACTIVE-IND-END", FieldType.STRING, 1);
        pnd_Superde_12_Form_01_End_Pnd_Sd12_Mobius_Ind_End = pnd_Superde_12_Form_01_End__R_Field_7.newFieldInGroup("pnd_Superde_12_Form_01_End_Pnd_Sd12_Mobius_Ind_End", 
            "#SD12-MOBIUS-IND-END", FieldType.STRING, 1);
        pnd_Superde_12_Form_01_End_Pnd_Sd12_Tin = pnd_Superde_12_Form_01_End__R_Field_7.newFieldInGroup("pnd_Superde_12_Form_01_End_Pnd_Sd12_Tin", "#SD12-TIN", 
            FieldType.STRING, 10);
        pnd_Form_Type_10_01_Found = localVariables.newFieldInRecord("pnd_Form_Type_10_01_Found", "#FORM-TYPE-10-01-FOUND", FieldType.BOOLEAN, 1);
        pnd_First_Time_Form_Process = localVariables.newFieldInRecord("pnd_First_Time_Form_Process", "#FIRST-TIME-FORM-PROCESS", FieldType.BOOLEAN, 1);
        pnd_Old_Tin = localVariables.newFieldInRecord("pnd_Old_Tin", "#OLD-TIN", FieldType.STRING, 10);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.NUMERIC, 2);
        pnd_Y_Cnt = localVariables.newFieldInRecord("pnd_Y_Cnt", "#Y-CNT", FieldType.NUMERIC, 2);
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.NUMERIC, 2);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.NUMERIC, 2);
        pnd_Tin_On_Hold = localVariables.newFieldInRecord("pnd_Tin_On_Hold", "#TIN-ON-HOLD", FieldType.STRING, 10);
        pnd_M = localVariables.newFieldInRecord("pnd_M", "#M", FieldType.PACKED_DECIMAL, 3);
        pnd_Mob_Prev_Pin = localVariables.newFieldInRecord("pnd_Mob_Prev_Pin", "#MOB-PREV-PIN", FieldType.STRING, 12);
        pnd_Mob_Prev_Tin = localVariables.newFieldInRecord("pnd_Mob_Prev_Tin", "#MOB-PREV-TIN", FieldType.STRING, 11);
        pnd_Mob_Suny_Ind = localVariables.newFieldInRecord("pnd_Mob_Suny_Ind", "#MOB-SUNY-IND", FieldType.STRING, 3);
        pnd_Mob_Hold_Pin = localVariables.newFieldInRecord("pnd_Mob_Hold_Pin", "#MOB-HOLD-PIN", FieldType.STRING, 12);
        pnd_Mob_Hold_Tin = localVariables.newFieldInRecord("pnd_Mob_Hold_Tin", "#MOB-HOLD-TIN", FieldType.STRING, 11);
        pnd_Hold_Mobdata = localVariables.newFieldInRecord("pnd_Hold_Mobdata", "#HOLD-MOBDATA", FieldType.STRING, 30);
        pnd_Contract_Txt = localVariables.newFieldInRecord("pnd_Contract_Txt", "#CONTRACT-TXT", FieldType.STRING, 9);
        pnd_Contract_Hdr_Lit = localVariables.newFieldInRecord("pnd_Contract_Hdr_Lit", "#CONTRACT-HDR-LIT", FieldType.STRING, 9);
        pnd_Test_Ind = localVariables.newFieldInRecord("pnd_Test_Ind", "#TEST-IND", FieldType.STRING, 1);
        pnd_Test_Ind_01 = localVariables.newFieldInRecord("pnd_Test_Ind_01", "#TEST-IND-01", FieldType.STRING, 1);
        pnd_Coupon_Ind = localVariables.newFieldInRecord("pnd_Coupon_Ind", "#COUPON-IND", FieldType.STRING, 1);
        pnd_Coupon_Ind_01 = localVariables.newFieldInRecord("pnd_Coupon_Ind_01", "#COUPON-IND-01", FieldType.STRING, 1);
        pnd_Suny_Bypassed = localVariables.newFieldInRecord("pnd_Suny_Bypassed", "#SUNY-BYPASSED", FieldType.BOOLEAN, 1);
        pnd_1099r_Bypassed = localVariables.newFieldInRecord("pnd_1099r_Bypassed", "#1099R-BYPASSED", FieldType.BOOLEAN, 1);
        pnd_Ny_Ltr_Bypass = localVariables.newFieldInRecord("pnd_Ny_Ltr_Bypass", "#NY-LTR-BYPASS", FieldType.BOOLEAN, 1);
        pnd_Ny_Ltr_Value_I = localVariables.newFieldInRecord("pnd_Ny_Ltr_Value_I", "#NY-LTR-VALUE-I", FieldType.BOOLEAN, 1);

        pnd_Ltr_Array = localVariables.newGroupArrayInRecord("pnd_Ltr_Array", "#LTR-ARRAY", new DbsArrayController(1, 999));
        pnd_Ltr_Array_Pnd_Ltr_Tin = pnd_Ltr_Array.newFieldInGroup("pnd_Ltr_Array_Pnd_Ltr_Tin", "#LTR-TIN", FieldType.STRING, 10);
        pnd_Ltr_Array_Pnd_Ltr_Cntrct = pnd_Ltr_Array.newFieldInGroup("pnd_Ltr_Array_Pnd_Ltr_Cntrct", "#LTR-CNTRCT", FieldType.STRING, 9);
        pnd_Ltr_Array_Pnd_Ltr_Data = pnd_Ltr_Array.newFieldInGroup("pnd_Ltr_Array_Pnd_Ltr_Data", "#LTR-DATA", FieldType.STRING, 1);
        pnd_Additional_Forms = localVariables.newFieldInRecord("pnd_Additional_Forms", "#ADDITIONAL-FORMS", FieldType.PACKED_DECIMAL, 7);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);

        pnd_Tiaa_Totals = localVariables.newGroupInRecord("pnd_Tiaa_Totals", "#TIAA-TOTALS");
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_R = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_R", "#TIAA-1099-R", FieldType.NUMERIC, 10);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Gross_Amt = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Gross_Amt", "#TIAA-1099-R-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Taxable = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Taxable", "#TIAA-1099-R-TAXABLE", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Fed = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Fed", "#TIAA-1099-R-FED", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_State = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_State", "#TIAA-1099-R-STATE", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Local = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Local", "#TIAA-1099-R-LOCAL", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Ivc = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Ivc", "#TIAA-1099-R-IVC", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Irr = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Irr", "#TIAA-1099-R-IRR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Int = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Int", "#TIAA-1099-R-INT", FieldType.PACKED_DECIMAL, 
            13, 2);

        pnd_Trust_Totals = localVariables.newGroupInRecord("pnd_Trust_Totals", "#TRUST-TOTALS");
        pnd_Trust_Totals_Pnd_Trust_1099_R = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_R", "#TRUST-1099-R", FieldType.NUMERIC, 
            10);
        pnd_Trust_Totals_Pnd_Trust_1099_R_Gross_Amt = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_R_Gross_Amt", "#TRUST-1099-R-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_R_Taxable = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_R_Taxable", "#TRUST-1099-R-TAXABLE", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_R_Fed = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_R_Fed", "#TRUST-1099-R-FED", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_R_State = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_R_State", "#TRUST-1099-R-STATE", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_R_Local = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_R_Local", "#TRUST-1099-R-LOCAL", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_R_Ivc = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_R_Ivc", "#TRUST-1099-R-IVC", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_R_Irr = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_R_Irr", "#TRUST-1099-R-IRR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_R_Int = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_R_Int", "#TRUST-1099-R-INT", FieldType.PACKED_DECIMAL, 
            13, 2);

        pnd_Tiaa_Email_Bypassed = localVariables.newGroupInRecord("pnd_Tiaa_Email_Bypassed", "#TIAA-EMAIL-BYPASSED");
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_B", "#TIAA-1099-R-B", 
            FieldType.NUMERIC, 10);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Gross_Amt_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Gross_Amt_B", 
            "#TIAA-1099-R-GROSS-AMT-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Taxable_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Taxable_B", 
            "#TIAA-1099-R-TAXABLE-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Ivc_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Ivc_B", "#TIAA-1099-R-IVC-B", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Fed_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Fed_B", "#TIAA-1099-R-FED-B", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Local_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Local_B", "#TIAA-1099-R-LOCAL-B", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Int_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Int_B", "#TIAA-1099-R-INT-B", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_State_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_State_B", "#TIAA-1099-R-STATE-B", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Irr_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Irr_B", "#TIAA-1099-R-IRR-B", 
            FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Tiaa_Email_Accepted = localVariables.newGroupInRecord("pnd_Tiaa_Email_Accepted", "#TIAA-EMAIL-ACCEPTED");
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_A", "#TIAA-1099-R-A", 
            FieldType.NUMERIC, 10);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Gross_Amt_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Gross_Amt_A", 
            "#TIAA-1099-R-GROSS-AMT-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Taxable_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Taxable_A", 
            "#TIAA-1099-R-TAXABLE-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Ivc_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Ivc_A", "#TIAA-1099-R-IVC-A", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Fed_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Fed_A", "#TIAA-1099-R-FED-A", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Local_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Local_A", "#TIAA-1099-R-LOCAL-A", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Int_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Int_A", "#TIAA-1099-R-INT-A", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_State_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_State_A", "#TIAA-1099-R-STATE-A", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Irr_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Irr_A", "#TIAA-1099-R-IRR-A", 
            FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Trust_Email_Bypassed = localVariables.newGroupInRecord("pnd_Trust_Email_Bypassed", "#TRUST-EMAIL-BYPASSED");
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_B", "#TRUST-1099-R-B", 
            FieldType.NUMERIC, 10);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Gross_Amt_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Gross_Amt_B", 
            "#TRUST-1099-R-GROSS-AMT-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Taxable_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Taxable_B", 
            "#TRUST-1099-R-TAXABLE-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Ivc_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Ivc_B", 
            "#TRUST-1099-R-IVC-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Fed_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Fed_B", 
            "#TRUST-1099-R-FED-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Local_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Local_B", 
            "#TRUST-1099-R-LOCAL-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Int_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Int_B", 
            "#TRUST-1099-R-INT-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_State_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_State_B", 
            "#TRUST-1099-R-STATE-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Irr_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Irr_B", 
            "#TRUST-1099-R-IRR-B", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Trust_Email_Accepted = localVariables.newGroupInRecord("pnd_Trust_Email_Accepted", "#TRUST-EMAIL-ACCEPTED");
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_A", "#TRUST-1099-R-A", 
            FieldType.NUMERIC, 10);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Gross_Amt_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Gross_Amt_A", 
            "#TRUST-1099-R-GROSS-AMT-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Taxable_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Taxable_A", 
            "#TRUST-1099-R-TAXABLE-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Ivc_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Ivc_A", 
            "#TRUST-1099-R-IVC-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Fed_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Fed_A", 
            "#TRUST-1099-R-FED-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Local_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Local_A", 
            "#TRUST-1099-R-LOCAL-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Int_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Int_A", 
            "#TRUST-1099-R-INT-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_State_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_State_A", 
            "#TRUST-1099-R-STATE-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Irr_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Irr_A", 
            "#TRUST-1099-R-IRR-A", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Trust_Paper_Bypassed = localVariables.newGroupInRecord("pnd_Trust_Paper_Bypassed", "#TRUST-PAPER-BYPASSED");
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_P_B", "#TRUST-1099-R-P-B", 
            FieldType.NUMERIC, 10);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Gross_Amt_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Gross_Amt_P_B", 
            "#TRUST-1099-R-GROSS-AMT-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Taxable_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Taxable_P_B", 
            "#TRUST-1099-R-TAXABLE-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Ivc_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Ivc_P_B", 
            "#TRUST-1099-R-IVC-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Fed_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Fed_P_B", 
            "#TRUST-1099-R-FED-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Local_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Local_P_B", 
            "#TRUST-1099-R-LOCAL-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Int_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Int_P_B", 
            "#TRUST-1099-R-INT-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Irr_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Irr_P_B", 
            "#TRUST-1099-R-IRR-P-B", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Tiaa_Trust_1099_R_Total = localVariables.newGroupInRecord("pnd_Tiaa_Trust_1099_R_Total", "#TIAA-TRUST-1099-R-TOTAL");
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R", 
            "#TIAA-TRUST-1099-R", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt", 
            "#TIAA-TRUST-1099-R-GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable", 
            "#TIAA-TRUST-1099-R-TAXABLE", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed", 
            "#TIAA-TRUST-1099-R-FED", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State", 
            "#TIAA-TRUST-1099-R-STATE", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local", 
            "#TIAA-TRUST-1099-R-LOCAL", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc", 
            "#TIAA-TRUST-1099-R-IVC", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr", 
            "#TIAA-TRUST-1099-R-IRR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int", 
            "#TIAA-TRUST-1099-R-INT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_B", 
            "#TIAA-TRUST-1099-R-B", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_B", 
            "#TIAA-TRUST-1099-R-GROSS-AMT-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_B", 
            "#TIAA-TRUST-1099-R-TAXABLE-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_B", 
            "#TIAA-TRUST-1099-R-IVC-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_B", 
            "#TIAA-TRUST-1099-R-FED-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_B", 
            "#TIAA-TRUST-1099-R-LOCAL-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_B", 
            "#TIAA-TRUST-1099-R-INT-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_B", 
            "#TIAA-TRUST-1099-R-STATE-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_B", 
            "#TIAA-TRUST-1099-R-IRR-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_A", 
            "#TIAA-TRUST-1099-R-A", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_A", 
            "#TIAA-TRUST-1099-R-GROSS-AMT-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_A", 
            "#TIAA-TRUST-1099-R-TAXABLE-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_A", 
            "#TIAA-TRUST-1099-R-IVC-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_A", 
            "#TIAA-TRUST-1099-R-FED-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_A", 
            "#TIAA-TRUST-1099-R-LOCAL-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_A", 
            "#TIAA-TRUST-1099-R-INT-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_A", 
            "#TIAA-TRUST-1099-R-STATE-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_A", 
            "#TIAA-TRUST-1099-R-IRR-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_P_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_P_B", 
            "#TIAA-TRUST-1099-R-P-B", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_P_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_P_B", 
            "#TIAA-TRUST-1099-R-GROSS-AMT-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_P_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_P_B", 
            "#TIAA-TRUST-1099-R-TAXABLE-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_P_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_P_B", 
            "#TIAA-TRUST-1099-R-IVC-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_P_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_P_B", 
            "#TIAA-TRUST-1099-R-FED-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_P_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_P_B", 
            "#TIAA-TRUST-1099-R-LOCAL-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_P_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_P_B", 
            "#TIAA-TRUST-1099-R-INT-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_P_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_P_B", 
            "#TIAA-TRUST-1099-R-IRR-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_P_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_P_A", 
            "#TIAA-TRUST-1099-R-P-A", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_P_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_P_A", 
            "#TIAA-TRUST-1099-R-GROSS-AMT-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_P_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_P_A", 
            "#TIAA-TRUST-1099-R-TAXABLE-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_P_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_P_A", 
            "#TIAA-TRUST-1099-R-IVC-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_P_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_P_A", 
            "#TIAA-TRUST-1099-R-FED-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_P_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_P_A", 
            "#TIAA-TRUST-1099-R-LOCAL-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_P_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_P_A", 
            "#TIAA-TRUST-1099-R-INT-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_P_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_P_A", 
            "#TIAA-TRUST-1099-R-STATE-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_P_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_P_A", 
            "#TIAA-TRUST-1099-R-IRR-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Unq_Tin = localVariables.newFieldInRecord("pnd_Total_Unq_Tin", "#TOTAL-UNQ-TIN", FieldType.NUMERIC, 8);
        pnd_Unq_Tin_Eb = localVariables.newFieldInRecord("pnd_Unq_Tin_Eb", "#UNQ-TIN-EB", FieldType.NUMERIC, 8);
        pnd_Unq_Tin_Ea = localVariables.newFieldInRecord("pnd_Unq_Tin_Ea", "#UNQ-TIN-EA", FieldType.NUMERIC, 8);
        pnd_Unq_Tin_Pb = localVariables.newFieldInRecord("pnd_Unq_Tin_Pb", "#UNQ-TIN-PB", FieldType.NUMERIC, 8);
        pnd_Unq_Tin_Pa = localVariables.newFieldInRecord("pnd_Unq_Tin_Pa", "#UNQ-TIN-PA", FieldType.NUMERIC, 8);
        pnd_Hold_Rpt_Tin = localVariables.newFieldInRecord("pnd_Hold_Rpt_Tin", "#HOLD-RPT-TIN", FieldType.STRING, 10);
        pls_Email_Address = WsIndependent.getInstance().newFieldInRecord("pls_Email_Address", "+EMAIL-ADDRESS", FieldType.STRING, 70);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_form.reset();
        vw_form_01.reset();
        vw_twrparti_Read.reset();
        vw_form_Upd.reset();

        ldaTwrl0600.initializeValues();
        ldaTwrl2001.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
        pnd_Ws_Pnd_Mobius_Limit.setInitialValue(30000);
        pnd_Ws_Pnd_Sys_Date.setInitialValue(Global.getDATX());
        pnd_Ws_Pnd_Sys_Time.setInitialValue(Global.getTIMX());
        pnd_Ws_Pnd_Read_Cnt_Suny.setInitialValue(0);
        pnd_Ws_Pnd_Read_Cnt_1099r.setInitialValue(0);
        pnd_Ws_Pnd_Form_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Part_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Empty_Form_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Cpm_Lookup_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Tax_Email_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_No_Tax_Email_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Email_Non_Email_Bypassed.setInitialValue(0);
        pnd_Ws_Pnd_Missing_Pin_Cnt_Suny.setInitialValue(0);
        pnd_Ws_Pnd_Missing_Pin_Cnt_1099r.setInitialValue(0);
        pnd_Ws_Pnd_Missing_Pin_Cnt_S_Good.setInitialValue(0);
        pnd_Ws_Pnd_Missing_Pin_Cnt_R_Good.setInitialValue(0);
        pnd_Ws_Pnd_Missing_Ssn_Cnt_R.setInitialValue(0);
        pnd_Ws_Pnd_Missing_Ssn_Cnt_S.setInitialValue(0);
        pnd_Ws_Pnd_Paper_Print_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Bypass_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_5498_Bypass_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Letr_Bypass_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Form_Pages_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Multi_Page_Form_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Total_Form_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Non_Suny_Part_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Suny_Det_Rec_Cnt.setInitialValue(0);
        et_Limit.setInitialValue(100);
        pnd_Contract_Hdr_Lit1.setInitialValue("Ltr Hdr");
        pnd_Ws_Form_Type.setInitialValue(" ");
        pnd_Read_Ctr.setInitialValue(0);
        pnd_Contract_Hdr_Lit.setInitialValue("Ltr Hdr");
        pnd_Additional_Forms.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp6892() throws Exception
    {
        super("Twrp6892");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        //*  SINHSN                                                                                                                                                       //Natural: FORMAT ( 0 ) LS = 80 PS = 60 ZP = ON
        //*  SINHSN                                                                                                                                                       //Natural: FORMAT ( 5 ) LS = 133 PS = 58 ZP = ON
        //*  SINHSN                                                                                                                                                       //Natural: FORMAT ( 2 ) LS = 132 PS = 100 ZP = OFF
        //*  SINHSN                                                                                                                                                       //Natural: FORMAT ( 3 ) LS = 132 PS = 60 ZP = ON
        //*  SINHSN                                                                                                                                                       //Natural: FORMAT ( 4 ) LS = 133 PS = 58 ZP = ON
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 80 ZP = ON
        //* **                                                                                                                                                            //Natural: FORMAT ( 6 ) LS = 132 PS = 80 ZP = ON
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 24T 'Tax Withholding and Reporting System' 68T 'Page:' *PAGE-NUMBER ( 05 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 24T 'Mass Mailing Form Bypassed Details' 68T 'Report: RPT2' // 'Form Type: 1099R with SUNY' //
        //*  WRITE(5) TITLE LEFT
        //*   #SYS-DATE (EM=MM/DD/YYYY) '-' #SYS-TIME (EM=HH:IIAP)
        //*   24T  'Tax Withholding and Reporting System'
        //*   68T  'Page:' *PAGE-NUMBER(05)(EM=ZZ,ZZ9) /
        //*   *INIT-USER '-' *PROGRAM
        //*   24T  'Mass Mailing Form Extract Details'
        //*   68T  'Report: RPT5' //
        //*   'Form Type: 1099R with SUNY' //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 3 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 24T 'Tax Withholding and Reporting System' 68T 'Page:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 29T 'Test Indicator Report' 68T 'Report: RPT3' / 29T 'Participant Mass Mailing' 'Form Type: 1099R with SUNY' //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 4 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 120T 'Page:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 55T 'Mobius Mass Migration' 120T 'Report: RPT4' / 51T 'Form file Update' 'Form Type: 1099R with SUNY' //
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT-PARMS
        sub_Process_Input_Parms();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-CONTROL-DATE
        sub_Get_Control_Date();
        if (condition(Global.isEscape())) {return;}
        pnd_Superde_12_Pnd_Sd12_Tax_Year.setValue(pnd_Ws_Pnd_Tax_Year);                                                                                                   //Natural: ASSIGN #SUPERDE-12.#SD12-TAX-YEAR := #TWRAFRMN.#TAX-YEAR := #WS.#TAX-YEAR
        pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);
        pnd_Superde_12_Pnd_Sd12_Form_Type.setValue(pnd_Ws_Pnd_Form_Type);                                                                                                 //Natural: ASSIGN #SUPERDE-12.#SD12-FORM-TYPE := #WS.#FORM-TYPE
        pnd_Superde_12_Pnd_Sd12_Active_Ind.setValue("A");                                                                                                                 //Natural: ASSIGN #SUPERDE-12.#SD12-ACTIVE-IND := 'A'
        pnd_Superde_12_Pnd_Sd12_Mobius_Ind.setValue("Y");                                                                                                                 //Natural: ASSIGN #SUPERDE-12.#SD12-MOBIUS-IND := 'Y'
        DbsUtil.callnat(Twrnfrmn.class , getCurrentProcessState(), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNFRMN' USING #TWRAFRMN.#INPUT-PARMS ( AD = O ) #TWRAFRMN.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM RESET-TWRL2001
        sub_Reset_Twrl2001();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-FORM-TYPE
        sub_Process_Form_Type();
        if (condition(Global.isEscape())) {return;}
        //* *
        //* *  CHECK RECORD LIMIT
        //* *
        if (condition(pnd_Ws_Pnd_Form_Cnt.equals(getZero())))                                                                                                             //Natural: IF #FORM-CNT = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Empty file. Terminating program",new TabSetting(77),"***");                              //Natural: WRITE ( 0 ) '***' 25T 'Empty file. Terminating program' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
        //*   /* SNEHA CHANGES STARTED - SINHSN
        getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,NEWLINE,NEWLINE,new TabSetting(12),"Tin Number",new TabSetting(24),"Participant Name",NEWLINE,new  //Natural: WRITE ( 3 ) NOTITLE NOHDR /// 12T 'Tin Number' 24T 'Participant Name' / 12T '-' ( 10 ) 24T '-' ( 16 ) //
            TabSetting(12),"-",new RepeatItem(10),new TabSetting(24),"-",new RepeatItem(16),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,new TabSetting(2),"-",new RepeatItem(131),NEWLINE,new TabSetting(2),"Test Indicator Count:",pnd_Test_Ind_Cnt,          //Natural: WRITE ( 3 ) 2T '-' ( 131 ) / 2T 'Test Indicator Count:' #TEST-IND-CNT ( EM = ZZZZ,ZZZ,Z99 )
            new ReportEditMask ("ZZZZ,ZZZ,Z99"));
        if (Global.isEscape()) return;
        //*  SINHSN
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(26),"***** END OF REPORT *****");                                                       //Natural: WRITE ( 3 ) // 26T '***** END OF REPORT *****'
        if (Global.isEscape()) return;
        //*   /* SNEHA CHANGES ENDED - SINHSN
        //* ************************
        //*  S U B R O U T I N E S *
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-FORM-TYPE
        //* *
        //* *  CALL TAX PRINT MODULE
        //* *
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-SUNY-FORM-RECORD
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUNY-REPORT-FILE
        //* **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PAPER-PRINT-HOLD-REPORT
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CONTROL-RPT
        //* **********************************
        //*   GET DUPLICATE COUNTS
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INPUT-PARMS
        //* ************************************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTROL-DATE
        //* ********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PROCESS-FORM-TYPE-1099R
        //*  IF AN ACTIVE 1099R FORM IS PRESENT FOR THE SUNY PARTICIPANT,THEN
        //*  SET THE FLAG AND PROCESS BOTH 1099R AND SUNY FORMS.
        //* *
        //* ** WHEN FORM-01.TIRF-PIN = 9999999
        //* *
        //* * REPORT CASES WITH ADDITIONAL PAGES
        //* *
        //* *  CALL TAX PRINT MODULE
        //* *
        //* *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-1099R-FORM-RECORD
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-1099R-REPORT-FILE
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-TWRL2001
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STATE-FIELDS
        //* *****************************
        //* **********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STATE-REPORT-FIELD-MOVE
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PAPER-PRINT-HOLD-REPORT-FORM-01
        //*   /* SNEHA CHANGES STARTED - SINHSN
        //* ************************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-SUNY-FORM-UPDATE
        //* ************************************
        //*  ADD 1 TO #READ-CNT-SUNY
        getReports().write(4, NEWLINE,NEWLINE,NEWLINE,"Form...........: SUNY",NEWLINE,"Records Read...:",pnd_Ws_Pnd_Read_Cnt_Suny, new ReportEditMask                     //Natural: WRITE ( 4 ) // / 'Form...........: SUNY' / 'Records Read...:' #READ-CNT-SUNY / 'Records Updated:' #UPDATE-CNT-SUNY
            ("-Z,ZZZ,ZZ9"),NEWLINE,"Records Updated:",pnd_Ws_Pnd_Update_Cnt_Suny, new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(4, NEWLINE,NEWLINE,new TabSetting(26),"***** END OF REPORT *****");                                                                            //Natural: WRITE ( 4 ) // 26T '***** END OF REPORT *****'
        if (Global.isEscape()) return;
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-1099R-FORM-UPDATE
        //* ***************************************
        //*  ADD 1 TO #READ-CNT-01
        getReports().write(4, NEWLINE,NEWLINE,NEWLINE,"Form...........: 1099R",NEWLINE,"Records Read...:",pnd_Ws_Pnd_Read_Cnt_1099r, new ReportEditMask                   //Natural: WRITE ( 4 ) // / 'Form...........: 1099R' / 'Records Read...:' #READ-CNT-1099R / 'Records Updated:' #UPDATE-CNT-1099R
            ("-Z,ZZZ,ZZ9"),NEWLINE,"Records Updated:",pnd_Ws_Pnd_Update_Cnt_1099r, new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(4, NEWLINE,NEWLINE,new TabSetting(26),"***** END OF REPORT *****");                                                                            //Natural: WRITE ( 4 ) // 26T '***** END OF REPORT *****'
        if (Global.isEscape()) return;
        //* *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-EXTRACT-CONTROL-REPORT
        //* ************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-EXTRACT-CONTROL-REPORT-01
        //* ************************************************
        //* *********************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TIAA
        //* **********************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRUST
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DB-READ-PROCESS-TIAA
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BYPASSED-PROCESS-TIAA
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCEPTED-PROCESS-TIAA
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DB-READ-PROCESS-TRUST
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BYPASSED-PROCESS-TRUST
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCEPTED-PROCESS-TRUST
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DB-READ-PROCESS
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BYPASSED-PROCESS
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCEPTED-PROCESS
        //* ***********************************************************************
        //*                     1099R REPORT GENERATION                           *
        //* ***********************************************************************
                                                                                                                                                                          //Natural: PERFORM TITLE-REPORT2
        sub_Title_Report2();
        if (condition(Global.isEscape())) {return;}
        getReports().write(6, ReportOption.NOTITLE,NEWLINE,NEWLINE,new ColumnSpacing(51),"     COMPANY :  TIAA",NEWLINE,NEWLINE,NEWLINE,new TabSetting(19),"FORM COUNT",new  //Natural: WRITE ( 6 ) NOTITLE // 51X '     COMPANY :  TIAA' /// 19T 'FORM COUNT' 34T '  GROSS AMOUNT' 53T ' TAXABLE AMOUNT' 77T ' IVC AMOUNT' 94T '    FEDERAL TAX' 117T '    LOCAL TAX ' / 77T ' INT AMOUNT' 94T '    STATE TAX' 117T '    IRR AMT   ' / '-' ( 131 ) //
            TabSetting(34),"  GROSS AMOUNT",new TabSetting(53)," TAXABLE AMOUNT",new TabSetting(77)," IVC AMOUNT",new TabSetting(94),"    FEDERAL TAX",new 
            TabSetting(117),"    LOCAL TAX ",NEWLINE,new TabSetting(77)," INT AMOUNT",new TabSetting(94),"    STATE TAX",new TabSetting(117),"    IRR AMT   ",NEWLINE,"-",new 
            RepeatItem(131),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(6, ReportOption.NOTITLE,"1099-R",NEWLINE,NEWLINE,"DATABASE READ",new TabSetting(18),pnd_Tiaa_Totals_Pnd_Tiaa_1099_R, new ReportEditMask        //Natural: WRITE ( 6 ) NOTITLE '1099-R' // 'DATABASE READ' 18T #TIAA-1099-R ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-1099-R-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-1099-R-TAXABLE ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-1099-R-IVC ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-1099-R-FED ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-1099-R-LOCAL ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-1099-R-INT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-1099-R-STATE ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-1099-R-IRR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 18T #TIAA-1099-R-B ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-1099-R-GROSS-AMT-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-1099-R-TAXABLE-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-1099-R-IVC-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-1099-R-FED-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-1099-R-LOCAL-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-1099-R-INT-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-1099-R-STATE-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-1099-R-IRR-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 18T #TIAA-1099-R-A ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-1099-R-GROSS-AMT-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-1099-R-TAXABLE-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-1099-R-IVC-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-1099-R-FED-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-1099-R-LOCAL-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-1099-R-INT-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-1099-R-STATE-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-1099-R-IRR-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / '-' ( 131 ) / //
            ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Taxable, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Ivc, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(91),pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Fed, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Local, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Int, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(91),pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_State, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Irr, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new TabSetting(18),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_B, new ReportEditMask 
            ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Gross_Amt_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Taxable_B, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Ivc_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(91),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Fed_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Local_B, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Int_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(91),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_State_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Irr_B, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new TabSetting(18),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_A, new ReportEditMask 
            ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Gross_Amt_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Taxable_A, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Ivc_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(91),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Fed_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Local_A, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Int_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(91),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_State_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Irr_A, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"-",new RepeatItem(131),NEWLINE,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(6, ReportOption.NOTITLE,NEWLINE,NEWLINE,new ColumnSpacing(51),"     COMPANY :  TRUST",NEWLINE,NEWLINE);                                        //Natural: WRITE ( 6 ) NOTITLE //51X '     COMPANY :  TRUST' //
        if (Global.isEscape()) return;
        getReports().write(6, ReportOption.NOTITLE,"1099-R",NEWLINE,NEWLINE,"DATABASE READ",new TabSetting(18),pnd_Trust_Totals_Pnd_Trust_1099_R, new                     //Natural: WRITE ( 6 ) NOTITLE '1099-R' // 'DATABASE READ' 18T #TRUST-1099-R ( EM = ZZZ,ZZZ,Z99 ) 30T #TRUST-1099-R-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TRUST-1099-R-TAXABLE ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRUST-1099-R-IVC ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TRUST-1099-R-FED ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-1099-R-LOCAL ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TRUST-1099-R-INT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TRUST-1099-R-STATE ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-1099-R-IRR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 18T #TRUST-1099-R-B ( EM = ZZZ,ZZZ,Z99 ) 30T #TRUST-1099-R-GROSS-AMT-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TRUST-1099-R-TAXABLE-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRUST-1099-R-IVC-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TRUST-1099-R-FED-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-1099-R-LOCAL-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TRUST-1099-R-INT-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TRUST-1099-R-STATE-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-1099-R-IRR-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 18T #TRUST-1099-R-A ( EM = ZZZ,ZZZ,Z99 ) 30T #TRUST-1099-R-GROSS-AMT-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TRUST-1099-R-TAXABLE-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRUST-1099-R-IVC-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TRUST-1099-R-FED-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-1099-R-LOCAL-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TRUST-1099-R-INT-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / //
            ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Trust_Totals_Pnd_Trust_1099_R_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(50),pnd_Trust_Totals_Pnd_Trust_1099_R_Taxable, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Trust_Totals_Pnd_Trust_1099_R_Ivc, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Trust_Totals_Pnd_Trust_1099_R_Fed, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(112),pnd_Trust_Totals_Pnd_Trust_1099_R_Local, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Trust_Totals_Pnd_Trust_1099_R_Int, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Trust_Totals_Pnd_Trust_1099_R_State, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(112),pnd_Trust_Totals_Pnd_Trust_1099_R_Irr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new TabSetting(18),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_B, 
            new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Gross_Amt_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(50),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Taxable_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Ivc_B, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Fed_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(112),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Local_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Int_B, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_State_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(112),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Irr_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new TabSetting(18),pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_A, 
            new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Gross_Amt_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(50),pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Taxable_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Ivc_A, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Fed_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(112),pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Local_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Int_A, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*  CALCULATION FOR TIAA & TRUST TOTALS FOR 1099-R
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R), pnd_Tiaa_Totals_Pnd_Tiaa_1099_R.add(pnd_Trust_Totals_Pnd_Trust_1099_R)); //Natural: ASSIGN #TIAA-TRUST-1099-R := #TIAA-1099-R + #TRUST-1099-R
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt),    //Natural: ASSIGN #TIAA-TRUST-1099-R-GROSS-AMT := #TIAA-1099-R-GROSS-AMT + #TRUST-1099-R-GROSS-AMT
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Gross_Amt.add(pnd_Trust_Totals_Pnd_Trust_1099_R_Gross_Amt));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable),        //Natural: ASSIGN #TIAA-TRUST-1099-R-TAXABLE := #TIAA-1099-R-TAXABLE + #TRUST-1099-R-TAXABLE
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Taxable.add(pnd_Trust_Totals_Pnd_Trust_1099_R_Taxable));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed),                //Natural: ASSIGN #TIAA-TRUST-1099-R-FED := #TIAA-1099-R-FED + #TRUST-1099-R-FED
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Fed.add(pnd_Trust_Totals_Pnd_Trust_1099_R_Fed));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State),            //Natural: ASSIGN #TIAA-TRUST-1099-R-STATE := #TIAA-1099-R-STATE + #TRUST-1099-R-STATE
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_State.add(pnd_Trust_Totals_Pnd_Trust_1099_R_State));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local),            //Natural: ASSIGN #TIAA-TRUST-1099-R-LOCAL := #TIAA-1099-R-LOCAL + #TRUST-1099-R-LOCAL
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Local.add(pnd_Trust_Totals_Pnd_Trust_1099_R_Local));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc),                //Natural: ASSIGN #TIAA-TRUST-1099-R-IVC := #TIAA-1099-R-IVC + #TRUST-1099-R-IVC
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Ivc.add(pnd_Trust_Totals_Pnd_Trust_1099_R_Ivc));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr),                //Natural: ASSIGN #TIAA-TRUST-1099-R-IRR := #TIAA-1099-R-IRR + #TRUST-1099-R-IRR
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Irr.add(pnd_Trust_Totals_Pnd_Trust_1099_R_Irr));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int),                //Natural: ASSIGN #TIAA-TRUST-1099-R-INT := #TIAA-1099-R-INT + #TRUST-1099-R-INT
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Int.add(pnd_Trust_Totals_Pnd_Trust_1099_R_Int));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_B),                    //Natural: ASSIGN #TIAA-TRUST-1099-R-B := #TIAA-1099-R-B + #TRUST-1099-R-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_B));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_B),  //Natural: ASSIGN #TIAA-TRUST-1099-R-GROSS-AMT-B := #TIAA-1099-R-GROSS-AMT-B + #TRUST-1099-R-GROSS-AMT-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Gross_Amt_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Gross_Amt_B));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_B),    //Natural: ASSIGN #TIAA-TRUST-1099-R-TAXABLE-B := #TIAA-1099-R-TAXABLE-B + #TRUST-1099-R-TAXABLE-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Taxable_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Taxable_B));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_B),            //Natural: ASSIGN #TIAA-TRUST-1099-R-IVC-B := #TIAA-1099-R-IVC-B + #TRUST-1099-R-IVC-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Ivc_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Ivc_B));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_B),            //Natural: ASSIGN #TIAA-TRUST-1099-R-FED-B := #TIAA-1099-R-FED-B + #TRUST-1099-R-FED-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Fed_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Fed_B));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_B),        //Natural: ASSIGN #TIAA-TRUST-1099-R-LOCAL-B := #TIAA-1099-R-LOCAL-B + #TRUST-1099-R-LOCAL-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Local_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Local_B));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_B),            //Natural: ASSIGN #TIAA-TRUST-1099-R-INT-B := #TIAA-1099-R-INT-B + #TRUST-1099-R-INT-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Int_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Int_B));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_B),        //Natural: ASSIGN #TIAA-TRUST-1099-R-STATE-B := #TIAA-1099-R-STATE-B + #TRUST-1099-R-STATE-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_State_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_State_B));
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Irr_B.nadd(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Irr_B);                                                              //Natural: ASSIGN #TIAA-1099-R-IRR-B := #TIAA-1099-R-IRR-B + #TRUST-1099-R-IRR-B
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_A),                    //Natural: ASSIGN #TIAA-TRUST-1099-R-A := #TIAA-1099-R-A + #TRUST-1099-R-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_A));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_A),  //Natural: ASSIGN #TIAA-TRUST-1099-R-GROSS-AMT-A := #TIAA-1099-R-GROSS-AMT-A + #TRUST-1099-R-GROSS-AMT-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Gross_Amt_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Gross_Amt_A));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_A),    //Natural: ASSIGN #TIAA-TRUST-1099-R-TAXABLE-A := #TIAA-1099-R-TAXABLE-A + #TRUST-1099-R-TAXABLE-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Taxable_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Taxable_A));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_A),            //Natural: ASSIGN #TIAA-TRUST-1099-R-IVC-A := #TIAA-1099-R-IVC-A + #TRUST-1099-R-IVC-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Ivc_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Ivc_A));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_A),            //Natural: ASSIGN #TIAA-TRUST-1099-R-FED-A := #TIAA-1099-R-FED-A + #TRUST-1099-R-FED-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Fed_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Fed_A));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_A),        //Natural: ASSIGN #TIAA-TRUST-1099-R-LOCAL-A := #TIAA-1099-R-LOCAL-A + #TRUST-1099-R-LOCAL-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Local_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Local_A));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_A),            //Natural: ASSIGN #TIAA-TRUST-1099-R-INT-A := #TIAA-1099-R-INT-A + #TRUST-1099-R-INT-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Int_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Int_A));
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_A),        //Natural: ASSIGN #TIAA-TRUST-1099-R-STATE-A := #TIAA-1099-R-STATE-A + #TRUST-1099-R-STATE-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_State_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_State_A));
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Irr_A.nadd(pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Irr_A);                                                              //Natural: ASSIGN #TIAA-1099-R-IRR-A := #TIAA-1099-R-IRR-A + #TRUST-1099-R-IRR-A
        //*  1099-R
                                                                                                                                                                          //Natural: PERFORM TITLE-REPORT2
        sub_Title_Report2();
        if (condition(Global.isEscape())) {return;}
        getReports().write(6, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(19),"FORM COUNT",new TabSetting(34),"  GROSS AMOUNT",new TabSetting(53)," TAXABLE AMOUNT",new  //Natural: WRITE ( 6 ) NOTITLE /// 19T 'FORM COUNT' 34T '  GROSS AMOUNT' 53T ' TAXABLE AMOUNT' 77T ' IVC AMOUNT' 94T '    FEDERAL TAX' 117T '    LOCAL TAX ' / 77T ' INT AMOUNT' 94T '    STATE TAX' 117T '    IRR AMT   ' / '-' ( 131 ) //
            TabSetting(77)," IVC AMOUNT",new TabSetting(94),"    FEDERAL TAX",new TabSetting(117),"    LOCAL TAX ",NEWLINE,new TabSetting(77)," INT AMOUNT",new 
            TabSetting(94),"    STATE TAX",new TabSetting(117),"    IRR AMT   ",NEWLINE,"-",new RepeatItem(131),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(6, ReportOption.NOTITLE,NEWLINE,NEWLINE,"GRAND TOTAL 1099-R",NEWLINE,NEWLINE,"TOTAL FORMS",new TabSetting(18),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R,  //Natural: WRITE ( 6 ) NOTITLE // 'GRAND TOTAL 1099-R' // 'TOTAL FORMS' 18T #TIAA-TRUST-1099-R ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-TRUST-1099-R-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-TRUST-1099-R-TAXABLE ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-TRUST-1099-R-IVC ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-TRUST-1099-R-FED ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-1099-R-LOCAL ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-TRUST-1099-R-INT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-TRUST-1099-R-STATE ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-1099-R-IRR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 18T #TIAA-TRUST-1099-R-B ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-TRUST-1099-R-GROSS-AMT-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-TRUST-1099-R-TAXABLE-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-TRUST-1099-R-IVC-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-TRUST-1099-R-FED-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-1099-R-LOCAL-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-TRUST-1099-R-INT-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-TRUST-1099-R-STATE-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-1099-R-IRR-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 18T #TIAA-TRUST-1099-R-A ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-TRUST-1099-R-GROSS-AMT-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-TRUST-1099-R-TAXABLE-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-TRUST-1099-R-IVC-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-TRUST-1099-R-FED-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-1099-R-LOCAL-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-TRUST-1099-R-INT-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-TRUST-1099-R-STATE-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-1099-R-IRR-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / '-' ( 131 ) / //
            new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(50),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(112),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(112),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new TabSetting(18),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_B, 
            new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(50),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_B, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(112),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_B, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(112),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new TabSetting(18),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_A, 
            new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(50),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_A, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(112),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_A, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(112),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"-",new RepeatItem(131),
            NEWLINE,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*  WRITE (2) //
        //*   / '   Missing PIN bypassed......:' #MISSING-PIN-CNT-SUNY
        //*   / '   Missing SSN for 1099R.....:' #MISSING-SSN-CNT-R
        //*   / '   Paper Print Hold 1099R....:' #PAPER-PRINT-CNT
        //*   / '   Empty Form................:' #EMPTY-FORM-CNT
        //*   // '     Missing PIN Processed...:' 5X #MISSING-PIN-CNT-S-GOOD
        //*   / '                                --------'
        //*  / 'Total Bypassed + Skipped.....:' #BYPASS-CNT
        //*   WRITE (5)
        //*   // 'Total Forms processed........:' #FORM-CNT
        //*  // 'Total Participant processed..:' #PART-CNT     /* SINHSN
        //*   // ' Extract file counts'
        //*   / '   1099R Forms...............:' #FORM-PAGES-CNT
        //*   / '    SUNY Forms...............:' #ADDITIONAL-FORMS
        //*   / '                                --------'
        //*   / 'Total Forms in Extract file..:' #TOTAL-FORM-CNT
        //*   // 26T '***** END OF REPORT *****'
        //* ***********************************************************************
        //*                      SUNY REPORT GENERATION                           *
        //* ***********************************************************************
                                                                                                                                                                          //Natural: PERFORM TITLE-REPORT1
        sub_Title_Report1();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(22),"NMBR OF",new TabSetting(34),"NMBR OF",new TabSetting(53),"  GROSS AMOUNT",new  //Natural: WRITE ( 01 ) NOTITLE /// 22T 'NMBR OF' 34T 'NMBR OF' 53T '  GROSS AMOUNT' 74T ' FEDERAL AMOUNT' 94T ' STATE TAX ' 112T '    LOCAL TAX  ' / 22T 'LETTERS' 34T 'DETAILS' // / 'SUNY' // 'DATABASE READ' 18T #LETTER-CNT-DB ( EM = ZZZ,ZZZ,Z99 ) 30T #DETL-CNT-DB ( EM = ZZZ,ZZZ,Z99 ) 50T #SUNY-GROSS-AMT-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #SUNY-FED-TAX-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #SUNY-SUMM-STATE-TAX-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 110T #SUNY-SUMM-LCL-TAX-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 18T #LETTER-CNT-B ( EM = ZZZ,ZZZ,Z99 ) 30T #DETL-CNT-B ( EM = ZZZ,ZZZ,Z99 ) 50T #SUNY-GROSS-AMT-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #SUNY-FED-TAX-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #SUNY-SUMM-STATE-TAX-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 110T #SUNY-SUMM-LCL-TAX-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 18T #LETTER-CNT-A ( EM = ZZZ,ZZZ,Z99 ) 30T #DETL-CNT-A ( EM = ZZZ,ZZZ,Z99 ) 50T #SUNY-GROSS-AMT-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #SUNY-FED-TAX-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #SUNY-SUMM-STATE-TAX-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 110T #SUNY-SUMM-LCL-TAX-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) /
            TabSetting(74)," FEDERAL AMOUNT",new TabSetting(94)," STATE TAX ",new TabSetting(112),"    LOCAL TAX  ",NEWLINE,new TabSetting(22),"LETTERS",new 
            TabSetting(34),"DETAILS",NEWLINE,NEWLINE,NEWLINE,"SUNY",NEWLINE,NEWLINE,"DATABASE READ",new TabSetting(18),pnd_Db_Read_Fields_Pnd_Letter_Cnt_Db, 
            new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Db_Read_Fields_Pnd_Detl_Cnt_Db, new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(50),pnd_Db_Read_Fields_Pnd_Suny_Gross_Amt_Db, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Db_Read_Fields_Pnd_Suny_Fed_Tax_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(91),pnd_Db_Read_Fields_Pnd_Suny_Summ_State_Tax_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_Db_Read_Fields_Pnd_Suny_Summ_Lcl_Tax_Db, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new TabSetting(18),pnd_E_Del_Bypassed_Fields_Pnd_Letter_Cnt_B, new ReportEditMask 
            ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_E_Del_Bypassed_Fields_Pnd_Detl_Cnt_B, new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(50),pnd_E_Del_Bypassed_Fields_Pnd_Suny_Gross_Amt_B, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_E_Del_Bypassed_Fields_Pnd_Suny_Fed_Tax_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(91),pnd_E_Del_Bypassed_Fields_Pnd_Suny_Summ_State_Tax_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_E_Del_Bypassed_Fields_Pnd_Suny_Summ_Lcl_Tax_B, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new TabSetting(18),pnd_E_Del_Accepted_Fields_Pnd_Letter_Cnt_A, new ReportEditMask 
            ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_E_Del_Accepted_Fields_Pnd_Detl_Cnt_A, new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(50),pnd_E_Del_Accepted_Fields_Pnd_Suny_Gross_Amt_A, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_E_Del_Accepted_Fields_Pnd_Suny_Fed_Tax_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(91),pnd_E_Del_Accepted_Fields_Pnd_Suny_Summ_State_Tax_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_E_Del_Accepted_Fields_Pnd_Suny_Summ_Lcl_Tax_A, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE);
        if (Global.isEscape()) return;
        //*  -------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TITLE-REPORT1
        //*  -------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TITLE-REPORT2
        //* ********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TEST-INDICATOR-REPORT
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TEST-IND-REPORT
        //*   /* SNEHA CHANGES ENDED - SINHSN
    }
    private void sub_Process_Form_Type() throws Exception                                                                                                                 //Natural: PROCESS-FORM-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        getReports().write(0, ReportOption.NOTITLE,"Reading form file from:",pnd_Superde_12,NEWLINE,"Tax Year..............:",pnd_Superde_12_Pnd_Sd12_Tax_Year,           //Natural: WRITE 'Reading form file from:' #SUPERDE-12 / 'Tax Year..............:' #SUPERDE-12.#SD12-TAX-YEAR / 'Form Type.............:' #SUPERDE-12.#SD12-FORM-TYPE / 'Active Indicator......:' #SUPERDE-12.#SD12-ACTIVE-IND / 'Mobius Indicator......:' #SUPERDE-12.#SD12-MOBIUS-IND
            NEWLINE,"Form Type.............:",pnd_Superde_12_Pnd_Sd12_Form_Type,NEWLINE,"Active Indicator......:",pnd_Superde_12_Pnd_Sd12_Active_Ind,NEWLINE,
            "Mobius Indicator......:",pnd_Superde_12_Pnd_Sd12_Mobius_Ind);
        if (Global.isEscape()) return;
        //*  / 'Tin...................:' #SUPERDE-12.#SD12-TIN
        //*  IF #RESTART-TEXT NE ' '
        //*   WRITE ///
        setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Superde_12,9,1);                                                                                                  //Natural: MOVE LOW-VALUES TO SUBSTR ( #SUPERDE-12,9,1 )
        pnd_Superde_12_End.setValue(pnd_Superde_12);                                                                                                                      //Natural: ASSIGN #SUPERDE-12-END := #SUPERDE-12
        setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Superde_12_End,9,1);                                                                                             //Natural: MOVE HIGH-VALUES TO SUBSTR ( #SUPERDE-12-END,9,1 )
        vw_form.startDatabaseRead                                                                                                                                         //Natural: READ FORM BY TIRF-SUPERDE-12 = #SUPERDE-12 THRU #SUPERDE-12-END
        (
        "READ_FORM",
        new Wc[] { new Wc("TIRF_SUPERDE_12", ">=", pnd_Superde_12, "And", WcType.BY) ,
        new Wc("TIRF_SUPERDE_12", "<=", pnd_Superde_12_End, WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_12", "ASC") }
        );
        READ_FORM:
        while (condition(vw_form.readNextRow("READ_FORM")))
        {
            pnd_Ws_Pnd_Read_Cnt_Suny.nadd(1);                                                                                                                             //Natural: ADD 1 TO #READ-CNT-SUNY
            //* *
            //* *
            //* * PROCESSING THE SUNY PARTICIPANTS ONLY WHEN THE REGULAR 1099R FORMS
            //* * ARE AVAILABLE FOR THE SAME PARTICIPANT
            //* *
            if (condition(form_Tirf_Tin.notEquals(pnd_Tin_On_Hold)))                                                                                                      //Natural: IF FORM.TIRF-TIN NE #TIN-ON-HOLD
            {
                pnd_Tin_On_Hold.setValue(form_Tirf_Tin);                                                                                                                  //Natural: ASSIGN #TIN-ON-HOLD := FORM.TIRF-TIN
                pnd_Form_Type_10_01_Found.reset();                                                                                                                        //Natural: RESET #FORM-TYPE-10-01-FOUND
                pnd_First_Time_Form_Process.setValue(true);                                                                                                               //Natural: ASSIGN #FIRST-TIME-FORM-PROCESS := TRUE
                                                                                                                                                                          //Natural: PERFORM CHECK-PROCESS-FORM-TYPE-1099R
                sub_Check_Process_Form_Type_1099r();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_First_Time_Form_Process.reset();                                                                                                                      //Natural: RESET #FIRST-TIME-FORM-PROCESS
            }                                                                                                                                                             //Natural: END-IF
            //*  1099R NOT FOUND FOR SUNY PARTICIPANT
            if (condition(! (pnd_Form_Type_10_01_Found.getBoolean())))                                                                                                    //Natural: IF NOT #FORM-TYPE-10-01-FOUND
            {
                pnd_Ws_Pnd_Non_Suny_Part_Cnt.nadd(1);                                                                                                                     //Natural: ADD 1 TO #NON-SUNY-PART-CNT
                //*  SO SKIPPING SUNY PROCESSING
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-IF
            //*  DO NOT BYPASS PREVIOUSLY REPORTED EMPTY FORMS.
            short decideConditionsMet1013 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN FORM.TIRF-EMPTY-FORM AND FORM.TIRF-FORM-TYPE = 10
            if (condition(form_Tirf_Empty_Form.getBoolean() && form_Tirf_Form_Type.equals(10)))
            {
                decideConditionsMet1013++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN FORM.TIRF-EMPTY-FORM AND NOT FORM.TIRF-PREV-RPT-IND
            else if (condition(form_Tirf_Empty_Form.getBoolean() && ! (form_Tirf_Prev_Rpt_Ind.getBoolean())))
            {
                decideConditionsMet1013++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet1013 > 0))
            {
                //*      ADD  1                   TO #EMPTY-FORM-CNT
                pnd_Ws_Pnd_Mobius_Status_S.reset();                                                                                                                       //Natural: RESET #MOBIUS-STATUS-S
                pnd_Ws_Pnd_Message_S.setValue("Previously reported empty form for SUNY");                                                                                 //Natural: ASSIGN #WS.#MESSAGE-S := 'Previously reported empty form for SUNY'
                                                                                                                                                                          //Natural: PERFORM PROCESS-SUNY-FORM-RECORD
                sub_Process_Suny_Form_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //* *
            //* *  BYPASS CASES
            //* *
            //*  NOT BYPASSING IF PIN IS ZERO, STILL NEED TO GET IT PRINTED
            if (condition(form_Tirf_Pin.equals(getZero())))                                                                                                               //Natural: IF FORM.TIRF-PIN = 0
            {
                pnd_Ws_Pnd_Missing_Pin_Cnt_S_Good.nadd(1);                                                                                                                //Natural: ADD 1 TO #MISSING-PIN-CNT-S-GOOD
            }                                                                                                                                                             //Natural: END-IF
            //*  JW0609 PIN EXP
            short decideConditionsMet1039 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN FORM.TIRF-PIN = 999999999999
            if (condition(form_Tirf_Pin.equals(new DbsDecimal("999999999999"))))
            {
                decideConditionsMet1039++;
                pnd_Ws_Pnd_Mobius_Status_S.setValue("B1");                                                                                                                //Natural: ASSIGN #MOBIUS-STATUS-S := 'B1'
                pnd_Ws_Pnd_Message_S.setValue("Missing PIN for SUNY");                                                                                                    //Natural: ASSIGN #WS.#MESSAGE-S := 'Missing PIN for SUNY'
                pnd_Ws_Pnd_Missing_Pin_Cnt_Suny.nadd(1);                                                                                                                  //Natural: ADD 1 TO #MISSING-PIN-CNT-SUNY
            }                                                                                                                                                             //Natural: WHEN FORM.TIRF-TAX-ID-TYPE = '5'
            else if (condition(form_Tirf_Tax_Id_Type.equals("5")))
            {
                decideConditionsMet1039++;
                pnd_Ws_Pnd_Mobius_Status_S.setValue("B1");                                                                                                                //Natural: ASSIGN #MOBIUS-STATUS-S := 'B1'
                pnd_Ws_Pnd_Message_S.setValue("Missing SSN for SUNY");                                                                                                    //Natural: ASSIGN #WS.#MESSAGE-S := 'Missing SSN for SUNY'
                pnd_Ws_Pnd_Missing_Ssn_Cnt_S.nadd(1);                                                                                                                     //Natural: ADD 1 TO #MISSING-SSN-CNT-S
            }                                                                                                                                                             //Natural: WHEN #NY-LTR-BYPASS
            else if (condition(pnd_Ny_Ltr_Bypass.getBoolean()))
            {
                decideConditionsMet1039++;
                pnd_Ws_Pnd_Mobius_Status_S.setValue("E");                                                                                                                 //Natural: ASSIGN #MOBIUS-STATUS-S := 'E'
                pnd_Ws_Pnd_Message_S.setValue("One of NY ltr error");                                                                                                     //Natural: ASSIGN #WS.#MESSAGE-S := 'One of NY ltr error'
            }                                                                                                                                                             //Natural: WHEN #1099R-BYPASSED
            else if (condition(pnd_1099r_Bypassed.getBoolean()))
            {
                decideConditionsMet1039++;
                pnd_Ws_Pnd_Mobius_Status_S.setValue(pnd_Ws_Pnd_Mobius_Status_01);                                                                                         //Natural: ASSIGN #MOBIUS-STATUS-S := #MOBIUS-STATUS-01
                pnd_Ws_Pnd_Message_S.setValue(pnd_Ws_Pnd_Message_01);                                                                                                     //Natural: ASSIGN #WS.#MESSAGE-S := #WS.#MESSAGE-01
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet1039 > 0))
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-PAPER-PRINT-HOLD-REPORT
                sub_Write_Paper_Print_Hold_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PROCESS-SUNY-FORM-RECORD
                sub_Process_Suny_Form_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ws_Pnd_Bypass_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #BYPASS-CNT
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //* *
            //* *  SUNY PARTICIPANT LEVEL REPORTING LOGIC (CALL TAX PRINT LOGIC ONCE
            //* *                                          PER TIN FOR SUNY.  TAX PRINT
            //* *                                          WILL FIND AND PRINT ALL SUNY
            //* *                                          FORMS ON ONE STATEMENT.)
            //* *
            if (condition(! (pnd_First_Time_Form_Process.getBoolean())))                                                                                                  //Natural: IF NOT #FIRST-TIME-FORM-PROCESS
            {
                pnd_Ws_Pnd_Message_S.setValue("Detail SUNY/CUNY contracts");                                                                                              //Natural: ASSIGN #WS.#MESSAGE-S := 'Detail SUNY/CUNY contracts'
                                                                                                                                                                          //Natural: PERFORM PROCESS-SUNY-FORM-RECORD
                sub_Process_Suny_Form_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ws_Pnd_Suny_Det_Rec_Cnt.nadd(1);                                                                                                                      //Natural: ADD 1 TO #SUNY-DET-REC-CNT
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  MOBIUS MASS MIGRATION
            }                                                                                                                                                             //Natural: END-IF
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Isn().setValue(vw_form.getAstISN("READ_FORM"));                                                            //Natural: ASSIGN #TWRA0214-ISN := *ISN ( READ-FORM. )
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Tax_Year().compute(new ComputeParameters(false, pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Tax_Year()),  //Natural: ASSIGN #TWRA0214-TAX-YEAR := VAL ( FORM.TIRF-TAX-YEAR )
                form_Tirf_Tax_Year.val());
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Tin().setValue(form_Tirf_Tin);                                                                             //Natural: ASSIGN #TWRA0214-TIN := FORM.TIRF-TIN
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Type().setValue(form_Tirf_Form_Type);                                                                 //Natural: ASSIGN #TWRA0214-FORM-TYPE := FORM.TIRF-FORM-TYPE
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind().setValue("M");                                                                           //Natural: ASSIGN #TWRA0214-MOBIUS-MIGR-IND := 'M'
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Key().setValue(" ");                                                                                //Natural: ASSIGN #TWRA0214-MOBIUS-KEY := ' '
            //*  SINHSN
            //*  SINHSN
            //*  SINHSN
            DbsUtil.callnat(Twrn0214.class , getCurrentProcessState(), pnd_Ws_Pnd_Form_Page_Cnt, pnd_Ws_Pnd_Zero_Isn, pdaTwra0214.getPnd_Twra0214_Link_Area(),            //Natural: CALLNAT 'TWRN0214' #FORM-PAGE-CNT #ZERO-ISN #TWRA0214-LINK-AREA PSTA9610 #LINK-LET-TYPE ( * ) #TEST-IND #COUPON-IND #LTR-ARRAY ( * ) #ADDITIONAL-FORMS
                pdaPsta9610.getPsta9610(), pnd_Ws_Pnd_Link_Let_Type.getValue("*"), pnd_Test_Ind, pnd_Coupon_Ind, pnd_Ltr_Array.getValue("*"), pnd_Additional_Forms);
            if (condition(Global.isEscape())) return;
            //*  ERROR OCCURED
            if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code().notEquals(getZero())))                                                            //Natural: IF #TWRA0214-RET-CODE NE 0
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Problem in Tax Print Module TWRN0214",new TabSetting(77),"***",NEWLINE,"***",new     //Natural: WRITE ( 0 ) '***' 25T 'Problem in Tax Print Module TWRN0214' 77T '***' / '***' 25T 'CODE :' #TWRA0214-RET-CODE 77T '***' / '***' 25T 'MSG  :' #TWRA0214-RET-MSG 77T '***'
                    TabSetting(25),"CODE :",pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code(),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"MSG  :",pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Msg(),new 
                    TabSetting(77),"***");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(102);  if (true) return;                                                                                                                //Natural: TERMINATE 102
                //*  MOBIUS MASS MIGRATION
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Mobius_Status_S.setValue("A1");                                                                                                                    //Natural: ASSIGN #MOBIUS-STATUS-S := 'A1'
            pnd_Ws_Pnd_Message_S.setValue("Normal Completion");                                                                                                           //Natural: ASSIGN #WS.#MESSAGE-S := 'Normal Completion'
                                                                                                                                                                          //Natural: PERFORM PROCESS-SUNY-FORM-RECORD
            sub_Process_Suny_Form_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ws_Pnd_Form_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #FORM-CNT
            //*  SINHSN
                                                                                                                                                                          //Natural: PERFORM WRITE-TEST-INDICATOR-REPORT
            sub_Write_Test_Indicator_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(26),"***** END OF REPORT *****");                                                   //Natural: WRITE ( 3 ) // 26T '***** END OF REPORT *****'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_Form_Cnt.equals(getZero()) && pnd_Ws_Pnd_Form_Type.equals(10)))                                                                          //Natural: IF #FORM-CNT = 0 AND #FORM-TYPE = 10
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTROL-RPT
            sub_Write_Control_Rpt();
            if (condition(Global.isEscape())) {return;}
            //*   WRITE (5) // 26T '***** END OF REPORT *****'
            getReports().write(2, NEWLINE,NEWLINE,new TabSetting(26),"***** END OF REPORT *****");                                                                        //Natural: WRITE ( 2 ) // 26T '***** END OF REPORT *****'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Suny_Form_Record() throws Exception                                                                                                          //Natural: PROCESS-SUNY-FORM-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        pnd_Contract_Txt.reset();                                                                                                                                         //Natural: RESET #CONTRACT-TXT
        if (condition(form_Tirf_Contract_Nbr.notEquals(" ")))                                                                                                             //Natural: IF FORM.TIRF-CONTRACT-NBR NE ' '
        {
            pnd_Contract_Txt.setValueEdited(form_Tirf_Contract_Nbr,new ReportEditMask("XXXXXXX'-'X"));                                                                    //Natural: MOVE EDITED FORM.TIRF-CONTRACT-NBR ( EM = XXXXXXX'-'X ) TO #CONTRACT-TXT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Contract_Txt.setValue(pnd_Contract_Hdr_Lit);                                                                                                              //Natural: ASSIGN #CONTRACT-TXT := #CONTRACT-HDR-LIT
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #MOBIUS-STATUS-S = 'A1'          /* ACCEPTED REPORT
        //*   DISPLAY(5)(HC=L)
        //*     'Tax/Year'       FORM.TIRF-TAX-YEAR
        //*     '/Form'          FORM.TIRF-FORM-TYPE     (EM=99)
        //*     '/Comp'          FORM.TIRF-COMPANY-CDE   (LC=�)
        //*     '/TIN'           FORM.TIRF-TIN
        //*     '/Contract     ' #CONTRACT-TXT
        //*     'Pay/ee   '      FORM.TIRF-PAYEE-CDE
        //*     'Form/Key '      FORM.TIRF-KEY
        //*     '/PIN  '         FORM.TIRF-PIN           (EM=999999999999)
        //*     'Mobius/Status'  #MOBIUS-STATUS-S        (LC=��)
        //*     '/Message'       #WS.#MESSAGE-S
        //*  END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-SUNY-REPORT-FILE
        sub_Write_Suny_Report_File();
        if (condition(Global.isEscape())) {return;}
        //*  SINHSN
                                                                                                                                                                          //Natural: PERFORM PROCESS-SUNY-FORM-UPDATE
        sub_Process_Suny_Form_Update();
        if (condition(Global.isEscape())) {return;}
        //*  SINHSN
                                                                                                                                                                          //Natural: PERFORM WRITE-EXTRACT-CONTROL-REPORT
        sub_Write_Extract_Control_Report();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Write_Suny_Report_File() throws Exception                                                                                                            //Natural: WRITE-SUNY-REPORT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
                                                                                                                                                                          //Natural: PERFORM RESET-TWRL2001
        sub_Reset_Twrl2001();
        if (condition(Global.isEscape())) {return;}
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().setValue(pnd_Ws_Pnd_Mobius_Status_S);                                                                  //Natural: MOVE #MOBIUS-STATUS-S TO #RPT-BYPASS-ACCPT-IND
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tax_Year().setValue(form_Tirf_Tax_Year);                                                                                  //Natural: MOVE FORM.TIRF-TAX-YEAR TO #RPT-TAX-YEAR
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().setValue(form_Tirf_Form_Type);                                                                                //Natural: MOVE FORM.TIRF-FORM-TYPE TO #RPT-FORM-TYPE
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Comp_Cde().setValue(form_Tirf_Company_Cde);                                                                               //Natural: MOVE FORM.TIRF-COMPANY-CDE TO #RPT-COMP-CDE
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin().setValue(form_Tirf_Tin);                                                                                            //Natural: MOVE FORM.TIRF-TIN TO #RPT-TIN
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Pin().setValue(form_Tirf_Pin);                                                                                            //Natural: MOVE FORM.TIRF-PIN TO #RPT-PIN
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Contract_Nbr().setValue(pnd_Contract_Txt);                                                                                //Natural: MOVE #CONTRACT-TXT TO #RPT-CONTRACT-NBR
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Payee_Cde().setValue(form_Tirf_Payee_Cde);                                                                                //Natural: MOVE FORM.TIRF-PAYEE-CDE TO #RPT-PAYEE-CDE
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Test_Ind().setValue(pnd_Test_Ind);                                                                                        //Natural: MOVE #TEST-IND TO #RPT-TEST-IND
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Part_Name().setValue(form_Tirf_Participant_Name);                                                                         //Natural: MOVE FORM.TIRF-PARTICIPANT-NAME TO #RPT-PART-NAME
        if (condition(form_Tirf_Contract_Nbr.equals(" ")))                                                                                                                //Natural: IF FORM.TIRF-CONTRACT-NBR = ' '
        {
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Header_Ind().setValue("Y");                                                                                      //Natural: MOVE 'Y' TO #RPT-SUNY-HEADER-IND
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #M 1 C*FORM.TIRF-LTR-GRP
        for (pnd_M.setValue(1); condition(pnd_M.lessOrEqual(form_Count_Casttirf_Ltr_Grp)); pnd_M.nadd(1))
        {
            if (condition(form_Tirf_Ltr_Gross_Amt.getValue(pnd_M).equals(getZero()) || form_Tirf_Ltr_Per.getValue(pnd_M).equals(getZero())))                              //Natural: IF FORM.TIRF-LTR-GROSS-AMT ( #M ) = 0 OR FORM.TIRF-LTR-PER ( #M ) = 0
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Details_Ind().getValue(pnd_M).setValue("Y");                                                                     //Natural: MOVE 'Y' TO #RPT-SUNY-DETAILS-IND ( #M )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Gross_Amt().nadd(form_Tirf_Gross_Amt);                                                                               //Natural: ADD FORM.TIRF-GROSS-AMT TO #RPT-SUNY-GROSS-AMT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Fed_Tax().nadd(form_Tirf_Fed_Tax_Wthld);                                                                             //Natural: ADD FORM.TIRF-FED-TAX-WTHLD TO #RPT-SUNY-FED-TAX
        //*  WE DON't have TIRF-SUMM-STATE-TAX-WTHLD & TIRF-SUMM-LOCAL-TAX-WTHLD
        //*  IN THE TWRFRM-FORM-FILE. IT HAS BEEN REDEFINED UNDER TWRL9705 FROM
        //*  TIRF-TRAD-IRA-CONTRIB AND TIRF-ROTH-IRA-CONTRIB FIELDS.
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Summ_State_Tax().nadd(form_Tirf_Trad_Ira_Contrib);                                                                   //Natural: ADD FORM.TIRF-TRAD-IRA-CONTRIB TO #RPT-SUNY-SUMM-STATE-TAX
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Summ_Lcl_Tax().nadd(form_Tirf_Roth_Ira_Contrib);                                                                     //Natural: ADD FORM.TIRF-ROTH-IRA-CONTRIB TO #RPT-SUNY-SUMM-LCL-TAX
    }
    private void sub_Write_Paper_Print_Hold_Report() throws Exception                                                                                                     //Natural: WRITE-PAPER-PRINT-HOLD-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************
        pnd_Contract_Txt.reset();                                                                                                                                         //Natural: RESET #CONTRACT-TXT
        if (condition(form_Tirf_Contract_Nbr.notEquals(" ")))                                                                                                             //Natural: IF FORM.TIRF-CONTRACT-NBR NE ' '
        {
            pnd_Contract_Txt.setValueEdited(form_Tirf_Contract_Nbr,new ReportEditMask("XXXXXXX'-'X"));                                                                    //Natural: MOVE EDITED FORM.TIRF-CONTRACT-NBR ( EM = XXXXXXX'-'X ) TO #CONTRACT-TXT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Contract_Txt.setValue(pnd_Contract_Hdr_Lit);                                                                                                              //Natural: ASSIGN #CONTRACT-TXT := #CONTRACT-HDR-LIT
            //*  JW0609
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(2, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"Tax/Year",                                                            //Natural: DISPLAY ( 2 ) ( HC = L ) 'Tax/Year' FORM.TIRF-TAX-YEAR '/Form' FORM.TIRF-FORM-TYPE ( EM = 99 ) '/Comp' FORM.TIRF-COMPANY-CDE ( LC = � ) '/TIN' FORM.TIRF-TIN '/Contract     ' #CONTRACT-TXT 'Pay/ee   ' FORM.TIRF-PAYEE-CDE 'Form/Key ' FORM.TIRF-KEY '/PIN  ' FORM.TIRF-PIN ( EM = 999999999999 ) 'Mobius/Status' #MOBIUS-STATUS-S ( LC = �� ) '/Message' #WS.#MESSAGE-S
        		form_Tirf_Tax_Year,"/Form",
        		form_Tirf_Form_Type, new ReportEditMask ("99"),"/Comp",
        		form_Tirf_Company_Cde, new FieldAttributes("LC=�"),"/TIN",
        		form_Tirf_Tin,"/Contract     ",
        		pnd_Contract_Txt,"Pay/ee   ",
        		form_Tirf_Payee_Cde,"Form/Key ",
        		form_Tirf_Key,"/PIN  ",
        		form_Tirf_Pin, new ReportEditMask ("999999999999"),"Mobius/Status",
        		pnd_Ws_Pnd_Mobius_Status_S, new FieldAttributes("LC=��"),"/Message",
        		pnd_Ws_Pnd_Message_S);
        if (Global.isEscape()) return;
    }
    private void sub_Write_Control_Rpt() throws Exception                                                                                                                 //Natural: WRITE-CONTROL-RPT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_Form_Pages_Cnt.compute(new ComputeParameters(false, pnd_Ws_Pnd_Form_Pages_Cnt), pnd_Ws_Pnd_Form_Cnt.subtract(pnd_Additional_Forms));                   //Natural: ASSIGN #FORM-PAGES-CNT := #FORM-CNT - #ADDITIONAL-FORMS
        pnd_Ws_Pnd_Total_Form_Cnt.compute(new ComputeParameters(false, pnd_Ws_Pnd_Total_Form_Cnt), pnd_Ws_Pnd_Form_Pages_Cnt.add(pnd_Additional_Forms));                  //Natural: ASSIGN #TOTAL-FORM-CNT := #FORM-PAGES-CNT + #ADDITIONAL-FORMS
        pnd_Ws_Pnd_Bypass_Cnt.nadd(pnd_Ws_Pnd_Empty_Form_Cnt);                                                                                                            //Natural: ASSIGN #BYPASS-CNT := #BYPASS-CNT + #EMPTY-FORM-CNT
        //*  + #SUNY-DET-REC-CNT
        //*  + #NON-SUNY-PART-CNT
    }
    private void sub_Process_Input_Parms() throws Exception                                                                                                               //Natural: PROCESS-INPUT-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("TWRP6892|sub_Process_Input_Parms");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_Input);                                                                                     //Natural: INPUT #WS.#INPUT
                if (condition(! (pnd_Ws_Pnd_Form_Type.greaterOrEqual(1) && pnd_Ws_Pnd_Form_Type.lessOrEqual(10))))                                                        //Natural: IF NOT #WS.#FORM-TYPE = 1 THRU 10
                {
                    if (condition(pnd_Ws_Pnd_Form_Type.equals(8) || pnd_Ws_Pnd_Form_Type.equals(9)))                                                                      //Natural: IF #WS.#FORM-TYPE = 8 OR = 9
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                        sub_Error_Display_Start();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Missing or Invalid Form Type:",pnd_Ws_Pnd_Form_Type,new TabSetting(77),      //Natural: WRITE ( 0 ) '***' 25T 'Missing or Invalid Form Type:' #WS.#FORM-TYPE 77T '***'
                            "***");
                        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                        sub_Error_Display_End();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                        DbsUtil.terminate(100);  if (true) return;                                                                                                        //Natural: TERMINATE 100
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Ws_Pnd_Tax_Year.equals(getZero())))                                                                                                     //Natural: IF #WS.#TAX-YEAR = 0
                {
                    pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), Global.getDATN().divide(10000).subtract(1));                           //Natural: ASSIGN #WS.#TAX-YEAR := *DATN / 10000 - 1
                }                                                                                                                                                         //Natural: END-IF
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Get_Control_Date() throws Exception                                                                                                                  //Natural: GET-CONTROL-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getWorkFiles().read(3, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                             //Natural: READ WORK FILE 03 ONCE RECORD #TWRP0600-CONTROL-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Missing Control Record",new TabSetting(77),"***");                                       //Natural: WRITE ( 0 ) '***' 25T 'Missing Control Record' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(101);  if (true) return;                                                                                                                    //Natural: TERMINATE 101
        }                                                                                                                                                                 //Natural: END-ENDFILE
        pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Letter_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd()); //Natural: MOVE EDITED #TWRP0600-INTERFACE-CCYYMMDD TO #LETTER-DTE ( EM = YYYYMMDD )
    }
    private void sub_Check_Process_Form_Type_1099r() throws Exception                                                                                                     //Natural: CHECK-PROCESS-FORM-TYPE-1099R
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************
        pnd_Ny_Ltr_Bypass.reset();                                                                                                                                        //Natural: RESET #NY-LTR-BYPASS #1099R-BYPASSED #LTR-ARRAY ( * ) #I #MOBIUS-STATUS-01 #WS.#MESSAGE-01 #NY-LTR-VALUE-I
        pnd_1099r_Bypassed.reset();
        pnd_Ltr_Array.getValue("*").reset();
        pnd_I.reset();
        pnd_Ws_Pnd_Mobius_Status_01.reset();
        pnd_Ws_Pnd_Message_01.reset();
        pnd_Ny_Ltr_Value_I.reset();
        pnd_Superde_12_Form_01_Pnd_Sd12_Tax_Year.setValue(pnd_Ws_Pnd_Tax_Year);                                                                                           //Natural: ASSIGN #SUPERDE-12-FORM-01.#SD12-TAX-YEAR := #WS.#TAX-YEAR
        pnd_Superde_12_Form_01_Pnd_Sd12_Form_Type.setValue(1);                                                                                                            //Natural: ASSIGN #SUPERDE-12-FORM-01.#SD12-FORM-TYPE := 01
        pnd_Superde_12_Form_01_Pnd_Sd12_Active_Ind.setValue("A");                                                                                                         //Natural: ASSIGN #SUPERDE-12-FORM-01.#SD12-ACTIVE-IND := 'A'
        pnd_Superde_12_Form_01_Pnd_Sd12_Mobius_Ind.setValue("Y");                                                                                                         //Natural: ASSIGN #SUPERDE-12-FORM-01.#SD12-MOBIUS-IND := 'Y'
        pnd_Superde_12_Form_01_Pnd_Sd12_Tin.setValue(form_Tirf_Tin);                                                                                                      //Natural: ASSIGN #SUPERDE-12-FORM-01.#SD12-TIN := FORM.TIRF-TIN
        setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Superde_12_Form_01,19,1);                                                                                         //Natural: MOVE LOW-VALUES TO SUBSTR ( #SUPERDE-12-FORM-01,19,1 )
        pnd_Superde_12_Form_01_End.setValue(pnd_Superde_12_Form_01);                                                                                                      //Natural: ASSIGN #SUPERDE-12-FORM-01-END := #SUPERDE-12-FORM-01
        setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Superde_12_Form_01_End,19,1);                                                                                    //Natural: MOVE HIGH-VALUES TO SUBSTR ( #SUPERDE-12-FORM-01-END,19,1 )
        //*  READ FORM FILE WITH SUPERDE-12 FOR 1099R, IF PRESENT THEN PROCESS OR
        //*  ELSE SKIP THE PROCESSING AND DO NOT PROCESS SUNY FORMS AS WELL.
        vw_form_01.startDatabaseRead                                                                                                                                      //Natural: READ FORM-01 BY TIRF-SUPERDE-12 = #SUPERDE-12-FORM-01 THRU #SUPERDE-12-FORM-01-END
        (
        "READ_FORM_01",
        new Wc[] { new Wc("TIRF_SUPERDE_12", ">=", pnd_Superde_12_Form_01, "And", WcType.BY) ,
        new Wc("TIRF_SUPERDE_12", "<=", pnd_Superde_12_Form_01_End, WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_12", "ASC") }
        );
        READ_FORM_01:
        while (condition(vw_form_01.readNextRow("READ_FORM_01")))
        {
            pnd_Ws_Pnd_Read_Cnt_1099r.nadd(1);                                                                                                                            //Natural: ADD 1 TO #READ-CNT-1099R
            //*  SET THE FLAG, IF TIRF-LTR-DATA = 'E' OR FOR ALL 1099-R,
            //*  TIRF-LTR-DATA = 'N' OR 'P' TO BYPASS THE SUNY PROCESSING.
            if (condition(form_01_Tirf_Ltr_Data.equals("E")))                                                                                                             //Natural: IF FORM-01.TIRF-LTR-DATA = 'E'
            {
                pnd_Ny_Ltr_Bypass.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #NY-LTR-BYPASS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(form_01_Tirf_Ltr_Data.equals("Y")))                                                                                                         //Natural: IF FORM-01.TIRF-LTR-DATA = 'Y'
                {
                    pnd_Ny_Ltr_Value_I.setValue(true);                                                                                                                    //Natural: MOVE TRUE TO #NY-LTR-VALUE-I
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  LOAD ALL 1099-R FORMS INTO THE ARRAY TO CHECK IF THE FORMS ARE
            //*  EXCLUDED OR PARTIALLY INCLUDED.
            if (condition(form_01_Tirf_Ltr_Data.equals("Y")))                                                                                                             //Natural: IF FORM-01.TIRF-LTR-DATA = 'Y'
            {
                pnd_I.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #I
                pnd_Ltr_Array_Pnd_Ltr_Tin.getValue(pnd_I).setValue(form_01_Tirf_Tin);                                                                                     //Natural: ASSIGN #LTR-ARRAY.#LTR-TIN ( #I ) := FORM-01.TIRF-TIN
                pnd_Ltr_Array_Pnd_Ltr_Cntrct.getValue(pnd_I).setValueEdited(form_01_Tirf_Contract_Nbr,new ReportEditMask("XXXXXXX'-'X"));                                 //Natural: MOVE EDITED FORM-01.TIRF-CONTRACT-NBR ( EM = XXXXXXX'-'X ) TO #LTR-ARRAY.#LTR-CNTRCT ( #I )
                pnd_Ltr_Array_Pnd_Ltr_Data.getValue(pnd_I).setValue(form_01_Tirf_Ltr_Data);                                                                               //Natural: ASSIGN #LTR-ARRAY.#LTR-DATA ( #I ) := FORM-01.TIRF-LTR-DATA
            }                                                                                                                                                             //Natural: END-IF
            pnd_Form_Type_10_01_Found.setValue(true);                                                                                                                     //Natural: ASSIGN #FORM-TYPE-10-01-FOUND := TRUE
            //*  DO NOT BYPASS PREVIOUSLY REPORTED EMPTY FORMS.
            short decideConditionsMet1265 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN FORM-01.TIRF-EMPTY-FORM AND NOT FORM-01.TIRF-PREV-RPT-IND
            if (condition(form_01_Tirf_Empty_Form.getBoolean() && ! (form_01_Tirf_Prev_Rpt_Ind.getBoolean())))
            {
                decideConditionsMet1265++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet1265 > 0))
            {
                pnd_Ws_Pnd_Empty_Form_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #EMPTY-FORM-CNT
                pnd_Ws_Pnd_Mobius_Status.reset();                                                                                                                         //Natural: RESET #MOBIUS-STATUS
                pnd_Ws_Pnd_Message.setValue("Previously reported Empty Form for 1099-R");                                                                                 //Natural: ASSIGN #WS.#MESSAGE := 'Previously reported Empty Form for 1099-R'
                //* *
                                                                                                                                                                          //Natural: PERFORM PROCESS-1099R-FORM-RECORD
                sub_Process_1099r_Form_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM_01"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM_01"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //* *
            //* *  BYPASS CASES
            //* *
            //* * NOT BYPASSING IF PIN IS ZERO, STILL NEED TO GET IT PRINTED
            if (condition(form_01_Tirf_Pin.equals(getZero())))                                                                                                            //Natural: IF FORM-01.TIRF-PIN = 0
            {
                pnd_Ws_Pnd_Missing_Pin_Cnt_R_Good.nadd(1);                                                                                                                //Natural: ADD 1 TO #MISSING-PIN-CNT-R-GOOD
            }                                                                                                                                                             //Natural: END-IF
            //*  JW0609
            short decideConditionsMet1287 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN FORM-01.TIRF-PIN = 999999999999
            if (condition(form_01_Tirf_Pin.equals(new DbsDecimal("999999999999"))))
            {
                decideConditionsMet1287++;
                pnd_Ws_Pnd_Mobius_Status.setValue("B2");                                                                                                                  //Natural: ASSIGN #MOBIUS-STATUS := 'B2'
                pnd_Ws_Pnd_Message.setValue("Missing PIN for 1099-R");                                                                                                    //Natural: ASSIGN #WS.#MESSAGE := 'Missing PIN for 1099-R'
                pnd_Ws_Pnd_Missing_Pin_Cnt_1099r.nadd(1);                                                                                                                 //Natural: ADD 1 TO #MISSING-PIN-CNT-1099R
            }                                                                                                                                                             //Natural: WHEN FORM-01.TIRF-TAX-ID-TYPE = '5'
            else if (condition(form_01_Tirf_Tax_Id_Type.equals("5")))
            {
                decideConditionsMet1287++;
                pnd_Ws_Pnd_Mobius_Status.setValue("B2");                                                                                                                  //Natural: ASSIGN #MOBIUS-STATUS := 'B2'
                pnd_Ws_Pnd_Message.setValue("Missing SSN for 1099-R");                                                                                                    //Natural: ASSIGN #WS.#MESSAGE := 'Missing SSN for 1099-R'
                pnd_Ws_Pnd_Missing_Ssn_Cnt_R.nadd(1);                                                                                                                     //Natural: ADD 1 TO #MISSING-SSN-CNT-R
            }                                                                                                                                                             //Natural: WHEN FORM-01.TIRF-PAPER-PRINT-HOLD-IND = 'Y'
            else if (condition(form_01_Tirf_Paper_Print_Hold_Ind.equals("Y")))
            {
                decideConditionsMet1287++;
                pnd_Ws_Pnd_Mobius_Status.setValue("C2");                                                                                                                  //Natural: ASSIGN #MOBIUS-STATUS := 'C2'
                pnd_Ws_Pnd_Message.setValue("Paper Print Hold for 1099-R");                                                                                               //Natural: ASSIGN #WS.#MESSAGE := 'Paper Print Hold for 1099-R'
                pnd_Ws_Pnd_Paper_Print_Cnt.nadd(1);                                                                                                                       //Natural: ADD 1 TO #PAPER-PRINT-CNT
            }                                                                                                                                                             //Natural: WHEN FORM-01.TIRF-MOORE-HOLD-IND NE ' '
            else if (condition(form_01_Tirf_Moore_Hold_Ind.notEquals(" ")))
            {
                decideConditionsMet1287++;
                pnd_Ws_Pnd_Mobius_Status.setValue("D2");                                                                                                                  //Natural: ASSIGN #MOBIUS-STATUS := 'D2'
                pnd_Ws_Pnd_Message.setValue("MOORE Hold for 1099-R");                                                                                                     //Natural: ASSIGN #WS.#MESSAGE := 'MOORE Hold for 1099-R'
                pnd_Ws_Pnd_Paper_Print_Cnt.nadd(1);                                                                                                                       //Natural: ADD 1 TO #PAPER-PRINT-CNT
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet1287 > 0))
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-PAPER-PRINT-HOLD-REPORT-FORM-01
                sub_Write_Paper_Print_Hold_Report_Form_01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM_01"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM_01"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-1099R-FORM-RECORD
                sub_Process_1099r_Form_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM_01"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM_01"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                pnd_Ws_Pnd_Bypass_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #BYPASS-CNT
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Isn().setValue(vw_form_01.getAstISN("READ_FORM_01"));                                                      //Natural: ASSIGN #TWRA0214-ISN := *ISN ( READ-FORM-01. )
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Tax_Year().compute(new ComputeParameters(false, pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Tax_Year()),  //Natural: ASSIGN #TWRA0214-TAX-YEAR := VAL ( FORM-01.TIRF-TAX-YEAR )
                form_01_Tirf_Tax_Year.val());
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Tin().setValue(form_01_Tirf_Tin);                                                                          //Natural: ASSIGN #TWRA0214-TIN := FORM-01.TIRF-TIN
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Type().setValue(form_01_Tirf_Form_Type);                                                              //Natural: ASSIGN #TWRA0214-FORM-TYPE := FORM-01.TIRF-FORM-TYPE
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind().setValue("M");                                                                           //Natural: ASSIGN #TWRA0214-MOBIUS-MIGR-IND := 'M'
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Key().setValue(" ");                                                                                //Natural: ASSIGN #TWRA0214-MOBIUS-KEY := ' '
            //*  SINHSN
            //*  SINHSN
            //*  DUTTAD 010515
            //*  DUTTAD 010515
            //*  SINHSN
            DbsUtil.callnat(Twrn0214.class , getCurrentProcessState(), pnd_Ws_Pnd_Form_Page_Cnt, pnd_Ws_Pnd_Zero_Isn, pdaTwra0214.getPnd_Twra0214_Link_Area(),            //Natural: CALLNAT 'TWRN0214' #FORM-PAGE-CNT #ZERO-ISN #TWRA0214-LINK-AREA PSTA9610 #LINK-LET-TYPE ( * ) #TEST-IND-01 #COUPON-IND-01 #LTR-ARRAY ( * ) #ADDITIONAL-FORMS
                pdaPsta9610.getPsta9610(), pnd_Ws_Pnd_Link_Let_Type.getValue("*"), pnd_Test_Ind_01, pnd_Coupon_Ind_01, pnd_Ltr_Array.getValue("*"), pnd_Additional_Forms);
            if (condition(Global.isEscape())) return;
            //*  ERROR OCCURED
            if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code().notEquals(getZero())))                                                            //Natural: IF #TWRA0214-RET-CODE NE 0
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM_01"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM_01"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Problem in Tax Print Module TWRN0214",new TabSetting(77),"***",NEWLINE,"***",new     //Natural: WRITE ( 0 ) '***' 25T 'Problem in Tax Print Module TWRN0214' 77T '***' / '***' 25T 'CODE :' #TWRA0214-RET-CODE 77T '***' / '***' 25T 'MSG  :' #TWRA0214-RET-MSG 77T '***'
                    TabSetting(25),"CODE :",pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code(),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"MSG  :",pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Msg(),new 
                    TabSetting(77),"***");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM_01"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM_01"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM_01"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM_01"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                DbsUtil.terminate(104);  if (true) return;                                                                                                                //Natural: TERMINATE 104
                //*  MOBIUS MASS MIGRATION
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Mobius_Status.setValue("A1");                                                                                                                      //Natural: ASSIGN #MOBIUS-STATUS := 'A1'
            pnd_Ws_Pnd_Message.setValue("Normal Completion");                                                                                                             //Natural: ASSIGN #WS.#MESSAGE := 'Normal Completion'
                                                                                                                                                                          //Natural: PERFORM PROCESS-1099R-FORM-RECORD
            sub_Process_1099r_Form_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_FORM_01"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_FORM_01"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
            pnd_Ws_Pnd_Form_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #FORM-CNT
            //*  PERFORM WRITE-TEST-INDICATOR-REPORT    /* SINHSN
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  SET THE #NY-LTR-BYPASS FLAG TO TRUE, IF ALL THE 1099-R FORMS ARE
        //*  EXCLUDED OR PARTIALLY INCLUDED FROM BEING DISPLAYED IN NY LTR
        if (condition(! (pnd_Ny_Ltr_Value_I.getBoolean())))                                                                                                               //Natural: IF NOT #NY-LTR-VALUE-I
        {
            pnd_Ny_Ltr_Bypass.setValue(true);                                                                                                                             //Natural: MOVE TRUE TO #NY-LTR-BYPASS
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_1099r_Form_Record() throws Exception                                                                                                         //Natural: PROCESS-1099R-FORM-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        pnd_Contract_Txt.reset();                                                                                                                                         //Natural: RESET #CONTRACT-TXT
        if (condition(form_01_Tirf_Contract_Nbr.notEquals(" ")))                                                                                                          //Natural: IF FORM-01.TIRF-CONTRACT-NBR NE ' '
        {
            pnd_Contract_Txt.setValueEdited(form_01_Tirf_Contract_Nbr,new ReportEditMask("XXXXXXX'-'X"));                                                                 //Natural: MOVE EDITED FORM-01.TIRF-CONTRACT-NBR ( EM = XXXXXXX'-'X ) TO #CONTRACT-TXT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Contract_Txt.setValue(pnd_Contract_Hdr_Lit);                                                                                                              //Natural: ASSIGN #CONTRACT-TXT := #CONTRACT-HDR-LIT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Pnd_Mobius_Status.notEquals("A1")))                                                                                                          //Natural: IF #MOBIUS-STATUS NE 'A1'
        {
            pnd_1099r_Bypassed.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #1099R-BYPASSED
            pnd_Ws_Pnd_Mobius_Status_01.setValue(pnd_Ws_Pnd_Mobius_Status);                                                                                               //Natural: ASSIGN #MOBIUS-STATUS-01 := #MOBIUS-STATUS
            pnd_Ws_Pnd_Message_01.setValue("1099-R BYAPSSED");                                                                                                            //Natural: ASSIGN #WS.#MESSAGE-01 := '1099-R BYAPSSED'
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #MOBIUS-STATUS EQ 'A1'
        //*   DISPLAY(5)(HC=L)
        //*     FORM-01.TIRF-TAX-YEAR
        //*     FORM-01.TIRF-FORM-TYPE       (EM=99)
        //*     FORM-01.TIRF-COMPANY-CDE     (LC=�)
        //*     FORM-01.TIRF-TIN
        //*     #CONTRACT-TXT
        //*     FORM-01.TIRF-PAYEE-CDE
        //*     FORM-01.TIRF-KEY
        //*     FORM-01.TIRF-PIN             (EM=999999999999)
        //*     #MOBIUS-STATUS               (LC=��)
        //*     #WS.#MESSAGE
        //*  END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-1099R-REPORT-FILE
        sub_Write_1099r_Report_File();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //*  SAIK
                                                                                                                                                                          //Natural: PERFORM PROCESS-1099R-FORM-UPDATE
        sub_Process_1099r_Form_Update();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //*  SAIK
                                                                                                                                                                          //Natural: PERFORM WRITE-EXTRACT-CONTROL-REPORT-01
        sub_Write_Extract_Control_Report_01();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    }
    private void sub_Write_1099r_Report_File() throws Exception                                                                                                           //Natural: WRITE-1099R-REPORT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
                                                                                                                                                                          //Natural: PERFORM RESET-TWRL2001
        sub_Reset_Twrl2001();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tax_Year().setValue(form_01_Tirf_Tax_Year);                                                                               //Natural: ASSIGN #RPT-TAX-YEAR := FORM-01.TIRF-TAX-YEAR
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().setValue(form_01_Tirf_Form_Type);                                                                             //Natural: ASSIGN #RPT-FORM-TYPE := FORM-01.TIRF-FORM-TYPE
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Comp_Cde().setValue(form_01_Tirf_Company_Cde);                                                                            //Natural: ASSIGN #RPT-COMP-CDE := FORM-01.TIRF-COMPANY-CDE
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin().setValue(form_01_Tirf_Tin);                                                                                         //Natural: ASSIGN #RPT-TIN := FORM-01.TIRF-TIN
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Contract_Nbr().setValue(pnd_Contract_Txt);                                                                                //Natural: ASSIGN #RPT-CONTRACT-NBR := #CONTRACT-TXT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Payee_Cde().setValue(form_01_Tirf_Payee_Cde);                                                                             //Natural: ASSIGN #RPT-PAYEE-CDE := FORM-01.TIRF-PAYEE-CDE
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Pin().setValue(form_01_Tirf_Pin);                                                                                         //Natural: ASSIGN #RPT-PIN := FORM-01.TIRF-PIN
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Test_Ind().setValue(pnd_Test_Ind_01);                                                                                     //Natural: ASSIGN #RPT-TEST-IND := #TEST-IND-01
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Part_Name().setValue(form_01_Tirf_Participant_Name);                                                                      //Natural: ASSIGN #RPT-PART-NAME := FORM-01.TIRF-PARTICIPANT-NAME
        //*   /* SNEHA CHANGES STARTED - SINHSN
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().setValue(pnd_Ws_Pnd_Mobius_Status);                                                                    //Natural: MOVE #MOBIUS-STATUS TO #RPT-BYPASS-ACCPT-IND
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Gross_Amt().setValue(0);                                                                                            //Natural: MOVE 0 TO #RPT-1099R-GROSS-AMT #RPT-1099R-TAXABLE-AMT #RPT-1099R-IVC-AMT #RPT-1099R-INT-AMT #RPT-1099R-FED-TAX #RPT-1099R-STATE-TAX #RPT-1099R-LCL-TAX #RPT-1099R-IRR-AMT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Taxable_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Ivc_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Int_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Fed_Tax().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_State_Tax().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Lcl_Tax().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Irr_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Gross_Amt().setValue(form_01_Tirf_Gross_Amt);                                                                       //Natural: MOVE FORM-01.TIRF-GROSS-AMT TO #RPT-1099R-GROSS-AMT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Taxable_Amt().setValue(form_01_Tirf_Taxable_Amt);                                                                   //Natural: MOVE FORM-01.TIRF-TAXABLE-AMT TO #RPT-1099R-TAXABLE-AMT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Ivc_Amt().setValue(form_01_Tirf_Ivc_Amt);                                                                           //Natural: MOVE FORM-01.TIRF-IVC-AMT TO #RPT-1099R-IVC-AMT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Fed_Tax().setValue(form_01_Tirf_Fed_Tax_Wthld);                                                                     //Natural: MOVE FORM-01.TIRF-FED-TAX-WTHLD TO #RPT-1099R-FED-TAX
        if (condition(form_01_Count_Casttirf_1099_R_State_Grp.notEquals(getZero())))                                                                                      //Natural: IF C*FORM-01.TIRF-1099-R-STATE-GRP NE 0
        {
            pnd_C.setValue(form_01_Count_Casttirf_1099_R_State_Grp);                                                                                                      //Natural: ASSIGN #C := C*FORM-01.TIRF-1099-R-STATE-GRP
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_State_Tax().nadd(form_01_Tirf_State_Tax_Wthld.getValue(1,":",pnd_C));                                           //Natural: ADD FORM-01.TIRF-STATE-TAX-WTHLD ( 1:#C ) TO #RPT-1099R-STATE-TAX
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Lcl_Tax().nadd(form_01_Tirf_Loc_Tax_Wthld.getValue(1,":",pnd_C));                                               //Natural: ADD FORM-01.TIRF-LOC-TAX-WTHLD ( 1:#C ) TO #RPT-1099R-LCL-TAX
        }                                                                                                                                                                 //Natural: END-IF
        //*  STATE REPORTING
        //*  SAIK
                                                                                                                                                                          //Natural: PERFORM STATE-REPORT-FIELD-MOVE
        sub_State_Report_Field_Move();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_Y_Cnt.setValue(0);                                                                                                                                            //Natural: ASSIGN #Y-CNT := 0
                                                                                                                                                                          //Natural: PERFORM STATE-FIELDS
        sub_State_Fields();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (condition(pnd_Y_Cnt.equals(getZero())))                                                                                                                       //Natural: IF #Y-CNT = 0
        {
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Year().setValue(form_01_Tirf_Roth_Year);                                                                         //Natural: MOVE FORM-01.TIRF-ROTH-YEAR TO #RPT-ROTH-YEAR
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Reset_Twrl2001() throws Exception                                                                                                                    //Natural: RESET-TWRL2001
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        ldaTwrl2001.getPnd_Rpt_Twrl2001().reset();                                                                                                                        //Natural: RESET #RPT-TWRL2001
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Gross_Amt().setValue(0);                                                                                            //Natural: MOVE 0 TO #RPT-1099R-GROSS-AMT #RPT-1099R-TAXABLE-AMT #RPT-1099R-IVC-AMT #RPT-1099R-INT-AMT #RPT-1099R-FED-TAX #RPT-1099R-STATE-TAX #RPT-1099R-LCL-TAX #RPT-1099R-IRR-AMT #RPT-1099I-GROSS-AMT #RPT-1099I-TAXABLE-AMT #RPT-1099I-IVC-AMT #RPT-1099I-INT-AMT #RPT-1099I-FED-TAX #RPT-1099I-STATE-TAX #RPT-1099I-LCL-TAX #RPT-1099I-IRR-AMT #RPT-SUNY-GROSS-AMT #RPT-SUNY-FED-TAX #RPT-SUNY-SUMM-STATE-TAX #RPT-SUNY-SUMM-LCL-TAX #RPT-PCT-OF-TOT #RPT-ROTH-YEAR #RPT-FED-GROSS-AMT #RPT-FED-IVC-AMT #RPT-FED-TAX-WTHLD #RPT-TAXABLE-AMT #RPT-TOT-CONTRACTUAL-IVC #RPT-IRR-AMT #RPT-STATE-DISTR ( * ) #RPT-STATE-TAX-WTHLD ( * ) #RPT-LOC-DISTR ( * ) #RPT-LOC-TAX-WTHLD ( * ) #RPT-IVC-AMT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Taxable_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Ivc_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Int_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Fed_Tax().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_State_Tax().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Lcl_Tax().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Irr_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Gross_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Taxable_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Ivc_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Int_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Fed_Tax().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_State_Tax().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Lcl_Tax().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Irr_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Gross_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Fed_Tax().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Summ_State_Tax().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Summ_Lcl_Tax().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Pct_Of_Tot().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Year().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Fed_Gross_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Fed_Ivc_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Fed_Tax_Wthld().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Taxable_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tot_Contractual_Ivc().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Irr_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_State_Distr().getValue("*").setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_State_Tax_Wthld().getValue("*").setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Loc_Distr().getValue("*").setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Loc_Tax_Wthld().getValue("*").setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ivc_Amt().setValue(0);
    }
    private void sub_State_Fields() throws Exception                                                                                                                      //Natural: STATE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #I2 1 TO C*FORM-01.TIRF-1099-R-STATE-GRP
        for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(form_01_Count_Casttirf_1099_R_State_Grp)); pnd_I2.nadd(1))
        {
            if (condition(form_01_Tirf_State_Rpt_Ind.getValue(pnd_I2).equals("Y")))                                                                                       //Natural: IF FORM-01.TIRF-STATE-RPT-IND ( #I2 ) = 'Y'
            {
                ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_State_Rpt_Ind().getValue(pnd_I2).setValue("Y");                                                                   //Natural: MOVE 'Y' TO #RPT-STATE-RPT-IND ( #I2 )
                ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_State_Code().getValue(pnd_I2).setValue(form_01_Tirf_State_Code.getValue(pnd_I2));                                 //Natural: MOVE FORM-01.TIRF-STATE-CODE ( #I2 ) TO #RPT-STATE-CODE ( #I2 )
                pnd_Y_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #Y-CNT
                if (condition(pnd_Y_Cnt.notEquals(1)))                                                                                                                    //Natural: IF #Y-CNT NE 1
                {
                    ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Fed_Gross_Amt().setValue(0);                                                                                  //Natural: MOVE 0 TO #RPT-FED-GROSS-AMT #RPT-FED-IVC-AMT #RPT-FED-TAX-WTHLD #RPT-TAXABLE-AMT #RPT-TOT-CONTRACTUAL-IVC #RPT-IRR-AMT
                    ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Fed_Ivc_Amt().setValue(0);
                    ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Fed_Tax_Wthld().setValue(0);
                    ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Taxable_Amt().setValue(0);
                    ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tot_Contractual_Ivc().setValue(0);
                    ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Irr_Amt().setValue(0);
                }                                                                                                                                                         //Natural: END-IF
                ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_State_Distr().getValue(pnd_I2).setValue(form_01_Tirf_State_Distr.getValue(pnd_I2));                               //Natural: MOVE FORM-01.TIRF-STATE-DISTR ( #I2 ) TO #RPT-STATE-DISTR ( #I2 )
                ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_State_Tax_Wthld().getValue(pnd_I2).setValue(form_01_Tirf_State_Tax_Wthld.getValue(pnd_I2));                       //Natural: MOVE FORM-01.TIRF-STATE-TAX-WTHLD ( #I2 ) TO #RPT-STATE-TAX-WTHLD ( #I2 )
                ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Loc_Distr().getValue(pnd_I2).setValue(form_01_Tirf_Loc_Distr.getValue(pnd_I2));                                   //Natural: MOVE FORM-01.TIRF-LOC-DISTR ( #I2 ) TO #RPT-LOC-DISTR ( #I2 )
                ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Loc_Tax_Wthld().getValue(pnd_I2).setValue(form_01_Tirf_Loc_Tax_Wthld.getValue(pnd_I2));                           //Natural: MOVE FORM-01.TIRF-LOC-TAX-WTHLD ( #I2 ) TO #RPT-LOC-TAX-WTHLD ( #I2 )
                ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_State_Hardcopy_Ind().getValue(pnd_I2).setValue(form_01_Tirf_State_Hardcopy_Ind.getValue(pnd_I2));                 //Natural: MOVE FORM-01.TIRF-STATE-HARDCOPY-IND ( #I2 ) TO #RPT-STATE-HARDCOPY-IND ( #I2 )
                ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Year().setValue(form_01_Tirf_Roth_Year);                                                                     //Natural: MOVE FORM-01.TIRF-ROTH-YEAR TO #RPT-ROTH-YEAR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_State_Report_Field_Move() throws Exception                                                                                                           //Natural: STATE-REPORT-FIELD-MOVE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Id_Ind().setValue(form_01_Tirf_Tax_Id_Type);                                                                              //Natural: MOVE FORM-01.TIRF-TAX-ID-TYPE TO #RPT-ID-IND
        if (condition(form_01_Tirf_Tax_Id_Type.equals("1") || form_01_Tirf_Tax_Id_Type.equals("2") || form_01_Tirf_Tax_Id_Type.equals("3")))                              //Natural: IF FORM-01.TIRF-TAX-ID-TYPE = '1' OR = '2' OR = '3'
        {
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin2().setValue(form_01_Tirf_Tin);                                                                                    //Natural: MOVE FORM-01.TIRF-TIN TO #RPT-TIN2
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Distribution_Cde().setValue(form_01_Tirf_Distribution_Cde);                                                               //Natural: MOVE FORM-01.TIRF-DISTRIBUTION-CDE TO #RPT-DISTRIBUTION-CDE
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Type_Cde().setValue("R");                                                                                                 //Natural: MOVE 'R' TO #RPT-TYPE-CDE
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Fed_Gross_Amt().setValue(form_01_Tirf_Gross_Amt);                                                                         //Natural: MOVE FORM-01.TIRF-GROSS-AMT TO #RPT-FED-GROSS-AMT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Taxable_Not_Det().setValue(form_01_Tirf_Taxable_Not_Det);                                                                 //Natural: MOVE FORM-01.TIRF-TAXABLE-NOT-DET TO #RPT-TAXABLE-NOT-DET
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Taxable_Amt().setValue(form_01_Tirf_Taxable_Amt);                                                                         //Natural: MOVE FORM-01.TIRF-TAXABLE-AMT TO #RPT-TAXABLE-AMT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ivc_Amt().setValue(form_01_Tirf_Ivc_Amt);                                                                                 //Natural: MOVE FORM-01.TIRF-IVC-AMT TO #RPT-IVC-AMT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Fed_Tax_Wthld().setValue(form_01_Tirf_Fed_Tax_Wthld);                                                                     //Natural: MOVE FORM-01.TIRF-FED-TAX-WTHLD TO #RPT-FED-TAX-WTHLD
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tot_Contractual_Ivc().setValue(form_01_Tirf_Tot_Contractual_Ivc);                                                         //Natural: MOVE FORM-01.TIRF-TOT-CONTRACTUAL-IVC TO #RPT-TOT-CONTRACTUAL-IVC
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ira_Distr().setValue(form_01_Tirf_Ira_Distr);                                                                             //Natural: MOVE FORM-01.TIRF-IRA-DISTR TO #RPT-IRA-DISTR
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Pct_Of_Tot().setValue(form_01_Tirf_Pct_Of_Tot);                                                                           //Natural: MOVE FORM-01.TIRF-PCT-OF-TOT TO #RPT-PCT-OF-TOT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tot_Distr().setValue(form_01_Tirf_Tot_Distr);                                                                             //Natural: MOVE FORM-01.TIRF-TOT-DISTR TO #RPT-TOT-DISTR
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Moore_Test_Ind().setValue(form_01_Tirf_Moore_Test_Ind);                                                                   //Natural: MOVE FORM-01.TIRF-MOORE-TEST-IND TO #RPT-MOORE-TEST-IND
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Moore_Hold_Ind().setValue(form_01_Tirf_Moore_Hold_Ind);                                                                   //Natural: MOVE FORM-01.TIRF-MOORE-HOLD-IND TO #RPT-MOORE-HOLD-IND
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Participant_Name().setValue(form_01_Tirf_Participant_Name);                                                               //Natural: MOVE FORM-01.TIRF-PARTICIPANT-NAME TO #RPT-PARTICIPANT-NAME
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Addr_Ln1().setValue(form_01_Tirf_Addr_Ln1);                                                                               //Natural: MOVE FORM-01.TIRF-ADDR-LN1 TO #RPT-ADDR-LN1
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Addr_Ln2().setValue(form_01_Tirf_Addr_Ln2);                                                                               //Natural: MOVE FORM-01.TIRF-ADDR-LN2 TO #RPT-ADDR-LN2
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Addr_Ln3().setValue(form_01_Tirf_Addr_Ln3);                                                                               //Natural: MOVE FORM-01.TIRF-ADDR-LN3 TO #RPT-ADDR-LN3
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Addr_Ln4().setValue(form_01_Tirf_Addr_Ln4);                                                                               //Natural: MOVE FORM-01.TIRF-ADDR-LN4 TO #RPT-ADDR-LN4
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Addr_Ln5().setValue(form_01_Tirf_Addr_Ln5);                                                                               //Natural: MOVE FORM-01.TIRF-ADDR-LN5 TO #RPT-ADDR-LN5
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Addr_Ln6().setValue(form_01_Tirf_Addr_Ln6);                                                                               //Natural: MOVE FORM-01.TIRF-ADDR-LN6 TO #RPT-ADDR-LN6
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Zip1().setValue(form_01_Tirf_Zip.getSubstring(1,5));                                                                      //Natural: ASSIGN #RPT-ZIP1 := SUBSTR ( FORM-01.TIRF-ZIP,1,5 )
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Zip4().setValue(form_01_Tirf_Zip.getSubstring(6,4));                                                                      //Natural: ASSIGN #RPT-ZIP4 := SUBSTR ( FORM-01.TIRF-ZIP,6,4 )
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Walk_Rte().setValue(form_01_Tirf_Walk_Rte);                                                                               //Natural: MOVE FORM-01.TIRF-WALK-RTE TO #RPT-WALK-RTE
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Geo_Cde().setValue(form_01_Tirf_Geo_Cde);                                                                                 //Natural: MOVE FORM-01.TIRF-GEO-CDE TO #RPT-GEO-CDE
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Country_Name().setValue(form_01_Tirf_Country_Name);                                                                       //Natural: MOVE FORM-01.TIRF-COUNTRY-NAME TO #RPT-COUNTRY-NAME
        if (condition(form_01_Tirf_Foreign_Addr.equals("Y")))                                                                                                             //Natural: IF FORM-01.TIRF-FOREIGN-ADDR EQ 'Y'
        {
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Foreign_Addr().setValue(form_01_Tirf_Foreign_Addr);                                                                   //Natural: MOVE FORM-01.TIRF-FOREIGN-ADDR TO #RPT-FOREIGN-ADDR
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Y_Cnt.setValue(0);                                                                                                                                            //Natural: ASSIGN #Y-CNT := 0
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Irr_Amt().setValue(form_01_Tirf_Irr_Amt);                                                                                 //Natural: MOVE FORM-01.TIRF-IRR-AMT TO #RPT-IRR-AMT
        //*  DUTTAD 010515 <<
    }
    private void sub_Write_Paper_Print_Hold_Report_Form_01() throws Exception                                                                                             //Natural: WRITE-PAPER-PRINT-HOLD-REPORT-FORM-01
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        pnd_Contract_Txt.reset();                                                                                                                                         //Natural: RESET #CONTRACT-TXT
        if (condition(form_01_Tirf_Contract_Nbr.notEquals(" ")))                                                                                                          //Natural: IF FORM-01.TIRF-CONTRACT-NBR NE ' '
        {
            pnd_Contract_Txt.setValueEdited(form_01_Tirf_Contract_Nbr,new ReportEditMask("XXXXXXX'-'X"));                                                                 //Natural: MOVE EDITED FORM-01.TIRF-CONTRACT-NBR ( EM = XXXXXXX'-'X ) TO #CONTRACT-TXT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Contract_Txt.setValue(pnd_Contract_Hdr_Lit);                                                                                                              //Natural: ASSIGN #CONTRACT-TXT := #CONTRACT-HDR-LIT
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(2, form_01_Tirf_Tax_Year,form_01_Tirf_Form_Type, new ReportEditMask ("99"),form_01_Tirf_Company_Cde, new FieldAttributes("LC=�"),form_01_Tirf_Tin,pnd_Contract_Txt,form_01_Tirf_Payee_Cde,form_01_Tirf_Key,form_01_Tirf_Pin,  //Natural: DISPLAY ( 2 ) FORM-01.TIRF-TAX-YEAR FORM-01.TIRF-FORM-TYPE ( EM = 99 ) FORM-01.TIRF-COMPANY-CDE ( LC = � ) FORM-01.TIRF-TIN #CONTRACT-TXT FORM-01.TIRF-PAYEE-CDE FORM-01.TIRF-KEY FORM-01.TIRF-PIN ( EM = 999999999999 ) #MOBIUS-STATUS ( LC = �� ) #WS.#MESSAGE
            new ReportEditMask ("999999999999"),pnd_Ws_Pnd_Mobius_Status, new FieldAttributes("LC=��"),pnd_Ws_Pnd_Message);
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 0 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 0 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 0 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Process_Suny_Form_Update() throws Exception                                                                                                          //Natural: PROCESS-SUNY-FORM-UPDATE
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Isn().setValue(vw_form.getAstISN("READ_FORM"));                                                                //Natural: ASSIGN #TWRA0214-ISN := *ISN ( READ-FORM. )
        GET:                                                                                                                                                              //Natural: GET FORM-UPD #TWRA0214-ISN
        vw_form_Upd.readByID(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Isn().getLong(), "GET");
        form_Upd_Tirf_Mobius_Stat.setValue(pnd_Ws_Pnd_Mobius_Status);                                                                                                     //Natural: ASSIGN FORM-UPD.TIRF-MOBIUS-STAT := #MOBIUS-STATUS
        form_Upd_Tirf_Mobius_Stat_Date.setValue(pnd_Ws_Pnd_Sys_Date);                                                                                                     //Natural: ASSIGN FORM-UPD.TIRF-MOBIUS-STAT-DATE := #SYS-DATE
        if (condition(pnd_Ws_Pnd_Mobius_Status.equals("A1")))                                                                                                             //Natural: IF #MOBIUS-STATUS EQ 'A1'
        {
            form_Upd_Tirf_Part_Rpt_Date.setValue(pnd_Ws_Pnd_Sys_Date);                                                                                                    //Natural: ASSIGN FORM-UPD.TIRF-PART-RPT-DATE := #SYS-DATE
        }                                                                                                                                                                 //Natural: END-IF
        form_Upd_Tirf_Mobius_Ind.setValue(" ");                                                                                                                           //Natural: ASSIGN FORM-UPD.TIRF-MOBIUS-IND := ' '
        form_Upd_Tirf_Part_Rpt_Ind.setValue("M");                                                                                                                         //Natural: ASSIGN FORM-UPD.TIRF-PART-RPT-IND := 'M'
        vw_form_Upd.updateDBRow("GET");                                                                                                                                   //Natural: UPDATE ( GET. )
        pnd_Ws_Pnd_Et_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ET-CNT
        if (condition(pnd_Ws_Pnd_Et_Cnt.greaterOrEqual(et_Limit)))                                                                                                        //Natural: IF #ET-CNT GE ET-LIMIT
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Ws_Pnd_Et_Cnt.reset();                                                                                                                                    //Natural: RESET #ET-CNT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Update_Cnt_Suny.nadd(1);                                                                                                                               //Natural: ADD 1 TO #UPDATE-CNT-SUNY
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Process_1099r_Form_Update() throws Exception                                                                                                         //Natural: PROCESS-1099R-FORM-UPDATE
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Isn().setValue(vw_form_01.getAstISN("READ_FORM_01"));                                                          //Natural: ASSIGN #TWRA0214-ISN := *ISN ( READ-FORM-01. )
        GET1:                                                                                                                                                             //Natural: GET FORM-UPD #TWRA0214-ISN
        vw_form_Upd.readByID(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Isn().getLong(), "GET1");
        form_Upd_Tirf_Mobius_Stat.setValue(pnd_Ws_Pnd_Mobius_Status);                                                                                                     //Natural: ASSIGN FORM-UPD.TIRF-MOBIUS-STAT := #MOBIUS-STATUS
        form_Upd_Tirf_Mobius_Stat_Date.setValue(pnd_Ws_Pnd_Sys_Date);                                                                                                     //Natural: ASSIGN FORM-UPD.TIRF-MOBIUS-STAT-DATE := #SYS-DATE
        if (condition(pnd_Ws_Pnd_Mobius_Status.equals("A1")))                                                                                                             //Natural: IF #MOBIUS-STATUS EQ 'A1'
        {
            form_Upd_Tirf_Part_Rpt_Date.setValue(pnd_Ws_Pnd_Sys_Date);                                                                                                    //Natural: ASSIGN FORM-UPD.TIRF-PART-RPT-DATE := #SYS-DATE
        }                                                                                                                                                                 //Natural: END-IF
        form_Upd_Tirf_Mobius_Ind.setValue(" ");                                                                                                                           //Natural: ASSIGN FORM-UPD.TIRF-MOBIUS-IND := ' '
        form_Upd_Tirf_Part_Rpt_Ind.setValue("M");                                                                                                                         //Natural: ASSIGN FORM-UPD.TIRF-PART-RPT-IND := 'M'
        vw_form_Upd.updateDBRow("GET1");                                                                                                                                  //Natural: UPDATE ( GET1. )
        pnd_Ws_Pnd_Et_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ET-CNT
        if (condition(pnd_Ws_Pnd_Et_Cnt.greaterOrEqual(et_Limit)))                                                                                                        //Natural: IF #ET-CNT GE ET-LIMIT
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Ws_Pnd_Et_Cnt.reset();                                                                                                                                    //Natural: RESET #ET-CNT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Update_Cnt_1099r.nadd(1);                                                                                                                              //Natural: ADD 1 TO #UPDATE-CNT-1099R
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Write_Extract_Control_Report() throws Exception                                                                                                      //Natural: WRITE-EXTRACT-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        //*  NON SUNY FORMS
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().notEquals(10)))                                                                                 //Natural: IF #RPT-FORM-TYPE NE 10
        {
            Global.setEscapeCode(EscapeType.Top); if (true) return;                                                                                                       //Natural: ESCAPE TOP
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DB-READ-PROCESS
        sub_Db_Read_Process();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Header_Ind().equals("Y")))                                                                             //Natural: IF #RPT-SUNY-HEADER-IND EQ 'Y'
        {
            Global.setEscapeCode(EscapeType.Top); if (true) return;                                                                                                       //Natural: ESCAPE TOP
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().notEquals("A1")))                                                                        //Natural: IF #RPT-BYPASS-ACCPT-IND NE 'A1'
        {
                                                                                                                                                                          //Natural: PERFORM BYPASSED-PROCESS
            sub_Bypassed_Process();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ACCEPTED-PROCESS
            sub_Accepted_Process();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Tax_Year_Pnd_Ws_Tax_Year_A.setValue(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tax_Year());                                                                   //Natural: ASSIGN #WS-TAX-YEAR-A := #RPT-TAX-YEAR
    }
    private void sub_Write_Extract_Control_Report_01() throws Exception                                                                                                   //Natural: WRITE-EXTRACT-CONTROL-REPORT-01
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Wkfle_Recd_Cnt.nadd(1);                                                                                                                                       //Natural: ASSIGN #WKFLE-RECD-CNT := #WKFLE-RECD-CNT + 1
        short decideConditionsMet1559 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #RPT-COMP-CDE = 'T' OR = 'A'
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Comp_Cde().equals("T") || ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Comp_Cde().equals("A")))
        {
            decideConditionsMet1559++;
                                                                                                                                                                          //Natural: PERFORM TIAA
            sub_Tiaa();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #RPT-COMP-CDE = 'X'
        else if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Comp_Cde().equals("X")))
        {
            decideConditionsMet1559++;
                                                                                                                                                                          //Natural: PERFORM TRUST
            sub_Trust();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            getReports().write(0, ReportOption.NOTITLE,"INVALID COMPANY CODE AT ROW NUMBER:",pnd_Wkfle_Recd_Cnt);                                                         //Natural: WRITE 'INVALID COMPANY CODE AT ROW NUMBER:' #WKFLE-RECD-CNT
            if (Global.isEscape()) return;
            Global.setEscapeCode(EscapeType.Top); if (true) return;                                                                                                       //Natural: ESCAPE TOP
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Ws_Tax_Year_Pnd_Ws_Tax_Year_A.setValue(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tax_Year());                                                                   //Natural: ASSIGN #WS-TAX-YEAR-A := #RPT-TAX-YEAR
        pnd_Hold_Rpt_Tin.setValue(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin());                                                                                         //Natural: ASSIGN #HOLD-RPT-TIN := #RPT-TIN
        pnd_Total_Unq_Tin.compute(new ComputeParameters(false, pnd_Total_Unq_Tin), pnd_Unq_Tin_Eb.add(pnd_Unq_Tin_Ea));                                                   //Natural: ASSIGN #TOTAL-UNQ-TIN := #UNQ-TIN-EB + #UNQ-TIN-EA
    }
    private void sub_Tiaa() throws Exception                                                                                                                              //Natural: TIAA
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************
                                                                                                                                                                          //Natural: PERFORM DB-READ-PROCESS-TIAA
        sub_Db_Read_Process_Tiaa();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().notEquals("A1")))                                                                        //Natural: IF #RPT-BYPASS-ACCPT-IND NE 'A1'
        {
                                                                                                                                                                          //Natural: PERFORM BYPASSED-PROCESS-TIAA
            sub_Bypassed_Process_Tiaa();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ACCEPTED-PROCESS-TIAA
            sub_Accepted_Process_Tiaa();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Trust() throws Exception                                                                                                                             //Natural: TRUST
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************
                                                                                                                                                                          //Natural: PERFORM DB-READ-PROCESS-TRUST
        sub_Db_Read_Process_Trust();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().notEquals("A1")))                                                                        //Natural: IF #RPT-BYPASS-ACCPT-IND NE 'A1'
        {
                                                                                                                                                                          //Natural: PERFORM BYPASSED-PROCESS-TRUST
            sub_Bypassed_Process_Trust();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ACCEPTED-PROCESS-TRUST
            sub_Accepted_Process_Trust();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Db_Read_Process_Tiaa() throws Exception                                                                                                              //Natural: DB-READ-PROCESS-TIAA
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //*  1099-R
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(1)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 1
        {
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R.nadd(1);                                                                                                                      //Natural: ADD 1 TO #TIAA-1099-R
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Gross_Amt.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Gross_Amt());                                                    //Natural: ADD #RPT-1099R-GROSS-AMT TO #TIAA-1099-R-GROSS-AMT
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Taxable.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Taxable_Amt());                                                    //Natural: ADD #RPT-1099R-TAXABLE-AMT TO #TIAA-1099-R-TAXABLE
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Ivc.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Ivc_Amt());                                                            //Natural: ADD #RPT-1099R-IVC-AMT TO #TIAA-1099-R-IVC
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Fed.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Fed_Tax());                                                            //Natural: ADD #RPT-1099R-FED-TAX TO #TIAA-1099-R-FED
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Local.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Lcl_Tax());                                                          //Natural: ADD #RPT-1099R-LCL-TAX TO #TIAA-1099-R-LOCAL
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Int.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Int_Amt());                                                            //Natural: ADD #RPT-1099R-INT-AMT TO #TIAA-1099-R-INT
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_State.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_State_Tax());                                                        //Natural: ADD #RPT-1099R-STATE-TAX TO #TIAA-1099-R-STATE
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Irr.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Irr_Amt());                                                            //Natural: ADD #RPT-1099R-IRR-AMT TO #TIAA-1099-R-IRR
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Bypassed_Process_Tiaa() throws Exception                                                                                                             //Natural: BYPASSED-PROCESS-TIAA
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(1)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 1
        {
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_B.nadd(1);                                                                                                            //Natural: ADD 1 TO #TIAA-1099-R-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Gross_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Gross_Amt());                                          //Natural: ADD #RPT-1099R-GROSS-AMT TO #TIAA-1099-R-GROSS-AMT-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Taxable_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Taxable_Amt());                                          //Natural: ADD #RPT-1099R-TAXABLE-AMT TO #TIAA-1099-R-TAXABLE-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Ivc_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Ivc_Amt());                                                  //Natural: ADD #RPT-1099R-IVC-AMT TO #TIAA-1099-R-IVC-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Fed_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Fed_Tax());                                                  //Natural: ADD #RPT-1099R-FED-TAX TO #TIAA-1099-R-FED-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Local_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Lcl_Tax());                                                //Natural: ADD #RPT-1099R-LCL-TAX TO #TIAA-1099-R-LOCAL-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Int_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Int_Amt());                                                  //Natural: ADD #RPT-1099R-INT-AMT TO #TIAA-1099-R-INT-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_State_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_State_Tax());                                              //Natural: ADD #RPT-1099R-STATE-TAX TO #TIAA-1099-R-STATE-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Irr_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Irr_Amt());                                                  //Natural: ADD #RPT-1099R-IRR-AMT TO #TIAA-1099-R-IRR-B
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Accepted_Process_Tiaa() throws Exception                                                                                                             //Natural: ACCEPTED-PROCESS-TIAA
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(1)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 1
        {
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_A.nadd(1);                                                                                                            //Natural: ADD 1 TO #TIAA-1099-R-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Gross_Amt_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Gross_Amt());                                          //Natural: ADD #RPT-1099R-GROSS-AMT TO #TIAA-1099-R-GROSS-AMT-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Taxable_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Taxable_Amt());                                          //Natural: ADD #RPT-1099R-TAXABLE-AMT TO #TIAA-1099-R-TAXABLE-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Ivc_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Ivc_Amt());                                                  //Natural: ADD #RPT-1099R-IVC-AMT TO #TIAA-1099-R-IVC-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Fed_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Fed_Tax());                                                  //Natural: ADD #RPT-1099R-FED-TAX TO #TIAA-1099-R-FED-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Local_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Lcl_Tax());                                                //Natural: ADD #RPT-1099R-LCL-TAX TO #TIAA-1099-R-LOCAL-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Int_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Int_Amt());                                                  //Natural: ADD #RPT-1099R-INT-AMT TO #TIAA-1099-R-INT-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_State_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_State_Tax());                                              //Natural: ADD #RPT-1099R-STATE-TAX TO #TIAA-1099-R-STATE-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Irr_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Irr_Amt());                                                  //Natural: ADD #RPT-1099R-IRR-AMT TO #TIAA-1099-R-IRR-A
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Db_Read_Process_Trust() throws Exception                                                                                                             //Natural: DB-READ-PROCESS-TRUST
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        //*  1099-R
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(1)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 1
        {
            pnd_Trust_Totals_Pnd_Trust_1099_R.nadd(1);                                                                                                                    //Natural: ADD 1 TO #TRUST-1099-R
            pnd_Trust_Totals_Pnd_Trust_1099_R_Gross_Amt.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Gross_Amt());                                                  //Natural: ADD #RPT-1099R-GROSS-AMT TO #TRUST-1099-R-GROSS-AMT
            pnd_Trust_Totals_Pnd_Trust_1099_R_Taxable.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Taxable_Amt());                                                  //Natural: ADD #RPT-1099R-TAXABLE-AMT TO #TRUST-1099-R-TAXABLE
            pnd_Trust_Totals_Pnd_Trust_1099_R_Ivc.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Ivc_Amt());                                                          //Natural: ADD #RPT-1099R-IVC-AMT TO #TRUST-1099-R-IVC
            pnd_Trust_Totals_Pnd_Trust_1099_R_Fed.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Fed_Tax());                                                          //Natural: ADD #RPT-1099R-FED-TAX TO #TRUST-1099-R-FED
            pnd_Trust_Totals_Pnd_Trust_1099_R_Local.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Lcl_Tax());                                                        //Natural: ADD #RPT-1099R-LCL-TAX TO #TRUST-1099-R-LOCAL
            pnd_Trust_Totals_Pnd_Trust_1099_R_Int.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Int_Amt());                                                          //Natural: ADD #RPT-1099R-INT-AMT TO #TRUST-1099-R-INT
            pnd_Trust_Totals_Pnd_Trust_1099_R_State.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_State_Tax());                                                      //Natural: ADD #RPT-1099R-STATE-TAX TO #TRUST-1099-R-STATE
            pnd_Trust_Totals_Pnd_Trust_1099_R_Irr.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Irr_Amt());                                                          //Natural: ADD #RPT-1099R-IRR-AMT TO #TRUST-1099-R-IRR
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Bypassed_Process_Trust() throws Exception                                                                                                            //Natural: BYPASSED-PROCESS-TRUST
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        //*  1099-R
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(1)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 1
        {
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_B.nadd(1);                                                                                                          //Natural: ADD 1 TO #TRUST-1099-R-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Gross_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Gross_Amt());                                        //Natural: ADD #RPT-1099R-GROSS-AMT TO #TRUST-1099-R-GROSS-AMT-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Taxable_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Taxable_Amt());                                        //Natural: ADD #RPT-1099R-TAXABLE-AMT TO #TRUST-1099-R-TAXABLE-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Ivc_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Ivc_Amt());                                                //Natural: ADD #RPT-1099R-IVC-AMT TO #TRUST-1099-R-IVC-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Fed_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Fed_Tax());                                                //Natural: ADD #RPT-1099R-FED-TAX TO #TRUST-1099-R-FED-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Local_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Lcl_Tax());                                              //Natural: ADD #RPT-1099R-LCL-TAX TO #TRUST-1099-R-LOCAL-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Int_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Int_Amt());                                                //Natural: ADD #RPT-1099R-INT-AMT TO #TRUST-1099-R-INT-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_State_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_State_Tax());                                            //Natural: ADD #RPT-1099R-STATE-TAX TO #TRUST-1099-R-STATE-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Irr_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Irr_Amt());                                                //Natural: ADD #RPT-1099R-IRR-AMT TO #TRUST-1099-R-IRR-B
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Accepted_Process_Trust() throws Exception                                                                                                            //Natural: ACCEPTED-PROCESS-TRUST
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        //*  1099-R
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(1)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 1
        {
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Gross_Amt_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Gross_Amt());                                        //Natural: ADD #RPT-1099R-GROSS-AMT TO #TRUST-1099-R-GROSS-AMT-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Taxable_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Taxable_Amt());                                        //Natural: ADD #RPT-1099R-TAXABLE-AMT TO #TRUST-1099-R-TAXABLE-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Ivc_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Ivc_Amt());                                                //Natural: ADD #RPT-1099R-IVC-AMT TO #TRUST-1099-R-IVC-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Fed_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Fed_Tax());                                                //Natural: ADD #RPT-1099R-FED-TAX TO #TRUST-1099-R-FED-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Local_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Lcl_Tax());                                              //Natural: ADD #RPT-1099R-LCL-TAX TO #TRUST-1099-R-LOCAL-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Int_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Int_Amt());                                                //Natural: ADD #RPT-1099R-INT-AMT TO #TRUST-1099-R-INT-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_State_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_State_Tax());                                            //Natural: ADD #RPT-1099R-STATE-TAX TO #TRUST-1099-R-STATE-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Irr_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Irr_Amt());                                                //Natural: ADD #RPT-1099R-IRR-AMT TO #TRUST-1099-R-IRR-A
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Db_Read_Process() throws Exception                                                                                                                   //Natural: DB-READ-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        //*  SUNY FORMS
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(10)))                                                                                    //Natural: IF #RPT-FORM-TYPE = 10
        {
            if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Header_Ind().equals("Y")))                                                                         //Natural: IF #RPT-SUNY-HEADER-IND EQ 'Y'
            {
                pnd_Db_Read_Fields_Pnd_Letter_Cnt_Db.nadd(1);                                                                                                             //Natural: ADD 1 TO #LETTER-CNT-DB
                short decideConditionsMet1684 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #RPT-BYPASS-ACCPT-IND NE 'A1'
                if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().notEquals("A1")))
                {
                    decideConditionsMet1684++;
                    pnd_E_Del_Bypassed_Fields_Pnd_Letter_Cnt_B.nadd(1);                                                                                                   //Natural: ADD 1 TO #LETTER-CNT-B
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: WHEN #RPT-BYPASS-ACCPT-IND EQ 'A1'
                else if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().equals("A1")))
                {
                    decideConditionsMet1684++;
                    pnd_E_Del_Accepted_Fields_Pnd_Letter_Cnt_A.nadd(1);                                                                                                   //Natural: ADD 1 TO #LETTER-CNT-A
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  FOR DETAILS RECORDS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                DbsUtil.examine(new ExamineSource(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Details_Ind().getValue("*")), new ExamineSearch("Y"), new                  //Natural: EXAMINE #RPT-SUNY-DETAILS-IND ( * ) FOR 'Y' GIVING NUMBER #DETL-TEMP-DB
                    ExamineGivingNumber(pnd_Detl_Temp_Db));
                pnd_Db_Read_Fields_Pnd_Detl_Cnt_Db.compute(new ComputeParameters(false, pnd_Db_Read_Fields_Pnd_Detl_Cnt_Db), pnd_Detl_Temp_Db.add(pnd_Db_Read_Fields_Pnd_Detl_Cnt_Db)); //Natural: ASSIGN #DETL-CNT-DB := #DETL-TEMP-DB + #DETL-CNT-DB
                pnd_Db_Read_Fields_Pnd_Suny_Gross_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Gross_Amt());                                                  //Natural: ADD #RPT-SUNY-GROSS-AMT TO #SUNY-GROSS-AMT-DB
                pnd_Db_Read_Fields_Pnd_Suny_Fed_Tax_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Fed_Tax());                                                      //Natural: ADD #RPT-SUNY-FED-TAX TO #SUNY-FED-TAX-DB
                pnd_Db_Read_Fields_Pnd_Suny_Summ_State_Tax_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Summ_State_Tax());                                        //Natural: ADD #RPT-SUNY-SUMM-STATE-TAX TO #SUNY-SUMM-STATE-TAX-DB
                pnd_Db_Read_Fields_Pnd_Suny_Summ_Lcl_Tax_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Summ_Lcl_Tax());                                            //Natural: ADD #RPT-SUNY-SUMM-LCL-TAX TO #SUNY-SUMM-LCL-TAX-DB
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Bypassed_Process() throws Exception                                                                                                                  //Natural: BYPASSED-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  SUNY FORMS
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(10)))                                                                                    //Natural: IF #RPT-FORM-TYPE = 10
        {
            DbsUtil.examine(new ExamineSource(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Details_Ind().getValue("*")), new ExamineSearch("Y"), new ExamineGivingNumber(pnd_Detl_Temp_B)); //Natural: EXAMINE #RPT-SUNY-DETAILS-IND ( * ) FOR 'Y' GIVING NUMBER #DETL-TEMP-B
            pnd_E_Del_Bypassed_Fields_Pnd_Detl_Cnt_B.compute(new ComputeParameters(false, pnd_E_Del_Bypassed_Fields_Pnd_Detl_Cnt_B), pnd_Detl_Temp_B.add(pnd_E_Del_Bypassed_Fields_Pnd_Detl_Cnt_B)); //Natural: ASSIGN #DETL-CNT-B := #DETL-TEMP-B + #DETL-CNT-B
            pnd_E_Del_Bypassed_Fields_Pnd_Suny_Gross_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Gross_Amt());                                                //Natural: ADD #RPT-SUNY-GROSS-AMT TO #SUNY-GROSS-AMT-B
            pnd_E_Del_Bypassed_Fields_Pnd_Suny_Fed_Tax_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Fed_Tax());                                                    //Natural: ADD #RPT-SUNY-FED-TAX TO #SUNY-FED-TAX-B
            pnd_E_Del_Bypassed_Fields_Pnd_Suny_Summ_State_Tax_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Summ_State_Tax());                                      //Natural: ADD #RPT-SUNY-SUMM-STATE-TAX TO #SUNY-SUMM-STATE-TAX-B
            pnd_E_Del_Bypassed_Fields_Pnd_Suny_Summ_Lcl_Tax_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Summ_Lcl_Tax());                                          //Natural: ADD #RPT-SUNY-SUMM-LCL-TAX TO #SUNY-SUMM-LCL-TAX-B
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Accepted_Process() throws Exception                                                                                                                  //Natural: ACCEPTED-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  SUNY FORMS
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(10)))                                                                                    //Natural: IF #RPT-FORM-TYPE = 10
        {
            DbsUtil.examine(new ExamineSource(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Details_Ind().getValue("*")), new ExamineSearch("Y"), new ExamineGivingNumber(pnd_Detl_Temp_A)); //Natural: EXAMINE #RPT-SUNY-DETAILS-IND ( * ) FOR 'Y' GIVING NUMBER #DETL-TEMP-A
            pnd_E_Del_Accepted_Fields_Pnd_Detl_Cnt_A.compute(new ComputeParameters(false, pnd_E_Del_Accepted_Fields_Pnd_Detl_Cnt_A), pnd_Detl_Temp_A.add(pnd_E_Del_Accepted_Fields_Pnd_Detl_Cnt_A)); //Natural: ASSIGN #DETL-CNT-A := #DETL-TEMP-A + #DETL-CNT-A
            pnd_E_Del_Accepted_Fields_Pnd_Suny_Gross_Amt_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Gross_Amt());                                                //Natural: ADD #RPT-SUNY-GROSS-AMT TO #SUNY-GROSS-AMT-A
            pnd_E_Del_Accepted_Fields_Pnd_Suny_Fed_Tax_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Fed_Tax());                                                    //Natural: ADD #RPT-SUNY-FED-TAX TO #SUNY-FED-TAX-A
            pnd_E_Del_Accepted_Fields_Pnd_Suny_Summ_State_Tax_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Summ_State_Tax());                                      //Natural: ADD #RPT-SUNY-SUMM-STATE-TAX TO #SUNY-SUMM-STATE-TAX-A
            pnd_E_Del_Accepted_Fields_Pnd_Suny_Summ_Lcl_Tax_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Suny_Summ_Lcl_Tax());                                          //Natural: ADD #RPT-SUNY-SUMM-LCL-TAX TO #SUNY-SUMM-LCL-TAX-A
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Title_Report1() throws Exception                                                                                                                     //Natural: TITLE-REPORT1
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(5));                                                                                                                 //Natural: NEWPAGE ( 5 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Payment System",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Payment System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' 'TWRP6892-01' /49X 'Extract Control Report' #WS-TAX-YEAR ( EM = 9999 ) /49X 'Participant Mass Mailing' //
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-","TWRP6892-01",NEWLINE,new 
            ColumnSpacing(49),"Extract Control Report",pnd_Ws_Tax_Year, new ReportEditMask ("9999"),NEWLINE,new ColumnSpacing(49),"Participant Mass Mailing",
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Title_Report2() throws Exception                                                                                                                     //Natural: TITLE-REPORT2
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(5));                                                                                                                 //Natural: NEWPAGE ( 5 )
        if (condition(Global.isEscape())){return;}
        getReports().write(6, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Payment System",new  //Natural: WRITE ( 6 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Payment System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' 'TWRP6892-01' /49X 'Extract Control Report' #WS-TAX-YEAR ( EM = 9999 ) /49X 'Participant Mass Mailing' //
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-","TWRP6892-01",NEWLINE,new 
            ColumnSpacing(49),"Extract Control Report",pnd_Ws_Tax_Year, new ReportEditMask ("9999"),NEWLINE,new ColumnSpacing(49),"Participant Mass Mailing",
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Write_Test_Indicator_Report() throws Exception                                                                                                       //Natural: WRITE-TEST-INDICATOR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Test_Ind().notEquals("Y")))                                                                                 //Natural: IF #RPT-TEST-IND NE 'Y'
        {
            Global.setEscapeCode(EscapeType.Top); if (true) return;                                                                                                       //Natural: ESCAPE TOP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM TEST-IND-REPORT
            sub_Test_Ind_Report();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Test_Ind_Report() throws Exception                                                                                                                   //Natural: TEST-IND-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        if (condition(pnd_Tin_Hold.notEquals(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin())))                                                                             //Natural: IF #TIN-HOLD NE #RPT-TIN
        {
            pnd_Tin_Hold.setValue(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin());                                                                                         //Natural: ASSIGN #TIN-HOLD := #RPT-TIN
            pnd_Test_Ind_Cnt.nadd(1);                                                                                                                                     //Natural: ASSIGN #TEST-IND-CNT := #TEST-IND-CNT + 1
            getReports().write(3, ReportOption.NOTITLE,new TabSetting(12),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin(),new TabSetting(24),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Part_Name()); //Natural: WRITE ( 3 ) 12T #RPT-TIN 24T #RPT-PART-NAME
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=80 PS=60 ZP=ON");
        Global.format(5, "LS=133 PS=58 ZP=ON");
        Global.format(2, "LS=132 PS=100 ZP=OFF");
        Global.format(3, "LS=132 PS=60 ZP=ON");
        Global.format(4, "LS=133 PS=58 ZP=ON");
        Global.format(1, "LS=132 PS=80 ZP=ON");
        Global.format(6, "LS=132 PS=80 ZP=ON");

        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Ws_Pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Ws_Pnd_Sys_Time, 
            new ReportEditMask ("HH:IIAP"),new TabSetting(24),"Tax Withholding and Reporting System",new TabSetting(68),"Page:",getReports().getPageNumberDbs(5), 
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(24),"Mass Mailing Form Bypassed Details",new 
            TabSetting(68),"Report: RPT2",NEWLINE,NEWLINE,"Form Type: 1099R with SUNY",NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Ws_Pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Ws_Pnd_Sys_Time, 
            new ReportEditMask ("HH:IIAP"),new TabSetting(24),"Tax Withholding and Reporting System",new TabSetting(68),"Page:",getReports().getPageNumberDbs(3), 
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(29),"Test Indicator Report",new TabSetting(68),"Report: RPT3",NEWLINE,new 
            TabSetting(29),"Participant Mass Mailing","Form Type: 1099R with SUNY",NEWLINE,NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Ws_Pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Ws_Pnd_Sys_Time, 
            new ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(120),"Page:",getReports().getPageNumberDbs(4), 
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(55),"Mobius Mass Migration",new TabSetting(120),"Report: RPT4",NEWLINE,new 
            TabSetting(51),"Form file Update","Form Type: 1099R with SUNY",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(2, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"Tax/Year",
        		form_Tirf_Tax_Year,"/Form",
        		form_Tirf_Form_Type, new ReportEditMask ("99"),"/Comp",
        		form_Tirf_Company_Cde, new FieldAttributes("LC=�"),"/TIN",
        		form_Tirf_Tin,"/Contract     ",
        		pnd_Contract_Txt,"Pay/ee   ",
        		form_Tirf_Payee_Cde,"Form/Key ",
        		form_Tirf_Key,"/PIN  ",
        		form_Tirf_Pin, new ReportEditMask ("999999999999"),"Mobius/Status",
        		pnd_Ws_Pnd_Mobius_Status_S, new FieldAttributes("LC=��"),"/Message",
        		pnd_Ws_Pnd_Message_S);
    }
}
