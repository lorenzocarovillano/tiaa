/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:34:36 PM
**        * FROM NATURAL PROGRAM : Twrp3101
************************************************************
**        * FILE NAME            : Twrp3101.java
**        * CLASS NAME           : Twrp3101
**        * INSTANCE NAME        : Twrp3101
************************************************************
************************************************************************
*
* PROGRAM : TWRP3101
* NOTE : COPY VERSION OF TWRP0901 EXCEPT THAT PAYMENT RECORDS ARE
*         EXTRACTED BASED ON INTERFACE DATE AND PAYMENT DATE RANGES.
* FUNCTION : CREATES FLAT-FILE3 TO PRINT DETAIL REPORTS.
* AUTHOR   : EDITH 4/21/99
* ------------------------------------------------------------------
* PARAMETER CARD :   INTERFACE-DATE-FROM,INTERFACE-DATE-TO
*                    PAYMENT-DATE-FROM,PAYMENT-DATE-TO
*       EXAMPLE  :   19981201-19990301
*                :   19990101-19990301
* ------------------------------------------------------------------
* UPDATES :
* 7/12/99 EDITH : WRITE IVC-PROTECT FIELDS TO FLAT-FILE3
*                 UPDATED DUE TO PERMANENT PE SOLUTION
*                  INCLUDED NEW COMPANY CODE 'L' FOR TIAA-LIFE
* 12/10/99 EDITH : UPDATED DUE TO UPDATED TWRL0900
*                  INCLUSION OF THE FF. SOURCE CODES :
*                     'TMAL' - CREATED DUE TO DELETED 'APTM'
*                     'TMIL' - CREATED DUE TO DELETED 'IATM'
*                  INCLUDED  RESIDENCY TYPE TO FLAT-FILE3
* 10/14/03:STOWED DUE TO UPDATE ON TWRL0900-ADDED CONTRACT TYPE & IRC-TO
* 11/05/04: MS - TOPS RELEASE 3 - TRUST
* 11/30/04: RM - TOPS RELEASE 3 CHANGES
*           ADD NEW COMPANY CODE 'X' FOR TRUST COMPANY AND NEW SOURCE
*           CODE 'OP', 'NL' AND 'PL'.
* 06/06/06: RM - NAVISYS CHANGES
*           THE NEW SOURCE CODE 'NV' HAS BEEN ADDED FOR NAVISYS PAYMENTS
*           AND THE EXISTING 'NL' AND 'PL'ONLINE MODIFICATION SOURCE
*           CODE WILL BE USED FOR BOTH TOPS AND NAVISYS. SINCE SOURCE
*           CODES ARE FOR SORTING PURPOSES ONLY IN THIS PROGRAM, THERE
*           IS NO NEED FOR ANY CODING CHANGES.
* 10/19/11  MB - ADDED COMPANY 'F'.                    /* MB
* 02/22/12  RC - SUNY/CUNY CHANGES
* 02/18/15: OS - RECOMPILED FOR UPDATED TWRL0900
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3101 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0900 ldaTwrl0900;
    private LdaTwrl3001 ldaTwrl3001;
    private LdaTwrl0902 ldaTwrl0902;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I;
    private DbsField pnd_Rec_Process;
    private DbsField pnd_Rec_Ritten;
    private DbsField pnd_Rec_Read;
    private DbsField pnd_Cref_Cnt;
    private DbsField pnd_Tiaa_Cnt;
    private DbsField pnd_Life_Cnt;
    private DbsField pnd_Tcii_Cnt;
    private DbsField pnd_Trst_Cnt;
    private DbsField pnd_Fsb_Cnt;
    private DbsField pnd_Othr_Cnt;
    private DbsField pnd_Cnt;
    private DbsField pnd_Comp;
    private DbsField pnd_Sc;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Run_Type;
    private DbsField pnd_Intf_Date_Parm;

    private DbsGroup pnd_Intf_Date_Parm__R_Field_1;
    private DbsField pnd_Intf_Date_Parm_Pnd_Intf_Date_From;
    private DbsField pnd_Intf_Date_Parm_Pnd_Filler;
    private DbsField pnd_Intf_Date_Parm_Pnd_Intf_Date_To;
    private DbsField pnd_Pymnt_Date_Parm;

    private DbsGroup pnd_Pymnt_Date_Parm__R_Field_2;
    private DbsField pnd_Pymnt_Date_Parm_Pnd_Pymnt_Date_From;
    private DbsField pnd_Pymnt_Date_Parm_Pnd_Filler1;
    private DbsField pnd_Pymnt_Date_Parm_Pnd_Pymnt_Date_To;
    private DbsField pnd_Tax_Year;
    private DbsField pnd_Parm_Date1;
    private DbsField pnd_Parm_Date2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0900 = new LdaTwrl0900();
        registerRecord(ldaTwrl0900);
        ldaTwrl3001 = new LdaTwrl3001();
        registerRecord(ldaTwrl3001);
        ldaTwrl0902 = new LdaTwrl0902();
        registerRecord(ldaTwrl0902);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Rec_Process = localVariables.newFieldInRecord("pnd_Rec_Process", "#REC-PROCESS", FieldType.NUMERIC, 9);
        pnd_Rec_Ritten = localVariables.newFieldInRecord("pnd_Rec_Ritten", "#REC-RITTEN", FieldType.NUMERIC, 9);
        pnd_Rec_Read = localVariables.newFieldInRecord("pnd_Rec_Read", "#REC-READ", FieldType.NUMERIC, 9);
        pnd_Cref_Cnt = localVariables.newFieldArrayInRecord("pnd_Cref_Cnt", "#CREF-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Tiaa_Cnt = localVariables.newFieldArrayInRecord("pnd_Tiaa_Cnt", "#TIAA-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Life_Cnt = localVariables.newFieldArrayInRecord("pnd_Life_Cnt", "#LIFE-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Tcii_Cnt = localVariables.newFieldArrayInRecord("pnd_Tcii_Cnt", "#TCII-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Trst_Cnt = localVariables.newFieldArrayInRecord("pnd_Trst_Cnt", "#TRST-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Fsb_Cnt = localVariables.newFieldArrayInRecord("pnd_Fsb_Cnt", "#FSB-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Othr_Cnt = localVariables.newFieldArrayInRecord("pnd_Othr_Cnt", "#OTHR-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 1);
        pnd_Comp = localVariables.newFieldInRecord("pnd_Comp", "#COMP", FieldType.STRING, 1);
        pnd_Sc = localVariables.newFieldInRecord("pnd_Sc", "#SC", FieldType.STRING, 2);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 1);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.NUMERIC, 2);
        pnd_Run_Type = localVariables.newFieldInRecord("pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 24);
        pnd_Intf_Date_Parm = localVariables.newFieldInRecord("pnd_Intf_Date_Parm", "#INTF-DATE-PARM", FieldType.STRING, 17);

        pnd_Intf_Date_Parm__R_Field_1 = localVariables.newGroupInRecord("pnd_Intf_Date_Parm__R_Field_1", "REDEFINE", pnd_Intf_Date_Parm);
        pnd_Intf_Date_Parm_Pnd_Intf_Date_From = pnd_Intf_Date_Parm__R_Field_1.newFieldInGroup("pnd_Intf_Date_Parm_Pnd_Intf_Date_From", "#INTF-DATE-FROM", 
            FieldType.STRING, 8);
        pnd_Intf_Date_Parm_Pnd_Filler = pnd_Intf_Date_Parm__R_Field_1.newFieldInGroup("pnd_Intf_Date_Parm_Pnd_Filler", "#FILLER", FieldType.STRING, 1);
        pnd_Intf_Date_Parm_Pnd_Intf_Date_To = pnd_Intf_Date_Parm__R_Field_1.newFieldInGroup("pnd_Intf_Date_Parm_Pnd_Intf_Date_To", "#INTF-DATE-TO", FieldType.STRING, 
            8);
        pnd_Pymnt_Date_Parm = localVariables.newFieldInRecord("pnd_Pymnt_Date_Parm", "#PYMNT-DATE-PARM", FieldType.STRING, 17);

        pnd_Pymnt_Date_Parm__R_Field_2 = localVariables.newGroupInRecord("pnd_Pymnt_Date_Parm__R_Field_2", "REDEFINE", pnd_Pymnt_Date_Parm);
        pnd_Pymnt_Date_Parm_Pnd_Pymnt_Date_From = pnd_Pymnt_Date_Parm__R_Field_2.newFieldInGroup("pnd_Pymnt_Date_Parm_Pnd_Pymnt_Date_From", "#PYMNT-DATE-FROM", 
            FieldType.STRING, 8);
        pnd_Pymnt_Date_Parm_Pnd_Filler1 = pnd_Pymnt_Date_Parm__R_Field_2.newFieldInGroup("pnd_Pymnt_Date_Parm_Pnd_Filler1", "#FILLER1", FieldType.STRING, 
            1);
        pnd_Pymnt_Date_Parm_Pnd_Pymnt_Date_To = pnd_Pymnt_Date_Parm__R_Field_2.newFieldInGroup("pnd_Pymnt_Date_Parm_Pnd_Pymnt_Date_To", "#PYMNT-DATE-TO", 
            FieldType.STRING, 8);
        pnd_Tax_Year = localVariables.newFieldInRecord("pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Parm_Date1 = localVariables.newFieldInRecord("pnd_Parm_Date1", "#PARM-DATE1", FieldType.STRING, 8);
        pnd_Parm_Date2 = localVariables.newFieldInRecord("pnd_Parm_Date2", "#PARM-DATE2", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0900.initializeValues();
        ldaTwrl3001.initializeValues();
        ldaTwrl0902.initializeValues();

        localVariables.reset();
        pnd_X.setInitialValue(0);
        pnd_Y.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3101() throws Exception
    {
        super("Twrp3101");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp3101|Main");
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 01 #INFO-FF1
                while (condition(getWorkFiles().read(1, ldaTwrl3001.getPnd_Info_Ff1())))
                {
                    pnd_Tax_Year.setValue(ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Tax_Yr());                                                                                  //Natural: ASSIGN #TAX-YEAR := #INFO-FF1.#FF1-TAX-YR
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                //*                             (VALIDATE PARAMETER CARD)
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Intf_Date_Parm);                                                                                   //Natural: INPUT #INTF-DATE-PARM
                pnd_Parm_Date1.setValue(pnd_Intf_Date_Parm_Pnd_Intf_Date_From);                                                                                           //Natural: ASSIGN #PARM-DATE1 := #INTF-DATE-FROM
                pnd_Parm_Date2.setValue(pnd_Intf_Date_Parm_Pnd_Intf_Date_To);                                                                                             //Natural: ASSIGN #PARM-DATE2 := #INTF-DATE-TO
                                                                                                                                                                          //Natural: PERFORM CHECK-PARM-DATE
                sub_Check_Parm_Date();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                DbsUtil.invokeInput(setInputStatus(INPUT_2), this, pnd_Pymnt_Date_Parm);                                                                                  //Natural: INPUT #PYMNT-DATE-PARM
                pnd_Parm_Date1.setValue(pnd_Pymnt_Date_Parm_Pnd_Pymnt_Date_From);                                                                                         //Natural: ASSIGN #PARM-DATE1 := #PYMNT-DATE-FROM
                pnd_Parm_Date2.setValue(pnd_Pymnt_Date_Parm_Pnd_Pymnt_Date_To);                                                                                           //Natural: ASSIGN #PARM-DATE2 := #PYMNT-DATE-TO
                                                                                                                                                                          //Natural: PERFORM CHECK-PARM-DATE
                sub_Check_Parm_Date();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*                             (WRITE PARAMS TO FF1)
                ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Tax_Yr().setValue(pnd_Tax_Year);                                                                                      //Natural: ASSIGN #FF1-TAX-YR := #TAX-YEAR
                ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Intf_Date_Fr().setValue(pnd_Intf_Date_Parm_Pnd_Intf_Date_From);                                                       //Natural: ASSIGN #FF1-INTF-DATE-FR := #INTF-DATE-FROM
                ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Intf_Date_To().setValue(pnd_Intf_Date_Parm_Pnd_Intf_Date_To);                                                         //Natural: ASSIGN #FF1-INTF-DATE-TO := #INTF-DATE-TO
                ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Pymt_Date_Fr().setValue(pnd_Pymnt_Date_Parm_Pnd_Pymnt_Date_From);                                                     //Natural: ASSIGN #FF1-PYMT-DATE-FR := #PYMNT-DATE-FROM
                ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Pymt_Date_To().setValue(pnd_Pymnt_Date_Parm_Pnd_Pymnt_Date_To);                                                       //Natural: ASSIGN #FF1-PYMT-DATE-TO := #PYMNT-DATE-TO
                getWorkFiles().write(4, false, ldaTwrl3001.getPnd_Info_Ff1());                                                                                            //Natural: WRITE WORK FILE 04 #INFO-FF1
                READWORK02:                                                                                                                                               //Natural: READ WORK FILE 02 RECORD #XTAXYR-F94
                while (condition(getWorkFiles().read(2, ldaTwrl0900.getPnd_Xtaxyr_F94())))
                {
                    pnd_Rec_Read.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #REC-READ
                    pnd_Cnt.setValue(1);                                                                                                                                  //Natural: ASSIGN #CNT := 1
                                                                                                                                                                          //Natural: PERFORM COMPANY-CNT
                    sub_Company_Cnt();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    FOR01:                                                                                                                                                //Natural: FOR #I 1 #XTAXYR-F94.#C-TWRPYMNT-PAYMENTS
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments())); pnd_I.nadd(1))
                    {
                        if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Intfce_Dte().getValue(pnd_I).greaterOrEqual(pnd_Intf_Date_Parm_Pnd_Intf_Date_From)     //Natural: IF #XTAXYR-F94.TWRPYMNT-PYMNT-INTFCE-DTE ( #I ) = #INTF-DATE-FROM THRU #INTF-DATE-TO AND #XTAXYR-F94.TWRPYMNT-PYMNT-DTE ( #I ) = #PYMNT-DATE-FROM THRU #PYMNT-DATE-TO
                            && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Intfce_Dte().getValue(pnd_I).lessOrEqual(pnd_Intf_Date_Parm_Pnd_Intf_Date_To) 
                            && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Dte().getValue(pnd_I).greaterOrEqual(pnd_Pymnt_Date_Parm_Pnd_Pymnt_Date_From) 
                            && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Dte().getValue(pnd_I).lessOrEqual(pnd_Pymnt_Date_Parm_Pnd_Pymnt_Date_To)))
                        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-DETAIL
                            sub_Process_Detail();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //* *-----------                                                                                                                                      //Natural: AT END OF DATA
                }                                                                                                                                                         //Natural: END-WORK
                READWORK02_Exit:
                if (condition(getWorkFiles().getAtEndOfData()))
                {
                    //* *---------------
                    //*  MB
                    //*  MB
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(34),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 01 ) NOTITLE NOHDR *INIT-USER '-' *PROGRAM 34T 'TAX WITHHOLDING AND REPORTING SYSTEM' 119T 'PAGE' *PAGE-NUMBER ( 01 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 42T 'CREATED FLAT FILE3' 90T 'INTERFACE DATE ' #INTF-DATE-FROM 'THRU' #INTF-DATE-TO / 'RUNTIME : ' *TIMX 44T 'TAX YEAR ' #TAX-YEAR 90T '  PAYMENT DATE ' #PYMNT-DATE-FROM 'THRU' #PYMNT-DATE-TO / 44T 'CONTROL TOTALS' /// 'TOTAL RECORDS READ..........: ' #REC-READ / '  1.  CREF Records..........: ' #CREF-CNT ( 1 ) / '  2.  TIAA Records..........: ' #TIAA-CNT ( 1 ) / '  3.  LIFE Records..........: ' #LIFE-CNT ( 1 ) / '  4.  TCII Records..........: ' #TCII-CNT ( 1 ) / '  5.  TRUST Records.........: ' #TRST-CNT ( 1 ) / '  6.  FSB Records...........: ' #FSB-CNT ( 1 ) / '  7.  Others................: ' #OTHR-CNT ( 1 ) /// 'TOTAL RECORDS WRITTEN.......: ' #REC-RITTEN / '  1.  CREF Records..........: ' #CREF-CNT ( 2 ) / '  2.  TIAA Records..........: ' #TIAA-CNT ( 2 ) / '  3.  LIFE Records..........: ' #LIFE-CNT ( 2 ) / '  4.  TCII Records..........: ' #TCII-CNT ( 2 ) / '  5.  TRUST Records.........: ' #TRST-CNT ( 2 ) / '  6.  FSB Records...........: ' #FSB-CNT ( 2 ) / '  7.  Others................: ' #OTHR-CNT ( 2 )
                        TabSetting(119),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(42),"CREATED FLAT FILE3",new TabSetting(90),"INTERFACE DATE ",pnd_Intf_Date_Parm_Pnd_Intf_Date_From,"THRU",pnd_Intf_Date_Parm_Pnd_Intf_Date_To,NEWLINE,"RUNTIME : ",Global.getTIMX(),new 
                        TabSetting(44),"TAX YEAR ",pnd_Tax_Year,new TabSetting(90),"  PAYMENT DATE ",pnd_Pymnt_Date_Parm_Pnd_Pymnt_Date_From,"THRU",pnd_Pymnt_Date_Parm_Pnd_Pymnt_Date_To,NEWLINE,new 
                        TabSetting(44),"CONTROL TOTALS",NEWLINE,NEWLINE,NEWLINE,"TOTAL RECORDS READ..........: ",pnd_Rec_Read,NEWLINE,"  1.  CREF Records..........: ",
                        pnd_Cref_Cnt.getValue(1),NEWLINE,"  2.  TIAA Records..........: ",pnd_Tiaa_Cnt.getValue(1),NEWLINE,"  3.  LIFE Records..........: ",
                        pnd_Life_Cnt.getValue(1),NEWLINE,"  4.  TCII Records..........: ",pnd_Tcii_Cnt.getValue(1),NEWLINE,"  5.  TRUST Records.........: ",
                        pnd_Trst_Cnt.getValue(1),NEWLINE,"  6.  FSB Records...........: ",pnd_Fsb_Cnt.getValue(1),NEWLINE,"  7.  Others................: ",
                        pnd_Othr_Cnt.getValue(1),NEWLINE,NEWLINE,NEWLINE,"TOTAL RECORDS WRITTEN.......: ",pnd_Rec_Ritten,NEWLINE,"  1.  CREF Records..........: ",
                        pnd_Cref_Cnt.getValue(2),NEWLINE,"  2.  TIAA Records..........: ",pnd_Tiaa_Cnt.getValue(2),NEWLINE,"  3.  LIFE Records..........: ",
                        pnd_Life_Cnt.getValue(2),NEWLINE,"  4.  TCII Records..........: ",pnd_Tcii_Cnt.getValue(2),NEWLINE,"  5.  TRUST Records.........: ",
                        pnd_Trst_Cnt.getValue(2),NEWLINE,"  6.  FSB Records...........: ",pnd_Fsb_Cnt.getValue(2),NEWLINE,"  7.  Others................: ",
                        pnd_Othr_Cnt.getValue(2));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-ENDDATA
                if (Global.isEscape()) return;
                //* *------
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-DETAIL
                //* *-------------------------------
                //*                                       ( ELEMENTARY FIELDS )
                //*                                       ( PERIODIC FIELDS )
                //*  TOPS RELEASE 3 CHANGES: 'ML' --> 'NL' , 'OL' --> 'PL'    11/30/04  RM
                //* *------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPANY-CNT
                //* *------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PARM-DATE
                //* *------------
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Process_Detail() throws Exception                                                                                                                    //Natural: PROCESS-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Id_Nbr().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr());                                                 //Natural: ASSIGN #FF3-TAX-ID-NBR := #XTAXYR-F94.TWRPYMNT-TAX-ID-NBR
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde());                                              //Natural: ASSIGN #FF3-COMPANY-CODE := #XTAXYR-F94.TWRPYMNT-COMPANY-CDE
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Citizenship());                                       //Natural: ASSIGN #FF3-TAX-CITIZENSHIP := #XTAXYR-F94.TWRPYMNT-TAX-CITIZENSHIP
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Contract_Nbr().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr());                                             //Natural: ASSIGN #FF3-CONTRACT-NBR := #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Payee_Cde().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payee_Cde());                                                   //Natural: ASSIGN #FF3-PAYEE-CDE := #XTAXYR-F94.TWRPYMNT-PAYEE-CDE
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde());                                     //Natural: ASSIGN #FF3-DISTRIBUTION-CDE := #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Type().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Type());                                         //Natural: ASSIGN #FF3-RESIDENCY-TYPE := #XTAXYR-F94.TWRPYMNT-RESIDENCY-TYPE
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code());                                         //Natural: ASSIGN #FF3-RESIDENCY-CODE := #XTAXYR-F94.TWRPYMNT-RESIDENCY-CODE
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Ind().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Ind().getValue(pnd_I));                                       //Natural: ASSIGN #FF3-IVC-IND := #XTAXYR-F94.TWRPYMNT-IVC-IND ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Protect().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Protect().getValue(pnd_I).getBoolean());                  //Natural: ASSIGN #FF3-IVC-PROTECT := #XTAXYR-F94.TWRPYMNT-IVC-PROTECT ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Pymnt_Date().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Dte().getValue(pnd_I));                                  //Natural: ASSIGN #FF3-PYMNT-DATE := #XTAXYR-F94.TWRPYMNT-PYMNT-DTE ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Intfce_Dte().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Intfce_Dte().getValue(pnd_I));                           //Natural: ASSIGN #FF3-INTFCE-DTE := #XTAXYR-F94.TWRPYMNT-PYMNT-INTFCE-DTE ( #I )
        //*  (CHECKING OF SOURCE CODES ARE DONE FOR SORTING PURPOSES ONLY, SO
        //*   AS TO PRINT PURE MANUAL RECORDS AT THE END OF THE DETAIL REPORT)
        short decideConditionsMet346 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) = '  ' AND #XTAXYR-F94.TWRPYMNT-UPDTE-SRCE-CDE ( #I ) = 'ML'
        if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).equals("  ") && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I).equals("ML")))
        {
            decideConditionsMet346++;
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).setValue("ZZ");                                                                        //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) := 'ZZ'
        }                                                                                                                                                                 //Natural: WHEN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) = 'ML' AND #XTAXYR-F94.TWRPYMNT-UPDTE-SRCE-CDE ( #I ) = 'OL'
        else if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).equals("ML") && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I).equals("OL")))
        {
            decideConditionsMet346++;
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).setValue("ZZ");                                                                        //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) := 'ZZ'
        }                                                                                                                                                                 //Natural: WHEN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) = '  ' AND #XTAXYR-F94.TWRPYMNT-UPDTE-SRCE-CDE ( #I ) = 'NL'
        else if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).equals("  ") && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I).equals("NL")))
        {
            decideConditionsMet346++;
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).setValue("YY");                                                                        //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) := 'YY'
        }                                                                                                                                                                 //Natural: WHEN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) = 'NL' AND #XTAXYR-F94.TWRPYMNT-UPDTE-SRCE-CDE ( #I ) = 'PL'
        else if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).equals("NL") && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I).equals("PL")))
        {
            decideConditionsMet346++;
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).setValue("YY");                                                                        //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) := 'YY'
        }                                                                                                                                                                 //Natural: WHEN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) = 'TM' AND #XTAXYR-F94.TWRPYMNT-UPDTE-SRCE-CDE ( #I ) = 'AL'
        else if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).equals("TM") && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I).equals("AL")))
        {
            decideConditionsMet346++;
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).setValue("AP");                                                                        //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) := 'AP'
        }                                                                                                                                                                 //Natural: WHEN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) = 'TM' AND #XTAXYR-F94.TWRPYMNT-UPDTE-SRCE-CDE ( #I ) = 'IL'
        else if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).equals("TM") && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I).equals("IL")))
        {
            decideConditionsMet346++;
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).setValue("IA");                                                                        //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) := 'IA'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I),  //Natural: COMPRESS #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) #XTAXYR-F94.TWRPYMNT-UPDTE-SRCE-CDE ( #I ) INTO #FF3-SOURCE-CODE LEAVING NO SPACE
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I)));
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Payset_Type().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payment_Type().getValue(pnd_I),  //Natural: COMPRESS #XTAXYR-F94.TWRPYMNT-PAYMENT-TYPE ( #I ) #XTAXYR-F94.TWRPYMNT-SETTLE-TYPE ( #I ) INTO #FF3-PAYSET-TYPE LEAVING NO SPACE
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_I)));
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_I));                                   //Natural: ASSIGN #FF3-GROSS-AMT := #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_I));                                       //Natural: ASSIGN #FF3-IVC-AMT := #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Int_Amt().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_I));                                       //Natural: ASSIGN #FF3-INT-AMT := #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Fed_Wthld_Amt().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_I));                           //Natural: ASSIGN #FF3-FED-WTHLD-AMT := #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Nra_Wthld_Amt().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Nra_Wthld_Amt().getValue(pnd_I));                           //Natural: ASSIGN #FF3-NRA-WTHLD-AMT := #XTAXYR-F94.TWRPYMNT-NRA-WTHLD-AMT ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Can_Wthld_Amt().getValue(pnd_I));                           //Natural: ASSIGN #FF3-CAN-WTHLD-AMT := #XTAXYR-F94.TWRPYMNT-CAN-WTHLD-AMT ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_I));                               //Natural: ASSIGN #FF3-STATE-WTHLD := #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Local_Wthld().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Local_Wthld().getValue(pnd_I));                               //Natural: ASSIGN #FF3-LOCAL-WTHLD := #XTAXYR-F94.TWRPYMNT-LOCAL-WTHLD ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Plan().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Omni_Plan().getValue(pnd_I));                                             //Natural: ASSIGN #FF3-PLAN := #XTAXYR-F94.#OMNI-PLAN ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Subplan().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Omni_Subplan().getValue(pnd_I));                                       //Natural: ASSIGN #FF3-SUBPLAN := #XTAXYR-F94.#OMNI-SUBPLAN ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Orgntng_Cntrct().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgntng_Contract().getValue(pnd_I));                       //Natural: ASSIGN #FF3-ORGNTNG-CNTRCT := #XTAXYR-F94.TWRPYMNT-ORGNTNG-CONTRACT ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Orgntng_Subplan().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgntng_Subplan().getValue(pnd_I));                       //Natural: ASSIGN #FF3-ORGNTNG-SUBPLAN := #XTAXYR-F94.TWRPYMNT-ORGNTNG-SUBPLAN ( #I )
        if (condition(DbsUtil.maskMatches(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Irr_Amt().getValue(pnd_I),"NNNNNNNNN")))                                                 //Natural: IF #XTAXYR-F94.TWRPYMNT-IRR-AMT ( #I ) = MASK ( NNNNNNNNN )
        {
            ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Irr_Amt().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Irr_Amt().getValue(pnd_I));                                   //Natural: ASSIGN #FF3-IRR-AMT := #XTAXYR-F94.TWRPYMNT-IRR-AMT ( #I )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Irr_Amt().setValue(0);                                                                                                  //Natural: ASSIGN #FF3-IRR-AMT := 0
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().write(3, false, ldaTwrl0902.getPnd_Flat_File3());                                                                                                  //Natural: WRITE WORK FILE 03 #FLAT-FILE3
        pnd_Rec_Ritten.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #REC-RITTEN
        pnd_Cnt.setValue(2);                                                                                                                                              //Natural: ASSIGN #CNT := 2
                                                                                                                                                                          //Natural: PERFORM COMPANY-CNT
        sub_Company_Cnt();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    }
    private void sub_Company_Cnt() throws Exception                                                                                                                       //Natural: COMPANY-CNT
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------
        //*  MB
        short decideConditionsMet404 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #XTAXYR-F94.TWRPYMNT-COMPANY-CDE;//Natural: VALUE 'C'
        if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("C"))))
        {
            decideConditionsMet404++;
            pnd_Cref_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #CREF-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("T"))))
        {
            decideConditionsMet404++;
            pnd_Tiaa_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #TIAA-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("L"))))
        {
            decideConditionsMet404++;
            pnd_Life_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #LIFE-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("S"))))
        {
            decideConditionsMet404++;
            pnd_Tcii_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #TCII-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("X"))))
        {
            decideConditionsMet404++;
            pnd_Trst_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #TRST-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("F"))))
        {
            decideConditionsMet404++;
            pnd_Fsb_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                        //Natural: ADD 1 TO #FSB-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Othr_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #OTHR-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Check_Parm_Date() throws Exception                                                                                                                   //Natural: CHECK-PARM-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------
        if (condition(DbsUtil.maskMatches(pnd_Parm_Date1,"YYYYMMDD")))                                                                                                    //Natural: IF #PARM-DATE1 = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "********  ERROR!! ******* ERROR!! ************",NEWLINE,"    THE VALID FORMAT OF ",pnd_Parm_Date1,NEWLINE,"            DATE IS GYYYYMMDDG. ", //Natural: WRITE '********  ERROR!! ******* ERROR!! ************' / '    THE VALID FORMAT OF ' #PARM-DATE1 / '            DATE IS ''YYYYMMDD''. ' / '**********************************************'
                NEWLINE,"**********************************************");
            if (Global.isEscape()) return;
            ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Tax_Yr().setValue(pnd_Tax_Year);                                                                                          //Natural: ASSIGN #FF1-TAX-YR := #TAX-YEAR
            getWorkFiles().write(4, false, ldaTwrl3001.getPnd_Info_Ff1());                                                                                                //Natural: WRITE WORK FILE 04 #INFO-FF1
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(pnd_Parm_Date2,"YYYYMMDD")))                                                                                                    //Natural: IF #PARM-DATE2 = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "********  ERROR!! ******* ERROR!! ************",NEWLINE,"    THE VALID FORMAT OF ",pnd_Parm_Date2,NEWLINE,"          DATE IS  GYYYYMMDDG. ", //Natural: WRITE '********  ERROR!! ******* ERROR!! ************' / '    THE VALID FORMAT OF ' #PARM-DATE2 / '          DATE IS  ''YYYYMMDD''. ' / '**********************************************'
                NEWLINE,"**********************************************");
            if (Global.isEscape()) return;
            ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Tax_Yr().setValue(pnd_Tax_Year);                                                                                          //Natural: ASSIGN #FF1-TAX-YR := #TAX-YEAR
            getWorkFiles().write(4, false, ldaTwrl3001.getPnd_Info_Ff1());                                                                                                //Natural: WRITE WORK FILE 04 #INFO-FF1
            DbsUtil.terminate(91);  if (true) return;                                                                                                                     //Natural: TERMINATE 91
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
    }
}
