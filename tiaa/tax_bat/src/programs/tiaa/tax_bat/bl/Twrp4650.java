/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:40:09 PM
**        * FROM NATURAL PROGRAM : Twrp4650
************************************************************
**        * FILE NAME            : Twrp4650.java
**        * CLASS NAME           : Twrp4650
**        * INSTANCE NAME        : Twrp4650
************************************************************
***********************************************************************
* PROGRAM  : TWRP4650
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : UPDATES THE CONTRIBUTIONS DATA BASE FILE.
* CREATED  : 06 / 26 / 1999.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM EDITS & UPDATES THE CONTRIBUTIONS DATA BASE FILE.
* MODIFIED : 08/23/05 J.ROTHOLZ - MODIFIED CODE TO HANDLE SEP IRA
*            DATA IN FEED FROM OMNIPLUS (TOPS 6.5)
*          : 01/12/06 J.ROTHOLZ - MODIFIED CONDITIONS FOR PRODUCING
*            ADJUSTED & ACCEPTED CONTRIBUTIONS REPORT TO INCLUDE P1.
*
* 04/27/2017  WEBBJ - PIN EXPANSION SCAN JW0427
* 12/21/2017  RAHUL - TAX ID TYPE LOOKUP FROM PARTICIPANT FILE
*                   - TAG: DASRAH
* 09/25/2020  VIKRAM - 2 FIELDS ADDED IN TWR-IRA  TWRC-RMD-REQUIRED-FLAG
*                  AND  TWRC-RMD-DECEDENT-NAME : TAG - SECURE-ACT
* 11/24/2020 - PALDE - RESTOW FOR IRS REPORTING 2020 - 5498 CHANGES
***********************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp4650 extends BLNatBase
{
    // Data Areas
    private LdaTwrl462a ldaTwrl462a;
    private LdaTwrl465b ldaTwrl465b;
    private LdaTwrl465c ldaTwrl465c;
    private LdaTwrl820e ldaTwrl820e;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_oldira;
    private DbsField oldira_Twrc_Unique_Id;

    private DataAccessProgramView vw_active;
    private DbsField active_Twrc_Tax_Year;
    private DbsField active_Twrc_Tax_Id;
    private DbsField active_Twrc_Contract;
    private DbsField active_Twrc_Payee;
    private DbsField active_Twrc_Status;

    private DataAccessProgramView vw_inactive;
    private DbsField inactive_Twrc_Status;
    private DbsField inactive_Twrc_Update_User;
    private DbsField inactive_Twrc_Update_Date;
    private DbsField inactive_Twrc_Update_Time;
    private DbsField inactive_Twrc_Source;
    private DbsField inactive_Twrc_Update_Source;
    private DbsField inactive_Twrc_Form_Period;

    private DataAccessProgramView vw_prtcpnt;
    private DbsField prtcpnt_Twrparti_Tax_Id;
    private DbsField prtcpnt_Twrparti_Tax_Id_Type;
    private DbsField prtcpnt_Twrparti_Status;
    private DbsField prtcpnt_Twrparti_Part_Eff_End_Dte;
    private DbsField prtcpnt_Twrparti_Citation_Ind;

    private DataAccessProgramView vw_updpar;
    private DbsField updpar_Twrparti_Citation_Ind;

    private DataAccessProgramView vw_cntlr;
    private DbsField cntlr_Tircntl_Tbl_Nbr;
    private DbsField cntlr_Tircntl_Tax_Year;
    private DbsField cntlr_Tircntl_Rpt_Source_Code;
    private DbsField cntlr_Tircntl_Company_Cde;
    private DbsField cntlr_Tircntl_5_Y_Sc_Co_To_Frm_Sp;

    private DataAccessProgramView vw_cntlu;
    private DbsField cntlu_Tircntl_Frm_Intrfce_Dte;
    private DbsField cntlu_Tircntl_To_Intrfce_Dte;
    private DbsField cntlu_Tircntl_Rpt_Update_Dte_Time;
    private DbsField pnd_Twrc_S1_Yr_Tin_Contract;

    private DbsGroup pnd_Twrc_S1_Yr_Tin_Contract__R_Field_1;
    private DbsField pnd_Twrc_S1_Yr_Tin_Contract_Pnd_S1_Tax_Year;
    private DbsField pnd_Twrc_S1_Yr_Tin_Contract_Pnd_S1_Tax_Id;
    private DbsField pnd_Twrc_S1_Yr_Tin_Contract_Pnd_S1_Contract;
    private DbsField pnd_Twrc_S1_Yr_Tin_Contract_Pnd_S1_Payee;
    private DbsField pnd_Twrc_Sys_Dte_Time;

    private DbsGroup pnd_Twrc_Sys_Dte_Time__R_Field_2;
    private DbsField pnd_Twrc_Sys_Dte_Time_Pnd_Twrc_Sys_Dte_Time_N;

    private DbsGroup pnd_Twrc_Sys_Dte_Time__R_Field_3;
    private DbsField pnd_Twrc_Sys_Dte_Time_Pnd_Twrc_Sys_Dte;
    private DbsField pnd_Twrc_Sys_Dte_Time_Pnd_Twrc_Sys_Time;
    private DbsField pnd_Twrparti_Curr_Rec_Sd;

    private DbsGroup pnd_Twrparti_Curr_Rec_Sd__R_Field_4;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Tax_Id;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Status;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Part_Eff_End_Dte;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp;

    private DbsGroup pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_5;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_5;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Year;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Source_Code;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Company;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_To_Ccyymmdd;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Frm_Ccyymmdd;
    private DbsField pnd_Tircntl_Tax_Year_A;

    private DbsGroup pnd_Tircntl_Tax_Year_A__R_Field_6;
    private DbsField pnd_Tircntl_Tax_Year_A_Pnd_Tircntl_Tax_Year;
    private DbsField pnd_Tircntl_Rpt_Source_Code;
    private DbsField pnd_Tircntl_Frm_Intrfce_Dte_A;

    private DbsGroup pnd_Tircntl_Frm_Intrfce_Dte_A__R_Field_7;
    private DbsField pnd_Tircntl_Frm_Intrfce_Dte_A_Pnd_Tircntl_Frm_Intrfce_Dte;
    private DbsField pnd_Tircntl_To_Intrfce_Dte_A;

    private DbsGroup pnd_Tircntl_To_Intrfce_Dte_A__R_Field_8;
    private DbsField pnd_Tircntl_To_Intrfce_Dte_A_Pnd_Tircntl_To_Intrfce_Dte;
    private DbsField pnd_Citation_Message_Code;
    private DbsField pnd_Ira_Isn;
    private DbsField pnd_Old_Ira_Isn;
    private DbsField pnd_Prev_Form;
    private DbsField pnd_Part_Ein_Found;
    private DbsField pnd_Part_Tax_Id_Type;
    private DbsField pnd_Timn_N;

    private DbsGroup pnd_Timn_N__R_Field_9;
    private DbsField pnd_Timn_N_Pnd_Timn_A;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Bypass_Ctr;
    private DbsField pnd_Store_Ctr;
    private DbsField pnd_Adjust_Ctr;
    private DbsField pnd_Update_New_Ctr;
    private DbsField pnd_Update_Int_Ctr;
    private DbsField pnd_Contribution_Processed;
    private DbsField pnd_Summary_Found;
    private DbsField pnd_Date_Found;
    private DbsField pnd_Source_Total_Header;
    private DbsField pnd_Prev_Origin_Form;
    private DbsField pnd_Header_Date;
    private DbsField pnd_Interface_Date;
    private DbsField pnd_T_Trans_Count;
    private DbsField pnd_T_Classic_Amt;
    private DbsField pnd_T_Roth_Amt;
    private DbsField pnd_T_Rollover_Amt;
    private DbsField pnd_T_Rechar_Amt;
    private DbsField pnd_T_Sep_Amt;
    private DbsField pnd_T_Fmv_Amt;
    private DbsField pnd_T_Roth_Conv_Amt;
    private DbsField pnd_C_Trans_Count;
    private DbsField pnd_C_Classic_Amt;
    private DbsField pnd_C_Roth_Amt;
    private DbsField pnd_C_Rollover_Amt;
    private DbsField pnd_C_Rechar_Amt;
    private DbsField pnd_C_Sep_Amt;
    private DbsField pnd_C_Fmv_Amt;
    private DbsField pnd_C_Roth_Conv_Amt;
    private DbsField pnd_L_Trans_Count;
    private DbsField pnd_L_Classic_Amt;
    private DbsField pnd_L_Roth_Amt;
    private DbsField pnd_L_Rollover_Amt;
    private DbsField pnd_L_Rechar_Amt;
    private DbsField pnd_L_Sep_Amt;
    private DbsField pnd_L_Fmv_Amt;
    private DbsField pnd_L_Roth_Conv_Amt;
    private DbsField pnd_S_Trans_Count;
    private DbsField pnd_S_Classic_Amt;
    private DbsField pnd_S_Roth_Amt;
    private DbsField pnd_S_Rollover_Amt;
    private DbsField pnd_S_Rechar_Amt;
    private DbsField pnd_S_Sep_Amt;
    private DbsField pnd_S_Fmv_Amt;
    private DbsField pnd_S_Roth_Conv_Amt;
    private DbsField pnd_Summary_Record;
    private DbsField pnd_Payment_Found;
    private DbsField pnd_Update_Payment;
    private DbsField pnd_Twrc_Create_Date;
    private DbsField pnd_Twrc_Create_Date_Inv;
    private DbsField pnd_Twrc_Create_Time;
    private DbsField pnd_Twrc_Create_Time_Inv;
    private DbsField pnd_Twrc_Lu_Ts;
    private DbsField pnd_Et_Ctr;
    private DbsField pnd_T;
    private DbsField i1;
    private DbsField i2;
    private DbsField i3;
    private DbsField i;
    private DbsField j;
    private DbsField k;
    private DbsField z;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl462a = new LdaTwrl462a();
        registerRecord(ldaTwrl462a);
        ldaTwrl465b = new LdaTwrl465b();
        registerRecord(ldaTwrl465b);
        ldaTwrl465c = new LdaTwrl465c();
        registerRecord(ldaTwrl465c);
        registerRecord(ldaTwrl465c.getVw_newira());
        ldaTwrl820e = new LdaTwrl820e();
        registerRecord(ldaTwrl820e);
        registerRecord(ldaTwrl820e.getVw_cntl());

        // Local Variables
        localVariables = new DbsRecord();

        vw_oldira = new DataAccessProgramView(new NameInfo("vw_oldira", "OLDIRA"), "TWR_IRA", "TWR_IRA");
        oldira_Twrc_Unique_Id = vw_oldira.getRecord().newFieldInGroup("oldira_Twrc_Unique_Id", "TWRC-UNIQUE-ID", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "TWRC_UNIQUE_ID");
        registerRecord(vw_oldira);

        vw_active = new DataAccessProgramView(new NameInfo("vw_active", "ACTIVE"), "TWR_IRA", "TWR_IRA");
        active_Twrc_Tax_Year = vw_active.getRecord().newFieldInGroup("active_Twrc_Tax_Year", "TWRC-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TWRC_TAX_YEAR");
        active_Twrc_Tax_Id = vw_active.getRecord().newFieldInGroup("active_Twrc_Tax_Id", "TWRC-TAX-ID", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TWRC_TAX_ID");
        active_Twrc_Contract = vw_active.getRecord().newFieldInGroup("active_Twrc_Contract", "TWRC-CONTRACT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_CONTRACT");
        active_Twrc_Payee = vw_active.getRecord().newFieldInGroup("active_Twrc_Payee", "TWRC-PAYEE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRC_PAYEE");
        active_Twrc_Status = vw_active.getRecord().newFieldInGroup("active_Twrc_Status", "TWRC-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRC_STATUS");
        registerRecord(vw_active);

        vw_inactive = new DataAccessProgramView(new NameInfo("vw_inactive", "INACTIVE"), "TWR_IRA", "TWR_IRA");
        inactive_Twrc_Status = vw_inactive.getRecord().newFieldInGroup("inactive_Twrc_Status", "TWRC-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRC_STATUS");
        inactive_Twrc_Update_User = vw_inactive.getRecord().newFieldInGroup("inactive_Twrc_Update_User", "TWRC-UPDATE-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_UPDATE_USER");
        inactive_Twrc_Update_Date = vw_inactive.getRecord().newFieldInGroup("inactive_Twrc_Update_Date", "TWRC-UPDATE-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_UPDATE_DATE");
        inactive_Twrc_Update_Time = vw_inactive.getRecord().newFieldInGroup("inactive_Twrc_Update_Time", "TWRC-UPDATE-TIME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TWRC_UPDATE_TIME");
        inactive_Twrc_Source = vw_inactive.getRecord().newFieldInGroup("inactive_Twrc_Source", "TWRC-SOURCE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TWRC_SOURCE");
        inactive_Twrc_Update_Source = vw_inactive.getRecord().newFieldInGroup("inactive_Twrc_Update_Source", "TWRC-UPDATE-SOURCE", FieldType.STRING, 3, 
            RepeatingFieldStrategy.None, "TWRC_UPDATE_SOURCE");
        inactive_Twrc_Form_Period = vw_inactive.getRecord().newFieldInGroup("inactive_Twrc_Form_Period", "TWRC-FORM-PERIOD", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRC_FORM_PERIOD");
        registerRecord(vw_inactive);

        vw_prtcpnt = new DataAccessProgramView(new NameInfo("vw_prtcpnt", "PRTCPNT"), "TWRPARTI_PARTICIPANT_FILE", "TWR_PARTICIPANT");
        prtcpnt_Twrparti_Tax_Id = vw_prtcpnt.getRecord().newFieldInGroup("prtcpnt_Twrparti_Tax_Id", "TWRPARTI-TAX-ID", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TWRPARTI_TAX_ID");
        prtcpnt_Twrparti_Tax_Id_Type = vw_prtcpnt.getRecord().newFieldInGroup("prtcpnt_Twrparti_Tax_Id_Type", "TWRPARTI-TAX-ID-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPARTI_TAX_ID_TYPE");
        prtcpnt_Twrparti_Status = vw_prtcpnt.getRecord().newFieldInGroup("prtcpnt_Twrparti_Status", "TWRPARTI-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_STATUS");
        prtcpnt_Twrparti_Part_Eff_End_Dte = vw_prtcpnt.getRecord().newFieldInGroup("prtcpnt_Twrparti_Part_Eff_End_Dte", "TWRPARTI-PART-EFF-END-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_PART_EFF_END_DTE");
        prtcpnt_Twrparti_Citation_Ind = vw_prtcpnt.getRecord().newFieldInGroup("prtcpnt_Twrparti_Citation_Ind", "TWRPARTI-CITATION-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPARTI_CITATION_IND");
        registerRecord(vw_prtcpnt);

        vw_updpar = new DataAccessProgramView(new NameInfo("vw_updpar", "UPDPAR"), "TWRPARTI_PARTICIPANT_FILE", "TWR_PARTICIPANT");
        updpar_Twrparti_Citation_Ind = vw_updpar.getRecord().newFieldInGroup("updpar_Twrparti_Citation_Ind", "TWRPARTI-CITATION-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPARTI_CITATION_IND");
        registerRecord(vw_updpar);

        vw_cntlr = new DataAccessProgramView(new NameInfo("vw_cntlr", "CNTLR"), "TIRCNTL_FEEDER_SYS_TBL_VIEW", "TIR_CONTROL");
        cntlr_Tircntl_Tbl_Nbr = vw_cntlr.getRecord().newFieldInGroup("cntlr_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        cntlr_Tircntl_Tax_Year = vw_cntlr.getRecord().newFieldInGroup("cntlr_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        cntlr_Tircntl_Rpt_Source_Code = vw_cntlr.getRecord().newFieldInGroup("cntlr_Tircntl_Rpt_Source_Code", "TIRCNTL-RPT-SOURCE-CODE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TIRCNTL_RPT_SOURCE_CODE");
        cntlr_Tircntl_Company_Cde = vw_cntlr.getRecord().newFieldInGroup("cntlr_Tircntl_Company_Cde", "TIRCNTL-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_COMPANY_CDE");
        cntlr_Tircntl_5_Y_Sc_Co_To_Frm_Sp = vw_cntlr.getRecord().newFieldInGroup("cntlr_Tircntl_5_Y_Sc_Co_To_Frm_Sp", "TIRCNTL-5-Y-SC-CO-TO-FRM-SP", FieldType.STRING, 
            24, RepeatingFieldStrategy.None, "TIRCNTL_5_Y_SC_CO_TO_FRM_SP");
        cntlr_Tircntl_5_Y_Sc_Co_To_Frm_Sp.setSuperDescriptor(true);
        registerRecord(vw_cntlr);

        vw_cntlu = new DataAccessProgramView(new NameInfo("vw_cntlu", "CNTLU"), "TIRCNTL_FEEDER_SYS_TBL_VIEW", "TIR_CONTROL");
        cntlu_Tircntl_Frm_Intrfce_Dte = vw_cntlu.getRecord().newFieldInGroup("cntlu_Tircntl_Frm_Intrfce_Dte", "TIRCNTL-FRM-INTRFCE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_FRM_INTRFCE_DTE");
        cntlu_Tircntl_To_Intrfce_Dte = vw_cntlu.getRecord().newFieldInGroup("cntlu_Tircntl_To_Intrfce_Dte", "TIRCNTL-TO-INTRFCE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_TO_INTRFCE_DTE");
        cntlu_Tircntl_Rpt_Update_Dte_Time = vw_cntlu.getRecord().newFieldInGroup("cntlu_Tircntl_Rpt_Update_Dte_Time", "TIRCNTL-RPT-UPDATE-DTE-TIME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "TIRCNTL_RPT_UPDATE_DTE_TIME");
        registerRecord(vw_cntlu);

        pnd_Twrc_S1_Yr_Tin_Contract = localVariables.newFieldInRecord("pnd_Twrc_S1_Yr_Tin_Contract", "#TWRC-S1-YR-TIN-CONTRACT", FieldType.STRING, 24);

        pnd_Twrc_S1_Yr_Tin_Contract__R_Field_1 = localVariables.newGroupInRecord("pnd_Twrc_S1_Yr_Tin_Contract__R_Field_1", "REDEFINE", pnd_Twrc_S1_Yr_Tin_Contract);
        pnd_Twrc_S1_Yr_Tin_Contract_Pnd_S1_Tax_Year = pnd_Twrc_S1_Yr_Tin_Contract__R_Field_1.newFieldInGroup("pnd_Twrc_S1_Yr_Tin_Contract_Pnd_S1_Tax_Year", 
            "#S1-TAX-YEAR", FieldType.STRING, 4);
        pnd_Twrc_S1_Yr_Tin_Contract_Pnd_S1_Tax_Id = pnd_Twrc_S1_Yr_Tin_Contract__R_Field_1.newFieldInGroup("pnd_Twrc_S1_Yr_Tin_Contract_Pnd_S1_Tax_Id", 
            "#S1-TAX-ID", FieldType.STRING, 10);
        pnd_Twrc_S1_Yr_Tin_Contract_Pnd_S1_Contract = pnd_Twrc_S1_Yr_Tin_Contract__R_Field_1.newFieldInGroup("pnd_Twrc_S1_Yr_Tin_Contract_Pnd_S1_Contract", 
            "#S1-CONTRACT", FieldType.STRING, 8);
        pnd_Twrc_S1_Yr_Tin_Contract_Pnd_S1_Payee = pnd_Twrc_S1_Yr_Tin_Contract__R_Field_1.newFieldInGroup("pnd_Twrc_S1_Yr_Tin_Contract_Pnd_S1_Payee", 
            "#S1-PAYEE", FieldType.STRING, 2);
        pnd_Twrc_Sys_Dte_Time = localVariables.newFieldInRecord("pnd_Twrc_Sys_Dte_Time", "#TWRC-SYS-DTE-TIME", FieldType.STRING, 15);

        pnd_Twrc_Sys_Dte_Time__R_Field_2 = localVariables.newGroupInRecord("pnd_Twrc_Sys_Dte_Time__R_Field_2", "REDEFINE", pnd_Twrc_Sys_Dte_Time);
        pnd_Twrc_Sys_Dte_Time_Pnd_Twrc_Sys_Dte_Time_N = pnd_Twrc_Sys_Dte_Time__R_Field_2.newFieldInGroup("pnd_Twrc_Sys_Dte_Time_Pnd_Twrc_Sys_Dte_Time_N", 
            "#TWRC-SYS-DTE-TIME-N", FieldType.NUMERIC, 15);

        pnd_Twrc_Sys_Dte_Time__R_Field_3 = localVariables.newGroupInRecord("pnd_Twrc_Sys_Dte_Time__R_Field_3", "REDEFINE", pnd_Twrc_Sys_Dte_Time);
        pnd_Twrc_Sys_Dte_Time_Pnd_Twrc_Sys_Dte = pnd_Twrc_Sys_Dte_Time__R_Field_3.newFieldInGroup("pnd_Twrc_Sys_Dte_Time_Pnd_Twrc_Sys_Dte", "#TWRC-SYS-DTE", 
            FieldType.STRING, 8);
        pnd_Twrc_Sys_Dte_Time_Pnd_Twrc_Sys_Time = pnd_Twrc_Sys_Dte_Time__R_Field_3.newFieldInGroup("pnd_Twrc_Sys_Dte_Time_Pnd_Twrc_Sys_Time", "#TWRC-SYS-TIME", 
            FieldType.STRING, 7);
        pnd_Twrparti_Curr_Rec_Sd = localVariables.newFieldInRecord("pnd_Twrparti_Curr_Rec_Sd", "#TWRPARTI-CURR-REC-SD", FieldType.STRING, 19);

        pnd_Twrparti_Curr_Rec_Sd__R_Field_4 = localVariables.newGroupInRecord("pnd_Twrparti_Curr_Rec_Sd__R_Field_4", "REDEFINE", pnd_Twrparti_Curr_Rec_Sd);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Tax_Id = pnd_Twrparti_Curr_Rec_Sd__R_Field_4.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Tax_Id", 
            "#TWRPARTI-TAX-ID", FieldType.STRING, 10);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Status = pnd_Twrparti_Curr_Rec_Sd__R_Field_4.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Status", 
            "#TWRPARTI-STATUS", FieldType.STRING, 1);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Part_Eff_End_Dte = pnd_Twrparti_Curr_Rec_Sd__R_Field_4.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Part_Eff_End_Dte", 
            "#TWRPARTI-PART-EFF-END-DTE", FieldType.STRING, 8);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp = localVariables.newFieldInRecord("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp", "#TIRCNTL-5-Y-SC-CO-TO-FRM-SP", FieldType.STRING, 
            24);

        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_5 = localVariables.newGroupInRecord("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_5", "REDEFINE", pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_5 = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_5.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_5", 
            "#SP-CNTL-5", FieldType.NUMERIC, 1);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Year = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_5.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Year", 
            "#SP-CNTL-YEAR", FieldType.NUMERIC, 4);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Source_Code = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_5.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Source_Code", 
            "#SP-CNTL-SOURCE-CODE", FieldType.STRING, 2);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Company = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_5.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Company", 
            "#SP-CNTL-COMPANY", FieldType.STRING, 1);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_To_Ccyymmdd = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_5.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_To_Ccyymmdd", 
            "#SP-CNTL-TO-CCYYMMDD", FieldType.STRING, 8);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Frm_Ccyymmdd = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_5.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Frm_Ccyymmdd", 
            "#SP-CNTL-FRM-CCYYMMDD", FieldType.STRING, 8);
        pnd_Tircntl_Tax_Year_A = localVariables.newFieldInRecord("pnd_Tircntl_Tax_Year_A", "#TIRCNTL-TAX-YEAR-A", FieldType.STRING, 4);

        pnd_Tircntl_Tax_Year_A__R_Field_6 = localVariables.newGroupInRecord("pnd_Tircntl_Tax_Year_A__R_Field_6", "REDEFINE", pnd_Tircntl_Tax_Year_A);
        pnd_Tircntl_Tax_Year_A_Pnd_Tircntl_Tax_Year = pnd_Tircntl_Tax_Year_A__R_Field_6.newFieldInGroup("pnd_Tircntl_Tax_Year_A_Pnd_Tircntl_Tax_Year", 
            "#TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Tircntl_Rpt_Source_Code = localVariables.newFieldInRecord("pnd_Tircntl_Rpt_Source_Code", "#TIRCNTL-RPT-SOURCE-CODE", FieldType.STRING, 2);
        pnd_Tircntl_Frm_Intrfce_Dte_A = localVariables.newFieldInRecord("pnd_Tircntl_Frm_Intrfce_Dte_A", "#TIRCNTL-FRM-INTRFCE-DTE-A", FieldType.STRING, 
            8);

        pnd_Tircntl_Frm_Intrfce_Dte_A__R_Field_7 = localVariables.newGroupInRecord("pnd_Tircntl_Frm_Intrfce_Dte_A__R_Field_7", "REDEFINE", pnd_Tircntl_Frm_Intrfce_Dte_A);
        pnd_Tircntl_Frm_Intrfce_Dte_A_Pnd_Tircntl_Frm_Intrfce_Dte = pnd_Tircntl_Frm_Intrfce_Dte_A__R_Field_7.newFieldInGroup("pnd_Tircntl_Frm_Intrfce_Dte_A_Pnd_Tircntl_Frm_Intrfce_Dte", 
            "#TIRCNTL-FRM-INTRFCE-DTE", FieldType.NUMERIC, 8);
        pnd_Tircntl_To_Intrfce_Dte_A = localVariables.newFieldInRecord("pnd_Tircntl_To_Intrfce_Dte_A", "#TIRCNTL-TO-INTRFCE-DTE-A", FieldType.STRING, 
            8);

        pnd_Tircntl_To_Intrfce_Dte_A__R_Field_8 = localVariables.newGroupInRecord("pnd_Tircntl_To_Intrfce_Dte_A__R_Field_8", "REDEFINE", pnd_Tircntl_To_Intrfce_Dte_A);
        pnd_Tircntl_To_Intrfce_Dte_A_Pnd_Tircntl_To_Intrfce_Dte = pnd_Tircntl_To_Intrfce_Dte_A__R_Field_8.newFieldInGroup("pnd_Tircntl_To_Intrfce_Dte_A_Pnd_Tircntl_To_Intrfce_Dte", 
            "#TIRCNTL-TO-INTRFCE-DTE", FieldType.NUMERIC, 8);
        pnd_Citation_Message_Code = localVariables.newFieldInRecord("pnd_Citation_Message_Code", "#CITATION-MESSAGE-CODE", FieldType.NUMERIC, 2);
        pnd_Ira_Isn = localVariables.newFieldInRecord("pnd_Ira_Isn", "#IRA-ISN", FieldType.PACKED_DECIMAL, 12);
        pnd_Old_Ira_Isn = localVariables.newFieldInRecord("pnd_Old_Ira_Isn", "#OLD-IRA-ISN", FieldType.PACKED_DECIMAL, 12);
        pnd_Prev_Form = localVariables.newFieldInRecord("pnd_Prev_Form", "#PREV-FORM", FieldType.STRING, 2);
        pnd_Part_Ein_Found = localVariables.newFieldInRecord("pnd_Part_Ein_Found", "#PART-EIN-FOUND", FieldType.BOOLEAN, 1);
        pnd_Part_Tax_Id_Type = localVariables.newFieldInRecord("pnd_Part_Tax_Id_Type", "#PART-TAX-ID-TYPE", FieldType.STRING, 1);
        pnd_Timn_N = localVariables.newFieldInRecord("pnd_Timn_N", "#TIMN-N", FieldType.NUMERIC, 7);

        pnd_Timn_N__R_Field_9 = localVariables.newGroupInRecord("pnd_Timn_N__R_Field_9", "REDEFINE", pnd_Timn_N);
        pnd_Timn_N_Pnd_Timn_A = pnd_Timn_N__R_Field_9.newFieldInGroup("pnd_Timn_N_Pnd_Timn_A", "#TIMN-A", FieldType.STRING, 7);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Bypass_Ctr = localVariables.newFieldInRecord("pnd_Bypass_Ctr", "#BYPASS-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Store_Ctr = localVariables.newFieldInRecord("pnd_Store_Ctr", "#STORE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Adjust_Ctr = localVariables.newFieldInRecord("pnd_Adjust_Ctr", "#ADJUST-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Update_New_Ctr = localVariables.newFieldInRecord("pnd_Update_New_Ctr", "#UPDATE-NEW-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Update_Int_Ctr = localVariables.newFieldInRecord("pnd_Update_Int_Ctr", "#UPDATE-INT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Contribution_Processed = localVariables.newFieldInRecord("pnd_Contribution_Processed", "#CONTRIBUTION-PROCESSED", FieldType.BOOLEAN, 1);
        pnd_Summary_Found = localVariables.newFieldInRecord("pnd_Summary_Found", "#SUMMARY-FOUND", FieldType.BOOLEAN, 1);
        pnd_Date_Found = localVariables.newFieldInRecord("pnd_Date_Found", "#DATE-FOUND", FieldType.BOOLEAN, 1);
        pnd_Source_Total_Header = localVariables.newFieldInRecord("pnd_Source_Total_Header", "#SOURCE-TOTAL-HEADER", FieldType.STRING, 17);
        pnd_Prev_Origin_Form = localVariables.newFieldInRecord("pnd_Prev_Origin_Form", "#PREV-ORIGIN-FORM", FieldType.STRING, 10);
        pnd_Header_Date = localVariables.newFieldInRecord("pnd_Header_Date", "#HEADER-DATE", FieldType.DATE);
        pnd_Interface_Date = localVariables.newFieldInRecord("pnd_Interface_Date", "#INTERFACE-DATE", FieldType.DATE);
        pnd_T_Trans_Count = localVariables.newFieldArrayInRecord("pnd_T_Trans_Count", "#T-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            6));
        pnd_T_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_T_Classic_Amt", "#T-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_T_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_T_Roth_Amt", "#T-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_T_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_T_Rollover_Amt", "#T-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_T_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_T_Rechar_Amt", "#T-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_T_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_T_Sep_Amt", "#T-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_T_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_T_Fmv_Amt", "#T-FMV-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            6));
        pnd_T_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_T_Roth_Conv_Amt", "#T-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_C_Trans_Count = localVariables.newFieldArrayInRecord("pnd_C_Trans_Count", "#C-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            6));
        pnd_C_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_C_Classic_Amt", "#C-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_C_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_C_Roth_Amt", "#C-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_C_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_C_Rollover_Amt", "#C-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_C_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_C_Rechar_Amt", "#C-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_C_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_C_Sep_Amt", "#C-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_C_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_C_Fmv_Amt", "#C-FMV-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            6));
        pnd_C_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_C_Roth_Conv_Amt", "#C-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_L_Trans_Count = localVariables.newFieldArrayInRecord("pnd_L_Trans_Count", "#L-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            6));
        pnd_L_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_L_Classic_Amt", "#L-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_L_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_L_Roth_Amt", "#L-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_L_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_L_Rollover_Amt", "#L-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_L_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_L_Rechar_Amt", "#L-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_L_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_L_Sep_Amt", "#L-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_L_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_L_Fmv_Amt", "#L-FMV-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            6));
        pnd_L_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_L_Roth_Conv_Amt", "#L-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_S_Trans_Count = localVariables.newFieldArrayInRecord("pnd_S_Trans_Count", "#S-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            6));
        pnd_S_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_S_Classic_Amt", "#S-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_S_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_S_Roth_Amt", "#S-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_S_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_S_Rollover_Amt", "#S-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_S_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_S_Rechar_Amt", "#S-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_S_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_S_Sep_Amt", "#S-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_S_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_S_Fmv_Amt", "#S-FMV-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            6));
        pnd_S_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_S_Roth_Conv_Amt", "#S-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            6));
        pnd_Summary_Record = localVariables.newFieldInRecord("pnd_Summary_Record", "#SUMMARY-RECORD", FieldType.BOOLEAN, 1);
        pnd_Payment_Found = localVariables.newFieldInRecord("pnd_Payment_Found", "#PAYMENT-FOUND", FieldType.BOOLEAN, 1);
        pnd_Update_Payment = localVariables.newFieldInRecord("pnd_Update_Payment", "#UPDATE-PAYMENT", FieldType.BOOLEAN, 1);
        pnd_Twrc_Create_Date = localVariables.newFieldInRecord("pnd_Twrc_Create_Date", "#TWRC-CREATE-DATE", FieldType.STRING, 8);
        pnd_Twrc_Create_Date_Inv = localVariables.newFieldInRecord("pnd_Twrc_Create_Date_Inv", "#TWRC-CREATE-DATE-INV", FieldType.STRING, 8);
        pnd_Twrc_Create_Time = localVariables.newFieldInRecord("pnd_Twrc_Create_Time", "#TWRC-CREATE-TIME", FieldType.STRING, 7);
        pnd_Twrc_Create_Time_Inv = localVariables.newFieldInRecord("pnd_Twrc_Create_Time_Inv", "#TWRC-CREATE-TIME-INV", FieldType.STRING, 7);
        pnd_Twrc_Lu_Ts = localVariables.newFieldInRecord("pnd_Twrc_Lu_Ts", "#TWRC-LU-TS", FieldType.PACKED_DECIMAL, 12);
        pnd_Et_Ctr = localVariables.newFieldInRecord("pnd_Et_Ctr", "#ET-CTR", FieldType.PACKED_DECIMAL, 4);
        pnd_T = localVariables.newFieldInRecord("pnd_T", "#T", FieldType.PACKED_DECIMAL, 3);
        i1 = localVariables.newFieldInRecord("i1", "I1", FieldType.PACKED_DECIMAL, 3);
        i2 = localVariables.newFieldInRecord("i2", "I2", FieldType.PACKED_DECIMAL, 3);
        i3 = localVariables.newFieldInRecord("i3", "I3", FieldType.PACKED_DECIMAL, 3);
        i = localVariables.newFieldInRecord("i", "I", FieldType.PACKED_DECIMAL, 3);
        j = localVariables.newFieldInRecord("j", "J", FieldType.PACKED_DECIMAL, 4);
        k = localVariables.newFieldInRecord("k", "K", FieldType.PACKED_DECIMAL, 4);
        z = localVariables.newFieldInRecord("z", "Z", FieldType.PACKED_DECIMAL, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_oldira.reset();
        vw_active.reset();
        vw_inactive.reset();
        vw_prtcpnt.reset();
        vw_updpar.reset();
        vw_cntlr.reset();
        vw_cntlu.reset();

        ldaTwrl462a.initializeValues();
        ldaTwrl465b.initializeValues();
        ldaTwrl465c.initializeValues();
        ldaTwrl820e.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp4650() throws Exception
    {
        super("Twrp4650");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP4650", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //* *--------
        //* *RMAT (01)  PS=60  LS=133  ZP=ON /* PIN EXPANSION JW0427                                                                                                      //Natural: FORMAT ( 00 ) PS = 60 LS = 133 ZP = ON
        //*  PIN EXPANSION JW0427
        //* *RMAT (03)  PS=60  LS=133  ZP=ON /* PIN EXPANSION JW0427                                                                                                      //Natural: FORMAT ( 01 ) PS = 60 LS = 138 ZP = ON;//Natural: FORMAT ( 02 ) PS = 60 LS = 133 ZP = ON
        //*  PIN EXPANSION JW0427
        //*                                                                                                                                                               //Natural: FORMAT ( 03 ) PS = 60 LS = 138 ZP = ON;//Natural: FORMAT ( 04 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 05 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 06 ) PS = 60 LS = 133 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *============**
        //*  MAIN PROGRAM *
        //* *============**
                                                                                                                                                                          //Natural: PERFORM HOUSE-KEEPING
        sub_House_Keeping();
        if (condition(Global.isEscape())) {return;}
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 RECORD IRA-RECORD
        while (condition(getWorkFiles().read(1, ldaTwrl462a.getIra_Record())))
        {
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            if (condition(pnd_Prev_Form.equals("IR")))                                                                                                                    //Natural: IF #PREV-FORM = 'IR'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Prev_Form.equals(" ")))                                                                                                                 //Natural: IF #PREV-FORM = ' '
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Source_Total_Header.setValue(DbsUtil.compress("Source System:", pnd_Prev_Form));                                                                  //Natural: COMPRESS 'Source System:' #PREV-FORM INTO #SOURCE-TOTAL-HEADER
                    i1.setValue(1);                                                                                                                                       //Natural: ASSIGN I1 := 1
                    i2.setValue(2);                                                                                                                                       //Natural: ASSIGN I2 := 2
                    i3.setValue(3);                                                                                                                                       //Natural: ASSIGN I3 := 3
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
                    sub_Display_Control_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM STORE-CONTROL-TOTALS
                    sub_Store_Control_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM UPDATE-GENERAL-TOTALS
                    sub_Update_General_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM RESET-CONTROL-TOTALS
                    sub_Reset_Control_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Prev_Form.setValue("IR");                                                                                                                             //Natural: ASSIGN #PREV-FORM := 'IR'
                pnd_Interface_Date.setValue(Global.getDATX());                                                                                                            //Natural: ASSIGN #INTERFACE-DATE := *DATX
                pnd_Header_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaTwrl462a.getIra_Record_Ira_Simulation_Date());                                           //Natural: MOVE EDITED IRA-SIMULATION-DATE TO #HEADER-DATE ( EM = YYYYMMDD )
                pnd_Tircntl_Tax_Year_A.setValue(ldaTwrl462a.getIra_Record_Ira_Tax_Year());                                                                                //Natural: ASSIGN #TIRCNTL-TAX-YEAR-A := IRA-TAX-YEAR
                pnd_Tircntl_Rpt_Source_Code.setValue("IR");                                                                                                               //Natural: ASSIGN #TIRCNTL-RPT-SOURCE-CODE := 'IR'
                pnd_Tircntl_Frm_Intrfce_Dte_A.setValue(Global.getDATN());                                                                                                 //Natural: ASSIGN #TIRCNTL-FRM-INTRFCE-DTE-A := *DATN
                pnd_Tircntl_To_Intrfce_Dte_A.setValue(ldaTwrl462a.getIra_Record_Ira_Simulation_Date());                                                                   //Natural: ASSIGN #TIRCNTL-TO-INTRFCE-DTE-A := IRA-SIMULATION-DATE
                                                                                                                                                                          //Natural: PERFORM READ-SUMMARY-TOTALS-RECORD
                sub_Read_Summary_Totals_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_T.setValue(1);                                                                                                                                            //Natural: ASSIGN #T := 1
            if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("T")))                                                                                      //Natural: IF IRA-COMPANY-CODE = 'T'
            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-T-CONTROL-TOTALS
                sub_Update_T_Control_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("C")))                                                                                  //Natural: IF IRA-COMPANY-CODE = 'C'
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-C-CONTROL-TOTALS
                    sub_Update_C_Control_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("L")))                                                                              //Natural: IF IRA-COMPANY-CODE = 'L'
                    {
                                                                                                                                                                          //Natural: PERFORM UPDATE-L-CONTROL-TOTALS
                        sub_Update_L_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  09-24-03 FRANK
                        if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("S")))                                                                          //Natural: IF IRA-COMPANY-CODE = 'S'
                        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-S-CONTROL-TOTALS
                            sub_Update_S_Control_Totals();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM LOOKUP-IRA-CONTRIBUTION
            sub_Lookup_Ira_Contribution();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Contribution_Processed.equals(true)))                                                                                                       //Natural: IF #CONTRIBUTION-PROCESSED = TRUE
            {
                pnd_T.setValue(2);                                                                                                                                        //Natural: ASSIGN #T := 2
                if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("T")))                                                                                  //Natural: IF IRA-COMPANY-CODE = 'T'
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-T-CONTROL-TOTALS
                    sub_Update_T_Control_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("C")))                                                                              //Natural: IF IRA-COMPANY-CODE = 'C'
                    {
                                                                                                                                                                          //Natural: PERFORM UPDATE-C-CONTROL-TOTALS
                        sub_Update_C_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("L")))                                                                          //Natural: IF IRA-COMPANY-CODE = 'L'
                        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-L-CONTROL-TOTALS
                            sub_Update_L_Control_Totals();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  09-24-03 FRANK
                            if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("S")))                                                                      //Natural: IF IRA-COMPANY-CODE = 'S'
                            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-S-CONTROL-TOTALS
                                sub_Update_S_Control_Totals();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Bypass_Ctr.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #BYPASS-CTR
                                                                                                                                                                          //Natural: PERFORM REPORT-BYPASSED-EXISTING-PAYMENTS
                sub_Report_Bypassed_Existing_Payments();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM STORE-NEW-IRA-CONTRIBUTION
                sub_Store_New_Ira_Contribution();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_T.setValue(3);                                                                                                                                        //Natural: ASSIGN #T := 3
                if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("T")))                                                                                  //Natural: IF IRA-COMPANY-CODE = 'T'
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-T-CONTROL-TOTALS
                    sub_Update_T_Control_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("C")))                                                                              //Natural: IF IRA-COMPANY-CODE = 'C'
                    {
                                                                                                                                                                          //Natural: PERFORM UPDATE-C-CONTROL-TOTALS
                        sub_Update_C_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("L")))                                                                          //Natural: IF IRA-COMPANY-CODE = 'L'
                        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-L-CONTROL-TOTALS
                            sub_Update_L_Control_Totals();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  09-24-03 FRANK
                            if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("S")))                                                                      //Natural: IF IRA-COMPANY-CODE = 'S'
                            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-S-CONTROL-TOTALS
                                sub_Update_S_Control_Totals();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(3, false, ldaTwrl462a.getIra_Record(), pnd_Ira_Isn);                                                                                     //Natural: WRITE WORK FILE 03 IRA-RECORD #IRA-ISN
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* *------
        if (condition(pnd_Read_Ctr.equals(getZero())))                                                                                                                    //Natural: IF #READ-CTR = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"Tax Transaction File (Work File 01) Is Empty",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new  //Natural: WRITE ( 00 ) '***' 06T 'Tax Transaction File (Work File 01) Is Empty' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-IF
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        pnd_Source_Total_Header.setValue(DbsUtil.compress("Source System:", pnd_Prev_Form));                                                                              //Natural: COMPRESS 'Source System:' #PREV-FORM INTO #SOURCE-TOTAL-HEADER
        i1.setValue(1);                                                                                                                                                   //Natural: ASSIGN I1 := 1
        i2.setValue(2);                                                                                                                                                   //Natural: ASSIGN I2 := 2
        i3.setValue(3);                                                                                                                                                   //Natural: ASSIGN I3 := 3
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
        sub_Display_Control_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM STORE-CONTROL-TOTALS
        sub_Store_Control_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-GENERAL-TOTALS
        sub_Update_General_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM RESET-CONTROL-TOTALS
        sub_Reset_Control_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *----------------------------------------
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //* * 09-24-03 ADDED SERVICES BY FRANK
        //* *------------
        //* *------------
        //* *------------
        //* * 09-24-03 ADDED SERVICES BY FRANK
        //* *------------
        //* *-------------------------------------------
        //* *------------
        //* *---------------------------------
        //* *------------
        //* **DASRAH START
        //* *---------------------------------
        //* *DASRAH END
        //* *------------
        //* *-------------------------------------
        //* * 09-24-03 ADDED SERVICES BY FRANK
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //* *------------------------------
        //* *------------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 01 )
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 02 )
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 03 )
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
        //* *-------                                                                                                                                                      //Natural: ON ERROR
    }
    private void sub_Lookup_Ira_Contribution() throws Exception                                                                                                           //Natural: LOOKUP-IRA-CONTRIBUTION
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Contribution_Processed.setValue(false);                                                                                                                       //Natural: ASSIGN #CONTRIBUTION-PROCESSED := FALSE
        vw_oldira.startDatabaseRead                                                                                                                                       //Natural: READ ( 1 ) OLDIRA WITH TWRC-UNIQUE-ID = IRA-UNIQUE-ID
        (
        "RL1",
        new Wc[] { new Wc("TWRC_UNIQUE_ID", ">=", ldaTwrl462a.getIra_Record_Ira_Unique_Id(), WcType.BY) },
        new Oc[] { new Oc("TWRC_UNIQUE_ID", "ASC") },
        1
        );
        RL1:
        while (condition(vw_oldira.readNextRow("RL1")))
        {
            if (condition(oldira_Twrc_Unique_Id.equals(ldaTwrl462a.getIra_Record_Ira_Unique_Id())))                                                                       //Natural: IF OLDIRA.TWRC-UNIQUE-ID = IRA-UNIQUE-ID
            {
                pnd_Contribution_Processed.setValue(true);                                                                                                                //Natural: ASSIGN #CONTRIBUTION-PROCESSED := TRUE
                pnd_Ira_Isn.setValue(vw_oldira.getAstISN("RL1"));                                                                                                         //Natural: ASSIGN #IRA-ISN := *ISN ( RL1. )
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Twrc_S1_Yr_Tin_Contract_Pnd_S1_Tax_Year.setValue(ldaTwrl462a.getIra_Record_Ira_Tax_Year());                                                                   //Natural: ASSIGN #S1-TAX-YEAR := IRA-TAX-YEAR
        pnd_Twrc_S1_Yr_Tin_Contract_Pnd_S1_Tax_Id.setValue(ldaTwrl462a.getIra_Record_Ira_Tax_Id());                                                                       //Natural: ASSIGN #S1-TAX-ID := IRA-TAX-ID
        pnd_Twrc_S1_Yr_Tin_Contract_Pnd_S1_Contract.setValue(ldaTwrl462a.getIra_Record_Ira_Contract());                                                                   //Natural: ASSIGN #S1-CONTRACT := IRA-CONTRACT
        pnd_Twrc_S1_Yr_Tin_Contract_Pnd_S1_Payee.setValue(ldaTwrl462a.getIra_Record_Ira_Payee());                                                                         //Natural: ASSIGN #S1-PAYEE := IRA-PAYEE
        vw_active.startDatabaseRead                                                                                                                                       //Natural: READ ( 1 ) ACTIVE WITH TWRC-S1-YR-TIN-CONTRACT = #TWRC-S1-YR-TIN-CONTRACT
        (
        "RL2",
        new Wc[] { new Wc("TWRC_S1_YR_TIN_CONTRACT", ">=", pnd_Twrc_S1_Yr_Tin_Contract, WcType.BY) },
        new Oc[] { new Oc("TWRC_S1_YR_TIN_CONTRACT", "ASC") },
        1
        );
        RL2:
        while (condition(vw_active.readNextRow("RL2")))
        {
            //*   BLANK  =  ACTIVE
            if (condition(active_Twrc_Tax_Year.equals(pnd_Twrc_S1_Yr_Tin_Contract_Pnd_S1_Tax_Year) && active_Twrc_Tax_Id.equals(pnd_Twrc_S1_Yr_Tin_Contract_Pnd_S1_Tax_Id)  //Natural: IF ACTIVE.TWRC-TAX-YEAR = #S1-TAX-YEAR AND ACTIVE.TWRC-TAX-ID = #S1-TAX-ID AND ACTIVE.TWRC-CONTRACT = #S1-CONTRACT AND ACTIVE.TWRC-PAYEE = #S1-PAYEE AND ACTIVE.TWRC-STATUS = ' '
                && active_Twrc_Contract.equals(pnd_Twrc_S1_Yr_Tin_Contract_Pnd_S1_Contract) && active_Twrc_Payee.equals(pnd_Twrc_S1_Yr_Tin_Contract_Pnd_S1_Payee) 
                && active_Twrc_Status.equals(" ")))
            {
                pnd_Old_Ira_Isn.setValue(vw_active.getAstISN("RL2"));                                                                                                     //Natural: ASSIGN #OLD-IRA-ISN := *ISN ( RL2. )
                G1:                                                                                                                                                       //Natural: GET INACTIVE #OLD-IRA-ISN
                vw_inactive.readByID(pnd_Old_Ira_Isn.getLong(), "G1");
                inactive_Twrc_Status.setValue("I");                                                                                                                       //Natural: ASSIGN INACTIVE.TWRC-STATUS := 'I'
                inactive_Twrc_Update_User.setValue(Global.getINIT_USER());                                                                                                //Natural: ASSIGN INACTIVE.TWRC-UPDATE-USER := *INIT-USER
                inactive_Twrc_Update_Date.setValue(pnd_Twrc_Create_Date);                                                                                                 //Natural: ASSIGN INACTIVE.TWRC-UPDATE-DATE := #TWRC-CREATE-DATE
                inactive_Twrc_Update_Time.setValue(pnd_Twrc_Create_Time);                                                                                                 //Natural: ASSIGN INACTIVE.TWRC-UPDATE-TIME := #TWRC-CREATE-TIME
                vw_inactive.updateDBRow("G1");                                                                                                                            //Natural: UPDATE ( G1. )
                pnd_Et_Ctr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #ET-CTR
                if (condition(ldaTwrl462a.getIra_Record_Ira_Form_Period().equals("P1") || ldaTwrl462a.getIra_Record_Ira_Form_Period().equals("P2") &&                     //Natural: IF IRA-FORM-PERIOD = 'P1' OR IRA-FORM-PERIOD = 'P2' AND ( INACTIVE.TWRC-SOURCE = 'ML' OR INACTIVE.TWRC-UPDATE-SOURCE = 'OL' OR INACTIVE.TWRC-FORM-PERIOD = 'P2' )
                    (inactive_Twrc_Source.equals("ML") || inactive_Twrc_Update_Source.equals("OL") || inactive_Twrc_Form_Period.equals("P2"))))
                {
                                                                                                                                                                          //Natural: PERFORM REPORT-ADJUSTED-ACCEPTED-TRANSACTION
                    sub_Report_Adjusted_Accepted_Transaction();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Store_New_Ira_Contribution() throws Exception                                                                                                        //Natural: STORE-NEW-IRA-CONTRIBUTION
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------------
        pnd_Store_Ctr.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #STORE-CTR
        //*  DASRAH
                                                                                                                                                                          //Natural: PERFORM LOOKUP-PARTICIPANT
        sub_Lookup_Participant();
        if (condition(Global.isEscape())) {return;}
        //*  DASRAH
        //*  DASRAH
        if (condition(pnd_Part_Ein_Found.getBoolean()))                                                                                                                   //Natural: IF #PART-EIN-FOUND
        {
            ldaTwrl465c.getNewira_Twrc_Tax_Id_Type().setValue(pnd_Part_Tax_Id_Type);                                                                                      //Natural: ASSIGN NEWIRA.TWRC-TAX-ID-TYPE := #PART-TAX-ID-TYPE
            //*  DASRAH
            //*  DASRAH
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl465c.getNewira_Twrc_Tax_Id_Type().setValue(ldaTwrl462a.getIra_Record_Ira_Tax_Id_Type());                                                               //Natural: ASSIGN NEWIRA.TWRC-TAX-ID-TYPE := IRA-TAX-ID-TYPE
            //*  DASRAH
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl465c.getNewira_Twrc_Tax_Year().setValue(ldaTwrl462a.getIra_Record_Ira_Tax_Year());                                                                         //Natural: ASSIGN NEWIRA.TWRC-TAX-YEAR := IRA-TAX-YEAR
        ldaTwrl465c.getNewira_Twrc_Company_Cde().setValue(ldaTwrl462a.getIra_Record_Ira_Company_Code());                                                                  //Natural: ASSIGN NEWIRA.TWRC-COMPANY-CDE := IRA-COMPANY-CODE
        ldaTwrl465c.getNewira_Twrc_Status().setValue(" ");                                                                                                                //Natural: ASSIGN NEWIRA.TWRC-STATUS := ' '
        ldaTwrl465c.getNewira_Twrc_Simulation_Date().setValue(ldaTwrl462a.getIra_Record_Ira_Simulation_Date());                                                           //Natural: ASSIGN NEWIRA.TWRC-SIMULATION-DATE := IRA-SIMULATION-DATE
        ldaTwrl465c.getNewira_Twrc_Citation_Ind().setValue(" ");                                                                                                          //Natural: ASSIGN NEWIRA.TWRC-CITATION-IND := ' '
        ldaTwrl465c.getNewira_Twrc_Tax_Id().setValue(ldaTwrl462a.getIra_Record_Ira_Tax_Id());                                                                             //Natural: ASSIGN NEWIRA.TWRC-TAX-ID := IRA-TAX-ID
        ldaTwrl465c.getNewira_Twrc_Contract().setValue(ldaTwrl462a.getIra_Record_Ira_Contract());                                                                         //Natural: ASSIGN NEWIRA.TWRC-CONTRACT := IRA-CONTRACT
        ldaTwrl465c.getNewira_Twrc_Payee().setValue(ldaTwrl462a.getIra_Record_Ira_Payee());                                                                               //Natural: ASSIGN NEWIRA.TWRC-PAYEE := IRA-PAYEE
        ldaTwrl465c.getNewira_Twrc_Contract_Xref().setValue(ldaTwrl462a.getIra_Record_Ira_Contract_Xref());                                                               //Natural: ASSIGN NEWIRA.TWRC-CONTRACT-XREF := IRA-CONTRACT-XREF
        ldaTwrl465c.getNewira_Twrc_Citizen_Code().setValue(ldaTwrl462a.getIra_Record_Ira_Citizenship_Code());                                                             //Natural: ASSIGN NEWIRA.TWRC-CITIZEN-CODE := IRA-CITIZENSHIP-CODE
        ldaTwrl465c.getNewira_Twrc_Residency_Code().setValue(ldaTwrl462a.getIra_Record_Ira_Residency_Code());                                                             //Natural: ASSIGN NEWIRA.TWRC-RESIDENCY-CODE := IRA-RESIDENCY-CODE
        ldaTwrl465c.getNewira_Twrc_Residency_Type().setValue(ldaTwrl462a.getIra_Record_Ira_Residency_Type());                                                             //Natural: ASSIGN NEWIRA.TWRC-RESIDENCY-TYPE := IRA-RESIDENCY-TYPE
        ldaTwrl465c.getNewira_Twrc_Locality_Cde().setValue(ldaTwrl462a.getIra_Record_Ira_Locality_Code());                                                                //Natural: ASSIGN NEWIRA.TWRC-LOCALITY-CDE := IRA-LOCALITY-CODE
        ldaTwrl465c.getNewira_Twrc_Dob().setValue(ldaTwrl462a.getIra_Record_Ira_Dob());                                                                                   //Natural: ASSIGN NEWIRA.TWRC-DOB := IRA-DOB
        ldaTwrl465c.getNewira_Twrc_Dod().setValue(ldaTwrl462a.getIra_Record_Ira_Dod());                                                                                   //Natural: ASSIGN NEWIRA.TWRC-DOD := IRA-DOD
        ldaTwrl465c.getNewira_Twrc_Pin().setValue(ldaTwrl462a.getIra_Record_Ira_Pin());                                                                                   //Natural: ASSIGN NEWIRA.TWRC-PIN := IRA-PIN
        ldaTwrl465c.getNewira_Twrc_Ira_Contract_Type().setValue(ldaTwrl462a.getIra_Record_Ira_Form_Type());                                                               //Natural: ASSIGN NEWIRA.TWRC-IRA-CONTRACT-TYPE := IRA-FORM-TYPE
        ldaTwrl465c.getNewira_Twrc_Source().setValue(ldaTwrl462a.getIra_Record_Ira_Form_Period());                                                                        //Natural: ASSIGN NEWIRA.TWRC-SOURCE := IRA-FORM-PERIOD
        ldaTwrl465c.getNewira_Twrc_Form_Period().setValue(ldaTwrl462a.getIra_Record_Ira_Form_Period());                                                                   //Natural: ASSIGN NEWIRA.TWRC-FORM-PERIOD := IRA-FORM-PERIOD
        ldaTwrl465c.getNewira_Twrc_Error_Reason().reset();                                                                                                                //Natural: RESET NEWIRA.TWRC-ERROR-REASON
        //*  SECURE-ACT
        //*  SECURE-ACT
        ldaTwrl465c.getNewira_Twrc_Orign_Area().reset();                                                                                                                  //Natural: RESET NEWIRA.TWRC-ORIGN-AREA
        ldaTwrl465c.getNewira_Twrc_Classic_Amt().setValue(ldaTwrl462a.getIra_Record_Ira_Classic_Amt());                                                                   //Natural: ASSIGN NEWIRA.TWRC-CLASSIC-AMT := IRA-CLASSIC-AMT
        ldaTwrl465c.getNewira_Twrc_Roth_Amt().setValue(ldaTwrl462a.getIra_Record_Ira_Roth_Amt());                                                                         //Natural: ASSIGN NEWIRA.TWRC-ROTH-AMT := IRA-ROTH-AMT
        ldaTwrl465c.getNewira_Twrc_Sep_Amt().setValue(ldaTwrl462a.getIra_Record_Ira_Sep_Amt());                                                                           //Natural: ASSIGN NEWIRA.TWRC-SEP-AMT := IRA-SEP-AMT
        ldaTwrl465c.getNewira_Twrc_Rollover_Amt().setValue(ldaTwrl462a.getIra_Record_Ira_Rollover_Amt());                                                                 //Natural: ASSIGN NEWIRA.TWRC-ROLLOVER-AMT := IRA-ROLLOVER-AMT
        ldaTwrl465c.getNewira_Twrc_Rechar_Amt().setValue(ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt());                                                                //Natural: ASSIGN NEWIRA.TWRC-RECHAR-AMT := IRA-RECHARACTER-AMT
        ldaTwrl465c.getNewira_Twrc_Fair_Mkt_Val_Amt().setValue(ldaTwrl462a.getIra_Record_Ira_Fmv_Amt());                                                                  //Natural: ASSIGN NEWIRA.TWRC-FAIR-MKT-VAL-AMT := IRA-FMV-AMT
        ldaTwrl465c.getNewira_Twrc_Roth_Conversion_Amt().setValue(ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt());                                                         //Natural: ASSIGN NEWIRA.TWRC-ROTH-CONVERSION-AMT := IRA-ROTH-CONV-AMT
        ldaTwrl465c.getNewira_Twrc_Name().setValue(ldaTwrl462a.getIra_Record_Ira_Name());                                                                                 //Natural: ASSIGN NEWIRA.TWRC-NAME := IRA-NAME
        ldaTwrl465c.getNewira_Twrc_Addr_1().setValue(ldaTwrl462a.getIra_Record_Ira_Addr_1());                                                                             //Natural: ASSIGN NEWIRA.TWRC-ADDR-1 := IRA-ADDR-1
        ldaTwrl465c.getNewira_Twrc_Addr_2().setValue(ldaTwrl462a.getIra_Record_Ira_Addr_2());                                                                             //Natural: ASSIGN NEWIRA.TWRC-ADDR-2 := IRA-ADDR-2
        ldaTwrl465c.getNewira_Twrc_Addr_3().setValue(ldaTwrl462a.getIra_Record_Ira_Addr_3());                                                                             //Natural: ASSIGN NEWIRA.TWRC-ADDR-3 := IRA-ADDR-3
        ldaTwrl465c.getNewira_Twrc_Addr_4().setValue(ldaTwrl462a.getIra_Record_Ira_Addr_4());                                                                             //Natural: ASSIGN NEWIRA.TWRC-ADDR-4 := IRA-ADDR-4
        ldaTwrl465c.getNewira_Twrc_Addr_5().setValue(ldaTwrl462a.getIra_Record_Ira_Addr_5());                                                                             //Natural: ASSIGN NEWIRA.TWRC-ADDR-5 := IRA-ADDR-5
        ldaTwrl465c.getNewira_Twrc_Addr_6().setValue(ldaTwrl462a.getIra_Record_Ira_Addr_6());                                                                             //Natural: ASSIGN NEWIRA.TWRC-ADDR-6 := IRA-ADDR-6
        ldaTwrl465c.getNewira_Twrc_City().setValue(ldaTwrl462a.getIra_Record_Ira_City());                                                                                 //Natural: ASSIGN NEWIRA.TWRC-CITY := IRA-CITY
        ldaTwrl465c.getNewira_Twrc_State().setValue(ldaTwrl462a.getIra_Record_Ira_State());                                                                               //Natural: ASSIGN NEWIRA.TWRC-STATE := IRA-STATE
        ldaTwrl465c.getNewira_Twrc_Zip_Code().setValue(ldaTwrl462a.getIra_Record_Ira_Zip_Code());                                                                         //Natural: ASSIGN NEWIRA.TWRC-ZIP-CODE := IRA-ZIP-CODE
        ldaTwrl465c.getNewira_Twrc_Addr_Foreign().setValue(ldaTwrl462a.getIra_Record_Ira_Foreign_Address());                                                              //Natural: ASSIGN NEWIRA.TWRC-ADDR-FOREIGN := IRA-FOREIGN-ADDRESS
        ldaTwrl465c.getNewira_Twrc_Create_User().setValue(Global.getINIT_USER());                                                                                         //Natural: ASSIGN NEWIRA.TWRC-CREATE-USER := *INIT-USER
        ldaTwrl465c.getNewira_Twrc_Create_Date().setValue(pnd_Twrc_Create_Date);                                                                                          //Natural: ASSIGN NEWIRA.TWRC-CREATE-DATE := #TWRC-CREATE-DATE
        ldaTwrl465c.getNewira_Twrc_Create_Date_Inv().setValue(pnd_Twrc_Create_Date_Inv);                                                                                  //Natural: ASSIGN NEWIRA.TWRC-CREATE-DATE-INV := #TWRC-CREATE-DATE-INV
        ldaTwrl465c.getNewira_Twrc_Create_Time().setValue(pnd_Twrc_Create_Time);                                                                                          //Natural: ASSIGN NEWIRA.TWRC-CREATE-TIME := #TWRC-CREATE-TIME
        ldaTwrl465c.getNewira_Twrc_Create_Time_Inv().setValue(pnd_Twrc_Create_Time_Inv);                                                                                  //Natural: ASSIGN NEWIRA.TWRC-CREATE-TIME-INV := #TWRC-CREATE-TIME-INV
        ldaTwrl465c.getNewira_Twrc_Lu_Ts().setValue(pnd_Twrc_Lu_Ts);                                                                                                      //Natural: ASSIGN NEWIRA.TWRC-LU-TS := #TWRC-LU-TS
        ldaTwrl465c.getNewira_Twrc_Rmd_Required_Flag().setValue(ldaTwrl462a.getIra_Record_Ira_Rmd_Required_Flag());                                                       //Natural: ASSIGN NEWIRA.TWRC-RMD-REQUIRED-FLAG := IRA-RMD-REQUIRED-FLAG
        ldaTwrl465c.getNewira_Twrc_Rmd_Decedent_Name().setValue(ldaTwrl462a.getIra_Record_Ira_Rmd_Decedent_Name());                                                       //Natural: ASSIGN NEWIRA.TWRC-RMD-DECEDENT-NAME := IRA-RMD-DECEDENT-NAME
        ST1:                                                                                                                                                              //Natural: STORE NEWIRA
        ldaTwrl465c.getVw_newira().insertDBRow("ST1");
        pnd_Ira_Isn.setValue(ldaTwrl465c.getVw_newira().getAstISN("ST1"));                                                                                                //Natural: ASSIGN #IRA-ISN := *ISN ( ST1. )
        pnd_Et_Ctr.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #ET-CTR
        if (condition(pnd_Et_Ctr.greater(300)))                                                                                                                           //Natural: IF #ET-CTR > 300
        {
            pnd_Et_Ctr.setValue(0);                                                                                                                                       //Natural: ASSIGN #ET-CTR := 0
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_T_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-T-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pnd_T_Trans_Count.getValue(pnd_T).nadd(1);                                                                                                                        //Natural: ADD 1 TO #T-TRANS-COUNT ( #T )
        pnd_T_Classic_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Classic_Amt());                                                                              //Natural: ADD IRA-CLASSIC-AMT TO #T-CLASSIC-AMT ( #T )
        pnd_T_Roth_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Amt());                                                                                    //Natural: ADD IRA-ROTH-AMT TO #T-ROTH-AMT ( #T )
        pnd_T_Sep_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Sep_Amt());                                                                                      //Natural: ADD IRA-SEP-AMT TO #T-SEP-AMT ( #T )
        pnd_T_Rollover_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Rollover_Amt());                                                                            //Natural: ADD IRA-ROLLOVER-AMT TO #T-ROLLOVER-AMT ( #T )
        pnd_T_Rechar_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt());                                                                           //Natural: ADD IRA-RECHARACTER-AMT TO #T-RECHAR-AMT ( #T )
        pnd_T_Fmv_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Fmv_Amt());                                                                                      //Natural: ADD IRA-FMV-AMT TO #T-FMV-AMT ( #T )
        pnd_T_Roth_Conv_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt());                                                                          //Natural: ADD IRA-ROTH-CONV-AMT TO #T-ROTH-CONV-AMT ( #T )
    }
    private void sub_Update_C_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-C-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pnd_C_Trans_Count.getValue(pnd_T).nadd(1);                                                                                                                        //Natural: ADD 1 TO #C-TRANS-COUNT ( #T )
        pnd_C_Classic_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Classic_Amt());                                                                              //Natural: ADD IRA-CLASSIC-AMT TO #C-CLASSIC-AMT ( #T )
        pnd_C_Roth_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Amt());                                                                                    //Natural: ADD IRA-ROTH-AMT TO #C-ROTH-AMT ( #T )
        pnd_C_Sep_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Sep_Amt());                                                                                      //Natural: ADD IRA-SEP-AMT TO #C-SEP-AMT ( #T )
        pnd_C_Rollover_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Rollover_Amt());                                                                            //Natural: ADD IRA-ROLLOVER-AMT TO #C-ROLLOVER-AMT ( #T )
        pnd_C_Rechar_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt());                                                                           //Natural: ADD IRA-RECHARACTER-AMT TO #C-RECHAR-AMT ( #T )
        pnd_C_Fmv_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Fmv_Amt());                                                                                      //Natural: ADD IRA-FMV-AMT TO #C-FMV-AMT ( #T )
        pnd_C_Roth_Conv_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt());                                                                          //Natural: ADD IRA-ROTH-CONV-AMT TO #C-ROTH-CONV-AMT ( #T )
    }
    private void sub_Update_L_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-L-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pnd_L_Trans_Count.getValue(pnd_T).nadd(1);                                                                                                                        //Natural: ADD 1 TO #L-TRANS-COUNT ( #T )
        pnd_L_Sep_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Sep_Amt());                                                                                      //Natural: ADD IRA-SEP-AMT TO #L-SEP-AMT ( #T )
        pnd_L_Classic_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Classic_Amt());                                                                              //Natural: ADD IRA-CLASSIC-AMT TO #L-CLASSIC-AMT ( #T )
        pnd_L_Roth_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Amt());                                                                                    //Natural: ADD IRA-ROTH-AMT TO #L-ROTH-AMT ( #T )
        pnd_L_Rollover_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Rollover_Amt());                                                                            //Natural: ADD IRA-ROLLOVER-AMT TO #L-ROLLOVER-AMT ( #T )
        pnd_L_Rechar_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt());                                                                           //Natural: ADD IRA-RECHARACTER-AMT TO #L-RECHAR-AMT ( #T )
        pnd_L_Fmv_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Fmv_Amt());                                                                                      //Natural: ADD IRA-FMV-AMT TO #L-FMV-AMT ( #T )
        pnd_L_Roth_Conv_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt());                                                                          //Natural: ADD IRA-ROTH-CONV-AMT TO #L-ROTH-CONV-AMT ( #T )
    }
    private void sub_Update_S_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-S-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pnd_S_Trans_Count.getValue(pnd_T).nadd(1);                                                                                                                        //Natural: ADD 1 TO #S-TRANS-COUNT ( #T )
        pnd_S_Classic_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Classic_Amt());                                                                              //Natural: ADD IRA-CLASSIC-AMT TO #S-CLASSIC-AMT ( #T )
        pnd_S_Roth_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Amt());                                                                                    //Natural: ADD IRA-ROTH-AMT TO #S-ROTH-AMT ( #T )
        pnd_S_Sep_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Sep_Amt());                                                                                      //Natural: ADD IRA-SEP-AMT TO #S-SEP-AMT ( #T )
        pnd_S_Rollover_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Rollover_Amt());                                                                            //Natural: ADD IRA-ROLLOVER-AMT TO #S-ROLLOVER-AMT ( #T )
        pnd_S_Rechar_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt());                                                                           //Natural: ADD IRA-RECHARACTER-AMT TO #S-RECHAR-AMT ( #T )
        pnd_S_Fmv_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Fmv_Amt());                                                                                      //Natural: ADD IRA-FMV-AMT TO #S-FMV-AMT ( #T )
        pnd_S_Roth_Conv_Amt.getValue(pnd_T).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt());                                                                          //Natural: ADD IRA-ROTH-CONV-AMT TO #S-ROTH-CONV-AMT ( #T )
    }
    private void sub_Display_Control_Totals() throws Exception                                                                                                            //Natural: DISPLAY-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------------
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 02 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"       (TIAA)       ",NEWLINE);                                                                     //Natural: WRITE ( 02 ) NOTITLE 01T '       (TIAA)       ' /
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(33),pnd_T_Trans_Count.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZ9"),new         //Natural: WRITE ( 02 ) NOTITLE 'Transaction Count...' 33T #T-TRANS-COUNT ( I1 ) 54T #T-TRANS-COUNT ( I2 ) 75T #T-TRANS-COUNT ( I3 )
            TabSetting(54),pnd_T_Trans_Count.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(75),pnd_T_Trans_Count.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Classic.............",new TabSetting(26),pnd_T_Classic_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 02 ) NOTITLE 'Classic.............' 26T #T-CLASSIC-AMT ( I1 ) 47T #T-CLASSIC-AMT ( I2 ) 68T #T-CLASSIC-AMT ( I3 )
            TabSetting(47),pnd_T_Classic_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_T_Classic_Amt.getValue(i3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Roth................",new TabSetting(26),pnd_T_Roth_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new     //Natural: WRITE ( 02 ) NOTITLE 'Roth................' 26T #T-ROTH-AMT ( I1 ) 47T #T-ROTH-AMT ( I2 ) 68T #T-ROTH-AMT ( I3 )
            TabSetting(47),pnd_T_Roth_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_T_Roth_Amt.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_T_Rollover_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new //Natural: WRITE ( 02 ) NOTITLE 'Rollover............' 26T #T-ROLLOVER-AMT ( I1 ) 47T #T-ROLLOVER-AMT ( I2 ) 68T #T-ROLLOVER-AMT ( I3 )
            TabSetting(47),pnd_T_Rollover_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_T_Rollover_Amt.getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_T_Rechar_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new   //Natural: WRITE ( 02 ) NOTITLE 'Recharacter.........' 26T #T-RECHAR-AMT ( I1 ) 47T #T-RECHAR-AMT ( I2 ) 68T #T-RECHAR-AMT ( I3 )
            TabSetting(47),pnd_T_Rechar_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_T_Rechar_Amt.getValue(i3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_T_Sep_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new      //Natural: WRITE ( 02 ) NOTITLE 'SEP.................' 26T #T-SEP-AMT ( I1 ) 47T #T-SEP-AMT ( I2 ) 68T #T-SEP-AMT ( I3 )
            TabSetting(47),pnd_T_Sep_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_T_Sep_Amt.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(25),pnd_T_Fmv_Amt.getValue(i1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new     //Natural: WRITE ( 02 ) NOTITLE 'Fair Market.........' 25T #T-FMV-AMT ( I1 ) 46T #T-FMV-AMT ( I2 ) 67T #T-FMV-AMT ( I3 )
            TabSetting(46),pnd_T_Fmv_Amt.getValue(i2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(67),pnd_T_Fmv_Amt.getValue(i3), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_T_Roth_Conv_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 02 ) NOTITLE 'Roth Conversion.....' 26T #T-ROTH-CONV-AMT ( I1 ) 47T #T-ROTH-CONV-AMT ( I2 ) 68T #T-ROTH-CONV-AMT ( I3 )
            TabSetting(47),pnd_T_Roth_Conv_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_T_Roth_Conv_Amt.getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 3);                                                                                                                                          //Natural: SKIP ( 02 ) 3
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"       (CREF)       ",NEWLINE);                                                                     //Natural: WRITE ( 02 ) NOTITLE 01T '       (CREF)       ' /
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(33),pnd_C_Trans_Count.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZ9"),new         //Natural: WRITE ( 02 ) NOTITLE 'Transaction Count...' 33T #C-TRANS-COUNT ( I1 ) 54T #C-TRANS-COUNT ( I2 ) 75T #C-TRANS-COUNT ( I3 )
            TabSetting(54),pnd_C_Trans_Count.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(75),pnd_C_Trans_Count.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Classic.............",new TabSetting(26),pnd_C_Classic_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 02 ) NOTITLE 'Classic.............' 26T #C-CLASSIC-AMT ( I1 ) 47T #C-CLASSIC-AMT ( I2 ) 68T #C-CLASSIC-AMT ( I3 )
            TabSetting(47),pnd_C_Classic_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_C_Classic_Amt.getValue(i3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Roth................",new TabSetting(26),pnd_C_Roth_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new     //Natural: WRITE ( 02 ) NOTITLE 'Roth................' 26T #C-ROTH-AMT ( I1 ) 47T #C-ROTH-AMT ( I2 ) 68T #C-ROTH-AMT ( I3 )
            TabSetting(47),pnd_C_Roth_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_C_Roth_Amt.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_C_Rollover_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new //Natural: WRITE ( 02 ) NOTITLE 'Rollover............' 26T #C-ROLLOVER-AMT ( I1 ) 47T #C-ROLLOVER-AMT ( I2 ) 68T #C-ROLLOVER-AMT ( I3 )
            TabSetting(47),pnd_C_Rollover_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_C_Rollover_Amt.getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_C_Rechar_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new   //Natural: WRITE ( 02 ) NOTITLE 'Recharacter.........' 26T #C-RECHAR-AMT ( I1 ) 47T #C-RECHAR-AMT ( I2 ) 68T #C-RECHAR-AMT ( I3 )
            TabSetting(47),pnd_C_Rechar_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_C_Rechar_Amt.getValue(i3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_C_Sep_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new      //Natural: WRITE ( 02 ) NOTITLE 'SEP.................' 26T #C-SEP-AMT ( I1 ) 47T #C-SEP-AMT ( I2 ) 68T #C-SEP-AMT ( I3 )
            TabSetting(47),pnd_C_Sep_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_C_Sep_Amt.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(25),pnd_C_Fmv_Amt.getValue(i1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new     //Natural: WRITE ( 02 ) NOTITLE 'Fair Market.........' 25T #C-FMV-AMT ( I1 ) 46T #C-FMV-AMT ( I2 ) 67T #C-FMV-AMT ( I3 )
            TabSetting(46),pnd_C_Fmv_Amt.getValue(i2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(67),pnd_C_Fmv_Amt.getValue(i3), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_C_Roth_Conv_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 02 ) NOTITLE 'Roth Conversion.....' 26T #C-ROTH-CONV-AMT ( I1 ) 47T #C-ROTH-CONV-AMT ( I2 ) 68T #C-ROTH-CONV-AMT ( I3 )
            TabSetting(47),pnd_C_Roth_Conv_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_C_Roth_Conv_Amt.getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 3);                                                                                                                                          //Natural: SKIP ( 02 ) 3
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"       (LIFE)       ",NEWLINE);                                                                     //Natural: WRITE ( 02 ) NOTITLE 01T '       (LIFE)       ' /
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(33),pnd_L_Trans_Count.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZ9"),new         //Natural: WRITE ( 02 ) NOTITLE 'Transaction Count...' 33T #L-TRANS-COUNT ( I1 ) 54T #L-TRANS-COUNT ( I2 ) 75T #L-TRANS-COUNT ( I3 )
            TabSetting(54),pnd_L_Trans_Count.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(75),pnd_L_Trans_Count.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Classic.............",new TabSetting(26),pnd_L_Classic_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 02 ) NOTITLE 'Classic.............' 26T #L-CLASSIC-AMT ( I1 ) 47T #L-CLASSIC-AMT ( I2 ) 68T #L-CLASSIC-AMT ( I3 )
            TabSetting(47),pnd_L_Classic_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_L_Classic_Amt.getValue(i3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Roth................",new TabSetting(26),pnd_L_Roth_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new     //Natural: WRITE ( 02 ) NOTITLE 'Roth................' 26T #L-ROTH-AMT ( I1 ) 47T #L-ROTH-AMT ( I2 ) 68T #L-ROTH-AMT ( I3 )
            TabSetting(47),pnd_L_Roth_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_L_Roth_Amt.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_L_Rollover_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new //Natural: WRITE ( 02 ) NOTITLE 'Rollover............' 26T #L-ROLLOVER-AMT ( I1 ) 47T #L-ROLLOVER-AMT ( I2 ) 68T #L-ROLLOVER-AMT ( I3 )
            TabSetting(47),pnd_L_Rollover_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_L_Rollover_Amt.getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_L_Rechar_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new   //Natural: WRITE ( 02 ) NOTITLE 'Recharacter.........' 26T #L-RECHAR-AMT ( I1 ) 47T #L-RECHAR-AMT ( I2 ) 68T #L-RECHAR-AMT ( I3 )
            TabSetting(47),pnd_L_Rechar_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_L_Rechar_Amt.getValue(i3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_L_Sep_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new      //Natural: WRITE ( 02 ) NOTITLE 'SEP.................' 26T #L-SEP-AMT ( I1 ) 47T #L-SEP-AMT ( I2 ) 68T #L-SEP-AMT ( I3 )
            TabSetting(47),pnd_L_Sep_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_L_Sep_Amt.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(25),pnd_L_Fmv_Amt.getValue(i1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new     //Natural: WRITE ( 02 ) NOTITLE 'Fair Market.........' 25T #L-FMV-AMT ( I1 ) 46T #L-FMV-AMT ( I2 ) 67T #L-FMV-AMT ( I3 )
            TabSetting(46),pnd_L_Fmv_Amt.getValue(i2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(67),pnd_L_Fmv_Amt.getValue(i3), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_L_Roth_Conv_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 02 ) NOTITLE 'Roth Conversion.....' 26T #L-ROTH-CONV-AMT ( I1 ) 47T #L-ROTH-CONV-AMT ( I2 ) 68T #L-ROTH-CONV-AMT ( I3 )
            TabSetting(47),pnd_L_Roth_Conv_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_L_Roth_Conv_Amt.getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 3);                                                                                                                                          //Natural: SKIP ( 02 ) 3
        //* * 09-24-03 ADDED TCII BY FRANK
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"       (TCII)       ",NEWLINE);                                                                     //Natural: WRITE ( 02 ) NOTITLE 01T '       (TCII)       ' /
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(33),pnd_S_Trans_Count.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZ9"),new         //Natural: WRITE ( 02 ) NOTITLE 'Transaction Count...' 33T #S-TRANS-COUNT ( I1 ) 54T #S-TRANS-COUNT ( I2 ) 75T #S-TRANS-COUNT ( I3 )
            TabSetting(54),pnd_S_Trans_Count.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(75),pnd_S_Trans_Count.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Classic.............",new TabSetting(26),pnd_S_Classic_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 02 ) NOTITLE 'Classic.............' 26T #S-CLASSIC-AMT ( I1 ) 47T #S-CLASSIC-AMT ( I2 ) 68T #S-CLASSIC-AMT ( I3 )
            TabSetting(47),pnd_S_Classic_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_S_Classic_Amt.getValue(i3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Roth................",new TabSetting(26),pnd_S_Roth_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new     //Natural: WRITE ( 02 ) NOTITLE 'Roth................' 26T #S-ROTH-AMT ( I1 ) 47T #S-ROTH-AMT ( I2 ) 68T #S-ROTH-AMT ( I3 )
            TabSetting(47),pnd_S_Roth_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_S_Roth_Amt.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_S_Rollover_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new //Natural: WRITE ( 02 ) NOTITLE 'Rollover............' 26T #S-ROLLOVER-AMT ( I1 ) 47T #S-ROLLOVER-AMT ( I2 ) 68T #S-ROLLOVER-AMT ( I3 )
            TabSetting(47),pnd_S_Rollover_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_S_Rollover_Amt.getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_S_Rechar_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new   //Natural: WRITE ( 02 ) NOTITLE 'Recharacter.........' 26T #S-RECHAR-AMT ( I1 ) 47T #S-RECHAR-AMT ( I2 ) 68T #S-RECHAR-AMT ( I3 )
            TabSetting(47),pnd_S_Rechar_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_S_Rechar_Amt.getValue(i3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_S_Sep_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new      //Natural: WRITE ( 02 ) NOTITLE 'SEP.................' 26T #S-SEP-AMT ( I1 ) 47T #S-SEP-AMT ( I2 ) 68T #S-SEP-AMT ( I3 )
            TabSetting(47),pnd_S_Sep_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_S_Sep_Amt.getValue(i3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(25),pnd_S_Fmv_Amt.getValue(i1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new     //Natural: WRITE ( 02 ) NOTITLE 'Fair Market.........' 25T #S-FMV-AMT ( I1 ) 46T #S-FMV-AMT ( I2 ) 67T #S-FMV-AMT ( I3 )
            TabSetting(46),pnd_S_Fmv_Amt.getValue(i2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(67),pnd_S_Fmv_Amt.getValue(i3), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_S_Roth_Conv_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 02 ) NOTITLE 'Roth Conversion.....' 26T #S-ROTH-CONV-AMT ( I1 ) 47T #S-ROTH-CONV-AMT ( I2 ) 68T #S-ROTH-CONV-AMT ( I3 )
            TabSetting(47),pnd_S_Roth_Conv_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_S_Roth_Conv_Amt.getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(2, 3);                                                                                                                                          //Natural: SKIP ( 02 ) 3
    }
    private void sub_Update_General_Totals() throws Exception                                                                                                             //Natural: UPDATE-GENERAL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------
        pnd_T_Trans_Count.getValue(4,":",6).nadd(pnd_T_Trans_Count.getValue(1,":",3));                                                                                    //Natural: ADD #T-TRANS-COUNT ( 1:3 ) TO #T-TRANS-COUNT ( 4:6 )
        pnd_T_Classic_Amt.getValue(4,":",6).nadd(pnd_T_Classic_Amt.getValue(1,":",3));                                                                                    //Natural: ADD #T-CLASSIC-AMT ( 1:3 ) TO #T-CLASSIC-AMT ( 4:6 )
        pnd_T_Roth_Amt.getValue(4,":",6).nadd(pnd_T_Roth_Amt.getValue(1,":",3));                                                                                          //Natural: ADD #T-ROTH-AMT ( 1:3 ) TO #T-ROTH-AMT ( 4:6 )
        pnd_T_Rollover_Amt.getValue(4,":",6).nadd(pnd_T_Rollover_Amt.getValue(1,":",3));                                                                                  //Natural: ADD #T-ROLLOVER-AMT ( 1:3 ) TO #T-ROLLOVER-AMT ( 4:6 )
        pnd_T_Rechar_Amt.getValue(4,":",6).nadd(pnd_T_Rechar_Amt.getValue(1,":",3));                                                                                      //Natural: ADD #T-RECHAR-AMT ( 1:3 ) TO #T-RECHAR-AMT ( 4:6 )
        pnd_T_Sep_Amt.getValue(4,":",6).nadd(pnd_T_Sep_Amt.getValue(1,":",3));                                                                                            //Natural: ADD #T-SEP-AMT ( 1:3 ) TO #T-SEP-AMT ( 4:6 )
        pnd_T_Fmv_Amt.getValue(4,":",6).nadd(pnd_T_Fmv_Amt.getValue(1,":",3));                                                                                            //Natural: ADD #T-FMV-AMT ( 1:3 ) TO #T-FMV-AMT ( 4:6 )
        pnd_T_Roth_Conv_Amt.getValue(4,":",6).nadd(pnd_T_Roth_Conv_Amt.getValue(1,":",3));                                                                                //Natural: ADD #T-ROTH-CONV-AMT ( 1:3 ) TO #T-ROTH-CONV-AMT ( 4:6 )
        pnd_C_Trans_Count.getValue(4,":",6).nadd(pnd_C_Trans_Count.getValue(1,":",3));                                                                                    //Natural: ADD #C-TRANS-COUNT ( 1:3 ) TO #C-TRANS-COUNT ( 4:6 )
        pnd_C_Classic_Amt.getValue(4,":",6).nadd(pnd_C_Classic_Amt.getValue(1,":",3));                                                                                    //Natural: ADD #C-CLASSIC-AMT ( 1:3 ) TO #C-CLASSIC-AMT ( 4:6 )
        pnd_C_Roth_Amt.getValue(4,":",6).nadd(pnd_C_Roth_Amt.getValue(1,":",3));                                                                                          //Natural: ADD #C-ROTH-AMT ( 1:3 ) TO #C-ROTH-AMT ( 4:6 )
        pnd_C_Rollover_Amt.getValue(4,":",6).nadd(pnd_C_Rollover_Amt.getValue(1,":",3));                                                                                  //Natural: ADD #C-ROLLOVER-AMT ( 1:3 ) TO #C-ROLLOVER-AMT ( 4:6 )
        pnd_C_Rechar_Amt.getValue(4,":",6).nadd(pnd_C_Rechar_Amt.getValue(1,":",3));                                                                                      //Natural: ADD #C-RECHAR-AMT ( 1:3 ) TO #C-RECHAR-AMT ( 4:6 )
        pnd_C_Sep_Amt.getValue(4,":",6).nadd(pnd_C_Sep_Amt.getValue(1,":",3));                                                                                            //Natural: ADD #C-SEP-AMT ( 1:3 ) TO #C-SEP-AMT ( 4:6 )
        pnd_C_Fmv_Amt.getValue(4,":",6).nadd(pnd_C_Fmv_Amt.getValue(1,":",3));                                                                                            //Natural: ADD #C-FMV-AMT ( 1:3 ) TO #C-FMV-AMT ( 4:6 )
        pnd_C_Roth_Conv_Amt.getValue(4,":",6).nadd(pnd_C_Roth_Conv_Amt.getValue(1,":",3));                                                                                //Natural: ADD #C-ROTH-CONV-AMT ( 1:3 ) TO #C-ROTH-CONV-AMT ( 4:6 )
        pnd_L_Trans_Count.getValue(4,":",6).nadd(pnd_L_Trans_Count.getValue(1,":",3));                                                                                    //Natural: ADD #L-TRANS-COUNT ( 1:3 ) TO #L-TRANS-COUNT ( 4:6 )
        pnd_L_Classic_Amt.getValue(4,":",6).nadd(pnd_L_Classic_Amt.getValue(1,":",3));                                                                                    //Natural: ADD #L-CLASSIC-AMT ( 1:3 ) TO #L-CLASSIC-AMT ( 4:6 )
        pnd_L_Roth_Amt.getValue(4,":",6).nadd(pnd_L_Roth_Amt.getValue(1,":",3));                                                                                          //Natural: ADD #L-ROTH-AMT ( 1:3 ) TO #L-ROTH-AMT ( 4:6 )
        pnd_L_Rollover_Amt.getValue(4,":",6).nadd(pnd_L_Rollover_Amt.getValue(1,":",3));                                                                                  //Natural: ADD #L-ROLLOVER-AMT ( 1:3 ) TO #L-ROLLOVER-AMT ( 4:6 )
        pnd_L_Rechar_Amt.getValue(4,":",6).nadd(pnd_L_Rechar_Amt.getValue(1,":",3));                                                                                      //Natural: ADD #L-RECHAR-AMT ( 1:3 ) TO #L-RECHAR-AMT ( 4:6 )
        pnd_L_Sep_Amt.getValue(4,":",6).nadd(pnd_L_Sep_Amt.getValue(1,":",3));                                                                                            //Natural: ADD #L-SEP-AMT ( 1:3 ) TO #L-SEP-AMT ( 4:6 )
        pnd_L_Fmv_Amt.getValue(4,":",6).nadd(pnd_L_Fmv_Amt.getValue(1,":",3));                                                                                            //Natural: ADD #L-FMV-AMT ( 1:3 ) TO #L-FMV-AMT ( 4:6 )
        pnd_L_Roth_Conv_Amt.getValue(4,":",6).nadd(pnd_L_Roth_Conv_Amt.getValue(1,":",3));                                                                                //Natural: ADD #L-ROTH-CONV-AMT ( 1:3 ) TO #L-ROTH-CONV-AMT ( 4:6 )
        //* * 09-24-03 ADDED SERVICES BY FRANK
        pnd_S_Trans_Count.getValue(4,":",6).nadd(pnd_S_Trans_Count.getValue(1,":",3));                                                                                    //Natural: ADD #S-TRANS-COUNT ( 1:3 ) TO #S-TRANS-COUNT ( 4:6 )
        pnd_S_Classic_Amt.getValue(4,":",6).nadd(pnd_S_Classic_Amt.getValue(1,":",3));                                                                                    //Natural: ADD #S-CLASSIC-AMT ( 1:3 ) TO #S-CLASSIC-AMT ( 4:6 )
        pnd_S_Roth_Amt.getValue(4,":",6).nadd(pnd_S_Roth_Amt.getValue(1,":",3));                                                                                          //Natural: ADD #S-ROTH-AMT ( 1:3 ) TO #S-ROTH-AMT ( 4:6 )
        pnd_S_Rollover_Amt.getValue(4,":",6).nadd(pnd_S_Rollover_Amt.getValue(1,":",3));                                                                                  //Natural: ADD #S-ROLLOVER-AMT ( 1:3 ) TO #S-ROLLOVER-AMT ( 4:6 )
        pnd_S_Rechar_Amt.getValue(4,":",6).nadd(pnd_S_Rechar_Amt.getValue(1,":",3));                                                                                      //Natural: ADD #S-RECHAR-AMT ( 1:3 ) TO #S-RECHAR-AMT ( 4:6 )
        pnd_S_Sep_Amt.getValue(4,":",6).nadd(pnd_S_Sep_Amt.getValue(1,":",3));                                                                                            //Natural: ADD #S-SEP-AMT ( 1:3 ) TO #S-SEP-AMT ( 4:6 )
        pnd_S_Fmv_Amt.getValue(4,":",6).nadd(pnd_S_Fmv_Amt.getValue(1,":",3));                                                                                            //Natural: ADD #S-FMV-AMT ( 1:3 ) TO #S-FMV-AMT ( 4:6 )
        pnd_S_Roth_Conv_Amt.getValue(4,":",6).nadd(pnd_S_Roth_Conv_Amt.getValue(1,":",3));                                                                                //Natural: ADD #S-ROTH-CONV-AMT ( 1:3 ) TO #S-ROTH-CONV-AMT ( 4:6 )
    }
    private void sub_Reset_Control_Totals() throws Exception                                                                                                              //Natural: RESET-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------
        pnd_T_Trans_Count.getValue(1,":",3).reset();                                                                                                                      //Natural: RESET #T-TRANS-COUNT ( 1:3 ) #C-TRANS-COUNT ( 1:3 ) #L-TRANS-COUNT ( 1:3 ) #T-CLASSIC-AMT ( 1:3 ) #C-CLASSIC-AMT ( 1:3 ) #L-CLASSIC-AMT ( 1:3 ) #T-ROTH-AMT ( 1:3 ) #C-ROTH-AMT ( 1:3 ) #L-ROTH-AMT ( 1:3 ) #T-ROLLOVER-AMT ( 1:3 ) #C-ROLLOVER-AMT ( 1:3 ) #L-ROLLOVER-AMT ( 1:3 ) #T-RECHAR-AMT ( 1:3 ) #C-RECHAR-AMT ( 1:3 ) #L-RECHAR-AMT ( 1:3 ) #T-SEP-AMT ( 1:3 ) #C-SEP-AMT ( 1:3 ) #L-SEP-AMT ( 1:3 ) #T-FMV-AMT ( 1:3 ) #C-FMV-AMT ( 1:3 ) #L-FMV-AMT ( 1:3 ) #T-ROTH-CONV-AMT ( 1:3 ) #C-ROTH-CONV-AMT ( 1:3 ) #L-ROTH-CONV-AMT ( 1:3 ) #S-TRANS-COUNT ( 1:3 ) #S-CLASSIC-AMT ( 1:3 ) #S-ROTH-AMT ( 1:3 ) #S-ROLLOVER-AMT ( 1:3 ) #S-RECHAR-AMT ( 1:3 ) #S-SEP-AMT ( 1:3 ) #S-FMV-AMT ( 1:3 ) #S-ROTH-CONV-AMT ( 1:3 )
        pnd_C_Trans_Count.getValue(1,":",3).reset();
        pnd_L_Trans_Count.getValue(1,":",3).reset();
        pnd_T_Classic_Amt.getValue(1,":",3).reset();
        pnd_C_Classic_Amt.getValue(1,":",3).reset();
        pnd_L_Classic_Amt.getValue(1,":",3).reset();
        pnd_T_Roth_Amt.getValue(1,":",3).reset();
        pnd_C_Roth_Amt.getValue(1,":",3).reset();
        pnd_L_Roth_Amt.getValue(1,":",3).reset();
        pnd_T_Rollover_Amt.getValue(1,":",3).reset();
        pnd_C_Rollover_Amt.getValue(1,":",3).reset();
        pnd_L_Rollover_Amt.getValue(1,":",3).reset();
        pnd_T_Rechar_Amt.getValue(1,":",3).reset();
        pnd_C_Rechar_Amt.getValue(1,":",3).reset();
        pnd_L_Rechar_Amt.getValue(1,":",3).reset();
        pnd_T_Sep_Amt.getValue(1,":",3).reset();
        pnd_C_Sep_Amt.getValue(1,":",3).reset();
        pnd_L_Sep_Amt.getValue(1,":",3).reset();
        pnd_T_Fmv_Amt.getValue(1,":",3).reset();
        pnd_C_Fmv_Amt.getValue(1,":",3).reset();
        pnd_L_Fmv_Amt.getValue(1,":",3).reset();
        pnd_T_Roth_Conv_Amt.getValue(1,":",3).reset();
        pnd_C_Roth_Conv_Amt.getValue(1,":",3).reset();
        pnd_L_Roth_Conv_Amt.getValue(1,":",3).reset();
        pnd_S_Trans_Count.getValue(1,":",3).reset();
        pnd_S_Classic_Amt.getValue(1,":",3).reset();
        pnd_S_Roth_Amt.getValue(1,":",3).reset();
        pnd_S_Rollover_Amt.getValue(1,":",3).reset();
        pnd_S_Rechar_Amt.getValue(1,":",3).reset();
        pnd_S_Sep_Amt.getValue(1,":",3).reset();
        pnd_S_Fmv_Amt.getValue(1,":",3).reset();
        pnd_S_Roth_Conv_Amt.getValue(1,":",3).reset();
    }
    private void sub_Read_Summary_Totals_Record() throws Exception                                                                                                        //Natural: READ-SUMMARY-TOTALS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Summary_Found.setValue(false);                                                                                                                                //Natural: ASSIGN #SUMMARY-FOUND := FALSE
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 02 RECORD IRA-SUM-RECORD
        while (condition(getWorkFiles().read(2, ldaTwrl465b.getIra_Sum_Record())))
        {
            if (condition(ldaTwrl465b.getIra_Sum_Record_Ira_Sum_Id().equals("IRA")))                                                                                      //Natural: IF IRA-SUM-RECORD.IRA-SUM-ID = 'IRA'
            {
                pnd_Summary_Found.setValue(true);                                                                                                                         //Natural: ASSIGN #SUMMARY-FOUND := TRUE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Summary_Found.equals(true)))                                                                                                                    //Natural: IF #SUMMARY-FOUND = TRUE
        {
            getWorkFiles().close(2);                                                                                                                                      //Natural: CLOSE WORK FILE 02
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"error - Source Payment : ",ldaTwrl465b.getIra_Sum_Record_Ira_Sum_Id(),new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) '***' 06T 'error - Source Payment : ' IRA-SUM-ID 77T '***' / '***' 06T 'Summary Totals Record Is Missing From' 77T '***' / '***' 06T 'The Summary Totals File CMWKF02 (Work File 02).' 77T '***'
                TabSetting(6),"Summary Totals Record Is Missing From",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"The Summary Totals File CMWKF02 (Work File 02).",new 
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(91);  if (true) return;                                                                                                                     //Natural: TERMINATE 91
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Cite_Participant() throws Exception                                                                                                                  //Natural: CITE-PARTICIPANT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Tax_Id.setValue(ldaTwrl462a.getIra_Record_Ira_Tax_Id());                                                                    //Natural: ASSIGN #TWRPARTI-TAX-ID := IRA-TAX-ID
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Status.setValue(" ");                                                                                                       //Natural: ASSIGN #TWRPARTI-STATUS := ' '
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Part_Eff_End_Dte.setValue("99999999");                                                                                      //Natural: ASSIGN #TWRPARTI-PART-EFF-END-DTE := '99999999'
        vw_prtcpnt.startDatabaseRead                                                                                                                                      //Natural: READ ( 1 ) PRTCPNT WITH TWRPARTI-CURR-REC-SD = #TWRPARTI-CURR-REC-SD
        (
        "P1",
        new Wc[] { new Wc("TWRPARTI_CURR_REC_SD", ">=", pnd_Twrparti_Curr_Rec_Sd, WcType.BY) },
        new Oc[] { new Oc("TWRPARTI_CURR_REC_SD", "ASC") },
        1
        );
        P1:
        while (condition(vw_prtcpnt.readNextRow("P1")))
        {
            if (condition(prtcpnt_Twrparti_Tax_Id.equals(ldaTwrl462a.getIra_Record_Ira_Tax_Id()) && prtcpnt_Twrparti_Status.equals(" ") && prtcpnt_Twrparti_Part_Eff_End_Dte.equals("99999999"))) //Natural: IF TWRPARTI-TAX-ID = IRA-TAX-ID AND TWRPARTI-STATUS = ' ' AND TWRPARTI-PART-EFF-END-DTE = '99999999'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(prtcpnt_Twrparti_Citation_Ind.equals("C")))                                                                                                     //Natural: IF PRTCPNT.TWRPARTI-CITATION-IND = 'C'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                P2:                                                                                                                                                       //Natural: GET UPDPAR *ISN ( P1. )
                vw_updpar.readByID(vw_prtcpnt.getAstISN("P1"), "P2");
                updpar_Twrparti_Citation_Ind.setValue("C");                                                                                                               //Natural: ASSIGN UPDPAR.TWRPARTI-CITATION-IND := 'C'
                vw_updpar.updateDBRow("P2");                                                                                                                              //Natural: UPDATE ( P2. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Lookup_Participant() throws Exception                                                                                                                //Natural: LOOKUP-PARTICIPANT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Tax_Id.setValue(ldaTwrl462a.getIra_Record_Ira_Tax_Id());                                                                    //Natural: ASSIGN #TWRPARTI-TAX-ID := IRA-TAX-ID
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Status.setValue(" ");                                                                                                       //Natural: ASSIGN #TWRPARTI-STATUS := ' '
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Part_Eff_End_Dte.setValue("99999999");                                                                                      //Natural: ASSIGN #TWRPARTI-PART-EFF-END-DTE := '99999999'
        pnd_Part_Ein_Found.setValue(false);                                                                                                                               //Natural: ASSIGN #PART-EIN-FOUND := FALSE
        vw_prtcpnt.startDatabaseRead                                                                                                                                      //Natural: READ ( 1 ) PRTCPNT WITH TWRPARTI-CURR-REC-SD = #TWRPARTI-CURR-REC-SD
        (
        "L1",
        new Wc[] { new Wc("TWRPARTI_CURR_REC_SD", ">=", pnd_Twrparti_Curr_Rec_Sd, WcType.BY) },
        new Oc[] { new Oc("TWRPARTI_CURR_REC_SD", "ASC") },
        1
        );
        L1:
        while (condition(vw_prtcpnt.readNextRow("L1")))
        {
            if (condition(prtcpnt_Twrparti_Tax_Id.equals(ldaTwrl462a.getIra_Record_Ira_Tax_Id()) && prtcpnt_Twrparti_Status.equals(" ") && prtcpnt_Twrparti_Part_Eff_End_Dte.equals("99999999")  //Natural: IF TWRPARTI-TAX-ID = IRA-TAX-ID AND TWRPARTI-STATUS = ' ' AND TWRPARTI-PART-EFF-END-DTE = '99999999' AND TWRPARTI-TAX-ID-TYPE = '2'
                && prtcpnt_Twrparti_Tax_Id_Type.equals("2")))
            {
                pnd_Part_Ein_Found.setValue(true);                                                                                                                        //Natural: ASSIGN #PART-EIN-FOUND := TRUE
                pnd_Part_Tax_Id_Type.setValue(prtcpnt_Twrparti_Tax_Id_Type);                                                                                              //Natural: ASSIGN #PART-TAX-ID-TYPE := PRTCPNT.TWRPARTI-TAX-ID-TYPE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Store_Control_Totals() throws Exception                                                                                                              //Natural: STORE-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp.setValue(" ");                                                                                                                    //Natural: ASSIGN #TIRCNTL-5-Y-SC-CO-TO-FRM-SP := ' '
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_5.setValue(5);                                                                                                        //Natural: ASSIGN #SP-CNTL-5 := 5
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Year.setValue(pnd_Tircntl_Tax_Year_A_Pnd_Tircntl_Tax_Year);                                                           //Natural: ASSIGN #SP-CNTL-YEAR := #TIRCNTL-TAX-YEAR
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Source_Code.setValue(pnd_Tircntl_Rpt_Source_Code);                                                                    //Natural: ASSIGN #SP-CNTL-SOURCE-CODE := #TIRCNTL-RPT-SOURCE-CODE
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Company.setValue(" ");                                                                                                //Natural: ASSIGN #SP-CNTL-COMPANY := ' '
        vw_cntlr.startDatabaseRead                                                                                                                                        //Natural: READ ( 1 ) CNTLR WITH TIRCNTL-5-Y-SC-CO-TO-FRM-SP = #TIRCNTL-5-Y-SC-CO-TO-FRM-SP
        (
        "RC1",
        new Wc[] { new Wc("TIRCNTL_5_Y_SC_CO_TO_FRM_SP", ">=", pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp, WcType.BY) },
        new Oc[] { new Oc("TIRCNTL_5_Y_SC_CO_TO_FRM_SP", "ASC") },
        1
        );
        RC1:
        while (condition(vw_cntlr.readNextRow("RC1")))
        {
            if (condition(cntlr_Tircntl_Tbl_Nbr.equals(5) && cntlr_Tircntl_Tax_Year.equals(pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Year) && cntlr_Tircntl_Rpt_Source_Code.equals(pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Source_Code)  //Natural: IF CNTLR.TIRCNTL-TBL-NBR = 5 AND CNTLR.TIRCNTL-TAX-YEAR = #SP-CNTL-YEAR AND CNTLR.TIRCNTL-RPT-SOURCE-CODE = #SP-CNTL-SOURCE-CODE AND CNTLR.TIRCNTL-COMPANY-CDE = ' '
                && cntlr_Tircntl_Company_Cde.equals(" ")))
            {
                RCU1:                                                                                                                                                     //Natural: GET CNTLU *ISN ( RC1. )
                vw_cntlu.readByID(vw_cntlr.getAstISN("RC1"), "RCU1");
                cntlu_Tircntl_Frm_Intrfce_Dte.setValue(pnd_Tircntl_Frm_Intrfce_Dte_A_Pnd_Tircntl_Frm_Intrfce_Dte);                                                        //Natural: ASSIGN CNTLU.TIRCNTL-FRM-INTRFCE-DTE := #TIRCNTL-FRM-INTRFCE-DTE
                cntlu_Tircntl_To_Intrfce_Dte.setValue(pnd_Tircntl_To_Intrfce_Dte_A_Pnd_Tircntl_To_Intrfce_Dte);                                                           //Natural: ASSIGN CNTLU.TIRCNTL-TO-INTRFCE-DTE := #TIRCNTL-TO-INTRFCE-DTE
                vw_cntlu.updateDBRow("RCU1");                                                                                                                             //Natural: UPDATE ( RCU1. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaTwrl820e.getCntl_Tircntl_Tbl_Nbr().setValue(5);                                                                                                                //Natural: ASSIGN CNTL.TIRCNTL-TBL-NBR := 5
        ldaTwrl820e.getCntl_Tircntl_Tax_Year().setValue(pnd_Tircntl_Tax_Year_A_Pnd_Tircntl_Tax_Year);                                                                     //Natural: ASSIGN CNTL.TIRCNTL-TAX-YEAR := #TIRCNTL-TAX-YEAR
        ldaTwrl820e.getCntl_Tircntl_Seq_Nbr().setValue(1);                                                                                                                //Natural: ASSIGN CNTL.TIRCNTL-SEQ-NBR := 1
        ldaTwrl820e.getCntl_Tircntl_Rpt_Source_Code().setValue(pnd_Tircntl_Rpt_Source_Code);                                                                              //Natural: ASSIGN CNTL.TIRCNTL-RPT-SOURCE-CODE := #TIRCNTL-RPT-SOURCE-CODE
        ldaTwrl820e.getCntl_Tircntl_Frm_Intrfce_Dte().setValue(pnd_Tircntl_Frm_Intrfce_Dte_A_Pnd_Tircntl_Frm_Intrfce_Dte);                                                //Natural: ASSIGN CNTL.TIRCNTL-FRM-INTRFCE-DTE := #TIRCNTL-FRM-INTRFCE-DTE
        ldaTwrl820e.getCntl_Tircntl_To_Intrfce_Dte().setValue(pnd_Tircntl_To_Intrfce_Dte_A_Pnd_Tircntl_To_Intrfce_Dte);                                                   //Natural: ASSIGN CNTL.TIRCNTL-TO-INTRFCE-DTE := #TIRCNTL-TO-INTRFCE-DTE
        ldaTwrl820e.getCntl_Tircntl_Company_Cde().setValue("T");                                                                                                          //Natural: ASSIGN CNTL.TIRCNTL-COMPANY-CDE := 'T'
        ldaTwrl820e.getCntl_Tircntl_Input_Recs_Tot().setValue(pnd_T_Trans_Count.getValue(3));                                                                             //Natural: ASSIGN CNTL.TIRCNTL-INPUT-RECS-TOT := #T-TRANS-COUNT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Gross_Amt().setValue(pnd_T_Classic_Amt.getValue(3));                                                                            //Natural: ASSIGN CNTL.TIRCNTL-INPUT-GROSS-AMT := #T-CLASSIC-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Ivt_Amt().setValue(pnd_T_Roth_Amt.getValue(3));                                                                                 //Natural: ASSIGN CNTL.TIRCNTL-INPUT-IVT-AMT := #T-ROTH-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Int_Amt().setValue(pnd_T_Rollover_Amt.getValue(3));                                                                             //Natural: ASSIGN CNTL.TIRCNTL-INPUT-INT-AMT := #T-ROLLOVER-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Nra_Amt().setValue(pnd_T_Rechar_Amt.getValue(3));                                                                               //Natural: ASSIGN CNTL.TIRCNTL-INPUT-NRA-AMT := #T-RECHAR-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Can_Amt().setValue(pnd_T_Sep_Amt.getValue(3));                                                                                  //Natural: ASSIGN CNTL.TIRCNTL-INPUT-CAN-AMT := #T-SEP-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Fed_Wthldg_Amt().setValue(pnd_T_Fmv_Amt.getValue(3));                                                                           //Natural: ASSIGN CNTL.TIRCNTL-INPUT-FED-WTHLDG-AMT := #T-FMV-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_State_Wthldg_Amt().setValue(pnd_T_Roth_Conv_Amt.getValue(3));                                                                   //Natural: ASSIGN CNTL.TIRCNTL-INPUT-STATE-WTHLDG-AMT := #T-ROTH-CONV-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Local_Wthldg_Amt().setValue(0);                                                                                                 //Natural: ASSIGN CNTL.TIRCNTL-INPUT-LOCAL-WTHLDG-AMT := 0.00
        ldaTwrl820e.getCntl_Tircntl_Rpt_Source_Code_Desc().setValue(" ");                                                                                                 //Natural: ASSIGN CNTL.TIRCNTL-RPT-SOURCE-CODE-DESC := ' '
        ldaTwrl820e.getCntl_Tircntl_Rpt_Update_Dte_Time().setValue(pnd_Twrc_Sys_Dte_Time_Pnd_Twrc_Sys_Dte_Time_N);                                                        //Natural: ASSIGN CNTL.TIRCNTL-RPT-UPDATE-DTE-TIME := #TWRC-SYS-DTE-TIME-N
        ldaTwrl820e.getVw_cntl().insertDBRow();                                                                                                                           //Natural: STORE CNTL
        ldaTwrl820e.getCntl_Tircntl_Tbl_Nbr().setValue(5);                                                                                                                //Natural: ASSIGN CNTL.TIRCNTL-TBL-NBR := 5
        ldaTwrl820e.getCntl_Tircntl_Tax_Year().setValue(pnd_Tircntl_Tax_Year_A_Pnd_Tircntl_Tax_Year);                                                                     //Natural: ASSIGN CNTL.TIRCNTL-TAX-YEAR := #TIRCNTL-TAX-YEAR
        ldaTwrl820e.getCntl_Tircntl_Seq_Nbr().setValue(1);                                                                                                                //Natural: ASSIGN CNTL.TIRCNTL-SEQ-NBR := 1
        ldaTwrl820e.getCntl_Tircntl_Rpt_Source_Code().setValue(pnd_Tircntl_Rpt_Source_Code);                                                                              //Natural: ASSIGN CNTL.TIRCNTL-RPT-SOURCE-CODE := #TIRCNTL-RPT-SOURCE-CODE
        ldaTwrl820e.getCntl_Tircntl_Frm_Intrfce_Dte().setValue(pnd_Tircntl_Frm_Intrfce_Dte_A_Pnd_Tircntl_Frm_Intrfce_Dte);                                                //Natural: ASSIGN CNTL.TIRCNTL-FRM-INTRFCE-DTE := #TIRCNTL-FRM-INTRFCE-DTE
        ldaTwrl820e.getCntl_Tircntl_To_Intrfce_Dte().setValue(pnd_Tircntl_To_Intrfce_Dte_A_Pnd_Tircntl_To_Intrfce_Dte);                                                   //Natural: ASSIGN CNTL.TIRCNTL-TO-INTRFCE-DTE := #TIRCNTL-TO-INTRFCE-DTE
        ldaTwrl820e.getCntl_Tircntl_Company_Cde().setValue("C");                                                                                                          //Natural: ASSIGN CNTL.TIRCNTL-COMPANY-CDE := 'C'
        ldaTwrl820e.getCntl_Tircntl_Input_Recs_Tot().setValue(pnd_C_Trans_Count.getValue(3));                                                                             //Natural: ASSIGN CNTL.TIRCNTL-INPUT-RECS-TOT := #C-TRANS-COUNT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Gross_Amt().setValue(pnd_C_Classic_Amt.getValue(3));                                                                            //Natural: ASSIGN CNTL.TIRCNTL-INPUT-GROSS-AMT := #C-CLASSIC-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Ivt_Amt().setValue(pnd_C_Roth_Amt.getValue(3));                                                                                 //Natural: ASSIGN CNTL.TIRCNTL-INPUT-IVT-AMT := #C-ROTH-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Int_Amt().setValue(pnd_C_Rollover_Amt.getValue(3));                                                                             //Natural: ASSIGN CNTL.TIRCNTL-INPUT-INT-AMT := #C-ROLLOVER-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Nra_Amt().setValue(pnd_C_Rechar_Amt.getValue(3));                                                                               //Natural: ASSIGN CNTL.TIRCNTL-INPUT-NRA-AMT := #C-RECHAR-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Can_Amt().setValue(pnd_C_Sep_Amt.getValue(3));                                                                                  //Natural: ASSIGN CNTL.TIRCNTL-INPUT-CAN-AMT := #C-SEP-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Fed_Wthldg_Amt().setValue(pnd_C_Fmv_Amt.getValue(3));                                                                           //Natural: ASSIGN CNTL.TIRCNTL-INPUT-FED-WTHLDG-AMT := #C-FMV-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_State_Wthldg_Amt().setValue(pnd_C_Roth_Conv_Amt.getValue(3));                                                                   //Natural: ASSIGN CNTL.TIRCNTL-INPUT-STATE-WTHLDG-AMT := #C-ROTH-CONV-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Local_Wthldg_Amt().setValue(0);                                                                                                 //Natural: ASSIGN CNTL.TIRCNTL-INPUT-LOCAL-WTHLDG-AMT := 0.00
        ldaTwrl820e.getCntl_Tircntl_Rpt_Source_Code_Desc().setValue(" ");                                                                                                 //Natural: ASSIGN CNTL.TIRCNTL-RPT-SOURCE-CODE-DESC := ' '
        ldaTwrl820e.getCntl_Tircntl_Rpt_Update_Dte_Time().setValue(pnd_Twrc_Sys_Dte_Time_Pnd_Twrc_Sys_Dte_Time_N);                                                        //Natural: ASSIGN CNTL.TIRCNTL-RPT-UPDATE-DTE-TIME := #TWRC-SYS-DTE-TIME-N
        ldaTwrl820e.getVw_cntl().insertDBRow();                                                                                                                           //Natural: STORE CNTL
        ldaTwrl820e.getCntl_Tircntl_Tbl_Nbr().setValue(5);                                                                                                                //Natural: ASSIGN CNTL.TIRCNTL-TBL-NBR := 5
        ldaTwrl820e.getCntl_Tircntl_Tax_Year().setValue(pnd_Tircntl_Tax_Year_A_Pnd_Tircntl_Tax_Year);                                                                     //Natural: ASSIGN CNTL.TIRCNTL-TAX-YEAR := #TIRCNTL-TAX-YEAR
        ldaTwrl820e.getCntl_Tircntl_Seq_Nbr().setValue(1);                                                                                                                //Natural: ASSIGN CNTL.TIRCNTL-SEQ-NBR := 1
        ldaTwrl820e.getCntl_Tircntl_Rpt_Source_Code().setValue(pnd_Tircntl_Rpt_Source_Code);                                                                              //Natural: ASSIGN CNTL.TIRCNTL-RPT-SOURCE-CODE := #TIRCNTL-RPT-SOURCE-CODE
        ldaTwrl820e.getCntl_Tircntl_Frm_Intrfce_Dte().setValue(pnd_Tircntl_Frm_Intrfce_Dte_A_Pnd_Tircntl_Frm_Intrfce_Dte);                                                //Natural: ASSIGN CNTL.TIRCNTL-FRM-INTRFCE-DTE := #TIRCNTL-FRM-INTRFCE-DTE
        ldaTwrl820e.getCntl_Tircntl_To_Intrfce_Dte().setValue(pnd_Tircntl_To_Intrfce_Dte_A_Pnd_Tircntl_To_Intrfce_Dte);                                                   //Natural: ASSIGN CNTL.TIRCNTL-TO-INTRFCE-DTE := #TIRCNTL-TO-INTRFCE-DTE
        ldaTwrl820e.getCntl_Tircntl_Company_Cde().setValue("L");                                                                                                          //Natural: ASSIGN CNTL.TIRCNTL-COMPANY-CDE := 'L'
        ldaTwrl820e.getCntl_Tircntl_Input_Recs_Tot().setValue(pnd_L_Trans_Count.getValue(3));                                                                             //Natural: ASSIGN CNTL.TIRCNTL-INPUT-RECS-TOT := #L-TRANS-COUNT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Gross_Amt().setValue(pnd_L_Classic_Amt.getValue(3));                                                                            //Natural: ASSIGN CNTL.TIRCNTL-INPUT-GROSS-AMT := #L-CLASSIC-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Ivt_Amt().setValue(pnd_L_Roth_Amt.getValue(3));                                                                                 //Natural: ASSIGN CNTL.TIRCNTL-INPUT-IVT-AMT := #L-ROTH-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Int_Amt().setValue(pnd_L_Rollover_Amt.getValue(3));                                                                             //Natural: ASSIGN CNTL.TIRCNTL-INPUT-INT-AMT := #L-ROLLOVER-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Nra_Amt().setValue(pnd_L_Rechar_Amt.getValue(3));                                                                               //Natural: ASSIGN CNTL.TIRCNTL-INPUT-NRA-AMT := #L-RECHAR-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Can_Amt().setValue(pnd_L_Sep_Amt.getValue(3));                                                                                  //Natural: ASSIGN CNTL.TIRCNTL-INPUT-CAN-AMT := #L-SEP-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Fed_Wthldg_Amt().setValue(pnd_L_Fmv_Amt.getValue(3));                                                                           //Natural: ASSIGN CNTL.TIRCNTL-INPUT-FED-WTHLDG-AMT := #L-FMV-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_State_Wthldg_Amt().setValue(pnd_L_Roth_Conv_Amt.getValue(3));                                                                   //Natural: ASSIGN CNTL.TIRCNTL-INPUT-STATE-WTHLDG-AMT := #L-ROTH-CONV-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Local_Wthldg_Amt().setValue(0);                                                                                                 //Natural: ASSIGN CNTL.TIRCNTL-INPUT-LOCAL-WTHLDG-AMT := 0.00
        ldaTwrl820e.getCntl_Tircntl_Rpt_Source_Code_Desc().setValue(" ");                                                                                                 //Natural: ASSIGN CNTL.TIRCNTL-RPT-SOURCE-CODE-DESC := ' '
        ldaTwrl820e.getCntl_Tircntl_Rpt_Update_Dte_Time().setValue(pnd_Twrc_Sys_Dte_Time_Pnd_Twrc_Sys_Dte_Time_N);                                                        //Natural: ASSIGN CNTL.TIRCNTL-RPT-UPDATE-DTE-TIME := #TWRC-SYS-DTE-TIME-N
        ldaTwrl820e.getVw_cntl().insertDBRow();                                                                                                                           //Natural: STORE CNTL
        ldaTwrl820e.getCntl_Tircntl_Tbl_Nbr().setValue(5);                                                                                                                //Natural: ASSIGN CNTL.TIRCNTL-TBL-NBR := 5
        ldaTwrl820e.getCntl_Tircntl_Tax_Year().setValue(pnd_Tircntl_Tax_Year_A_Pnd_Tircntl_Tax_Year);                                                                     //Natural: ASSIGN CNTL.TIRCNTL-TAX-YEAR := #TIRCNTL-TAX-YEAR
        ldaTwrl820e.getCntl_Tircntl_Seq_Nbr().setValue(1);                                                                                                                //Natural: ASSIGN CNTL.TIRCNTL-SEQ-NBR := 1
        ldaTwrl820e.getCntl_Tircntl_Rpt_Source_Code().setValue(pnd_Tircntl_Rpt_Source_Code);                                                                              //Natural: ASSIGN CNTL.TIRCNTL-RPT-SOURCE-CODE := #TIRCNTL-RPT-SOURCE-CODE
        ldaTwrl820e.getCntl_Tircntl_Frm_Intrfce_Dte().setValue(pnd_Tircntl_Frm_Intrfce_Dte_A_Pnd_Tircntl_Frm_Intrfce_Dte);                                                //Natural: ASSIGN CNTL.TIRCNTL-FRM-INTRFCE-DTE := #TIRCNTL-FRM-INTRFCE-DTE
        ldaTwrl820e.getCntl_Tircntl_To_Intrfce_Dte().setValue(pnd_Tircntl_To_Intrfce_Dte_A_Pnd_Tircntl_To_Intrfce_Dte);                                                   //Natural: ASSIGN CNTL.TIRCNTL-TO-INTRFCE-DTE := #TIRCNTL-TO-INTRFCE-DTE
        ldaTwrl820e.getCntl_Tircntl_Company_Cde().setValue("S");                                                                                                          //Natural: ASSIGN CNTL.TIRCNTL-COMPANY-CDE := 'S'
        ldaTwrl820e.getCntl_Tircntl_Input_Recs_Tot().setValue(pnd_S_Trans_Count.getValue(3));                                                                             //Natural: ASSIGN CNTL.TIRCNTL-INPUT-RECS-TOT := #S-TRANS-COUNT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Gross_Amt().setValue(pnd_S_Classic_Amt.getValue(3));                                                                            //Natural: ASSIGN CNTL.TIRCNTL-INPUT-GROSS-AMT := #S-CLASSIC-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Ivt_Amt().setValue(pnd_S_Roth_Amt.getValue(3));                                                                                 //Natural: ASSIGN CNTL.TIRCNTL-INPUT-IVT-AMT := #S-ROTH-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Int_Amt().setValue(pnd_S_Rollover_Amt.getValue(3));                                                                             //Natural: ASSIGN CNTL.TIRCNTL-INPUT-INT-AMT := #S-ROLLOVER-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Nra_Amt().setValue(pnd_S_Rechar_Amt.getValue(3));                                                                               //Natural: ASSIGN CNTL.TIRCNTL-INPUT-NRA-AMT := #S-RECHAR-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Can_Amt().setValue(pnd_S_Sep_Amt.getValue(3));                                                                                  //Natural: ASSIGN CNTL.TIRCNTL-INPUT-CAN-AMT := #S-SEP-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Fed_Wthldg_Amt().setValue(pnd_S_Fmv_Amt.getValue(3));                                                                           //Natural: ASSIGN CNTL.TIRCNTL-INPUT-FED-WTHLDG-AMT := #S-FMV-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_State_Wthldg_Amt().setValue(pnd_S_Roth_Conv_Amt.getValue(3));                                                                   //Natural: ASSIGN CNTL.TIRCNTL-INPUT-STATE-WTHLDG-AMT := #S-ROTH-CONV-AMT ( 3 )
        ldaTwrl820e.getCntl_Tircntl_Input_Local_Wthldg_Amt().setValue(0);                                                                                                 //Natural: ASSIGN CNTL.TIRCNTL-INPUT-LOCAL-WTHLDG-AMT := 0.00
        ldaTwrl820e.getCntl_Tircntl_Rpt_Source_Code_Desc().setValue(" ");                                                                                                 //Natural: ASSIGN CNTL.TIRCNTL-RPT-SOURCE-CODE-DESC := ' '
        ldaTwrl820e.getCntl_Tircntl_Rpt_Update_Dte_Time().setValue(pnd_Twrc_Sys_Dte_Time_Pnd_Twrc_Sys_Dte_Time_N);                                                        //Natural: ASSIGN CNTL.TIRCNTL-RPT-UPDATE-DTE-TIME := #TWRC-SYS-DTE-TIME-N
        ldaTwrl820e.getVw_cntl().insertDBRow();                                                                                                                           //Natural: STORE CNTL
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        pnd_Header_Date.reset();                                                                                                                                          //Natural: RESET #HEADER-DATE #INTERFACE-DATE
        pnd_Interface_Date.reset();
        pnd_Source_Total_Header.setValue("  Report Totals  ");                                                                                                            //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '  Report Totals  '
        i1.setValue(4);                                                                                                                                                   //Natural: ASSIGN I1 := 4
        i2.setValue(5);                                                                                                                                                   //Natural: ASSIGN I2 := 5
        i3.setValue(6);                                                                                                                                                   //Natural: ASSIGN I3 := 6
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
        sub_Display_Control_Totals();
        if (condition(Global.isEscape())) {return;}
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, new TabSetting(1),"IRA Transaction Records Read..................",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                         //Natural: WRITE ( 00 ) 01T 'IRA Transaction Records Read..................' #READ-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Duplicate IRA Transaction Records Bypassed....",pnd_Bypass_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));               //Natural: WRITE ( 00 ) / 01T 'Duplicate IRA Transaction Records Bypassed....' #BYPASS-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"IRA Contributions Added To The File...........",pnd_Store_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                //Natural: WRITE ( 00 ) / 01T 'IRA Contributions Added To The File...........' #STORE-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"IRA Contributions Adjusted Accepted...........",pnd_Adjust_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));               //Natural: WRITE ( 00 ) / 01T 'IRA Contributions Adjusted Accepted...........' #ADJUST-CTR
        if (Global.isEscape()) return;
        getReports().skip(2, 4);                                                                                                                                          //Natural: SKIP ( 02 ) 4
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"IRA Transaction Records Read..................",pnd_Read_Ctr, new ReportEditMask                    //Natural: WRITE ( 02 ) 01T 'IRA Transaction Records Read..................' #READ-CTR
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"Duplicate IRA Transaction Records Bypassed....",pnd_Bypass_Ctr, new ReportEditMask          //Natural: WRITE ( 02 ) / 01T 'Duplicate IRA Transaction Records Bypassed....' #BYPASS-CTR
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"IRA Contributions Added To The File...........",pnd_Store_Ctr, new ReportEditMask           //Natural: WRITE ( 02 ) / 01T 'IRA Contributions Added To The File...........' #STORE-CTR
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"IRA Contributions Adjusted Accepted...........",pnd_Adjust_Ctr, new ReportEditMask          //Natural: WRITE ( 02 ) / 01T 'IRA Contributions Adjusted Accepted...........' #ADJUST-CTR
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(3, 4);                                                                                                                                          //Natural: SKIP ( 03 ) 4
        getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),"IRA Transaction Records Read..................",pnd_Read_Ctr, new ReportEditMask                    //Natural: WRITE ( 03 ) 01T 'IRA Transaction Records Read..................' #READ-CTR
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"Duplicate IRA Transaction Records Bypassed....",pnd_Bypass_Ctr, new ReportEditMask          //Natural: WRITE ( 03 ) / 01T 'Duplicate IRA Transaction Records Bypassed....' #BYPASS-CTR
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"IRA Contributions Added To The File...........",pnd_Store_Ctr, new ReportEditMask           //Natural: WRITE ( 03 ) / 01T 'IRA Contributions Added To The File...........' #STORE-CTR
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"IRA Contributions Adjusted Accepted...........",pnd_Adjust_Ctr, new ReportEditMask          //Natural: WRITE ( 03 ) / 01T 'IRA Contributions Adjusted Accepted...........' #ADJUST-CTR
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Report_Adjusted_Accepted_Transaction() throws Exception                                                                                              //Natural: REPORT-ADJUSTED-ACCEPTED-TRANSACTION
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------------------------
        getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,ldaTwrl462a.getIra_Record_Ira_Company_Code(),ldaTwrl462a.getIra_Record_Ira_Tax_Id(),ldaTwrl462a.getIra_Record_Ira_Contract(),ldaTwrl462a.getIra_Record_Ira_Payee(),ldaTwrl462a.getIra_Record_Ira_Citizenship_Code(),ldaTwrl462a.getIra_Record_Ira_Residency_Code(),ldaTwrl462a.getIra_Record_Ira_Residency_Type(),ldaTwrl462a.getIra_Record_Ira_Pin(),ldaTwrl462a.getIra_Record_Ira_Form_Type(),ldaTwrl462a.getIra_Record_Ira_Form_Period(),ldaTwrl462a.getIra_Record_Ira_Classic_Amt(),  //Natural: WRITE ( 03 ) NOTITLE NOHDR IRA-COMPANY-CODE IRA-TAX-ID IRA-CONTRACT IRA-PAYEE IRA-CITIZENSHIP-CODE IRA-RESIDENCY-CODE IRA-RESIDENCY-TYPE IRA-PIN IRA-FORM-TYPE IRA-FORM-PERIOD IRA-CLASSIC-AMT ( EM = Z,ZZ9.99- ) IRA-ROTH-AMT ( EM = Z,ZZ9.99- ) IRA-SEP-AMT ( EM = ZZ,ZZ9.99- ) IRA-ROLLOVER-AMT ( EM = Z,ZZZ,ZZ9.99- ) IRA-RECHARACTER-AMT ( EM = Z,ZZZ,ZZ9.99- ) IRA-ROTH-CONV-AMT ( EM = Z,ZZZ,ZZ9.99 ) IRA-FMV-AMT ( EM = Z,ZZZ,ZZ9.99 )
            new ReportEditMask ("Z,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Roth_Amt(), new ReportEditMask ("Z,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Sep_Amt(), 
            new ReportEditMask ("ZZ,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Rollover_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),ldaTwrl462a.getIra_Record_Ira_Fmv_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Adjust_Ctr.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #ADJUST-CTR
    }
    private void sub_Report_Bypassed_Existing_Payments() throws Exception                                                                                                 //Natural: REPORT-BYPASSED-EXISTING-PAYMENTS
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,ldaTwrl462a.getIra_Record_Ira_Company_Code(),ldaTwrl462a.getIra_Record_Ira_Tax_Id(),ldaTwrl462a.getIra_Record_Ira_Contract(),ldaTwrl462a.getIra_Record_Ira_Payee(),ldaTwrl462a.getIra_Record_Ira_Citizenship_Code(),ldaTwrl462a.getIra_Record_Ira_Residency_Code(),ldaTwrl462a.getIra_Record_Ira_Residency_Type(),ldaTwrl462a.getIra_Record_Ira_Pin(),ldaTwrl462a.getIra_Record_Ira_Form_Type(),ldaTwrl462a.getIra_Record_Ira_Form_Period(),ldaTwrl462a.getIra_Record_Ira_Classic_Amt(),  //Natural: WRITE ( 01 ) NOTITLE NOHDR IRA-COMPANY-CODE IRA-TAX-ID IRA-CONTRACT IRA-PAYEE IRA-CITIZENSHIP-CODE IRA-RESIDENCY-CODE IRA-RESIDENCY-TYPE IRA-PIN IRA-FORM-TYPE IRA-FORM-PERIOD IRA-CLASSIC-AMT ( EM = Z,ZZ9.99- ) IRA-ROTH-AMT ( EM = Z,ZZ9.99- ) IRA-SEP-AMT ( EM = ZZ,ZZ9.99- ) IRA-ROLLOVER-AMT ( EM = Z,ZZZ,ZZ9.99- ) IRA-RECHARACTER-AMT ( EM = Z,ZZZ,ZZ9.99- ) IRA-ROTH-CONV-AMT ( EM = Z,ZZZ,ZZ9.99 ) IRA-FMV-AMT ( EM = Z,ZZZ,ZZ9.99 )
            new ReportEditMask ("Z,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Roth_Amt(), new ReportEditMask ("Z,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Sep_Amt(), 
            new ReportEditMask ("ZZ,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Rollover_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),ldaTwrl462a.getIra_Record_Ira_Fmv_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
    }
    private void sub_House_Keeping() throws Exception                                                                                                                     //Natural: HOUSE-KEEPING
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Timn_N.setValue(Global.getTIMN());                                                                                                                            //Natural: ASSIGN #TIMN-N := *TIMN
        pnd_Twrc_Sys_Dte_Time_Pnd_Twrc_Sys_Dte.setValue(Global.getDATN());                                                                                                //Natural: ASSIGN #TWRC-SYS-DTE := *DATN
        pnd_Twrc_Sys_Dte_Time_Pnd_Twrc_Sys_Time.setValue(pnd_Timn_N_Pnd_Timn_A);                                                                                          //Natural: ASSIGN #TWRC-SYS-TIME := #TIMN-A
        pnd_Twrc_Create_Date.setValue(Global.getDATN());                                                                                                                  //Natural: ASSIGN #TWRC-CREATE-DATE := *DATN
        pnd_Twrc_Create_Date_Inv.compute(new ComputeParameters(false, pnd_Twrc_Create_Date_Inv), DbsField.subtract(100000000,Global.getDATN()));                          //Natural: ASSIGN #TWRC-CREATE-DATE-INV := 100000000 - *DATN
        pnd_Twrc_Create_Time.setValue(pnd_Timn_N_Pnd_Timn_A);                                                                                                             //Natural: ASSIGN #TWRC-CREATE-TIME := #TIMN-A
        pnd_Twrc_Create_Time_Inv.compute(new ComputeParameters(false, pnd_Twrc_Create_Time_Inv), DbsField.subtract(10000000,Global.getTIMN()));                           //Natural: ASSIGN #TWRC-CREATE-TIME-INV := 10000000 - *TIMN
        pnd_Twrc_Lu_Ts.setValue(Global.getTIMX());                                                                                                                        //Natural: ASSIGN #TWRC-LU-TS := *TIMX
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new               //Natural: WRITE ( 01 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 45T 'Bypassed IRA Contributions Already On File' 120T 'REPORT: RPT1' / 01T 'Tax Year :' IRA-TAX-YEAR 20T 'Simulation :' IRA-SIMULATION-DATE ( EM = XXXX'/'XX'/'XX ) 47T 'Interface :' *DATN ( EM = 9999/99/99 ) // 01T 'C' 03T '  Tax ID  ' 14T 'CONTRACT' 23T 'PY' 26T 'CZ' 29T 'RE' 32T 'RT' 35T ' PIN ' 46T 'IRA' 50T 'Per' 55T 'Classic' 68T 'Roth' 80T 'SEP' 89T 'Rollover' 100T 'Recharacter' 115T 'Roth Conv.' 132T 'F.M.V.'
                        TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask 
                        ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(45),"Bypassed IRA Contributions Already On File",new 
                        TabSetting(120),"REPORT: RPT1",NEWLINE,new TabSetting(1),"Tax Year :",ldaTwrl462a.getIra_Record_Ira_Tax_Year(),new TabSetting(20),"Simulation :",ldaTwrl462a.getIra_Record_Ira_Simulation_Date(), 
                        new ReportEditMask ("XXXX'/'XX'/'XX"),new TabSetting(47),"Interface :",Global.getDATN(), new ReportEditMask ("9999/99/99"),NEWLINE,NEWLINE,new 
                        TabSetting(1),"C",new TabSetting(3),"  Tax ID  ",new TabSetting(14),"CONTRACT",new TabSetting(23),"PY",new TabSetting(26),"CZ",new 
                        TabSetting(29),"RE",new TabSetting(32),"RT",new TabSetting(35)," PIN ",new TabSetting(46),"IRA",new TabSetting(50),"Per",new TabSetting(55),"Classic",new 
                        TabSetting(68),"Roth",new TabSetting(80),"SEP",new TabSetting(89),"Rollover",new TabSetting(100),"Recharacter",new TabSetting(115),"Roth Conv.",new 
                        TabSetting(132),"F.M.V.");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(2, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 02 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(2, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(54),"Tax Update Control Report",new           //Natural: WRITE ( 02 ) NOTITLE *INIT-USER '-' *PROGRAM 54T 'Tax Update Control Report' 120T 'REPORT: RPT2'
                        TabSetting(120),"REPORT: RPT2");
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(25),pnd_Header_Date, new ReportEditMask ("MM/DD/YYYY"),new TabSetting(58),pnd_Source_Total_Header,new  //Natural: WRITE ( 02 ) NOTITLE 25T #HEADER-DATE ( EM = MM/DD/YYYY ) 58T #SOURCE-TOTAL-HEADER 97T #INTERFACE-DATE ( EM = MM/DD/YYYY )
                        TabSetting(97),pnd_Interface_Date, new ReportEditMask ("MM/DD/YYYY"));
                    getReports().skip(2, 2);                                                                                                                              //Natural: SKIP ( 02 ) 2 LINES
                    //*  WRITE (02) NOTITLE
                    //*    22T '                    '
                    //*    43T '                    '
                    //*    64T '                    '
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(22),"       Input        ",new TabSetting(43),"      Rejected      ",new                    //Natural: WRITE ( 02 ) NOTITLE 22T '       Input        ' 43T '      Rejected      ' 64T '      Accepted      '
                        TabSetting(64),"      Accepted      ");
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"                    ",new TabSetting(22),"====================",new                     //Natural: WRITE ( 02 ) NOTITLE 01T '                    ' 22T '====================' 43T '====================' 64T '===================='
                        TabSetting(43),"====================",new TabSetting(64),"====================");
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 02 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new               //Natural: WRITE ( 03 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 45T '    Tax Withholding & Reporting System    ' 120T 'Page:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 45T '  Adjusted & Accepted IRA Contributions   ' 120T 'REPORT: RPT3' / 01T 'Tax Year :' IRA-TAX-YEAR 20T 'Simulation :' #HEADER-DATE ( EM = MM/DD/YYYY ) 47T 'Interface :' #INTERFACE-DATE ( EM = MM/DD/YYYY ) // 01T 'C' 03T '  Tax ID  ' 14T 'CONTRACT' 23T 'PY' 26T 'CZ' 29T 'RE' 32T 'RT' 35T ' PIN ' 46T 'IRA' 50T 'Per' 55T 'Classic' 68T 'Roth' 80T 'SEP' 89T 'Rollover' 100T 'Recharacter' 115T 'Roth Conv.' 132T 'F.M.V.'
                        TabSetting(45),"    Tax Withholding & Reporting System    ",new TabSetting(120),"Page:",getReports().getPageNumberDbs(3), new ReportEditMask 
                        ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(45),"  Adjusted & Accepted IRA Contributions   ",new 
                        TabSetting(120),"REPORT: RPT3",NEWLINE,new TabSetting(1),"Tax Year :",ldaTwrl462a.getIra_Record_Ira_Tax_Year(),new TabSetting(20),"Simulation :",pnd_Header_Date, 
                        new ReportEditMask ("MM/DD/YYYY"),new TabSetting(47),"Interface :",pnd_Interface_Date, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,new 
                        TabSetting(1),"C",new TabSetting(3),"  Tax ID  ",new TabSetting(14),"CONTRACT",new TabSetting(23),"PY",new TabSetting(26),"CZ",new 
                        TabSetting(29),"RE",new TabSetting(32),"RT",new TabSetting(35)," PIN ",new TabSetting(46),"IRA",new TabSetting(50),"Per",new TabSetting(55),"Classic",new 
                        TabSetting(68),"Roth",new TabSetting(80),"SEP",new TabSetting(89),"Rollover",new TabSetting(100),"Recharacter",new TabSetting(115),"Roth Conv.",new 
                        TabSetting(132),"F.M.V.");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
        pnd_Source_Total_Header.setValue(DbsUtil.compress("Source System:", pnd_Prev_Form));                                                                              //Natural: COMPRESS 'Source System:' #PREV-FORM INTO #SOURCE-TOTAL-HEADER
        i1.setValue(1);                                                                                                                                                   //Natural: ASSIGN I1 := 1
        i2.setValue(2);                                                                                                                                                   //Natural: ASSIGN I2 := 2
        i3.setValue(3);                                                                                                                                                   //Natural: ASSIGN I3 := 3
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
        sub_Display_Control_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-GENERAL-TOTALS
        sub_Update_General_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133 ZP=ON");
        Global.format(1, "PS=60 LS=138 ZP=ON");
        Global.format(2, "PS=60 LS=133 ZP=ON");
        Global.format(3, "PS=60 LS=138 ZP=ON");
        Global.format(4, "PS=60 LS=133 ZP=ON");
        Global.format(5, "PS=60 LS=133 ZP=ON");
        Global.format(6, "PS=60 LS=133 ZP=ON");
    }
}
