/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:35:43 PM
**        * FROM NATURAL PROGRAM : Twrp3528
************************************************************
**        * FILE NAME            : Twrp3528.java
**        * CLASS NAME           : Twrp3528
**        * INSTANCE NAME        : Twrp3528
************************************************************
************************************************************************
** PROGRAM : TWRP3528
** SYSTEM  : TAXWARS
** AUTHOR  : TED PROIMOS
** FUNCTION: PRINT 1099 FORMS FOR STATES (STATE CORRECTION REPORTING)
** HISTORY.....: 10/22/2002 - MARINA NACHBER
**               ADDED THIS HEADER TO THE PROGRAM
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3528 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Process_Type;
    private DbsField pnd_Ws_State_Name;
    private DbsField pnd_Ws_Form_Page_Cnt;
    private DbsField pnd_Ws_Ws_Isn;
    private DbsField pnd_Ws_Pnd_Test_Timx;

    private DbsGroup pnd_Input_Parms;
    private DbsField pnd_Input_Parms_Pnd_Printer;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Process_Type = pnd_Ws.newFieldInGroup("pnd_Ws_Process_Type", "PROCESS-TYPE", FieldType.STRING, 1);
        pnd_Ws_State_Name = pnd_Ws.newFieldInGroup("pnd_Ws_State_Name", "STATE-NAME", FieldType.STRING, 19);
        pnd_Ws_Form_Page_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Form_Page_Cnt", "FORM-PAGE-CNT", FieldType.PACKED_DECIMAL, 2);
        pnd_Ws_Ws_Isn = pnd_Ws.newFieldInGroup("pnd_Ws_Ws_Isn", "WS-ISN", FieldType.NUMERIC, 8);
        pnd_Ws_Pnd_Test_Timx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Test_Timx", "#TEST-TIMX", FieldType.TIME);

        pnd_Input_Parms = localVariables.newGroupInRecord("pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Input_Parms_Pnd_Printer = pnd_Input_Parms.newFieldInGroup("pnd_Input_Parms_Pnd_Printer", "#PRINTER", FieldType.STRING, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3528() throws Exception
    {
        super("Twrp3528");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp3528|Main");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parms);                                                                                      //Natural: INPUT #INPUT-PARMS
                READWORK01:                                                                                                                                               //Natural: READ WORK 01 STATE-NAME FORM-PAGE-CNT WS-ISN
                while (condition(getWorkFiles().read(1, pnd_Ws_State_Name, pnd_Ws_Form_Page_Cnt, pnd_Ws_Ws_Isn)))
                {
                    pnd_Ws_Process_Type.setValue("B");                                                                                                                    //Natural: ASSIGN PROCESS-TYPE := 'B'
                    REPEAT01:                                                                                                                                             //Natural: REPEAT
                    while (condition(whileTrue))
                    {
                        if (condition(pnd_Ws_Pnd_Test_Timx.notEquals(Global.getTIMX()))) {break;}                                                                         //Natural: UNTIL #WS.#TEST-TIMX NE *TIMX
                        ignore();
                    }                                                                                                                                                     //Natural: END-REPEAT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Pnd_Test_Timx.setValue(Global.getTIMX());                                                                                                      //Natural: ASSIGN #WS.#TEST-TIMX := *TIMX
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Input_Parms_Pnd_Printer, pnd_Ws_Process_Type, pnd_Ws_State_Name, pnd_Ws_Form_Page_Cnt,                //Natural: FETCH RETURN 'TWRP0115' #PRINTER PROCESS-TYPE STATE-NAME FORM-PAGE-CNT WS-ISN
                        pnd_Ws_Ws_Isn);
                    DbsUtil.invokeMain(DbsUtil.getBlType("TWRP0115"), getCurrentProcessState());
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //
}
