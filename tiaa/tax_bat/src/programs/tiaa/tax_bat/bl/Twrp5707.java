/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:41:45 PM
**        * FROM NATURAL PROGRAM : Twrp5707
************************************************************
**        * FILE NAME            : Twrp5707.java
**        * CLASS NAME           : Twrp5707
**        * INSTANCE NAME        : Twrp5707
************************************************************
************************************************************************
* PROGRAM : TWRP5707
* SYSTEM  : TAXWARS
* FUNCTION: READS PAYMENTS EXTRACT CREATED BY TWRP0900 AND PRODUCES
*           A W2 PAYMENTS SUBSET FOR USE IN W-2 CORRECTIONS REPORTING.
*           NOTE: EXTRACT SUBSET IS CREATED WITH ONE RECORD PER
*           PAYMENT DETAIL.
* AUTHOR  : J.ROTHOLZ
* HISTORY : 06/02/10 PROGRAM CLONED FROM TWRP5706
* 06/17/11 RITA RE-POSITION EXTRACT OF N&A AND RESIDENCY FOR EFFICIENCY
*
* 10/19/11 M.BERLIN  - CHANGED REFERENCES FROM FIELD
*                      TWRPYMNT-COMPANY-CDE TO FIELD
*                      TWRPYMNT-COMPANY-CDE-FORM     /* MB
* 02/18/15: OS - RECOMPILED FOR UPDATED TWRL0900
* 06/08/15: FENDAYA COR AND NAS SUNSET. FE201506
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5707 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private LdaTwrl0600 ldaTwrl0600;
    private LdaTwrl0900 ldaTwrl0900;
    private PdaTwratbl2 pdaTwratbl2;
    private PdaMdma101 pdaMdma101;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Debug;

    private DbsGroup pnd_W2_Record;
    private DbsField pnd_W2_Record_Pnd_Tax_Year;

    private DbsGroup pnd_W2_Record__R_Field_1;
    private DbsField pnd_W2_Record_Pnd_Tax_Year_N;
    private DbsField pnd_W2_Record_Pnd_Tax_Id_Type;
    private DbsField pnd_W2_Record_Pnd_Tax_Id_Nbr;
    private DbsField pnd_W2_Record_Pnd_Pin_Nbr;
    private DbsField pnd_W2_Record_Pnd_Company_Code;
    private DbsField pnd_W2_Record_Pnd_Contract_Nbr;
    private DbsField pnd_W2_Record_Pnd_Payee_Cde;
    private DbsField pnd_W2_Record_Pnd_Residency_Type;
    private DbsField pnd_W2_Record_Pnd_Residency_Code;
    private DbsField pnd_W2_Record_Pnd_Gross_Amount;
    private DbsField pnd_W2_Record_Pnd_Federal_Tax;
    private DbsField pnd_W2_Record_Pnd_State_Tax;
    private DbsField pnd_W2_Record_Pnd_Name_And_Address;

    private DbsGroup pnd_W2_Record__R_Field_2;
    private DbsField pnd_W2_Record_Pnd_Name;
    private DbsField pnd_W2_Record_Pnd_Address_1;
    private DbsField pnd_W2_Record_Pnd_Address_2;
    private DbsField pnd_W2_Record_Pnd_Address_3;
    private DbsField pnd_W2_Record_Pnd_Address_4;
    private DbsField pnd_W2_Record_Pnd_Address_5;
    private DbsField pnd_W2_Record_Pnd_Employer_State_Id;

    private DbsGroup pnd_W2_Record__R_Field_3;
    private DbsField pnd_W2_Record_Pnd_State_Code;
    private DbsField pnd_W2_Record_Pnd_State_Id;
    private DbsField pnd_W2_Record_Pnd_Transaction_Dt;
    private DbsField pnd_W2_Record_Pnd_W2_Op_Ia_Ind;
    private DbsField pnd_W2_Record_Pnd_Record_Type;
    private DbsField pnd_W2_Record_Pnd_Sys_Dte_Time;

    private DbsGroup pnd_Hold_Data;
    private DbsField pnd_Hold_Data_Pnd_H_Name;
    private DbsField pnd_Hold_Data_Pnd_H_Address_1;
    private DbsField pnd_Hold_Data_Pnd_H_Address_2;
    private DbsField pnd_Hold_Data_Pnd_H_Address_3;
    private DbsField pnd_Hold_Data_Pnd_H_Address_4;
    private DbsField pnd_Hold_Data_Pnd_H_Address_5;
    private DbsField pnd_Hold_Data_Pnd_H_State_Code;
    private DbsField pnd_Hold_Data_Pnd_H_State_Id;
    private DbsField pnd_I;
    private DbsField pnd_Temp_State_Id;
    private DbsField pnd_Temp_Date;

    private DbsGroup pnd_Temp_Date__R_Field_4;
    private DbsField pnd_Temp_Date_Pnd_Yyyy;
    private DbsField pnd_Temp_Date_Pnd_Mm;
    private DbsField pnd_Temp_Date_Pnd_Dd;
    private DbsField pnd_Record_Read;
    private DbsField pnd_Records_Sel;
    private DbsField pnd_Tot_Payments;
    private DbsField pnd_W2_Written;
    private DbsField pnd_New_Addrs;
    private DbsField pnd_Sv_Tin;
    private DbsField pnd_Sv_Cntrct;
    private DbsField pnd_Sv_Payee;
    private DbsField pnd_Sv_Resdnce;
    private DbsField pnd_Rc;
    private DbsField pnd_I2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);
        ldaTwrl0900 = new LdaTwrl0900();
        registerRecord(ldaTwrl0900);
        localVariables = new DbsRecord();
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        pdaMdma101 = new PdaMdma101(localVariables);

        // Local Variables
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);

        pnd_W2_Record = localVariables.newGroupInRecord("pnd_W2_Record", "#W2-RECORD");
        pnd_W2_Record_Pnd_Tax_Year = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);

        pnd_W2_Record__R_Field_1 = pnd_W2_Record.newGroupInGroup("pnd_W2_Record__R_Field_1", "REDEFINE", pnd_W2_Record_Pnd_Tax_Year);
        pnd_W2_Record_Pnd_Tax_Year_N = pnd_W2_Record__R_Field_1.newFieldInGroup("pnd_W2_Record_Pnd_Tax_Year_N", "#TAX-YEAR-N", FieldType.NUMERIC, 4);
        pnd_W2_Record_Pnd_Tax_Id_Type = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Tax_Id_Type", "#TAX-ID-TYPE", FieldType.STRING, 1);
        pnd_W2_Record_Pnd_Tax_Id_Nbr = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.STRING, 10);
        pnd_W2_Record_Pnd_Pin_Nbr = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.STRING, 12);
        pnd_W2_Record_Pnd_Company_Code = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Company_Code", "#COMPANY-CODE", FieldType.STRING, 1);
        pnd_W2_Record_Pnd_Contract_Nbr = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Contract_Nbr", "#CONTRACT-NBR", FieldType.STRING, 8);
        pnd_W2_Record_Pnd_Payee_Cde = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.STRING, 2);
        pnd_W2_Record_Pnd_Residency_Type = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Residency_Type", "#RESIDENCY-TYPE", FieldType.STRING, 1);
        pnd_W2_Record_Pnd_Residency_Code = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Residency_Code", "#RESIDENCY-CODE", FieldType.STRING, 2);
        pnd_W2_Record_Pnd_Gross_Amount = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Gross_Amount", "#GROSS-AMOUNT", FieldType.PACKED_DECIMAL, 11, 
            2);
        pnd_W2_Record_Pnd_Federal_Tax = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Federal_Tax", "#FEDERAL-TAX", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W2_Record_Pnd_State_Tax = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_State_Tax", "#STATE-TAX", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W2_Record_Pnd_Name_And_Address = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Name_And_Address", "#NAME-AND-ADDRESS", FieldType.STRING, 
            210);

        pnd_W2_Record__R_Field_2 = pnd_W2_Record.newGroupInGroup("pnd_W2_Record__R_Field_2", "REDEFINE", pnd_W2_Record_Pnd_Name_And_Address);
        pnd_W2_Record_Pnd_Name = pnd_W2_Record__R_Field_2.newFieldInGroup("pnd_W2_Record_Pnd_Name", "#NAME", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Address_1 = pnd_W2_Record__R_Field_2.newFieldInGroup("pnd_W2_Record_Pnd_Address_1", "#ADDRESS-1", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Address_2 = pnd_W2_Record__R_Field_2.newFieldInGroup("pnd_W2_Record_Pnd_Address_2", "#ADDRESS-2", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Address_3 = pnd_W2_Record__R_Field_2.newFieldInGroup("pnd_W2_Record_Pnd_Address_3", "#ADDRESS-3", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Address_4 = pnd_W2_Record__R_Field_2.newFieldInGroup("pnd_W2_Record_Pnd_Address_4", "#ADDRESS-4", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Address_5 = pnd_W2_Record__R_Field_2.newFieldInGroup("pnd_W2_Record_Pnd_Address_5", "#ADDRESS-5", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Employer_State_Id = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Employer_State_Id", "#EMPLOYER-STATE-ID", FieldType.STRING, 
            16);

        pnd_W2_Record__R_Field_3 = pnd_W2_Record.newGroupInGroup("pnd_W2_Record__R_Field_3", "REDEFINE", pnd_W2_Record_Pnd_Employer_State_Id);
        pnd_W2_Record_Pnd_State_Code = pnd_W2_Record__R_Field_3.newFieldInGroup("pnd_W2_Record_Pnd_State_Code", "#STATE-CODE", FieldType.STRING, 2);
        pnd_W2_Record_Pnd_State_Id = pnd_W2_Record__R_Field_3.newFieldInGroup("pnd_W2_Record_Pnd_State_Id", "#STATE-ID", FieldType.STRING, 14);
        pnd_W2_Record_Pnd_Transaction_Dt = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Transaction_Dt", "#TRANSACTION-DT", FieldType.STRING, 8);
        pnd_W2_Record_Pnd_W2_Op_Ia_Ind = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_W2_Op_Ia_Ind", "#W2-OP-IA-IND", FieldType.STRING, 1);
        pnd_W2_Record_Pnd_Record_Type = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Record_Type", "#RECORD-TYPE", FieldType.NUMERIC, 1);
        pnd_W2_Record_Pnd_Sys_Dte_Time = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Sys_Dte_Time", "#SYS-DTE-TIME", FieldType.TIME);

        pnd_Hold_Data = localVariables.newGroupInRecord("pnd_Hold_Data", "#HOLD-DATA");
        pnd_Hold_Data_Pnd_H_Name = pnd_Hold_Data.newFieldInGroup("pnd_Hold_Data_Pnd_H_Name", "#H-NAME", FieldType.STRING, 35);
        pnd_Hold_Data_Pnd_H_Address_1 = pnd_Hold_Data.newFieldInGroup("pnd_Hold_Data_Pnd_H_Address_1", "#H-ADDRESS-1", FieldType.STRING, 35);
        pnd_Hold_Data_Pnd_H_Address_2 = pnd_Hold_Data.newFieldInGroup("pnd_Hold_Data_Pnd_H_Address_2", "#H-ADDRESS-2", FieldType.STRING, 35);
        pnd_Hold_Data_Pnd_H_Address_3 = pnd_Hold_Data.newFieldInGroup("pnd_Hold_Data_Pnd_H_Address_3", "#H-ADDRESS-3", FieldType.STRING, 35);
        pnd_Hold_Data_Pnd_H_Address_4 = pnd_Hold_Data.newFieldInGroup("pnd_Hold_Data_Pnd_H_Address_4", "#H-ADDRESS-4", FieldType.STRING, 35);
        pnd_Hold_Data_Pnd_H_Address_5 = pnd_Hold_Data.newFieldInGroup("pnd_Hold_Data_Pnd_H_Address_5", "#H-ADDRESS-5", FieldType.STRING, 35);
        pnd_Hold_Data_Pnd_H_State_Code = pnd_Hold_Data.newFieldInGroup("pnd_Hold_Data_Pnd_H_State_Code", "#H-STATE-CODE", FieldType.STRING, 2);
        pnd_Hold_Data_Pnd_H_State_Id = pnd_Hold_Data.newFieldInGroup("pnd_Hold_Data_Pnd_H_State_Id", "#H-STATE-ID", FieldType.STRING, 14);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Temp_State_Id = localVariables.newFieldInRecord("pnd_Temp_State_Id", "#TEMP-STATE-ID", FieldType.STRING, 14);
        pnd_Temp_Date = localVariables.newFieldInRecord("pnd_Temp_Date", "#TEMP-DATE", FieldType.STRING, 8);

        pnd_Temp_Date__R_Field_4 = localVariables.newGroupInRecord("pnd_Temp_Date__R_Field_4", "REDEFINE", pnd_Temp_Date);
        pnd_Temp_Date_Pnd_Yyyy = pnd_Temp_Date__R_Field_4.newFieldInGroup("pnd_Temp_Date_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 4);
        pnd_Temp_Date_Pnd_Mm = pnd_Temp_Date__R_Field_4.newFieldInGroup("pnd_Temp_Date_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Temp_Date_Pnd_Dd = pnd_Temp_Date__R_Field_4.newFieldInGroup("pnd_Temp_Date_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);
        pnd_Record_Read = localVariables.newFieldInRecord("pnd_Record_Read", "#RECORD-READ", FieldType.PACKED_DECIMAL, 8);
        pnd_Records_Sel = localVariables.newFieldInRecord("pnd_Records_Sel", "#RECORDS-SEL", FieldType.PACKED_DECIMAL, 8);
        pnd_Tot_Payments = localVariables.newFieldInRecord("pnd_Tot_Payments", "#TOT-PAYMENTS", FieldType.PACKED_DECIMAL, 8);
        pnd_W2_Written = localVariables.newFieldInRecord("pnd_W2_Written", "#W2-WRITTEN", FieldType.PACKED_DECIMAL, 8);
        pnd_New_Addrs = localVariables.newFieldInRecord("pnd_New_Addrs", "#NEW-ADDRS", FieldType.BOOLEAN, 1);
        pnd_Sv_Tin = localVariables.newFieldInRecord("pnd_Sv_Tin", "#SV-TIN", FieldType.STRING, 10);
        pnd_Sv_Cntrct = localVariables.newFieldInRecord("pnd_Sv_Cntrct", "#SV-CNTRCT", FieldType.STRING, 8);
        pnd_Sv_Payee = localVariables.newFieldInRecord("pnd_Sv_Payee", "#SV-PAYEE", FieldType.STRING, 2);
        pnd_Sv_Resdnce = localVariables.newFieldInRecord("pnd_Sv_Resdnce", "#SV-RESDNCE", FieldType.STRING, 2);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0600.initializeValues();
        ldaTwrl0900.initializeValues();

        localVariables.reset();
        pnd_Debug.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5707() throws Exception
    {
        super("Twrp5707");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP5707", onError);
        //*  FE201506
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
        sub_Open_Mq();
        if (condition(Global.isEscape())) {return;}
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-CONTROL-RECORD
        sub_Process_Control_Record();
        if (condition(Global.isEscape())) {return;}
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 2 RECORD #XTAXYR-F94
        while (condition(getWorkFiles().read(2, ldaTwrl0900.getPnd_Xtaxyr_F94())))
        {
            pnd_Record_Read.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #RECORD-READ
            if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().notEquals("7")))                                                                      //Natural: IF TWRPYMNT-DISTRIBUTION-CDE NE '7'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Records_Sel.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #RECORDS-SEL
            pnd_W2_Record_Pnd_Tax_Year_N.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());                                                                     //Natural: ASSIGN #TAX-YEAR-N := #XTAXYR-F94.TWRPYMNT-TAX-YEAR
            //*  RS START
            if (condition(pnd_Sv_Tin.notEquals(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr()) || pnd_Sv_Cntrct.notEquals(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr())  //Natural: IF #SV-TIN NE #XTAXYR-F94.TWRPYMNT-TAX-ID-NBR OR #SV-CNTRCT NE #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR OR #SV-PAYEE NE #XTAXYR-F94.TWRPYMNT-PAYEE-CDE
                || pnd_Sv_Payee.notEquals(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payee_Cde())))
            {
                pnd_New_Addrs.setValue(true);                                                                                                                             //Natural: ASSIGN #NEW-ADDRS := TRUE
                pnd_Sv_Tin.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr());                                                                                 //Natural: ASSIGN #SV-TIN := #XTAXYR-F94.TWRPYMNT-TAX-ID-NBR
                pnd_Sv_Cntrct.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr());                                                                            //Natural: ASSIGN #SV-CNTRCT := #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR
                pnd_Sv_Payee.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payee_Cde());                                                                                //Natural: ASSIGN #SV-PAYEE := #XTAXYR-F94.TWRPYMNT-PAYEE-CDE
                //*  RS END
                //*   ALL PAYMENTS
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO #C-TWRPYMNT-PAYMENTS
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments())); pnd_I.nadd(1))
            {
                if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payment_Type().getValue(pnd_I).equals("M") && (((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_I).equals("S")  //Natural: IF ( TWRPYMNT-PAYMENT-TYPE ( #I ) = 'M' AND ( TWRPYMNT-SETTLE-TYPE ( #I ) = 'S' OR = 'T' OR = 'E' OR = 'M' ) )
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_I).equals("T")) || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_I).equals("E")) 
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_I).equals("M")))))
                {
                    //*  RS START
                    if (condition(pnd_New_Addrs.getBoolean()))                                                                                                            //Natural: IF #NEW-ADDRS
                    {
                                                                                                                                                                          //Natural: PERFORM GET-NAME-ADDRESS
                        sub_Get_Name_Address();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_New_Addrs.setValue(false);                                                                                                                    //Natural: ASSIGN #NEW-ADDRS := FALSE
                    }                                                                                                                                                     //Natural: END-IF
                    //*  RS END
                    if (condition(pnd_Sv_Resdnce.notEquals(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code())))                                                     //Natural: IF #SV-RESDNCE NE #XTAXYR-F94.TWRPYMNT-RESIDENCY-CODE
                    {
                        pnd_Sv_Resdnce.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code());                                                                 //Natural: ASSIGN #SV-RESDNCE := #XTAXYR-F94.TWRPYMNT-RESIDENCY-CODE
                                                                                                                                                                          //Natural: PERFORM GET-STATE-INFO
                        sub_Get_State_Info();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM SETUP-W2-RECORD
                    sub_Setup_W2_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getWorkFiles().write(3, false, pnd_W2_Record);                                                                                                        //Natural: WRITE WORK FILE 3 #W2-RECORD
                    pnd_W2_Written.nadd(1);                                                                                                                               //Natural: ADD 1 TO #W2-WRITTEN
                    pnd_W2_Record.reset();                                                                                                                                //Natural: RESET #W2-RECORD
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, "Total records read from Payment file =",pnd_Record_Read);                                                                                  //Natural: WRITE 'Total records read from Payment file =' #RECORD-READ
        if (Global.isEscape()) return;
        getReports().write(0, "Total W2 Payment records written     =",pnd_W2_Written);                                                                                   //Natural: WRITE 'Total W2 Payment records written     =' #W2-WRITTEN
        if (Global.isEscape()) return;
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NAME-ADDRESS
        //*      SUBSTR(#MDMA101.#O-MIDDLE-NAME,1,1)
        //*      SUBSTR(#MDMA100.#O-MIDDLE-NAME,1,1)
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-W2-RECORD
        //* **************************************
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CONTROL-RECORD
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATE-INFO
        //* ********************************
        //* ********
        //* ********                                                                                                                                                      //Natural: ON ERROR
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-ROUTINE
        //* ***************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-MQ
        //*  FE201506 END
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
    }
    private void sub_Get_Name_Address() throws Exception                                                                                                                  //Natural: GET-NAME-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        //*  #TWRAPART.#FUNCTION    := 'R' /* FE201506 START
        //*  #TWRAPART.#DISPLAY-IND := TRUE
        //*  #TWRAPART.#TAX-YEAR    := *DATN / 10000
        //*  #TWRAPART.#TIN         :=  TWRPYMNT-TAX-ID-NBR
        //*  PIN-EXPANSION-START
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        pdaMdma101.getPnd_Mdma101_Pnd_I_Ssn_A9().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr());                                                           //Natural: ASSIGN #MDMA101.#I-SSN-A9 := TWRPYMNT-TAX-ID-NBR
        //*  FE201506
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' USING #MDMA101
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA101.#O-RETURN-CODE EQ '0000'
        {
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix().equals("MR") || pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix().equals("MR.") || pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix().equals("MRS")  //Natural: IF #MDMA101.#O-PREFIX = 'MR' OR = 'MR.' OR = 'MRS' OR = 'MRS.' OR = 'MS' OR = 'MS.'
                || pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix().equals("MRS.") || pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix().equals("MS") || pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix().equals("MS.")))
            {
                pnd_Hold_Data_Pnd_H_Name.setValue(DbsUtil.compress(pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name(),           //Natural: COMPRESS #MDMA101.#O-FIRST-NAME #MDMA101.#O-MIDDLE-NAME #MDMA101.#O-LAST-NAME #MDMA101.#O-SUFFIX INTO #H-NAME
                    pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Suffix()));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Hold_Data_Pnd_H_Name.setValue(DbsUtil.compress(pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix(), pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name(),                //Natural: COMPRESS #MDMA101.#O-PREFIX #MDMA101.#O-FIRST-NAME #MDMA101.#O-MIDDLE-NAME #MDMA101.#O-LAST-NAME #MDMA101.#O-SUFFIX INTO #H-NAME
                    pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Suffix()));
            }                                                                                                                                                             //Natural: END-IF
            pnd_Hold_Data_Pnd_H_Address_1.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_1());                                                                //Natural: MOVE #MDMA101.#O-BASE-ADDRESS-LINE-1 TO #H-ADDRESS-1
            pnd_Hold_Data_Pnd_H_Address_2.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_2());                                                                //Natural: MOVE #MDMA101.#O-BASE-ADDRESS-LINE-2 TO #H-ADDRESS-2
            pnd_Hold_Data_Pnd_H_Address_3.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_3());                                                                //Natural: MOVE #MDMA101.#O-BASE-ADDRESS-LINE-3 TO #H-ADDRESS-3
            pnd_Hold_Data_Pnd_H_Address_4.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_4());                                                                //Natural: MOVE #MDMA101.#O-BASE-ADDRESS-LINE-4 TO #H-ADDRESS-4
            pnd_Hold_Data_Pnd_H_Address_5.reset();                                                                                                                        //Natural: RESET #H-ADDRESS-5
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Hold_Data_Pnd_H_Name.setValue("*** PARTICIPANT NAME NOT FOUND ***");                                                                                      //Natural: MOVE '*** PARTICIPANT NAME NOT FOUND ***' TO #H-NAME
            //*  PIN-EXPANSION-END
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  MB
    private void sub_Setup_W2_Record() throws Exception                                                                                                                   //Natural: SETUP-W2-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_W2_Record_Pnd_Company_Code.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form());                                                               //Natural: ASSIGN #W2-RECORD.#COMPANY-CODE := TWRPYMNT-COMPANY-CDE-FORM
        pnd_W2_Record_Pnd_Tax_Year.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());                                                                           //Natural: ASSIGN #W2-RECORD.#TAX-YEAR := #XTAXYR-F94.TWRPYMNT-TAX-YEAR
        pnd_W2_Record_Pnd_Tax_Id_Type.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Type());                                                                     //Natural: ASSIGN #W2-RECORD.#TAX-ID-TYPE := TWRPYMNT-TAX-ID-TYPE
        pnd_W2_Record_Pnd_Tax_Id_Nbr.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr());                                                                       //Natural: ASSIGN #W2-RECORD.#TAX-ID-NBR := TWRPYMNT-TAX-ID-NBR
        pnd_W2_Record_Pnd_Contract_Nbr.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr());                                                                   //Natural: ASSIGN #W2-RECORD.#CONTRACT-NBR := TWRPYMNT-CONTRACT-NBR
        pnd_W2_Record_Pnd_Payee_Cde.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payee_Cde());                                                                         //Natural: ASSIGN #W2-RECORD.#PAYEE-CDE := TWRPYMNT-PAYEE-CDE
        pnd_W2_Record_Pnd_Residency_Type.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Type());                                                               //Natural: ASSIGN #W2-RECORD.#RESIDENCY-TYPE := TWRPYMNT-RESIDENCY-TYPE
        pnd_W2_Record_Pnd_Residency_Code.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code());                                                               //Natural: ASSIGN #W2-RECORD.#RESIDENCY-CODE := TWRPYMNT-RESIDENCY-CODE
        if (condition(pnd_W2_Record_Pnd_Transaction_Dt.equals(" ")))                                                                                                      //Natural: IF #W2-RECORD.#TRANSACTION-DT = ' '
        {
            pnd_W2_Record_Pnd_Transaction_Dt.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Dte().getValue(pnd_I));                                                //Natural: ASSIGN #W2-RECORD.#TRANSACTION-DT := TWRPYMNT-PYMNT-DTE ( #I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  JRR 05/28/10
        pnd_W2_Record_Pnd_Sys_Dte_Time.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Sys_Dte_Time().getValue(pnd_I));       //Natural: MOVE EDITED TWRPYMNT-SYS-DTE-TIME ( #I ) TO #W2-RECORD.#SYS-DTE-TIME ( EM = YYYYMMDDHHIISST )
        //*  YEAR-TO-DATE
        ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_I).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_I));                        //Natural: ADD TWRPYMNT-INT-AMT ( #I ) TO TWRPYMNT-GROSS-AMT ( #I )
        pnd_W2_Record_Pnd_Gross_Amount.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_I));                                                      //Natural: ASSIGN #W2-RECORD.#GROSS-AMOUNT := TWRPYMNT-GROSS-AMT ( #I )
        pnd_W2_Record_Pnd_Federal_Tax.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_I));                                                   //Natural: ASSIGN #W2-RECORD.#FEDERAL-TAX := TWRPYMNT-FED-WTHLD-AMT ( #I )
        pnd_W2_Record_Pnd_State_Tax.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_I));                                                       //Natural: ASSIGN #W2-RECORD.#STATE-TAX := TWRPYMNT-STATE-WTHLD ( #I )
        pnd_W2_Record_Pnd_Record_Type.setValue(1);                                                                                                                        //Natural: ASSIGN #W2-RECORD.#RECORD-TYPE := 1
        pnd_W2_Record_Pnd_Name.setValue(pnd_Hold_Data_Pnd_H_Name);                                                                                                        //Natural: ASSIGN #W2-RECORD.#NAME := #H-NAME
        pnd_W2_Record_Pnd_Address_1.setValue(pnd_Hold_Data_Pnd_H_Address_1);                                                                                              //Natural: ASSIGN #W2-RECORD.#ADDRESS-1 := #H-ADDRESS-1
        pnd_W2_Record_Pnd_Address_2.setValue(pnd_Hold_Data_Pnd_H_Address_2);                                                                                              //Natural: ASSIGN #W2-RECORD.#ADDRESS-2 := #H-ADDRESS-2
        pnd_W2_Record_Pnd_Address_3.setValue(pnd_Hold_Data_Pnd_H_Address_3);                                                                                              //Natural: ASSIGN #W2-RECORD.#ADDRESS-3 := #H-ADDRESS-3
        pnd_W2_Record_Pnd_Address_4.setValue(pnd_Hold_Data_Pnd_H_Address_4);                                                                                              //Natural: ASSIGN #W2-RECORD.#ADDRESS-4 := #H-ADDRESS-4
        pnd_W2_Record_Pnd_Address_5.setValue(pnd_Hold_Data_Pnd_H_Address_5);                                                                                              //Natural: ASSIGN #W2-RECORD.#ADDRESS-5 := #H-ADDRESS-5
        pnd_W2_Record_Pnd_State_Code.setValue(pnd_Hold_Data_Pnd_H_State_Code);                                                                                            //Natural: ASSIGN #W2-RECORD.#STATE-CODE := #H-STATE-CODE
        pnd_W2_Record_Pnd_State_Id.setValue(pnd_Hold_Data_Pnd_H_State_Id);                                                                                                //Natural: ASSIGN #W2-RECORD.#STATE-ID := #H-STATE-ID
    }
    private void sub_Process_Control_Record() throws Exception                                                                                                            //Natural: PROCESS-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        getWorkFiles().read(1, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                             //Natural: READ WORK FILE 1 ONCE #TWRP0600-CONTROL-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"CONTROL RECORD IS EMPTY",new TabSetting(77),"***");                                      //Natural: WRITE ( 1 ) '***' 25T 'CONTROL RECORD IS EMPTY' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(101);  if (true) return;                                                                                                                    //Natural: TERMINATE 101
        }                                                                                                                                                                 //Natural: END-ENDFILE
        getReports().write(0, "FROM DATE",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd());                                                       //Natural: WRITE 'FROM DATE' #TWRP0600-FROM-CCYYMMDD
        if (Global.isEscape()) return;
        getReports().write(0, "TO   DATE",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd());                                                         //Natural: WRITE 'TO   DATE' #TWRP0600-TO-CCYYMMDD
        if (Global.isEscape()) return;
        getReports().write(0, "TAX  YEAR",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy());                                                       //Natural: WRITE 'TAX  YEAR' #TWRP0600-TAX-YEAR-CCYY
        if (Global.isEscape()) return;
        getReports().write(0, "INTF DATE",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd());                                                  //Natural: WRITE 'INTF DATE' #TWRP0600-INTERFACE-CCYYMMDD
        if (Global.isEscape()) return;
        pnd_Temp_Date.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd());                                                             //Natural: MOVE #TWRP0600-INTERFACE-CCYYMMDD TO #TEMP-DATE
        if (condition(pnd_Temp_Date_Pnd_Mm.less(3)))                                                                                                                      //Natural: IF #MM < 3
        {
            pnd_Temp_Date_Pnd_Yyyy.nsubtract(1);                                                                                                                          //Natural: COMPUTE #YYYY = #YYYY - 1
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Temp_Date_Pnd_Mm.setValue(1);                                                                                                                                 //Natural: MOVE 1 TO #MM #DD
        pnd_Temp_Date_Pnd_Dd.setValue(1);
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd().setValue(pnd_Temp_Date);                                                                  //Natural: MOVE #TEMP-DATE TO #TWRP0600-FROM-CCYYMMDD
        getReports().write(0, "NEW FROM DATE",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd());                                                   //Natural: WRITE 'NEW FROM DATE' #TWRP0600-FROM-CCYYMMDD
        if (Global.isEscape()) return;
    }
    private void sub_Get_State_Info() throws Exception                                                                                                                    //Natural: GET-STATE-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().setValue(pnd_W2_Record_Pnd_Tax_Year_N);                                                                                    //Natural: ASSIGN TWRATBL2.#TAX-YEAR := #W2-RECORD.#TAX-YEAR-N
        pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().setValue(false);                                                                                                          //Natural: ASSIGN TWRATBL2.#ABEND-IND := FALSE
        pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().setValue(false);                                                                                                        //Natural: ASSIGN TWRATBL2.#DISPLAY-IND := FALSE
        //*  WRITE // '=' #W2-RECORD.#TAX-YEAR-N
        //*  WRITE // '=' TWRPYMNT-RESIDENCY-CODE
        if (condition(DbsUtil.maskMatches(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code(),"NN")))                                                                 //Natural: IF TWRPYMNT-RESIDENCY-CODE = MASK ( NN )
        {
            pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                         //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "0", ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code())); //Natural: COMPRESS '0' TWRPYMNT-RESIDENCY-CODE INTO TWRATBL2.#STATE-CDE LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("2");                                                                                                         //Natural: ASSIGN TWRATBL2.#FUNCTION := '2'
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code());                                                    //Natural: ASSIGN TWRATBL2.#STATE-CDE := TWRPYMNT-RESIDENCY-CODE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        short decideConditionsMet607 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE TWRPYMNT-COMPANY-CDE-FORM;//Natural: VALUE 'T'
        if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form().equals("T"))))
        {
            decideConditionsMet607++;
            pnd_Temp_State_Id.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                                                                              //Natural: MOVE TWRATBL2.TIRCNTL-STATE-TAX-ID-TIAA TO #TEMP-STATE-ID
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form().equals("X"))))
        {
            decideConditionsMet607++;
            pnd_Temp_State_Id.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Trust());                                                                             //Natural: MOVE TWRATBL2.TIRCNTL-STATE-TAX-ID-TRUST TO #TEMP-STATE-ID
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet607 > 0))
        {
            pnd_Hold_Data_Pnd_H_State_Code.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code());                                                                  //Natural: MOVE TIRCNTL-STATE-ALPHA-CODE TO #H-STATE-CODE
            pnd_Hold_Data_Pnd_H_State_Id.setValue(pnd_Temp_State_Id);                                                                                                     //Natural: MOVE #TEMP-STATE-ID TO #H-STATE-ID
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Error_Routine() throws Exception                                                                                                                     //Natural: ERROR-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        getReports().write(0, "ERR: ",Global.getERROR_NR());                                                                                                              //Natural: WRITE '=' *ERROR-NR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_W2_Record_Pnd_Tax_Id_Nbr);                                                                                                          //Natural: WRITE '=' #W2-RECORD.#TAX-ID-NBR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_W2_Record_Pnd_Address_1);                                                                                                           //Natural: WRITE '=' #W2-RECORD.#ADDRESS-1
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_W2_Record_Pnd_Address_2);                                                                                                           //Natural: WRITE '=' #W2-RECORD.#ADDRESS-2
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_W2_Record_Pnd_Address_3);                                                                                                           //Natural: WRITE '=' #W2-RECORD.#ADDRESS-3
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_W2_Record_Pnd_Address_4);                                                                                                           //Natural: WRITE '=' #W2-RECORD.#ADDRESS-4
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    //*  FE201506 START
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(2).equals("0")  //Natural: IF ##DATA-RESPONSE ( 1 ) = '0' AND ##DATA-RESPONSE ( 2 ) = '0' AND ##DATA-RESPONSE ( 3 ) = '0' AND ##DATA-RESPONSE ( 4 ) = '0'
            && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(3).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(4).equals("0")))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #I2 = 1 TO 60
        for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(60)); pnd_I2.nadd(1))
        {
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_I2)));           //Natural: COMPRESS #RC ##DATA-RESPONSE ( #I2 ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Rc);                                                                                                                                //Natural: WRITE '=' #RC
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,NEWLINE,"****************************", //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
                                                                                                                                                                          //Natural: PERFORM ERROR-ROUTINE
        sub_Error_Routine();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
}
