/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:29:53 PM
**        * FROM NATURAL PROGRAM : Twrp0370
************************************************************
**        * FILE NAME            : Twrp0370.java
**        * CLASS NAME           : Twrp0370
**        * INSTANCE NAME        : Twrp0370
************************************************************
************************************************************************
** PROGRAM NAME:  TWRP0370
** SYSTEM      :  TAX REPORTING SYSTEM
** AUTHOR      :  ANATOLY
** PURPOSE     :  CITE FILE DUMP
**             :  (CITED TRANSACTIONS DISPLAY)
************************************************************************
** MODIFICATION LOG
** ----------------
** 02/19/99    :
** 08/11/00    : KK          - LIST ONLY RECORDS MATCHING PARM DATE.
** 12/12/01    : KK          - CHANGE #ERR-STATS(N04/1:99) TO N05/1:99.
** 06-04-04    : A. YOUNG    - SUPRESS PROCESSING OF ERRORS 74, 75, AND
**             :               76 FOR ALL SOURCE CODES.
**             :             - MOVED REPORT WRITING PROCESS TO NEW SUB-
**             :               ROUTINE A100-WRITE-REPORT, AND SIMPLIFIED
**             :               CODE.
**             :             - INCREASED #ERR-STATS(N05/1:99) TO
**             :               #ERR-STATS(P9/1:99)(EM=-ZZZ,ZZZ,ZZ9).
** 02-08-06    : B. KARCHEVSKY - ADD WORK FILE FOR NEXT REPORT WILL BE
**                               SORTED BY ERRORS.
**                               PROCESS BY ONE YEAR
** 08-29-11    : R. OPPENHEIM - ADDED PERFORM PROCESS-INPUT-PARMS TO
**             :                PROCESS REPORTS FOR TO CONSECUTIVE YEARS
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0370 extends BLNatBase
{
    // Data Areas
    private LdaTwrl9900 ldaTwrl9900;
    private LdaTwrl9807 ldaTwrl9807;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Err_Stats;
    private DbsField pnd_Error;
    private DbsField pnd_Nbr;
    private DbsField pnd_Msg;
    private DbsField pnd_K;
    private DbsField pnd_Er_Su;

    private DbsGroup pnd_Er_Su__R_Field_1;
    private DbsField pnd_Er_Su_Pnd_Er_Tbl;
    private DbsField pnd_Er_Su_Pnd_Er_Yr;
    private DbsField pnd_Er_No;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_O;
    private DbsField pnd_Comp_Name;
    private DbsField pnd_Ws_No_Of_Prtcpnts;
    private DbsField pnd_Main_Print;
    private DbsField pnd_Datn;
    private DbsField pnd_Parm_Tax_Year;

    private DbsGroup pnd_Parm_Tax_Year__R_Field_2;
    private DbsField pnd_Parm_Tax_Year_Pnd_Parm_Tax_Yr;
    private DbsField pnd_Tax_Year;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl9900 = new LdaTwrl9900();
        registerRecord(ldaTwrl9900);
        registerRecord(ldaTwrl9900.getVw_citation_View());
        ldaTwrl9807 = new LdaTwrl9807();
        registerRecord(ldaTwrl9807);
        registerRecord(ldaTwrl9807.getVw_tircntl_Error_Msg_Tbl_View_View());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Err_Stats = localVariables.newFieldArrayInRecord("pnd_Err_Stats", "#ERR-STATS", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 100));
        pnd_Error = localVariables.newFieldArrayInRecord("pnd_Error", "#ERROR", FieldType.STRING, 37, new DbsArrayController(1, 100));
        pnd_Nbr = localVariables.newFieldInRecord("pnd_Nbr", "#NBR", FieldType.PACKED_DECIMAL, 2);
        pnd_Msg = localVariables.newFieldInRecord("pnd_Msg", "#MSG", FieldType.STRING, 37);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_Er_Su = localVariables.newFieldInRecord("pnd_Er_Su", "#ER-SU", FieldType.NUMERIC, 8);

        pnd_Er_Su__R_Field_1 = localVariables.newGroupInRecord("pnd_Er_Su__R_Field_1", "REDEFINE", pnd_Er_Su);
        pnd_Er_Su_Pnd_Er_Tbl = pnd_Er_Su__R_Field_1.newFieldInGroup("pnd_Er_Su_Pnd_Er_Tbl", "#ER-TBL", FieldType.NUMERIC, 1);
        pnd_Er_Su_Pnd_Er_Yr = pnd_Er_Su__R_Field_1.newFieldInGroup("pnd_Er_Su_Pnd_Er_Yr", "#ER-YR", FieldType.NUMERIC, 4);
        pnd_Er_No = localVariables.newFieldInRecord("pnd_Er_No", "#ER-NO", FieldType.NUMERIC, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 8);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 8);
        pnd_O = localVariables.newFieldInRecord("pnd_O", "#O", FieldType.PACKED_DECIMAL, 8);
        pnd_Comp_Name = localVariables.newFieldInRecord("pnd_Comp_Name", "#COMP-NAME", FieldType.STRING, 4);
        pnd_Ws_No_Of_Prtcpnts = localVariables.newFieldInRecord("pnd_Ws_No_Of_Prtcpnts", "#WS-NO-OF-PRTCPNTS", FieldType.PACKED_DECIMAL, 9);
        pnd_Main_Print = localVariables.newFieldInRecord("pnd_Main_Print", "#MAIN-PRINT", FieldType.BOOLEAN, 1);
        pnd_Datn = localVariables.newFieldInRecord("pnd_Datn", "#DATN", FieldType.NUMERIC, 4);
        pnd_Parm_Tax_Year = localVariables.newFieldInRecord("pnd_Parm_Tax_Year", "#PARM-TAX-YEAR", FieldType.STRING, 4);

        pnd_Parm_Tax_Year__R_Field_2 = localVariables.newGroupInRecord("pnd_Parm_Tax_Year__R_Field_2", "REDEFINE", pnd_Parm_Tax_Year);
        pnd_Parm_Tax_Year_Pnd_Parm_Tax_Yr = pnd_Parm_Tax_Year__R_Field_2.newFieldInGroup("pnd_Parm_Tax_Year_Pnd_Parm_Tax_Yr", "#PARM-TAX-YR", FieldType.NUMERIC, 
            4);
        pnd_Tax_Year = localVariables.newFieldInRecord("pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl9900.initializeValues();
        ldaTwrl9807.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0370() throws Exception
    {
        super("Twrp0370");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp0370|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        while(true)
        {
            try
            {
                //* *---------------------------------------------**
                //* *                                                                                                                                                     //Natural: FORMAT ( 01 ) LS = 132 PS = 60;//Natural: FORMAT ( 02 ) LS = 132 PS = 60
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Parm_Tax_Year);                                                                                    //Natural: INPUT #PARM-TAX-YEAR
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT-PARMS
                sub_Process_Input_Parms();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Er_Su_Pnd_Er_Yr.setValue(pnd_Tax_Year);                                                                                                               //Natural: ASSIGN #ER-YR := #TAX-YEAR
                pnd_Er_Su_Pnd_Er_Tbl.setValue(7);                                                                                                                         //Natural: MOVE 7 TO #ER-TBL
                ldaTwrl9807.getVw_tircntl_Error_Msg_Tbl_View_View().startDatabaseRead                                                                                     //Natural: READ TIRCNTL-ERROR-MSG-TBL-VIEW-VIEW WITH TIRCNTL-NBR-YEAR-SEQ = #ER-SU
                (
                "READ01",
                new Wc[] { new Wc("TIRCNTL_NBR_YEAR_SEQ", ">=", pnd_Er_Su, WcType.BY) },
                new Oc[] { new Oc("TIRCNTL_NBR_YEAR_SEQ", "ASC") }
                );
                READ01:
                while (condition(ldaTwrl9807.getVw_tircntl_Error_Msg_Tbl_View_View().readNextRow("READ01")))
                {
                    if (condition(ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Tbl_Nbr().notEquals(pnd_Er_Su_Pnd_Er_Tbl) || ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Tax_Year().notEquals(pnd_Tax_Year))) //Natural: IF TIRCNTL-TBL-NBR NE #ER-TBL OR TIRCNTL-TAX-YEAR NE #TAX-YEAR
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Seq_Nbr().greater(99)))                                                          //Natural: IF TIRCNTL-SEQ-NBR > 99
                    {
                        getReports().write(0, "TABLE FOR ERRORS OVERFLOW.",Global.getPROGRAM()," REQUIRE UPDATE",NEWLINE,"CONTACT DEVELOPERS");                           //Natural: WRITE 'TABLE FOR ERRORS OVERFLOW.' *PROGRAM ' REQUIRE UPDATE' / 'CONTACT DEVELOPERS'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        DbsUtil.terminate(90);  if (true) return;                                                                                                         //Natural: TERMINATE 90
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Error.getValue(ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Seq_Nbr().getInt() + 1).setValue(ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr()); //Natural: MOVE TIRCNTL-ERROR-DESCR TO #ERROR ( TIRCNTL-SEQ-NBR )
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                pnd_Error.getValue(0 + 1).setValue("UNKNOWN ERROR NUMBER");                                                                                               //Natural: ASSIGN #ERROR ( 0 ) := 'UNKNOWN ERROR NUMBER'
                //* *
                ldaTwrl9900.getVw_citation_View().startDatabaseRead                                                                                                       //Natural: READ CITATION-VIEW BY TWRCITED-TIN-CNTRCT-PMNT-DTE WHERE TWRCITED-TAX-YEAR EQ #TAX-YEAR
                (
                "READ02",
                new Wc[] { new Wc("TWRCITED_TAX_YEAR", "=", pnd_Tax_Year, WcType.WHERE) },
                new Oc[] { new Oc("TWRCITED_TIN_CNTRCT_PMNT_DTE", "ASC") }
                );
                boolean endOfDataRead02 = true;
                boolean firstRead02 = true;
                READ02:
                while (condition(ldaTwrl9900.getVw_citation_View().readNextRow("READ02")))
                {
                    if (condition(ldaTwrl9900.getVw_citation_View().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventRead02();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataRead02 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    pnd_I.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #I
                    pnd_Main_Print.reset();                                                                                                                               //Natural: RESET #MAIN-PRINT #ER-NO
                    pnd_Er_No.reset();
                                                                                                                                                                          //Natural: PERFORM A100-WRITE-REPORT
                    sub_A100_Write_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: AT BREAK OF TWRCITED-TAX-ID-NBR;//Natural: END-READ
                if (condition(ldaTwrl9900.getVw_citation_View().getAstCOUNTER().greater(0)))
                {
                    atBreakEventRead02(endOfDataRead02);
                }
                if (Global.isEscape()) return;
                //* *
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"*",new RepeatItem(131),NEWLINE,"FOR YEAR :",pnd_Tax_Year, new ReportEditMask          //Natural: WRITE ( 01 ) /// '*' ( 131 ) / 'FOR YEAR :' #TAX-YEAR / 'TOTAL RECORDS OF THE CITE FILE     :' #I / 'TOTAL OUTPUT RECORDS               :' #O / 'NO. OF PARTICIPANTS WITH 1 OR MORE CITES' #WS-NO-OF-PRTCPNTS
                    ("9999"),NEWLINE,"TOTAL RECORDS OF THE CITE FILE     :",pnd_I,NEWLINE,"TOTAL OUTPUT RECORDS               :",pnd_O,NEWLINE,"NO. OF PARTICIPANTS WITH 1 OR MORE CITES",
                    pnd_Ws_No_Of_Prtcpnts);
                if (Global.isEscape()) return;
                //* *
                //* *PRINT ERROR STATISTICS
                FOR01:                                                                                                                                                    //Natural: FOR #NBR 0 98
                for (pnd_Nbr.setValue(0); condition(pnd_Nbr.lessOrEqual(98)); pnd_Nbr.nadd(1))
                {
                    if (condition(pnd_Err_Stats.getValue(pnd_Nbr.getInt() + 1).greater(getZero())))                                                                       //Natural: IF #ERR-STATS ( #NBR ) > 0
                    {
                        getReports().write(2, ReportOption.NOTITLE,pnd_Nbr," - ",pnd_Error.getValue(pnd_Nbr.getInt() + 1),":",pnd_Err_Stats.getValue(pnd_Nbr.getInt() + 1),  //Natural: WRITE ( 02 ) #NBR ' - ' #ERROR ( #NBR ) ':' #ERR-STATS ( #NBR )
                            new ReportEditMask ("-ZZZ,ZZZ,ZZ9"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //* *
                //* *                                                                                                                                                     //Natural: AT TOP OF PAGE ( 01 )
                //* *                                                                                                                                                     //Natural: AT TOP OF PAGE ( 02 )
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A100-WRITE-REPORT
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INPUT-PARMS
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_A100_Write_Report() throws Exception                                                                                                                 //Natural: A100-WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #K 1 30
        for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(30)); pnd_K.nadd(1))
        {
            if (condition(ldaTwrl9900.getCitation_View_Twrcited_Err_Msg_Nbrs().getValue(pnd_K).greaterOrEqual(74) && ldaTwrl9900.getCitation_View_Twrcited_Err_Msg_Nbrs().getValue(pnd_K).lessOrEqual(76))) //Natural: IF TWRCITED-ERR-MSG-NBRS ( #K ) = 74 THRU 76
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl9900.getCitation_View_Twrcited_Err_Msg_Nbrs().getValue(pnd_K).greater(getZero()) || pnd_K.equals(1)))                                    //Natural: IF TWRCITED-ERR-MSG-NBRS ( #K ) > 0 OR #K = 1
            {
                pnd_Er_No.setValue(ldaTwrl9900.getCitation_View_Twrcited_Err_Msg_Nbrs().getValue(pnd_K));                                                                 //Natural: MOVE TWRCITED-ERR-MSG-NBRS ( #K ) TO #ER-NO
                pnd_Err_Stats.getValue(pnd_Er_No.getInt() + 1).nadd(1);                                                                                                   //Natural: COMPUTE #ERR-STATS ( #ER-NO ) = #ERR-STATS ( #ER-NO ) + 1
                pnd_Msg.setValue(pnd_Error.getValue(pnd_Er_No.getInt() + 1));                                                                                             //Natural: ASSIGN #MSG := #ERROR ( #ER-NO )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Main_Print.getBoolean())))                                                                                                               //Natural: IF NOT #MAIN-PRINT
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,ldaTwrl9900.getCitation_View_Twrcited_Contract_Nbr(),ldaTwrl9900.getCitation_View_Twrcited_Payee_Cde(),new  //Natural: WRITE ( 01 ) / TWRCITED-CONTRACT-NBR TWRCITED-PAYEE-CDE 1X TWRCITED-PYMNT-DTE 1X TWRCITED-TAX-ID-NBR TWRCITED-PROCESSED-IND 2X TWRCITED-TAX-ID-TYPE TWRCITED-GROSS-AMT TWRCITED-INT-AMT TWRCITED-IVC-AMT TWRCITED-FED-WTHLD-AMT TWRCITED-NRA-WTHLD-AMT TWRCITED-STATE-WTHLD TWRCITED-LOCAL-WTHLD 3X TWRCITED-RESIDENCY-CODE 2X TWRCITED-TAX-CITIZENSHIP / 'Source Code:' TWRCITED-ORGN-SRCE-CDE '                        Error Description:' #MSG
                    ColumnSpacing(1),ldaTwrl9900.getCitation_View_Twrcited_Pymnt_Dte(),new ColumnSpacing(1),ldaTwrl9900.getCitation_View_Twrcited_Tax_Id_Nbr(),ldaTwrl9900.getCitation_View_Twrcited_Processed_Ind(),new 
                    ColumnSpacing(2),ldaTwrl9900.getCitation_View_Twrcited_Tax_Id_Type(),ldaTwrl9900.getCitation_View_Twrcited_Gross_Amt(),ldaTwrl9900.getCitation_View_Twrcited_Int_Amt(),ldaTwrl9900.getCitation_View_Twrcited_Ivc_Amt(),ldaTwrl9900.getCitation_View_Twrcited_Fed_Wthld_Amt(),ldaTwrl9900.getCitation_View_Twrcited_Nra_Wthld_Amt(),ldaTwrl9900.getCitation_View_Twrcited_State_Wthld(),ldaTwrl9900.getCitation_View_Twrcited_Local_Wthld(),new 
                    ColumnSpacing(3),ldaTwrl9900.getCitation_View_Twrcited_Residency_Code(),new ColumnSpacing(2),ldaTwrl9900.getCitation_View_Twrcited_Tax_Citizenship(),
                    NEWLINE,"Source Code:",ldaTwrl9900.getCitation_View_Twrcited_Orgn_Srce_Cde(),"                        Error Description:",pnd_Msg);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Main_Print.setValue(true);                                                                                                                            //Natural: ASSIGN #MAIN-PRINT := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(59),pnd_Msg);                                                                                //Natural: WRITE ( 01 ) 59X #MSG
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_O.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #O
            getWorkFiles().write(1, false, pnd_Er_No, ldaTwrl9900.getVw_citation_View());                                                                                 //Natural: WRITE WORK FILE 1 #ER-NO CITATION-VIEW
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  A100-WRITE-REPORT
    }
    private void sub_Process_Input_Parms() throws Exception                                                                                                               //Natural: PROCESS-INPUT-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        //*  CURRENT YR + 1
        //*  CURRENT YR
        //*  CURRENT YR - 1
        //*  CURRENT YR - 2
        //*  CURRENT YR - 3
        short decideConditionsMet189 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST #PARM-TAX-YEAR;//Natural: VALUE '+1'
        if (condition((pnd_Parm_Tax_Year.equals("+1"))))
        {
            decideConditionsMet189++;
            pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Tax_Year), Global.getDATN().divide(10000).add(1));                                                      //Natural: ASSIGN #TAX-YEAR := *DATN / 10000 + 1
        }                                                                                                                                                                 //Natural: VALUE ' ','0'
        else if (condition((pnd_Parm_Tax_Year.equals(" ") || pnd_Parm_Tax_Year.equals("0"))))
        {
            decideConditionsMet189++;
            pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Tax_Year), Global.getDATN().divide(10000).subtract(getZero()));                                         //Natural: ASSIGN #TAX-YEAR := *DATN / 10000 - 0
        }                                                                                                                                                                 //Natural: VALUE '-1'
        else if (condition((pnd_Parm_Tax_Year.equals("-1"))))
        {
            decideConditionsMet189++;
            pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Tax_Year), Global.getDATN().divide(10000).subtract(1));                                                 //Natural: ASSIGN #TAX-YEAR := *DATN / 10000 - 1
        }                                                                                                                                                                 //Natural: VALUE '-2'
        else if (condition((pnd_Parm_Tax_Year.equals("-2"))))
        {
            decideConditionsMet189++;
            pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Tax_Year), Global.getDATN().divide(10000).subtract(2));                                                 //Natural: ASSIGN #TAX-YEAR := *DATN / 10000 - 2
        }                                                                                                                                                                 //Natural: VALUE '-3'
        else if (condition((pnd_Parm_Tax_Year.equals("-3"))))
        {
            decideConditionsMet189++;
            pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Tax_Year), Global.getDATN().divide(10000).subtract(3));                                                 //Natural: ASSIGN #TAX-YEAR := *DATN / 10000 - 3
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Tax_Year.setValue(pnd_Parm_Tax_Year_Pnd_Parm_Tax_Yr);                                                                                                     //Natural: ASSIGN #TAX-YEAR := #PARM-TAX-YR
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  PROCESS-INPUT-PARMS
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name:",Global.getINIT_USER(),NEWLINE,"Program: TWRP0370-01",new                    //Natural: WRITE ( 01 ) NOTITLE NOHDR 'Job Name:' *INIT-USER / 'Program: TWRP0370-01' 18X 'Tax Reporting And Withholding System' 30X 'Page:   ' *PAGE-NUMBER ( 01 ) / 33X 'Cited Transactions Loaded Into the Cite File' 27X 'Date: '*DATU / 54X #COMP-NAME // 'Contract Payee Pay   Taxpayer Proc Tax ID    Gross     Interest' '     IVC        Fed Tax        NRA       State       Local  Res Ctz' / 'Number   Code  Date  Ind.ID No Ind Type     Amount       Amount  ' '   Amount     Amount       Amount      Amount      Amount Cde Cde' / '                                                                '
                        ColumnSpacing(18),"Tax Reporting And Withholding System",new ColumnSpacing(30),"Page:   ",getReports().getPageNumberDbs(1),NEWLINE,new 
                        ColumnSpacing(33),"Cited Transactions Loaded Into the Cite File",new ColumnSpacing(27),"Date: ",Global.getDATU(),NEWLINE,new ColumnSpacing(54),
                        pnd_Comp_Name,NEWLINE,NEWLINE,"Contract Payee Pay   Taxpayer Proc Tax ID    Gross     Interest","     IVC        Fed Tax        NRA       State       Local  Res Ctz",
                        NEWLINE,"Number   Code  Date  Ind.ID No Ind Type     Amount       Amount  ","   Amount     Amount       Amount      Amount      Amount Cde Cde",
                        NEWLINE,"                                                                ");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name:",Global.getINIT_USER(),NEWLINE,"Program: TWRP0370-02",new                    //Natural: WRITE ( 02 ) NOTITLE NOHDR 'Job Name:' *INIT-USER / 'Program: TWRP0370-02' 18X 'Tax Reporting And Withholding System' 30X 'Page:   ' *PAGE-NUMBER ( 02 ) / 33X 'Histogram of the cited errors of the Cite File' 25X 'Date: '*DATU // 'Error       Error                          Number of' / 'Number    Description                      Occurrences' / '*' ( 77 )
                        ColumnSpacing(18),"Tax Reporting And Withholding System",new ColumnSpacing(30),"Page:   ",getReports().getPageNumberDbs(2),NEWLINE,new 
                        ColumnSpacing(33),"Histogram of the cited errors of the Cite File",new ColumnSpacing(25),"Date: ",Global.getDATU(),NEWLINE,NEWLINE,"Error       Error                          Number of",NEWLINE,"Number    Description                      Occurrences",NEWLINE,"*",new 
                        RepeatItem(77));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventRead02() throws Exception {atBreakEventRead02(false);}
    private void atBreakEventRead02(boolean endOfData) throws Exception
    {
        boolean ldaTwrl9900_getCitation_View_Twrcited_Tax_Id_NbrIsBreak = ldaTwrl9900.getCitation_View_Twrcited_Tax_Id_Nbr().isBreak(endOfData);
        if (condition(ldaTwrl9900_getCitation_View_Twrcited_Tax_Id_NbrIsBreak))
        {
            pnd_Ws_No_Of_Prtcpnts.nadd(1);                                                                                                                                //Natural: ADD 1 TO #WS-NO-OF-PRTCPNTS
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60");
        Global.format(2, "LS=132 PS=60");
    }
}
