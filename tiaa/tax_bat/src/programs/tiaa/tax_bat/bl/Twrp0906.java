/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:31:53 PM
**        * FROM NATURAL PROGRAM : Twrp0906
************************************************************
**        * FILE NAME            : Twrp0906.java
**        * CLASS NAME           : Twrp0906
**        * INSTANCE NAME        : Twrp0906
************************************************************
* PROGRAM : TWRP0906
* FUNCTION: TO UPDATE FLAT FILE1 WITH PROPER DATE RANGE AND PROCESSING
*           TYPE, IN ORDER TO DO MONTHLY RUN OF TWRP0970 IN JOB
*           P1370TWM.(ADDED 4 STEPS IN THIS JOB)
* AUTHOR  : ROSE MA
* DATE    : 08/26/2004
* HISTORY :
* 12/01/04  RM  UPDATE LOGIC TO PREVENT DATE OVERRUN TO NEXT MONTH.
*
***********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0906 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0901 ldaTwrl0901;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Ws_Run_Date;

    private DbsGroup pnd_Ws_Run_Date__R_Field_1;
    private DbsField pnd_Ws_Run_Date_Pnd_Ws_Date_A;

    private DbsGroup pnd_Ws_Run_Date__R_Field_2;
    private DbsField pnd_Ws_Run_Date_Pnd_Ws_Yyyy;
    private DbsField pnd_Ws_Run_Date_Pnd_Ws_Mm;
    private DbsField pnd_Ws_Run_Date_Pnd_Ws_Dd;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0901 = new LdaTwrl0901();
        registerRecord(ldaTwrl0901);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Ws_Run_Date = localVariables.newFieldInRecord("pnd_Ws_Run_Date", "#WS-RUN-DATE", FieldType.NUMERIC, 8);

        pnd_Ws_Run_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Run_Date__R_Field_1", "REDEFINE", pnd_Ws_Run_Date);
        pnd_Ws_Run_Date_Pnd_Ws_Date_A = pnd_Ws_Run_Date__R_Field_1.newFieldInGroup("pnd_Ws_Run_Date_Pnd_Ws_Date_A", "#WS-DATE-A", FieldType.STRING, 8);

        pnd_Ws_Run_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Run_Date__R_Field_2", "REDEFINE", pnd_Ws_Run_Date);
        pnd_Ws_Run_Date_Pnd_Ws_Yyyy = pnd_Ws_Run_Date__R_Field_2.newFieldInGroup("pnd_Ws_Run_Date_Pnd_Ws_Yyyy", "#WS-YYYY", FieldType.NUMERIC, 4);
        pnd_Ws_Run_Date_Pnd_Ws_Mm = pnd_Ws_Run_Date__R_Field_2.newFieldInGroup("pnd_Ws_Run_Date_Pnd_Ws_Mm", "#WS-MM", FieldType.NUMERIC, 2);
        pnd_Ws_Run_Date_Pnd_Ws_Dd = pnd_Ws_Run_Date__R_Field_2.newFieldInGroup("pnd_Ws_Run_Date_Pnd_Ws_Dd", "#WS-DD", FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0901.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0906() throws Exception
    {
        super("Twrp0906");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132
        pnd_Ws_Run_Date.setValue(Global.getDATN());                                                                                                                       //Natural: ASSIGN #WS-RUN-DATE := *DATN
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #FLAT-FILE1
        while (condition(getWorkFiles().read(1, ldaTwrl0901.getPnd_Flat_File1())))
        {
            //*  PREVENT DATE OVERRUN TO NEXT MONTH 12/01/04 RM
            if (condition(pnd_Ws_Run_Date_Pnd_Ws_Dd.less("27")))                                                                                                          //Natural: IF #WS-DD LT '27'
            {
                pnd_Ws_Run_Date_Pnd_Ws_Mm.nsubtract(1);                                                                                                                   //Natural: ASSIGN #WS-MM := #WS-MM - 1
                if (condition(pnd_Ws_Run_Date_Pnd_Ws_Mm.equals(getZero())))                                                                                               //Natural: IF #WS-MM = 0
                {
                    pnd_Ws_Run_Date_Pnd_Ws_Mm.setValue(12);                                                                                                               //Natural: ASSIGN #WS-MM := 12
                    pnd_Ws_Run_Date_Pnd_Ws_Yyyy.nsubtract(1);                                                                                                             //Natural: ASSIGN #WS-YYYY := #WS-YYYY - 1
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet49 = 0;                                                                                                                          //Natural: DECIDE ON FIRST VALUE OF #WS-MM;//Natural: VALUE 01, 03, 05, 07, 08, 10, 12
                if (condition((pnd_Ws_Run_Date_Pnd_Ws_Mm.equals(1) || pnd_Ws_Run_Date_Pnd_Ws_Mm.equals(3) || pnd_Ws_Run_Date_Pnd_Ws_Mm.equals(5) || pnd_Ws_Run_Date_Pnd_Ws_Mm.equals(7) 
                    || pnd_Ws_Run_Date_Pnd_Ws_Mm.equals(8) || pnd_Ws_Run_Date_Pnd_Ws_Mm.equals(10) || pnd_Ws_Run_Date_Pnd_Ws_Mm.equals(12))))
                {
                    decideConditionsMet49++;
                    pnd_Ws_Run_Date_Pnd_Ws_Dd.setValue(31);                                                                                                               //Natural: ASSIGN #WS-DD := 31
                }                                                                                                                                                         //Natural: VALUE 04, 06, 09, 11
                else if (condition((pnd_Ws_Run_Date_Pnd_Ws_Mm.equals(4) || pnd_Ws_Run_Date_Pnd_Ws_Mm.equals(6) || pnd_Ws_Run_Date_Pnd_Ws_Mm.equals(9) 
                    || pnd_Ws_Run_Date_Pnd_Ws_Mm.equals(11))))
                {
                    decideConditionsMet49++;
                    pnd_Ws_Run_Date_Pnd_Ws_Dd.setValue(30);                                                                                                               //Natural: ASSIGN #WS-DD := 30
                }                                                                                                                                                         //Natural: VALUE 02
                else if (condition((pnd_Ws_Run_Date_Pnd_Ws_Mm.equals(2))))
                {
                    decideConditionsMet49++;
                    pnd_Ws_Run_Date_Pnd_Ws_Dd.setValue(29);                                                                                                               //Natural: ASSIGN #WS-DD := 29
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_End_Date().setValue(pnd_Ws_Run_Date_Pnd_Ws_Date_A);                                                                     //Natural: ASSIGN #FF1-END-DATE := #WS-DATE-A
            ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Intf_To_Date().setValue(pnd_Ws_Run_Date_Pnd_Ws_Date_A);                                                                 //Natural: ASSIGN #FF1-INTF-TO-DATE := #WS-DATE-A
            ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Tax_Year().setValue(pnd_Ws_Run_Date_Pnd_Ws_Yyyy);                                                                       //Natural: ASSIGN #FF1-TAX-YEAR := #WS-YYYY
            pnd_Ws_Run_Date_Pnd_Ws_Dd.setValue("01");                                                                                                                     //Natural: ASSIGN #WS-DD := '01'
            ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Start_Date().setValue(pnd_Ws_Run_Date_Pnd_Ws_Date_A);                                                                   //Natural: ASSIGN #FF1-START-DATE := #WS-DATE-A
            ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Frequency().setValue("Y");                                                                                              //Natural: ASSIGN #FF1-FREQUENCY := 'Y'
            ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Ytd_Run_Type().setValue("S");                                                                                           //Natural: ASSIGN #FF1-YTD-RUN-TYPE := 'S'
            getWorkFiles().write(2, false, ldaTwrl0901.getPnd_Flat_File1());                                                                                              //Natural: WRITE WORK FILE 2 #FLAT-FILE1
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"***** Work File1 had been updated to run monthly State Summary","Report - TWRP0970 *****"); //Natural: WRITE ( 1 ) NOTITLE NOHDR / '***** Work File1 had been updated to run monthly State Summary' 'Report - TWRP0970 *****'
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(34),"Tax Withholding and Reporting System",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR *INIT-USER '-' *PROGRAM 34T 'Tax Withholding and Reporting System' 119T 'PAGE' *PAGE-NUMBER ( 1 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 42T 'Updated Flat File1' / 'RUNTIME : ' *TIMX 44T 'Tax Year ' #WS-YYYY / 44T 'CONTROL TOTALS' / 32T 'Date Range : ' #FF1-START-DATE ' thru ' #FF1-END-DATE ///
                        TabSetting(119),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(42),"Updated Flat File1",NEWLINE,"RUNTIME : ",Global.getTIMX(),new TabSetting(44),"Tax Year ",pnd_Ws_Run_Date_Pnd_Ws_Yyyy,NEWLINE,new 
                        TabSetting(44),"CONTROL TOTALS",NEWLINE,new TabSetting(32),"Date Range : ",ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Start_Date()," thru ",
                        ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_End_Date(),NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");
    }
}
