/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:43:37 PM
**        * FROM NATURAL PROGRAM : Twrp6114
************************************************************
**        * FILE NAME            : Twrp6114.java
**        * CLASS NAME           : Twrp6114
**        * INSTANCE NAME        : Twrp6114
************************************************************
************************************************************************
*
* PROGRAM  : TWRP6114 (UPDATED FROM RIADFR06)
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : MOORE FORMS EXTRACT CONTROL TOTALS REPORTS.
* CREATED  : 07 / 09 / 2001.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM READS IRA CONTRIBUTION DATA, AND PRODUCES CONTROL
*            TOTAL REPORTS FOR EACH ONE OF THE 'IRA' FORM TYPES
*           (I.E. 10, 20, AND 30).
*********
* UPDATED  : 04 / 01 / 2005.
*   BY     : ROSE MA
*            TAKE OUT ALL HARD CODED TAX REPORTING YEAR AND REPLACED
*            WITH THE EXTRACTED TAX YEAR BY THE PREVIOUS PROGRAM.
*
* HISTORY:
* 02/08/2012 MS REMOVED ABEND, IF NO DATA IS SELCTED FOR THE REPORT.
* 02/01/2005 RM CHANGE ALL HARD CODED TAX REPORTING YEAR (2001-->2004)
* 02/04/2004 RM CHANGE ALL HARD CODED TAX REPORTING YEAR (2000-->2001)
*
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp6114 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Form_Record;
    private DbsField pnd_Form_Record_Pnd_Company;
    private DbsField pnd_Form_Record_Pnd_Tirf_Tax_Year;
    private DbsField pnd_Form_Record_Pnd_Tirf_Form_Type;
    private DbsField pnd_Form_Record_Pnd_Create_Yyyymmdd;

    private DbsGroup pnd_Form_Record__R_Field_1;
    private DbsField pnd_Form_Record_Pnd_Create_Yyyymm;
    private DbsField pnd_Form_Record_Pnd_Tirf_Company_Cde;
    private DbsField pnd_Form_Record_Pnd_Tirf_Tin;
    private DbsField pnd_Form_Record_Pnd_Tirf_Tax_Id_Type;
    private DbsField pnd_Form_Record_Pnd_Tirf_Contract_Nbr;
    private DbsField pnd_Form_Record_Pnd_Tirf_Payee_Cde;
    private DbsField pnd_Form_Record_Pnd_Tirf_Key;
    private DbsField pnd_Form_Record_Pnd_Tirf_Form_Seq;
    private DbsField pnd_Form_Record_Pnd_Tirf_Part_Rpt_Ind;
    private DbsField pnd_Form_Record_Pnd_Tirf_Srce_Cde;
    private DbsField pnd_Form_Record_Pnd_Tirf_Participant_Name;
    private DbsField pnd_Form_Record_Pnd_Tirf_Distribution_Cde;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Accept_Ctr;
    private DbsField pnd_Total;
    private DbsField pnd_Sub_Total;
    private DbsField pnd_Save_Tax_Year;
    private DbsField pnd_Save_Form_Type;
    private DbsField pnd_Save_Create_Date;

    private DbsGroup pnd_Save_Create_Date__R_Field_2;
    private DbsField pnd_Save_Create_Date_Pnd_Save_Create_Yyyymm;
    private DbsField pnd_Save_Company;
    private DbsField pnd_Save_Form_Type_Desc;
    private DbsField pnd_Variable_Header;
    private DbsField pnd_First_Time;
    private DbsField pnd_Tax_Yr_Header;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Form_Record = localVariables.newGroupInRecord("pnd_Form_Record", "#FORM-RECORD");
        pnd_Form_Record_Pnd_Company = pnd_Form_Record.newFieldInGroup("pnd_Form_Record_Pnd_Company", "#COMPANY", FieldType.STRING, 1);
        pnd_Form_Record_Pnd_Tirf_Tax_Year = pnd_Form_Record.newFieldInGroup("pnd_Form_Record_Pnd_Tirf_Tax_Year", "#TIRF-TAX-YEAR", FieldType.STRING, 4);
        pnd_Form_Record_Pnd_Tirf_Form_Type = pnd_Form_Record.newFieldInGroup("pnd_Form_Record_Pnd_Tirf_Form_Type", "#TIRF-FORM-TYPE", FieldType.NUMERIC, 
            2);
        pnd_Form_Record_Pnd_Create_Yyyymmdd = pnd_Form_Record.newFieldInGroup("pnd_Form_Record_Pnd_Create_Yyyymmdd", "#CREATE-YYYYMMDD", FieldType.STRING, 
            8);

        pnd_Form_Record__R_Field_1 = pnd_Form_Record.newGroupInGroup("pnd_Form_Record__R_Field_1", "REDEFINE", pnd_Form_Record_Pnd_Create_Yyyymmdd);
        pnd_Form_Record_Pnd_Create_Yyyymm = pnd_Form_Record__R_Field_1.newFieldInGroup("pnd_Form_Record_Pnd_Create_Yyyymm", "#CREATE-YYYYMM", FieldType.STRING, 
            6);
        pnd_Form_Record_Pnd_Tirf_Company_Cde = pnd_Form_Record.newFieldInGroup("pnd_Form_Record_Pnd_Tirf_Company_Cde", "#TIRF-COMPANY-CDE", FieldType.STRING, 
            1);
        pnd_Form_Record_Pnd_Tirf_Tin = pnd_Form_Record.newFieldInGroup("pnd_Form_Record_Pnd_Tirf_Tin", "#TIRF-TIN", FieldType.STRING, 10);
        pnd_Form_Record_Pnd_Tirf_Tax_Id_Type = pnd_Form_Record.newFieldInGroup("pnd_Form_Record_Pnd_Tirf_Tax_Id_Type", "#TIRF-TAX-ID-TYPE", FieldType.STRING, 
            1);
        pnd_Form_Record_Pnd_Tirf_Contract_Nbr = pnd_Form_Record.newFieldInGroup("pnd_Form_Record_Pnd_Tirf_Contract_Nbr", "#TIRF-CONTRACT-NBR", FieldType.STRING, 
            8);
        pnd_Form_Record_Pnd_Tirf_Payee_Cde = pnd_Form_Record.newFieldInGroup("pnd_Form_Record_Pnd_Tirf_Payee_Cde", "#TIRF-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Form_Record_Pnd_Tirf_Key = pnd_Form_Record.newFieldInGroup("pnd_Form_Record_Pnd_Tirf_Key", "#TIRF-KEY", FieldType.STRING, 5);
        pnd_Form_Record_Pnd_Tirf_Form_Seq = pnd_Form_Record.newFieldInGroup("pnd_Form_Record_Pnd_Tirf_Form_Seq", "#TIRF-FORM-SEQ", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Form_Record_Pnd_Tirf_Part_Rpt_Ind = pnd_Form_Record.newFieldInGroup("pnd_Form_Record_Pnd_Tirf_Part_Rpt_Ind", "#TIRF-PART-RPT-IND", FieldType.STRING, 
            1);
        pnd_Form_Record_Pnd_Tirf_Srce_Cde = pnd_Form_Record.newFieldInGroup("pnd_Form_Record_Pnd_Tirf_Srce_Cde", "#TIRF-SRCE-CDE", FieldType.STRING, 4);
        pnd_Form_Record_Pnd_Tirf_Participant_Name = pnd_Form_Record.newFieldInGroup("pnd_Form_Record_Pnd_Tirf_Participant_Name", "#TIRF-PARTICIPANT-NAME", 
            FieldType.STRING, 35);
        pnd_Form_Record_Pnd_Tirf_Distribution_Cde = pnd_Form_Record.newFieldInGroup("pnd_Form_Record_Pnd_Tirf_Distribution_Cde", "#TIRF-DISTRIBUTION-CDE", 
            FieldType.STRING, 2);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Accept_Ctr = localVariables.newFieldInRecord("pnd_Accept_Ctr", "#ACCEPT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Total = localVariables.newFieldInRecord("pnd_Total", "#TOTAL", FieldType.PACKED_DECIMAL, 7);
        pnd_Sub_Total = localVariables.newFieldInRecord("pnd_Sub_Total", "#SUB-TOTAL", FieldType.PACKED_DECIMAL, 7);
        pnd_Save_Tax_Year = localVariables.newFieldInRecord("pnd_Save_Tax_Year", "#SAVE-TAX-YEAR", FieldType.STRING, 4);
        pnd_Save_Form_Type = localVariables.newFieldInRecord("pnd_Save_Form_Type", "#SAVE-FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_Save_Create_Date = localVariables.newFieldInRecord("pnd_Save_Create_Date", "#SAVE-CREATE-DATE", FieldType.STRING, 8);

        pnd_Save_Create_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Save_Create_Date__R_Field_2", "REDEFINE", pnd_Save_Create_Date);
        pnd_Save_Create_Date_Pnd_Save_Create_Yyyymm = pnd_Save_Create_Date__R_Field_2.newFieldInGroup("pnd_Save_Create_Date_Pnd_Save_Create_Yyyymm", "#SAVE-CREATE-YYYYMM", 
            FieldType.STRING, 6);
        pnd_Save_Company = localVariables.newFieldInRecord("pnd_Save_Company", "#SAVE-COMPANY", FieldType.STRING, 1);
        pnd_Save_Form_Type_Desc = localVariables.newFieldInRecord("pnd_Save_Form_Type_Desc", "#SAVE-FORM-TYPE-DESC", FieldType.STRING, 10);
        pnd_Variable_Header = localVariables.newFieldInRecord("pnd_Variable_Header", "#VARIABLE-HEADER", FieldType.STRING, 30);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.STRING, 1);
        pnd_Tax_Yr_Header = localVariables.newFieldInRecord("pnd_Tax_Yr_Header", "#TAX-YR-HEADER", FieldType.STRING, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp6114() throws Exception
    {
        super("Twrp6114");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP6114", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133;//Natural: FORMAT ( 02 ) PS = 60 LS = 133
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        RD1:                                                                                                                                                              //Natural: READ WORK FILE 01 RECORD #FORM-RECORD
        while (condition(getWorkFiles().read(1, pnd_Form_Record)))
        {
            if (condition(pnd_Form_Record_Pnd_Tirf_Srce_Cde.equals("ED") || pnd_Form_Record_Pnd_Tirf_Distribution_Cde.equals("D") || pnd_Form_Record_Pnd_Tirf_Distribution_Cde.equals("P")  //Natural: IF #TIRF-SRCE-CDE = 'ED' OR ( #TIRF-DISTRIBUTION-CDE = 'D' OR = 'P' OR = '8' )
                || pnd_Form_Record_Pnd_Tirf_Distribution_Cde.equals("8")))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            //*  FOR REPORT HEADING RM
            if (condition(pnd_First_Time.equals(" ")))                                                                                                                    //Natural: IF #FIRST-TIME = ' '
            {
                pnd_Save_Company.setValue(pnd_Form_Record_Pnd_Company);                                                                                                   //Natural: ASSIGN #SAVE-COMPANY := #COMPANY
                pnd_Save_Tax_Year.setValue(pnd_Form_Record_Pnd_Tirf_Tax_Year);                                                                                            //Natural: ASSIGN #SAVE-TAX-YEAR := #TIRF-TAX-YEAR
                pnd_Tax_Yr_Header.setValue(pnd_Form_Record_Pnd_Tirf_Tax_Year);                                                                                            //Natural: ASSIGN #TAX-YR-HEADER := #TIRF-TAX-YEAR
                pnd_Save_Form_Type.setValue(pnd_Form_Record_Pnd_Tirf_Form_Type);                                                                                          //Natural: ASSIGN #SAVE-FORM-TYPE := #TIRF-FORM-TYPE
                pnd_Save_Create_Date.setValue(pnd_Form_Record_Pnd_Create_Yyyymmdd);                                                                                       //Natural: ASSIGN #SAVE-CREATE-DATE := #CREATE-YYYYMMDD
                                                                                                                                                                          //Natural: PERFORM SELECT-COMPANY-DESCRIPTION
                sub_Select_Company_Description();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM SELECT-FORM-TYPE-DESCRIPTION
                sub_Select_Form_Type_Description();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_First_Time.setValue("N");                                                                                                                             //Natural: ASSIGN #FIRST-TIME := 'N'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Save_Company.equals(pnd_Form_Record_Pnd_Company)))                                                                                          //Natural: IF #SAVE-COMPANY = #COMPANY
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM MONTH-BREAK-PROCESSING
                sub_Month_Break_Processing();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM FORM-TYPE-BREAK-PROCESSING
                sub_Form_Type_Break_Processing();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Save_Company.setValue(pnd_Form_Record_Pnd_Company);                                                                                                   //Natural: ASSIGN #SAVE-COMPANY := #COMPANY
                pnd_Save_Form_Type.setValue(pnd_Form_Record_Pnd_Tirf_Form_Type);                                                                                          //Natural: ASSIGN #SAVE-FORM-TYPE := #TIRF-FORM-TYPE
                pnd_Save_Create_Date.setValue(pnd_Form_Record_Pnd_Create_Yyyymmdd);                                                                                       //Natural: ASSIGN #SAVE-CREATE-DATE := #CREATE-YYYYMMDD
                pnd_Sub_Total.setValue(0);                                                                                                                                //Natural: ASSIGN #SUB-TOTAL := 0
                pnd_Total.setValue(0);                                                                                                                                    //Natural: ASSIGN #TOTAL := 0
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 01 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM SELECT-COMPANY-DESCRIPTION
                sub_Select_Company_Description();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM SELECT-FORM-TYPE-DESCRIPTION
                sub_Select_Form_Type_Description();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Save_Form_Type.equals(pnd_Form_Record_Pnd_Tirf_Form_Type)))                                                                                 //Natural: IF #SAVE-FORM-TYPE = #TIRF-FORM-TYPE
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM MONTH-BREAK-PROCESSING
                sub_Month_Break_Processing();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM FORM-TYPE-BREAK-PROCESSING
                sub_Form_Type_Break_Processing();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Save_Form_Type.setValue(pnd_Form_Record_Pnd_Tirf_Form_Type);                                                                                          //Natural: ASSIGN #SAVE-FORM-TYPE := #TIRF-FORM-TYPE
                pnd_Save_Create_Date.setValue(pnd_Form_Record_Pnd_Create_Yyyymmdd);                                                                                       //Natural: ASSIGN #SAVE-CREATE-DATE := #CREATE-YYYYMMDD
                pnd_Sub_Total.setValue(0);                                                                                                                                //Natural: ASSIGN #SUB-TOTAL := 0
                pnd_Total.setValue(0);                                                                                                                                    //Natural: ASSIGN #TOTAL := 0
                                                                                                                                                                          //Natural: PERFORM SELECT-FORM-TYPE-DESCRIPTION
                sub_Select_Form_Type_Description();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Save_Create_Date_Pnd_Save_Create_Yyyymm.equals(pnd_Form_Record_Pnd_Create_Yyyymm)))                                                         //Natural: IF #SAVE-CREATE-YYYYMM = #CREATE-YYYYMM
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM MONTH-BREAK-PROCESSING
                sub_Month_Break_Processing();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Save_Create_Date.setValue(pnd_Form_Record_Pnd_Create_Yyyymmdd);                                                                                       //Natural: ASSIGN #SAVE-CREATE-DATE := #CREATE-YYYYMMDD
                pnd_Sub_Total.setValue(0);                                                                                                                                //Natural: ASSIGN #SUB-TOTAL := 0
            }                                                                                                                                                             //Natural: END-IF
            //*   (A01)
            //*   (A04)
            //*   (N02)
            //*   (A08)
            //*   (A01)
            //*   (A10)
            //*   (A01)
            //*   (A08)
            //*   (A02)
            //*   (A05)
            //*   (P03)
            //*   (A01)
            //*   (A04)
            //*   (A35)
            getReports().display(2, "Comp/type",                                                                                                                          //Natural: DISPLAY ( 02 ) 'Comp/type' #COMPANY 'Tax/Year' #TIRF-TAX-YEAR 'Form/Type' #TIRF-FORM-TYPE 'Create/Date' #CREATE-YYYYMMDD 'Comp/Code' #TIRF-COMPANY-CDE 'Tin' #TIRF-TIN 'Tin/Type' #TIRF-TAX-ID-TYPE 'Contract' #TIRF-CONTRACT-NBR 'Payee' #TIRF-PAYEE-CDE 'Key' #TIRF-KEY 'Form/Seq' #TIRF-FORM-SEQ 'RPT/IND' #TIRF-PART-RPT-IND 'SRCE/CODE' #TIRF-SRCE-CDE 'Name' #TIRF-PARTICIPANT-NAME
            		pnd_Form_Record_Pnd_Company,"Tax/Year",
            		pnd_Form_Record_Pnd_Tirf_Tax_Year,"Form/Type",
            		pnd_Form_Record_Pnd_Tirf_Form_Type,"Create/Date",
            		pnd_Form_Record_Pnd_Create_Yyyymmdd,"Comp/Code",
            		pnd_Form_Record_Pnd_Tirf_Company_Cde,"Tin",
            		pnd_Form_Record_Pnd_Tirf_Tin,"Tin/Type",
            		pnd_Form_Record_Pnd_Tirf_Tax_Id_Type,"Contract",
            		pnd_Form_Record_Pnd_Tirf_Contract_Nbr,"Payee",
            		pnd_Form_Record_Pnd_Tirf_Payee_Cde,"Key",
            		pnd_Form_Record_Pnd_Tirf_Key,"Form/Seq",
            		pnd_Form_Record_Pnd_Tirf_Form_Seq,"RPT/IND",
            		pnd_Form_Record_Pnd_Tirf_Part_Rpt_Ind,"SRCE/CODE",
            		pnd_Form_Record_Pnd_Tirf_Srce_Cde,"Name",
            		pnd_Form_Record_Pnd_Tirf_Participant_Name);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Sub_Total.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #SUB-TOTAL
            pnd_Total.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #TOTAL
        }                                                                                                                                                                 //Natural: END-WORK
        RD1_Exit:
        if (Global.isEscape()) return;
        //* *------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                                                                                                                                                                          //Natural: PERFORM MONTH-BREAK-PROCESSING
        sub_Month_Break_Processing();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FORM-TYPE-BREAK-PROCESSING
        sub_Form_Type_Break_Processing();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //*  01T 'Total Number Of Form Records Accepted...........'  #ACCEPT-CTR
        //*  01T 'Total Number Of Form Records Accepted...........'  #ACCEPT-CTR
        //* *------------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 01 )
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 02 )
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
        //* *-------                                                                                                                                                      //Natural: ON ERROR
    }
    private void sub_Month_Break_Processing() throws Exception                                                                                                            //Natural: MONTH-BREAK-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------------
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,pnd_Save_Tax_Year,pnd_Save_Form_Type_Desc,pnd_Save_Create_Date_Pnd_Save_Create_Yyyymm,              //Natural: WRITE ( 01 ) NOTITLE NOHDR #SAVE-TAX-YEAR #SAVE-FORM-TYPE-DESC #SAVE-CREATE-YYYYMM ( EM = XXXX/XX ) #SUB-TOTAL
            new ReportEditMask ("XXXX/XX"),pnd_Sub_Total, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Form_Type_Break_Processing() throws Exception                                                                                                        //Natural: FORM-TYPE-BREAK-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------------
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"=",new RepeatItem(34));                                                                            //Natural: WRITE ( 01 ) NOTITLE NOHDR '=' ( 34 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,pnd_Save_Tax_Year,pnd_Save_Form_Type_Desc,"Total  ",pnd_Total, new ReportEditMask                   //Natural: WRITE ( 01 ) NOTITLE NOHDR #SAVE-TAX-YEAR #SAVE-FORM-TYPE-DESC 'Total  ' #TOTAL
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
    }
    private void sub_Select_Form_Type_Description() throws Exception                                                                                                      //Natural: SELECT-FORM-TYPE-DESCRIPTION
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------------------
        if (condition(pnd_Save_Form_Type.equals(1)))                                                                                                                      //Natural: IF #SAVE-FORM-TYPE = 01
        {
            pnd_Save_Form_Type_Desc.setValue("1099-R    ");                                                                                                               //Natural: ASSIGN #SAVE-FORM-TYPE-DESC := '1099-R    '
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Form_Type.equals(2)))                                                                                                                      //Natural: IF #SAVE-FORM-TYPE = 02
        {
            pnd_Save_Form_Type_Desc.setValue("1099-INT  ");                                                                                                               //Natural: ASSIGN #SAVE-FORM-TYPE-DESC := '1099-INT  '
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Form_Type.equals(3)))                                                                                                                      //Natural: IF #SAVE-FORM-TYPE = 03
        {
            pnd_Save_Form_Type_Desc.setValue("1042-S    ");                                                                                                               //Natural: ASSIGN #SAVE-FORM-TYPE-DESC := '1042-S    '
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Form_Type.equals(4)))                                                                                                                      //Natural: IF #SAVE-FORM-TYPE = 04
        {
            pnd_Save_Form_Type_Desc.setValue("5498      ");                                                                                                               //Natural: ASSIGN #SAVE-FORM-TYPE-DESC := '5498      '
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Form_Type.equals(5)))                                                                                                                      //Natural: IF #SAVE-FORM-TYPE = 05
        {
            pnd_Save_Form_Type_Desc.setValue("480.6A    ");                                                                                                               //Natural: ASSIGN #SAVE-FORM-TYPE-DESC := '480.6A    '
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Form_Type.equals(6)))                                                                                                                      //Natural: IF #SAVE-FORM-TYPE = 06
        {
            pnd_Save_Form_Type_Desc.setValue("480.6B    ");                                                                                                               //Natural: ASSIGN #SAVE-FORM-TYPE-DESC := '480.6B    '
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Form_Type.equals(7)))                                                                                                                      //Natural: IF #SAVE-FORM-TYPE = 07
        {
            pnd_Save_Form_Type_Desc.setValue("NR-4      ");                                                                                                               //Natural: ASSIGN #SAVE-FORM-TYPE-DESC := 'NR-4      '
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Select_Company_Description() throws Exception                                                                                                        //Natural: SELECT-COMPANY-DESCRIPTION
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------------
        if (condition(pnd_Save_Company.equals("1")))                                                                                                                      //Natural: IF #SAVE-COMPANY = '1'
        {
            pnd_Variable_Header.setValue("     Personal Annuities     ");                                                                                                 //Natural: ASSIGN #VARIABLE-HEADER := '     Personal Annuities     '
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Company.equals("2")))                                                                                                                      //Natural: IF #SAVE-COMPANY = '2'
        {
            pnd_Variable_Header.setValue("Insurance - Individual Life ");                                                                                                 //Natural: ASSIGN #VARIABLE-HEADER := 'Insurance - Individual Life '
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Company.equals("3")))                                                                                                                      //Natural: IF #SAVE-COMPANY = '3'
        {
            pnd_Variable_Header.setValue("          Pension           ");                                                                                                 //Natural: ASSIGN #VARIABLE-HEADER := '          Pension           '
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Company.equals("4")))                                                                                                                      //Natural: IF #SAVE-COMPANY = '4'
        {
            pnd_Variable_Header.setValue("         Group Life         ");                                                                                                 //Natural: ASSIGN #VARIABLE-HEADER := '         Group Life         '
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Company.equals("5")))                                                                                                                      //Natural: IF #SAVE-COMPANY = '5'
        {
            pnd_Variable_Header.setValue("       Long Term Care       ");                                                                                                 //Natural: ASSIGN #VARIABLE-HEADER := '       Long Term Care       '
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        getReports().skip(0, 4);                                                                                                                                          //Natural: SKIP ( 00 ) 4
        getReports().write(0, new TabSetting(1),"Total Number Of Form Records Found..............",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                       //Natural: WRITE ( 00 ) 01T 'Total Number Of Form Records Found..............' #READ-CTR
        if (Global.isEscape()) return;
        getReports().skip(1, 4);                                                                                                                                          //Natural: SKIP ( 01 ) 4
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"Total Number Of Forms Found.....................",pnd_Read_Ctr, new ReportEditMask                  //Natural: WRITE ( 01 ) 01T 'Total Number Of Forms Found.....................' #READ-CTR
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(2, 4);                                                                                                                                          //Natural: SKIP ( 02 ) 4
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"Total Number Of Forms Found.....................",pnd_Read_Ctr, new ReportEditMask                  //Natural: WRITE ( 02 ) 01T 'Total Number Of Forms Found.....................' #READ-CTR
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  01T 'Total Number Of Form Records Accepted...........'  #ACCEPT-CTR
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(47),"  Tax Withholding & Reporting System  ",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 47T '  Tax Withholding & Reporting System  ' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 27T 'Tax Year' #TAX-YR-HEADER 46T 'Post Mass Mailing Deferrals Forms Summary' 95T 'Tax Year' #TAX-YR-HEADER 120T 'REPORT: RPT1' / 50T #VARIABLE-HEADER // 'Tax ' 06T ' Form' 17T ' Year ' 28T ' Forms' / 'Year' 06T ' Type' 17T ' Month' 28T ' Total' / '====' 06T '==========' 17T '=======' 25T '==========' //
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                        TabSetting(27),"Tax Year",pnd_Tax_Yr_Header,new TabSetting(46),"Post Mass Mailing Deferrals Forms Summary",new TabSetting(95),"Tax Year",pnd_Tax_Yr_Header,new 
                        TabSetting(120),"REPORT: RPT1",NEWLINE,new TabSetting(50),pnd_Variable_Header,NEWLINE,NEWLINE,"Tax ",new TabSetting(6)," Form",new 
                        TabSetting(17)," Year ",new TabSetting(28)," Forms",NEWLINE,"Year",new TabSetting(6)," Type",new TabSetting(17)," Month",new TabSetting(28)," Total",NEWLINE,"====",new 
                        TabSetting(6),"==========",new TabSetting(17),"=======",new TabSetting(25),"==========",NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(2, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(47)," Tax Withholding & Reporting System  ",new  //Natural: WRITE ( 02 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 47T ' Tax Withholding & Reporting System  ' 120T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 27T 'Tax Year' #TAX-YR-HEADER 47T 'Post Mass Mailing Deferrals Forms Detail' 95T 'Tax Year' #TAX-YR-HEADER 120T 'REPORT: RPT2' //
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                        TabSetting(27),"Tax Year",pnd_Tax_Yr_Header,new TabSetting(47),"Post Mass Mailing Deferrals Forms Detail",new TabSetting(95),"Tax Year",pnd_Tax_Yr_Header,new 
                        TabSetting(120),"REPORT: RPT2",NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
        Global.format(2, "PS=60 LS=133");

        getReports().setDisplayColumns(2, "Comp/type",
        		pnd_Form_Record_Pnd_Company,"Tax/Year",
        		pnd_Form_Record_Pnd_Tirf_Tax_Year,"Form/Type",
        		pnd_Form_Record_Pnd_Tirf_Form_Type,"Create/Date",
        		pnd_Form_Record_Pnd_Create_Yyyymmdd,"Comp/Code",
        		pnd_Form_Record_Pnd_Tirf_Company_Cde,"Tin",
        		pnd_Form_Record_Pnd_Tirf_Tin,"Tin/Type",
        		pnd_Form_Record_Pnd_Tirf_Tax_Id_Type,"Contract",
        		pnd_Form_Record_Pnd_Tirf_Contract_Nbr,"Payee",
        		pnd_Form_Record_Pnd_Tirf_Payee_Cde,"Key",
        		pnd_Form_Record_Pnd_Tirf_Key,"Form/Seq",
        		pnd_Form_Record_Pnd_Tirf_Form_Seq,"RPT/IND",
        		pnd_Form_Record_Pnd_Tirf_Part_Rpt_Ind,"SRCE/CODE",
        		pnd_Form_Record_Pnd_Tirf_Srce_Cde,"Name",
        		pnd_Form_Record_Pnd_Tirf_Participant_Name);
    }
}
