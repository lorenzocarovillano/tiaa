/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:40:56 PM
**        * FROM NATURAL PROGRAM : Twrp5504
************************************************************
**        * FILE NAME            : Twrp5504.java
**        * CLASS NAME           : Twrp5504
**        * INSTANCE NAME        : Twrp5504
************************************************************
************************************************************************
** PROGRAM : TWRP5504
** SYSTEM  : TAXWARS
** AUTHOR  : MICHAEL SUPONITSKY
** FUNCTION: RECONCILIATION REPORT FOR NR4.
** HISTORY.....:
** 07/15/2009 - W2 FORM AUTOMATION PROJECT             R. MA
**              PUT IN COMMENTS AND TAG WITH '*W2' WHERE CHANGES ARE
**              NECESSARY.
**              IN THIS PROGRAM, THE W2 DATA HAD BEEN IDENTIFIED AND
**              BLOCKED FOR NR4 REPORTING.
** 12/06/2005 - BK. TAX YEAR ADDED TO TWRNCOMP PARMS
** 11/12/2004 - M. SUPONITSKY - TOPS - TRUST & TIAA - >= 2004.
** 11/20/2014   O SOTTO FATCA CHANGES MARKED 11/2014.
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5504 extends BLNatBase
{
    // Data Areas
    private PdaTwracomp pdaTwracomp;
    private LdaTwrl0600 ldaTwrl0600;
    private LdaTwrl0900 ldaTwrl0900;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Pnd_Cntl_Max;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_Company_Line;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Ws_Pnd_S1;
    private DbsField pnd_Ws_Pnd_S2;
    private DbsField pnd_Ws_Pnd_Tran_Cv;
    private DbsField pnd_Ws_Pnd_Gross_Cv;
    private DbsField pnd_Ws_Pnd_Gross_Tax_Cv;
    private DbsField pnd_Ws_Pnd_Can_Cv;
    private DbsField pnd_Ws_Pnd_Fed_Cv;
    private DbsField pnd_Ws_Pnd_Nra_Cv;

    private DbsGroup pnd_Cntl;
    private DbsField pnd_Cntl_Pnd_Cntl_Text;
    private DbsField pnd_Cntl_Pnd_Tran_Cnt_Cv;
    private DbsField pnd_Cntl_Pnd_Gross_Amt_Cv;
    private DbsField pnd_Cntl_Pnd_Gross_Tax_Amt_Cv;
    private DbsField pnd_Cntl_Pnd_Can_Tax_Cv;
    private DbsField pnd_Cntl_Pnd_Fed_Tax_Cv;
    private DbsField pnd_Cntl_Pnd_Nra_Tax_Cv;

    private DbsGroup pnd_Cntl_Pnd_Totals;
    private DbsField pnd_Cntl_Pnd_Tran_Cnt;
    private DbsField pnd_Cntl_Pnd_Gross_Amt;
    private DbsField pnd_Cntl_Pnd_Gross_Tax;
    private DbsField pnd_Cntl_Pnd_Can_Tax;
    private DbsField pnd_Cntl_Pnd_Fed_Tax;
    private DbsField pnd_Cntl_Pnd_Nra_Tax;
    private DbsField pnd_Cntl_Pnd_Tran_Cnt_F;
    private DbsField pnd_Cntl_Pnd_Gross_Amt_F;
    private DbsField pnd_Cntl_Pnd_Gross_Tax_F;
    private DbsField pnd_Cntl_Pnd_Can_Tax_F;
    private DbsField pnd_Cntl_Pnd_Fed_Tax_F;
    private DbsField pnd_Cntl_Pnd_Nra_Tax_F;

    private DbsGroup pnd_Cmpy_Totals;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Gross_Amt;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Gross_Tax;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Can_Tax;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Fed_Tax;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Nra_Tax;

    private DbsGroup pnd_Grnd_Totals;
    private DbsField pnd_Grnd_Totals_Pnd_Grd_Gross_Amt;
    private DbsField pnd_Grnd_Totals_Pnd_Grd_Gross_Tax;
    private DbsField pnd_Grnd_Totals_Pnd_Grd_Can_Tax;
    private DbsField pnd_Grnd_Totals_Pnd_Grd_Fed_Tax;
    private DbsField pnd_Grnd_Totals_Pnd_Grd_Nra_Tax;
    private DbsField pnd_Dashes;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Twrpymnt_Company_Cde_FormOld;
    private DbsField readWork01Twrpymnt_Tax_YearOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwracomp = new PdaTwracomp(localVariables);
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);
        ldaTwrl0900 = new LdaTwrl0900();
        registerRecord(ldaTwrl0900);

        // Local Variables

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Pnd_Cntl_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_Cntl_Max", "#CNTL-MAX", FieldType.PACKED_DECIMAL, 3);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Tax_Year = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Company_Line = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Line", "#COMPANY-LINE", FieldType.STRING, 72);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S1", "#S1", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S2", "#S2", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Tran_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tran_Cv", "#TRAN-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Gross_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Gross_Cv", "#GROSS-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Gross_Tax_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Gross_Tax_Cv", "#GROSS-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Can_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Can_Cv", "#CAN-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Fed_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Fed_Cv", "#FED-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Nra_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Nra_Cv", "#NRA-CV", FieldType.ATTRIBUTE_CONTROL, 2);

        pnd_Cntl = localVariables.newGroupArrayInRecord("pnd_Cntl", "#CNTL", new DbsArrayController(1, 6));
        pnd_Cntl_Pnd_Cntl_Text = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Cntl_Text", "#CNTL-TEXT", FieldType.STRING, 17);
        pnd_Cntl_Pnd_Tran_Cnt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Tran_Cnt_Cv", "#TRAN-CNT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Gross_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Gross_Amt_Cv", "#GROSS-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Gross_Tax_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Gross_Tax_Amt_Cv", "#GROSS-TAX-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Can_Tax_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Can_Tax_Cv", "#CAN-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Fed_Tax_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Fed_Tax_Cv", "#FED-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Nra_Tax_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Nra_Tax_Cv", "#NRA-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 2);

        pnd_Cntl_Pnd_Totals = pnd_Cntl.newGroupArrayInGroup("pnd_Cntl_Pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Cntl_Pnd_Tran_Cnt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Tran_Cnt", "#TRAN-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Cntl_Pnd_Gross_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Cntl_Pnd_Gross_Tax = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Gross_Tax", "#GROSS-TAX", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Cntl_Pnd_Can_Tax = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Can_Tax", "#CAN-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Fed_Tax = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Fed_Tax", "#FED-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Nra_Tax = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Nra_Tax", "#NRA-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Tran_Cnt_F = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Tran_Cnt_F", "#TRAN-CNT-F", FieldType.PACKED_DECIMAL, 9);
        pnd_Cntl_Pnd_Gross_Amt_F = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Gross_Amt_F", "#GROSS-AMT-F", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Cntl_Pnd_Gross_Tax_F = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Gross_Tax_F", "#GROSS-TAX-F", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Cntl_Pnd_Can_Tax_F = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Can_Tax_F", "#CAN-TAX-F", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Fed_Tax_F = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Fed_Tax_F", "#FED-TAX-F", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Nra_Tax_F = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Nra_Tax_F", "#NRA-TAX-F", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Cmpy_Totals = localVariables.newGroupInRecord("pnd_Cmpy_Totals", "#CMPY-TOTALS");
        pnd_Cmpy_Totals_Pnd_Tot_Gross_Amt = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Gross_Amt", "#TOT-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Cmpy_Totals_Pnd_Tot_Gross_Tax = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Gross_Tax", "#TOT-GROSS-TAX", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Cmpy_Totals_Pnd_Tot_Can_Tax = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Can_Tax", "#TOT-CAN-TAX", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Cmpy_Totals_Pnd_Tot_Fed_Tax = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Fed_Tax", "#TOT-FED-TAX", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Cmpy_Totals_Pnd_Tot_Nra_Tax = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Nra_Tax", "#TOT-NRA-TAX", FieldType.PACKED_DECIMAL, 
            11, 2);

        pnd_Grnd_Totals = localVariables.newGroupInRecord("pnd_Grnd_Totals", "#GRND-TOTALS");
        pnd_Grnd_Totals_Pnd_Grd_Gross_Amt = pnd_Grnd_Totals.newFieldInGroup("pnd_Grnd_Totals_Pnd_Grd_Gross_Amt", "#GRD-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Grnd_Totals_Pnd_Grd_Gross_Tax = pnd_Grnd_Totals.newFieldInGroup("pnd_Grnd_Totals_Pnd_Grd_Gross_Tax", "#GRD-GROSS-TAX", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Grnd_Totals_Pnd_Grd_Can_Tax = pnd_Grnd_Totals.newFieldInGroup("pnd_Grnd_Totals_Pnd_Grd_Can_Tax", "#GRD-CAN-TAX", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Grnd_Totals_Pnd_Grd_Fed_Tax = pnd_Grnd_Totals.newFieldInGroup("pnd_Grnd_Totals_Pnd_Grd_Fed_Tax", "#GRD-FED-TAX", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Grnd_Totals_Pnd_Grd_Nra_Tax = pnd_Grnd_Totals.newFieldInGroup("pnd_Grnd_Totals_Pnd_Grd_Nra_Tax", "#GRD-NRA-TAX", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Dashes = localVariables.newFieldInRecord("pnd_Dashes", "#DASHES", FieldType.STRING, 125);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Twrpymnt_Company_Cde_FormOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrpymnt_Company_Cde_Form_OLD", "Twrpymnt_Company_Cde_Form_OLD", 
            FieldType.STRING, 1);
        readWork01Twrpymnt_Tax_YearOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrpymnt_Tax_Year_OLD", "Twrpymnt_Tax_Year_OLD", FieldType.NUMERIC, 
            4);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaTwrl0600.initializeValues();
        ldaTwrl0900.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Pnd_Cntl_Max.setInitialValue(6);
        pnd_Cntl_Pnd_Cntl_Text.getValue(1).setInitialValue("All Payments");
        pnd_Cntl_Pnd_Cntl_Text.getValue(2).setInitialValue("Active Payments");
        pnd_Cntl_Pnd_Cntl_Text.getValue(3).setInitialValue("Periodic Payments");
        pnd_Cntl_Pnd_Cntl_Text.getValue(4).setInitialValue("Lump Sum / RTB");
        pnd_Cntl_Pnd_Cntl_Text.getValue(5).setInitialValue("Interest");
        pnd_Cntl_Pnd_Cntl_Text.getValue(6).setInitialValue("Form Total");
        pnd_Cntl_Pnd_Tran_Cnt_Cv.setInitialAttributeValue("AD=I)#CNTL.#TRAN-CNT-CV(2)	(AD=I)#CNTL.#TRAN-CNT-CV(3)	(AD=N)#CNTL.#TRAN-CNT-CV(4)	(AD=N)#CNTL.#TRAN-CNT-CV(5)	(AD=N)#CNTL.#TRAN-CNT-CV(6)	(AD=N");
        pnd_Cntl_Pnd_Gross_Amt_Cv.setInitialAttributeValue("AD=I)#CNTL.#GROSS-AMT-CV(2)	(AD=I)#CNTL.#GROSS-AMT-CV(3)	(AD=I)#CNTL.#GROSS-AMT-CV(4)	(AD=I)#CNTL.#GROSS-AMT-CV(5)	(AD=I)#CNTL.#GROSS-AMT-CV(6)	(AD=I");
        pnd_Cntl_Pnd_Gross_Tax_Amt_Cv.setInitialAttributeValue("AD=I)#CNTL.#GROSS-TAX-AMT-CV(2)	(AD=I)#CNTL.#GROSS-TAX-AMT-CV(3)	(AD=I)#CNTL.#GROSS-TAX-AMT-CV(4)	(AD=I)#CNTL.#GROSS-TAX-AMT-CV(5)	(AD=N)#CNTL.#GROSS-TAX-AMT-CV(6)	(AD=I");
        pnd_Cntl_Pnd_Can_Tax_Cv.setInitialAttributeValue("AD=I)#CNTL.#CAN-TAX-CV(2)	(AD=I)#CNTL.#CAN-TAX-CV(3)	(AD=I)#CNTL.#CAN-TAX-CV(4)	(AD=I)#CNTL.#CAN-TAX-CV(5)	(AD=N)#CNTL.#CAN-TAX-CV(6)	(AD=I");
        pnd_Cntl_Pnd_Fed_Tax_Cv.setInitialAttributeValue("AD=I)#CNTL.#FED-TAX-CV(2)	(AD=I)#CNTL.#FED-TAX-CV(3)	(AD=I)#CNTL.#FED-TAX-CV(4)	(AD=I)#CNTL.#FED-TAX-CV(5)	(AD=N)#CNTL.#FED-TAX-CV(6)	(AD=I");
        pnd_Cntl_Pnd_Nra_Tax_Cv.setInitialAttributeValue("AD=I)#CNTL.#NRA-TAX-CV(2)	(AD=I)#CNTL.#NRA-TAX-CV(3)	(AD=I)#CNTL.#NRA-TAX-CV(4)	(AD=I)#CNTL.#NRA-TAX-CV(5)	(AD=N)#CNTL.#NRA-TAX-CV(6)	(AD=I");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5504() throws Exception
    {
        super("Twrp5504");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 133 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  11/2014
        pnd_Dashes.moveAll("=");                                                                                                                                          //Natural: MOVE ALL '=' TO #DASHES
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 50T 'Summary of Canadian Transactions' 120T 'Report: RPT1' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / #WS.#COMPANY-LINE //
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
                                                                                                                                                                          //Natural: PERFORM PROCESS-CONTROL-RECORD
        sub_Process_Control_Record();
        if (condition(Global.isEscape())) {return;}
        short decideConditionsMet227 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #TWRP0600-TAX-YEAR-CCYY;//Natural: VALUE '2004' : '9999'
        if (condition((ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy().equals("2004' : '9999"))))
        {
            decideConditionsMet227++;
            ignore();
        }                                                                                                                                                                 //Natural: VALUE '2003'
        else if (condition((ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy().equals("2003"))))
        {
            decideConditionsMet227++;
            //*  <= 2002
            Global.setFetchProgram(DbsUtil.getBlType("TWRP5524"));                                                                                                        //Natural: FETCH 'TWRP5524'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            Global.setFetchProgram(DbsUtil.getBlType("TWRP5514"));                                                                                                        //Natural: FETCH 'TWRP5514'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* *READ WORK FILE 1 RECORD #XTAXYR-F94  /* 11/2014
        //*  11/2014
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #XTAXYR-F94
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaTwrl0900.getPnd_Xtaxyr_F94())))
        {
            CheckAtStartofData240();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*  PP                                                                                                                                                       //Natural: AT START OF DATA;//Natural: AT BREAK OF #XTAXYR-F94.TWRPYMNT-COMPANY-CDE-FORM
            if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Paymt_Category().equals("1")))                                                                           //Natural: IF #XTAXYR-F94.TWRPYMNT-PAYMT-CATEGORY = '1'
            {
                pnd_Ws_Pnd_J.setValue(3);                                                                                                                                 //Natural: ASSIGN #WS.#J := 3
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_J.setValue(4);                                                                                                                                 //Natural: ASSIGN #WS.#J := 4
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO #XTAXYR-F94.#C-TWRPYMNT-PAYMENTS
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments())); pnd_Ws_Pnd_I.nadd(1))
            {
                //*  *W2                                EXCLUDE W2 DATA    07/15/2009  RM
                if (condition(((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("7") && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payment_Type().getValue(pnd_Ws_Pnd_I).equals("M"))  //Natural: IF #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE = '7' AND #XTAXYR-F94.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'M' AND ( #XTAXYR-F94.TWRPYMNT-SETTLE-TYPE ( #I ) = 'S' OR = 'T' OR = 'E' OR = 'U' OR = 'M' )
                    && ((((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("S") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("T")) 
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("E")) || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("U")) 
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("M")))))
                {
                    //*                                           OR = 'F'
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*                                                     ALL PAYMENTS
                //*  11/2014 START
                if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fatca_Ind().getValue(1).equals("Y")))                                                                //Natural: IF #XTAXYR-F94.TWRPYMNT-FATCA-IND ( 1 ) = 'Y'
                {
                    pnd_Cntl_Pnd_Tran_Cnt_F.getValue(1,1).nadd(1);                                                                                                        //Natural: ADD 1 TO #CNTL.#TRAN-CNT-F ( 1,1 )
                    pnd_Cntl_Pnd_Gross_Amt_F.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                               //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT-F ( 1,1 )
                    if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Can_Wthld_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00"))))                       //Natural: IF #XTAXYR-F94.TWRPYMNT-CAN-WTHLD-AMT ( #I ) NE 0.00
                    {
                        pnd_Cntl_Pnd_Gross_Tax_F.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                           //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-TAX-F ( 1,1 )
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Cntl_Pnd_Can_Tax_F.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Can_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                             //Natural: ADD #XTAXYR-F94.TWRPYMNT-CAN-WTHLD-AMT ( #I ) TO #CNTL.#CAN-TAX-F ( 1,1 )
                    pnd_Cntl_Pnd_Fed_Tax_F.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                             //Natural: ADD #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #CNTL.#FED-TAX-F ( 1,1 )
                    pnd_Cntl_Pnd_Nra_Tax_F.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Nra_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                             //Natural: ADD #XTAXYR-F94.TWRPYMNT-NRA-WTHLD-AMT ( #I ) TO #CNTL.#NRA-TAX-F ( 1,1 )
                    pnd_Cntl_Pnd_Gross_Amt_F.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#GROSS-AMT-F ( 1,1 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cntl_Pnd_Tran_Cnt.getValue(1,1).nadd(1);                                                                                                          //Natural: ADD 1 TO #CNTL.#TRAN-CNT ( 1,1 )
                    pnd_Cntl_Pnd_Gross_Amt.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 1,1 )
                    if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Can_Wthld_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00"))))                       //Natural: IF #XTAXYR-F94.TWRPYMNT-CAN-WTHLD-AMT ( #I ) NE 0.00
                    {
                        pnd_Cntl_Pnd_Gross_Tax.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                             //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-TAX ( 1,1 )
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Cntl_Pnd_Can_Tax.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Can_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                               //Natural: ADD #XTAXYR-F94.TWRPYMNT-CAN-WTHLD-AMT ( #I ) TO #CNTL.#CAN-TAX ( 1,1 )
                    pnd_Cntl_Pnd_Fed_Tax.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                               //Natural: ADD #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #CNTL.#FED-TAX ( 1,1 )
                    pnd_Cntl_Pnd_Nra_Tax.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Nra_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                               //Natural: ADD #XTAXYR-F94.TWRPYMNT-NRA-WTHLD-AMT ( #I ) TO #CNTL.#NRA-TAX ( 1,1 )
                    pnd_Cntl_Pnd_Gross_Amt.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                   //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 1,1 )
                }                                                                                                                                                         //Natural: END-IF
                //*  11/2014 END
                if (condition(! (ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals(" ") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals("C")))) //Natural: IF NOT #XTAXYR-F94.TWRPYMNT-PYMNT-STATUS ( #I ) = ' ' OR = 'C'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*                                                   ACTIVE PAYMENTS
                //*  11/2014 START
                if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fatca_Ind().getValue(1).equals("Y")))                                                                //Natural: IF #XTAXYR-F94.TWRPYMNT-FATCA-IND ( 1 ) = 'Y'
                {
                    pnd_Cntl_Pnd_Tran_Cnt_F.getValue(2,1).nadd(1);                                                                                                        //Natural: ADD 1 TO #CNTL.#TRAN-CNT-F ( 2,1 )
                    pnd_Cntl_Pnd_Gross_Amt_F.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                               //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT-F ( 2,1 )
                    if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Can_Wthld_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00"))))                       //Natural: IF #XTAXYR-F94.TWRPYMNT-CAN-WTHLD-AMT ( #I ) NE 0.00
                    {
                        pnd_Cntl_Pnd_Gross_Tax_F.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                           //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-TAX-F ( 2,1 )
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Cntl_Pnd_Can_Tax_F.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Can_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                             //Natural: ADD #XTAXYR-F94.TWRPYMNT-CAN-WTHLD-AMT ( #I ) TO #CNTL.#CAN-TAX-F ( 2,1 )
                    pnd_Cntl_Pnd_Fed_Tax_F.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                             //Natural: ADD #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #CNTL.#FED-TAX-F ( 2,1 )
                    pnd_Cntl_Pnd_Nra_Tax_F.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Nra_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                             //Natural: ADD #XTAXYR-F94.TWRPYMNT-NRA-WTHLD-AMT ( #I ) TO #CNTL.#NRA-TAX-F ( 2,1 )
                    pnd_Cntl_Pnd_Gross_Amt_F.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#GROSS-AMT-F ( 2,1 )
                    pnd_Cntl_Pnd_Gross_Amt_F.getValue(pnd_Ws_Pnd_J,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                    //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT-F ( #J,1 )
                    if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Can_Wthld_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00"))))                       //Natural: IF #XTAXYR-F94.TWRPYMNT-CAN-WTHLD-AMT ( #I ) NE 0.00
                    {
                        pnd_Cntl_Pnd_Gross_Tax_F.getValue(pnd_Ws_Pnd_J,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-TAX-F ( #J,1 )
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Cntl_Pnd_Can_Tax_F.getValue(pnd_Ws_Pnd_J,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Can_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                  //Natural: ADD #XTAXYR-F94.TWRPYMNT-CAN-WTHLD-AMT ( #I ) TO #CNTL.#CAN-TAX-F ( #J,1 )
                    pnd_Cntl_Pnd_Fed_Tax_F.getValue(pnd_Ws_Pnd_J,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                  //Natural: ADD #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #CNTL.#FED-TAX-F ( #J,1 )
                    pnd_Cntl_Pnd_Nra_Tax_F.getValue(pnd_Ws_Pnd_J,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Nra_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                  //Natural: ADD #XTAXYR-F94.TWRPYMNT-NRA-WTHLD-AMT ( #I ) TO #CNTL.#NRA-TAX-F ( #J,1 )
                    pnd_Cntl_Pnd_Gross_Amt_F.getValue(5,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#GROSS-AMT-F ( 5,1 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cntl_Pnd_Tran_Cnt.getValue(2,1).nadd(1);                                                                                                          //Natural: ADD 1 TO #CNTL.#TRAN-CNT ( 2,1 )
                    pnd_Cntl_Pnd_Gross_Amt.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 2,1 )
                    if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Can_Wthld_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00"))))                       //Natural: IF #XTAXYR-F94.TWRPYMNT-CAN-WTHLD-AMT ( #I ) NE 0.00
                    {
                        pnd_Cntl_Pnd_Gross_Tax.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                             //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-TAX ( 2,1 )
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Cntl_Pnd_Can_Tax.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Can_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                               //Natural: ADD #XTAXYR-F94.TWRPYMNT-CAN-WTHLD-AMT ( #I ) TO #CNTL.#CAN-TAX ( 2,1 )
                    pnd_Cntl_Pnd_Fed_Tax.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                               //Natural: ADD #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #CNTL.#FED-TAX ( 2,1 )
                    pnd_Cntl_Pnd_Nra_Tax.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Nra_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                               //Natural: ADD #XTAXYR-F94.TWRPYMNT-NRA-WTHLD-AMT ( #I ) TO #CNTL.#NRA-TAX ( 2,1 )
                    pnd_Cntl_Pnd_Gross_Amt.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                   //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 2,1 )
                    pnd_Cntl_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_J,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                      //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT ( #J,1 )
                    if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Can_Wthld_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00"))))                       //Natural: IF #XTAXYR-F94.TWRPYMNT-CAN-WTHLD-AMT ( #I ) NE 0.00
                    {
                        pnd_Cntl_Pnd_Gross_Tax.getValue(pnd_Ws_Pnd_J,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                  //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-TAX ( #J,1 )
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Cntl_Pnd_Can_Tax.getValue(pnd_Ws_Pnd_J,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Can_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                    //Natural: ADD #XTAXYR-F94.TWRPYMNT-CAN-WTHLD-AMT ( #I ) TO #CNTL.#CAN-TAX ( #J,1 )
                    pnd_Cntl_Pnd_Fed_Tax.getValue(pnd_Ws_Pnd_J,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                    //Natural: ADD #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #CNTL.#FED-TAX ( #J,1 )
                    pnd_Cntl_Pnd_Nra_Tax.getValue(pnd_Ws_Pnd_J,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Nra_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                    //Natural: ADD #XTAXYR-F94.TWRPYMNT-NRA-WTHLD-AMT ( #I ) TO #CNTL.#NRA-TAX ( #J,1 )
                    pnd_Cntl_Pnd_Gross_Amt.getValue(5,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                   //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 5,1 )
                }                                                                                                                                                         //Natural: END-IF
                //*  11/2014 END
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            readWork01Twrpymnt_Company_Cde_FormOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form());                                                   //Natural: END-WORK
            readWork01Twrpymnt_Tax_YearOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Company_Line.setValue("Grand Totals");                                                                                                                 //Natural: ASSIGN #WS.#COMPANY-LINE := 'Grand Totals'
        pnd_Ws_Pnd_S2.setValue(2);                                                                                                                                        //Natural: ASSIGN #WS.#S2 := 2
                                                                                                                                                                          //Natural: PERFORM WRITE-CANADIAN-REPORT
        sub_Write_Canadian_Report();
        if (condition(Global.isEscape())) {return;}
        //* **********************
        //*  S U B R O U T I N E S
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CANADIAN-REPORT
        //* **************************************
        //*    3X '//NRA Tax'       #CNTL.#NRA-TAX         (#S1,#S2)(CV=#NRA-CV)
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COMPANY
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CONTROL-RECORD
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Write_Canadian_Report() throws Exception                                                                                                             //Natural: WRITE-CANADIAN-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Cntl_Pnd_Gross_Amt.getValue(6,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Gross_Amt.getValue(6,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Gross_Amt.getValue(3,":",5,pnd_Ws_Pnd_S2).add(new  //Natural: ASSIGN #CNTL.#GROSS-AMT ( 6,#S2 ) := #CNTL.#GROSS-AMT ( 3:5,#S2 ) + 0.00
            DbsDecimal("0.00")));
        pnd_Cntl_Pnd_Gross_Tax.getValue(6,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Gross_Tax.getValue(6,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Gross_Tax.getValue(3,":",5,pnd_Ws_Pnd_S2).add(new  //Natural: ASSIGN #CNTL.#GROSS-TAX ( 6,#S2 ) := #CNTL.#GROSS-TAX ( 3:5,#S2 ) + 0.00
            DbsDecimal("0.00")));
        pnd_Cntl_Pnd_Can_Tax.getValue(6,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Can_Tax.getValue(6,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Can_Tax.getValue(3,":",5,pnd_Ws_Pnd_S2).add(new  //Natural: ASSIGN #CNTL.#CAN-TAX ( 6,#S2 ) := #CNTL.#CAN-TAX ( 3:5,#S2 ) + 0.00
            DbsDecimal("0.00")));
        pnd_Cntl_Pnd_Fed_Tax.getValue(6,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Fed_Tax.getValue(6,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Fed_Tax.getValue(3,":",5,pnd_Ws_Pnd_S2).add(new  //Natural: ASSIGN #CNTL.#FED-TAX ( 6,#S2 ) := #CNTL.#FED-TAX ( 3:5,#S2 ) + 0.00
            DbsDecimal("0.00")));
        pnd_Cntl_Pnd_Nra_Tax.getValue(6,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Nra_Tax.getValue(6,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Nra_Tax.getValue(3,":",5,pnd_Ws_Pnd_S2).add(new  //Natural: ASSIGN #CNTL.#NRA-TAX ( 6,#S2 ) := #CNTL.#NRA-TAX ( 3:5,#S2 ) + 0.00
            DbsDecimal("0.00")));
        pnd_Cntl_Pnd_Gross_Amt_F.getValue(6,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Gross_Amt_F.getValue(6,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Gross_Amt_F.getValue(3,":",5,pnd_Ws_Pnd_S2).add(new  //Natural: ASSIGN #CNTL.#GROSS-AMT-F ( 6,#S2 ) := #CNTL.#GROSS-AMT-F ( 3:5,#S2 ) + 0.00
            DbsDecimal("0.00")));
        pnd_Cntl_Pnd_Gross_Tax_F.getValue(6,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Gross_Tax_F.getValue(6,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Gross_Tax_F.getValue(3,":",5,pnd_Ws_Pnd_S2).add(new  //Natural: ASSIGN #CNTL.#GROSS-TAX-F ( 6,#S2 ) := #CNTL.#GROSS-TAX-F ( 3:5,#S2 ) + 0.00
            DbsDecimal("0.00")));
        pnd_Cntl_Pnd_Can_Tax_F.getValue(6,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Can_Tax_F.getValue(6,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Can_Tax_F.getValue(3,":",5,pnd_Ws_Pnd_S2).add(new  //Natural: ASSIGN #CNTL.#CAN-TAX-F ( 6,#S2 ) := #CNTL.#CAN-TAX-F ( 3:5,#S2 ) + 0.00
            DbsDecimal("0.00")));
        pnd_Cntl_Pnd_Fed_Tax_F.getValue(6,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Fed_Tax_F.getValue(6,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Fed_Tax_F.getValue(3,":",5,pnd_Ws_Pnd_S2).add(new  //Natural: ASSIGN #CNTL.#FED-TAX-F ( 6,#S2 ) := #CNTL.#FED-TAX-F ( 3:5,#S2 ) + 0.00
            DbsDecimal("0.00")));
        pnd_Cntl_Pnd_Nra_Tax_F.getValue(6,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Nra_Tax_F.getValue(6,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Nra_Tax_F.getValue(3,":",5,pnd_Ws_Pnd_S2).add(new  //Natural: ASSIGN #CNTL.#NRA-TAX-F ( 6,#S2 ) := #CNTL.#NRA-TAX-F ( 3:5,#S2 ) + 0.00
            DbsDecimal("0.00")));
        //*  COMPANY BREAK
        if (condition(pnd_Ws_Pnd_S2.equals(1)))                                                                                                                           //Natural: IF #S2 = 1
        {
            pnd_Cmpy_Totals_Pnd_Tot_Gross_Amt.compute(new ComputeParameters(false, pnd_Cmpy_Totals_Pnd_Tot_Gross_Amt), pnd_Cntl_Pnd_Gross_Amt.getValue(6,                 //Natural: ASSIGN #TOT-GROSS-AMT := #CNTL.#GROSS-AMT ( 6,#S2 ) + #CNTL.#GROSS-AMT-F ( 6,#S2 )
                pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Gross_Amt_F.getValue(6,pnd_Ws_Pnd_S2)));
            pnd_Cmpy_Totals_Pnd_Tot_Gross_Tax.compute(new ComputeParameters(false, pnd_Cmpy_Totals_Pnd_Tot_Gross_Tax), pnd_Cntl_Pnd_Gross_Tax.getValue(6,                 //Natural: ASSIGN #TOT-GROSS-TAX := #CNTL.#GROSS-TAX ( 6,#S2 ) + #CNTL.#GROSS-TAX-F ( 6,#S2 )
                pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Gross_Tax_F.getValue(6,pnd_Ws_Pnd_S2)));
            pnd_Cmpy_Totals_Pnd_Tot_Can_Tax.compute(new ComputeParameters(false, pnd_Cmpy_Totals_Pnd_Tot_Can_Tax), pnd_Cntl_Pnd_Can_Tax.getValue(6,pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Can_Tax_F.getValue(6, //Natural: ASSIGN #TOT-CAN-TAX := #CNTL.#CAN-TAX ( 6,#S2 ) + #CNTL.#CAN-TAX-F ( 6,#S2 )
                pnd_Ws_Pnd_S2)));
            pnd_Cmpy_Totals_Pnd_Tot_Fed_Tax.compute(new ComputeParameters(false, pnd_Cmpy_Totals_Pnd_Tot_Fed_Tax), pnd_Cntl_Pnd_Fed_Tax.getValue(6,pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Fed_Tax_F.getValue(6, //Natural: ASSIGN #TOT-FED-TAX := #CNTL.#FED-TAX ( 6,#S2 ) + #CNTL.#FED-TAX-F ( 6,#S2 )
                pnd_Ws_Pnd_S2)));
            pnd_Cmpy_Totals_Pnd_Tot_Nra_Tax.compute(new ComputeParameters(false, pnd_Cmpy_Totals_Pnd_Tot_Nra_Tax), pnd_Cntl_Pnd_Nra_Tax.getValue(6,pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Nra_Tax_F.getValue(6, //Natural: ASSIGN #TOT-NRA-TAX := #CNTL.#NRA-TAX ( 6,#S2 ) + #CNTL.#NRA-TAX-F ( 6,#S2 )
                pnd_Ws_Pnd_S2)));
        }                                                                                                                                                                 //Natural: END-IF
        //*  GRAND TOTAL
        if (condition(pnd_Ws_Pnd_S2.equals(2)))                                                                                                                           //Natural: IF #S2 = 2
        {
            pnd_Grnd_Totals_Pnd_Grd_Gross_Amt.compute(new ComputeParameters(false, pnd_Grnd_Totals_Pnd_Grd_Gross_Amt), pnd_Cntl_Pnd_Gross_Amt.getValue(6,                 //Natural: ASSIGN #GRD-GROSS-AMT := #CNTL.#GROSS-AMT ( 6,#S2 ) + #CNTL.#GROSS-AMT-F ( 6,#S2 )
                pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Gross_Amt_F.getValue(6,pnd_Ws_Pnd_S2)));
            pnd_Grnd_Totals_Pnd_Grd_Gross_Tax.compute(new ComputeParameters(false, pnd_Grnd_Totals_Pnd_Grd_Gross_Tax), pnd_Cntl_Pnd_Gross_Tax.getValue(6,                 //Natural: ASSIGN #GRD-GROSS-TAX := #CNTL.#GROSS-TAX ( 6,#S2 ) + #CNTL.#GROSS-TAX-F ( 6,#S2 )
                pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Gross_Tax_F.getValue(6,pnd_Ws_Pnd_S2)));
            pnd_Grnd_Totals_Pnd_Grd_Can_Tax.compute(new ComputeParameters(false, pnd_Grnd_Totals_Pnd_Grd_Can_Tax), pnd_Cntl_Pnd_Can_Tax.getValue(6,pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Can_Tax_F.getValue(6, //Natural: ASSIGN #GRD-CAN-TAX := #CNTL.#CAN-TAX ( 6,#S2 ) + #CNTL.#CAN-TAX-F ( 6,#S2 )
                pnd_Ws_Pnd_S2)));
            pnd_Grnd_Totals_Pnd_Grd_Fed_Tax.compute(new ComputeParameters(false, pnd_Grnd_Totals_Pnd_Grd_Fed_Tax), pnd_Cntl_Pnd_Fed_Tax.getValue(6,pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Fed_Tax_F.getValue(6, //Natural: ASSIGN #GRD-FED-TAX := #CNTL.#FED-TAX ( 6,#S2 ) + #CNTL.#FED-TAX-F ( 6,#S2 )
                pnd_Ws_Pnd_S2)));
            pnd_Grnd_Totals_Pnd_Grd_Nra_Tax.compute(new ComputeParameters(false, pnd_Grnd_Totals_Pnd_Grd_Nra_Tax), pnd_Cntl_Pnd_Nra_Tax.getValue(6,pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Nra_Tax_F.getValue(6, //Natural: ASSIGN #GRD-NRA-TAX := #CNTL.#NRA-TAX ( 6,#S2 ) + #CNTL.#NRA-TAX-F ( 6,#S2 )
                pnd_Ws_Pnd_S2)));
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #WS.#S1 = 1 TO #CNTL-MAX
        for (pnd_Ws_Pnd_S1.setValue(1); condition(pnd_Ws_Pnd_S1.lessOrEqual(pnd_Ws_Const_Pnd_Cntl_Max)); pnd_Ws_Pnd_S1.nadd(1))
        {
            pnd_Ws_Pnd_Tran_Cv.setValue(pnd_Cntl_Pnd_Tran_Cnt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                //Natural: ASSIGN #WS.#TRAN-CV := #CNTL.#TRAN-CNT-CV ( #S1 )
            pnd_Ws_Pnd_Gross_Cv.setValue(pnd_Cntl_Pnd_Gross_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                              //Natural: ASSIGN #WS.#GROSS-CV := #CNTL.#GROSS-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Gross_Tax_Cv.setValue(pnd_Cntl_Pnd_Gross_Tax_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                      //Natural: ASSIGN #WS.#GROSS-TAX-CV := #CNTL.#GROSS-TAX-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Can_Cv.setValue(pnd_Cntl_Pnd_Can_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#CAN-CV := #CNTL.#CAN-TAX-CV ( #S1 )
            pnd_Ws_Pnd_Fed_Cv.setValue(pnd_Cntl_Pnd_Fed_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#FED-CV := #CNTL.#FED-TAX-CV ( #S1 )
            pnd_Ws_Pnd_Nra_Cv.setValue(pnd_Cntl_Pnd_Nra_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#NRA-CV := #CNTL.#NRA-TAX-CV ( #S1 )
            getReports().display(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",                         //Natural: DISPLAY ( 1 ) ( HC = R ES = ON ) '/' #CNTL-TEXT ( #S1 ) '//Trans Count' #CNTL.#TRAN-CNT ( #S1,#S2 ) ( CV = #TRAN-CV ) '//Gross Amount' #CNTL.#GROSS-AMT ( #S1,#S2 ) ( CV = #GROSS-CV ) 'Gross Amount/for payments with/Canadian Withholding' #CNTL.#GROSS-TAX ( #S1,#S2 ) ( CV = #GROSS-TAX-CV ) 3X '//Canadian Tax' #CNTL.#CAN-TAX ( #S1,#S2 ) ( CV = #CAN-CV ) 3X '//Federal  Tax' #CNTL.#FED-TAX ( #S1,#S2 ) ( CV = #FED-CV ) 3X '//Non-FATCA Tax' #CNTL.#NRA-TAX ( #S1,#S2 ) ( CV = #NRA-CV )
            		pnd_Cntl_Pnd_Cntl_Text.getValue(pnd_Ws_Pnd_S1),"//Trans Count",
            		pnd_Cntl_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Tran_Cv,"//Gross Amount",
            		pnd_Cntl_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Gross_Cv,"Gross Amount/for payments with/Canadian Withholding",
            		pnd_Cntl_Pnd_Gross_Tax.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Gross_Tax_Cv,new ColumnSpacing(3),"//Canadian Tax",
            		pnd_Cntl_Pnd_Can_Tax.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Can_Cv,new ColumnSpacing(3),"//Federal  Tax",
            		pnd_Cntl_Pnd_Fed_Tax.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Fed_Cv,new ColumnSpacing(3),"//Non-FATCA Tax",
            		pnd_Cntl_Pnd_Nra_Tax.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Nra_Cv);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1 LINES
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  11/2014 START
        getReports().write(1, ReportOption.NOTITLE,pnd_Dashes);                                                                                                           //Natural: WRITE ( 1 ) #DASHES
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(60),"Gross Amount",NEWLINE,new TabSetting(55),"for payments with",NEWLINE,new           //Natural: WRITE ( 1 ) // 60T 'Gross Amount' / 55T 'for payments with' / 20T 'Trans Count' 38T 'Gross Amount' 51T 'Canadian Withhodling' 77T 'Canadian Tax' 95T 'Federal Tax' 116T 'FATCA Tax' / 20T '-----------' 32T '------------------' 51T '--------------------' 74T '---------------' 91T '---------------' 110T '---------------'
            TabSetting(20),"Trans Count",new TabSetting(38),"Gross Amount",new TabSetting(51),"Canadian Withhodling",new TabSetting(77),"Canadian Tax",new 
            TabSetting(95),"Federal Tax",new TabSetting(116),"FATCA Tax",NEWLINE,new TabSetting(20),"-----------",new TabSetting(32),"------------------",new 
            TabSetting(51),"--------------------",new TabSetting(74),"---------------",new TabSetting(91),"---------------",new TabSetting(110),"---------------");
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #WS.#S1 = 1 TO #CNTL-MAX
        for (pnd_Ws_Pnd_S1.setValue(1); condition(pnd_Ws_Pnd_S1.lessOrEqual(pnd_Ws_Const_Pnd_Cntl_Max)); pnd_Ws_Pnd_S1.nadd(1))
        {
            pnd_Ws_Pnd_Tran_Cv.setValue(pnd_Cntl_Pnd_Tran_Cnt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                //Natural: ASSIGN #WS.#TRAN-CV := #CNTL.#TRAN-CNT-CV ( #S1 )
            pnd_Ws_Pnd_Gross_Cv.setValue(pnd_Cntl_Pnd_Gross_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                              //Natural: ASSIGN #WS.#GROSS-CV := #CNTL.#GROSS-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Gross_Tax_Cv.setValue(pnd_Cntl_Pnd_Gross_Tax_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                      //Natural: ASSIGN #WS.#GROSS-TAX-CV := #CNTL.#GROSS-TAX-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Can_Cv.setValue(pnd_Cntl_Pnd_Can_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#CAN-CV := #CNTL.#CAN-TAX-CV ( #S1 )
            pnd_Ws_Pnd_Fed_Cv.setValue(pnd_Cntl_Pnd_Fed_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#FED-CV := #CNTL.#FED-TAX-CV ( #S1 )
            pnd_Ws_Pnd_Nra_Cv.setValue(pnd_Cntl_Pnd_Nra_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#NRA-CV := #CNTL.#NRA-TAX-CV ( #S1 )
            getReports().write(1, ReportOption.NOTITLE,pnd_Cntl_Pnd_Cntl_Text.getValue(pnd_Ws_Pnd_S1),new TabSetting(19),pnd_Cntl_Pnd_Tran_Cnt_F.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2),  //Natural: WRITE ( 1 ) #CNTL-TEXT ( #S1 ) 19T #CNTL.#TRAN-CNT-F ( #S1,#S2 ) ( CV = #TRAN-CV ) 32T #CNTL.#GROSS-AMT-F ( #S1,#S2 ) ( CV = #GROSS-CV ) 51T #CNTL.#GROSS-TAX-F ( #S1,#S2 ) ( CV = #GROSS-TAX-CV ) 74T #CNTL.#CAN-TAX-F ( #S1,#S2 ) ( CV = #CAN-CV ) 91T #CNTL.#FED-TAX-F ( #S1,#S2 ) ( CV = #FED-CV ) 110T#CNTL.#NRA-TAX-F ( #S1,#S2 ) ( CV = #NRA-CV )
                pnd_Ws_Pnd_Tran_Cv, new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),new TabSetting(32),pnd_Cntl_Pnd_Gross_Amt_F.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), 
                pnd_Ws_Pnd_Gross_Cv, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(51),pnd_Cntl_Pnd_Gross_Tax_F.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), 
                pnd_Ws_Pnd_Gross_Tax_Cv, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(74),pnd_Cntl_Pnd_Can_Tax_F.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), 
                pnd_Ws_Pnd_Can_Cv, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Cntl_Pnd_Fed_Tax_F.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), 
                pnd_Ws_Pnd_Fed_Cv, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_Cntl_Pnd_Nra_Tax_F.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), 
                pnd_Ws_Pnd_Nra_Cv, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1 LINES
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,pnd_Dashes);                                                                                                           //Natural: WRITE ( 1 ) #DASHES
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_S2.equals(1)))                                                                                                                           //Natural: IF #S2 = 1
        {
            getReports().write(1, ReportOption.NOTITLE,"Company Total",new TabSetting(32),pnd_Cmpy_Totals_Pnd_Tot_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 1 ) 'Company Total' 32T #TOT-GROSS-AMT 51T #TOT-GROSS-TAX 74T #TOT-CAN-TAX 91T #TOT-FED-TAX 110T #TOT-NRA-TAX
                TabSetting(51),pnd_Cmpy_Totals_Pnd_Tot_Gross_Tax, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(74),pnd_Cmpy_Totals_Pnd_Tot_Can_Tax, 
                new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Cmpy_Totals_Pnd_Tot_Fed_Tax, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_Cmpy_Totals_Pnd_Tot_Nra_Tax, 
                new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Pnd_S2.equals(2)))                                                                                                                           //Natural: IF #S2 = 2
        {
            getReports().write(1, ReportOption.NOTITLE,"Grand Total",new TabSetting(32),pnd_Grnd_Totals_Pnd_Grd_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 1 ) 'Grand Total' 32T #GRD-GROSS-AMT 51T #GRD-GROSS-TAX 74T #GRD-CAN-TAX 91T #GRD-FED-TAX 110T #GRD-NRA-TAX
                TabSetting(51),pnd_Grnd_Totals_Pnd_Grd_Gross_Tax, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(74),pnd_Grnd_Totals_Pnd_Grd_Can_Tax, 
                new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Grnd_Totals_Pnd_Grd_Fed_Tax, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_Grnd_Totals_Pnd_Grd_Nra_Tax, 
                new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  11/2014 END
    }
    private void sub_Process_Company() throws Exception                                                                                                                   //Natural: PROCESS-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        DbsUtil.callnat(Twrncomp.class , getCurrentProcessState(), pdaTwracomp.getPnd_Twracomp_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracomp.getPnd_Twracomp_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOMP' USING #TWRACOMP.#INPUT-PARMS ( AD = O ) #TWRACOMP.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Process_Control_Record() throws Exception                                                                                                            //Natural: PROCESS-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        getWorkFiles().read(3, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                             //Natural: READ WORK FILE 03 ONCE #TWRP0600-CONTROL-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Control Record is Empty",new TabSetting(77),"***");                                      //Natural: WRITE ( 1 ) '***' 25T 'Control Record is Empty' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(101);  if (true) return;                                                                                                                    //Natural: TERMINATE 101
        }                                                                                                                                                                 //Natural: END-ENDFILE
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaTwrl0900_getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_FormIsBreak = ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form().isBreak(endOfData);
        if (condition(ldaTwrl0900_getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_FormIsBreak))
        {
            pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(readWork01Twrpymnt_Company_Cde_FormOld);                                                                 //Natural: ASSIGN #TWRACOMP.#COMP-CODE := OLD ( #XTAXYR-F94.TWRPYMNT-COMPANY-CDE-FORM )
            pdaTwracomp.getPnd_Twracomp_Pnd_Tax_Year().setValue(readWork01Twrpymnt_Tax_YearOld);                                                                          //Natural: ASSIGN #TWRACOMP.#TAX-YEAR := OLD ( #XTAXYR-F94.TWRPYMNT-TAX-YEAR )
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMPANY
            sub_Process_Company();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Pnd_Company_Line.setValue("Company:");                                                                                                                 //Natural: ASSIGN #WS.#COMPANY-LINE := 'Company:'
            setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),pnd_Ws_Pnd_Company_Line,11,4);                                                          //Natural: MOVE #TWRACOMP.#COMP-SHORT-NAME TO SUBSTR ( #WS.#COMPANY-LINE,11,4 )
            setValueToSubstring("-",pnd_Ws_Pnd_Company_Line,16,1);                                                                                                        //Natural: MOVE '-' TO SUBSTR ( #WS.#COMPANY-LINE,16,1 )
            setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Name(),pnd_Ws_Pnd_Company_Line,18,55);                                                               //Natural: MOVE #TWRACOMP.#COMP-NAME TO SUBSTR ( #WS.#COMPANY-LINE,18,55 )
            pnd_Ws_Pnd_S2.setValue(1);                                                                                                                                    //Natural: ASSIGN #WS.#S2 := 1
                                                                                                                                                                          //Natural: PERFORM WRITE-CANADIAN-REPORT
            sub_Write_Canadian_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Cntl_Pnd_Tran_Cnt.getValue("*",2).nadd(pnd_Cntl_Pnd_Tran_Cnt.getValue("*",1));                                                                            //Natural: ADD #CNTL.#TRAN-CNT ( *,1 ) TO #CNTL.#TRAN-CNT ( *,2 )
            pnd_Cntl_Pnd_Gross_Amt.getValue("*",2).nadd(pnd_Cntl_Pnd_Gross_Amt.getValue("*",1));                                                                          //Natural: ADD #CNTL.#GROSS-AMT ( *,1 ) TO #CNTL.#GROSS-AMT ( *,2 )
            pnd_Cntl_Pnd_Gross_Tax.getValue("*",2).nadd(pnd_Cntl_Pnd_Gross_Tax.getValue("*",1));                                                                          //Natural: ADD #CNTL.#GROSS-TAX ( *,1 ) TO #CNTL.#GROSS-TAX ( *,2 )
            pnd_Cntl_Pnd_Can_Tax.getValue("*",2).nadd(pnd_Cntl_Pnd_Can_Tax.getValue("*",1));                                                                              //Natural: ADD #CNTL.#CAN-TAX ( *,1 ) TO #CNTL.#CAN-TAX ( *,2 )
            pnd_Cntl_Pnd_Fed_Tax.getValue("*",2).nadd(pnd_Cntl_Pnd_Fed_Tax.getValue("*",1));                                                                              //Natural: ADD #CNTL.#FED-TAX ( *,1 ) TO #CNTL.#FED-TAX ( *,2 )
            pnd_Cntl_Pnd_Nra_Tax.getValue("*",2).nadd(pnd_Cntl_Pnd_Nra_Tax.getValue("*",1));                                                                              //Natural: ADD #CNTL.#NRA-TAX ( *,1 ) TO #CNTL.#NRA-TAX ( *,2 )
            //*  11/2014 START
            pnd_Cntl_Pnd_Tran_Cnt_F.getValue("*",2).nadd(pnd_Cntl_Pnd_Tran_Cnt_F.getValue("*",1));                                                                        //Natural: ADD #CNTL.#TRAN-CNT-F ( *,1 ) TO #CNTL.#TRAN-CNT-F ( *,2 )
            pnd_Cntl_Pnd_Gross_Amt_F.getValue("*",2).nadd(pnd_Cntl_Pnd_Gross_Amt_F.getValue("*",1));                                                                      //Natural: ADD #CNTL.#GROSS-AMT-F ( *,1 ) TO #CNTL.#GROSS-AMT-F ( *,2 )
            pnd_Cntl_Pnd_Gross_Tax_F.getValue("*",2).nadd(pnd_Cntl_Pnd_Gross_Tax_F.getValue("*",1));                                                                      //Natural: ADD #CNTL.#GROSS-TAX-F ( *,1 ) TO #CNTL.#GROSS-TAX-F ( *,2 )
            pnd_Cntl_Pnd_Can_Tax_F.getValue("*",2).nadd(pnd_Cntl_Pnd_Can_Tax_F.getValue("*",1));                                                                          //Natural: ADD #CNTL.#CAN-TAX-F ( *,1 ) TO #CNTL.#CAN-TAX-F ( *,2 )
            pnd_Cntl_Pnd_Fed_Tax_F.getValue("*",2).nadd(pnd_Cntl_Pnd_Fed_Tax_F.getValue("*",1));                                                                          //Natural: ADD #CNTL.#FED-TAX-F ( *,1 ) TO #CNTL.#FED-TAX-F ( *,2 )
            pnd_Cntl_Pnd_Nra_Tax_F.getValue("*",2).nadd(pnd_Cntl_Pnd_Nra_Tax_F.getValue("*",1));                                                                          //Natural: ADD #CNTL.#NRA-TAX-F ( *,1 ) TO #CNTL.#NRA-TAX-F ( *,2 )
            //*  11/2014 END
            pnd_Cntl_Pnd_Totals.getValue("*",1).reset();                                                                                                                  //Natural: RESET #CNTL.#TOTALS ( *,1 )
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=133 ZP=ON");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(50),"Summary of Canadian Transactions",new TabSetting(120),"Report: RPT1",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",
            
        		pnd_Cntl_Pnd_Cntl_Text,"//Trans Count",
        		pnd_Cntl_Pnd_Tran_Cnt, pnd_Ws_Pnd_Tran_Cv,"//Gross Amount",
        		pnd_Cntl_Pnd_Gross_Amt, pnd_Ws_Pnd_Gross_Cv,"Gross Amount/for payments with/Canadian Withholding",
        		pnd_Cntl_Pnd_Gross_Tax, pnd_Ws_Pnd_Gross_Tax_Cv,new ColumnSpacing(3),"//Canadian Tax",
        		pnd_Cntl_Pnd_Can_Tax, pnd_Ws_Pnd_Can_Cv,new ColumnSpacing(3),"//Federal  Tax",
        		pnd_Cntl_Pnd_Fed_Tax, pnd_Ws_Pnd_Fed_Cv,new ColumnSpacing(3),"//Non-FATCA Tax",
        		pnd_Cntl_Pnd_Nra_Tax, pnd_Ws_Pnd_Nra_Cv);
    }
    private void CheckAtStartofData240() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Ws_Pnd_Tax_Year.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());                                                                              //Natural: ASSIGN #WS.#TAX-YEAR := #XTAXYR-F94.TWRPYMNT-TAX-YEAR
            //*  BK
        }                                                                                                                                                                 //Natural: END-START
    }
}
