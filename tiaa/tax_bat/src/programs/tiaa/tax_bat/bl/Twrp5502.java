/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:40:52 PM
**        * FROM NATURAL PROGRAM : Twrp5502
************************************************************
**        * FILE NAME            : Twrp5502.java
**        * CLASS NAME           : Twrp5502
**        * INSTANCE NAME        : Twrp5502
************************************************************
************************************************************************
** PROGRAM : TWRP5502
** SYSTEM  : TAXWARS
** AUTHOR  : MICHAEL SUPONITSKY
** FUNCTION: RECONCILIATION REPORT FOR 1042-S FROM THE  PAYMENT FILE.
**           (TAX-CITIZENSHIP = F.
** HISTORY.....:
** 10/22/11  MS DESIGNATED ROTH QUALIFIED DISTRIBUTIONS
**              USED FOR TAX YEARS >= 2010
** 11/20/14  O SOTTO FATCA CHANGES MARKED 11/2014.
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5502 extends BLNatBase
{
    // Data Areas
    private PdaTwracomp pdaTwracomp;
    private LdaTwrl0600 ldaTwrl0600;
    private LdaTwrl0900 ldaTwrl0900;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Pnd_Cntl_Max;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_Company_Line;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_S1;
    private DbsField pnd_Ws_Pnd_S2;
    private DbsField pnd_Ws_Pnd_Tran_Cv;
    private DbsField pnd_Ws_Pnd_Gross_Cv;
    private DbsField pnd_Ws_Pnd_Ivc_Cv;
    private DbsField pnd_Ws_Pnd_Taxable_Cv;
    private DbsField pnd_Ws_Pnd_Fed_Cv;
    private DbsField pnd_Ws_Pnd_Nra_Cv;
    private DbsField pnd_Ws_Pnd_Refund_Cv;
    private DbsField pnd_Ws_Pnd_Int_Cv;

    private DbsGroup pnd_Cntl;
    private DbsField pnd_Cntl_Pnd_Cntl_Text;
    private DbsField pnd_Cntl_Pnd_Cntl_Text1;
    private DbsField pnd_Cntl_Pnd_Tran_Cnt_Cv;
    private DbsField pnd_Cntl_Pnd_Gross_Amt_Cv;
    private DbsField pnd_Cntl_Pnd_Ivc_Amt_Cv;
    private DbsField pnd_Cntl_Pnd_Taxable_Amt_Cv;
    private DbsField pnd_Cntl_Pnd_Fed_Tax_Cv;
    private DbsField pnd_Cntl_Pnd_Nra_Tax_Cv;
    private DbsField pnd_Cntl_Pnd_Nra_Refund_Cv;
    private DbsField pnd_Cntl_Pnd_Int_Amt_Cv;

    private DbsGroup pnd_Cntl_Pnd_Totals;
    private DbsField pnd_Cntl_Pnd_Tran_Cnt;
    private DbsField pnd_Cntl_Pnd_Gross_Amt;
    private DbsField pnd_Cntl_Pnd_Ivc_Amt;
    private DbsField pnd_Cntl_Pnd_Taxable_Amt;
    private DbsField pnd_Cntl_Pnd_Fed_Tax;
    private DbsField pnd_Cntl_Pnd_Nra_Tax;
    private DbsField pnd_Cntl_Pnd_Nra_Refund;
    private DbsField pnd_Cntl_Pnd_Int_Amt;
    private DbsField pnd_Cntl_Pnd_Tran_Cnt_F;
    private DbsField pnd_Cntl_Pnd_Gross_Amt_F;
    private DbsField pnd_Cntl_Pnd_Ivc_Amt_F;
    private DbsField pnd_Cntl_Pnd_Taxable_Amt_F;
    private DbsField pnd_Cntl_Pnd_Fed_Tax_F;
    private DbsField pnd_Cntl_Pnd_Nra_Tax_F;
    private DbsField pnd_Cntl_Pnd_Nra_Refund_F;
    private DbsField pnd_Cntl_Pnd_Int_Amt_F;

    private DbsGroup pnd_Cmpy_Totals;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Gross_Amt;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Ivc_Amt;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Taxable_Amt;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Fed_Tax;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Nra_Tax;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Nra_Refund;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Int_Amt;

    private DbsGroup pnd_Grnd_Totals;
    private DbsField pnd_Grnd_Totals_Pnd_Grd_Gross_Amt;
    private DbsField pnd_Grnd_Totals_Pnd_Grd_Ivc_Amt;
    private DbsField pnd_Grnd_Totals_Pnd_Grd_Taxable_Amt;
    private DbsField pnd_Grnd_Totals_Pnd_Grd_Fed_Tax;
    private DbsField pnd_Grnd_Totals_Pnd_Grd_Nra_Tax;
    private DbsField pnd_Grnd_Totals_Pnd_Grd_Nra_Refund;
    private DbsField pnd_Grnd_Totals_Pnd_Grd_Int_Amt;
    private DbsField pnd_Dashes;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Twrpymnt_Company_Cde_FormOld;
    private DbsField readWork01Twrpymnt_Tax_YearOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwracomp = new PdaTwracomp(localVariables);
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);
        ldaTwrl0900 = new LdaTwrl0900();
        registerRecord(ldaTwrl0900);

        // Local Variables

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Pnd_Cntl_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_Cntl_Max", "#CNTL-MAX", FieldType.PACKED_DECIMAL, 3);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Tax_Year = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Company_Line = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Line", "#COMPANY-LINE", FieldType.STRING, 72);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S1", "#S1", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S2", "#S2", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Tran_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tran_Cv", "#TRAN-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Gross_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Gross_Cv", "#GROSS-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Ivc_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ivc_Cv", "#IVC-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Taxable_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Taxable_Cv", "#TAXABLE-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Fed_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Fed_Cv", "#FED-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Nra_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Nra_Cv", "#NRA-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Refund_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Refund_Cv", "#REFUND-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Int_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Int_Cv", "#INT-CV", FieldType.ATTRIBUTE_CONTROL, 2);

        pnd_Cntl = localVariables.newGroupArrayInRecord("pnd_Cntl", "#CNTL", new DbsArrayController(1, 6));
        pnd_Cntl_Pnd_Cntl_Text = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Cntl_Text", "#CNTL-TEXT", FieldType.STRING, 26);
        pnd_Cntl_Pnd_Cntl_Text1 = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Cntl_Text1", "#CNTL-TEXT1", FieldType.STRING, 26);
        pnd_Cntl_Pnd_Tran_Cnt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Tran_Cnt_Cv", "#TRAN-CNT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Gross_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Gross_Amt_Cv", "#GROSS-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Ivc_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Ivc_Amt_Cv", "#IVC-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Taxable_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Taxable_Amt_Cv", "#TAXABLE-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Fed_Tax_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Fed_Tax_Cv", "#FED-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Nra_Tax_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Nra_Tax_Cv", "#NRA-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Nra_Refund_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Nra_Refund_Cv", "#NRA-REFUND-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Int_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Int_Amt_Cv", "#INT-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);

        pnd_Cntl_Pnd_Totals = pnd_Cntl.newGroupArrayInGroup("pnd_Cntl_Pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Cntl_Pnd_Tran_Cnt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Tran_Cnt", "#TRAN-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Cntl_Pnd_Gross_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Cntl_Pnd_Ivc_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Taxable_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Cntl_Pnd_Fed_Tax = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Fed_Tax", "#FED-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Nra_Tax = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Nra_Tax", "#NRA-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Nra_Refund = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Nra_Refund", "#NRA-REFUND", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Int_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Tran_Cnt_F = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Tran_Cnt_F", "#TRAN-CNT-F", FieldType.PACKED_DECIMAL, 9);
        pnd_Cntl_Pnd_Gross_Amt_F = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Gross_Amt_F", "#GROSS-AMT-F", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Cntl_Pnd_Ivc_Amt_F = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Ivc_Amt_F", "#IVC-AMT-F", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Taxable_Amt_F = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Taxable_Amt_F", "#TAXABLE-AMT-F", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Cntl_Pnd_Fed_Tax_F = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Fed_Tax_F", "#FED-TAX-F", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Nra_Tax_F = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Nra_Tax_F", "#NRA-TAX-F", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Nra_Refund_F = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Nra_Refund_F", "#NRA-REFUND-F", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Int_Amt_F = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Int_Amt_F", "#INT-AMT-F", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Cmpy_Totals = localVariables.newGroupInRecord("pnd_Cmpy_Totals", "#CMPY-TOTALS");
        pnd_Cmpy_Totals_Pnd_Tot_Gross_Amt = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Gross_Amt", "#TOT-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Cmpy_Totals_Pnd_Tot_Ivc_Amt = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Ivc_Amt", "#TOT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Cmpy_Totals_Pnd_Tot_Taxable_Amt = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Taxable_Amt", "#TOT-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Cmpy_Totals_Pnd_Tot_Fed_Tax = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Fed_Tax", "#TOT-FED-TAX", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Cmpy_Totals_Pnd_Tot_Nra_Tax = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Nra_Tax", "#TOT-NRA-TAX", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Cmpy_Totals_Pnd_Tot_Nra_Refund = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Nra_Refund", "#TOT-NRA-REFUND", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Cmpy_Totals_Pnd_Tot_Int_Amt = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Int_Amt", "#TOT-INT-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);

        pnd_Grnd_Totals = localVariables.newGroupInRecord("pnd_Grnd_Totals", "#GRND-TOTALS");
        pnd_Grnd_Totals_Pnd_Grd_Gross_Amt = pnd_Grnd_Totals.newFieldInGroup("pnd_Grnd_Totals_Pnd_Grd_Gross_Amt", "#GRD-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Grnd_Totals_Pnd_Grd_Ivc_Amt = pnd_Grnd_Totals.newFieldInGroup("pnd_Grnd_Totals_Pnd_Grd_Ivc_Amt", "#GRD-IVC-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Grnd_Totals_Pnd_Grd_Taxable_Amt = pnd_Grnd_Totals.newFieldInGroup("pnd_Grnd_Totals_Pnd_Grd_Taxable_Amt", "#GRD-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Grnd_Totals_Pnd_Grd_Fed_Tax = pnd_Grnd_Totals.newFieldInGroup("pnd_Grnd_Totals_Pnd_Grd_Fed_Tax", "#GRD-FED-TAX", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Grnd_Totals_Pnd_Grd_Nra_Tax = pnd_Grnd_Totals.newFieldInGroup("pnd_Grnd_Totals_Pnd_Grd_Nra_Tax", "#GRD-NRA-TAX", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Grnd_Totals_Pnd_Grd_Nra_Refund = pnd_Grnd_Totals.newFieldInGroup("pnd_Grnd_Totals_Pnd_Grd_Nra_Refund", "#GRD-NRA-REFUND", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Grnd_Totals_Pnd_Grd_Int_Amt = pnd_Grnd_Totals.newFieldInGroup("pnd_Grnd_Totals_Pnd_Grd_Int_Amt", "#GRD-INT-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Dashes = localVariables.newFieldInRecord("pnd_Dashes", "#DASHES", FieldType.STRING, 125);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Twrpymnt_Company_Cde_FormOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrpymnt_Company_Cde_Form_OLD", "Twrpymnt_Company_Cde_Form_OLD", 
            FieldType.STRING, 1);
        readWork01Twrpymnt_Tax_YearOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrpymnt_Tax_Year_OLD", "Twrpymnt_Tax_Year_OLD", FieldType.NUMERIC, 
            4);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaTwrl0600.initializeValues();
        ldaTwrl0900.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Pnd_Cntl_Max.setInitialValue(6);
        pnd_Cntl_Pnd_Cntl_Text.getValue(1).setInitialValue("All Payments");
        pnd_Cntl_Pnd_Cntl_Text.getValue(2).setInitialValue("Active Payments");
        pnd_Cntl_Pnd_Cntl_Text.getValue(3).setInitialValue("  'U' IVC with $");
        pnd_Cntl_Pnd_Cntl_Text.getValue(4).setInitialValue("+ 'U' IVC (all)");
        pnd_Cntl_Pnd_Cntl_Text.getValue(5).setInitialValue("- Qualified Distributions");
        pnd_Cntl_Pnd_Cntl_Text.getValue(6).setInitialValue("= 1042-S form");
        pnd_Cntl_Pnd_Cntl_Text1.getValue(3).setInitialValue("  (Not for reconciliation)");
        pnd_Cntl_Pnd_Cntl_Text1.getValue(5).setInitialValue("  from Designated Roth");
        pnd_Cntl_Pnd_Tran_Cnt_Cv.setInitialAttributeValue("AD=I)#CNTL.#TRAN-CNT-CV(2)	(AD=I)#CNTL.#TRAN-CNT-CV(3)	(AD=N)#CNTL.#TRAN-CNT-CV(4)	(AD=N)#CNTL.#TRAN-CNT-CV(5)	(AD=N)#CNTL.#TRAN-CNT-CV(6)	(AD=N");
        pnd_Cntl_Pnd_Gross_Amt_Cv.setInitialAttributeValue("AD=I)#CNTL.#GROSS-AMT-CV(2)	(AD=I)#CNTL.#GROSS-AMT-CV(3)	(AD=N)#CNTL.#GROSS-AMT-CV(4)	(AD=N)#CNTL.#GROSS-AMT-CV(5)	(AD=N)#CNTL.#GROSS-AMT-CV(6)	(AD=I");
        pnd_Cntl_Pnd_Ivc_Amt_Cv.setInitialAttributeValue("AD=I)#CNTL.#IVC-AMT-CV(2)	(AD=I)#CNTL.#IVC-AMT-CV(3)	(AD=I)#CNTL.#IVC-AMT-CV(4)	(AD=N)#CNTL.#IVC-AMT-CV(5)	(AD=N)#CNTL.#IVC-AMT-CV(6)	(AD=I");
        pnd_Cntl_Pnd_Taxable_Amt_Cv.setInitialAttributeValue("AD=N)#CNTL.#TAXABLE-AMT-CV(2)	(AD=I)#CNTL.#TAXABLE-AMT-CV(3)	(AD=N)#CNTL.#TAXABLE-AMT-CV(4)	(AD=I)#CNTL.#TAXABLE-AMT-CV(5)	(AD=I)#CNTL.#TAXABLE-AMT-CV(6)	(AD=I");
        pnd_Cntl_Pnd_Fed_Tax_Cv.setInitialAttributeValue("AD=I)#CNTL.#FED-TAX-CV(2)	(AD=I)#CNTL.#FED-TAX-CV(3)	(AD=N)#CNTL.#FED-TAX-CV(4)	(AD=N)#CNTL.#FED-TAX-CV(5)	(AD=N)#CNTL.#FED-TAX-CV(6)	(AD=I");
        pnd_Cntl_Pnd_Nra_Tax_Cv.setInitialAttributeValue("AD=I)#CNTL.#NRA-TAX-CV(2)	(AD=I)#CNTL.#NRA-TAX-CV(3)	(AD=N)#CNTL.#NRA-TAX-CV(4)	(AD=N)#CNTL.#NRA-TAX-CV(5)	(AD=N)#CNTL.#NRA-TAX-CV(6)	(AD=I");
        pnd_Cntl_Pnd_Nra_Refund_Cv.setInitialAttributeValue("AD=I)#CNTL.#NRA-REFUND-CV(2)	(AD=I)#CNTL.#NRA-REFUND-CV(3)	(AD=N)#CNTL.#NRA-REFUND-CV(4)	(AD=N)#CNTL.#NRA-REFUND-CV(5)	(AD=N)#CNTL.#NRA-REFUND-CV(6)	(AD=I");
        pnd_Cntl_Pnd_Int_Amt_Cv.setInitialAttributeValue("AD=I)#CNTL.#INT-AMT-CV(2)	(AD=I)#CNTL.#INT-AMT-CV(3)	(AD=N)#CNTL.#INT-AMT-CV(4)	(AD=N)#CNTL.#INT-AMT-CV(5)	(AD=N)#CNTL.#INT-AMT-CV(6)	(AD=I");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5502() throws Exception
    {
        super("Twrp5502");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 133 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        pnd_Dashes.moveAll("=");                                                                                                                                          //Natural: MOVE ALL '=' TO #DASHES
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 49T 'Summary of NRA Payment Transactions' 120T 'Report: RPT1' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / #WS.#COMPANY-LINE //
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
                                                                                                                                                                          //Natural: PERFORM PROCESS-CONTROL-RECORD
        sub_Process_Control_Record();
        if (condition(Global.isEscape())) {return;}
        short decideConditionsMet233 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #TWRP0600-TAX-YEAR-CCYY;//Natural: VALUE '2011' : '9999'
        if (condition((ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy().equals("2011' : '9999"))))
        {
            decideConditionsMet233++;
            ignore();
        }                                                                                                                                                                 //Natural: VALUE '2004' : '2010'
        else if (condition((ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy().equals("2004' : '2010"))))
        {
            decideConditionsMet233++;
            Global.setFetchProgram(DbsUtil.getBlType("TWRP5532"));                                                                                                        //Natural: FETCH 'TWRP5532'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: VALUE '2003'
        else if (condition((ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy().equals("2003"))))
        {
            decideConditionsMet233++;
            //*  <= 2002
            Global.setFetchProgram(DbsUtil.getBlType("TWRP5522"));                                                                                                        //Natural: FETCH 'TWRP5522'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            Global.setFetchProgram(DbsUtil.getBlType("TWRP5512"));                                                                                                        //Natural: FETCH 'TWRP5512'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* *READ WORK FILE 1 RECORD #XTAXYR-F94  /* 11/2014
        //*  11/2014
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #XTAXYR-F94
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaTwrl0900.getPnd_Xtaxyr_F94())))
        {
            CheckAtStartofData248();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //*  BEFORE BREAK PROCESSING
            //*    ACCEPT IF #XTAXYR-F94.TWRPYMNT-TAX-ID-NBR       = '121620001'
            //*      OR =                                            '121620004'
            //*  END-BEFORE
            //*                                                                                                                                                           //Natural: AT BREAK OF #XTAXYR-F94.TWRPYMNT-COMPANY-CDE-FORM
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO #XTAXYR-F94.#C-TWRPYMNT-PAYMENTS
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments())); pnd_Ws_Pnd_I.nadd(1))
            {
                //*  *W2                                EXCLUDE W2 DATA    07/15/2009  RM
                if (condition(((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("7") && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payment_Type().getValue(pnd_Ws_Pnd_I).equals("M"))  //Natural: IF #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE = '7' AND #XTAXYR-F94.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'M' AND #XTAXYR-F94.TWRPYMNT-SETTLE-TYPE ( #I ) = 'S' OR = 'T' OR = 'E' OR = 'U' OR = 'M'
                    && ((((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("S") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("T")) 
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("E")) || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("U")) 
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("M")))))
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*                                                   ALL PAYMENTS
                //*  11/2014 START
                if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fatca_Ind().getValue(1).equals("Y")))                                                                //Natural: IF #XTAXYR-F94.TWRPYMNT-FATCA-IND ( 1 ) = 'Y'
                {
                    pnd_Cntl_Pnd_Tran_Cnt_F.getValue(1,1).nadd(1);                                                                                                        //Natural: ADD 1 TO #CNTL.#TRAN-CNT-F ( 1,1 )
                    pnd_Cntl_Pnd_Gross_Amt_F.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                               //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT-F ( 1,1 )
                    pnd_Cntl_Pnd_Ivc_Amt_F.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                                   //Natural: ADD #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) TO #CNTL.#IVC-AMT-F ( 1,1 )
                    pnd_Cntl_Pnd_Fed_Tax_F.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                             //Natural: ADD #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #CNTL.#FED-TAX-F ( 1,1 )
                    pnd_Cntl_Pnd_Nra_Tax_F.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Nra_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                             //Natural: ADD #XTAXYR-F94.TWRPYMNT-NRA-WTHLD-AMT ( #I ) TO #CNTL.#NRA-TAX-F ( 1,1 )
                    pnd_Cntl_Pnd_Nra_Refund_F.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Nra_Refund_Amt().getValue(pnd_Ws_Pnd_I));                         //Natural: ADD #XTAXYR-F94.TWRPYMNT-NRA-REFUND-AMT ( #I ) TO #CNTL.#NRA-REFUND-F ( 1,1 )
                    pnd_Cntl_Pnd_Int_Amt_F.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                   //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#INT-AMT-F ( 1,1 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cntl_Pnd_Tran_Cnt.getValue(1,1).nadd(1);                                                                                                          //Natural: ADD 1 TO #CNTL.#TRAN-CNT ( 1,1 )
                    pnd_Cntl_Pnd_Gross_Amt.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 1,1 )
                    pnd_Cntl_Pnd_Ivc_Amt.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) TO #CNTL.#IVC-AMT ( 1,1 )
                    pnd_Cntl_Pnd_Fed_Tax.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                               //Natural: ADD #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #CNTL.#FED-TAX ( 1,1 )
                    pnd_Cntl_Pnd_Nra_Tax.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Nra_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                               //Natural: ADD #XTAXYR-F94.TWRPYMNT-NRA-WTHLD-AMT ( #I ) TO #CNTL.#NRA-TAX ( 1,1 )
                    pnd_Cntl_Pnd_Nra_Refund.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Nra_Refund_Amt().getValue(pnd_Ws_Pnd_I));                           //Natural: ADD #XTAXYR-F94.TWRPYMNT-NRA-REFUND-AMT ( #I ) TO #CNTL.#NRA-REFUND ( 1,1 )
                    pnd_Cntl_Pnd_Int_Amt.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#INT-AMT ( 1,1 )
                }                                                                                                                                                         //Natural: END-IF
                //*  11/2014 END
                if (condition(! (ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals(" ") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals("C")))) //Natural: IF NOT #XTAXYR-F94.TWRPYMNT-PYMNT-STATUS ( #I ) = ' ' OR = 'C'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*                                                   ACTIVE PAYMENTS
                //*  11/2014 START
                if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fatca_Ind().getValue(1).equals("Y")))                                                                //Natural: IF #XTAXYR-F94.TWRPYMNT-FATCA-IND ( 1 ) = 'Y'
                {
                    pnd_Cntl_Pnd_Tran_Cnt_F.getValue(2,1).nadd(1);                                                                                                        //Natural: ADD 1 TO #CNTL.#TRAN-CNT-F ( 2,1 )
                    pnd_Cntl_Pnd_Gross_Amt_F.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                               //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT-F ( 2,1 )
                    pnd_Cntl_Pnd_Ivc_Amt_F.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                                   //Natural: ADD #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) TO #CNTL.#IVC-AMT-F ( 2,1 )
                    if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Ind().getValue(pnd_Ws_Pnd_I).equals("K") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Protect().getValue(pnd_Ws_Pnd_I).getBoolean())) //Natural: IF #XTAXYR-F94.TWRPYMNT-IVC-IND ( #I ) = 'K' OR #XTAXYR-F94.TWRPYMNT-IVC-PROTECT ( #I )
                    {
                        pnd_Cntl_Pnd_Taxable_Amt_F.getValue(2,1).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Taxable_Amt_F.getValue(2,1)), pnd_Cntl_Pnd_Taxable_Amt_F.getValue(2, //Natural: COMPUTE #CNTL.#TAXABLE-AMT-F ( 2,1 ) = #CNTL.#TAXABLE-AMT-F ( 2,1 ) + #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) - #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I )
                            1).add(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I)).subtract(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I)));
                        //*  ROLLOVER
                        if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Roth_Qual_Ind().getValue(pnd_Ws_Pnd_I).equals("Y") && ! (((((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("G")  //Natural: IF TWRPYMNT-ROTH-QUAL-IND ( #I ) = 'Y' AND NOT #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE = 'G' OR = 'H' OR = 'Z' OR = '[' OR = '='
                            || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("H")) || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("Z")) 
                            || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("[")) || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("="))))))
                        {
                            //*                                        BG        H4
                            pnd_Cntl_Pnd_Taxable_Amt_F.getValue(5,1).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Taxable_Amt_F.getValue(5,1)), pnd_Cntl_Pnd_Taxable_Amt_F.getValue(5, //Natural: ASSIGN #CNTL.#TAXABLE-AMT-F ( 5,1 ) := #CNTL.#TAXABLE-AMT-F ( 5,1 ) + #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) - #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I )
                                1).add(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I)).subtract(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I)));
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Cntl_Pnd_Ivc_Amt_F.getValue(3,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                               //Natural: ADD #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) TO #CNTL.#IVC-AMT-F ( 3,1 )
                        pnd_Cntl_Pnd_Taxable_Amt_F.getValue(4,1).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Taxable_Amt_F.getValue(4,1)), pnd_Cntl_Pnd_Taxable_Amt_F.getValue(4, //Natural: COMPUTE #CNTL.#TAXABLE-AMT-F ( 4,1 ) = #CNTL.#TAXABLE-AMT-F ( 4,1 ) + #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) - #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I )
                            1).add(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I)).subtract(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I)));
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Cntl_Pnd_Fed_Tax_F.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                             //Natural: ADD #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #CNTL.#FED-TAX-F ( 2,1 )
                    pnd_Cntl_Pnd_Nra_Tax_F.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Nra_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                             //Natural: ADD #XTAXYR-F94.TWRPYMNT-NRA-WTHLD-AMT ( #I ) TO #CNTL.#NRA-TAX-F ( 2,1 )
                    pnd_Cntl_Pnd_Nra_Refund_F.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Nra_Refund_Amt().getValue(pnd_Ws_Pnd_I));                         //Natural: ADD #XTAXYR-F94.TWRPYMNT-NRA-REFUND-AMT ( #I ) TO #CNTL.#NRA-REFUND-F ( 2,1 )
                    pnd_Cntl_Pnd_Int_Amt_F.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                   //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#INT-AMT-F ( 2,1 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cntl_Pnd_Tran_Cnt.getValue(2,1).nadd(1);                                                                                                          //Natural: ADD 1 TO #CNTL.#TRAN-CNT ( 2,1 )
                    pnd_Cntl_Pnd_Gross_Amt.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 2,1 )
                    pnd_Cntl_Pnd_Ivc_Amt.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) TO #CNTL.#IVC-AMT ( 2,1 )
                    if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Ind().getValue(pnd_Ws_Pnd_I).equals("K") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Protect().getValue(pnd_Ws_Pnd_I).getBoolean())) //Natural: IF #XTAXYR-F94.TWRPYMNT-IVC-IND ( #I ) = 'K' OR #XTAXYR-F94.TWRPYMNT-IVC-PROTECT ( #I )
                    {
                        pnd_Cntl_Pnd_Taxable_Amt.getValue(2,1).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Taxable_Amt.getValue(2,1)), pnd_Cntl_Pnd_Taxable_Amt.getValue(2, //Natural: COMPUTE #CNTL.#TAXABLE-AMT ( 2,1 ) = #CNTL.#TAXABLE-AMT ( 2,1 ) + #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) - #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I )
                            1).add(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I)).subtract(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I)));
                        //*  ROLLOVER
                        if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Roth_Qual_Ind().getValue(pnd_Ws_Pnd_I).equals("Y") && ! (((((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("G")  //Natural: IF TWRPYMNT-ROTH-QUAL-IND ( #I ) = 'Y' AND NOT #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE = 'G' OR = 'H' OR = 'Z' OR = '[' OR = '='
                            || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("H")) || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("Z")) 
                            || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("[")) || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("="))))))
                        {
                            //*                                        BG        H4
                            pnd_Cntl_Pnd_Taxable_Amt.getValue(5,1).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Taxable_Amt.getValue(5,1)), pnd_Cntl_Pnd_Taxable_Amt.getValue(5, //Natural: ASSIGN #CNTL.#TAXABLE-AMT ( 5,1 ) := #CNTL.#TAXABLE-AMT ( 5,1 ) + #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) - #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I )
                                1).add(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I)).subtract(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I)));
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Cntl_Pnd_Ivc_Amt.getValue(3,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) TO #CNTL.#IVC-AMT ( 3,1 )
                        pnd_Cntl_Pnd_Taxable_Amt.getValue(4,1).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Taxable_Amt.getValue(4,1)), pnd_Cntl_Pnd_Taxable_Amt.getValue(4, //Natural: COMPUTE #CNTL.#TAXABLE-AMT ( 4,1 ) = #CNTL.#TAXABLE-AMT ( 4,1 ) + #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) - #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I )
                            1).add(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I)).subtract(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I)));
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Cntl_Pnd_Fed_Tax.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                               //Natural: ADD #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #CNTL.#FED-TAX ( 2,1 )
                    pnd_Cntl_Pnd_Nra_Tax.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Nra_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                               //Natural: ADD #XTAXYR-F94.TWRPYMNT-NRA-WTHLD-AMT ( #I ) TO #CNTL.#NRA-TAX ( 2,1 )
                    pnd_Cntl_Pnd_Nra_Refund.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Nra_Refund_Amt().getValue(pnd_Ws_Pnd_I));                           //Natural: ADD #XTAXYR-F94.TWRPYMNT-NRA-REFUND-AMT ( #I ) TO #CNTL.#NRA-REFUND ( 2,1 )
                    pnd_Cntl_Pnd_Int_Amt.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#INT-AMT ( 2,1 )
                }                                                                                                                                                         //Natural: END-IF
                //*  11/2014 END
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            readWork01Twrpymnt_Company_Cde_FormOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form());                                                   //Natural: END-WORK
            readWork01Twrpymnt_Tax_YearOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Company_Line.setValue("Grand Totals");                                                                                                                 //Natural: ASSIGN #WS.#COMPANY-LINE := 'Grand Totals'
        pnd_Ws_Pnd_S2.setValue(2);                                                                                                                                        //Natural: ASSIGN #WS.#S2 := 2
                                                                                                                                                                          //Natural: PERFORM WRITE-NRA-REPORT
        sub_Write_Nra_Report();
        if (condition(Global.isEscape())) {return;}
        //* **********************
        //*  S U B R O U T I N E S
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-NRA-REPORT
        //* *********************************
        //*    'NRA Tax'          #CNTL.#NRA-TAX        (#S1,#S2)(CV=#NRA-CV) /
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COMPANY
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CONTROL-RECORD
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Write_Nra_Report() throws Exception                                                                                                                  //Natural: WRITE-NRA-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Cntl_Pnd_Gross_Amt.getValue(6,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Gross_Amt.getValue(2,pnd_Ws_Pnd_S2));                                                      //Natural: ASSIGN #CNTL.#GROSS-AMT ( 6,#S2 ) := #CNTL.#GROSS-AMT ( 2,#S2 )
        pnd_Cntl_Pnd_Int_Amt.getValue(6,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Int_Amt.getValue(2,pnd_Ws_Pnd_S2));                                                          //Natural: ASSIGN #CNTL.#INT-AMT ( 6,#S2 ) := #CNTL.#INT-AMT ( 2,#S2 )
        pnd_Cntl_Pnd_Ivc_Amt.getValue(6,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Ivc_Amt.getValue(2,pnd_Ws_Pnd_S2));                                                          //Natural: ASSIGN #CNTL.#IVC-AMT ( 6,#S2 ) := #CNTL.#IVC-AMT ( 2,#S2 )
        pnd_Cntl_Pnd_Taxable_Amt.getValue(6,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Taxable_Amt.getValue(6,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Taxable_Amt.getValue(2, //Natural: ASSIGN #CNTL.#TAXABLE-AMT ( 6,#S2 ) := #CNTL.#TAXABLE-AMT ( 2,#S2 ) + #CNTL.#TAXABLE-AMT ( 4,#S2 ) - #CNTL.#TAXABLE-AMT ( 5,#S2 )
            pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Taxable_Amt.getValue(4,pnd_Ws_Pnd_S2)).subtract(pnd_Cntl_Pnd_Taxable_Amt.getValue(5,pnd_Ws_Pnd_S2)));
        pnd_Cntl_Pnd_Nra_Tax.getValue(6,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Nra_Tax.getValue(2,pnd_Ws_Pnd_S2));                                                          //Natural: ASSIGN #CNTL.#NRA-TAX ( 6,#S2 ) := #CNTL.#NRA-TAX ( 2,#S2 )
        pnd_Cntl_Pnd_Nra_Refund.getValue(6,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Nra_Refund.getValue(2,pnd_Ws_Pnd_S2));                                                    //Natural: ASSIGN #CNTL.#NRA-REFUND ( 6,#S2 ) := #CNTL.#NRA-REFUND ( 2,#S2 )
        pnd_Cntl_Pnd_Fed_Tax.getValue(6,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Fed_Tax.getValue(2,pnd_Ws_Pnd_S2));                                                          //Natural: ASSIGN #CNTL.#FED-TAX ( 6,#S2 ) := #CNTL.#FED-TAX ( 2,#S2 )
        pnd_Cntl_Pnd_Gross_Amt_F.getValue(6,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Gross_Amt_F.getValue(2,pnd_Ws_Pnd_S2));                                                  //Natural: ASSIGN #CNTL.#GROSS-AMT-F ( 6,#S2 ) := #CNTL.#GROSS-AMT-F ( 2,#S2 )
        pnd_Cntl_Pnd_Int_Amt_F.getValue(6,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Int_Amt_F.getValue(2,pnd_Ws_Pnd_S2));                                                      //Natural: ASSIGN #CNTL.#INT-AMT-F ( 6,#S2 ) := #CNTL.#INT-AMT-F ( 2,#S2 )
        pnd_Cntl_Pnd_Ivc_Amt_F.getValue(6,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Ivc_Amt_F.getValue(2,pnd_Ws_Pnd_S2));                                                      //Natural: ASSIGN #CNTL.#IVC-AMT-F ( 6,#S2 ) := #CNTL.#IVC-AMT-F ( 2,#S2 )
        pnd_Cntl_Pnd_Taxable_Amt_F.getValue(6,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Taxable_Amt_F.getValue(6,pnd_Ws_Pnd_S2)),                  //Natural: ASSIGN #CNTL.#TAXABLE-AMT-F ( 6,#S2 ) := #CNTL.#TAXABLE-AMT-F ( 2,#S2 ) + #CNTL.#TAXABLE-AMT-F ( 4,#S2 ) - #CNTL.#TAXABLE-AMT-F ( 5,#S2 )
            pnd_Cntl_Pnd_Taxable_Amt_F.getValue(2,pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Taxable_Amt_F.getValue(4,pnd_Ws_Pnd_S2)).subtract(pnd_Cntl_Pnd_Taxable_Amt_F.getValue(5,
            pnd_Ws_Pnd_S2)));
        pnd_Cntl_Pnd_Nra_Tax_F.getValue(6,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Nra_Tax_F.getValue(2,pnd_Ws_Pnd_S2));                                                      //Natural: ASSIGN #CNTL.#NRA-TAX-F ( 6,#S2 ) := #CNTL.#NRA-TAX-F ( 2,#S2 )
        pnd_Cntl_Pnd_Nra_Refund_F.getValue(6,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Nra_Refund_F.getValue(2,pnd_Ws_Pnd_S2));                                                //Natural: ASSIGN #CNTL.#NRA-REFUND-F ( 6,#S2 ) := #CNTL.#NRA-REFUND-F ( 2,#S2 )
        pnd_Cntl_Pnd_Fed_Tax_F.getValue(6,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Fed_Tax_F.getValue(2,pnd_Ws_Pnd_S2));                                                      //Natural: ASSIGN #CNTL.#FED-TAX-F ( 6,#S2 ) := #CNTL.#FED-TAX-F ( 2,#S2 )
        //*  COMPANY BREAK
        if (condition(pnd_Ws_Pnd_S2.equals(1)))                                                                                                                           //Natural: IF #S2 = 1
        {
            pnd_Cmpy_Totals_Pnd_Tot_Gross_Amt.compute(new ComputeParameters(false, pnd_Cmpy_Totals_Pnd_Tot_Gross_Amt), pnd_Cntl_Pnd_Gross_Amt.getValue(6,                 //Natural: ASSIGN #TOT-GROSS-AMT := #CNTL.#GROSS-AMT ( 6,#S2 ) + #CNTL.#GROSS-AMT-F ( 6,#S2 )
                pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Gross_Amt_F.getValue(6,pnd_Ws_Pnd_S2)));
            pnd_Cmpy_Totals_Pnd_Tot_Ivc_Amt.compute(new ComputeParameters(false, pnd_Cmpy_Totals_Pnd_Tot_Ivc_Amt), pnd_Cntl_Pnd_Ivc_Amt.getValue(6,pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Ivc_Amt_F.getValue(6, //Natural: ASSIGN #TOT-IVC-AMT := #CNTL.#IVC-AMT ( 6,#S2 ) + #CNTL.#IVC-AMT-F ( 6,#S2 )
                pnd_Ws_Pnd_S2)));
            pnd_Cmpy_Totals_Pnd_Tot_Taxable_Amt.compute(new ComputeParameters(false, pnd_Cmpy_Totals_Pnd_Tot_Taxable_Amt), pnd_Cntl_Pnd_Taxable_Amt.getValue(6,           //Natural: ASSIGN #TOT-TAXABLE-AMT := #CNTL.#TAXABLE-AMT ( 6,#S2 ) + #CNTL.#TAXABLE-AMT-F ( 6,#S2 )
                pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Taxable_Amt_F.getValue(6,pnd_Ws_Pnd_S2)));
            pnd_Cmpy_Totals_Pnd_Tot_Fed_Tax.compute(new ComputeParameters(false, pnd_Cmpy_Totals_Pnd_Tot_Fed_Tax), pnd_Cntl_Pnd_Fed_Tax.getValue(6,pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Fed_Tax_F.getValue(6, //Natural: ASSIGN #TOT-FED-TAX := #CNTL.#FED-TAX ( 6,#S2 ) + #CNTL.#FED-TAX-F ( 6,#S2 )
                pnd_Ws_Pnd_S2)));
            pnd_Cmpy_Totals_Pnd_Tot_Nra_Tax.compute(new ComputeParameters(false, pnd_Cmpy_Totals_Pnd_Tot_Nra_Tax), pnd_Cntl_Pnd_Nra_Tax.getValue(6,pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Nra_Tax_F.getValue(6, //Natural: ASSIGN #TOT-NRA-TAX := #CNTL.#NRA-TAX ( 6,#S2 ) + #CNTL.#NRA-TAX-F ( 6,#S2 )
                pnd_Ws_Pnd_S2)));
            pnd_Cmpy_Totals_Pnd_Tot_Nra_Refund.compute(new ComputeParameters(false, pnd_Cmpy_Totals_Pnd_Tot_Nra_Refund), pnd_Cntl_Pnd_Nra_Refund.getValue(6,              //Natural: ASSIGN #TOT-NRA-REFUND := #CNTL.#NRA-REFUND ( 6,#S2 ) + #CNTL.#NRA-REFUND-F ( 6,#S2 )
                pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Nra_Refund_F.getValue(6,pnd_Ws_Pnd_S2)));
            pnd_Cmpy_Totals_Pnd_Tot_Int_Amt.compute(new ComputeParameters(false, pnd_Cmpy_Totals_Pnd_Tot_Int_Amt), pnd_Cntl_Pnd_Int_Amt.getValue(6,pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Int_Amt_F.getValue(6, //Natural: ASSIGN #TOT-INT-AMT := #CNTL.#INT-AMT ( 6,#S2 ) + #CNTL.#INT-AMT-F ( 6,#S2 )
                pnd_Ws_Pnd_S2)));
        }                                                                                                                                                                 //Natural: END-IF
        //*  GRAND TOTAL
        if (condition(pnd_Ws_Pnd_S2.equals(2)))                                                                                                                           //Natural: IF #S2 = 2
        {
            pnd_Grnd_Totals_Pnd_Grd_Gross_Amt.compute(new ComputeParameters(false, pnd_Grnd_Totals_Pnd_Grd_Gross_Amt), pnd_Cntl_Pnd_Gross_Amt.getValue(6,                 //Natural: ASSIGN #GRD-GROSS-AMT := #CNTL.#GROSS-AMT ( 6,#S2 ) + #CNTL.#GROSS-AMT-F ( 6,#S2 )
                pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Gross_Amt_F.getValue(6,pnd_Ws_Pnd_S2)));
            pnd_Grnd_Totals_Pnd_Grd_Ivc_Amt.compute(new ComputeParameters(false, pnd_Grnd_Totals_Pnd_Grd_Ivc_Amt), pnd_Cntl_Pnd_Ivc_Amt.getValue(6,pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Ivc_Amt_F.getValue(6, //Natural: ASSIGN #GRD-IVC-AMT := #CNTL.#IVC-AMT ( 6,#S2 ) + #CNTL.#IVC-AMT-F ( 6,#S2 )
                pnd_Ws_Pnd_S2)));
            pnd_Grnd_Totals_Pnd_Grd_Taxable_Amt.compute(new ComputeParameters(false, pnd_Grnd_Totals_Pnd_Grd_Taxable_Amt), pnd_Cntl_Pnd_Taxable_Amt.getValue(6,           //Natural: ASSIGN #GRD-TAXABLE-AMT := #CNTL.#TAXABLE-AMT ( 6,#S2 ) + #CNTL.#TAXABLE-AMT-F ( 6,#S2 )
                pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Taxable_Amt_F.getValue(6,pnd_Ws_Pnd_S2)));
            pnd_Grnd_Totals_Pnd_Grd_Fed_Tax.compute(new ComputeParameters(false, pnd_Grnd_Totals_Pnd_Grd_Fed_Tax), pnd_Cntl_Pnd_Fed_Tax.getValue(6,pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Fed_Tax_F.getValue(6, //Natural: ASSIGN #GRD-FED-TAX := #CNTL.#FED-TAX ( 6,#S2 ) + #CNTL.#FED-TAX-F ( 6,#S2 )
                pnd_Ws_Pnd_S2)));
            pnd_Grnd_Totals_Pnd_Grd_Nra_Tax.compute(new ComputeParameters(false, pnd_Grnd_Totals_Pnd_Grd_Nra_Tax), pnd_Cntl_Pnd_Nra_Tax.getValue(6,pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Nra_Tax_F.getValue(6, //Natural: ASSIGN #GRD-NRA-TAX := #CNTL.#NRA-TAX ( 6,#S2 ) + #CNTL.#NRA-TAX-F ( 6,#S2 )
                pnd_Ws_Pnd_S2)));
            pnd_Grnd_Totals_Pnd_Grd_Nra_Refund.compute(new ComputeParameters(false, pnd_Grnd_Totals_Pnd_Grd_Nra_Refund), pnd_Cntl_Pnd_Nra_Refund.getValue(6,              //Natural: ASSIGN #GRD-NRA-REFUND := #CNTL.#NRA-REFUND ( 6,#S2 ) + #CNTL.#NRA-REFUND-F ( 6,#S2 )
                pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Nra_Refund_F.getValue(6,pnd_Ws_Pnd_S2)));
            pnd_Grnd_Totals_Pnd_Grd_Int_Amt.compute(new ComputeParameters(false, pnd_Grnd_Totals_Pnd_Grd_Int_Amt), pnd_Cntl_Pnd_Int_Amt.getValue(6,pnd_Ws_Pnd_S2).add(pnd_Cntl_Pnd_Int_Amt_F.getValue(6, //Natural: ASSIGN #GRD-INT-AMT := #CNTL.#INT-AMT ( 6,#S2 ) + #CNTL.#INT-AMT-F ( 6,#S2 )
                pnd_Ws_Pnd_S2)));
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #WS.#S1 = 1 TO #CNTL-MAX
        for (pnd_Ws_Pnd_S1.setValue(1); condition(pnd_Ws_Pnd_S1.lessOrEqual(pnd_Ws_Const_Pnd_Cntl_Max)); pnd_Ws_Pnd_S1.nadd(1))
        {
            pnd_Ws_Pnd_Tran_Cv.setValue(pnd_Cntl_Pnd_Tran_Cnt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                //Natural: ASSIGN #WS.#TRAN-CV := #CNTL.#TRAN-CNT-CV ( #S1 )
            pnd_Ws_Pnd_Gross_Cv.setValue(pnd_Cntl_Pnd_Gross_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                              //Natural: ASSIGN #WS.#GROSS-CV := #CNTL.#GROSS-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Ivc_Cv.setValue(pnd_Cntl_Pnd_Ivc_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#IVC-CV := #CNTL.#IVC-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Taxable_Cv.setValue(pnd_Cntl_Pnd_Taxable_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                          //Natural: ASSIGN #WS.#TAXABLE-CV := #CNTL.#TAXABLE-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Fed_Cv.setValue(pnd_Cntl_Pnd_Fed_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#FED-CV := #CNTL.#FED-TAX-CV ( #S1 )
            pnd_Ws_Pnd_Nra_Cv.setValue(pnd_Cntl_Pnd_Nra_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#NRA-CV := #CNTL.#NRA-TAX-CV ( #S1 )
            pnd_Ws_Pnd_Refund_Cv.setValue(pnd_Cntl_Pnd_Nra_Refund_Cv.getValue(pnd_Ws_Pnd_S1));                                                                            //Natural: ASSIGN #WS.#REFUND-CV := #CNTL.#NRA-REFUND-CV ( #S1 )
            pnd_Ws_Pnd_Int_Cv.setValue(pnd_Cntl_Pnd_Int_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#INT-CV := #CNTL.#INT-AMT-CV ( #S1 )
            getReports().display(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",                         //Natural: DISPLAY ( 1 ) ( HC = R ES = ON ) '/' #CNTL-TEXT ( #S1 ) / '/' #CNTL-TEXT1 ( #S1 ) '/Trans Count' #CNTL.#TRAN-CNT ( #S1,#S2 ) ( CV = #TRAN-CV ) 'Gross Amount' #CNTL.#GROSS-AMT ( #S1,#S2 ) ( CV = #GROSS-CV ) / 3X 'Interest' #CNTL.#INT-AMT ( #S1,#S2 ) ( CV = #INT-CV ) '/IVC Amount' #CNTL.#IVC-AMT ( #S1,#S2 ) ( CV = #IVC-CV ) '/Taxable Amount' #CNTL.#TAXABLE-AMT ( #S1,#S2 ) ( CV = #TAXABLE-CV ) 'Non-FATCA Tax' #CNTL.#NRA-TAX ( #S1,#S2 ) ( CV = #NRA-CV ) / 'Amount Repaid' #CNTL.#NRA-REFUND ( #S1,#S2 ) ( CV = #REFUND-CV ) '/Federal Tax' #CNTL.#FED-TAX ( #S1,#S2 ) ( CV = #FED-CV )
            		pnd_Cntl_Pnd_Cntl_Text.getValue(pnd_Ws_Pnd_S1),NEWLINE,"/",
            		pnd_Cntl_Pnd_Cntl_Text1.getValue(pnd_Ws_Pnd_S1),"/Trans Count",
            		pnd_Cntl_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Tran_Cv,"Gross Amount",
            		pnd_Cntl_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Gross_Cv,NEWLINE,new ColumnSpacing(3),"Interest",
            		pnd_Cntl_Pnd_Int_Amt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Int_Cv,"/IVC Amount",
            		pnd_Cntl_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Ivc_Cv,"/Taxable Amount",
            		pnd_Cntl_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Taxable_Cv,"Non-FATCA Tax",
            		pnd_Cntl_Pnd_Nra_Tax.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Nra_Cv,NEWLINE,"Amount Repaid",
            		pnd_Cntl_Pnd_Nra_Refund.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Refund_Cv,"/Federal Tax",
            		pnd_Cntl_Pnd_Fed_Tax.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Fed_Cv);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1 LINES
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  11/2014 START
        getReports().write(1, ReportOption.NOTITLE,pnd_Dashes);                                                                                                           //Natural: WRITE ( 1 ) #DASHES
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(47),"Gross Amount",new TabSetting(101),"FATCA Tax",NEWLINE,new TabSetting(29),"Trans Count",new  //Natural: WRITE ( 1 ) // 47T 'Gross Amount' 101T 'FATCA Tax' / 29T 'Trans Count' 51T 'Interest' 65T 'IVC Amount' 80T 'Taxable Amount' 97T 'Amount Repaid' 115T 'Federal Tax' / 29T '-----------' 41T '------------------' 60T '---------------' 76T '------------------' 95T '---------------' 111T '---------------'
            TabSetting(51),"Interest",new TabSetting(65),"IVC Amount",new TabSetting(80),"Taxable Amount",new TabSetting(97),"Amount Repaid",new TabSetting(115),"Federal Tax",NEWLINE,new 
            TabSetting(29),"-----------",new TabSetting(41),"------------------",new TabSetting(60),"---------------",new TabSetting(76),"------------------",new 
            TabSetting(95),"---------------",new TabSetting(111),"---------------");
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #WS.#S1 = 1 TO #CNTL-MAX
        for (pnd_Ws_Pnd_S1.setValue(1); condition(pnd_Ws_Pnd_S1.lessOrEqual(pnd_Ws_Const_Pnd_Cntl_Max)); pnd_Ws_Pnd_S1.nadd(1))
        {
            pnd_Ws_Pnd_Tran_Cv.setValue(pnd_Cntl_Pnd_Tran_Cnt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                //Natural: ASSIGN #WS.#TRAN-CV := #CNTL.#TRAN-CNT-CV ( #S1 )
            pnd_Ws_Pnd_Gross_Cv.setValue(pnd_Cntl_Pnd_Gross_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                              //Natural: ASSIGN #WS.#GROSS-CV := #CNTL.#GROSS-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Ivc_Cv.setValue(pnd_Cntl_Pnd_Ivc_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#IVC-CV := #CNTL.#IVC-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Taxable_Cv.setValue(pnd_Cntl_Pnd_Taxable_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                          //Natural: ASSIGN #WS.#TAXABLE-CV := #CNTL.#TAXABLE-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Fed_Cv.setValue(pnd_Cntl_Pnd_Fed_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#FED-CV := #CNTL.#FED-TAX-CV ( #S1 )
            pnd_Ws_Pnd_Nra_Cv.setValue(pnd_Cntl_Pnd_Nra_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#NRA-CV := #CNTL.#NRA-TAX-CV ( #S1 )
            pnd_Ws_Pnd_Refund_Cv.setValue(pnd_Cntl_Pnd_Nra_Refund_Cv.getValue(pnd_Ws_Pnd_S1));                                                                            //Natural: ASSIGN #WS.#REFUND-CV := #CNTL.#NRA-REFUND-CV ( #S1 )
            pnd_Ws_Pnd_Int_Cv.setValue(pnd_Cntl_Pnd_Int_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#INT-CV := #CNTL.#INT-AMT-CV ( #S1 )
            getReports().write(1, ReportOption.NOTITLE,pnd_Cntl_Pnd_Cntl_Text.getValue(pnd_Ws_Pnd_S1),new TabSetting(28),pnd_Cntl_Pnd_Tran_Cnt_F.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2),  //Natural: WRITE ( 1 ) #CNTL-TEXT ( #S1 ) 28T #CNTL.#TRAN-CNT-F ( #S1,#S2 ) ( CV = #TRAN-CV ) 41T #CNTL.#GROSS-AMT-F ( #S1,#S2 ) ( CV = #GROSS-CV ) 60T #CNTL.#IVC-AMT-F ( #S1,#S2 ) ( CV = #IVC-CV ) 76T #CNTL.#TAXABLE-AMT-F ( #S1,#S2 ) ( CV = #TAXABLE-CV ) 95T #CNTL.#NRA-TAX-F ( #S1,#S2 ) ( CV = #NRA-CV ) 111T #CNTL.#FED-TAX-F ( #S1,#S2 ) ( CV = #FED-CV ) / 44T #CNTL.#INT-AMT-F ( #S1,#S2 ) ( CV = #INT-CV ) 95T #CNTL.#NRA-REFUND-F ( #S1,#S2 ) ( CV = #REFUND-CV ) / #CNTL-TEXT1 ( #S1 )
                pnd_Ws_Pnd_Tran_Cv, new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),new TabSetting(41),pnd_Cntl_Pnd_Gross_Amt_F.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), 
                pnd_Ws_Pnd_Gross_Cv, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),pnd_Cntl_Pnd_Ivc_Amt_F.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), 
                pnd_Ws_Pnd_Ivc_Cv, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(76),pnd_Cntl_Pnd_Taxable_Amt_F.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), 
                pnd_Ws_Pnd_Taxable_Cv, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(95),pnd_Cntl_Pnd_Nra_Tax_F.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), 
                pnd_Ws_Pnd_Nra_Cv, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(111),pnd_Cntl_Pnd_Fed_Tax_F.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), 
                pnd_Ws_Pnd_Fed_Cv, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(44),pnd_Cntl_Pnd_Int_Amt_F.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), 
                pnd_Ws_Pnd_Int_Cv, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(95),pnd_Cntl_Pnd_Nra_Refund_F.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), 
                pnd_Ws_Pnd_Refund_Cv, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,pnd_Cntl_Pnd_Cntl_Text1.getValue(pnd_Ws_Pnd_S1));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1 LINES
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,pnd_Dashes);                                                                                                           //Natural: WRITE ( 1 ) #DASHES
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_S2.equals(1)))                                                                                                                           //Natural: IF #S2 = 1
        {
            getReports().write(1, ReportOption.NOTITLE,"Company Total",new TabSetting(41),pnd_Cmpy_Totals_Pnd_Tot_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 1 ) 'Company Total' 41T #TOT-GROSS-AMT 60T #TOT-IVC-AMT 76T #TOT-TAXABLE-AMT 95T #TOT-NRA-TAX 111T #TOT-FED-TAX / 44T #TOT-INT-AMT 95T #TOT-NRA-REFUND
                TabSetting(60),pnd_Cmpy_Totals_Pnd_Tot_Ivc_Amt, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(76),pnd_Cmpy_Totals_Pnd_Tot_Taxable_Amt, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(95),pnd_Cmpy_Totals_Pnd_Tot_Nra_Tax, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(111),pnd_Cmpy_Totals_Pnd_Tot_Fed_Tax, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(44),pnd_Cmpy_Totals_Pnd_Tot_Int_Amt, 
                new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(95),pnd_Cmpy_Totals_Pnd_Tot_Nra_Refund, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Pnd_S2.equals(2)))                                                                                                                           //Natural: IF #S2 = 2
        {
            getReports().write(1, ReportOption.NOTITLE,"Grand Total",new TabSetting(41),pnd_Grnd_Totals_Pnd_Grd_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 1 ) 'Grand Total' 41T #GRD-GROSS-AMT 60T #GRD-IVC-AMT 76T #GRD-TAXABLE-AMT 95T #GRD-NRA-TAX 111T #GRD-FED-TAX / 44T #GRD-INT-AMT 95T #GRD-NRA-REFUND
                TabSetting(60),pnd_Grnd_Totals_Pnd_Grd_Ivc_Amt, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(76),pnd_Grnd_Totals_Pnd_Grd_Taxable_Amt, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(95),pnd_Grnd_Totals_Pnd_Grd_Nra_Tax, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(111),pnd_Grnd_Totals_Pnd_Grd_Fed_Tax, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(44),pnd_Grnd_Totals_Pnd_Grd_Int_Amt, 
                new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(95),pnd_Grnd_Totals_Pnd_Grd_Nra_Refund, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  11/2014 END
    }
    private void sub_Process_Company() throws Exception                                                                                                                   //Natural: PROCESS-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        DbsUtil.callnat(Twrncomp.class , getCurrentProcessState(), pdaTwracomp.getPnd_Twracomp_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracomp.getPnd_Twracomp_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOMP' USING #TWRACOMP.#INPUT-PARMS ( AD = O ) #TWRACOMP.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Process_Control_Record() throws Exception                                                                                                            //Natural: PROCESS-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        getWorkFiles().read(3, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                             //Natural: READ WORK FILE 03 ONCE #TWRP0600-CONTROL-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Control Record is Empty",new TabSetting(77),"***");                                      //Natural: WRITE ( 1 ) '***' 25T 'Control Record is Empty' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(101);  if (true) return;                                                                                                                    //Natural: TERMINATE 101
        }                                                                                                                                                                 //Natural: END-ENDFILE
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaTwrl0900_getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_FormIsBreak = ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form().isBreak(endOfData);
        if (condition(ldaTwrl0900_getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_FormIsBreak))
        {
            pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(readWork01Twrpymnt_Company_Cde_FormOld);                                                                 //Natural: ASSIGN #TWRACOMP.#COMP-CODE := OLD ( #XTAXYR-F94.TWRPYMNT-COMPANY-CDE-FORM )
            pdaTwracomp.getPnd_Twracomp_Pnd_Tax_Year().setValue(readWork01Twrpymnt_Tax_YearOld);                                                                          //Natural: ASSIGN #TWRACOMP.#TAX-YEAR := OLD ( #XTAXYR-F94.TWRPYMNT-TAX-YEAR )
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMPANY
            sub_Process_Company();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Pnd_Company_Line.setValue("Company:");                                                                                                                 //Natural: ASSIGN #WS.#COMPANY-LINE := 'Company:'
            setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),pnd_Ws_Pnd_Company_Line,11,4);                                                          //Natural: MOVE #TWRACOMP.#COMP-SHORT-NAME TO SUBSTR ( #WS.#COMPANY-LINE,11,4 )
            setValueToSubstring("-",pnd_Ws_Pnd_Company_Line,16,1);                                                                                                        //Natural: MOVE '-' TO SUBSTR ( #WS.#COMPANY-LINE,16,1 )
            setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Name(),pnd_Ws_Pnd_Company_Line,18,55);                                                               //Natural: MOVE #TWRACOMP.#COMP-NAME TO SUBSTR ( #WS.#COMPANY-LINE,18,55 )
            pnd_Ws_Pnd_S2.setValue(1);                                                                                                                                    //Natural: ASSIGN #WS.#S2 := 1
                                                                                                                                                                          //Natural: PERFORM WRITE-NRA-REPORT
            sub_Write_Nra_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Cntl_Pnd_Tran_Cnt.getValue("*",2).nadd(pnd_Cntl_Pnd_Tran_Cnt.getValue("*",1));                                                                            //Natural: ADD #CNTL.#TRAN-CNT ( *,1 ) TO #CNTL.#TRAN-CNT ( *,2 )
            pnd_Cntl_Pnd_Gross_Amt.getValue("*",2).nadd(pnd_Cntl_Pnd_Gross_Amt.getValue("*",1));                                                                          //Natural: ADD #CNTL.#GROSS-AMT ( *,1 ) TO #CNTL.#GROSS-AMT ( *,2 )
            pnd_Cntl_Pnd_Ivc_Amt.getValue("*",2).nadd(pnd_Cntl_Pnd_Ivc_Amt.getValue("*",1));                                                                              //Natural: ADD #CNTL.#IVC-AMT ( *,1 ) TO #CNTL.#IVC-AMT ( *,2 )
            pnd_Cntl_Pnd_Taxable_Amt.getValue("*",2).nadd(pnd_Cntl_Pnd_Taxable_Amt.getValue("*",1));                                                                      //Natural: ADD #CNTL.#TAXABLE-AMT ( *,1 ) TO #CNTL.#TAXABLE-AMT ( *,2 )
            pnd_Cntl_Pnd_Fed_Tax.getValue("*",2).nadd(pnd_Cntl_Pnd_Fed_Tax.getValue("*",1));                                                                              //Natural: ADD #CNTL.#FED-TAX ( *,1 ) TO #CNTL.#FED-TAX ( *,2 )
            pnd_Cntl_Pnd_Nra_Tax.getValue("*",2).nadd(pnd_Cntl_Pnd_Nra_Tax.getValue("*",1));                                                                              //Natural: ADD #CNTL.#NRA-TAX ( *,1 ) TO #CNTL.#NRA-TAX ( *,2 )
            pnd_Cntl_Pnd_Nra_Refund.getValue("*",2).nadd(pnd_Cntl_Pnd_Nra_Refund.getValue("*",1));                                                                        //Natural: ADD #CNTL.#NRA-REFUND ( *,1 ) TO #CNTL.#NRA-REFUND ( *,2 )
            pnd_Cntl_Pnd_Int_Amt.getValue("*",2).nadd(pnd_Cntl_Pnd_Int_Amt.getValue("*",1));                                                                              //Natural: ADD #CNTL.#INT-AMT ( *,1 ) TO #CNTL.#INT-AMT ( *,2 )
            //*  11/2014 START
            pnd_Cntl_Pnd_Tran_Cnt_F.getValue("*",2).nadd(pnd_Cntl_Pnd_Tran_Cnt_F.getValue("*",1));                                                                        //Natural: ADD #CNTL.#TRAN-CNT-F ( *,1 ) TO #CNTL.#TRAN-CNT-F ( *,2 )
            pnd_Cntl_Pnd_Gross_Amt_F.getValue("*",2).nadd(pnd_Cntl_Pnd_Gross_Amt_F.getValue("*",1));                                                                      //Natural: ADD #CNTL.#GROSS-AMT-F ( *,1 ) TO #CNTL.#GROSS-AMT-F ( *,2 )
            pnd_Cntl_Pnd_Ivc_Amt_F.getValue("*",2).nadd(pnd_Cntl_Pnd_Ivc_Amt_F.getValue("*",1));                                                                          //Natural: ADD #CNTL.#IVC-AMT-F ( *,1 ) TO #CNTL.#IVC-AMT-F ( *,2 )
            pnd_Cntl_Pnd_Taxable_Amt_F.getValue("*",2).nadd(pnd_Cntl_Pnd_Taxable_Amt_F.getValue("*",1));                                                                  //Natural: ADD #CNTL.#TAXABLE-AMT-F ( *,1 ) TO #CNTL.#TAXABLE-AMT-F ( *,2 )
            pnd_Cntl_Pnd_Fed_Tax_F.getValue("*",2).nadd(pnd_Cntl_Pnd_Fed_Tax_F.getValue("*",1));                                                                          //Natural: ADD #CNTL.#FED-TAX-F ( *,1 ) TO #CNTL.#FED-TAX-F ( *,2 )
            pnd_Cntl_Pnd_Nra_Tax_F.getValue("*",2).nadd(pnd_Cntl_Pnd_Nra_Tax_F.getValue("*",1));                                                                          //Natural: ADD #CNTL.#NRA-TAX-F ( *,1 ) TO #CNTL.#NRA-TAX-F ( *,2 )
            pnd_Cntl_Pnd_Nra_Refund_F.getValue("*",2).nadd(pnd_Cntl_Pnd_Nra_Refund_F.getValue("*",1));                                                                    //Natural: ADD #CNTL.#NRA-REFUND-F ( *,1 ) TO #CNTL.#NRA-REFUND-F ( *,2 )
            pnd_Cntl_Pnd_Int_Amt_F.getValue("*",2).nadd(pnd_Cntl_Pnd_Int_Amt_F.getValue("*",1));                                                                          //Natural: ADD #CNTL.#INT-AMT-F ( *,1 ) TO #CNTL.#INT-AMT-F ( *,2 )
            //*  11/2014 END
            pnd_Cntl_Pnd_Totals.getValue("*",1).reset();                                                                                                                  //Natural: RESET #CNTL.#TOTALS ( *,1 )
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=133 ZP=ON");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(49),"Summary of NRA Payment Transactions",new TabSetting(120),"Report: RPT1",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",
            
        		pnd_Cntl_Pnd_Cntl_Text,NEWLINE,"/",
        		pnd_Cntl_Pnd_Cntl_Text1,"/Trans Count",
        		pnd_Cntl_Pnd_Tran_Cnt, pnd_Ws_Pnd_Tran_Cv,"Gross Amount",
        		pnd_Cntl_Pnd_Gross_Amt, pnd_Ws_Pnd_Gross_Cv,NEWLINE,new ColumnSpacing(3),"Interest",
        		pnd_Cntl_Pnd_Int_Amt, pnd_Ws_Pnd_Int_Cv,"/IVC Amount",
        		pnd_Cntl_Pnd_Ivc_Amt, pnd_Ws_Pnd_Ivc_Cv,"/Taxable Amount",
        		pnd_Cntl_Pnd_Taxable_Amt, pnd_Ws_Pnd_Taxable_Cv,"Non-FATCA Tax",
        		pnd_Cntl_Pnd_Nra_Tax, pnd_Ws_Pnd_Nra_Cv,NEWLINE,"Amount Repaid",
        		pnd_Cntl_Pnd_Nra_Refund, pnd_Ws_Pnd_Refund_Cv,"/Federal Tax",
        		pnd_Cntl_Pnd_Fed_Tax, pnd_Ws_Pnd_Fed_Cv);
    }
    private void CheckAtStartofData248() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Ws_Pnd_Tax_Year.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());                                                                              //Natural: ASSIGN #WS.#TAX-YEAR := #XTAXYR-F94.TWRPYMNT-TAX-YEAR
            //*  BK
        }                                                                                                                                                                 //Natural: END-START
    }
}
