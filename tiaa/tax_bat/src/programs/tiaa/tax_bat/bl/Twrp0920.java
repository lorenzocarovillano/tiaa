/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:31:56 PM
**        * FROM NATURAL PROGRAM : Twrp0920
************************************************************
**        * FILE NAME            : Twrp0920.java
**        * CLASS NAME           : Twrp0920
**        * INSTANCE NAME        : Twrp0920
************************************************************
******************************************************************
* PROGRAM   : TWRP0920
* FUNCTION  : TO CREATE A FLAT FILE FOR TWRP0925 AND TWRP0930
* INPUT     : EXTRACTED RECORDS FROM TAX PAYMENT FILE BY TAX YEAR
* HISTORY   :
* 11/23/98  : THIS REFERS TO INTERFACE-DATE INSTEAD OF PAYMENT-DATE
*                 TO SELECT RECORDS TO PROCESS FOR 'D' RUN
* 2/3/99    : TO USE SUMMARY AMT FIELDS FOR A REGULAR YTD AND
*                PROCESS BY PYMNT FOR A SPECIAL OR DAILY RUN
* 2/8/99    : TO USE IVC-IND (SUMM.LEVEL) FOR REGULAR YTD AND
*                CHECK VALUE OF IVC-IND (PYMNT LEVEL- PE) FOR A
*                SPECIAL OR DAILY RUN (FOR MIXED VALUE OF IVC-IND
*                USE IVC-IND IN THE SUMM. LEVEL ELSE USE WHAT IS
*                BEING READ FROM THE PYMNT LEVEL)
*
* 3/4/99   : IF IT'S REGULAR YTD - MAKE A SUMMARY RECORD USING THE
*                 SUMMARY FIELDS
*            FOR  SPECIAL YTD AND DAILY RUN - PROCESSING WILL BE BY
*               PAYMENT, AND IF SELECTED PAYMENT RECORDS HAVE DIFF.
*               IVC INDICATOR THE OUTPUT RECORD WILL USE THE IVC
*               INDICATOR IN THE SUMMARY LEVEL AND ALL AMOUNTS WILL
*               BE ADDED TO COME OUT WITH THE SUMMARIZED AMT FIELDS.
*
* 3/11/99  : IF REGULAR YTD  & SUMMARY IVC-IND = 'M'  CREATE AN
*              OUTPUT FILE CONTAINING ALL 'K'NOWN PAYMENTS ONLY.
*            THIS OUTPUT FILE IS ANOTHER INPUT OF PROGRAM TWRP0920
*
* 6/3/99   : STOWED DUE TO UPDATED TWRL0900
*
* 6/15/99  : FOR REGULAR YTD:
*              IF SUMMARY IVC-IND = 'M' WRITE ALSO TO FLAT-FILE5 ALL
*                RECORDS WITH ADJUSTMENT DUE TO IA MAINTENANCE (WHERE
*                IVC-PROTECT IS ON/TRUE).
*
* 6/28/99  : DUE TO PERMANENT PE GROUP SOLUTION (OCC. OF PE FLDS. = 24)
*             THIS WILL READ A SORTED EXTRACTED PAYMENT FILE BY TAX YR.,
*             SORTED BY COMPANY, TIN, CONTRACT#/PAYEE CODE, PAYMENT
*             PAYMENT CATEGORY, DISTRIBUTION CODE & RESIDENCY CODE.
*          : UPDATED PROGRAM DUE TO PERMANENT PE GROUP SOLUTION.
*              - A NEW RECORD WILL BE CREATED IF EXISTING RECORD EXCEEDS
*                  24 OCC (PAYMENTS).
*              - SUMMARY 'financial amount' FIELDS OF EACH RELATED REC.
*                  CONTAINS THE TOTAL AMOUNT OF ALL PAYMENT WITHIN
*                  A RECORD
*              - SUMMARY 'non-financial' FIELDS ARE THE SAME TO ALL
*                   RELATED RECORDS BUT SUMMARY IVC IND FOR ALL RELATED
*                   RECORDS CAN ONLY BE FOUND ON THE FIRST & LAST RECORD
* 7/08/99  : CREATE AN OUTPUT FILE CONTAINING ALL 'K'NOWN PAYMENTS FOR
*              YTD RUN (REGULAR & SPECIAL) & SUMMARY IVC-IND = 'M' .
* 10/9/99  : NEW PROCESSING AND COMPUTATION OF TAXABLE AMT
* EDITH        A) NRA - PROCESS BY PAYMENT
*                       IVC-IND = 'u'  TAXABLE AMT = GROSS AMT
*                       IVC-IND = 'k'  TAXABLE AMT = GROSS AMT - IVC AMT
*              B) FEDERAL - PROCESS USING SUMMARY FIELDS
*                       IVC-IND = U/M  TAXABLE AMT = 0
*                       IVC-IND = U(IVC-PROTECTED) OR K ,
*                                      TAXABLE AMT = GROSS AMT - IVC AMT
*          : IN CASE OF BLANK SUMMARY IVC-IND
*              A) CHECK ALL ACTIVE PAYMENTS TO DEFINE THE VALUE OF
*                     SUMMARY IVC-IND
*          : INCLUSION OF NEW COMPANY, TIAA-LIFE
*          : INCLUDED TAX-CITIZENSHIP AS KEY IN SUMMARIZING RELATED
*                  PHYSICAL RECORDS.
* 2/28/02  : REMOVE DEAD CODE  - J.ROTHOLZ & F.CHIN
* 04/02/02 : ADD NEW COMPANY - TCII (J.ROTHOLZ)
* 01/06/03 : INCREASE SIZE OF SOME COUNTERS (M.NACHBER)
* 01/08/03 : MARINA NACHBER
*            RECOMPILED TO PICK UP TWRL0905 THAT IS A COPY OF THE
*            LATEST VERSION OF TWRL0904
* 10/14/03:STOWED DUE TO UPDATE ON TWRL0900-ADDED CONTRACT TYPE & IRC-TO
* 09/21/04 RM TOPS RELEASE 3 CHANGES
*             ADD NEW COMPANY CODE 'X' FOR TRUST COMPANY AND COUNTER FOR
*             'TRST'.
* 10/18/11  JB NON-ERISA TDA - ADD COMPANY CODE 'F' SCAN JB1011
* 02/18/15: OS - RECOMPILED FOR UPDATED TWRL0900
******************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0920 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0901 ldaTwrl0901;
    private LdaTwrl0905 ldaTwrl0905;
    private LdaTwrl0900 ldaTwrl0900;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Flat_File5;
    private DbsField pnd_Flat_File5_Pnd_Ff5_Company_Code;
    private DbsField pnd_Flat_File5_Pnd_Ff5_Tin;
    private DbsField pnd_Flat_File5_Pnd_Ff5_Contract_Nbr;
    private DbsField pnd_Flat_File5_Pnd_Ff5_Tax_Citizenship;
    private DbsField pnd_Flat_File5_Pnd_Ff5_Payset_Type;
    private DbsField pnd_Flat_File5_Pnd_Ff5_Orig_Srce_Cde;
    private DbsField pnd_Flat_File5_Pnd_Ff5_Updt_Srce_Cde;
    private DbsField pnd_Flat_File5_Pnd_Ff5_Gross_Amt;
    private DbsField pnd_Flat_File5_Pnd_Ff5_Ivc_Amt;
    private DbsField pnd_Flat_File5_Pnd_Ff5_Ivc_Ind;
    private DbsField pnd_Flat_File5_Pnd_Ff5_Ivc_Protect;
    private DbsField pnd_Tax_Year;
    private DbsField pnd_Rec_Read;
    private DbsField pnd_Rec_Ritten;
    private DbsField pnd_Ff5_Rec_Ritten;
    private DbsField pnd_Cref_Cnt;
    private DbsField pnd_Life_Cnt;
    private DbsField pnd_Tiaa_Cnt;
    private DbsField pnd_Tcii_Cnt;
    private DbsField pnd_Trst_Cnt;
    private DbsField pnd_Fsb_Cnt;
    private DbsField pnd_Othr_Cnt;
    private DbsField pnd_Cnt;
    private DbsField pnd_Start_Date;
    private DbsField pnd_End_Date;
    private DbsField pnd_Ytd_Run_Type;
    private DbsField pnd_Freq;
    private DbsField pnd_I;
    private DbsField pnd_Diff_Ivc_Ind;
    private DbsField pnd_Pass;
    private DbsField pnd_First_Rec;
    private DbsField pnd_Mixed_Ind;
    private DbsField pnd_Sytd_Daily;
    private DbsField pnd_S_Ytd;
    private DbsField pnd_Sv_Ivc_Ind;

    private DbsGroup pnd_Savers;
    private DbsField pnd_Savers_Pnd_Sv_Co;
    private DbsField pnd_Savers_Pnd_Sv_Tin;
    private DbsField pnd_Savers_Pnd_Sv_Contractpnd;
    private DbsField pnd_Savers_Pnd_Sv_Payee;
    private DbsField pnd_Savers_Pnd_Sv_Pay_Ctgry;
    private DbsField pnd_Savers_Pnd_Sv_Dis_Cde;
    private DbsField pnd_Savers_Pnd_Sv_Res_Cde;
    private DbsField pnd_Savers_Pnd_Sv_Tax_Ctz;

    private DbsGroup pnd_Summ_Acc;
    private DbsField pnd_Summ_Acc_Pnd_Sum_Amt1;
    private DbsField pnd_Summ_Acc_Pnd_Sum_Amts;
    private DbsField pnd_Summ_Acc_Pnd_Sum_Cnt;
    private DbsField pnd_Error;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0901 = new LdaTwrl0901();
        registerRecord(ldaTwrl0901);
        ldaTwrl0905 = new LdaTwrl0905();
        registerRecord(ldaTwrl0905);
        ldaTwrl0900 = new LdaTwrl0900();
        registerRecord(ldaTwrl0900);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Flat_File5 = localVariables.newGroupInRecord("pnd_Flat_File5", "#FLAT-FILE5");
        pnd_Flat_File5_Pnd_Ff5_Company_Code = pnd_Flat_File5.newFieldInGroup("pnd_Flat_File5_Pnd_Ff5_Company_Code", "#FF5-COMPANY-CODE", FieldType.STRING, 
            1);
        pnd_Flat_File5_Pnd_Ff5_Tin = pnd_Flat_File5.newFieldInGroup("pnd_Flat_File5_Pnd_Ff5_Tin", "#FF5-TIN", FieldType.STRING, 10);
        pnd_Flat_File5_Pnd_Ff5_Contract_Nbr = pnd_Flat_File5.newFieldInGroup("pnd_Flat_File5_Pnd_Ff5_Contract_Nbr", "#FF5-CONTRACT-NBR", FieldType.STRING, 
            8);
        pnd_Flat_File5_Pnd_Ff5_Tax_Citizenship = pnd_Flat_File5.newFieldInGroup("pnd_Flat_File5_Pnd_Ff5_Tax_Citizenship", "#FF5-TAX-CITIZENSHIP", FieldType.STRING, 
            1);
        pnd_Flat_File5_Pnd_Ff5_Payset_Type = pnd_Flat_File5.newFieldInGroup("pnd_Flat_File5_Pnd_Ff5_Payset_Type", "#FF5-PAYSET-TYPE", FieldType.STRING, 
            2);
        pnd_Flat_File5_Pnd_Ff5_Orig_Srce_Cde = pnd_Flat_File5.newFieldInGroup("pnd_Flat_File5_Pnd_Ff5_Orig_Srce_Cde", "#FF5-ORIG-SRCE-CDE", FieldType.STRING, 
            2);
        pnd_Flat_File5_Pnd_Ff5_Updt_Srce_Cde = pnd_Flat_File5.newFieldInGroup("pnd_Flat_File5_Pnd_Ff5_Updt_Srce_Cde", "#FF5-UPDT-SRCE-CDE", FieldType.STRING, 
            2);
        pnd_Flat_File5_Pnd_Ff5_Gross_Amt = pnd_Flat_File5.newFieldInGroup("pnd_Flat_File5_Pnd_Ff5_Gross_Amt", "#FF5-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Flat_File5_Pnd_Ff5_Ivc_Amt = pnd_Flat_File5.newFieldInGroup("pnd_Flat_File5_Pnd_Ff5_Ivc_Amt", "#FF5-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 
            2);
        pnd_Flat_File5_Pnd_Ff5_Ivc_Ind = pnd_Flat_File5.newFieldInGroup("pnd_Flat_File5_Pnd_Ff5_Ivc_Ind", "#FF5-IVC-IND", FieldType.STRING, 1);
        pnd_Flat_File5_Pnd_Ff5_Ivc_Protect = pnd_Flat_File5.newFieldInGroup("pnd_Flat_File5_Pnd_Ff5_Ivc_Protect", "#FF5-IVC-PROTECT", FieldType.BOOLEAN, 
            1);
        pnd_Tax_Year = localVariables.newFieldInRecord("pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Rec_Read = localVariables.newFieldInRecord("pnd_Rec_Read", "#REC-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Rec_Ritten = localVariables.newFieldInRecord("pnd_Rec_Ritten", "#REC-RITTEN", FieldType.PACKED_DECIMAL, 7);
        pnd_Ff5_Rec_Ritten = localVariables.newFieldInRecord("pnd_Ff5_Rec_Ritten", "#FF5-REC-RITTEN", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Cnt = localVariables.newFieldArrayInRecord("pnd_Cref_Cnt", "#CREF-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 3));
        pnd_Life_Cnt = localVariables.newFieldArrayInRecord("pnd_Life_Cnt", "#LIFE-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 3));
        pnd_Tiaa_Cnt = localVariables.newFieldArrayInRecord("pnd_Tiaa_Cnt", "#TIAA-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 3));
        pnd_Tcii_Cnt = localVariables.newFieldArrayInRecord("pnd_Tcii_Cnt", "#TCII-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 3));
        pnd_Trst_Cnt = localVariables.newFieldArrayInRecord("pnd_Trst_Cnt", "#TRST-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 3));
        pnd_Fsb_Cnt = localVariables.newFieldArrayInRecord("pnd_Fsb_Cnt", "#FSB-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 3));
        pnd_Othr_Cnt = localVariables.newFieldArrayInRecord("pnd_Othr_Cnt", "#OTHR-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 3));
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 1);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);
        pnd_End_Date = localVariables.newFieldInRecord("pnd_End_Date", "#END-DATE", FieldType.STRING, 8);
        pnd_Ytd_Run_Type = localVariables.newFieldInRecord("pnd_Ytd_Run_Type", "#YTD-RUN-TYPE", FieldType.STRING, 1);
        pnd_Freq = localVariables.newFieldInRecord("pnd_Freq", "#FREQ", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Diff_Ivc_Ind = localVariables.newFieldInRecord("pnd_Diff_Ivc_Ind", "#DIFF-IVC-IND", FieldType.BOOLEAN, 1);
        pnd_Pass = localVariables.newFieldInRecord("pnd_Pass", "#PASS", FieldType.BOOLEAN, 1);
        pnd_First_Rec = localVariables.newFieldInRecord("pnd_First_Rec", "#FIRST-REC", FieldType.BOOLEAN, 1);
        pnd_Mixed_Ind = localVariables.newFieldInRecord("pnd_Mixed_Ind", "#MIXED-IND", FieldType.BOOLEAN, 1);
        pnd_Sytd_Daily = localVariables.newFieldInRecord("pnd_Sytd_Daily", "#SYTD-DAILY", FieldType.BOOLEAN, 1);
        pnd_S_Ytd = localVariables.newFieldInRecord("pnd_S_Ytd", "#S-YTD", FieldType.BOOLEAN, 1);
        pnd_Sv_Ivc_Ind = localVariables.newFieldInRecord("pnd_Sv_Ivc_Ind", "#SV-IVC-IND", FieldType.STRING, 1);

        pnd_Savers = localVariables.newGroupInRecord("pnd_Savers", "#SAVERS");
        pnd_Savers_Pnd_Sv_Co = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Co", "#SV-CO", FieldType.STRING, 1);
        pnd_Savers_Pnd_Sv_Tin = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Tin", "#SV-TIN", FieldType.STRING, 10);
        pnd_Savers_Pnd_Sv_Contractpnd = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Contractpnd", "#SV-CONTRACT#", FieldType.STRING, 8);
        pnd_Savers_Pnd_Sv_Payee = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Payee", "#SV-PAYEE", FieldType.STRING, 2);
        pnd_Savers_Pnd_Sv_Pay_Ctgry = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Pay_Ctgry", "#SV-PAY-CTGRY", FieldType.STRING, 1);
        pnd_Savers_Pnd_Sv_Dis_Cde = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Dis_Cde", "#SV-DIS-CDE", FieldType.STRING, 2);
        pnd_Savers_Pnd_Sv_Res_Cde = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Res_Cde", "#SV-RES-CDE", FieldType.STRING, 2);
        pnd_Savers_Pnd_Sv_Tax_Ctz = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Tax_Ctz", "#SV-TAX-CTZ", FieldType.STRING, 1);

        pnd_Summ_Acc = localVariables.newGroupInRecord("pnd_Summ_Acc", "#SUMM-ACC");
        pnd_Summ_Acc_Pnd_Sum_Amt1 = pnd_Summ_Acc.newFieldInGroup("pnd_Summ_Acc_Pnd_Sum_Amt1", "#SUM-AMT1", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Summ_Acc_Pnd_Sum_Amts = pnd_Summ_Acc.newFieldArrayInGroup("pnd_Summ_Acc_Pnd_Sum_Amts", "#SUM-AMTS", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            7));
        pnd_Summ_Acc_Pnd_Sum_Cnt = pnd_Summ_Acc.newFieldInGroup("pnd_Summ_Acc_Pnd_Sum_Cnt", "#SUM-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Error = localVariables.newFieldInRecord("pnd_Error", "#ERROR", FieldType.STRING, 25);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0901.initializeValues();
        ldaTwrl0905.initializeValues();
        ldaTwrl0900.initializeValues();

        localVariables.reset();
        pnd_Diff_Ivc_Ind.setInitialValue(false);
        pnd_Pass.setInitialValue(true);
        pnd_First_Rec.setInitialValue(true);
        pnd_Mixed_Ind.setInitialValue(false);
        pnd_Sytd_Daily.setInitialValue(false);
        pnd_S_Ytd.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0920() throws Exception
    {
        super("Twrp0920");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #FLAT-FILE1
        while (condition(getWorkFiles().read(1, ldaTwrl0901.getPnd_Flat_File1())))
        {
            pnd_Tax_Year.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Tax_Year());                                                                                      //Natural: ASSIGN #TAX-YEAR := #FF1-TAX-YEAR
            pnd_Freq.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Frequency());                                                                                         //Natural: ASSIGN #FREQ := #FF1-FREQUENCY
            pnd_Start_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Start_Date());                                                                                  //Natural: ASSIGN #START-DATE := #FF1-START-DATE
            pnd_End_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_End_Date());                                                                                      //Natural: ASSIGN #END-DATE := #FF1-END-DATE
            pnd_Ytd_Run_Type.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Ytd_Run_Type());                                                                              //Natural: ASSIGN #YTD-RUN-TYPE := #FF1-YTD-RUN-TYPE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        pnd_Summ_Acc.reset();                                                                                                                                             //Natural: RESET #SUMM-ACC
        //* *READ WORK FILE 2 RECORD #FLAT-FILE7
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 2 RECORD #XTAXYR-F94
        while (condition(getWorkFiles().read(2, ldaTwrl0900.getPnd_Xtaxyr_F94())))
        {
            if (condition(pnd_First_Rec.getBoolean()))                                                                                                                    //Natural: IF #FIRST-REC
            {
                                                                                                                                                                          //Natural: PERFORM SAVING-ROUTINE
                sub_Saving_Routine();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_First_Rec.setValue(false);                                                                                                                            //Natural: ASSIGN #FIRST-REC := FALSE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Rec_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-READ
            //*  JB1011
            short decideConditionsMet319 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #XTAXYR-F94.TWRPYMNT-COMPANY-CDE;//Natural: VALUE 'C'
            if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("C"))))
            {
                decideConditionsMet319++;
                pnd_Cref_Cnt.getValue(1).nadd(1);                                                                                                                         //Natural: ADD 1 TO #CREF-CNT ( 1 )
            }                                                                                                                                                             //Natural: VALUE 'L'
            else if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("L"))))
            {
                decideConditionsMet319++;
                pnd_Life_Cnt.getValue(1).nadd(1);                                                                                                                         //Natural: ADD 1 TO #LIFE-CNT ( 1 )
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("T"))))
            {
                decideConditionsMet319++;
                pnd_Tiaa_Cnt.getValue(1).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TIAA-CNT ( 1 )
            }                                                                                                                                                             //Natural: VALUE 'S'
            else if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("S"))))
            {
                decideConditionsMet319++;
                pnd_Tcii_Cnt.getValue(1).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TCII-CNT ( 1 )
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("X"))))
            {
                decideConditionsMet319++;
                pnd_Trst_Cnt.getValue(1).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TRST-CNT ( 1 )
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("F"))))
            {
                decideConditionsMet319++;
                pnd_Fsb_Cnt.getValue(1).nadd(1);                                                                                                                          //Natural: ADD 1 TO #FSB-CNT ( 1 )
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pnd_Othr_Cnt.getValue(1).nadd(1);                                                                                                                         //Natural: ADD 1 TO #OTHR-CNT ( 1 )
            }                                                                                                                                                             //Natural: END-DECIDE
            //*    ------ PROCESS-REC ------
            //*    -------------------------
            if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals(pnd_Savers_Pnd_Sv_Co) && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr().equals(pnd_Savers_Pnd_Sv_Tin)  //Natural: IF #XTAXYR-F94.TWRPYMNT-COMPANY-CDE = #SV-CO AND #XTAXYR-F94.TWRPYMNT-TAX-ID-NBR = #SV-TIN AND #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR = #SV-CONTRACT# AND #XTAXYR-F94.TWRPYMNT-PAYEE-CDE = #SV-PAYEE AND #XTAXYR-F94.TWRPYMNT-PAYMT-CATEGORY = #SV-PAY-CTGRY AND #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE = #SV-DIS-CDE AND #XTAXYR-F94.TWRPYMNT-RESIDENCY-CODE = #SV-RES-CDE AND #XTAXYR-F94.TWRPYMNT-TAX-CITIZENSHIP = #SV-TAX-CTZ
                && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr().equals(pnd_Savers_Pnd_Sv_Contractpnd) && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payee_Cde().equals(pnd_Savers_Pnd_Sv_Payee) 
                && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Paymt_Category().equals(pnd_Savers_Pnd_Sv_Pay_Ctgry) && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals(pnd_Savers_Pnd_Sv_Dis_Cde) 
                && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code().equals(pnd_Savers_Pnd_Sv_Res_Cde) && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Citizenship().equals(pnd_Savers_Pnd_Sv_Tax_Ctz)))
            {
                                                                                                                                                                          //Natural: PERFORM RELATED-RECORD
                sub_Related_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  IF WITH PROCESSED RECORD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Summ_Acc_Pnd_Sum_Cnt.greater(getZero())))                                                                                               //Natural: IF #SUM-CNT > 0
                {
                                                                                                                                                                          //Natural: PERFORM NEW-RECORD
                    sub_New_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Summ_Acc.reset();                                                                                                                                 //Natural: RESET #SUMM-ACC
                }                                                                                                                                                         //Natural: END-IF
                ldaTwrl0905.getPnd_Flat_File4().reset();                                                                                                                  //Natural: RESET #FLAT-FILE4 #FLAT-FILE5
                pnd_Flat_File5.reset();
                                                                                                                                                                          //Natural: PERFORM RELATED-RECORD
                sub_Related_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM SAVING-ROUTINE
                sub_Saving_Routine();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT END OF DATA
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            //*  IF WITH PROCESSED RECORD
            if (condition(pnd_Summ_Acc_Pnd_Sum_Cnt.greater(getZero())))                                                                                                   //Natural: IF #SUM-CNT > 0
            {
                                                                                                                                                                          //Natural: PERFORM NEW-RECORD
                sub_New_Record();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SAVING-ROUTINE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RELATED-RECORD
        //*                              ( COMMON FIELDS OF FLAT-FILE5
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-BY-SUMMARY
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NEW-RECORD
        //*                  ----------
        //*                                    (CREATE FLAT-FILE4)
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MIXED-IVC-IND
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BY-PAYMENT-PROCESSING
        //*                  ---------------------
        //* *DEFINE SUBROUTINE CHECK-ACTIVE-REC
        //*                  ----------------
        //* * FOR #I 1  #XTAXYR-F94.#C-TWRPYMNT-PAYMENTS
        //* *   IF  #XTAXYR-F94.TWRPYMNT-PAY-STATUS = ' ' OR= 'C'
        //* *     IF #FIRST-ACT
        //* *        #SV-PAY-STA := #XTAXYR-F94.TWRPYMNT-PAY-STATUS (#I)
        //* *        #SV-IVC-PTK := #XTAXYR-F94.TWRPYMNT-IVC-PROTECT(#I)
        //* *        #FIRST-ACT  := FALSE
        //* *     END-IF
        //* *     IF #XTAXYR-F94.TWRPYMNT-PAY-STATUS(#I)  = #SV-PAY-STATUS
        //* *          IGNORE
        //*  ???????????? K AND U (IVC-PROTECTED) ARE EQUAL
        //* *     ELSE IF #FLAT-FILE7.#TWRPYMNT-IVC-PROTECT(#I) = #SV-IVC-PTK
        //* *          #SV-PAY-STATUS := #FLAT-FILE7.#TWRPYMNT-PAY-STATUS(#I)
        //* *          #DIFF-PAY-STA := TRUE
        //* *     END-IF
        //* *END-DEFINE
        //*  JB1011
        //*  JB1011
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"TOTAL RECORDS READ           : ",pnd_Rec_Read,NEWLINE,"  1. CREF RECORDS            : ",           //Natural: WRITE ( 1 ) NOTITLE NOHDR 'TOTAL RECORDS READ           : ' #REC-READ / '  1. CREF RECORDS            : ' #CREF-CNT ( 1 ) / '  2. LIFE RECORDS            : ' #LIFE-CNT ( 1 ) / '  3. TIAA RECORDS            : ' #TIAA-CNT ( 1 ) / '  4. TCII RECORDS            : ' #TCII-CNT ( 1 ) / '  5. TRST RECORDS            : ' #TRST-CNT ( 1 ) / '  6. FSB  RECORDS            : ' #FSB-CNT ( 1 ) / '  7. OTHER CO. RECORDS       : ' #OTHR-CNT ( 1 ) /// 'TOTAL RECORDS WRITTEN TO FF4 : ' #REC-RITTEN / '  1. CREF RECORDS            : ' #CREF-CNT ( 2 ) / '  2. LIFE RECORDS            : ' #LIFE-CNT ( 2 ) / '  3. TIAA RECORDS            : ' #TIAA-CNT ( 2 ) / '  4. TCII RECORDS            : ' #TCII-CNT ( 2 ) / '  5. TRST RECORDS            : ' #TRST-CNT ( 2 ) / '  6. FSB  RECORDS            : ' #FSB-CNT ( 2 ) / '  7. OTHER CO. RECORDS       : ' #OTHR-CNT ( 2 ) ////
            pnd_Cref_Cnt.getValue(1),NEWLINE,"  2. LIFE RECORDS            : ",pnd_Life_Cnt.getValue(1),NEWLINE,"  3. TIAA RECORDS            : ",pnd_Tiaa_Cnt.getValue(1),
            NEWLINE,"  4. TCII RECORDS            : ",pnd_Tcii_Cnt.getValue(1),NEWLINE,"  5. TRST RECORDS            : ",pnd_Trst_Cnt.getValue(1),NEWLINE,
            "  6. FSB  RECORDS            : ",pnd_Fsb_Cnt.getValue(1),NEWLINE,"  7. OTHER CO. RECORDS       : ",pnd_Othr_Cnt.getValue(1),NEWLINE,NEWLINE,
            NEWLINE,"TOTAL RECORDS WRITTEN TO FF4 : ",pnd_Rec_Ritten,NEWLINE,"  1. CREF RECORDS            : ",pnd_Cref_Cnt.getValue(2),NEWLINE,"  2. LIFE RECORDS            : ",
            pnd_Life_Cnt.getValue(2),NEWLINE,"  3. TIAA RECORDS            : ",pnd_Tiaa_Cnt.getValue(2),NEWLINE,"  4. TCII RECORDS            : ",pnd_Tcii_Cnt.getValue(2),
            NEWLINE,"  5. TRST RECORDS            : ",pnd_Trst_Cnt.getValue(2),NEWLINE,"  6. FSB  RECORDS            : ",pnd_Fsb_Cnt.getValue(2),NEWLINE,
            "  7. OTHER CO. RECORDS       : ",pnd_Othr_Cnt.getValue(2),NEWLINE,NEWLINE,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*  IF #FREQ = 'Y' AND #YTD-RUN-TYPE = 'R'
        //*    WRITE (1) NOTITLE NOHDR
        //*      ' ***  FOR REGULAR YTD RUN ONLY - (WITH MIXED IVC INDICATOR) ' /
        //*      'TOTAL RECORDS WRITTEN TO FF5 : ' #FF5-REC-RITTEN  /
        //*      '  1. CREF RECORDS            : ' #CREF-CNT(3)    /
        //*      '  2. TIAA RECORDS            : ' #TIAA-CNT(3)    /
        //*      '  3. NON CREF/TIAA RECORDS   : ' #OTHR-CNT(3)
        //*  END-IF
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-ERROR-REC
    }
    private void sub_Saving_Routine() throws Exception                                                                                                                    //Natural: SAVING-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //*                  --------------
        pnd_Savers.reset();                                                                                                                                               //Natural: RESET #SAVERS
        pnd_Savers_Pnd_Sv_Co.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde());                                                                              //Natural: ASSIGN #SV-CO := #XTAXYR-F94.TWRPYMNT-COMPANY-CDE
        pnd_Savers_Pnd_Sv_Tin.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr());                                                                              //Natural: ASSIGN #SV-TIN := #XTAXYR-F94.TWRPYMNT-TAX-ID-NBR
        pnd_Savers_Pnd_Sv_Contractpnd.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr());                                                                    //Natural: ASSIGN #SV-CONTRACT# := #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR
        pnd_Savers_Pnd_Sv_Payee.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payee_Cde());                                                                             //Natural: ASSIGN #SV-PAYEE := #XTAXYR-F94.TWRPYMNT-PAYEE-CDE
        pnd_Savers_Pnd_Sv_Pay_Ctgry.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Paymt_Category());                                                                    //Natural: ASSIGN #SV-PAY-CTGRY := #XTAXYR-F94.TWRPYMNT-PAYMT-CATEGORY
        pnd_Savers_Pnd_Sv_Dis_Cde.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde());                                                                    //Natural: ASSIGN #SV-DIS-CDE := #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE
        pnd_Savers_Pnd_Sv_Res_Cde.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code());                                                                      //Natural: ASSIGN #SV-RES-CDE := #XTAXYR-F94.TWRPYMNT-RESIDENCY-CODE
        pnd_Savers_Pnd_Sv_Tax_Ctz.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Citizenship());                                                                     //Natural: ASSIGN #SV-TAX-CTZ := #XTAXYR-F94.TWRPYMNT-TAX-CITIZENSHIP
    }
    private void sub_Related_Record() throws Exception                                                                                                                    //Natural: RELATED-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*                  --------------
        if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Seq_No().equals(getZero())))                                                                        //Natural: IF #XTAXYR-F94.TWRPYMNT-CONTRACT-SEQ-NO = 0
        {
            pnd_Error.setValue("NO CONTRACT SEQ#");                                                                                                                       //Natural: ASSIGN #ERROR := 'NO CONTRACT SEQ#'
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR-REC
            sub_Print_Error_Rec();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*                            (FOR FIRST PHYSICAL RECORD)
        if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Seq_No().equals(1)))                                                                                //Natural: IF #XTAXYR-F94.TWRPYMNT-CONTRACT-SEQ-NO = 1
        {
            //*                                                     10/9/99
            if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Indicator().equals(" ")))                                                                            //Natural: IF #XTAXYR-F94.TWRPYMNT-IVC-INDICATOR = ' '
            {
                pnd_Error.setValue("NO SUMMARY IVC-IND");                                                                                                                 //Natural: ASSIGN #ERROR := 'NO SUMMARY IVC-IND'
                //*    PERFORM PRINT-ERROR-REC
                //* *        PERFORM CHECK-ACTIVE-REC
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Residency_Code().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code());                                     //Natural: ASSIGN #FF4-RESIDENCY-CODE := #XTAXYR-F94.TWRPYMNT-RESIDENCY-CODE
            ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Distribution_Cde().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde());                                 //Natural: ASSIGN #FF4-DISTRIBUTION-CDE := #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE
            ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Company_Code().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde());                                          //Natural: ASSIGN #FF4-COMPANY-CODE := #XTAXYR-F94.TWRPYMNT-COMPANY-CDE
            ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Contract_Nbr().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr());                                         //Natural: ASSIGN #FF4-CONTRACT-NBR := #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR
            ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Tax_Citizenship().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Citizenship());                                   //Natural: ASSIGN #FF4-TAX-CITIZENSHIP := #XTAXYR-F94.TWRPYMNT-TAX-CITIZENSHIP
            ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Payset_Type().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payment_Type().getValue(1),  //Natural: COMPRESS #XTAXYR-F94.TWRPYMNT-PAYMENT-TYPE ( 1 ) #XTAXYR-F94.TWRPYMNT-SETTLE-TYPE ( 1 ) TO #FF4-PAYSET-TYPE LEAVING NO SPACE
                ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(1)));
            ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Ivc_Ind().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Indicator());                                             //Natural: ASSIGN #FF4-IVC-IND := #XTAXYR-F94.TWRPYMNT-IVC-INDICATOR
            if (condition(pnd_Freq.equals("Y") && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Indicator().equals("M")))                                                    //Natural: IF #FREQ = 'Y' AND #XTAXYR-F94.TWRPYMNT-IVC-INDICATOR = 'M'
            {
                pnd_Mixed_Ind.setValue(true);                                                                                                                             //Natural: ASSIGN #MIXED-IND := TRUE
                pnd_Flat_File5_Pnd_Ff5_Company_Code.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde());                                                       //Natural: ASSIGN #FF5-COMPANY-CODE := #XTAXYR-F94.TWRPYMNT-COMPANY-CDE
                pnd_Flat_File5_Pnd_Ff5_Tin.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr());                                                                 //Natural: ASSIGN #FF5-TIN := #XTAXYR-F94.TWRPYMNT-TAX-ID-NBR
                pnd_Flat_File5_Pnd_Ff5_Contract_Nbr.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr());                                                      //Natural: ASSIGN #FF5-CONTRACT-NBR := #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR
                pnd_Flat_File5_Pnd_Ff5_Tax_Citizenship.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Citizenship());                                                //Natural: ASSIGN #FF5-TAX-CITIZENSHIP := #XTAXYR-F94.TWRPYMNT-TAX-CITIZENSHIP
                pnd_Flat_File5_Pnd_Ff5_Payset_Type.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payment_Type().getValue(1),  //Natural: COMPRESS #XTAXYR-F94.TWRPYMNT-PAYMENT-TYPE ( 1 ) #XTAXYR-F94.TWRPYMNT-SETTLE-TYPE ( 1 ) TO #FF5-PAYSET-TYPE LEAVING NO SPACE
                    ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(1)));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*                              ( FOR ALL RELATED PHYSICAL RECORDS )
        //*   10/9/99
        //*  NRA
        if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Citizenship().equals("F")))                                                                              //Natural: IF #XTAXYR-F94.TWRPYMNT-TAX-CITIZENSHIP = 'F'
        {
            //* *        PERFORM PROCESS-BY-PAYMENT    /* FC, JR 2-12-02
            //*  FC, JR 2-12-02
                                                                                                                                                                          //Natural: PERFORM BY-PAYMENT-PROCESSING
            sub_By_Payment_Processing();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-BY-SUMMARY
            sub_Process_By_Summary();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  FOR FEDERAL - 10/9/99
    private void sub_Process_By_Summary() throws Exception                                                                                                                //Natural: PROCESS-BY-SUMMARY
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ------------------
        if (condition(pnd_Freq.equals("Y")))                                                                                                                              //Natural: IF #FREQ = 'Y'
        {
            short decideConditionsMet487 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #YTD-RUN-TYPE;//Natural: VALUE 'R'
            if (condition((pnd_Ytd_Run_Type.equals("R"))))
            {
                decideConditionsMet487++;
                //*                             (ACCUMULATE  SUMMARY FINANCIAL FIELDS)
                pnd_Summ_Acc_Pnd_Sum_Amt1.nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Sum_Gross_Amt());                                                                   //Natural: ADD #XTAXYR-F94.TWRPYMNT-SUM-GROSS-AMT TO #SUM-AMT1
                pnd_Summ_Acc_Pnd_Sum_Amts.getValue(1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Sum_Ivc_Amt());                                                         //Natural: ADD #XTAXYR-F94.TWRPYMNT-SUM-IVC-AMT TO #SUM-AMTS ( 1 )
                pnd_Summ_Acc_Pnd_Sum_Amts.getValue(2).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Sum_Int_Amt());                                                         //Natural: ADD #XTAXYR-F94.TWRPYMNT-SUM-INT-AMT TO #SUM-AMTS ( 2 )
                pnd_Summ_Acc_Pnd_Sum_Amts.getValue(3).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Sum_Fed_Wthld());                                                       //Natural: ADD #XTAXYR-F94.TWRPYMNT-SUM-FED-WTHLD TO #SUM-AMTS ( 3 )
                pnd_Summ_Acc_Pnd_Sum_Amts.getValue(4).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Sum_Nra_Wthld());                                                       //Natural: ADD #XTAXYR-F94.TWRPYMNT-SUM-NRA-WTHLD TO #SUM-AMTS ( 4 )
                pnd_Summ_Acc_Pnd_Sum_Amts.getValue(5).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Sum_Can_Wthld());                                                       //Natural: ADD #XTAXYR-F94.TWRPYMNT-SUM-CAN-WTHLD TO #SUM-AMTS ( 5 )
                pnd_Summ_Acc_Pnd_Sum_Amts.getValue(6).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Sum_State_Wthld());                                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-SUM-STATE-WTHLD TO #SUM-AMTS ( 6 )
                pnd_Summ_Acc_Pnd_Sum_Amts.getValue(7).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Sum_Local_Wthld());                                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-SUM-LOCAL-WTHLD TO #SUM-AMTS ( 7 )
                pnd_Summ_Acc_Pnd_Sum_Cnt.nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments());                                                                   //Natural: ADD #XTAXYR-F94.#C-TWRPYMNT-PAYMENTS TO #SUM-CNT
                if (condition(pnd_Summ_Acc_Pnd_Sum_Cnt.greater(99)))                                                                                                      //Natural: IF #SUM-CNT > 99
                {
                    getReports().write(0, NEWLINE,"SUMMARY PAYMENTS COUNT > 99", new FieldAttributes ("AD=I"),NEWLINE,"SUMMARY PAYMENT COUNT:",pnd_Summ_Acc_Pnd_Sum_Cnt,new  //Natural: WRITE / 'SUMMARY PAYMENTS COUNT > 99' ( AD = I ) / 'SUMMARY PAYMENT COUNT:' #SUM-CNT 3X 'ON RECORD:' #REC-READ
                        ColumnSpacing(3),"ON RECORD:",pnd_Rec_Read);
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Mixed_Ind.getBoolean()))                                                                                                                //Natural: IF #MIXED-IND
                {
                    FOR01:                                                                                                                                                //Natural: FOR #I 1 #XTAXYR-F94.#C-TWRPYMNT-PAYMENTS
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments())); pnd_I.nadd(1))
                    {
                                                                                                                                                                          //Natural: PERFORM MIXED-IVC-IND
                        sub_Mixed_Ivc_Ind();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'S'
            else if (condition((pnd_Ytd_Run_Type.equals("S"))))
            {
                decideConditionsMet487++;
                pnd_S_Ytd.setValue(true);                                                                                                                                 //Natural: ASSIGN #S-YTD := TRUE
                                                                                                                                                                          //Natural: PERFORM BY-PAYMENT-PROCESSING
                sub_By_Payment_Processing();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  (DAILY RUN)
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM BY-PAYMENT-PROCESSING
            sub_By_Payment_Processing();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_New_Record() throws Exception                                                                                                                        //Natural: NEW-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Sum_Gross_Amt().setValue(pnd_Summ_Acc_Pnd_Sum_Amt1);                                                                        //Natural: ASSIGN #FF4-SUM-GROSS-AMT := #SUM-AMT1
        ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Sum_Ivc_Amt().setValue(pnd_Summ_Acc_Pnd_Sum_Amts.getValue(1));                                                              //Natural: ASSIGN #FF4-SUM-IVC-AMT := #SUM-AMTS ( 1 )
        ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Sum_Int_Amt().setValue(pnd_Summ_Acc_Pnd_Sum_Amts.getValue(2));                                                              //Natural: ASSIGN #FF4-SUM-INT-AMT := #SUM-AMTS ( 2 )
        ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Sum_Fed_Wthld().setValue(pnd_Summ_Acc_Pnd_Sum_Amts.getValue(3));                                                            //Natural: ASSIGN #FF4-SUM-FED-WTHLD := #SUM-AMTS ( 3 )
        ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Sum_Nra_Wthld().setValue(pnd_Summ_Acc_Pnd_Sum_Amts.getValue(4));                                                            //Natural: ASSIGN #FF4-SUM-NRA-WTHLD := #SUM-AMTS ( 4 )
        ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Sum_Can_Wthld().setValue(pnd_Summ_Acc_Pnd_Sum_Amts.getValue(5));                                                            //Natural: ASSIGN #FF4-SUM-CAN-WTHLD := #SUM-AMTS ( 5 )
        ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Sum_State_Wthld().setValue(pnd_Summ_Acc_Pnd_Sum_Amts.getValue(6));                                                          //Natural: ASSIGN #FF4-SUM-STATE-WTHLD := #SUM-AMTS ( 6 )
        ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Sum_Local_Wthld().setValue(pnd_Summ_Acc_Pnd_Sum_Amts.getValue(7));                                                          //Natural: ASSIGN #FF4-SUM-LOCAL-WTHLD := #SUM-AMTS ( 7 )
        ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Trans_Cnt().setValue(pnd_Summ_Acc_Pnd_Sum_Cnt);                                                                             //Natural: ASSIGN #FF4-TRANS-CNT := #SUM-CNT
        //*                         (CHECK IVC-IND FIRST BEFORE WRITING TO A FILE)
        if (condition(pnd_Sytd_Daily.getBoolean()))                                                                                                                       //Natural: IF #SYTD-DAILY
        {
            //*  SELECTED PAYMENTS HAVE DIFFERENT IVC-IND
            //*  SINCE #FF4-IVC-IND CONTAINS SUMM-IVC-IND
            if (condition(pnd_Diff_Ivc_Ind.getBoolean()))                                                                                                                 //Natural: IF #DIFF-IVC-IND
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Ivc_Ind().setValue(pnd_Sv_Ivc_Ind);                                                                                 //Natural: ASSIGN #FF4-IVC-IND := #SV-IVC-IND
            }                                                                                                                                                             //Natural: END-IF
            pnd_Sv_Ivc_Ind.reset();                                                                                                                                       //Natural: RESET #SV-IVC-IND
            pnd_Sytd_Daily.resetInitial();                                                                                                                                //Natural: RESET INITIAL #SYTD-DAILY #PASS #DIFF-IVC-IND #S-YTD
            pnd_Pass.resetInitial();
            pnd_Diff_Ivc_Ind.resetInitial();
            pnd_S_Ytd.resetInitial();
        }                                                                                                                                                                 //Natural: END-IF
        //*                    --- COMPUTE FOR TAXABLE AMT  10/9/99
        //*    IF #FREQ = 'Y'  AND #FLAT-FILE7.TWRPYMNT-TAX-CITIZENSHIP = 'U'
        //*    IF #FF4-IVC-IND = 'K' AND
        //*                 NOT (#FF3-DISTRIBUTION-CDE = 'G' OR= 'H' OR= 'R'
        //*                                          OR= 'Y' OR= 'Z' OR= '6'
        //*          #FF4-TAX-AMT := #FF4-SUM-GROSS-AMT - #FF4-SUM-IVC-AMT
        //*    ELSE  #FF4-TAX-AMT := 0
        //*    END-IF
        getWorkFiles().write(3, false, ldaTwrl0905.getPnd_Flat_File4());                                                                                                  //Natural: WRITE WORK FILE 3 #FLAT-FILE4
        pnd_Rec_Ritten.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #REC-RITTEN
        pnd_Cnt.setValue(2);                                                                                                                                              //Natural: ASSIGN #CNT := 2
                                                                                                                                                                          //Natural: PERFORM COMPANY-CNT
        sub_Company_Cnt();
        if (condition(Global.isEscape())) {return;}
        pnd_Mixed_Ind.resetInitial();                                                                                                                                     //Natural: RESET INITIAL #MIXED-IND
    }
    private void sub_Mixed_Ivc_Ind() throws Exception                                                                                                                     //Natural: MIXED-IVC-IND
    {
        if (BLNatReinput.isReinput()) return;

        //*                  -------------
        if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Protect().getValue(pnd_I).getBoolean() || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Ind().getValue(pnd_I).equals("K"))) //Natural: IF #XTAXYR-F94.TWRPYMNT-IVC-PROTECT ( #I ) OR #XTAXYR-F94.TWRPYMNT-IVC-IND ( #I ) = 'K'
        {
            pnd_Flat_File5_Pnd_Ff5_Orig_Srce_Cde.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I));                                        //Natural: ASSIGN #FF5-ORIG-SRCE-CDE := #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I )
            pnd_Flat_File5_Pnd_Ff5_Updt_Srce_Cde.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I));                                       //Natural: ASSIGN #FF5-UPDT-SRCE-CDE := #XTAXYR-F94.TWRPYMNT-UPDTE-SRCE-CDE ( #I )
            pnd_Flat_File5_Pnd_Ff5_Gross_Amt.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_I));                                                //Natural: ASSIGN #FF5-GROSS-AMT := #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I )
            pnd_Flat_File5_Pnd_Ff5_Ivc_Amt.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_I));                                                    //Natural: ASSIGN #FF5-IVC-AMT := #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I )
            pnd_Flat_File5_Pnd_Ff5_Ivc_Ind.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Ind().getValue(pnd_I));                                                    //Natural: ASSIGN #FF5-IVC-IND := #XTAXYR-F94.TWRPYMNT-IVC-IND ( #I )
            pnd_Flat_File5_Pnd_Ff5_Ivc_Protect.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Protect().getValue(pnd_I).getBoolean());                               //Natural: ASSIGN #FF5-IVC-PROTECT := #XTAXYR-F94.TWRPYMNT-IVC-PROTECT ( #I )
            //*  WRITE WORK FILE 4 #FLAT-FILE5
            pnd_Ff5_Rec_Ritten.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #FF5-REC-RITTEN
            pnd_Cnt.setValue(3);                                                                                                                                          //Natural: ASSIGN #CNT := 3
                                                                                                                                                                          //Natural: PERFORM COMPANY-CNT
            sub_Company_Cnt();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_By_Payment_Processing() throws Exception                                                                                                             //Natural: BY-PAYMENT-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #I 1 #XTAXYR-F94.#C-TWRPYMNT-PAYMENTS
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments())); pnd_I.nadd(1))
        {
            if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Intfce_Dte().getValue(pnd_I).greaterOrEqual(pnd_Start_Date) && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Intfce_Dte().getValue(pnd_I).lessOrEqual(pnd_End_Date))) //Natural: IF #XTAXYR-F94.TWRPYMNT-PYMNT-INTFCE-DTE ( #I ) = #START-DATE THRU #END-DATE
            {
                if (condition(pnd_Pass.getBoolean()))                                                                                                                     //Natural: IF #PASS
                {
                    pnd_Sv_Ivc_Ind.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Ind().getValue(pnd_I));                                                            //Natural: ASSIGN #SV-IVC-IND := #XTAXYR-F94.TWRPYMNT-IVC-IND ( #I )
                    pnd_Pass.setValue(false);                                                                                                                             //Natural: ASSIGN #PASS := FALSE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Ind().getValue(pnd_I).equals(pnd_Sv_Ivc_Ind)))                                                   //Natural: IF #XTAXYR-F94.TWRPYMNT-IVC-IND ( #I ) = #SV-IVC-IND
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Sv_Ivc_Ind.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Ind().getValue(pnd_I));                                                            //Natural: ASSIGN #SV-IVC-IND := #XTAXYR-F94.TWRPYMNT-IVC-IND ( #I )
                    pnd_Diff_Ivc_Ind.setValue(true);                                                                                                                      //Natural: ASSIGN #DIFF-IVC-IND := TRUE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Summ_Acc_Pnd_Sum_Amt1.nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_I));                                                       //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #SUM-AMT1
                pnd_Summ_Acc_Pnd_Sum_Amts.getValue(1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_I));                                             //Natural: ADD #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) TO #SUM-AMTS ( 1 )
                pnd_Summ_Acc_Pnd_Sum_Amts.getValue(2).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_I));                                             //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #SUM-AMTS ( 2 )
                pnd_Summ_Acc_Pnd_Sum_Amts.getValue(3).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_I));                                       //Natural: ADD #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #SUM-AMTS ( 3 )
                pnd_Summ_Acc_Pnd_Sum_Amts.getValue(4).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Nra_Wthld_Amt().getValue(pnd_I));                                       //Natural: ADD #XTAXYR-F94.TWRPYMNT-NRA-WTHLD-AMT ( #I ) TO #SUM-AMTS ( 4 )
                pnd_Summ_Acc_Pnd_Sum_Amts.getValue(5).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Can_Wthld_Amt().getValue(pnd_I));                                       //Natural: ADD #XTAXYR-F94.TWRPYMNT-CAN-WTHLD-AMT ( #I ) TO #SUM-AMTS ( 5 )
                pnd_Summ_Acc_Pnd_Sum_Amts.getValue(6).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_I));                                         //Natural: ADD #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) TO #SUM-AMTS ( 6 )
                pnd_Summ_Acc_Pnd_Sum_Amts.getValue(7).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Local_Wthld().getValue(pnd_I));                                         //Natural: ADD #XTAXYR-F94.TWRPYMNT-LOCAL-WTHLD ( #I ) TO #SUM-AMTS ( 7 )
                pnd_Summ_Acc_Pnd_Sum_Cnt.nadd(1);                                                                                                                         //Natural: ADD 1 TO #SUM-CNT
                //*  FOR SPECIAL YTD WITH MIXED PYMNTS
                if (condition(pnd_S_Ytd.getBoolean() && pnd_Mixed_Ind.getBoolean()))                                                                                      //Natural: IF #S-YTD AND #MIXED-IND
                {
                                                                                                                                                                          //Natural: PERFORM MIXED-IVC-IND
                    sub_Mixed_Ivc_Ind();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Summ_Acc_Pnd_Sum_Cnt.greater(getZero())))                                                                                                       //Natural: IF #SUM-CNT > 0
        {
            pnd_Sytd_Daily.setValue(true);                                                                                                                                //Natural: ASSIGN #SYTD-DAILY := TRUE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Company_Cnt() throws Exception                                                                                                                       //Natural: COMPANY-CNT
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ------------
        //*  JB1011
        short decideConditionsMet625 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SV-CO;//Natural: VALUE 'C'
        if (condition((pnd_Savers_Pnd_Sv_Co.equals("C"))))
        {
            decideConditionsMet625++;
            pnd_Cref_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #CREF-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pnd_Savers_Pnd_Sv_Co.equals("L"))))
        {
            decideConditionsMet625++;
            pnd_Life_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #LIFE-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pnd_Savers_Pnd_Sv_Co.equals("T"))))
        {
            decideConditionsMet625++;
            pnd_Tiaa_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #TIAA-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Savers_Pnd_Sv_Co.equals("S"))))
        {
            decideConditionsMet625++;
            pnd_Tcii_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #TCII-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((pnd_Savers_Pnd_Sv_Co.equals("X"))))
        {
            decideConditionsMet625++;
            pnd_Trst_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #TRST-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((pnd_Savers_Pnd_Sv_Co.equals("F"))))
        {
            decideConditionsMet625++;
            pnd_Fsb_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                        //Natural: ADD 1 TO #FSB-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Othr_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #OTHR-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Print_Error_Rec() throws Exception                                                                                                                   //Natural: PRINT-ERROR-REC
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(0, "CO",                                                                                                                                     //Natural: DISPLAY 'CO' #XTAXYR-F94.TWRPYMNT-COMPANY-CDE 'CONTRACT/NBR' #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR 'RES/CDE' #XTAXYR-F94.TWRPYMNT-RESIDENCY-CODE 'DIS/CDE' #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE 'TAX/CTZ' #XTAXYR-F94.TWRPYMNT-TAX-CITIZENSHIP 'SUMM-IVC/IND' #XTAXYR-F94.TWRPYMNT-IVC-INDICATOR 'SEQ#' #XTAXYR-F94.TWRPYMNT-CONTRACT-SEQ-NO 'PAY/CNT' #XTAXYR-F94.#C-TWRPYMNT-PAYMENTS 'ERROR' #ERROR
        		ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde(),"CONTRACT/NBR",
        		ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr(),"RES/CDE",
        		ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code(),"DIS/CDE",
        		ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde(),"TAX/CTZ",
        		ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Citizenship(),"SUMM-IVC/IND",
        		ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Indicator(),"SEQ#",
        		ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Seq_No(),"PAY/CNT",
        		ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments(),"ERROR",
        		pnd_Error);
        if (Global.isEscape()) return;
        pnd_Error.reset();                                                                                                                                                //Natural: RESET #ERROR
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getPROGRAM(),new TabSetting(34),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR / *PROGRAM 34T 'TAX WITHHOLDING AND REPORTING SYSTEM' 119T 'PAGE' *PAGE-NUMBER ( 1 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 43T 'CREATED FLAT FILES' / 'RUNTIME : ' *TIMX 45T 'TAX YEAR ' #TAX-YEAR / 46T 'CONTROL TOTALS' / 33T 'DATE RANGE : ' #START-DATE ' THRU ' #END-DATE ///
                        TabSetting(119),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(43),"CREATED FLAT FILES",NEWLINE,"RUNTIME : ",Global.getTIMX(),new TabSetting(45),"TAX YEAR ",pnd_Tax_Year,NEWLINE,new 
                        TabSetting(46),"CONTROL TOTALS",NEWLINE,new TabSetting(33),"DATE RANGE : ",pnd_Start_Date," THRU ",pnd_End_Date,NEWLINE,NEWLINE,
                        NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");

        getReports().setDisplayColumns(0, "CO",
        		ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde(),"CONTRACT/NBR",
        		ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr(),"RES/CDE",
        		ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code(),"DIS/CDE",
        		ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde(),"TAX/CTZ",
        		ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Citizenship(),"SUMM-IVC/IND",
        		ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Indicator(),"SEQ#",
        		ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Seq_No(),"PAY/CNT",
        		ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments(),"ERROR",
        		pnd_Error);
    }
}
