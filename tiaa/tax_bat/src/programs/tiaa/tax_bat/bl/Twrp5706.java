/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:41:43 PM
**        * FROM NATURAL PROGRAM : Twrp5706
************************************************************
**        * FILE NAME            : Twrp5706.java
**        * CLASS NAME           : Twrp5706
**        * INSTANCE NAME        : Twrp5706
************************************************************
************************************************************************
* PROGRAM : TWRP5706
* SYSTEM  : TAXWARS
* FUNCTION: READS PAYMENTS EXTRACT CREATED BY TWRP0900 AND PRODUCES
*           A W2 PAYMENTS SUBSET FOR USE IN W2 YEAR-TO-DATE AND
*           AND MONTHLY REPORTS.
* AUTHOR  : J.ROTHOLZ
* HISTORY : 03/26/10  - PROGRAM CLONED FROM TWRP5705
*           05/28/10  - TWRPYMNT-SYS-DTE-TIME ADDED TO EXTRACT FOR
*                       W2 CORRECTIONS REPORTING. NOTE THAT ALPHA
*                       FORMAT OF THIS FIELD ON PAYMENTS FILE (ADABAS)
*                       IS CONVERTED TO T FORMAT IN THIS PROGRAM.
*           10/19/11  M.BERLIN  - CHANGED REFERENCES FROM FIELD
*                                 TWRPYMNT-COMPANY-CDE TO FIELD
*                                 TWRPYMNT-COMPANY-CDE-FORM     /* MB
* 02/18/15: OS - RECOMPILED FOR UPDATED TWRL0900
* 08/04/2016 FENDAYA COR/NAS SUNSET CALLING PROCOBOL.  FE201808
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5706 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0600 ldaTwrl0600;
    private LdaTwrl0900 ldaTwrl0900;
    private PdaTwratbl2 pdaTwratbl2;
    private LdaTwrlpsg1 ldaTwrlpsg1;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_W2_Record;
    private DbsField pnd_W2_Record_Pnd_Tax_Year;

    private DbsGroup pnd_W2_Record__R_Field_1;
    private DbsField pnd_W2_Record_Pnd_Tax_Year_N;
    private DbsField pnd_W2_Record_Pnd_Tax_Id_Type;
    private DbsField pnd_W2_Record_Pnd_Tax_Id_Nbr;
    private DbsField pnd_W2_Record_Pnd_Pin_Nbr;
    private DbsField pnd_W2_Record_Pnd_Company_Code;
    private DbsField pnd_W2_Record_Pnd_Contract_Nbr;
    private DbsField pnd_W2_Record_Pnd_Payee_Cde;
    private DbsField pnd_W2_Record_Pnd_Residency_Type;
    private DbsField pnd_W2_Record_Pnd_Residency_Code;
    private DbsField pnd_W2_Record_Pnd_Gross_Amount;
    private DbsField pnd_W2_Record_Pnd_Federal_Tax;
    private DbsField pnd_W2_Record_Pnd_State_Tax;
    private DbsField pnd_W2_Record_Pnd_Name_And_Address;

    private DbsGroup pnd_W2_Record__R_Field_2;
    private DbsField pnd_W2_Record_Pnd_Name;
    private DbsField pnd_W2_Record_Pnd_Address_1;
    private DbsField pnd_W2_Record_Pnd_Address_2;
    private DbsField pnd_W2_Record_Pnd_Address_3;
    private DbsField pnd_W2_Record_Pnd_Address_4;
    private DbsField pnd_W2_Record_Pnd_Address_5;
    private DbsField pnd_W2_Record_Pnd_Employer_State_Id;

    private DbsGroup pnd_W2_Record__R_Field_3;
    private DbsField pnd_W2_Record_Pnd_State_Code;
    private DbsField pnd_W2_Record_Pnd_State_Id;
    private DbsField pnd_W2_Record_Pnd_Transaction_Dt;
    private DbsField pnd_W2_Record_Pnd_W2_Op_Ia_Ind;
    private DbsField pnd_W2_Record_Pnd_Record_Type;
    private DbsField pnd_W2_Record_Pnd_Sys_Dte_Time;
    private DbsField pnd_Gross_Amt_Y;
    private DbsField pnd_Fed_Tax_Y;
    private DbsField pnd_Sta_Tax_Y;
    private DbsField pnd_Gross_Amt_M;
    private DbsField pnd_Fed_Tax_M;
    private DbsField pnd_Sta_Tax_M;
    private DbsField pnd_I;
    private DbsField pnd_Temp_State_Id;
    private DbsField pnd_Temp_Date;

    private DbsGroup pnd_Temp_Date__R_Field_4;
    private DbsField pnd_Temp_Date_Pnd_Yyyy;
    private DbsField pnd_Temp_Date_Pnd_Mm;
    private DbsField pnd_Temp_Date_Pnd_Dd;
    private DbsField pnd_Op;
    private DbsField pnd_Ia;
    private DbsField pnd_Ml;
    private DbsField pnd_Nl;
    private DbsField pnd_Record_Read;
    private DbsField pnd_Records_Sel;
    private DbsField pnd_Tot_Payments;
    private DbsField pnd_Source_Op;
    private DbsField pnd_Source_Ia;
    private DbsField pnd_Source_Nl;
    private DbsField pnd_Source_Ml;
    private DbsField pnd_W2_Written;
    private DbsField pnd_Op_Contracts;
    private DbsField pnd_Ia_Contracts;
    private DbsField pnd_Ml_Contracts;
    private DbsField pnd_Nl_Contracts;
    private DbsField pnd_Opia_Cntrcts;
    private DbsField pnd_W2_Monthly;
    private DbsField pnd_Monthly_Date_From;
    private DbsField pnd_Monthly_Date_To;
    private DbsField pnd_Monthly_Date;

    private DbsGroup pnd_Monthly_Date__R_Field_5;
    private DbsField pnd_Monthly_Date_Pnd_Year;
    private DbsField pnd_Monthly_Date_Pnd_Month;
    private DbsField pnd_Monthly_Date_Pnd_Day;
    private DbsField pnd_Compare_Date;
    private DbsField pnd_Ws_Pref;
    private int psgm001ReturnCode;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);
        ldaTwrl0900 = new LdaTwrl0900();
        registerRecord(ldaTwrl0900);
        localVariables = new DbsRecord();
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        ldaTwrlpsg1 = new LdaTwrlpsg1();
        registerRecord(ldaTwrlpsg1);

        // Local Variables

        pnd_W2_Record = localVariables.newGroupInRecord("pnd_W2_Record", "#W2-RECORD");
        pnd_W2_Record_Pnd_Tax_Year = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);

        pnd_W2_Record__R_Field_1 = pnd_W2_Record.newGroupInGroup("pnd_W2_Record__R_Field_1", "REDEFINE", pnd_W2_Record_Pnd_Tax_Year);
        pnd_W2_Record_Pnd_Tax_Year_N = pnd_W2_Record__R_Field_1.newFieldInGroup("pnd_W2_Record_Pnd_Tax_Year_N", "#TAX-YEAR-N", FieldType.NUMERIC, 4);
        pnd_W2_Record_Pnd_Tax_Id_Type = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Tax_Id_Type", "#TAX-ID-TYPE", FieldType.STRING, 1);
        pnd_W2_Record_Pnd_Tax_Id_Nbr = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.STRING, 10);
        pnd_W2_Record_Pnd_Pin_Nbr = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.STRING, 12);
        pnd_W2_Record_Pnd_Company_Code = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Company_Code", "#COMPANY-CODE", FieldType.STRING, 1);
        pnd_W2_Record_Pnd_Contract_Nbr = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Contract_Nbr", "#CONTRACT-NBR", FieldType.STRING, 8);
        pnd_W2_Record_Pnd_Payee_Cde = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.STRING, 2);
        pnd_W2_Record_Pnd_Residency_Type = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Residency_Type", "#RESIDENCY-TYPE", FieldType.STRING, 1);
        pnd_W2_Record_Pnd_Residency_Code = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Residency_Code", "#RESIDENCY-CODE", FieldType.STRING, 2);
        pnd_W2_Record_Pnd_Gross_Amount = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Gross_Amount", "#GROSS-AMOUNT", FieldType.PACKED_DECIMAL, 11, 
            2);
        pnd_W2_Record_Pnd_Federal_Tax = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Federal_Tax", "#FEDERAL-TAX", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W2_Record_Pnd_State_Tax = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_State_Tax", "#STATE-TAX", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W2_Record_Pnd_Name_And_Address = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Name_And_Address", "#NAME-AND-ADDRESS", FieldType.STRING, 
            210);

        pnd_W2_Record__R_Field_2 = pnd_W2_Record.newGroupInGroup("pnd_W2_Record__R_Field_2", "REDEFINE", pnd_W2_Record_Pnd_Name_And_Address);
        pnd_W2_Record_Pnd_Name = pnd_W2_Record__R_Field_2.newFieldInGroup("pnd_W2_Record_Pnd_Name", "#NAME", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Address_1 = pnd_W2_Record__R_Field_2.newFieldInGroup("pnd_W2_Record_Pnd_Address_1", "#ADDRESS-1", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Address_2 = pnd_W2_Record__R_Field_2.newFieldInGroup("pnd_W2_Record_Pnd_Address_2", "#ADDRESS-2", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Address_3 = pnd_W2_Record__R_Field_2.newFieldInGroup("pnd_W2_Record_Pnd_Address_3", "#ADDRESS-3", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Address_4 = pnd_W2_Record__R_Field_2.newFieldInGroup("pnd_W2_Record_Pnd_Address_4", "#ADDRESS-4", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Address_5 = pnd_W2_Record__R_Field_2.newFieldInGroup("pnd_W2_Record_Pnd_Address_5", "#ADDRESS-5", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Employer_State_Id = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Employer_State_Id", "#EMPLOYER-STATE-ID", FieldType.STRING, 
            16);

        pnd_W2_Record__R_Field_3 = pnd_W2_Record.newGroupInGroup("pnd_W2_Record__R_Field_3", "REDEFINE", pnd_W2_Record_Pnd_Employer_State_Id);
        pnd_W2_Record_Pnd_State_Code = pnd_W2_Record__R_Field_3.newFieldInGroup("pnd_W2_Record_Pnd_State_Code", "#STATE-CODE", FieldType.STRING, 2);
        pnd_W2_Record_Pnd_State_Id = pnd_W2_Record__R_Field_3.newFieldInGroup("pnd_W2_Record_Pnd_State_Id", "#STATE-ID", FieldType.STRING, 14);
        pnd_W2_Record_Pnd_Transaction_Dt = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Transaction_Dt", "#TRANSACTION-DT", FieldType.STRING, 8);
        pnd_W2_Record_Pnd_W2_Op_Ia_Ind = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_W2_Op_Ia_Ind", "#W2-OP-IA-IND", FieldType.STRING, 1);
        pnd_W2_Record_Pnd_Record_Type = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Record_Type", "#RECORD-TYPE", FieldType.NUMERIC, 1);
        pnd_W2_Record_Pnd_Sys_Dte_Time = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Sys_Dte_Time", "#SYS-DTE-TIME", FieldType.TIME);
        pnd_Gross_Amt_Y = localVariables.newFieldInRecord("pnd_Gross_Amt_Y", "#GROSS-AMT-Y", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Fed_Tax_Y = localVariables.newFieldInRecord("pnd_Fed_Tax_Y", "#FED-TAX-Y", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Sta_Tax_Y = localVariables.newFieldInRecord("pnd_Sta_Tax_Y", "#STA-TAX-Y", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Gross_Amt_M = localVariables.newFieldInRecord("pnd_Gross_Amt_M", "#GROSS-AMT-M", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Fed_Tax_M = localVariables.newFieldInRecord("pnd_Fed_Tax_M", "#FED-TAX-M", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Sta_Tax_M = localVariables.newFieldInRecord("pnd_Sta_Tax_M", "#STA-TAX-M", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Temp_State_Id = localVariables.newFieldInRecord("pnd_Temp_State_Id", "#TEMP-STATE-ID", FieldType.STRING, 14);
        pnd_Temp_Date = localVariables.newFieldInRecord("pnd_Temp_Date", "#TEMP-DATE", FieldType.STRING, 8);

        pnd_Temp_Date__R_Field_4 = localVariables.newGroupInRecord("pnd_Temp_Date__R_Field_4", "REDEFINE", pnd_Temp_Date);
        pnd_Temp_Date_Pnd_Yyyy = pnd_Temp_Date__R_Field_4.newFieldInGroup("pnd_Temp_Date_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 4);
        pnd_Temp_Date_Pnd_Mm = pnd_Temp_Date__R_Field_4.newFieldInGroup("pnd_Temp_Date_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Temp_Date_Pnd_Dd = pnd_Temp_Date__R_Field_4.newFieldInGroup("pnd_Temp_Date_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);
        pnd_Op = localVariables.newFieldInRecord("pnd_Op", "#OP", FieldType.BOOLEAN, 1);
        pnd_Ia = localVariables.newFieldInRecord("pnd_Ia", "#IA", FieldType.BOOLEAN, 1);
        pnd_Ml = localVariables.newFieldInRecord("pnd_Ml", "#ML", FieldType.BOOLEAN, 1);
        pnd_Nl = localVariables.newFieldInRecord("pnd_Nl", "#NL", FieldType.BOOLEAN, 1);
        pnd_Record_Read = localVariables.newFieldInRecord("pnd_Record_Read", "#RECORD-READ", FieldType.PACKED_DECIMAL, 8);
        pnd_Records_Sel = localVariables.newFieldInRecord("pnd_Records_Sel", "#RECORDS-SEL", FieldType.PACKED_DECIMAL, 8);
        pnd_Tot_Payments = localVariables.newFieldInRecord("pnd_Tot_Payments", "#TOT-PAYMENTS", FieldType.PACKED_DECIMAL, 8);
        pnd_Source_Op = localVariables.newFieldInRecord("pnd_Source_Op", "#SOURCE-OP", FieldType.PACKED_DECIMAL, 8);
        pnd_Source_Ia = localVariables.newFieldInRecord("pnd_Source_Ia", "#SOURCE-IA", FieldType.PACKED_DECIMAL, 8);
        pnd_Source_Nl = localVariables.newFieldInRecord("pnd_Source_Nl", "#SOURCE-NL", FieldType.PACKED_DECIMAL, 8);
        pnd_Source_Ml = localVariables.newFieldInRecord("pnd_Source_Ml", "#SOURCE-ML", FieldType.PACKED_DECIMAL, 8);
        pnd_W2_Written = localVariables.newFieldInRecord("pnd_W2_Written", "#W2-WRITTEN", FieldType.PACKED_DECIMAL, 8);
        pnd_Op_Contracts = localVariables.newFieldInRecord("pnd_Op_Contracts", "#OP-CONTRACTS", FieldType.PACKED_DECIMAL, 8);
        pnd_Ia_Contracts = localVariables.newFieldInRecord("pnd_Ia_Contracts", "#IA-CONTRACTS", FieldType.PACKED_DECIMAL, 8);
        pnd_Ml_Contracts = localVariables.newFieldInRecord("pnd_Ml_Contracts", "#ML-CONTRACTS", FieldType.PACKED_DECIMAL, 8);
        pnd_Nl_Contracts = localVariables.newFieldInRecord("pnd_Nl_Contracts", "#NL-CONTRACTS", FieldType.PACKED_DECIMAL, 8);
        pnd_Opia_Cntrcts = localVariables.newFieldInRecord("pnd_Opia_Cntrcts", "#OPIA-CNTRCTS", FieldType.PACKED_DECIMAL, 8);
        pnd_W2_Monthly = localVariables.newFieldInRecord("pnd_W2_Monthly", "#W2-MONTHLY", FieldType.PACKED_DECIMAL, 8);
        pnd_Monthly_Date_From = localVariables.newFieldInRecord("pnd_Monthly_Date_From", "#MONTHLY-DATE-FROM", FieldType.STRING, 8);
        pnd_Monthly_Date_To = localVariables.newFieldInRecord("pnd_Monthly_Date_To", "#MONTHLY-DATE-TO", FieldType.STRING, 8);
        pnd_Monthly_Date = localVariables.newFieldInRecord("pnd_Monthly_Date", "#MONTHLY-DATE", FieldType.STRING, 8);

        pnd_Monthly_Date__R_Field_5 = localVariables.newGroupInRecord("pnd_Monthly_Date__R_Field_5", "REDEFINE", pnd_Monthly_Date);
        pnd_Monthly_Date_Pnd_Year = pnd_Monthly_Date__R_Field_5.newFieldInGroup("pnd_Monthly_Date_Pnd_Year", "#YEAR", FieldType.NUMERIC, 4);
        pnd_Monthly_Date_Pnd_Month = pnd_Monthly_Date__R_Field_5.newFieldInGroup("pnd_Monthly_Date_Pnd_Month", "#MONTH", FieldType.NUMERIC, 2);
        pnd_Monthly_Date_Pnd_Day = pnd_Monthly_Date__R_Field_5.newFieldInGroup("pnd_Monthly_Date_Pnd_Day", "#DAY", FieldType.NUMERIC, 2);
        pnd_Compare_Date = localVariables.newFieldInRecord("pnd_Compare_Date", "#COMPARE-DATE", FieldType.STRING, 8);
        pnd_Ws_Pref = localVariables.newFieldInRecord("pnd_Ws_Pref", "#WS-PREF", FieldType.STRING, 20);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0600.initializeValues();
        ldaTwrl0900.initializeValues();
        ldaTwrlpsg1.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5706() throws Exception
    {
        super("Twrp5706");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP5706", onError);
        setupReports();
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: FORMAT LS = 132;//Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-CONTROL-RECORD
        sub_Process_Control_Record();
        if (condition(Global.isEscape())) {return;}
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 2 RECORD #XTAXYR-F94
        while (condition(getWorkFiles().read(2, ldaTwrl0900.getPnd_Xtaxyr_F94())))
        {
            CheckAtStartofData315();

            //*                                                                                                                                                           //Natural: AT START OF DATA
            pnd_Record_Read.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #RECORD-READ
            //*  IF TWRPYMNT-DISTRIBUTION-CDE  NE '7'
            //*    ESCAPE TOP
            //*  END-IF
            pnd_Op.setValue(false);                                                                                                                                       //Natural: MOVE FALSE TO #OP #IA #ML #NL
            pnd_Ia.setValue(false);
            pnd_Ml.setValue(false);
            pnd_Nl.setValue(false);
            //*   ALL PAYMENTS
            pnd_Records_Sel.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #RECORDS-SEL
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO #C-TWRPYMNT-PAYMENTS
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments())); pnd_I.nadd(1))
            {
                if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payment_Type().getValue(pnd_I).equals("M") && ((((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_I).equals("S")  //Natural: IF ( TWRPYMNT-PAYMENT-TYPE ( #I ) = 'M' AND ( TWRPYMNT-SETTLE-TYPE ( #I ) = 'S' OR = 'T' OR = 'E' OR = 'M' OR = 'U' ) )
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_I).equals("T")) || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_I).equals("E")) 
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_I).equals("M")) || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_I).equals("U")))))
                {
                    if (condition(pnd_W2_Record_Pnd_Transaction_Dt.equals(" ")))                                                                                          //Natural: IF #W2-RECORD.#TRANSACTION-DT = ' '
                    {
                        pnd_W2_Record_Pnd_Transaction_Dt.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Dte().getValue(pnd_I));                                    //Natural: ASSIGN #W2-RECORD.#TRANSACTION-DT := TWRPYMNT-PYMNT-DTE ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                    //*  JRR 05/28/10
                    pnd_W2_Record_Pnd_Sys_Dte_Time.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Sys_Dte_Time().getValue(pnd_I)); //Natural: MOVE EDITED TWRPYMNT-SYS-DTE-TIME ( #I ) TO #W2-RECORD.#SYS-DTE-TIME ( EM = YYYYMMDDHHIISST )
                                                                                                                                                                          //Natural: PERFORM PAYMENT-SOURCE-COUNTS
                    sub_Payment_Source_Counts();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_I).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_I));            //Natural: ADD TWRPYMNT-INT-AMT ( #I ) TO TWRPYMNT-GROSS-AMT ( #I )
                    pnd_Gross_Amt_Y.nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_I));                                                             //Natural: ADD TWRPYMNT-GROSS-AMT ( #I ) TO #GROSS-AMT-Y
                    pnd_Fed_Tax_Y.nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_I));                                                           //Natural: ADD TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #FED-TAX-Y
                    pnd_Sta_Tax_Y.nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_I));                                                             //Natural: ADD TWRPYMNT-STATE-WTHLD ( #I ) TO #STA-TAX-Y
                                                                                                                                                                          //Natural: PERFORM SET-COMPARE-DATE
                    sub_Set_Compare_Date();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Compare_Date.greaterOrEqual(pnd_Monthly_Date_From) && pnd_Compare_Date.lessOrEqual(pnd_Monthly_Date_To)))                           //Natural: IF #COMPARE-DATE GE #MONTHLY-DATE-FROM AND #COMPARE-DATE LE #MONTHLY-DATE-TO
                    {
                        pnd_Gross_Amt_M.nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_I));                                                         //Natural: ADD TWRPYMNT-GROSS-AMT ( #I ) TO #GROSS-AMT-M
                        pnd_Fed_Tax_M.nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_I));                                                       //Natural: ADD TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #FED-TAX-M
                        pnd_Sta_Tax_M.nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_I));                                                         //Natural: ADD TWRPYMNT-STATE-WTHLD ( #I ) TO #STA-TAX-M
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Gross_Amt_Y.notEquals(getZero())))                                                                                                          //Natural: IF #GROSS-AMT-Y NE 0
            {
                //*  YEAR-TO-DATE
                                                                                                                                                                          //Natural: PERFORM GET-NAME-ADDRESS
                sub_Get_Name_Address();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_W2_Record_Pnd_Record_Type.setValue(1);                                                                                                                //Natural: ASSIGN #W2-RECORD.#RECORD-TYPE := 1
                pnd_W2_Record_Pnd_Gross_Amount.setValue(pnd_Gross_Amt_Y);                                                                                                 //Natural: ASSIGN #W2-RECORD.#GROSS-AMOUNT := #GROSS-AMT-Y
                pnd_W2_Record_Pnd_Federal_Tax.setValue(pnd_Fed_Tax_Y);                                                                                                    //Natural: ASSIGN #W2-RECORD.#FEDERAL-TAX := #FED-TAX-Y
                pnd_W2_Record_Pnd_State_Tax.setValue(pnd_Sta_Tax_Y);                                                                                                      //Natural: ASSIGN #W2-RECORD.#STATE-TAX := #STA-TAX-Y
                                                                                                                                                                          //Natural: PERFORM SETUP-W2-RECORD
                sub_Setup_W2_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM GET-STATE-INFO
                sub_Get_State_Info();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getWorkFiles().write(3, false, pnd_W2_Record);                                                                                                            //Natural: WRITE WORK FILE 3 #W2-RECORD
                pnd_W2_Written.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #W2-WRITTEN
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Gross_Amt_M.notEquals(getZero())))                                                                                                          //Natural: IF #GROSS-AMT-M NE 0
            {
                if (condition(pnd_Gross_Amt_Y.equals(getZero())))                                                                                                         //Natural: IF #GROSS-AMT-Y = 0
                {
                    //*      PERFORM GET-NAME-ADDRESS         /* FE201608
                                                                                                                                                                          //Natural: PERFORM SETUP-W2-RECORD
                    sub_Setup_W2_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM GET-STATE-INFO
                    sub_Get_State_Info();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  MONTHLY
                }                                                                                                                                                         //Natural: END-IF
                pnd_W2_Record_Pnd_Record_Type.setValue(2);                                                                                                                //Natural: ASSIGN #W2-RECORD.#RECORD-TYPE := 2
                pnd_W2_Record_Pnd_Gross_Amount.setValue(pnd_Gross_Amt_M);                                                                                                 //Natural: ASSIGN #W2-RECORD.#GROSS-AMOUNT := #GROSS-AMT-M
                pnd_W2_Record_Pnd_Federal_Tax.setValue(pnd_Fed_Tax_M);                                                                                                    //Natural: ASSIGN #W2-RECORD.#FEDERAL-TAX := #FED-TAX-M
                pnd_W2_Record_Pnd_State_Tax.setValue(pnd_Sta_Tax_M);                                                                                                      //Natural: ASSIGN #W2-RECORD.#STATE-TAX := #STA-TAX-M
                getWorkFiles().write(3, false, pnd_W2_Record);                                                                                                            //Natural: WRITE WORK FILE 3 #W2-RECORD
                pnd_W2_Monthly.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #W2-MONTHLY
            }                                                                                                                                                             //Natural: END-IF
            pnd_W2_Record.reset();                                                                                                                                        //Natural: RESET #W2-RECORD #GROSS-AMT-Y #FED-TAX-Y #STA-TAX-Y #GROSS-AMT-M #FED-TAX-M #STA-TAX-M
            pnd_Gross_Amt_Y.reset();
            pnd_Fed_Tax_Y.reset();
            pnd_Sta_Tax_Y.reset();
            pnd_Gross_Amt_M.reset();
            pnd_Fed_Tax_M.reset();
            pnd_Sta_Tax_M.reset();
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, "Total records read from Payment file =",pnd_Record_Read);                                                                                  //Natural: WRITE 'Total records read from Payment file =' #RECORD-READ
        if (Global.isEscape()) return;
        getReports().write(0, "Total payments for 2009              =",pnd_Records_Sel);                                                                                  //Natural: WRITE 'Total payments for 2009              =' #RECORDS-SEL
        if (Global.isEscape()) return;
        getReports().write(0, "Total W2 Payments                    =",pnd_Tot_Payments);                                                                                 //Natural: WRITE 'Total W2 Payments                    =' #TOT-PAYMENTS
        if (Global.isEscape()) return;
        getReports().write(0, "   Omni Payments                     =",pnd_Source_Op);                                                                                    //Natural: WRITE '   Omni Payments                     =' #SOURCE-OP
        if (Global.isEscape()) return;
        getReports().write(0, "   IA Payments                       =",pnd_Source_Ia);                                                                                    //Natural: WRITE '   IA Payments                       =' #SOURCE-IA
        if (Global.isEscape()) return;
        getReports().write(0, "   ML Payments                       =",pnd_Source_Ml);                                                                                    //Natural: WRITE '   ML Payments                       =' #SOURCE-ML
        if (Global.isEscape()) return;
        getReports().write(0, "   NL Payments                       =",pnd_Source_Nl);                                                                                    //Natural: WRITE '   NL Payments                       =' #SOURCE-NL
        if (Global.isEscape()) return;
        getReports().write(0, "Total Contracts for Yearly Report    =",pnd_W2_Written);                                                                                   //Natural: WRITE 'Total Contracts for Yearly Report    =' #W2-WRITTEN
        if (Global.isEscape()) return;
        getReports().write(0, "      OP Contracts                   =",pnd_Op_Contracts);                                                                                 //Natural: WRITE '      OP Contracts                   =' #OP-CONTRACTS
        if (Global.isEscape()) return;
        getReports().write(0, "      IA Contracts                   =",pnd_Ia_Contracts);                                                                                 //Natural: WRITE '      IA Contracts                   =' #IA-CONTRACTS
        if (Global.isEscape()) return;
        getReports().write(0, "      ML Contracts                   =",pnd_Ml_Contracts);                                                                                 //Natural: WRITE '      ML Contracts                   =' #ML-CONTRACTS
        if (Global.isEscape()) return;
        getReports().write(0, "      NL Contracts                   =",pnd_Nl_Contracts);                                                                                 //Natural: WRITE '      NL Contracts                   =' #NL-CONTRACTS
        if (Global.isEscape()) return;
        getReports().write(0, "      Both OP & IA Contracts         =",pnd_Opia_Cntrcts);                                                                                 //Natural: WRITE '      Both OP & IA Contracts         =' #OPIA-CNTRCTS
        if (Global.isEscape()) return;
        getReports().write(0, "Total Contracts for Monthly Report   =",pnd_W2_Monthly);                                                                                   //Natural: WRITE 'Total Contracts for Monthly Report   =' #W2-MONTHLY
        if (Global.isEscape()) return;
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NAME-ADDRESS
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-W2-RECORD
        //* **************************************
        //*  #W2-RECORD.#TAX-YEAR       := #YYYY
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PAYMENT-SOURCE-COUNTS
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-COMPARE-DATE
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CONTROL-RECORD
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATE-INFO
        //* ********************************
        //* ********
        //* ********                                                                                                                                                      //Natural: ON ERROR
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-ROUTINE
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Get_Name_Address() throws Exception                                                                                                                  //Natural: GET-NAME-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        //*  #TWRAPART.#FUNCTION    := 'R'   /* FE201608 COMMENT OUT START
        //*  #TWRAPART.#DISPLAY-IND := TRUE
        //*  #TWRAPART.#TAX-YEAR    := *DATN / 10000
        //*  #TWRAPART.#TIN         :=  TWRPYMNT-TAX-ID-NBR
        //*  CALLNAT 'TWRNPART' USING #TWRAPART /* FE201608 COMMENT OUT END
        //*  FE201608 START
        //*  FE201608 START
        ldaTwrlpsg1.getPnd_Psgm001().reset();                                                                                                                             //Natural: RESET #PSGM001
        ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Function().setValue("PR");                                                                                                      //Natural: ASSIGN #IN-FUNCTION := 'PR'
        ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Addr_Usg_Typ().setValue("CO");                                                                                                  //Natural: ASSIGN #IN-ADDR-USG-TYP := 'CO'
        ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Type().setValue("SSN");                                                                                                         //Natural: ASSIGN #IN-TYPE := 'SSN'
        ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Pin_Ssn().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr());                                                        //Natural: MOVE TWRPYMNT-TAX-ID-NBR TO #IN-PIN-SSN
        //*  PIN-EXP TEST
        psgm001ReturnCode = DbsUtil.callExternalProgram("PSGM001",ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Function(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Addr_Usg_Typ(),          //Natural: CALL 'PSGM001' #PSGM001
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Type(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Pin_Ssn(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Ret_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Input_String(),
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Out_Pin(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Out_Ssn(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Inact_Date(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Birth_Date(),
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Dec_Date(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Gender_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Last_Name(),
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_First_Name(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Second_Name(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Suffix_Desc(),
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Person_Org_Cd(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_1(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_2(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_3(),
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_4(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_5(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_City(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Postal_Code(),
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Iso_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Country(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Irs_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Irs_Country_Nm(),
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_State_Nm(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Geo_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_Type_Cd(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Rcrd_Status());
        if (condition(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Ret_Code().equals("00") && ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Last_Name().notEquals(" ") && ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_1().notEquals(" "))) //Natural: IF #RT-RET-CODE = '00' AND #RT-LAST-NAME NE ' ' AND #RT-ADDR-1 NE ' '
        {
            if (condition(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref().equals("UNKNOWN")))                                                                                    //Natural: IF #RT-PREF = 'UNKNOWN'
            {
                ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref().reset();                                                                                                         //Natural: RESET #RT-PREF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref().equals("MR") || ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref().equals("MR.") || ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref().equals("MRS")  //Natural: IF #RT-PREF = 'MR' OR = 'MR.' OR = 'MRS' OR = 'MRS.' OR = 'MS' OR = 'MS.'
                || ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref().equals("MRS.") || ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref().equals("MS") || ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref().equals("MS.")))
            {
                pnd_W2_Record_Pnd_Name.setValue(DbsUtil.compress(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_First_Name(), ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Second_Name(),         //Natural: COMPRESS #RT-FIRST-NAME #RT-SECOND-NAME #RT-LAST-NAME #RT-SUFFIX-DESC INTO #W2-RECORD.#NAME
                    ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Last_Name(), ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Suffix_Desc()));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_W2_Record_Pnd_Name.setValue(DbsUtil.compress(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref(), ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_First_Name(),                //Natural: COMPRESS #RT-PREF #RT-FIRST-NAME #RT-SECOND-NAME #RT-LAST-NAME #RT-SUFFIX-DESC INTO #W2-RECORD.#NAME
                    ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Second_Name(), ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Last_Name(), ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Suffix_Desc()));
            }                                                                                                                                                             //Natural: END-IF
            pnd_W2_Record_Pnd_Address_1.setValue(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_1());                                                                             //Natural: MOVE #RT-ADDR-1 TO #W2-RECORD.#ADDRESS-1
            pnd_W2_Record_Pnd_Address_2.setValue(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_2());                                                                             //Natural: MOVE #RT-ADDR-2 TO #W2-RECORD.#ADDRESS-2
            pnd_W2_Record_Pnd_Address_3.setValue(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_3());                                                                             //Natural: MOVE #RT-ADDR-3 TO #W2-RECORD.#ADDRESS-3
            pnd_W2_Record_Pnd_Address_4.setValue(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_4());                                                                             //Natural: MOVE #RT-ADDR-4 TO #W2-RECORD.#ADDRESS-4
            pnd_W2_Record_Pnd_Address_5.setValue(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_5());                                                                             //Natural: MOVE #RT-ADDR-5 TO #W2-RECORD.#ADDRESS-5
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  FE201608 START  /* PIN-EXP TEST
            //*  FE201608 START
            ldaTwrlpsg1.getPnd_Psgm001().reset();                                                                                                                         //Natural: RESET #PSGM001
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Function().setValue("PR");                                                                                                  //Natural: ASSIGN #IN-FUNCTION := 'PR'
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Addr_Usg_Typ().setValue("RS");                                                                                              //Natural: ASSIGN #IN-ADDR-USG-TYP := 'RS'
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Type().setValue("SSN");                                                                                                     //Natural: ASSIGN #IN-TYPE := 'SSN'
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Pin_Ssn().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr());                                                    //Natural: MOVE TWRPYMNT-TAX-ID-NBR TO #IN-PIN-SSN
            psgm001ReturnCode = DbsUtil.callExternalProgram("PSGM001",ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Function(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Addr_Usg_Typ(),      //Natural: CALL 'PSGM001' #PSGM001
                ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Type(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Pin_Ssn(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Ret_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Input_String(),
                ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Out_Pin(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Out_Ssn(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Inact_Date(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Birth_Date(),
                ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Dec_Date(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Gender_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Last_Name(),
                ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_First_Name(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Second_Name(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Suffix_Desc(),
                ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Person_Org_Cd(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_1(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_2(),
                ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_3(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_4(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_5(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_City(),
                ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Postal_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Iso_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Country(),
                ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Irs_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Irs_Country_Nm(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_State_Nm(),
                ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Geo_Code(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_Type_Cd(),ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Rcrd_Status());
            if (condition(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Ret_Code().equals("00") && ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Last_Name().notEquals(" ") &&                    //Natural: IF #RT-RET-CODE = '00' AND #RT-LAST-NAME NE ' ' AND #RT-ADDR-1 NE ' '
                ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_1().notEquals(" ")))
            {
                if (condition(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref().equals("UNKNOWN")))                                                                                //Natural: IF #RT-PREF = 'UNKNOWN'
                {
                    ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref().reset();                                                                                                     //Natural: RESET #RT-PREF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref().equals("MR") || ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref().equals("MR.") || ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref().equals("MRS")  //Natural: IF #RT-PREF = 'MR' OR = 'MR.' OR = 'MRS' OR = 'MRS.' OR = 'MS' OR = 'MS.'
                    || ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref().equals("MRS.") || ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref().equals("MS") || ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref().equals("MS.")))
                {
                    pnd_W2_Record_Pnd_Name.setValue(DbsUtil.compress(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_First_Name(), ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Second_Name(),     //Natural: COMPRESS #RT-FIRST-NAME #RT-SECOND-NAME #RT-LAST-NAME #RT-SUFFIX-DESC INTO #W2-RECORD.#NAME
                        ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Last_Name(), ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Suffix_Desc()));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_W2_Record_Pnd_Name.setValue(DbsUtil.compress(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Pref(), ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_First_Name(),            //Natural: COMPRESS #RT-PREF #RT-FIRST-NAME #RT-SECOND-NAME #RT-LAST-NAME #RT-SUFFIX-DESC INTO #W2-RECORD.#NAME
                        ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Second_Name(), ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Last_Name(), ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Suffix_Desc()));
                }                                                                                                                                                         //Natural: END-IF
                pnd_W2_Record_Pnd_Address_1.setValue(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_1());                                                                         //Natural: MOVE #RT-ADDR-1 TO #W2-RECORD.#ADDRESS-1
                pnd_W2_Record_Pnd_Address_2.setValue(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_2());                                                                         //Natural: MOVE #RT-ADDR-2 TO #W2-RECORD.#ADDRESS-2
                pnd_W2_Record_Pnd_Address_3.setValue(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_3());                                                                         //Natural: MOVE #RT-ADDR-3 TO #W2-RECORD.#ADDRESS-3
                pnd_W2_Record_Pnd_Address_4.setValue(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_4());                                                                         //Natural: MOVE #RT-ADDR-4 TO #W2-RECORD.#ADDRESS-4
                //*  FE201506 END
                pnd_W2_Record_Pnd_Address_5.setValue(ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_5());                                                                         //Natural: MOVE #RT-ADDR-5 TO #W2-RECORD.#ADDRESS-5
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_W2_Record_Pnd_Name.setValue("*** PARTICIPANT NAME NOT FOUND ***");                                                                                    //Natural: MOVE '*** PARTICIPANT NAME NOT FOUND ***' TO #W2-RECORD.#NAME
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  MB
    private void sub_Setup_W2_Record() throws Exception                                                                                                                   //Natural: SETUP-W2-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_W2_Record_Pnd_Company_Code.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form());                                                               //Natural: ASSIGN #W2-RECORD.#COMPANY-CODE := TWRPYMNT-COMPANY-CDE-FORM
        pnd_W2_Record_Pnd_Tax_Year.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());                                                                           //Natural: ASSIGN #W2-RECORD.#TAX-YEAR := #XTAXYR-F94.TWRPYMNT-TAX-YEAR
        pnd_W2_Record_Pnd_Tax_Id_Type.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Type());                                                                     //Natural: ASSIGN #W2-RECORD.#TAX-ID-TYPE := TWRPYMNT-TAX-ID-TYPE
        pnd_W2_Record_Pnd_Tax_Id_Nbr.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr());                                                                       //Natural: ASSIGN #W2-RECORD.#TAX-ID-NBR := TWRPYMNT-TAX-ID-NBR
        pnd_W2_Record_Pnd_Contract_Nbr.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr());                                                                   //Natural: ASSIGN #W2-RECORD.#CONTRACT-NBR := TWRPYMNT-CONTRACT-NBR
        pnd_W2_Record_Pnd_Payee_Cde.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payee_Cde());                                                                         //Natural: ASSIGN #W2-RECORD.#PAYEE-CDE := TWRPYMNT-PAYEE-CDE
        pnd_W2_Record_Pnd_Residency_Type.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Type());                                                               //Natural: ASSIGN #W2-RECORD.#RESIDENCY-TYPE := TWRPYMNT-RESIDENCY-TYPE
        pnd_W2_Record_Pnd_Residency_Code.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code());                                                               //Natural: ASSIGN #W2-RECORD.#RESIDENCY-CODE := TWRPYMNT-RESIDENCY-CODE
        //*  #W2-RECORD.#TRANSACTION-DT := TWRPYMNT-UPDT-DTE
        if (condition(pnd_Op.getBoolean() && pnd_Ia.getBoolean()))                                                                                                        //Natural: IF #OP AND #IA
        {
            if (condition(pnd_Gross_Amt_Y.notEquals(getZero())))                                                                                                          //Natural: IF #GROSS-AMT-Y NE 0
            {
                pnd_Opia_Cntrcts.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #OPIA-CNTRCTS
            }                                                                                                                                                             //Natural: END-IF
            pnd_W2_Record_Pnd_W2_Op_Ia_Ind.setValue(5);                                                                                                                   //Natural: MOVE 5 TO #W2-OP-IA-IND
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Op.getBoolean()))                                                                                                                           //Natural: IF #OP
            {
                if (condition(pnd_Gross_Amt_Y.notEquals(getZero())))                                                                                                      //Natural: IF #GROSS-AMT-Y NE 0
                {
                    pnd_Op_Contracts.nadd(1);                                                                                                                             //Natural: ADD 1 TO #OP-CONTRACTS
                }                                                                                                                                                         //Natural: END-IF
                pnd_W2_Record_Pnd_W2_Op_Ia_Ind.setValue(1);                                                                                                               //Natural: MOVE 1 TO #W2-OP-IA-IND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Ia.getBoolean()))                                                                                                                       //Natural: IF #IA
                {
                    if (condition(pnd_Gross_Amt_Y.notEquals(getZero())))                                                                                                  //Natural: IF #GROSS-AMT-Y NE 0
                    {
                        pnd_Ia_Contracts.nadd(1);                                                                                                                         //Natural: ADD 1 TO #IA-CONTRACTS
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_W2_Record_Pnd_W2_Op_Ia_Ind.setValue(2);                                                                                                           //Natural: MOVE 2 TO #W2-OP-IA-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Nl.getBoolean()))                                                                                                                   //Natural: IF #NL
                    {
                        if (condition(pnd_Gross_Amt_Y.notEquals(getZero())))                                                                                              //Natural: IF #GROSS-AMT-Y NE 0
                        {
                            pnd_Nl_Contracts.nadd(1);                                                                                                                     //Natural: ADD 1 TO #NL-CONTRACTS
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_W2_Record_Pnd_W2_Op_Ia_Ind.setValue(3);                                                                                                       //Natural: MOVE 3 TO #W2-OP-IA-IND
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Ml.getBoolean()))                                                                                                               //Natural: IF #ML
                        {
                            if (condition(pnd_Gross_Amt_Y.notEquals(getZero())))                                                                                          //Natural: IF #GROSS-AMT-Y NE 0
                            {
                                pnd_Ml_Contracts.nadd(1);                                                                                                                 //Natural: ADD 1 TO #ML-CONTRACTS
                            }                                                                                                                                             //Natural: END-IF
                            pnd_W2_Record_Pnd_W2_Op_Ia_Ind.setValue(4);                                                                                                   //Natural: MOVE 4 TO #W2-OP-IA-IND
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Payment_Source_Counts() throws Exception                                                                                                             //Natural: PAYMENT-SOURCE-COUNTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        pnd_Tot_Payments.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TOT-PAYMENTS
        if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_I).equals("E") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_I).equals("M")  //Natural: IF ( TWRPYMNT-SETTLE-TYPE ( #I ) = 'E' OR = 'M' OR = 'T' )
            || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_I).equals("T")))
        {
            pnd_Source_Op.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #SOURCE-OP
            pnd_Op.setValue(true);                                                                                                                                        //Natural: MOVE TRUE TO #OP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).equals("OP")))                                                           //Natural: IF TWRPYMNT-ORGN-SRCE-CDE ( #I ) = 'OP'
            {
                pnd_Source_Op.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #SOURCE-OP
                pnd_Op.setValue(true);                                                                                                                                    //Natural: MOVE TRUE TO #OP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).equals("AP")))                                                       //Natural: IF TWRPYMNT-ORGN-SRCE-CDE ( #I ) = 'AP'
                {
                    pnd_Source_Ia.nadd(1);                                                                                                                                //Natural: ADD 1 TO #SOURCE-IA
                    pnd_Ia.setValue(true);                                                                                                                                //Natural: MOVE TRUE TO #IA
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I).equals("ML")))                                                  //Natural: IF TWRPYMNT-UPDTE-SRCE-CDE ( #I ) = 'ML'
                    {
                        pnd_Source_Ml.nadd(1);                                                                                                                            //Natural: ADD 1 TO #SOURCE-ML
                        pnd_Ml.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #ML
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Source_Nl.nadd(1);                                                                                                                            //Natural: ADD 1 TO #SOURCE-NL
                        pnd_Nl.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #NL
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Set_Compare_Date() throws Exception                                                                                                                  //Natural: SET-COMPARE-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pnd_Compare_Date.reset();                                                                                                                                         //Natural: RESET #COMPARE-DATE
        if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Intfce_Dte().getValue(pnd_I).greaterOrEqual(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Dte().getValue(pnd_I)))) //Natural: IF TWRPYMNT-PYMNT-INTFCE-DTE ( #I ) >= TWRPYMNT-PYMNT-DTE ( #I )
        {
            pnd_Compare_Date.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Intfce_Dte().getValue(pnd_I));                                                         //Natural: ASSIGN #COMPARE-DATE := TWRPYMNT-PYMNT-INTFCE-DTE ( #I )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Compare_Date.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Dte().getValue(pnd_I));                                                                //Natural: ASSIGN #COMPARE-DATE := TWRPYMNT-PYMNT-DTE ( #I )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Control_Record() throws Exception                                                                                                            //Natural: PROCESS-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        getWorkFiles().read(1, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                             //Natural: READ WORK FILE 1 ONCE #TWRP0600-CONTROL-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"CONTROL RECORD IS EMPTY",new TabSetting(77),"***");                                      //Natural: WRITE ( 1 ) '***' 25T 'CONTROL RECORD IS EMPTY' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(101);  if (true) return;                                                                                                                    //Natural: TERMINATE 101
        }                                                                                                                                                                 //Natural: END-ENDFILE
        getReports().write(0, "FROM DATE",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd());                                                       //Natural: WRITE 'FROM DATE' #TWRP0600-FROM-CCYYMMDD
        if (Global.isEscape()) return;
        getReports().write(0, "TO   DATE",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd());                                                         //Natural: WRITE 'TO   DATE' #TWRP0600-TO-CCYYMMDD
        if (Global.isEscape()) return;
        getReports().write(0, "TAX  YEAR",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy());                                                       //Natural: WRITE 'TAX  YEAR' #TWRP0600-TAX-YEAR-CCYY
        if (Global.isEscape()) return;
        getReports().write(0, "INTF DATE",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd());                                                  //Natural: WRITE 'INTF DATE' #TWRP0600-INTERFACE-CCYYMMDD
        if (Global.isEscape()) return;
        pnd_Temp_Date.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd());                                                             //Natural: MOVE #TWRP0600-INTERFACE-CCYYMMDD TO #TEMP-DATE
        if (condition(pnd_Temp_Date_Pnd_Mm.less(3)))                                                                                                                      //Natural: IF #MM < 3
        {
            pnd_Temp_Date_Pnd_Yyyy.nsubtract(1);                                                                                                                          //Natural: COMPUTE #YYYY = #YYYY - 1
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Temp_Date_Pnd_Mm.setValue(1);                                                                                                                                 //Natural: MOVE 1 TO #MM #DD
        pnd_Temp_Date_Pnd_Dd.setValue(1);
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd().setValue(pnd_Temp_Date);                                                                  //Natural: MOVE #TEMP-DATE TO #TWRP0600-FROM-CCYYMMDD
        getReports().write(0, "NEW FROM DATE",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd());                                                   //Natural: WRITE 'NEW FROM DATE' #TWRP0600-FROM-CCYYMMDD
        if (Global.isEscape()) return;
        pnd_Monthly_Date.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd());                                                          //Natural: MOVE #TWRP0600-INTERFACE-CCYYMMDD TO #MONTHLY-DATE
        if (condition(pnd_Monthly_Date_Pnd_Day.less(26)))                                                                                                                 //Natural: IF #DAY < 26
        {
            if (condition(pnd_Monthly_Date_Pnd_Month.equals(1)))                                                                                                          //Natural: IF #MONTH = 1
            {
                pnd_Monthly_Date_Pnd_Year.nsubtract(1);                                                                                                                   //Natural: COMPUTE #YEAR = #YEAR - 1
                pnd_Monthly_Date_Pnd_Month.setValue(12);                                                                                                                  //Natural: MOVE 12 TO #MONTH
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Monthly_Date_Pnd_Month.nsubtract(1);                                                                                                                  //Natural: COMPUTE #MONTH = #MONTH - 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Monthly_Date_Pnd_Day.setValue(1);                                                                                                                             //Natural: MOVE 1 TO #DAY
        pnd_Monthly_Date_From.setValue(pnd_Monthly_Date);                                                                                                                 //Natural: MOVE #MONTHLY-DATE TO #MONTHLY-DATE-FROM
        pnd_Monthly_Date_Pnd_Day.setValue(31);                                                                                                                            //Natural: MOVE 31 TO #DAY
        pnd_Monthly_Date_To.setValue(pnd_Monthly_Date);                                                                                                                   //Natural: MOVE #MONTHLY-DATE TO #MONTHLY-DATE-TO
        getReports().write(0, "=",pnd_Monthly_Date_From);                                                                                                                 //Natural: WRITE '=' #MONTHLY-DATE-FROM
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Monthly_Date_To);                                                                                                                   //Natural: WRITE '=' #MONTHLY-DATE-TO
        if (Global.isEscape()) return;
    }
    private void sub_Get_State_Info() throws Exception                                                                                                                    //Natural: GET-STATE-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().setValue(pnd_W2_Record_Pnd_Tax_Year_N);                                                                                    //Natural: ASSIGN TWRATBL2.#TAX-YEAR := #W2-RECORD.#TAX-YEAR-N
        pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().setValue(false);                                                                                                          //Natural: ASSIGN TWRATBL2.#ABEND-IND := FALSE
        pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().setValue(false);                                                                                                        //Natural: ASSIGN TWRATBL2.#DISPLAY-IND := FALSE
        if (condition(DbsUtil.maskMatches(pnd_W2_Record_Pnd_Residency_Code,"NN")))                                                                                        //Natural: IF #RESIDENCY-CODE = MASK ( NN )
        {
            pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                         //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "0", pnd_W2_Record_Pnd_Residency_Code));                     //Natural: COMPRESS '0' #RESIDENCY-CODE INTO TWRATBL2.#STATE-CDE LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("2");                                                                                                         //Natural: ASSIGN TWRATBL2.#FUNCTION := '2'
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(pnd_W2_Record_Pnd_Residency_Code);                                                                           //Natural: ASSIGN TWRATBL2.#STATE-CDE := #RESIDENCY-CODE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        short decideConditionsMet632 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #W2-RECORD.#COMPANY-CODE;//Natural: VALUE 'T'
        if (condition((pnd_W2_Record_Pnd_Company_Code.equals("T"))))
        {
            decideConditionsMet632++;
            pnd_Temp_State_Id.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                                                                              //Natural: MOVE TWRATBL2.TIRCNTL-STATE-TAX-ID-TIAA TO #TEMP-STATE-ID
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((pnd_W2_Record_Pnd_Company_Code.equals("X"))))
        {
            decideConditionsMet632++;
            pnd_Temp_State_Id.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Trust());                                                                             //Natural: MOVE TWRATBL2.TIRCNTL-STATE-TAX-ID-TRUST TO #TEMP-STATE-ID
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet632 > 0))
        {
            pnd_W2_Record_Pnd_State_Code.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code());                                                                    //Natural: MOVE TIRCNTL-STATE-ALPHA-CODE TO #STATE-CODE
            pnd_W2_Record_Pnd_State_Id.setValue(pnd_Temp_State_Id);                                                                                                       //Natural: MOVE #TEMP-STATE-ID TO #STATE-ID
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Error_Routine() throws Exception                                                                                                                     //Natural: ERROR-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        getReports().write(0, "ERR: ",Global.getERROR_NR());                                                                                                              //Natural: WRITE '=' *ERROR-NR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_W2_Record_Pnd_Tax_Id_Nbr);                                                                                                          //Natural: WRITE '=' #W2-RECORD.#TAX-ID-NBR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_W2_Record_Pnd_Address_1);                                                                                                           //Natural: WRITE '=' #W2-RECORD.#ADDRESS-1
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_W2_Record_Pnd_Address_2);                                                                                                           //Natural: WRITE '=' #W2-RECORD.#ADDRESS-2
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_W2_Record_Pnd_Address_3);                                                                                                           //Natural: WRITE '=' #W2-RECORD.#ADDRESS-3
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_W2_Record_Pnd_Address_4);                                                                                                           //Natural: WRITE '=' #W2-RECORD.#ADDRESS-4
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_W2_Record_Pnd_Name);                                                                                                                //Natural: WRITE '=' #W2-RECORD.#NAME
        if (Global.isEscape()) return;
        getReports().write(0, "=",ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_1(), new AlphanumericLength (50));                                                               //Natural: WRITE '=' #RT-ADDR-1 ( AL = 50 )
        if (Global.isEscape()) return;
        getReports().write(0, "=",ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_2(), new AlphanumericLength (50));                                                               //Natural: WRITE '=' #RT-ADDR-2 ( AL = 50 )
        if (Global.isEscape()) return;
        getReports().write(0, "=",ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_3(), new AlphanumericLength (50));                                                               //Natural: WRITE '=' #RT-ADDR-3 ( AL = 50 )
        if (Global.isEscape()) return;
        getReports().write(0, "=",ldaTwrlpsg1.getPnd_Psgm001_Pnd_Rt_Addr_4(), new AlphanumericLength (50));                                                               //Natural: WRITE '=' #RT-ADDR-4 ( AL = 50 )
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
                                                                                                                                                                          //Natural: PERFORM ERROR-ROUTINE
        sub_Error_Routine();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132");
    }
    private void CheckAtStartofData315() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_W2_Record_Pnd_Tax_Year_N.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());                                                                     //Natural: ASSIGN #TAX-YEAR-N := #XTAXYR-F94.TWRPYMNT-TAX-YEAR
            //*  $$$$
            //*    #YYYY                         := #XTAXYR-F94.TWRPYMNT-TAX-YEAR
            //*  $$$$
        }                                                                                                                                                                 //Natural: END-START
    }
}
