/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:46:31 PM
**        * FROM NATURAL PROGRAM : Txwp5065
************************************************************
**        * FILE NAME            : Txwp5065.java
**        * CLASS NAME           : Txwp5065
**        * INSTANCE NAME        : Txwp5065
************************************************************
************************************************************************
* SUBPROGRAM: TXWP5065
* FUNCTION  : REPORT W8 OVERRIDE FLAG UPDATE ACTIVITY
* PROGRAMMER: JONATHAN ROTHOLZ - 10/11/2001
* HISTORY...:
*    06/10/15 FENDAYA COR AND NAS SUNSET. FE201608
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Txwp5065 extends BLNatBase
{
    // Data Areas
    private PdaTwratin pdaTwratin;
    private PdaTxwapart pdaTxwapart;
    private LdaTxwl2425 ldaTxwl2425;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_part_View2;
    private DbsField part_View2_Tin;
    private DbsField part_View2_Tin_Type;
    private DbsField part_View2_Active_Ind;
    private DbsField part_View2_Citizen_Cde;
    private DbsField part_View2_Create_User;
    private DbsField part_View2_Lu_User;
    private DbsField part_View2_Lu_Ts;
    private DbsField part_View2_Create_Ts;
    private DbsField part_View2_Invrse_Create_Ts;
    private DbsField part_View2_W8_Ben_Country;
    private DbsField part_View2_W8_Ben_Override;

    private DbsGroup pnd_Txwp5065;
    private DbsField pnd_Txwp5065_Pnd_Debug_Ind;
    private DbsField pnd_Txwp5065_Pnd_Abend_Ind;
    private DbsField pnd_Txwp5065_Pnd_Display_Ind;
    private DbsField pnd_Txwp5065_Pnd_Ret_Msg;
    private DbsField pnd_Cnt_Tot_Recs_Rpt;
    private DbsField pnd_Hold_Isn;
    private DbsField pnd_Hold_Override_Date;
    private DbsField pnd_Read_Key;

    private DbsGroup pnd_Read_Key__R_Field_1;
    private DbsField pnd_Read_Key_Pnd_Read_Key_Tin;
    private DbsField pnd_Read_Key_Pnd_Read_Key_Tin_Type;
    private DbsField pnd_Read_Key_Pnd_Read_Key_Invrse_Ts;
    private DbsField pnd_P_Tin;
    private DbsField pnd_Rc;
    private DbsField pnd_I2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwratin = new PdaTwratin(localVariables);
        pdaTxwapart = new PdaTxwapart(localVariables);
        ldaTxwl2425 = new LdaTxwl2425();
        registerRecord(ldaTxwl2425);
        registerRecord(ldaTxwl2425.getVw_form_Receipt_View());

        // Local Variables

        vw_part_View2 = new DataAccessProgramView(new NameInfo("vw_part_View2", "PART-VIEW2"), "TWR_FORM_RECEIPT", "TWR_FORM_RECEIPT");
        part_View2_Tin = vw_part_View2.getRecord().newFieldInGroup("part_View2_Tin", "TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIN");
        part_View2_Tin.setDdmHeader("TAX/ID/NUMBER");
        part_View2_Tin_Type = vw_part_View2.getRecord().newFieldInGroup("part_View2_Tin_Type", "TIN-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIN_TYPE");
        part_View2_Tin_Type.setDdmHeader("TIN/TYPE");
        part_View2_Active_Ind = vw_part_View2.getRecord().newFieldInGroup("part_View2_Active_Ind", "ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ACTIVE_IND");
        part_View2_Active_Ind.setDdmHeader("ACT/IND");
        part_View2_Citizen_Cde = vw_part_View2.getRecord().newFieldInGroup("part_View2_Citizen_Cde", "CITIZEN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CITIZEN_CDE");
        part_View2_Citizen_Cde.setDdmHeader("CITI-/ZEN-/SHIP");
        part_View2_Create_User = vw_part_View2.getRecord().newFieldInGroup("part_View2_Create_User", "CREATE-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CREATE_USER");
        part_View2_Create_User.setDdmHeader("CREATE/USER/ID");
        part_View2_Lu_User = vw_part_View2.getRecord().newFieldInGroup("part_View2_Lu_User", "LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LU_USER");
        part_View2_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        part_View2_Lu_Ts = vw_part_View2.getRecord().newFieldInGroup("part_View2_Lu_Ts", "LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "LU_TS");
        part_View2_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        part_View2_Create_Ts = vw_part_View2.getRecord().newFieldInGroup("part_View2_Create_Ts", "CREATE-TS", FieldType.TIME, RepeatingFieldStrategy.None, 
            "CREATE_TS");
        part_View2_Create_Ts.setDdmHeader("CREATE/TIME/STAMP");
        part_View2_Invrse_Create_Ts = vw_part_View2.getRecord().newFieldInGroup("part_View2_Invrse_Create_Ts", "INVRSE-CREATE-TS", FieldType.PACKED_DECIMAL, 
            13, RepeatingFieldStrategy.None, "INVRSE_CREATE_TS");
        part_View2_Invrse_Create_Ts.setDdmHeader("INVERSE/CREATE/TIMESTAMP");
        part_View2_W8_Ben_Country = vw_part_View2.getRecord().newFieldInGroup("part_View2_W8_Ben_Country", "W8-BEN-COUNTRY", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "W8_BEN_COUNTRY");
        part_View2_W8_Ben_Country.setDdmHeader("W8-BEN/COUNTRY");
        part_View2_W8_Ben_Override = vw_part_View2.getRecord().newFieldInGroup("part_View2_W8_Ben_Override", "W8-BEN-OVERRIDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "W8_BEN_OVERRIDE");
        part_View2_W8_Ben_Override.setDdmHeader("W8-BEN/OVER-/RIDE");
        registerRecord(vw_part_View2);

        pnd_Txwp5065 = localVariables.newGroupInRecord("pnd_Txwp5065", "#TXWP5065");
        pnd_Txwp5065_Pnd_Debug_Ind = pnd_Txwp5065.newFieldInGroup("pnd_Txwp5065_Pnd_Debug_Ind", "#DEBUG-IND", FieldType.BOOLEAN, 1);
        pnd_Txwp5065_Pnd_Abend_Ind = pnd_Txwp5065.newFieldInGroup("pnd_Txwp5065_Pnd_Abend_Ind", "#ABEND-IND", FieldType.BOOLEAN, 1);
        pnd_Txwp5065_Pnd_Display_Ind = pnd_Txwp5065.newFieldInGroup("pnd_Txwp5065_Pnd_Display_Ind", "#DISPLAY-IND", FieldType.BOOLEAN, 1);
        pnd_Txwp5065_Pnd_Ret_Msg = pnd_Txwp5065.newFieldInGroup("pnd_Txwp5065_Pnd_Ret_Msg", "#RET-MSG", FieldType.STRING, 70);
        pnd_Cnt_Tot_Recs_Rpt = localVariables.newFieldInRecord("pnd_Cnt_Tot_Recs_Rpt", "#CNT-TOT-RECS-RPT", FieldType.PACKED_DECIMAL, 7);
        pnd_Hold_Isn = localVariables.newFieldInRecord("pnd_Hold_Isn", "#HOLD-ISN", FieldType.PACKED_DECIMAL, 12);
        pnd_Hold_Override_Date = localVariables.newFieldInRecord("pnd_Hold_Override_Date", "#HOLD-OVERRIDE-DATE", FieldType.TIME);
        pnd_Read_Key = localVariables.newFieldInRecord("pnd_Read_Key", "#READ-KEY", FieldType.STRING, 18);

        pnd_Read_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Read_Key__R_Field_1", "REDEFINE", pnd_Read_Key);
        pnd_Read_Key_Pnd_Read_Key_Tin = pnd_Read_Key__R_Field_1.newFieldInGroup("pnd_Read_Key_Pnd_Read_Key_Tin", "#READ-KEY-TIN", FieldType.STRING, 10);
        pnd_Read_Key_Pnd_Read_Key_Tin_Type = pnd_Read_Key__R_Field_1.newFieldInGroup("pnd_Read_Key_Pnd_Read_Key_Tin_Type", "#READ-KEY-TIN-TYPE", FieldType.STRING, 
            1);
        pnd_Read_Key_Pnd_Read_Key_Invrse_Ts = pnd_Read_Key__R_Field_1.newFieldInGroup("pnd_Read_Key_Pnd_Read_Key_Invrse_Ts", "#READ-KEY-INVRSE-TS", FieldType.PACKED_DECIMAL, 
            13);
        pnd_P_Tin = localVariables.newFieldInRecord("pnd_P_Tin", "#P-TIN", FieldType.STRING, 11);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_part_View2.reset();

        ldaTxwl2425.initializeValues();

        localVariables.reset();
        pnd_Txwp5065_Pnd_Debug_Ind.setInitialValue(true);
        pnd_Txwp5065_Pnd_Abend_Ind.setInitialValue(false);
        pnd_Txwp5065_Pnd_Display_Ind.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Txwp5065() throws Exception
    {
        super("Txwp5065", true);
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* ********************************************************************
        //*  M A I N   P R O G R A M
        //* ********************************************************************
        setKeys(ControlKeys.PF5, "%W<80", (String)null);                                                                                                                  //Natural: FORMAT ( 1 ) LS = 132 PS = 40;//Natural: SET KEY PF5 = '%W<80' PF6 = '%W>80'
        setKeys(ControlKeys.PF6, "%W>80", (String)null);
        pnd_Read_Key_Pnd_Read_Key_Invrse_Ts.setValue(0);                                                                                                                  //Natural: ASSIGN #READ-KEY-INVRSE-TS := 0
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *TIMX ( EM = MM/DD/YYYY�HH:IIAP ) 117T 'Page  :' *PAGE-NUMBER ( 1 ) ( EM = Z,ZZ9 ) / *PROGRAM 46T 'W8 BEN OVERRIDE UPDATE HISTORY' 117T 'Report:  RPT1' /// 65T '================W8 DATA==================' 107T '=RESIDENCY=' 119T '=LEGAL OVR=' / 12T 'TIN' 54T 'OVERRIDE' 65T 'OVER' 70T 'PERM' 98T 'SIG' 107T 'RES RES LOC' 119T 'RES RES LOC' / 5T 'TIN' 12T 'TYPE' 18T 'NAME / ADDRESS' 56T 'DATE' 65T 'RIDE' 70T 'RES' 75T 'TIN' 80T 'TRTY' 85T 'CTRY' 90T 'SIG' 98T 'DATE' 107T 'CDE TYP CDE' 119T 'CDE TYP CDE' / 1T '-' ( 130 )
        ldaTxwl2425.getVw_form_Receipt_View().startDatabaseRead                                                                                                           //Natural: READ FORM-RECEIPT-VIEW BY SUPER-4
        (
        "RD1",
        new Oc[] { new Oc("SUPER_4", "ASC") }
        );
        RD1:
        while (condition(ldaTxwl2425.getVw_form_Receipt_View().readNextRow("RD1")))
        {
            if (condition(ldaTxwl2425.getForm_Receipt_View_W8_Ben_Override().equals("Y")))                                                                                //Natural: IF FORM-RECEIPT-VIEW.W8-BEN-OVERRIDE = 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM READ-BACKWARD
                sub_Read_Backward();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,NEWLINE,new ColumnSpacing(2),"TOTAL RECORDS ON REPORT     :",pnd_Cnt_Tot_Recs_Rpt,NEWLINE);                                         //Natural: WRITE ( 1 ) // 2X 'TOTAL RECORDS ON REPORT     :' #CNT-TOT-RECS-RPT /
        if (Global.isEscape()) return;
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-BACKWARD
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SHOW-DETAIL
        //* **********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-CONDITION
        //*  **************************
        //*  DISPLAY (1)
        //*  '/TIN'        PART-VIEW2.TIN
        //*   '/TIN'        TWRATIN.#O-TIN
        //*   'TIN/Type'    PART-VIEW2.TIN-TYPE
        //*   'Name/Addr'   #TXWAPART.#PART-NAME
        //*  'Act/Ind'     PART-VIEW2.ACTIVE-IND
        //*  'Ctz/Cde'     PART-VIEW2.CITIZEN-CDE
        //*  'Inv/TS'      PART-VIEW2.INVRSE-CREATE-TS
        //*  'W8/Cntry'    PART-VIEW2.W8-BEN-COUNTRY
        //*  'W8/Ovrrd'    PART-VIEW2.W8-BEN-OVERRIDE
        //*  'Create/User' PART-VIEW2.CREATE-USER
        //*  'Upd/User'    PART-VIEW2.LU-USER
        //*   'Create/Date' PART-VIEW2.CREATE-TS (EM=MM'/'DD'/'YYYY)
        //*  'Upd/TS'      PART-VIEW2.LU-TS
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Read_Backward() throws Exception                                                                                                                     //Natural: READ-BACKWARD
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pnd_Hold_Isn.reset();                                                                                                                                             //Natural: RESET #HOLD-ISN #HOLD-OVERRIDE-DATE
        pnd_Hold_Override_Date.reset();
        pnd_Read_Key_Pnd_Read_Key_Tin.setValue(ldaTxwl2425.getForm_Receipt_View_Tin());                                                                                   //Natural: ASSIGN #READ-KEY-TIN := FORM-RECEIPT-VIEW.TIN
        pnd_Read_Key_Pnd_Read_Key_Tin_Type.setValue(ldaTxwl2425.getForm_Receipt_View_Tin_Type());                                                                         //Natural: ASSIGN #READ-KEY-TIN-TYPE := FORM-RECEIPT-VIEW.TIN-TYPE
        vw_part_View2.startDatabaseRead                                                                                                                                   //Natural: READ PART-VIEW2 BY SUPER-2 STARTING FROM #READ-KEY
        (
        "RD2",
        new Wc[] { new Wc("SUPER_2", ">=", pnd_Read_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("SUPER_2", "ASC") }
        );
        RD2:
        while (condition(vw_part_View2.readNextRow("RD2")))
        {
            if (condition(part_View2_Tin.notEquals(pnd_Read_Key_Pnd_Read_Key_Tin) || part_View2_Tin_Type.notEquals(pnd_Read_Key_Pnd_Read_Key_Tin_Type)))                  //Natural: IF PART-VIEW2.TIN NE #READ-KEY-TIN OR PART-VIEW2.TIN-TYPE NE #READ-KEY-TIN-TYPE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(part_View2_W8_Ben_Override.equals("Y")))                                                                                                        //Natural: IF PART-VIEW2.W8-BEN-OVERRIDE = 'Y'
            {
                pnd_Hold_Override_Date.setValue(part_View2_Create_Ts);                                                                                                    //Natural: ASSIGN #HOLD-OVERRIDE-DATE := PART-VIEW2.CREATE-TS
                pnd_Hold_Isn.setValue(vw_part_View2.getAstISN("RD2"));                                                                                                    //Natural: ASSIGN #HOLD-ISN := *ISN ( RD2. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM SHOW-DETAIL
        sub_Show_Detail();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Show_Detail() throws Exception                                                                                                                       //Natural: SHOW-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        if (condition(pnd_Hold_Isn.equals(getZero())))                                                                                                                    //Natural: IF #HOLD-ISN = 0
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cnt_Tot_Recs_Rpt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #CNT-TOT-RECS-RPT
        GT1:                                                                                                                                                              //Natural: GET PART-VIEW2 #HOLD-ISN
        vw_part_View2.readByID(pnd_Hold_Isn.getLong(), "GT1");
        pdaTwratin.getTwratin_Pnd_I_Tin().setValue(part_View2_Tin);                                                                                                       //Natural: ASSIGN TWRATIN.#I-TIN := PART-VIEW2.TIN
        pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(part_View2_Tin_Type);                                                                                             //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := PART-VIEW2.TIN-TYPE
        DbsUtil.callnat(Twrntin.class , getCurrentProcessState(), pdaTwratin.getTwratin());                                                                               //Natural: CALLNAT 'TWRNTIN' USING TWRATIN
        if (condition(Global.isEscape())) return;
        pdaTxwapart.getPnd_Txwapart_Pnd_Func_Type().setValue("2");                                                                                                        //Natural: ASSIGN #TXWAPART.#FUNC-TYPE := '2'
        pdaTxwapart.getPnd_Txwapart_Pnd_Tin().setValue(part_View2_Tin);                                                                                                   //Natural: ASSIGN #TXWAPART.#TIN := PART-VIEW2.TIN
        pdaTxwapart.getPnd_Txwapart_Pnd_Tin_Type().setValue(part_View2_Tin_Type);                                                                                         //Natural: ASSIGN #TXWAPART.#TIN-TYPE := PART-VIEW2.TIN-TYPE
        DbsUtil.callnat(Txwnparm.class , getCurrentProcessState(), pdaTxwapart.getPnd_Txwapart());                                                                        //Natural: CALLNAT 'TXWNPARM' USING #TXWAPART
        if (condition(Global.isEscape())) return;
        getReports().write(1, new TabSetting(1),pdaTwratin.getTwratin_Pnd_O_Tin(),new TabSetting(13),part_View2_Tin_Type,new TabSetting(17),pdaTxwapart.getPnd_Txwapart_Pnd_Part_Name(),new  //Natural: WRITE ( 1 ) 01T TWRATIN.#O-TIN 13T PART-VIEW2.TIN-TYPE 17T #TXWAPART.#PART-NAME 53T PART-VIEW2.CREATE-TS ( EM = MM'/'DD'/'YYYY ) 66T #TXWAPART.W8-BEN-OVERRIDE 71T #TXWAPART.W8-BEN-PERM-RES 76T #TXWAPART.W8-BEN-TIN 81T #TXWAPART.W8-BEN-TREATY 86T #TXWAPART.W8-BEN-COUNTRY 91T #TXWAPART.W8-BEN-SIGNATURE 96T #TXWAPART.W8-BEN-SIGNED-DATE 107T #TXWAPART.#RES-CDE-O 112T #TXWAPART.#RES-TYPE 115T #TXWAPART.#LOC-CDE-O 119T #TXWAPART.LGO-RES-CDE 124T #TXWAPART.LGO-RES-TYPE 127T #TXWAPART.LGO-LOC-CDE
            TabSetting(53),part_View2_Create_Ts, new ReportEditMask ("MM'/'DD'/'YYYY"),new TabSetting(66),pdaTxwapart.getPnd_Txwapart_W8_Ben_Override(),new 
            TabSetting(71),pdaTxwapart.getPnd_Txwapart_W8_Ben_Perm_Res(),new TabSetting(76),pdaTxwapart.getPnd_Txwapart_W8_Ben_Tin(),new TabSetting(81),pdaTxwapart.getPnd_Txwapart_W8_Ben_Treaty(),new 
            TabSetting(86),pdaTxwapart.getPnd_Txwapart_W8_Ben_Country(),new TabSetting(91),pdaTxwapart.getPnd_Txwapart_W8_Ben_Signature(),new TabSetting(96),pdaTxwapart.getPnd_Txwapart_W8_Ben_Signed_Date(),new 
            TabSetting(107),pdaTxwapart.getPnd_Txwapart_Pnd_Res_Cde_O(),new TabSetting(112),pdaTxwapart.getPnd_Txwapart_Pnd_Res_Type(),new TabSetting(115),pdaTxwapart.getPnd_Txwapart_Pnd_Loc_Cde_O(),new 
            TabSetting(119),pdaTxwapart.getPnd_Txwapart_Lgo_Res_Cde(),new TabSetting(124),pdaTxwapart.getPnd_Txwapart_Lgo_Res_Type(),new TabSetting(127),
            pdaTxwapart.getPnd_Txwapart_Lgo_Loc_Cde());
        if (Global.isEscape()) return;
        short decideConditionsMet413 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #TXWAPART.#ADDR-LINE1 NE ' '
        if (condition(pdaTxwapart.getPnd_Txwapart_Pnd_Addr_Line1().notEquals(" ")))
        {
            decideConditionsMet413++;
            getReports().write(1, new TabSetting(17),pdaTxwapart.getPnd_Txwapart_Pnd_Addr_Line1());                                                                       //Natural: WRITE ( 1 ) 17T #TXWAPART.#ADDR-LINE1
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #TXWAPART.#ADDR-LINE2 NE ' '
        if (condition(pdaTxwapart.getPnd_Txwapart_Pnd_Addr_Line2().notEquals(" ")))
        {
            decideConditionsMet413++;
            getReports().write(1, new TabSetting(17),pdaTxwapart.getPnd_Txwapart_Pnd_Addr_Line2());                                                                       //Natural: WRITE ( 1 ) 17T #TXWAPART.#ADDR-LINE2
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #TXWAPART.#ADDR-LINE3 NE ' '
        if (condition(pdaTxwapart.getPnd_Txwapart_Pnd_Addr_Line3().notEquals(" ")))
        {
            decideConditionsMet413++;
            getReports().write(1, new TabSetting(17),pdaTxwapart.getPnd_Txwapart_Pnd_Addr_Line3());                                                                       //Natural: WRITE ( 1 ) 17T #TXWAPART.#ADDR-LINE3
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #TXWAPART.#ADDR-LINE4 NE ' '
        if (condition(pdaTxwapart.getPnd_Txwapart_Pnd_Addr_Line4().notEquals(" ")))
        {
            decideConditionsMet413++;
            getReports().write(1, new TabSetting(17),pdaTxwapart.getPnd_Txwapart_Pnd_Addr_Line4());                                                                       //Natural: WRITE ( 1 ) 17T #TXWAPART.#ADDR-LINE4
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #TXWAPART.#ADDR-LINE5 NE ' '
        if (condition(pdaTxwapart.getPnd_Txwapart_Pnd_Addr_Line5().notEquals(" ")))
        {
            decideConditionsMet413++;
            getReports().write(1, new TabSetting(17),pdaTxwapart.getPnd_Txwapart_Pnd_Addr_Line5());                                                                       //Natural: WRITE ( 1 ) 17T #TXWAPART.#ADDR-LINE5
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #TXWAPART.#ADDR-LINE6 NE ' '
        if (condition(pdaTxwapart.getPnd_Txwapart_Pnd_Addr_Line6().notEquals(" ")))
        {
            decideConditionsMet413++;
            getReports().write(1, new TabSetting(17),pdaTxwapart.getPnd_Txwapart_Pnd_Addr_Line6());                                                                       //Natural: WRITE ( 1 ) 17T #TXWAPART.#ADDR-LINE6
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet413 == 0))
        {
            getReports().write(1, new TabSetting(17),"(ADDRESS NOT FOUND)");                                                                                              //Natural: WRITE ( 1 ) 17T '(ADDRESS NOT FOUND)'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
        getReports().write(1, " ");                                                                                                                                       //Natural: WRITE ( 1 ) ' '
        if (Global.isEscape()) return;
    }
    private void sub_Error_Condition() throws Exception                                                                                                                   //Natural: ERROR-CONDITION
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************
        if (condition(pnd_Txwp5065_Pnd_Abend_Ind.getBoolean() || pnd_Txwp5065_Pnd_Display_Ind.getBoolean()))                                                              //Natural: IF #TXWP5065.#ABEND-IND OR #TXWP5065.#DISPLAY-IND
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(1, "***",new TabSetting(25),pnd_Txwp5065_Pnd_Ret_Msg, new AlphanumericLength (45),new TabSetting(77),"***",NEWLINE,"***",new               //Natural: WRITE ( 1 ) '***' 25T #TXWP5065.#RET-MSG ( AL = 45 ) 77T '***' / '***' 25T 'TIN:         ' FORM-RECEIPT-VIEW.TIN 77T '***' / '***' 25T 'TIN Type:    ' FORM-RECEIPT-VIEW.TIN-TYPE 77T '***' / '***' 25T 'PIN:         ' FORM-RECEIPT-VIEW.PIN 77T '***' / '***' 25T 'Citizen Code:' FORM-RECEIPT-VIEW.CITIZEN-CDE 77T '***' / '***' 25T 'Res Code:    ' FORM-RECEIPT-VIEW.RES-CDE 77T '***' / '***' 25T 'Res Type:    ' FORM-RECEIPT-VIEW.RES-TYPE 77T '***' / '***' 25T 'Loc Code:    ' FORM-RECEIPT-VIEW.LOC-CDE 77T '***'
                TabSetting(25),"TIN:         ",ldaTxwl2425.getForm_Receipt_View_Tin(),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"TIN Type:    ",ldaTxwl2425.getForm_Receipt_View_Tin_Type(),new 
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"PIN:         ",ldaTxwl2425.getForm_Receipt_View_Pin(),new TabSetting(77),"***",NEWLINE,"***",new 
                TabSetting(25),"Citizen Code:",ldaTxwl2425.getForm_Receipt_View_Citizen_Cde(),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Res Code:    ",ldaTxwl2425.getForm_Receipt_View_Res_Cde(),new 
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Res Type:    ",ldaTxwl2425.getForm_Receipt_View_Res_Type(),new TabSetting(77),"***",NEWLINE,"***",new 
                TabSetting(25),"Loc Code:    ",ldaTxwl2425.getForm_Receipt_View_Loc_Cde(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Txwp5065_Pnd_Abend_Ind.getBoolean()))                                                                                                           //Natural: IF #TXWP5065.#ABEND-IND
        {
            DbsUtil.terminate(100);  if (true) return;                                                                                                                    //Natural: TERMINATE 100
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 0 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 0 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 0 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=40");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:IIAP"),new TabSetting(117),"Page  :",getReports().getPageNumberDbs(1), 
            new ReportEditMask ("Z,ZZ9"),NEWLINE,Global.getPROGRAM(),new TabSetting(46),"W8 BEN OVERRIDE UPDATE HISTORY",new TabSetting(117),"Report:  RPT1",NEWLINE,NEWLINE,NEWLINE,new 
            TabSetting(65),"================W8 DATA==================",new TabSetting(107),"=RESIDENCY=",new TabSetting(119),"=LEGAL OVR=",NEWLINE,new TabSetting(12),"TIN",new 
            TabSetting(54),"OVERRIDE",new TabSetting(65),"OVER",new TabSetting(70),"PERM",new TabSetting(98),"SIG",new TabSetting(107),"RES RES LOC",new 
            TabSetting(119),"RES RES LOC",NEWLINE,new TabSetting(5),"TIN",new TabSetting(12),"TYPE",new TabSetting(18),"NAME / ADDRESS",new TabSetting(56),"DATE",new 
            TabSetting(65),"RIDE",new TabSetting(70),"RES",new TabSetting(75),"TIN",new TabSetting(80),"TRTY",new TabSetting(85),"CTRY",new TabSetting(90),"SIG",new 
            TabSetting(98),"DATE",new TabSetting(107),"CDE TYP CDE",new TabSetting(119),"CDE TYP CDE",NEWLINE,new TabSetting(1),"-",new RepeatItem(130));
    }
}
