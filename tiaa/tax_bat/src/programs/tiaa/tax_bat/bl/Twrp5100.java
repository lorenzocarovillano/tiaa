/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:40:31 PM
**        * FROM NATURAL PROGRAM : Twrp5100
************************************************************
**        * FILE NAME            : Twrp5100.java
**        * CLASS NAME           : Twrp5100
**        * INSTANCE NAME        : Twrp5100
************************************************************
************************************************************************
** PROGRAM : TWRP5100
** SYSTEM  : TAXWARS
** AUTHOR  : MICHAEL SUPONITSKY
** FUNCTION: LINK TIN PROCESS.
** HISTORY.....:
**    11/07/2001 - J.ROTHOLZ - STRIPPED DOWN TO PERFORM DRIVERFUNCTION,
**                             FETCHING TWRP5101 FOR TAX YEAR <2001 AND
**                             FETCHING TWRP5102 FOR TAX YEAR >= 2001
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5100 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Input_Parms;
    private DbsField pnd_Ws_Pnd_Form;
    private DbsField pnd_Ws_Pnd_Tax_Year;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Input_Parms = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Ws_Pnd_Form = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Form", "#FORM", FieldType.STRING, 6);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5100() throws Exception
    {
        super("Twrp5100");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp5100|Main");
        while(true)
        {
            try
            {
                //*   INPUT PARMS
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_Input_Parms);                                                                               //Natural: INPUT #WS.#INPUT-PARMS
                if (condition(pnd_Ws_Pnd_Tax_Year.equals(getZero())))                                                                                                     //Natural: IF #WS.#TAX-YEAR = 0
                {
                    pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), Global.getDATN().divide(10000).subtract(1));                           //Natural: ASSIGN #WS.#TAX-YEAR := *DATN / 10000 - 1
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet27 = 0;                                                                                                                          //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #TAX-YEAR = 1999 OR = 2000
                if (condition(pnd_Ws_Pnd_Tax_Year.equals(1999) || pnd_Ws_Pnd_Tax_Year.equals(2000)))
                {
                    decideConditionsMet27++;
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Ws_Pnd_Form, pnd_Ws_Pnd_Tax_Year);                                                                    //Natural: FETCH 'TWRP5101' #FORM #TAX-YEAR
                    Global.setFetchProgram(DbsUtil.getBlType("TWRP5101"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: WHEN #TAX-YEAR >= 2001
                else if (condition(pnd_Ws_Pnd_Tax_Year.greaterOrEqual(2001)))
                {
                    decideConditionsMet27++;
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Ws_Pnd_Form, pnd_Ws_Pnd_Tax_Year);                                                                    //Natural: FETCH 'TWRP5102' #FORM #TAX-YEAR
                    Global.setFetchProgram(DbsUtil.getBlType("TWRP5102"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //
}
