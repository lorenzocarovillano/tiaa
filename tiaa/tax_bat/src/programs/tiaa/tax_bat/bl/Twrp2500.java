/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:33:59 PM
**        * FROM NATURAL PROGRAM : Twrp2500
************************************************************
**        * FILE NAME            : Twrp2500.java
**        * CLASS NAME           : Twrp2500
**        * INSTANCE NAME        : Twrp2500
************************************************************
************************************************************************
** PROGRAM    : TWRP2500
** SYSTEM     : TAXWARS
** AUTHOR     : ROXAN CARREON
** FUNCTION   : ELECTRONIC FILING NUMBER UPDATE
** HISTORY.....:
**
** 06/29/2016 DEBASISH - GOING FORWARD THE FILING NUMBER WILL ONLY BE
**                       UPDATED FOR NEWLY ISSUED & CORRECTED FORMS,
**                       WHICH HAS BEEN FILED FOR THAT PERIOD OF TIME
**                       WITH PUERTO RICO AS PART OF ORIGINAL/CORRECTION
**                       REPORTING. TAG - DUTTAD.
** 7/18/2017 RUPOLEENA - AS PER BUSINESS REQUEST WE NEED TO REPORT
**                       RE-ISSUED FORMS AS WELL. TAG - MUKH
** 9/8/2017  KAUSHIK   - CHANGED THE CODE TO RUN FOR ORIGINAL AND
**                       CORRECTED CASES. TAG- KAUSHIK
** 11/27/2018 VIKRAM   - TO CHANGE THE DATA FORMAT OF ELECTRONIC FILING
**                       NUMBER FROM (A07) TO (A11).  TAG - VIKRAM2
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp2500 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_form;
    private DbsField form_Tirf_Tax_Year;
    private DbsField form_Tirf_Active_Ind;
    private DbsField form_Tirf_Form_Type;
    private DbsField form_Tirf_Tin;
    private DbsField form_Tirf_Contract_Nbr;
    private DbsField form_Tirf_Payee_Cde;
    private DbsField form_Tirf_Form_Issue_Type;
    private DbsField form_Tirf_Irs_Rpt_Ind;
    private DbsField form_Tirf_Irs_Rpt_Date;
    private DbsField form_Tirf_Future_Use;

    private DataAccessProgramView vw_form_U;
    private DbsField form_U_Tirf_Tax_Year;
    private DbsField form_U_Tirf_Active_Ind;
    private DbsField form_U_Tirf_Form_Type;
    private DbsField form_U_Tirf_Tin;
    private DbsField form_U_Tirf_Contract_Nbr;
    private DbsField form_U_Tirf_Payee_Cde;
    private DbsField form_U_Tirf_Future_Use;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Isn;
    private DbsField pnd_Ws_Pnd_Upd_Ctr;
    private DbsField pnd_Ws_Pnd_Updated_Ctr;

    private DbsGroup pnd_Ws_Pnd_Input_Parm;
    private DbsField pnd_Ws_Pnd_Control;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_Form_Type;
    private DbsField pnd_Ws_Pnd_Pr_Key;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Pnd_Key_Yyyy;

    private DbsGroup pnd_Ws__R_Field_2;
    private DbsField pnd_Ws_Pnd_Key_Yyyy_A;
    private DbsField pnd_Ws_Pnd_Irs_Rpt_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_form = new DataAccessProgramView(new NameInfo("vw_form", "FORM"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form_Tirf_Tax_Year = vw_form.getRecord().newFieldInGroup("form_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_TAX_YEAR");
        form_Tirf_Tax_Year.setDdmHeader("TAX/YEAR");
        form_Tirf_Active_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Active_Ind", "TIRF-ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_ACTIVE_IND");
        form_Tirf_Active_Ind.setDdmHeader("ACT/IND");
        form_Tirf_Form_Type = vw_form.getRecord().newFieldInGroup("form_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TIRF_FORM_TYPE");
        form_Tirf_Form_Type.setDdmHeader("FORM");
        form_Tirf_Tin = vw_form.getRecord().newFieldInGroup("form_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIRF_TIN");
        form_Tirf_Tin.setDdmHeader("TAX/ID/NUMBER");
        form_Tirf_Contract_Nbr = vw_form.getRecord().newFieldInGroup("form_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_CONTRACT_NBR");
        form_Tirf_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        form_Tirf_Payee_Cde = vw_form.getRecord().newFieldInGroup("form_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_PAYEE_CDE");
        form_Tirf_Payee_Cde.setDdmHeader("PAYEE/CODE");
        form_Tirf_Form_Issue_Type = vw_form.getRecord().newFieldInGroup("form_Tirf_Form_Issue_Type", "TIRF-FORM-ISSUE-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_FORM_ISSUE_TYPE");
        form_Tirf_Form_Issue_Type.setDdmHeader("FORM/ISSUE/TYPE");
        form_Tirf_Irs_Rpt_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Irs_Rpt_Ind", "TIRF-IRS-RPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_IND");
        form_Tirf_Irs_Rpt_Ind.setDdmHeader("IRS/RPT/IND");
        form_Tirf_Irs_Rpt_Date = vw_form.getRecord().newFieldInGroup("form_Tirf_Irs_Rpt_Date", "TIRF-IRS-RPT-DATE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_DATE");
        form_Tirf_Irs_Rpt_Date.setDdmHeader("IRS/RPT/DATE");
        form_Tirf_Future_Use = vw_form.getRecord().newFieldInGroup("form_Tirf_Future_Use", "TIRF-FUTURE-USE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "TIRF_FUTURE_USE");
        registerRecord(vw_form);

        vw_form_U = new DataAccessProgramView(new NameInfo("vw_form_U", "FORM-U"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form_U_Tirf_Tax_Year = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_TAX_YEAR");
        form_U_Tirf_Tax_Year.setDdmHeader("TAX/YEAR");
        form_U_Tirf_Active_Ind = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Active_Ind", "TIRF-ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_ACTIVE_IND");
        form_U_Tirf_Active_Ind.setDdmHeader("ACT/IND");
        form_U_Tirf_Form_Type = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TIRF_FORM_TYPE");
        form_U_Tirf_Form_Type.setDdmHeader("FORM");
        form_U_Tirf_Tin = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIRF_TIN");
        form_U_Tirf_Tin.setDdmHeader("TAX/ID/NUMBER");
        form_U_Tirf_Contract_Nbr = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_CONTRACT_NBR");
        form_U_Tirf_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        form_U_Tirf_Payee_Cde = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_PAYEE_CDE");
        form_U_Tirf_Payee_Cde.setDdmHeader("PAYEE/CODE");
        form_U_Tirf_Future_Use = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Future_Use", "TIRF-FUTURE-USE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "TIRF_FUTURE_USE");
        registerRecord(vw_form_U);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Isn = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Ws_Pnd_Upd_Ctr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Upd_Ctr", "#UPD-CTR", FieldType.NUMERIC, 3);
        pnd_Ws_Pnd_Updated_Ctr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Updated_Ctr", "#UPDATED-CTR", FieldType.NUMERIC, 7);

        pnd_Ws_Pnd_Input_Parm = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input_Parm", "#INPUT-PARM");
        pnd_Ws_Pnd_Control = pnd_Ws_Pnd_Input_Parm.newFieldInGroup("pnd_Ws_Pnd_Control", "#CONTROL", FieldType.STRING, 11);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws_Pnd_Input_Parm.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Form_Type = pnd_Ws_Pnd_Input_Parm.newFieldInGroup("pnd_Ws_Pnd_Form_Type", "#FORM-TYPE", FieldType.STRING, 3);
        pnd_Ws_Pnd_Pr_Key = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pr_Key", "#PR-KEY", FieldType.STRING, 7);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Pr_Key);
        pnd_Ws_Pnd_Key_Yyyy = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Key_Yyyy", "#KEY-YYYY", FieldType.NUMERIC, 4);

        pnd_Ws__R_Field_2 = pnd_Ws__R_Field_1.newGroupInGroup("pnd_Ws__R_Field_2", "REDEFINE", pnd_Ws_Pnd_Key_Yyyy);
        pnd_Ws_Pnd_Key_Yyyy_A = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_Key_Yyyy_A", "#KEY-YYYY-A", FieldType.STRING, 4);
        pnd_Ws_Pnd_Irs_Rpt_Dte = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Irs_Rpt_Dte", "#IRS-RPT-DTE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_form.reset();
        vw_form_U.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp2500() throws Exception
    {
        super("Twrp2500");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp2500|Main");
        setupReports();
        while(true)
        {
            try
            {
                if (Global.isEscape()) return;                                                                                                                            //Natural: FORMAT ( 1 ) PS = 20 LS = 80;//Natural: WRITE ( 1 ) TITLE LEFT *TIMX ( EM = MM/DD/YYYY�HH:IIAP ) 32T 'TIAA CREF TAXWARS' 67T 'PAGE  :' *PAGE-NUMBER ( 1 ) ( EM = Z,ZZ9 ) / *PROGRAM 27T 'ELECTRONIC FILING NUMBER UPDATE' 67T 'REPORT:  RPT1'
                //*   //
                //* ***************
                //*  MAIN PROGRAM *
                //* ***************
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_Input_Parm);                                                                                //Natural: INPUT #INPUT-PARM
                if (condition(pnd_Ws_Pnd_Tax_Year.equals(getZero())))                                                                                                     //Natural: IF #TAX-YEAR = 0
                {
                    pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), Global.getDATN().divide(10000).subtract(1));                           //Natural: ASSIGN #TAX-YEAR := *DATN / 10000 - 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_Tax_Year.setValue(pnd_Ws_Pnd_Tax_Year);                                                                                                    //Natural: ASSIGN #TAX-YEAR := #TAX-YEAR
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_Key_Yyyy.setValue(pnd_Ws_Pnd_Tax_Year);                                                                                                        //Natural: ASSIGN #KEY-YYYY := #TAX-YEAR
                setValueToSubstring("A",pnd_Ws_Pnd_Pr_Key,5,1);                                                                                                           //Natural: MOVE 'A' TO SUBSTR ( #PR-KEY,5,1 )
                setValueToSubstring("05",pnd_Ws_Pnd_Pr_Key,6,2);                                                                                                          //Natural: MOVE '05' TO SUBSTR ( #PR-KEY,6,2 )
                getReports().write(0, "=",pnd_Ws_Pnd_Pr_Key);                                                                                                             //Natural: WRITE '=' #PR-KEY
                if (Global.isEscape()) return;
                vw_form.startDatabaseRead                                                                                                                                 //Natural: READ FORM BY TIRF-SUPERDE-9 = #PR-KEY
                (
                "READ01",
                new Wc[] { new Wc("TIRF_SUPERDE_9", ">=", pnd_Ws_Pnd_Pr_Key, WcType.BY) },
                new Oc[] { new Oc("TIRF_SUPERDE_9", "ASC") }
                );
                READ01:
                while (condition(vw_form.readNextRow("READ01")))
                {
                    if (condition(form_Tirf_Form_Type.greater(5) || form_Tirf_Tax_Year.greater(pnd_Ws_Pnd_Key_Yyyy_A)))                                                   //Natural: IF FORM.TIRF-FORM-TYPE > 05 OR FORM.TIRF-TAX-YEAR > #KEY-YYYY-A
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    //*                                                           /* << DUTTAD
                    pnd_Ws_Pnd_Irs_Rpt_Dte.setValueEdited(form_Tirf_Irs_Rpt_Date,new ReportEditMask("YYYYMMDD"));                                                         //Natural: MOVE EDITED FORM.TIRF-IRS-RPT-DATE ( EM = YYYYMMDD ) TO #IRS-RPT-DTE
                    //*  2014 ONWARDS
                    if (condition(form_Tirf_Tax_Year.greaterOrEqual("2014")))                                                                                             //Natural: IF FORM.TIRF-TAX-YEAR GE '2014'
                    {
                        //*    IF FORM.TIRF-FORM-ISSUE-TYPE EQ '1' OR EQ '3' /* ISSUED OR CORRECTED
                        //*  KAUSHIK
                        if (condition(pnd_Ws_Pnd_Form_Type.equals("ORG")))                                                                                                //Natural: IF #FORM-TYPE = 'ORG'
                        {
                            //*  KAUSHIK
                            if (condition(form_Tirf_Form_Issue_Type.equals("1") || form_Tirf_Form_Issue_Type.equals("2")))                                                //Natural: IF FORM.TIRF-FORM-ISSUE-TYPE EQ '1' OR EQ '2'
                            {
                                //*  ISSUED OR RE-ISSUED OR CORRECTED   MUKH
                                //*  REPORTED
                                if (condition(pnd_Ws_Pnd_Irs_Rpt_Dte.notEquals(" ")))                                                                                     //Natural: IF #IRS-RPT-DTE NE ' '
                                {
                                    //*  FOR CURRENT PERIOD
                                    if (condition(form_Tirf_Future_Use.equals(" ")))                                                                                      //Natural: IF FORM.TIRF-FUTURE-USE EQ ' '
                                    {
                                        //*                                                           /* >> DUTTAD
                                        pnd_Ws_Pnd_Isn.setValue(vw_form.getAstISN("Read01"));                                                                             //Natural: MOVE *ISN TO #ISN
                                        GET:                                                                                                                              //Natural: GET FORM-U #ISN
                                        vw_form_U.readByID(pnd_Ws_Pnd_Isn.getLong(), "GET");
                                        //* *        MOVE #CONTROL TO SUBSTR(FORM-U.TIRF-FUTURE-USE,1,7)
                                        //*  VIKRAM2
                                        setValueToSubstring(pnd_Ws_Pnd_Control,form_U_Tirf_Future_Use,1,11);                                                              //Natural: MOVE #CONTROL TO SUBSTR ( FORM-U.TIRF-FUTURE-USE,1,11 )
                                        pnd_Ws_Pnd_Upd_Ctr.nadd(1);                                                                                                       //Natural: ADD 1 TO #UPD-CTR
                                        getReports().display(1, "ISN",                                                                                                    //Natural: DISPLAY ( 1 ) 'ISN' *ISN 'TIN' FORM-U.TIRF-TIN 'PPCN' FORM-U.TIRF-CONTRACT-NBR 'PAYEE' FORM-U.TIRF-PAYEE-CDE 'YEAR' FORM-U.TIRF-TAX-YEAR 'FORM' FORM-U.TIRF-FORM-TYPE 'CONTROL' FORM-U.TIRF-FUTURE-USE
                                        		Global.getAstISN(),"TIN",
                                        		form_U_Tirf_Tin,"PPCN",
                                        		form_U_Tirf_Contract_Nbr,"PAYEE",
                                        		form_U_Tirf_Payee_Cde,"YEAR",
                                        		form_U_Tirf_Tax_Year,"FORM",
                                        		form_U_Tirf_Form_Type,"CONTROL",
                                        		form_U_Tirf_Future_Use);
                                        if (condition(Global.isEscape()))
                                        {
                                            if (condition(Global.isEscapeBottom())) break;
                                            else if (condition(Global.isEscapeBottomImmediate())) break;
                                            else if (condition(Global.isEscapeTop())) continue;
                                            else if (condition(Global.isEscapeRoutine())) return;
                                            else break;
                                        }
                                        vw_form_U.updateDBRow("GET");                                                                                                     //Natural: UPDATE ( GET. )
                                        if (condition(pnd_Ws_Pnd_Upd_Ctr.equals(100)))                                                                                    //Natural: IF #UPD-CTR = 100
                                        {
                                            pnd_Ws_Pnd_Upd_Ctr.reset();                                                                                                   //Natural: RESET #UPD-CTR
                                            getCurrentProcessState().getDbConv().dbCommit();                                                                              //Natural: END TRANSACTION
                                        }                                                                                                                                 //Natural: END-IF
                                        pnd_Ws_Pnd_Updated_Ctr.nadd(1);                                                                                                   //Natural: ADD 1 TO #UPDATED-CTR
                                        //*  << DUTTAD
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                            //* **                                               /* KAUSHIK CODE START
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_Ws_Pnd_Form_Type.equals("COR")))                                                                                            //Natural: IF #FORM-TYPE = 'COR'
                            {
                                if (condition(form_Tirf_Form_Issue_Type.equals("3")))                                                                                     //Natural: IF FORM.TIRF-FORM-ISSUE-TYPE EQ '3'
                                {
                                    //*  REPORTED
                                    if (condition(pnd_Ws_Pnd_Irs_Rpt_Dte.notEquals(" ")))                                                                                 //Natural: IF #IRS-RPT-DTE NE ' '
                                    {
                                        //*  FOR CURRENT
                                        if (condition(form_Tirf_Future_Use.equals(" ")))                                                                                  //Natural: IF FORM.TIRF-FUTURE-USE EQ ' '
                                        {
                                            //*                                                           /* >
                                            pnd_Ws_Pnd_Isn.setValue(vw_form.getAstISN("Read01"));                                                                         //Natural: MOVE *ISN TO #ISN
                                            GET1:                                                                                                                         //Natural: GET FORM-U #ISN
                                            vw_form_U.readByID(pnd_Ws_Pnd_Isn.getLong(), "GET1");
                                            //* *        MOVE #CONTROL TO SUBSTR(FORM-U.TIRF-FUTURE-USE,1,7)
                                            //*  VIKRAM2
                                            setValueToSubstring(pnd_Ws_Pnd_Control,form_U_Tirf_Future_Use,1,11);                                                          //Natural: MOVE #CONTROL TO SUBSTR ( FORM-U.TIRF-FUTURE-USE,1,11 )
                                            pnd_Ws_Pnd_Upd_Ctr.nadd(1);                                                                                                   //Natural: ADD 1 TO #UPD-CTR
                                            getReports().display(2, "ISN",                                                                                                //Natural: DISPLAY ( 2 ) 'ISN' *ISN 'TIN' FORM-U.TIRF-TIN 'PPCN' FORM-U.TIRF-CONTRACT-NBR 'PAYEE' FORM-U.TIRF-PAYEE-CDE 'YEAR' FORM-U.TIRF-TAX-YEAR 'FORM' FORM-U.TIRF-FORM-TYPE 'CONTROL' FORM-U.TIRF-FUTURE-USE
                                            		Global.getAstISN(),"TIN",
                                            		form_U_Tirf_Tin,"PPCN",
                                            		form_U_Tirf_Contract_Nbr,"PAYEE",
                                            		form_U_Tirf_Payee_Cde,"YEAR",
                                            		form_U_Tirf_Tax_Year,"FORM",
                                            		form_U_Tirf_Form_Type,"CONTROL",
                                            		form_U_Tirf_Future_Use);
                                            if (condition(Global.isEscape()))
                                            {
                                                if (condition(Global.isEscapeBottom())) break;
                                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                                else if (condition(Global.isEscapeTop())) continue;
                                                else if (condition(Global.isEscapeRoutine())) return;
                                                else break;
                                            }
                                            vw_form_U.updateDBRow("GET1");                                                                                                //Natural: UPDATE ( GET1. )
                                            if (condition(pnd_Ws_Pnd_Upd_Ctr.equals(100)))                                                                                //Natural: IF #UPD-CTR = 100
                                            {
                                                pnd_Ws_Pnd_Upd_Ctr.reset();                                                                                               //Natural: RESET #UPD-CTR
                                                getCurrentProcessState().getDbConv().dbCommit();                                                                          //Natural: END TRANSACTION
                                            }                                                                                                                             //Natural: END-IF
                                            pnd_Ws_Pnd_Updated_Ctr.nadd(1);                                                                                               //Natural: ADD 1 TO #UPDATED-CTR
                                        }                                                                                                                                 //Natural: END-IF
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-IF
                                //* ***                                              /* KAUSHIK CODE END
                            }                                                                                                                                             //Natural: END-IF
                            //*  >> DUTTAD
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,"Total Number of Updated Forms >> ",pnd_Ws_Pnd_Updated_Ctr);                                        //Natural: WRITE ( 1 ) //// 'Total Number of Updated Forms >> ' #UPDATED-CTR
                if (Global.isEscape()) return;
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=20 LS=80");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:IIAP"),new TabSetting(32),"TIAA CREF TAXWARS",new 
            TabSetting(67),"PAGE  :",getReports().getPageNumberDbs(1), new ReportEditMask ("Z,ZZ9"),NEWLINE,Global.getPROGRAM(),new TabSetting(27),"ELECTRONIC FILING NUMBER UPDATE",new 
            TabSetting(67),"REPORT:  RPT1");

        getReports().setDisplayColumns(1, "ISN",
        		Global.getAstISN(),"TIN",
        		form_U_Tirf_Tin,"PPCN",
        		form_U_Tirf_Contract_Nbr,"PAYEE",
        		form_U_Tirf_Payee_Cde,"YEAR",
        		form_U_Tirf_Tax_Year,"FORM",
        		form_U_Tirf_Form_Type,"CONTROL",
        		form_U_Tirf_Future_Use);
        getReports().setDisplayColumns(2, "ISN",
        		Global.getAstISN(),"TIN",
        		form_U_Tirf_Tin,"PPCN",
        		form_U_Tirf_Contract_Nbr,"PAYEE",
        		form_U_Tirf_Payee_Cde,"YEAR",
        		form_U_Tirf_Tax_Year,"FORM",
        		form_U_Tirf_Form_Type,"CONTROL",
        		form_U_Tirf_Future_Use);
    }
}
