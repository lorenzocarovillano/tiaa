/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:33:00 PM
**        * FROM NATURAL PROGRAM : Twrp116a
************************************************************
**        * FILE NAME            : Twrp116a.java
**        * CLASS NAME           : Twrp116a
**        * INSTANCE NAME        : Twrp116a
************************************************************
************************************************************************
************************* T W R P 1 1 6 A  *****************************
************************************************************************
*************************  FORM SELECTION  *****************************
************************************************************************
**                                                                    **
** PROGRAM NAME:  TWRP116A                                            **
** SYSTEM      :  TAX WITHHOLDING & REPORTING SYSTEM                  **
** AUTHOR      :  A.WILNER                                            **
** PURPOSE     :  MAGNATIC REPORT TO CANADA (CONTROL REPORT)          **
**                                                                    **
**                                                                    **
** HISTORY.....:                                                      **
** 08/13/02       J.ROTHOLZ - RECOMPILED DUE TO INCREASE IN #COMP-NAME
**                LENGTH IN TWRLCOMP & TWRACOMP
**                                                                    **
** 09/04/02       M.NACHBER - CORRECTED HEADER ADDING COMPANY NAME AND**
**                TAX YEAR                                            **
** 02/10/05       A. YOUNG  - REVISED TO REPLACE TWR?COMP WITH        **
**                            TWR?COM2 WHEREVER APPLICABLE.           **
**                          - POPULATE #TWRACOM2.#TAX-YEAR BEFORE     **
**                            CALLING TWRNCOM2.                       **
** 12/28/05       R MA      - ADDED AN EXTRA REPORT TO SEND TO CANADA **
**                            REVENUE. SERVED AS A TEMPORARY SOLUTION **
**                            UNTIL THE ELECTRONIC FILE TRANSFER COULD**
**                            BE SET UP IN THE SYSTEM.                **
**                            DISCOVERED PAYER-ID FIELD WAS MIXED UP  **
**                            WITH NON-RESIDENT-ACCT, MADE CORRECTION **
**                            IN THIS PROGRAM AND TWRP0116, ALSO LDA  **
**                            TWRL116B.                               **
** 03/08/07       R MA      - UPDATED FOR 2006 SPEC CHANGES.          **
**                            XML FILE WILL BE SENT INSTEAD OF REPORT.**
**
** 4/21/2017 - WEBBJ - PIN EXPANSION. RESTOW ONLY
** 10/13/2020 - RE-STOW COMPONENT FOR 5498           /* SECURE-ACT
** 12/04/2020 - RE-STOW COMPONENT FOR 5498 IRS REPORTING  2020
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp116a extends BLNatBase
{
    // Data Areas
    private LdaTwrl116a ldaTwrl116a;
    private LdaTwrl116b ldaTwrl116b;
    private LdaTwrl116c ldaTwrl116c;
    private PdaTwracom2 pdaTwracom2;
    private LdaTwrl9710 ldaTwrl9710;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_Area;
    private DbsField pnd_Work_Area_Pnd_Company_Gross;
    private DbsField pnd_Work_Area_Pnd_Tot_Read;
    private DbsField pnd_Work_Area_Pnd_Tot_Gross;
    private DbsField pnd_Work_Area_Pnd_Company_Tax;
    private DbsField pnd_Work_Area_Pnd_Tot_Tax;
    private DbsField pnd_Work_Area_Pnd_Ws_Read_Counter;
    private DbsField pnd_Work_Area_Pnd_Ws_Read_Counter1;
    private DbsField pnd_Work_Area_Pnd_Ws_Company;
    private DbsField pnd_Work_Area_Pnd_Ws_Filler;
    private DbsField pnd_Work_Area_Pnd_Ws_Trailer;
    private DbsField pnd_Work_Area_Pnd_Full_Name;
    private DbsField pnd_Work_Area_Pnd_Addr_2;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone;

    private DbsGroup pnd_Work_Area__R_Field_1;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone_F1;
    private DbsField pnd_Work_Area_Pnd_Ws_Area_Code;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone_F2;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone_Num3;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone_F3;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone_Num4;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone_Numbera;

    private DbsGroup pnd_Work_Area__R_Field_2;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone_Number;
    private DbsField pnd_Work_Area_Pnd_In_Parm;

    private DbsGroup pnd_Work_Area__R_Field_3;
    private DbsField pnd_Work_Area_Pnd_Parm_Company;
    private DbsField pnd_Work_Area_Pnd_Parm_Year;
    private DbsField pnd_Work_Area_Pnd_Company_Line;
    private DbsField pnd_Work_Area_Pnd_Tax_Year;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl116a = new LdaTwrl116a();
        registerRecord(ldaTwrl116a);
        ldaTwrl116b = new LdaTwrl116b();
        registerRecord(ldaTwrl116b);
        ldaTwrl116c = new LdaTwrl116c();
        registerRecord(ldaTwrl116c);
        localVariables = new DbsRecord();
        pdaTwracom2 = new PdaTwracom2(localVariables);
        ldaTwrl9710 = new LdaTwrl9710();
        registerRecord(ldaTwrl9710);
        registerRecord(ldaTwrl9710.getVw_form());

        // Local Variables

        pnd_Work_Area = localVariables.newGroupInRecord("pnd_Work_Area", "#WORK-AREA");
        pnd_Work_Area_Pnd_Company_Gross = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Company_Gross", "#COMPANY-GROSS", FieldType.NUMERIC, 15, 2);
        pnd_Work_Area_Pnd_Tot_Read = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Read", "#TOT-READ", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Tot_Gross = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Gross", "#TOT-GROSS", FieldType.NUMERIC, 15, 2);
        pnd_Work_Area_Pnd_Company_Tax = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Company_Tax", "#COMPANY-TAX", FieldType.NUMERIC, 15, 2);
        pnd_Work_Area_Pnd_Tot_Tax = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Tax", "#TOT-TAX", FieldType.NUMERIC, 15, 2);
        pnd_Work_Area_Pnd_Ws_Read_Counter = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Read_Counter", "#WS-READ-COUNTER", FieldType.NUMERIC, 
            6);
        pnd_Work_Area_Pnd_Ws_Read_Counter1 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Read_Counter1", "#WS-READ-COUNTER1", FieldType.NUMERIC, 
            6);
        pnd_Work_Area_Pnd_Ws_Company = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Company", "#WS-COMPANY", FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Ws_Filler = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Filler", "#WS-FILLER", FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Ws_Trailer = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Trailer", "#WS-TRAILER", FieldType.STRING, 25);
        pnd_Work_Area_Pnd_Full_Name = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Full_Name", "#FULL-NAME", FieldType.STRING, 40);
        pnd_Work_Area_Pnd_Addr_2 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Addr_2", "#ADDR-2", FieldType.STRING, 40);
        pnd_Work_Area_Pnd_Ws_Phone = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone", "#WS-PHONE", FieldType.STRING, 14);

        pnd_Work_Area__R_Field_1 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area__R_Field_1", "REDEFINE", pnd_Work_Area_Pnd_Ws_Phone);
        pnd_Work_Area_Pnd_Ws_Phone_F1 = pnd_Work_Area__R_Field_1.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone_F1", "#WS-PHONE-F1", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Ws_Area_Code = pnd_Work_Area__R_Field_1.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Area_Code", "#WS-AREA-CODE", FieldType.NUMERIC, 
            3);
        pnd_Work_Area_Pnd_Ws_Phone_F2 = pnd_Work_Area__R_Field_1.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone_F2", "#WS-PHONE-F2", FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Ws_Phone_Num3 = pnd_Work_Area__R_Field_1.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone_Num3", "#WS-PHONE-NUM3", FieldType.STRING, 
            3);
        pnd_Work_Area_Pnd_Ws_Phone_F3 = pnd_Work_Area__R_Field_1.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone_F3", "#WS-PHONE-F3", FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Ws_Phone_Num4 = pnd_Work_Area__R_Field_1.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone_Num4", "#WS-PHONE-NUM4", FieldType.STRING, 
            4);
        pnd_Work_Area_Pnd_Ws_Phone_Numbera = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone_Numbera", "#WS-PHONE-NUMBERA", FieldType.STRING, 
            7);

        pnd_Work_Area__R_Field_2 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area__R_Field_2", "REDEFINE", pnd_Work_Area_Pnd_Ws_Phone_Numbera);
        pnd_Work_Area_Pnd_Ws_Phone_Number = pnd_Work_Area__R_Field_2.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone_Number", "#WS-PHONE-NUMBER", FieldType.NUMERIC, 
            7);
        pnd_Work_Area_Pnd_In_Parm = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_In_Parm", "#IN-PARM", FieldType.STRING, 5);

        pnd_Work_Area__R_Field_3 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area__R_Field_3", "REDEFINE", pnd_Work_Area_Pnd_In_Parm);
        pnd_Work_Area_Pnd_Parm_Company = pnd_Work_Area__R_Field_3.newFieldInGroup("pnd_Work_Area_Pnd_Parm_Company", "#PARM-COMPANY", FieldType.STRING, 
            1);
        pnd_Work_Area_Pnd_Parm_Year = pnd_Work_Area__R_Field_3.newFieldInGroup("pnd_Work_Area_Pnd_Parm_Year", "#PARM-YEAR", FieldType.STRING, 4);
        pnd_Work_Area_Pnd_Company_Line = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Company_Line", "#COMPANY-LINE", FieldType.STRING, 72);
        pnd_Work_Area_Pnd_Tax_Year = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl116a.initializeValues();
        ldaTwrl116b.initializeValues();
        ldaTwrl116c.initializeValues();
        ldaTwrl9710.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp116a() throws Exception
    {
        super("Twrp116a");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp116a|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        while(true)
        {
            try
            {
                //*  MAIN PROGRAM
                //*  12/28/05  RM                                                                                                                                         //Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: FORMAT ( 2 ) PS = 60 LS = 132
                //*                                                                                                                                                       //Natural: FORMAT ( 3 ) PS = 60 LS = 132
                //*   09/04/02  MARINA NACHER
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Work_Area_Pnd_In_Parm);                                                                            //Natural: INPUT #IN-PARM
                //*  TAX YEAR IS ALWAYS
                if (condition(pnd_Work_Area_Pnd_Parm_Year.equals(" ")))                                                                                                   //Natural: IF #PARM-YEAR = ' '
                {
                    pnd_Work_Area_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Work_Area_Pnd_Tax_Year), Global.getDATN().divide(10000).subtract(1));             //Natural: ASSIGN #WORK-AREA.#TAX-YEAR := *DATN / 10000 - 1
                    //*  CALENDAR YEAR MINUS 1
                    //*  02-10-2005
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Work_Area_Pnd_Tax_Year.setValue(pnd_Work_Area_Pnd_Parm_Year);                                                                                     //Natural: ASSIGN #WORK-AREA.#TAX-YEAR := #PARM-YEAR
                }                                                                                                                                                         //Natural: END-IF
                //*  PERFORM PROCESS-COMPANY
                //*   09/04/02 END
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 1 RECORD #TWRL116A
                while (condition(getWorkFiles().read(1, ldaTwrl116a.getPnd_Twrl116a())))
                {
                    getWorkFiles().write(2, false, ldaTwrl116a.getPnd_Twrl116a());                                                                                        //Natural: WRITE WORK FILE 2 #TWRL116A
                    getReports().write(2, ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_1(),NEWLINE,ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_2(),NEWLINE,ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_3(), //Natural: WRITE ( 2 ) #TWRL116A-1 / #TWRL116A-2 / #TWRL116A-3 / #TWRL116A-4 '********** END OF REC **********'
                        NEWLINE,ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_4(),"********** END OF REC **********");
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  IF  #TWRL116A-TYPE-CODE  =  421                          /* 02-10-2005
                    //*  SKIPPED SUMMARY AND TRANSMITTER RECORDS                 03-08-07    RM
                    if (condition(ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Rec_Type().equals("S") || ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Rec_Type().equals("T")))    //Natural: IF #TWRL116A-REC-TYPE = 'S' OR = 'T'
                    {
                        //*     IF #WS-READ-COUNTER  NE  0
                        //*      PERFORM COMPANY-TOT                             /* 12/28/05    RM
                        //*     END-IF
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*  IF #TWRL116A-TYPE-CODE  =  916                           /* 02-10-2005
                    //*    PERFORM REPORT-TOT
                    //*    ESCAPE TOP
                    //*  END-IF
                    pnd_Work_Area_Pnd_Ws_Read_Counter.nadd(1);                                                                                                            //Natural: ADD 1 TO #WS-READ-COUNTER
                    //*  12/28/05 RM
                    //*  12/28/05 RM
                    getReports().display(1, "SSN",                                                                                                                        //Natural: DISPLAY ( 1 ) 'SSN' #TWRL116A-FOREIGN-SSN 'N A M E ' #TWRL116A-FIRST-NAME / ' ' #TWRL116A-INITIAL / ' ' #TWRL116A-SURNAME 'A D D R E S S' #TWRL116A-ADDRESS-1 / ' ' #TWRL116A-CITY / ' ' #TWRL116A-PROVINCE / ' ' #TWRL116A-ZIP 'ACCT' #TWRL116A-NON-RESIDENT-ACCT 'PAYER-ID' #TWRL116A-PAYER-ID / 'R-TYPE' #TWRL116A-RECIPIENT-TYPE / 'COUNTRY' #TWRL116A-TAX-COUNTRY-CODE / 'INC-CODE' #TWRL116A-INCOME-CODE 'CURR' #TWRL116A-CURRENCY-CODE 'GROSS' #TWRL116A-GROSS-INCOME / 'WITHHELD' #TWRL116A-NON-RES-TAX-HELD
                    		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Foreign_Ssn(),"N A M E ",
                    		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_First_Name(),NEWLINE," ",
                    		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Initial(),NEWLINE," ",
                    		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Surname(),"A D D R E S S",
                    		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Address_1(),NEWLINE," ",
                    		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_City(),NEWLINE," ",
                    		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Province(),NEWLINE," ",
                    		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Zip(),"ACCT",
                    		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Non_Resident_Acct(),"PAYER-ID",
                    		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Payer_Id(),NEWLINE,"R-TYPE",
                    		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Recipient_Type(),NEWLINE,"COUNTRY",
                    		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Tax_Country_Code(),NEWLINE,"INC-CODE",
                    		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Income_Code(),"CURR",
                    		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Currency_Code(),"GROSS",
                    		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Gross_Income(),NEWLINE,"WITHHELD",
                    		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Non_Res_Tax_Held());
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Work_Area_Pnd_Company_Gross.nadd(ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Gross_Income());                                                        //Natural: ADD #TWRL116A-GROSS-INCOME TO #COMPANY-GROSS
                    pnd_Work_Area_Pnd_Company_Tax.nadd(ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Non_Res_Tax_Held());                                                      //Natural: ADD #TWRL116A-NON-RES-TAX-HELD TO #COMPANY-TAX
                    pnd_Work_Area_Pnd_Full_Name.setValue(DbsUtil.compress(ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_First_Name(), ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Initial(),  //Natural: COMPRESS #TWRL116A-FIRST-NAME #TWRL116A-INITIAL #TWRL116A-SURNAME INTO #FULL-NAME
                        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Surname()));
                    pnd_Work_Area_Pnd_Addr_2.setValue(DbsUtil.compress(ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_City(), ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Province(),  //Natural: COMPRESS #TWRL116A-CITY #TWRL116A-PROVINCE #TWRL116A-ZIP INTO #ADDR-2
                        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Zip()));
                    getReports().display(3, new ReportEmptyLineSuppression(true),"/Soc Sec Num",                                                                          //Natural: DISPLAY ( 3 ) ( ES = ON ) '/Soc Sec Num' #TWRL116A-FOREIGN-SSN ( HC = L ) '/Contract Num' #TWRL116A-NON-RESIDENT-ACCT ( HC = L ) '/Name' #FULL-NAME ( HC = L ) / 'Address' #TWRL116A-ADDRESS-1 ( HC = L ) / '/' #ADDR-2 ( HC = L ) '/Gross Amount' #TWRL116A-GROSS-INCOME ( HC = R EM = -ZZZ,ZZZ,ZZ9.99 ) 3X 'Tax  /Withheld' #TWRL116A-NON-RES-TAX-HELD ( HC = R EM = -Z,ZZZ,ZZ9.99 )
                    		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Foreign_Ssn(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/Contract Num",
                        
                    		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Non_Resident_Acct(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),
                        "/Name",
                    		pnd_Work_Area_Pnd_Full_Name, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
                    		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Address_1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,
                        "/",
                    		pnd_Work_Area_Pnd_Addr_2, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/Gross Amount",
                    		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Gross_Income(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new 
                        ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),"Tax  /Withheld",
                    		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Non_Res_Tax_Held(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), 
                        new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM COMPANY-TOT
                sub_Company_Tot();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM REPORT-TOT
                sub_Report_Tot();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR," ",NEWLINE,"=",new RepeatItem(131),NEWLINE,new TabSetting(44),"END OF REPORT 1",NEWLINE,"=",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR ' ' / '=' ( 131 ) / 44T 'END OF REPORT 1' / '=' ( 131 )
                    RepeatItem(131));
                if (Global.isEscape()) return;
                getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR," ",NEWLINE,"=",new RepeatItem(131),NEWLINE,new TabSetting(44),"END OF REPORT 3",NEWLINE,"=",new  //Natural: WRITE ( 3 ) NOTITLE NOHDR ' ' / '=' ( 131 ) / 44T 'END OF REPORT 3' / '=' ( 131 )
                    RepeatItem(131));
                if (Global.isEscape()) return;
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 1 )
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 2 )
                //*   09/04/02  MARINA NACHER
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 3 )
                //* ********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COMPANY
                //* ********************************
                //*   09/04/02  END
                //* ******************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPANY-TOT
                //* ******************************
                //* *****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-TOT
                //* *****************************
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    //*  02-10-2005
    //*  02-10-2005
    //*  02-10-2005
    private void sub_Process_Company() throws Exception                                                                                                                   //Natural: PROCESS-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue(pnd_Work_Area_Pnd_Parm_Company);                                                                             //Natural: ASSIGN #TWRACOM2.#COMP-CODE := #PARM-COMPANY
        pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year()), pnd_Work_Area_Pnd_Tax_Year.val());   //Natural: ASSIGN #TWRACOM2.#TAX-YEAR := VAL ( #WORK-AREA.#TAX-YEAR )
        pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(7);                                                                                                          //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 7
        //*  02-10-2005
        //*  02-10-2005
        DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOM2' USING #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        pnd_Work_Area_Pnd_Company_Line.setValue("Company:");                                                                                                              //Natural: ASSIGN #COMPANY-LINE := 'Company:'
        //*  AAY
        setValueToSubstring(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),pnd_Work_Area_Pnd_Company_Line,11,4);                                                       //Natural: MOVE #TWRACOM2.#COMP-SHORT-NAME TO SUBSTR ( #COMPANY-LINE,11,4 )
        setValueToSubstring("-",pnd_Work_Area_Pnd_Company_Line,16,1);                                                                                                     //Natural: MOVE '-' TO SUBSTR ( #COMPANY-LINE,16,1 )
        //*  AAY
        setValueToSubstring(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Name(),pnd_Work_Area_Pnd_Company_Line,18,55);                                                            //Natural: MOVE #TWRACOM2.#COMP-NAME TO SUBSTR ( #COMPANY-LINE,18,55 )
    }
    //*  12/28/05  RM
    private void sub_Company_Tot() throws Exception                                                                                                                       //Natural: COMPANY-TOT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"COMPANY TOTAL:",new ColumnSpacing(10),"RECORD COUNT:",new ColumnSpacing(3),pnd_Work_Area_Pnd_Ws_Read_Counter,new  //Natural: WRITE ( 1 ) // 'COMPANY TOTAL:' 10X 'RECORD COUNT:' 3X #WS-READ-COUNTER 64T #COMPANY-GROSS ( EM = ZZZZZZZ,ZZZ,ZZ9.99 ) #COMPANY-TAX ( EM = ZZZZZZZ,ZZZ,ZZ9.99 )
            TabSetting(64),pnd_Work_Area_Pnd_Company_Gross, new ReportEditMask ("ZZZZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Company_Tax, new ReportEditMask 
            ("ZZZZZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE," ** COMPANY TOTAL ** ",new ColumnSpacing(12),"RECORD COUNT:",new ColumnSpacing(3),pnd_Work_Area_Pnd_Ws_Read_Counter,new  //Natural: WRITE ( 3 ) // ' ** COMPANY TOTAL ** ' 12X 'RECORD COUNT:' 3X #WS-READ-COUNTER 76T #COMPANY-GROSS ( EM = ZZZZZZZ,ZZZ,ZZ9.99 ) #COMPANY-TAX ( EM = ZZZZZZZ,ZZZ,ZZ9.99 )
            TabSetting(76),pnd_Work_Area_Pnd_Company_Gross, new ReportEditMask ("ZZZZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Company_Tax, new ReportEditMask 
            ("ZZZZZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  IF #TWRL116A-TYPE-CODE  NE  9165                       /* 02-10-2005
        //*   NEWPAGE(1)
        //*  END-IF
        pnd_Work_Area_Pnd_Tot_Gross.nadd(pnd_Work_Area_Pnd_Company_Gross);                                                                                                //Natural: ADD #COMPANY-GROSS TO #TOT-GROSS
        pnd_Work_Area_Pnd_Tot_Tax.nadd(pnd_Work_Area_Pnd_Company_Tax);                                                                                                    //Natural: ADD #COMPANY-TAX TO #TOT-TAX
        pnd_Work_Area_Pnd_Tot_Read.nadd(pnd_Work_Area_Pnd_Ws_Read_Counter);                                                                                               //Natural: ADD #WS-READ-COUNTER TO #TOT-READ
        pnd_Work_Area_Pnd_Company_Gross.reset();                                                                                                                          //Natural: RESET #COMPANY-GROSS #COMPANY-TAX #WS-READ-COUNTER
        pnd_Work_Area_Pnd_Company_Tax.reset();
        pnd_Work_Area_Pnd_Ws_Read_Counter.reset();
    }
    //*  12/28/05  RM
    private void sub_Report_Tot() throws Exception                                                                                                                        //Natural: REPORT-TOT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,"REPORT  TOTAL:",new ColumnSpacing(10),"RECORD COUNT:",pnd_Work_Area_Pnd_Tot_Read,new TabSetting(64),pnd_Work_Area_Pnd_Tot_Gross,  //Natural: WRITE ( 1 ) 'REPORT  TOTAL:' 10X 'RECORD COUNT:' #TOT-READ 64T #TOT-GROSS ( EM = ZZZZZZZ,ZZZ,ZZ9.99 ) #TOT-TAX ( EM = ZZZZZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZZZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Tot_Tax, new ReportEditMask ("ZZZZZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,"*** REPORT  TOTAL ***",new ColumnSpacing(12),"RECORD COUNT:",pnd_Work_Area_Pnd_Tot_Read,new TabSetting(76),pnd_Work_Area_Pnd_Tot_Gross,  //Natural: WRITE ( 3 ) '*** REPORT  TOTAL ***' 12X 'RECORD COUNT:' #TOT-READ 76T #TOT-GROSS ( EM = ZZZZZZZ,ZZZ,ZZ9.99 ) #TOT-TAX ( EM = ZZZZZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZZZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Tot_Tax, new ReportEditMask ("ZZZZZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*   09/04/02  MARINA NACHER
                    //*   WRITE (1) NOTITLE NOHDR
                    //*    'PGM:' *PROGRAM
                    //*    44T  'TIAA-CREF TAX WAR SYSTEM'
                    //*    110T  'DATE:' *DATU
                    //*  WRITE (1)
                    //*    44T  '   NR4 DETIAL REPORT'
                    //*    110T  'TIME:' *TIME
                    //*  WRITE (1)
                    //*    44T  '------------------------'
                    //*   //
                    //*  02-10-2005
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:IIAP"),new TabSetting(28),"TaxWaRS - Annual Original Tax Report - Details ",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR *TIMX ( EM = MM/DD/YYYY�HH:IIAP ) 28T 'TaxWaRS - Annual Original Tax Report - Details ' 92T 'Page:' *PAGE-NUMBER ( 1 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 36T #WORK-AREA.#TAX-YEAR '- Canada - Form NR4' / 25T #COMPANY-LINE //
                        TabSetting(92),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                        TabSetting(36),pnd_Work_Area_Pnd_Tax_Year,"- Canada - Form NR4",NEWLINE,new TabSetting(25),pnd_Work_Area_Pnd_Company_Line,NEWLINE,
                        NEWLINE);
                    //*   09/04/02  END
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, "....+....1....+....2....+....3....+....4....+....5....+....6....+....7","...+....8....+....9....+....100");                    //Natural: WRITE ( 2 ) '....+....1....+....2....+....3....+....4....+....5....+....6....+....7' '...+....8....+....9....+....100'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:IIAP"),new TabSetting(28),"          Annual Original Tax Report           ",new  //Natural: WRITE ( 3 ) NOTITLE NOHDR *TIMX ( EM = MM/DD/YYYY�HH:IIAP ) 28T '          Annual Original Tax Report           ' 92T 'Page:' *PAGE-NUMBER ( 3 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 39T #WORK-AREA.#TAX-YEAR '- Canada - Form NR4' / 25T #COMPANY-LINE //
                        TabSetting(92),"Page:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                        TabSetting(39),pnd_Work_Area_Pnd_Tax_Year,"- Canada - Form NR4",NEWLINE,new TabSetting(25),pnd_Work_Area_Pnd_Company_Line,NEWLINE,
                        NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132");
        Global.format(2, "PS=60 LS=132");
        Global.format(3, "PS=60 LS=132");

        getReports().setDisplayColumns(1, "SSN",
        		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Foreign_Ssn(),"N A M E ",
        		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_First_Name(),NEWLINE," ",
        		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Initial(),NEWLINE," ",
        		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Surname(),"A D D R E S S",
        		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Address_1(),NEWLINE," ",
        		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_City(),NEWLINE," ",
        		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Province(),NEWLINE," ",
        		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Zip(),"ACCT",
        		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Non_Resident_Acct(),"PAYER-ID",
        		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Payer_Id(),NEWLINE,"R-TYPE",
        		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Recipient_Type(),NEWLINE,"COUNTRY",
        		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Tax_Country_Code(),NEWLINE,"INC-CODE",
        		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Income_Code(),"CURR",
        		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Currency_Code(),"GROSS",
        		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Gross_Income(),NEWLINE,"WITHHELD",
        		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Non_Res_Tax_Held());
        getReports().setDisplayColumns(3, new ReportEmptyLineSuppression(true),"/Soc Sec Num",
        		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Foreign_Ssn(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/Contract Num",
        		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Non_Resident_Acct(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/Name",
        		pnd_Work_Area_Pnd_Full_Name, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Address_1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		pnd_Work_Area_Pnd_Addr_2, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/Gross Amount",
        		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Gross_Income(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask 
            ("-ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),"Tax  /Withheld",
        		ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Non_Res_Tax_Held(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask 
            ("-Z,ZZZ,ZZ9.99"));
    }
}
