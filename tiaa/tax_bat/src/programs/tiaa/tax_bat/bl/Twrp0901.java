/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:31:41 PM
**        * FROM NATURAL PROGRAM : Twrp0901
************************************************************
**        * FILE NAME            : Twrp0901.java
**        * CLASS NAME           : Twrp0901
**        * INSTANCE NAME        : Twrp0901
************************************************************
***********************************************************************
* PROGRAM : TWRP0901
* FUNCTION: CREATES FLAT FILE3 TO BE USED TO PRINT
*           YTD AND DAILY REPORTABLE TRANSACTIONS BY SYSTEM SOURCE PER
*           COMPANY (TWRP0905) AND BY STATE (TWRP0910)
*           AND TO UPDATE FLAT FILE1 FOR START-DATE, END-DATE AND
*                PROCESSING TYPE (YTD OR D)
* INPUTS  : FILE 94 (TWRPYMNT-PAYMENT-FILE)
*           FILE 98 (CONTROL FILE)
* OUTPUT  : CONTROL TOTAL AND FLAT FILE
* AUTHOR  : EDITH 9/23/98
* UPDATES : USE THE INTERFACE-DATE FIELD (PE) IN SELECTING RECORDS
*                 TO BE PROCESSED INSTEAD OF PAYMENT DATE IN PE
*           FOR FREQ = 'Y' ,USE 'END-DATE' GEN. BY TWRP0600 AS
*                             LATEST DATE  11/21/98
*           FOR FREQ = 'D' , USE FROM/TO DATES GEN. BY TWRP0600
*                       AS START/END DATES CORRESPONDINGLY
* 11/25   : A PARAMETER CARD WILL PROVIDE FOR THE LATEST DATE
* 12/9    : #PARAM-INTF-TO-DATE   - IS THE EXTRACT DATE(FROM CPS FILE)
*           #PARAM-INTF-FROM-DATE - IS THE LOAD DATE(TO PAYMENT FILE)
*
*            1) FOR NORMAL RUN / DAY TO DAY RUN
*                #PARAM-INTF-TO-DATE  = #PARAM-INTF-FROM-DATE
*            2) FOR MULTIPLE EXTRACTS BUT FOR THE SAME LOAD DATES :
*                #PARAM-INTF-TO-DATE IS NOT =  #PARAM-INTF-FROM-DATE
*
*         : ACCUM. FINANCIAL AMTS. OF @ SRCE.CODES WITH THE SAME
*               FROM-INTF-DATE (THIS IS DONE USING F98T5)
* 12/19   : TO MATCH SOURCE CODES 'AP' AND 'IA'
*         : ORIG.SRCE.CODE\UPD.SRCE.CODE = 'MLOL' FOR AN UPDATED 'ML'
*         : ORIG.SRCE.CODE\UPD.SRCE.CODE = '  TM' (FOUND FROM AP & IA)
* 12/24   : TO ACCEPT DATE RANGE (ON-REQUEST) FOR YTD PROCESSING
* 2/1     : WRITE RUN-TYPE TO FLAT-FILE TO BE USED BY TWRP0920
* 2/8     : WRITE IVC INDICATOR (THE ONE FROM PAYMENT LEVEL - PE )
*              TO FLAT-FILE3
*         : IF REGULAR YTD, USE IVC-IND IN SUMMARY LEVEL ELSE
*           FOR SPECIAL YTD (SAME AS IN DAILY RUN) ,
*             IF ALL PAYMENTS WITHIN THE DATE RATE ARE THE SAME,
*                 USE THE IVC-IND(PYMNT LEVEL)
*               ELSE IVC INDS ARE MIXED SO USE THE IVC-IND
*                    IN THE SUMMARY LEVEL.
* 2/23/99 : TO USE IVC-IND(PYMNT LEVEL) FOR ALL  RUN-TYPES
* 3/2/99  : TO PROCESS 'APTM' SOURCE CODE (AP MAINTENANCE)
*             NO MORE '__TM' SOURCE CODE (TAKE EFFECT 4/14/99)
* 4/2/99  : TO CHECK FINANCIAL AMOUNT OF COMMUNITY PROPERTY (CP)
*              FROM CONTROL FILE - FEEDER SYSTEM TABLE
* 10/14/03: RE-STOWED - CONTRACT TYPE & IRC-TO ADDED TO TWRL0900
* 06/06/07: ADDED NEW OUTPUT FILE FOR DATAWAREHOUSE INTERFACE
* 05/20/07: ADDED NEW FIELDS TO DATAWAREHOUSE INTERFACE FILE. (M001)
* 10/18/11: SUPPORTING NEW COMPANY CODE "F"; CHANGE CAN BE
*           REFERENCED AS DY1                                  (D.YHUN)
* 02/21/12: PASS PLAN,SUBPLAN,ORGNTG-SUBPLAN AND ORGNTNG-CONTRACT TO
*          #FLAT-FILE3 AND #TX-DW-DISTR
* 11/20/2014 O SOTTO FATCA CHANGES MARKED 11/2014.
***********************************************************************
* PARAMETER CARD :
************************************************************************
* --- RUN TYPE ----�------------ F O R M A T -------�----- EXAMPLE----
* DAILY            �D                               �D
*                  �EXTRACT DATE,INTERFACE/LOAD DATE�19990324,19990324
* -----------------�--------------------------------�-------------------
* DAILY W/ MULTIPLE�D                               �D
* EXTRACT DATES &  �EARLIEST EXTRACT DATE,INTERFACE �19990322,19990324
* SAME INTF.DATE   �                        DATE    �
* -----------------�--------------------------------�-------------------
* REGULAR YTD      �Y                               �Y
*                  �R,INTERFACE DATE,INTERFACE DATE �R,19990324,19990324
*                  �  (NOTE: THE 2 PARAM. DATES MUST�
*                  �  BE THE LATEST INTERFACE DATE &�
*                  �  MUST BE THE SAME DATES.)      �
* -----------------�--------------------------------�-------------------
* SPECIAL YTD      �Y                               �Y
*                  �S,INTERFACE DATE,INTERFACE DATE �S,19990101,19990324
*                  �  (NOTE: THE USER WILL DEFINE   �
*                  �    THE TWO DATES)              �
* ----------------------------------------------------------------------
* 6/19/99  : GET THE FROM/TO-DATES (FOR SPECIAL YTD) & INTERFACE-DATE,
*                 FOR (REG. YTD & DAILY RUN) FROM CONTROL FILE (FLAT
*               FILE - OUTPUT OF TWRP0820) AND THE RUN TYPE WILL DEPEND
*               ON JOB NAME BEING USED.
* 6/25/99  : FREQ (Y/D) & RUN TYPE WILL BE PART OF THE PARAMETER (EDITH)
* 6/26/99  : TO WRITE IVC-PROTECT FIELD TO FLAT-FILE3  (EDITH)
* 7/22/99  : TO USE THE FF. CONTROL RECORD FIELDS (TWRL0600) AS :
*  (EDITH)     'TO-CCYYMMDD'         --> INTERFACE-TO-DATE
*              'INTERFACE-CCYYMMDD'  -->INTERFACE-FROM-DATE
* 8/10/99  : TO SET THE 'interface-to-date' OF  THE FF. SOURCE CODES
*  (EDITH)    TO ACCESS CONTROL FILE (TABLE5) FOR THEIR FINANCIAL AMTS.
*                 SOURCE CODE               DATE
*                 ------------   -----------------------------
*                  AP OR IA    (1ST DAY OF THE FOLLOWING 'load' MONTH -
*                                 SINCE 'AP/IA' ARE LOADED ON THE MONTH
*                                 PRIOR TO IT's extraction/payment date)
*                     CP       (1ST DAY OF THE CURRENT 'load' MONTH -
*                                 SINCE 'cp' IS BEING LOADED EVERY
*                                 3RD WORKING DAY OF THE MONTH)
* 10/8/99 : INCLUSION OF NEW COMPANY , TIAA-LIFE
*  (EDITH): UPDATED TWRL0901 - ADDED 1 ARRAY ELEMENT FOR THE NEW CO.
* 10/15/99: REDUCED SIZE OF FLAT-FILE1. CHECKING OF FINANCIAL AMOUNTS
*           OF SELECTED SOURCE CODES BY CO BY INTERFACE DATE WILL BE
*           DONE BY PROGRAM TWRP0905
* 11/10/99: INCLUSION OF THE FF. SOURCE CODES :
*  (EDITH)       'TMAL' - CREATED DUE TO DELETED 'APTM'
*                'TMIL' - CREATED DUE TO DELETED 'IATM'
* 11/18/99: UPDATED TO WRITE RESIDENCY TYPE TO FLAT-FILE3
* 04/02/02: UPDATED TO INCLUDE NEW COMPANY - TCII (JRR)
* 09/16/04: TOPS RELEASE 3 CHANGES
*  (RM)     ADD NEW COMPANY CODE 'X' FOR TRUST COMPANY AND NEW SOURCE
*           CODE 'OP', 'NL' AND 'PL'.
* 06/06/06: NAVISYS CHANGES   R. MA
*           SOURCE CODE 'NV' HAD BEEN ADDED TO ALL CONTROL REPORTS.
*           THIS PROGRAM IS ONE OF THE CONTROL REPORTS BUT DON't need
*           ANY CODE CHANGES THIS TIME.
* 10/30/06: VUL CHANGES   R. MA
*           SOURCE CODE 'VL' HAD BEEN ADDED TO ALL CONTROL REPORTS.
*           THIS PROGRAM IS ONE OF THE CONTROL REPORTS BUT DON't need
*           ANY CODE CHANGES THIS TIME.
*
***********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0901 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0900 ldaTwrl0900;
    private LdaTwrl0901 ldaTwrl0901;
    private LdaTwrl0902 ldaTwrl0902;
    private LdaTwrl0907 ldaTwrl0907;
    private LdaTwrl0600 ldaTwrl0600;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Param_Freq;
    private DbsField pnd_Run_Type;
    private DbsField pnd_Start_Date;

    private DbsGroup pnd_Start_Date__R_Field_1;
    private DbsField pnd_Start_Date_Pnd_Sd_Yy;
    private DbsField pnd_Start_Date_Pnd_Sd_Mm;
    private DbsField pnd_Start_Date_Pnd_Sd_Dd;
    private DbsField pnd_End_Date;
    private DbsField pnd_Date1;
    private DbsField pnd_Date2;
    private DbsField pnd_Param_Intf_From_Date;

    private DbsGroup pnd_Param_Intf_From_Date__R_Field_2;
    private DbsField pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Yy;
    private DbsField pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Mm;
    private DbsField pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Dd;
    private DbsField pnd_Param_Intf_To_Date;
    private DbsField pnd_Param_Sytd_From_Date;
    private DbsField pnd_Param_Sytd_To_Date;
    private DbsField pnd_I;
    private DbsField pnd_T;
    private DbsField pnd_S;
    private DbsField pnd_Rec_Process;
    private DbsField pnd_Rec_Ritten;
    private DbsField pnd_Rec_Read;
    private DbsField pnd_Cref_Cnt;
    private DbsField pnd_Life_Cnt;
    private DbsField pnd_Tiaa_Cnt;
    private DbsField pnd_Tcii_Cnt;
    private DbsField pnd_Fsbc_Cnt;
    private DbsField pnd_Trst_Cnt;
    private DbsField pnd_Othr_Cnt;
    private DbsField pnd_Cnt;
    private DbsField pnd_Tax_Year;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0900 = new LdaTwrl0900();
        registerRecord(ldaTwrl0900);
        ldaTwrl0901 = new LdaTwrl0901();
        registerRecord(ldaTwrl0901);
        ldaTwrl0902 = new LdaTwrl0902();
        registerRecord(ldaTwrl0902);
        ldaTwrl0907 = new LdaTwrl0907();
        registerRecord(ldaTwrl0907);
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Param_Freq = localVariables.newFieldInRecord("pnd_Param_Freq", "#PARAM-FREQ", FieldType.STRING, 1);
        pnd_Run_Type = localVariables.newFieldInRecord("pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 1);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);

        pnd_Start_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Start_Date__R_Field_1", "REDEFINE", pnd_Start_Date);
        pnd_Start_Date_Pnd_Sd_Yy = pnd_Start_Date__R_Field_1.newFieldInGroup("pnd_Start_Date_Pnd_Sd_Yy", "#SD-YY", FieldType.STRING, 4);
        pnd_Start_Date_Pnd_Sd_Mm = pnd_Start_Date__R_Field_1.newFieldInGroup("pnd_Start_Date_Pnd_Sd_Mm", "#SD-MM", FieldType.STRING, 2);
        pnd_Start_Date_Pnd_Sd_Dd = pnd_Start_Date__R_Field_1.newFieldInGroup("pnd_Start_Date_Pnd_Sd_Dd", "#SD-DD", FieldType.STRING, 2);
        pnd_End_Date = localVariables.newFieldInRecord("pnd_End_Date", "#END-DATE", FieldType.STRING, 8);
        pnd_Date1 = localVariables.newFieldInRecord("pnd_Date1", "#DATE1", FieldType.STRING, 8);
        pnd_Date2 = localVariables.newFieldInRecord("pnd_Date2", "#DATE2", FieldType.STRING, 8);
        pnd_Param_Intf_From_Date = localVariables.newFieldInRecord("pnd_Param_Intf_From_Date", "#PARAM-INTF-FROM-DATE", FieldType.STRING, 8);

        pnd_Param_Intf_From_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Param_Intf_From_Date__R_Field_2", "REDEFINE", pnd_Param_Intf_From_Date);
        pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Yy = pnd_Param_Intf_From_Date__R_Field_2.newFieldInGroup("pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Yy", 
            "#PARAM-INTF-FROM-DATE-YY", FieldType.NUMERIC, 4);
        pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Mm = pnd_Param_Intf_From_Date__R_Field_2.newFieldInGroup("pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Mm", 
            "#PARAM-INTF-FROM-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Dd = pnd_Param_Intf_From_Date__R_Field_2.newFieldInGroup("pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Dd", 
            "#PARAM-INTF-FROM-DATE-DD", FieldType.NUMERIC, 2);
        pnd_Param_Intf_To_Date = localVariables.newFieldInRecord("pnd_Param_Intf_To_Date", "#PARAM-INTF-TO-DATE", FieldType.STRING, 8);
        pnd_Param_Sytd_From_Date = localVariables.newFieldInRecord("pnd_Param_Sytd_From_Date", "#PARAM-SYTD-FROM-DATE", FieldType.STRING, 8);
        pnd_Param_Sytd_To_Date = localVariables.newFieldInRecord("pnd_Param_Sytd_To_Date", "#PARAM-SYTD-TO-DATE", FieldType.STRING, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_T = localVariables.newFieldInRecord("pnd_T", "#T", FieldType.NUMERIC, 2);
        pnd_S = localVariables.newFieldInRecord("pnd_S", "#S", FieldType.NUMERIC, 2);
        pnd_Rec_Process = localVariables.newFieldInRecord("pnd_Rec_Process", "#REC-PROCESS", FieldType.NUMERIC, 9);
        pnd_Rec_Ritten = localVariables.newFieldInRecord("pnd_Rec_Ritten", "#REC-RITTEN", FieldType.NUMERIC, 9);
        pnd_Rec_Read = localVariables.newFieldInRecord("pnd_Rec_Read", "#REC-READ", FieldType.NUMERIC, 9);
        pnd_Cref_Cnt = localVariables.newFieldArrayInRecord("pnd_Cref_Cnt", "#CREF-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 3));
        pnd_Life_Cnt = localVariables.newFieldArrayInRecord("pnd_Life_Cnt", "#LIFE-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 3));
        pnd_Tiaa_Cnt = localVariables.newFieldArrayInRecord("pnd_Tiaa_Cnt", "#TIAA-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 3));
        pnd_Tcii_Cnt = localVariables.newFieldArrayInRecord("pnd_Tcii_Cnt", "#TCII-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 3));
        pnd_Fsbc_Cnt = localVariables.newFieldArrayInRecord("pnd_Fsbc_Cnt", "#FSBC-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 3));
        pnd_Trst_Cnt = localVariables.newFieldArrayInRecord("pnd_Trst_Cnt", "#TRST-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 3));
        pnd_Othr_Cnt = localVariables.newFieldArrayInRecord("pnd_Othr_Cnt", "#OTHR-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 3));
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 1);
        pnd_Tax_Year = localVariables.newFieldInRecord("pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0900.initializeValues();
        ldaTwrl0901.initializeValues();
        ldaTwrl0902.initializeValues();
        ldaTwrl0907.initializeValues();
        ldaTwrl0600.initializeValues();

        localVariables.reset();
        pnd_T.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0901() throws Exception
    {
        super("Twrp0901");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp0901|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //* **
                //*                                                                                                                                                       //Natural: FORMAT PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                getWorkFiles().read(5, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                     //Natural: READ WORK FILE 5 ONCE #TWRP0600-CONTROL-RECORD
                if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                  //Natural: AT END OF FILE
                {
                    getReports().write(0, ReportOption.NOTITLE," ************************************ ",NEWLINE," ***                              *** ",                 //Natural: WRITE ' ************************************ ' / ' ***                              *** ' / ' ***    CONTROL RECORD IS EMPTY   *** ' / ' ***    PLEASE INFORM SYSTEMS !!  *** ' / ' ***                              *** ' / ' ************************************ '
                        NEWLINE," ***    CONTROL RECORD IS EMPTY   *** ",NEWLINE," ***    PLEASE INFORM SYSTEMS !!  *** ",NEWLINE," ***                              *** ",
                        NEWLINE," ************************************ ");
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(90);  if (true) return;                                                                                                             //Natural: TERMINATE 90
                }                                                                                                                                                         //Natural: END-ENDFILE
                pnd_Param_Intf_To_Date.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd());                                                   //Natural: ASSIGN #PARAM-INTF-TO-DATE := #TWRP0600-TO-CCYYMMDD
                pnd_Param_Intf_From_Date.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd());                                          //Natural: ASSIGN #PARAM-INTF-FROM-DATE := #TWRP0600-INTERFACE-CCYYMMDD
                pnd_Param_Sytd_From_Date.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd());                                               //Natural: ASSIGN #PARAM-SYTD-FROM-DATE := #TWRP0600-FROM-CCYYMMDD
                pnd_Param_Sytd_To_Date.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd());                                                   //Natural: ASSIGN #PARAM-SYTD-TO-DATE := #TWRP0600-TO-CCYYMMDD
                pnd_Tax_Year.setValueEdited(new ReportEditMask("9999"),ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy());                          //Natural: MOVE EDITED #TWRP0600-TAX-YEAR-CCYY TO #TAX-YEAR ( EM = 9999 )
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Param_Freq);                                                                                       //Natural: INPUT #PARAM-FREQ
                short decideConditionsMet485 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #PARAM-FREQ;//Natural: VALUES 'Y'
                if (condition((pnd_Param_Freq.equals("Y")), INPUT_2))
                {
                    decideConditionsMet485++;
                    DbsUtil.invokeInput(setInputStatus(INPUT_2), this, pnd_Run_Type);                                                                                     //Natural: INPUT #RUN-TYPE
                    short decideConditionsMet488 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF #RUN-TYPE;//Natural: VALUE 'R'
                    if (condition((pnd_Run_Type.equals("R"))))
                    {
                        decideConditionsMet488++;
                        pnd_Start_Date_Pnd_Sd_Yy.compute(new ComputeParameters(false, pnd_Start_Date_Pnd_Sd_Yy), pnd_Tax_Year.subtract(1));                               //Natural: ASSIGN #SD-YY := #TAX-YEAR - 1
                        pnd_Start_Date_Pnd_Sd_Mm.setValue("12");                                                                                                          //Natural: ASSIGN #SD-MM := '12'
                        pnd_Start_Date_Pnd_Sd_Dd.setValue("01");                                                                                                          //Natural: ASSIGN #SD-DD := '01'
                        pnd_End_Date.setValue(pnd_Param_Intf_From_Date);                                                                                                  //Natural: ASSIGN #END-DATE := #PARAM-INTF-FROM-DATE
                                                                                                                                                                          //Natural: PERFORM WRITE-PARAMS-TO-FF1
                        sub_Write_Params_To_Ff1();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: VALUE 'S'
                    else if (condition((pnd_Run_Type.equals("S"))))
                    {
                        decideConditionsMet488++;
                        pnd_Start_Date.setValue(pnd_Param_Sytd_From_Date);                                                                                                //Natural: ASSIGN #START-DATE := #PARAM-SYTD-FROM-DATE
                        pnd_End_Date.setValue(pnd_Param_Sytd_To_Date);                                                                                                    //Natural: ASSIGN #END-DATE := #PARAM-SYTD-TO-DATE
                                                                                                                                                                          //Natural: PERFORM WRITE-PARAMS-TO-FF1
                        sub_Write_Params_To_Ff1();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        getReports().write(0, ReportOption.NOTITLE,"******  ERROR!! ******* ERROR!!******",NEWLINE,"**                                 **",               //Natural: WRITE '******  ERROR!! ******* ERROR!!******' / '**                                 **' / '**  Valid RUN-TYPE  :  R  or  S    **' / '**  then restart from this step.   **' / '**                                 **' / '*************************************'
                            NEWLINE,"**  Valid RUN-TYPE  :  R  or  S    **",NEWLINE,"**  then restart from this step.   **",NEWLINE,"**                                 **",
                            NEWLINE,"*************************************");
                        if (Global.isEscape()) return;
                        ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Tax_Year().setValue(pnd_Tax_Year);                                                                          //Natural: ASSIGN #FF1-TAX-YEAR := #TAX-YEAR
                        getWorkFiles().write(4, false, ldaTwrl0901.getPnd_Flat_File1());                                                                                  //Natural: WRITE WORK FILE 4 #FLAT-FILE1
                        DbsUtil.terminate(91);  if (true) return;                                                                                                         //Natural: TERMINATE 91
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: VALUES 'D'
                else if (condition((pnd_Param_Freq.equals("D"))))
                {
                    decideConditionsMet485++;
                    pnd_Start_Date.setValue(pnd_Param_Intf_From_Date);                                                                                                    //Natural: ASSIGN #START-DATE := #PARAM-INTF-FROM-DATE
                    pnd_End_Date.setValue(pnd_Param_Intf_From_Date);                                                                                                      //Natural: ASSIGN #END-DATE := #PARAM-INTF-FROM-DATE
                                                                                                                                                                          //Natural: PERFORM WRITE-PARAMS-TO-FF1
                    sub_Write_Params_To_Ff1();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    getReports().write(0, ReportOption.NOTITLE,"******  ERROR!! ******* ERROR!!******",NEWLINE,"**                                 **",                   //Natural: WRITE '******  ERROR!! ******* ERROR!!******' / '**                                 **' / '**  Valid PARAM-FREQ : Y  or  D    **' / '**  then restart from this step.   **' / '**                                 **' / '*************************************'
                        NEWLINE,"**  Valid PARAM-FREQ : Y  or  D    **",NEWLINE,"**  then restart from this step.   **",NEWLINE,"**                                 **",
                        NEWLINE,"*************************************");
                    if (Global.isEscape()) return;
                    ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Tax_Year().setValue(pnd_Tax_Year);                                                                              //Natural: ASSIGN #FF1-TAX-YEAR := #TAX-YEAR
                    getWorkFiles().write(4, false, ldaTwrl0901.getPnd_Flat_File1());                                                                                      //Natural: WRITE WORK FILE 4 #FLAT-FILE1
                    DbsUtil.terminate(92);  if (true) return;                                                                                                             //Natural: TERMINATE 92
                }                                                                                                                                                         //Natural: END-DECIDE
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 2 RECORD #XTAXYR-F94
                while (condition(getWorkFiles().read(2, ldaTwrl0900.getPnd_Xtaxyr_F94())))
                {
                    pnd_Rec_Read.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #REC-READ
                    pnd_Cnt.setValue(1);                                                                                                                                  //Natural: ASSIGN #CNT := 1
                                                                                                                                                                          //Natural: PERFORM COMPANY-CNT
                    sub_Company_Cnt();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    FOR01:                                                                                                                                                //Natural: FOR #I 1 #XTAXYR-F94.#C-TWRPYMNT-PAYMENTS
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments())); pnd_I.nadd(1))
                    {
                        if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Intfce_Dte().getValue(pnd_I).greaterOrEqual(pnd_Start_Date) && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Intfce_Dte().getValue(pnd_I).lessOrEqual(pnd_End_Date))) //Natural: IF #XTAXYR-F94.TWRPYMNT-PYMNT-INTFCE-DTE ( #I ) = #START-DATE THRU #END-DATE
                        {
                            pnd_Rec_Process.nadd(1);                                                                                                                      //Natural: ADD 1 TO #REC-PROCESS
                            pnd_Cnt.setValue(2);                                                                                                                          //Natural: ASSIGN #CNT := 2
                                                                                                                                                                          //Natural: PERFORM COMPANY-CNT
                            sub_Company_Cnt();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM DATA-WAREHOUSE
                            sub_Data_Warehouse();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-DETAIL
                            sub_Process_Detail();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: AT END OF DATA;//Natural: END-WORK
                READWORK01_Exit:
                if (condition(getWorkFiles().getAtEndOfData()))
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"TOTAL RECORDS READ     : ",pnd_Rec_Read,NEWLINE,"  1. CREF RECORDS      : ",           //Natural: WRITE ( 1 ) NOTITLE NOHDR 'TOTAL RECORDS READ     : ' #REC-READ / '  1. CREF RECORDS      : ' #CREF-CNT ( 1 ) / '  2. LIFE RECORDS      : ' #LIFE-CNT ( 1 ) / '  3. TIAA RECORDS      : ' #TIAA-CNT ( 1 ) / '  4. TCII RECORDS      : ' #TCII-CNT ( 1 ) / '  5. FSB   RECORDS     : ' #FSBC-CNT ( 1 ) / '  6. TRUST RECORDS     : ' #TRST-CNT ( 1 ) / '  7. OTHER CO. RECORDS : ' #OTHR-CNT ( 1 ) /// 'TOTAL RECORDS PROCESS  : ' #REC-PROCESS / '  1. CREF RECORDS      : ' #CREF-CNT ( 2 ) / '  2. LIFE RECORDS      : ' #LIFE-CNT ( 2 ) / '  3. TIAA RECORDS      : ' #TIAA-CNT ( 2 ) / '  4. TCII RECORDS      : ' #TCII-CNT ( 2 ) / '  5. FSB   RECORDS     : ' #FSBC-CNT ( 2 ) / '  6. TRUST RECORDS     : ' #TRST-CNT ( 2 ) / '  7. OTHER CO. RECORDS : ' #OTHR-CNT ( 2 ) /// 'TOTAL RECORDS WRITTEN  : ' #REC-RITTEN / '  1. CREF RECORDS      : ' #CREF-CNT ( 3 ) / '  2. LIFE RECORDS      : ' #LIFE-CNT ( 3 ) / '  3. TIAA RECORDS      : ' #TIAA-CNT ( 3 ) / '  4. TCII RECORDS      : ' #TCII-CNT ( 3 ) / '  5. FSB   RECORDS     : ' #FSBC-CNT ( 3 ) / '  6. TRUST RECORDS     : ' #TRST-CNT ( 3 ) / '  7. OTHER CO. RECORDS : ' #OTHR-CNT ( 3 ) ///
                        pnd_Cref_Cnt.getValue(1),NEWLINE,"  2. LIFE RECORDS      : ",pnd_Life_Cnt.getValue(1),NEWLINE,"  3. TIAA RECORDS      : ",pnd_Tiaa_Cnt.getValue(1),
                        NEWLINE,"  4. TCII RECORDS      : ",pnd_Tcii_Cnt.getValue(1),NEWLINE,"  5. FSB   RECORDS     : ",pnd_Fsbc_Cnt.getValue(1),NEWLINE,
                        "  6. TRUST RECORDS     : ",pnd_Trst_Cnt.getValue(1),NEWLINE,"  7. OTHER CO. RECORDS : ",pnd_Othr_Cnt.getValue(1),NEWLINE,NEWLINE,
                        NEWLINE,"TOTAL RECORDS PROCESS  : ",pnd_Rec_Process,NEWLINE,"  1. CREF RECORDS      : ",pnd_Cref_Cnt.getValue(2),NEWLINE,"  2. LIFE RECORDS      : ",
                        pnd_Life_Cnt.getValue(2),NEWLINE,"  3. TIAA RECORDS      : ",pnd_Tiaa_Cnt.getValue(2),NEWLINE,"  4. TCII RECORDS      : ",pnd_Tcii_Cnt.getValue(2),
                        NEWLINE,"  5. FSB   RECORDS     : ",pnd_Fsbc_Cnt.getValue(2),NEWLINE,"  6. TRUST RECORDS     : ",pnd_Trst_Cnt.getValue(2),NEWLINE,
                        "  7. OTHER CO. RECORDS : ",pnd_Othr_Cnt.getValue(2),NEWLINE,NEWLINE,NEWLINE,"TOTAL RECORDS WRITTEN  : ",pnd_Rec_Ritten,NEWLINE,
                        "  1. CREF RECORDS      : ",pnd_Cref_Cnt.getValue(3),NEWLINE,"  2. LIFE RECORDS      : ",pnd_Life_Cnt.getValue(3),NEWLINE,"  3. TIAA RECORDS      : ",
                        pnd_Tiaa_Cnt.getValue(3),NEWLINE,"  4. TCII RECORDS      : ",pnd_Tcii_Cnt.getValue(3),NEWLINE,"  5. FSB   RECORDS     : ",pnd_Fsbc_Cnt.getValue(3),
                        NEWLINE,"  6. TRUST RECORDS     : ",pnd_Trst_Cnt.getValue(3),NEWLINE,"  7. OTHER CO. RECORDS : ",pnd_Othr_Cnt.getValue(3),NEWLINE,
                        NEWLINE,NEWLINE);
                    if (condition(Global.isEscape())) return;
                    //*      '  5. TRUST RECORDS     : ' #TRST-CNT(1)    /
                    //*      '  6. OTHER CO. RECORDS : ' #OTHR-CNT(1)   ///
                    //*   DY1 FIX ENDS   <<
                    //*   DY1 FIX BEGINS >>
                    //*      '  5. TRUST RECORDS     : ' #TRST-CNT(2)    /
                    //*      '  6. OTHER CO. RECORDS : ' #OTHR-CNT(2)   ///
                    //*   DY1 FIX ENDS   <<
                    //*   DY1 FIX BEGINS >>
                    //*      '  5. TRUST RECORDS     : ' #TRST-CNT(3)    /
                    //*      '  6. OTHER CO. RECORDS : ' #OTHR-CNT(3)
                    //*   DY1 FIX ENDS   <<
                    //*  TEST ONLY
                    getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,NEWLINE,"FREQUENCY   : ",pnd_Param_Freq,NEWLINE,"TYPE OF RUN : ",               //Natural: WRITE NOTITLE NOHDR // 'FREQUENCY   : ' #PARAM-FREQ / 'TYPE OF RUN : ' #RUN-TYPE // 'DATA READ FROM CONTROL FILE : ' // '   INTERFACE DATE : ' #PARAM-INTF-FROM-DATE / '   FROM-DATE      : ' #PARAM-SYTD-FROM-DATE / '   TO-DATE        : ' #PARAM-SYTD-TO-DATE
                        pnd_Run_Type,NEWLINE,NEWLINE,"DATA READ FROM CONTROL FILE : ",NEWLINE,NEWLINE,"   INTERFACE DATE : ",pnd_Param_Intf_From_Date,NEWLINE,
                        "   FROM-DATE      : ",pnd_Param_Sytd_From_Date,NEWLINE,"   TO-DATE        : ",pnd_Param_Sytd_To_Date);
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-ENDDATA
                if (Global.isEscape()) return;
                //* *************************************
                //* *************************************
                //*              SOURCE CODES OTHER THAN 'AP', 'IA' & 'CP'.
                //* *****************************
                //* ********************************
                //* ********************************
                //*  SUMMARY FIELDS
                //*  PERIODIC FIELDS
                //* *******************************
                //* **
                //* **                                                                                                                                                    //Natural: AT TOP OF PAGE ( 1 )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    //*  10/15/99
    private void sub_Write_Params_To_Ff1() throws Exception                                                                                                               //Natural: WRITE-PARAMS-TO-FF1
    {
        if (BLNatReinput.isReinput()) return;

        ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Tax_Year().setValue(pnd_Tax_Year);                                                                                          //Natural: ASSIGN #FF1-TAX-YEAR := #TAX-YEAR
        ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Start_Date().setValue(pnd_Start_Date);                                                                                      //Natural: ASSIGN #FF1-START-DATE := #START-DATE
        ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_End_Date().setValue(pnd_End_Date);                                                                                          //Natural: ASSIGN #FF1-END-DATE := #END-DATE
        ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Frequency().setValue(pnd_Param_Freq);                                                                                       //Natural: ASSIGN #FF1-FREQUENCY := #PARAM-FREQ
        ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Intf_To_Date().setValue(pnd_Param_Intf_To_Date);                                                                            //Natural: ASSIGN #FF1-INTF-TO-DATE := #PARAM-INTF-TO-DATE
        //*  10/15/99
        if (condition(pnd_Param_Freq.equals("Y")))                                                                                                                        //Natural: IF #PARAM-FREQ = 'Y'
        {
            ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Ytd_Run_Type().setValue(pnd_Run_Type);                                                                                  //Natural: ASSIGN #FF1-YTD-RUN-TYPE := #RUN-TYPE
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().write(4, false, ldaTwrl0901.getPnd_Flat_File1());                                                                                                  //Natural: WRITE WORK FILE 4 #FLAT-FILE1
    }
    private void sub_Company_Cnt() throws Exception                                                                                                                       //Natural: COMPANY-CNT
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        //*    DY1
        short decideConditionsMet605 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #XTAXYR-F94.TWRPYMNT-COMPANY-CDE;//Natural: VALUE 'C'
        if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("C"))))
        {
            decideConditionsMet605++;
            pnd_Cref_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #CREF-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("L"))))
        {
            decideConditionsMet605++;
            pnd_Life_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #LIFE-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("T"))))
        {
            decideConditionsMet605++;
            pnd_Tiaa_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #TIAA-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("S"))))
        {
            decideConditionsMet605++;
            pnd_Tcii_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #TCII-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("F"))))
        {
            decideConditionsMet605++;
            pnd_Fsbc_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #FSBC-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("X"))))
        {
            decideConditionsMet605++;
            pnd_Trst_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #TRST-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Othr_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #OTHR-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Process_Detail() throws Exception                                                                                                                    //Natural: PROCESS-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Id_Nbr().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr());                                                 //Natural: ASSIGN #FF3-TAX-ID-NBR := #XTAXYR-F94.TWRPYMNT-TAX-ID-NBR
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde());                                              //Natural: ASSIGN #FF3-COMPANY-CODE := #XTAXYR-F94.TWRPYMNT-COMPANY-CDE
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Citizenship());                                       //Natural: ASSIGN #FF3-TAX-CITIZENSHIP := #XTAXYR-F94.TWRPYMNT-TAX-CITIZENSHIP
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Contract_Nbr().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr());                                             //Natural: ASSIGN #FF3-CONTRACT-NBR := #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Payee_Cde().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payee_Cde());                                                   //Natural: ASSIGN #FF3-PAYEE-CDE := #XTAXYR-F94.TWRPYMNT-PAYEE-CDE
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde());                                     //Natural: ASSIGN #FF3-DISTRIBUTION-CDE := #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Type().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Type());                                         //Natural: ASSIGN #FF3-RESIDENCY-TYPE := #XTAXYR-F94.TWRPYMNT-RESIDENCY-TYPE
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code());                                         //Natural: ASSIGN #FF3-RESIDENCY-CODE := #XTAXYR-F94.TWRPYMNT-RESIDENCY-CODE
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Ind().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Ind().getValue(pnd_I));                                       //Natural: ASSIGN #FF3-IVC-IND := #XTAXYR-F94.TWRPYMNT-IVC-IND ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Protect().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Protect().getValue(pnd_I).getBoolean());                  //Natural: ASSIGN #FF3-IVC-PROTECT := #XTAXYR-F94.TWRPYMNT-IVC-PROTECT ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Pymnt_Date().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Dte().getValue(pnd_I));                                  //Natural: ASSIGN #FF3-PYMNT-DATE := #XTAXYR-F94.TWRPYMNT-PYMNT-DTE ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Intfce_Dte().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Intfce_Dte().getValue(pnd_I));                           //Natural: ASSIGN #FF3-INTFCE-DTE := #XTAXYR-F94.TWRPYMNT-PYMNT-INTFCE-DTE ( #I )
        //*     /*  THE FF. ARE DONE FOR SORTING PURPOSE ONLY SO  AS
        //*     /*  TO PRINT PURE MANUAL RECORDS AT THE END OF THE REPORT
        //* **  THE 'NL','PL' ARE CODED TO MIMIC 'ML','OL' FOR SORTING PURPOSE.
        short decideConditionsMet641 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) = '  ' AND #XTAXYR-F94.TWRPYMNT-UPDTE-SRCE-CDE ( #I ) = 'ML'
        if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).equals("  ") && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I).equals("ML")))
        {
            decideConditionsMet641++;
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).setValue("ZZ");                                                                        //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) := 'ZZ'
        }                                                                                                                                                                 //Natural: WHEN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) = 'ML' AND #XTAXYR-F94.TWRPYMNT-UPDTE-SRCE-CDE ( #I ) = 'OL'
        else if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).equals("ML") && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I).equals("OL")))
        {
            decideConditionsMet641++;
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).setValue("ZZ");                                                                        //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) := 'ZZ'
        }                                                                                                                                                                 //Natural: WHEN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) = '  ' AND #XTAXYR-F94.TWRPYMNT-UPDTE-SRCE-CDE ( #I ) = 'NL'
        else if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).equals("  ") && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I).equals("NL")))
        {
            decideConditionsMet641++;
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).setValue("YY");                                                                        //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) := 'YY'
        }                                                                                                                                                                 //Natural: WHEN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) = 'NL' AND #XTAXYR-F94.TWRPYMNT-UPDTE-SRCE-CDE ( #I ) = 'PL'
        else if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).equals("NL") && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I).equals("PL")))
        {
            decideConditionsMet641++;
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).setValue("YY");                                                                        //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) := 'YY'
        }                                                                                                                                                                 //Natural: WHEN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) = 'TM' AND #XTAXYR-F94.TWRPYMNT-UPDTE-SRCE-CDE ( #I ) = 'AL'
        else if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).equals("TM") && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I).equals("AL")))
        {
            decideConditionsMet641++;
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).setValue("AP");                                                                        //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) := 'AP'
        }                                                                                                                                                                 //Natural: WHEN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) = 'TM' AND #XTAXYR-F94.TWRPYMNT-UPDTE-SRCE-CDE ( #I ) = 'IL'
        else if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).equals("TM") && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I).equals("IL")))
        {
            decideConditionsMet641++;
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).setValue("IA");                                                                        //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) := 'IA'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I),  //Natural: COMPRESS #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) #XTAXYR-F94.TWRPYMNT-UPDTE-SRCE-CDE ( #I ) TO #FF3-SOURCE-CODE LEAVING NO SPACE
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I)));
        //*  11/2014
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Payset_Type().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payment_Type().getValue(pnd_I),  //Natural: COMPRESS #XTAXYR-F94.TWRPYMNT-PAYMENT-TYPE ( #I ) #XTAXYR-F94.TWRPYMNT-SETTLE-TYPE ( #I ) TO #FF3-PAYSET-TYPE LEAVING NO SPACE
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_I)));
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_I));                                   //Natural: ASSIGN #FF3-GROSS-AMT := #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_I));                                       //Natural: ASSIGN #FF3-IVC-AMT := #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Int_Amt().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_I));                                       //Natural: ASSIGN #FF3-INT-AMT := #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Fed_Wthld_Amt().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_I));                           //Natural: ASSIGN #FF3-FED-WTHLD-AMT := #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Nra_Wthld_Amt().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Nra_Wthld_Amt().getValue(pnd_I));                           //Natural: ASSIGN #FF3-NRA-WTHLD-AMT := #XTAXYR-F94.TWRPYMNT-NRA-WTHLD-AMT ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Can_Wthld_Amt().getValue(pnd_I));                           //Natural: ASSIGN #FF3-CAN-WTHLD-AMT := #XTAXYR-F94.TWRPYMNT-CAN-WTHLD-AMT ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_I));                               //Natural: ASSIGN #FF3-STATE-WTHLD := #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Local_Wthld().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Local_Wthld().getValue(pnd_I));                               //Natural: ASSIGN #FF3-LOCAL-WTHLD := #XTAXYR-F94.TWRPYMNT-LOCAL-WTHLD ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Plan().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Omni_Plan().getValue(pnd_I));                                             //Natural: ASSIGN #FF3-PLAN := #XTAXYR-F94.#OMNI-PLAN ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Subplan().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Omni_Subplan().getValue(pnd_I));                                       //Natural: ASSIGN #FF3-SUBPLAN := #XTAXYR-F94.#OMNI-SUBPLAN ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Orgntng_Cntrct().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgntng_Contract().getValue(pnd_I));                       //Natural: ASSIGN #FF3-ORGNTNG-CNTRCT := #XTAXYR-F94.TWRPYMNT-ORGNTNG-CONTRACT ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Orgntng_Subplan().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgntng_Subplan().getValue(pnd_I));                       //Natural: ASSIGN #FF3-ORGNTNG-SUBPLAN := #XTAXYR-F94.TWRPYMNT-ORGNTNG-SUBPLAN ( #I )
        ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Twrpymnt_Fatca_Ind().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fatca_Ind().getValue(1));                              //Natural: ASSIGN #FF3-TWRPYMNT-FATCA-IND := #XTAXYR-F94.TWRPYMNT-FATCA-IND ( 1 )
        getWorkFiles().write(3, false, ldaTwrl0902.getPnd_Flat_File3());                                                                                                  //Natural: WRITE WORK FILE 3 #FLAT-FILE3
        pnd_Rec_Ritten.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #REC-RITTEN
        pnd_Cnt.setValue(3);                                                                                                                                              //Natural: ASSIGN #CNT := 3
                                                                                                                                                                          //Natural: PERFORM COMPANY-CNT
        sub_Company_Cnt();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    }
    private void sub_Data_Warehouse() throws Exception                                                                                                                    //Natural: DATA-WAREHOUSE
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year().lessOrEqual(2005)))                                                                               //Natural: IF #XTAXYR-F94.TWRPYMNT-TAX-YEAR LE 2005
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Tax_Yr().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());                                                          //Natural: ASSIGN #TX-DW-DISTR.#TAX-YR := #XTAXYR-F94.TWRPYMNT-TAX-YEAR
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Company_Cd().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde());                                                   //Natural: ASSIGN #TX-DW-DISTR.#COMPANY-CD := #XTAXYR-F94.TWRPYMNT-COMPANY-CDE
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Tin().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr());                                                           //Natural: ASSIGN #TX-DW-DISTR.#TIN := #XTAXYR-F94.TWRPYMNT-TAX-ID-NBR
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Tin_Type().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Type());                                                     //Natural: ASSIGN #TX-DW-DISTR.#TIN-TYPE := #XTAXYR-F94.TWRPYMNT-TAX-ID-TYPE
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Contract().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr());                                                    //Natural: ASSIGN #TX-DW-DISTR.#CONTRACT := #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Payee().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payee_Cde());                                                          //Natural: ASSIGN #TX-DW-DISTR.#PAYEE := #XTAXYR-F94.TWRPYMNT-PAYEE-CDE
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Payment_Cat().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Paymt_Category());                                               //Natural: ASSIGN #TX-DW-DISTR.#PAYMENT-CAT := #XTAXYR-F94.TWRPYMNT-PAYMT-CATEGORY
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Distrib_Cd().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde());                                              //Natural: ASSIGN #TX-DW-DISTR.#DISTRIB-CD := #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Residency_Type().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Type());                                            //Natural: ASSIGN #TX-DW-DISTR.#RESIDENCY-TYPE := #XTAXYR-F94.TWRPYMNT-RESIDENCY-TYPE
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Residency_Cd().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code());                                              //Natural: ASSIGN #TX-DW-DISTR.#RESIDENCY-CD := #XTAXYR-F94.TWRPYMNT-RESIDENCY-CODE
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Locality_Cd().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Locality_Cde());                                                 //Natural: ASSIGN #TX-DW-DISTR.#LOCALITY-CD := #XTAXYR-F94.TWRPYMNT-LOCALITY-CDE
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Citizenship_Cd().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Citizen_Cde());                                               //Natural: ASSIGN #TX-DW-DISTR.#CITIZENSHIP-CD := #XTAXYR-F94.TWRPYMNT-CITIZEN-CDE
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Tax_Citizenship().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Citizenship());                                          //Natural: ASSIGN #TX-DW-DISTR.#TAX-CITIZENSHIP := #XTAXYR-F94.TWRPYMNT-TAX-CITIZENSHIP
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Payment_Dt().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Dte().getValue(pnd_I));                                     //Natural: ASSIGN #TX-DW-DISTR.#PAYMENT-DT := #XTAXYR-F94.TWRPYMNT-PYMNT-DTE ( #I )
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Payment_Type().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payment_Type().getValue(pnd_I));                                //Natural: ASSIGN #TX-DW-DISTR.#PAYMENT-TYPE := #XTAXYR-F94.TWRPYMNT-PAYMENT-TYPE ( #I )
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Settle_Type().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_I));                                  //Natural: ASSIGN #TX-DW-DISTR.#SETTLE-TYPE := #XTAXYR-F94.TWRPYMNT-SETTLE-TYPE ( #I )
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Srce_Orig().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I));                                  //Natural: ASSIGN #TX-DW-DISTR.#SRCE-ORIG := #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I )
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Srce_Updt().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I));                                 //Natural: ASSIGN #TX-DW-DISTR.#SRCE-UPDT := #XTAXYR-F94.TWRPYMNT-UPDTE-SRCE-CDE ( #I )
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_W8_Ben_1001().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Frm1001_Ind().getValue(pnd_I));                                  //Natural: ASSIGN #TX-DW-DISTR.#W8-BEN-1001 := #XTAXYR-F94.TWRPYMNT-FRM1001-IND ( #I )
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_W9_1078().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Frm1078_Ind().getValue(pnd_I));                                      //Natural: ASSIGN #TX-DW-DISTR.#W9-1078 := #XTAXYR-F94.TWRPYMNT-FRM1078-IND ( #I )
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Omni_Plan().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Omni_Plan().getValue(pnd_I));                                           //Natural: ASSIGN #TX-DW-DISTR.#OMNI-PLAN := #XTAXYR-F94.#OMNI-PLAN ( #I )
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Omni_Sub_Plan().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Omni_Subplan().getValue(pnd_I));                                    //Natural: ASSIGN #TX-DW-DISTR.#OMNI-SUB-PLAN := #XTAXYR-F94.#OMNI-SUBPLAN ( #I )
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Ivc_Ind().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Ind().getValue(pnd_I));                                          //Natural: ASSIGN #TX-DW-DISTR.#IVC-IND := #XTAXYR-F94.TWRPYMNT-IVC-IND ( #I )
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Intfce_Dt().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Intfce_Dte().getValue(pnd_I));                               //Natural: ASSIGN #TX-DW-DISTR.#INTFCE-DT := #XTAXYR-F94.TWRPYMNT-PYMNT-INTFCE-DTE ( #I )
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Contract_Type().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Type().getValue(pnd_I));                              //Natural: ASSIGN #TX-DW-DISTR.#CONTRACT-TYPE := #XTAXYR-F94.TWRPYMNT-CONTRACT-TYPE ( #I )
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Ivc_Protect().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Protect().getValue(pnd_I).getBoolean());                     //Natural: ASSIGN #TX-DW-DISTR.#IVC-PROTECT := #XTAXYR-F94.TWRPYMNT-IVC-PROTECT ( #I )
        setValueToSubstring(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Sys_Dte_Time().getValue(pnd_I).getSubstring(1,8),ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Updt_Dt(),         //Natural: MOVE SUBSTR ( #XTAXYR-F94.TWRPYMNT-SYS-DTE-TIME ( #I ) ,1,8 ) TO SUBSTR ( #TX-DW-DISTR.#UPDT-DT ,1,8 )
            1,8);
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Updt_User().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_User().getValue(pnd_I));                                     //Natural: ASSIGN #TX-DW-DISTR.#UPDT-USER := #XTAXYR-F94.TWRPYMNT-UPDTE-USER ( #I )
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Status().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Status().getValue(pnd_I));                                      //Natural: ASSIGN #TX-DW-DISTR.#STATUS := #XTAXYR-F94.TWRPYMNT-PYMNT-STATUS ( #I )
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Roth_Qual_Ind().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Roth_Qual_Ind().getValue(pnd_I));                              //Natural: ASSIGN #TX-DW-DISTR.#ROTH-QUAL-IND := #XTAXYR-F94.TWRPYMNT-ROTH-QUAL-IND ( #I )
        //*  M001
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Error_Reason_Code().setValueEdited(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Error_Reason().getValue(pnd_I),new                   //Natural: MOVE EDITED #XTAXYR-F94.TWRPYMNT-ERROR-REASON ( #I ) ( EM = 99 ) TO #TX-DW-DISTR.#ERROR-REASON-CODE
            ReportEditMask("99"));
        //*  11/2014
        //*  11/2014
        //*  11/2014
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Origin_Area().setValueEdited(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Origin_Area().getValue(pnd_I),new ReportEditMask("99"));   //Natural: MOVE EDITED #XTAXYR-F94.TWRPYMNT-ORIGIN-AREA ( #I ) ( EM = 99 ) TO #TX-DW-DISTR.#ORIGIN-AREA
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Subplan_Nbr().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Omni_Subplan().getValue(pnd_I));                             //Natural: ASSIGN #TX-DW-DISTR.#TWRPYMNT-SUBPLAN-NBR := #XTAXYR-F94.#OMNI-SUBPLAN ( #I )
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Orgntng_Contract().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgntng_Contract().getValue(pnd_I));               //Natural: ASSIGN #TX-DW-DISTR.#TWRPYMNT-ORGNTNG-CONTRACT := #XTAXYR-F94.TWRPYMNT-ORGNTNG-CONTRACT ( #I )
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Orgntng_Subplan().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgntng_Subplan().getValue(pnd_I));                 //Natural: ASSIGN #TX-DW-DISTR.#TWRPYMNT-ORGNTNG-SUBPLAN := #XTAXYR-F94.TWRPYMNT-ORGNTNG-SUBPLAN ( #I )
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Giin().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Giin().getValue(1));                                           //Natural: ASSIGN #TX-DW-DISTR.#TWRPYMNT-GIIN := #XTAXYR-F94.TWRPYMNT-GIIN ( 1 )
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Chptr4_Exempt_Cde().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Chptr4_Exempt_Cde().getValue(pnd_I));             //Natural: ASSIGN #TX-DW-DISTR.#TWRPYMNT-CHPTR4-EXEMPT-CDE := #XTAXYR-F94.TWRPYMNT-CHPTR4-EXEMPT-CDE ( #I )
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Fatca_Ind().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fatca_Ind().getValue(1));                                 //Natural: ASSIGN #TX-DW-DISTR.#TWRPYMNT-FATCA-IND := #XTAXYR-F94.TWRPYMNT-FATCA-IND ( 1 )
        //* *********************************************************************
        //*  MOVE EDITED DOLLAR VALUES TO 15 BYTE ALPHA FIELDS FOR DATA WAREHOUSE
        //* *********************************************************************
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Payment_Amt().setValueEdited(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_I),new ReportEditMask("-99999999999.99")); //Natural: MOVE EDITED #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) ( EM = -99999999999.99 ) TO #TX-DW-DISTR.#PAYMENT-AMT
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Ivc_Amt().setValueEdited(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_I),new ReportEditMask("-99999999999.99")); //Natural: MOVE EDITED #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) ( EM = -99999999999.99 ) TO #TX-DW-DISTR.#IVC-AMT
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Interest_Amt().setValueEdited(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_I),new ReportEditMask("-99999999999.99")); //Natural: MOVE EDITED #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) ( EM = -99999999999.99 ) TO #TX-DW-DISTR.#INTEREST-AMT
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Fed_Wh_Amt().setValueEdited(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_I),new ReportEditMask("-99999999999.99")); //Natural: MOVE EDITED #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) ( EM = -99999999999.99 ) TO #TX-DW-DISTR.#FED-WH-AMT
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_State_Wh_Amt().setValueEdited(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_I),new ReportEditMask("-99999999999.99")); //Natural: MOVE EDITED #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) ( EM = -99999999999.99 ) TO #TX-DW-DISTR.#STATE-WH-AMT
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Local_Wh_Amt().setValueEdited(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Local_Wthld().getValue(pnd_I),new ReportEditMask("-99999999999.99")); //Natural: MOVE EDITED #XTAXYR-F94.TWRPYMNT-LOCAL-WTHLD ( #I ) ( EM = -99999999999.99 ) TO #TX-DW-DISTR.#LOCAL-WH-AMT
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Nra_Wh_Amt().setValueEdited(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Nra_Wthld_Amt().getValue(pnd_I),new ReportEditMask("-99999999999.99")); //Natural: MOVE EDITED #XTAXYR-F94.TWRPYMNT-NRA-WTHLD-AMT ( #I ) ( EM = -99999999999.99 ) TO #TX-DW-DISTR.#NRA-WH-AMT
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Canada_Wh_Amt().setValueEdited(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Can_Wthld_Amt().getValue(pnd_I),new ReportEditMask("-99999999999.99")); //Natural: MOVE EDITED #XTAXYR-F94.TWRPYMNT-CAN-WTHLD-AMT ( #I ) ( EM = -99999999999.99 ) TO #TX-DW-DISTR.#CANADA-WH-AMT
        //*  RC
        //*  RCC
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Irr_Amt().setValueEdited(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Irr_Amt().getValue(pnd_I),new ReportEditMask("-99999999999.99")); //Natural: MOVE EDITED #XTAXYR-F94.TWRPYMNT-IRR-AMT ( #I ) ( EM = -99999999999.99 ) TO #TX-DW-DISTR.#TWRPYMNT-IRR-AMT
        //*  11/2014
        ldaTwrl0907.getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Chptr4_Tax_Rate().setValueEdited(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Chptr4_Tax_Rate().getValue(pnd_I),new         //Natural: MOVE EDITED #XTAXYR-F94.TWRPYMNT-CHPTR4-TAX-RATE ( #I ) ( EM = 999 ) TO #TX-DW-DISTR.#TWRPYMNT-CHPTR4-TAX-RATE
            ReportEditMask("999"));
        getWorkFiles().write(6, false, ldaTwrl0907.getPnd_Tx_Dw_Distr());                                                                                                 //Natural: WRITE WORK FILE 6 #TX-DW-DISTR
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(34),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR *INIT-USER '-' *PROGRAM 34T 'TAX WITHHOLDING AND REPORTING SYSTEM' 119T 'PAGE' *PAGE-NUMBER ( 1 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 42T 'CREATED FLAT FILE3' / 'RUNTIME : ' *TIMX 44T 'TAX YEAR ' #TAX-YEAR / 44T 'CONTROL TOTALS' / 32T 'DATE RANGE : ' #START-DATE ' THRU ' #END-DATE ///
                        TabSetting(119),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(42),"CREATED FLAT FILE3",NEWLINE,"RUNTIME : ",Global.getTIMX(),new TabSetting(44),"TAX YEAR ",pnd_Tax_Year,NEWLINE,new 
                        TabSetting(44),"CONTROL TOTALS",NEWLINE,new TabSetting(32),"DATE RANGE : ",pnd_Start_Date," THRU ",pnd_End_Date,NEWLINE,NEWLINE,
                        NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");
    }
}
