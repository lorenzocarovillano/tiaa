/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:44:27 PM
**        * FROM NATURAL PROGRAM : Twrp8100
************************************************************
**        * FILE NAME            : Twrp8100.java
**        * CLASS NAME           : Twrp8100
**        * INSTANCE NAME        : Twrp8100
************************************************************
************************************************************************
* PROGRAM     :  TWRP8100
* SYSTEM      :  TAX REPORTING AND WITHHOLDINGS SYSTEM.
* AUTHOR      :  RIAD A. LOUTFI
* PURPOSE     :  PRINT 7 REPORTS OF ALL CONTROL TABLES ON THE CONTROL
*                FILE (CONTROL FILE 98).
* DATE        :  11/13/1999
************************************************************************
*  CHANGE  LOG
* ----------------------------------------------------------------------
* 08/13/02 JR  CHANGED TIRCNTL-STATE-TAX-ID-ONE-MORE TO
*                      TIRCNTL-STATE-TAX-ID-TCII
* 09/12/02 RM  CHANGED REPORT 2 HEADING TO REFLECT TCII CHANGES
* 09/21/04 RM  TOPS RELEASE 3 CHANGES
*              A NEW COMPANY (TRUST COMPANY) HAD BEEN ADDED TO THE STATE
*              AND COMPANY TABLES. CHANGE REPORT 2 TO REFLECT THAT.
* 10/18/11  JB NON-ERISA TDA - ADD COMPANY CODE 'F' SCAN JB1011
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp8100 extends BLNatBase
{
    // Data Areas
    private LdaTwrl9801 ldaTwrl9801;
    private LdaTwrl9802 ldaTwrl9802;
    private LdaTwrl9803 ldaTwrl9803;
    private LdaTwrl9804 ldaTwrl9804;
    private LdaTwrl9805 ldaTwrl9805;
    private LdaTwrl9806 ldaTwrl9806;
    private LdaTwrl9807 ldaTwrl9807;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Tbl1_Ctr;
    private DbsField pnd_Tbl2_Ctr;
    private DbsField pnd_Tbl3_Ctr;
    private DbsField pnd_Tbl4_Ctr;
    private DbsField pnd_Tbl5_Ctr;
    private DbsField pnd_Tbl6_Ctr;
    private DbsField pnd_Tbl7_Ctr;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_1;
    private DbsField pnd_Date_Pnd_Date_Yyyy;
    private DbsField pnd_Date_Pnd_Date_Mm;
    private DbsField pnd_Date_Pnd_Date_Dd;
    private DbsField pnd_Isn;
    private DbsField pnd_S;

    private DbsGroup pnd_S__R_Field_2;
    private DbsField pnd_S_Pnd_Tircntl_Tbl_Nbr;
    private DbsField pnd_S_Pnd_Tircntl_Tax_Year;
    private DbsField pnd_S_Pnd_Tircntl_Seq_Nbr;
    private DbsField pnd_I;
    private DbsField pnd_Tax_Year_A;

    private DbsGroup pnd_Tax_Year_A__R_Field_3;
    private DbsField pnd_Tax_Year_A_Pnd_Tax_Year;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl9801 = new LdaTwrl9801();
        registerRecord(ldaTwrl9801);
        registerRecord(ldaTwrl9801.getVw_tircntl_Company_Info_Tbl_View_Vi());
        ldaTwrl9802 = new LdaTwrl9802();
        registerRecord(ldaTwrl9802);
        registerRecord(ldaTwrl9802.getVw_tircntl_Country_Code_Tbl_View_Vi());
        ldaTwrl9803 = new LdaTwrl9803();
        registerRecord(ldaTwrl9803);
        registerRecord(ldaTwrl9803.getVw_tircntl_Dist_Code_Tbl_View_View());
        ldaTwrl9804 = new LdaTwrl9804();
        registerRecord(ldaTwrl9804);
        registerRecord(ldaTwrl9804.getVw_tircntl_Feeder_Sys_Tbl_View_View());
        ldaTwrl9805 = new LdaTwrl9805();
        registerRecord(ldaTwrl9805);
        registerRecord(ldaTwrl9805.getVw_tircntl_Reporting_Tbl_View_View());
        ldaTwrl9806 = new LdaTwrl9806();
        registerRecord(ldaTwrl9806);
        registerRecord(ldaTwrl9806.getVw_tircntl_State_Code_Tbl_View_View());
        ldaTwrl9807 = new LdaTwrl9807();
        registerRecord(ldaTwrl9807);
        registerRecord(ldaTwrl9807.getVw_tircntl_Error_Msg_Tbl_View_View());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Tbl1_Ctr = localVariables.newFieldInRecord("pnd_Tbl1_Ctr", "#TBL1-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Tbl2_Ctr = localVariables.newFieldInRecord("pnd_Tbl2_Ctr", "#TBL2-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Tbl3_Ctr = localVariables.newFieldInRecord("pnd_Tbl3_Ctr", "#TBL3-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Tbl4_Ctr = localVariables.newFieldInRecord("pnd_Tbl4_Ctr", "#TBL4-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Tbl5_Ctr = localVariables.newFieldInRecord("pnd_Tbl5_Ctr", "#TBL5-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Tbl6_Ctr = localVariables.newFieldInRecord("pnd_Tbl6_Ctr", "#TBL6-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Tbl7_Ctr = localVariables.newFieldInRecord("pnd_Tbl7_Ctr", "#TBL7-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.NUMERIC, 8);

        pnd_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Date__R_Field_1", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Date_Yyyy = pnd_Date__R_Field_1.newFieldInGroup("pnd_Date_Pnd_Date_Yyyy", "#DATE-YYYY", FieldType.NUMERIC, 4);
        pnd_Date_Pnd_Date_Mm = pnd_Date__R_Field_1.newFieldInGroup("pnd_Date_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 2);
        pnd_Date_Pnd_Date_Dd = pnd_Date__R_Field_1.newFieldInGroup("pnd_Date_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_S = localVariables.newFieldInRecord("pnd_S", "#S", FieldType.NUMERIC, 8);

        pnd_S__R_Field_2 = localVariables.newGroupInRecord("pnd_S__R_Field_2", "REDEFINE", pnd_S);
        pnd_S_Pnd_Tircntl_Tbl_Nbr = pnd_S__R_Field_2.newFieldInGroup("pnd_S_Pnd_Tircntl_Tbl_Nbr", "#TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1);
        pnd_S_Pnd_Tircntl_Tax_Year = pnd_S__R_Field_2.newFieldInGroup("pnd_S_Pnd_Tircntl_Tax_Year", "#TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_S_Pnd_Tircntl_Seq_Nbr = pnd_S__R_Field_2.newFieldInGroup("pnd_S_Pnd_Tircntl_Seq_Nbr", "#TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 6);
        pnd_Tax_Year_A = localVariables.newFieldInRecord("pnd_Tax_Year_A", "#TAX-YEAR-A", FieldType.STRING, 4);

        pnd_Tax_Year_A__R_Field_3 = localVariables.newGroupInRecord("pnd_Tax_Year_A__R_Field_3", "REDEFINE", pnd_Tax_Year_A);
        pnd_Tax_Year_A_Pnd_Tax_Year = pnd_Tax_Year_A__R_Field_3.newFieldInGroup("pnd_Tax_Year_A_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl9801.initializeValues();
        ldaTwrl9802.initializeValues();
        ldaTwrl9803.initializeValues();
        ldaTwrl9804.initializeValues();
        ldaTwrl9805.initializeValues();
        ldaTwrl9806.initializeValues();
        ldaTwrl9807.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp8100() throws Exception
    {
        super("Twrp8100");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        getReports().atTopOfPage(atTopEventRpt5, 5);
        getReports().atTopOfPage(atTopEventRpt6, 6);
        getReports().atTopOfPage(atTopEventRpt7, 7);
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133;//Natural: FORMAT ( 02 ) PS = 60 LS = 133;//Natural: FORMAT ( 03 ) PS = 60 LS = 133;//Natural: FORMAT ( 04 ) PS = 60 LS = 133;//Natural: FORMAT ( 05 ) PS = 60 LS = 133;//Natural: FORMAT ( 06 ) PS = 60 LS = 133;//Natural: FORMAT ( 07 ) PS = 60 LS = 133
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *****************************
        //* * PRINT DISTRIBUTION TABLE **
        //* *****************************
        pnd_Tbl1_Ctr.setValue(0);                                                                                                                                         //Natural: ASSIGN #TBL1-CTR := 0
        ldaTwrl9803.getVw_tircntl_Dist_Code_Tbl_View_View().startDatabaseRead                                                                                             //Natural: READ TIRCNTL-DIST-CODE-TBL-VIEW-VIEW BY TIRCNTL-NBR-YEAR-DISTR-CODE
        (
        "READ01",
        new Oc[] { new Oc("TIRCNTL_NBR_YEAR_DISTR_CODE", "ASC") }
        );
        READ01:
        while (condition(ldaTwrl9803.getVw_tircntl_Dist_Code_Tbl_View_View().readNextRow("READ01")))
        {
            if (condition(ldaTwrl9803.getTircntl_Dist_Code_Tbl_View_View_Tircntl_Tbl_Nbr().equals(1)))                                                                    //Natural: IF TIRCNTL-TBL-NBR = 1
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tbl1_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TBL1-CTR
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),ldaTwrl9803.getTircntl_Dist_Code_Tbl_View_View_Tircntl_Tbl_Nbr(),new          //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T TIRCNTL-TBL-NBR 05T TIRCNTL-TAX-YEAR 11T TIRCNTL-SEQ-NBR 19T TIRCNTL-TYPE-REFER 26T TIRCNTL-PAYMT-TYPE 33T TIRCNTL-SETTL-TYPE 38T TIRCNTL-SETTL-PAYM-DESCR 83T TIRCNTL-OVER59-AGE-IND 94T TIRCNTL-DIST-CODE 108T TIRCNTL-DIST-METHOD 120T TIRCNTL-PAYMT-CATEGORY
                TabSetting(5),ldaTwrl9803.getTircntl_Dist_Code_Tbl_View_View_Tircntl_Tax_Year(),new TabSetting(11),ldaTwrl9803.getTircntl_Dist_Code_Tbl_View_View_Tircntl_Seq_Nbr(),new 
                TabSetting(19),ldaTwrl9803.getTircntl_Dist_Code_Tbl_View_View_Tircntl_Type_Refer(),new TabSetting(26),ldaTwrl9803.getTircntl_Dist_Code_Tbl_View_View_Tircntl_Paymt_Type(),new 
                TabSetting(33),ldaTwrl9803.getTircntl_Dist_Code_Tbl_View_View_Tircntl_Settl_Type(),new TabSetting(38),ldaTwrl9803.getTircntl_Dist_Code_Tbl_View_View_Tircntl_Settl_Paym_Descr(),new 
                TabSetting(83),ldaTwrl9803.getTircntl_Dist_Code_Tbl_View_View_Tircntl_Over59_Age_Ind(),new TabSetting(94),ldaTwrl9803.getTircntl_Dist_Code_Tbl_View_View_Tircntl_Dist_Code(),new 
                TabSetting(108),ldaTwrl9803.getTircntl_Dist_Code_Tbl_View_View_Tircntl_Dist_Method(),new TabSetting(120),ldaTwrl9803.getTircntl_Dist_Code_Tbl_View_View_Tircntl_Paymt_Category());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),"ISN: ",Global.getAstISN());                                                  //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T '=' *ISN
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaTwrl9803.getTircntl_Dist_Code_Tbl_View_View_Tircntl_Dist_Code_Descr().equals(" ")))                                                          //Natural: IF TIRCNTL-DIST-CODE-DESCR = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(38),ldaTwrl9803.getTircntl_Dist_Code_Tbl_View_View_Tircntl_Dist_Code_Descr()); //Natural: WRITE ( 01 ) NOTITLE NOHDR 38T TIRCNTL-DIST-CODE-DESCR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ****************************
        //* *    PRINT STATE TABLE    **
        //* ****************************
        pnd_Tbl2_Ctr.setValue(0);                                                                                                                                         //Natural: ASSIGN #TBL2-CTR := 0
        ldaTwrl9806.getVw_tircntl_State_Code_Tbl_View_View().startDatabaseRead                                                                                            //Natural: READ TIRCNTL-STATE-CODE-TBL-VIEW-VIEW BY TIRCNTL-NBR-YEAR-STATE-CDE
        (
        "READ02",
        new Oc[] { new Oc("TIRCNTL_NBR_YEAR_STATE_CDE", "ASC") }
        );
        READ02:
        while (condition(ldaTwrl9806.getVw_tircntl_State_Code_Tbl_View_View().readNextRow("READ02")))
        {
            if (condition(ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_Tbl_Nbr().equals(2)))                                                                   //Natural: IF TIRCNTL-TBL-NBR = 2
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tbl2_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TBL2-CTR
            getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_Tbl_Nbr(),new         //Natural: WRITE ( 02 ) NOTITLE NOHDR 01T TIRCNTL-TBL-NBR 04T TIRCNTL-TAX-YEAR 10T TIRCNTL-SEQ-NBR ( EM = 999 SG = OFF ) 14T TIRCNTL-STATE-OLD-CODE 20T TIRCNTL-STATE-ALPHA-CODE 25T TIRCNTL-STATE-FULL-NAME 45T TIRCNTL-STATE-TAX-ID-TIAA 68T TIRCNTL-COMB-FED-IND 72T TIRCNTL-COMB-FED-UPDTE-DTE 85T TIRCNTL-STATE-IND 92T TIRCNTL-MEDIA-CODE 99T TIRCNTL-STATE-WHHLD-IND 103T TIRCNTL-STATE-ENTRY-DTE 113T TIRCNTL-STATE-UPDTE-DTE 124T TIRCNTL-STATE-UPDTE-USER
                TabSetting(4),ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_Tax_Year(),new TabSetting(10),ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_Seq_Nbr(), 
                new ReportEditMask ("999"), new SignPosition (false),new TabSetting(14),ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_State_Old_Code(),new 
                TabSetting(20),ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_State_Alpha_Code(),new TabSetting(25),ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_State_Full_Name(),new 
                TabSetting(45),ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_State_Tax_Id_Tiaa(),new TabSetting(68),ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_Comb_Fed_Ind(),new 
                TabSetting(72),ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_Comb_Fed_Updte_Dte(),new TabSetting(85),ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_State_Ind(),new 
                TabSetting(92),ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_Media_Code(),new TabSetting(99),ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_State_Whhld_Ind(),new 
                TabSetting(103),ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_State_Entry_Dte(),new TabSetting(113),ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_State_Updte_Dte(),new 
                TabSetting(124),ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_State_Updte_User());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(16),"Threshold:",ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_Threshold_Amt(),  //Natural: WRITE ( 02 ) NOTITLE NOHDR 16T 'Threshold:' TIRCNTL-THRESHOLD-AMT ( EM = Z,ZZ9.99 ) 45T TIRCNTL-STATE-TAX-ID-CREF 'Fed:' TIRCNTL-FED-IND 1X 'Corr Fed:' TIRCNTL-CORR-FED-IND 1X 'Corr Comb Fed:' TIRCNTL-CORR-COMB-FED-IND 1X 'Corr Media:' TIRCNTL-CORR-MEDIA-CODE 1X 'Corr State:' TIRCNTL-CORR-STATE-IND
                new ReportEditMask ("Z,ZZ9.99"),new TabSetting(45),ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_State_Tax_Id_Cref(),"Fed:",ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_Fed_Ind(),new 
                ColumnSpacing(1),"Corr Fed:",ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_Corr_Fed_Ind(),new ColumnSpacing(1),"Corr Comb Fed:",ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_Corr_Comb_Fed_Ind(),new 
                ColumnSpacing(1),"Corr Media:",ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_Corr_Media_Code(),new ColumnSpacing(1),"Corr State:",
                ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_Corr_State_Ind());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_State_Tax_Id_Life().equals(" ")))                                                       //Natural: IF TIRCNTL-STATE-TAX-ID-LIFE = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(45),ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_State_Tax_Id_Life()); //Natural: WRITE ( 02 ) NOTITLE NOHDR 45T TIRCNTL-STATE-TAX-ID-LIFE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_State_Tax_Id_Tcii().equals(" ")))                                                       //Natural: IF TIRCNTL-STATE-TAX-ID-TCII = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(45),ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_State_Tax_Id_Tcii()); //Natural: WRITE ( 02 ) NOTITLE NOHDR 45T TIRCNTL-STATE-TAX-ID-TCII
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_State_Tax_Id_Trust().equals(" ")))                                                      //Natural: IF TIRCNTL-STATE-TAX-ID-TRUST = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(45),ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_State_Tax_Id_Trust()); //Natural: WRITE ( 02 ) NOTITLE NOHDR 45T TIRCNTL-STATE-TAX-ID-TRUST
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //* JB1011 NEXT 6 LINES
            if (condition(ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_State_Tax_Id_Fsb().equals(" ")))                                                        //Natural: IF TIRCNTL-STATE-TAX-ID-FSB = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(45),ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_State_Tax_Id_Fsb()); //Natural: WRITE ( 02 ) NOTITLE NOHDR 45T TIRCNTL-STATE-TAX-ID-FSB
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ************************
        //* * PRINT COUNTRY TABLE **
        //* ************************
        pnd_Tbl3_Ctr.setValue(0);                                                                                                                                         //Natural: ASSIGN #TBL3-CTR := 0
        ldaTwrl9802.getVw_tircntl_Country_Code_Tbl_View_Vi().startDatabaseRead                                                                                            //Natural: READ TIRCNTL-COUNTRY-CODE-TBL-VIEW-VI BY TIRCNTL-NBR-YEAR-UNQ-CDE
        (
        "READ03",
        new Oc[] { new Oc("TIRCNTL_NBR_YEAR_UNQ_CDE", "ASC") }
        );
        READ03:
        while (condition(ldaTwrl9802.getVw_tircntl_Country_Code_Tbl_View_Vi().readNextRow("READ03")))
        {
            if (condition(ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Tbl_Nbr().equals(3)))                                                                   //Natural: IF TIRCNTL-TBL-NBR = 3
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tbl3_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TBL3-CTR
            getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Tbl_Nbr(),new         //Natural: WRITE ( 03 ) NOTITLE NOHDR 01T TIRCNTL-TBL-NBR 05T TIRCNTL-TAX-YEAR 11T TIRCNTL-SEQ-NBR 18T TIRCNTL-CNTRY-ALPHA-CODE 25T TIRCNTL-CNTRY-ABBR 31T TIRCNTL-CNTRY-FULL-NAME 70T TIRCNTL-CNTRY-TREATY-IND 75T TIRCNTL-CNTRY-TAX-PERIOD 84T TIRCNTL-CNTRY-TAX-NON-PERIOD 93T TIRCNTL-CNTRY-TAX-DPI 102T TIRCNTL-CNTRY-TAX-RTB 112T TIRCNTL-CNTRY-ACTIVE-CDE 116T TIRCNTL-CNTRY-START-DTE
                TabSetting(5),ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Tax_Year(),new TabSetting(11),ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Seq_Nbr(),new 
                TabSetting(18),ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Alpha_Code(),new TabSetting(25),ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Abbr(),new 
                TabSetting(31),ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Full_Name(),new TabSetting(70),ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Treaty_Ind(),new 
                TabSetting(75),ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Tax_Period(),new TabSetting(84),ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Tax_Non_Period(),new 
                TabSetting(93),ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Tax_Dpi(),new TabSetting(102),ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Tax_Rtb(),new 
                TabSetting(112),ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Active_Cde(),new TabSetting(116),ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Start_Dte());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(17),"Unique:",ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Unique_Cde(),new  //Natural: WRITE ( 03 ) NOTITLE NOHDR 17T 'Unique:' TIRCNTL-CNTRY-UNIQUE-CDE 1X '"PA PP":' TIRCNTL-CNTRY-PA-PP ( EM = Z9.999 ) 1X '"PA NP":' TIRCNTL-CNTRY-PA-NP ( EM = Z9.999 ) 1X '"IRA PP":' TIRCNTL-CNTRY-IRA-PP ( EM = Z9.999 ) 1X '"IRA NP":' TIRCNTL-CNTRY-IRA-NP ( EM = Z9.999 ) 1X 'DPI:' TIRCNTL-CNTRY-DPI-NP-SP-IND 1X 'DPI Rate:' TIRCNTL-CNTRY-DPI-NP-SP-RATE ( EM = Z9.999 ) 1X 'Early Dist:' TIRCNTL-CNTRY-EARLY-DISTRIB-IND
                ColumnSpacing(1),"'PA PP':",ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Pa_Pp(), new ReportEditMask ("Z9.999"),new ColumnSpacing(1),"'PA NP':",ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Pa_Np(), 
                new ReportEditMask ("Z9.999"),new ColumnSpacing(1),"'IRA PP':",ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Ira_Pp(), new 
                ReportEditMask ("Z9.999"),new ColumnSpacing(1),"'IRA NP':",ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Ira_Np(), new ReportEditMask 
                ("Z9.999"),new ColumnSpacing(1),"DPI:",ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Dpi_Np_Sp_Ind(),new ColumnSpacing(1),"DPI Rate:",ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Dpi_Np_Sp_Rate(), 
                new ReportEditMask ("Z9.999"),new ColumnSpacing(1),"Early Dist:",ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Early_Distrib_Ind());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Form_Name().notEquals(" ")))                                                      //Natural: IF TIRCNTL-CNTRY-FORM-NAME NE ' '
            {
                getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(17),"Form:",ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Form_Name()); //Natural: WRITE ( 03 ) NOTITLE NOHDR 17T 'Form:' TIRCNTL-CNTRY-FORM-NAME
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* **********************
        //* * PRINT FORMS TABLE **
        //* **********************
        pnd_Tbl4_Ctr.setValue(0);                                                                                                                                         //Natural: ASSIGN #TBL4-CTR := 0
        ldaTwrl9805.getVw_tircntl_Reporting_Tbl_View_View().startDatabaseRead                                                                                             //Natural: READ TIRCNTL-REPORTING-TBL-VIEW-VIEW BY TIRCNTL-NBR-YEAR-FORM-NAME
        (
        "READ04",
        new Oc[] { new Oc("TIRCNTL_NBR_YEAR_FORM_NAME", "ASC") }
        );
        READ04:
        while (condition(ldaTwrl9805.getVw_tircntl_Reporting_Tbl_View_View().readNextRow("READ04")))
        {
            if (condition(ldaTwrl9805.getTircntl_Reporting_Tbl_View_View_Tircntl_Tbl_Nbr().equals(4)))                                                                    //Natural: IF TIRCNTL-TBL-NBR = 4
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tbl4_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TBL4-CTR
            getReports().write(4, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),ldaTwrl9805.getTircntl_Reporting_Tbl_View_View_Tircntl_Tbl_Nbr(),new          //Natural: WRITE ( 04 ) NOTITLE NOHDR 01T TIRCNTL-TBL-NBR 05T TIRCNTL-TAX-YEAR 11T TIRCNTL-SEQ-NBR 17T TIRCNTL-RPT-FORM-NAME 25T TIRCNTL-RPT-MASS-MAIL-DTE 35T TIRCNTL-RPT-2ND-MASS-MAIL-DTE 45T TIRCNTL-FUTURE-USE-DATE 56T TIRCNTL-EFF-DTE 67T TIRCNTL-RPT-DTE ( 1 ) ( EM = YYYYMMDD )
                TabSetting(5),ldaTwrl9805.getTircntl_Reporting_Tbl_View_View_Tircntl_Tax_Year(),new TabSetting(11),ldaTwrl9805.getTircntl_Reporting_Tbl_View_View_Tircntl_Seq_Nbr(),new 
                TabSetting(17),ldaTwrl9805.getTircntl_Reporting_Tbl_View_View_Tircntl_Rpt_Form_Name(),new TabSetting(25),ldaTwrl9805.getTircntl_Reporting_Tbl_View_View_Tircntl_Rpt_Mass_Mail_Dte(),new 
                TabSetting(35),ldaTwrl9805.getTircntl_Reporting_Tbl_View_View_Tircntl_Rpt_2nd_Mass_Mail_Dte(),new TabSetting(45),ldaTwrl9805.getTircntl_Reporting_Tbl_View_View_Tircntl_Future_Use_Date(),new 
                TabSetting(56),ldaTwrl9805.getTircntl_Reporting_Tbl_View_View_Tircntl_Eff_Dte(),new TabSetting(67),ldaTwrl9805.getTircntl_Reporting_Tbl_View_View_Tircntl_Rpt_Dte().getValue(1), 
                new ReportEditMask ("YYYYMMDD"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ***********************
        //* * PRINT FEEDER TABLE **
        //* ***********************
        pnd_Tbl5_Ctr.setValue(0);                                                                                                                                         //Natural: ASSIGN #TBL5-CTR := 0
        ldaTwrl9804.getVw_tircntl_Feeder_Sys_Tbl_View_View().startDatabaseRead                                                                                            //Natural: READ TIRCNTL-FEEDER-SYS-TBL-VIEW-VIEW BY TIRCNTL-5-Y-SC-CO-TO-FRM-SP
        (
        "READ05",
        new Oc[] { new Oc("TIRCNTL_5_Y_SC_CO_TO_FRM_SP", "ASC") }
        );
        READ05:
        while (condition(ldaTwrl9804.getVw_tircntl_Feeder_Sys_Tbl_View_View().readNextRow("READ05")))
        {
            if (condition(ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Tbl_Nbr().equals(5)))                                                                   //Natural: IF TIRCNTL-TBL-NBR = 5
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tbl5_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TBL5-CTR
            getReports().write(5, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Tbl_Nbr(),new         //Natural: WRITE ( 05 ) NOTITLE NOHDR 01T TIRCNTL-TBL-NBR 04T TIRCNTL-TAX-YEAR 10T TIRCNTL-SEQ-NBR 16T TIRCNTL-RPT-SOURCE-CODE 19T TIRCNTL-COMPANY-CDE 21T TIRCNTL-FRM-INTRFCE-DTE ( EM = ZZZZZZZZ ) 31T TIRCNTL-TO-INTRFCE-DTE ( EM = ZZZZZZZZ ) 41T TIRCNTL-INPUT-RECS-TOT ( EM = ZZZZ,ZZZ ) 52T TIRCNTL-INPUT-GROSS-AMT 68T TIRCNTL-INPUT-FED-WTHLDG-AMT 84T TIRCNTL-INPUT-STATE-WTHLDG-AMT 100T TIRCNTL-INPUT-LOCAL-WTHLDG-AMT 116T TIRCNTL-RPT-UPDATE-DTE-TIME
                TabSetting(4),ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Tax_Year(),new TabSetting(10),ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Seq_Nbr(),new 
                TabSetting(16),ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Rpt_Source_Code(),new TabSetting(19),ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Company_Cde(),new 
                TabSetting(21),ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Frm_Intrfce_Dte(), new ReportEditMask ("ZZZZZZZZ"),new TabSetting(31),ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_To_Intrfce_Dte(), 
                new ReportEditMask ("ZZZZZZZZ"),new TabSetting(41),ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Input_Recs_Tot(), new ReportEditMask 
                ("ZZZZ,ZZZ"),new TabSetting(52),ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Input_Gross_Amt(),new TabSetting(68),ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Input_Fed_Wthldg_Amt(),new 
                TabSetting(84),ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Input_State_Wthldg_Amt(),new TabSetting(100),ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Input_Local_Wthldg_Amt(),new 
                TabSetting(116),ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Rpt_Update_Dte_Time());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(5, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),"Desc :",ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Rpt_Source_Code_Desc(),new  //Natural: WRITE ( 05 ) NOTITLE NOHDR 01T 'Desc :' TIRCNTL-RPT-SOURCE-CODE-DESC 52T TIRCNTL-INPUT-IVT-AMT 68T TIRCNTL-INPUT-INT-AMT 84T TIRCNTL-INPUT-NRA-AMT 100T TIRCNTL-INPUT-CAN-AMT
                TabSetting(52),ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Input_Ivt_Amt(),new TabSetting(68),ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Input_Int_Amt(),new 
                TabSetting(84),ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Input_Nra_Amt(),new TabSetting(100),ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Input_Can_Amt());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ************************
        //* * PRINT COMPANY TABLE **
        //* ************************
        pnd_Tbl6_Ctr.setValue(0);                                                                                                                                         //Natural: ASSIGN #TBL6-CTR := 0
        ldaTwrl9801.getVw_tircntl_Company_Info_Tbl_View_Vi().startDatabaseRead                                                                                            //Natural: READ TIRCNTL-COMPANY-INFO-TBL-VIEW-VI BY TIRCNTL-NBR-YEAR-NAME
        (
        "READ06",
        new Oc[] { new Oc("TIRCNTL_NBR_YEAR_NAME", "ASC") }
        );
        READ06:
        while (condition(ldaTwrl9801.getVw_tircntl_Company_Info_Tbl_View_Vi().readNextRow("READ06")))
        {
            if (condition(ldaTwrl9801.getTircntl_Company_Info_Tbl_View_Vi_Tircntl_Tbl_Nbr().equals(6)))                                                                   //Natural: IF TIRCNTL-TBL-NBR = 6
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tbl6_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TBL6-CTR
            getReports().write(6, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),ldaTwrl9801.getTircntl_Company_Info_Tbl_View_Vi_Tircntl_Tbl_Nbr(),new         //Natural: WRITE ( 06 ) NOTITLE NOHDR 01T TIRCNTL-TBL-NBR 05T TIRCNTL-TAX-YEAR 11T TIRCNTL-SEQ-NBR 20T TIRCNTL-COMPANY-CODE 27T TIRCNTL-COMPANY-NAME 35T TIRCNTL-COMPANY-TAX-ID
                TabSetting(5),ldaTwrl9801.getTircntl_Company_Info_Tbl_View_Vi_Tircntl_Tax_Year(),new TabSetting(11),ldaTwrl9801.getTircntl_Company_Info_Tbl_View_Vi_Tircntl_Seq_Nbr(),new 
                TabSetting(20),ldaTwrl9801.getTircntl_Company_Info_Tbl_View_Vi_Tircntl_Company_Code(),new TabSetting(27),ldaTwrl9801.getTircntl_Company_Info_Tbl_View_Vi_Tircntl_Company_Name(),new 
                TabSetting(35),ldaTwrl9801.getTircntl_Company_Info_Tbl_View_Vi_Tircntl_Company_Tax_Id());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ******************************
        //* * PRINT ERROR MESSAGE TABLE **
        //* ******************************
        pnd_Tbl7_Ctr.setValue(0);                                                                                                                                         //Natural: ASSIGN #TBL7-CTR := 0
        ldaTwrl9807.getVw_tircntl_Error_Msg_Tbl_View_View().startDatabaseRead                                                                                             //Natural: READ TIRCNTL-ERROR-MSG-TBL-VIEW-VIEW WITH TIRCNTL-NBR-YEAR-SEQ = 1
        (
        "READ07",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_SEQ", ">=", 1, WcType.BY) },
        new Oc[] { new Oc("TIRCNTL_NBR_YEAR_SEQ", "ASC") }
        );
        READ07:
        while (condition(ldaTwrl9807.getVw_tircntl_Error_Msg_Tbl_View_View().readNextRow("READ07")))
        {
            if (condition(ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Tbl_Nbr().equals(7)))                                                                    //Natural: IF TIRCNTL-TBL-NBR = 7
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tbl7_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TBL7-CTR
            getReports().write(7, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Tbl_Nbr(),new          //Natural: WRITE ( 07 ) NOTITLE NOHDR 01T TIRCNTL-TBL-NBR 05T TIRCNTL-TAX-YEAR 11T TIRCNTL-SEQ-NBR 19T TIRCNTL-ERROR-TYPE 24T TIRCNTL-ERROR-DESCR
                TabSetting(5),ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Tax_Year(),new TabSetting(11),ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Seq_Nbr(),new 
                TabSetting(19),ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Type(),new TabSetting(24),ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 01 )
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 02 )
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 03 )
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 04 )
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 05 )
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 06 )
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 07 )
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(51),"Distribution Code Table Report",new      //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 51T 'Distribution Code Table Report' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                    getReports().skip(1, 2);                                                                                                                              //Natural: SKIP ( 01 ) 2 LINES
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TBL",new TabSetting(6),"Tax ",new TabSetting(12),"Seq",new TabSetting(17),"Refer",new   //Natural: WRITE ( 01 ) NOTITLE 01T 'TBL' 06T 'Tax ' 12T 'Seq' 17T 'Refer' 24T 'Pymnt' 31T 'Settl' 80T 'Over 59' 89T 'Distribution' 103T 'Distribution' 117T 'Payment '
                        TabSetting(24),"Pymnt",new TabSetting(31),"Settl",new TabSetting(80),"Over 59",new TabSetting(89),"Distribution",new TabSetting(103),"Distribution",new 
                        TabSetting(117),"Payment ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"NBR",new TabSetting(6),"Year",new TabSetting(12),"No.",new TabSetting(17),"Type ",new   //Natural: WRITE ( 01 ) NOTITLE 01T 'NBR' 06T 'Year' 12T 'No.' 17T 'Type ' 24T 'Type ' 31T 'Type ' 38T 'Payment / Settlement Description' 80T '  IND  ' 89T '    Code    ' 103T '   Method   ' 117T 'Category'
                        TabSetting(24),"Type ",new TabSetting(31),"Type ",new TabSetting(38),"Payment / Settlement Description",new TabSetting(80),"  IND  ",new 
                        TabSetting(89),"    Code    ",new TabSetting(103),"   Method   ",new TabSetting(117),"Category");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"===",new TabSetting(6),"====",new TabSetting(12),"===",new TabSetting(17),"=====",new   //Natural: WRITE ( 01 ) NOTITLE 01T '===' 06T '====' 12T '===' 17T '=====' 24T '=====' 31T '=====' 38T '========================================' 80T '=======' 89T '============' 103T '============' 117T '========'
                        TabSetting(24),"=====",new TabSetting(31),"=====",new TabSetting(38),"========================================",new TabSetting(80),"=======",new 
                        TabSetting(89),"============",new TabSetting(103),"============",new TabSetting(117),"========");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(2, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 02 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(2, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(54),"State Code Table Report",new             //Natural: WRITE ( 02 ) NOTITLE *INIT-USER '-' *PROGRAM 54T 'State Code Table Report' 120T 'REPORT: RPT2'
                        TabSetting(120),"REPORT: RPT2");
                    getReports().skip(2, 2);                                                                                                                              //Natural: SKIP ( 02 ) 2 LINES
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(45)," TIAA Tax Id  ");                                                                      //Natural: WRITE ( 02 ) NOTITLE 45T ' TIAA Tax Id  '
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(45)," CREF Tax Id  ");                                                                      //Natural: WRITE ( 02 ) NOTITLE 45T ' CREF Tax Id  '
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(45)," LIFE Tax Id  ");                                                                      //Natural: WRITE ( 02 ) NOTITLE 45T ' LIFE Tax Id  '
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"Tbl",new TabSetting(5),"Tax ",new TabSetting(10),"Seq",new TabSetting(14),"Old ",new    //Natural: WRITE ( 02 ) NOTITLE 01T 'Tbl' 05T 'Tax ' 10T 'Seq' 14T 'Old ' 19T 'Alpha' 25T '                   ' 45T ' TCII Tax Id  ' 67T 'Comb' 73T 'Comb Ind' 83T 'State' 90T 'Media' 97T 'State' 104T 'State   ' 114T 'State   ' 124T ' Update '
                        TabSetting(19),"Alpha",new TabSetting(25),"                   ",new TabSetting(45)," TCII Tax Id  ",new TabSetting(67),"Comb",new 
                        TabSetting(73),"Comb Ind",new TabSetting(83),"State",new TabSetting(90),"Media",new TabSetting(97),"State",new TabSetting(104),"State   ",new 
                        TabSetting(114),"State   ",new TabSetting(124)," Update ");
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"Nbr",new TabSetting(5),"Year",new TabSetting(10),"No.",new TabSetting(14),"Code",new    //Natural: WRITE ( 02 ) NOTITLE 01T 'Nbr' 05T 'Year' 10T 'No.' 14T 'Code' 19T 'Code ' 25T 'State Full Name    ' 45T ' TRST Tax Id  ' 67T 'Ind ' 73T 'Updt Dte' 83T ' Ind ' 90T 'Code ' 97T 'Whhld' 104T 'Entr Dte' 114T 'Updt Dte' 124T '  User  '
                        TabSetting(19),"Code ",new TabSetting(25),"State Full Name    ",new TabSetting(45)," TRST Tax Id  ",new TabSetting(67),"Ind ",new 
                        TabSetting(73),"Updt Dte",new TabSetting(83)," Ind ",new TabSetting(90),"Code ",new TabSetting(97),"Whhld",new TabSetting(104),"Entr Dte",new 
                        TabSetting(114),"Updt Dte",new TabSetting(124),"  User  ");
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"===",new TabSetting(5),"====",new TabSetting(10),"===",new TabSetting(14),"====",new    //Natural: WRITE ( 02 ) NOTITLE 01T '===' 05T '====' 10T '===' 14T '====' 19T '=====' 25T '===================' 45T '====================' 67T '====' 73T '========' 83T '=====' 90T '=====' 97T '=====' 104T '========' 114T '========' 124T '========'
                        TabSetting(19),"=====",new TabSetting(25),"===================",new TabSetting(45),"====================",new TabSetting(67),"====",new 
                        TabSetting(73),"========",new TabSetting(83),"=====",new TabSetting(90),"=====",new TabSetting(97),"=====",new TabSetting(104),"========",new 
                        TabSetting(114),"========",new TabSetting(124),"========");
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 02 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(3, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 03 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(3, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"Country Code Table Report",new           //Natural: WRITE ( 03 ) NOTITLE *INIT-USER '-' *PROGRAM 53T 'Country Code Table Report' 120T 'REPORT: RPT3'
                        TabSetting(120),"REPORT: RPT3");
                    getReports().skip(3, 2);                                                                                                                              //Natural: SKIP ( 03 ) 2 LINES
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),"TBL",new TabSetting(6),"Tax ",new TabSetting(12),"Seq",new TabSetting(17),"CNTRY",new   //Natural: WRITE ( 03 ) NOTITLE 01T 'TBL' 06T 'Tax ' 12T 'Seq' 17T 'CNTRY' 24T 'CNTRY' 31T '                                   ' 68T 'TRETY' 75T ' Tax   ' 84T 'No Tax ' 93T '  Tax  ' 102T '  Tax  ' 111T 'ACTV' 117T ' Start  '
                        TabSetting(24),"CNTRY",new TabSetting(31),"                                   ",new TabSetting(68),"TRETY",new TabSetting(75)," Tax   ",new 
                        TabSetting(84),"No Tax ",new TabSetting(93),"  Tax  ",new TabSetting(102),"  Tax  ",new TabSetting(111),"ACTV",new TabSetting(117),
                        " Start  ");
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),"NBR",new TabSetting(6),"Year",new TabSetting(12),"No.",new TabSetting(17),"Alpha",new   //Natural: WRITE ( 03 ) NOTITLE 01T 'NBR' 06T 'Year' 12T 'No.' 17T 'Alpha' 24T 'ABBRV' 31T 'Country Full Name                  ' 68T ' IND ' 75T 'Period ' 84T 'Period ' 93T '  DPI  ' 102T '  RTB  ' 111T 'IND ' 117T '  Date  '
                        TabSetting(24),"ABBRV",new TabSetting(31),"Country Full Name                  ",new TabSetting(68)," IND ",new TabSetting(75),"Period ",new 
                        TabSetting(84),"Period ",new TabSetting(93),"  DPI  ",new TabSetting(102),"  RTB  ",new TabSetting(111),"IND ",new TabSetting(117),
                        "  Date  ");
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),"===",new TabSetting(6),"====",new TabSetting(12),"===",new TabSetting(17),"=====",new   //Natural: WRITE ( 03 ) NOTITLE 01T '===' 06T '====' 12T '===' 17T '=====' 24T '=====' 31T '===================================' 68T '=====' 75T '=======' 84T '=======' 93T '=======' 102T '=======' 111T '====' 117T '========'
                        TabSetting(24),"=====",new TabSetting(31),"===================================",new TabSetting(68),"=====",new TabSetting(75),"=======",new 
                        TabSetting(84),"=======",new TabSetting(93),"=======",new TabSetting(102),"=======",new TabSetting(111),"====",new TabSetting(117),
                        "========");
                    getReports().skip(3, 1);                                                                                                                              //Natural: SKIP ( 03 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(4, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 04 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(4), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(4, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(55),"Reporting Table Report",new              //Natural: WRITE ( 04 ) NOTITLE *INIT-USER '-' *PROGRAM 55T 'Reporting Table Report' 120T 'REPORT: RPT4'
                        TabSetting(120),"REPORT: RPT4");
                    getReports().skip(4, 2);                                                                                                                              //Natural: SKIP ( 04 ) 2 LINES
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),"TBL",new TabSetting(6),"Tax ",new TabSetting(12),"Seq",new TabSetting(17)," Form  ",new //Natural: WRITE ( 04 ) NOTITLE 01T 'TBL' 06T 'Tax ' 12T 'Seq' 17T ' Form  ' 26T '  Mass  ' 36T '2nd Mass' 46T ' Future ' 56T 'Effective' 67T 'Original'
                        TabSetting(26),"  Mass  ",new TabSetting(36),"2nd Mass",new TabSetting(46)," Future ",new TabSetting(56),"Effective",new TabSetting(67),
                        "Original");
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),"NBR",new TabSetting(6),"Year",new TabSetting(12),"No.",new TabSetting(17)," Name  ",new //Natural: WRITE ( 04 ) NOTITLE 01T 'NBR' 06T 'Year' 12T 'No.' 17T ' Name  ' 26T 'Mailing ' 36T 'Mailing ' 46T '  Date  ' 56T '  Date   ' 67T '  Date  '
                        TabSetting(26),"Mailing ",new TabSetting(36),"Mailing ",new TabSetting(46),"  Date  ",new TabSetting(56),"  Date   ",new TabSetting(67),
                        "  Date  ");
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),"===",new TabSetting(6),"====",new TabSetting(12),"===",new TabSetting(17),"=======",new //Natural: WRITE ( 04 ) NOTITLE 01T '===' 06T '====' 12T '===' 17T '=======' 26T '========' 36T '========' 46T '========' 56T '=========' 67T '========'
                        TabSetting(26),"========",new TabSetting(36),"========",new TabSetting(46),"========",new TabSetting(56),"=========",new TabSetting(67),
                        "========");
                    getReports().skip(4, 1);                                                                                                                              //Natural: SKIP ( 04 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt5 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(5, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 05 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 05 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(5), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(5, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"Feeder System Table Report",new          //Natural: WRITE ( 05 ) NOTITLE *INIT-USER '-' *PROGRAM 53T 'Feeder System Table Report' 120T 'REPORT: RPT5'
                        TabSetting(120),"REPORT: RPT5");
                    getReports().skip(5, 2);                                                                                                                              //Natural: SKIP ( 05 ) 2 LINES
                    getReports().write(5, ReportOption.NOTITLE,new TabSetting(1),"TBL",new TabSetting(5),"Tax ",new TabSetting(11),"Seq",new TabSetting(16),"SR",new      //Natural: WRITE ( 05 ) NOTITLE 01T 'TBL' 05T 'Tax ' 11T 'Seq' 16T 'SR' 19T 'C' 21T 'intrface' 31T '   To   ' 41T 'Records ' 52T ' Gross Amount  ' 68T ' Fed Wtlhd Amt ' 84T ' Sta Wthld Amt ' 100T ' Loc Wthld Amt ' 117T '  Update Date  '
                        TabSetting(19),"C",new TabSetting(21),"intrface",new TabSetting(31),"   To   ",new TabSetting(41),"Records ",new TabSetting(52)," Gross Amount  ",new 
                        TabSetting(68)," Fed Wtlhd Amt ",new TabSetting(84)," Sta Wthld Amt ",new TabSetting(100)," Loc Wthld Amt ",new TabSetting(117),
                        "  Update Date  ");
                    getReports().write(5, ReportOption.NOTITLE,new TabSetting(1),"NBR",new TabSetting(5),"Year",new TabSetting(11),"No.",new TabSetting(16),"CE",new      //Natural: WRITE ( 05 ) NOTITLE 01T 'NBR' 05T 'Year' 11T 'No.' 16T 'CE' 19T 'O' 21T '  Date  ' 31T '  Date  ' 41T 'Procesed' 52T '  IVC Amount   ' 68T '  INT Amount   ' 84T '  NRA Amount   ' 100T '  CAN Amount   ' 117T '    and  Time  '
                        TabSetting(19),"O",new TabSetting(21),"  Date  ",new TabSetting(31),"  Date  ",new TabSetting(41),"Procesed",new TabSetting(52),"  IVC Amount   ",new 
                        TabSetting(68),"  INT Amount   ",new TabSetting(84),"  NRA Amount   ",new TabSetting(100),"  CAN Amount   ",new TabSetting(117),
                        "    and  Time  ");
                    getReports().write(5, ReportOption.NOTITLE,new TabSetting(1),"===",new TabSetting(5),"====",new TabSetting(11),"===",new TabSetting(16),"==",new      //Natural: WRITE ( 05 ) NOTITLE 01T '===' 05T '====' 11T '===' 16T '==' 19T '=' 21T '========' 31T '========' 41T '========' 52T '===============' 68T '===============' 84T '===============' 100T '===============' 117T '==============='
                        TabSetting(19),"=",new TabSetting(21),"========",new TabSetting(31),"========",new TabSetting(41),"========",new TabSetting(52),"===============",new 
                        TabSetting(68),"===============",new TabSetting(84),"===============",new TabSetting(100),"===============",new TabSetting(117),
                        "===============");
                    getReports().skip(5, 1);                                                                                                                              //Natural: SKIP ( 05 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt6 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(6, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 06 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 06 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(6), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(6, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(56),"Company Table Report",new                //Natural: WRITE ( 06 ) NOTITLE *INIT-USER '-' *PROGRAM 56T 'Company Table Report' 120T 'REPORT: RPT6'
                        TabSetting(120),"REPORT: RPT6");
                    getReports().skip(6, 2);                                                                                                                              //Natural: SKIP ( 06 ) 2 LINES
                    getReports().write(6, ReportOption.NOTITLE,new TabSetting(1),"TBL",new TabSetting(6),"Tax ",new TabSetting(12),"Seq",new TabSetting(17),"Company",new //Natural: WRITE ( 06 ) NOTITLE 01T 'TBL' 06T 'Tax ' 12T 'Seq' 17T 'Company' 26T 'Company' 35T '   Company    '
                        TabSetting(26),"Company",new TabSetting(35),"   Company    ");
                    getReports().write(6, ReportOption.NOTITLE,new TabSetting(1),"NBR",new TabSetting(6),"Year",new TabSetting(12),"No.",new TabSetting(17)," Code  ",new //Natural: WRITE ( 06 ) NOTITLE 01T 'NBR' 06T 'Year' 12T 'No.' 17T ' Code  ' 26T ' Name  ' 35T '   Tax ID     '
                        TabSetting(26)," Name  ",new TabSetting(35),"   Tax ID     ");
                    getReports().write(6, ReportOption.NOTITLE,new TabSetting(1),"===",new TabSetting(6),"====",new TabSetting(12),"===",new TabSetting(17),"=======",new //Natural: WRITE ( 06 ) NOTITLE 01T '===' 06T '====' 12T '===' 17T '=======' 26T '=======' 35T '=============='
                        TabSetting(26),"=======",new TabSetting(35),"==============");
                    getReports().skip(6, 1);                                                                                                                              //Natural: SKIP ( 06 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt7 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(7, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 07 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 07 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(7), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(7, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"Error Messages Table Report",new         //Natural: WRITE ( 07 ) NOTITLE *INIT-USER '-' *PROGRAM 53T 'Error Messages Table Report' 120T 'REPORT: RPT7'
                        TabSetting(120),"REPORT: RPT7");
                    getReports().skip(7, 2);                                                                                                                              //Natural: SKIP ( 07 ) 2 LINES
                    getReports().write(7, ReportOption.NOTITLE,new TabSetting(1),"TBL",new TabSetting(6),"Tax ",new TabSetting(12),"Seq",new TabSetting(17),"Error",new   //Natural: WRITE ( 07 ) NOTITLE 01T 'TBL' 06T 'Tax ' 12T 'Seq' 17T 'Error' 24T '                                        '
                        TabSetting(24),"                                        ");
                    getReports().write(7, ReportOption.NOTITLE,new TabSetting(1),"NBR",new TabSetting(6),"Year",new TabSetting(12),"No.",new TabSetting(17),"Type ",new   //Natural: WRITE ( 07 ) NOTITLE 01T 'NBR' 06T 'Year' 12T 'No.' 17T 'Type ' 24T 'Error Description                       '
                        TabSetting(24),"Error Description                       ");
                    getReports().write(7, ReportOption.NOTITLE,new TabSetting(1),"===",new TabSetting(6),"====",new TabSetting(12),"===",new TabSetting(17),"=====",new   //Natural: WRITE ( 07 ) NOTITLE 01T '===' 06T '====' 12T '===' 17T '=====' 24T '========================================'
                        TabSetting(24),"========================================");
                    getReports().skip(7, 1);                                                                                                                              //Natural: SKIP ( 07 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
        Global.format(2, "PS=60 LS=133");
        Global.format(3, "PS=60 LS=133");
        Global.format(4, "PS=60 LS=133");
        Global.format(5, "PS=60 LS=133");
        Global.format(6, "PS=60 LS=133");
        Global.format(7, "PS=60 LS=133");
    }
}
