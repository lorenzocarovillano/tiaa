/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:39:28 PM
**        * FROM NATURAL PROGRAM : Twrp4500
************************************************************
**        * FILE NAME            : Twrp4500.java
**        * CLASS NAME           : Twrp4500
**        * INSTANCE NAME        : Twrp4500
************************************************************
************************************************************************
*
* PROGRAM  : TWRP4500  (IRS - 5498  ORIGINAL REPORTING).
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : PRODUCE FORM 5498 IRS REJECTED DETAIL REPORT.
* CREATED  : 06 / 20 / 2000.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM PRODUCES A REPORT OF ALL REJECTED 5498 RECORDS
*            NOT REPORTED TO THE 'IRS' (ORIGINAL REPORTING).
* HISTORY  :
*     11/10/2005 MS RECOMPILED - CHANGE IN TWRL5001
*     11/19/02 - MN - RECOMPILED DUE TO FIELDS ADDED TO TWRL5001
*     11/29/05 - BK - RECOMPILED DUE TO FIELDS ADDED TO TWRL442A
*                     UNUSED CODE REMOVED
*     12/01/05 - SK - RECOMPILE FOR CHANGES IN TWRL442A
*     18-11-20 : PALDE RESTOW FOR IRS REPORTING 2020  CHANGES
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp4500 extends BLNatBase
{
    // Data Areas
    private LdaTwrl442a ldaTwrl442a;
    private LdaTwrl5001 ldaTwrl5001;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_F_Name;
    private DbsField j;
    private DbsField k;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl442a = new LdaTwrl442a();
        registerRecord(ldaTwrl442a);
        ldaTwrl5001 = new LdaTwrl5001();
        registerRecord(ldaTwrl5001);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Name = localVariables.newFieldInRecord("pnd_F_Name", "#F-NAME", FieldType.STRING, 40);
        j = localVariables.newFieldInRecord("j", "J", FieldType.PACKED_DECIMAL, 4);
        k = localVariables.newFieldInRecord("k", "K", FieldType.PACKED_DECIMAL, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl442a.initializeValues();
        ldaTwrl5001.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp4500() throws Exception
    {
        super("Twrp4500");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP4500", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        RD1:                                                                                                                                                              //Natural: READ WORK FILE 01 RECORD #FORM
        while (condition(getWorkFiles().read(1, ldaTwrl442a.getPnd_Form())))
        {
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            pnd_F_Name.setValue(DbsUtil.compress(ldaTwrl442a.getPnd_Form_Pnd_F_Part_First_Nme(), ldaTwrl442a.getPnd_Form_Pnd_F_Part_Mddle_Nme(), ldaTwrl442a.getPnd_Form_Pnd_F_Part_Last_Nme())); //Natural: COMPRESS #F-PART-FIRST-NME #F-PART-MDDLE-NME #F-PART-LAST-NME INTO #F-NAME
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),ldaTwrl442a.getPnd_Form_Pnd_F_Tin(), new ReportEditMask ("XXX-XX-XXXXX"),new  //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T #F-TIN ( EM = XXX-XX-XXXXX ) 15T #F-TAX-ID-TYPE 18T #F-CONTRACT-NBR 28T #F-PAYEE-CDE 34T #F-IRA-TYPE 38T #F-COMPANY-CDE 40T #F-NAME
                TabSetting(15),ldaTwrl442a.getPnd_Form_Pnd_F_Tax_Id_Type(),new TabSetting(18),ldaTwrl442a.getPnd_Form_Pnd_F_Contract_Nbr(),new TabSetting(28),ldaTwrl442a.getPnd_Form_Pnd_F_Payee_Cde(),new 
                TabSetting(34),ldaTwrl442a.getPnd_Form_Pnd_F_Ira_Type(),new TabSetting(38),ldaTwrl442a.getPnd_Form_Pnd_F_Company_Cde(),new TabSetting(40),
                pnd_F_Name);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR01:                                                                                                                                                        //Natural: FOR J = 1 TO #F-SYS-ERR-COUNT
            for (j.setValue(1); condition(j.lessOrEqual(ldaTwrl442a.getPnd_Form_Pnd_F_Sys_Err_Count())); j.nadd(1))
            {
                k.setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Sys_Err().getValue(j));                                                                                          //Natural: ASSIGN K := #F-SYS-ERR ( J )
                getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),ldaTwrl442a.getPnd_Form_Pnd_F_Sys_Err().getValue(j),ldaTwrl5001.getPnd_Twrl5001_Pnd_Err_Desc().getValue(k.getInt() + 1)); //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T #F-SYS-ERR ( J ) #ERR-DESC ( K )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        RD1_Exit:
        if (Global.isEscape()) return;
        //* *------
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *------------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 01 )
        //* *-------                                                                                                                                                      //Natural: ON ERROR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        getReports().write(0, NEWLINE,new TabSetting(1),"Number Of Rejected Records Found..............",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                 //Natural: WRITE ( 00 ) / 01T 'Number Of Rejected Records Found..............' #READ-CTR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"Number Of Rejected Records Found..............",pnd_Read_Ctr, new ReportEditMask            //Natural: WRITE ( 01 ) / 01T 'Number Of Rejected Records Found..............' #READ-CTR
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(48),"IRS 5498 Rejected Forms Detail Report",new  //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 48T 'IRS 5498 Rejected Forms Detail Report' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"Tax Year......",ldaTwrl442a.getPnd_Form_Pnd_F_Tax_Year());                              //Natural: WRITE ( 01 ) NOTITLE 01T 'Tax Year......' #F-TAX-YEAR
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1 LINES
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(14),"ID ",new TabSetting(33),"IRA ",new TabSetting(38),"C");                                //Natural: WRITE ( 01 ) NOTITLE 14T 'ID ' 33T 'IRA ' 38T 'C'
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"   Tax ID   ",new TabSetting(14),"Typ",new TabSetting(18),"Contract",new                //Natural: WRITE ( 01 ) NOTITLE 01T '   Tax ID   ' 14T 'Typ' 18T 'Contract' 27T 'Payee' 33T 'Type' 38T 'C' 40T '       Name Of contributor              '
                        TabSetting(27),"Payee",new TabSetting(33),"Type",new TabSetting(38),"C",new TabSetting(40),"       Name Of contributor              ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"============",new TabSetting(14),"===",new TabSetting(18),"========",new                //Natural: WRITE ( 01 ) NOTITLE 01T '============' 14T '===' 18T '========' 27T '=====' 33T '====' 38T '=' 40T '========================================'
                        TabSetting(27),"=====",new TabSetting(33),"====",new TabSetting(38),"=",new TabSetting(40),"========================================");
                    //*  SKIP (01) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
        sub_Error_Display_Start();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
        sub_Error_Display_End();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
    }
}
