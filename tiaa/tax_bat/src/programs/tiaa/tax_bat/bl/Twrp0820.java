/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:31:33 PM
**        * FROM NATURAL PROGRAM : Twrp0820
************************************************************
**        * FILE NAME            : Twrp0820.java
**        * CLASS NAME           : Twrp0820
**        * INSTANCE NAME        : Twrp0820
************************************************************
***********************************************************************
* PROGRAM  : TWRP0820
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : EDIT / UPDATE THE PAYMENTS DATA BASE FILE.
* CREATED  : 10 / 23 / 1998.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM EDITS & UPDATES THE PAYMENTS DATA BASE FILE.
* HISTORY  :
*    11/10/2014 - RC - BYPASS FATCA PAYMENT RECORDS            /* RC08
*    12/02/2013 - RC - DISTRIBUTION D                           /* RC08
*    04/02/2013 - RC REMOVE VL CODE
*    03/15/2013 - RC ADDED SOURCE CODE 'EW' GENERIC WARRANTS    /* RC07
*    06/01/2012 - RC - ADDED SOURCE CODE VL                  RC06
*    01/13/2012 - RC - SUNY-CUNY CHANGES. REPLACED TWRL810A W/ TWRL0702
*    10/18/2011 - RC - NON-ERISA TDA CHANGES
*    09/28/11 M BERLIN - ADDED MCCAMISH SOURCE CODE "AM" MODELLED
*             FROM "NV"                                /* 09/28/11
*    03/12/10 J.ROTHOLZ - FIX PAYMENT LOOKUP FOR REVERSALS WHERE MULTI-
*             COUNTRY LOCALITY (E.G., UK1) CAUSES FAILURE TO FIND
*             MATCHING PAYMENT.
*    07/30/08 JOHN OSTEEN - INTERNAL ATRA 1035 EXCHANGE RECON PROJECT
*             TRADE DATE AND REASON CODE OF 'CY' STORED ON ALL
*             INTERNAL ATRA 1035 EXCHANGE RECORDS.
*    05/08/08 ROSE MA - ROTH 401K/403B PROJECT
*             NEW CONTRACT TYPE 'ROTHP' WAS CREATED. FIVE ADDITIONAL NEW
*             FIELDS WERE PASSED FROM CPS TO TAXWARS.
*             UPDATED PROGRAMS IN JOB P1010TWD AND P1020TWD TO INCORPO-
*             RATE THESE FIELDS.
*             BASED ON THESE FIELDS, TWO EXISTING TAXWARS FIELDS WERE
*             DETERMINED AND POPULATED.
*             THIS PGM IS TO UPDATE THESE 2 FIELDS ON THE PAYMENT FILE.
*    09/26/2006 - JR - ADD TOTAL DISTRIBUTION AMT & PERCENT OF TOTAL
*                      FROM VUL FEED
*    08/08/2006 - JR - FIX PAYMENT SEQUENCE NUMBERS FOR REVERSALS
*    04/18/2006 - JR - NRA ROLLOVER CHECK ADDED; DIST CODE "G" FORCED
*    03/29/2006 - JR - OMNIPAY FEED CHANGED FROM ANNUAL TO DAILY
*    12/06/2005 - BK - RESTOWED FOR NEW TWRNCOMP PARMS
*    09/02/2005 - MS - NAVISYS
*    04/19/2005 - MS - NEW PROVINCE CODE 88 - NU
*    09/23/2004 - MS - TRUST - TOPS CHANGES.
*    08/03/1999 - MS - PAYMENT FILE SUPER CHANGE.
* 04/24/17 - J BREMER PIN EXPANSION - STOW FOR TWRL0702
* 04/11/18   'VL'ADDED WITH TWRT-SOURCE'AM' FOR DAILY PROCESS- VIKRAM2
* 07/11/19    TWRT-LOB-CDE = VT IS NOT PROCESSED -CPS SUNSET - SAURAV
***********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0820 extends BLNatBase
{
    // Data Areas
    private PdaTwracomp pdaTwracomp;
    private LdaTwrl0600 ldaTwrl0600;
    private LdaTwrl0702 ldaTwrl0702;
    private LdaTwrl0710 ldaTwrl0710;
    private LdaTwrl820a ldaTwrl820a;
    private LdaTwrl820b ldaTwrl820b;
    private LdaTwrl820d ldaTwrl820d;
    private LdaTwrl820e ldaTwrl820e;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Debug;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;
    private DbsField pnd_Ws_Const_Comp_Max;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Invalid_Comp;
    private DbsField pnd_Ws_Pnd_Source_Break;
    private DbsField pnd_Ws_Pnd_Comp_Idx;

    private DbsGroup pnd_Comp_Tot_Table;
    private DbsField pnd_Comp_Tot_Table_Pnd_Comp_Cde;
    private DbsField pnd_Comp_Tot_Table_Pnd_Comp_Nme;

    private DbsGroup pnd_Comp_Tot_Table_Pnd_Comp_Totals;
    private DbsField pnd_Comp_Tot_Table_Pnd_Trans_Count;
    private DbsField pnd_Comp_Tot_Table_Pnd_Gross_Amt;
    private DbsField pnd_Comp_Tot_Table_Pnd_Ivc_Amt;
    private DbsField pnd_Comp_Tot_Table_Pnd_Taxable_Amt;
    private DbsField pnd_Comp_Tot_Table_Pnd_Int_Amt;
    private DbsField pnd_Comp_Tot_Table_Pnd_Fed_Tax_Amt;
    private DbsField pnd_Comp_Tot_Table_Pnd_Nra_Tax_Amt;
    private DbsField pnd_Comp_Tot_Table_Pnd_Sta_Tax_Amt;
    private DbsField pnd_Comp_Tot_Table_Pnd_Loc_Tax_Amt;
    private DbsField pnd_Comp_Tot_Table_Pnd_Can_Tax_Amt;
    private DbsField pnd_Comp_Tot_Table_Pnd_P_R_Tax_Amt;

    private DataAccessProgramView vw_oldpay;
    private DbsField oldpay_Twrpymnt_Tax_Year;
    private DbsField oldpay_Twrpymnt_Company_Cde;
    private DbsField oldpay_Twrpymnt_Tax_Id_Nbr;
    private DbsField oldpay_Twrpymnt_Tax_Citizenship;
    private DbsField oldpay_Twrpymnt_Contract_Nbr;
    private DbsField oldpay_Twrpymnt_Payee_Cde;
    private DbsField oldpay_Twrpymnt_Paymt_Category;
    private DbsField oldpay_Twrpymnt_Distribution_Cde;
    private DbsField oldpay_Twrpymnt_Residency_Type;
    private DbsField oldpay_Twrpymnt_Residency_Code;
    private DbsField oldpay_Twrpymnt_Locality_Cde;
    private DbsField oldpay_Twrpymnt_Sum_Gross_Amt;
    private DbsField oldpay_Twrpymnt_Sum_Ivc_Amt;
    private DbsField oldpay_Twrpymnt_Sum_Int_Amt;
    private DbsField oldpay_Twrpymnt_Sum_Fed_Wthld;
    private DbsField oldpay_Twrpymnt_Sum_Nra_Wthld;
    private DbsField oldpay_Twrpymnt_Sum_Can_Wthld;
    private DbsField oldpay_Twrpymnt_Sum_State_Wthld;
    private DbsField oldpay_Twrpymnt_Sum_Local_Wthld;

    private DbsGroup oldpay_Twrpymnt_Payments;
    private DbsField oldpay_Twrpymnt_Pymnt_Dte;
    private DbsField oldpay_Twrpymnt_Orgn_Srce_Cde;
    private DbsField oldpay_Twrpymnt_Pymnt_Status;
    private DbsField oldpay_Twrpymnt_Gross_Amt;
    private DbsField oldpay_Twrpymnt_Ivc_Amt;
    private DbsField oldpay_Twrpymnt_Int_Amt;
    private DbsField oldpay_Twrpymnt_Fed_Wthld_Amt;
    private DbsField oldpay_Twrpymnt_Nra_Wthld_Amt;
    private DbsField oldpay_Twrpymnt_Can_Wthld_Amt;
    private DbsField oldpay_Twrpymnt_State_Wthld;
    private DbsField oldpay_Twrpymnt_Local_Wthld;
    private DbsField oldpay_Twrpymnt_Gtn_Dte_Time;
    private DbsField oldpay_Count_Casttwrpymnt_Payments;
    private DbsField oldpay_Twrpymnt_Fatca_Ind;

    private DataAccessProgramView vw_firstpay;
    private DbsField firstpay_Twrpymnt_Ivc_Indicator;

    private DataAccessProgramView vw_prtcpnt;
    private DbsField prtcpnt_Twrparti_Tax_Id;
    private DbsField prtcpnt_Twrparti_Status;
    private DbsField prtcpnt_Twrparti_Part_Eff_End_Dte;
    private DbsField prtcpnt_Twrparti_Citation_Ind;

    private DataAccessProgramView vw_updpar;
    private DbsField updpar_Twrparti_Citation_Ind;

    private DataAccessProgramView vw_cntlr;
    private DbsField cntlr_Tircntl_Tbl_Nbr;
    private DbsField cntlr_Tircntl_Tax_Year;
    private DbsField cntlr_Tircntl_Rpt_Source_Code;
    private DbsField cntlr_Tircntl_Company_Cde;
    private DbsField cntlr_Tircntl_5_Y_Sc_Co_To_Frm_Sp;

    private DataAccessProgramView vw_cntlu;
    private DbsField cntlu_Tircntl_Frm_Intrfce_Dte;
    private DbsField cntlu_Tircntl_To_Intrfce_Dte;
    private DbsField cntlu_Tircntl_Rpt_Update_Dte_Time;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField pnd_Twrparti_Curr_Rec_Sd;

    private DbsGroup pnd_Twrparti_Curr_Rec_Sd__R_Field_1;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Tax_Id;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Status;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Part_Eff_End_Dte;
    private DbsField pnd_Twrpymnt_Pymnt_Intfce_Dte;
    private DbsField pnd_Twrpymnt_Residency_Type;
    private DbsField pnd_Twrpymnt_Tax_Citizenship;
    private DbsField pnd_Twrpymnt_Pymnt_Refer_Nbr;
    private DbsField pnd_Twrpymnt_Paymt_Category;
    private DbsField pnd_Twrpymnt_Dist_Method;
    private DbsField pnd_Twrpymnt_Locality_Cde;
    private DbsField pnd_Twrpymnt_Contract_Seq_No;
    private DbsField pnd_Twrpymnt_Contract_Ppcn_Nbr;

    private DbsGroup pnd_Twrpymnt_Contract_Ppcn_Nbr__R_Field_2;
    private DbsField pnd_Twrpymnt_Contract_Ppcn_Nbr_Pnd_Twrpymnt_Contract_Nbr;
    private DbsField pnd_Twrpymnt_Contract_Ppcn_Nbr_Pnd_Twrpymnt_Payee_Cde;
    private DbsField pnd_Twrpymnt_Sys_Dte_Time;

    private DbsGroup pnd_Twrpymnt_Sys_Dte_Time__R_Field_3;
    private DbsField pnd_Twrpymnt_Sys_Dte_Time_Pnd_Twrpymnt_Sys_Dte_Time_N;

    private DbsGroup pnd_Twrpymnt_Sys_Dte_Time__R_Field_4;
    private DbsField pnd_Twrpymnt_Sys_Dte_Time_Pnd_Twrpymnt_Sys_Dte;
    private DbsField pnd_Twrpymnt_Sys_Dte_Time_Pnd_Twrpymnt_Sys_Time;
    private DbsField pnd_Twrpymnt_Form_Sd;

    private DbsGroup pnd_Twrpymnt_Form_Sd__R_Field_5;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Year;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Id_Nbr;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Contract_Nbr;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Payee_Cde;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Company_Cde;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Citizenship;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Paymt_Category;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Dist_Code;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Residency_Type;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Residency_Code;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Locality_Code;
    private DbsField pnd_Super_Dist_Code;

    private DbsGroup pnd_Super_Dist_Code__R_Field_6;
    private DbsField pnd_Super_Dist_Code_Pnd_S_Tbl_Nbr;
    private DbsField pnd_Super_Dist_Code_Pnd_S_Tax_Year;
    private DbsField pnd_Super_Dist_Code_Pnd_S_Settl_Type;
    private DbsField pnd_Super_Dist_Code_Pnd_S_Pay_Type;
    private DbsField pnd_Super_Dist_Code_Pnd_S_Dist_Code;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp;

    private DbsGroup pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_7;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_5;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Year;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Source_Code;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Company;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_To_Ccyymmdd;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Frm_Ccyymmdd;
    private DbsField pnd_Tircntl_Tax_Year_A;

    private DbsGroup pnd_Tircntl_Tax_Year_A__R_Field_8;
    private DbsField pnd_Tircntl_Tax_Year_A_Pnd_Tircntl_Tax_Year;
    private DbsField pnd_Tircntl_Rpt_Source_Code;
    private DbsField pnd_Tircntl_Frm_Intrfce_Dte_A;

    private DbsGroup pnd_Tircntl_Frm_Intrfce_Dte_A__R_Field_9;
    private DbsField pnd_Tircntl_Frm_Intrfce_Dte_A_Pnd_Tircntl_Frm_Intrfce_Dte;
    private DbsField pnd_Tircntl_To_Intrfce_Dte_A;

    private DbsGroup pnd_Tircntl_To_Intrfce_Dte_A__R_Field_10;
    private DbsField pnd_Tircntl_To_Intrfce_Dte_A_Pnd_Tircntl_To_Intrfce_Dte;
    private DbsField pnd_Cat_Dist_Table;

    private DbsGroup pnd_Cat_Dist_Table__R_Field_11;

    private DbsGroup pnd_Cat_Dist_Table_Pnd_Cat_Dist_Entries;
    private DbsField pnd_Cat_Dist_Table_Pnd_Cat_Dist_Method;

    private DbsGroup pnd_Cat_Dist_Table__R_Field_12;
    private DbsField pnd_Cat_Dist_Table_Pnd_Pay_Category;
    private DbsField pnd_Cat_Dist_Table_Pnd_Dist_Method;
    private DbsField pnd_Cat_Dist_Table_Pnd_Dist_Method_Code;
    private DbsField pnd_Search_Cat_Dist;

    private DbsGroup pnd_Search_Cat_Dist__R_Field_13;
    private DbsField pnd_Search_Cat_Dist_Pnd_Search_Cat;
    private DbsField pnd_Search_Cat_Dist_Pnd_Search_Dist;
    private DbsField pnd_Iaa_Contract;
    private DbsField pnd_Oc_N;

    private DbsGroup pnd_Oc_N__R_Field_14;
    private DbsField pnd_Oc_N_Pnd_Oc_A;
    private DbsField pnd_Citation_Message_Code;
    private DbsField pnd_Twrt_Locality_Cde;
    private DbsField pnd_Pay_Isn;
    private DbsField pnd_First_Seq_No_Isn;
    private DbsField pnd_Prev_Source;
    private DbsField pnd_Timn_N;

    private DbsGroup pnd_Timn_N__R_Field_15;
    private DbsField pnd_Timn_N_Pnd_Timn_A;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Bypass_Ctr;
    private DbsField pnd_Max_Ctr;
    private DbsField pnd_Store_Ctr;
    private DbsField pnd_Update_New_Ctr;
    private DbsField pnd_Update_Int_Ctr;
    private DbsField pnd_Bypass_Payment;
    private DbsField pnd_Summary_Found;
    private DbsField pnd_Us_Resident;
    private DbsField pnd_Date_Found;
    private DbsField pnd_Ivc_Ind_Updated;
    private DbsField pnd_Payment_24;
    private DbsField pnd_Rev_Match;
    private DbsField pnd_Rev_Orig_Updated;
    private DbsField pnd_Source_Total_Header;
    private DbsField pnd_Prev_Origin_Code;
    private DbsField pnd_Header_Date;
    private DbsField pnd_Interface_Date;
    private DbsField pnd_W_Additions_1;

    private DbsGroup pnd_W_Additions_1__R_Field_16;
    private DbsField pnd_W_Additions_1_Pnd_W_Contract_Type;
    private DbsField pnd_W_Additions_1__Filler1;
    private DbsField pnd_W_Additions_1_Pnd_W_Election_Code;
    private DbsField pnd_W_Additions_2;

    private DbsGroup pnd_W_Additions_2__R_Field_17;
    private DbsField pnd_W_Additions_2_Pnd_W_Nra_Refund_Amt;
    private DbsField pnd_W_Additions_2_Pnd_W_Irc_To;
    private DbsField pnd_W_Additions_3;

    private DbsGroup pnd_W_Additions_3__R_Field_18;
    private DbsField pnd_W_Additions_3_Pnd_W_Irc_From;
    private DbsField pnd_Summary_Record;
    private DbsField pnd_Payment_Found;
    private DbsField pnd_Update_Payment;
    private DbsField pnd_Et_Ctr;
    private DbsField pnd_Et_Const;
    private DbsField pnd_T;
    private DbsField i1;
    private DbsField i2;
    private DbsField i3;
    private DbsField i;
    private DbsField j;
    private DbsField k;
    private DbsField z;
    private DbsField pnd_Trade_Date_A;

    private DbsGroup pnd_Trade_Date_A__R_Field_19;
    private DbsField pnd_Trade_Date_A_Pnd_Trade_Date_D;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwracomp = new PdaTwracomp(localVariables);
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);
        ldaTwrl0702 = new LdaTwrl0702();
        registerRecord(ldaTwrl0702);
        ldaTwrl0710 = new LdaTwrl0710();
        registerRecord(ldaTwrl0710);
        ldaTwrl820a = new LdaTwrl820a();
        registerRecord(ldaTwrl820a);
        registerRecord(ldaTwrl820a.getVw_newpay());
        ldaTwrl820b = new LdaTwrl820b();
        registerRecord(ldaTwrl820b);
        registerRecord(ldaTwrl820b.getVw_updpay());
        ldaTwrl820d = new LdaTwrl820d();
        registerRecord(ldaTwrl820d);
        registerRecord(ldaTwrl820d.getVw_dist_Tbl());
        ldaTwrl820e = new LdaTwrl820e();
        registerRecord(ldaTwrl820e);
        registerRecord(ldaTwrl820e.getVw_cntl());

        // Local Variables
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_Comp_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Comp_Max", "COMP-MAX", FieldType.PACKED_DECIMAL, 3);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Invalid_Comp = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Invalid_Comp", "#INVALID-COMP", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Source_Break = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Source_Break", "#SOURCE-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Comp_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Comp_Idx", "#COMP-IDX", FieldType.PACKED_DECIMAL, 3);

        pnd_Comp_Tot_Table = localVariables.newGroupArrayInRecord("pnd_Comp_Tot_Table", "#COMP-TOT-TABLE", new DbsArrayController(1, 7));
        pnd_Comp_Tot_Table_Pnd_Comp_Cde = pnd_Comp_Tot_Table.newFieldInGroup("pnd_Comp_Tot_Table_Pnd_Comp_Cde", "#COMP-CDE", FieldType.STRING, 1);
        pnd_Comp_Tot_Table_Pnd_Comp_Nme = pnd_Comp_Tot_Table.newFieldInGroup("pnd_Comp_Tot_Table_Pnd_Comp_Nme", "#COMP-NME", FieldType.STRING, 4);

        pnd_Comp_Tot_Table_Pnd_Comp_Totals = pnd_Comp_Tot_Table.newGroupArrayInGroup("pnd_Comp_Tot_Table_Pnd_Comp_Totals", "#COMP-TOTALS", new DbsArrayController(1, 
            6));
        pnd_Comp_Tot_Table_Pnd_Trans_Count = pnd_Comp_Tot_Table_Pnd_Comp_Totals.newFieldInGroup("pnd_Comp_Tot_Table_Pnd_Trans_Count", "#TRANS-COUNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Comp_Tot_Table_Pnd_Gross_Amt = pnd_Comp_Tot_Table_Pnd_Comp_Totals.newFieldInGroup("pnd_Comp_Tot_Table_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 
            12, 2);
        pnd_Comp_Tot_Table_Pnd_Ivc_Amt = pnd_Comp_Tot_Table_Pnd_Comp_Totals.newFieldInGroup("pnd_Comp_Tot_Table_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 
            12, 2);
        pnd_Comp_Tot_Table_Pnd_Taxable_Amt = pnd_Comp_Tot_Table_Pnd_Comp_Totals.newFieldInGroup("pnd_Comp_Tot_Table_Pnd_Taxable_Amt", "#TAXABLE-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Comp_Tot_Table_Pnd_Int_Amt = pnd_Comp_Tot_Table_Pnd_Comp_Totals.newFieldInGroup("pnd_Comp_Tot_Table_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 
            12, 2);
        pnd_Comp_Tot_Table_Pnd_Fed_Tax_Amt = pnd_Comp_Tot_Table_Pnd_Comp_Totals.newFieldInGroup("pnd_Comp_Tot_Table_Pnd_Fed_Tax_Amt", "#FED-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Comp_Tot_Table_Pnd_Nra_Tax_Amt = pnd_Comp_Tot_Table_Pnd_Comp_Totals.newFieldInGroup("pnd_Comp_Tot_Table_Pnd_Nra_Tax_Amt", "#NRA-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Comp_Tot_Table_Pnd_Sta_Tax_Amt = pnd_Comp_Tot_Table_Pnd_Comp_Totals.newFieldInGroup("pnd_Comp_Tot_Table_Pnd_Sta_Tax_Amt", "#STA-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Comp_Tot_Table_Pnd_Loc_Tax_Amt = pnd_Comp_Tot_Table_Pnd_Comp_Totals.newFieldInGroup("pnd_Comp_Tot_Table_Pnd_Loc_Tax_Amt", "#LOC-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Comp_Tot_Table_Pnd_Can_Tax_Amt = pnd_Comp_Tot_Table_Pnd_Comp_Totals.newFieldInGroup("pnd_Comp_Tot_Table_Pnd_Can_Tax_Amt", "#CAN-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Comp_Tot_Table_Pnd_P_R_Tax_Amt = pnd_Comp_Tot_Table_Pnd_Comp_Totals.newFieldInGroup("pnd_Comp_Tot_Table_Pnd_P_R_Tax_Amt", "#P-R-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);

        vw_oldpay = new DataAccessProgramView(new NameInfo("vw_oldpay", "OLDPAY"), "TWRPYMNT_PAYMENT_FILE", "TIR_PAYMENT", DdmPeriodicGroups.getInstance().getGroups("TWRPYMNT_PAYMENT_FILE"));
        oldpay_Twrpymnt_Tax_Year = vw_oldpay.getRecord().newFieldInGroup("oldpay_Twrpymnt_Tax_Year", "TWRPYMNT-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TWRPYMNT_TAX_YEAR");
        oldpay_Twrpymnt_Company_Cde = vw_oldpay.getRecord().newFieldInGroup("oldpay_Twrpymnt_Company_Cde", "TWRPYMNT-COMPANY-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TWRPYMNT_COMPANY_CDE");
        oldpay_Twrpymnt_Tax_Id_Nbr = vw_oldpay.getRecord().newFieldInGroup("oldpay_Twrpymnt_Tax_Id_Nbr", "TWRPYMNT-TAX-ID-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "TWRPYMNT_TAX_ID_NBR");
        oldpay_Twrpymnt_Tax_Citizenship = vw_oldpay.getRecord().newFieldInGroup("oldpay_Twrpymnt_Tax_Citizenship", "TWRPYMNT-TAX-CITIZENSHIP", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPYMNT_TAX_CITIZENSHIP");
        oldpay_Twrpymnt_Contract_Nbr = vw_oldpay.getRecord().newFieldInGroup("oldpay_Twrpymnt_Contract_Nbr", "TWRPYMNT-CONTRACT-NBR", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPYMNT_CONTRACT_NBR");
        oldpay_Twrpymnt_Payee_Cde = vw_oldpay.getRecord().newFieldInGroup("oldpay_Twrpymnt_Payee_Cde", "TWRPYMNT-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRPYMNT_PAYEE_CDE");
        oldpay_Twrpymnt_Paymt_Category = vw_oldpay.getRecord().newFieldInGroup("oldpay_Twrpymnt_Paymt_Category", "TWRPYMNT-PAYMT-CATEGORY", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPYMNT_PAYMT_CATEGORY");
        oldpay_Twrpymnt_Distribution_Cde = vw_oldpay.getRecord().newFieldInGroup("oldpay_Twrpymnt_Distribution_Cde", "TWRPYMNT-DISTRIBUTION-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TWRPYMNT_DISTRIBUTION_CDE");
        oldpay_Twrpymnt_Residency_Type = vw_oldpay.getRecord().newFieldInGroup("oldpay_Twrpymnt_Residency_Type", "TWRPYMNT-RESIDENCY-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPYMNT_RESIDENCY_TYPE");
        oldpay_Twrpymnt_Residency_Code = vw_oldpay.getRecord().newFieldInGroup("oldpay_Twrpymnt_Residency_Code", "TWRPYMNT-RESIDENCY-CODE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TWRPYMNT_RESIDENCY_CODE");
        oldpay_Twrpymnt_Locality_Cde = vw_oldpay.getRecord().newFieldInGroup("oldpay_Twrpymnt_Locality_Cde", "TWRPYMNT-LOCALITY-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TWRPYMNT_LOCALITY_CDE");
        oldpay_Twrpymnt_Sum_Gross_Amt = vw_oldpay.getRecord().newFieldInGroup("oldpay_Twrpymnt_Sum_Gross_Amt", "TWRPYMNT-SUM-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_GROSS_AMT");
        oldpay_Twrpymnt_Sum_Ivc_Amt = vw_oldpay.getRecord().newFieldInGroup("oldpay_Twrpymnt_Sum_Ivc_Amt", "TWRPYMNT-SUM-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_IVC_AMT");
        oldpay_Twrpymnt_Sum_Int_Amt = vw_oldpay.getRecord().newFieldInGroup("oldpay_Twrpymnt_Sum_Int_Amt", "TWRPYMNT-SUM-INT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_INT_AMT");
        oldpay_Twrpymnt_Sum_Fed_Wthld = vw_oldpay.getRecord().newFieldInGroup("oldpay_Twrpymnt_Sum_Fed_Wthld", "TWRPYMNT-SUM-FED-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_FED_WTHLD");
        oldpay_Twrpymnt_Sum_Nra_Wthld = vw_oldpay.getRecord().newFieldInGroup("oldpay_Twrpymnt_Sum_Nra_Wthld", "TWRPYMNT-SUM-NRA-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_NRA_WTHLD");
        oldpay_Twrpymnt_Sum_Can_Wthld = vw_oldpay.getRecord().newFieldInGroup("oldpay_Twrpymnt_Sum_Can_Wthld", "TWRPYMNT-SUM-CAN-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_CAN_WTHLD");
        oldpay_Twrpymnt_Sum_State_Wthld = vw_oldpay.getRecord().newFieldInGroup("oldpay_Twrpymnt_Sum_State_Wthld", "TWRPYMNT-SUM-STATE-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_STATE_WTHLD");
        oldpay_Twrpymnt_Sum_Local_Wthld = vw_oldpay.getRecord().newFieldInGroup("oldpay_Twrpymnt_Sum_Local_Wthld", "TWRPYMNT-SUM-LOCAL-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_LOCAL_WTHLD");

        oldpay_Twrpymnt_Payments = vw_oldpay.getRecord().newGroupInGroup("oldpay_Twrpymnt_Payments", "TWRPYMNT-PAYMENTS", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        oldpay_Twrpymnt_Pymnt_Dte = oldpay_Twrpymnt_Payments.newFieldArrayInGroup("oldpay_Twrpymnt_Pymnt_Dte", "TWRPYMNT-PYMNT-DTE", FieldType.STRING, 
            8, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_PYMNT_DTE", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        oldpay_Twrpymnt_Orgn_Srce_Cde = oldpay_Twrpymnt_Payments.newFieldArrayInGroup("oldpay_Twrpymnt_Orgn_Srce_Cde", "TWRPYMNT-ORGN-SRCE-CDE", FieldType.STRING, 
            2, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ORGN_SRCE_CDE", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        oldpay_Twrpymnt_Pymnt_Status = oldpay_Twrpymnt_Payments.newFieldArrayInGroup("oldpay_Twrpymnt_Pymnt_Status", "TWRPYMNT-PYMNT-STATUS", FieldType.STRING, 
            1, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_PYMNT_STATUS", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        oldpay_Twrpymnt_Gross_Amt = oldpay_Twrpymnt_Payments.newFieldArrayInGroup("oldpay_Twrpymnt_Gross_Amt", "TWRPYMNT-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_GROSS_AMT", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        oldpay_Twrpymnt_Ivc_Amt = oldpay_Twrpymnt_Payments.newFieldArrayInGroup("oldpay_Twrpymnt_Ivc_Amt", "TWRPYMNT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_IVC_AMT", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        oldpay_Twrpymnt_Int_Amt = oldpay_Twrpymnt_Payments.newFieldArrayInGroup("oldpay_Twrpymnt_Int_Amt", "TWRPYMNT-INT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_INT_AMT", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        oldpay_Twrpymnt_Fed_Wthld_Amt = oldpay_Twrpymnt_Payments.newFieldArrayInGroup("oldpay_Twrpymnt_Fed_Wthld_Amt", "TWRPYMNT-FED-WTHLD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_FED_WTHLD_AMT", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        oldpay_Twrpymnt_Nra_Wthld_Amt = oldpay_Twrpymnt_Payments.newFieldArrayInGroup("oldpay_Twrpymnt_Nra_Wthld_Amt", "TWRPYMNT-NRA-WTHLD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_NRA_WTHLD_AMT", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        oldpay_Twrpymnt_Can_Wthld_Amt = oldpay_Twrpymnt_Payments.newFieldArrayInGroup("oldpay_Twrpymnt_Can_Wthld_Amt", "TWRPYMNT-CAN-WTHLD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_CAN_WTHLD_AMT", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        oldpay_Twrpymnt_State_Wthld = oldpay_Twrpymnt_Payments.newFieldArrayInGroup("oldpay_Twrpymnt_State_Wthld", "TWRPYMNT-STATE-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_STATE_WTHLD", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        oldpay_Twrpymnt_Local_Wthld = oldpay_Twrpymnt_Payments.newFieldArrayInGroup("oldpay_Twrpymnt_Local_Wthld", "TWRPYMNT-LOCAL-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_LOCAL_WTHLD", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        oldpay_Twrpymnt_Gtn_Dte_Time = oldpay_Twrpymnt_Payments.newFieldArrayInGroup("oldpay_Twrpymnt_Gtn_Dte_Time", "TWRPYMNT-GTN-DTE-TIME", FieldType.STRING, 
            15, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_GTN_DTE_TIME", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        oldpay_Count_Casttwrpymnt_Payments = oldpay_Twrpymnt_Payments.newFieldInGroup("oldpay_Count_Casttwrpymnt_Payments", "C*TWRPYMNT-PAYMENTS", RepeatingFieldStrategy.CAsteriskVariable, 
            "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        oldpay_Twrpymnt_Fatca_Ind = vw_oldpay.getRecord().newFieldInGroup("oldpay_Twrpymnt_Fatca_Ind", "TWRPYMNT-FATCA-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPYMNT_FATCA_IND");
        registerRecord(vw_oldpay);

        vw_firstpay = new DataAccessProgramView(new NameInfo("vw_firstpay", "FIRSTPAY"), "TWRPYMNT_PAYMENT_FILE", "TIR_PAYMENT");
        firstpay_Twrpymnt_Ivc_Indicator = vw_firstpay.getRecord().newFieldInGroup("firstpay_Twrpymnt_Ivc_Indicator", "TWRPYMNT-IVC-INDICATOR", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPYMNT_IVC_INDICATOR");
        registerRecord(vw_firstpay);

        vw_prtcpnt = new DataAccessProgramView(new NameInfo("vw_prtcpnt", "PRTCPNT"), "TWRPARTI_PARTICIPANT_FILE", "TWR_PARTICIPANT");
        prtcpnt_Twrparti_Tax_Id = vw_prtcpnt.getRecord().newFieldInGroup("prtcpnt_Twrparti_Tax_Id", "TWRPARTI-TAX-ID", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TWRPARTI_TAX_ID");
        prtcpnt_Twrparti_Status = vw_prtcpnt.getRecord().newFieldInGroup("prtcpnt_Twrparti_Status", "TWRPARTI-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_STATUS");
        prtcpnt_Twrparti_Part_Eff_End_Dte = vw_prtcpnt.getRecord().newFieldInGroup("prtcpnt_Twrparti_Part_Eff_End_Dte", "TWRPARTI-PART-EFF-END-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_PART_EFF_END_DTE");
        prtcpnt_Twrparti_Citation_Ind = vw_prtcpnt.getRecord().newFieldInGroup("prtcpnt_Twrparti_Citation_Ind", "TWRPARTI-CITATION-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPARTI_CITATION_IND");
        registerRecord(vw_prtcpnt);

        vw_updpar = new DataAccessProgramView(new NameInfo("vw_updpar", "UPDPAR"), "TWRPARTI_PARTICIPANT_FILE", "TWR_PARTICIPANT");
        updpar_Twrparti_Citation_Ind = vw_updpar.getRecord().newFieldInGroup("updpar_Twrparti_Citation_Ind", "TWRPARTI-CITATION-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPARTI_CITATION_IND");
        registerRecord(vw_updpar);

        vw_cntlr = new DataAccessProgramView(new NameInfo("vw_cntlr", "CNTLR"), "TIRCNTL_FEEDER_SYS_TBL_VIEW", "TIR_CONTROL");
        cntlr_Tircntl_Tbl_Nbr = vw_cntlr.getRecord().newFieldInGroup("cntlr_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        cntlr_Tircntl_Tax_Year = vw_cntlr.getRecord().newFieldInGroup("cntlr_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        cntlr_Tircntl_Rpt_Source_Code = vw_cntlr.getRecord().newFieldInGroup("cntlr_Tircntl_Rpt_Source_Code", "TIRCNTL-RPT-SOURCE-CODE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TIRCNTL_RPT_SOURCE_CODE");
        cntlr_Tircntl_Company_Cde = vw_cntlr.getRecord().newFieldInGroup("cntlr_Tircntl_Company_Cde", "TIRCNTL-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_COMPANY_CDE");
        cntlr_Tircntl_5_Y_Sc_Co_To_Frm_Sp = vw_cntlr.getRecord().newFieldInGroup("cntlr_Tircntl_5_Y_Sc_Co_To_Frm_Sp", "TIRCNTL-5-Y-SC-CO-TO-FRM-SP", FieldType.STRING, 
            24, RepeatingFieldStrategy.None, "TIRCNTL_5_Y_SC_CO_TO_FRM_SP");
        cntlr_Tircntl_5_Y_Sc_Co_To_Frm_Sp.setSuperDescriptor(true);
        registerRecord(vw_cntlr);

        vw_cntlu = new DataAccessProgramView(new NameInfo("vw_cntlu", "CNTLU"), "TIRCNTL_FEEDER_SYS_TBL_VIEW", "TIR_CONTROL");
        cntlu_Tircntl_Frm_Intrfce_Dte = vw_cntlu.getRecord().newFieldInGroup("cntlu_Tircntl_Frm_Intrfce_Dte", "TIRCNTL-FRM-INTRFCE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_FRM_INTRFCE_DTE");
        cntlu_Tircntl_To_Intrfce_Dte = vw_cntlu.getRecord().newFieldInGroup("cntlu_Tircntl_To_Intrfce_Dte", "TIRCNTL-TO-INTRFCE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_TO_INTRFCE_DTE");
        cntlu_Tircntl_Rpt_Update_Dte_Time = vw_cntlu.getRecord().newFieldInGroup("cntlu_Tircntl_Rpt_Update_Dte_Time", "TIRCNTL-RPT-UPDATE-DTE-TIME", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "TIRCNTL_RPT_UPDATE_DTE_TIME");
        registerRecord(vw_cntlu);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        registerRecord(vw_iaa_Cntrct);

        pnd_Twrparti_Curr_Rec_Sd = localVariables.newFieldInRecord("pnd_Twrparti_Curr_Rec_Sd", "#TWRPARTI-CURR-REC-SD", FieldType.STRING, 19);

        pnd_Twrparti_Curr_Rec_Sd__R_Field_1 = localVariables.newGroupInRecord("pnd_Twrparti_Curr_Rec_Sd__R_Field_1", "REDEFINE", pnd_Twrparti_Curr_Rec_Sd);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Tax_Id = pnd_Twrparti_Curr_Rec_Sd__R_Field_1.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Tax_Id", 
            "#TWRPARTI-TAX-ID", FieldType.STRING, 10);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Status = pnd_Twrparti_Curr_Rec_Sd__R_Field_1.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Status", 
            "#TWRPARTI-STATUS", FieldType.STRING, 1);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Part_Eff_End_Dte = pnd_Twrparti_Curr_Rec_Sd__R_Field_1.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Part_Eff_End_Dte", 
            "#TWRPARTI-PART-EFF-END-DTE", FieldType.STRING, 8);
        pnd_Twrpymnt_Pymnt_Intfce_Dte = localVariables.newFieldInRecord("pnd_Twrpymnt_Pymnt_Intfce_Dte", "#TWRPYMNT-PYMNT-INTFCE-DTE", FieldType.STRING, 
            8);
        pnd_Twrpymnt_Residency_Type = localVariables.newFieldInRecord("pnd_Twrpymnt_Residency_Type", "#TWRPYMNT-RESIDENCY-TYPE", FieldType.STRING, 1);
        pnd_Twrpymnt_Tax_Citizenship = localVariables.newFieldInRecord("pnd_Twrpymnt_Tax_Citizenship", "#TWRPYMNT-TAX-CITIZENSHIP", FieldType.STRING, 
            1);
        pnd_Twrpymnt_Pymnt_Refer_Nbr = localVariables.newFieldInRecord("pnd_Twrpymnt_Pymnt_Refer_Nbr", "#TWRPYMNT-PYMNT-REFER-NBR", FieldType.STRING, 
            2);
        pnd_Twrpymnt_Paymt_Category = localVariables.newFieldInRecord("pnd_Twrpymnt_Paymt_Category", "#TWRPYMNT-PAYMT-CATEGORY", FieldType.STRING, 1);
        pnd_Twrpymnt_Dist_Method = localVariables.newFieldInRecord("pnd_Twrpymnt_Dist_Method", "#TWRPYMNT-DIST-METHOD", FieldType.STRING, 1);
        pnd_Twrpymnt_Locality_Cde = localVariables.newFieldInRecord("pnd_Twrpymnt_Locality_Cde", "#TWRPYMNT-LOCALITY-CDE", FieldType.STRING, 3);
        pnd_Twrpymnt_Contract_Seq_No = localVariables.newFieldInRecord("pnd_Twrpymnt_Contract_Seq_No", "#TWRPYMNT-CONTRACT-SEQ-NO", FieldType.NUMERIC, 
            2);
        pnd_Twrpymnt_Contract_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Twrpymnt_Contract_Ppcn_Nbr", "#TWRPYMNT-CONTRACT-PPCN-NBR", FieldType.STRING, 
            10);

        pnd_Twrpymnt_Contract_Ppcn_Nbr__R_Field_2 = localVariables.newGroupInRecord("pnd_Twrpymnt_Contract_Ppcn_Nbr__R_Field_2", "REDEFINE", pnd_Twrpymnt_Contract_Ppcn_Nbr);
        pnd_Twrpymnt_Contract_Ppcn_Nbr_Pnd_Twrpymnt_Contract_Nbr = pnd_Twrpymnt_Contract_Ppcn_Nbr__R_Field_2.newFieldInGroup("pnd_Twrpymnt_Contract_Ppcn_Nbr_Pnd_Twrpymnt_Contract_Nbr", 
            "#TWRPYMNT-CONTRACT-NBR", FieldType.STRING, 8);
        pnd_Twrpymnt_Contract_Ppcn_Nbr_Pnd_Twrpymnt_Payee_Cde = pnd_Twrpymnt_Contract_Ppcn_Nbr__R_Field_2.newFieldInGroup("pnd_Twrpymnt_Contract_Ppcn_Nbr_Pnd_Twrpymnt_Payee_Cde", 
            "#TWRPYMNT-PAYEE-CDE", FieldType.STRING, 2);
        pnd_Twrpymnt_Sys_Dte_Time = localVariables.newFieldInRecord("pnd_Twrpymnt_Sys_Dte_Time", "#TWRPYMNT-SYS-DTE-TIME", FieldType.STRING, 15);

        pnd_Twrpymnt_Sys_Dte_Time__R_Field_3 = localVariables.newGroupInRecord("pnd_Twrpymnt_Sys_Dte_Time__R_Field_3", "REDEFINE", pnd_Twrpymnt_Sys_Dte_Time);
        pnd_Twrpymnt_Sys_Dte_Time_Pnd_Twrpymnt_Sys_Dte_Time_N = pnd_Twrpymnt_Sys_Dte_Time__R_Field_3.newFieldInGroup("pnd_Twrpymnt_Sys_Dte_Time_Pnd_Twrpymnt_Sys_Dte_Time_N", 
            "#TWRPYMNT-SYS-DTE-TIME-N", FieldType.NUMERIC, 15);

        pnd_Twrpymnt_Sys_Dte_Time__R_Field_4 = localVariables.newGroupInRecord("pnd_Twrpymnt_Sys_Dte_Time__R_Field_4", "REDEFINE", pnd_Twrpymnt_Sys_Dte_Time);
        pnd_Twrpymnt_Sys_Dte_Time_Pnd_Twrpymnt_Sys_Dte = pnd_Twrpymnt_Sys_Dte_Time__R_Field_4.newFieldInGroup("pnd_Twrpymnt_Sys_Dte_Time_Pnd_Twrpymnt_Sys_Dte", 
            "#TWRPYMNT-SYS-DTE", FieldType.STRING, 8);
        pnd_Twrpymnt_Sys_Dte_Time_Pnd_Twrpymnt_Sys_Time = pnd_Twrpymnt_Sys_Dte_Time__R_Field_4.newFieldInGroup("pnd_Twrpymnt_Sys_Dte_Time_Pnd_Twrpymnt_Sys_Time", 
            "#TWRPYMNT-SYS-TIME", FieldType.STRING, 7);
        pnd_Twrpymnt_Form_Sd = localVariables.newFieldInRecord("pnd_Twrpymnt_Form_Sd", "#TWRPYMNT-FORM-SD", FieldType.STRING, 35);

        pnd_Twrpymnt_Form_Sd__R_Field_5 = localVariables.newGroupInRecord("pnd_Twrpymnt_Form_Sd__R_Field_5", "REDEFINE", pnd_Twrpymnt_Form_Sd);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Year = pnd_Twrpymnt_Form_Sd__R_Field_5.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Year", "#TWR-TAX-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Id_Nbr = pnd_Twrpymnt_Form_Sd__R_Field_5.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Id_Nbr", "#TWR-TAX-ID-NBR", 
            FieldType.STRING, 10);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Contract_Nbr = pnd_Twrpymnt_Form_Sd__R_Field_5.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Contract_Nbr", "#TWR-CONTRACT-NBR", 
            FieldType.STRING, 8);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Payee_Cde = pnd_Twrpymnt_Form_Sd__R_Field_5.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Payee_Cde", "#TWR-PAYEE-CDE", 
            FieldType.STRING, 2);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Company_Cde = pnd_Twrpymnt_Form_Sd__R_Field_5.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Company_Cde", "#TWR-COMPANY-CDE", 
            FieldType.STRING, 1);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Citizenship = pnd_Twrpymnt_Form_Sd__R_Field_5.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Citizenship", 
            "#TWR-TAX-CITIZENSHIP", FieldType.STRING, 1);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Paymt_Category = pnd_Twrpymnt_Form_Sd__R_Field_5.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Paymt_Category", "#TWR-PAYMT-CATEGORY", 
            FieldType.STRING, 1);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Dist_Code = pnd_Twrpymnt_Form_Sd__R_Field_5.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Dist_Code", "#TWR-DIST-CODE", 
            FieldType.STRING, 2);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Residency_Type = pnd_Twrpymnt_Form_Sd__R_Field_5.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Residency_Type", "#TWR-RESIDENCY-TYPE", 
            FieldType.STRING, 1);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Residency_Code = pnd_Twrpymnt_Form_Sd__R_Field_5.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Residency_Code", "#TWR-RESIDENCY-CODE", 
            FieldType.STRING, 2);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Locality_Code = pnd_Twrpymnt_Form_Sd__R_Field_5.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Locality_Code", "#TWR-LOCALITY-CODE", 
            FieldType.STRING, 3);
        pnd_Super_Dist_Code = localVariables.newFieldInRecord("pnd_Super_Dist_Code", "#SUPER-DIST-CODE", FieldType.STRING, 8);

        pnd_Super_Dist_Code__R_Field_6 = localVariables.newGroupInRecord("pnd_Super_Dist_Code__R_Field_6", "REDEFINE", pnd_Super_Dist_Code);
        pnd_Super_Dist_Code_Pnd_S_Tbl_Nbr = pnd_Super_Dist_Code__R_Field_6.newFieldInGroup("pnd_Super_Dist_Code_Pnd_S_Tbl_Nbr", "#S-TBL-NBR", FieldType.NUMERIC, 
            1);
        pnd_Super_Dist_Code_Pnd_S_Tax_Year = pnd_Super_Dist_Code__R_Field_6.newFieldInGroup("pnd_Super_Dist_Code_Pnd_S_Tax_Year", "#S-TAX-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Super_Dist_Code_Pnd_S_Settl_Type = pnd_Super_Dist_Code__R_Field_6.newFieldInGroup("pnd_Super_Dist_Code_Pnd_S_Settl_Type", "#S-SETTL-TYPE", 
            FieldType.STRING, 1);
        pnd_Super_Dist_Code_Pnd_S_Pay_Type = pnd_Super_Dist_Code__R_Field_6.newFieldInGroup("pnd_Super_Dist_Code_Pnd_S_Pay_Type", "#S-PAY-TYPE", FieldType.STRING, 
            1);
        pnd_Super_Dist_Code_Pnd_S_Dist_Code = pnd_Super_Dist_Code__R_Field_6.newFieldInGroup("pnd_Super_Dist_Code_Pnd_S_Dist_Code", "#S-DIST-CODE", FieldType.STRING, 
            1);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp = localVariables.newFieldInRecord("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp", "#TIRCNTL-5-Y-SC-CO-TO-FRM-SP", FieldType.STRING, 
            24);

        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_7 = localVariables.newGroupInRecord("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_7", "REDEFINE", pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_5 = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_7.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_5", 
            "#SP-CNTL-5", FieldType.NUMERIC, 1);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Year = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_7.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Year", 
            "#SP-CNTL-YEAR", FieldType.NUMERIC, 4);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Source_Code = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_7.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Source_Code", 
            "#SP-CNTL-SOURCE-CODE", FieldType.STRING, 2);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Company = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_7.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Company", 
            "#SP-CNTL-COMPANY", FieldType.STRING, 1);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_To_Ccyymmdd = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_7.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_To_Ccyymmdd", 
            "#SP-CNTL-TO-CCYYMMDD", FieldType.STRING, 8);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Frm_Ccyymmdd = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_7.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Frm_Ccyymmdd", 
            "#SP-CNTL-FRM-CCYYMMDD", FieldType.STRING, 8);
        pnd_Tircntl_Tax_Year_A = localVariables.newFieldInRecord("pnd_Tircntl_Tax_Year_A", "#TIRCNTL-TAX-YEAR-A", FieldType.STRING, 4);

        pnd_Tircntl_Tax_Year_A__R_Field_8 = localVariables.newGroupInRecord("pnd_Tircntl_Tax_Year_A__R_Field_8", "REDEFINE", pnd_Tircntl_Tax_Year_A);
        pnd_Tircntl_Tax_Year_A_Pnd_Tircntl_Tax_Year = pnd_Tircntl_Tax_Year_A__R_Field_8.newFieldInGroup("pnd_Tircntl_Tax_Year_A_Pnd_Tircntl_Tax_Year", 
            "#TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Tircntl_Rpt_Source_Code = localVariables.newFieldInRecord("pnd_Tircntl_Rpt_Source_Code", "#TIRCNTL-RPT-SOURCE-CODE", FieldType.STRING, 2);
        pnd_Tircntl_Frm_Intrfce_Dte_A = localVariables.newFieldInRecord("pnd_Tircntl_Frm_Intrfce_Dte_A", "#TIRCNTL-FRM-INTRFCE-DTE-A", FieldType.STRING, 
            8);

        pnd_Tircntl_Frm_Intrfce_Dte_A__R_Field_9 = localVariables.newGroupInRecord("pnd_Tircntl_Frm_Intrfce_Dte_A__R_Field_9", "REDEFINE", pnd_Tircntl_Frm_Intrfce_Dte_A);
        pnd_Tircntl_Frm_Intrfce_Dte_A_Pnd_Tircntl_Frm_Intrfce_Dte = pnd_Tircntl_Frm_Intrfce_Dte_A__R_Field_9.newFieldInGroup("pnd_Tircntl_Frm_Intrfce_Dte_A_Pnd_Tircntl_Frm_Intrfce_Dte", 
            "#TIRCNTL-FRM-INTRFCE-DTE", FieldType.NUMERIC, 8);
        pnd_Tircntl_To_Intrfce_Dte_A = localVariables.newFieldInRecord("pnd_Tircntl_To_Intrfce_Dte_A", "#TIRCNTL-TO-INTRFCE-DTE-A", FieldType.STRING, 
            8);

        pnd_Tircntl_To_Intrfce_Dte_A__R_Field_10 = localVariables.newGroupInRecord("pnd_Tircntl_To_Intrfce_Dte_A__R_Field_10", "REDEFINE", pnd_Tircntl_To_Intrfce_Dte_A);
        pnd_Tircntl_To_Intrfce_Dte_A_Pnd_Tircntl_To_Intrfce_Dte = pnd_Tircntl_To_Intrfce_Dte_A__R_Field_10.newFieldInGroup("pnd_Tircntl_To_Intrfce_Dte_A_Pnd_Tircntl_To_Intrfce_Dte", 
            "#TIRCNTL-TO-INTRFCE-DTE", FieldType.NUMERIC, 8);
        pnd_Cat_Dist_Table = localVariables.newFieldInRecord("pnd_Cat_Dist_Table", "#CAT-DIST-TABLE", FieldType.STRING, 18);

        pnd_Cat_Dist_Table__R_Field_11 = localVariables.newGroupInRecord("pnd_Cat_Dist_Table__R_Field_11", "REDEFINE", pnd_Cat_Dist_Table);

        pnd_Cat_Dist_Table_Pnd_Cat_Dist_Entries = pnd_Cat_Dist_Table__R_Field_11.newGroupArrayInGroup("pnd_Cat_Dist_Table_Pnd_Cat_Dist_Entries", "#CAT-DIST-ENTRIES", 
            new DbsArrayController(1, 6));
        pnd_Cat_Dist_Table_Pnd_Cat_Dist_Method = pnd_Cat_Dist_Table_Pnd_Cat_Dist_Entries.newFieldInGroup("pnd_Cat_Dist_Table_Pnd_Cat_Dist_Method", "#CAT-DIST-METHOD", 
            FieldType.STRING, 2);

        pnd_Cat_Dist_Table__R_Field_12 = pnd_Cat_Dist_Table_Pnd_Cat_Dist_Entries.newGroupInGroup("pnd_Cat_Dist_Table__R_Field_12", "REDEFINE", pnd_Cat_Dist_Table_Pnd_Cat_Dist_Method);
        pnd_Cat_Dist_Table_Pnd_Pay_Category = pnd_Cat_Dist_Table__R_Field_12.newFieldInGroup("pnd_Cat_Dist_Table_Pnd_Pay_Category", "#PAY-CATEGORY", FieldType.STRING, 
            1);
        pnd_Cat_Dist_Table_Pnd_Dist_Method = pnd_Cat_Dist_Table__R_Field_12.newFieldInGroup("pnd_Cat_Dist_Table_Pnd_Dist_Method", "#DIST-METHOD", FieldType.STRING, 
            1);
        pnd_Cat_Dist_Table_Pnd_Dist_Method_Code = pnd_Cat_Dist_Table_Pnd_Cat_Dist_Entries.newFieldInGroup("pnd_Cat_Dist_Table_Pnd_Dist_Method_Code", "#DIST-METHOD-CODE", 
            FieldType.STRING, 1);
        pnd_Search_Cat_Dist = localVariables.newFieldInRecord("pnd_Search_Cat_Dist", "#SEARCH-CAT-DIST", FieldType.STRING, 2);

        pnd_Search_Cat_Dist__R_Field_13 = localVariables.newGroupInRecord("pnd_Search_Cat_Dist__R_Field_13", "REDEFINE", pnd_Search_Cat_Dist);
        pnd_Search_Cat_Dist_Pnd_Search_Cat = pnd_Search_Cat_Dist__R_Field_13.newFieldInGroup("pnd_Search_Cat_Dist_Pnd_Search_Cat", "#SEARCH-CAT", FieldType.STRING, 
            1);
        pnd_Search_Cat_Dist_Pnd_Search_Dist = pnd_Search_Cat_Dist__R_Field_13.newFieldInGroup("pnd_Search_Cat_Dist_Pnd_Search_Dist", "#SEARCH-DIST", FieldType.STRING, 
            1);
        pnd_Iaa_Contract = localVariables.newFieldInRecord("pnd_Iaa_Contract", "#IAA-CONTRACT", FieldType.STRING, 10);
        pnd_Oc_N = localVariables.newFieldInRecord("pnd_Oc_N", "#OC-N", FieldType.NUMERIC, 2);

        pnd_Oc_N__R_Field_14 = localVariables.newGroupInRecord("pnd_Oc_N__R_Field_14", "REDEFINE", pnd_Oc_N);
        pnd_Oc_N_Pnd_Oc_A = pnd_Oc_N__R_Field_14.newFieldInGroup("pnd_Oc_N_Pnd_Oc_A", "#OC-A", FieldType.STRING, 2);
        pnd_Citation_Message_Code = localVariables.newFieldInRecord("pnd_Citation_Message_Code", "#CITATION-MESSAGE-CODE", FieldType.NUMERIC, 2);
        pnd_Twrt_Locality_Cde = localVariables.newFieldInRecord("pnd_Twrt_Locality_Cde", "#TWRT-LOCALITY-CDE", FieldType.STRING, 3);
        pnd_Pay_Isn = localVariables.newFieldInRecord("pnd_Pay_Isn", "#PAY-ISN", FieldType.PACKED_DECIMAL, 12);
        pnd_First_Seq_No_Isn = localVariables.newFieldInRecord("pnd_First_Seq_No_Isn", "#FIRST-SEQ-NO-ISN", FieldType.PACKED_DECIMAL, 12);
        pnd_Prev_Source = localVariables.newFieldInRecord("pnd_Prev_Source", "#PREV-SOURCE", FieldType.STRING, 2);
        pnd_Timn_N = localVariables.newFieldInRecord("pnd_Timn_N", "#TIMN-N", FieldType.NUMERIC, 7);

        pnd_Timn_N__R_Field_15 = localVariables.newGroupInRecord("pnd_Timn_N__R_Field_15", "REDEFINE", pnd_Timn_N);
        pnd_Timn_N_Pnd_Timn_A = pnd_Timn_N__R_Field_15.newFieldInGroup("pnd_Timn_N_Pnd_Timn_A", "#TIMN-A", FieldType.STRING, 7);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Bypass_Ctr = localVariables.newFieldInRecord("pnd_Bypass_Ctr", "#BYPASS-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Max_Ctr = localVariables.newFieldInRecord("pnd_Max_Ctr", "#MAX-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Store_Ctr = localVariables.newFieldInRecord("pnd_Store_Ctr", "#STORE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Update_New_Ctr = localVariables.newFieldInRecord("pnd_Update_New_Ctr", "#UPDATE-NEW-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Update_Int_Ctr = localVariables.newFieldInRecord("pnd_Update_Int_Ctr", "#UPDATE-INT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Bypass_Payment = localVariables.newFieldInRecord("pnd_Bypass_Payment", "#BYPASS-PAYMENT", FieldType.BOOLEAN, 1);
        pnd_Summary_Found = localVariables.newFieldInRecord("pnd_Summary_Found", "#SUMMARY-FOUND", FieldType.BOOLEAN, 1);
        pnd_Us_Resident = localVariables.newFieldInRecord("pnd_Us_Resident", "#US-RESIDENT", FieldType.BOOLEAN, 1);
        pnd_Date_Found = localVariables.newFieldInRecord("pnd_Date_Found", "#DATE-FOUND", FieldType.BOOLEAN, 1);
        pnd_Ivc_Ind_Updated = localVariables.newFieldInRecord("pnd_Ivc_Ind_Updated", "#IVC-IND-UPDATED", FieldType.BOOLEAN, 1);
        pnd_Payment_24 = localVariables.newFieldInRecord("pnd_Payment_24", "#PAYMENT-24", FieldType.BOOLEAN, 1);
        pnd_Rev_Match = localVariables.newFieldInRecord("pnd_Rev_Match", "#REV-MATCH", FieldType.BOOLEAN, 1);
        pnd_Rev_Orig_Updated = localVariables.newFieldInRecord("pnd_Rev_Orig_Updated", "#REV-ORIG-UPDATED", FieldType.BOOLEAN, 1);
        pnd_Source_Total_Header = localVariables.newFieldInRecord("pnd_Source_Total_Header", "#SOURCE-TOTAL-HEADER", FieldType.STRING, 17);
        pnd_Prev_Origin_Code = localVariables.newFieldInRecord("pnd_Prev_Origin_Code", "#PREV-ORIGIN-CODE", FieldType.STRING, 2);
        pnd_Header_Date = localVariables.newFieldInRecord("pnd_Header_Date", "#HEADER-DATE", FieldType.DATE);
        pnd_Interface_Date = localVariables.newFieldInRecord("pnd_Interface_Date", "#INTERFACE-DATE", FieldType.DATE);
        pnd_W_Additions_1 = localVariables.newFieldInRecord("pnd_W_Additions_1", "#W-ADDITIONS-1", FieldType.STRING, 15);

        pnd_W_Additions_1__R_Field_16 = localVariables.newGroupInRecord("pnd_W_Additions_1__R_Field_16", "REDEFINE", pnd_W_Additions_1);
        pnd_W_Additions_1_Pnd_W_Contract_Type = pnd_W_Additions_1__R_Field_16.newFieldInGroup("pnd_W_Additions_1_Pnd_W_Contract_Type", "#W-CONTRACT-TYPE", 
            FieldType.STRING, 5);
        pnd_W_Additions_1__Filler1 = pnd_W_Additions_1__R_Field_16.newFieldInGroup("pnd_W_Additions_1__Filler1", "_FILLER1", FieldType.STRING, 3);
        pnd_W_Additions_1_Pnd_W_Election_Code = pnd_W_Additions_1__R_Field_16.newFieldInGroup("pnd_W_Additions_1_Pnd_W_Election_Code", "#W-ELECTION-CODE", 
            FieldType.NUMERIC, 3);
        pnd_W_Additions_2 = localVariables.newFieldInRecord("pnd_W_Additions_2", "#W-ADDITIONS-2", FieldType.STRING, 15);

        pnd_W_Additions_2__R_Field_17 = localVariables.newGroupInRecord("pnd_W_Additions_2__R_Field_17", "REDEFINE", pnd_W_Additions_2);
        pnd_W_Additions_2_Pnd_W_Nra_Refund_Amt = pnd_W_Additions_2__R_Field_17.newFieldInGroup("pnd_W_Additions_2_Pnd_W_Nra_Refund_Amt", "#W-NRA-REFUND-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W_Additions_2_Pnd_W_Irc_To = pnd_W_Additions_2__R_Field_17.newFieldInGroup("pnd_W_Additions_2_Pnd_W_Irc_To", "#W-IRC-TO", FieldType.STRING, 
            2);
        pnd_W_Additions_3 = localVariables.newFieldInRecord("pnd_W_Additions_3", "#W-ADDITIONS-3", FieldType.STRING, 21);

        pnd_W_Additions_3__R_Field_18 = localVariables.newGroupInRecord("pnd_W_Additions_3__R_Field_18", "REDEFINE", pnd_W_Additions_3);
        pnd_W_Additions_3_Pnd_W_Irc_From = pnd_W_Additions_3__R_Field_18.newFieldInGroup("pnd_W_Additions_3_Pnd_W_Irc_From", "#W-IRC-FROM", FieldType.STRING, 
            20);
        pnd_Summary_Record = localVariables.newFieldInRecord("pnd_Summary_Record", "#SUMMARY-RECORD", FieldType.BOOLEAN, 1);
        pnd_Payment_Found = localVariables.newFieldInRecord("pnd_Payment_Found", "#PAYMENT-FOUND", FieldType.BOOLEAN, 1);
        pnd_Update_Payment = localVariables.newFieldInRecord("pnd_Update_Payment", "#UPDATE-PAYMENT", FieldType.BOOLEAN, 1);
        pnd_Et_Ctr = localVariables.newFieldInRecord("pnd_Et_Ctr", "#ET-CTR", FieldType.PACKED_DECIMAL, 4);
        pnd_Et_Const = localVariables.newFieldInRecord("pnd_Et_Const", "#ET-CONST", FieldType.PACKED_DECIMAL, 3);
        pnd_T = localVariables.newFieldInRecord("pnd_T", "#T", FieldType.PACKED_DECIMAL, 3);
        i1 = localVariables.newFieldInRecord("i1", "I1", FieldType.PACKED_DECIMAL, 3);
        i2 = localVariables.newFieldInRecord("i2", "I2", FieldType.PACKED_DECIMAL, 3);
        i3 = localVariables.newFieldInRecord("i3", "I3", FieldType.PACKED_DECIMAL, 3);
        i = localVariables.newFieldInRecord("i", "I", FieldType.PACKED_DECIMAL, 3);
        j = localVariables.newFieldInRecord("j", "J", FieldType.PACKED_DECIMAL, 4);
        k = localVariables.newFieldInRecord("k", "K", FieldType.PACKED_DECIMAL, 4);
        z = localVariables.newFieldInRecord("z", "Z", FieldType.PACKED_DECIMAL, 4);
        pnd_Trade_Date_A = localVariables.newFieldInRecord("pnd_Trade_Date_A", "#TRADE-DATE-A", FieldType.STRING, 4);

        pnd_Trade_Date_A__R_Field_19 = localVariables.newGroupInRecord("pnd_Trade_Date_A__R_Field_19", "REDEFINE", pnd_Trade_Date_A);
        pnd_Trade_Date_A_Pnd_Trade_Date_D = pnd_Trade_Date_A__R_Field_19.newFieldInGroup("pnd_Trade_Date_A_Pnd_Trade_Date_D", "#TRADE-DATE-D", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_oldpay.reset();
        vw_firstpay.reset();
        vw_prtcpnt.reset();
        vw_updpar.reset();
        vw_cntlr.reset();
        vw_cntlu.reset();
        vw_iaa_Cntrct.reset();

        ldaTwrl0600.initializeValues();
        ldaTwrl0702.initializeValues();
        ldaTwrl0710.initializeValues();
        ldaTwrl820a.initializeValues();
        ldaTwrl820b.initializeValues();
        ldaTwrl820d.initializeValues();
        ldaTwrl820e.initializeValues();

        localVariables.reset();
        pnd_Debug.setInitialValue(true);
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
        pnd_Ws_Const_Comp_Max.setInitialValue(6);
        pnd_Ws_Pnd_Source_Break.setInitialValue(true);
        pnd_Comp_Tot_Table_Pnd_Comp_Nme.getValue(0 + 1).setInitialValue("????");
        pnd_Cat_Dist_Table.setInitialValue("PCAPRBLCDLRERCDRRE");
        pnd_Et_Const.setInitialValue(200);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0820() throws Exception
    {
        super("Twrp0820");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP0820", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 02 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 03 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 04 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 05 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 06 ) PS = 60 LS = 133 ZP = ON
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* *============**
        //*  MAIN PROGRAM *
        //* *============**
        pdaTwracomp.getPnd_Twracomp_Pnd_Abend_Ind().reset();                                                                                                              //Natural: RESET #TWRACOMP.#ABEND-IND #TWRACOMP.#DISPLAY-IND
        pdaTwracomp.getPnd_Twracomp_Pnd_Display_Ind().reset();
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 01 RECORD TWRT-RECORD
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaTwrl0702.getTwrt_Record())))
        {
            CheckAtStartofData1048();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*  BEFORE BREAK PROCESSING
            //*  TWRT-PAYMT-DTE := 20080611
            //*  END-BEFORE
            //*                                                                                                                                                           //Natural: AT START OF DATA;//Natural: AT BREAK OF TWRT-SOURCE
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            if (condition(pnd_Ws_Pnd_Source_Break.getBoolean()))                                                                                                          //Natural: IF #WS.#SOURCE-BREAK
            {
                pnd_Prev_Source.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Source());                                                                                       //Natural: ASSIGN #PREV-SOURCE := TWRT-SOURCE
                                                                                                                                                                          //Natural: PERFORM GET-HEADER-DATE
                sub_Get_Header_Date();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ws_Pnd_Source_Break.reset();                                                                                                                          //Natural: RESET #WS.#SOURCE-BREAK
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Type().equals("S") && ldaTwrl0702.getTwrt_Record_Twrt_Nra_Exemption_Code().equals("02")                     //Natural: IF TWRT-TAX-TYPE = 'S' AND TWRT-NRA-EXEMPTION-CODE = '02' AND TWRT-GROSS-AMT NE 0
                && ldaTwrl0702.getTwrt_Record_Twrt_Gross_Amt().notEquals(getZero())))
            {
                ldaTwrl0702.getTwrt_Record_Twrt_Distribution_Code_1().setValue("G");                                                                                      //Natural: ASSIGN TWRT-DISTRIBUTION-CODE-1 := 'G'
            }                                                                                                                                                             //Natural: END-IF
            //*  RC08
                                                                                                                                                                          //Natural: PERFORM DISTRIBUTION-D-CONVERSION
            sub_Distribution_D_Conversion();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY
            sub_Get_Company();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_T.setValue(1);                                                                                                                                            //Natural: ASSIGN #T := 1
                                                                                                                                                                          //Natural: PERFORM COMPANY-TOTALS
            sub_Company_Totals();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  IF #DEBUG
            //*    WRITE '=' TWRT-TAX-ID 5X '=' TWRT-CNTRCT-NBR
            //*  END-IF
                                                                                                                                                                          //Natural: PERFORM LOOKUP-PAYMENT
            sub_Lookup_Payment();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Bypass_Payment.equals(true)))                                                                                                               //Natural: IF #BYPASS-PAYMENT = TRUE
            {
                pnd_T.setValue(2);                                                                                                                                        //Natural: ASSIGN #T := 2
                                                                                                                                                                          //Natural: PERFORM COMPANY-TOTALS
                sub_Company_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Bypass_Ctr.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #BYPASS-CTR
                                                                                                                                                                          //Natural: PERFORM REPORT-BYPASSED-EXISTING-PAYMENTS
                sub_Report_Bypassed_Existing_Payments();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Op_Rev_Ind().equals("X")))                                                                                  //Natural: IF TWRT-OP-REV-IND = 'X'
                {
                                                                                                                                                                          //Natural: PERFORM CHANGE-REVERSAL-SIGNS
                    sub_Change_Reversal_Signs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Payment_Found.equals(false)))                                                                                                           //Natural: IF #PAYMENT-FOUND = FALSE
                {
                                                                                                                                                                          //Natural: PERFORM STORE-NEW-PAYMENT
                    sub_Store_New_Payment();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-NEW-PAYMENT
                    sub_Update_New_Payment();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_T.setValue(3);                                                                                                                                        //Natural: ASSIGN #T := 3
                                                                                                                                                                          //Natural: PERFORM COMPANY-TOTALS
                sub_Company_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl0702.getTwrt_Record_Twrt_Isn().setValue(pnd_Pay_Isn);                                                                                                  //Natural: ASSIGN TWRT-ISN := #PAY-ISN
            //*   #PAY-ISN
            getWorkFiles().write(3, false, ldaTwrl0702.getTwrt_Record());                                                                                                 //Natural: WRITE WORK FILE 03 TWRT-RECORD
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        if (condition(pnd_Read_Ctr.equals(getZero())))                                                                                                                    //Natural: IF #READ-CTR = 0
        {
            //*  PERFORM  ERROR-DISPLAY-START
            getReports().write(0, "***",new TabSetting(6),"Tax Transaction File (Work File 01) Is Empty",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new  //Natural: WRITE ( 00 ) '***' 06T 'Tax Transaction File (Work File 01) Is Empty' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
            //*  PERFORM  ERROR-DISPLAY-END
            //*  TERMINATE 90
        }                                                                                                                                                                 //Natural: END-IF
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *      = 'VL' OR
        //* *------------
        //*  -NRA-TAX-RATE   :=  TWRT-FED-NRA-TAX-RATE
        //* * 11-13-03 BY FRANK - REVERSED COMMENT/UNCOMMENT THE NEXT 2 LINES
        //* *UPDPAY.TWRPYMNT-FRM1001-IND      (J)  :=  TWRT-PAYMENT-FORM1001
        //*  UPDPAY.TWRPYMNT-SUB-PLAN-TYPE-NOT-USED(J)  :=  TWRT-LOB-CDE
        //* *------------
        //* *----------------------------------
        //*  IF #DEBUG
        //*    WRITE 'BEGIN SUBROUTINE - STORE-NEW-PAYMENT'
        //*  END-IF
        //*  -NRA-TAX-RATE   :=  TWRT-FED-NRA-TAX-RATE
        //* *              OR = 'VL'
        //* * UNCOMMENTED 01-12-04 BY FRANK
        //* *NEWPAY.TWRPYMNT-FRM1001-IND      (1)  :=  TWRT-PAYMENT-FORM1001
        //*  WPAY.TWRPYMNT-SUB-PLAN-TYPE-NOT-USED(1)  :=  TWRT-LOB-CDE
        //* *------------
        //* *--------------------------------------
        //* *------------
        //* ****************************************
        //* *******************************
        //* ***************************************
        //* ***************************************
        //* **************************************
        //* **************************************
        //* *************************************
        //* *-------------------------------------------
        //* *------------
        //* *--------------------------------
        //*                 OR = 'VL'
        //* *------------
        //* *---------------------------------
        //* *************************************
        //* *************************************
        //* *--------------------------------------
        //* *------------
        //* *------------
        //* ****************************
        //* *****************************
        //* ****************************
        //* ***************************************
        //* ***************************************
        //*  IF #DEBUG
        //*    WRITE 'BEGIN SUBROUTINE - CHANGE-REVERSAL-SIGNS'
        //*  END-IF
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 01 )
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 02 )
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Lookup_Payment() throws Exception                                                                                                                    //Natural: LOOKUP-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------
        pnd_Rev_Match.resetInitial();                                                                                                                                     //Natural: RESET INITIAL #REV-MATCH #REV-ORIG-UPDATED
        pnd_Rev_Orig_Updated.resetInitial();
        pnd_Twrpymnt_Contract_Seq_No.setValue(0);                                                                                                                         //Natural: ASSIGN #TWRPYMNT-CONTRACT-SEQ-NO := 0
        pnd_Bypass_Payment.setValue(false);                                                                                                                               //Natural: ASSIGN #BYPASS-PAYMENT := FALSE
        pnd_Payment_Found.setValue(false);                                                                                                                                //Natural: ASSIGN #PAYMENT-FOUND := FALSE
        pnd_Payment_24.setValue(false);                                                                                                                                   //Natural: ASSIGN #PAYMENT-24 := FALSE
                                                                                                                                                                          //Natural: PERFORM LOOKUP-RESIDENCY-TYPE
        sub_Lookup_Residency_Type();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM LOOKUP-CITIZENSHIP-CODE
        sub_Lookup_Citizenship_Code();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM LOOKUP-DIST-CODE-TABLE
        sub_Lookup_Dist_Code_Table();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde().equals("3E")))                                                                                       //Natural: IF TWRT-LOCALITY-CDE = '3E'
        {
            pnd_Twrt_Locality_Cde.setValue(" ");                                                                                                                          //Natural: ASSIGN #TWRT-LOCALITY-CDE := ' '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Annt_Locality_Code().notEquals(" ") && ! (DbsUtil.maskMatches(ldaTwrl0702.getTwrt_Record_Twrt_Annt_Locality_Code(), //Natural: IF TWRT-ANNT-LOCALITY-CODE NE ' ' AND TWRT-ANNT-LOCALITY-CODE NE MASK ( ..' ' )
                "..' '"))))
            {
                pnd_Twrt_Locality_Cde.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Annt_Locality_Code());                                                                     //Natural: ASSIGN #TWRT-LOCALITY-CDE := TWRT-ANNT-LOCALITY-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Twrt_Locality_Cde.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde());                                                                           //Natural: ASSIGN #TWRT-LOCALITY-CDE := TWRT-LOCALITY-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Year.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year());                                                                       //Natural: ASSIGN #TWR-TAX-YEAR := TWRT-TAX-YEAR
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Id_Nbr.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id());                                                                       //Natural: ASSIGN #TWR-TAX-ID-NBR := TWRT-TAX-ID
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Contract_Nbr.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr());                                                                 //Natural: ASSIGN #TWR-CONTRACT-NBR := TWRT-CNTRCT-NBR
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Payee_Cde.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde());                                                                     //Natural: ASSIGN #TWR-PAYEE-CDE := TWRT-PAYEE-CDE
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Company_Cde.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde());                                                                 //Natural: ASSIGN #TWR-COMPANY-CDE := TWRT-COMPANY-CDE
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Citizenship.setValue(pnd_Twrpymnt_Tax_Citizenship);                                                                              //Natural: ASSIGN #TWR-TAX-CITIZENSHIP := #TWRPYMNT-TAX-CITIZENSHIP
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Paymt_Category.setValue(pnd_Twrpymnt_Paymt_Category);                                                                                //Natural: ASSIGN #TWR-PAYMT-CATEGORY := #TWRPYMNT-PAYMT-CATEGORY
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Dist_Code.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Distrib_Cde());                                                                   //Natural: ASSIGN #TWR-DIST-CODE := TWRT-DISTRIB-CDE
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Residency_Type.setValue(pnd_Twrpymnt_Residency_Type);                                                                                //Natural: ASSIGN #TWR-RESIDENCY-TYPE := #TWRPYMNT-RESIDENCY-TYPE
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Residency_Code.setValue(ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy());                                                             //Natural: ASSIGN #TWR-RESIDENCY-CODE := TWRT-STATE-RSDNCY
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Locality_Code.setValue(pnd_Twrt_Locality_Cde);                                                                                       //Natural: ASSIGN #TWR-LOCALITY-CODE := #TWRT-LOCALITY-CDE
        vw_oldpay.startDatabaseRead                                                                                                                                       //Natural: READ OLDPAY WITH TWRPYMNT-FORM-SD = #TWRPYMNT-FORM-SD
        (
        "RL1",
        new Wc[] { new Wc("TWRPYMNT_FORM_SD", ">=", pnd_Twrpymnt_Form_Sd, WcType.BY) },
        new Oc[] { new Oc("TWRPYMNT_FORM_SD", "ASC") }
        );
        RL1:
        while (condition(vw_oldpay.readNextRow("RL1")))
        {
            if (condition(pnd_Payment_24.equals(false)))                                                                                                                  //Natural: IF #PAYMENT-24 = FALSE
            {
                if (condition(oldpay_Twrpymnt_Locality_Cde.equals("3E")))                                                                                                 //Natural: IF OLDPAY.TWRPYMNT-LOCALITY-CDE = '3E'
                {
                    pnd_Twrpymnt_Locality_Cde.setValue(" ");                                                                                                              //Natural: ASSIGN #TWRPYMNT-LOCALITY-CDE := ' '
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Twrpymnt_Locality_Cde.setValue(oldpay_Twrpymnt_Locality_Cde);                                                                                     //Natural: ASSIGN #TWRPYMNT-LOCALITY-CDE := OLDPAY.TWRPYMNT-LOCALITY-CDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*                                         /* ROX - 11/10/2014
            //*  FATCA ARE MANUALLY ENTERED
            if (condition(oldpay_Twrpymnt_Fatca_Ind.equals("Y")))                                                                                                         //Natural: IF OLDPAY.TWRPYMNT-FATCA-IND = 'Y'
            {
                //*  WILL NOT BE INCLUDED IN ANY
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  FEED
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Year.equals(oldpay_Twrpymnt_Tax_Year) && pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Id_Nbr.equals(oldpay_Twrpymnt_Tax_Id_Nbr)  //Natural: IF #TWR-TAX-YEAR = OLDPAY.TWRPYMNT-TAX-YEAR AND #TWR-TAX-ID-NBR = OLDPAY.TWRPYMNT-TAX-ID-NBR AND #TWR-CONTRACT-NBR = OLDPAY.TWRPYMNT-CONTRACT-NBR AND #TWR-PAYEE-CDE = OLDPAY.TWRPYMNT-PAYEE-CDE AND #TWR-COMPANY-CDE = OLDPAY.TWRPYMNT-COMPANY-CDE AND #TWR-TAX-CITIZENSHIP = OLDPAY.TWRPYMNT-TAX-CITIZENSHIP AND #TWR-PAYMT-CATEGORY = OLDPAY.TWRPYMNT-PAYMT-CATEGORY AND #TWR-DIST-CODE = OLDPAY.TWRPYMNT-DISTRIBUTION-CDE AND #TWR-RESIDENCY-TYPE = OLDPAY.TWRPYMNT-RESIDENCY-TYPE AND #TWR-RESIDENCY-CODE = OLDPAY.TWRPYMNT-RESIDENCY-CODE AND #TWR-LOCALITY-CODE = #TWRPYMNT-LOCALITY-CDE
                && pnd_Twrpymnt_Form_Sd_Pnd_Twr_Contract_Nbr.equals(oldpay_Twrpymnt_Contract_Nbr) && pnd_Twrpymnt_Form_Sd_Pnd_Twr_Payee_Cde.equals(oldpay_Twrpymnt_Payee_Cde) 
                && pnd_Twrpymnt_Form_Sd_Pnd_Twr_Company_Cde.equals(oldpay_Twrpymnt_Company_Cde) && pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Citizenship.equals(oldpay_Twrpymnt_Tax_Citizenship) 
                && pnd_Twrpymnt_Form_Sd_Pnd_Twr_Paymt_Category.equals(oldpay_Twrpymnt_Paymt_Category) && pnd_Twrpymnt_Form_Sd_Pnd_Twr_Dist_Code.equals(oldpay_Twrpymnt_Distribution_Cde) 
                && pnd_Twrpymnt_Form_Sd_Pnd_Twr_Residency_Type.equals(oldpay_Twrpymnt_Residency_Type) && pnd_Twrpymnt_Form_Sd_Pnd_Twr_Residency_Code.equals(oldpay_Twrpymnt_Residency_Code) 
                && pnd_Twrpymnt_Form_Sd_Pnd_Twr_Locality_Code.equals(pnd_Twrpymnt_Locality_Cde)))
            {
                pnd_Payment_Found.setValue(true);                                                                                                                         //Natural: ASSIGN #PAYMENT-FOUND := TRUE
                pnd_Twrpymnt_Contract_Seq_No.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TWRPYMNT-CONTRACT-SEQ-NO
                if (condition(pnd_Twrpymnt_Contract_Seq_No.equals(1)))                                                                                                    //Natural: IF #TWRPYMNT-CONTRACT-SEQ-NO = 1
                {
                    pnd_First_Seq_No_Isn.setValue(vw_oldpay.getAstISN("RL1"));                                                                                            //Natural: ASSIGN #FIRST-SEQ-NO-ISN := *ISN ( RL1. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Pay_Isn.setValue(vw_oldpay.getAstISN("RL1"));                                                                                                             //Natural: ASSIGN #PAY-ISN := *ISN ( RL1. )
            j.setValue(oldpay_Count_Casttwrpymnt_Payments);                                                                                                               //Natural: ASSIGN J := C*OLDPAY.TWRPYMNT-PAYMENTS
            if (condition(j.greaterOrEqual(24)))                                                                                                                          //Natural: IF J >= 24
            {
                j.setValue(24);                                                                                                                                           //Natural: ASSIGN J := 24
                pnd_Payment_24.setValue(true);                                                                                                                            //Natural: ASSIGN #PAYMENT-24 := TRUE
                pnd_Payment_Found.setValue(false);                                                                                                                        //Natural: ASSIGN #PAYMENT-FOUND := FALSE
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR K = 1 TO J
            for (k.setValue(1); condition(k.lessOrEqual(j)); k.nadd(1))
            {
                //*  $$$$  TO FORCE LOAD PAYMENT DATE FOR TESTING COMMENT THE FOLLOWING
                if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Unique_Id_A().equals(oldpay_Twrpymnt_Gtn_Dte_Time.getValue(k)) && ldaTwrl0702.getTwrt_Record_Twrt_Op_Rev_Ind().equals(" "))) //Natural: IF TWRT-UNIQUE-ID-A = OLDPAY.TWRPYMNT-GTN-DTE-TIME ( K ) AND TWRT-OP-REV-IND = ' '
                {
                    pnd_Bypass_Payment.setValue(true);                                                                                                                    //Natural: ASSIGN #BYPASS-PAYMENT := TRUE
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                //*  $$$$
                //*  09/28/11
                //*  VIKRAM2
                //*  RC07
                if (condition(((((((((((ldaTwrl0702.getTwrt_Record_Twrt_Op_Rev_Ind().equals("X") && ! (pnd_Rev_Orig_Updated.getBoolean())) && oldpay_Twrpymnt_Pymnt_Dte.getValue(k).equals(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha()))  //Natural: IF TWRT-OP-REV-IND = 'X' AND NOT #REV-ORIG-UPDATED AND OLDPAY.TWRPYMNT-PYMNT-DTE ( K ) = TWRT-PAYMT-DTE-ALPHA AND ( OLDPAY.TWRPYMNT-ORGN-SRCE-CDE ( K ) = 'OP' OR = 'NV' OR = 'AM' OR = 'VL' OR = 'EW' OR ( #DEBUG AND OLDPAY.TWRPYMNT-ORGN-SRCE-CDE ( K ) = 'NL' OR = ' ' ) ) AND OLDPAY.TWRPYMNT-PYMNT-STATUS ( K ) = ' ' OR = 'C' AND OLDPAY.TWRPYMNT-GROSS-AMT ( K ) = TWRT-GROSS-AMT AND OLDPAY.TWRPYMNT-IVC-AMT ( K ) = TWRT-IVC-AMT AND OLDPAY.TWRPYMNT-INT-AMT ( K ) = TWRT-INTEREST-AMT AND OLDPAY.TWRPYMNT-FED-WTHLD-AMT ( K ) = TWRT-FED-WHHLD-AMT AND OLDPAY.TWRPYMNT-NRA-WTHLD-AMT ( K ) = TWRT-NRA-WHHLD-AMT AND OLDPAY.TWRPYMNT-STATE-WTHLD ( K ) = TWRT-STATE-WHHLD-AMT
                    && (((((oldpay_Twrpymnt_Orgn_Srce_Cde.getValue(k).equals("OP") || oldpay_Twrpymnt_Orgn_Srce_Cde.getValue(k).equals("NV")) || oldpay_Twrpymnt_Orgn_Srce_Cde.getValue(k).equals("AM")) 
                    || oldpay_Twrpymnt_Orgn_Srce_Cde.getValue(k).equals("VL")) || oldpay_Twrpymnt_Orgn_Srce_Cde.getValue(k).equals("EW")) || (pnd_Debug.getBoolean() 
                    && (oldpay_Twrpymnt_Orgn_Srce_Cde.getValue(k).equals("NL") || oldpay_Twrpymnt_Orgn_Srce_Cde.getValue(k).equals(" "))))) && (oldpay_Twrpymnt_Pymnt_Status.getValue(k).equals(" ") 
                    || oldpay_Twrpymnt_Pymnt_Status.getValue(k).equals("C"))) && oldpay_Twrpymnt_Gross_Amt.getValue(k).equals(ldaTwrl0702.getTwrt_Record_Twrt_Gross_Amt())) 
                    && oldpay_Twrpymnt_Ivc_Amt.getValue(k).equals(ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Amt())) && oldpay_Twrpymnt_Int_Amt.getValue(k).equals(ldaTwrl0702.getTwrt_Record_Twrt_Interest_Amt())) 
                    && oldpay_Twrpymnt_Fed_Wthld_Amt.getValue(k).equals(ldaTwrl0702.getTwrt_Record_Twrt_Fed_Whhld_Amt())) && oldpay_Twrpymnt_Nra_Wthld_Amt.getValue(k).equals(ldaTwrl0702.getTwrt_Record_Twrt_Nra_Whhld_Amt())) 
                    && oldpay_Twrpymnt_State_Wthld.getValue(k).equals(ldaTwrl0702.getTwrt_Record_Twrt_State_Whhld_Amt()))))
                {
                    pnd_Rev_Match.setValue(true);                                                                                                                         //Natural: ASSIGN #REV-MATCH := TRUE
                    G0:                                                                                                                                                   //Natural: GET OLDPAY #PAY-ISN
                    vw_oldpay.readByID(pnd_Pay_Isn.getLong(), "G0");
                    if (condition(pnd_Payment_24.getBoolean()))                                                                                                           //Natural: IF #PAYMENT-24
                    {
                        oldpay_Twrpymnt_Sum_Gross_Amt.nsubtract(oldpay_Twrpymnt_Gross_Amt.getValue(k));                                                                   //Natural: SUBTRACT OLDPAY.TWRPYMNT-GROSS-AMT ( K ) FROM OLDPAY.TWRPYMNT-SUM-GROSS-AMT
                        oldpay_Twrpymnt_Sum_Ivc_Amt.nsubtract(oldpay_Twrpymnt_Ivc_Amt.getValue(k));                                                                       //Natural: SUBTRACT OLDPAY.TWRPYMNT-IVC-AMT ( K ) FROM OLDPAY.TWRPYMNT-SUM-IVC-AMT
                        oldpay_Twrpymnt_Sum_Int_Amt.nsubtract(oldpay_Twrpymnt_Int_Amt.getValue(k));                                                                       //Natural: SUBTRACT OLDPAY.TWRPYMNT-INT-AMT ( K ) FROM OLDPAY.TWRPYMNT-SUM-INT-AMT
                        oldpay_Twrpymnt_Sum_Fed_Wthld.nsubtract(oldpay_Twrpymnt_Fed_Wthld_Amt.getValue(k));                                                               //Natural: SUBTRACT OLDPAY.TWRPYMNT-FED-WTHLD-AMT ( K ) FROM OLDPAY.TWRPYMNT-SUM-FED-WTHLD
                        oldpay_Twrpymnt_Sum_Nra_Wthld.nsubtract(oldpay_Twrpymnt_Nra_Wthld_Amt.getValue(k));                                                               //Natural: SUBTRACT OLDPAY.TWRPYMNT-NRA-WTHLD-AMT ( K ) FROM OLDPAY.TWRPYMNT-SUM-NRA-WTHLD
                        oldpay_Twrpymnt_Sum_Can_Wthld.nsubtract(oldpay_Twrpymnt_Can_Wthld_Amt.getValue(k));                                                               //Natural: SUBTRACT OLDPAY.TWRPYMNT-CAN-WTHLD-AMT ( K ) FROM OLDPAY.TWRPYMNT-SUM-CAN-WTHLD
                        oldpay_Twrpymnt_Sum_State_Wthld.nsubtract(oldpay_Twrpymnt_State_Wthld.getValue(k));                                                               //Natural: SUBTRACT OLDPAY.TWRPYMNT-STATE-WTHLD ( K ) FROM OLDPAY.TWRPYMNT-SUM-STATE-WTHLD
                        oldpay_Twrpymnt_Sum_Local_Wthld.nsubtract(oldpay_Twrpymnt_Local_Wthld.getValue(k));                                                               //Natural: SUBTRACT OLDPAY.TWRPYMNT-LOCAL-WTHLD ( K ) FROM OLDPAY.TWRPYMNT-SUM-LOCAL-WTHLD
                        //*  MARK REVERSAL TARGET "D"
                    }                                                                                                                                                     //Natural: END-IF
                    oldpay_Twrpymnt_Pymnt_Status.getValue(k).setValue("D");                                                                                               //Natural: ASSIGN OLDPAY.TWRPYMNT-PYMNT-STATUS ( K ) := 'D'
                    vw_oldpay.updateDBRow("G0");                                                                                                                          //Natural: UPDATE ( G0. )
                    pnd_Rev_Orig_Updated.setValue(true);                                                                                                                  //Natural: MOVE TRUE TO #REV-ORIG-UPDATED
                    //*      ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RL1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(j.less(24)))                                                                                                                                    //Natural: IF J < 24
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Update_New_Payment() throws Exception                                                                                                                //Natural: UPDATE-NEW-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------
        //*  IF #DEBUG
        //*    WRITE 'BEGIN SUBROUTINE - UPDATE-NEW-PAYMENT'
        //*  END-IF
        pnd_Update_New_Ctr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #UPDATE-NEW-CTR
        pnd_Timn_N.setValue(Global.getTIMN());                                                                                                                            //Natural: ASSIGN #TIMN-N := *TIMN
        pnd_Twrpymnt_Sys_Dte_Time_Pnd_Twrpymnt_Sys_Dte.setValue(Global.getDATN());                                                                                        //Natural: ASSIGN #TWRPYMNT-SYS-DTE := *DATN
        pnd_Twrpymnt_Sys_Dte_Time_Pnd_Twrpymnt_Sys_Time.setValue(pnd_Timn_N_Pnd_Timn_A);                                                                                  //Natural: ASSIGN #TWRPYMNT-SYS-TIME := #TIMN-A
        G1:                                                                                                                                                               //Natural: GET UPDPAY #PAY-ISN
        ldaTwrl820b.getVw_updpay().readByID(pnd_Pay_Isn.getLong(), "G1");
        ldaTwrl820b.getUpdpay_Twrpymnt_Sum_Gross_Amt().nadd(ldaTwrl0702.getTwrt_Record_Twrt_Gross_Amt());                                                                 //Natural: ADD TWRT-GROSS-AMT TO UPDPAY.TWRPYMNT-SUM-GROSS-AMT
        ldaTwrl820b.getUpdpay_Twrpymnt_Sum_Ivc_Amt().nadd(ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Amt());                                                                     //Natural: ADD TWRT-IVC-AMT TO UPDPAY.TWRPYMNT-SUM-IVC-AMT
        ldaTwrl820b.getUpdpay_Twrpymnt_Sum_Int_Amt().nadd(ldaTwrl0702.getTwrt_Record_Twrt_Interest_Amt());                                                                //Natural: ADD TWRT-INTEREST-AMT TO UPDPAY.TWRPYMNT-SUM-INT-AMT
        ldaTwrl820b.getUpdpay_Twrpymnt_Sum_Fed_Wthld().nadd(ldaTwrl0702.getTwrt_Record_Twrt_Fed_Whhld_Amt());                                                             //Natural: ADD TWRT-FED-WHHLD-AMT TO UPDPAY.TWRPYMNT-SUM-FED-WTHLD
        ldaTwrl820b.getUpdpay_Twrpymnt_Sum_Nra_Wthld().nadd(ldaTwrl0702.getTwrt_Record_Twrt_Nra_Whhld_Amt());                                                             //Natural: ADD TWRT-NRA-WHHLD-AMT TO UPDPAY.TWRPYMNT-SUM-NRA-WTHLD
        ldaTwrl820b.getUpdpay_Twrpymnt_Sum_Can_Wthld().nadd(ldaTwrl0702.getTwrt_Record_Twrt_Can_Whhld_Amt());                                                             //Natural: ADD TWRT-CAN-WHHLD-AMT TO UPDPAY.TWRPYMNT-SUM-CAN-WTHLD
        ldaTwrl820b.getUpdpay_Twrpymnt_Sum_State_Wthld().nadd(ldaTwrl0702.getTwrt_Record_Twrt_State_Whhld_Amt());                                                         //Natural: ADD TWRT-STATE-WHHLD-AMT TO UPDPAY.TWRPYMNT-SUM-STATE-WTHLD
        ldaTwrl820b.getUpdpay_Twrpymnt_Sum_Local_Wthld().nadd(ldaTwrl0702.getTwrt_Record_Twrt_Local_Whhld_Amt());                                                         //Natural: ADD TWRT-LOCAL-WHHLD-AMT TO UPDPAY.TWRPYMNT-SUM-LOCAL-WTHLD
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde().equals(" ")))                                                                                        //Natural: IF TWRT-LOCALITY-CDE = ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl820b.getUpdpay_Twrpymnt_Locality_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde());                                                       //Natural: ASSIGN UPDPAY.TWRPYMNT-LOCALITY-CDE := TWRT-LOCALITY-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Total_Distr_Amt().notEquals(getZero())))                                                                            //Natural: IF TWRT-TOTAL-DISTR-AMT NE 0
        {
            ldaTwrl820b.getUpdpay_Twrpymnt_Tot_Dist().setValue("Y");                                                                                                      //Natural: ASSIGN UPDPAY.TWRPYMNT-TOT-DIST := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl820b.getUpdpay_Twrpymnt_Pct_Of_Tot().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Distr_Pct());                                                                //Natural: ASSIGN UPDPAY.TWRPYMNT-PCT-OF-TOT := TWRT-DISTR-PCT
        ldaTwrl820b.getUpdpay_Twrpymnt_Dist_Updte_User().setValue(Global.getINIT_USER());                                                                                 //Natural: ASSIGN UPDPAY.TWRPYMNT-DIST-UPDTE-USER := *INIT-USER
        ldaTwrl820b.getUpdpay_Twrpymnt_Dist_Updte_Dte().setValue(pnd_Twrpymnt_Pymnt_Intfce_Dte);                                                                          //Natural: ASSIGN UPDPAY.TWRPYMNT-DIST-UPDTE-DTE := #TWRPYMNT-PYMNT-INTFCE-DTE
        ldaTwrl820b.getUpdpay_Twrpymnt_Dist_Updte_Time().setValue(pnd_Timn_N_Pnd_Timn_A);                                                                                 //Natural: ASSIGN UPDPAY.TWRPYMNT-DIST-UPDTE-TIME := #TIMN-A
        ldaTwrl820b.getUpdpay_Twrpymnt_Lu_Ts().setValue(Global.getTIMX());                                                                                                //Natural: ASSIGN UPDPAY.TWRPYMNT-LU-TS := *TIMX
        ldaTwrl820b.getUpdpay_Twrpymnt_Updt_Dte().setValue(Global.getDATN());                                                                                             //Natural: ASSIGN UPDPAY.TWRPYMNT-UPDT-DTE := *DATN
        ldaTwrl820b.getUpdpay_Twrpymnt_Updt_Time().setValue(pnd_Timn_N_Pnd_Timn_A);                                                                                       //Natural: ASSIGN UPDPAY.TWRPYMNT-UPDT-TIME := #TIMN-A
        j.nadd(1);                                                                                                                                                        //Natural: ADD 1 TO J
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Cde().equals(ldaTwrl820b.getUpdpay_Twrpymnt_Ivc_Indicator())))                                                  //Natural: IF TWRT-IVC-CDE = UPDPAY.TWRPYMNT-IVC-INDICATOR
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Twrpymnt_Contract_Seq_No.greater(1)))                                                                                                       //Natural: IF #TWRPYMNT-CONTRACT-SEQ-NO > 1
            {
                G2:                                                                                                                                                       //Natural: GET FIRSTPAY #FIRST-SEQ-NO-ISN
                vw_firstpay.readByID(pnd_First_Seq_No_Isn.getLong(), "G2");
                firstpay_Twrpymnt_Ivc_Indicator.setValue("M");                                                                                                            //Natural: ASSIGN FIRSTPAY.TWRPYMNT-IVC-INDICATOR := 'M'
                vw_firstpay.updateDBRow("G2");                                                                                                                            //Natural: UPDATE ( G2. )
                pnd_Et_Ctr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #ET-CTR
                //*   CITED.
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl820b.getUpdpay_Twrpymnt_Ivc_Indicator().setValue("M");                                                                                                 //Natural: ASSIGN UPDPAY.TWRPYMNT-IVC-INDICATOR := 'M'
            ldaTwrl0702.getTwrt_Record_Twrt_Flag().setValue("C");                                                                                                         //Natural: ASSIGN TWRT-FLAG := 'C'
            pnd_Citation_Message_Code.setValue(61);                                                                                                                       //Natural: ASSIGN #CITATION-MESSAGE-CODE := 61
                                                                                                                                                                          //Natural: PERFORM UPDATE-CITATION-TABLE
            sub_Update_Citation_Table();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CITE-PARTICIPANT
            sub_Cite_Participant();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_W_Additions_1_Pnd_W_Contract_Type.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Contract_Type());                                                                  //Natural: ASSIGN #W-CONTRACT-TYPE := TWRT-CONTRACT-TYPE
        pnd_W_Additions_1_Pnd_W_Election_Code.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Election_Code());                                                                  //Natural: ASSIGN #W-ELECTION-CODE := TWRT-ELECTION-CODE
        pnd_W_Additions_3_Pnd_W_Irc_From.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Distributing_Irc_Cde());                                                                //Natural: ASSIGN #W-IRC-FROM := TWRT-DISTRIBUTING-IRC-CDE
        pnd_W_Additions_2_Pnd_W_Irc_To.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Receiving_Irc_Cde());                                                                     //Natural: ASSIGN #W-IRC-TO := TWRT-RECEIVING-IRC-CDE
        ldaTwrl820b.getUpdpay_Twrpymnt_Pymnt_Dte().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte());                                                     //Natural: ASSIGN UPDPAY.TWRPYMNT-PYMNT-DTE ( J ) := TWRT-PAYMT-DTE
        ldaTwrl820b.getUpdpay_Twrpymnt_Pymnt_Status().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Flag());                                                       //Natural: ASSIGN UPDPAY.TWRPYMNT-PYMNT-STATUS ( J ) := TWRT-FLAG
        ldaTwrl820b.getUpdpay_Twrpymnt_Account_Dte().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Acctg_Dte());                                             //Natural: ASSIGN UPDPAY.TWRPYMNT-ACCOUNT-DTE ( J ) := TWRT-PYMNT-ACCTG-DTE
        ldaTwrl820b.getUpdpay_Twrpymnt_Effective_Dte().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Settlmnt_Dte());                                        //Natural: ASSIGN UPDPAY.TWRPYMNT-EFFECTIVE-DTE ( J ) := TWRT-PYMNT-SETTLMNT-DTE
        ldaTwrl820b.getUpdpay_Twrpymnt_Pymnt_Intfce_Dte().getValue(j).setValue(pnd_Twrpymnt_Pymnt_Intfce_Dte);                                                            //Natural: ASSIGN UPDPAY.TWRPYMNT-PYMNT-INTFCE-DTE ( J ) := #TWRPYMNT-PYMNT-INTFCE-DTE
        ldaTwrl820b.getUpdpay_Twrpymnt_Pymnt_Refer_Nbr().getValue(j).setValue(pnd_Twrpymnt_Pymnt_Refer_Nbr);                                                              //Natural: ASSIGN UPDPAY.TWRPYMNT-PYMNT-REFER-NBR ( J ) := #TWRPYMNT-PYMNT-REFER-NBR
        ldaTwrl820b.getUpdpay_Twrpymnt_Dist_Method().getValue(j).setValue(pnd_Twrpymnt_Dist_Method);                                                                      //Natural: ASSIGN UPDPAY.TWRPYMNT-DIST-METHOD ( J ) := #TWRPYMNT-DIST-METHOD
        ldaTwrl820b.getUpdpay_Twrpymnt_Orgn_Srce_Cde().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Source());                                                    //Natural: ASSIGN UPDPAY.TWRPYMNT-ORGN-SRCE-CDE ( J ) := TWRT-SOURCE
        ldaTwrl820b.getUpdpay_Twrpymnt_Payment_Type().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type());                                                 //Natural: ASSIGN UPDPAY.TWRPYMNT-PAYMENT-TYPE ( J ) := TWRT-PAYMT-TYPE
        ldaTwrl820b.getUpdpay_Twrpymnt_Settle_Type().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type());                                                  //Natural: ASSIGN UPDPAY.TWRPYMNT-SETTLE-TYPE ( J ) := TWRT-SETTL-TYPE
        ldaTwrl820b.getUpdpay_Twrpymnt_Frm1001_Ind().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());                                         //Natural: ASSIGN UPDPAY.TWRPYMNT-FRM1001-IND ( J ) := TWRT-PYMNT-TAX-FORM-1001
        ldaTwrl820b.getUpdpay_Twrpymnt_Frm1078_Ind().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078());                                         //Natural: ASSIGN UPDPAY.TWRPYMNT-FRM1078-IND ( J ) := TWRT-PYMNT-TAX-FORM-1078
        ldaTwrl820b.getUpdpay_Twrpymnt_Gross_Amt().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Gross_Amt());                                                     //Natural: ASSIGN UPDPAY.TWRPYMNT-GROSS-AMT ( J ) := TWRT-GROSS-AMT
        ldaTwrl820b.getUpdpay_Twrpymnt_Ivc_Protect().getValue(j).setValue(false);                                                                                         //Natural: ASSIGN UPDPAY.TWRPYMNT-IVC-PROTECT ( J ) := FALSE
        ldaTwrl820b.getUpdpay_Twrpymnt_Ivc_Ind().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Cde());                                                         //Natural: ASSIGN UPDPAY.TWRPYMNT-IVC-IND ( J ) := TWRT-IVC-CDE
        ldaTwrl820b.getUpdpay_Twrpymnt_Ivc_Amt().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Amt());                                                         //Natural: ASSIGN UPDPAY.TWRPYMNT-IVC-AMT ( J ) := TWRT-IVC-AMT
        ldaTwrl820b.getUpdpay_Twrpymnt_Int_Amt().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Interest_Amt());                                                    //Natural: ASSIGN UPDPAY.TWRPYMNT-INT-AMT ( J ) := TWRT-INTEREST-AMT
        ldaTwrl820b.getUpdpay_Twrpymnt_Fed_Wthld_Amt().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Fed_Whhld_Amt());                                             //Natural: ASSIGN UPDPAY.TWRPYMNT-FED-WTHLD-AMT ( J ) := TWRT-FED-WHHLD-AMT
        ldaTwrl820b.getUpdpay_Twrpymnt_Nra_Wthld_Amt().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Nra_Whhld_Amt());                                             //Natural: ASSIGN UPDPAY.TWRPYMNT-NRA-WTHLD-AMT ( J ) := TWRT-NRA-WHHLD-AMT
        ldaTwrl820b.getUpdpay_Twrpymnt_Can_Wthld_Amt().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Can_Whhld_Amt());                                             //Natural: ASSIGN UPDPAY.TWRPYMNT-CAN-WTHLD-AMT ( J ) := TWRT-CAN-WHHLD-AMT
        ldaTwrl820b.getUpdpay_Twrpymnt_State_Wthld().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_State_Whhld_Amt());                                             //Natural: ASSIGN UPDPAY.TWRPYMNT-STATE-WTHLD ( J ) := TWRT-STATE-WHHLD-AMT
        ldaTwrl820b.getUpdpay_Twrpymnt_Local_Wthld().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Local_Whhld_Amt());                                             //Natural: ASSIGN UPDPAY.TWRPYMNT-LOCAL-WTHLD ( J ) := TWRT-LOCAL-WHHLD-AMT
        ldaTwrl820b.getUpdpay_Twrpymnt_Additions_1().getValue(j).setValue(pnd_W_Additions_1);                                                                             //Natural: ASSIGN UPDPAY.TWRPYMNT-ADDITIONS-1 ( J ) := #W-ADDITIONS-1
        //*   05/08/08   RM
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Contract_Type().equals("ROTHP")))                                                                                   //Natural: IF TWRT-CONTRACT-TYPE = 'ROTHP'
        {
            if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Roth_First_Yyyymmdd().notEquals(" ")))                                                                          //Natural: IF TWRT-ROTH-FIRST-YYYYMMDD NE ' '
            {
                ldaTwrl820b.getUpdpay_Twrpymnt_Roth_Year().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Roth_First_Year());                                       //Natural: ASSIGN UPDPAY.TWRPYMNT-ROTH-YEAR ( J ) := TWRT-ROTH-FIRST-YEAR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl820b.getUpdpay_Twrpymnt_Roth_Year().getValue(j).setValue(0);                                                                                       //Natural: ASSIGN UPDPAY.TWRPYMNT-ROTH-YEAR ( J ) := 0
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl820b.getUpdpay_Twrpymnt_Roth_Qual_Ind().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Roth_Qual_Ind());                                         //Natural: ASSIGN UPDPAY.TWRPYMNT-ROTH-QUAL-IND ( J ) := TWRT-ROTH-QUAL-IND
            //*   05/08/08   RM
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl820b.getUpdpay_Twrpymnt_Additions_2().getValue(j).setValue(pnd_W_Additions_2);                                                                             //Natural: ASSIGN UPDPAY.TWRPYMNT-ADDITIONS-2 ( J ) := #W-ADDITIONS-2
        ldaTwrl820b.getUpdpay_Twrpymnt_Additions_3().getValue(j).setValue(pnd_W_Additions_3);                                                                             //Natural: ASSIGN UPDPAY.TWRPYMNT-ADDITIONS-3 ( J ) := #W-ADDITIONS-3
        ldaTwrl820b.getUpdpay_Twrpymnt_Updte_User().getValue(j).setValue(Global.getINIT_USER());                                                                          //Natural: ASSIGN UPDPAY.TWRPYMNT-UPDTE-USER ( J ) := *INIT-USER
        ldaTwrl820b.getUpdpay_Twrpymnt_Sys_Dte_Time().getValue(j).setValue(pnd_Twrpymnt_Sys_Dte_Time);                                                                    //Natural: ASSIGN UPDPAY.TWRPYMNT-SYS-DTE-TIME ( J ) := #TWRPYMNT-SYS-DTE-TIME
        ldaTwrl820b.getUpdpay_Twrpymnt_Gtn_Dte_Time().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Unique_Id());                                                  //Natural: ASSIGN UPDPAY.TWRPYMNT-GTN-DTE-TIME ( J ) := TWRT-UNIQUE-ID
        ldaTwrl820b.getUpdpay_Twrpymnt_Gtn_Ts().getValue(j).setValue(Global.getTIMX());                                                                                   //Natural: ASSIGN UPDPAY.TWRPYMNT-GTN-TS ( J ) := *TIMX
        ldaTwrl820b.getUpdpay_Twrpymnt_Dist_Type().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Distribution_Type());                                             //Natural: ASSIGN UPDPAY.TWRPYMNT-DIST-TYPE ( J ) := TWRT-DISTRIBUTION-TYPE
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type().equals("C") && ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type().equals("X")))                              //Natural: IF TWRT-PAYMT-TYPE = 'C' AND TWRT-SETTL-TYPE = 'X'
        {
            ldaTwrl820b.getUpdpay_Twrpymnt_Check_Reason().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Reason_Code());                                            //Natural: ASSIGN UPDPAY.TWRPYMNT-CHECK-REASON ( J ) := TWRT-REASON-CODE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl820b.getUpdpay_Twrpymnt_Check_Reason().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Check_Reason());                                           //Natural: ASSIGN UPDPAY.TWRPYMNT-CHECK-REASON ( J ) := TWRT-CHECK-REASON
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(ldaTwrl0702.getTwrt_Record_Twrt_Trade_Date(),"YYYYMMDD")))                                                                      //Natural: IF TWRT-TRADE-DATE = MASK ( YYYYMMDD )
        {
            pnd_Trade_Date_A_Pnd_Trade_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaTwrl0702.getTwrt_Record_Twrt_Trade_Date());                                //Natural: MOVE EDITED TWRT-TRADE-DATE TO #TRADE-DATE-D ( EM = YYYYMMDD )
            ldaTwrl820b.getUpdpay_Twrpymnt_Trade_Date().getValue(j).setValue(pnd_Trade_Date_A);                                                                           //Natural: ASSIGN UPDPAY.TWRPYMNT-TRADE-DATE ( J ) := #TRADE-DATE-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl820b.getUpdpay_Twrpymnt_Trade_Date().getValue(j).reset();                                                                                              //Natural: RESET UPDPAY.TWRPYMNT-TRADE-DATE ( J )
            //*  RCC
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl820b.getUpdpay_Twrpymnt_Plan_Type().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Plan_Type());                                                     //Natural: ASSIGN UPDPAY.TWRPYMNT-PLAN-TYPE ( J ) := TWRT-PLAN-TYPE
        ldaTwrl820b.getUpdpay_Twrpymnt_Omni_Plan().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Plan_Num());                                                      //Natural: ASSIGN UPDPAY.TWRPYMNT-OMNI-PLAN ( J ) := TWRT-PLAN-NUM
        ldaTwrl820b.getUpdpay_Twrpymnt_Tax_Type().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Type());                                                       //Natural: ASSIGN UPDPAY.TWRPYMNT-TAX-TYPE ( J ) := TWRT-TAX-TYPE
        ldaTwrl820b.getUpdpay_Twrpymnt_Subplan().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Subplan());                                                         //Natural: ASSIGN UPDPAY.TWRPYMNT-SUBPLAN ( J ) := TWRT-SUBPLAN
        ldaTwrl820b.getUpdpay_Twrpymnt_Orig_Subplan().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Orig_Subplan());                                               //Natural: ASSIGN UPDPAY.TWRPYMNT-ORIG-SUBPLAN ( J ) := TWRT-ORIG-SUBPLAN
        ldaTwrl820b.getUpdpay_Twrpymnt_Orig_Contract_Nbr().getValue(j).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Orig_Contract_Nbr());                                     //Natural: ASSIGN UPDPAY.TWRPYMNT-ORIG-CONTRACT-NBR ( J ) := TWRT-ORIG-CONTRACT-NBR
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Op_Rev_Ind().equals("X")))                                                                                          //Natural: IF TWRT-OP-REV-IND = 'X'
        {
            ldaTwrl820b.getUpdpay_Twrpymnt_Pymnt_Status().getValue(j).setValue("D");                                                                                      //Natural: ASSIGN UPDPAY.TWRPYMNT-PYMNT-STATUS ( J ) := 'D'
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl820b.getVw_updpay().updateDBRow("G1");                                                                                                                     //Natural: UPDATE ( G1. )
        pnd_Et_Ctr.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #ET-CTR
        if (condition(pnd_Et_Ctr.greater(pnd_Et_Const)))                                                                                                                  //Natural: IF #ET-CTR > #ET-CONST
        {
            pnd_Et_Ctr.setValue(0);                                                                                                                                       //Natural: ASSIGN #ET-CTR := 0
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Store_New_Payment() throws Exception                                                                                                                 //Natural: STORE-NEW-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ivc_Ind_Updated.setValue(false);                                                                                                                              //Natural: ASSIGN #IVC-IND-UPDATED := FALSE
        pnd_Twrpymnt_Contract_Seq_No.nadd(1);                                                                                                                             //Natural: ADD 1 TO #TWRPYMNT-CONTRACT-SEQ-NO
        if (condition(pnd_Twrpymnt_Contract_Seq_No.equals(1)))                                                                                                            //Natural: IF #TWRPYMNT-CONTRACT-SEQ-NO = 1
        {
            pnd_Store_Ctr.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #STORE-CTR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Update_New_Ctr.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #UPDATE-NEW-CTR
            G3:                                                                                                                                                           //Natural: GET FIRSTPAY #FIRST-SEQ-NO-ISN
            vw_firstpay.readByID(pnd_First_Seq_No_Isn.getLong(), "G3");
            if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Cde().equals(firstpay_Twrpymnt_Ivc_Indicator)))                                                             //Natural: IF TWRT-IVC-CDE = FIRSTPAY.TWRPYMNT-IVC-INDICATOR
            {
                ignore();
                //*   CITED.
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                firstpay_Twrpymnt_Ivc_Indicator.setValue("M");                                                                                                            //Natural: ASSIGN FIRSTPAY.TWRPYMNT-IVC-INDICATOR := 'M'
                ldaTwrl820a.getNewpay_Twrpymnt_Ivc_Indicator().setValue("M");                                                                                             //Natural: ASSIGN NEWPAY.TWRPYMNT-IVC-INDICATOR := 'M'
                pnd_Ivc_Ind_Updated.setValue(true);                                                                                                                       //Natural: ASSIGN #IVC-IND-UPDATED := TRUE
                ldaTwrl0702.getTwrt_Record_Twrt_Flag().setValue("C");                                                                                                     //Natural: ASSIGN TWRT-FLAG := 'C'
                pnd_Citation_Message_Code.setValue(61);                                                                                                                   //Natural: ASSIGN #CITATION-MESSAGE-CODE := 61
                                                                                                                                                                          //Natural: PERFORM UPDATE-CITATION-TABLE
                sub_Update_Citation_Table();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CITE-PARTICIPANT
                sub_Cite_Participant();
                if (condition(Global.isEscape())) {return;}
                vw_firstpay.updateDBRow("G3");                                                                                                                            //Natural: UPDATE ( G3. )
                pnd_Et_Ctr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #ET-CTR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Timn_N.setValue(Global.getTIMN());                                                                                                                            //Natural: ASSIGN #TIMN-N := *TIMN
        pnd_Twrpymnt_Sys_Dte_Time_Pnd_Twrpymnt_Sys_Dte.setValue(Global.getDATN());                                                                                        //Natural: ASSIGN #TWRPYMNT-SYS-DTE := *DATN
        pnd_Twrpymnt_Sys_Dte_Time_Pnd_Twrpymnt_Sys_Time.setValue(pnd_Timn_N_Pnd_Timn_A);                                                                                  //Natural: ASSIGN #TWRPYMNT-SYS-TIME := #TIMN-A
        pnd_W_Additions_1_Pnd_W_Contract_Type.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Contract_Type());                                                                  //Natural: ASSIGN #W-CONTRACT-TYPE := TWRT-CONTRACT-TYPE
        pnd_W_Additions_1_Pnd_W_Election_Code.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Election_Code());                                                                  //Natural: ASSIGN #W-ELECTION-CODE := TWRT-ELECTION-CODE
        pnd_W_Additions_3_Pnd_W_Irc_From.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Distributing_Irc_Cde());                                                                //Natural: ASSIGN #W-IRC-FROM := TWRT-DISTRIBUTING-IRC-CDE
        pnd_W_Additions_2_Pnd_W_Irc_To.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Receiving_Irc_Cde());                                                                     //Natural: ASSIGN #W-IRC-TO := TWRT-RECEIVING-IRC-CDE
        pnd_Twrpymnt_Contract_Ppcn_Nbr.setValue(" ");                                                                                                                     //Natural: ASSIGN #TWRPYMNT-CONTRACT-PPCN-NBR := ' '
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde().equals("T")))                                                                                         //Natural: IF TWRT-COMPANY-CDE = 'T'
        {
            pnd_Twrpymnt_Contract_Ppcn_Nbr_Pnd_Twrpymnt_Contract_Nbr.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr());                                              //Natural: ASSIGN #TWRPYMNT-CONTRACT-NBR := TWRT-CNTRCT-NBR
            pnd_Twrpymnt_Contract_Ppcn_Nbr_Pnd_Twrpymnt_Payee_Cde.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde());                                                  //Natural: ASSIGN #TWRPYMNT-PAYEE-CDE := TWRT-PAYEE-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ivc_Ind_Updated.equals(true)))                                                                                                                  //Natural: IF #IVC-IND-UPDATED = TRUE
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl820a.getNewpay_Twrpymnt_Ivc_Indicator().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Cde());                                                           //Natural: ASSIGN NEWPAY.TWRPYMNT-IVC-INDICATOR := TWRT-IVC-CDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  09/28/11
        //*  VIKRAM2
        //*  RC07
        if (condition(! (ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("OP") || ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("NV") || ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("AM")  //Natural: IF NOT TWRT-SOURCE = 'OP' OR = 'NV' OR = 'AM' OR = 'VL' OR = 'EW'
            || ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("VL") || ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("EW"))))
        {
            ldaTwrl820a.getNewpay_Twrpymnt_Contract_Lob_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Lob_Cde());                                                        //Natural: ASSIGN NEWPAY.TWRPYMNT-CONTRACT-LOB-CDE := TWRT-LOB-CDE
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl820a.getNewpay_Twrpymnt_Tax_Year().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year());                                                                   //Natural: ASSIGN NEWPAY.TWRPYMNT-TAX-YEAR := TWRT-TAX-YEAR
        ldaTwrl820a.getNewpay_Twrpymnt_Tax_Id_Type().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type());                                                             //Natural: ASSIGN NEWPAY.TWRPYMNT-TAX-ID-TYPE := TWRT-TAX-ID-TYPE
        ldaTwrl820a.getNewpay_Twrpymnt_Tax_Id_Nbr().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id());                                                                   //Natural: ASSIGN NEWPAY.TWRPYMNT-TAX-ID-NBR := TWRT-TAX-ID
        ldaTwrl820a.getNewpay_Twrpymnt_Company_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde());                                                             //Natural: ASSIGN NEWPAY.TWRPYMNT-COMPANY-CDE := TWRT-COMPANY-CDE
        ldaTwrl820a.getNewpay_Twrpymnt_Citizen_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Citizenship());                                                             //Natural: ASSIGN NEWPAY.TWRPYMNT-CITIZEN-CDE := TWRT-CITIZENSHIP
        ldaTwrl820a.getNewpay_Twrpymnt_Tax_Citizenship().setValue(pnd_Twrpymnt_Tax_Citizenship);                                                                          //Natural: ASSIGN NEWPAY.TWRPYMNT-TAX-CITIZENSHIP := #TWRPYMNT-TAX-CITIZENSHIP
        ldaTwrl820a.getNewpay_Twrpymnt_Contract_Nbr().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr());                                                             //Natural: ASSIGN NEWPAY.TWRPYMNT-CONTRACT-NBR := TWRT-CNTRCT-NBR
        ldaTwrl820a.getNewpay_Twrpymnt_Payee_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde());                                                                 //Natural: ASSIGN NEWPAY.TWRPYMNT-PAYEE-CDE := TWRT-PAYEE-CDE
        ldaTwrl820a.getNewpay_Twrpymnt_Contract_Seq_No().setValue(pnd_Twrpymnt_Contract_Seq_No);                                                                          //Natural: ASSIGN NEWPAY.TWRPYMNT-CONTRACT-SEQ-NO := #TWRPYMNT-CONTRACT-SEQ-NO
        ldaTwrl820a.getNewpay_Twrpymnt_Distribution_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Distrib_Cde());                                                        //Natural: ASSIGN NEWPAY.TWRPYMNT-DISTRIBUTION-CDE := TWRT-DISTRIB-CDE
        ldaTwrl820a.getNewpay_Twrpymnt_Residency_Type().setValue(pnd_Twrpymnt_Residency_Type);                                                                            //Natural: ASSIGN NEWPAY.TWRPYMNT-RESIDENCY-TYPE := #TWRPYMNT-RESIDENCY-TYPE
        ldaTwrl820a.getNewpay_Twrpymnt_Residency_Code().setValue(ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy());                                                         //Natural: ASSIGN NEWPAY.TWRPYMNT-RESIDENCY-CODE := TWRT-STATE-RSDNCY
        ldaTwrl820a.getNewpay_Twrpymnt_Locality_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde());                                                           //Natural: ASSIGN NEWPAY.TWRPYMNT-LOCALITY-CDE := TWRT-LOCALITY-CDE
        ldaTwrl820a.getNewpay_Twrpymnt_Currency_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Currency_Cde());                                                           //Natural: ASSIGN NEWPAY.TWRPYMNT-CURRENCY-CDE := TWRT-CURRENCY-CDE
        ldaTwrl820a.getNewpay_Twrpymnt_Paymt_Category().setValue(pnd_Twrpymnt_Paymt_Category);                                                                            //Natural: ASSIGN NEWPAY.TWRPYMNT-PAYMT-CATEGORY := #TWRPYMNT-PAYMT-CATEGORY
        ldaTwrl820a.getNewpay_Twrpymnt_Contract_Ppcn_Nbr().setValue(pnd_Twrpymnt_Contract_Ppcn_Nbr);                                                                      //Natural: ASSIGN NEWPAY.TWRPYMNT-CONTRACT-PPCN-NBR := #TWRPYMNT-CONTRACT-PPCN-NBR
        ldaTwrl820a.getNewpay_Twrpymnt_Tot_Contractual_Ivc().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Tot_Ivc_9b_Amt());                                                  //Natural: ASSIGN NEWPAY.TWRPYMNT-TOT-CONTRACTUAL-IVC := TWRT-TOT-IVC-9B-AMT
        ldaTwrl820a.getNewpay_Twrpymnt_Updt_Dte().setValue(Global.getDATN());                                                                                             //Natural: ASSIGN NEWPAY.TWRPYMNT-UPDT-DTE := *DATN
        ldaTwrl820a.getNewpay_Twrpymnt_Updt_Time().setValue(pnd_Timn_N_Pnd_Timn_A);                                                                                       //Natural: ASSIGN NEWPAY.TWRPYMNT-UPDT-TIME := #TIMN-A
        ldaTwrl820a.getNewpay_Twrpymnt_Lu_Ts().setValue(Global.getTIMX());                                                                                                //Natural: ASSIGN NEWPAY.TWRPYMNT-LU-TS := *TIMX
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Op_Rev_Ind().equals("X")))                                                                                          //Natural: IF TWRT-OP-REV-IND = 'X'
        {
            ldaTwrl820a.getNewpay_Twrpymnt_Pymnt_Status().getValue(1).setValue("D");                                                                                      //Natural: ASSIGN NEWPAY.TWRPYMNT-PYMNT-STATUS ( 1 ) := 'D'
            ldaTwrl820a.getNewpay_Twrpymnt_Sum_Gross_Amt().reset();                                                                                                       //Natural: RESET NEWPAY.TWRPYMNT-SUM-GROSS-AMT NEWPAY.TWRPYMNT-SUM-IVC-AMT NEWPAY.TWRPYMNT-SUM-INT-AMT NEWPAY.TWRPYMNT-SUM-FED-WTHLD NEWPAY.TWRPYMNT-SUM-NRA-WTHLD NEWPAY.TWRPYMNT-SUM-CAN-WTHLD NEWPAY.TWRPYMNT-SUM-STATE-WTHLD NEWPAY.TWRPYMNT-SUM-LOCAL-WTHLD
            ldaTwrl820a.getNewpay_Twrpymnt_Sum_Ivc_Amt().reset();
            ldaTwrl820a.getNewpay_Twrpymnt_Sum_Int_Amt().reset();
            ldaTwrl820a.getNewpay_Twrpymnt_Sum_Fed_Wthld().reset();
            ldaTwrl820a.getNewpay_Twrpymnt_Sum_Nra_Wthld().reset();
            ldaTwrl820a.getNewpay_Twrpymnt_Sum_Can_Wthld().reset();
            ldaTwrl820a.getNewpay_Twrpymnt_Sum_State_Wthld().reset();
            ldaTwrl820a.getNewpay_Twrpymnt_Sum_Local_Wthld().reset();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl820a.getNewpay_Twrpymnt_Sum_Gross_Amt().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Gross_Amt());                                                         //Natural: ASSIGN NEWPAY.TWRPYMNT-SUM-GROSS-AMT := TWRT-GROSS-AMT
            ldaTwrl820a.getNewpay_Twrpymnt_Sum_Ivc_Amt().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Amt());                                                             //Natural: ASSIGN NEWPAY.TWRPYMNT-SUM-IVC-AMT := TWRT-IVC-AMT
            ldaTwrl820a.getNewpay_Twrpymnt_Sum_Int_Amt().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Interest_Amt());                                                        //Natural: ASSIGN NEWPAY.TWRPYMNT-SUM-INT-AMT := TWRT-INTEREST-AMT
            ldaTwrl820a.getNewpay_Twrpymnt_Sum_Fed_Wthld().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Fed_Whhld_Amt());                                                     //Natural: ASSIGN NEWPAY.TWRPYMNT-SUM-FED-WTHLD := TWRT-FED-WHHLD-AMT
            ldaTwrl820a.getNewpay_Twrpymnt_Sum_Nra_Wthld().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Nra_Whhld_Amt());                                                     //Natural: ASSIGN NEWPAY.TWRPYMNT-SUM-NRA-WTHLD := TWRT-NRA-WHHLD-AMT
            ldaTwrl820a.getNewpay_Twrpymnt_Sum_Can_Wthld().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Can_Whhld_Amt());                                                     //Natural: ASSIGN NEWPAY.TWRPYMNT-SUM-CAN-WTHLD := TWRT-CAN-WHHLD-AMT
            ldaTwrl820a.getNewpay_Twrpymnt_Sum_State_Wthld().setValue(ldaTwrl0702.getTwrt_Record_Twrt_State_Whhld_Amt());                                                 //Natural: ASSIGN NEWPAY.TWRPYMNT-SUM-STATE-WTHLD := TWRT-STATE-WHHLD-AMT
            ldaTwrl820a.getNewpay_Twrpymnt_Sum_Local_Wthld().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Local_Whhld_Amt());                                                 //Natural: ASSIGN NEWPAY.TWRPYMNT-SUM-LOCAL-WTHLD := TWRT-LOCAL-WHHLD-AMT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Total_Distr_Amt().notEquals(getZero())))                                                                            //Natural: IF TWRT-TOTAL-DISTR-AMT NE 0
        {
            ldaTwrl820a.getNewpay_Twrpymnt_Tot_Dist().setValue("Y");                                                                                                      //Natural: ASSIGN NEWPAY.TWRPYMNT-TOT-DIST := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl820a.getNewpay_Twrpymnt_Pct_Of_Tot().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Distr_Pct());                                                                //Natural: ASSIGN NEWPAY.TWRPYMNT-PCT-OF-TOT := TWRT-DISTR-PCT
        ldaTwrl820a.getNewpay_Twrpymnt_Dist_Updte_User().setValue(Global.getINIT_USER());                                                                                 //Natural: ASSIGN NEWPAY.TWRPYMNT-DIST-UPDTE-USER := *INIT-USER
        ldaTwrl820a.getNewpay_Twrpymnt_Dist_Updte_Dte().setValue(pnd_Twrpymnt_Pymnt_Intfce_Dte);                                                                          //Natural: ASSIGN NEWPAY.TWRPYMNT-DIST-UPDTE-DTE := #TWRPYMNT-PYMNT-INTFCE-DTE
        ldaTwrl820a.getNewpay_Twrpymnt_Dist_Updte_Time().setValue(pnd_Timn_N_Pnd_Timn_A);                                                                                 //Natural: ASSIGN NEWPAY.TWRPYMNT-DIST-UPDTE-TIME := #TIMN-A
        ldaTwrl820a.getNewpay_Twrpymnt_Pymnt_Dte().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte());                                                     //Natural: ASSIGN NEWPAY.TWRPYMNT-PYMNT-DTE ( 1 ) := TWRT-PAYMT-DTE
        ldaTwrl820a.getNewpay_Twrpymnt_Account_Dte().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Acctg_Dte());                                             //Natural: ASSIGN NEWPAY.TWRPYMNT-ACCOUNT-DTE ( 1 ) := TWRT-PYMNT-ACCTG-DTE
        ldaTwrl820a.getNewpay_Twrpymnt_Effective_Dte().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Settlmnt_Dte());                                        //Natural: ASSIGN NEWPAY.TWRPYMNT-EFFECTIVE-DTE ( 1 ) := TWRT-PYMNT-SETTLMNT-DTE
        ldaTwrl820a.getNewpay_Twrpymnt_Pymnt_Intfce_Dte().getValue(1).setValue(pnd_Twrpymnt_Pymnt_Intfce_Dte);                                                            //Natural: ASSIGN NEWPAY.TWRPYMNT-PYMNT-INTFCE-DTE ( 1 ) := #TWRPYMNT-PYMNT-INTFCE-DTE
        ldaTwrl820a.getNewpay_Twrpymnt_Pymnt_Refer_Nbr().getValue(1).setValue(pnd_Twrpymnt_Pymnt_Refer_Nbr);                                                              //Natural: ASSIGN NEWPAY.TWRPYMNT-PYMNT-REFER-NBR ( 1 ) := #TWRPYMNT-PYMNT-REFER-NBR
        ldaTwrl820a.getNewpay_Twrpymnt_Dist_Method().getValue(1).setValue(pnd_Twrpymnt_Dist_Method);                                                                      //Natural: ASSIGN NEWPAY.TWRPYMNT-DIST-METHOD ( 1 ) := #TWRPYMNT-DIST-METHOD
        ldaTwrl820a.getNewpay_Twrpymnt_Orgn_Srce_Cde().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Source());                                                    //Natural: ASSIGN NEWPAY.TWRPYMNT-ORGN-SRCE-CDE ( 1 ) := TWRT-SOURCE
        ldaTwrl820a.getNewpay_Twrpymnt_Payment_Type().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type());                                                 //Natural: ASSIGN NEWPAY.TWRPYMNT-PAYMENT-TYPE ( 1 ) := TWRT-PAYMT-TYPE
        ldaTwrl820a.getNewpay_Twrpymnt_Settle_Type().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type());                                                  //Natural: ASSIGN NEWPAY.TWRPYMNT-SETTLE-TYPE ( 1 ) := TWRT-SETTL-TYPE
        ldaTwrl820a.getNewpay_Twrpymnt_Frm1001_Ind().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());                                         //Natural: ASSIGN NEWPAY.TWRPYMNT-FRM1001-IND ( 1 ) := TWRT-PYMNT-TAX-FORM-1001
        ldaTwrl820a.getNewpay_Twrpymnt_Frm1078_Ind().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078());                                         //Natural: ASSIGN NEWPAY.TWRPYMNT-FRM1078-IND ( 1 ) := TWRT-PYMNT-TAX-FORM-1078
        ldaTwrl820a.getNewpay_Twrpymnt_Gross_Amt().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Gross_Amt());                                                     //Natural: ASSIGN NEWPAY.TWRPYMNT-GROSS-AMT ( 1 ) := TWRT-GROSS-AMT
        ldaTwrl820a.getNewpay_Twrpymnt_Ivc_Protect().getValue(1).setValue(false);                                                                                         //Natural: ASSIGN NEWPAY.TWRPYMNT-IVC-PROTECT ( 1 ) := FALSE
        ldaTwrl820a.getNewpay_Twrpymnt_Ivc_Ind().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Cde());                                                         //Natural: ASSIGN NEWPAY.TWRPYMNT-IVC-IND ( 1 ) := TWRT-IVC-CDE
        ldaTwrl820a.getNewpay_Twrpymnt_Ivc_Amt().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Amt());                                                         //Natural: ASSIGN NEWPAY.TWRPYMNT-IVC-AMT ( 1 ) := TWRT-IVC-AMT
        ldaTwrl820a.getNewpay_Twrpymnt_Int_Amt().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Interest_Amt());                                                    //Natural: ASSIGN NEWPAY.TWRPYMNT-INT-AMT ( 1 ) := TWRT-INTEREST-AMT
        ldaTwrl820a.getNewpay_Twrpymnt_Fed_Wthld_Amt().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Fed_Whhld_Amt());                                             //Natural: ASSIGN NEWPAY.TWRPYMNT-FED-WTHLD-AMT ( 1 ) := TWRT-FED-WHHLD-AMT
        ldaTwrl820a.getNewpay_Twrpymnt_Nra_Wthld_Amt().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Nra_Whhld_Amt());                                             //Natural: ASSIGN NEWPAY.TWRPYMNT-NRA-WTHLD-AMT ( 1 ) := TWRT-NRA-WHHLD-AMT
        ldaTwrl820a.getNewpay_Twrpymnt_Can_Wthld_Amt().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Can_Whhld_Amt());                                             //Natural: ASSIGN NEWPAY.TWRPYMNT-CAN-WTHLD-AMT ( 1 ) := TWRT-CAN-WHHLD-AMT
        ldaTwrl820a.getNewpay_Twrpymnt_State_Wthld().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_State_Whhld_Amt());                                             //Natural: ASSIGN NEWPAY.TWRPYMNT-STATE-WTHLD ( 1 ) := TWRT-STATE-WHHLD-AMT
        ldaTwrl820a.getNewpay_Twrpymnt_Local_Wthld().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Local_Whhld_Amt());                                             //Natural: ASSIGN NEWPAY.TWRPYMNT-LOCAL-WTHLD ( 1 ) := TWRT-LOCAL-WHHLD-AMT
        ldaTwrl820a.getNewpay_Twrpymnt_Additions_1().getValue(1).setValue(pnd_W_Additions_1);                                                                             //Natural: ASSIGN NEWPAY.TWRPYMNT-ADDITIONS-1 ( 1 ) := #W-ADDITIONS-1
        ldaTwrl820a.getNewpay_Twrpymnt_Additions_2().getValue(1).setValue(pnd_W_Additions_2);                                                                             //Natural: ASSIGN NEWPAY.TWRPYMNT-ADDITIONS-2 ( 1 ) := #W-ADDITIONS-2
        ldaTwrl820a.getNewpay_Twrpymnt_Additions_3().getValue(1).setValue(pnd_W_Additions_3);                                                                             //Natural: ASSIGN NEWPAY.TWRPYMNT-ADDITIONS-3 ( 1 ) := #W-ADDITIONS-3
        ldaTwrl820a.getNewpay_Twrpymnt_Updte_User().getValue(1).setValue(Global.getINIT_USER());                                                                          //Natural: ASSIGN NEWPAY.TWRPYMNT-UPDTE-USER ( 1 ) := *INIT-USER
        ldaTwrl820a.getNewpay_Twrpymnt_Sys_Dte_Time().getValue(1).setValue(pnd_Twrpymnt_Sys_Dte_Time);                                                                    //Natural: ASSIGN NEWPAY.TWRPYMNT-SYS-DTE-TIME ( 1 ) := #TWRPYMNT-SYS-DTE-TIME
        ldaTwrl820a.getNewpay_Twrpymnt_Gtn_Dte_Time().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Unique_Id());                                                  //Natural: ASSIGN NEWPAY.TWRPYMNT-GTN-DTE-TIME ( 1 ) := TWRT-UNIQUE-ID
        ldaTwrl820a.getNewpay_Twrpymnt_Gtn_Ts().getValue(1).setValue(Global.getTIMX());                                                                                   //Natural: ASSIGN NEWPAY.TWRPYMNT-GTN-TS ( 1 ) := *TIMX
        ldaTwrl820a.getNewpay_Twrpymnt_Dist_Type().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Distribution_Type());                                             //Natural: ASSIGN NEWPAY.TWRPYMNT-DIST-TYPE ( 1 ) := TWRT-DISTRIBUTION-TYPE
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type().equals("C") && ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type().equals("X")))                              //Natural: IF TWRT-PAYMT-TYPE = 'C' AND TWRT-SETTL-TYPE = 'X'
        {
            ldaTwrl820a.getNewpay_Twrpymnt_Check_Reason().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Reason_Code());                                            //Natural: ASSIGN NEWPAY.TWRPYMNT-CHECK-REASON ( 1 ) := TWRT-REASON-CODE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl820a.getNewpay_Twrpymnt_Check_Reason().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Check_Reason());                                           //Natural: ASSIGN NEWPAY.TWRPYMNT-CHECK-REASON ( 1 ) := TWRT-CHECK-REASON
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(ldaTwrl0702.getTwrt_Record_Twrt_Trade_Date(),"YYYYMMDD")))                                                                      //Natural: IF TWRT-TRADE-DATE = MASK ( YYYYMMDD )
        {
            pnd_Trade_Date_A_Pnd_Trade_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaTwrl0702.getTwrt_Record_Twrt_Trade_Date());                                //Natural: MOVE EDITED TWRT-TRADE-DATE TO #TRADE-DATE-D ( EM = YYYYMMDD )
            ldaTwrl820a.getNewpay_Twrpymnt_Trade_Date().getValue(1).setValue(pnd_Trade_Date_A);                                                                           //Natural: ASSIGN NEWPAY.TWRPYMNT-TRADE-DATE ( 1 ) := #TRADE-DATE-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl820b.getUpdpay_Twrpymnt_Trade_Date().getValue(1).reset();                                                                                              //Natural: RESET UPDPAY.TWRPYMNT-TRADE-DATE ( 1 )
            //*  RCC
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl820a.getNewpay_Twrpymnt_Check_Reason().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Reason_Code());                                                //Natural: ASSIGN NEWPAY.TWRPYMNT-CHECK-REASON ( 1 ) := TWRT-REASON-CODE
        ldaTwrl820a.getNewpay_Twrpymnt_Plan_Type().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Plan_Type());                                                     //Natural: ASSIGN NEWPAY.TWRPYMNT-PLAN-TYPE ( 1 ) := TWRT-PLAN-TYPE
        ldaTwrl820a.getNewpay_Twrpymnt_Omni_Plan().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Plan_Num());                                                      //Natural: ASSIGN NEWPAY.TWRPYMNT-OMNI-PLAN ( 1 ) := TWRT-PLAN-NUM
        ldaTwrl820a.getNewpay_Twrpymnt_Tax_Type().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Type());                                                       //Natural: ASSIGN NEWPAY.TWRPYMNT-TAX-TYPE ( 1 ) := TWRT-TAX-TYPE
        ldaTwrl820a.getNewpay_Twrpymnt_Subplan().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Subplan());                                                         //Natural: ASSIGN NEWPAY.TWRPYMNT-SUBPLAN ( 1 ) := TWRT-SUBPLAN
        ldaTwrl820a.getNewpay_Twrpymnt_Orig_Subplan().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Orig_Subplan());                                               //Natural: ASSIGN NEWPAY.TWRPYMNT-ORIG-SUBPLAN ( 1 ) := TWRT-ORIG-SUBPLAN
        ldaTwrl820a.getNewpay_Twrpymnt_Orig_Contract_Nbr().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Orig_Contract_Nbr());                                     //Natural: ASSIGN NEWPAY.TWRPYMNT-ORIG-CONTRACT-NBR ( 1 ) := TWRT-ORIG-CONTRACT-NBR
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Op_Rev_Ind().notEquals("X")))                                                                                       //Natural: IF TWRT-OP-REV-IND NE 'X'
        {
            ldaTwrl820a.getNewpay_Twrpymnt_Pymnt_Status().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Flag());                                                   //Natural: ASSIGN NEWPAY.TWRPYMNT-PYMNT-STATUS ( 1 ) := TWRT-FLAG
        }                                                                                                                                                                 //Natural: END-IF
        //*   05/08/08   RM
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Contract_Type().equals("ROTHP")))                                                                                   //Natural: IF TWRT-CONTRACT-TYPE = 'ROTHP'
        {
            if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Roth_First_Yyyymmdd().notEquals(" ")))                                                                          //Natural: IF TWRT-ROTH-FIRST-YYYYMMDD NE ' '
            {
                ldaTwrl820a.getNewpay_Twrpymnt_Roth_Year().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Roth_First_Year());                                       //Natural: ASSIGN NEWPAY.TWRPYMNT-ROTH-YEAR ( 1 ) := TWRT-ROTH-FIRST-YEAR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl820a.getNewpay_Twrpymnt_Roth_Year().getValue(1).setValue(0);                                                                                       //Natural: ASSIGN NEWPAY.TWRPYMNT-ROTH-YEAR ( 1 ) := 0
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl820a.getNewpay_Twrpymnt_Roth_Qual_Ind().getValue(1).setValue(ldaTwrl0702.getTwrt_Record_Twrt_Roth_Qual_Ind());                                         //Natural: ASSIGN NEWPAY.TWRPYMNT-ROTH-QUAL-IND ( 1 ) := TWRT-ROTH-QUAL-IND
            //*   05/08/08   RM
        }                                                                                                                                                                 //Natural: END-IF
        ST1:                                                                                                                                                              //Natural: STORE NEWPAY
        ldaTwrl820a.getVw_newpay().insertDBRow("ST1");
        pnd_Pay_Isn.setValue(ldaTwrl820a.getVw_newpay().getAstISN("ST1"));                                                                                                //Natural: ASSIGN #PAY-ISN := *ISN ( ST1. )
        pnd_Et_Ctr.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #ET-CTR
        if (condition(pnd_Et_Ctr.greater(pnd_Et_Const)))                                                                                                                  //Natural: IF #ET-CTR > #ET-CONST
        {
            pnd_Et_Ctr.setValue(0);                                                                                                                                       //Natural: ASSIGN #ET-CTR := 0
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Lookup_Residency_Type() throws Exception                                                                                                             //Natural: LOOKUP-RESIDENCY-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Twrpymnt_Residency_Type.setValue(" ");                                                                                                                        //Natural: ASSIGN #TWRPYMNT-RESIDENCY-TYPE := ' '
        pnd_Us_Resident.setValue(false);                                                                                                                                  //Natural: ASSIGN #US-RESIDENT := FALSE
        //*  CANADA
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("CA")))                                                                                       //Natural: IF TWRT-STATE-RSDNCY = 'CA'
        {
            pnd_Twrpymnt_Residency_Type.setValue("4");                                                                                                                    //Natural: ASSIGN #TWRPYMNT-RESIDENCY-TYPE := '4'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  COMMONWEALTH OF PUERTO RICO
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("42") || ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("PR") || ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("RQ"))) //Natural: IF TWRT-STATE-RSDNCY = '42' OR = 'PR' OR = 'RQ'
        {
            pnd_Twrpymnt_Residency_Type.setValue("3");                                                                                                                    //Natural: ASSIGN #TWRPYMNT-RESIDENCY-TYPE := '3'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  PROVINCE OF CANADA
        if (condition(((ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().greaterOrEqual("74") && ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().lessOrEqual("88"))          //Natural: IF TWRT-STATE-RSDNCY = '74' THRU '88' OR TWRT-STATE-RSDNCY = '96'
            || ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("96"))))
        {
            pnd_Twrpymnt_Residency_Type.setValue("5");                                                                                                                    //Natural: ASSIGN #TWRPYMNT-RESIDENCY-TYPE := '5'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  STATE
        if (condition((((ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().greaterOrEqual("00") && ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().lessOrEqual("41"))         //Natural: IF TWRT-STATE-RSDNCY = '00' THRU '41' OR TWRT-STATE-RSDNCY = '43' THRU '57' OR TWRT-STATE-RSDNCY = '97' OR = 'US'
            || (ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().greaterOrEqual("43") && ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().lessOrEqual("57"))) 
            || (ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("97") || ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("US")))))
        {
            pnd_Twrpymnt_Residency_Type.setValue("1");                                                                                                                    //Natural: ASSIGN #TWRPYMNT-RESIDENCY-TYPE := '1'
            pnd_Us_Resident.setValue(true);                                                                                                                               //Natural: ASSIGN #US-RESIDENT := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  COUNTRY
        if (condition(DbsUtil.maskMatches(ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"AA") || ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("98")             //Natural: IF TWRT-STATE-RSDNCY = MASK ( AA ) OR = '98' OR = '99'
            || ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("99")))
        {
            pnd_Twrpymnt_Residency_Type.setValue("2");                                                                                                                    //Natural: ASSIGN #TWRPYMNT-RESIDENCY-TYPE := '2'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Lookup_Citizenship_Code() throws Exception                                                                                                           //Natural: LOOKUP-CITIZENSHIP-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Citizenship().equals("01") || ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().equals("Y")))                   //Natural: IF TWRT-CITIZENSHIP = '01' OR TWRT-PYMNT-TAX-FORM-1078 = 'Y'
        {
            pnd_Twrpymnt_Tax_Citizenship.setValue("U");                                                                                                                   //Natural: ASSIGN #TWRPYMNT-TAX-CITIZENSHIP := 'U'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Twrpymnt_Tax_Citizenship.setValue("F");                                                                                                                   //Natural: ASSIGN #TWRPYMNT-TAX-CITIZENSHIP := 'F'
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Lookup_Dist_Code_Table() throws Exception                                                                                                            //Natural: LOOKUP-DIST-CODE-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------------
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Type_Refer().equals(" ")))                                                                                          //Natural: IF TWRT-TYPE-REFER = ' '
        {
            pnd_Twrpymnt_Pymnt_Refer_Nbr.setValue("  ");                                                                                                                  //Natural: ASSIGN #TWRPYMNT-PYMNT-REFER-NBR := '  '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Twrpymnt_Pymnt_Refer_Nbr.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Type_Refer());                                                                          //Natural: ASSIGN #TWRPYMNT-PYMNT-REFER-NBR := TWRT-TYPE-REFER
            //*   1/P, 2/R, OR 3/L
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Twrpymnt_Paymt_Category.setValue(" ");                                                                                                                        //Natural: ASSIGN #TWRPYMNT-PAYMT-CATEGORY := ' '
        pnd_Twrpymnt_Dist_Method.setValue(" ");                                                                                                                           //Natural: ASSIGN #TWRPYMNT-DIST-METHOD := ' '
        pnd_Super_Dist_Code_Pnd_S_Tbl_Nbr.setValue(1);                                                                                                                    //Natural: ASSIGN #S-TBL-NBR := 1
        pnd_Super_Dist_Code_Pnd_S_Tax_Year.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year());                                                                          //Natural: ASSIGN #S-TAX-YEAR := TWRT-TAX-YEAR
        pnd_Super_Dist_Code_Pnd_S_Settl_Type.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type());                                                                      //Natural: ASSIGN #S-SETTL-TYPE := TWRT-SETTL-TYPE
        pnd_Super_Dist_Code_Pnd_S_Pay_Type.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type());                                                                        //Natural: ASSIGN #S-PAY-TYPE := TWRT-PAYMT-TYPE
        pnd_Super_Dist_Code_Pnd_S_Dist_Code.setValue(" ");                                                                                                                //Natural: ASSIGN #S-DIST-CODE := ' '
        ldaTwrl820d.getVw_dist_Tbl().startDatabaseRead                                                                                                                    //Natural: READ ( 1 ) DIST-TBL WITH TIRCNTL-NBR-YEAR-DISTR-CODE = #SUPER-DIST-CODE
        (
        "READ02",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_DISTR_CODE", ">=", pnd_Super_Dist_Code, WcType.BY) },
        new Oc[] { new Oc("TIRCNTL_NBR_YEAR_DISTR_CODE", "ASC") },
        1
        );
        READ02:
        while (condition(ldaTwrl820d.getVw_dist_Tbl().readNextRow("READ02")))
        {
            if (condition(pnd_Super_Dist_Code_Pnd_S_Tbl_Nbr.equals(ldaTwrl820d.getDist_Tbl_Tircntl_Tbl_Nbr()) && pnd_Super_Dist_Code_Pnd_S_Tax_Year.equals(ldaTwrl820d.getDist_Tbl_Tircntl_Tax_Year())  //Natural: IF #S-TBL-NBR = DIST-TBL.TIRCNTL-TBL-NBR AND #S-TAX-YEAR = DIST-TBL.TIRCNTL-TAX-YEAR AND #S-SETTL-TYPE = DIST-TBL.TIRCNTL-SETTL-TYPE AND #S-PAY-TYPE = DIST-TBL.TIRCNTL-PAYMT-TYPE
                && pnd_Super_Dist_Code_Pnd_S_Settl_Type.equals(ldaTwrl820d.getDist_Tbl_Tircntl_Settl_Type()) && pnd_Super_Dist_Code_Pnd_S_Pay_Type.equals(ldaTwrl820d.getDist_Tbl_Tircntl_Paymt_Type())))
            {
                if (condition(pnd_Twrpymnt_Pymnt_Refer_Nbr.equals("  ")))                                                                                                 //Natural: IF #TWRPYMNT-PYMNT-REFER-NBR = '  '
                {
                    pnd_Twrpymnt_Pymnt_Refer_Nbr.setValue(ldaTwrl820d.getDist_Tbl_Tircntl_Type_Refer());                                                                  //Natural: ASSIGN #TWRPYMNT-PYMNT-REFER-NBR := DIST-TBL.TIRCNTL-TYPE-REFER
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet1801 = 0;                                                                                                                        //Natural: DECIDE ON FIRST DIST-TBL.TIRCNTL-PAYMT-CATEGORY;//Natural: VALUE 'P'
                if (condition((ldaTwrl820d.getDist_Tbl_Tircntl_Paymt_Category().equals("P"))))
                {
                    decideConditionsMet1801++;
                    pnd_Twrpymnt_Paymt_Category.setValue("1");                                                                                                            //Natural: ASSIGN #TWRPYMNT-PAYMT-CATEGORY := '1'
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((ldaTwrl820d.getDist_Tbl_Tircntl_Paymt_Category().equals("R"))))
                {
                    decideConditionsMet1801++;
                    pnd_Twrpymnt_Paymt_Category.setValue("2");                                                                                                            //Natural: ASSIGN #TWRPYMNT-PAYMT-CATEGORY := '2'
                }                                                                                                                                                         //Natural: VALUE 'L'
                else if (condition((ldaTwrl820d.getDist_Tbl_Tircntl_Paymt_Category().equals("L"))))
                {
                    decideConditionsMet1801++;
                    pnd_Twrpymnt_Paymt_Category.setValue("3");                                                                                                            //Natural: ASSIGN #TWRPYMNT-PAYMT-CATEGORY := '3'
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Search_Cat_Dist_Pnd_Search_Cat.setValue(ldaTwrl820d.getDist_Tbl_Tircntl_Paymt_Category());                                                            //Natural: ASSIGN #SEARCH-CAT := DIST-TBL.TIRCNTL-PAYMT-CATEGORY
                pnd_Search_Cat_Dist_Pnd_Search_Dist.setValue(ldaTwrl820d.getDist_Tbl_Tircntl_Dist_Method());                                                              //Natural: ASSIGN #SEARCH-DIST := DIST-TBL.TIRCNTL-DIST-METHOD
                FOR02:                                                                                                                                                    //Natural: FOR I = 1 TO 6
                for (i.setValue(1); condition(i.lessOrEqual(6)); i.nadd(1))
                {
                    if (condition(pnd_Search_Cat_Dist.equals(pnd_Cat_Dist_Table_Pnd_Cat_Dist_Method.getValue(i))))                                                        //Natural: IF #SEARCH-CAT-DIST = #CAT-DIST-METHOD ( I )
                    {
                        pnd_Twrpymnt_Dist_Method.setValue(pnd_Cat_Dist_Table_Pnd_Dist_Method_Code.getValue(i));                                                           //Natural: ASSIGN #TWRPYMNT-DIST-METHOD := #DIST-METHOD-CODE ( I )
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Company_Totals() throws Exception                                                                                                                    //Natural: COMPANY-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        pnd_Comp_Tot_Table_Pnd_Trans_Count.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,pnd_T).nadd(1);                                                                      //Natural: ADD 1 TO #TRANS-COUNT ( #COMP-IDX,#T )
        pnd_Comp_Tot_Table_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,pnd_T).compute(new ComputeParameters(false, pnd_Comp_Tot_Table_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt()  //Natural: ASSIGN #TAXABLE-AMT ( #COMP-IDX,#T ) := #TAXABLE-AMT ( #COMP-IDX,#T ) + TWRT-GROSS-AMT - TWRT-IVC-AMT
            + 1,pnd_T)), pnd_Comp_Tot_Table_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,pnd_T).add(ldaTwrl0702.getTwrt_Record_Twrt_Gross_Amt()).subtract(ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Amt()));
        pnd_Comp_Tot_Table_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,pnd_T).nadd(ldaTwrl0702.getTwrt_Record_Twrt_Gross_Amt());                              //Natural: ADD TWRT-GROSS-AMT TO #GROSS-AMT ( #COMP-IDX,#T )
        pnd_Comp_Tot_Table_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,pnd_T).nadd(ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Amt());                                  //Natural: ADD TWRT-IVC-AMT TO #IVC-AMT ( #COMP-IDX,#T )
        pnd_Comp_Tot_Table_Pnd_Int_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,pnd_T).nadd(ldaTwrl0702.getTwrt_Record_Twrt_Interest_Amt());                             //Natural: ADD TWRT-INTEREST-AMT TO #INT-AMT ( #COMP-IDX,#T )
        pnd_Comp_Tot_Table_Pnd_Loc_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,pnd_T).nadd(ldaTwrl0702.getTwrt_Record_Twrt_Local_Whhld_Amt());                      //Natural: ADD TWRT-LOCAL-WHHLD-AMT TO #LOC-TAX-AMT ( #COMP-IDX,#T )
        pnd_Comp_Tot_Table_Pnd_Nra_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,pnd_T).nadd(ldaTwrl0702.getTwrt_Record_Twrt_Nra_Whhld_Amt());                        //Natural: ADD TWRT-NRA-WHHLD-AMT TO #NRA-TAX-AMT ( #COMP-IDX,#T )
        pnd_Comp_Tot_Table_Pnd_Fed_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,pnd_T).nadd(ldaTwrl0702.getTwrt_Record_Twrt_Fed_Whhld_Amt());                        //Natural: ADD TWRT-FED-WHHLD-AMT TO #FED-TAX-AMT ( #COMP-IDX,#T )
        pnd_Comp_Tot_Table_Pnd_Can_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,pnd_T).nadd(ldaTwrl0702.getTwrt_Record_Twrt_Can_Whhld_Amt());                        //Natural: ADD TWRT-CAN-WHHLD-AMT TO #CAN-TAX-AMT ( #COMP-IDX,#T )
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("42") || ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("RQ") || ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("PR"))) //Natural: IF TWRT-STATE-RSDNCY = '42' OR = 'RQ' OR = 'PR'
        {
            pnd_Comp_Tot_Table_Pnd_P_R_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,pnd_T).nadd(ldaTwrl0702.getTwrt_Record_Twrt_State_Whhld_Amt());                  //Natural: ADD TWRT-STATE-WHHLD-AMT TO #P-R-TAX-AMT ( #COMP-IDX,#T )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Comp_Tot_Table_Pnd_Sta_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,pnd_T).nadd(ldaTwrl0702.getTwrt_Record_Twrt_State_Whhld_Amt());                  //Natural: ADD TWRT-STATE-WHHLD-AMT TO #STA-TAX-AMT ( #COMP-IDX,#T )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Display_Control_Totals() throws Exception                                                                                                            //Natural: DISPLAY-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #COMP-IDX = 0 TO COMP-MAX
        for (pnd_Ws_Pnd_Comp_Idx.setValue(0); condition(pnd_Ws_Pnd_Comp_Idx.lessOrEqual(pnd_Ws_Const_Comp_Max)); pnd_Ws_Pnd_Comp_Idx.nadd(1))
        {
            if (condition(pnd_Ws_Pnd_Comp_Idx.equals(getZero()) && ! (pnd_Ws_Pnd_Invalid_Comp.getBoolean())))                                                             //Natural: IF #COMP-IDX = 0 AND NOT #INVALID-COMP
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 02 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(8),pnd_Comp_Tot_Table_Pnd_Comp_Nme.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),NEWLINE);             //Natural: WRITE ( 02 ) NOTITLE 08T #COMP-NME ( #COMP-IDX ) /
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(2, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(33),pnd_Comp_Tot_Table_Pnd_Trans_Count.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i1),  //Natural: WRITE ( 02 ) NOTITLE 'Transaction Count...' 33T #TRANS-COUNT ( #COMP-IDX,I1 ) 54T #TRANS-COUNT ( #COMP-IDX,I2 ) 75T #TRANS-COUNT ( #COMP-IDX,I3 ) / 'Gross Amount........' 26T #GROSS-AMT ( #COMP-IDX,I1 ) 47T #GROSS-AMT ( #COMP-IDX,I2 ) 68T #GROSS-AMT ( #COMP-IDX,I3 ) / 'Tax Free IVC........' 26T #IVC-AMT ( #COMP-IDX,I1 ) 47T #IVC-AMT ( #COMP-IDX,I2 ) 68T #IVC-AMT ( #COMP-IDX,I3 ) / 'Taxable Amount......' 26T #TAXABLE-AMT ( #COMP-IDX,I1 ) 47T #TAXABLE-AMT ( #COMP-IDX,I2 ) 68T #TAXABLE-AMT ( #COMP-IDX,I3 ) / 'Interest Amount.....' 26T #INT-AMT ( #COMP-IDX,I1 ) 47T #INT-AMT ( #COMP-IDX,I2 ) 68T #INT-AMT ( #COMP-IDX,I3 ) / 'Fed. Tax Withheld...' 26T #FED-TAX-AMT ( #COMP-IDX,I1 ) 47T #FED-TAX-AMT ( #COMP-IDX,I2 ) 68T #FED-TAX-AMT ( #COMP-IDX,I3 ) / 'NRA Tax Withheld....' 26T #NRA-TAX-AMT ( #COMP-IDX,I1 ) 47T #NRA-TAX-AMT ( #COMP-IDX,I2 ) 68T #NRA-TAX-AMT ( #COMP-IDX,I3 ) / 'Sta. Tax Withheld...' 26T #STA-TAX-AMT ( #COMP-IDX,I1 ) 47T #STA-TAX-AMT ( #COMP-IDX,I2 ) 68T #STA-TAX-AMT ( #COMP-IDX,I3 ) / 'Loc. Tax Withheld...' 26T #LOC-TAX-AMT ( #COMP-IDX,I1 ) 47T #LOC-TAX-AMT ( #COMP-IDX,I2 ) 68T #LOC-TAX-AMT ( #COMP-IDX,I3 ) / 'Can. Tax Withheld...' 26T #CAN-TAX-AMT ( #COMP-IDX,I1 ) 47T #CAN-TAX-AMT ( #COMP-IDX,I2 ) 68T #CAN-TAX-AMT ( #COMP-IDX,I3 ) / 'P.R. Tax Withheld...' 26T #P-R-TAX-AMT ( #COMP-IDX,I1 ) 47T #P-R-TAX-AMT ( #COMP-IDX,I2 ) 68T #P-R-TAX-AMT ( #COMP-IDX,I3 )
                new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(54),pnd_Comp_Tot_Table_Pnd_Trans_Count.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i2), new 
                ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(75),pnd_Comp_Tot_Table_Pnd_Trans_Count.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i3), new ReportEditMask 
                ("Z,ZZZ,ZZ9"),NEWLINE,"Gross Amount........",new TabSetting(26),pnd_Comp_Tot_Table_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i1), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),pnd_Comp_Tot_Table_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i2), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_Comp_Tot_Table_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i3), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Tax Free IVC........",new TabSetting(26),pnd_Comp_Tot_Table_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i1), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),pnd_Comp_Tot_Table_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i2), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_Comp_Tot_Table_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i3), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Taxable Amount......",new TabSetting(26),pnd_Comp_Tot_Table_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i1), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),pnd_Comp_Tot_Table_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i2), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_Comp_Tot_Table_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i3), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Interest Amount.....",new TabSetting(26),pnd_Comp_Tot_Table_Pnd_Int_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i1), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),pnd_Comp_Tot_Table_Pnd_Int_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i2), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_Comp_Tot_Table_Pnd_Int_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i3), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Fed. Tax Withheld...",new TabSetting(26),pnd_Comp_Tot_Table_Pnd_Fed_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i1), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),pnd_Comp_Tot_Table_Pnd_Fed_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i2), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_Comp_Tot_Table_Pnd_Fed_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i3), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"NRA Tax Withheld....",new TabSetting(26),pnd_Comp_Tot_Table_Pnd_Nra_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i1), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),pnd_Comp_Tot_Table_Pnd_Nra_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i2), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_Comp_Tot_Table_Pnd_Nra_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i3), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Sta. Tax Withheld...",new TabSetting(26),pnd_Comp_Tot_Table_Pnd_Sta_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i1), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),pnd_Comp_Tot_Table_Pnd_Sta_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i2), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_Comp_Tot_Table_Pnd_Sta_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i3), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Loc. Tax Withheld...",new TabSetting(26),pnd_Comp_Tot_Table_Pnd_Loc_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i1), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),pnd_Comp_Tot_Table_Pnd_Loc_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i2), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_Comp_Tot_Table_Pnd_Loc_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i3), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Can. Tax Withheld...",new TabSetting(26),pnd_Comp_Tot_Table_Pnd_Can_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i1), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),pnd_Comp_Tot_Table_Pnd_Can_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i2), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_Comp_Tot_Table_Pnd_Can_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i3), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P.R. Tax Withheld...",new TabSetting(26),pnd_Comp_Tot_Table_Pnd_P_R_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i1), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),pnd_Comp_Tot_Table_Pnd_P_R_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i2), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_Comp_Tot_Table_Pnd_P_R_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,i3), 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Update_General_Totals() throws Exception                                                                                                             //Natural: UPDATE-GENERAL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        FOR04:                                                                                                                                                            //Natural: FOR #COMP-IDX = 0 TO COMP-MAX
        for (pnd_Ws_Pnd_Comp_Idx.setValue(0); condition(pnd_Ws_Pnd_Comp_Idx.lessOrEqual(pnd_Ws_Const_Comp_Max)); pnd_Ws_Pnd_Comp_Idx.nadd(1))
        {
            pnd_Comp_Tot_Table_Pnd_Trans_Count.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,4,":",6).nadd(pnd_Comp_Tot_Table_Pnd_Trans_Count.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1, //Natural: ADD #TRANS-COUNT ( #COMP-IDX,1:3 ) TO #TRANS-COUNT ( #COMP-IDX,4:6 )
                1,":",3));
            pnd_Comp_Tot_Table_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,4,":",6).nadd(pnd_Comp_Tot_Table_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1, //Natural: ADD #GROSS-AMT ( #COMP-IDX,1:3 ) TO #GROSS-AMT ( #COMP-IDX,4:6 )
                1,":",3));
            pnd_Comp_Tot_Table_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,4,":",6).nadd(pnd_Comp_Tot_Table_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1, //Natural: ADD #IVC-AMT ( #COMP-IDX,1:3 ) TO #IVC-AMT ( #COMP-IDX,4:6 )
                1,":",3));
            pnd_Comp_Tot_Table_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,4,":",6).nadd(pnd_Comp_Tot_Table_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1, //Natural: ADD #TAXABLE-AMT ( #COMP-IDX,1:3 ) TO #TAXABLE-AMT ( #COMP-IDX,4:6 )
                1,":",3));
            pnd_Comp_Tot_Table_Pnd_Int_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,4,":",6).nadd(pnd_Comp_Tot_Table_Pnd_Int_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1, //Natural: ADD #INT-AMT ( #COMP-IDX,1:3 ) TO #INT-AMT ( #COMP-IDX,4:6 )
                1,":",3));
            pnd_Comp_Tot_Table_Pnd_Fed_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,4,":",6).nadd(pnd_Comp_Tot_Table_Pnd_Fed_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1, //Natural: ADD #FED-TAX-AMT ( #COMP-IDX,1:3 ) TO #FED-TAX-AMT ( #COMP-IDX,4:6 )
                1,":",3));
            pnd_Comp_Tot_Table_Pnd_Nra_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,4,":",6).nadd(pnd_Comp_Tot_Table_Pnd_Nra_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1, //Natural: ADD #NRA-TAX-AMT ( #COMP-IDX,1:3 ) TO #NRA-TAX-AMT ( #COMP-IDX,4:6 )
                1,":",3));
            pnd_Comp_Tot_Table_Pnd_Sta_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,4,":",6).nadd(pnd_Comp_Tot_Table_Pnd_Sta_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1, //Natural: ADD #STA-TAX-AMT ( #COMP-IDX,1:3 ) TO #STA-TAX-AMT ( #COMP-IDX,4:6 )
                1,":",3));
            pnd_Comp_Tot_Table_Pnd_Loc_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,4,":",6).nadd(pnd_Comp_Tot_Table_Pnd_Loc_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1, //Natural: ADD #LOC-TAX-AMT ( #COMP-IDX,1:3 ) TO #LOC-TAX-AMT ( #COMP-IDX,4:6 )
                1,":",3));
            pnd_Comp_Tot_Table_Pnd_Can_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,4,":",6).nadd(pnd_Comp_Tot_Table_Pnd_Can_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1, //Natural: ADD #CAN-TAX-AMT ( #COMP-IDX,1:3 ) TO #CAN-TAX-AMT ( #COMP-IDX,4:6 )
                1,":",3));
            pnd_Comp_Tot_Table_Pnd_P_R_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,4,":",6).nadd(pnd_Comp_Tot_Table_Pnd_P_R_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1, //Natural: ADD #P-R-TAX-AMT ( #COMP-IDX,1:3 ) TO #P-R-TAX-AMT ( #COMP-IDX,4:6 )
                1,":",3));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Reset_Control_Totals() throws Exception                                                                                                              //Natural: RESET-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        pnd_Comp_Tot_Table_Pnd_Comp_Totals.getValue("*",1,":",3).reset();                                                                                                 //Natural: RESET #COMP-TOT-TABLE.#COMP-TOTALS ( *,1:3 )
    }
    private void sub_Read_Summary_Totals_Record() throws Exception                                                                                                        //Natural: READ-SUMMARY-TOTALS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Summary_Found.setValue(false);                                                                                                                                //Natural: ASSIGN #SUMMARY-FOUND := FALSE
        READWORK03:                                                                                                                                                       //Natural: READ WORK FILE 02 RECORD #INP-SUMMARY
        while (condition(getWorkFiles().read(2, ldaTwrl0710.getPnd_Inp_Summary())))
        {
            if (condition(ldaTwrl0710.getPnd_Inp_Summary_Pnd_Source_Code().equals(pnd_Prev_Source)))                                                                      //Natural: IF #INP-SUMMARY.#SOURCE-CODE = #PREV-SOURCE
            {
                pnd_Summary_Found.setValue(true);                                                                                                                         //Natural: ASSIGN #SUMMARY-FOUND := TRUE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK03_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Summary_Found.equals(true)))                                                                                                                    //Natural: IF #SUMMARY-FOUND = TRUE
        {
            getWorkFiles().close(2);                                                                                                                                      //Natural: CLOSE WORK FILE 02
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"error - Source Payment : ",pnd_Prev_Source,new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Summary Totals Record Is Missing From",new  //Natural: WRITE ( 00 ) '***' 06T 'error - Source Payment : ' #PREV-SOURCE 77T '***' / '***' 06T 'Summary Totals Record Is Missing From' 77T '***' / '***' 06T 'The Summary Totals File CMWKF02 (Work File 02).' 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"The Summary Totals File CMWKF02 (Work File 02).",new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(91);  if (true) return;                                                                                                                     //Natural: TERMINATE 91
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Header_Date() throws Exception                                                                                                                   //Natural: GET-HEADER-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Date_Found.setValue(false);                                                                                                                                   //Natural: ASSIGN #DATE-FOUND := FALSE
        READWORK04:                                                                                                                                                       //Natural: READ WORK FILE 05 RECORD #TWRP0600-CONTROL-RECORD
        while (condition(getWorkFiles().read(5, ldaTwrl0600.getPnd_Twrp0600_Control_Record())))
        {
            if (condition(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals(pnd_Prev_Source)))                                                 //Natural: IF #TWRP0600-ORIGIN-CODE = #PREV-SOURCE
            {
                pnd_Date_Found.setValue(true);                                                                                                                            //Natural: ASSIGN #DATE-FOUND := TRUE
                pnd_Header_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd());                     //Natural: MOVE EDITED #TWRP0600-TO-CCYYMMDD TO #HEADER-DATE ( EM = YYYYMMDD )
                pnd_Interface_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd());           //Natural: MOVE EDITED #TWRP0600-INTERFACE-CCYYMMDD TO #INTERFACE-DATE ( EM = YYYYMMDD )
                pnd_Twrpymnt_Pymnt_Intfce_Dte.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd());                                     //Natural: ASSIGN #TWRPYMNT-PYMNT-INTFCE-DTE := #TWRP0600-INTERFACE-CCYYMMDD
                pnd_Tircntl_Tax_Year_A.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy());                                                 //Natural: ASSIGN #TIRCNTL-TAX-YEAR-A := #TWRP0600-TAX-YEAR-CCYY
                pnd_Tircntl_Rpt_Source_Code.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code());                                              //Natural: ASSIGN #TIRCNTL-RPT-SOURCE-CODE := #TWRP0600-ORIGIN-CODE
                //*  09/28/11
                //*  VIKRAM2
                //*  RC07
                if (condition(pnd_Prev_Source.equals("OP") || pnd_Prev_Source.equals("NV") || pnd_Prev_Source.equals("AM") || pnd_Prev_Source.equals("VL")                //Natural: IF #PREV-SOURCE = 'OP' OR = 'NV' OR = 'AM' OR = 'VL' OR = 'EW'
                    || pnd_Prev_Source.equals("EW")))
                {
                    pnd_Tircntl_Frm_Intrfce_Dte_A.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd());                                      //Natural: ASSIGN #TIRCNTL-FRM-INTRFCE-DTE-A := #TWRP0600-FROM-CCYYMMDD
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tircntl_Frm_Intrfce_Dte_A.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd());                                 //Natural: ASSIGN #TIRCNTL-FRM-INTRFCE-DTE-A := #TWRP0600-INTERFACE-CCYYMMDD
                }                                                                                                                                                         //Natural: END-IF
                pnd_Tircntl_To_Intrfce_Dte_A.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd());                                             //Natural: ASSIGN #TIRCNTL-TO-INTRFCE-DTE-A := #TWRP0600-TO-CCYYMMDD
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK04_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Date_Found.equals(true)))                                                                                                                       //Natural: IF #DATE-FOUND = TRUE
        {
            getWorkFiles().close(5);                                                                                                                                      //Natural: CLOSE WORK FILE 05
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"Error - Source Payment : ",pnd_Prev_Source,new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Summary Control Record Is Missing From",new  //Natural: WRITE ( 00 ) '***' 06T 'Error - Source Payment : ' #PREV-SOURCE 77T '***' / '***' 06T 'Summary Control Record Is Missing From' 77T '***' / '***' 06T 'The Control File CMWKF05 (Work File 05).' 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"The Control File CMWKF05 (Work File 05).",new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(91);  if (true) return;                                                                                                                     //Natural: TERMINATE 91
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Cite_Participant() throws Exception                                                                                                                  //Natural: CITE-PARTICIPANT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Tax_Id.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id());                                                                  //Natural: ASSIGN #TWRPARTI-TAX-ID := TWRT-TAX-ID
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Status.setValue(" ");                                                                                                       //Natural: ASSIGN #TWRPARTI-STATUS := ' '
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Twrparti_Part_Eff_End_Dte.setValue("99999999");                                                                                      //Natural: ASSIGN #TWRPARTI-PART-EFF-END-DTE := '99999999'
        vw_prtcpnt.startDatabaseRead                                                                                                                                      //Natural: READ ( 1 ) PRTCPNT WITH TWRPARTI-CURR-REC-SD = #TWRPARTI-CURR-REC-SD
        (
        "P1",
        new Wc[] { new Wc("TWRPARTI_CURR_REC_SD", ">=", pnd_Twrparti_Curr_Rec_Sd, WcType.BY) },
        new Oc[] { new Oc("TWRPARTI_CURR_REC_SD", "ASC") },
        1
        );
        P1:
        while (condition(vw_prtcpnt.readNextRow("P1")))
        {
            if (condition(prtcpnt_Twrparti_Tax_Id.equals(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id()) && prtcpnt_Twrparti_Status.equals(" ") && prtcpnt_Twrparti_Part_Eff_End_Dte.equals("99999999"))) //Natural: IF TWRPARTI-TAX-ID = TWRT-TAX-ID AND TWRPARTI-STATUS = ' ' AND TWRPARTI-PART-EFF-END-DTE = '99999999'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(prtcpnt_Twrparti_Citation_Ind.equals("C")))                                                                                                     //Natural: IF PRTCPNT.TWRPARTI-CITATION-IND = 'C'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                P2:                                                                                                                                                       //Natural: GET UPDPAR *ISN ( P1. )
                vw_updpar.readByID(vw_prtcpnt.getAstISN("P1"), "P2");
                updpar_Twrparti_Citation_Ind.setValue("C");                                                                                                               //Natural: ASSIGN UPDPAR.TWRPARTI-CITATION-IND := 'C'
                vw_updpar.updateDBRow("P2");                                                                                                                              //Natural: UPDATE ( P2. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Store_Control_Totals() throws Exception                                                                                                              //Natural: STORE-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Timn_N.setValue(Global.getTIMN());                                                                                                                            //Natural: ASSIGN #TIMN-N := *TIMN
        pnd_Twrpymnt_Sys_Dte_Time_Pnd_Twrpymnt_Sys_Dte.setValue(Global.getDATN());                                                                                        //Natural: ASSIGN #TWRPYMNT-SYS-DTE := *DATN
        pnd_Twrpymnt_Sys_Dte_Time_Pnd_Twrpymnt_Sys_Time.setValue(pnd_Timn_N_Pnd_Timn_A);                                                                                  //Natural: ASSIGN #TWRPYMNT-SYS-TIME := #TIMN-A
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp.setValue(" ");                                                                                                                    //Natural: ASSIGN #TIRCNTL-5-Y-SC-CO-TO-FRM-SP := ' '
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_5.setValue(5);                                                                                                        //Natural: ASSIGN #SP-CNTL-5 := 5
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Year.setValue(pnd_Tircntl_Tax_Year_A_Pnd_Tircntl_Tax_Year);                                                           //Natural: ASSIGN #SP-CNTL-YEAR := #TIRCNTL-TAX-YEAR
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Source_Code.setValue(pnd_Tircntl_Rpt_Source_Code);                                                                    //Natural: ASSIGN #SP-CNTL-SOURCE-CODE := #TIRCNTL-RPT-SOURCE-CODE
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Company.setValue(" ");                                                                                                //Natural: ASSIGN #SP-CNTL-COMPANY := ' '
        vw_cntlr.startDatabaseRead                                                                                                                                        //Natural: READ ( 1 ) CNTLR WITH TIRCNTL-5-Y-SC-CO-TO-FRM-SP = #TIRCNTL-5-Y-SC-CO-TO-FRM-SP
        (
        "RC1",
        new Wc[] { new Wc("TIRCNTL_5_Y_SC_CO_TO_FRM_SP", ">=", pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp, WcType.BY) },
        new Oc[] { new Oc("TIRCNTL_5_Y_SC_CO_TO_FRM_SP", "ASC") },
        1
        );
        RC1:
        while (condition(vw_cntlr.readNextRow("RC1")))
        {
            if (condition(cntlr_Tircntl_Tbl_Nbr.equals(5) && cntlr_Tircntl_Tax_Year.equals(pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Year) && cntlr_Tircntl_Rpt_Source_Code.equals(pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Sp_Cntl_Source_Code)  //Natural: IF CNTLR.TIRCNTL-TBL-NBR = 5 AND CNTLR.TIRCNTL-TAX-YEAR = #SP-CNTL-YEAR AND CNTLR.TIRCNTL-RPT-SOURCE-CODE = #SP-CNTL-SOURCE-CODE AND CNTLR.TIRCNTL-COMPANY-CDE = ' '
                && cntlr_Tircntl_Company_Cde.equals(" ")))
            {
                RCU1:                                                                                                                                                     //Natural: GET CNTLU *ISN ( RC1. )
                vw_cntlu.readByID(vw_cntlr.getAstISN("RC1"), "RCU1");
                cntlu_Tircntl_Frm_Intrfce_Dte.setValue(pnd_Tircntl_Frm_Intrfce_Dte_A_Pnd_Tircntl_Frm_Intrfce_Dte);                                                        //Natural: ASSIGN CNTLU.TIRCNTL-FRM-INTRFCE-DTE := #TIRCNTL-FRM-INTRFCE-DTE
                cntlu_Tircntl_To_Intrfce_Dte.setValue(pnd_Tircntl_To_Intrfce_Dte_A_Pnd_Tircntl_To_Intrfce_Dte);                                                           //Natural: ASSIGN CNTLU.TIRCNTL-TO-INTRFCE-DTE := #TIRCNTL-TO-INTRFCE-DTE
                vw_cntlu.updateDBRow("RCU1");                                                                                                                             //Natural: UPDATE ( RCU1. )
                //*  12-21-00 FRANK
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        FOR05:                                                                                                                                                            //Natural: FOR #COMP-IDX = 1 TO COMP-MAX
        for (pnd_Ws_Pnd_Comp_Idx.setValue(1); condition(pnd_Ws_Pnd_Comp_Idx.lessOrEqual(pnd_Ws_Const_Comp_Max)); pnd_Ws_Pnd_Comp_Idx.nadd(1))
        {
            ldaTwrl820e.getCntl_Tircntl_Tbl_Nbr().setValue(5);                                                                                                            //Natural: ASSIGN CNTL.TIRCNTL-TBL-NBR := 5
            ldaTwrl820e.getCntl_Tircntl_Tax_Year().setValue(pnd_Tircntl_Tax_Year_A_Pnd_Tircntl_Tax_Year);                                                                 //Natural: ASSIGN CNTL.TIRCNTL-TAX-YEAR := #TIRCNTL-TAX-YEAR
            ldaTwrl820e.getCntl_Tircntl_Seq_Nbr().setValue(1);                                                                                                            //Natural: ASSIGN CNTL.TIRCNTL-SEQ-NBR := 1
            ldaTwrl820e.getCntl_Tircntl_Rpt_Source_Code().setValue(pnd_Tircntl_Rpt_Source_Code);                                                                          //Natural: ASSIGN CNTL.TIRCNTL-RPT-SOURCE-CODE := #TIRCNTL-RPT-SOURCE-CODE
            ldaTwrl820e.getCntl_Tircntl_Frm_Intrfce_Dte().setValue(pnd_Tircntl_Frm_Intrfce_Dte_A_Pnd_Tircntl_Frm_Intrfce_Dte);                                            //Natural: ASSIGN CNTL.TIRCNTL-FRM-INTRFCE-DTE := #TIRCNTL-FRM-INTRFCE-DTE
            ldaTwrl820e.getCntl_Tircntl_To_Intrfce_Dte().setValue(pnd_Tircntl_To_Intrfce_Dte_A_Pnd_Tircntl_To_Intrfce_Dte);                                               //Natural: ASSIGN CNTL.TIRCNTL-TO-INTRFCE-DTE := #TIRCNTL-TO-INTRFCE-DTE
            ldaTwrl820e.getCntl_Tircntl_Company_Cde().setValue(pnd_Comp_Tot_Table_Pnd_Comp_Cde.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1));                               //Natural: ASSIGN CNTL.TIRCNTL-COMPANY-CDE := #COMP-CDE ( #COMP-IDX )
            ldaTwrl820e.getCntl_Tircntl_Input_Recs_Tot().setValue(pnd_Comp_Tot_Table_Pnd_Trans_Count.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,4));                       //Natural: ASSIGN CNTL.TIRCNTL-INPUT-RECS-TOT := #TRANS-COUNT ( #COMP-IDX,4 )
            ldaTwrl820e.getCntl_Tircntl_Input_Gross_Amt().setValue(pnd_Comp_Tot_Table_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,4));                        //Natural: ASSIGN CNTL.TIRCNTL-INPUT-GROSS-AMT := #GROSS-AMT ( #COMP-IDX,4 )
            ldaTwrl820e.getCntl_Tircntl_Input_Ivt_Amt().setValue(pnd_Comp_Tot_Table_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,4));                            //Natural: ASSIGN CNTL.TIRCNTL-INPUT-IVT-AMT := #IVC-AMT ( #COMP-IDX,4 )
            ldaTwrl820e.getCntl_Tircntl_Input_Int_Amt().setValue(pnd_Comp_Tot_Table_Pnd_Int_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,4));                            //Natural: ASSIGN CNTL.TIRCNTL-INPUT-INT-AMT := #INT-AMT ( #COMP-IDX,4 )
            ldaTwrl820e.getCntl_Tircntl_Input_Nra_Amt().setValue(pnd_Comp_Tot_Table_Pnd_Nra_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,4));                        //Natural: ASSIGN CNTL.TIRCNTL-INPUT-NRA-AMT := #NRA-TAX-AMT ( #COMP-IDX,4 )
            ldaTwrl820e.getCntl_Tircntl_Input_Can_Amt().setValue(pnd_Comp_Tot_Table_Pnd_Can_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,4));                        //Natural: ASSIGN CNTL.TIRCNTL-INPUT-CAN-AMT := #CAN-TAX-AMT ( #COMP-IDX,4 )
            ldaTwrl820e.getCntl_Tircntl_Input_Fed_Wthldg_Amt().setValue(pnd_Comp_Tot_Table_Pnd_Fed_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,4));                 //Natural: ASSIGN CNTL.TIRCNTL-INPUT-FED-WTHLDG-AMT := #FED-TAX-AMT ( #COMP-IDX,4 )
            ldaTwrl820e.getCntl_Tircntl_Input_State_Wthldg_Amt().setValue(pnd_Comp_Tot_Table_Pnd_Sta_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,                   //Natural: ASSIGN CNTL.TIRCNTL-INPUT-STATE-WTHLDG-AMT := #STA-TAX-AMT ( #COMP-IDX,4 )
                4));
            ldaTwrl820e.getCntl_Tircntl_Input_Local_Wthldg_Amt().setValue(pnd_Comp_Tot_Table_Pnd_Loc_Tax_Amt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1,                   //Natural: ASSIGN CNTL.TIRCNTL-INPUT-LOCAL-WTHLDG-AMT := #LOC-TAX-AMT ( #COMP-IDX,4 )
                4));
            ldaTwrl820e.getCntl_Tircntl_Rpt_Source_Code_Desc().setValue(" ");                                                                                             //Natural: ASSIGN CNTL.TIRCNTL-RPT-SOURCE-CODE-DESC := ' '
            ldaTwrl820e.getCntl_Tircntl_Rpt_Update_Dte_Time().setValue(pnd_Twrpymnt_Sys_Dte_Time_Pnd_Twrpymnt_Sys_Dte_Time_N);                                            //Natural: ASSIGN CNTL.TIRCNTL-RPT-UPDATE-DTE-TIME := #TWRPYMNT-SYS-DTE-TIME-N
            ldaTwrl820e.getVw_cntl().insertDBRow();                                                                                                                       //Natural: STORE CNTL
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  12-21-00 FRANK
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Update_Citation_Table() throws Exception                                                                                                             //Natural: UPDATE-CITATION-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        FOR06:                                                                                                                                                            //Natural: FOR Z = 1 TO 60
        for (z.setValue(1); condition(z.lessOrEqual(60)); z.nadd(1))
        {
            if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Error_Table().getValue(z).equals(getZero())))                                                                   //Natural: IF TWRT-ERROR-TABLE ( Z ) = 0
            {
                ldaTwrl0702.getTwrt_Record_Twrt_Error_Table().getValue(z).setValue(pnd_Citation_Message_Code);                                                            //Natural: ASSIGN TWRT-ERROR-TABLE ( Z ) := #CITATION-MESSAGE-CODE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        pnd_Header_Date.reset();                                                                                                                                          //Natural: RESET #HEADER-DATE #INTERFACE-DATE
        pnd_Interface_Date.reset();
        pnd_Source_Total_Header.setValue("  Report Totals  ");                                                                                                            //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '  Report Totals  '
        i1.setValue(4);                                                                                                                                                   //Natural: ASSIGN I1 := 4
        i2.setValue(5);                                                                                                                                                   //Natural: ASSIGN I2 := 5
        i3.setValue(6);                                                                                                                                                   //Natural: ASSIGN I3 := 6
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
        sub_Display_Control_Totals();
        if (condition(Global.isEscape())) {return;}
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, new TabSetting(1),"Tax Transaction Records Read..................",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                         //Natural: WRITE ( 00 ) 01T 'Tax Transaction Records Read..................' #READ-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Duplicate Tax Transaction Records Bypassed....",pnd_Bypass_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));               //Natural: WRITE ( 00 ) / 01T 'Duplicate Tax Transaction Records Bypassed....' #BYPASS-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"New Tax Payments Added For The First Time.....",pnd_Store_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                //Natural: WRITE ( 00 ) / 01T 'New Tax Payments Added For The First Time.....' #STORE-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Tax Payments Added To Existing Payment Recs...",pnd_Update_New_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 00 ) / 01T 'Tax Payments Added To Existing Payment Recs...' #UPDATE-NEW-CTR
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 02 )
        if (condition(Global.isEscape())){return;}
        getReports().skip(2, 4);                                                                                                                                          //Natural: SKIP ( 02 ) 4
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"Tax Transaction Records Read..................",pnd_Read_Ctr, new ReportEditMask                    //Natural: WRITE ( 02 ) 01T 'Tax Transaction Records Read..................' #READ-CTR
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"Duplicate Tax Transaction Records Bypassed....",pnd_Bypass_Ctr, new ReportEditMask          //Natural: WRITE ( 02 ) / 01T 'Duplicate Tax Transaction Records Bypassed....' #BYPASS-CTR
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"New Tax Payments Added For The First Time.....",pnd_Store_Ctr, new ReportEditMask           //Natural: WRITE ( 02 ) / 01T 'New Tax Payments Added For The First Time.....' #STORE-CTR
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"Tax Payments Added To Existing Payment Recs...",pnd_Update_New_Ctr, new                     //Natural: WRITE ( 02 ) / 01T 'Tax Payments Added To Existing Payment Recs...' #UPDATE-NEW-CTR
            ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Report_Bypassed_Existing_Payments() throws Exception                                                                                                 //Natural: REPORT-BYPASSED-EXISTING-PAYMENTS
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(),  //Natural: WRITE ( 01 ) NOTITLE NOHDR TWRT-CNTRCT-NBR TWRT-PAYEE-CDE TWRT-PAYMT-DTE-ALPHA ( EM = XXXX'/'XX'/'XX ) TWRT-TAX-ID TWRT-TAX-ID-TYPE TWRT-GROSS-AMT TWRT-INTEREST-AMT TWRT-IVC-AMT TWRT-FED-WHHLD-AMT 2X TWRT-NRA-WHHLD-AMT TWRT-STATE-WHHLD-AMT TWRT-LOCAL-WHHLD-AMT 2X TWRT-STATE-RSDNCY 2X TWRT-CITIZENSHIP
            new ReportEditMask ("XXXX'/'XX'/'XX"),ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),ldaTwrl0702.getTwrt_Record_Twrt_Gross_Amt(),ldaTwrl0702.getTwrt_Record_Twrt_Interest_Amt(),ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Amt(),ldaTwrl0702.getTwrt_Record_Twrt_Fed_Whhld_Amt(),new 
            ColumnSpacing(2),ldaTwrl0702.getTwrt_Record_Twrt_Nra_Whhld_Amt(),ldaTwrl0702.getTwrt_Record_Twrt_State_Whhld_Amt(),ldaTwrl0702.getTwrt_Record_Twrt_Local_Whhld_Amt(),new 
            ColumnSpacing(2),ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),new ColumnSpacing(2),ldaTwrl0702.getTwrt_Record_Twrt_Citizenship());
        if (Global.isEscape()) return;
    }
    private void sub_Get_Company() throws Exception                                                                                                                       //Natural: GET-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        short decideConditionsMet2065 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF TWRT-COMPANY-CDE;//Natural: VALUE 'C'
        if (condition((ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde().equals("C"))))
        {
            decideConditionsMet2065++;
            pnd_Ws_Pnd_Comp_Idx.setValue(1);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 1
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde().equals("L"))))
        {
            decideConditionsMet2065++;
            pnd_Ws_Pnd_Comp_Idx.setValue(2);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 2
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde().equals("S"))))
        {
            decideConditionsMet2065++;
            pnd_Ws_Pnd_Comp_Idx.setValue(3);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 3
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde().equals("T"))))
        {
            decideConditionsMet2065++;
            pnd_Ws_Pnd_Comp_Idx.setValue(4);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 4
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde().equals("X"))))
        {
            decideConditionsMet2065++;
            pnd_Ws_Pnd_Comp_Idx.setValue(5);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 5
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde().equals("F"))))
        {
            decideConditionsMet2065++;
            pnd_Ws_Pnd_Comp_Idx.setValue(6);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 6
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Ws_Pnd_Invalid_Comp.setValue(true);                                                                                                                       //Natural: ASSIGN #INVALID-COMP := TRUE
            pnd_Ws_Pnd_Comp_Idx.reset();                                                                                                                                  //Natural: RESET #COMP-IDX
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Load_Company() throws Exception                                                                                                                      //Natural: LOAD-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        FOR07:                                                                                                                                                            //Natural: FOR #COMP-IDX = 1 TO COMP-MAX
        for (pnd_Ws_Pnd_Comp_Idx.setValue(1); condition(pnd_Ws_Pnd_Comp_Idx.lessOrEqual(pnd_Ws_Const_Comp_Max)); pnd_Ws_Pnd_Comp_Idx.nadd(1))
        {
            //*  RCC
            short decideConditionsMet2087 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #COMP-IDX;//Natural: VALUE 1
            if (condition((pnd_Ws_Pnd_Comp_Idx.equals(1))))
            {
                decideConditionsMet2087++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("C");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'C'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Ws_Pnd_Comp_Idx.equals(2))))
            {
                decideConditionsMet2087++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("L");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'L'
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Ws_Pnd_Comp_Idx.equals(3))))
            {
                decideConditionsMet2087++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("S");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'S'
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pnd_Ws_Pnd_Comp_Idx.equals(4))))
            {
                decideConditionsMet2087++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("T");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'T'
            }                                                                                                                                                             //Natural: VALUE 5
            else if (condition((pnd_Ws_Pnd_Comp_Idx.equals(5))))
            {
                decideConditionsMet2087++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("X");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'X'
            }                                                                                                                                                             //Natural: VALUE 6
            else if (condition((pnd_Ws_Pnd_Comp_Idx.equals(6))))
            {
                decideConditionsMet2087++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("F");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'F'
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet2087 > 0))
            {
                pnd_Comp_Tot_Table_Pnd_Comp_Cde.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code());                         //Natural: ASSIGN #COMP-CDE ( #COMP-IDX ) := #TWRACOMP.#COMP-CODE
                DbsUtil.callnat(Twrncomp.class , getCurrentProcessState(), pdaTwracomp.getPnd_Twracomp_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNCOMP' USING #TWRACOMP.#INPUT-PARMS ( AD = O ) #TWRACOMP.#OUTPUT-DATA ( AD = M )
                    pdaTwracomp.getPnd_Twracomp_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                if (condition(pdaTwracomp.getPnd_Twracomp_Pnd_Ret_Code().getBoolean()))                                                                                   //Natural: IF #TWRACOMP.#RET-CODE
                {
                    pnd_Comp_Tot_Table_Pnd_Comp_Nme.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name());               //Natural: ASSIGN #COMP-NME ( #COMP-IDX ) := #TWRACOMP.#COMP-SHORT-NAME
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_Comp_Idx.reset();                                                                                                                          //Natural: RESET #COMP-IDX
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Change_Reversal_Signs() throws Exception                                                                                                             //Natural: CHANGE-REVERSAL-SIGNS
    {
        if (BLNatReinput.isReinput()) return;

        ldaTwrl0702.getTwrt_Record_Twrt_Gross_Amt().compute(new ComputeParameters(false, ldaTwrl0702.getTwrt_Record_Twrt_Gross_Amt()), ldaTwrl0702.getTwrt_Record_Twrt_Gross_Amt().multiply(-1)); //Natural: ASSIGN TWRT-GROSS-AMT := TWRT-GROSS-AMT * -1
        ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Amt().compute(new ComputeParameters(false, ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Amt()), ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Amt().multiply(-1)); //Natural: ASSIGN TWRT-IVC-AMT := TWRT-IVC-AMT * -1
        ldaTwrl0702.getTwrt_Record_Twrt_Interest_Amt().compute(new ComputeParameters(false, ldaTwrl0702.getTwrt_Record_Twrt_Interest_Amt()), ldaTwrl0702.getTwrt_Record_Twrt_Interest_Amt().multiply(-1)); //Natural: ASSIGN TWRT-INTEREST-AMT := TWRT-INTEREST-AMT * -1
        ldaTwrl0702.getTwrt_Record_Twrt_Fed_Whhld_Amt().compute(new ComputeParameters(false, ldaTwrl0702.getTwrt_Record_Twrt_Fed_Whhld_Amt()), ldaTwrl0702.getTwrt_Record_Twrt_Fed_Whhld_Amt().multiply(-1)); //Natural: ASSIGN TWRT-FED-WHHLD-AMT := TWRT-FED-WHHLD-AMT * -1
        ldaTwrl0702.getTwrt_Record_Twrt_Nra_Whhld_Amt().compute(new ComputeParameters(false, ldaTwrl0702.getTwrt_Record_Twrt_Nra_Whhld_Amt()), ldaTwrl0702.getTwrt_Record_Twrt_Nra_Whhld_Amt().multiply(-1)); //Natural: ASSIGN TWRT-NRA-WHHLD-AMT := TWRT-NRA-WHHLD-AMT * -1
        ldaTwrl0702.getTwrt_Record_Twrt_Can_Whhld_Amt().compute(new ComputeParameters(false, ldaTwrl0702.getTwrt_Record_Twrt_Can_Whhld_Amt()), ldaTwrl0702.getTwrt_Record_Twrt_Can_Whhld_Amt().multiply(-1)); //Natural: ASSIGN TWRT-CAN-WHHLD-AMT := TWRT-CAN-WHHLD-AMT * -1
        ldaTwrl0702.getTwrt_Record_Twrt_State_Whhld_Amt().compute(new ComputeParameters(false, ldaTwrl0702.getTwrt_Record_Twrt_State_Whhld_Amt()), ldaTwrl0702.getTwrt_Record_Twrt_State_Whhld_Amt().multiply(-1)); //Natural: ASSIGN TWRT-STATE-WHHLD-AMT := TWRT-STATE-WHHLD-AMT * -1
        ldaTwrl0702.getTwrt_Record_Twrt_Local_Whhld_Amt().compute(new ComputeParameters(false, ldaTwrl0702.getTwrt_Record_Twrt_Local_Whhld_Amt()), ldaTwrl0702.getTwrt_Record_Twrt_Local_Whhld_Amt().multiply(-1)); //Natural: ASSIGN TWRT-LOCAL-WHHLD-AMT := TWRT-LOCAL-WHHLD-AMT * -1
    }
    //*   RC08
    private void sub_Distribution_D_Conversion() throws Exception                                                                                                         //Natural: DISTRIBUTION-D-CONVERSION
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------------------------------------
        //*   DIST D CHANGES APPLIES TO US CITIZEN ONLY
        //*  SAURAV
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Citizenship().equals("01") || ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().equals("Y")                     //Natural: IF TWRT-CITIZENSHIP = '01' OR TWRT-PYMNT-TAX-FORM-1078 = 'Y' OR TWRT-LOB-CDE NE 'VT'
            || ldaTwrl0702.getTwrt_Record_Twrt_Lob_Cde().notEquals("VT")))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Iaa_Contract.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr());                                                                                          //Natural: ASSIGN #IAA-CONTRACT := TWRT-CNTRCT-NBR
        pnd_Oc_N.reset();                                                                                                                                                 //Natural: RESET #OC-N
        vw_iaa_Cntrct.startDatabaseFind                                                                                                                                   //Natural: FIND IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #IAA-CONTRACT
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Iaa_Contract, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_iaa_Cntrct.readNextRow("FIND01", true)))
        {
            vw_iaa_Cntrct.setIfNotFoundControlFlag(false);
            if (condition(vw_iaa_Cntrct.getAstCOUNTER().equals(0)))                                                                                                       //Natural: IF NO RECORDS FOUND
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  OPTION CODE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Oc_N.setValue(iaa_Cntrct_Cntrct_Orgn_Cde);                                                                                                                //Natural: ASSIGN #OC-N := CNTRCT-ORGN-CDE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition((ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type().equals("I") && ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type().equals("T")) || (ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type().equals("C")  //Natural: IF ( TWRT-PAYMT-TYPE = 'I' AND TWRT-SETTL-TYPE = 'T' ) OR ( TWRT-PAYMT-TYPE = 'C' AND TWRT-SETTL-TYPE = 'X' ) OR ( TWRT-PAYMT-TYPE = 'N' AND TWRT-SETTL-TYPE = 'T' )
            && ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type().equals("X")) || (ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type().equals("N") && ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type().equals("T"))))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Oc_N_Pnd_Oc_A.equals("03") || pnd_Oc_N_Pnd_Oc_A.equals("17") || pnd_Oc_N_Pnd_Oc_A.equals("18") || pnd_Oc_N_Pnd_Oc_A.equals("35")                //Natural: IF #OC-A = '03' OR = '17' OR = '18' OR = '35' OR = '36' OR = '37' OR = '38' OR = '40' OR = '42' OR = '43' OR = '44' OR = '45' OR = '46' OR TWRT-CONTRACT-TYPE = 'PA' OR = 'SR1' OR TWRT-SOURCE = 'VL' OR = 'NV'
            || pnd_Oc_N_Pnd_Oc_A.equals("36") || pnd_Oc_N_Pnd_Oc_A.equals("37") || pnd_Oc_N_Pnd_Oc_A.equals("38") || pnd_Oc_N_Pnd_Oc_A.equals("40") || pnd_Oc_N_Pnd_Oc_A.equals("42") 
            || pnd_Oc_N_Pnd_Oc_A.equals("43") || pnd_Oc_N_Pnd_Oc_A.equals("44") || pnd_Oc_N_Pnd_Oc_A.equals("45") || pnd_Oc_N_Pnd_Oc_A.equals("46") || ldaTwrl0702.getTwrt_Record_Twrt_Contract_Type().equals("PA") 
            || ldaTwrl0702.getTwrt_Record_Twrt_Contract_Type().equals("SR1") || ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("VL") || ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("NV")))
        {
            if (condition((ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type().equals("A") && ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type().equals("C")) || (ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type().equals("E")  //Natural: IF ( TWRT-PAYMT-TYPE = 'A' AND TWRT-SETTL-TYPE = 'C' ) OR ( TWRT-PAYMT-TYPE = 'E' AND TWRT-SETTL-TYPE = 'C' ) OR ( TWRT-PAYMT-TYPE = 'D' AND TWRT-SETTL-TYPE = 'C' ) OR ( TWRT-PAYMT-TYPE = 'N' AND TWRT-SETTL-TYPE = 'C' ) OR ( TWRT-PAYMT-TYPE = 'C' AND TWRT-SETTL-TYPE = 'A' ) OR ( TWRT-PAYMT-TYPE = 'N' AND TWRT-SETTL-TYPE = 'K' ) OR ( TWRT-PAYMT-TYPE = 'B' AND TWRT-SETTL-TYPE = 'C' )
                && ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type().equals("C")) || (ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type().equals("D") && ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type().equals("C")) 
                || (ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type().equals("N") && ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type().equals("C")) || (ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type().equals("C") 
                && ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type().equals("A")) || (ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type().equals("N") && ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type().equals("K")) 
                || (ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type().equals("B") && ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type().equals("C"))))
            {
                //* *          DECIDE ON FIRST VALUE OF TWRT-DISTRIBUTION-CODE-1
                short decideConditionsMet2149 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF TWRT-DISTRIB-CDE;//Natural: VALUE '1'
                if (condition((ldaTwrl0702.getTwrt_Record_Twrt_Distrib_Cde().equals("1"))))
                {
                    decideConditionsMet2149++;
                    ldaTwrl0702.getTwrt_Record_Twrt_Distrib_Cde().setValue("<");                                                                                          //Natural: ASSIGN TWRT-DISTRIB-CDE := '<'
                }                                                                                                                                                         //Natural: VALUE '2'
                else if (condition((ldaTwrl0702.getTwrt_Record_Twrt_Distrib_Cde().equals("2"))))
                {
                    decideConditionsMet2149++;
                    ldaTwrl0702.getTwrt_Record_Twrt_Distrib_Cde().setValue(">");                                                                                          //Natural: ASSIGN TWRT-DISTRIB-CDE := '>'
                }                                                                                                                                                         //Natural: VALUE '3'
                else if (condition((ldaTwrl0702.getTwrt_Record_Twrt_Distrib_Cde().equals("3"))))
                {
                    decideConditionsMet2149++;
                    ldaTwrl0702.getTwrt_Record_Twrt_Distrib_Cde().setValue("X");                                                                                          //Natural: ASSIGN TWRT-DISTRIB-CDE := 'X'
                }                                                                                                                                                         //Natural: VALUE '4'
                else if (condition((ldaTwrl0702.getTwrt_Record_Twrt_Distrib_Cde().equals("4"))))
                {
                    decideConditionsMet2149++;
                    ldaTwrl0702.getTwrt_Record_Twrt_Distrib_Cde().setValue("W");                                                                                          //Natural: ASSIGN TWRT-DISTRIB-CDE := 'W'
                }                                                                                                                                                         //Natural: VALUE '7'
                else if (condition((ldaTwrl0702.getTwrt_Record_Twrt_Distrib_Cde().equals("7"))))
                {
                    decideConditionsMet2149++;
                    ldaTwrl0702.getTwrt_Record_Twrt_Distrib_Cde().setValue("V");                                                                                          //Natural: ASSIGN TWRT-DISTRIB-CDE := 'V'
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                getReports().display(0, "PPCN",                                                                                                                           //Natural: DISPLAY 'PPCN' #IAA-CONTRACT 'SRC' TWRT-SOURCE 'CNTR TYP' TWRT-CONTRACT-TYPE 'ORG' #OC-A 'CTZN' TWRT-CITIZENSHIP 'DIST' TWRT-DISTRIBUTION-CODE 'DIST1' TWRT-DISTRIB-CDE '1078' TWRT-PYMNT-TAX-FORM-1078 'PAYTYP' TWRT-PAYMT-TYPE 'SETTLE' TWRT-SETTL-TYPE
                		pnd_Iaa_Contract,"SRC",
                		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"CNTR TYP",
                		ldaTwrl0702.getTwrt_Record_Twrt_Contract_Type(),"ORG",
                		pnd_Oc_N_Pnd_Oc_A,"CTZN",
                		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"DIST",
                		ldaTwrl0702.getTwrt_Record_Twrt_Distribution_Code(),"DIST1",
                		ldaTwrl0702.getTwrt_Record_Twrt_Distrib_Cde(),"1078",
                		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"PAYTYP",
                		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"SETTLE",
                		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type());
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new               //Natural: WRITE ( 01 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 49T 'Bypassed Payments Already On File' 120T 'REPORT: RPT1' // 'Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest' '     IVC        Fed Tax        NRA       State       Local  Res Ctz' / 'Number   Code  Date    Ind.ID No  Type     Amount       Amount  ' '   Amount     Amount       Amount      Amount      Amount Cde Cde'
                        TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask 
                        ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(49),"Bypassed Payments Already On File",new TabSetting(120),
                        "REPORT: RPT1",NEWLINE,NEWLINE,"Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest","     IVC        Fed Tax        NRA       State       Local  Res Ctz",
                        NEWLINE,"Number   Code  Date    Ind.ID No  Type     Amount       Amount  ","   Amount     Amount       Amount      Amount      Amount Cde Cde");
                    //*     '                                                                '
                    //*     '                                    /Puerto-Rico  /Canada        '
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(2, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 02 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(2, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(54),"Tax Update Control Report",new           //Natural: WRITE ( 02 ) NOTITLE *INIT-USER '-' *PROGRAM 54T 'Tax Update Control Report' 120T 'REPORT: RPT2'
                        TabSetting(120),"REPORT: RPT2");
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(25),pnd_Header_Date, new ReportEditMask ("MM/DD/YYYY"),new TabSetting(58),pnd_Source_Total_Header,new  //Natural: WRITE ( 02 ) NOTITLE 25T #HEADER-DATE ( EM = MM/DD/YYYY ) 58T #SOURCE-TOTAL-HEADER 97T #INTERFACE-DATE ( EM = MM/DD/YYYY )
                        TabSetting(97),pnd_Interface_Date, new ReportEditMask ("MM/DD/YYYY"));
                    getReports().skip(2, 2);                                                                                                                              //Natural: SKIP ( 02 ) 2 LINES
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(22),"                    ",new TabSetting(43),"                    ",new                    //Natural: WRITE ( 02 ) NOTITLE 22T '                    ' 43T '                    ' 64T '       Cited &      '
                        TabSetting(64),"       Cited &      ");
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(22),"       Input        ",new TabSetting(43),"      Rejected      ",new                    //Natural: WRITE ( 02 ) NOTITLE 22T '       Input        ' 43T '      Rejected      ' 64T '      Accepted      '
                        TabSetting(64),"      Accepted      ");
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(22),"====================",new TabSetting(43),"====================",new                    //Natural: WRITE ( 02 ) NOTITLE 22T '====================' 43T '====================' 64T '===================='
                        TabSetting(64),"====================");
                    //*    01T '       (TIAA)       '
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 02 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
        pnd_Source_Total_Header.setValue(DbsUtil.compress("Source System:", pnd_Prev_Source));                                                                            //Natural: COMPRESS 'Source System:' #PREV-SOURCE INTO #SOURCE-TOTAL-HEADER
        i1.setValue(1);                                                                                                                                                   //Natural: ASSIGN I1 := 1
        i2.setValue(2);                                                                                                                                                   //Natural: ASSIGN I2 := 2
        i3.setValue(3);                                                                                                                                                   //Natural: ASSIGN I3 := 3
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
        sub_Display_Control_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-GENERAL-TOTALS
        sub_Update_General_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaTwrl0702_getTwrt_Record_Twrt_SourceIsBreak = ldaTwrl0702.getTwrt_Record_Twrt_Source().isBreak(endOfData);
        if (condition(ldaTwrl0702_getTwrt_Record_Twrt_SourceIsBreak))
        {
            pnd_Source_Total_Header.setValue(DbsUtil.compress("Source System:", pnd_Prev_Source));                                                                        //Natural: COMPRESS 'Source System:' #PREV-SOURCE INTO #SOURCE-TOTAL-HEADER
            i1.setValue(1);                                                                                                                                               //Natural: ASSIGN I1 := 1
            i2.setValue(2);                                                                                                                                               //Natural: ASSIGN I2 := 2
            i3.setValue(3);                                                                                                                                               //Natural: ASSIGN I3 := 3
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
            sub_Display_Control_Totals();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM READ-SUMMARY-TOTALS-RECORD
            sub_Read_Summary_Totals_Record();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM STORE-CONTROL-TOTALS
            sub_Store_Control_Totals();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-GENERAL-TOTALS
            sub_Update_General_Totals();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM RESET-CONTROL-TOTALS
            sub_Reset_Control_Totals();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Pnd_Source_Break.setValue(true);                                                                                                                       //Natural: ASSIGN #WS.#SOURCE-BREAK := TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133 ZP=ON");
        Global.format(1, "PS=60 LS=133 ZP=ON");
        Global.format(2, "PS=60 LS=133 ZP=ON");
        Global.format(3, "PS=60 LS=133 ZP=ON");
        Global.format(4, "PS=60 LS=133 ZP=ON");
        Global.format(5, "PS=60 LS=133 ZP=ON");
        Global.format(6, "PS=60 LS=133 ZP=ON");

        getReports().setDisplayColumns(0, "PPCN",
        		pnd_Iaa_Contract,"SRC",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"CNTR TYP",
        		ldaTwrl0702.getTwrt_Record_Twrt_Contract_Type(),"ORG",
        		pnd_Oc_N_Pnd_Oc_A,"CTZN",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"DIST",
        		ldaTwrl0702.getTwrt_Record_Twrt_Distribution_Code(),"DIST1",
        		ldaTwrl0702.getTwrt_Record_Twrt_Distrib_Cde(),"1078",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"PAYTYP",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"SETTLE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type());
    }
    private void CheckAtStartofData1048() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM LOAD-COMPANY
            sub_Load_Company();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
