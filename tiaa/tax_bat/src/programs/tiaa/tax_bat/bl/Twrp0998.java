/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:32:26 PM
**        * FROM NATURAL PROGRAM : Twrp0998
************************************************************
**        * FILE NAME            : Twrp0998.java
**        * CLASS NAME           : Twrp0998
**        * INSTANCE NAME        : Twrp0998
************************************************************
* PROGRAM  : TWRP0998
* FUNCTION : PRINTS DAILY ONLINE MAINTENANCE DETAIL REPORT BY  COMPANY
*             AND RESIDENCY CODE (SORTED BY COMPANY, RESIDENCY CODE &
*                                  TAX CITIZENSHIP)
* AUTHOR   : EDITH  12/99
* UPDATES  : EDITH 1/13/2000
*              PRINTS TOTAL AT BREAK OF RESIDENCY CODE
*          : EDITH 1/19/2000
*              INPUT TO BE SORTED BY COMPANY,RESIDENCY CODE, TIN &
*                                  TAX CITIZENSHIP.
* 09/20/04 RM TOPS RELEASE 3 CHANGES
*             ADD NEW COMPANY CODE 'X' FOR TRUST COMPANY AND NEW SOURCE
*             CODES 'PL', 'NL'.
* 11/21/05 - BK SYNCHRONIZED LOCAL TWRLMUCN TO PROD
* 06/06/06: NAVISYS CHANGES   R. MA
*           SOURCE CODE 'NV' HAD BEEN ADDED TO ALL CONTROL REPORTS.
*           THIS PROGRAM IS ONE OF THE CONTROL REPORTS BUT DON't need
*           ANY CODE CHANGE THIS TIME.
* 10/18/11  JB NON-ERISA TDA - ADD COMPANY CODE 'F' SCAN JB1011
* 11/20/14  O SOTTO FATCA CHANGES MARKED 11/2014.
********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0998 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0901 ldaTwrl0901;
    private LdaTwrl0902 ldaTwrl0902;
    private LdaTwrlmucn ldaTwrlmucn;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_Area;
    private DbsField pnd_Work_Area_Pnd_Rec_Read;
    private DbsField pnd_Work_Area_Pnd_Process_Rec;
    private DbsField pnd_Work_Area_Pnd_Sv_Company;
    private DbsField pnd_Work_Area_Pnd_Sv_Source;
    private DbsField pnd_Work_Area_Pnd_Sv_Payset;
    private DbsField pnd_Work_Area_Pnd_Print_Com;
    private DbsField pnd_Work_Area_Pnd_Trans_Desc;
    private DbsField pnd_Work_Area_Pnd_Old_Sc;
    private DbsField pnd_Work_Area_Pnd_Old_Co;
    private DbsField pnd_Work_Area_Pnd_Contract_Payee;
    private DbsField pnd_Work_Area_Pnd_I;
    private DbsField pnd_Work_Area_Pnd_Prt_Source_Code;
    private DbsField pnd_Work_Area_Pnd_New_Record;
    private DbsField pnd_Work_Area_Pnd_Found;
    private DbsField pnd_Work_Area_Pnd_Sv_Res_Code;
    private DbsField pnd_Work_Area_Pnd_K;
    private DbsField pnd_Work_Area_Pnd_J;
    private DbsField pnd_Work_Area_Pnd_Sv_Search_Rc;
    private DbsField pnd_Work_Area_Pnd_Sv_State_Desc;

    private DbsGroup pnd_Work_Area__R_Field_1;
    private DbsField pnd_Work_Area_Pnd_Sv_State_Desc_Sub;
    private DbsField pnd_Work_Area_Pnd_Print_Total;
    private DbsField pnd_Work_Area_Pnd_Temp_Pr_Wthld;
    private DbsField pnd_Work_Area_Pnd_Temp_State_Wthld;
    private DbsField pnd_Work_Area_Pnd_Fatca;
    private DbsField pnd_Work_Area_Pnd_Hdg1;

    private DbsGroup pnd_Totals;
    private DbsField pnd_Totals_Pnd_Total_Cnt;
    private DbsField pnd_Totals_Pnd_Total_Grs;
    private DbsField pnd_Totals_Pnd_Total_Ivc;
    private DbsField pnd_Totals_Pnd_Total_Int;
    private DbsField pnd_Totals_Pnd_Total_Fed;
    private DbsField pnd_Totals_Pnd_Total_Nra;
    private DbsField pnd_Totals_Pnd_Total_Sta;
    private DbsField pnd_Totals_Pnd_Total_Lcl;
    private DbsField pnd_Totals_Pnd_Total_Can;
    private DbsField pnd_Totals_Pnd_Total_Pr;
    private DbsField pnd_Cmpy_Cnt;

    private DbsGroup pnd_Company_Totals;
    private DbsField pnd_Company_Totals_Pnd_Tot_Cmpny;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Cnt;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Grs;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Ivc;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Int;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Fed;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Nra;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Sta;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Lcl;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Can;
    private DbsField pnd_Company_Totals_Pnd_Tot_Total_Pr;
    private DbsField pnd_Tot_Ndx;
    private DbsField pnd_Dashes;
    private DbsField pnd_State_Desc;

    private DbsGroup pnd_State_Table;
    private DbsField pnd_State_Table_Pnd_Residency_Code;
    private DbsField pnd_State_Table_Pnd_State_Indicator;
    private DbsField pnd_State_Table_Pnd_State_Description;

    private DbsGroup pnd_Var_File1;
    private DbsField pnd_Var_File1_Pnd_Tax_Year;

    private DbsGroup pnd_Var_File1__R_Field_2;
    private DbsField pnd_Var_File1_Pnd_Tax_Year_A;
    private DbsField pnd_Var_File1_Pnd_Start_Date;
    private DbsField pnd_Var_File1_Pnd_End_Date;
    private DbsField pnd_Var_File1_Pnd_Freq;
    private DbsField pnd_Cntry_Ind;
    private DbsField pnd_Cntry_Name;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0901 = new LdaTwrl0901();
        registerRecord(ldaTwrl0901);
        ldaTwrl0902 = new LdaTwrl0902();
        registerRecord(ldaTwrl0902);
        ldaTwrlmucn = new LdaTwrlmucn();
        registerRecord(ldaTwrlmucn);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Work_Area = localVariables.newGroupInRecord("pnd_Work_Area", "#WORK-AREA");
        pnd_Work_Area_Pnd_Rec_Read = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Rec_Read", "#REC-READ", FieldType.NUMERIC, 7);
        pnd_Work_Area_Pnd_Process_Rec = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Process_Rec", "#PROCESS-REC", FieldType.NUMERIC, 7);
        pnd_Work_Area_Pnd_Sv_Company = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Sv_Company", "#SV-COMPANY", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Sv_Source = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Sv_Source", "#SV-SOURCE", FieldType.STRING, 4);
        pnd_Work_Area_Pnd_Sv_Payset = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Sv_Payset", "#SV-PAYSET", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Print_Com = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Print_Com", "#PRINT-COM", FieldType.STRING, 4);
        pnd_Work_Area_Pnd_Trans_Desc = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Trans_Desc", "#TRANS-DESC", FieldType.STRING, 19);
        pnd_Work_Area_Pnd_Old_Sc = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Old_Sc", "#OLD-SC", FieldType.STRING, 4);
        pnd_Work_Area_Pnd_Old_Co = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Old_Co", "#OLD-CO", FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Contract_Payee = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Contract_Payee", "#CONTRACT-PAYEE", FieldType.STRING, 11);
        pnd_Work_Area_Pnd_I = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_I", "#I", FieldType.NUMERIC, 1);
        pnd_Work_Area_Pnd_Prt_Source_Code = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Prt_Source_Code", "#PRT-SOURCE-CODE", FieldType.STRING, 4);
        pnd_Work_Area_Pnd_New_Record = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_New_Record", "#NEW-RECORD", FieldType.BOOLEAN, 1);
        pnd_Work_Area_Pnd_Found = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Found", "#FOUND", FieldType.BOOLEAN, 1);
        pnd_Work_Area_Pnd_Sv_Res_Code = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Sv_Res_Code", "#SV-RES-CODE", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_K = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_Work_Area_Pnd_J = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Work_Area_Pnd_Sv_Search_Rc = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Sv_Search_Rc", "#SV-SEARCH-RC", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Sv_State_Desc = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Sv_State_Desc", "#SV-STATE-DESC", FieldType.STRING, 15);

        pnd_Work_Area__R_Field_1 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area__R_Field_1", "REDEFINE", pnd_Work_Area_Pnd_Sv_State_Desc);
        pnd_Work_Area_Pnd_Sv_State_Desc_Sub = pnd_Work_Area__R_Field_1.newFieldInGroup("pnd_Work_Area_Pnd_Sv_State_Desc_Sub", "#SV-STATE-DESC-SUB", FieldType.STRING, 
            11);
        pnd_Work_Area_Pnd_Print_Total = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Print_Total", "#PRINT-TOTAL", FieldType.STRING, 19);
        pnd_Work_Area_Pnd_Temp_Pr_Wthld = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Temp_Pr_Wthld", "#TEMP-PR-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Area_Pnd_Temp_State_Wthld = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Temp_State_Wthld", "#TEMP-STATE-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Work_Area_Pnd_Fatca = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Fatca", "#FATCA", FieldType.BOOLEAN, 1);
        pnd_Work_Area_Pnd_Hdg1 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Hdg1", "#HDG1", FieldType.STRING, 13);

        pnd_Totals = localVariables.newGroupArrayInRecord("pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Totals_Pnd_Total_Cnt = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Cnt", "#TOTAL-CNT", FieldType.NUMERIC, 7);
        pnd_Totals_Pnd_Total_Grs = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Grs", "#TOTAL-GRS", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Totals_Pnd_Total_Ivc = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Ivc", "#TOTAL-IVC", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Total_Int = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Int", "#TOTAL-INT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Total_Fed = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Fed", "#TOTAL-FED", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Total_Nra = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Nra", "#TOTAL-NRA", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Total_Sta = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Sta", "#TOTAL-STA", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Total_Lcl = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Lcl", "#TOTAL-LCL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Total_Can = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Can", "#TOTAL-CAN", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Total_Pr = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Total_Pr", "#TOTAL-PR", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cmpy_Cnt = localVariables.newFieldInRecord("pnd_Cmpy_Cnt", "#CMPY-CNT", FieldType.PACKED_DECIMAL, 3);

        pnd_Company_Totals = localVariables.newGroupArrayInRecord("pnd_Company_Totals", "#COMPANY-TOTALS", new DbsArrayController(1, 7));
        pnd_Company_Totals_Pnd_Tot_Cmpny = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Cmpny", "#TOT-CMPNY", FieldType.STRING, 4);
        pnd_Company_Totals_Pnd_Tot_Total_Cnt = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Cnt", "#TOT-TOTAL-CNT", FieldType.NUMERIC, 
            7);
        pnd_Company_Totals_Pnd_Tot_Total_Grs = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Grs", "#TOT-TOTAL-GRS", FieldType.PACKED_DECIMAL, 
            14, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Ivc = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Ivc", "#TOT-TOTAL-IVC", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Int = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Int", "#TOT-TOTAL-INT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Fed = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Fed", "#TOT-TOTAL-FED", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Nra = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Nra", "#TOT-TOTAL-NRA", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Sta = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Sta", "#TOT-TOTAL-STA", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Lcl = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Lcl", "#TOT-TOTAL-LCL", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Can = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Can", "#TOT-TOTAL-CAN", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Company_Totals_Pnd_Tot_Total_Pr = pnd_Company_Totals.newFieldInGroup("pnd_Company_Totals_Pnd_Tot_Total_Pr", "#TOT-TOTAL-PR", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Tot_Ndx = localVariables.newFieldInRecord("pnd_Tot_Ndx", "#TOT-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Dashes = localVariables.newFieldInRecord("pnd_Dashes", "#DASHES", FieldType.STRING, 131);
        pnd_State_Desc = localVariables.newFieldInRecord("pnd_State_Desc", "#STATE-DESC", FieldType.STRING, 19);

        pnd_State_Table = localVariables.newGroupInRecord("pnd_State_Table", "#STATE-TABLE");
        pnd_State_Table_Pnd_Residency_Code = pnd_State_Table.newFieldArrayInGroup("pnd_State_Table_Pnd_Residency_Code", "#RESIDENCY-CODE", FieldType.STRING, 
            2, new DbsArrayController(1, 100));
        pnd_State_Table_Pnd_State_Indicator = pnd_State_Table.newFieldArrayInGroup("pnd_State_Table_Pnd_State_Indicator", "#STATE-INDICATOR", FieldType.STRING, 
            1, new DbsArrayController(1, 100));
        pnd_State_Table_Pnd_State_Description = pnd_State_Table.newFieldArrayInGroup("pnd_State_Table_Pnd_State_Description", "#STATE-DESCRIPTION", FieldType.STRING, 
            19, new DbsArrayController(1, 100));

        pnd_Var_File1 = localVariables.newGroupInRecord("pnd_Var_File1", "#VAR-FILE1");
        pnd_Var_File1_Pnd_Tax_Year = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);

        pnd_Var_File1__R_Field_2 = pnd_Var_File1.newGroupInGroup("pnd_Var_File1__R_Field_2", "REDEFINE", pnd_Var_File1_Pnd_Tax_Year);
        pnd_Var_File1_Pnd_Tax_Year_A = pnd_Var_File1__R_Field_2.newFieldInGroup("pnd_Var_File1_Pnd_Tax_Year_A", "#TAX-YEAR-A", FieldType.STRING, 4);
        pnd_Var_File1_Pnd_Start_Date = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);
        pnd_Var_File1_Pnd_End_Date = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);
        pnd_Var_File1_Pnd_Freq = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Freq", "#FREQ", FieldType.STRING, 1);
        pnd_Cntry_Ind = localVariables.newFieldArrayInRecord("pnd_Cntry_Ind", "#CNTRY-IND", FieldType.STRING, 2, new DbsArrayController(1, 350));
        pnd_Cntry_Name = localVariables.newFieldArrayInRecord("pnd_Cntry_Name", "#CNTRY-NAME", FieldType.STRING, 35, new DbsArrayController(1, 350));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0901.initializeValues();
        ldaTwrl0902.initializeValues();
        ldaTwrlmucn.initializeValues();

        localVariables.reset();
        pnd_Work_Area_Pnd_New_Record.setInitialValue(true);
        pnd_Work_Area_Pnd_Found.setInitialValue(false);
        pnd_Work_Area_Pnd_Fatca.setInitialValue(false);
        pnd_Work_Area_Pnd_Hdg1.setInitialValue("NON-FATCA TAX");
        pnd_Cmpy_Cnt.setInitialValue(7);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0998() throws Exception
    {
        super("Twrp0998");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: FORMAT ( 2 ) PS = 60 LS = 132
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  11/2014
        pnd_Dashes.moveAll("=");                                                                                                                                          //Natural: MOVE ALL '=' TO #DASHES
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #FLAT-FILE1
        while (condition(getWorkFiles().read(1, ldaTwrl0901.getPnd_Flat_File1())))
        {
            pnd_Var_File1_Pnd_Tax_Year.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Tax_Year());                                                                        //Natural: ASSIGN #TAX-YEAR := #FF1-TAX-YEAR
            pnd_Var_File1_Pnd_Start_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Start_Date());                                                                    //Natural: ASSIGN #START-DATE := #FF1-START-DATE
            pnd_Var_File1_Pnd_End_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_End_Date());                                                                        //Natural: ASSIGN #END-DATE := #FF1-END-DATE
            pnd_Var_File1_Pnd_Freq.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Frequency());                                                                           //Natural: ASSIGN #FREQ := #FF1-FREQUENCY
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*                 /* CREATE A STATE TABLE BASED ON FILE98-TABLE2
        //*                         FOR NUMERIC RESIDENCY CODE
        DbsUtil.callnat(Twrn0903.class , getCurrentProcessState(), pnd_Var_File1_Pnd_Tax_Year_A, pnd_State_Table_Pnd_Residency_Code.getValue("*"), pnd_State_Table_Pnd_State_Indicator.getValue("*"),  //Natural: CALLNAT 'TWRN0903' #TAX-YEAR-A #RESIDENCY-CODE ( * ) #STATE-INDICATOR ( * ) #STATE-DESCRIPTION ( * )
            pnd_State_Table_Pnd_State_Description.getValue("*"));
        if (condition(Global.isEscape())) return;
        //*                 /* CREATE A TABLE FOR NON-NUMERIC RESIDENCY CODE
        DbsUtil.callnat(Twrn0904.class , getCurrentProcessState(), pnd_Var_File1_Pnd_Tax_Year_A, pnd_Cntry_Ind.getValue("*"), pnd_Cntry_Name.getValue("*"));              //Natural: CALLNAT 'TWRN0904' #TAX-YEAR-A #CNTRY-IND ( * ) #CNTRY-NAME ( * )
        if (condition(Global.isEscape())) return;
        //* *READ WORK FILE 2  RECORD #FLAT-FILE3 /* 11/2014
        //*  11/2014
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 2 #FLAT-FILE3
        while (condition(getWorkFiles().read(2, ldaTwrl0902.getPnd_Flat_File3())))
        {
            pnd_Work_Area_Pnd_Rec_Read.nadd(1);                                                                                                                           //Natural: ADD 1 TO #REC-READ
            //*  9/20/04 RM
            //*  11/2014
            if (condition(!(((((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("OL") || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("ML"))  //Natural: ACCEPT IF ( SUBSTRING ( #FF3-SOURCE-CODE,3,2 ) = 'OL' OR = 'ML' OR = 'PL' OR = 'NL' ) AND #FF3-TWRPYMNT-FATCA-IND NE 'Y'
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("PL")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("NL")) 
                && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Twrpymnt_Fatca_Ind().notEquals("Y")))))
            {
                continue;
            }
            pnd_Work_Area_Pnd_Process_Rec.nadd(1);                                                                                                                        //Natural: ADD 1 TO #PROCESS-REC
            //*   12/23/97
            pnd_Work_Area_Pnd_Found.setValue(false);                                                                                                                      //Natural: ASSIGN #FOUND := FALSE
            //*                                 /* CORRECTION ON ERRONEOUS ENTRY (PR)
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("PR")))                                                                           //Natural: IF #FF3-RESIDENCY-CODE = 'PR'
            {
                ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().setValue("42");                                                                                    //Natural: ASSIGN #FF3-RESIDENCY-CODE := '42'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Area_Pnd_Sv_Res_Code.equals(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code())))                                                  //Natural: IF #SV-RES-CODE = #FF3-RESIDENCY-CODE
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  NUMERIC RESIDENCY CODE
                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().greaterOrEqual("00") && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().lessOrEqual("99"))) //Natural: IF #FF3-RESIDENCY-CODE = '00' THRU '99'
                {
                    //*  SPECIAL CASE DUE ERROR ENTRY
                    if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("42")))                                                                   //Natural: IF #FF3-RESIDENCY-CODE = '42'
                    {
                        pnd_State_Desc.setValue("PUERTO RICO");                                                                                                           //Natural: ASSIGN #STATE-DESC := 'PUERTO RICO'
                        pnd_Work_Area_Pnd_Found.setValue(true);                                                                                                           //Natural: ASSIGN #FOUND := TRUE
                        //*  TO USE CREATED TABLE FOR NON-US
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        PND_PND_L1530:                                                                                                                                    //Natural: FOR #K 1 100
                        for (pnd_Work_Area_Pnd_K.setValue(1); condition(pnd_Work_Area_Pnd_K.lessOrEqual(100)); pnd_Work_Area_Pnd_K.nadd(1))
                        {
                            if (condition(pnd_State_Table_Pnd_Residency_Code.getValue(pnd_Work_Area_Pnd_K).equals("  ")))                                                 //Natural: IF #RESIDENCY-CODE ( #K ) = '  '
                            {
                                if (true) break PND_PND_L1530;                                                                                                            //Natural: ESCAPE BOTTOM ( ##L1530. )
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals(pnd_State_Table_Pnd_Residency_Code.getValue(pnd_Work_Area_Pnd_K)))) //Natural: IF #FF3-RESIDENCY-CODE = #RESIDENCY-CODE ( #K )
                                {
                                    pnd_State_Desc.setValue(pnd_State_Table_Pnd_State_Description.getValue(pnd_Work_Area_Pnd_K));                                         //Natural: ASSIGN #STATE-DESC := #STATE-DESCRIPTION ( #K )
                                    pnd_Work_Area_Pnd_Found.setValue(true);                                                                                               //Natural: ASSIGN #FOUND := TRUE
                                    if (true) break PND_PND_L1530;                                                                                                        //Natural: ESCAPE BOTTOM ( ##L1530. )
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  NON-NUMERIC RESIDENCY CODE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*    CHECK TWRLMUCN FIRST FOR COMMON RESIDENCY CODE BUT DIFF. COUNTRY
                    //*          BEFORE CHECKING THE COUNTRY CODE TABLE
                    PND_PND_L1700:                                                                                                                                        //Natural: FOR #J 1 #CNTRY-MAX
                    for (pnd_Work_Area_Pnd_J.setValue(1); condition(pnd_Work_Area_Pnd_J.lessOrEqual(ldaTwrlmucn.getPnd_Twrlmucn_Pnd_Cntry_Max())); pnd_Work_Area_Pnd_J.nadd(1))
                    {
                        if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals(ldaTwrlmucn.getPnd_Twrlmucn_Pnd_Irs_Cntry_Code().getValue(pnd_Work_Area_Pnd_J)))) //Natural: IF #FF3-RESIDENCY-CODE = #IRS-CNTRY-CODE ( #J )
                        {
                            pnd_State_Desc.setValue(ldaTwrlmucn.getPnd_Twrlmucn_Pnd_Cntry_Desc().getValue(pnd_Work_Area_Pnd_J));                                          //Natural: ASSIGN #STATE-DESC := #CNTRY-DESC ( #J )
                            pnd_Work_Area_Pnd_Found.setValue(true);                                                                                                       //Natural: ASSIGN #FOUND := TRUE
                            if (true) break PND_PND_L1700;                                                                                                                //Natural: ESCAPE BOTTOM ( ##L1700. )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  DO NOT SEARCH ANYMORE
                    if (condition(pnd_Work_Area_Pnd_Found.getBoolean()))                                                                                                  //Natural: IF #FOUND
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        PND_PND_L1810:                                                                                                                                    //Natural: FOR #K 1 350
                        for (pnd_Work_Area_Pnd_K.setValue(1); condition(pnd_Work_Area_Pnd_K.lessOrEqual(350)); pnd_Work_Area_Pnd_K.nadd(1))
                        {
                            if (condition(pnd_Cntry_Ind.getValue(pnd_Work_Area_Pnd_K).equals("  ")))                                                                      //Natural: IF #CNTRY-IND ( #K ) = '  '
                            {
                                if (true) break PND_PND_L1810;                                                                                                            //Natural: ESCAPE BOTTOM ( ##L1810. )
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals(pnd_Cntry_Ind.getValue(pnd_Work_Area_Pnd_K))))                //Natural: IF #FF3-RESIDENCY-CODE = #CNTRY-IND ( #K )
                                {
                                    pnd_State_Desc.setValue(pnd_Cntry_Name.getValue(pnd_Work_Area_Pnd_K));                                                                //Natural: ASSIGN #STATE-DESC := #CNTRY-NAME ( #K )
                                    pnd_Work_Area_Pnd_Found.setValue(true);                                                                                               //Natural: ASSIGN #FOUND := TRUE
                                    if (true) break PND_PND_L1810;                                                                                                        //Natural: ESCAPE BOTTOM ( ##L1810. )
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  IF #FF3-RESIDENCY-CODE = '00' THRU '99'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Work_Area_Pnd_Found.equals(false)))                                                                                                     //Natural: IF #FOUND = FALSE
                {
                    if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals(pnd_Work_Area_Pnd_Sv_Search_Rc)))                                         //Natural: IF #FF3-RESIDENCY-CODE = #SV-SEARCH-RC
                    {
                        ignore();
                        //*  11/2014 START
                        //*      ELSE  DISPLAY (2)
                        //*          'COMPANY/CODE '  #FF3-COMPANY-CODE
                        //*          'STATE/CODE'     #FF3-RESIDENCY-CODE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(3),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code(),new                  //Natural: WRITE ( 2 ) / 3X #FF3-COMPANY-CODE 8X #FF3-RESIDENCY-CODE
                            ColumnSpacing(8),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code());
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Work_Area_Pnd_Sv_Search_Rc.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code());                                                  //Natural: ASSIGN #SV-SEARCH-RC := #FF3-RESIDENCY-CODE
                        pnd_State_Desc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code(),                   //Natural: COMPRESS #FF3-RESIDENCY-CODE '*** NO NAME ***' TO #STATE-DESC LEAVING NO SPACE
                            "*** NO NAME ***"));
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*   IF #SV-RES-CODE = #FF3-RESIDENCY-CODE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Area_Pnd_New_Record.getBoolean()))                                                                                                     //Natural: IF #NEW-RECORD
            {
                pnd_Work_Area_Pnd_New_Record.setValue(false);                                                                                                             //Natural: ASSIGN #NEW-RECORD := FALSE
                pnd_Work_Area_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                              //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
                //*  12/23/99
                                                                                                                                                                          //Natural: PERFORM SAVE-RES-DESC
                sub_Save_Res_Desc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code().equals(pnd_Work_Area_Pnd_Sv_Company)))                                                     //Natural: IF #FF3-COMPANY-CODE = #SV-COMPANY
            {
                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals(pnd_Work_Area_Pnd_Sv_Res_Code)))                                              //Natural: IF #FF3-RESIDENCY-CODE = #SV-RES-CODE
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM BREAK-RESIDENCY-CDE
                    sub_Break_Residency_Cde();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM BREAK-COMPANY
                sub_Break_Company();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL
            sub_Print_Detail();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: AT END OF DATA;//Natural: END-WORK
        READWORK02_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            //*  11/2014
            if (condition(pnd_Work_Area_Pnd_Process_Rec.equals(getZero())))                                                                                               //Natural: IF #PROCESS-REC = 0
            {
                //*      WRITE (1) 40T '*****  NO RECORDS PROCESSED ***** '
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM BREAK-COMPANY
                sub_Break_Company();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            getReports().eject(1, true);                                                                                                                                  //Natural: EJECT ( 1 )
            getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM()," - CONTROL TOTAL",NEWLINE,NEWLINE,NEWLINE,"RUNDATE    : ",Global.getDATX(),                   //Natural: WRITE ( 1 ) *PROGRAM ' - CONTROL TOTAL' // / 'RUNDATE    : ' *DATX ( EM = MM/DD/YYYY ) / 'RUNTIME    : ' *TIMX / 'TAX YEAR   : ' #TAX-YEAR / 'DATE RANGE : ' #START-DATE 'THRU' #END-DATE /// '1) NO. OF RECORDS READ      : ' #REC-READ / '2) NO. OF RECORDS PROCESSED : ' #PROCESS-REC
                new ReportEditMask ("MM/DD/YYYY"),NEWLINE,"RUNTIME    : ",Global.getTIMX(),NEWLINE,"TAX YEAR   : ",pnd_Var_File1_Pnd_Tax_Year,NEWLINE,"DATE RANGE : ",
                pnd_Var_File1_Pnd_Start_Date,"THRU",pnd_Var_File1_Pnd_End_Date,NEWLINE,NEWLINE,NEWLINE,"1) NO. OF RECORDS READ      : ",pnd_Work_Area_Pnd_Rec_Read,
                NEWLINE,"2) NO. OF RECORDS PROCESSED : ",pnd_Work_Area_Pnd_Process_Rec);
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //*  11/2014 START
        if (condition(pnd_Work_Area_Pnd_Process_Rec.equals(getZero())))                                                                                                   //Natural: IF #PROCESS-REC = 0
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(40),"*****  NO RECORDS PROCESSED ***** ");                                                          //Natural: WRITE ( 1 ) 40T '*****  NO RECORDS PROCESSED ***** '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Work_Area.resetInitial();                                                                                                                                     //Natural: RESET INITIAL #WORK-AREA #TOTALS ( * )
        pnd_Totals.getValue("*").resetInitial();
        getWorkFiles().close(2);                                                                                                                                          //Natural: CLOSE WORK FILE 2
        pnd_Work_Area_Pnd_Fatca.setValue(true);                                                                                                                           //Natural: ASSIGN #FATCA := TRUE
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //*  11/2014
        READWORK03:                                                                                                                                                       //Natural: READ WORK FILE 2 #FLAT-FILE3
        while (condition(getWorkFiles().read(2, ldaTwrl0902.getPnd_Flat_File3())))
        {
            pnd_Work_Area_Pnd_Rec_Read.nadd(1);                                                                                                                           //Natural: ADD 1 TO #REC-READ
            //*  9/20/04 RM
            if (condition(!(((((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("OL") || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("ML"))  //Natural: ACCEPT IF ( SUBSTRING ( #FF3-SOURCE-CODE,3,2 ) = 'OL' OR = 'ML' OR = 'PL' OR = 'NL' ) AND #FF3-TWRPYMNT-FATCA-IND EQ 'Y'
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("PL")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(3,2).equals("NL")) 
                && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Twrpymnt_Fatca_Ind().equals("Y")))))
            {
                continue;
            }
            pnd_Work_Area_Pnd_Process_Rec.nadd(1);                                                                                                                        //Natural: ADD 1 TO #PROCESS-REC
            //*   12/23/97
            pnd_Work_Area_Pnd_Found.setValue(false);                                                                                                                      //Natural: ASSIGN #FOUND := FALSE
            //*                                 /* CORRECTION ON ERRONEOUS ENTRY (PR)
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("PR")))                                                                           //Natural: IF #FF3-RESIDENCY-CODE = 'PR'
            {
                ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().setValue("42");                                                                                    //Natural: ASSIGN #FF3-RESIDENCY-CODE := '42'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Area_Pnd_Sv_Res_Code.equals(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code())))                                                  //Natural: IF #SV-RES-CODE = #FF3-RESIDENCY-CODE
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  NUMERIC RESIDENCY CODE
                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().greaterOrEqual("00") && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().lessOrEqual("99"))) //Natural: IF #FF3-RESIDENCY-CODE = '00' THRU '99'
                {
                    //*  SPECIAL CASE DUE ERROR ENTRY
                    if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("42")))                                                                   //Natural: IF #FF3-RESIDENCY-CODE = '42'
                    {
                        pnd_State_Desc.setValue("PUERTO RICO");                                                                                                           //Natural: ASSIGN #STATE-DESC := 'PUERTO RICO'
                        pnd_Work_Area_Pnd_Found.setValue(true);                                                                                                           //Natural: ASSIGN #FOUND := TRUE
                        //*  TO USE CREATED TABLE FOR NON-US
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        PND_PND_L2880:                                                                                                                                    //Natural: FOR #K 1 100
                        for (pnd_Work_Area_Pnd_K.setValue(1); condition(pnd_Work_Area_Pnd_K.lessOrEqual(100)); pnd_Work_Area_Pnd_K.nadd(1))
                        {
                            if (condition(pnd_State_Table_Pnd_Residency_Code.getValue(pnd_Work_Area_Pnd_K).equals("  ")))                                                 //Natural: IF #RESIDENCY-CODE ( #K ) = '  '
                            {
                                if (true) break PND_PND_L2880;                                                                                                            //Natural: ESCAPE BOTTOM ( ##L2880. )
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals(pnd_State_Table_Pnd_Residency_Code.getValue(pnd_Work_Area_Pnd_K)))) //Natural: IF #FF3-RESIDENCY-CODE = #RESIDENCY-CODE ( #K )
                                {
                                    pnd_State_Desc.setValue(pnd_State_Table_Pnd_State_Description.getValue(pnd_Work_Area_Pnd_K));                                         //Natural: ASSIGN #STATE-DESC := #STATE-DESCRIPTION ( #K )
                                    pnd_Work_Area_Pnd_Found.setValue(true);                                                                                               //Natural: ASSIGN #FOUND := TRUE
                                    if (true) break PND_PND_L2880;                                                                                                        //Natural: ESCAPE BOTTOM ( ##L2880. )
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  NON-NUMERIC RESIDENCY CODE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*    CHECK TWRLMUCN FIRST FOR COMMON RESIDENCY CODE BUT DIFF. COUNTRY
                    //*          BEFORE CHECKING THE COUNTRY CODE TABLE
                    PND_PND_L3050:                                                                                                                                        //Natural: FOR #J 1 #CNTRY-MAX
                    for (pnd_Work_Area_Pnd_J.setValue(1); condition(pnd_Work_Area_Pnd_J.lessOrEqual(ldaTwrlmucn.getPnd_Twrlmucn_Pnd_Cntry_Max())); pnd_Work_Area_Pnd_J.nadd(1))
                    {
                        if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals(ldaTwrlmucn.getPnd_Twrlmucn_Pnd_Irs_Cntry_Code().getValue(pnd_Work_Area_Pnd_J)))) //Natural: IF #FF3-RESIDENCY-CODE = #IRS-CNTRY-CODE ( #J )
                        {
                            pnd_State_Desc.setValue(ldaTwrlmucn.getPnd_Twrlmucn_Pnd_Cntry_Desc().getValue(pnd_Work_Area_Pnd_J));                                          //Natural: ASSIGN #STATE-DESC := #CNTRY-DESC ( #J )
                            pnd_Work_Area_Pnd_Found.setValue(true);                                                                                                       //Natural: ASSIGN #FOUND := TRUE
                            if (true) break PND_PND_L3050;                                                                                                                //Natural: ESCAPE BOTTOM ( ##L3050. )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  DO NOT SEARCH ANYMORE
                    if (condition(pnd_Work_Area_Pnd_Found.getBoolean()))                                                                                                  //Natural: IF #FOUND
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        PND_PND_L3160:                                                                                                                                    //Natural: FOR #K 1 350
                        for (pnd_Work_Area_Pnd_K.setValue(1); condition(pnd_Work_Area_Pnd_K.lessOrEqual(350)); pnd_Work_Area_Pnd_K.nadd(1))
                        {
                            if (condition(pnd_Cntry_Ind.getValue(pnd_Work_Area_Pnd_K).equals("  ")))                                                                      //Natural: IF #CNTRY-IND ( #K ) = '  '
                            {
                                if (true) break PND_PND_L3160;                                                                                                            //Natural: ESCAPE BOTTOM ( ##L3160. )
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals(pnd_Cntry_Ind.getValue(pnd_Work_Area_Pnd_K))))                //Natural: IF #FF3-RESIDENCY-CODE = #CNTRY-IND ( #K )
                                {
                                    pnd_State_Desc.setValue(pnd_Cntry_Name.getValue(pnd_Work_Area_Pnd_K));                                                                //Natural: ASSIGN #STATE-DESC := #CNTRY-NAME ( #K )
                                    pnd_Work_Area_Pnd_Found.setValue(true);                                                                                               //Natural: ASSIGN #FOUND := TRUE
                                    if (true) break PND_PND_L3160;                                                                                                        //Natural: ESCAPE BOTTOM ( ##L3160. )
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  IF #FF3-RESIDENCY-CODE = '00' THRU '99'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Work_Area_Pnd_Found.equals(false)))                                                                                                     //Natural: IF #FOUND = FALSE
                {
                    if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals(pnd_Work_Area_Pnd_Sv_Search_Rc)))                                         //Natural: IF #FF3-RESIDENCY-CODE = #SV-SEARCH-RC
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(3),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code(),new                  //Natural: WRITE ( 2 ) / 3X #FF3-COMPANY-CODE 8X #FF3-RESIDENCY-CODE
                            ColumnSpacing(8),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code());
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Work_Area_Pnd_Sv_Search_Rc.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code());                                                  //Natural: ASSIGN #SV-SEARCH-RC := #FF3-RESIDENCY-CODE
                        pnd_State_Desc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code(),                   //Natural: COMPRESS #FF3-RESIDENCY-CODE '*** NO NAME ***' TO #STATE-DESC LEAVING NO SPACE
                            "*** NO NAME ***"));
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*   IF #SV-RES-CODE = #FF3-RESIDENCY-CODE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Area_Pnd_New_Record.getBoolean()))                                                                                                     //Natural: IF #NEW-RECORD
            {
                pnd_Work_Area_Pnd_New_Record.setValue(false);                                                                                                             //Natural: ASSIGN #NEW-RECORD := FALSE
                pnd_Work_Area_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                              //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
                //*  12/23/99
                                                                                                                                                                          //Natural: PERFORM SAVE-RES-DESC
                sub_Save_Res_Desc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code().equals(pnd_Work_Area_Pnd_Sv_Company)))                                                     //Natural: IF #FF3-COMPANY-CODE = #SV-COMPANY
            {
                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals(pnd_Work_Area_Pnd_Sv_Res_Code)))                                              //Natural: IF #FF3-RESIDENCY-CODE = #SV-RES-CODE
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM BREAK-RESIDENCY-CDE
                    sub_Break_Residency_Cde();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM BREAK-COMPANY
                sub_Break_Company();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL
            sub_Print_Detail();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: AT END OF DATA;//Natural: END-WORK
        READWORK03_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            if (condition(pnd_Work_Area_Pnd_Process_Rec.equals(getZero())))                                                                                               //Natural: IF #PROCESS-REC = 0
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM BREAK-COMPANY
                sub_Break_Company();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            getReports().eject(1, true);                                                                                                                                  //Natural: EJECT ( 1 )
            getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM()," - CONTROL TOTAL",NEWLINE,NEWLINE,NEWLINE,"RUNDATE    : ",Global.getDATX(),                   //Natural: WRITE ( 1 ) *PROGRAM ' - CONTROL TOTAL' // / 'RUNDATE    : ' *DATX ( EM = MM/DD/YYYY ) / 'RUNTIME    : ' *TIMX / 'TAX YEAR   : ' #TAX-YEAR / 'DATE RANGE : ' #START-DATE 'THRU' #END-DATE /// '1) NO. OF RECORDS READ      : ' #REC-READ / '2) NO. OF RECORDS PROCESSED : ' #PROCESS-REC
                new ReportEditMask ("MM/DD/YYYY"),NEWLINE,"RUNTIME    : ",Global.getTIMX(),NEWLINE,"TAX YEAR   : ",pnd_Var_File1_Pnd_Tax_Year,NEWLINE,"DATE RANGE : ",
                pnd_Var_File1_Pnd_Start_Date,"THRU",pnd_Var_File1_Pnd_End_Date,NEWLINE,NEWLINE,NEWLINE,"1) NO. OF RECORDS READ      : ",pnd_Work_Area_Pnd_Rec_Read,
                NEWLINE,"2) NO. OF RECORDS PROCESSED : ",pnd_Work_Area_Pnd_Process_Rec);
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        if (condition(pnd_Work_Area_Pnd_Process_Rec.equals(getZero())))                                                                                                   //Natural: IF #PROCESS-REC = 0
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(40),"*****  NO RECORDS PROCESSED ***** ");                                                          //Natural: WRITE ( 1 ) 40T '*****  NO RECORDS PROCESSED ***** '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-COMPANY-TOTALS
        sub_Print_Company_Totals();
        if (condition(Global.isEscape())) {return;}
        //*                  -------------
        //* ***********************************************************************
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CO-DESC                                                                                                              //Natural: AT TOP OF PAGE ( 2 )
        //*  NONE IGNORE
    }
    private void sub_Save_Res_Desc() throws Exception                                                                                                                     //Natural: SAVE-RES-DESC
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Work_Area_Pnd_Sv_Res_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code());                                                                   //Natural: ASSIGN #SV-RES-CODE := #FF3-RESIDENCY-CODE
        pnd_Work_Area_Pnd_Sv_State_Desc.setValue(pnd_State_Desc);                                                                                                         //Natural: ASSIGN #SV-STATE-DESC := #STATE-DESC
    }
    private void sub_Break_Residency_Cde() throws Exception                                                                                                               //Natural: BREAK-RESIDENCY-CDE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Work_Area_Pnd_Print_Total.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TOTAL (", pnd_Work_Area_Pnd_Sv_State_Desc_Sub, ")"));                     //Natural: COMPRESS 'TOTAL (' #SV-STATE-DESC-SUB ')' TO #PRINT-TOTAL LEAVING NO SPACE
        getReports().write(1, ReportOption.NOTITLE,pnd_Work_Area_Pnd_Print_Total,new TabSetting(24),pnd_Totals_Pnd_Total_Cnt.getValue(1), new ReportEditMask              //Natural: WRITE ( 1 ) #PRINT-TOTAL 24T #TOTAL-CNT ( 1 ) 46T #TOTAL-GRS ( 1 ) 67T #TOTAL-IVC ( 1 ) 84T #TOTAL-FED ( 1 ) 101T #TOTAL-NRA ( 1 ) 117T #TOTAL-STA ( 1 ) / 67T #TOTAL-LCL ( 1 ) 84T #TOTAL-CAN ( 1 ) 101T #TOTAL-PR ( 1 ) 117T #TOTAL-INT ( 1 )
            ("Z,ZZZ,ZZ9"),new TabSetting(46),pnd_Totals_Pnd_Total_Grs.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(67),pnd_Totals_Pnd_Total_Ivc.getValue(1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(84),pnd_Totals_Pnd_Total_Fed.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(101),pnd_Totals_Pnd_Total_Nra.getValue(1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(117),pnd_Totals_Pnd_Total_Sta.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new 
            TabSetting(67),pnd_Totals_Pnd_Total_Lcl.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(84),pnd_Totals_Pnd_Total_Can.getValue(1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(101),pnd_Totals_Pnd_Total_Pr.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(117),pnd_Totals_Pnd_Total_Int.getValue(1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        pnd_Totals.getValue(1).reset();                                                                                                                                   //Natural: RESET #TOTALS ( 1 )
                                                                                                                                                                          //Natural: PERFORM SAVE-RES-DESC
        sub_Save_Res_Desc();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Break_Company() throws Exception                                                                                                                     //Natural: BREAK-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

                                                                                                                                                                          //Natural: PERFORM BREAK-RESIDENCY-CDE
        sub_Break_Residency_Cde();
        if (condition(Global.isEscape())) {return;}
        //*  11/2014 START
                                                                                                                                                                          //Natural: PERFORM CO-DESC
        sub_Co_Desc();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Company_Totals_Pnd_Tot_Cmpny.getValue("*").equals(pnd_Work_Area_Pnd_Print_Com)))                                                                //Natural: IF #TOT-CMPNY ( * ) = #PRINT-COM
        {
            DbsUtil.examine(new ExamineSource(pnd_Company_Totals_Pnd_Tot_Cmpny.getValue("*")), new ExamineSearch(pnd_Work_Area_Pnd_Print_Com), new ExamineGivingIndex(pnd_Tot_Ndx)); //Natural: EXAMINE #TOT-CMPNY ( * ) FOR #PRINT-COM GIVING INDEX #TOT-NDX
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR01:                                                                                                                                                        //Natural: FOR #TOT-NDX 1 #CMPY-CNT
            for (pnd_Tot_Ndx.setValue(1); condition(pnd_Tot_Ndx.lessOrEqual(pnd_Cmpy_Cnt)); pnd_Tot_Ndx.nadd(1))
            {
                if (condition(pnd_Company_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx).equals(" ")))                                                                        //Natural: IF #TOT-CMPNY ( #TOT-NDX ) = ' '
                {
                    pnd_Company_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx).setValue(pnd_Work_Area_Pnd_Print_Com);                                                         //Natural: ASSIGN #TOT-CMPNY ( #TOT-NDX ) := #PRINT-COM
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Company_Totals_Pnd_Tot_Total_Cnt.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Cnt.getValue(2));                                                            //Natural: ASSIGN #TOT-TOTAL-CNT ( #TOT-NDX ) := #TOT-TOTAL-CNT ( #TOT-NDX ) + #TOTAL-CNT ( 2 )
        pnd_Company_Totals_Pnd_Tot_Total_Grs.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Grs.getValue(2));                                                            //Natural: ASSIGN #TOT-TOTAL-GRS ( #TOT-NDX ) := #TOT-TOTAL-GRS ( #TOT-NDX ) + #TOTAL-GRS ( 2 )
        pnd_Company_Totals_Pnd_Tot_Total_Ivc.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Ivc.getValue(2));                                                            //Natural: ASSIGN #TOT-TOTAL-IVC ( #TOT-NDX ) := #TOT-TOTAL-IVC ( #TOT-NDX ) + #TOTAL-IVC ( 2 )
        pnd_Company_Totals_Pnd_Tot_Total_Int.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Int.getValue(2));                                                            //Natural: ASSIGN #TOT-TOTAL-INT ( #TOT-NDX ) := #TOT-TOTAL-INT ( #TOT-NDX ) + #TOTAL-INT ( 2 )
        pnd_Company_Totals_Pnd_Tot_Total_Fed.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Fed.getValue(2));                                                            //Natural: ASSIGN #TOT-TOTAL-FED ( #TOT-NDX ) := #TOT-TOTAL-FED ( #TOT-NDX ) + #TOTAL-FED ( 2 )
        pnd_Company_Totals_Pnd_Tot_Total_Nra.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Nra.getValue(2));                                                            //Natural: ASSIGN #TOT-TOTAL-NRA ( #TOT-NDX ) := #TOT-TOTAL-NRA ( #TOT-NDX ) + #TOTAL-NRA ( 2 )
        pnd_Company_Totals_Pnd_Tot_Total_Sta.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Sta.getValue(2));                                                            //Natural: ASSIGN #TOT-TOTAL-STA ( #TOT-NDX ) := #TOT-TOTAL-STA ( #TOT-NDX ) + #TOTAL-STA ( 2 )
        pnd_Company_Totals_Pnd_Tot_Total_Lcl.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Lcl.getValue(2));                                                            //Natural: ASSIGN #TOT-TOTAL-LCL ( #TOT-NDX ) := #TOT-TOTAL-LCL ( #TOT-NDX ) + #TOTAL-LCL ( 2 )
        pnd_Company_Totals_Pnd_Tot_Total_Can.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Can.getValue(2));                                                            //Natural: ASSIGN #TOT-TOTAL-CAN ( #TOT-NDX ) := #TOT-TOTAL-CAN ( #TOT-NDX ) + #TOTAL-CAN ( 2 )
        pnd_Company_Totals_Pnd_Tot_Total_Pr.getValue(pnd_Tot_Ndx).nadd(pnd_Totals_Pnd_Total_Pr.getValue(2));                                                              //Natural: ASSIGN #TOT-TOTAL-PR ( #TOT-NDX ) := #TOT-TOTAL-PR ( #TOT-NDX ) + #TOTAL-PR ( 2 )
        //*  11/2014 END
        getReports().write(1, ReportOption.NOTITLE,"GRANDTOTAL (",pnd_Work_Area_Pnd_Print_Com,")",new TabSetting(24),pnd_Totals_Pnd_Total_Cnt.getValue(2),                //Natural: WRITE ( 1 ) 'GRANDTOTAL (' #PRINT-COM ')' 24T #TOTAL-CNT ( 2 ) 46T #TOTAL-GRS ( 2 ) 67T #TOTAL-IVC ( 2 ) 84T #TOTAL-FED ( 2 ) 101T #TOTAL-NRA ( 2 ) 117T #TOTAL-STA ( 2 ) / 67T #TOTAL-LCL ( 2 ) 84T #TOTAL-CAN ( 2 ) 101T #TOTAL-PR ( 2 ) 117T #TOTAL-INT ( 2 )
            new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(46),pnd_Totals_Pnd_Total_Grs.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(67),pnd_Totals_Pnd_Total_Ivc.getValue(2), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(84),pnd_Totals_Pnd_Total_Fed.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(101),pnd_Totals_Pnd_Total_Nra.getValue(2), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(117),pnd_Totals_Pnd_Total_Sta.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new 
            TabSetting(67),pnd_Totals_Pnd_Total_Lcl.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(84),pnd_Totals_Pnd_Total_Can.getValue(2), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(101),pnd_Totals_Pnd_Total_Pr.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(117),pnd_Totals_Pnd_Total_Int.getValue(2), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
        pnd_Totals.getValue("*").reset();                                                                                                                                 //Natural: RESET #TOTALS ( * )
        pnd_Work_Area_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                                      //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
    }
    private void sub_Print_Company_Totals() throws Exception                                                                                                              //Natural: PRINT-COMPANY-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(1, ReportOption.NOTITLE,pnd_Dashes);                                                                                                           //Natural: WRITE ( 1 ) #DASHES
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #TOT-NDX 1 #CMPY-CNT
        for (pnd_Tot_Ndx.setValue(1); condition(pnd_Tot_Ndx.lessOrEqual(pnd_Cmpy_Cnt)); pnd_Tot_Ndx.nadd(1))
        {
            if (condition(pnd_Company_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx).equals(" ")))                                                                            //Natural: IF #TOT-CMPNY ( #TOT-NDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,"OVERALLL TOTAL",pnd_Company_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx),new TabSetting(24),pnd_Company_Totals_Pnd_Tot_Total_Cnt.getValue(pnd_Tot_Ndx),  //Natural: WRITE ( 1 ) 'OVERALLL TOTAL' #TOT-CMPNY ( #TOT-NDX ) 24T #TOT-TOTAL-CNT ( #TOT-NDX ) 46T #TOT-TOTAL-GRS ( #TOT-NDX ) 67T #TOT-TOTAL-IVC ( #TOT-NDX ) 84T #TOT-TOTAL-FED ( #TOT-NDX ) 101T #TOT-TOTAL-NRA ( #TOT-NDX ) 117T #TOT-TOTAL-STA ( #TOT-NDX ) / 67T #TOT-TOTAL-LCL ( #TOT-NDX ) 84T #TOT-TOTAL-CAN ( #TOT-NDX ) 101T #TOT-TOTAL-PR ( #TOT-NDX ) 117T #TOT-TOTAL-INT ( #TOT-NDX )
                new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(46),pnd_Company_Totals_Pnd_Tot_Total_Grs.getValue(pnd_Tot_Ndx), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9.99-"),new 
                TabSetting(67),pnd_Company_Totals_Pnd_Tot_Total_Ivc.getValue(pnd_Tot_Ndx), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(84),pnd_Company_Totals_Pnd_Tot_Total_Fed.getValue(pnd_Tot_Ndx), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(101),pnd_Company_Totals_Pnd_Tot_Total_Nra.getValue(pnd_Tot_Ndx), new ReportEditMask 
                ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(117),pnd_Company_Totals_Pnd_Tot_Total_Sta.getValue(pnd_Tot_Ndx), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new 
                TabSetting(67),pnd_Company_Totals_Pnd_Tot_Total_Lcl.getValue(pnd_Tot_Ndx), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(84),pnd_Company_Totals_Pnd_Tot_Total_Can.getValue(pnd_Tot_Ndx), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(101),pnd_Company_Totals_Pnd_Tot_Total_Pr.getValue(pnd_Tot_Ndx), new ReportEditMask 
                ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(117),pnd_Company_Totals_Pnd_Tot_Total_Int.getValue(pnd_Tot_Ndx), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Print_Detail() throws Exception                                                                                                                      //Natural: PRINT-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ------------
        pnd_Work_Area_Pnd_Temp_Pr_Wthld.reset();                                                                                                                          //Natural: RESET #TEMP-PR-WTHLD #TEMP-STATE-WTHLD
        pnd_Work_Area_Pnd_Temp_State_Wthld.reset();
        if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("PR") || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("RQ")          //Natural: IF #FF3-RESIDENCY-CODE = 'PR' OR = 'RQ' OR = '42'
            || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("42")))
        {
            pnd_Work_Area_Pnd_Temp_Pr_Wthld.nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld());                                                                    //Natural: ADD #FF3-STATE-WTHLD TO #TEMP-PR-WTHLD
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Work_Area_Pnd_Temp_State_Wthld.nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld());                                                                 //Natural: ADD #FF3-STATE-WTHLD TO #TEMP-STATE-WTHLD
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Work_Area_Pnd_Contract_Payee.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Contract_Nbr(),                   //Natural: COMPRESS #FF3-CONTRACT-NBR '-' #FF3-PAYEE-CDE TO #CONTRACT-PAYEE LEAVING NO SPACE
            "-", ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Payee_Cde()));
        //*  11/2014 START
        //* *DISPLAY (1)
        //* *  '////RC'                    #FF3-RESIDENCY-CODE
        //* *  '///TAX-ID-NO'              #FF3-TAX-ID-NBR
        //* *  /  'RES.CODE DESC'             #STATE-DESC
        //* *  '///CONTRACT/PAYEE CODE'    #CONTRACT-PAYEE
        //* *  '///PAYMENT/DATE'           #FF3-PYMNT-DATE
        //* *  '///IVC/IND'                #FF3-IVC-IND
        //* *  '///GROSS/AMOUNT'           #FF3-GROSS-AMT     (EM=ZZZ,ZZZ,ZZ9.99-)
        //* *  '  '
        //* *  'TAX FREE/IVC/-----------'  #FF3-IVC-AMT       (EM=Z,ZZZ,ZZ9.99-)
        //* *  /  '  '
        //* *  'LOCAL TAX/WITHHOLDING'     #FF3-LOCAL-WTHLD   (EM=Z,ZZZ,ZZ9.99-)
        //* *  '  '
        //* *  'FEDERAL TAX/WITHHOLDING/-----------'
        //* *  #FF3-FED-WTHLD-AMT (EM=Z,ZZZ,ZZ9.99-)
        //* *  /  '  '
        //* *  'CANADIAN/WITHHOLDING'      #FF3-CAN-WTHLD-AMT (EM=Z,ZZZ,ZZ9.99-)
        //* *  '  '
        //* *  'NRA TAX/WITHHOLDING/-----------'
        //* *  #FF3-NRA-WTHLD-AMT (EM=Z,ZZZ,ZZ9.99-)
        //* *  /  '  '
        //* *  'PUERTO RICO/WITHHOLDING'   #TEMP-PR-WTHLD     (EM=Z,ZZZ,ZZ9.99-)
        //* *  '  '
        //* *  'STATE TAX/WITHHOLDING/-----------'
        //* *  #TEMP-STATE-WTHLD  (EM=Z,ZZZ,ZZ9.99-)
        //* *  /  '  '
        //* *  'INTEREST/AMOUNT'           #FF3-INT-AMT       (EM=Z,ZZZ,ZZ9.99-)
        //* *SKIP(1) 1
        getReports().write(1, ReportOption.NOTITLE,ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code(),new TabSetting(4),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Id_Nbr(),new  //Natural: WRITE ( 1 ) #FF3-RESIDENCY-CODE 4T #FF3-TAX-ID-NBR 24T #CONTRACT-PAYEE 36T #FF3-PYMNT-DATE 45T #FF3-IVC-IND 50T #FF3-GROSS-AMT ( EM = ZZZ,ZZZ,ZZ9.99- ) 69T #FF3-IVC-AMT ( EM = Z,ZZZ,ZZ9.99- ) 86T #FF3-FED-WTHLD-AMT ( EM = Z,ZZZ,ZZ9.99- ) 103T #FF3-NRA-WTHLD-AMT ( EM = Z,ZZZ,ZZ9.99- ) 119T #TEMP-STATE-WTHLD ( EM = Z,ZZZ,ZZ9.99- ) / 4T #STATE-DESC 69T #FF3-LOCAL-WTHLD ( EM = Z,ZZZ,ZZ9.99- ) 86T #FF3-CAN-WTHLD-AMT ( EM = Z,ZZZ,ZZ9.99- ) 103T #TEMP-PR-WTHLD ( EM = Z,ZZZ,ZZ9.99- ) 119T #FF3-INT-AMT ( EM = Z,ZZZ,ZZ9.99- )
            TabSetting(24),pnd_Work_Area_Pnd_Contract_Payee,new TabSetting(36),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Pymnt_Date(),new TabSetting(45),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Ind(),new 
            TabSetting(50),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(69),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new TabSetting(86),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Fed_Wthld_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new 
            TabSetting(103),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Nra_Wthld_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new TabSetting(119),pnd_Work_Area_Pnd_Temp_State_Wthld, 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(4),pnd_State_Desc,new TabSetting(69),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Local_Wthld(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new TabSetting(86),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new 
            TabSetting(103),pnd_Work_Area_Pnd_Temp_Pr_Wthld, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new TabSetting(119),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Int_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #I 1 2
        for (pnd_Work_Area_Pnd_I.setValue(1); condition(pnd_Work_Area_Pnd_I.lessOrEqual(2)); pnd_Work_Area_Pnd_I.nadd(1))
        {
            pnd_Totals_Pnd_Total_Cnt.getValue(pnd_Work_Area_Pnd_I).nadd(1);                                                                                               //Natural: ADD 1 TO #TOTAL-CNT ( #I )
            pnd_Totals_Pnd_Total_Grs.getValue(pnd_Work_Area_Pnd_I).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt());                                               //Natural: ADD #FF3-GROSS-AMT TO #TOTAL-GRS ( #I )
            pnd_Totals_Pnd_Total_Ivc.getValue(pnd_Work_Area_Pnd_I).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt());                                                 //Natural: ADD #FF3-IVC-AMT TO #TOTAL-IVC ( #I )
            pnd_Totals_Pnd_Total_Int.getValue(pnd_Work_Area_Pnd_I).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Int_Amt());                                                 //Natural: ADD #FF3-INT-AMT TO #TOTAL-INT ( #I )
            pnd_Totals_Pnd_Total_Fed.getValue(pnd_Work_Area_Pnd_I).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Fed_Wthld_Amt());                                           //Natural: ADD #FF3-FED-WTHLD-AMT TO #TOTAL-FED ( #I )
            pnd_Totals_Pnd_Total_Nra.getValue(pnd_Work_Area_Pnd_I).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Nra_Wthld_Amt());                                           //Natural: ADD #FF3-NRA-WTHLD-AMT TO #TOTAL-NRA ( #I )
            pnd_Totals_Pnd_Total_Sta.getValue(pnd_Work_Area_Pnd_I).nadd(pnd_Work_Area_Pnd_Temp_State_Wthld);                                                              //Natural: ADD #TEMP-STATE-WTHLD TO #TOTAL-STA ( #I )
            pnd_Totals_Pnd_Total_Lcl.getValue(pnd_Work_Area_Pnd_I).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Local_Wthld());                                             //Natural: ADD #FF3-LOCAL-WTHLD TO #TOTAL-LCL ( #I )
            pnd_Totals_Pnd_Total_Can.getValue(pnd_Work_Area_Pnd_I).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt());                                           //Natural: ADD #FF3-CAN-WTHLD-AMT TO #TOTAL-CAN ( #I )
            pnd_Totals_Pnd_Total_Pr.getValue(pnd_Work_Area_Pnd_I).nadd(pnd_Work_Area_Pnd_Temp_Pr_Wthld);                                                                  //Natural: ADD #TEMP-PR-WTHLD TO #TOTAL-PR ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Co_Desc() throws Exception                                                                                                                           //Natural: CO-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //*                  -------
        //*  9/20/04   RM
        //*  JB1011
        //*  11/2014 END
        short decideConditionsMet588 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SV-COMPANY;//Natural: VALUE 'C'
        if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("C"))))
        {
            decideConditionsMet588++;
            pnd_Work_Area_Pnd_Print_Com.setValue("CREF");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'CREF'
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("L"))))
        {
            decideConditionsMet588++;
            pnd_Work_Area_Pnd_Print_Com.setValue("LIFE");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'LIFE'
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("T"))))
        {
            decideConditionsMet588++;
            pnd_Work_Area_Pnd_Print_Com.setValue("TIAA");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'TIAA'
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("S"))))
        {
            decideConditionsMet588++;
            pnd_Work_Area_Pnd_Print_Com.setValue("TCII");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'TCII'
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("X"))))
        {
            decideConditionsMet588++;
            pnd_Work_Area_Pnd_Print_Com.setValue("TRST");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'TRST'
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("F"))))
        {
            decideConditionsMet588++;
            pnd_Work_Area_Pnd_Print_Com.setValue("FSB ");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'FSB '
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Work_Area_Pnd_Print_Com.setValue("OTHR");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'OTHR'
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM CO-DESC
                    sub_Co_Desc();
                    if (condition(Global.isEscape())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR / *INIT-USER '-' *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 121T 'PAGE' *PAGE-NUMBER ( 1 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 32T 'DAILY ONLINE MAINTENANCE DETAIL REPORT-BY COMPANY & RES.CODE' / 'RUNTIME : ' *TIMX 42T 'FINANCIAL TRANSACTIONS - TAX YEAR' #TAX-YEAR 97T 'DATE RANGE :' #START-DATE 'THRU' #END-DATE /// / 'COMPANY : ' #PRINT-COM //
                        TabSetting(121),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(32),"DAILY ONLINE MAINTENANCE DETAIL REPORT-BY COMPANY & RES.CODE",NEWLINE,"RUNTIME : ",Global.getTIMX(),new TabSetting(42),"FINANCIAL TRANSACTIONS - TAX YEAR",pnd_Var_File1_Pnd_Tax_Year,new 
                        TabSetting(97),"DATE RANGE :",pnd_Var_File1_Pnd_Start_Date,"THRU",pnd_Var_File1_Pnd_End_Date,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"COMPANY : ",
                        pnd_Work_Area_Pnd_Print_Com,NEWLINE,NEWLINE);
                    //*  11/2014 START
                    if (condition(pnd_Work_Area_Pnd_Fatca.getBoolean()))                                                                                                  //Natural: IF #FATCA
                    {
                        pnd_Work_Area_Pnd_Hdg1.setValue("  FATCA TAX  ");                                                                                                 //Natural: ASSIGN #HDG1 := '  FATCA TAX  '
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(69),"TAX FREE",new TabSetting(84),"FEDERAL TAX",new TabSetting(100),pnd_Work_Area_Pnd_Hdg1,new  //Natural: WRITE ( 1 ) 69T 'TAX FREE' 84T 'FEDERAL TAX' 100T #HDG1 120T 'STATE TAX' / 71T 'IVC' 84T 'WITHHOLDING' 101T 'WITHHOLDING' 118T 'WITHHOLDING' / 67T '-----------      -----------      -----------      -----------' / 9T 'TAX-ID-NO' 25T 'CONTRACT' 36T 'PAYMENT' 45T 'IVC' 54T 'GROSS' 68T 'LOCAL TAX' 86T 'CANADIAN' 101T 'PUERTO RICO' 120T 'INTEREST' / 'RC' 7T 'RES.CODE DESC' 24T 'PAYEE CODE' 38T 'DATE' 45T 'IND' 53T 'AMOUNT' 67T 'WITHHOLDING' 84T 'WITHHOLDING' 101T 'WITHHOLDING' 121T 'AMOUNT' / '--' 4T '-------------------' 24T '-----------' 36T '--------' 45T '---' 49T '---------------' 65T '----------------' 82T '----------------' 99T '---------------- ---------------'
                        TabSetting(120),"STATE TAX",NEWLINE,new TabSetting(71),"IVC",new TabSetting(84),"WITHHOLDING",new TabSetting(101),"WITHHOLDING",new 
                        TabSetting(118),"WITHHOLDING",NEWLINE,new TabSetting(67),"-----------      -----------      -----------      -----------",NEWLINE,new 
                        TabSetting(9),"TAX-ID-NO",new TabSetting(25),"CONTRACT",new TabSetting(36),"PAYMENT",new TabSetting(45),"IVC",new TabSetting(54),"GROSS",new 
                        TabSetting(68),"LOCAL TAX",new TabSetting(86),"CANADIAN",new TabSetting(101),"PUERTO RICO",new TabSetting(120),"INTEREST",NEWLINE,"RC",new 
                        TabSetting(7),"RES.CODE DESC",new TabSetting(24),"PAYEE CODE",new TabSetting(38),"DATE",new TabSetting(45),"IND",new TabSetting(53),"AMOUNT",new 
                        TabSetting(67),"WITHHOLDING",new TabSetting(84),"WITHHOLDING",new TabSetting(101),"WITHHOLDING",new TabSetting(121),"AMOUNT",NEWLINE,"--",new 
                        TabSetting(4),"-------------------",new TabSetting(24),"-----------",new TabSetting(36),"--------",new TabSetting(45),"---",new 
                        TabSetting(49),"---------------",new TabSetting(65),"----------------",new TabSetting(82),"----------------",new TabSetting(99),
                        "---------------- ---------------");
                    //*  11/2014 END
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  11/2014
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 2 ) NOTITLE NOHDR / *INIT-USER '-' *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 121T 'PAGE' *PAGE-NUMBER ( 2 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 32T 'DAILY ONLINE MAINTENANCE DETAIL REPORT-BY COMPANY & RES.CODE' / 'RUNTIME : ' *TIMX 36T 'EXCEPTION REPORT : STATE/COUNTRY CODES NOT IN TABLE' / 42T 'FINANCIAL TRANSACTIONS - TAX YEAR' #TAX-YEAR 97T 'DATE RANGE :' #START-DATE 'THRU' #END-DATE /// 'COMPANY' 4X 'STATE' / 1X 'CODE' 6X 'CODE'
                        TabSetting(121),"PAGE",getReports().getPageNumberDbs(2),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(32),"DAILY ONLINE MAINTENANCE DETAIL REPORT-BY COMPANY & RES.CODE",NEWLINE,"RUNTIME : ",Global.getTIMX(),new TabSetting(36),"EXCEPTION REPORT : STATE/COUNTRY CODES NOT IN TABLE",NEWLINE,new 
                        TabSetting(42),"FINANCIAL TRANSACTIONS - TAX YEAR",pnd_Var_File1_Pnd_Tax_Year,new TabSetting(97),"DATE RANGE :",pnd_Var_File1_Pnd_Start_Date,"THRU",pnd_Var_File1_Pnd_End_Date,NEWLINE,NEWLINE,NEWLINE,"COMPANY",new 
                        ColumnSpacing(4),"STATE",NEWLINE,new ColumnSpacing(1),"CODE",new ColumnSpacing(6),"CODE");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");
        Global.format(2, "PS=60 LS=132");
    }
}
