/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:25:43 PM
**        * FROM NATURAL PROGRAM : Twrp0116
************************************************************
**        * FILE NAME            : Twrp0116.java
**        * CLASS NAME           : Twrp0116
**        * INSTANCE NAME        : Twrp0116
************************************************************
************************************************************************
**
** PROGRAM:  TWRP0116 - FORM SELECTION
** SYSTEM :  TAX WITHHOLDING & REPORTING SYSTEM
** AUTHOR :  A.WILNER
** PURPOSE:  MAGNETIC REPORT TO CANADA
**
** HISTORY.....:
** -----------------------------------------------------------------
** 02/13/2001  FELIX ORTIZ
**               TAX YEAR IS NOW AN OPTIONAL INPUT PARAMETER. SYSTEM
**               CALCULATES TAX YEAR WHEN PARAMETER IS NOT PASSED.
**               THIS DATE IS USED TO POPULATE THE OUTPUT FILE.
**               ADD CODE TO MARK RECS AS HELD DUE TO IRS REJECT.
** 02/08/2002  JH - ADD DETAIL REPORTS AND TOTALS
** 08/13/2002  JR - RECOMPILED DUE TO INCREASE IN #COMP-NAME LENGTH
**                 IN TWRLCOMP & TWRACOMP
** 09/05/2002 -MN- CHANGED TO MOVE NR4-ID FIELD TO THE RECORD INSTEAD
**                  OF HARDCODED VALUES; TWRNCOM2 WILL BE CALLED
**                  INSTEAD OF TWRNCOMP TO EXTRACT 30-BYTE COMPANY
**                  NAME FOR 'Services'
** 11/19/2002 -MN- RECOMPILED DUE TO REFERENCE TO ADDRESS FIELD
** 01/30/2003 -RM- CHANGED TO GET NR4-ID FIELD FROM 'S' COMPANY ONLY.
**                 THE COMPANY NAME, PAYER NAME AND AGENT NAME WERE ALL
**                 UPDATED BY TWRNCOM2 PER USER SPECIFICATION.
** 10/28/2003 -KH- CHANGED TO PRODUCE ONE REPORT AND ONE FILE FOR -
**                 SERVICES - TCII
** 02/10/2005 -AY- REVISED TO REPLACE SERVICES (TCII) WITH TIAA.
**               - REVISED TO REJECT FORM IF FORM.TIRF-COMPANY-CDE NE
**                 'T' (TIAA), AS ONLY TIAA PAYMENTS S/B ACCEPTED.
**               - REVISED TO POPULATE #TWRACOM2.#TAX-YEAR BEFORE
**                 CALLING COMPANY CODE TABLE.
**               - REVISED TO PRINT FORM.TIRF-COMPANY-CDE ON REPORTS.
** 12/29/05   -RM- #TWRL116A-PAYER-ID FIELD WAS MIXED UP WITH
**                 #TWRL116A-NONE-RESIDENT-ACCT FIELD. REVERSE THAT IN
**                 ORDER TO PRODUCE A CORRECT REPORT TO SEND OUT TO
**                 CANADA REVENUE.
**                 ALSO CHANGE THE FIELD IN TWRL116B AND FIX THE TOTAL
**                 FIELDS PRINTING NOT LINE-UP PROBLEM.
** 02/07/07   -RM- FOR 2006, CANADA REVENUE REQUIRES XML FILE FOR EFILE.
**                 UPDATE TWRL116A, TWRL116B AND TWRL116C FOR 2006 SPEC.
**                 ADD TWRATBL3 AND FIX COUNTRY CODE ERROR IN SUBROUTINE
**                 WRITE-NR4-REC
** 03/07/07   -RM- ADD A 'Record Type' FIELD AT THE END OF EACH RECORD
**                 TO DIFFERENTIATE RECORD TYPES, WHICH IS NECESSARY TO
**                 ENABLE THE XML FILE SCHEMA.
** 03/04/08   -RM- VERIFY 2007 SPECS WITH USER, CORRECTED A FEW ERRORS
**                 USER MADE. UPDATE THIS PROGRAM WITH ANY NECESSARY
**                 CHANGES.
** 02/19/16   -MUKHERR
**                 TRANSMITTER RECORD - FIELD #TWRL116C-TRANS-ADDRESS-2
**                 WILL HAVE HARD-CODED VALUE E0-S4.
**                 THE CONTACT-PHONE HAS BEEN UPDATED IN TWRLCOMP.
**                 CHANGES TAGGED WITH MUKHERR
**
* 4/14/2017 - WEBBJ - PIN EXPANSION. RESTOW ONLY
** 02/22/18 - MUKH2
**                 REPORT REJECTED ITEMS IN IRS FILE
** 07/23/18 - SAIK
**                 BACK OUT THE CHANGES MADE ON 02/22
**10/13/2020 - RE-STOW COMPONENT FOR 5498           /* SECURE-ACT
** 12/04/2020 - RE-STOW COMPONENT FOR 5498 IRS REPORTING  2020
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0116 extends BLNatBase
{
    // Data Areas
    private LdaTwrl116a ldaTwrl116a;
    private LdaTwrl116b ldaTwrl116b;
    private LdaTwrl116c ldaTwrl116c;
    private LdaTwrl9710 ldaTwrl9710;
    private PdaTwracom2 pdaTwracom2;
    private PdaTwratbl2 pdaTwratbl2;
    private PdaTwratbl3 pdaTwratbl3;
    private PdaTwratbl4 pdaTwratbl4;
    private LdaTwrltb4r ldaTwrltb4r;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_form1;
    private DbsField form1_Tirf_Irs_Rpt_Ind;
    private DbsField form1_Tirf_Irs_Rpt_Date;
    private DbsField form1_Tirf_Lu_User;
    private DbsField form1_Tirf_Lu_Ts;
    private DbsField pnd_In_Parm;

    private DbsGroup pnd_In_Parm__R_Field_1;
    private DbsField pnd_In_Parm_Pnd_In_Active_Ind;
    private DbsField pnd_In_Parm_Pnd_In_Form_Type;
    private DbsField pnd_In_Parm_Pnd_In_Inc_Old;
    private DbsField pnd_In_Parm_Pnd_In_Update;
    private DbsField pnd_In_Parm_Pnd_In_Tax_Year;

    private DbsGroup pnd_In_Parm__R_Field_2;
    private DbsField pnd_In_Parm_Pnd_In_Tax_Yearn;
    private DbsField pnd_Tirf_Superde_3;

    private DbsGroup pnd_Tirf_Superde_3__R_Field_3;
    private DbsField pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year;

    private DbsGroup pnd_Tirf_Superde_3__R_Field_4;
    private DbsField pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn;
    private DbsField pnd_Tirf_Superde_3_Pnd_Tirf_Active_Ind;
    private DbsField pnd_Tirf_Superde_3_Pnd_Tirf_Form_Type;

    private DbsGroup pnd_Work_Area;
    private DbsField pnd_Work_Area_Pnd_Ws_Trailer;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone;

    private DbsGroup pnd_Work_Area__R_Field_5;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone_F1;
    private DbsField pnd_Work_Area_Pnd_Ws_Area_Code;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone_F2;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone_Num3;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone_F3;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone_Num4;

    private DbsGroup pnd_Work_Area__R_Field_6;
    private DbsField pnd_Work_Area_Pnd_Ws_Area_Code_3;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone_7;
    private DbsField pnd_Work_Area_Pnd_Ws_Extension_5;
    private DbsField pnd_Work_Area_Pnd_Ws_Company_Counter;
    private DbsField pnd_Work_Area_Pnd_Ws_Tot_Nr4_Count;
    private DbsField pnd_Work_Area_Pnd_Ws_Old_Company;
    private DbsField pnd_Work_Area_Pnd_Ws_Fed_Id;
    private DbsField pnd_Work_Area_Pnd_Ws_Nr4_Id;
    private DbsField pnd_Work_Area_Pnd_Ws_Tot_Gross_Income;
    private DbsField pnd_Work_Area_Pnd_Ws_Tot_Non_Res_Tax_Held;
    private DbsField pnd_Work_Area_Pnd_Ws_Tot_Gross_Income_Box_26;
    private DbsField pnd_Work_Area_Pnd_Ws_Tot_Tax_Held_Box_27;
    private DbsField pnd_Work_Area_Pnd_Ws_Read_Counter;
    private DbsField pnd_Work_Area_Pnd_Ws_Accept_Counter;
    private DbsField pnd_Work_Area_Pnd_Ws_Reject_Counter;
    private DbsField pnd_Work_Area_Pnd_Ws_Et;
    private DbsField pnd_Work_Area_Pnd_Ws_Isn;
    private DbsField pnd_Work_Area_Pnd_Empty_Form_Cnt;
    private DbsField pnd_Work_Area_Pnd_Reject;

    private DbsGroup pnd_Work_Area_Pnd_Ws_Totals;
    private DbsField pnd_Work_Area_Pnd_Ws_Cnt;
    private DbsField pnd_Work_Area_Pnd_Ws_Gross;
    private DbsField pnd_Work_Area_Pnd_Ws_Int;
    private DbsField pnd_Work_Area_Pnd_Ws_Wthld;
    private DbsField pnd_Work_Area_Pnd_Ws_Cnt_Tot;
    private DbsField pnd_Work_Area_Pnd_Ws_Gross_Tot;
    private DbsField pnd_Work_Area_Pnd_Ws_Int_Tot;
    private DbsField pnd_Work_Area_Pnd_Ws_Wthld_Tot;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl116a = new LdaTwrl116a();
        registerRecord(ldaTwrl116a);
        ldaTwrl116b = new LdaTwrl116b();
        registerRecord(ldaTwrl116b);
        ldaTwrl116c = new LdaTwrl116c();
        registerRecord(ldaTwrl116c);
        ldaTwrl9710 = new LdaTwrl9710();
        registerRecord(ldaTwrl9710);
        registerRecord(ldaTwrl9710.getVw_form());
        localVariables = new DbsRecord();
        pdaTwracom2 = new PdaTwracom2(localVariables);
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        pdaTwratbl3 = new PdaTwratbl3(localVariables);
        pdaTwratbl4 = new PdaTwratbl4(localVariables);
        ldaTwrltb4r = new LdaTwrltb4r();
        registerRecord(ldaTwrltb4r);
        registerRecord(ldaTwrltb4r.getVw_tircntl_Rpt());

        // Local Variables

        vw_form1 = new DataAccessProgramView(new NameInfo("vw_form1", "FORM1"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form1_Tirf_Irs_Rpt_Ind = vw_form1.getRecord().newFieldInGroup("form1_Tirf_Irs_Rpt_Ind", "TIRF-IRS-RPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_IND");
        form1_Tirf_Irs_Rpt_Ind.setDdmHeader("IRS/RPT/IND");
        form1_Tirf_Irs_Rpt_Date = vw_form1.getRecord().newFieldInGroup("form1_Tirf_Irs_Rpt_Date", "TIRF-IRS-RPT-DATE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_DATE");
        form1_Tirf_Irs_Rpt_Date.setDdmHeader("IRS/RPT/DATE");
        form1_Tirf_Lu_User = vw_form1.getRecord().newFieldInGroup("form1_Tirf_Lu_User", "TIRF-LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_LU_USER");
        form1_Tirf_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        form1_Tirf_Lu_Ts = vw_form1.getRecord().newFieldInGroup("form1_Tirf_Lu_Ts", "TIRF-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "TIRF_LU_TS");
        form1_Tirf_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        registerRecord(vw_form1);

        pnd_In_Parm = localVariables.newFieldInRecord("pnd_In_Parm", "#IN-PARM", FieldType.STRING, 79);

        pnd_In_Parm__R_Field_1 = localVariables.newGroupInRecord("pnd_In_Parm__R_Field_1", "REDEFINE", pnd_In_Parm);
        pnd_In_Parm_Pnd_In_Active_Ind = pnd_In_Parm__R_Field_1.newFieldInGroup("pnd_In_Parm_Pnd_In_Active_Ind", "#IN-ACTIVE-IND", FieldType.STRING, 1);
        pnd_In_Parm_Pnd_In_Form_Type = pnd_In_Parm__R_Field_1.newFieldInGroup("pnd_In_Parm_Pnd_In_Form_Type", "#IN-FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_In_Parm_Pnd_In_Inc_Old = pnd_In_Parm__R_Field_1.newFieldInGroup("pnd_In_Parm_Pnd_In_Inc_Old", "#IN-INC-OLD", FieldType.STRING, 1);
        pnd_In_Parm_Pnd_In_Update = pnd_In_Parm__R_Field_1.newFieldInGroup("pnd_In_Parm_Pnd_In_Update", "#IN-UPDATE", FieldType.STRING, 1);
        pnd_In_Parm_Pnd_In_Tax_Year = pnd_In_Parm__R_Field_1.newFieldInGroup("pnd_In_Parm_Pnd_In_Tax_Year", "#IN-TAX-YEAR", FieldType.STRING, 4);

        pnd_In_Parm__R_Field_2 = pnd_In_Parm__R_Field_1.newGroupInGroup("pnd_In_Parm__R_Field_2", "REDEFINE", pnd_In_Parm_Pnd_In_Tax_Year);
        pnd_In_Parm_Pnd_In_Tax_Yearn = pnd_In_Parm__R_Field_2.newFieldInGroup("pnd_In_Parm_Pnd_In_Tax_Yearn", "#IN-TAX-YEARN", FieldType.NUMERIC, 4);
        pnd_Tirf_Superde_3 = localVariables.newFieldInRecord("pnd_Tirf_Superde_3", "#TIRF-SUPERDE-3", FieldType.STRING, 33);

        pnd_Tirf_Superde_3__R_Field_3 = localVariables.newGroupInRecord("pnd_Tirf_Superde_3__R_Field_3", "REDEFINE", pnd_Tirf_Superde_3);
        pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year = pnd_Tirf_Superde_3__R_Field_3.newFieldInGroup("pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year", "#TIRF-TAX-YEAR", 
            FieldType.STRING, 4);

        pnd_Tirf_Superde_3__R_Field_4 = pnd_Tirf_Superde_3__R_Field_3.newGroupInGroup("pnd_Tirf_Superde_3__R_Field_4", "REDEFINE", pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year);
        pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn = pnd_Tirf_Superde_3__R_Field_4.newFieldInGroup("pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn", "#TIRF-TAX-YEARN", 
            FieldType.NUMERIC, 4);
        pnd_Tirf_Superde_3_Pnd_Tirf_Active_Ind = pnd_Tirf_Superde_3__R_Field_3.newFieldInGroup("pnd_Tirf_Superde_3_Pnd_Tirf_Active_Ind", "#TIRF-ACTIVE-IND", 
            FieldType.STRING, 1);
        pnd_Tirf_Superde_3_Pnd_Tirf_Form_Type = pnd_Tirf_Superde_3__R_Field_3.newFieldInGroup("pnd_Tirf_Superde_3_Pnd_Tirf_Form_Type", "#TIRF-FORM-TYPE", 
            FieldType.NUMERIC, 2);

        pnd_Work_Area = localVariables.newGroupInRecord("pnd_Work_Area", "#WORK-AREA");
        pnd_Work_Area_Pnd_Ws_Trailer = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Trailer", "#WS-TRAILER", FieldType.STRING, 25);
        pnd_Work_Area_Pnd_Ws_Phone = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone", "#WS-PHONE", FieldType.STRING, 15);

        pnd_Work_Area__R_Field_5 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area__R_Field_5", "REDEFINE", pnd_Work_Area_Pnd_Ws_Phone);
        pnd_Work_Area_Pnd_Ws_Phone_F1 = pnd_Work_Area__R_Field_5.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone_F1", "#WS-PHONE-F1", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Ws_Area_Code = pnd_Work_Area__R_Field_5.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Area_Code", "#WS-AREA-CODE", FieldType.NUMERIC, 
            3);
        pnd_Work_Area_Pnd_Ws_Phone_F2 = pnd_Work_Area__R_Field_5.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone_F2", "#WS-PHONE-F2", FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Ws_Phone_Num3 = pnd_Work_Area__R_Field_5.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone_Num3", "#WS-PHONE-NUM3", FieldType.STRING, 
            3);
        pnd_Work_Area_Pnd_Ws_Phone_F3 = pnd_Work_Area__R_Field_5.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone_F3", "#WS-PHONE-F3", FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Ws_Phone_Num4 = pnd_Work_Area__R_Field_5.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone_Num4", "#WS-PHONE-NUM4", FieldType.STRING, 
            4);

        pnd_Work_Area__R_Field_6 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area__R_Field_6", "REDEFINE", pnd_Work_Area_Pnd_Ws_Phone);
        pnd_Work_Area_Pnd_Ws_Area_Code_3 = pnd_Work_Area__R_Field_6.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Area_Code_3", "#WS-AREA-CODE-3", FieldType.NUMERIC, 
            3);
        pnd_Work_Area_Pnd_Ws_Phone_7 = pnd_Work_Area__R_Field_6.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone_7", "#WS-PHONE-7", FieldType.NUMERIC, 7);
        pnd_Work_Area_Pnd_Ws_Extension_5 = pnd_Work_Area__R_Field_6.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Extension_5", "#WS-EXTENSION-5", FieldType.NUMERIC, 
            5);
        pnd_Work_Area_Pnd_Ws_Company_Counter = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Company_Counter", "#WS-COMPANY-COUNTER", FieldType.NUMERIC, 
            2);
        pnd_Work_Area_Pnd_Ws_Tot_Nr4_Count = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Tot_Nr4_Count", "#WS-TOT-NR4-COUNT", FieldType.NUMERIC, 
            7);
        pnd_Work_Area_Pnd_Ws_Old_Company = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Old_Company", "#WS-OLD-COMPANY", FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Ws_Fed_Id = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Fed_Id", "#WS-FED-ID", FieldType.STRING, 9);
        pnd_Work_Area_Pnd_Ws_Nr4_Id = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Nr4_Id", "#WS-NR4-ID", FieldType.STRING, 9);
        pnd_Work_Area_Pnd_Ws_Tot_Gross_Income = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Tot_Gross_Income", "#WS-TOT-GROSS-INCOME", FieldType.NUMERIC, 
            11, 2);
        pnd_Work_Area_Pnd_Ws_Tot_Non_Res_Tax_Held = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Tot_Non_Res_Tax_Held", "#WS-TOT-NON-RES-TAX-HELD", 
            FieldType.NUMERIC, 11, 2);
        pnd_Work_Area_Pnd_Ws_Tot_Gross_Income_Box_26 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Tot_Gross_Income_Box_26", "#WS-TOT-GROSS-INCOME-BOX-26", 
            FieldType.NUMERIC, 11, 2);
        pnd_Work_Area_Pnd_Ws_Tot_Tax_Held_Box_27 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Tot_Tax_Held_Box_27", "#WS-TOT-TAX-HELD-BOX-27", 
            FieldType.NUMERIC, 11, 2);
        pnd_Work_Area_Pnd_Ws_Read_Counter = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Read_Counter", "#WS-READ-COUNTER", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Work_Area_Pnd_Ws_Accept_Counter = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Accept_Counter", "#WS-ACCEPT-COUNTER", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Work_Area_Pnd_Ws_Reject_Counter = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Reject_Counter", "#WS-REJECT-COUNTER", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Work_Area_Pnd_Ws_Et = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Et", "#WS-ET", FieldType.PACKED_DECIMAL, 5);
        pnd_Work_Area_Pnd_Ws_Isn = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Isn", "#WS-ISN", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Empty_Form_Cnt = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Empty_Form_Cnt", "#EMPTY-FORM-CNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Work_Area_Pnd_Reject = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Reject", "#REJECT", FieldType.BOOLEAN, 1);

        pnd_Work_Area_Pnd_Ws_Totals = pnd_Work_Area.newGroupArrayInGroup("pnd_Work_Area_Pnd_Ws_Totals", "#WS-TOTALS", new DbsArrayController(1, 3));
        pnd_Work_Area_Pnd_Ws_Cnt = pnd_Work_Area_Pnd_Ws_Totals.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Cnt", "#WS-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Work_Area_Pnd_Ws_Gross = pnd_Work_Area_Pnd_Ws_Totals.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Gross", "#WS-GROSS", FieldType.PACKED_DECIMAL, 17, 
            2);
        pnd_Work_Area_Pnd_Ws_Int = pnd_Work_Area_Pnd_Ws_Totals.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Int", "#WS-INT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Work_Area_Pnd_Ws_Wthld = pnd_Work_Area_Pnd_Ws_Totals.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Wthld", "#WS-WTHLD", FieldType.PACKED_DECIMAL, 15, 
            2);
        pnd_Work_Area_Pnd_Ws_Cnt_Tot = pnd_Work_Area_Pnd_Ws_Totals.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Cnt_Tot", "#WS-CNT-TOT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Work_Area_Pnd_Ws_Gross_Tot = pnd_Work_Area_Pnd_Ws_Totals.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Gross_Tot", "#WS-GROSS-TOT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Work_Area_Pnd_Ws_Int_Tot = pnd_Work_Area_Pnd_Ws_Totals.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Int_Tot", "#WS-INT-TOT", FieldType.PACKED_DECIMAL, 
            15, 2);
        pnd_Work_Area_Pnd_Ws_Wthld_Tot = pnd_Work_Area_Pnd_Ws_Totals.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Wthld_Tot", "#WS-WTHLD-TOT", FieldType.PACKED_DECIMAL, 
            15, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_form1.reset();

        ldaTwrl116a.initializeValues();
        ldaTwrl116b.initializeValues();
        ldaTwrl116c.initializeValues();
        ldaTwrl9710.initializeValues();
        ldaTwrltb4r.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0116() throws Exception
    {
        super("Twrp0116");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp0116|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*  -----------------------------------------
                //*  FO 02/01
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                //*  TOTALS
                //*  ACCEPTED
                //*  REJECTED
                //*                                                                                                                                                       //Natural: FORMAT LS = 133 PS = 56 ES = ON;//Natural: FORMAT ( 1 ) PS = 60 LS = 133 ES = ON;//Natural: FORMAT ( 2 ) PS = 60 LS = 133 ES = ON;//Natural: FORMAT ( 3 ) PS = 60 LS = 133 ES = ON
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_In_Parm);                                                                                          //Natural: INPUT #IN-PARM
                //*  TAX YEAR IS ALWAYS CALENDAR
                if (condition(pnd_In_Parm_Pnd_In_Tax_Year.equals(" ")))                                                                                                   //Natural: IF #IN-TAX-YEAR = ' '
                {
                    pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year.compute(new ComputeParameters(false, pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year), Global.getDATN().divide(10000).subtract(1)); //Natural: ASSIGN #TIRF-TAX-YEAR := *DATN / 10000 - 1
                    //*  YEAR MINUS 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year.setValue(pnd_In_Parm_Pnd_In_Tax_Year);                                                                           //Natural: ASSIGN #TIRF-TAX-YEAR := #IN-TAX-YEAR
                }                                                                                                                                                         //Natural: END-IF
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 1 ) TITLE LEFT *TIMX ( EM = MM/DD/YYYY�HH:IIAP ) 28T 'TaxWaRS - Annual Original Tax Report - Totals  ' 92T 'Page:' *PAGE-NUMBER ( 1 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 36T #TIRF-TAX-YEAR '- Canada - Form NR4' // 26T '  Accepted        Rejected                  Total' / 26T '  --------        --------                  -----'
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 2 ) TITLE LEFT *TIMX ( EM = MM/DD/YYYY�HH:IIAP ) 28T 'TaxWaRS - Annual Original Tax Report - Accepted' 92T 'Page:' *PAGE-NUMBER ( 2 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 32T #TIRF-TAX-YEAR '-' #COMP-SHORT-NAME '- Canada - Form NR4'
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 3 ) TITLE LEFT *TIMX ( EM = MM/DD/YYYY�HH:IIAP ) 28T 'TaxWaRS - Annual Original Tax Report - Rejected' 92T 'Page:' *PAGE-NUMBER ( 3 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 32T #TIRF-TAX-YEAR '-' #COMP-SHORT-NAME '- Canada - Form NR4'
                //*  -----------------------------------------------
                pnd_Tirf_Superde_3_Pnd_Tirf_Active_Ind.setValue(pnd_In_Parm_Pnd_In_Active_Ind);                                                                           //Natural: ASSIGN #TIRF-ACTIVE-IND := #IN-ACTIVE-IND
                pnd_Tirf_Superde_3_Pnd_Tirf_Form_Type.setValue(pnd_In_Parm_Pnd_In_Form_Type);                                                                             //Natural: ASSIGN #TIRF-FORM-TYPE := #IN-FORM-TYPE
                //*  02-10-2005
                //*  02-10-2005
                                                                                                                                                                          //Natural: PERFORM CHECK-PREV-RUN
                sub_Check_Prev_Run();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  ------------------------------------
                //*  SAVE THE 'T' COMPANY NR4-ID FOR LATER USE
                //*                       FED-ID
                pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue("T");                                                                                                //Natural: ASSIGN #TWRACOM2.#COMP-CODE := 'T'
                pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().setValue(pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn);                                                               //Natural: ASSIGN #TWRACOM2.#TAX-YEAR := #TIRF-TAX-YEARN
                pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(7);                                                                                                  //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 7
                DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNCOM2' USING #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
                    pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                pnd_Work_Area_Pnd_Ws_Fed_Id.setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id());                                                                           //Natural: ASSIGN #WS-FED-ID := #FED-ID
                pnd_Work_Area_Pnd_Ws_Nr4_Id.setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Nr4_Id());                                                                           //Natural: ASSIGN #WS-NR4-ID := #NR4-ID
                //*  ------------------------------------
                ldaTwrl9710.getVw_form().startDatabaseRead                                                                                                                //Natural: READ FORM WITH TIRF-SUPERDE-3 = #TIRF-SUPERDE-3
                (
                "READ01",
                new Wc[] { new Wc("TIRF_SUPERDE_3", ">=", pnd_Tirf_Superde_3, WcType.BY) },
                new Oc[] { new Oc("TIRF_SUPERDE_3", "ASC") }
                );
                READ01:
                while (condition(ldaTwrl9710.getVw_form().readNextRow("READ01")))
                {
                    pnd_Work_Area_Pnd_Ws_Read_Counter.nadd(1);                                                                                                            //Natural: ADD 1 TO #WS-READ-COUNTER
                    if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().notEquals(pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year) || ldaTwrl9710.getForm_Tirf_Active_Ind().notEquals(pnd_Tirf_Superde_3_Pnd_Tirf_Active_Ind)  //Natural: IF TIRF-TAX-YEAR NE #TIRF-TAX-YEAR OR TIRF-ACTIVE-IND NE #TIRF-ACTIVE-IND OR TIRF-FORM-TYPE NE #TIRF-FORM-TYPE
                        || ldaTwrl9710.getForm_Tirf_Form_Type().notEquals(pnd_Tirf_Superde_3_Pnd_Tirf_Form_Type)))
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl9710.getForm_Tirf_Empty_Form().getBoolean()))                                                                                    //Natural: IF TIRF-EMPTY-FORM
                    {
                        //*  FO 2/01
                        pnd_Work_Area_Pnd_Empty_Form_Cnt.nadd(1);                                                                                                         //Natural: ADD 1 TO #EMPTY-FORM-CNT
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*  02-10-2005
                    if (condition((pnd_In_Parm_Pnd_In_Inc_Old.equals("N") && ldaTwrl9710.getForm_Tirf_Irs_Rpt_Ind().notEquals(" ")) || (pnd_In_Parm_Pnd_In_Inc_Old.equals("O")  //Natural: IF ( #IN-INC-OLD = 'N' AND FORM.TIRF-IRS-RPT-IND NE ' ' ) OR ( #IN-INC-OLD = 'O' AND FORM.TIRF-IRS-RPT-IND EQ ' ' ) OR ( FORM.TIRF-COMPANY-CDE NE 'T' )
                        && ldaTwrl9710.getForm_Tirf_Irs_Rpt_Ind().equals(" ")) || (ldaTwrl9710.getForm_Tirf_Company_Cde().notEquals("T"))))
                    {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-REJECTED-FORM
                        sub_Display_Rejected_Form();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Work_Area_Pnd_Ws_Isn.setValue(ldaTwrl9710.getVw_form().getAstISN("Read01"));                                                                      //Natural: MOVE *ISN TO #WS-ISN
                    pnd_Work_Area_Pnd_Reject.reset();                                                                                                                     //Natural: RESET #REJECT
                    if (condition(pnd_In_Parm_Pnd_In_Update.equals("Y")))                                                                                                 //Natural: IF #IN-UPDATE = 'Y'
                    {
                        GET21:                                                                                                                                            //Natural: GET FORM1 #WS-ISN
                        vw_form1.readByID(pnd_Work_Area_Pnd_Ws_Isn.getLong(), "GET21");
                        //*  FO 02/01
                        //*  ORIGINAL
                        if (condition(ldaTwrl9710.getForm_Tirf_Irs_Reject_Ind().equals(" ")))                                                                             //Natural: IF FORM.TIRF-IRS-REJECT-IND = ' '
                        {
                            form1_Tirf_Irs_Rpt_Ind.setValue("O");                                                                                                         //Natural: ASSIGN FORM1.TIRF-IRS-RPT-IND := 'O'
                            //*  HELD
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            form1_Tirf_Irs_Rpt_Ind.setValue("H");                                                                                                         //Natural: ASSIGN FORM1.TIRF-IRS-RPT-IND := 'H'
                                                                                                                                                                          //Natural: PERFORM DISPLAY-REJECTED-FORM
                            sub_Display_Rejected_Form();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            pnd_Work_Area_Pnd_Reject.setValue(true);                                                                                                      //Natural: ASSIGN #REJECT := TRUE
                            //*  MUKH2
                                                                                                                                                                          //Natural: PERFORM WRITE-NR4-REC
                            sub_Write_Nr4_Rec();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            //*  #WS-UPDATE-DTE
                        }                                                                                                                                                 //Natural: END-IF
                        form1_Tirf_Lu_User.setValue(Global.getINIT_USER());                                                                                               //Natural: ASSIGN FORM1.TIRF-LU-USER := *INIT-USER
                        form1_Tirf_Lu_Ts.setValue(Global.getDATX());                                                                                                      //Natural: ASSIGN FORM1.TIRF-LU-TS := *DATX
                        form1_Tirf_Irs_Rpt_Date.setValue(Global.getDATX());                                                                                               //Natural: ASSIGN FORM1.TIRF-IRS-RPT-DATE := *DATX
                        vw_form1.updateDBRow("GET21");                                                                                                                    //Natural: UPDATE ( GET21. )
                        pnd_Work_Area_Pnd_Ws_Et.nadd(1);                                                                                                                  //Natural: ADD 1 TO #WS-ET
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Work_Area_Pnd_Ws_Et.greater(250)))                                                                                                  //Natural: IF #WS-ET GT 250
                    {
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                        pnd_Work_Area_Pnd_Ws_Et.reset();                                                                                                                  //Natural: RESET #WS-ET
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Work_Area_Pnd_Reject.getBoolean()))                                                                                                 //Natural: IF #REJECT
                    {
                        //*  PROCESS NEXT RECORD
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*  -------------------------------------------------
                    pnd_Work_Area_Pnd_Ws_Accept_Counter.nadd(1);                                                                                                          //Natural: ADD 1 TO #WS-ACCEPT-COUNTER
                    if (condition(ldaTwrl9710.getForm_Tirf_Company_Cde().notEquals(pnd_Work_Area_Pnd_Ws_Old_Company) && pnd_Work_Area_Pnd_Ws_Old_Company.notEquals(" "))) //Natural: IF TIRF-COMPANY-CDE NE #WS-OLD-COMPANY AND #WS-OLD-COMPANY NE ' '
                    {
                        //*    PERFORM COMPANY-BREAK                  /* COMMENTED OUT 10/03 KH
                        getReports().newPage(new ReportSpecification(2));                                                                                                 //Natural: NEWPAGE ( 2 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().newPage(new ReportSpecification(3));                                                                                                 //Natural: NEWPAGE ( 3 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*                                                                                                                                                   //Natural: AT END OF DATA
                                                                                                                                                                          //Natural: PERFORM WRITE-NR4-REC
                    sub_Write_Nr4_Rec();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM DISPLAY-ACCEPTED-FORM
                    sub_Display_Accepted_Form();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    pnd_Work_Area_Pnd_Ws_Old_Company.setValue(ldaTwrl9710.getForm_Tirf_Company_Cde());                                                                    //Natural: MOVE TIRF-COMPANY-CDE TO #WS-OLD-COMPANY
                }                                                                                                                                                         //Natural: END-READ
                if (condition(ldaTwrl9710.getVw_form().getAtEndOfData()))
                {
                    //*  02-10-2005
                                                                                                                                                                          //Natural: PERFORM COMPANY-BREAK
                    sub_Company_Break();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    //*  02-10-2005
                    getReports().newPage(new ReportSpecification(2));                                                                                                     //Natural: NEWPAGE ( 2 )
                    if (condition(Global.isEscape())){return;}
                    getReports().newPage(new ReportSpecification(3));                                                                                                     //Natural: NEWPAGE ( 3 )
                    if (condition(Global.isEscape())){return;}
                    getReports().write(2, NEWLINE,"  Total Accepted ",pnd_Work_Area_Pnd_Ws_Cnt.getValue(1),new TabSetting(36),pnd_Work_Area_Pnd_Ws_Gross.getValue(1),     //Natural: WRITE ( 2 ) / '  Total Accepted ' #WS-CNT ( 1 ) 36T #WS-GROSS ( 1 ) ( EM = ZZ,ZZZ,ZZ9.99 ) #WS-INT ( 1 ) ( EM = ZZZ,ZZ9.99 ) #WS-WTHLD ( 1 ) ( EM = ZZZZZZ,ZZ9.99 )
                        new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Int.getValue(1), new ReportEditMask ("ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Wthld.getValue(1), 
                        new ReportEditMask ("ZZZZZZ,ZZ9.99"));
                    if (condition(Global.isEscape())) return;
                    getReports().write(3, NEWLINE,"  Total Rejected ",pnd_Work_Area_Pnd_Ws_Cnt.getValue(2),new TabSetting(36),pnd_Work_Area_Pnd_Ws_Gross.getValue(2),     //Natural: WRITE ( 3 ) / '  Total Rejected ' #WS-CNT ( 2 ) 36T #WS-GROSS ( 2 ) ( EM = ZZ,ZZZ,ZZ9.99 ) #WS-INT ( 2 ) ( EM = ZZZ,ZZ9.99 ) #WS-WTHLD ( 2 ) ( EM = ZZZZZZ,ZZ9.99 )
                        new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Int.getValue(2), new ReportEditMask ("ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Wthld.getValue(2), 
                        new ReportEditMask ("ZZZZZZ,ZZ9.99"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-ENDDATA
                if (Global.isEscape()) return;
                //*  ------------------------------------
                //*  PERFORM COMPANY-BREAK                             /* KH
                getReports().write(1, "Total Gross         ",pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(2),  //Natural: WRITE ( 1 ) 'Total Gross         ' #WS-GROSS-TOT ( 1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 2X #WS-GROSS-TOT ( 2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) #WS-GROSS-TOT ( 3 ) ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(3), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(1, "Total Interest     ",pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(1), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(2),  //Natural: WRITE ( 1 ) 'Total Interest     ' #WS-INT-TOT ( 1 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) 1X #WS-INT-TOT ( 2 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) 3X #WS-INT-TOT ( 3 ) ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(3), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(1, "Total Tax Withheld ",pnd_Work_Area_Pnd_Ws_Wthld_Tot.getValue(1), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Work_Area_Pnd_Ws_Wthld_Tot.getValue(2),  //Natural: WRITE ( 1 ) 'Total Tax Withheld ' #WS-WTHLD-TOT ( 1 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) 1X #WS-WTHLD-TOT ( 2 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) 3X #WS-WTHLD-TOT ( 3 ) ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Work_Area_Pnd_Ws_Wthld_Tot.getValue(3), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(1, "Total Forms       ",new TabSetting(25),pnd_Work_Area_Pnd_Ws_Cnt_Tot.getValue(1),new TabSetting(41),pnd_Work_Area_Pnd_Ws_Cnt_Tot.getValue(2),new  //Natural: WRITE ( 1 ) 'Total Forms       ' 25T #WS-CNT-TOT ( 1 ) 41T #WS-CNT-TOT ( 2 ) 64T #WS-CNT-TOT ( 3 )
                    TabSetting(64),pnd_Work_Area_Pnd_Ws_Cnt_Tot.getValue(3));
                if (Global.isEscape()) return;
                getReports().write(1, "* ---------------------------------------------------------");                                                                     //Natural: WRITE ( 1 ) '* ---------------------------------------------------------'
                if (Global.isEscape()) return;
                //*  -----------------------------------------
                if (condition(pnd_In_Parm_Pnd_In_Update.equals("Y")))                                                                                                     //Natural: IF #IN-UPDATE = 'Y'
                {
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn);                                                           //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #TIRF-TAX-YEARN
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(7);                                                                                               //Natural: ASSIGN #TWRATBL4.#FORM-IND := 7
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(false);                                                                                          //Natural: ASSIGN #TWRATBL4.#ABEND-IND := FALSE
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Rec_Type().setValue(" ");                                                                                             //Natural: ASSIGN #TWRATBL4.#REC-TYPE := ' '
                    DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4());                                                            //Natural: CALLNAT 'TWRNTB4R' #TWRATBL4
                    if (condition(Global.isEscape())) return;
                    if (condition(! (pdaTwratbl4.getPnd_Twratbl4_Pnd_Ret_Code().getBoolean())))                                                                           //Natural: IF NOT #TWRATBL4.#RET-CODE
                    {
                        DbsUtil.terminate(112);  if (true) return;                                                                                                        //Natural: TERMINATE 112
                        //*  #WS-UPDATE-DTE
                    }                                                                                                                                                     //Natural: END-IF
                    pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(1).setValue(Global.getDATX());                                                                 //Natural: ASSIGN #TWRATBL4.TIRCNTL-RPT-DTE ( 1 ) := *DATX
                    DbsUtil.callnat(Twrntb4u.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4());                                                            //Natural: CALLNAT 'TWRNTB4U' #TWRATBL4
                    if (condition(Global.isEscape())) return;
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                }                                                                                                                                                         //Natural: END-IF
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                //*  ------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-ACCEPTED-FORM
                //*  ------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-REJECTED-FORM
                //*  ------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-NR4-REC
                //*                                       NEW LOGIC ENDS HERE  02/07/07 RM
                //*  #TWRL116A-TYPE-CODE           := 220        NOT IN SPECS  02/07/07 RM
                //* * #TWRL116A-COUNTRY             := FORM.TIRF-COUNTRY-CODE   02/07/07 RM
                //*  ------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-COMPANY-INFO
                //*  ================================
                //*   09/05/2002 MARINA
                //*  #TWRACOMP.#COMP-CODE       := FORM.TIRF-COMPANY-CDE
                //*  ------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPANY-BREAK
                //*   09/05/2002 MARINA
                //*  #TWRACOMP.#COMP-CODE       := #WS-OLD-COMPANY
                //*  #TWRL116B-TYPE-CODE           :=  421
                //*   09/05/2002 MARINA
                //*  IF #WS-OLD-COMPANY      =  'T'
                //*   09/05/2002 END
                //* *#TWRL116B-PAYER-PROVINCE      := 'NY'
                //* *#TWRL116B-CONTACT-AREA-CODE   :=  #WS-AREA-CODE
                //* *#TWRL116B-CONTACT-PHONE       :=  8422733
                //*  ------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TOTAL-REC
                //*  ========================
                //*   09/05/2002 MARINA
                //*  #TWRACOMP.#COMP-CODE       := 'T'
                //*  #TWRACOM2.#COMP-CODE       := 'S'
                //* *COMPRESS #WS-PHONE-NUM3 #WS-PHONE-NUM4
                //* *  INTO #WS-PHONE-NUMBERA LEAVING NO
                //*  #TWRL116C-TYPE-CODE       :=  916
                //*   09/05/2002 MARINA
                //*  #TWRL116C-TRANS-NUMBER    :=  'MM002014'
                //* *#TWRL116C-TRANS-NUMBER    :=  'MM999999'
                //*   09/05/2002 END
                //*  #TWRL116C-NUM-OF-SUMMARY  :=  1
                //*   09/05/2002 MARINA
                //*  #TWRL116C-TRANS-NAME-1    :=  #COMP-NAME
                //*  #TWRL116C-TRANS-NAME-2    :=  ' '
                //*   09/05/2002 END
                //*  #TWRL116C-TRANS-ADDRESS-2 :=  'C5 08'
                //*  02-10-2005 : ACCORDING TO SPECIFICATIONS, THIS FIELD S/B NUMERIC.
                //*               HOWEVER, IT IS POPULATED WITH THE ALPHA-NUMERIC STATE
                //*               CODE 'NY'.
                //* *#TWRL116C-TRANS-AREA-CODE       :=  #WS-AREA-CODE
                //*  #TWRL116C-TRANS-PHONE           :=  #WS-PHONE-NUMBER
                //* *#TWRL116C-TRANS-PHONE     :=  8422733
                //*  #TWRL116C-TRANS-AREA-CODE := #WS-AREA-CODE-3
                //*  #TWRL116C-TRANS-PHONE     := #WS-PHONE-7
                //*  ------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PREV-RUN
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Display_Accepted_Form() throws Exception                                                                                                             //Natural: DISPLAY-ACCEPTED-FORM
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        //*  02-10-2005
        getReports().display(2, "/ TIN ",                                                                                                                                 //Natural: DISPLAY ( 2 ) '/ TIN ' FORM.TIRF-TIN 'TIN/Typ' FORM.TIRF-TAX-ID-TYPE '/Contract' FORM.TIRF-CONTRACT-NBR '/Payee' FORM.TIRF-PAYEE-CDE 'Inc/Cde' FORM.TIRF-NR4-INCOME-CODE '/    Gross' FORM.TIRF-GROSS-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) '/ Interest' FORM.TIRF-INT-AMT ( EM = ZZZ,ZZ9.99 ) 3X '  Tax/  Wthld' FORM.TIRF-FED-TAX-WTHLD ( EM = ZZZZZZ,ZZ9.99 ) '/C/o' FORM.TIRF-COMPANY-CDE
        		ldaTwrl9710.getForm_Tirf_Tin(),"TIN/Typ",
        		ldaTwrl9710.getForm_Tirf_Tax_Id_Type(),"/Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"/Payee",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"Inc/Cde",
        		ldaTwrl9710.getForm_Tirf_Nr4_Income_Code(),"/    Gross",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"/ Interest",
        		ldaTwrl9710.getForm_Tirf_Int_Amt(), new ReportEditMask ("ZZZ,ZZ9.99"),new ColumnSpacing(3),"  Tax/  Wthld",
        		ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld(), new ReportEditMask ("ZZZZZZ,ZZ9.99"),"/C/o",
        		ldaTwrl9710.getForm_Tirf_Company_Cde());
        if (Global.isEscape()) return;
        //* *'/Prt/Hld'      FORM.TIRF-PRINT-HOLD-IND
        //* *'/Rpt/Ind'      FORM.TIRF-PART-RPT-IND
        //* *'#IN/INC/OLD'   #IN-INC-OLD
        //* *'IRS/Rej/Ind'   FORM.TIRF-IRS-REJECT-IND
        //* *'IRS/Rpt/Ind'   FORM.TIRF-IRS-RPT-IND
        //* *'//Ctry'        FORM.TIRF-COUNTRY-CODE
        //* *'/Report/ Date' FORM.TIRF-PART-RPT-DATE
        //* *'/ Last/ User'  FORM.TIRF-LU-USER
        //* *'/ Last/Update' FORM.TIRF-LU-TS         (EM=MM/DD/YY)
        pnd_Work_Area_Pnd_Ws_Cnt.getValue(1).nadd(1);                                                                                                                     //Natural: ADD 1 TO #WS-CNT ( 1 )
        pnd_Work_Area_Pnd_Ws_Cnt.getValue(3).nadd(1);                                                                                                                     //Natural: ADD 1 TO #WS-CNT ( 3 )
        pnd_Work_Area_Pnd_Ws_Cnt_Tot.getValue(1).nadd(1);                                                                                                                 //Natural: ADD 1 TO #WS-CNT-TOT ( 1 )
        pnd_Work_Area_Pnd_Ws_Cnt_Tot.getValue(3).nadd(1);                                                                                                                 //Natural: ADD 1 TO #WS-CNT-TOT ( 3 )
        pnd_Work_Area_Pnd_Ws_Gross.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                                //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS ( 1 )
        pnd_Work_Area_Pnd_Ws_Gross.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                                //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS ( 3 )
        pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                            //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS-TOT ( 1 )
        pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                            //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS-TOT ( 3 )
        pnd_Work_Area_Pnd_Ws_Int.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                    //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT ( 1 )
        pnd_Work_Area_Pnd_Ws_Int.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                    //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT ( 3 )
        pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT-TOT ( 1 )
        pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT-TOT ( 3 )
        pnd_Work_Area_Pnd_Ws_Wthld.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                                                            //Natural: ADD FORM.TIRF-FED-TAX-WTHLD TO #WS-WTHLD ( 1 )
        pnd_Work_Area_Pnd_Ws_Wthld.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                                                            //Natural: ADD FORM.TIRF-FED-TAX-WTHLD TO #WS-WTHLD ( 3 )
        pnd_Work_Area_Pnd_Ws_Wthld_Tot.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                                                        //Natural: ADD FORM.TIRF-FED-TAX-WTHLD TO #WS-WTHLD-TOT ( 1 )
        pnd_Work_Area_Pnd_Ws_Wthld_Tot.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                                                        //Natural: ADD FORM.TIRF-FED-TAX-WTHLD TO #WS-WTHLD-TOT ( 3 )
        //*  DISPLAY-ACCEPTED
    }
    private void sub_Display_Rejected_Form() throws Exception                                                                                                             //Natural: DISPLAY-REJECTED-FORM
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        pnd_Work_Area_Pnd_Ws_Reject_Counter.nadd(1);                                                                                                                      //Natural: ADD 1 TO #WS-REJECT-COUNTER
        if (condition(pnd_Work_Area_Pnd_Ws_Reject_Counter.equals(1)))                                                                                                     //Natural: IF #WS-REJECT-COUNTER = 1
        {
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-INFO
            sub_Get_Company_Info();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  02-10-2005
        getReports().display(3, "/ TIN ",                                                                                                                                 //Natural: DISPLAY ( 3 ) '/ TIN ' FORM.TIRF-TIN 'TIN/Typ' FORM.TIRF-TAX-ID-TYPE '/Contract' FORM.TIRF-CONTRACT-NBR '/Payee' FORM.TIRF-PAYEE-CDE 'Inc/Cde' FORM.TIRF-NR4-INCOME-CODE '/    Gross' FORM.TIRF-GROSS-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) '/ Interest' FORM.TIRF-INT-AMT ( EM = ZZZ,ZZ9.99 ) 3X '  Tax/  Wthld' FORM.TIRF-FED-TAX-WTHLD ( EM = ZZZZZZ,ZZ9.99 ) 'C/o' FORM.TIRF-COMPANY-CDE
        		ldaTwrl9710.getForm_Tirf_Tin(),"TIN/Typ",
        		ldaTwrl9710.getForm_Tirf_Tax_Id_Type(),"/Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"/Payee",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"Inc/Cde",
        		ldaTwrl9710.getForm_Tirf_Nr4_Income_Code(),"/    Gross",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"/ Interest",
        		ldaTwrl9710.getForm_Tirf_Int_Amt(), new ReportEditMask ("ZZZ,ZZ9.99"),new ColumnSpacing(3),"  Tax/  Wthld",
        		ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld(), new ReportEditMask ("ZZZZZZ,ZZ9.99"),"C/o",
        		ldaTwrl9710.getForm_Tirf_Company_Cde());
        if (Global.isEscape()) return;
        //* *'/Prt/Hld'      FORM.TIRF-PRINT-HOLD-IND
        //* *'/Rpt/Ind'      FORM.TIRF-PART-RPT-IND
        //* *'#IN/INC/OLD'   #IN-INC-OLD
        //* *'IRS/Rej/Ind'   FORM.TIRF-IRS-REJECT-IND
        //* *'IRS/Rpt/Ind'   FORM.TIRF-IRS-RPT-IND
        //* *'//Ctry'        FORM.TIRF-COUNTRY-CODE
        //* *'/Report/ Date' FORM.TIRF-PART-RPT-DATE
        //* *'/ Last/ User'  FORM.TIRF-LU-USER
        //* *'/ Last/Update' FORM.TIRF-LU-TS         (EM=MM/DD/YY)
        pnd_Work_Area_Pnd_Ws_Cnt.getValue(2).nadd(1);                                                                                                                     //Natural: ADD 1 TO #WS-CNT ( 2 )
        pnd_Work_Area_Pnd_Ws_Cnt.getValue(3).nadd(1);                                                                                                                     //Natural: ADD 1 TO #WS-CNT ( 3 )
        pnd_Work_Area_Pnd_Ws_Cnt_Tot.getValue(2).nadd(1);                                                                                                                 //Natural: ADD 1 TO #WS-CNT-TOT ( 2 )
        pnd_Work_Area_Pnd_Ws_Cnt_Tot.getValue(3).nadd(1);                                                                                                                 //Natural: ADD 1 TO #WS-CNT-TOT ( 3 )
        pnd_Work_Area_Pnd_Ws_Gross.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                                //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS ( 2 )
        pnd_Work_Area_Pnd_Ws_Gross.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                                //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS ( 3 )
        pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                            //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS-TOT ( 2 )
        pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                            //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS-TOT ( 3 )
        pnd_Work_Area_Pnd_Ws_Int.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                    //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT ( 2 )
        pnd_Work_Area_Pnd_Ws_Int.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                    //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT ( 3 )
        pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT-TOT ( 2 )
        pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT-TOT ( 3 )
        pnd_Work_Area_Pnd_Ws_Wthld.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                                                            //Natural: ADD FORM.TIRF-FED-TAX-WTHLD TO #WS-WTHLD ( 2 )
        pnd_Work_Area_Pnd_Ws_Wthld.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                                                            //Natural: ADD FORM.TIRF-FED-TAX-WTHLD TO #WS-WTHLD ( 3 )
        pnd_Work_Area_Pnd_Ws_Wthld_Tot.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                                                        //Natural: ADD FORM.TIRF-FED-TAX-WTHLD TO #WS-WTHLD-TOT ( 2 )
        pnd_Work_Area_Pnd_Ws_Wthld_Tot.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                                                        //Natural: ADD FORM.TIRF-FED-TAX-WTHLD TO #WS-WTHLD-TOT ( 3 )
        //*  DISPLAY-REJECTED
    }
    private void sub_Write_Nr4_Rec() throws Exception                                                                                                                     //Natural: WRITE-NR4-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*  =============================
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-INFO
        sub_Get_Company_Info();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //*  ADD NEW LOGIC TO GET CORRECT COUNTRY AND PROVINCE CODES   02/07/07 RM
        //*                                              STARTS HERE
        ldaTwrl116a.getPnd_Twrl116a().reset();                                                                                                                            //Natural: RESET #TWRL116A
        if (condition(ldaTwrl9710.getForm_Tirf_Foreign_Addr().equals("Y")))                                                                                               //Natural: IF FORM.TIRF-FOREIGN-ADDR = 'Y'
        {
            if (condition(ldaTwrl9710.getForm_Tirf_Province_Code().equals(" ")))                                                                                          //Natural: IF FORM.TIRF-PROVINCE-CODE = ' '
            {
                pdaTwratbl3.getTwratbl3_Pnd_Function().setValue("1");                                                                                                     //Natural: ASSIGN TWRATBL3.#FUNCTION := '1'
                pdaTwratbl3.getTwratbl3_Pnd_Tax_Year().setValue(pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn);                                                                   //Natural: ASSIGN TWRATBL3.#TAX-YEAR := #TIRF-TAX-YEARN
                pdaTwratbl3.getTwratbl3_Pnd_Country_Cde().setValue(ldaTwrl9710.getForm_Tirf_Geo_Cde());                                                                   //Natural: ASSIGN TWRATBL3.#COUNTRY-CDE := FORM.TIRF-GEO-CDE
                DbsUtil.callnat(Twrntbl3.class , getCurrentProcessState(), pdaTwratbl3.getTwratbl3_Input_Parms(), new AttributeParameter("O"), pdaTwratbl3.getTwratbl3_Output_Data(),  //Natural: CALLNAT 'TWRNTBL3' USING TWRATBL3.INPUT-PARMS ( AD = O ) TWRATBL3.OUTPUT-DATA ( AD = M )
                    new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Country().setValue(pdaTwratbl3.getTwratbl3_Tircntl_Cntry_Abbr());                                                //Natural: ASSIGN #TWRL116A-COUNTRY := TWRATBL3.TIRCNTL-CNTRY-ABBR
                pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code().setValue("ZZ");                                                                                        //Natural: ASSIGN TIRCNTL-STATE-ALPHA-CODE := 'ZZ'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Country().setValue("CAN");                                                                                       //Natural: ASSIGN #TWRL116A-COUNTRY := 'CAN'
                pdaTwratbl2.getTwratbl2_Pnd_Function().setValue(1);                                                                                                       //Natural: ASSIGN TWRATBL2.#FUNCTION := 1
                pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().setValue(pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn);                                                                   //Natural: ASSIGN TWRATBL2.#TAX-YEAR := #TIRF-TAX-YEARN
                pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(ldaTwrl9710.getForm_Tirf_Province_Code());                                                               //Natural: ASSIGN TWRATBL2.#STATE-CDE := FORM.TIRF-PROVINCE-CODE
                DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
                    new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Country().setValue("USA");                                                                                           //Natural: ASSIGN #TWRL116A-COUNTRY := 'USA'
            pdaTwratbl2.getTwratbl2_Pnd_Function().setValue(1);                                                                                                           //Natural: ASSIGN TWRATBL2.#FUNCTION := 1
            pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().setValue(pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn);                                                                       //Natural: ASSIGN TWRATBL2.#TAX-YEAR := #TIRF-TAX-YEARN
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "0", ldaTwrl9710.getForm_Tirf_Geo_Cde()));                   //Natural: COMPRESS '0' FORM.TIRF-GEO-CDE INTO TWRATBL2.#STATE-CDE LEAVING NO
            DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
            //*     03/07/07 RM
            //*     03/07/07 RM
            //*  02/07/07 RM
            //*  03/07/07 RM
            //*  03/07/07 RM
            //*  03/07/07 RM
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Corp_Name_1().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Trans_A());                                                  //Natural: ASSIGN #TWRL116A-CORP-NAME-1 := #COMP-TRANS-A
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Corp_Name_2().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Trans_B());                                                  //Natural: ASSIGN #TWRL116A-CORP-NAME-2 := #COMP-TRANS-B
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Surname().setValue(ldaTwrl9710.getForm_Tirf_Part_Last_Nme());                                                            //Natural: ASSIGN #TWRL116A-SURNAME := FORM.TIRF-PART-LAST-NME
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_First_Name().setValue(ldaTwrl9710.getForm_Tirf_Part_First_Nme());                                                        //Natural: ASSIGN #TWRL116A-FIRST-NAME := FORM.TIRF-PART-FIRST-NME
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Initial().setValue(ldaTwrl9710.getForm_Tirf_Part_Mddle_Nme());                                                           //Natural: ASSIGN #TWRL116A-INITIAL := FORM.TIRF-PART-MDDLE-NME
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Sec_Surname().setValue(" ");                                                                                             //Natural: ASSIGN #TWRL116A-SEC-SURNAME := ' '
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Sec_First_Name().setValue(" ");                                                                                          //Natural: ASSIGN #TWRL116A-SEC-FIRST-NAME := ' '
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Sec_Initial().setValue(" ");                                                                                             //Natural: ASSIGN #TWRL116A-SEC-INITIAL := ' '
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Address_1().setValue(ldaTwrl9710.getForm_Tirf_Street_Addr());                                                            //Natural: ASSIGN #TWRL116A-ADDRESS-1 := FORM.TIRF-STREET-ADDR
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Address_2().setValue(ldaTwrl9710.getForm_Tirf_Street_Addr_Cont_1());                                                     //Natural: ASSIGN #TWRL116A-ADDRESS-2 := FORM.TIRF-STREET-ADDR-CONT-1
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_City().setValue(ldaTwrl9710.getForm_Tirf_City());                                                                        //Natural: ASSIGN #TWRL116A-CITY := FORM.TIRF-CITY
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Province().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code());                                                 //Natural: ASSIGN #TWRL116A-PROVINCE := TIRCNTL-STATE-ALPHA-CODE
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Zip().setValue(ldaTwrl9710.getForm_Tirf_Zip());                                                                          //Natural: ASSIGN #TWRL116A-ZIP := FORM.TIRF-ZIP
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Tax_Country_Code().setValue(ldaTwrl9710.getForm_Tirf_Country_Code());                                                    //Natural: ASSIGN #TWRL116A-TAX-COUNTRY-CODE := FORM.TIRF-COUNTRY-CODE
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Foreign_Ssn().setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                  //Natural: ASSIGN #TWRL116A-FOREIGN-SSN := FORM.TIRF-TIN
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Non_Resident_Acct().setValue(pnd_Work_Area_Pnd_Ws_Nr4_Id);                                                               //Natural: ASSIGN #TWRL116A-NON-RESIDENT-ACCT := #WS-NR4-ID
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Recipient_Type().setValue(1);                                                                                            //Natural: ASSIGN #TWRL116A-RECIPIENT-TYPE := 1
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Payer_Id().setValue(pnd_Work_Area_Pnd_Ws_Fed_Id);                                                                        //Natural: ASSIGN #TWRL116A-PAYER-ID := #WS-FED-ID
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Income_Code().compute(new ComputeParameters(false, ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Income_Code()),              //Natural: ASSIGN #TWRL116A-INCOME-CODE := VAL ( TIRF-NR4-INCOME-CODE )
            ldaTwrl9710.getForm_Tirf_Nr4_Income_Code().val());
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Currency_Code().setValue("USD");                                                                                         //Natural: ASSIGN #TWRL116A-CURRENCY-CODE := 'USD'
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Exemption_Code().setValue(" ");                                                                                          //Natural: ASSIGN #TWRL116A-EXEMPTION-CODE := ' '
        //*   09/05/2002 MARINA - STARTING 2002 '02' WILL BE REPLACED BY '39'
        if (condition(ldaTwrl9710.getForm_Tirf_Nr4_Income_Code().equals("02") || ldaTwrl9710.getForm_Tirf_Nr4_Income_Code().equals("03")))                                //Natural: IF FORM.TIRF-NR4-INCOME-CODE = '02' OR = '03'
        {
            ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Gross_Income().setValue(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                       //Natural: ASSIGN #TWRL116A-GROSS-INCOME := FORM.TIRF-GROSS-AMT
            //*  62 - INTEREST
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Gross_Income().setValue(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                         //Natural: ASSIGN #TWRL116A-GROSS-INCOME := FORM.TIRF-INT-AMT
            //*  03/07/07 RM
            //*  03/07/07 RM
            //*  03/07/07 RM
            //*  03/07/07 RM
            //*   NEW FOR 2006    03/07/07 RM
            //*   ADDED FOR XML   03/07/07 RM
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Non_Res_Tax_Held().setValue(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                                   //Natural: ASSIGN #TWRL116A-NON-RES-TAX-HELD := FORM.TIRF-FED-TAX-WTHLD
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Income_Code_Box_24().setValue(0);                                                                                        //Natural: ASSIGN #TWRL116A-INCOME-CODE-BOX-24 := 0
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Currency_Code_Box_25().setValue("USD");                                                                                  //Natural: ASSIGN #TWRL116A-CURRENCY-CODE-BOX-25 := 'USD'
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Gross_Income_Box_26().setValue(0);                                                                                       //Natural: ASSIGN #TWRL116A-GROSS-INCOME-BOX-26 := 0
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Tax_Held_Box_27().setValue(0);                                                                                           //Natural: ASSIGN #TWRL116A-TAX-HELD-BOX-27 := 0
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Exemp_Code_Box_28().setValue(" ");                                                                                       //Natural: ASSIGN #TWRL116A-EXEMP-CODE-BOX-28 := ' '
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Rpt_Type().setValue("O");                                                                                                //Natural: ASSIGN #TWRL116A-RPT-TYPE := 'O'
        ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Rec_Type().setValue("D");                                                                                                //Natural: ASSIGN #TWRL116A-REC-TYPE := 'D'
        pnd_Work_Area_Pnd_Ws_Trailer.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "B", ldaTwrl9710.getForm_Tirf_Company_Cde(), ldaTwrl9710.getForm_Tirf_Contract_Nbr())); //Natural: COMPRESS 'B' TIRF-COMPANY-CDE TIRF-CONTRACT-NBR INTO #WS-TRAILER LEAVING NO
        //*  KH
        getWorkFiles().write(1, false, ldaTwrl116a.getPnd_Twrl116a(), pnd_Work_Area_Pnd_Ws_Trailer);                                                                      //Natural: WRITE WORK FILE 1 #TWRL116A #WS-TRAILER
        //*   02/07/07 RM
        pnd_Work_Area_Pnd_Ws_Tot_Nr4_Count.nadd(1);                                                                                                                       //Natural: ADD 1 TO #WS-TOT-NR4-COUNT
        pnd_Work_Area_Pnd_Ws_Tot_Gross_Income.nadd(ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Gross_Income());                                                              //Natural: ADD #TWRL116A-GROSS-INCOME TO #WS-TOT-GROSS-INCOME
        pnd_Work_Area_Pnd_Ws_Tot_Non_Res_Tax_Held.nadd(ldaTwrl116a.getPnd_Twrl116a_Pnd_Twrl116a_Non_Res_Tax_Held());                                                      //Natural: ADD #TWRL116A-NON-RES-TAX-HELD TO #WS-TOT-NON-RES-TAX-HELD
        //*  TAKE OUT THESE TWO LINES, SINCE THERE's no accumulations 03/07/07 RM
        //*  ADD #TWRL116A-GROSS-INCOME-BOX-26  TO #WS-TOT-GROSS-INCOME-BOX-26
        //*  ADD #TWRL116A-TAX-HELD-BOX-27      TO #WS-TOT-TAX-HELD-BOX-27
    }
    //*  02-10-2005
    private void sub_Get_Company_Info() throws Exception                                                                                                                  //Natural: GET-COMPANY-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue(ldaTwrl9710.getForm_Tirf_Company_Cde());                                                                     //Natural: ASSIGN #TWRACOM2.#COMP-CODE := FORM.TIRF-COMPANY-CDE
        pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().setValue(pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn);                                                                       //Natural: ASSIGN #TWRACOM2.#TAX-YEAR := #TIRF-TAX-YEARN
        pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(7);                                                                                                          //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 7
        //*  CALLNAT 'TWRNCOMP'
        //*   USING
        //*   #TWRACOMP.#INPUT-PARMS (AD=O)
        //*   #TWRACOMP.#OUTPUT-DATA (AD=M)
        DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOM2' USING #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        //*   09/05/2002 MARINA
    }
    //*  12/29/2005  RM
    private void sub_Company_Break() throws Exception                                                                                                                     //Natural: COMPANY-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        //*  =============================
        pnd_Work_Area_Pnd_Ws_Company_Counter.nadd(1);                                                                                                                     //Natural: ADD 1 TO #WS-COMPANY-COUNTER
        getReports().write(1, pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name()," Gross:        ",pnd_Work_Area_Pnd_Ws_Gross.getValue(1), new ReportEditMask              //Natural: WRITE ( 1 ) #COMP-SHORT-NAME ' Gross:        ' #WS-GROSS ( 1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 2X #WS-GROSS ( 2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 9X #WS-GROSS ( 3 ) ( EM = ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Work_Area_Pnd_Ws_Gross.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(9),pnd_Work_Area_Pnd_Ws_Gross.getValue(3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(1, pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name()," Interest:     ",pnd_Work_Area_Pnd_Ws_Int.getValue(1), new ReportEditMask                //Natural: WRITE ( 1 ) #COMP-SHORT-NAME ' Interest:     ' #WS-INT ( 1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 2X #WS-INT ( 2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 9X #WS-INT ( 3 ) ( EM = ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Work_Area_Pnd_Ws_Int.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(9),pnd_Work_Area_Pnd_Ws_Int.getValue(3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(1, pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name()," Tax Withheld: ",pnd_Work_Area_Pnd_Ws_Wthld.getValue(1), new ReportEditMask              //Natural: WRITE ( 1 ) #COMP-SHORT-NAME ' Tax Withheld: ' #WS-WTHLD ( 1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 2X #WS-WTHLD ( 2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 9X #WS-WTHLD ( 3 ) ( EM = ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Work_Area_Pnd_Ws_Wthld.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(9),pnd_Work_Area_Pnd_Ws_Wthld.getValue(3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(1, pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name()," Forms:        ",new TabSetting(25),pnd_Work_Area_Pnd_Ws_Cnt.getValue(1),new             //Natural: WRITE ( 1 ) #COMP-SHORT-NAME ' Forms:        ' 25T #WS-CNT ( 1 ) 41T #WS-CNT ( 2 ) 64T #WS-CNT ( 3 )
            TabSetting(41),pnd_Work_Area_Pnd_Ws_Cnt.getValue(2),new TabSetting(64),pnd_Work_Area_Pnd_Ws_Cnt.getValue(3));
        if (Global.isEscape()) return;
        getReports().write(1, "* --------------------------------------------------------");                                                                              //Natural: WRITE ( 1 ) '* --------------------------------------------------------'
        if (Global.isEscape()) return;
        getReports().write(2, NEWLINE,"  Total",pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),"Accepted ",pnd_Work_Area_Pnd_Ws_Cnt.getValue(1),new                    //Natural: WRITE ( 2 ) / '  Total' #COMP-SHORT-NAME 'Accepted ' #WS-CNT ( 1 ) 36T #WS-GROSS ( 1 ) ( EM = ZZ,ZZZ,ZZ9.99 ) #WS-INT ( 1 ) ( EM = ZZZ,ZZ9.99 ) #WS-WTHLD ( 1 ) ( EM = ZZZZZZ,ZZ9.99 )
            TabSetting(36),pnd_Work_Area_Pnd_Ws_Gross.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Int.getValue(1), new ReportEditMask 
            ("ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Wthld.getValue(1), new ReportEditMask ("ZZZZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(3, NEWLINE,"  Total",pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),"Rejected ",pnd_Work_Area_Pnd_Ws_Cnt.getValue(2),new                    //Natural: WRITE ( 3 ) / '  Total' #COMP-SHORT-NAME 'Rejected ' #WS-CNT ( 2 ) 36T #WS-GROSS ( 2 ) ( EM = ZZ,ZZZ,ZZ9.99 ) #WS-INT ( 2 ) ( EM = ZZZ,ZZ9.99 ) #WS-WTHLD ( 2 ) ( EM = ZZZZZZ,ZZ9.99 )
            TabSetting(36),pnd_Work_Area_Pnd_Ws_Gross.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Int.getValue(2), new ReportEditMask 
            ("ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Wthld.getValue(2), new ReportEditMask ("ZZZZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  02-10-2005
        pnd_Work_Area_Pnd_Ws_Cnt.getValue(1).reset();                                                                                                                     //Natural: RESET #WS-CNT ( 1 ) #WS-GROSS ( 1 ) #WS-INT ( 1 ) #WS-WTHLD ( 1 ) #WS-CNT ( 2 ) #WS-GROSS ( 2 ) #WS-INT ( 2 ) #WS-WTHLD ( 2 ) #WS-CNT ( 3 ) #WS-GROSS ( 3 ) #WS-INT ( 3 ) #WS-WTHLD ( 3 )
        pnd_Work_Area_Pnd_Ws_Gross.getValue(1).reset();
        pnd_Work_Area_Pnd_Ws_Int.getValue(1).reset();
        pnd_Work_Area_Pnd_Ws_Wthld.getValue(1).reset();
        pnd_Work_Area_Pnd_Ws_Cnt.getValue(2).reset();
        pnd_Work_Area_Pnd_Ws_Gross.getValue(2).reset();
        pnd_Work_Area_Pnd_Ws_Int.getValue(2).reset();
        pnd_Work_Area_Pnd_Ws_Wthld.getValue(2).reset();
        pnd_Work_Area_Pnd_Ws_Cnt.getValue(3).reset();
        pnd_Work_Area_Pnd_Ws_Gross.getValue(3).reset();
        pnd_Work_Area_Pnd_Ws_Int.getValue(3).reset();
        pnd_Work_Area_Pnd_Ws_Wthld.getValue(3).reset();
        pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue(pnd_Work_Area_Pnd_Ws_Old_Company);                                                                           //Natural: ASSIGN #TWRACOM2.#COMP-CODE := #WS-OLD-COMPANY
        pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().setValue(pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn);                                                                       //Natural: ASSIGN #TWRACOM2.#TAX-YEAR := #TIRF-TAX-YEARN
        pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(7);                                                                                                          //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 7
        //*  CALLNAT 'TWRNCOMP'
        //*   USING
        //*   #TWRACOMP.#INPUT-PARMS (AD=O)
        //*   #TWRACOMP.#OUTPUT-DATA (AD=M)
        DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOM2' USING #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        //*   09/05/2002 END
        pnd_Work_Area_Pnd_Ws_Phone.setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Phone());                                                                                     //Natural: MOVE #PHONE TO #WS-PHONE
        //* *COMPRESS #WS-PHONE-NUM3 #WS-PHONE-NUM4
        //* *  INTO #WS-PHONE-NUMBERA LEAVING NO
        //*  ONLY TIAA  02/07/07 RM
        ldaTwrl116b.getPnd_Twrl116b().reset();                                                                                                                            //Natural: RESET #TWRL116B
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Non_Resident_Acct().setValue("NRX156967");                                                                               //Natural: ASSIGN #TWRL116B-NON-RESIDENT-ACCT := 'NRX156967'
        //*  ELSE
        //*   #TWRL116B-NON-RES-ACCT      := 'NRX632587'
        //*  END-IF
        //*  02-10-2005
        //*  02-10-2005
        //*  02-10-2005
        if (condition(pnd_Work_Area_Pnd_Ws_Old_Company.equals("T")))                                                                                                      //Natural: IF #WS-OLD-COMPANY = 'T'
        {
            ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Payer_Name_1().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Payer_A());                                             //Natural: ASSIGN #TWRL116B-PAYER-NAME-1 := #COMP-PAYER-A
            ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Payer_Name_2().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Payer_B());                                             //Natural: ASSIGN #TWRL116B-PAYER-NAME-2 := #COMP-PAYER-B
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  02-10-2005
            //*  02-10-2005
            if (condition(pnd_Work_Area_Pnd_Ws_Old_Company.equals("S")))                                                                                                  //Natural: IF #WS-OLD-COMPANY = 'S'
            {
                ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Payer_Name_1().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Agent_A());                                         //Natural: ASSIGN #TWRL116B-PAYER-NAME-1 := #COMP-AGENT-A
                //*  02-10-2005
                //*  OLD CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Payer_Name_1().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Name());                                            //Natural: ASSIGN #TWRL116B-PAYER-NAME-1 := #COMP-NAME
            }                                                                                                                                                             //Natural: END-IF
            //*  02-10-2005
            ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Payer_Name_2().reset();                                                                                              //Natural: RESET #TWRL116B-PAYER-NAME-2
            //*   02/07/07 RM
            //*  12-14-12 RCC
            //*  02-10-2005
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Payer_Name_3().setValue(" ");                                                                                            //Natural: ASSIGN #TWRL116B-PAYER-NAME-3 := ' '
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Payer_Address_1().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Address().getValue(1));                                       //Natural: ASSIGN #TWRL116B-PAYER-ADDRESS-1 := #ADDRESS ( 1 )
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Payer_Address_2().setValue("C5 08");                                                                                     //Natural: ASSIGN #TWRL116B-PAYER-ADDRESS-2 := 'C5 08'
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Payer_City().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_City());                                                           //Natural: ASSIGN #TWRL116B-PAYER-CITY := #CITY
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Payer_Province().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_State());                                                      //Natural: ASSIGN #TWRL116B-PAYER-PROVINCE := #STATE
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Payer_Country_Code().setValue("USA");                                                                                    //Natural: ASSIGN #TWRL116B-PAYER-COUNTRY-CODE := 'USA'
        //* *#TWRL116B-PAYER-ZIP           := #ZIP                    /* 02-10-2005
        //*  02-10-2005
        //*   03/07/07 RM
        //*  02-10-2005
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Payer_Zip().setValueEdited(pdaTwracom2.getPnd_Twracom2_Pnd_Zip_9(),new ReportEditMask("XXXXX-XXXX"));                    //Natural: MOVE EDITED #ZIP-9 ( EM = XXXXX-XXXX ) TO #TWRL116B-PAYER-ZIP
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Contact_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Contact_Name());                                                 //Natural: ASSIGN #TWRL116B-CONTACT-NAME := #CONTACT-NAME
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Contact_Area_Code().setValue(pnd_Work_Area_Pnd_Ws_Area_Code_3);                                                          //Natural: ASSIGN #TWRL116B-CONTACT-AREA-CODE := #WS-AREA-CODE-3
        //*  #TWRL116B-CONTACT-PHONE       := #WS-PHONE-7             /* 02-10-2005
        //*   03/07/07 RM
        //*  FO 02/01
        //*   03/07/07 RM
        //*  ADDED FOR 2006 03/07/07 RM
        //*   03/07/07 RM
        //*   03/07/07 RM
        //*  ADDED FOR XML  03/07/07 RM
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Contact_Phone().setValueEdited(pnd_Work_Area_Pnd_Ws_Phone_7,new ReportEditMask("999-9999"));                             //Natural: MOVE EDITED #WS-PHONE-7 ( EM = 999-9999 ) TO #TWRL116B-CONTACT-PHONE
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Contact_Phone_Ext().setValue(pnd_Work_Area_Pnd_Ws_Extension_5);                                                          //Natural: ASSIGN #TWRL116B-CONTACT-PHONE-EXT := #WS-EXTENSION-5
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Tax_Year().compute(new ComputeParameters(false, ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Tax_Year()),                    //Natural: ASSIGN #TWRL116B-TAX-YEAR := VAL ( #TIRF-TAX-YEAR )
            pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year.val());
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Total_Nr4().setValue(pnd_Work_Area_Pnd_Ws_Tot_Nr4_Count);                                                                //Natural: ASSIGN #TWRL116B-TOTAL-NR4 := #WS-TOT-NR4-COUNT
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Rpt_Type().setValue("O");                                                                                                //Natural: ASSIGN #TWRL116B-RPT-TYPE := 'O'
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Gross_Income().setValue(pnd_Work_Area_Pnd_Ws_Tot_Gross_Income);                                                          //Natural: ASSIGN #TWRL116B-GROSS-INCOME := #WS-TOT-GROSS-INCOME
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Non_Res_Hld().setValue(pnd_Work_Area_Pnd_Ws_Tot_Non_Res_Tax_Held);                                                       //Natural: ASSIGN #TWRL116B-NON-RES-HLD := #WS-TOT-NON-RES-TAX-HELD
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Tot_Gross_Box_26().setValue(0);                                                                                          //Natural: ASSIGN #TWRL116B-TOT-GROSS-BOX-26 := 0
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Tot_Nrh_Box_27().setValue(0);                                                                                            //Natural: ASSIGN #TWRL116B-TOT-NRH-BOX-27 := 0
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Tot_Not_Reported().setValue(0);                                                                                          //Natural: ASSIGN #TWRL116B-TOT-NOT-REPORTED := 0
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Tot_Held_Not_Rpt().setValue(0);                                                                                          //Natural: ASSIGN #TWRL116B-TOT-HELD-NOT-RPT := 0
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Filler().setValue(" ");                                                                                                  //Natural: ASSIGN #TWRL116B-FILLER := ' '
        ldaTwrl116b.getPnd_Twrl116b_Pnd_Twrl116b_Rec_Type().setValue("S");                                                                                                //Natural: ASSIGN #TWRL116B-REC-TYPE := 'S'
        pnd_Work_Area_Pnd_Ws_Trailer.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "B", pnd_Work_Area_Pnd_Ws_Old_Company, "999999999999"));                    //Natural: COMPRESS 'B' #WS-OLD-COMPANY '999999999999' INTO #WS-TRAILER LEAVING NO
        //*  KH
        getWorkFiles().write(1, false, ldaTwrl116b.getPnd_Twrl116b(), pnd_Work_Area_Pnd_Ws_Trailer);                                                                      //Natural: WRITE WORK FILE 1 #TWRL116B #WS-TRAILER
        pnd_Work_Area_Pnd_Ws_Tot_Gross_Income.reset();                                                                                                                    //Natural: RESET #WS-TOT-GROSS-INCOME #WS-TOT-NON-RES-TAX-HELD #WS-TOT-GROSS-INCOME-BOX-26 #WS-TOT-TAX-HELD-BOX-27 #WS-TOT-NR4-COUNT
        pnd_Work_Area_Pnd_Ws_Tot_Non_Res_Tax_Held.reset();
        pnd_Work_Area_Pnd_Ws_Tot_Gross_Income_Box_26.reset();
        pnd_Work_Area_Pnd_Ws_Tot_Tax_Held_Box_27.reset();
        pnd_Work_Area_Pnd_Ws_Tot_Nr4_Count.reset();
                                                                                                                                                                          //Natural: PERFORM TOTAL-REC
        sub_Total_Rec();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    }
    //*  TRANSMITTER RECORD  03/07/07 RM
    //*  02-10-2005
    //*  02-10-2005
    private void sub_Total_Rec() throws Exception                                                                                                                         //Natural: TOTAL-REC
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue("T");                                                                                                        //Natural: ASSIGN #TWRACOM2.#COMP-CODE := 'T'
        pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().setValue(pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn);                                                                       //Natural: ASSIGN #TWRACOM2.#TAX-YEAR := #TIRF-TAX-YEARN
        pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(7);                                                                                                          //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 7
        //*  CALLNAT 'TWRNCOMP'
        //*   USING
        //*   #TWRACOMP.#INPUT-PARMS (AD=O)
        //*   #TWRACOMP.#OUTPUT-DATA (AD=M)
        DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOM2' USING #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        //*   09/05/2002 END
        //*  NEW FOR 2006  02/07/07 RM
        //*  ORIGINAL      02/07/07 RM
        //*  02-10-2005
        //*   02/07/07 RM
        //*  MUKHERR
        pnd_Work_Area_Pnd_Ws_Phone.setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Phone());                                                                                     //Natural: MOVE #PHONE TO #WS-PHONE
        ldaTwrl116c.getPnd_Twrl116c_Pnd_Twrl116c_Sub_Ref_Id().setValue("00000000");                                                                                       //Natural: ASSIGN #TWRL116C-SUB-REF-ID := '00000000'
        ldaTwrl116c.getPnd_Twrl116c_Pnd_Twrl116c_Rpt_Type_Code().setValue("O");                                                                                           //Natural: ASSIGN #TWRL116C-RPT-TYPE-CODE := 'O'
        ldaTwrl116c.getPnd_Twrl116c_Pnd_Twrl116c_Trans_Number().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Tcc());                                                     //Natural: ASSIGN #TWRL116C-TRANS-NUMBER := #COMP-TCC
        ldaTwrl116c.getPnd_Twrl116c_Pnd_Twrl116c_Trans_Type().setValue(1);                                                                                                //Natural: ASSIGN #TWRL116C-TRANS-TYPE := 1
        ldaTwrl116c.getPnd_Twrl116c_Pnd_Twrl116c_Num_Of_Summary().setValue(pnd_Work_Area_Pnd_Ws_Company_Counter);                                                         //Natural: ASSIGN #TWRL116C-NUM-OF-SUMMARY := #WS-COMPANY-COUNTER
        ldaTwrl116c.getPnd_Twrl116c_Pnd_Twrl116c_Trans_Lang().setValue("E");                                                                                              //Natural: ASSIGN #TWRL116C-TRANS-LANG := 'E'
        ldaTwrl116c.getPnd_Twrl116c_Pnd_Twrl116c_Trans_Name_1().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Trans_A());                                                 //Natural: ASSIGN #TWRL116C-TRANS-NAME-1 := #COMP-TRANS-A
        ldaTwrl116c.getPnd_Twrl116c_Pnd_Twrl116c_Trans_Name_2().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Trans_B());                                                 //Natural: ASSIGN #TWRL116C-TRANS-NAME-2 := #COMP-TRANS-B
        ldaTwrl116c.getPnd_Twrl116c_Pnd_Twrl116c_Trans_Address_1().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Address().getValue(1));                                       //Natural: ASSIGN #TWRL116C-TRANS-ADDRESS-1 := #ADDRESS ( 1 )
        ldaTwrl116c.getPnd_Twrl116c_Pnd_Twrl116c_Trans_Address_2().setValue("E0-S4");                                                                                     //Natural: ASSIGN #TWRL116C-TRANS-ADDRESS-2 := 'E0-S4'
        ldaTwrl116c.getPnd_Twrl116c_Pnd_Twrl116c_Trans_City().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_City());                                                           //Natural: ASSIGN #TWRL116C-TRANS-CITY := #CITY
        ldaTwrl116c.getPnd_Twrl116c_Pnd_Twrl116c_Trans_Province().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_State());                                                      //Natural: ASSIGN #TWRL116C-TRANS-PROVINCE := #STATE
        ldaTwrl116c.getPnd_Twrl116c_Pnd_Twrl116c_Trans_Country().setValue("USA");                                                                                         //Natural: ASSIGN #TWRL116C-TRANS-COUNTRY := 'USA'
        //* *#TWRL116C-TRANS-ZIP             :=  #ZIP                 /* 02-10-2005
        //*  02-10-2005
        //*   02/07/07 RM
        //*   02/07/07 RM
        ldaTwrl116c.getPnd_Twrl116c_Pnd_Twrl116c_Trans_Zip().setValueEdited(pdaTwracom2.getPnd_Twracom2_Pnd_Zip_9(),new ReportEditMask("XXXXX-XXXX"));                    //Natural: MOVE EDITED #ZIP-9 ( EM = XXXXX-XXXX ) TO #TWRL116C-TRANS-ZIP
        ldaTwrl116c.getPnd_Twrl116c_Pnd_Twrl116c_Trans_Contact_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Contact_Name());                                           //Natural: ASSIGN #TWRL116C-TRANS-CONTACT-NAME := #CONTACT-NAME
        ldaTwrl116c.getPnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Area_Code().setValue(pnd_Work_Area_Pnd_Ws_Area_Code_3);                                                       //Natural: ASSIGN #TWRL116C-TRANS-CONT-AREA-CODE := #WS-AREA-CODE-3
        //*   02/07/07 RM
        //*   02/07/07 RM
        //*   02/07/07 RM
        ldaTwrl116c.getPnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Phone().setValueEdited(pnd_Work_Area_Pnd_Ws_Phone_7,new ReportEditMask("999-9999"));                          //Natural: MOVE EDITED #WS-PHONE-7 ( EM = 999-9999 ) TO #TWRL116C-TRANS-CONT-PHONE
        ldaTwrl116c.getPnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Ext().setValue(pnd_Work_Area_Pnd_Ws_Extension_5);                                                             //Natural: ASSIGN #TWRL116C-TRANS-CONT-EXT := #WS-EXTENSION-5
        ldaTwrl116c.getPnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Email().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Contact_Email_Addr());                                       //Natural: ASSIGN #TWRL116C-TRANS-CONT-EMAIL := #CONTACT-EMAIL-ADDR
        ldaTwrl116c.getPnd_Twrl116c_Pnd_Twrl116c_Filler().setValue(" ");                                                                                                  //Natural: ASSIGN #TWRL116C-FILLER := ' '
        ldaTwrl116c.getPnd_Twrl116c_Pnd_Twrl116c_Rec_Type().setValue("T");                                                                                                //Natural: ASSIGN #TWRL116C-REC-TYPE := 'T'
        pnd_Work_Area_Pnd_Ws_Trailer.setValue("A");                                                                                                                       //Natural: ASSIGN #WS-TRAILER := 'A'
        pnd_Work_Area_Pnd_Ws_Trailer.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "A", pnd_Work_Area_Pnd_Ws_Old_Company, "999999999999"));                    //Natural: COMPRESS 'A' #WS-OLD-COMPANY '999999999999' INTO #WS-TRAILER LEAVING NO
        //*   KH
        getWorkFiles().write(1, false, ldaTwrl116c.getPnd_Twrl116c(), pnd_Work_Area_Pnd_Ws_Trailer);                                                                      //Natural: WRITE WORK FILE 1 #TWRL116C #WS-TRAILER
    }
    private void sub_Check_Prev_Run() throws Exception                                                                                                                    //Natural: CHECK-PREV-RUN
    {
        if (BLNatReinput.isReinput()) return;

        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_In_Parm_Pnd_In_Tax_Yearn);                                                                                //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #IN-TAX-YEARN
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(7);                                                                                                           //Natural: ASSIGN #TWRATBL4.#FORM-IND := 7
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(false);                                                                                                      //Natural: ASSIGN #TWRATBL4.#ABEND-IND := FALSE
        //*  #RPT-STATE-CODE := #IN-INPUT-STATE
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Rec_Type().reset();                                                                                                               //Natural: RESET #TWRATBL4.#REC-TYPE
        DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4());                                                                        //Natural: CALLNAT 'TWRNTB4R' #TWRATBL4
        if (condition(Global.isEscape())) return;
        if (condition(! (pdaTwratbl4.getPnd_Twratbl4_Pnd_Ret_Code().getBoolean())))                                                                                       //Natural: IF NOT #TWRATBL4.#RET-CODE
        {
            if (condition(pnd_In_Parm_Pnd_In_Inc_Old.equals("N")))                                                                                                        //Natural: IF #IN-INC-OLD = 'N'
            {
                DbsUtil.terminate(111);  if (true) return;                                                                                                                //Natural: TERMINATE 111
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                GET1:                                                                                                                                                     //Natural: GET TIRCNTL-RPT #TWRATBL4.#ISN
                ldaTwrltb4r.getVw_tircntl_Rpt().readByID(pdaTwratbl4.getPnd_Twratbl4_Pnd_Isn().getLong(), "GET1");
                ldaTwrltb4r.getVw_tircntl_Rpt().deleteDBRow("GET1");                                                                                                      //Natural: DELETE
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=56 ES=ON");
        Global.format(1, "PS=60 LS=133 ES=ON");
        Global.format(2, "PS=60 LS=133 ES=ON");
        Global.format(3, "PS=60 LS=133 ES=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:IIAP"),new TabSetting(28),"TaxWaRS - Annual Original Tax Report - Totals  ",new 
            TabSetting(92),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(36),pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year,"- Canada - Form NR4",NEWLINE,NEWLINE,new TabSetting(26),"  Accepted        Rejected                  Total",NEWLINE,new 
            TabSetting(26),"  --------        --------                  -----");
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:IIAP"),new TabSetting(28),"TaxWaRS - Annual Original Tax Report - Accepted",new 
            TabSetting(92),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(32),pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year,"-",pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),"- Canada - Form NR4");
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:IIAP"),new TabSetting(28),"TaxWaRS - Annual Original Tax Report - Rejected",new 
            TabSetting(92),"Page:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(32),pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year,"-",pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),"- Canada - Form NR4");

        getReports().setDisplayColumns(2, "/ TIN ",
        		ldaTwrl9710.getForm_Tirf_Tin(),"TIN/Typ",
        		ldaTwrl9710.getForm_Tirf_Tax_Id_Type(),"/Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"/Payee",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"Inc/Cde",
        		ldaTwrl9710.getForm_Tirf_Nr4_Income_Code(),"/    Gross",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"/ Interest",
        		ldaTwrl9710.getForm_Tirf_Int_Amt(), new ReportEditMask ("ZZZ,ZZ9.99"),new ColumnSpacing(3),"  Tax/  Wthld",
        		ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld(), new ReportEditMask ("ZZZZZZ,ZZ9.99"),"/C/o",
        		ldaTwrl9710.getForm_Tirf_Company_Cde());
        getReports().setDisplayColumns(3, "/ TIN ",
        		ldaTwrl9710.getForm_Tirf_Tin(),"TIN/Typ",
        		ldaTwrl9710.getForm_Tirf_Tax_Id_Type(),"/Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"/Payee",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"Inc/Cde",
        		ldaTwrl9710.getForm_Tirf_Nr4_Income_Code(),"/    Gross",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"/ Interest",
        		ldaTwrl9710.getForm_Tirf_Int_Amt(), new ReportEditMask ("ZZZ,ZZ9.99"),new ColumnSpacing(3),"  Tax/  Wthld",
        		ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld(), new ReportEditMask ("ZZZZZZ,ZZ9.99"),"C/o",
        		ldaTwrl9710.getForm_Tirf_Company_Cde());
    }
}
