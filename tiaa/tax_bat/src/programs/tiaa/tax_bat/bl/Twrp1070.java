/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:32:37 PM
**        * FROM NATURAL PROGRAM : Twrp1070
************************************************************
**        * FILE NAME            : Twrp1070.java
**        * CLASS NAME           : Twrp1070
**        * INSTANCE NAME        : Twrp1070
************************************************************
****** TEST VERSION DO NOT MOVE TO PRODUCTION  *************************
************************************************************************
** PROGRAM : TWRP1070
** THIS PROGRAM IS BASED ON TWRP5959 WITH ADDITION OF E-DELIVERY DATA
** ON THE FORMS REPORTS.  CREATED BY M. BERLIN.
** SYSTEM  : TAXWARS
** AUTHOR  : FELIX ORTIZ
** FUNCTION: FORMS DATABASE CONTROL REPORTS
**           USED FOR TAX YEARS >= 2011
** HISTORY.....:
**  10/22/11  MS      - IRR AMOUNT FOR 1099-R
**  05/08/12  RC      - 5498 P2 REPORT
**  08/17/12  RC      - SUNY PHASE 2
**  04/05/13  RC      - PARTICIPANT COUNT P2 ONLY
**  04/11/13  RC      - E-DELIVERY ONLY CONTROL REPORT
**  12/05/14  FENDAYA - FATCA CHANGES FOR 1042-S REPORT. FE141205
**  02/16/2016 PAULS  - RECOMPILED  - TWRLCOMP CHANGES.
*    09/27/16  JB - CPM ELIMINATION  SCAN 092716
**  01/19/2017 SINHASN- RECOMPILED  - TWRLCOMP CHANGES.
**  10/19/2017 DASDH- 5498 BOX CHANGES
**  10/05/2018 ARIVU -  RECOMPILED - TWRLCOMP CHANGES. - EINCHG
** 11/24/2020 - PALDE - RESTOW FOR IRS REPORTING 2020 CHANGES
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp1070 extends BLNatBase
{
    // Data Areas
    private LdaTwrlcomp ldaTwrlcomp;
    private PdaTwrafrmn pdaTwrafrmn;
    private LdaTwrl5001 ldaTwrl5001;
    private LdaTwrl5951 ldaTwrl5951;
    private LdaTwrl9710 ldaTwrl9710;
    private LdaTwrl9610 ldaTwrl9610;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_hist_Form;
    private DbsField hist_Form_Tirf_Superde_4;

    private DbsGroup hist_Form__R_Field_1;
    private DbsField hist_Form_Pnd_Tirf_Tax_Year;
    private DbsField hist_Form_Pnd_Tirf_Tin;
    private DbsField hist_Form_Pnd_Tirf_Form_Type;
    private DbsField hist_Form_Pnd_Tirf_Contract_Nbr;
    private DbsField hist_Form_Pnd_Tirf_Payee_Cde;
    private DbsField hist_Form_Pnd_Tirf_Key;
    private DbsField hist_Form_Pnd_Tirf_Invrse_Create_Ts;
    private DbsField hist_Form_Pnd_Tirf_Irs_Rpt_Ind;

    private DataAccessProgramView vw_form_1099_R;
    private DbsField form_1099_R_Tirf_Tax_Year;
    private DbsField form_1099_R_Tirf_Form_Type;
    private DbsField form_1099_R_Tirf_Company_Cde;
    private DbsField form_1099_R_Tirf_Tax_Id_Type;
    private DbsField form_1099_R_Tirf_Tin;
    private DbsField form_1099_R_Tirf_Contract_Nbr;
    private DbsField form_1099_R_Tirf_Payee_Cde;
    private DbsField form_1099_R_Tirf_Key;
    private DbsField form_1099_R_Count_Casttirf_1099_R_State_Grp;

    private DbsGroup form_1099_R_Tirf_1099_R_State_Grp;
    private DbsField form_1099_R_Tirf_State_Rpt_Ind;
    private DbsField form_1099_R_Tirf_State_Hardcopy_Ind;
    private DbsField form_1099_R_Tirf_State_Irs_Rpt_Ind;
    private DbsField form_1099_R_Tirf_State_Code;
    private DbsField form_1099_R_Tirf_State_Distr;
    private DbsField form_1099_R_Tirf_State_Tax_Wthld;
    private DbsField form_1099_R_Tirf_Loc_Code;
    private DbsField form_1099_R_Tirf_Loc_Distr;
    private DbsField form_1099_R_Tirf_Loc_Tax_Wthld;
    private DbsField form_1099_R_Tirf_State_Auth_Rpt_Ind;
    private DbsField form_1099_R_Tirf_State_Auth_Rpt_Date;

    private DataAccessProgramView vw_form_X;
    private DbsField form_X_Tirf_Tax_Year;
    private DbsField form_X_Tirf_Form_Type;
    private DbsField form_X_Tirf_Company_Cde;
    private DbsField form_X_Tirf_Tin;
    private DbsField form_X_Tirf_Contract_Nbr;
    private DbsField form_X_Tirf_Payee_Cde;
    private DbsField form_X_Tirf_Key;
    private DbsField form_X_Tirf_Active_Ind;
    private DbsField form_X_Tirf_Srce_Cde;
    private DbsField form_X_Tirf_Superde_6;

    private DataAccessProgramView vw_form_R;
    private DbsField form_R_Tirf_Tax_Year;
    private DbsField form_R_Tirf_Form_Type;
    private DbsField form_R_Tirf_Company_Cde;
    private DbsField form_R_Tirf_Tin;
    private DbsField form_R_Tirf_Contract_Nbr;
    private DbsField form_R_Tirf_Payee_Cde;
    private DbsField form_R_Tirf_Srce_Cde;
    private DbsField form_R_Tirf_Ira_Acct_Type;
    private DbsField form_R_Tirf_Deceased_Ind;
    private DbsField form_R_Tirf_Contract_Xref;
    private DbsField form_R_Tirf_Trad_Ira_Contrib;

    private DbsGroup form_R__R_Field_2;
    private DbsField form_R_Tirf_Pr_Gross_Amt_Roll;
    private DbsField form_R_Tirf_Roth_Ira_Contrib;

    private DbsGroup form_R__R_Field_3;
    private DbsField form_R_Tirf_Pr_Ivc_Amt_Roll;
    private DbsField form_R_Tirf_Ira_Rechar_Amt;
    private DbsField form_R_Tirf_Roth_Ira_Conversion;
    private DbsField form_R_Tirf_Trad_Ira_Rollover;

    private DbsGroup form_R__R_Field_4;
    private DbsField form_R_Tirf_Irr_Amt;
    private DbsField form_R_Tirf_Account_Fmv;
    private DbsField form_R_Tirf_Pin;
    private DbsField form_R_Tirf_Sep_Amt;
    private DbsField form_R_Tirf_Postpn_Amt;
    private DbsField form_R_Tirf_Postpn_Yr;
    private DbsField form_R_Tirf_Postpn_Code;
    private DbsField form_R_Tirf_Superde_6;

    private DataAccessProgramView vw_twrparti_Read;
    private DbsField twrparti_Read_Twrparti_Status;
    private DbsField twrparti_Read_Twrparti_Tax_Id_Type;
    private DbsField twrparti_Read_Twrparti_Tax_Id;
    private DbsField twrparti_Read_Twrparti_Pin;
    private DbsField twrparti_Read_Twrparti_Rlup_Email_Addr;
    private DbsField twrparti_Read_Twrparti_Rlup_Email_Pref;
    private DbsField twrparti_Read_Twrparti_Rlup_Email_Addr_Dte;
    private DbsField twrparti_Read_Twrparti_Rlup_Email_Pref_Dte;
    private DbsField pnd_Twrparti_Curr_Rec_Sd;

    private DbsGroup pnd_Twrparti_Curr_Rec_Sd__R_Field_5;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Tax_Id;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Status;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Part_Eff_End_Dte;
    private DbsField pnd_Input_Rec;

    private DbsGroup pnd_Input_Rec__R_Field_6;
    private DbsField pnd_Input_Rec_Pnd_Tax_Year;
    private DbsField pnd_Form_Text;

    private DbsGroup pnd_1099r_1099i_Control_Totals;
    private DbsField pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt;
    private DbsField pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt;
    private DbsField pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld;
    private DbsField pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt;
    private DbsField pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt;
    private DbsField pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Distr;
    private DbsField pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld;
    private DbsField pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Distr;
    private DbsField pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld;
    private DbsField pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt;

    private DbsGroup pnd_1099r_1099i_Control_Totals_E;
    private DbsField pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Gross_Amt_E;
    private DbsField pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Taxable_Amt_E;
    private DbsField pnd_1099r_1099i_Control_Totals_E_Pnd_1099_Fed_Tax_Wthld_E;
    private DbsField pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Ivc_Amt_E;
    private DbsField pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Irr_Amt_E;
    private DbsField pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_State_Distr_E;
    private DbsField pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_State_Tax_Wthld_E;
    private DbsField pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Loc_Distr_E;
    private DbsField pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Loc_Tax_Wthld_E;
    private DbsField pnd_1099r_1099i_Control_Totals_E_Pnd_1099i_Int_Amt_E;

    private DbsGroup pnd_1099r_1099i_Control_Totals_E_No;
    private DbsField pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Gross_Amt_E_No;
    private DbsField pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Taxable_Amt_E_No;
    private DbsField pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099_Fed_Tax_Wthld_E_No;
    private DbsField pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Ivc_Amt_E_No;
    private DbsField pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Irr_Amt_E_No;
    private DbsField pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_State_Distr_E_No;
    private DbsField pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_State_Tax_Wthld_E_No;
    private DbsField pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Loc_Distr_E_No;
    private DbsField pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Loc_Tax_Wthld_E_No;
    private DbsField pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099i_Int_Amt_E_No;

    private DbsGroup pnd_1099_Grand_Totals;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Form_Cnt;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Hold_Cnt;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Empty_Form_Cnt;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Gross_Amt;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Taxable_Amt;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Fed_Tax_Wthld;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Ivc_Amt;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Irr_Amt;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_State_Distr;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_State_Tax_Wthld;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Loc_Distr;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Loc_Tax_Wthld;
    private DbsField pnd_1099_Grand_Totals_Pnd_Total_1099_Int_Amt;

    private DbsGroup pnd_1099_Grand_Totals_E;
    private DbsField pnd_1099_Grand_Totals_E_Pnd_Total_1099_Form_Cnt_E;
    private DbsField pnd_1099_Grand_Totals_E_Pnd_Total_1099_Hold_Cnt_E;
    private DbsField pnd_1099_Grand_Totals_E_Pnd_Total_1099_Empty_Form_Cnt_E;
    private DbsField pnd_1099_Grand_Totals_E_Pnd_Total_1099_Gross_Amt_E;
    private DbsField pnd_1099_Grand_Totals_E_Pnd_Total_1099_Taxable_Amt_E;
    private DbsField pnd_1099_Grand_Totals_E_Pnd_Total_1099_Fed_Tax_Wthld_E;
    private DbsField pnd_1099_Grand_Totals_E_Pnd_Total_1099_Ivc_Amt_E;
    private DbsField pnd_1099_Grand_Totals_E_Pnd_Total_1099_Irr_Amt_E;
    private DbsField pnd_1099_Grand_Totals_E_Pnd_Total_1099_State_Distr_E;
    private DbsField pnd_1099_Grand_Totals_E_Pnd_Total_1099_State_Tax_Wthld_E;
    private DbsField pnd_1099_Grand_Totals_E_Pnd_Total_1099_Loc_Distr_E;
    private DbsField pnd_1099_Grand_Totals_E_Pnd_Total_1099_Loc_Tax_Wthld_E;
    private DbsField pnd_1099_Grand_Totals_E_Pnd_Total_1099_Int_Amt_E;

    private DbsGroup pnd_1099_Grand_Totals_E_No;
    private DbsField pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Form_Cnt_E_No;
    private DbsField pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Hold_Cnt_E_No;
    private DbsField pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Empty_Form_Cnt_E_No;
    private DbsField pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Gross_Amt_E_No;
    private DbsField pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Taxable_Amt_E_No;
    private DbsField pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Fed_Tax_Wthld_E_No;
    private DbsField pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Ivc_Amt_E_No;
    private DbsField pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Irr_Amt_E_No;
    private DbsField pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_State_Distr_E_No;
    private DbsField pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_State_Tax_Wthld_E_No;
    private DbsField pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Loc_Distr_E_No;
    private DbsField pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Loc_Tax_Wthld_E_No;
    private DbsField pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Int_Amt_E_No;

    private DbsGroup pnd_1042s_Control_Totals;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income_Fatca;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt_Fatca;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt_Fatca;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld_Fatca;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt_Fatca;

    private DbsGroup pnd_1042s_Control_Totals_E;
    private DbsField pnd_1042s_Control_Totals_E_Pnd_1042s_Gross_Income_E;
    private DbsField pnd_1042s_Control_Totals_E_Pnd_1042s_Interest_Amt_E;
    private DbsField pnd_1042s_Control_Totals_E_Pnd_1042s_Pension_Amt_E;
    private DbsField pnd_1042s_Control_Totals_E_Pnd_1042s_Nra_Tax_Wthld_E;
    private DbsField pnd_1042s_Control_Totals_E_Pnd_1042s_Refund_Amt_E;
    private DbsField pnd_1042s_Control_Totals_E_Pnd_1042s_Gross_Income_E_Fatca;
    private DbsField pnd_1042s_Control_Totals_E_Pnd_1042s_Interest_Amt_E_Fatca;
    private DbsField pnd_1042s_Control_Totals_E_Pnd_1042s_Pension_Amt_E_Fatca;
    private DbsField pnd_1042s_Control_Totals_E_Pnd_1042s_Nra_Tax_Wthld_E_Fatca;
    private DbsField pnd_1042s_Control_Totals_E_Pnd_1042s_Refund_Amt_E_Fatca;

    private DbsGroup pnd_1042s_Control_Totals_E_No;
    private DbsField pnd_1042s_Control_Totals_E_No_Pnd_1042s_Gross_Income_E_No;
    private DbsField pnd_1042s_Control_Totals_E_No_Pnd_1042s_Interest_Amt_E_No;
    private DbsField pnd_1042s_Control_Totals_E_No_Pnd_1042s_Pension_Amt_E_No;
    private DbsField pnd_1042s_Control_Totals_E_No_Pnd_1042s_Nra_Tax_Wthld_E_No;
    private DbsField pnd_1042s_Control_Totals_E_No_Pnd_1042s_Refund_Amt_E_No;
    private DbsField pnd_1042s_Control_Totals_E_No_Pnd_1042s_Gross_Income_E_No_Fatca;
    private DbsField pnd_1042s_Control_Totals_E_No_Pnd_1042s_Interest_Amt_E_No_Fatca;
    private DbsField pnd_1042s_Control_Totals_E_No_Pnd_1042s_Pension_Amt_E_No_Fatca;
    private DbsField pnd_1042s_Control_Totals_E_No_Pnd_1042s_Nra_Tax_Wthld_E_No_Fatca;
    private DbsField pnd_1042s_Control_Totals_E_No_Pnd_1042s_Refund_Amt_E_No_Fatca;

    private DbsGroup pnd_5498_Control_Totals;
    private DbsField pnd_5498_Control_Totals_Pnd_5498_Form_Cnt;
    private DbsField pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Contrib;
    private DbsField pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Contrib;
    private DbsField pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rollover;
    private DbsField pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rechar;
    private DbsField pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Conversion;
    private DbsField pnd_5498_Control_Totals_Pnd_5498_Account_Fmv;
    private DbsField pnd_5498_Control_Totals_Pnd_5498_Sep_Amt;
    private DbsField pnd_5498_Control_Totals_Pnd_5498_Postpn_Amt;

    private DbsGroup pnd_5498_Control_Totals_E;
    private DbsField pnd_5498_Control_Totals_E_Pnd_5498_Form_Cnt_E;
    private DbsField pnd_5498_Control_Totals_E_Pnd_5498_Trad_Ira_Contrib_E;
    private DbsField pnd_5498_Control_Totals_E_Pnd_5498_Roth_Ira_Contrib_E;
    private DbsField pnd_5498_Control_Totals_E_Pnd_5498_Trad_Ira_Rollover_E;
    private DbsField pnd_5498_Control_Totals_E_Pnd_5498_Trad_Ira_Rechar_E;
    private DbsField pnd_5498_Control_Totals_E_Pnd_5498_Roth_Ira_Conversion_E;
    private DbsField pnd_5498_Control_Totals_E_Pnd_5498_Account_Fmv_E;
    private DbsField pnd_5498_Control_Totals_E_Pnd_5498_Sep_Amt_E;
    private DbsField pnd_5498_Control_Totals_E_Pnd_5498_Postpn_Amt_E;

    private DbsGroup pnd_5498_Control_Totals_E_No;
    private DbsField pnd_5498_Control_Totals_E_No_Pnd_5498_Form_Cnt_E_No;
    private DbsField pnd_5498_Control_Totals_E_No_Pnd_5498_Trad_Ira_Contrib_E_No;
    private DbsField pnd_5498_Control_Totals_E_No_Pnd_5498_Roth_Ira_Contrib_E_No;
    private DbsField pnd_5498_Control_Totals_E_No_Pnd_5498_Trad_Ira_Rollover_E_No;
    private DbsField pnd_5498_Control_Totals_E_No_Pnd_5498_Trad_Ira_Rechar_E_No;
    private DbsField pnd_5498_Control_Totals_E_No_Pnd_5498_Roth_Ira_Conversion_E_No;
    private DbsField pnd_5498_Control_Totals_E_No_Pnd_5498_Account_Fmv_E_No;
    private DbsField pnd_5498_Control_Totals_E_No_Pnd_5498_Sep_Amt_E_No;
    private DbsField pnd_5498_Control_Totals_E_No_Pnd_5498_Postpn_Amt_E_No;

    private DbsGroup pnd_5498_Control_Totals_Gr;
    private DbsField pnd_5498_Control_Totals_Gr_Pnd_5498_Form_Cnt_Gr;
    private DbsField pnd_5498_Control_Totals_Gr_Pnd_5498_Trad_Ira_Contrib_Gr;
    private DbsField pnd_5498_Control_Totals_Gr_Pnd_5498_Roth_Ira_Contrib_Gr;
    private DbsField pnd_5498_Control_Totals_Gr_Pnd_5498_Trad_Ira_Rollover_Gr;
    private DbsField pnd_5498_Control_Totals_Gr_Pnd_5498_Trad_Ira_Rechar_Gr;
    private DbsField pnd_5498_Control_Totals_Gr_Pnd_5498_Roth_Ira_Conversion_Gr;
    private DbsField pnd_5498_Control_Totals_Gr_Pnd_5498_Account_Fmv_Gr;
    private DbsField pnd_5498_Control_Totals_Gr_Pnd_5498_Sep_Amt_Gr;
    private DbsField pnd_5498_Control_Totals_Gr_Pnd_5498_Postpn_Amt_Gr;

    private DbsGroup pnd_5498_Control_Totals_E_Gr;
    private DbsField pnd_5498_Control_Totals_E_Gr_Pnd_5498_Form_Cnt_E_Gr;
    private DbsField pnd_5498_Control_Totals_E_Gr_Pnd_5498_Trad_Ira_Contrib_E_Gr;
    private DbsField pnd_5498_Control_Totals_E_Gr_Pnd_5498_Roth_Ira_Contrib_E_Gr;
    private DbsField pnd_5498_Control_Totals_E_Gr_Pnd_5498_Trad_Ira_Rollover_E_Gr;
    private DbsField pnd_5498_Control_Totals_E_Gr_Pnd_5498_Trad_Ira_Rechar_E_Gr;
    private DbsField pnd_5498_Control_Totals_E_Gr_Pnd_5498_Roth_Ira_Conversion_E_Gr;
    private DbsField pnd_5498_Control_Totals_E_Gr_Pnd_5498_Account_Fmv_E_Gr;
    private DbsField pnd_5498_Control_Totals_E_Gr_Pnd_5498_Sep_Amt_E_Gr;
    private DbsField pnd_5498_Control_Totals_E_Gr_Pnd_5498_Postpn_Amt_E_Gr;

    private DbsGroup pnd_5498_Control_Totals_E_No_Gr;
    private DbsField pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Form_Cnt_E_No_Gr;
    private DbsField pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Trad_Ira_Contrib_E_No_Gr;
    private DbsField pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Roth_Ira_Contrib_E_No_Gr;
    private DbsField pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Trad_Ira_Rollover_E_No_Gr;
    private DbsField pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Trad_Ira_Rechar_E_No_Gr;
    private DbsField pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Roth_Ira_Conversion_E_No_Gr;
    private DbsField pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Account_Fmv_E_No_Gr;
    private DbsField pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Sep_Amt_E_No_Gr;
    private DbsField pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Postpn_Amt_E_No_Gr;
    private DbsField pnd_Form_Cnt_Comp_5498;
    private DbsField pnd_Form_Cnt_Comp_E_5498;
    private DbsField pnd_Form_Cnt_Comp_E_No_5498;

    private DbsGroup pnd_5498_Control_Totals_P2;
    private DbsField pnd_5498_Control_Totals_P2_Pnd_5498_Part_Cnt_P2;
    private DbsField pnd_5498_Control_Totals_P2_Pnd_5498_Form_Cnt_P2;
    private DbsField pnd_5498_Control_Totals_P2_Pnd_5498_Trad_Ira_Contrib_P2;
    private DbsField pnd_5498_Control_Totals_P2_Pnd_5498_Roth_Ira_Contrib_P2;
    private DbsField pnd_5498_Control_Totals_P2_Pnd_5498_Trad_Ira_Rollover_P2;
    private DbsField pnd_5498_Control_Totals_P2_Pnd_5498_Trad_Ira_Rechar_P2;
    private DbsField pnd_5498_Control_Totals_P2_Pnd_5498_Roth_Ira_Conversion_P2;
    private DbsField pnd_5498_Control_Totals_P2_Pnd_5498_Account_Fmv_P2;
    private DbsField pnd_5498_Control_Totals_P2_Pnd_5498_Sep_Amt_P2;
    private DbsField pnd_5498_Control_Totals_P2_Pnd_5498_Postpn_Amt_P2;

    private DbsGroup pnd_5498_Control_Totals_E_P2;
    private DbsField pnd_5498_Control_Totals_E_P2_Pnd_5498_Part_Cnt_E_P2;
    private DbsField pnd_5498_Control_Totals_E_P2_Pnd_5498_Form_Cnt_E_P2;
    private DbsField pnd_5498_Control_Totals_E_P2_Pnd_5498_Trad_Ira_Contrib_E_P2;
    private DbsField pnd_5498_Control_Totals_E_P2_Pnd_5498_Roth_Ira_Contrib_E_P2;
    private DbsField pnd_5498_Control_Totals_E_P2_Pnd_5498_Trad_Ira_Rollover_E_P2;
    private DbsField pnd_5498_Control_Totals_E_P2_Pnd_5498_Trad_Ira_Rechar_E_P2;
    private DbsField pnd_5498_Control_Totals_E_P2_Pnd_5498_Roth_Ira_Conversion_E_P2;
    private DbsField pnd_5498_Control_Totals_E_P2_Pnd_5498_Account_Fmv_E_P2;
    private DbsField pnd_5498_Control_Totals_E_P2_Pnd_5498_Sep_Amt_E_P2;
    private DbsField pnd_5498_Control_Totals_E_P2_Pnd_5498_Postpn_Amt_E_P2;

    private DbsGroup pnd_5498_Control_Totals_E_N_P2;
    private DbsField pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Part_Cnt_E_N_P2;
    private DbsField pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Form_Cnt_E_N_P2;
    private DbsField pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Trad_Ira_Contrib_E_N_P2;
    private DbsField pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Roth_Ira_Contrib_E_N_P2;
    private DbsField pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Trad_Ira_Rollover_E_N_P2;
    private DbsField pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Trad_Ira_Rechar_E_N_P2;
    private DbsField pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Roth_Ira_Conversion_E_N_P2;
    private DbsField pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Account_Fmv_E_N_P2;
    private DbsField pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Sep_Amt_E_N_P2;
    private DbsField pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Postpn_Amt_E_N_P2;
    private DbsField pnd_M;

    private DbsGroup pnd_Suny_Control_Totals;
    private DbsField pnd_Suny_Control_Totals_Pnd_Letter_Count;
    private DbsField pnd_Suny_Control_Totals_Pnd_Suny_Contract_Ctr;
    private DbsField pnd_Suny_Control_Totals_Pnd_Suny_Gross_Amt;
    private DbsField pnd_Suny_Control_Totals_Pnd_Suny_Fed_Tax_Wthld;
    private DbsField pnd_Suny_Control_Totals_Pnd_Suny_State_Tax_Wthld;
    private DbsField pnd_Suny_Control_Totals_Pnd_Suny_Loc_Tax_Wthld;

    private DbsGroup pnd_Suny_Control_Totals_E;
    private DbsField pnd_Suny_Control_Totals_E_Pnd_Letter_Count_E;
    private DbsField pnd_Suny_Control_Totals_E_Pnd_Suny_Contract_Ctr_E;
    private DbsField pnd_Suny_Control_Totals_E_Pnd_Suny_Gross_Amt_E;
    private DbsField pnd_Suny_Control_Totals_E_Pnd_Suny_Fed_Tax_Wthld_E;
    private DbsField pnd_Suny_Control_Totals_E_Pnd_Suny_State_Tax_Wthld_E;
    private DbsField pnd_Suny_Control_Totals_E_Pnd_Suny_Loc_Tax_Wthld_E;

    private DbsGroup pnd_Suny_Control_Totals_E_No;
    private DbsField pnd_Suny_Control_Totals_E_No_Pnd_Letter_Count_E_No;
    private DbsField pnd_Suny_Control_Totals_E_No_Pnd_Suny_Contract_Ctr_E_No;
    private DbsField pnd_Suny_Control_Totals_E_No_Pnd_Suny_Gross_Amt_E_No;
    private DbsField pnd_Suny_Control_Totals_E_No_Pnd_Suny_Fed_Tax_Wthld_E_No;
    private DbsField pnd_Suny_Control_Totals_E_No_Pnd_Suny_State_Tax_Wthld_E_No;
    private DbsField pnd_Suny_Control_Totals_E_No_Pnd_Suny_Loc_Tax_Wthld_E_No;
    private DbsField pnd_5498_Type;

    private DbsGroup pnd_5498_Type__R_Field_7;
    private DbsField pnd_5498_Type_Pnd_5498_Type_N;
    private DbsField pnd_Ira_Start;

    private DbsGroup pnd_Ira_Start__R_Field_8;
    private DbsField pnd_Ira_Start_Twrc_Tax_Year;
    private DbsField pnd_Ira_Start_Twrc_Status;
    private DbsField pnd_Ira_Start_Twrc_Tax_Id;
    private DbsField pnd_Ira_Start_Twrc_Contract;
    private DbsField pnd_Ira_Start_Twrc_Payee;
    private DbsField pnd_Ira_End;

    private DbsGroup pnd_4807c_Control_Totals;
    private DbsField pnd_4807c_Control_Totals_Pnd_4807c_Form_Cnt;
    private DbsField pnd_4807c_Control_Totals_Pnd_4807c_Empty_Form_Cnt;
    private DbsField pnd_4807c_Control_Totals_Pnd_4807c_Hold_Cnt;
    private DbsField pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt;
    private DbsField pnd_4807c_Control_Totals_Pnd_4807c_Int_Amt;
    private DbsField pnd_4807c_Control_Totals_Pnd_4807c_Distrib_Amt;
    private DbsField pnd_4807c_Control_Totals_Pnd_4807c_Taxable_Amt;
    private DbsField pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt;
    private DbsField pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt_Roll;
    private DbsField pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt_Roll;

    private DbsGroup pnd_4807c_Control_Totals_E;
    private DbsField pnd_4807c_Control_Totals_E_Pnd_4807c_Form_Cnt_E;
    private DbsField pnd_4807c_Control_Totals_E_Pnd_4807c_Empty_Form_Cnt_E;
    private DbsField pnd_4807c_Control_Totals_E_Pnd_4807c_Hold_Cnt_E;
    private DbsField pnd_4807c_Control_Totals_E_Pnd_4807c_Gross_Amt_E;
    private DbsField pnd_4807c_Control_Totals_E_Pnd_4807c_Int_Amt_E;
    private DbsField pnd_4807c_Control_Totals_E_Pnd_4807c_Distrib_Amt_E;
    private DbsField pnd_4807c_Control_Totals_E_Pnd_4807c_Taxable_Amt_E;
    private DbsField pnd_4807c_Control_Totals_E_Pnd_4807c_Ivc_Amt_E;
    private DbsField pnd_4807c_Control_Totals_E_Pnd_4807c_Gross_Amt_Roll_E;
    private DbsField pnd_4807c_Control_Totals_E_Pnd_4807c_Ivc_Amt_Roll_E;

    private DbsGroup pnd_4807c_Control_Totals_E_No;
    private DbsField pnd_4807c_Control_Totals_E_No_Pnd_4807c_Form_Cnt_E_No;
    private DbsField pnd_4807c_Control_Totals_E_No_Pnd_4807c_Empty_Form_Cnt_E_No;
    private DbsField pnd_4807c_Control_Totals_E_No_Pnd_4807c_Hold_Cnt_E_No;
    private DbsField pnd_4807c_Control_Totals_E_No_Pnd_4807c_Gross_Amt_E_No;
    private DbsField pnd_4807c_Control_Totals_E_No_Pnd_4807c_Int_Amt_E_No;
    private DbsField pnd_4807c_Control_Totals_E_No_Pnd_4807c_Distrib_Amt_E_No;
    private DbsField pnd_4807c_Control_Totals_E_No_Pnd_4807c_Taxable_Amt_E_No;
    private DbsField pnd_4807c_Control_Totals_E_No_Pnd_4807c_Ivc_Amt_E_No;
    private DbsField pnd_4807c_Control_Totals_E_No_Pnd_4807c_Gross_Amt_Roll_E_No;
    private DbsField pnd_4807c_Control_Totals_E_No_Pnd_4807c_Ivc_Amt_Roll_E_No;

    private DbsGroup pnd_4807c_Control_Totals_Gr;
    private DbsField pnd_4807c_Control_Totals_Gr_Pnd_4807c_Form_Cnt_Gr;
    private DbsField pnd_4807c_Control_Totals_Gr_Pnd_4807c_Empty_Form_Cnt_Gr;
    private DbsField pnd_4807c_Control_Totals_Gr_Pnd_4807c_Hold_Cnt_Gr;
    private DbsField pnd_4807c_Control_Totals_Gr_Pnd_4807c_Gross_Amt_Gr;
    private DbsField pnd_4807c_Control_Totals_Gr_Pnd_4807c_Int_Amt_Gr;
    private DbsField pnd_4807c_Control_Totals_Gr_Pnd_4807c_Distrib_Amt_Gr;
    private DbsField pnd_4807c_Control_Totals_Gr_Pnd_4807c_Taxable_Amt_Gr;
    private DbsField pnd_4807c_Control_Totals_Gr_Pnd_4807c_Ivc_Amt_Gr;
    private DbsField pnd_4807c_Control_Totals_Gr_Pnd_4807c_Gross_Amt_Roll_Gr;
    private DbsField pnd_4807c_Control_Totals_Gr_Pnd_4807c_Ivc_Amt_Roll_Gr;

    private DbsGroup pnd_4807c_Control_Totals_E_Gr;
    private DbsField pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Form_Cnt_E_Gr;
    private DbsField pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Empty_Form_Cnt_E_Gr;
    private DbsField pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Hold_Cnt_E_Gr;
    private DbsField pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Gross_Amt_E_Gr;
    private DbsField pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Int_Amt_E_Gr;
    private DbsField pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Distrib_Amt_E_Gr;
    private DbsField pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Taxable_Amt_E_Gr;
    private DbsField pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Ivc_Amt_E_Gr;
    private DbsField pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Gross_Amt_Roll_E_Gr;
    private DbsField pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Ivc_Amt_Roll_E_Gr;

    private DbsGroup pnd_4807c_Control_Totals_E_No_Gr;
    private DbsField pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Form_Cnt_E_No_Gr;
    private DbsField pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Empty_Form_Cnt_E_No_Gr;
    private DbsField pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Hold_Cnt_E_No_Gr;
    private DbsField pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Gross_Amt_E_No_Gr;
    private DbsField pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Int_Amt_E_No_Gr;
    private DbsField pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Distrib_Amt_E_No_Gr;
    private DbsField pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Taxable_Amt_E_No_Gr;
    private DbsField pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Ivc_Amt_E_No_Gr;
    private DbsField pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Gross_Amt_Roll_E_No_Gr;
    private DbsField pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Ivc_Amt_Roll_E_No_Gr;

    private DbsGroup pnd_Nr4_Control_Totals;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt_E;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt_E_No;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Hold_Cnt;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Income_Code;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Income_Code_E;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Income_Code_E_No;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt_E;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt_E_No;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt_E;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt_E_No;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld_E;
    private DbsField pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld_E_No;

    private DbsGroup pnd_Nr4_Control_Totals_Gr;
    private DbsField pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Form_Cnt_Gr;
    private DbsField pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Form_Cnt_E_Gr;
    private DbsField pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Form_Cnt_E_No_Gr;
    private DbsField pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Hold_Cnt_Gr;
    private DbsField pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Income_Code_Gr;
    private DbsField pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Income_Code_E_Gr;
    private DbsField pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Income_Code_E_No_Gr;
    private DbsField pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Gross_Amt_Gr;
    private DbsField pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Gross_Amt_E_Gr;
    private DbsField pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Gross_Amt_E_No_Gr;
    private DbsField pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Interest_Amt_Gr;
    private DbsField pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Interest_Amt_E_Gr;
    private DbsField pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Interest_Amt_E_No_Gr;
    private DbsField pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Fed_Tax_Wthld_Gr;
    private DbsField pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Fed_Tax_Wthld_E_Gr;
    private DbsField pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Fed_Tax_Wthld_E_No_Gr;
    private DbsField pnd_Form_Cnt;
    private DbsField pnd_Form_Cnt_E;
    private DbsField pnd_Form_Cnt_E_No;
    private DbsField pnd_Form_Cnt_Fatca;
    private DbsField pnd_Form_Cnt_E_Fatca;
    private DbsField pnd_Form_Cnt_E_No_Fatca;
    private DbsField pnd_Hold_Cnt;
    private DbsField pnd_Empty_Form_Cnt;
    private DbsField pnd_Tirf_Superde_3_S;
    private DbsField pnd_Tirf_Superde_3_E;
    private DbsField pnd_S3;

    private DbsGroup pnd_S3__R_Field_9;
    private DbsField pnd_S3_Pnd_Tax_Year;
    private DbsField pnd_S3_Pnd_Status;
    private DbsField pnd_S3_Pnd_Form;
    private DbsField low_Values;
    private DbsField high_Values;
    private DbsField pnd_K;
    private DbsField pnd_I;
    private DbsField pnd_Company_Ndx;
    private DbsField pnd_Err_Ndx;
    private DbsField pnd_Nr4_Ndx;
    private DbsField pnd_Sys_Err_Ndx;
    private DbsField pnd_Reportable_Ndx;
    private DbsField pnd_State_Ndx;

    private DbsGroup pnd_Summary_Amounts;
    private DbsField pnd_Summary_Amounts_Pnd_Summ_State_Form_Cnt;
    private DbsField pnd_Summary_Amounts_Pnd_Summ_State_Distr_Amt;
    private DbsField pnd_Summary_Amounts_Pnd_Summ_State_Wthhld_Amt;
    private DbsField pnd_Summary_Amounts_Pnd_Summ_Local_Distr_Amt;
    private DbsField pnd_Summary_Amounts_Pnd_Summ_Local_Wthhld_Amt;
    private DbsField pnd_Summary_Amounts_Pnd_Summ_Res_Gross_Amt;
    private DbsField pnd_Summary_Amounts_Pnd_Summ_Res_Taxable_Amt;
    private DbsField pnd_Summary_Amounts_Pnd_Summ_Res_Ivc_Amt;
    private DbsField pnd_Summary_Amounts_Pnd_Summ_Res_Fed_Tax_Wthld;
    private DbsField pnd_Summary_Amounts_Pnd_Summ_Res_Irr_Amt;
    private DbsField pnd_Sys_Err_Cnt;
    private DbsField pnd_Form_Key;

    private DbsGroup pnd_Form_Key__R_Field_10;
    private DbsField pnd_Form_Key_Pnd_Tax_Year;
    private DbsField pnd_Form_Key_Pnd_Status;
    private DbsField pnd_Hist_Key;

    private DbsGroup pnd_Hist_Key__R_Field_11;
    private DbsField pnd_Hist_Key_Pnd_Tax_Year;
    private DbsField pnd_Hist_Key_Pnd_Tin;
    private DbsField pnd_Hist_Key_Pnd_Form_Type;
    private DbsField pnd_Hist_Key_Pnd_Contract_Nbr;
    private DbsField pnd_Hist_Key_Pnd_Payee;
    private DbsField pnd_Hist_Key_Pnd_Key;
    private DbsField pnd_Hist_Key_Pnd_Ts;
    private DbsField pnd_Hist_Key_Pnd_Irs_Rpt_Ind;
    private DbsField pnd_Sys_Date;
    private DbsField pnd_Sys_Date_N;
    private DbsField pnd_Sys_Time;
    private DbsField pnd_Err_Msg;
    private DbsField pnd_Read_Cnt_In;
    private DbsField pnd_Error_Cnt;
    private DbsField pnd_Et_Cnt;
    private DbsField pnd_1042s_Max;
    private DbsField pnd_Old_Form_Type;
    private DbsField pnd_Old_Company_Cde;
    private DbsField pnd_C_1099_R;
    private DbsField pnd_C_1042_S;
    private DbsField pnd_Nr4_Income_Desc;
    private DbsField pnd_State_Ind_N;
    private DbsField pnd_Reported_To_Irs;
    private DbsField pnd_E_Deliver;
    private DbsField pnd_Customer_Id;

    private DbsGroup pnd_Customer_Id__R_Field_12;
    private DbsField pnd_Customer_Id_Pnd_Pin;
    private DbsField pnd_Form_Cnt_Comp;
    private DbsField pnd_Form_Cnt_Comp_E;
    private DbsField pnd_Form_Cnt_Comp_E_No;
    private DbsField pnd_Form_Cnt_Comp_1099;
    private DbsField pnd_Form_Cnt_Comp_1099r;
    private DbsField pnd_Form_Cnt_Comp_1099int;
    private DbsField pnd_Form_Cnt_Comp_E_1099;
    private DbsField pnd_Form_Cnt_Comp_E_No_1099;
    private DbsField pnd_Form_Cnt_Comp_1042s;
    private DbsField pnd_Form_Cnt_Comp_E_1042s;
    private DbsField pnd_Form_Cnt_Comp_E_No_1042s;
    private DbsField pnd_Form_Cnt_Comp_1042s_Fatca;
    private DbsField pnd_Form_Cnt_Comp_E_1042s_Fatca;
    private DbsField pnd_Form_Cnt_Comp_E_No_1042s_Fatca;
    private DbsField pnd_Form_Cnt_Comp_Grand;
    private DbsField pnd_Form_Cnt_Comp_E_Grand;
    private DbsField pnd_Form_Cnt_Comp_E_No_Grand;
    private DbsField pnd_1099r_Loop;
    private DbsField pnd_Print_1099_Loop;
    private DbsField pnd_Form_Cnt_All_1099r;
    private DbsField pnd_1099r_Gross_Amt_All_1099r;
    private DbsField pnd_1099r_Ivc_Amt_All_1099r;
    private DbsField pnd_1099r_Taxable_Amt_All_1099r;
    private DbsField pnd_1099r_State_Tax_Wthld_All_1099r;
    private DbsField pnd_1099i_Int_Amt_All_1099r;
    private DbsField pnd_1099r_Loc_Tax_Wthld_All_1099r;
    private DbsField pnd_1099_Fed_Tax_Wthld_All_1099r;
    private DbsField pnd_1099r_Irr_Amt_All_1099r;
    private DbsField pnd_Form_Cnt_All_1099int;
    private DbsField pnd_1099r_Gross_Amt_All_1099int;
    private DbsField pnd_1099r_Ivc_Amt_All_1099int;
    private DbsField pnd_1099r_Taxable_Amt_All_1099int;
    private DbsField pnd_1099r_State_Tax_Wthld_All_1099int;
    private DbsField pnd_1099i_Int_Amt_All_1099int;
    private DbsField pnd_1099r_Loc_Tax_Wthld_All_1099int;
    private DbsField pnd_1099_Fed_Tax_Wthld_All_1099int;
    private DbsField pnd_1099r_Irr_Amt_All_1099int;
    private DbsField pnd_Form_Cnt_All_1099tot;
    private DbsField pnd_1099r_Gross_Amt_All_1099tot;
    private DbsField pnd_1099r_Ivc_Amt_All_1099tot;
    private DbsField pnd_1099r_Taxable_Amt_All_1099tot;
    private DbsField pnd_1099r_State_Tax_Wthld_All_1099tot;
    private DbsField pnd_1099i_Int_Amt_All_1099tot;
    private DbsField pnd_1099r_Loc_Tax_Wthld_All_1099tot;
    private DbsField pnd_1099_Fed_Tax_Wthld_All_1099tot;
    private DbsField pnd_1099r_Irr_Amt_All_1099tot;
    private DbsField pnd_Form_Cnt_E_1099r;
    private DbsField pnd_1099r_Gross_Amt_E_1099r;
    private DbsField pnd_1099r_Ivc_Amt_E_1099r;
    private DbsField pnd_1099r_Taxable_Amt_E_1099r;
    private DbsField pnd_1099i_Int_Amt_E_1099r;
    private DbsField pnd_1099_Fed_Tax_Wthld_E_1099r;
    private DbsField pnd_1099r_State_Tax_Wthld_E_1099r;
    private DbsField pnd_1099r_Loc_Tax_Wthld_E_1099r;
    private DbsField pnd_1099r_Irr_Amt_E_1099r;
    private DbsField pnd_Form_Cnt_E_No_1099r;
    private DbsField pnd_1099r_Gross_Amt_E_No_1099r;
    private DbsField pnd_1099r_Ivc_Amt_E_No_1099r;
    private DbsField pnd_1099r_Taxable_Amt_E_No_1099r;
    private DbsField pnd_1099i_Int_Amt_E_No_1099r;
    private DbsField pnd_1099_Fed_Tax_Wthld_E_No_1099r;
    private DbsField pnd_1099r_State_Tax_Wthld_E_No_109r;
    private DbsField pnd_1099r_Loc_Tax_Wthld_E_No_1099r;
    private DbsField pnd_1099r_Irr_Amt_E_No_1099r;
    private DbsField pnd_Form_Cnt_E_1099i;
    private DbsField pnd_1099r_Gross_Amt_E_1099i;
    private DbsField pnd_1099r_Ivc_Amt_E_1099i;
    private DbsField pnd_1099r_Taxable_Amt_E_1099i;
    private DbsField pnd_1099i_Int_Amt_E_1099i;
    private DbsField pnd_1099_Fed_Tax_Wthld_E_1099i;
    private DbsField pnd_1099r_State_Tax_Wthld_E_1099i;
    private DbsField pnd_1099r_Loc_Tax_Wthld_E_1099i;
    private DbsField pnd_1099r_Irr_Amt_E_1099i;
    private DbsField pnd_Form_Cnt_E_No_1099i;
    private DbsField pnd_1099r_Gross_Amt_E_No_1099i;
    private DbsField pnd_1099r_Ivc_Amt_E_No_1099i;
    private DbsField pnd_1099r_Taxable_Amt_E_No_1099i;
    private DbsField pnd_1099i_Int_Amt_E_No_1099i;
    private DbsField pnd_1099_Fed_Tax_Wthld_E_No_1099i;
    private DbsField pnd_1099r_State_Tax_Wthld_E_No_109i;
    private DbsField pnd_1099r_Loc_Tax_Wthld_E_No_1099i;
    private DbsField pnd_1099r_Irr_Amt_E_No_1099i;
    private DbsField pnd_Total_1099_Form_Cnt_1099r;
    private DbsField pnd_Total_1099_Gross_Amt_1099r;
    private DbsField pnd_Total_1099_Taxable_Amt_1099r;
    private DbsField pnd_Total_1099_Fed_Tax_Wthld_1099r;
    private DbsField pnd_Total_1099_Ivc_Amt_1099r;
    private DbsField pnd_Total_1099_State_Tax_Wthld_1099r;
    private DbsField pnd_Total_1099_Loc_Tax_Wthld_1099r;
    private DbsField pnd_Total_1099_Int_Amt_1099r;
    private DbsField pnd_Total_1099_Irr_Amt_1099r;
    private DbsField pnd_Total_1099_Form_Cnt_E_Gr;
    private DbsField pnd_Total_1099_Gross_Amt_E_Gr;
    private DbsField pnd_Total_1099_Ivc_Amt_E_Gr;
    private DbsField pnd_Total_1099_Taxable_Amt_E_Gr;
    private DbsField pnd_Total_1099_Int_Amt_E_Gr;
    private DbsField pnd_Total_1099_Fed_Tax_Wthld_E_Gr;
    private DbsField pnd_Total_1099_State_Tax_Wthld_E_Gr;
    private DbsField pnd_Total_1099_Loc_Tax_Wthld_E_Gr;
    private DbsField pnd_Total_1099_Irr_Amt_E_Gr;
    private DbsField pnd_Total_1099_Form_Cnt_E_No_Gr;
    private DbsField pnd_Total_1099_Gross_Amt_E_No_Gr;
    private DbsField pnd_Total_1099_Ivc_Amt_E_No_Gr;
    private DbsField pnd_Total_1099_Taxable_Amt_E_No_Gr;
    private DbsField pnd_Total_1099_Int_Amt_E_No_Gr;
    private DbsField pnd_Total_1099_Fed_Tax_Wthld_E_No_Gr;
    private DbsField pnd_Total_1099_State_Tax_E_No_Gr;
    private DbsField pnd_Total_1099_Loc_Tax_E_No_Gr;
    private DbsField pnd_Total_1099_Irr_Amt_E_No_Gr;
    private DbsField pnd_Form_Cnt_1042s_Gr;
    private DbsField pnd_1042s_Gross_Income_1042s_Gr;
    private DbsField pnd_1042s_Pension_Amt_1042s_Gr;
    private DbsField pnd_1042s_Interest_Amt_1042s_Gr;
    private DbsField pnd_1042s_Nra_Tax_Wthld_1042s_Gr;
    private DbsField pnd_1042s_Refund_Amt_1042s_Gr;
    private DbsField pnd_Form_Cnt_E_1042s_Gr;
    private DbsField pnd_1042s_Gross_Income_E_1042s_Gr;
    private DbsField pnd_1042s_Pension_Amt_E_1042s_Gr;
    private DbsField pnd_1042s_Interest_Amt_E_1042s_Gr;
    private DbsField pnd_1042s_Nra_Tax_Wthld_E_1042s_Gr;
    private DbsField pnd_1042s_Refund_Amt_E_1042s_Gr;
    private DbsField pnd_Form_Cnt_E_No_1042s_Gr;
    private DbsField pnd_1042s_Gross_Income_E_No_1042s_Gr;
    private DbsField pnd_1042s_Pension_Amt_E_No_1042s_Gr;
    private DbsField pnd_1042s_Interest_Amt_E_No_1042s_Gr;
    private DbsField pnd_1042s_Nra_Tax_Wthld_E_No_1042s_Gr;
    private DbsField pnd_1042s_Refund_Amt_E_No_1042s_Gr;
    private DbsField pnd_Form_Cnt_1042s_Gr_Fatca;
    private DbsField pnd_1042s_Gross_Income_1042s_Gr_Fatca;
    private DbsField pnd_1042s_Pension_Amt_1042s_Gr_Fatca;
    private DbsField pnd_1042s_Interest_Amt_1042s_Gr_Fatca;
    private DbsField pnd_1042s_Nra_Tax_Wthld_1042s_Gr_Fatca;
    private DbsField pnd_1042s_Refund_Amt_1042s_Gr_Fatca;
    private DbsField pnd_Form_Cnt_E_1042s_Gr_Fatca;
    private DbsField pnd_1042s_Gross_Income_E_1042s_Gr_Fatca;
    private DbsField pnd_1042s_Pension_Amt_E_1042s_Gr_Fatca;
    private DbsField pnd_1042s_Interest_Amt_E_1042s_Gr_Fatca;
    private DbsField pnd_1042s_Nra_Tax_Wthld_E_1042s_Gr_Fatca;
    private DbsField pnd_1042s_Refund_Amt_E_1042s_Gr_Fatca;
    private DbsField pnd_Form_Cnt_E_No_1042s_Gr_Fatca;
    private DbsField pnd_1042s_Gross_Income_E_No_Gr_Fatca;
    private DbsField pnd_1042s_Pension_Amt_E_No_Gr_Fatca;
    private DbsField pnd_1042s_Interest_Amt_E_No_Gr_Fatca;
    private DbsField pnd_1042s_Nra_Tax_Wthld_E_No_Gr_Fatca;
    private DbsField pnd_1042s_Refund_Amt_E_No_Gr_Fatca;
    private DbsField pnd_Fatca_Sw;
    private DbsField pnd_F_Ndx;

    private DbsRecord internalLoopRecord;
    private DbsField rEAD_FORMTirf_Tax_YearOld;
    private DbsField rEAD_FORMTirf_Company_CdeOld;
    private DbsField rEAD_FORMTirf_Form_TypeOld;
    private DbsField rD1Tirf_Superde_6Old;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrlcomp = new LdaTwrlcomp();
        registerRecord(ldaTwrlcomp);
        localVariables = new DbsRecord();
        pdaTwrafrmn = new PdaTwrafrmn(localVariables);
        ldaTwrl5001 = new LdaTwrl5001();
        registerRecord(ldaTwrl5001);
        ldaTwrl5951 = new LdaTwrl5951();
        registerRecord(ldaTwrl5951);
        ldaTwrl9710 = new LdaTwrl9710();
        registerRecord(ldaTwrl9710);
        registerRecord(ldaTwrl9710.getVw_form());
        ldaTwrl9610 = new LdaTwrl9610();
        registerRecord(ldaTwrl9610);
        registerRecord(ldaTwrl9610.getVw_ira());

        // Local Variables

        vw_hist_Form = new DataAccessProgramView(new NameInfo("vw_hist_Form", "HIST-FORM"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        hist_Form_Tirf_Superde_4 = vw_hist_Form.getRecord().newFieldInGroup("hist_Form_Tirf_Superde_4", "TIRF-SUPERDE-4", FieldType.BINARY, 39, RepeatingFieldStrategy.None, 
            "TIRF_SUPERDE_4");
        hist_Form_Tirf_Superde_4.setSuperDescriptor(true);

        hist_Form__R_Field_1 = vw_hist_Form.getRecord().newGroupInGroup("hist_Form__R_Field_1", "REDEFINE", hist_Form_Tirf_Superde_4);
        hist_Form_Pnd_Tirf_Tax_Year = hist_Form__R_Field_1.newFieldInGroup("hist_Form_Pnd_Tirf_Tax_Year", "#TIRF-TAX-YEAR", FieldType.STRING, 4);
        hist_Form_Pnd_Tirf_Tin = hist_Form__R_Field_1.newFieldInGroup("hist_Form_Pnd_Tirf_Tin", "#TIRF-TIN", FieldType.STRING, 10);
        hist_Form_Pnd_Tirf_Form_Type = hist_Form__R_Field_1.newFieldInGroup("hist_Form_Pnd_Tirf_Form_Type", "#TIRF-FORM-TYPE", FieldType.NUMERIC, 2);
        hist_Form_Pnd_Tirf_Contract_Nbr = hist_Form__R_Field_1.newFieldInGroup("hist_Form_Pnd_Tirf_Contract_Nbr", "#TIRF-CONTRACT-NBR", FieldType.STRING, 
            8);
        hist_Form_Pnd_Tirf_Payee_Cde = hist_Form__R_Field_1.newFieldInGroup("hist_Form_Pnd_Tirf_Payee_Cde", "#TIRF-PAYEE-CDE", FieldType.STRING, 2);
        hist_Form_Pnd_Tirf_Key = hist_Form__R_Field_1.newFieldInGroup("hist_Form_Pnd_Tirf_Key", "#TIRF-KEY", FieldType.STRING, 5);
        hist_Form_Pnd_Tirf_Invrse_Create_Ts = hist_Form__R_Field_1.newFieldInGroup("hist_Form_Pnd_Tirf_Invrse_Create_Ts", "#TIRF-INVRSE-CREATE-TS", FieldType.TIME);
        hist_Form_Pnd_Tirf_Irs_Rpt_Ind = hist_Form__R_Field_1.newFieldInGroup("hist_Form_Pnd_Tirf_Irs_Rpt_Ind", "#TIRF-IRS-RPT-IND", FieldType.STRING, 
            1);
        registerRecord(vw_hist_Form);

        vw_form_1099_R = new DataAccessProgramView(new NameInfo("vw_form_1099_R", "FORM-1099-R"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE", DdmPeriodicGroups.getInstance().getGroups("TWRFRM_FORM_FILE"));
        form_1099_R_Tirf_Tax_Year = vw_form_1099_R.getRecord().newFieldInGroup("form_1099_R_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_TAX_YEAR");
        form_1099_R_Tirf_Tax_Year.setDdmHeader("TAX/YEAR");
        form_1099_R_Tirf_Form_Type = vw_form_1099_R.getRecord().newFieldInGroup("form_1099_R_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "TIRF_FORM_TYPE");
        form_1099_R_Tirf_Form_Type.setDdmHeader("FORM");
        form_1099_R_Tirf_Company_Cde = vw_form_1099_R.getRecord().newFieldInGroup("form_1099_R_Tirf_Company_Cde", "TIRF-COMPANY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRF_COMPANY_CDE");
        form_1099_R_Tirf_Company_Cde.setDdmHeader("COM-/PANY/CODE");
        form_1099_R_Tirf_Tax_Id_Type = vw_form_1099_R.getRecord().newFieldInGroup("form_1099_R_Tirf_Tax_Id_Type", "TIRF-TAX-ID-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRF_TAX_ID_TYPE");
        form_1099_R_Tirf_Tin = vw_form_1099_R.getRecord().newFieldInGroup("form_1099_R_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TIRF_TIN");
        form_1099_R_Tirf_Tin.setDdmHeader("TAX/ID/NUMBER");
        form_1099_R_Tirf_Contract_Nbr = vw_form_1099_R.getRecord().newFieldInGroup("form_1099_R_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TIRF_CONTRACT_NBR");
        form_1099_R_Tirf_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        form_1099_R_Tirf_Payee_Cde = vw_form_1099_R.getRecord().newFieldInGroup("form_1099_R_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_PAYEE_CDE");
        form_1099_R_Tirf_Payee_Cde.setDdmHeader("PAYEE/CODE");
        form_1099_R_Tirf_Key = vw_form_1099_R.getRecord().newFieldInGroup("form_1099_R_Tirf_Key", "TIRF-KEY", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "TIRF_KEY");
        form_1099_R_Tirf_Key.setDdmHeader("KEY");
        form_1099_R_Count_Casttirf_1099_R_State_Grp = vw_form_1099_R.getRecord().newFieldInGroup("form_1099_R_Count_Casttirf_1099_R_State_Grp", "C*TIRF-1099-R-STATE-GRP", 
            RepeatingFieldStrategy.CAsteriskVariable, "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");

        form_1099_R_Tirf_1099_R_State_Grp = vw_form_1099_R.getRecord().newGroupInGroup("form_1099_R_Tirf_1099_R_State_Grp", "TIRF-1099-R-STATE-GRP", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_1099_R_State_Grp.setDdmHeader("1099-R/STATE/GROUP");
        form_1099_R_Tirf_State_Rpt_Ind = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_State_Rpt_Ind", "TIRF-STATE-RPT-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_RPT_IND", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_State_Rpt_Ind.setDdmHeader("STATE/RPT/IND");
        form_1099_R_Tirf_State_Hardcopy_Ind = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_State_Hardcopy_Ind", "TIRF-STATE-HARDCOPY-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_HARDCOPY_IND", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_State_Hardcopy_Ind.setDdmHeader("HARD-/COPY/IND");
        form_1099_R_Tirf_State_Irs_Rpt_Ind = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_State_Irs_Rpt_Ind", "TIRF-STATE-IRS-RPT-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_IRS_RPT_IND", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_State_Irs_Rpt_Ind.setDdmHeader("IRS/RPT/IND");
        form_1099_R_Tirf_State_Code = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_State_Code", "TIRF-STATE-CODE", FieldType.STRING, 
            2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_CODE", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_State_Code.setDdmHeader("RESI-/DENCY/CODE");
        form_1099_R_Tirf_State_Distr = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_State_Distr", "TIRF-STATE-DISTR", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_DISTR", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_State_Distr.setDdmHeader("STATE/DISTRI-/BUTION");
        form_1099_R_Tirf_State_Tax_Wthld = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_State_Tax_Wthld", "TIRF-STATE-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_State_Tax_Wthld.setDdmHeader("STATE/TAX/WITHHELD");
        form_1099_R_Tirf_Loc_Code = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_Loc_Code", "TIRF-LOC-CODE", FieldType.STRING, 
            5, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LOC_CODE", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_Loc_Code.setDdmHeader("LOCA-/LITY/CODE");
        form_1099_R_Tirf_Loc_Distr = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_Loc_Distr", "TIRF-LOC-DISTR", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LOC_DISTR", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_Loc_Distr.setDdmHeader("LOCALITY/DISTRI-/BUTION");
        form_1099_R_Tirf_Loc_Tax_Wthld = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_Loc_Tax_Wthld", "TIRF-LOC-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LOC_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_Loc_Tax_Wthld.setDdmHeader("LOCAL/TAX/WITHHELD");
        form_1099_R_Tirf_State_Auth_Rpt_Ind = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_State_Auth_Rpt_Ind", "TIRF-STATE-AUTH-RPT-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_AUTH_RPT_IND", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_State_Auth_Rpt_Ind.setDdmHeader("STATE/RPT/IND");
        form_1099_R_Tirf_State_Auth_Rpt_Date = form_1099_R_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_1099_R_Tirf_State_Auth_Rpt_Date", "TIRF-STATE-AUTH-RPT-DATE", 
            FieldType.DATE, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_AUTH_RPT_DATE", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_1099_R_Tirf_State_Auth_Rpt_Date.setDdmHeader("STATE/AUTH RPT/DATE");
        registerRecord(vw_form_1099_R);

        vw_form_X = new DataAccessProgramView(new NameInfo("vw_form_X", "FORM-X"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form_X_Tirf_Tax_Year = vw_form_X.getRecord().newFieldInGroup("form_X_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_TAX_YEAR");
        form_X_Tirf_Tax_Year.setDdmHeader("TAX/YEAR");
        form_X_Tirf_Form_Type = vw_form_X.getRecord().newFieldInGroup("form_X_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TIRF_FORM_TYPE");
        form_X_Tirf_Form_Type.setDdmHeader("FORM");
        form_X_Tirf_Company_Cde = vw_form_X.getRecord().newFieldInGroup("form_X_Tirf_Company_Cde", "TIRF-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_COMPANY_CDE");
        form_X_Tirf_Company_Cde.setDdmHeader("COM-/PANY/CODE");
        form_X_Tirf_Tin = vw_form_X.getRecord().newFieldInGroup("form_X_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIRF_TIN");
        form_X_Tirf_Tin.setDdmHeader("TAX/ID/NUMBER");
        form_X_Tirf_Contract_Nbr = vw_form_X.getRecord().newFieldInGroup("form_X_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_CONTRACT_NBR");
        form_X_Tirf_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        form_X_Tirf_Payee_Cde = vw_form_X.getRecord().newFieldInGroup("form_X_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_PAYEE_CDE");
        form_X_Tirf_Payee_Cde.setDdmHeader("PAYEE/CODE");
        form_X_Tirf_Key = vw_form_X.getRecord().newFieldInGroup("form_X_Tirf_Key", "TIRF-KEY", FieldType.STRING, 5, RepeatingFieldStrategy.None, "TIRF_KEY");
        form_X_Tirf_Key.setDdmHeader("KEY");
        form_X_Tirf_Active_Ind = vw_form_X.getRecord().newFieldInGroup("form_X_Tirf_Active_Ind", "TIRF-ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_ACTIVE_IND");
        form_X_Tirf_Active_Ind.setDdmHeader("ACT/IND");
        form_X_Tirf_Srce_Cde = vw_form_X.getRecord().newFieldInGroup("form_X_Tirf_Srce_Cde", "TIRF-SRCE-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_SRCE_CDE");
        form_X_Tirf_Srce_Cde.setDdmHeader("SOURCE/CODE");
        form_X_Tirf_Superde_6 = vw_form_X.getRecord().newFieldInGroup("form_X_Tirf_Superde_6", "TIRF-SUPERDE-6", FieldType.STRING, 33, RepeatingFieldStrategy.None, 
            "TIRF_SUPERDE_6");
        form_X_Tirf_Superde_6.setSuperDescriptor(true);
        registerRecord(vw_form_X);

        vw_form_R = new DataAccessProgramView(new NameInfo("vw_form_R", "FORM-R"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form_R_Tirf_Tax_Year = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_TAX_YEAR");
        form_R_Tirf_Tax_Year.setDdmHeader("TAX/YEAR");
        form_R_Tirf_Form_Type = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TIRF_FORM_TYPE");
        form_R_Tirf_Form_Type.setDdmHeader("FORM");
        form_R_Tirf_Company_Cde = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Company_Cde", "TIRF-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_COMPANY_CDE");
        form_R_Tirf_Company_Cde.setDdmHeader("COM-/PANY/CODE");
        form_R_Tirf_Tin = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIRF_TIN");
        form_R_Tirf_Tin.setDdmHeader("TAX/ID/NUMBER");
        form_R_Tirf_Contract_Nbr = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_CONTRACT_NBR");
        form_R_Tirf_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        form_R_Tirf_Payee_Cde = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_PAYEE_CDE");
        form_R_Tirf_Payee_Cde.setDdmHeader("PAYEE/CODE");
        form_R_Tirf_Srce_Cde = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Srce_Cde", "TIRF-SRCE-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_SRCE_CDE");
        form_R_Tirf_Srce_Cde.setDdmHeader("SOURCE/CODE");
        form_R_Tirf_Ira_Acct_Type = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Ira_Acct_Type", "TIRF-IRA-ACCT-TYPE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_IRA_ACCT_TYPE");
        form_R_Tirf_Ira_Acct_Type.setDdmHeader("IRA/ACCOUNT/TYPE");
        form_R_Tirf_Deceased_Ind = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Deceased_Ind", "TIRF-DECEASED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_DECEASED_IND");
        form_R_Tirf_Deceased_Ind.setDdmHeader("DECE-/ASED/IND");
        form_R_Tirf_Contract_Xref = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Contract_Xref", "TIRF-CONTRACT-XREF", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_CONTRACT_XREF");
        form_R_Tirf_Contract_Xref.setDdmHeader("CONT-/RACT/XREF");
        form_R_Tirf_Trad_Ira_Contrib = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Trad_Ira_Contrib", "TIRF-TRAD-IRA-CONTRIB", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TIRF_TRAD_IRA_CONTRIB");
        form_R_Tirf_Trad_Ira_Contrib.setDdmHeader("TRADITIONAL/IRA/CONTRIBUTION");

        form_R__R_Field_2 = vw_form_R.getRecord().newGroupInGroup("form_R__R_Field_2", "REDEFINE", form_R_Tirf_Trad_Ira_Contrib);
        form_R_Tirf_Pr_Gross_Amt_Roll = form_R__R_Field_2.newFieldInGroup("form_R_Tirf_Pr_Gross_Amt_Roll", "TIRF-PR-GROSS-AMT-ROLL", FieldType.PACKED_DECIMAL, 
            11, 2);
        form_R_Tirf_Roth_Ira_Contrib = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Roth_Ira_Contrib", "TIRF-ROTH-IRA-CONTRIB", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TIRF_ROTH_IRA_CONTRIB");
        form_R_Tirf_Roth_Ira_Contrib.setDdmHeader("ROTH/IRA/CONTRIBUTION");

        form_R__R_Field_3 = vw_form_R.getRecord().newGroupInGroup("form_R__R_Field_3", "REDEFINE", form_R_Tirf_Roth_Ira_Contrib);
        form_R_Tirf_Pr_Ivc_Amt_Roll = form_R__R_Field_3.newFieldInGroup("form_R_Tirf_Pr_Ivc_Amt_Roll", "TIRF-PR-IVC-AMT-ROLL", FieldType.PACKED_DECIMAL, 
            11, 2);
        form_R_Tirf_Ira_Rechar_Amt = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Ira_Rechar_Amt", "TIRF-IRA-RECHAR-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TIRF_IRA_RECHAR_AMT");
        form_R_Tirf_Ira_Rechar_Amt.setDdmHeader("IRA/RECHAR./AMOUNT");
        form_R_Tirf_Roth_Ira_Conversion = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Roth_Ira_Conversion", "TIRF-ROTH-IRA-CONVERSION", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TIRF_ROTH_IRA_CONVERSION");
        form_R_Tirf_Roth_Ira_Conversion.setDdmHeader("ROTH/IRA/CONVERSION");
        form_R_Tirf_Trad_Ira_Rollover = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Trad_Ira_Rollover", "TIRF-TRAD-IRA-ROLLOVER", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TIRF_TRAD_IRA_ROLLOVER");
        form_R_Tirf_Trad_Ira_Rollover.setDdmHeader("TRADITIONAL/IRA/ROLLOVERS");

        form_R__R_Field_4 = vw_form_R.getRecord().newGroupInGroup("form_R__R_Field_4", "REDEFINE", form_R_Tirf_Trad_Ira_Rollover);
        form_R_Tirf_Irr_Amt = form_R__R_Field_4.newFieldInGroup("form_R_Tirf_Irr_Amt", "TIRF-IRR-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        form_R_Tirf_Account_Fmv = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Account_Fmv", "TIRF-ACCOUNT-FMV", FieldType.PACKED_DECIMAL, 11, 2, 
            RepeatingFieldStrategy.None, "TIRF_ACCOUNT_FMV");
        form_R_Tirf_Account_Fmv.setDdmHeader("FAIR/MARKET/VALUE");
        form_R_Tirf_Pin = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Pin", "TIRF-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "TIRF_PIN");
        form_R_Tirf_Pin.setDdmHeader("PIN");
        form_R_Tirf_Sep_Amt = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Sep_Amt", "TIRF-SEP-AMT", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "TIRF_SEP_AMT");
        form_R_Tirf_Sep_Amt.setDdmHeader("SEP IRA/AMT");
        form_R_Tirf_Postpn_Amt = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Postpn_Amt", "TIRF-POSTPN-AMT", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "TIRF_POSTPN_AMT");
        form_R_Tirf_Postpn_Yr = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Postpn_Yr", "TIRF-POSTPN-YR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_POSTPN_YR");
        form_R_Tirf_Postpn_Code = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Postpn_Code", "TIRF-POSTPN-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_POSTPN_CODE");
        form_R_Tirf_Superde_6 = vw_form_R.getRecord().newFieldInGroup("form_R_Tirf_Superde_6", "TIRF-SUPERDE-6", FieldType.STRING, 33, RepeatingFieldStrategy.None, 
            "TIRF_SUPERDE_6");
        form_R_Tirf_Superde_6.setSuperDescriptor(true);
        registerRecord(vw_form_R);

        vw_twrparti_Read = new DataAccessProgramView(new NameInfo("vw_twrparti_Read", "TWRPARTI-READ"), "TWRPARTI_PARTICIPANT_FILE", "TWR_PARTICIPANT");
        twrparti_Read_Twrparti_Status = vw_twrparti_Read.getRecord().newFieldInGroup("twrparti_Read_Twrparti_Status", "TWRPARTI-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPARTI_STATUS");
        twrparti_Read_Twrparti_Tax_Id_Type = vw_twrparti_Read.getRecord().newFieldInGroup("twrparti_Read_Twrparti_Tax_Id_Type", "TWRPARTI-TAX-ID-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TWRPARTI_TAX_ID_TYPE");
        twrparti_Read_Twrparti_Tax_Id = vw_twrparti_Read.getRecord().newFieldInGroup("twrparti_Read_Twrparti_Tax_Id", "TWRPARTI-TAX-ID", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TWRPARTI_TAX_ID");
        twrparti_Read_Twrparti_Pin = vw_twrparti_Read.getRecord().newFieldInGroup("twrparti_Read_Twrparti_Pin", "TWRPARTI-PIN", FieldType.STRING, 12, 
            RepeatingFieldStrategy.None, "TWRPARTI_PIN");
        twrparti_Read_Twrparti_Rlup_Email_Addr = vw_twrparti_Read.getRecord().newFieldInGroup("twrparti_Read_Twrparti_Rlup_Email_Addr", "TWRPARTI-RLUP-EMAIL-ADDR", 
            FieldType.STRING, 70, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_EMAIL_ADDR");
        twrparti_Read_Twrparti_Rlup_Email_Pref = vw_twrparti_Read.getRecord().newFieldInGroup("twrparti_Read_Twrparti_Rlup_Email_Pref", "TWRPARTI-RLUP-EMAIL-PREF", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_EMAIL_PREF");
        twrparti_Read_Twrparti_Rlup_Email_Addr_Dte = vw_twrparti_Read.getRecord().newFieldInGroup("twrparti_Read_Twrparti_Rlup_Email_Addr_Dte", "TWRPARTI-RLUP-EMAIL-ADDR-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_EMAIL_ADDR_DTE");
        twrparti_Read_Twrparti_Rlup_Email_Pref_Dte = vw_twrparti_Read.getRecord().newFieldInGroup("twrparti_Read_Twrparti_Rlup_Email_Pref_Dte", "TWRPARTI-RLUP-EMAIL-PREF-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_EMAIL_PREF_DTE");
        registerRecord(vw_twrparti_Read);

        pnd_Twrparti_Curr_Rec_Sd = localVariables.newFieldInRecord("pnd_Twrparti_Curr_Rec_Sd", "#TWRPARTI-CURR-REC-SD", FieldType.STRING, 19);

        pnd_Twrparti_Curr_Rec_Sd__R_Field_5 = localVariables.newGroupInRecord("pnd_Twrparti_Curr_Rec_Sd__R_Field_5", "REDEFINE", pnd_Twrparti_Curr_Rec_Sd);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Tax_Id = pnd_Twrparti_Curr_Rec_Sd__R_Field_5.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Tax_Id", 
            "#KEY-TWRPARTI-TAX-ID", FieldType.STRING, 10);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Status = pnd_Twrparti_Curr_Rec_Sd__R_Field_5.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Status", 
            "#KEY-TWRPARTI-STATUS", FieldType.STRING, 1);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Part_Eff_End_Dte = pnd_Twrparti_Curr_Rec_Sd__R_Field_5.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Part_Eff_End_Dte", 
            "#KEY-TWRPARTI-PART-EFF-END-DTE", FieldType.STRING, 8);
        pnd_Input_Rec = localVariables.newFieldInRecord("pnd_Input_Rec", "#INPUT-REC", FieldType.STRING, 80);

        pnd_Input_Rec__R_Field_6 = localVariables.newGroupInRecord("pnd_Input_Rec__R_Field_6", "REDEFINE", pnd_Input_Rec);
        pnd_Input_Rec_Pnd_Tax_Year = pnd_Input_Rec__R_Field_6.newFieldInGroup("pnd_Input_Rec_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Form_Text = localVariables.newFieldInRecord("pnd_Form_Text", "#FORM-TEXT", FieldType.STRING, 8);

        pnd_1099r_1099i_Control_Totals = localVariables.newGroupInRecord("pnd_1099r_1099i_Control_Totals", "#1099R-1099I-CONTROL-TOTALS");
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt = pnd_1099r_1099i_Control_Totals.newFieldInGroup("pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt", 
            "#1099R-GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt = pnd_1099r_1099i_Control_Totals.newFieldInGroup("pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt", 
            "#1099R-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld = pnd_1099r_1099i_Control_Totals.newFieldInGroup("pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld", 
            "#1099-FED-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt = pnd_1099r_1099i_Control_Totals.newFieldInGroup("pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt", 
            "#1099R-IVC-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt = pnd_1099r_1099i_Control_Totals.newFieldInGroup("pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt", 
            "#1099R-IRR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Distr = pnd_1099r_1099i_Control_Totals.newFieldInGroup("pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Distr", 
            "#1099R-STATE-DISTR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld = pnd_1099r_1099i_Control_Totals.newFieldInGroup("pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld", 
            "#1099R-STATE-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Distr = pnd_1099r_1099i_Control_Totals.newFieldInGroup("pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Distr", 
            "#1099R-LOC-DISTR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld = pnd_1099r_1099i_Control_Totals.newFieldInGroup("pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld", 
            "#1099R-LOC-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt = pnd_1099r_1099i_Control_Totals.newFieldInGroup("pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt", 
            "#1099I-INT-AMT", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_1099r_1099i_Control_Totals_E = localVariables.newGroupInRecord("pnd_1099r_1099i_Control_Totals_E", "#1099R-1099I-CONTROL-TOTALS-E");
        pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Gross_Amt_E = pnd_1099r_1099i_Control_Totals_E.newFieldInGroup("pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Gross_Amt_E", 
            "#1099R-GROSS-AMT-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Taxable_Amt_E = pnd_1099r_1099i_Control_Totals_E.newFieldInGroup("pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Taxable_Amt_E", 
            "#1099R-TAXABLE-AMT-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_E_Pnd_1099_Fed_Tax_Wthld_E = pnd_1099r_1099i_Control_Totals_E.newFieldInGroup("pnd_1099r_1099i_Control_Totals_E_Pnd_1099_Fed_Tax_Wthld_E", 
            "#1099-FED-TAX-WTHLD-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Ivc_Amt_E = pnd_1099r_1099i_Control_Totals_E.newFieldInGroup("pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Ivc_Amt_E", 
            "#1099R-IVC-AMT-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Irr_Amt_E = pnd_1099r_1099i_Control_Totals_E.newFieldInGroup("pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Irr_Amt_E", 
            "#1099R-IRR-AMT-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_State_Distr_E = pnd_1099r_1099i_Control_Totals_E.newFieldInGroup("pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_State_Distr_E", 
            "#1099R-STATE-DISTR-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_State_Tax_Wthld_E = pnd_1099r_1099i_Control_Totals_E.newFieldInGroup("pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_State_Tax_Wthld_E", 
            "#1099R-STATE-TAX-WTHLD-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Loc_Distr_E = pnd_1099r_1099i_Control_Totals_E.newFieldInGroup("pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Loc_Distr_E", 
            "#1099R-LOC-DISTR-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Loc_Tax_Wthld_E = pnd_1099r_1099i_Control_Totals_E.newFieldInGroup("pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Loc_Tax_Wthld_E", 
            "#1099R-LOC-TAX-WTHLD-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_E_Pnd_1099i_Int_Amt_E = pnd_1099r_1099i_Control_Totals_E.newFieldInGroup("pnd_1099r_1099i_Control_Totals_E_Pnd_1099i_Int_Amt_E", 
            "#1099I-INT-AMT-E", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_1099r_1099i_Control_Totals_E_No = localVariables.newGroupInRecord("pnd_1099r_1099i_Control_Totals_E_No", "#1099R-1099I-CONTROL-TOTALS-E-NO");
        pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Gross_Amt_E_No = pnd_1099r_1099i_Control_Totals_E_No.newFieldInGroup("pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Gross_Amt_E_No", 
            "#1099R-GROSS-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Taxable_Amt_E_No = pnd_1099r_1099i_Control_Totals_E_No.newFieldInGroup("pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Taxable_Amt_E_No", 
            "#1099R-TAXABLE-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099_Fed_Tax_Wthld_E_No = pnd_1099r_1099i_Control_Totals_E_No.newFieldInGroup("pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099_Fed_Tax_Wthld_E_No", 
            "#1099-FED-TAX-WTHLD-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Ivc_Amt_E_No = pnd_1099r_1099i_Control_Totals_E_No.newFieldInGroup("pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Ivc_Amt_E_No", 
            "#1099R-IVC-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Irr_Amt_E_No = pnd_1099r_1099i_Control_Totals_E_No.newFieldInGroup("pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Irr_Amt_E_No", 
            "#1099R-IRR-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_State_Distr_E_No = pnd_1099r_1099i_Control_Totals_E_No.newFieldInGroup("pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_State_Distr_E_No", 
            "#1099R-STATE-DISTR-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_State_Tax_Wthld_E_No = pnd_1099r_1099i_Control_Totals_E_No.newFieldInGroup("pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_State_Tax_Wthld_E_No", 
            "#1099R-STATE-TAX-WTHLD-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Loc_Distr_E_No = pnd_1099r_1099i_Control_Totals_E_No.newFieldInGroup("pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Loc_Distr_E_No", 
            "#1099R-LOC-DISTR-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Loc_Tax_Wthld_E_No = pnd_1099r_1099i_Control_Totals_E_No.newFieldInGroup("pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Loc_Tax_Wthld_E_No", 
            "#1099R-LOC-TAX-WTHLD-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099i_Int_Amt_E_No = pnd_1099r_1099i_Control_Totals_E_No.newFieldInGroup("pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099i_Int_Amt_E_No", 
            "#1099I-INT-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_1099_Grand_Totals = localVariables.newGroupInRecord("pnd_1099_Grand_Totals", "#1099-GRAND-TOTALS");
        pnd_1099_Grand_Totals_Pnd_Total_1099_Form_Cnt = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Form_Cnt", "#TOTAL-1099-FORM-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_1099_Grand_Totals_Pnd_Total_1099_Hold_Cnt = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Hold_Cnt", "#TOTAL-1099-HOLD-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_1099_Grand_Totals_Pnd_Total_1099_Empty_Form_Cnt = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Empty_Form_Cnt", 
            "#TOTAL-1099-EMPTY-FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_1099_Grand_Totals_Pnd_Total_1099_Gross_Amt = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Gross_Amt", "#TOTAL-1099-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_Pnd_Total_1099_Taxable_Amt = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Taxable_Amt", "#TOTAL-1099-TAXABLE-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_Pnd_Total_1099_Fed_Tax_Wthld = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Fed_Tax_Wthld", 
            "#TOTAL-1099-FED-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_Pnd_Total_1099_Ivc_Amt = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Ivc_Amt", "#TOTAL-1099-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_Pnd_Total_1099_Irr_Amt = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Irr_Amt", "#TOTAL-1099-IRR-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_Pnd_Total_1099_State_Distr = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_State_Distr", "#TOTAL-1099-STATE-DISTR", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_Pnd_Total_1099_State_Tax_Wthld = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_State_Tax_Wthld", 
            "#TOTAL-1099-STATE-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_Pnd_Total_1099_Loc_Distr = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Loc_Distr", "#TOTAL-1099-LOC-DISTR", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_Pnd_Total_1099_Loc_Tax_Wthld = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Loc_Tax_Wthld", 
            "#TOTAL-1099-LOC-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_Pnd_Total_1099_Int_Amt = pnd_1099_Grand_Totals.newFieldInGroup("pnd_1099_Grand_Totals_Pnd_Total_1099_Int_Amt", "#TOTAL-1099-INT-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);

        pnd_1099_Grand_Totals_E = localVariables.newGroupInRecord("pnd_1099_Grand_Totals_E", "#1099-GRAND-TOTALS-E");
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_Form_Cnt_E = pnd_1099_Grand_Totals_E.newFieldInGroup("pnd_1099_Grand_Totals_E_Pnd_Total_1099_Form_Cnt_E", 
            "#TOTAL-1099-FORM-CNT-E", FieldType.PACKED_DECIMAL, 7);
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_Hold_Cnt_E = pnd_1099_Grand_Totals_E.newFieldInGroup("pnd_1099_Grand_Totals_E_Pnd_Total_1099_Hold_Cnt_E", 
            "#TOTAL-1099-HOLD-CNT-E", FieldType.PACKED_DECIMAL, 7);
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_Empty_Form_Cnt_E = pnd_1099_Grand_Totals_E.newFieldInGroup("pnd_1099_Grand_Totals_E_Pnd_Total_1099_Empty_Form_Cnt_E", 
            "#TOTAL-1099-EMPTY-FORM-CNT-E", FieldType.PACKED_DECIMAL, 7);
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_Gross_Amt_E = pnd_1099_Grand_Totals_E.newFieldInGroup("pnd_1099_Grand_Totals_E_Pnd_Total_1099_Gross_Amt_E", 
            "#TOTAL-1099-GROSS-AMT-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_Taxable_Amt_E = pnd_1099_Grand_Totals_E.newFieldInGroup("pnd_1099_Grand_Totals_E_Pnd_Total_1099_Taxable_Amt_E", 
            "#TOTAL-1099-TAXABLE-AMT-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_Fed_Tax_Wthld_E = pnd_1099_Grand_Totals_E.newFieldInGroup("pnd_1099_Grand_Totals_E_Pnd_Total_1099_Fed_Tax_Wthld_E", 
            "#TOTAL-1099-FED-TAX-WTHLD-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_Ivc_Amt_E = pnd_1099_Grand_Totals_E.newFieldInGroup("pnd_1099_Grand_Totals_E_Pnd_Total_1099_Ivc_Amt_E", 
            "#TOTAL-1099-IVC-AMT-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_Irr_Amt_E = pnd_1099_Grand_Totals_E.newFieldInGroup("pnd_1099_Grand_Totals_E_Pnd_Total_1099_Irr_Amt_E", 
            "#TOTAL-1099-IRR-AMT-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_State_Distr_E = pnd_1099_Grand_Totals_E.newFieldInGroup("pnd_1099_Grand_Totals_E_Pnd_Total_1099_State_Distr_E", 
            "#TOTAL-1099-STATE-DISTR-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_State_Tax_Wthld_E = pnd_1099_Grand_Totals_E.newFieldInGroup("pnd_1099_Grand_Totals_E_Pnd_Total_1099_State_Tax_Wthld_E", 
            "#TOTAL-1099-STATE-TAX-WTHLD-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_Loc_Distr_E = pnd_1099_Grand_Totals_E.newFieldInGroup("pnd_1099_Grand_Totals_E_Pnd_Total_1099_Loc_Distr_E", 
            "#TOTAL-1099-LOC-DISTR-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_Loc_Tax_Wthld_E = pnd_1099_Grand_Totals_E.newFieldInGroup("pnd_1099_Grand_Totals_E_Pnd_Total_1099_Loc_Tax_Wthld_E", 
            "#TOTAL-1099-LOC-TAX-WTHLD-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_Int_Amt_E = pnd_1099_Grand_Totals_E.newFieldInGroup("pnd_1099_Grand_Totals_E_Pnd_Total_1099_Int_Amt_E", 
            "#TOTAL-1099-INT-AMT-E", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_1099_Grand_Totals_E_No = localVariables.newGroupInRecord("pnd_1099_Grand_Totals_E_No", "#1099-GRAND-TOTALS-E-NO");
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Form_Cnt_E_No = pnd_1099_Grand_Totals_E_No.newFieldInGroup("pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Form_Cnt_E_No", 
            "#TOTAL-1099-FORM-CNT-E-NO", FieldType.PACKED_DECIMAL, 7);
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Hold_Cnt_E_No = pnd_1099_Grand_Totals_E_No.newFieldInGroup("pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Hold_Cnt_E_No", 
            "#TOTAL-1099-HOLD-CNT-E-NO", FieldType.PACKED_DECIMAL, 7);
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Empty_Form_Cnt_E_No = pnd_1099_Grand_Totals_E_No.newFieldInGroup("pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Empty_Form_Cnt_E_No", 
            "#TOTAL-1099-EMPTY-FORM-CNT-E-NO", FieldType.PACKED_DECIMAL, 7);
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Gross_Amt_E_No = pnd_1099_Grand_Totals_E_No.newFieldInGroup("pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Gross_Amt_E_No", 
            "#TOTAL-1099-GROSS-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Taxable_Amt_E_No = pnd_1099_Grand_Totals_E_No.newFieldInGroup("pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Taxable_Amt_E_No", 
            "#TOTAL-1099-TAXABLE-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Fed_Tax_Wthld_E_No = pnd_1099_Grand_Totals_E_No.newFieldInGroup("pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Fed_Tax_Wthld_E_No", 
            "#TOTAL-1099-FED-TAX-WTHLD-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Ivc_Amt_E_No = pnd_1099_Grand_Totals_E_No.newFieldInGroup("pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Ivc_Amt_E_No", 
            "#TOTAL-1099-IVC-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Irr_Amt_E_No = pnd_1099_Grand_Totals_E_No.newFieldInGroup("pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Irr_Amt_E_No", 
            "#TOTAL-1099-IRR-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_State_Distr_E_No = pnd_1099_Grand_Totals_E_No.newFieldInGroup("pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_State_Distr_E_No", 
            "#TOTAL-1099-STATE-DISTR-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_State_Tax_Wthld_E_No = pnd_1099_Grand_Totals_E_No.newFieldInGroup("pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_State_Tax_Wthld_E_No", 
            "#TOTAL-1099-STATE-TAX-WTHLD-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Loc_Distr_E_No = pnd_1099_Grand_Totals_E_No.newFieldInGroup("pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Loc_Distr_E_No", 
            "#TOTAL-1099-LOC-DISTR-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Loc_Tax_Wthld_E_No = pnd_1099_Grand_Totals_E_No.newFieldInGroup("pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Loc_Tax_Wthld_E_No", 
            "#TOTAL-1099-LOC-TAX-WTHLD-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Int_Amt_E_No = pnd_1099_Grand_Totals_E_No.newFieldInGroup("pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Int_Amt_E_No", 
            "#TOTAL-1099-INT-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_1042s_Control_Totals = localVariables.newGroupInRecord("pnd_1042s_Control_Totals", "#1042S-CONTROL-TOTALS");
        pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income = pnd_1042s_Control_Totals.newFieldInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income", 
            "#1042S-GROSS-INCOME", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt = pnd_1042s_Control_Totals.newFieldInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt", 
            "#1042S-INTEREST-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt = pnd_1042s_Control_Totals.newFieldInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt", "#1042S-PENSION-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld = pnd_1042s_Control_Totals.newFieldInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld", 
            "#1042S-NRA-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt = pnd_1042s_Control_Totals.newFieldInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt", "#1042S-REFUND-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income_Fatca = pnd_1042s_Control_Totals.newFieldArrayInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income_Fatca", 
            "#1042S-GROSS-INCOME-FATCA", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt_Fatca = pnd_1042s_Control_Totals.newFieldArrayInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt_Fatca", 
            "#1042S-INTEREST-AMT-FATCA", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt_Fatca = pnd_1042s_Control_Totals.newFieldArrayInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt_Fatca", 
            "#1042S-PENSION-AMT-FATCA", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld_Fatca = pnd_1042s_Control_Totals.newFieldArrayInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld_Fatca", 
            "#1042S-NRA-TAX-WTHLD-FATCA", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt_Fatca = pnd_1042s_Control_Totals.newFieldArrayInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt_Fatca", 
            "#1042S-REFUND-AMT-FATCA", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));

        pnd_1042s_Control_Totals_E = localVariables.newGroupInRecord("pnd_1042s_Control_Totals_E", "#1042S-CONTROL-TOTALS-E");
        pnd_1042s_Control_Totals_E_Pnd_1042s_Gross_Income_E = pnd_1042s_Control_Totals_E.newFieldInGroup("pnd_1042s_Control_Totals_E_Pnd_1042s_Gross_Income_E", 
            "#1042S-GROSS-INCOME-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_E_Pnd_1042s_Interest_Amt_E = pnd_1042s_Control_Totals_E.newFieldInGroup("pnd_1042s_Control_Totals_E_Pnd_1042s_Interest_Amt_E", 
            "#1042S-INTEREST-AMT-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_E_Pnd_1042s_Pension_Amt_E = pnd_1042s_Control_Totals_E.newFieldInGroup("pnd_1042s_Control_Totals_E_Pnd_1042s_Pension_Amt_E", 
            "#1042S-PENSION-AMT-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_E_Pnd_1042s_Nra_Tax_Wthld_E = pnd_1042s_Control_Totals_E.newFieldInGroup("pnd_1042s_Control_Totals_E_Pnd_1042s_Nra_Tax_Wthld_E", 
            "#1042S-NRA-TAX-WTHLD-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_E_Pnd_1042s_Refund_Amt_E = pnd_1042s_Control_Totals_E.newFieldInGroup("pnd_1042s_Control_Totals_E_Pnd_1042s_Refund_Amt_E", 
            "#1042S-REFUND-AMT-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_E_Pnd_1042s_Gross_Income_E_Fatca = pnd_1042s_Control_Totals_E.newFieldArrayInGroup("pnd_1042s_Control_Totals_E_Pnd_1042s_Gross_Income_E_Fatca", 
            "#1042S-GROSS-INCOME-E-FATCA", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Control_Totals_E_Pnd_1042s_Interest_Amt_E_Fatca = pnd_1042s_Control_Totals_E.newFieldArrayInGroup("pnd_1042s_Control_Totals_E_Pnd_1042s_Interest_Amt_E_Fatca", 
            "#1042S-INTEREST-AMT-E-FATCA", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Control_Totals_E_Pnd_1042s_Pension_Amt_E_Fatca = pnd_1042s_Control_Totals_E.newFieldArrayInGroup("pnd_1042s_Control_Totals_E_Pnd_1042s_Pension_Amt_E_Fatca", 
            "#1042S-PENSION-AMT-E-FATCA", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Control_Totals_E_Pnd_1042s_Nra_Tax_Wthld_E_Fatca = pnd_1042s_Control_Totals_E.newFieldArrayInGroup("pnd_1042s_Control_Totals_E_Pnd_1042s_Nra_Tax_Wthld_E_Fatca", 
            "#1042S-NRA-TAX-WTHLD-E-FATCA", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Control_Totals_E_Pnd_1042s_Refund_Amt_E_Fatca = pnd_1042s_Control_Totals_E.newFieldArrayInGroup("pnd_1042s_Control_Totals_E_Pnd_1042s_Refund_Amt_E_Fatca", 
            "#1042S-REFUND-AMT-E-FATCA", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));

        pnd_1042s_Control_Totals_E_No = localVariables.newGroupInRecord("pnd_1042s_Control_Totals_E_No", "#1042S-CONTROL-TOTALS-E-NO");
        pnd_1042s_Control_Totals_E_No_Pnd_1042s_Gross_Income_E_No = pnd_1042s_Control_Totals_E_No.newFieldInGroup("pnd_1042s_Control_Totals_E_No_Pnd_1042s_Gross_Income_E_No", 
            "#1042S-GROSS-INCOME-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_E_No_Pnd_1042s_Interest_Amt_E_No = pnd_1042s_Control_Totals_E_No.newFieldInGroup("pnd_1042s_Control_Totals_E_No_Pnd_1042s_Interest_Amt_E_No", 
            "#1042S-INTEREST-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_E_No_Pnd_1042s_Pension_Amt_E_No = pnd_1042s_Control_Totals_E_No.newFieldInGroup("pnd_1042s_Control_Totals_E_No_Pnd_1042s_Pension_Amt_E_No", 
            "#1042S-PENSION-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_E_No_Pnd_1042s_Nra_Tax_Wthld_E_No = pnd_1042s_Control_Totals_E_No.newFieldInGroup("pnd_1042s_Control_Totals_E_No_Pnd_1042s_Nra_Tax_Wthld_E_No", 
            "#1042S-NRA-TAX-WTHLD-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_E_No_Pnd_1042s_Refund_Amt_E_No = pnd_1042s_Control_Totals_E_No.newFieldInGroup("pnd_1042s_Control_Totals_E_No_Pnd_1042s_Refund_Amt_E_No", 
            "#1042S-REFUND-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_E_No_Pnd_1042s_Gross_Income_E_No_Fatca = pnd_1042s_Control_Totals_E_No.newFieldArrayInGroup("pnd_1042s_Control_Totals_E_No_Pnd_1042s_Gross_Income_E_No_Fatca", 
            "#1042S-GROSS-INCOME-E-NO-FATCA", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Control_Totals_E_No_Pnd_1042s_Interest_Amt_E_No_Fatca = pnd_1042s_Control_Totals_E_No.newFieldArrayInGroup("pnd_1042s_Control_Totals_E_No_Pnd_1042s_Interest_Amt_E_No_Fatca", 
            "#1042S-INTEREST-AMT-E-NO-FATCA", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Control_Totals_E_No_Pnd_1042s_Pension_Amt_E_No_Fatca = pnd_1042s_Control_Totals_E_No.newFieldArrayInGroup("pnd_1042s_Control_Totals_E_No_Pnd_1042s_Pension_Amt_E_No_Fatca", 
            "#1042S-PENSION-AMT-E-NO-FATCA", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Control_Totals_E_No_Pnd_1042s_Nra_Tax_Wthld_E_No_Fatca = pnd_1042s_Control_Totals_E_No.newFieldArrayInGroup("pnd_1042s_Control_Totals_E_No_Pnd_1042s_Nra_Tax_Wthld_E_No_Fatca", 
            "#1042S-NRA-TAX-WTHLD-E-NO-FATCA", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Control_Totals_E_No_Pnd_1042s_Refund_Amt_E_No_Fatca = pnd_1042s_Control_Totals_E_No.newFieldArrayInGroup("pnd_1042s_Control_Totals_E_No_Pnd_1042s_Refund_Amt_E_No_Fatca", 
            "#1042S-REFUND-AMT-E-NO-FATCA", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));

        pnd_5498_Control_Totals = localVariables.newGroupInRecord("pnd_5498_Control_Totals", "#5498-CONTROL-TOTALS");
        pnd_5498_Control_Totals_Pnd_5498_Form_Cnt = pnd_5498_Control_Totals.newFieldInGroup("pnd_5498_Control_Totals_Pnd_5498_Form_Cnt", "#5498-FORM-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Contrib = pnd_5498_Control_Totals.newFieldInGroup("pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Contrib", 
            "#5498-TRAD-IRA-CONTRIB", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Contrib = pnd_5498_Control_Totals.newFieldInGroup("pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Contrib", 
            "#5498-ROTH-IRA-CONTRIB", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rollover = pnd_5498_Control_Totals.newFieldInGroup("pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rollover", 
            "#5498-TRAD-IRA-ROLLOVER", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rechar = pnd_5498_Control_Totals.newFieldInGroup("pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rechar", 
            "#5498-TRAD-IRA-RECHAR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Conversion = pnd_5498_Control_Totals.newFieldInGroup("pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Conversion", 
            "#5498-ROTH-IRA-CONVERSION", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Pnd_5498_Account_Fmv = pnd_5498_Control_Totals.newFieldInGroup("pnd_5498_Control_Totals_Pnd_5498_Account_Fmv", "#5498-ACCOUNT-FMV", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Pnd_5498_Sep_Amt = pnd_5498_Control_Totals.newFieldInGroup("pnd_5498_Control_Totals_Pnd_5498_Sep_Amt", "#5498-SEP-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Pnd_5498_Postpn_Amt = pnd_5498_Control_Totals.newFieldInGroup("pnd_5498_Control_Totals_Pnd_5498_Postpn_Amt", "#5498-POSTPN-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);

        pnd_5498_Control_Totals_E = localVariables.newGroupInRecord("pnd_5498_Control_Totals_E", "#5498-CONTROL-TOTALS-E");
        pnd_5498_Control_Totals_E_Pnd_5498_Form_Cnt_E = pnd_5498_Control_Totals_E.newFieldInGroup("pnd_5498_Control_Totals_E_Pnd_5498_Form_Cnt_E", "#5498-FORM-CNT-E", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_5498_Control_Totals_E_Pnd_5498_Trad_Ira_Contrib_E = pnd_5498_Control_Totals_E.newFieldInGroup("pnd_5498_Control_Totals_E_Pnd_5498_Trad_Ira_Contrib_E", 
            "#5498-TRAD-IRA-CONTRIB-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_Pnd_5498_Roth_Ira_Contrib_E = pnd_5498_Control_Totals_E.newFieldInGroup("pnd_5498_Control_Totals_E_Pnd_5498_Roth_Ira_Contrib_E", 
            "#5498-ROTH-IRA-CONTRIB-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_Pnd_5498_Trad_Ira_Rollover_E = pnd_5498_Control_Totals_E.newFieldInGroup("pnd_5498_Control_Totals_E_Pnd_5498_Trad_Ira_Rollover_E", 
            "#5498-TRAD-IRA-ROLLOVER-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_Pnd_5498_Trad_Ira_Rechar_E = pnd_5498_Control_Totals_E.newFieldInGroup("pnd_5498_Control_Totals_E_Pnd_5498_Trad_Ira_Rechar_E", 
            "#5498-TRAD-IRA-RECHAR-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_Pnd_5498_Roth_Ira_Conversion_E = pnd_5498_Control_Totals_E.newFieldInGroup("pnd_5498_Control_Totals_E_Pnd_5498_Roth_Ira_Conversion_E", 
            "#5498-ROTH-IRA-CONVERSION-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_Pnd_5498_Account_Fmv_E = pnd_5498_Control_Totals_E.newFieldInGroup("pnd_5498_Control_Totals_E_Pnd_5498_Account_Fmv_E", 
            "#5498-ACCOUNT-FMV-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_Pnd_5498_Sep_Amt_E = pnd_5498_Control_Totals_E.newFieldInGroup("pnd_5498_Control_Totals_E_Pnd_5498_Sep_Amt_E", "#5498-SEP-AMT-E", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_Pnd_5498_Postpn_Amt_E = pnd_5498_Control_Totals_E.newFieldInGroup("pnd_5498_Control_Totals_E_Pnd_5498_Postpn_Amt_E", 
            "#5498-POSTPN-AMT-E", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_5498_Control_Totals_E_No = localVariables.newGroupInRecord("pnd_5498_Control_Totals_E_No", "#5498-CONTROL-TOTALS-E-NO");
        pnd_5498_Control_Totals_E_No_Pnd_5498_Form_Cnt_E_No = pnd_5498_Control_Totals_E_No.newFieldInGroup("pnd_5498_Control_Totals_E_No_Pnd_5498_Form_Cnt_E_No", 
            "#5498-FORM-CNT-E-NO", FieldType.PACKED_DECIMAL, 7);
        pnd_5498_Control_Totals_E_No_Pnd_5498_Trad_Ira_Contrib_E_No = pnd_5498_Control_Totals_E_No.newFieldInGroup("pnd_5498_Control_Totals_E_No_Pnd_5498_Trad_Ira_Contrib_E_No", 
            "#5498-TRAD-IRA-CONTRIB-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_No_Pnd_5498_Roth_Ira_Contrib_E_No = pnd_5498_Control_Totals_E_No.newFieldInGroup("pnd_5498_Control_Totals_E_No_Pnd_5498_Roth_Ira_Contrib_E_No", 
            "#5498-ROTH-IRA-CONTRIB-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_No_Pnd_5498_Trad_Ira_Rollover_E_No = pnd_5498_Control_Totals_E_No.newFieldInGroup("pnd_5498_Control_Totals_E_No_Pnd_5498_Trad_Ira_Rollover_E_No", 
            "#5498-TRAD-IRA-ROLLOVER-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_No_Pnd_5498_Trad_Ira_Rechar_E_No = pnd_5498_Control_Totals_E_No.newFieldInGroup("pnd_5498_Control_Totals_E_No_Pnd_5498_Trad_Ira_Rechar_E_No", 
            "#5498-TRAD-IRA-RECHAR-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_No_Pnd_5498_Roth_Ira_Conversion_E_No = pnd_5498_Control_Totals_E_No.newFieldInGroup("pnd_5498_Control_Totals_E_No_Pnd_5498_Roth_Ira_Conversion_E_No", 
            "#5498-ROTH-IRA-CONVERSION-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_No_Pnd_5498_Account_Fmv_E_No = pnd_5498_Control_Totals_E_No.newFieldInGroup("pnd_5498_Control_Totals_E_No_Pnd_5498_Account_Fmv_E_No", 
            "#5498-ACCOUNT-FMV-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_No_Pnd_5498_Sep_Amt_E_No = pnd_5498_Control_Totals_E_No.newFieldInGroup("pnd_5498_Control_Totals_E_No_Pnd_5498_Sep_Amt_E_No", 
            "#5498-SEP-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_No_Pnd_5498_Postpn_Amt_E_No = pnd_5498_Control_Totals_E_No.newFieldInGroup("pnd_5498_Control_Totals_E_No_Pnd_5498_Postpn_Amt_E_No", 
            "#5498-POSTPN-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_5498_Control_Totals_Gr = localVariables.newGroupInRecord("pnd_5498_Control_Totals_Gr", "#5498-CONTROL-TOTALS-GR");
        pnd_5498_Control_Totals_Gr_Pnd_5498_Form_Cnt_Gr = pnd_5498_Control_Totals_Gr.newFieldInGroup("pnd_5498_Control_Totals_Gr_Pnd_5498_Form_Cnt_Gr", 
            "#5498-FORM-CNT-GR", FieldType.PACKED_DECIMAL, 7);
        pnd_5498_Control_Totals_Gr_Pnd_5498_Trad_Ira_Contrib_Gr = pnd_5498_Control_Totals_Gr.newFieldInGroup("pnd_5498_Control_Totals_Gr_Pnd_5498_Trad_Ira_Contrib_Gr", 
            "#5498-TRAD-IRA-CONTRIB-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Gr_Pnd_5498_Roth_Ira_Contrib_Gr = pnd_5498_Control_Totals_Gr.newFieldInGroup("pnd_5498_Control_Totals_Gr_Pnd_5498_Roth_Ira_Contrib_Gr", 
            "#5498-ROTH-IRA-CONTRIB-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Gr_Pnd_5498_Trad_Ira_Rollover_Gr = pnd_5498_Control_Totals_Gr.newFieldInGroup("pnd_5498_Control_Totals_Gr_Pnd_5498_Trad_Ira_Rollover_Gr", 
            "#5498-TRAD-IRA-ROLLOVER-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Gr_Pnd_5498_Trad_Ira_Rechar_Gr = pnd_5498_Control_Totals_Gr.newFieldInGroup("pnd_5498_Control_Totals_Gr_Pnd_5498_Trad_Ira_Rechar_Gr", 
            "#5498-TRAD-IRA-RECHAR-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Gr_Pnd_5498_Roth_Ira_Conversion_Gr = pnd_5498_Control_Totals_Gr.newFieldInGroup("pnd_5498_Control_Totals_Gr_Pnd_5498_Roth_Ira_Conversion_Gr", 
            "#5498-ROTH-IRA-CONVERSION-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Gr_Pnd_5498_Account_Fmv_Gr = pnd_5498_Control_Totals_Gr.newFieldInGroup("pnd_5498_Control_Totals_Gr_Pnd_5498_Account_Fmv_Gr", 
            "#5498-ACCOUNT-FMV-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Gr_Pnd_5498_Sep_Amt_Gr = pnd_5498_Control_Totals_Gr.newFieldInGroup("pnd_5498_Control_Totals_Gr_Pnd_5498_Sep_Amt_Gr", 
            "#5498-SEP-AMT-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_Gr_Pnd_5498_Postpn_Amt_Gr = pnd_5498_Control_Totals_Gr.newFieldInGroup("pnd_5498_Control_Totals_Gr_Pnd_5498_Postpn_Amt_Gr", 
            "#5498-POSTPN-AMT-GR", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_5498_Control_Totals_E_Gr = localVariables.newGroupInRecord("pnd_5498_Control_Totals_E_Gr", "#5498-CONTROL-TOTALS-E-GR");
        pnd_5498_Control_Totals_E_Gr_Pnd_5498_Form_Cnt_E_Gr = pnd_5498_Control_Totals_E_Gr.newFieldInGroup("pnd_5498_Control_Totals_E_Gr_Pnd_5498_Form_Cnt_E_Gr", 
            "#5498-FORM-CNT-E-GR", FieldType.PACKED_DECIMAL, 7);
        pnd_5498_Control_Totals_E_Gr_Pnd_5498_Trad_Ira_Contrib_E_Gr = pnd_5498_Control_Totals_E_Gr.newFieldInGroup("pnd_5498_Control_Totals_E_Gr_Pnd_5498_Trad_Ira_Contrib_E_Gr", 
            "#5498-TRAD-IRA-CONTRIB-E-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_Gr_Pnd_5498_Roth_Ira_Contrib_E_Gr = pnd_5498_Control_Totals_E_Gr.newFieldInGroup("pnd_5498_Control_Totals_E_Gr_Pnd_5498_Roth_Ira_Contrib_E_Gr", 
            "#5498-ROTH-IRA-CONTRIB-E-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_Gr_Pnd_5498_Trad_Ira_Rollover_E_Gr = pnd_5498_Control_Totals_E_Gr.newFieldInGroup("pnd_5498_Control_Totals_E_Gr_Pnd_5498_Trad_Ira_Rollover_E_Gr", 
            "#5498-TRAD-IRA-ROLLOVER-E-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_Gr_Pnd_5498_Trad_Ira_Rechar_E_Gr = pnd_5498_Control_Totals_E_Gr.newFieldInGroup("pnd_5498_Control_Totals_E_Gr_Pnd_5498_Trad_Ira_Rechar_E_Gr", 
            "#5498-TRAD-IRA-RECHAR-E-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_Gr_Pnd_5498_Roth_Ira_Conversion_E_Gr = pnd_5498_Control_Totals_E_Gr.newFieldInGroup("pnd_5498_Control_Totals_E_Gr_Pnd_5498_Roth_Ira_Conversion_E_Gr", 
            "#5498-ROTH-IRA-CONVERSION-E-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_Gr_Pnd_5498_Account_Fmv_E_Gr = pnd_5498_Control_Totals_E_Gr.newFieldInGroup("pnd_5498_Control_Totals_E_Gr_Pnd_5498_Account_Fmv_E_Gr", 
            "#5498-ACCOUNT-FMV-E-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_Gr_Pnd_5498_Sep_Amt_E_Gr = pnd_5498_Control_Totals_E_Gr.newFieldInGroup("pnd_5498_Control_Totals_E_Gr_Pnd_5498_Sep_Amt_E_Gr", 
            "#5498-SEP-AMT-E-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_Gr_Pnd_5498_Postpn_Amt_E_Gr = pnd_5498_Control_Totals_E_Gr.newFieldInGroup("pnd_5498_Control_Totals_E_Gr_Pnd_5498_Postpn_Amt_E_Gr", 
            "#5498-POSTPN-AMT-E-GR", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_5498_Control_Totals_E_No_Gr = localVariables.newGroupInRecord("pnd_5498_Control_Totals_E_No_Gr", "#5498-CONTROL-TOTALS-E-NO-GR");
        pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Form_Cnt_E_No_Gr = pnd_5498_Control_Totals_E_No_Gr.newFieldInGroup("pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Form_Cnt_E_No_Gr", 
            "#5498-FORM-CNT-E-NO-GR", FieldType.PACKED_DECIMAL, 7);
        pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Trad_Ira_Contrib_E_No_Gr = pnd_5498_Control_Totals_E_No_Gr.newFieldInGroup("pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Trad_Ira_Contrib_E_No_Gr", 
            "#5498-TRAD-IRA-CONTRIB-E-NO-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Roth_Ira_Contrib_E_No_Gr = pnd_5498_Control_Totals_E_No_Gr.newFieldInGroup("pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Roth_Ira_Contrib_E_No_Gr", 
            "#5498-ROTH-IRA-CONTRIB-E-NO-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Trad_Ira_Rollover_E_No_Gr = pnd_5498_Control_Totals_E_No_Gr.newFieldInGroup("pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Trad_Ira_Rollover_E_No_Gr", 
            "#5498-TRAD-IRA-ROLLOVER-E-NO-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Trad_Ira_Rechar_E_No_Gr = pnd_5498_Control_Totals_E_No_Gr.newFieldInGroup("pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Trad_Ira_Rechar_E_No_Gr", 
            "#5498-TRAD-IRA-RECHAR-E-NO-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Roth_Ira_Conversion_E_No_Gr = pnd_5498_Control_Totals_E_No_Gr.newFieldInGroup("pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Roth_Ira_Conversion_E_No_Gr", 
            "#5498-ROTH-IRA-CONVERSION-E-NO-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Account_Fmv_E_No_Gr = pnd_5498_Control_Totals_E_No_Gr.newFieldInGroup("pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Account_Fmv_E_No_Gr", 
            "#5498-ACCOUNT-FMV-E-NO-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Sep_Amt_E_No_Gr = pnd_5498_Control_Totals_E_No_Gr.newFieldInGroup("pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Sep_Amt_E_No_Gr", 
            "#5498-SEP-AMT-E-NO-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Postpn_Amt_E_No_Gr = pnd_5498_Control_Totals_E_No_Gr.newFieldInGroup("pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Postpn_Amt_E_No_Gr", 
            "#5498-POSTPN-AMT-E-NO-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Form_Cnt_Comp_5498 = localVariables.newFieldInRecord("pnd_Form_Cnt_Comp_5498", "#FORM-CNT-COMP-5498", FieldType.PACKED_DECIMAL, 7);
        pnd_Form_Cnt_Comp_E_5498 = localVariables.newFieldInRecord("pnd_Form_Cnt_Comp_E_5498", "#FORM-CNT-COMP-E-5498", FieldType.PACKED_DECIMAL, 7);
        pnd_Form_Cnt_Comp_E_No_5498 = localVariables.newFieldInRecord("pnd_Form_Cnt_Comp_E_No_5498", "#FORM-CNT-COMP-E-NO-5498", FieldType.PACKED_DECIMAL, 
            7);

        pnd_5498_Control_Totals_P2 = localVariables.newGroupInRecord("pnd_5498_Control_Totals_P2", "#5498-CONTROL-TOTALS-P2");
        pnd_5498_Control_Totals_P2_Pnd_5498_Part_Cnt_P2 = pnd_5498_Control_Totals_P2.newFieldInGroup("pnd_5498_Control_Totals_P2_Pnd_5498_Part_Cnt_P2", 
            "#5498-PART-CNT-P2", FieldType.PACKED_DECIMAL, 7);
        pnd_5498_Control_Totals_P2_Pnd_5498_Form_Cnt_P2 = pnd_5498_Control_Totals_P2.newFieldInGroup("pnd_5498_Control_Totals_P2_Pnd_5498_Form_Cnt_P2", 
            "#5498-FORM-CNT-P2", FieldType.PACKED_DECIMAL, 7);
        pnd_5498_Control_Totals_P2_Pnd_5498_Trad_Ira_Contrib_P2 = pnd_5498_Control_Totals_P2.newFieldInGroup("pnd_5498_Control_Totals_P2_Pnd_5498_Trad_Ira_Contrib_P2", 
            "#5498-TRAD-IRA-CONTRIB-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_P2_Pnd_5498_Roth_Ira_Contrib_P2 = pnd_5498_Control_Totals_P2.newFieldInGroup("pnd_5498_Control_Totals_P2_Pnd_5498_Roth_Ira_Contrib_P2", 
            "#5498-ROTH-IRA-CONTRIB-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_P2_Pnd_5498_Trad_Ira_Rollover_P2 = pnd_5498_Control_Totals_P2.newFieldInGroup("pnd_5498_Control_Totals_P2_Pnd_5498_Trad_Ira_Rollover_P2", 
            "#5498-TRAD-IRA-ROLLOVER-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_P2_Pnd_5498_Trad_Ira_Rechar_P2 = pnd_5498_Control_Totals_P2.newFieldInGroup("pnd_5498_Control_Totals_P2_Pnd_5498_Trad_Ira_Rechar_P2", 
            "#5498-TRAD-IRA-RECHAR-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_P2_Pnd_5498_Roth_Ira_Conversion_P2 = pnd_5498_Control_Totals_P2.newFieldInGroup("pnd_5498_Control_Totals_P2_Pnd_5498_Roth_Ira_Conversion_P2", 
            "#5498-ROTH-IRA-CONVERSION-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_P2_Pnd_5498_Account_Fmv_P2 = pnd_5498_Control_Totals_P2.newFieldInGroup("pnd_5498_Control_Totals_P2_Pnd_5498_Account_Fmv_P2", 
            "#5498-ACCOUNT-FMV-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_P2_Pnd_5498_Sep_Amt_P2 = pnd_5498_Control_Totals_P2.newFieldInGroup("pnd_5498_Control_Totals_P2_Pnd_5498_Sep_Amt_P2", 
            "#5498-SEP-AMT-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_P2_Pnd_5498_Postpn_Amt_P2 = pnd_5498_Control_Totals_P2.newFieldInGroup("pnd_5498_Control_Totals_P2_Pnd_5498_Postpn_Amt_P2", 
            "#5498-POSTPN-AMT-P2", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_5498_Control_Totals_E_P2 = localVariables.newGroupInRecord("pnd_5498_Control_Totals_E_P2", "#5498-CONTROL-TOTALS-E-P2");
        pnd_5498_Control_Totals_E_P2_Pnd_5498_Part_Cnt_E_P2 = pnd_5498_Control_Totals_E_P2.newFieldInGroup("pnd_5498_Control_Totals_E_P2_Pnd_5498_Part_Cnt_E_P2", 
            "#5498-PART-CNT-E-P2", FieldType.PACKED_DECIMAL, 7);
        pnd_5498_Control_Totals_E_P2_Pnd_5498_Form_Cnt_E_P2 = pnd_5498_Control_Totals_E_P2.newFieldInGroup("pnd_5498_Control_Totals_E_P2_Pnd_5498_Form_Cnt_E_P2", 
            "#5498-FORM-CNT-E-P2", FieldType.PACKED_DECIMAL, 7);
        pnd_5498_Control_Totals_E_P2_Pnd_5498_Trad_Ira_Contrib_E_P2 = pnd_5498_Control_Totals_E_P2.newFieldInGroup("pnd_5498_Control_Totals_E_P2_Pnd_5498_Trad_Ira_Contrib_E_P2", 
            "#5498-TRAD-IRA-CONTRIB-E-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_P2_Pnd_5498_Roth_Ira_Contrib_E_P2 = pnd_5498_Control_Totals_E_P2.newFieldInGroup("pnd_5498_Control_Totals_E_P2_Pnd_5498_Roth_Ira_Contrib_E_P2", 
            "#5498-ROTH-IRA-CONTRIB-E-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_P2_Pnd_5498_Trad_Ira_Rollover_E_P2 = pnd_5498_Control_Totals_E_P2.newFieldInGroup("pnd_5498_Control_Totals_E_P2_Pnd_5498_Trad_Ira_Rollover_E_P2", 
            "#5498-TRAD-IRA-ROLLOVER-E-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_P2_Pnd_5498_Trad_Ira_Rechar_E_P2 = pnd_5498_Control_Totals_E_P2.newFieldInGroup("pnd_5498_Control_Totals_E_P2_Pnd_5498_Trad_Ira_Rechar_E_P2", 
            "#5498-TRAD-IRA-RECHAR-E-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_P2_Pnd_5498_Roth_Ira_Conversion_E_P2 = pnd_5498_Control_Totals_E_P2.newFieldInGroup("pnd_5498_Control_Totals_E_P2_Pnd_5498_Roth_Ira_Conversion_E_P2", 
            "#5498-ROTH-IRA-CONVERSION-E-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_P2_Pnd_5498_Account_Fmv_E_P2 = pnd_5498_Control_Totals_E_P2.newFieldInGroup("pnd_5498_Control_Totals_E_P2_Pnd_5498_Account_Fmv_E_P2", 
            "#5498-ACCOUNT-FMV-E-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_P2_Pnd_5498_Sep_Amt_E_P2 = pnd_5498_Control_Totals_E_P2.newFieldInGroup("pnd_5498_Control_Totals_E_P2_Pnd_5498_Sep_Amt_E_P2", 
            "#5498-SEP-AMT-E-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_P2_Pnd_5498_Postpn_Amt_E_P2 = pnd_5498_Control_Totals_E_P2.newFieldInGroup("pnd_5498_Control_Totals_E_P2_Pnd_5498_Postpn_Amt_E_P2", 
            "#5498-POSTPN-AMT-E-P2", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_5498_Control_Totals_E_N_P2 = localVariables.newGroupInRecord("pnd_5498_Control_Totals_E_N_P2", "#5498-CONTROL-TOTALS-E-N-P2");
        pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Part_Cnt_E_N_P2 = pnd_5498_Control_Totals_E_N_P2.newFieldInGroup("pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Part_Cnt_E_N_P2", 
            "#5498-PART-CNT-E-N-P2", FieldType.PACKED_DECIMAL, 7);
        pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Form_Cnt_E_N_P2 = pnd_5498_Control_Totals_E_N_P2.newFieldInGroup("pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Form_Cnt_E_N_P2", 
            "#5498-FORM-CNT-E-N-P2", FieldType.PACKED_DECIMAL, 7);
        pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Trad_Ira_Contrib_E_N_P2 = pnd_5498_Control_Totals_E_N_P2.newFieldInGroup("pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Trad_Ira_Contrib_E_N_P2", 
            "#5498-TRAD-IRA-CONTRIB-E-N-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Roth_Ira_Contrib_E_N_P2 = pnd_5498_Control_Totals_E_N_P2.newFieldInGroup("pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Roth_Ira_Contrib_E_N_P2", 
            "#5498-ROTH-IRA-CONTRIB-E-N-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Trad_Ira_Rollover_E_N_P2 = pnd_5498_Control_Totals_E_N_P2.newFieldInGroup("pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Trad_Ira_Rollover_E_N_P2", 
            "#5498-TRAD-IRA-ROLLOVER-E-N-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Trad_Ira_Rechar_E_N_P2 = pnd_5498_Control_Totals_E_N_P2.newFieldInGroup("pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Trad_Ira_Rechar_E_N_P2", 
            "#5498-TRAD-IRA-RECHAR-E-N-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Roth_Ira_Conversion_E_N_P2 = pnd_5498_Control_Totals_E_N_P2.newFieldInGroup("pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Roth_Ira_Conversion_E_N_P2", 
            "#5498-ROTH-IRA-CONVERSION-E-N-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Account_Fmv_E_N_P2 = pnd_5498_Control_Totals_E_N_P2.newFieldInGroup("pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Account_Fmv_E_N_P2", 
            "#5498-ACCOUNT-FMV-E-N-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Sep_Amt_E_N_P2 = pnd_5498_Control_Totals_E_N_P2.newFieldInGroup("pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Sep_Amt_E_N_P2", 
            "#5498-SEP-AMT-E-N-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Postpn_Amt_E_N_P2 = pnd_5498_Control_Totals_E_N_P2.newFieldInGroup("pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Postpn_Amt_E_N_P2", 
            "#5498-POSTPN-AMT-E-N-P2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_M = localVariables.newFieldInRecord("pnd_M", "#M", FieldType.NUMERIC, 3);

        pnd_Suny_Control_Totals = localVariables.newGroupInRecord("pnd_Suny_Control_Totals", "#SUNY-CONTROL-TOTALS");
        pnd_Suny_Control_Totals_Pnd_Letter_Count = pnd_Suny_Control_Totals.newFieldInGroup("pnd_Suny_Control_Totals_Pnd_Letter_Count", "#LETTER-COUNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Suny_Control_Totals_Pnd_Suny_Contract_Ctr = pnd_Suny_Control_Totals.newFieldInGroup("pnd_Suny_Control_Totals_Pnd_Suny_Contract_Ctr", "#SUNY-CONTRACT-CTR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Suny_Control_Totals_Pnd_Suny_Gross_Amt = pnd_Suny_Control_Totals.newFieldInGroup("pnd_Suny_Control_Totals_Pnd_Suny_Gross_Amt", "#SUNY-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Suny_Control_Totals_Pnd_Suny_Fed_Tax_Wthld = pnd_Suny_Control_Totals.newFieldInGroup("pnd_Suny_Control_Totals_Pnd_Suny_Fed_Tax_Wthld", "#SUNY-FED-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Suny_Control_Totals_Pnd_Suny_State_Tax_Wthld = pnd_Suny_Control_Totals.newFieldInGroup("pnd_Suny_Control_Totals_Pnd_Suny_State_Tax_Wthld", 
            "#SUNY-STATE-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Suny_Control_Totals_Pnd_Suny_Loc_Tax_Wthld = pnd_Suny_Control_Totals.newFieldInGroup("pnd_Suny_Control_Totals_Pnd_Suny_Loc_Tax_Wthld", "#SUNY-LOC-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Suny_Control_Totals_E = localVariables.newGroupInRecord("pnd_Suny_Control_Totals_E", "#SUNY-CONTROL-TOTALS-E");
        pnd_Suny_Control_Totals_E_Pnd_Letter_Count_E = pnd_Suny_Control_Totals_E.newFieldInGroup("pnd_Suny_Control_Totals_E_Pnd_Letter_Count_E", "#LETTER-COUNT-E", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Suny_Control_Totals_E_Pnd_Suny_Contract_Ctr_E = pnd_Suny_Control_Totals_E.newFieldInGroup("pnd_Suny_Control_Totals_E_Pnd_Suny_Contract_Ctr_E", 
            "#SUNY-CONTRACT-CTR-E", FieldType.PACKED_DECIMAL, 7);
        pnd_Suny_Control_Totals_E_Pnd_Suny_Gross_Amt_E = pnd_Suny_Control_Totals_E.newFieldInGroup("pnd_Suny_Control_Totals_E_Pnd_Suny_Gross_Amt_E", "#SUNY-GROSS-AMT-E", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Suny_Control_Totals_E_Pnd_Suny_Fed_Tax_Wthld_E = pnd_Suny_Control_Totals_E.newFieldInGroup("pnd_Suny_Control_Totals_E_Pnd_Suny_Fed_Tax_Wthld_E", 
            "#SUNY-FED-TAX-WTHLD-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Suny_Control_Totals_E_Pnd_Suny_State_Tax_Wthld_E = pnd_Suny_Control_Totals_E.newFieldInGroup("pnd_Suny_Control_Totals_E_Pnd_Suny_State_Tax_Wthld_E", 
            "#SUNY-STATE-TAX-WTHLD-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Suny_Control_Totals_E_Pnd_Suny_Loc_Tax_Wthld_E = pnd_Suny_Control_Totals_E.newFieldInGroup("pnd_Suny_Control_Totals_E_Pnd_Suny_Loc_Tax_Wthld_E", 
            "#SUNY-LOC-TAX-WTHLD-E", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Suny_Control_Totals_E_No = localVariables.newGroupInRecord("pnd_Suny_Control_Totals_E_No", "#SUNY-CONTROL-TOTALS-E-NO");
        pnd_Suny_Control_Totals_E_No_Pnd_Letter_Count_E_No = pnd_Suny_Control_Totals_E_No.newFieldInGroup("pnd_Suny_Control_Totals_E_No_Pnd_Letter_Count_E_No", 
            "#LETTER-COUNT-E-NO", FieldType.PACKED_DECIMAL, 7);
        pnd_Suny_Control_Totals_E_No_Pnd_Suny_Contract_Ctr_E_No = pnd_Suny_Control_Totals_E_No.newFieldInGroup("pnd_Suny_Control_Totals_E_No_Pnd_Suny_Contract_Ctr_E_No", 
            "#SUNY-CONTRACT-CTR-E-NO", FieldType.PACKED_DECIMAL, 7);
        pnd_Suny_Control_Totals_E_No_Pnd_Suny_Gross_Amt_E_No = pnd_Suny_Control_Totals_E_No.newFieldInGroup("pnd_Suny_Control_Totals_E_No_Pnd_Suny_Gross_Amt_E_No", 
            "#SUNY-GROSS-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Suny_Control_Totals_E_No_Pnd_Suny_Fed_Tax_Wthld_E_No = pnd_Suny_Control_Totals_E_No.newFieldInGroup("pnd_Suny_Control_Totals_E_No_Pnd_Suny_Fed_Tax_Wthld_E_No", 
            "#SUNY-FED-TAX-WTHLD-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Suny_Control_Totals_E_No_Pnd_Suny_State_Tax_Wthld_E_No = pnd_Suny_Control_Totals_E_No.newFieldInGroup("pnd_Suny_Control_Totals_E_No_Pnd_Suny_State_Tax_Wthld_E_No", 
            "#SUNY-STATE-TAX-WTHLD-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Suny_Control_Totals_E_No_Pnd_Suny_Loc_Tax_Wthld_E_No = pnd_Suny_Control_Totals_E_No.newFieldInGroup("pnd_Suny_Control_Totals_E_No_Pnd_Suny_Loc_Tax_Wthld_E_No", 
            "#SUNY-LOC-TAX-WTHLD-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_5498_Type = localVariables.newFieldInRecord("pnd_5498_Type", "#5498-TYPE", FieldType.STRING, 2);

        pnd_5498_Type__R_Field_7 = localVariables.newGroupInRecord("pnd_5498_Type__R_Field_7", "REDEFINE", pnd_5498_Type);
        pnd_5498_Type_Pnd_5498_Type_N = pnd_5498_Type__R_Field_7.newFieldInGroup("pnd_5498_Type_Pnd_5498_Type_N", "#5498-TYPE-N", FieldType.NUMERIC, 2);
        pnd_Ira_Start = localVariables.newFieldInRecord("pnd_Ira_Start", "#IRA-START", FieldType.STRING, 26);

        pnd_Ira_Start__R_Field_8 = localVariables.newGroupInRecord("pnd_Ira_Start__R_Field_8", "REDEFINE", pnd_Ira_Start);
        pnd_Ira_Start_Twrc_Tax_Year = pnd_Ira_Start__R_Field_8.newFieldInGroup("pnd_Ira_Start_Twrc_Tax_Year", "TWRC-TAX-YEAR", FieldType.STRING, 4);
        pnd_Ira_Start_Twrc_Status = pnd_Ira_Start__R_Field_8.newFieldInGroup("pnd_Ira_Start_Twrc_Status", "TWRC-STATUS", FieldType.STRING, 1);
        pnd_Ira_Start_Twrc_Tax_Id = pnd_Ira_Start__R_Field_8.newFieldInGroup("pnd_Ira_Start_Twrc_Tax_Id", "TWRC-TAX-ID", FieldType.STRING, 10);
        pnd_Ira_Start_Twrc_Contract = pnd_Ira_Start__R_Field_8.newFieldInGroup("pnd_Ira_Start_Twrc_Contract", "TWRC-CONTRACT", FieldType.STRING, 8);
        pnd_Ira_Start_Twrc_Payee = pnd_Ira_Start__R_Field_8.newFieldInGroup("pnd_Ira_Start_Twrc_Payee", "TWRC-PAYEE", FieldType.STRING, 2);
        pnd_Ira_End = localVariables.newFieldInRecord("pnd_Ira_End", "#IRA-END", FieldType.STRING, 26);

        pnd_4807c_Control_Totals = localVariables.newGroupArrayInRecord("pnd_4807c_Control_Totals", "#4807C-CONTROL-TOTALS", new DbsArrayController(1, 
            2));
        pnd_4807c_Control_Totals_Pnd_4807c_Form_Cnt = pnd_4807c_Control_Totals.newFieldInGroup("pnd_4807c_Control_Totals_Pnd_4807c_Form_Cnt", "#4807C-FORM-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_4807c_Control_Totals_Pnd_4807c_Empty_Form_Cnt = pnd_4807c_Control_Totals.newFieldInGroup("pnd_4807c_Control_Totals_Pnd_4807c_Empty_Form_Cnt", 
            "#4807C-EMPTY-FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_4807c_Control_Totals_Pnd_4807c_Hold_Cnt = pnd_4807c_Control_Totals.newFieldInGroup("pnd_4807c_Control_Totals_Pnd_4807c_Hold_Cnt", "#4807C-HOLD-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt = pnd_4807c_Control_Totals.newFieldInGroup("pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt", "#4807C-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_Pnd_4807c_Int_Amt = pnd_4807c_Control_Totals.newFieldInGroup("pnd_4807c_Control_Totals_Pnd_4807c_Int_Amt", "#4807C-INT-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_Pnd_4807c_Distrib_Amt = pnd_4807c_Control_Totals.newFieldInGroup("pnd_4807c_Control_Totals_Pnd_4807c_Distrib_Amt", "#4807C-DISTRIB-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_Pnd_4807c_Taxable_Amt = pnd_4807c_Control_Totals.newFieldInGroup("pnd_4807c_Control_Totals_Pnd_4807c_Taxable_Amt", "#4807C-TAXABLE-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt = pnd_4807c_Control_Totals.newFieldInGroup("pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt", "#4807C-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt_Roll = pnd_4807c_Control_Totals.newFieldInGroup("pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt_Roll", 
            "#4807C-GROSS-AMT-ROLL", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt_Roll = pnd_4807c_Control_Totals.newFieldInGroup("pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt_Roll", 
            "#4807C-IVC-AMT-ROLL", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_4807c_Control_Totals_E = localVariables.newGroupArrayInRecord("pnd_4807c_Control_Totals_E", "#4807C-CONTROL-TOTALS-E", new DbsArrayController(1, 
            2));
        pnd_4807c_Control_Totals_E_Pnd_4807c_Form_Cnt_E = pnd_4807c_Control_Totals_E.newFieldInGroup("pnd_4807c_Control_Totals_E_Pnd_4807c_Form_Cnt_E", 
            "#4807C-FORM-CNT-E", FieldType.PACKED_DECIMAL, 7);
        pnd_4807c_Control_Totals_E_Pnd_4807c_Empty_Form_Cnt_E = pnd_4807c_Control_Totals_E.newFieldInGroup("pnd_4807c_Control_Totals_E_Pnd_4807c_Empty_Form_Cnt_E", 
            "#4807C-EMPTY-FORM-CNT-E", FieldType.PACKED_DECIMAL, 7);
        pnd_4807c_Control_Totals_E_Pnd_4807c_Hold_Cnt_E = pnd_4807c_Control_Totals_E.newFieldInGroup("pnd_4807c_Control_Totals_E_Pnd_4807c_Hold_Cnt_E", 
            "#4807C-HOLD-CNT-E", FieldType.PACKED_DECIMAL, 7);
        pnd_4807c_Control_Totals_E_Pnd_4807c_Gross_Amt_E = pnd_4807c_Control_Totals_E.newFieldInGroup("pnd_4807c_Control_Totals_E_Pnd_4807c_Gross_Amt_E", 
            "#4807C-GROSS-AMT-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_Pnd_4807c_Int_Amt_E = pnd_4807c_Control_Totals_E.newFieldInGroup("pnd_4807c_Control_Totals_E_Pnd_4807c_Int_Amt_E", 
            "#4807C-INT-AMT-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_Pnd_4807c_Distrib_Amt_E = pnd_4807c_Control_Totals_E.newFieldInGroup("pnd_4807c_Control_Totals_E_Pnd_4807c_Distrib_Amt_E", 
            "#4807C-DISTRIB-AMT-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_Pnd_4807c_Taxable_Amt_E = pnd_4807c_Control_Totals_E.newFieldInGroup("pnd_4807c_Control_Totals_E_Pnd_4807c_Taxable_Amt_E", 
            "#4807C-TAXABLE-AMT-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_Pnd_4807c_Ivc_Amt_E = pnd_4807c_Control_Totals_E.newFieldInGroup("pnd_4807c_Control_Totals_E_Pnd_4807c_Ivc_Amt_E", 
            "#4807C-IVC-AMT-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_Pnd_4807c_Gross_Amt_Roll_E = pnd_4807c_Control_Totals_E.newFieldInGroup("pnd_4807c_Control_Totals_E_Pnd_4807c_Gross_Amt_Roll_E", 
            "#4807C-GROSS-AMT-ROLL-E", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_Pnd_4807c_Ivc_Amt_Roll_E = pnd_4807c_Control_Totals_E.newFieldInGroup("pnd_4807c_Control_Totals_E_Pnd_4807c_Ivc_Amt_Roll_E", 
            "#4807C-IVC-AMT-ROLL-E", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_4807c_Control_Totals_E_No = localVariables.newGroupArrayInRecord("pnd_4807c_Control_Totals_E_No", "#4807C-CONTROL-TOTALS-E-NO", new DbsArrayController(1, 
            2));
        pnd_4807c_Control_Totals_E_No_Pnd_4807c_Form_Cnt_E_No = pnd_4807c_Control_Totals_E_No.newFieldInGroup("pnd_4807c_Control_Totals_E_No_Pnd_4807c_Form_Cnt_E_No", 
            "#4807C-FORM-CNT-E-NO", FieldType.PACKED_DECIMAL, 7);
        pnd_4807c_Control_Totals_E_No_Pnd_4807c_Empty_Form_Cnt_E_No = pnd_4807c_Control_Totals_E_No.newFieldInGroup("pnd_4807c_Control_Totals_E_No_Pnd_4807c_Empty_Form_Cnt_E_No", 
            "#4807C-EMPTY-FORM-CNT-E-NO", FieldType.PACKED_DECIMAL, 7);
        pnd_4807c_Control_Totals_E_No_Pnd_4807c_Hold_Cnt_E_No = pnd_4807c_Control_Totals_E_No.newFieldInGroup("pnd_4807c_Control_Totals_E_No_Pnd_4807c_Hold_Cnt_E_No", 
            "#4807C-HOLD-CNT-E-NO", FieldType.PACKED_DECIMAL, 7);
        pnd_4807c_Control_Totals_E_No_Pnd_4807c_Gross_Amt_E_No = pnd_4807c_Control_Totals_E_No.newFieldInGroup("pnd_4807c_Control_Totals_E_No_Pnd_4807c_Gross_Amt_E_No", 
            "#4807C-GROSS-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_No_Pnd_4807c_Int_Amt_E_No = pnd_4807c_Control_Totals_E_No.newFieldInGroup("pnd_4807c_Control_Totals_E_No_Pnd_4807c_Int_Amt_E_No", 
            "#4807C-INT-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_No_Pnd_4807c_Distrib_Amt_E_No = pnd_4807c_Control_Totals_E_No.newFieldInGroup("pnd_4807c_Control_Totals_E_No_Pnd_4807c_Distrib_Amt_E_No", 
            "#4807C-DISTRIB-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_No_Pnd_4807c_Taxable_Amt_E_No = pnd_4807c_Control_Totals_E_No.newFieldInGroup("pnd_4807c_Control_Totals_E_No_Pnd_4807c_Taxable_Amt_E_No", 
            "#4807C-TAXABLE-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_No_Pnd_4807c_Ivc_Amt_E_No = pnd_4807c_Control_Totals_E_No.newFieldInGroup("pnd_4807c_Control_Totals_E_No_Pnd_4807c_Ivc_Amt_E_No", 
            "#4807C-IVC-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_No_Pnd_4807c_Gross_Amt_Roll_E_No = pnd_4807c_Control_Totals_E_No.newFieldInGroup("pnd_4807c_Control_Totals_E_No_Pnd_4807c_Gross_Amt_Roll_E_No", 
            "#4807C-GROSS-AMT-ROLL-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_No_Pnd_4807c_Ivc_Amt_Roll_E_No = pnd_4807c_Control_Totals_E_No.newFieldInGroup("pnd_4807c_Control_Totals_E_No_Pnd_4807c_Ivc_Amt_Roll_E_No", 
            "#4807C-IVC-AMT-ROLL-E-NO", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_4807c_Control_Totals_Gr = localVariables.newGroupInRecord("pnd_4807c_Control_Totals_Gr", "#4807C-CONTROL-TOTALS-GR");
        pnd_4807c_Control_Totals_Gr_Pnd_4807c_Form_Cnt_Gr = pnd_4807c_Control_Totals_Gr.newFieldInGroup("pnd_4807c_Control_Totals_Gr_Pnd_4807c_Form_Cnt_Gr", 
            "#4807C-FORM-CNT-GR", FieldType.PACKED_DECIMAL, 7);
        pnd_4807c_Control_Totals_Gr_Pnd_4807c_Empty_Form_Cnt_Gr = pnd_4807c_Control_Totals_Gr.newFieldInGroup("pnd_4807c_Control_Totals_Gr_Pnd_4807c_Empty_Form_Cnt_Gr", 
            "#4807C-EMPTY-FORM-CNT-GR", FieldType.PACKED_DECIMAL, 7);
        pnd_4807c_Control_Totals_Gr_Pnd_4807c_Hold_Cnt_Gr = pnd_4807c_Control_Totals_Gr.newFieldInGroup("pnd_4807c_Control_Totals_Gr_Pnd_4807c_Hold_Cnt_Gr", 
            "#4807C-HOLD-CNT-GR", FieldType.PACKED_DECIMAL, 7);
        pnd_4807c_Control_Totals_Gr_Pnd_4807c_Gross_Amt_Gr = pnd_4807c_Control_Totals_Gr.newFieldInGroup("pnd_4807c_Control_Totals_Gr_Pnd_4807c_Gross_Amt_Gr", 
            "#4807C-GROSS-AMT-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_Gr_Pnd_4807c_Int_Amt_Gr = pnd_4807c_Control_Totals_Gr.newFieldInGroup("pnd_4807c_Control_Totals_Gr_Pnd_4807c_Int_Amt_Gr", 
            "#4807C-INT-AMT-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_Gr_Pnd_4807c_Distrib_Amt_Gr = pnd_4807c_Control_Totals_Gr.newFieldInGroup("pnd_4807c_Control_Totals_Gr_Pnd_4807c_Distrib_Amt_Gr", 
            "#4807C-DISTRIB-AMT-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_Gr_Pnd_4807c_Taxable_Amt_Gr = pnd_4807c_Control_Totals_Gr.newFieldInGroup("pnd_4807c_Control_Totals_Gr_Pnd_4807c_Taxable_Amt_Gr", 
            "#4807C-TAXABLE-AMT-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_Gr_Pnd_4807c_Ivc_Amt_Gr = pnd_4807c_Control_Totals_Gr.newFieldInGroup("pnd_4807c_Control_Totals_Gr_Pnd_4807c_Ivc_Amt_Gr", 
            "#4807C-IVC-AMT-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_Gr_Pnd_4807c_Gross_Amt_Roll_Gr = pnd_4807c_Control_Totals_Gr.newFieldInGroup("pnd_4807c_Control_Totals_Gr_Pnd_4807c_Gross_Amt_Roll_Gr", 
            "#4807C-GROSS-AMT-ROLL-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_Gr_Pnd_4807c_Ivc_Amt_Roll_Gr = pnd_4807c_Control_Totals_Gr.newFieldInGroup("pnd_4807c_Control_Totals_Gr_Pnd_4807c_Ivc_Amt_Roll_Gr", 
            "#4807C-IVC-AMT-ROLL-GR", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_4807c_Control_Totals_E_Gr = localVariables.newGroupInRecord("pnd_4807c_Control_Totals_E_Gr", "#4807C-CONTROL-TOTALS-E-GR");
        pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Form_Cnt_E_Gr = pnd_4807c_Control_Totals_E_Gr.newFieldInGroup("pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Form_Cnt_E_Gr", 
            "#4807C-FORM-CNT-E-GR", FieldType.PACKED_DECIMAL, 7);
        pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Empty_Form_Cnt_E_Gr = pnd_4807c_Control_Totals_E_Gr.newFieldInGroup("pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Empty_Form_Cnt_E_Gr", 
            "#4807C-EMPTY-FORM-CNT-E-GR", FieldType.PACKED_DECIMAL, 7);
        pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Hold_Cnt_E_Gr = pnd_4807c_Control_Totals_E_Gr.newFieldInGroup("pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Hold_Cnt_E_Gr", 
            "#4807C-HOLD-CNT-E-GR", FieldType.PACKED_DECIMAL, 7);
        pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Gross_Amt_E_Gr = pnd_4807c_Control_Totals_E_Gr.newFieldInGroup("pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Gross_Amt_E_Gr", 
            "#4807C-GROSS-AMT-E-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Int_Amt_E_Gr = pnd_4807c_Control_Totals_E_Gr.newFieldInGroup("pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Int_Amt_E_Gr", 
            "#4807C-INT-AMT-E-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Distrib_Amt_E_Gr = pnd_4807c_Control_Totals_E_Gr.newFieldInGroup("pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Distrib_Amt_E_Gr", 
            "#4807C-DISTRIB-AMT-E-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Taxable_Amt_E_Gr = pnd_4807c_Control_Totals_E_Gr.newFieldInGroup("pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Taxable_Amt_E_Gr", 
            "#4807C-TAXABLE-AMT-E-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Ivc_Amt_E_Gr = pnd_4807c_Control_Totals_E_Gr.newFieldInGroup("pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Ivc_Amt_E_Gr", 
            "#4807C-IVC-AMT-E-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Gross_Amt_Roll_E_Gr = pnd_4807c_Control_Totals_E_Gr.newFieldInGroup("pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Gross_Amt_Roll_E_Gr", 
            "#4807C-GROSS-AMT-ROLL-E-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Ivc_Amt_Roll_E_Gr = pnd_4807c_Control_Totals_E_Gr.newFieldInGroup("pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Ivc_Amt_Roll_E_Gr", 
            "#4807C-IVC-AMT-ROLL-E-GR", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_4807c_Control_Totals_E_No_Gr = localVariables.newGroupInRecord("pnd_4807c_Control_Totals_E_No_Gr", "#4807C-CONTROL-TOTALS-E-NO-GR");
        pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Form_Cnt_E_No_Gr = pnd_4807c_Control_Totals_E_No_Gr.newFieldInGroup("pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Form_Cnt_E_No_Gr", 
            "#4807C-FORM-CNT-E-NO-GR", FieldType.PACKED_DECIMAL, 7);
        pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Empty_Form_Cnt_E_No_Gr = pnd_4807c_Control_Totals_E_No_Gr.newFieldInGroup("pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Empty_Form_Cnt_E_No_Gr", 
            "#4807C-EMPTY-FORM-CNT-E-NO-GR", FieldType.PACKED_DECIMAL, 7);
        pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Hold_Cnt_E_No_Gr = pnd_4807c_Control_Totals_E_No_Gr.newFieldInGroup("pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Hold_Cnt_E_No_Gr", 
            "#4807C-HOLD-CNT-E-NO-GR", FieldType.PACKED_DECIMAL, 7);
        pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Gross_Amt_E_No_Gr = pnd_4807c_Control_Totals_E_No_Gr.newFieldInGroup("pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Gross_Amt_E_No_Gr", 
            "#4807C-GROSS-AMT-E-NO-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Int_Amt_E_No_Gr = pnd_4807c_Control_Totals_E_No_Gr.newFieldInGroup("pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Int_Amt_E_No_Gr", 
            "#4807C-INT-AMT-E-NO-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Distrib_Amt_E_No_Gr = pnd_4807c_Control_Totals_E_No_Gr.newFieldInGroup("pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Distrib_Amt_E_No_Gr", 
            "#4807C-DISTRIB-AMT-E-NO-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Taxable_Amt_E_No_Gr = pnd_4807c_Control_Totals_E_No_Gr.newFieldInGroup("pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Taxable_Amt_E_No_Gr", 
            "#4807C-TAXABLE-AMT-E-NO-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Ivc_Amt_E_No_Gr = pnd_4807c_Control_Totals_E_No_Gr.newFieldInGroup("pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Ivc_Amt_E_No_Gr", 
            "#4807C-IVC-AMT-E-NO-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Gross_Amt_Roll_E_No_Gr = pnd_4807c_Control_Totals_E_No_Gr.newFieldInGroup("pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Gross_Amt_Roll_E_No_Gr", 
            "#4807C-GROSS-AMT-ROLL-E-NO-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Ivc_Amt_Roll_E_No_Gr = pnd_4807c_Control_Totals_E_No_Gr.newFieldInGroup("pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Ivc_Amt_Roll_E_No_Gr", 
            "#4807C-IVC-AMT-ROLL-E-NO-GR", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Nr4_Control_Totals = localVariables.newGroupArrayInRecord("pnd_Nr4_Control_Totals", "#NR4-CONTROL-TOTALS", new DbsArrayController(1, 4));
        pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt", "#NR4-FORM-CNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt_E = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt_E", "#NR4-FORM-CNT-E", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt_E_No = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt_E_No", "#NR4-FORM-CNT-E-NO", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Nr4_Control_Totals_Pnd_Nr4_Hold_Cnt = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Hold_Cnt", "#NR4-HOLD-CNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Nr4_Control_Totals_Pnd_Nr4_Income_Code = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Income_Code", "#NR4-INCOME-CODE", 
            FieldType.STRING, 2);
        pnd_Nr4_Control_Totals_Pnd_Nr4_Income_Code_E = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Income_Code_E", "#NR4-INCOME-CODE-E", 
            FieldType.STRING, 2);
        pnd_Nr4_Control_Totals_Pnd_Nr4_Income_Code_E_No = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Income_Code_E_No", "#NR4-INCOME-CODE-E-NO", 
            FieldType.STRING, 2);
        pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt", "#NR4-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt_E = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt_E", "#NR4-GROSS-AMT-E", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt_E_No = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt_E_No", "#NR4-GROSS-AMT-E-NO", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt", "#NR4-INTEREST-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt_E = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt_E", "#NR4-INTEREST-AMT-E", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt_E_No = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt_E_No", 
            "#NR4-INTEREST-AMT-E-NO", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld", "#NR4-FED-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld_E = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld_E", "#NR4-FED-TAX-WTHLD-E", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld_E_No = pnd_Nr4_Control_Totals.newFieldInGroup("pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld_E_No", 
            "#NR4-FED-TAX-WTHLD-E-NO", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Nr4_Control_Totals_Gr = localVariables.newGroupArrayInRecord("pnd_Nr4_Control_Totals_Gr", "#NR4-CONTROL-TOTALS-GR", new DbsArrayController(1, 
            4));
        pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Form_Cnt_Gr = pnd_Nr4_Control_Totals_Gr.newFieldInGroup("pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Form_Cnt_Gr", "#NR4-FORM-CNT-GR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Form_Cnt_E_Gr = pnd_Nr4_Control_Totals_Gr.newFieldInGroup("pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Form_Cnt_E_Gr", 
            "#NR4-FORM-CNT-E-GR", FieldType.PACKED_DECIMAL, 7);
        pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Form_Cnt_E_No_Gr = pnd_Nr4_Control_Totals_Gr.newFieldInGroup("pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Form_Cnt_E_No_Gr", 
            "#NR4-FORM-CNT-E-NO-GR", FieldType.PACKED_DECIMAL, 7);
        pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Hold_Cnt_Gr = pnd_Nr4_Control_Totals_Gr.newFieldInGroup("pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Hold_Cnt_Gr", "#NR4-HOLD-CNT-GR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Income_Code_Gr = pnd_Nr4_Control_Totals_Gr.newFieldInGroup("pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Income_Code_Gr", 
            "#NR4-INCOME-CODE-GR", FieldType.STRING, 2);
        pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Income_Code_E_Gr = pnd_Nr4_Control_Totals_Gr.newFieldInGroup("pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Income_Code_E_Gr", 
            "#NR4-INCOME-CODE-E-GR", FieldType.STRING, 2);
        pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Income_Code_E_No_Gr = pnd_Nr4_Control_Totals_Gr.newFieldInGroup("pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Income_Code_E_No_Gr", 
            "#NR4-INCOME-CODE-E-NO-GR", FieldType.STRING, 2);
        pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Gross_Amt_Gr = pnd_Nr4_Control_Totals_Gr.newFieldInGroup("pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Gross_Amt_Gr", "#NR4-GROSS-AMT-GR", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Gross_Amt_E_Gr = pnd_Nr4_Control_Totals_Gr.newFieldInGroup("pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Gross_Amt_E_Gr", 
            "#NR4-GROSS-AMT-E-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Gross_Amt_E_No_Gr = pnd_Nr4_Control_Totals_Gr.newFieldInGroup("pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Gross_Amt_E_No_Gr", 
            "#NR4-GROSS-AMT-E-NO-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Interest_Amt_Gr = pnd_Nr4_Control_Totals_Gr.newFieldInGroup("pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Interest_Amt_Gr", 
            "#NR4-INTEREST-AMT-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Interest_Amt_E_Gr = pnd_Nr4_Control_Totals_Gr.newFieldInGroup("pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Interest_Amt_E_Gr", 
            "#NR4-INTEREST-AMT-E-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Interest_Amt_E_No_Gr = pnd_Nr4_Control_Totals_Gr.newFieldInGroup("pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Interest_Amt_E_No_Gr", 
            "#NR4-INTEREST-AMT-E-NO-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Fed_Tax_Wthld_Gr = pnd_Nr4_Control_Totals_Gr.newFieldInGroup("pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Fed_Tax_Wthld_Gr", 
            "#NR4-FED-TAX-WTHLD-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Fed_Tax_Wthld_E_Gr = pnd_Nr4_Control_Totals_Gr.newFieldInGroup("pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Fed_Tax_Wthld_E_Gr", 
            "#NR4-FED-TAX-WTHLD-E-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Fed_Tax_Wthld_E_No_Gr = pnd_Nr4_Control_Totals_Gr.newFieldInGroup("pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Fed_Tax_Wthld_E_No_Gr", 
            "#NR4-FED-TAX-WTHLD-E-NO-GR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Form_Cnt = localVariables.newFieldInRecord("pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Form_Cnt_E = localVariables.newFieldInRecord("pnd_Form_Cnt_E", "#FORM-CNT-E", FieldType.PACKED_DECIMAL, 7);
        pnd_Form_Cnt_E_No = localVariables.newFieldInRecord("pnd_Form_Cnt_E_No", "#FORM-CNT-E-NO", FieldType.PACKED_DECIMAL, 7);
        pnd_Form_Cnt_Fatca = localVariables.newFieldArrayInRecord("pnd_Form_Cnt_Fatca", "#FORM-CNT-FATCA", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            2));
        pnd_Form_Cnt_E_Fatca = localVariables.newFieldArrayInRecord("pnd_Form_Cnt_E_Fatca", "#FORM-CNT-E-FATCA", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            2));
        pnd_Form_Cnt_E_No_Fatca = localVariables.newFieldArrayInRecord("pnd_Form_Cnt_E_No_Fatca", "#FORM-CNT-E-NO-FATCA", FieldType.PACKED_DECIMAL, 7, 
            new DbsArrayController(1, 2));
        pnd_Hold_Cnt = localVariables.newFieldInRecord("pnd_Hold_Cnt", "#HOLD-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Empty_Form_Cnt = localVariables.newFieldInRecord("pnd_Empty_Form_Cnt", "#EMPTY-FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tirf_Superde_3_S = localVariables.newFieldInRecord("pnd_Tirf_Superde_3_S", "#TIRF-SUPERDE-3-S", FieldType.STRING, 19);
        pnd_Tirf_Superde_3_E = localVariables.newFieldInRecord("pnd_Tirf_Superde_3_E", "#TIRF-SUPERDE-3-E", FieldType.STRING, 19);
        pnd_S3 = localVariables.newFieldInRecord("pnd_S3", "#S3", FieldType.STRING, 7);

        pnd_S3__R_Field_9 = localVariables.newGroupInRecord("pnd_S3__R_Field_9", "REDEFINE", pnd_S3);
        pnd_S3_Pnd_Tax_Year = pnd_S3__R_Field_9.newFieldInGroup("pnd_S3_Pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);
        pnd_S3_Pnd_Status = pnd_S3__R_Field_9.newFieldInGroup("pnd_S3_Pnd_Status", "#STATUS", FieldType.STRING, 1);
        pnd_S3_Pnd_Form = pnd_S3__R_Field_9.newFieldInGroup("pnd_S3_Pnd_Form", "#FORM", FieldType.NUMERIC, 2);
        low_Values = localVariables.newFieldInRecord("low_Values", "LOW-VALUES", FieldType.STRING, 1);
        high_Values = localVariables.newFieldInRecord("high_Values", "HIGH-VALUES", FieldType.STRING, 1);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Company_Ndx = localVariables.newFieldInRecord("pnd_Company_Ndx", "#COMPANY-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Err_Ndx = localVariables.newFieldInRecord("pnd_Err_Ndx", "#ERR-NDX", FieldType.NUMERIC, 3);
        pnd_Nr4_Ndx = localVariables.newFieldInRecord("pnd_Nr4_Ndx", "#NR4-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Sys_Err_Ndx = localVariables.newFieldInRecord("pnd_Sys_Err_Ndx", "#SYS-ERR-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Reportable_Ndx = localVariables.newFieldInRecord("pnd_Reportable_Ndx", "#REPORTABLE-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_State_Ndx = localVariables.newFieldInRecord("pnd_State_Ndx", "#STATE-NDX", FieldType.PACKED_DECIMAL, 3);

        pnd_Summary_Amounts = localVariables.newGroupInRecord("pnd_Summary_Amounts", "#SUMMARY-AMOUNTS");
        pnd_Summary_Amounts_Pnd_Summ_State_Form_Cnt = pnd_Summary_Amounts.newFieldInGroup("pnd_Summary_Amounts_Pnd_Summ_State_Form_Cnt", "#SUMM-STATE-FORM-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Summary_Amounts_Pnd_Summ_State_Distr_Amt = pnd_Summary_Amounts.newFieldInGroup("pnd_Summary_Amounts_Pnd_Summ_State_Distr_Amt", "#SUMM-STATE-DISTR-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Amounts_Pnd_Summ_State_Wthhld_Amt = pnd_Summary_Amounts.newFieldInGroup("pnd_Summary_Amounts_Pnd_Summ_State_Wthhld_Amt", "#SUMM-STATE-WTHHLD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Amounts_Pnd_Summ_Local_Distr_Amt = pnd_Summary_Amounts.newFieldInGroup("pnd_Summary_Amounts_Pnd_Summ_Local_Distr_Amt", "#SUMM-LOCAL-DISTR-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Amounts_Pnd_Summ_Local_Wthhld_Amt = pnd_Summary_Amounts.newFieldInGroup("pnd_Summary_Amounts_Pnd_Summ_Local_Wthhld_Amt", "#SUMM-LOCAL-WTHHLD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Amounts_Pnd_Summ_Res_Gross_Amt = pnd_Summary_Amounts.newFieldInGroup("pnd_Summary_Amounts_Pnd_Summ_Res_Gross_Amt", "#SUMM-RES-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Amounts_Pnd_Summ_Res_Taxable_Amt = pnd_Summary_Amounts.newFieldInGroup("pnd_Summary_Amounts_Pnd_Summ_Res_Taxable_Amt", "#SUMM-RES-TAXABLE-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Amounts_Pnd_Summ_Res_Ivc_Amt = pnd_Summary_Amounts.newFieldInGroup("pnd_Summary_Amounts_Pnd_Summ_Res_Ivc_Amt", "#SUMM-RES-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Amounts_Pnd_Summ_Res_Fed_Tax_Wthld = pnd_Summary_Amounts.newFieldInGroup("pnd_Summary_Amounts_Pnd_Summ_Res_Fed_Tax_Wthld", "#SUMM-RES-FED-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Amounts_Pnd_Summ_Res_Irr_Amt = pnd_Summary_Amounts.newFieldInGroup("pnd_Summary_Amounts_Pnd_Summ_Res_Irr_Amt", "#SUMM-RES-IRR-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Sys_Err_Cnt = localVariables.newFieldArrayInRecord("pnd_Sys_Err_Cnt", "#SYS-ERR-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            61));
        pnd_Form_Key = localVariables.newFieldInRecord("pnd_Form_Key", "#FORM-KEY", FieldType.STRING, 5);

        pnd_Form_Key__R_Field_10 = localVariables.newGroupInRecord("pnd_Form_Key__R_Field_10", "REDEFINE", pnd_Form_Key);
        pnd_Form_Key_Pnd_Tax_Year = pnd_Form_Key__R_Field_10.newFieldInGroup("pnd_Form_Key_Pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);
        pnd_Form_Key_Pnd_Status = pnd_Form_Key__R_Field_10.newFieldInGroup("pnd_Form_Key_Pnd_Status", "#STATUS", FieldType.STRING, 1);
        pnd_Hist_Key = localVariables.newFieldInRecord("pnd_Hist_Key", "#HIST-KEY", FieldType.STRING, 39);

        pnd_Hist_Key__R_Field_11 = localVariables.newGroupInRecord("pnd_Hist_Key__R_Field_11", "REDEFINE", pnd_Hist_Key);
        pnd_Hist_Key_Pnd_Tax_Year = pnd_Hist_Key__R_Field_11.newFieldInGroup("pnd_Hist_Key_Pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);
        pnd_Hist_Key_Pnd_Tin = pnd_Hist_Key__R_Field_11.newFieldInGroup("pnd_Hist_Key_Pnd_Tin", "#TIN", FieldType.STRING, 10);
        pnd_Hist_Key_Pnd_Form_Type = pnd_Hist_Key__R_Field_11.newFieldInGroup("pnd_Hist_Key_Pnd_Form_Type", "#FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_Hist_Key_Pnd_Contract_Nbr = pnd_Hist_Key__R_Field_11.newFieldInGroup("pnd_Hist_Key_Pnd_Contract_Nbr", "#CONTRACT-NBR", FieldType.STRING, 8);
        pnd_Hist_Key_Pnd_Payee = pnd_Hist_Key__R_Field_11.newFieldInGroup("pnd_Hist_Key_Pnd_Payee", "#PAYEE", FieldType.STRING, 2);
        pnd_Hist_Key_Pnd_Key = pnd_Hist_Key__R_Field_11.newFieldInGroup("pnd_Hist_Key_Pnd_Key", "#KEY", FieldType.STRING, 5);
        pnd_Hist_Key_Pnd_Ts = pnd_Hist_Key__R_Field_11.newFieldInGroup("pnd_Hist_Key_Pnd_Ts", "#TS", FieldType.TIME);
        pnd_Hist_Key_Pnd_Irs_Rpt_Ind = pnd_Hist_Key__R_Field_11.newFieldInGroup("pnd_Hist_Key_Pnd_Irs_Rpt_Ind", "#IRS-RPT-IND", FieldType.STRING, 1);
        pnd_Sys_Date = localVariables.newFieldInRecord("pnd_Sys_Date", "#SYS-DATE", FieldType.DATE);
        pnd_Sys_Date_N = localVariables.newFieldInRecord("pnd_Sys_Date_N", "#SYS-DATE-N", FieldType.NUMERIC, 8);
        pnd_Sys_Time = localVariables.newFieldInRecord("pnd_Sys_Time", "#SYS-TIME", FieldType.TIME);
        pnd_Err_Msg = localVariables.newFieldInRecord("pnd_Err_Msg", "#ERR-MSG", FieldType.STRING, 60);
        pnd_Read_Cnt_In = localVariables.newFieldInRecord("pnd_Read_Cnt_In", "#READ-CNT-IN", FieldType.PACKED_DECIMAL, 7);
        pnd_Error_Cnt = localVariables.newFieldInRecord("pnd_Error_Cnt", "#ERROR-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Et_Cnt = localVariables.newFieldInRecord("pnd_Et_Cnt", "#ET-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_1042s_Max = localVariables.newFieldInRecord("pnd_1042s_Max", "#1042S-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Old_Form_Type = localVariables.newFieldInRecord("pnd_Old_Form_Type", "#OLD-FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_Old_Company_Cde = localVariables.newFieldInRecord("pnd_Old_Company_Cde", "#OLD-COMPANY-CDE", FieldType.STRING, 1);
        pnd_C_1099_R = localVariables.newFieldInRecord("pnd_C_1099_R", "#C-1099-R", FieldType.PACKED_DECIMAL, 3);
        pnd_C_1042_S = localVariables.newFieldInRecord("pnd_C_1042_S", "#C-1042-S", FieldType.PACKED_DECIMAL, 3);
        pnd_Nr4_Income_Desc = localVariables.newFieldInRecord("pnd_Nr4_Income_Desc", "#NR4-INCOME-DESC", FieldType.STRING, 16);
        pnd_State_Ind_N = localVariables.newFieldInRecord("pnd_State_Ind_N", "#STATE-IND-N", FieldType.NUMERIC, 1);
        pnd_Reported_To_Irs = localVariables.newFieldInRecord("pnd_Reported_To_Irs", "#REPORTED-TO-IRS", FieldType.BOOLEAN, 1);
        pnd_E_Deliver = localVariables.newFieldInRecord("pnd_E_Deliver", "#E-DELIVER", FieldType.BOOLEAN, 1);
        pnd_Customer_Id = localVariables.newFieldInRecord("pnd_Customer_Id", "#CUSTOMER-ID", FieldType.STRING, 15);

        pnd_Customer_Id__R_Field_12 = localVariables.newGroupInRecord("pnd_Customer_Id__R_Field_12", "REDEFINE", pnd_Customer_Id);
        pnd_Customer_Id_Pnd_Pin = pnd_Customer_Id__R_Field_12.newFieldInGroup("pnd_Customer_Id_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_Form_Cnt_Comp = localVariables.newFieldInRecord("pnd_Form_Cnt_Comp", "#FORM-CNT-COMP", FieldType.PACKED_DECIMAL, 7);
        pnd_Form_Cnt_Comp_E = localVariables.newFieldInRecord("pnd_Form_Cnt_Comp_E", "#FORM-CNT-COMP-E", FieldType.PACKED_DECIMAL, 7);
        pnd_Form_Cnt_Comp_E_No = localVariables.newFieldInRecord("pnd_Form_Cnt_Comp_E_No", "#FORM-CNT-COMP-E-NO", FieldType.PACKED_DECIMAL, 7);
        pnd_Form_Cnt_Comp_1099 = localVariables.newFieldInRecord("pnd_Form_Cnt_Comp_1099", "#FORM-CNT-COMP-1099", FieldType.PACKED_DECIMAL, 7);
        pnd_Form_Cnt_Comp_1099r = localVariables.newFieldInRecord("pnd_Form_Cnt_Comp_1099r", "#FORM-CNT-COMP-1099R", FieldType.PACKED_DECIMAL, 7);
        pnd_Form_Cnt_Comp_1099int = localVariables.newFieldInRecord("pnd_Form_Cnt_Comp_1099int", "#FORM-CNT-COMP-1099INT", FieldType.PACKED_DECIMAL, 7);
        pnd_Form_Cnt_Comp_E_1099 = localVariables.newFieldInRecord("pnd_Form_Cnt_Comp_E_1099", "#FORM-CNT-COMP-E-1099", FieldType.PACKED_DECIMAL, 7);
        pnd_Form_Cnt_Comp_E_No_1099 = localVariables.newFieldInRecord("pnd_Form_Cnt_Comp_E_No_1099", "#FORM-CNT-COMP-E-NO-1099", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Form_Cnt_Comp_1042s = localVariables.newFieldInRecord("pnd_Form_Cnt_Comp_1042s", "#FORM-CNT-COMP-1042S", FieldType.PACKED_DECIMAL, 7);
        pnd_Form_Cnt_Comp_E_1042s = localVariables.newFieldInRecord("pnd_Form_Cnt_Comp_E_1042s", "#FORM-CNT-COMP-E-1042S", FieldType.PACKED_DECIMAL, 7);
        pnd_Form_Cnt_Comp_E_No_1042s = localVariables.newFieldInRecord("pnd_Form_Cnt_Comp_E_No_1042s", "#FORM-CNT-COMP-E-NO-1042S", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Form_Cnt_Comp_1042s_Fatca = localVariables.newFieldArrayInRecord("pnd_Form_Cnt_Comp_1042s_Fatca", "#FORM-CNT-COMP-1042S-FATCA", FieldType.PACKED_DECIMAL, 
            7, new DbsArrayController(1, 2));
        pnd_Form_Cnt_Comp_E_1042s_Fatca = localVariables.newFieldArrayInRecord("pnd_Form_Cnt_Comp_E_1042s_Fatca", "#FORM-CNT-COMP-E-1042S-FATCA", FieldType.PACKED_DECIMAL, 
            7, new DbsArrayController(1, 2));
        pnd_Form_Cnt_Comp_E_No_1042s_Fatca = localVariables.newFieldArrayInRecord("pnd_Form_Cnt_Comp_E_No_1042s_Fatca", "#FORM-CNT-COMP-E-NO-1042S-FATCA", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 2));
        pnd_Form_Cnt_Comp_Grand = localVariables.newFieldInRecord("pnd_Form_Cnt_Comp_Grand", "#FORM-CNT-COMP-GRAND", FieldType.PACKED_DECIMAL, 7);
        pnd_Form_Cnt_Comp_E_Grand = localVariables.newFieldInRecord("pnd_Form_Cnt_Comp_E_Grand", "#FORM-CNT-COMP-E-GRAND", FieldType.PACKED_DECIMAL, 7);
        pnd_Form_Cnt_Comp_E_No_Grand = localVariables.newFieldInRecord("pnd_Form_Cnt_Comp_E_No_Grand", "#FORM-CNT-COMP-E-NO-GRAND", FieldType.PACKED_DECIMAL, 
            7);
        pnd_1099r_Loop = localVariables.newFieldInRecord("pnd_1099r_Loop", "#1099R-LOOP", FieldType.NUMERIC, 1);
        pnd_Print_1099_Loop = localVariables.newFieldInRecord("pnd_Print_1099_Loop", "#PRINT-1099-LOOP", FieldType.NUMERIC, 1);
        pnd_Form_Cnt_All_1099r = localVariables.newFieldInRecord("pnd_Form_Cnt_All_1099r", "#FORM-CNT-ALL-1099R", FieldType.PACKED_DECIMAL, 7);
        pnd_1099r_Gross_Amt_All_1099r = localVariables.newFieldInRecord("pnd_1099r_Gross_Amt_All_1099r", "#1099R-GROSS-AMT-ALL-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_Ivc_Amt_All_1099r = localVariables.newFieldInRecord("pnd_1099r_Ivc_Amt_All_1099r", "#1099R-IVC-AMT-ALL-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_Taxable_Amt_All_1099r = localVariables.newFieldInRecord("pnd_1099r_Taxable_Amt_All_1099r", "#1099R-TAXABLE-AMT-ALL-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_State_Tax_Wthld_All_1099r = localVariables.newFieldInRecord("pnd_1099r_State_Tax_Wthld_All_1099r", "#1099R-STATE-TAX-WTHLD-ALL-1099R", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099i_Int_Amt_All_1099r = localVariables.newFieldInRecord("pnd_1099i_Int_Amt_All_1099r", "#1099I-INT-AMT-ALL-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_Loc_Tax_Wthld_All_1099r = localVariables.newFieldInRecord("pnd_1099r_Loc_Tax_Wthld_All_1099r", "#1099R-LOC-TAX-WTHLD-ALL-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099_Fed_Tax_Wthld_All_1099r = localVariables.newFieldInRecord("pnd_1099_Fed_Tax_Wthld_All_1099r", "#1099-FED-TAX-WTHLD-ALL-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_Irr_Amt_All_1099r = localVariables.newFieldInRecord("pnd_1099r_Irr_Amt_All_1099r", "#1099R-IRR-AMT-ALL-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Form_Cnt_All_1099int = localVariables.newFieldInRecord("pnd_Form_Cnt_All_1099int", "#FORM-CNT-ALL-1099INT", FieldType.PACKED_DECIMAL, 7);
        pnd_1099r_Gross_Amt_All_1099int = localVariables.newFieldInRecord("pnd_1099r_Gross_Amt_All_1099int", "#1099R-GROSS-AMT-ALL-1099INT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_Ivc_Amt_All_1099int = localVariables.newFieldInRecord("pnd_1099r_Ivc_Amt_All_1099int", "#1099R-IVC-AMT-ALL-1099INT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_Taxable_Amt_All_1099int = localVariables.newFieldInRecord("pnd_1099r_Taxable_Amt_All_1099int", "#1099R-TAXABLE-AMT-ALL-1099INT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_State_Tax_Wthld_All_1099int = localVariables.newFieldInRecord("pnd_1099r_State_Tax_Wthld_All_1099int", "#1099R-STATE-TAX-WTHLD-ALL-1099INT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099i_Int_Amt_All_1099int = localVariables.newFieldInRecord("pnd_1099i_Int_Amt_All_1099int", "#1099I-INT-AMT-ALL-1099INT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_Loc_Tax_Wthld_All_1099int = localVariables.newFieldInRecord("pnd_1099r_Loc_Tax_Wthld_All_1099int", "#1099R-LOC-TAX-WTHLD-ALL-1099INT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Fed_Tax_Wthld_All_1099int = localVariables.newFieldInRecord("pnd_1099_Fed_Tax_Wthld_All_1099int", "#1099-FED-TAX-WTHLD-ALL-1099INT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_Irr_Amt_All_1099int = localVariables.newFieldInRecord("pnd_1099r_Irr_Amt_All_1099int", "#1099R-IRR-AMT-ALL-1099INT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Form_Cnt_All_1099tot = localVariables.newFieldInRecord("pnd_Form_Cnt_All_1099tot", "#FORM-CNT-ALL-1099TOT", FieldType.PACKED_DECIMAL, 7);
        pnd_1099r_Gross_Amt_All_1099tot = localVariables.newFieldInRecord("pnd_1099r_Gross_Amt_All_1099tot", "#1099R-GROSS-AMT-ALL-1099TOT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_Ivc_Amt_All_1099tot = localVariables.newFieldInRecord("pnd_1099r_Ivc_Amt_All_1099tot", "#1099R-IVC-AMT-ALL-1099TOT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_Taxable_Amt_All_1099tot = localVariables.newFieldInRecord("pnd_1099r_Taxable_Amt_All_1099tot", "#1099R-TAXABLE-AMT-ALL-1099TOT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_State_Tax_Wthld_All_1099tot = localVariables.newFieldInRecord("pnd_1099r_State_Tax_Wthld_All_1099tot", "#1099R-STATE-TAX-WTHLD-ALL-1099TOT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099i_Int_Amt_All_1099tot = localVariables.newFieldInRecord("pnd_1099i_Int_Amt_All_1099tot", "#1099I-INT-AMT-ALL-1099TOT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_Loc_Tax_Wthld_All_1099tot = localVariables.newFieldInRecord("pnd_1099r_Loc_Tax_Wthld_All_1099tot", "#1099R-LOC-TAX-WTHLD-ALL-1099TOT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099_Fed_Tax_Wthld_All_1099tot = localVariables.newFieldInRecord("pnd_1099_Fed_Tax_Wthld_All_1099tot", "#1099-FED-TAX-WTHLD-ALL-1099TOT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_Irr_Amt_All_1099tot = localVariables.newFieldInRecord("pnd_1099r_Irr_Amt_All_1099tot", "#1099R-IRR-AMT-ALL-1099TOT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Form_Cnt_E_1099r = localVariables.newFieldInRecord("pnd_Form_Cnt_E_1099r", "#FORM-CNT-E-1099R", FieldType.PACKED_DECIMAL, 7);
        pnd_1099r_Gross_Amt_E_1099r = localVariables.newFieldInRecord("pnd_1099r_Gross_Amt_E_1099r", "#1099R-GROSS-AMT-E-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_Ivc_Amt_E_1099r = localVariables.newFieldInRecord("pnd_1099r_Ivc_Amt_E_1099r", "#1099R-IVC-AMT-E-1099R", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_1099r_Taxable_Amt_E_1099r = localVariables.newFieldInRecord("pnd_1099r_Taxable_Amt_E_1099r", "#1099R-TAXABLE-AMT-E-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099i_Int_Amt_E_1099r = localVariables.newFieldInRecord("pnd_1099i_Int_Amt_E_1099r", "#1099I-INT-AMT-E-1099R", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_1099_Fed_Tax_Wthld_E_1099r = localVariables.newFieldInRecord("pnd_1099_Fed_Tax_Wthld_E_1099r", "#1099-FED-TAX-WTHLD-E-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_State_Tax_Wthld_E_1099r = localVariables.newFieldInRecord("pnd_1099r_State_Tax_Wthld_E_1099r", "#1099R-STATE-TAX-WTHLD-E-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_Loc_Tax_Wthld_E_1099r = localVariables.newFieldInRecord("pnd_1099r_Loc_Tax_Wthld_E_1099r", "#1099R-LOC-TAX-WTHLD-E-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_Irr_Amt_E_1099r = localVariables.newFieldInRecord("pnd_1099r_Irr_Amt_E_1099r", "#1099R-IRR-AMT-E-1099R", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Form_Cnt_E_No_1099r = localVariables.newFieldInRecord("pnd_Form_Cnt_E_No_1099r", "#FORM-CNT-E-NO-1099R", FieldType.PACKED_DECIMAL, 7);
        pnd_1099r_Gross_Amt_E_No_1099r = localVariables.newFieldInRecord("pnd_1099r_Gross_Amt_E_No_1099r", "#1099R-GROSS-AMT-E-NO-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_Ivc_Amt_E_No_1099r = localVariables.newFieldInRecord("pnd_1099r_Ivc_Amt_E_No_1099r", "#1099R-IVC-AMT-E-NO-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_Taxable_Amt_E_No_1099r = localVariables.newFieldInRecord("pnd_1099r_Taxable_Amt_E_No_1099r", "#1099R-TAXABLE-AMT-E-NO-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099i_Int_Amt_E_No_1099r = localVariables.newFieldInRecord("pnd_1099i_Int_Amt_E_No_1099r", "#1099I-INT-AMT-E-NO-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099_Fed_Tax_Wthld_E_No_1099r = localVariables.newFieldInRecord("pnd_1099_Fed_Tax_Wthld_E_No_1099r", "#1099-FED-TAX-WTHLD-E-NO-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_State_Tax_Wthld_E_No_109r = localVariables.newFieldInRecord("pnd_1099r_State_Tax_Wthld_E_No_109r", "#1099R-STATE-TAX-WTHLD-E-NO-109R", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_Loc_Tax_Wthld_E_No_1099r = localVariables.newFieldInRecord("pnd_1099r_Loc_Tax_Wthld_E_No_1099r", "#1099R-LOC-TAX-WTHLD-E-NO-1099R", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_Irr_Amt_E_No_1099r = localVariables.newFieldInRecord("pnd_1099r_Irr_Amt_E_No_1099r", "#1099R-IRR-AMT-E-NO-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Form_Cnt_E_1099i = localVariables.newFieldInRecord("pnd_Form_Cnt_E_1099i", "#FORM-CNT-E-1099I", FieldType.PACKED_DECIMAL, 7);
        pnd_1099r_Gross_Amt_E_1099i = localVariables.newFieldInRecord("pnd_1099r_Gross_Amt_E_1099i", "#1099R-GROSS-AMT-E-1099I", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_Ivc_Amt_E_1099i = localVariables.newFieldInRecord("pnd_1099r_Ivc_Amt_E_1099i", "#1099R-IVC-AMT-E-1099I", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_1099r_Taxable_Amt_E_1099i = localVariables.newFieldInRecord("pnd_1099r_Taxable_Amt_E_1099i", "#1099R-TAXABLE-AMT-E-1099I", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099i_Int_Amt_E_1099i = localVariables.newFieldInRecord("pnd_1099i_Int_Amt_E_1099i", "#1099I-INT-AMT-E-1099I", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_1099_Fed_Tax_Wthld_E_1099i = localVariables.newFieldInRecord("pnd_1099_Fed_Tax_Wthld_E_1099i", "#1099-FED-TAX-WTHLD-E-1099I", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_State_Tax_Wthld_E_1099i = localVariables.newFieldInRecord("pnd_1099r_State_Tax_Wthld_E_1099i", "#1099R-STATE-TAX-WTHLD-E-1099I", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_Loc_Tax_Wthld_E_1099i = localVariables.newFieldInRecord("pnd_1099r_Loc_Tax_Wthld_E_1099i", "#1099R-LOC-TAX-WTHLD-E-1099I", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_Irr_Amt_E_1099i = localVariables.newFieldInRecord("pnd_1099r_Irr_Amt_E_1099i", "#1099R-IRR-AMT-E-1099I", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Form_Cnt_E_No_1099i = localVariables.newFieldInRecord("pnd_Form_Cnt_E_No_1099i", "#FORM-CNT-E-NO-1099I", FieldType.PACKED_DECIMAL, 7);
        pnd_1099r_Gross_Amt_E_No_1099i = localVariables.newFieldInRecord("pnd_1099r_Gross_Amt_E_No_1099i", "#1099R-GROSS-AMT-E-NO-1099I", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_Ivc_Amt_E_No_1099i = localVariables.newFieldInRecord("pnd_1099r_Ivc_Amt_E_No_1099i", "#1099R-IVC-AMT-E-NO-1099I", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_Taxable_Amt_E_No_1099i = localVariables.newFieldInRecord("pnd_1099r_Taxable_Amt_E_No_1099i", "#1099R-TAXABLE-AMT-E-NO-1099I", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099i_Int_Amt_E_No_1099i = localVariables.newFieldInRecord("pnd_1099i_Int_Amt_E_No_1099i", "#1099I-INT-AMT-E-NO-1099I", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099_Fed_Tax_Wthld_E_No_1099i = localVariables.newFieldInRecord("pnd_1099_Fed_Tax_Wthld_E_No_1099i", "#1099-FED-TAX-WTHLD-E-NO-1099I", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1099r_State_Tax_Wthld_E_No_109i = localVariables.newFieldInRecord("pnd_1099r_State_Tax_Wthld_E_No_109i", "#1099R-STATE-TAX-WTHLD-E-NO-109I", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_Loc_Tax_Wthld_E_No_1099i = localVariables.newFieldInRecord("pnd_1099r_Loc_Tax_Wthld_E_No_1099i", "#1099R-LOC-TAX-WTHLD-E-NO-1099I", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1099r_Irr_Amt_E_No_1099i = localVariables.newFieldInRecord("pnd_1099r_Irr_Amt_E_No_1099i", "#1099R-IRR-AMT-E-NO-1099I", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Total_1099_Form_Cnt_1099r = localVariables.newFieldInRecord("pnd_Total_1099_Form_Cnt_1099r", "#TOTAL-1099-FORM-CNT-1099R", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Total_1099_Gross_Amt_1099r = localVariables.newFieldInRecord("pnd_Total_1099_Gross_Amt_1099r", "#TOTAL-1099-GROSS-AMT-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Total_1099_Taxable_Amt_1099r = localVariables.newFieldInRecord("pnd_Total_1099_Taxable_Amt_1099r", "#TOTAL-1099-TAXABLE-AMT-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Total_1099_Fed_Tax_Wthld_1099r = localVariables.newFieldInRecord("pnd_Total_1099_Fed_Tax_Wthld_1099r", "#TOTAL-1099-FED-TAX-WTHLD-1099R", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_1099_Ivc_Amt_1099r = localVariables.newFieldInRecord("pnd_Total_1099_Ivc_Amt_1099r", "#TOTAL-1099-IVC-AMT-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Total_1099_State_Tax_Wthld_1099r = localVariables.newFieldInRecord("pnd_Total_1099_State_Tax_Wthld_1099r", "#TOTAL-1099-STATE-TAX-WTHLD-1099R", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_1099_Loc_Tax_Wthld_1099r = localVariables.newFieldInRecord("pnd_Total_1099_Loc_Tax_Wthld_1099r", "#TOTAL-1099-LOC-TAX-WTHLD-1099R", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_1099_Int_Amt_1099r = localVariables.newFieldInRecord("pnd_Total_1099_Int_Amt_1099r", "#TOTAL-1099-INT-AMT-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Total_1099_Irr_Amt_1099r = localVariables.newFieldInRecord("pnd_Total_1099_Irr_Amt_1099r", "#TOTAL-1099-IRR-AMT-1099R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Total_1099_Form_Cnt_E_Gr = localVariables.newFieldInRecord("pnd_Total_1099_Form_Cnt_E_Gr", "#TOTAL-1099-FORM-CNT-E-GR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Total_1099_Gross_Amt_E_Gr = localVariables.newFieldInRecord("pnd_Total_1099_Gross_Amt_E_Gr", "#TOTAL-1099-GROSS-AMT-E-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Total_1099_Ivc_Amt_E_Gr = localVariables.newFieldInRecord("pnd_Total_1099_Ivc_Amt_E_Gr", "#TOTAL-1099-IVC-AMT-E-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Total_1099_Taxable_Amt_E_Gr = localVariables.newFieldInRecord("pnd_Total_1099_Taxable_Amt_E_Gr", "#TOTAL-1099-TAXABLE-AMT-E-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Total_1099_Int_Amt_E_Gr = localVariables.newFieldInRecord("pnd_Total_1099_Int_Amt_E_Gr", "#TOTAL-1099-INT-AMT-E-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Total_1099_Fed_Tax_Wthld_E_Gr = localVariables.newFieldInRecord("pnd_Total_1099_Fed_Tax_Wthld_E_Gr", "#TOTAL-1099-FED-TAX-WTHLD-E-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Total_1099_State_Tax_Wthld_E_Gr = localVariables.newFieldInRecord("pnd_Total_1099_State_Tax_Wthld_E_Gr", "#TOTAL-1099-STATE-TAX-WTHLD-E-GR", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_1099_Loc_Tax_Wthld_E_Gr = localVariables.newFieldInRecord("pnd_Total_1099_Loc_Tax_Wthld_E_Gr", "#TOTAL-1099-LOC-TAX-WTHLD-E-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Total_1099_Irr_Amt_E_Gr = localVariables.newFieldInRecord("pnd_Total_1099_Irr_Amt_E_Gr", "#TOTAL-1099-IRR-AMT-E-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Total_1099_Form_Cnt_E_No_Gr = localVariables.newFieldInRecord("pnd_Total_1099_Form_Cnt_E_No_Gr", "#TOTAL-1099-FORM-CNT-E-NO-GR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Total_1099_Gross_Amt_E_No_Gr = localVariables.newFieldInRecord("pnd_Total_1099_Gross_Amt_E_No_Gr", "#TOTAL-1099-GROSS-AMT-E-NO-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Total_1099_Ivc_Amt_E_No_Gr = localVariables.newFieldInRecord("pnd_Total_1099_Ivc_Amt_E_No_Gr", "#TOTAL-1099-IVC-AMT-E-NO-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Total_1099_Taxable_Amt_E_No_Gr = localVariables.newFieldInRecord("pnd_Total_1099_Taxable_Amt_E_No_Gr", "#TOTAL-1099-TAXABLE-AMT-E-NO-GR", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_1099_Int_Amt_E_No_Gr = localVariables.newFieldInRecord("pnd_Total_1099_Int_Amt_E_No_Gr", "#TOTAL-1099-INT-AMT-E-NO-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Total_1099_Fed_Tax_Wthld_E_No_Gr = localVariables.newFieldInRecord("pnd_Total_1099_Fed_Tax_Wthld_E_No_Gr", "#TOTAL-1099-FED-TAX-WTHLD-E-NO-GR", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_1099_State_Tax_E_No_Gr = localVariables.newFieldInRecord("pnd_Total_1099_State_Tax_E_No_Gr", "#TOTAL-1099-STATE-TAX-E-NO-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Total_1099_Loc_Tax_E_No_Gr = localVariables.newFieldInRecord("pnd_Total_1099_Loc_Tax_E_No_Gr", "#TOTAL-1099-LOC-TAX-E-NO-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Total_1099_Irr_Amt_E_No_Gr = localVariables.newFieldInRecord("pnd_Total_1099_Irr_Amt_E_No_Gr", "#TOTAL-1099-IRR-AMT-E-NO-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Form_Cnt_1042s_Gr = localVariables.newFieldInRecord("pnd_Form_Cnt_1042s_Gr", "#FORM-CNT-1042S-GR", FieldType.PACKED_DECIMAL, 7);
        pnd_1042s_Gross_Income_1042s_Gr = localVariables.newFieldInRecord("pnd_1042s_Gross_Income_1042s_Gr", "#1042S-GROSS-INCOME-1042S-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1042s_Pension_Amt_1042s_Gr = localVariables.newFieldInRecord("pnd_1042s_Pension_Amt_1042s_Gr", "#1042S-PENSION-AMT-1042S-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1042s_Interest_Amt_1042s_Gr = localVariables.newFieldInRecord("pnd_1042s_Interest_Amt_1042s_Gr", "#1042S-INTEREST-AMT-1042S-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1042s_Nra_Tax_Wthld_1042s_Gr = localVariables.newFieldInRecord("pnd_1042s_Nra_Tax_Wthld_1042s_Gr", "#1042S-NRA-TAX-WTHLD-1042S-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1042s_Refund_Amt_1042s_Gr = localVariables.newFieldInRecord("pnd_1042s_Refund_Amt_1042s_Gr", "#1042S-REFUND-AMT-1042S-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Form_Cnt_E_1042s_Gr = localVariables.newFieldInRecord("pnd_Form_Cnt_E_1042s_Gr", "#FORM-CNT-E-1042S-GR", FieldType.PACKED_DECIMAL, 7);
        pnd_1042s_Gross_Income_E_1042s_Gr = localVariables.newFieldInRecord("pnd_1042s_Gross_Income_E_1042s_Gr", "#1042S-GROSS-INCOME-E-1042S-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1042s_Pension_Amt_E_1042s_Gr = localVariables.newFieldInRecord("pnd_1042s_Pension_Amt_E_1042s_Gr", "#1042S-PENSION-AMT-E-1042S-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1042s_Interest_Amt_E_1042s_Gr = localVariables.newFieldInRecord("pnd_1042s_Interest_Amt_E_1042s_Gr", "#1042S-INTEREST-AMT-E-1042S-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_1042s_Nra_Tax_Wthld_E_1042s_Gr = localVariables.newFieldInRecord("pnd_1042s_Nra_Tax_Wthld_E_1042s_Gr", "#1042S-NRA-TAX-WTHLD-E-1042S-GR", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Refund_Amt_E_1042s_Gr = localVariables.newFieldInRecord("pnd_1042s_Refund_Amt_E_1042s_Gr", "#1042S-REFUND-AMT-E-1042S-GR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Form_Cnt_E_No_1042s_Gr = localVariables.newFieldInRecord("pnd_Form_Cnt_E_No_1042s_Gr", "#FORM-CNT-E-NO-1042S-GR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_1042s_Gross_Income_E_No_1042s_Gr = localVariables.newFieldInRecord("pnd_1042s_Gross_Income_E_No_1042s_Gr", "#1042S-GROSS-INCOME-E-NO-1042S-GR", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Pension_Amt_E_No_1042s_Gr = localVariables.newFieldInRecord("pnd_1042s_Pension_Amt_E_No_1042s_Gr", "#1042S-PENSION-AMT-E-NO-1042S-GR", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Interest_Amt_E_No_1042s_Gr = localVariables.newFieldInRecord("pnd_1042s_Interest_Amt_E_No_1042s_Gr", "#1042S-INTEREST-AMT-E-NO-1042S-GR", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Nra_Tax_Wthld_E_No_1042s_Gr = localVariables.newFieldInRecord("pnd_1042s_Nra_Tax_Wthld_E_No_1042s_Gr", "#1042S-NRA-TAX-WTHLD-E-NO-1042S-GR", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Refund_Amt_E_No_1042s_Gr = localVariables.newFieldInRecord("pnd_1042s_Refund_Amt_E_No_1042s_Gr", "#1042S-REFUND-AMT-E-NO-1042S-GR", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Form_Cnt_1042s_Gr_Fatca = localVariables.newFieldArrayInRecord("pnd_Form_Cnt_1042s_Gr_Fatca", "#FORM-CNT-1042S-GR-FATCA", FieldType.PACKED_DECIMAL, 
            7, new DbsArrayController(1, 2));
        pnd_1042s_Gross_Income_1042s_Gr_Fatca = localVariables.newFieldArrayInRecord("pnd_1042s_Gross_Income_1042s_Gr_Fatca", "#1042S-GROSS-INCOME-1042S-GR-FATCA", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Pension_Amt_1042s_Gr_Fatca = localVariables.newFieldArrayInRecord("pnd_1042s_Pension_Amt_1042s_Gr_Fatca", "#1042S-PENSION-AMT-1042S-GR-FATCA", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Interest_Amt_1042s_Gr_Fatca = localVariables.newFieldArrayInRecord("pnd_1042s_Interest_Amt_1042s_Gr_Fatca", "#1042S-INTEREST-AMT-1042S-GR-FATCA", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Nra_Tax_Wthld_1042s_Gr_Fatca = localVariables.newFieldArrayInRecord("pnd_1042s_Nra_Tax_Wthld_1042s_Gr_Fatca", "#1042S-NRA-TAX-WTHLD-1042S-GR-FATCA", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Refund_Amt_1042s_Gr_Fatca = localVariables.newFieldArrayInRecord("pnd_1042s_Refund_Amt_1042s_Gr_Fatca", "#1042S-REFUND-AMT-1042S-GR-FATCA", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_Form_Cnt_E_1042s_Gr_Fatca = localVariables.newFieldArrayInRecord("pnd_Form_Cnt_E_1042s_Gr_Fatca", "#FORM-CNT-E-1042S-GR-FATCA", FieldType.PACKED_DECIMAL, 
            7, new DbsArrayController(1, 2));
        pnd_1042s_Gross_Income_E_1042s_Gr_Fatca = localVariables.newFieldArrayInRecord("pnd_1042s_Gross_Income_E_1042s_Gr_Fatca", "#1042S-GROSS-INCOME-E-1042S-GR-FATCA", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Pension_Amt_E_1042s_Gr_Fatca = localVariables.newFieldArrayInRecord("pnd_1042s_Pension_Amt_E_1042s_Gr_Fatca", "#1042S-PENSION-AMT-E-1042S-GR-FATCA", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Interest_Amt_E_1042s_Gr_Fatca = localVariables.newFieldArrayInRecord("pnd_1042s_Interest_Amt_E_1042s_Gr_Fatca", "#1042S-INTEREST-AMT-E-1042S-GR-FATCA", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Nra_Tax_Wthld_E_1042s_Gr_Fatca = localVariables.newFieldArrayInRecord("pnd_1042s_Nra_Tax_Wthld_E_1042s_Gr_Fatca", "#1042S-NRA-TAX-WTHLD-E-1042S-GR-FATCA", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Refund_Amt_E_1042s_Gr_Fatca = localVariables.newFieldArrayInRecord("pnd_1042s_Refund_Amt_E_1042s_Gr_Fatca", "#1042S-REFUND-AMT-E-1042S-GR-FATCA", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_Form_Cnt_E_No_1042s_Gr_Fatca = localVariables.newFieldArrayInRecord("pnd_Form_Cnt_E_No_1042s_Gr_Fatca", "#FORM-CNT-E-NO-1042S-GR-FATCA", FieldType.PACKED_DECIMAL, 
            7, new DbsArrayController(1, 2));
        pnd_1042s_Gross_Income_E_No_Gr_Fatca = localVariables.newFieldArrayInRecord("pnd_1042s_Gross_Income_E_No_Gr_Fatca", "#1042S-GROSS-INCOME-E-NO-GR-FATCA", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Pension_Amt_E_No_Gr_Fatca = localVariables.newFieldArrayInRecord("pnd_1042s_Pension_Amt_E_No_Gr_Fatca", "#1042S-PENSION-AMT-E-NO-GR-FATCA", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Interest_Amt_E_No_Gr_Fatca = localVariables.newFieldArrayInRecord("pnd_1042s_Interest_Amt_E_No_Gr_Fatca", "#1042S-INTEREST-AMT-E-NO-GR-FATCA", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Nra_Tax_Wthld_E_No_Gr_Fatca = localVariables.newFieldArrayInRecord("pnd_1042s_Nra_Tax_Wthld_E_No_Gr_Fatca", "#1042S-NRA-TAX-WTHLD-E-NO-GR-FATCA", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_1042s_Refund_Amt_E_No_Gr_Fatca = localVariables.newFieldArrayInRecord("pnd_1042s_Refund_Amt_E_No_Gr_Fatca", "#1042S-REFUND-AMT-E-NO-GR-FATCA", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 2));
        pnd_Fatca_Sw = localVariables.newFieldInRecord("pnd_Fatca_Sw", "#FATCA-SW", FieldType.BOOLEAN, 1);
        pnd_F_Ndx = localVariables.newFieldInRecord("pnd_F_Ndx", "#F-NDX", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        rEAD_FORMTirf_Tax_YearOld = internalLoopRecord.newFieldInRecord("READ_FORM_Tirf_Tax_Year_OLD", "Tirf_Tax_Year_OLD", FieldType.STRING, 4);
        rEAD_FORMTirf_Company_CdeOld = internalLoopRecord.newFieldInRecord("READ_FORM_Tirf_Company_Cde_OLD", "Tirf_Company_Cde_OLD", FieldType.STRING, 
            1);
        rEAD_FORMTirf_Form_TypeOld = internalLoopRecord.newFieldInRecord("READ_FORM_Tirf_Form_Type_OLD", "Tirf_Form_Type_OLD", FieldType.NUMERIC, 2);
        rD1Tirf_Superde_6Old = internalLoopRecord.newFieldInRecord("RD1_Tirf_Superde_6_OLD", "Tirf_Superde_6_OLD", FieldType.STRING, 33);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_hist_Form.reset();
        vw_form_1099_R.reset();
        vw_form_X.reset();
        vw_form_R.reset();
        vw_twrparti_Read.reset();
        internalLoopRecord.reset();

        ldaTwrlcomp.initializeValues();
        ldaTwrl5001.initializeValues();
        ldaTwrl5951.initializeValues();
        ldaTwrl9710.initializeValues();
        ldaTwrl9610.initializeValues();

        localVariables.reset();
        low_Values.setInitialValue("H'00'");
        high_Values.setInitialValue("H'FF'");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp1070() throws Exception
    {
        super("Twrp1070");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp1070|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT ( 0 ) PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 1 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 2 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 3 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 4 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 5 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 6 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 7 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 8 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 9 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 10 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 11 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 12 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 13 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 14 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 15 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 16 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 18 ) PS = 58 LS = 133 ZP = ON
                getReports().definePrinter(3, "'CMPRT10'");                                                                                                               //Natural: DEFINE PRINTER ( 2 ) OUTPUT 'CMPRT10'
                getReports().definePrinter(4, "'CMPRT10'");                                                                                                               //Natural: DEFINE PRINTER ( 3 ) OUTPUT 'CMPRT10'
                getReports().definePrinter(5, "'CMPRT10'");                                                                                                               //Natural: DEFINE PRINTER ( 4 ) OUTPUT 'CMPRT10'
                getReports().definePrinter(6, "'CMPRT10'");                                                                                                               //Natural: DEFINE PRINTER ( 5 ) OUTPUT 'CMPRT10'
                getReports().definePrinter(7, "'CMPRT10'");                                                                                                               //Natural: DEFINE PRINTER ( 6 ) OUTPUT 'CMPRT10'
                getReports().definePrinter(12, "'CMPRT10'");                                                                                                              //Natural: DEFINE PRINTER ( 11 ) OUTPUT 'CMPRT10'
                getReports().definePrinter(13, "'CMPRT10'");                                                                                                              //Natural: DEFINE PRINTER ( 12 ) OUTPUT 'CMPRT10'
                getReports().definePrinter(14, "'CMPRT10'");                                                                                                              //Natural: DEFINE PRINTER ( 13 ) OUTPUT 'CMPRT10'
                getReports().definePrinter(15, "'CMPRT10'");                                                                                                              //Natural: DEFINE PRINTER ( 14 ) OUTPUT 'CMPRT10'
                getReports().definePrinter(16, "'CMPRT10'");                                                                                                              //Natural: DEFINE PRINTER ( 15 ) OUTPUT 'CMPRT10'
                getReports().definePrinter(17, "'CMPRT10'");                                                                                                              //Natural: DEFINE PRINTER ( 16 ) OUTPUT 'CMPRT10'
                //*  ACTIVE
                getReports().definePrinter(19, "'CMPRT10'");                                                                                                              //Natural: DEFINE PRINTER ( 18 ) OUTPUT 'CMPRT10'
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                pnd_Sys_Date.setValue(Global.getDATX());                                                                                                                  //Natural: ASSIGN #SYS-DATE := *DATX
                pnd_Sys_Time.setValue(Global.getTIMX());                                                                                                                  //Natural: ASSIGN #SYS-TIME := *TIMX
                pnd_Sys_Date_N.setValue(Global.getDATN());                                                                                                                //Natural: ASSIGN #SYS-DATE-N := *DATN
                //*    REPORT HEADING
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 2 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'YTD Form Database Control' 110T 'Report: RPT2' / 59T 'Tax Year' #INPUT-REC.#TAX-YEAR ( EM = 9999 ) // 'Company: ' #TWRLCOMP.#COMP-SHORT-NAME ( #COMPANY-NDX ) //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 3 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 03 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'YTD Form Database Control' 110T 'Report: RPT3' / 59T 'Tax Year' #INPUT-REC.#TAX-YEAR ( EM = 9999 ) // 'Company: ' #TWRLCOMP.#COMP-SHORT-NAME ( #COMPANY-NDX ) //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 4 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 04 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'YTD Form Database Control' 110T 'Report: RPT4' / 59T 'Tax Year' #INPUT-REC.#TAX-YEAR ( EM = 9999 ) // 'Company: ' #TWRLCOMP.#COMP-SHORT-NAME ( #COMPANY-NDX ) //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 5 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 05 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'YTD Form Database Control' 110T 'Report: RPT5' / 59T 'Tax Year' #INPUT-REC.#TAX-YEAR ( EM = 9999 ) // 'Company: ' #TWRLCOMP.#COMP-SHORT-NAME ( #COMPANY-NDX ) //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 6 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 06 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'YTD Form Database Control' 110T 'Report: RPT6' / 59T 'Tax Year' #INPUT-REC.#TAX-YEAR ( EM = 9999 ) // 'Company: ' #TWRLCOMP.#COMP-SHORT-NAME ( #COMPANY-NDX ) //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 11 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 11 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'YTD Form Database Control' 110T 'Report: RPT11' / 59T 'Tax Year' #INPUT-REC.#TAX-YEAR ( EM = 9999 ) // 'Grand Total forms 1099-R and 1099-INT' //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 12 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 12 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'YTD Form Database Control' 110T 'Report: RPT12' / 59T 'Tax Year' #INPUT-REC.#TAX-YEAR ( EM = 9999 ) // 'Grand Total form 1042-S' //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 13 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 13 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'YTD Form Database Control' 110T 'Report: RPT13' / 59T 'Tax Year' #INPUT-REC.#TAX-YEAR ( EM = 9999 ) // 'Grand Total form 5498 P1' //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 14 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 14 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'YTD Form Database Control' 110T 'Report: RPT14' / 59T 'Tax Year' #INPUT-REC.#TAX-YEAR ( EM = 9999 ) // 'Grand Total form 480.7C' //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 15 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 15 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'YTD Form Database Control' 110T 'Report: RPT15' / 59T 'Tax Year' #INPUT-REC.#TAX-YEAR ( EM = 9999 ) // 'Grand Total form NR4' //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 16 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 16 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'YTD Form Database Control' 110T 'Report: RPT16' / 59T 'Tax Year' #INPUT-REC.#TAX-YEAR ( EM = 9999 ) // 'Grand Total form 5498 P2' //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 18 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 110T 'PAGE:' *PAGE-NUMBER ( 18 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'YTD Form Database Control' 110T 'REPORT: RPT18' / 59T 'Tax Year' #INPUT-REC.#TAX-YEAR ( EM = 9999 ) // 'GRAND TOTAL SUNY        ' //
                //* ***************
                //*  MAIN PROGRAM *
                //* ***************
                //*    GET TAX YEAR FROM DRIVER PROGRAM
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Rec_Pnd_Tax_Year);                                                                           //Natural: INPUT #INPUT-REC.#TAX-YEAR
                //*    READ FORM FILE
                pnd_Form_Key_Pnd_Tax_Year.setValue(pnd_Input_Rec_Pnd_Tax_Year);                                                                                           //Natural: ASSIGN #FORM-KEY.#TAX-YEAR := #TWRAFRMN.#TAX-YEAR := #INPUT-REC.#TAX-YEAR
                pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Tax_Year().setValue(pnd_Input_Rec_Pnd_Tax_Year);
                pnd_Form_Key_Pnd_Status.setValue("A");                                                                                                                    //Natural: ASSIGN #FORM-KEY.#STATUS := 'A'
                //*  01/30/08 - AY
                DbsUtil.callnat(Twrnfrmn.class , getCurrentProcessState(), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNFRMN' USING #TWRAFRMN.#INPUT-PARMS ( AD = O ) #TWRAFRMN.#OUTPUT-DATA ( AD = M )
                    pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                ldaTwrl9710.getVw_form().startDatabaseRead                                                                                                                //Natural: READ FORM BY TIRF-SUPERDE-6 = #FORM-KEY
                (
                "READ_FORM",
                new Wc[] { new Wc("TIRF_SUPERDE_6", ">=", pnd_Form_Key, WcType.BY) },
                new Oc[] { new Oc("TIRF_SUPERDE_6", "ASC") }
                );
                boolean endOfDataReadForm = true;
                boolean firstReadForm = true;
                READ_FORM:
                while (condition(ldaTwrl9710.getVw_form().readNextRow("READ_FORM")))
                {
                    CheckAtStartofData1373();

                    if (condition(ldaTwrl9710.getVw_form().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventRead_Form();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataReadForm = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().notEquals(pnd_Form_Key_Pnd_Tax_Year)))                                                              //Natural: IF TIRF-TAX-YEAR NE #FORM-KEY.#TAX-YEAR
                    {
                        if (true) break READ_FORM;                                                                                                                        //Natural: ESCAPE BOTTOM ( READ-FORM. )
                    }                                                                                                                                                     //Natural: END-IF
                    //* **************************************************************                                                                                    //Natural: AT START OF DATA
                    //*                                                                                                                                                   //Natural: AT BREAK OF TIRF-FORM-TYPE
                    if (condition(pnd_Customer_Id_Pnd_Pin.notEquals(ldaTwrl9710.getForm_Tirf_Pin())))                                                                     //Natural: AT BREAK OF TIRF-COMPANY-CDE;//Natural: IF #PIN NE TIRF-PIN
                    {
                        pnd_Customer_Id_Pnd_Pin.setValue(ldaTwrl9710.getForm_Tirf_Pin());                                                                                 //Natural: MOVE TIRF-PIN TO #PIN
                        pnd_E_Deliver.setValue(false);                                                                                                                    //Natural: MOVE FALSE TO #E-DELIVER
                        //*    #COMM-CPMN550.#COMM-CUSTOMER-PIN   := #CUSTOMER-ID  092716 START
                        //*    #COMM-CPMN550.#COMM-CUSTOMER-TYP   := 'PH'
                        //*    #COMM-CPMN550.#COMM-CUSTOMER-FK    := 'PPPPPPPPPPPPPPP'
                        //*    #COMM-CPMN550.#COMM-DOC-ID         := 'D35'   /* TAX
                        //*    CALLNAT 'CPMN550' USING #COMM-CPMN550
                        //*    IF #COMM-OUT-USER-ERROR-CODE        = 0 /* TAX EMAIL PREFERENCE
                        pnd_Twrparti_Curr_Rec_Sd.reset();                                                                                                                 //Natural: RESET #TWRPARTI-CURR-REC-SD
                        pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Tax_Id.setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                        //Natural: ASSIGN #KEY-TWRPARTI-TAX-ID := FORM.TIRF-TIN
                        pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Status.setValue(" ");                                                                                   //Natural: ASSIGN #KEY-TWRPARTI-STATUS := ' '
                        pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Part_Eff_End_Dte.setValue("99999999");                                                                  //Natural: ASSIGN #KEY-TWRPARTI-PART-EFF-END-DTE := '99999999'
                        vw_twrparti_Read.startDatabaseRead                                                                                                                //Natural: READ ( 1 ) TWRPARTI-READ WITH TWRPARTI-CURR-REC-SD EQ #TWRPARTI-CURR-REC-SD
                        (
                        "READ01",
                        new Wc[] { new Wc("TWRPARTI_CURR_REC_SD", ">=", pnd_Twrparti_Curr_Rec_Sd, WcType.BY) },
                        new Oc[] { new Oc("TWRPARTI_CURR_REC_SD", "ASC") },
                        1
                        );
                        READ01:
                        while (condition(vw_twrparti_Read.readNextRow("READ01")))
                        {
                            if (condition(twrparti_Read_Twrparti_Tax_Id.notEquals(ldaTwrl9710.getForm_Tirf_Tin())))                                                       //Natural: IF TWRPARTI-TAX-ID NE FORM.TIRF-TIN
                            {
                                getReports().write(0, "=",twrparti_Read_Twrparti_Tax_Id,"=",ldaTwrl9710.getForm_Tirf_Tin());                                              //Natural: WRITE '=' TWRPARTI-TAX-ID '=' FORM.TIRF-TIN
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                getReports().write(0, "** SYSTEM ERROR: SSN NOT ON PARTICIPANT FILE **");                                                                 //Natural: WRITE '** SYSTEM ERROR: SSN NOT ON PARTICIPANT FILE **'
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                DbsUtil.terminate(99);  if (true) return;                                                                                                 //Natural: TERMINATE 99
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(twrparti_Read_Twrparti_Rlup_Email_Pref.equals("Y") && ldaTwrl9710.getForm_Tirf_Deceased_Ind().notEquals("Y")))                  //Natural: IF TWRPARTI-RLUP-EMAIL-PREF = 'Y' AND TIRF-DECEASED-IND NE 'Y'
                            {
                                pnd_E_Deliver.setValue(true);                                                                                                             //Natural: MOVE TRUE TO #E-DELIVER
                            }                                                                                                                                             //Natural: END-IF
                            //*   092716  END
                        }                                                                                                                                                 //Natural: END-READ
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*    CHECK EMPTY FORM
                    if (condition(ldaTwrl9710.getForm_Tirf_Empty_Form().getBoolean()))                                                                                    //Natural: IF TIRF-EMPTY-FORM
                    {
                        pnd_Empty_Form_Cnt.nadd(1);                                                                                                                       //Natural: ADD 1 TO #EMPTY-FORM-CNT
                                                                                                                                                                          //Natural: PERFORM CHECK-REPORTED-TO-IRS
                        sub_Check_Reported_To_Irs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(pnd_Reported_To_Irs.getBoolean()))                                                                                                  //Natural: IF #REPORTED-TO-IRS
                        {
                                                                                                                                                                          //Natural: PERFORM CHECK-SYSTEM-HOLD-CODE
                            sub_Check_System_Hold_Code();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*    CHECK HOLD INDICATOR
                                                                                                                                                                          //Natural: PERFORM CHECK-SYSTEM-HOLD-CODE
                    sub_Check_System_Hold_Code();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*    ACCUMMULATE TOTALS BY FORM TYPE
                    pnd_Form_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #FORM-CNT
                    //*  FE141215 START
                    if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().equals(3)))                                                                                        //Natural: IF TIRF-FORM-TYPE EQ 3
                    {
                        pnd_Fatca_Sw.setValue(false);                                                                                                                     //Natural: ASSIGN #FATCA-SW := FALSE
                        if (condition(ldaTwrl9710.getForm_Tirf_Future_Use().getSubstring(20,1).equals("Y")))                                                              //Natural: IF SUBSTR ( TIRF-FUTURE-USE,20,1 ) = 'Y'
                        {
                            pnd_Fatca_Sw.setValue(true);                                                                                                                  //Natural: ASSIGN #FATCA-SW := TRUE
                            pnd_Form_Cnt_Fatca.getValue(2).nadd(1);                                                                                                       //Natural: ADD 1 TO #FORM-CNT-FATCA ( 2 )
                            if (condition(pnd_E_Deliver.getBoolean()))                                                                                                    //Natural: IF #E-DELIVER
                            {
                                pnd_Form_Cnt_E_Fatca.getValue(2).nadd(1);                                                                                                 //Natural: ADD 1 TO #FORM-CNT-E-FATCA ( 2 )
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Form_Cnt_E_No_Fatca.getValue(2).nadd(1);                                                                                              //Natural: ADD 1 TO #FORM-CNT-E-NO-FATCA ( 2 )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Form_Cnt_Fatca.getValue(1).nadd(1);                                                                                                       //Natural: ADD 1 TO #FORM-CNT-FATCA ( 1 )
                            if (condition(pnd_E_Deliver.getBoolean()))                                                                                                    //Natural: IF #E-DELIVER
                            {
                                pnd_Form_Cnt_E_Fatca.getValue(1).nadd(1);                                                                                                 //Natural: ADD 1 TO #FORM-CNT-E-FATCA ( 1 )
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Form_Cnt_E_No_Fatca.getValue(1).nadd(1);                                                                                              //Natural: ADD 1 TO #FORM-CNT-E-NO-FATCA ( 1 )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        //*  FE141201 END
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_E_Deliver.equals(true)))                                                                                                            //Natural: IF #E-DELIVER EQ TRUE
                    {
                        pnd_Form_Cnt_E.nadd(1);                                                                                                                           //Natural: ADD 1 TO #FORM-CNT-E
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Form_Cnt_E_No.nadd(1);                                                                                                                        //Natural: ADD 1 TO #FORM-CNT-E-NO
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().equals(10)))                                                                                       //Natural: IF TIRF-FORM-TYPE = 10
                    {
                        if (condition(ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(" ")))                                                                               //Natural: IF TIRF-CONTRACT-NBR = ' '
                        {
                            pnd_Suny_Control_Totals_Pnd_Letter_Count.nadd(1);                                                                                             //Natural: ADD 1 TO #LETTER-COUNT
                            //*      DISPLAY 'TIN' TIRF-TIN
                            //*            'PIN' TIRF-PIN
                            //*             'FORM' TIRF-FORM-TYPE
                            //*            '=' TIRF-CONTRACT-NBR
                            //*            '=' #E-DELIVER
                            if (condition(pnd_E_Deliver.getBoolean()))                                                                                                    //Natural: IF #E-DELIVER
                            {
                                pnd_Suny_Control_Totals_E_Pnd_Letter_Count_E.nadd(1);                                                                                     //Natural: ADD 1 TO #LETTER-COUNT-E
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Suny_Control_Totals_E_No_Pnd_Letter_Count_E_No.nadd(1);                                                                               //Natural: ADD 1 TO #LETTER-COUNT-E-NO
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  1099-R
                    short decideConditionsMet1655 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE OF TIRF-FORM-TYPE;//Natural: VALUE 1
                    if (condition((ldaTwrl9710.getForm_Tirf_Form_Type().equals(1))))
                    {
                        decideConditionsMet1655++;
                        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt.nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                    //Natural: ADD TIRF-GROSS-AMT TO #1099R-GROSS-AMT
                        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt.nadd(ldaTwrl9710.getForm_Tirf_Taxable_Amt());                                                //Natural: ADD TIRF-TAXABLE-AMT TO #1099R-TAXABLE-AMT
                        pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld.nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                             //Natural: ADD TIRF-FED-TAX-WTHLD TO #1099-FED-TAX-WTHLD
                        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt.nadd(ldaTwrl9710.getForm_Tirf_Ivc_Amt());                                                        //Natural: ADD TIRF-IVC-AMT TO #1099R-IVC-AMT
                        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt.nadd(ldaTwrl9710.getForm_Tirf_Irr_Amt());                                                        //Natural: ADD TIRF-IRR-AMT TO #1099R-IRR-AMT
                        if (condition(ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp().notEquals(getZero())))                                                        //Natural: IF C*TIRF-1099-R-STATE-GRP NE 0
                        {
                            pnd_C_1099_R.setValue(ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp());                                                                 //Natural: ASSIGN #C-1099-R := C*TIRF-1099-R-STATE-GRP
                            pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld.nadd(ldaTwrl9710.getForm_Tirf_State_Tax_Wthld().getValue(1,":",pnd_C_1099_R));       //Natural: ADD TIRF-STATE-TAX-WTHLD ( 1:#C-1099-R ) TO #1099R-STATE-TAX-WTHLD
                            pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld.nadd(ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(1,":",pnd_C_1099_R));           //Natural: ADD TIRF-LOC-TAX-WTHLD ( 1:#C-1099-R ) TO #1099R-LOC-TAX-WTHLD
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_E_Deliver.equals(true)))                                                                                                        //Natural: IF #E-DELIVER EQ TRUE
                        {
                            pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Gross_Amt_E.nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                            //Natural: ADD TIRF-GROSS-AMT TO #1099R-GROSS-AMT-E
                            pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Taxable_Amt_E.nadd(ldaTwrl9710.getForm_Tirf_Taxable_Amt());                                        //Natural: ADD TIRF-TAXABLE-AMT TO #1099R-TAXABLE-AMT-E
                            pnd_1099r_1099i_Control_Totals_E_Pnd_1099_Fed_Tax_Wthld_E.nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                     //Natural: ADD TIRF-FED-TAX-WTHLD TO #1099-FED-TAX-WTHLD-E
                            pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Ivc_Amt_E.nadd(ldaTwrl9710.getForm_Tirf_Ivc_Amt());                                                //Natural: ADD TIRF-IVC-AMT TO #1099R-IVC-AMT-E
                            pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Irr_Amt_E.nadd(ldaTwrl9710.getForm_Tirf_Irr_Amt());                                                //Natural: ADD TIRF-IRR-AMT TO #1099R-IRR-AMT-E
                            if (condition(ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp().notEquals(getZero())))                                                    //Natural: IF C*TIRF-1099-R-STATE-GRP NE 0
                            {
                                pnd_C_1099_R.setValue(ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp());                                                             //Natural: ASSIGN #C-1099-R := C*TIRF-1099-R-STATE-GRP
                                pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_State_Tax_Wthld_E.nadd(ldaTwrl9710.getForm_Tirf_State_Tax_Wthld().getValue(1,                  //Natural: ADD TIRF-STATE-TAX-WTHLD ( 1:#C-1099-R ) TO #1099R-STATE-TAX-WTHLD-E
                                    ":",pnd_C_1099_R));
                                pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Loc_Tax_Wthld_E.nadd(ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(1,":",                  //Natural: ADD TIRF-LOC-TAX-WTHLD ( 1:#C-1099-R ) TO #1099R-LOC-TAX-WTHLD-E
                                    pnd_C_1099_R));
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Gross_Amt_E_No.nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                      //Natural: ADD TIRF-GROSS-AMT TO #1099R-GROSS-AMT-E-NO
                            pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Taxable_Amt_E_No.nadd(ldaTwrl9710.getForm_Tirf_Taxable_Amt());                                  //Natural: ADD TIRF-TAXABLE-AMT TO #1099R-TAXABLE-AMT-E-NO
                            pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099_Fed_Tax_Wthld_E_No.nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                               //Natural: ADD TIRF-FED-TAX-WTHLD TO #1099-FED-TAX-WTHLD-E-NO
                            pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Ivc_Amt_E_No.nadd(ldaTwrl9710.getForm_Tirf_Ivc_Amt());                                          //Natural: ADD TIRF-IVC-AMT TO #1099R-IVC-AMT-E-NO
                            pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Irr_Amt_E_No.nadd(ldaTwrl9710.getForm_Tirf_Irr_Amt());                                          //Natural: ADD TIRF-IRR-AMT TO #1099R-IRR-AMT-E-NO
                            if (condition(ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp().notEquals(getZero())))                                                    //Natural: IF C*TIRF-1099-R-STATE-GRP NE 0
                            {
                                pnd_C_1099_R.setValue(ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp());                                                             //Natural: ASSIGN #C-1099-R := C*TIRF-1099-R-STATE-GRP
                                pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_State_Tax_Wthld_E_No.nadd(ldaTwrl9710.getForm_Tirf_State_Tax_Wthld().getValue(1,            //Natural: ADD TIRF-STATE-TAX-WTHLD ( 1:#C-1099-R ) TO #1099R-STATE-TAX-WTHLD-E-NO
                                    ":",pnd_C_1099_R));
                                pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Loc_Tax_Wthld_E_No.nadd(ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(1,                //Natural: ADD TIRF-LOC-TAX-WTHLD ( 1:#C-1099-R ) TO #1099R-LOC-TAX-WTHLD-E-NO
                                    ":",pnd_C_1099_R));
                            }                                                                                                                                             //Natural: END-IF
                            //*  1099-INT
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 2
                    else if (condition((ldaTwrl9710.getForm_Tirf_Form_Type().equals(2))))
                    {
                        decideConditionsMet1655++;
                        pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt.nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                        //Natural: ADD TIRF-INT-AMT TO #1099I-INT-AMT
                        pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld.nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                             //Natural: ADD TIRF-FED-TAX-WTHLD TO #1099-FED-TAX-WTHLD
                        if (condition(pnd_E_Deliver.equals(true)))                                                                                                        //Natural: IF #E-DELIVER EQ TRUE
                        {
                            pnd_1099r_1099i_Control_Totals_E_Pnd_1099i_Int_Amt_E.nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                //Natural: ADD TIRF-INT-AMT TO #1099I-INT-AMT-E
                            pnd_1099r_1099i_Control_Totals_E_Pnd_1099_Fed_Tax_Wthld_E.nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                     //Natural: ADD TIRF-FED-TAX-WTHLD TO #1099-FED-TAX-WTHLD-E
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099i_Int_Amt_E_No.nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                          //Natural: ADD TIRF-INT-AMT TO #1099I-INT-AMT-E-NO
                            pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099_Fed_Tax_Wthld_E_No.nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                               //Natural: ADD TIRF-FED-TAX-WTHLD TO #1099-FED-TAX-WTHLD-E-NO
                            //*  1042-S
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 3
                    else if (condition((ldaTwrl9710.getForm_Tirf_Form_Type().equals(3))))
                    {
                        decideConditionsMet1655++;
                        //*  FE141205 START
                        if (condition(pnd_Fatca_Sw.getBoolean()))                                                                                                         //Natural: IF #FATCA-SW
                        {
                            pnd_F_Ndx.setValue(2);                                                                                                                        //Natural: ASSIGN #F-NDX := 2
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_F_Ndx.setValue(1);                                                                                                                        //Natural: ASSIGN #F-NDX := 1
                            //*  FE141205 END
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaTwrl9710.getForm_Count_Casttirf_1042_S_Line_Grp().notEquals(getZero())))                                                         //Natural: IF C*TIRF-1042-S-LINE-GRP NE 0
                        {
                            FOR01:                                                                                                                                        //Natural: FOR #C-1042-S = 1 TO C*TIRF-1042-S-LINE-GRP
                            for (pnd_C_1042_S.setValue(1); condition(pnd_C_1042_S.lessOrEqual(ldaTwrl9710.getForm_Count_Casttirf_1042_S_Line_Grp())); 
                                pnd_C_1042_S.nadd(1))
                            {
                                pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));                     //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-GROSS-INCOME
                                pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld.nadd(ldaTwrl9710.getForm_Tirf_Nra_Tax_Wthld().getValue(pnd_C_1042_S));                   //Natural: ADD TIRF-NRA-TAX-WTHLD ( #C-1042-S ) TO #1042S-NRA-TAX-WTHLD
                                pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt.nadd(ldaTwrl9710.getForm_Tirf_1042_S_Refund_Amt().getValue(pnd_C_1042_S));                  //Natural: ADD TIRF-1042-S-REFUND-AMT ( #C-1042-S ) TO #1042S-REFUND-AMT
                                //*  FE141201 START
                                pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income_Fatca.getValue(pnd_F_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-GROSS-INCOME-FATCA ( #F-NDX )
                                pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld_Fatca.getValue(pnd_F_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Nra_Tax_Wthld().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-NRA-TAX-WTHLD ( #C-1042-S ) TO #1042S-NRA-TAX-WTHLD-FATCA ( #F-NDX )
                                //*  FE141201 END
                                pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt_Fatca.getValue(pnd_F_Ndx).nadd(ldaTwrl9710.getForm_Tirf_1042_S_Refund_Amt().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-1042-S-REFUND-AMT ( #C-1042-S ) TO #1042S-REFUND-AMT-FATCA ( #F-NDX )
                                //*  INTEREST
                                short decideConditionsMet1723 = 0;                                                                                                        //Natural: DECIDE ON FIRST VALUE TIRF-1042-S-INCOME-CODE ( #C-1042-S );//Natural: VALUE '01'
                                if (condition((ldaTwrl9710.getForm_Tirf_1042_S_Income_Code().getValue(pnd_C_1042_S).equals("01"))))
                                {
                                    decideConditionsMet1723++;
                                    pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));                 //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-INTEREST-AMT
                                    //*  FE141201
                                    //*  FE141201
                                    //*  PENSION AND ANNUITIES
                                    pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt_Fatca.getValue(pnd_F_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-INTEREST-AMT-FATCA ( #F-NDX )
                                }                                                                                                                                         //Natural: VALUE '14'
                                else if (condition((ldaTwrl9710.getForm_Tirf_1042_S_Income_Code().getValue(pnd_C_1042_S).equals("14"))))
                                {
                                    decideConditionsMet1723++;
                                    pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));                  //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-PENSION-AMT
                                    //*  FE141201
                                    //*  FE141201
                                    pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt_Fatca.getValue(pnd_F_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-PENSION-AMT-FATCA ( #F-NDX )
                                }                                                                                                                                         //Natural: NONE
                                else if (condition())
                                {
                                    ignore();
                                }                                                                                                                                         //Natural: END-DECIDE
                                if (condition(pnd_E_Deliver.equals(true)))                                                                                                //Natural: IF #E-DELIVER EQ TRUE
                                {
                                    pnd_1042s_Control_Totals_E_Pnd_1042s_Gross_Income_E.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));             //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-GROSS-INCOME-E
                                    pnd_1042s_Control_Totals_E_Pnd_1042s_Nra_Tax_Wthld_E.nadd(ldaTwrl9710.getForm_Tirf_Nra_Tax_Wthld().getValue(pnd_C_1042_S));           //Natural: ADD TIRF-NRA-TAX-WTHLD ( #C-1042-S ) TO #1042S-NRA-TAX-WTHLD-E
                                    pnd_1042s_Control_Totals_E_Pnd_1042s_Refund_Amt_E.nadd(ldaTwrl9710.getForm_Tirf_1042_S_Refund_Amt().getValue(pnd_C_1042_S));          //Natural: ADD TIRF-1042-S-REFUND-AMT ( #C-1042-S ) TO #1042S-REFUND-AMT-E
                                    //*  FE141201 START
                                    pnd_1042s_Control_Totals_E_Pnd_1042s_Gross_Income_E_Fatca.getValue(pnd_F_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-GROSS-INCOME-E-FATCA ( #F-NDX )
                                    pnd_1042s_Control_Totals_E_Pnd_1042s_Nra_Tax_Wthld_E_Fatca.getValue(pnd_F_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Nra_Tax_Wthld().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-NRA-TAX-WTHLD ( #C-1042-S ) TO #1042S-NRA-TAX-WTHLD-E-FATCA ( #F-NDX )
                                    //*  FE141201 END
                                    pnd_1042s_Control_Totals_E_Pnd_1042s_Refund_Amt_E_Fatca.getValue(pnd_F_Ndx).nadd(ldaTwrl9710.getForm_Tirf_1042_S_Refund_Amt().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-1042-S-REFUND-AMT ( #C-1042-S ) TO #1042S-REFUND-AMT-E-FATCA ( #F-NDX )
                                    //*  INTEREST
                                    short decideConditionsMet1748 = 0;                                                                                                    //Natural: DECIDE ON FIRST VALUE TIRF-1042-S-INCOME-CODE ( #C-1042-S );//Natural: VALUE '01'
                                    if (condition((ldaTwrl9710.getForm_Tirf_1042_S_Income_Code().getValue(pnd_C_1042_S).equals("01"))))
                                    {
                                        decideConditionsMet1748++;
                                        pnd_1042s_Control_Totals_E_Pnd_1042s_Interest_Amt_E.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));         //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-INTEREST-AMT-E
                                        //*  FE141201
                                        //*  FE141201
                                        //*  PENSION AND ANNUITIES
                                        pnd_1042s_Control_Totals_E_Pnd_1042s_Interest_Amt_E_Fatca.getValue(pnd_F_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-INTEREST-AMT-E-FATCA ( #F-NDX )
                                    }                                                                                                                                     //Natural: VALUE '14'
                                    else if (condition((ldaTwrl9710.getForm_Tirf_1042_S_Income_Code().getValue(pnd_C_1042_S).equals("14"))))
                                    {
                                        decideConditionsMet1748++;
                                        pnd_1042s_Control_Totals_E_Pnd_1042s_Pension_Amt_E.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));          //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-PENSION-AMT-E
                                        //*  FE141201
                                        //*  FE141201
                                        pnd_1042s_Control_Totals_E_Pnd_1042s_Pension_Amt_E_Fatca.getValue(pnd_F_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-PENSION-AMT-E-FATCA ( #F-NDX )
                                    }                                                                                                                                     //Natural: NONE
                                    else if (condition())
                                    {
                                        ignore();
                                    }                                                                                                                                     //Natural: END-DECIDE
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    pnd_1042s_Control_Totals_E_No_Pnd_1042s_Gross_Income_E_No.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));       //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-GROSS-INCOME-E-NO
                                    pnd_1042s_Control_Totals_E_No_Pnd_1042s_Nra_Tax_Wthld_E_No.nadd(ldaTwrl9710.getForm_Tirf_Nra_Tax_Wthld().getValue(pnd_C_1042_S));     //Natural: ADD TIRF-NRA-TAX-WTHLD ( #C-1042-S ) TO #1042S-NRA-TAX-WTHLD-E-NO
                                    pnd_1042s_Control_Totals_E_No_Pnd_1042s_Refund_Amt_E_No.nadd(ldaTwrl9710.getForm_Tirf_1042_S_Refund_Amt().getValue(pnd_C_1042_S));    //Natural: ADD TIRF-1042-S-REFUND-AMT ( #C-1042-S ) TO #1042S-REFUND-AMT-E-NO
                                    //*  FE141201 START
                                    pnd_1042s_Control_Totals_E_No_Pnd_1042s_Gross_Income_E_No_Fatca.getValue(pnd_F_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-GROSS-INCOME-E-NO-FATCA ( #F-NDX )
                                    pnd_1042s_Control_Totals_E_No_Pnd_1042s_Nra_Tax_Wthld_E_No_Fatca.getValue(pnd_F_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Nra_Tax_Wthld().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-NRA-TAX-WTHLD ( #C-1042-S ) TO #1042S-NRA-TAX-WTHLD-E-NO-FATCA ( #F-NDX )
                                    //*  FE141201 END
                                    pnd_1042s_Control_Totals_E_No_Pnd_1042s_Refund_Amt_E_No_Fatca.getValue(pnd_F_Ndx).nadd(ldaTwrl9710.getForm_Tirf_1042_S_Refund_Amt().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-1042-S-REFUND-AMT ( #C-1042-S ) TO #1042S-REFUND-AMT-E-NO-FATCA ( #F-NDX )
                                    //*  INTEREST
                                    short decideConditionsMet1773 = 0;                                                                                                    //Natural: DECIDE ON FIRST VALUE TIRF-1042-S-INCOME-CODE ( #C-1042-S );//Natural: VALUE '01'
                                    if (condition((ldaTwrl9710.getForm_Tirf_1042_S_Income_Code().getValue(pnd_C_1042_S).equals("01"))))
                                    {
                                        decideConditionsMet1773++;
                                        pnd_1042s_Control_Totals_E_No_Pnd_1042s_Interest_Amt_E_No.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));   //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-INTEREST-AMT-E-NO
                                        //*  FE141201
                                        //*  FE141201
                                        //*  PENSION AND ANNUITIES
                                        pnd_1042s_Control_Totals_E_No_Pnd_1042s_Interest_Amt_E_No_Fatca.getValue(pnd_F_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-INTEREST-AMT-E-NO-FATCA ( #F-NDX )
                                    }                                                                                                                                     //Natural: VALUE '14'
                                    else if (condition((ldaTwrl9710.getForm_Tirf_1042_S_Income_Code().getValue(pnd_C_1042_S).equals("14"))))
                                    {
                                        decideConditionsMet1773++;
                                        pnd_1042s_Control_Totals_E_No_Pnd_1042s_Pension_Amt_E_No.nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S));    //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-PENSION-AMT-E-NO
                                        //*  FE141201
                                        //*  FE141201
                                        pnd_1042s_Control_Totals_E_No_Pnd_1042s_Pension_Amt_E_No_Fatca.getValue(pnd_F_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_C_1042_S)); //Natural: ADD TIRF-GROSS-INCOME ( #C-1042-S ) TO #1042S-PENSION-AMT-E-NO-FATCA ( #F-NDX )
                                    }                                                                                                                                     //Natural: NONE
                                    else if (condition())
                                    {
                                        ignore();
                                    }                                                                                                                                     //Natural: END-DECIDE
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*  5498
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 4
                    else if (condition((ldaTwrl9710.getForm_Tirf_Form_Type().equals(4))))
                    {
                        decideConditionsMet1655++;
                        pnd_5498_Control_Totals_Pnd_5498_Form_Cnt.nadd(1);                                                                                                //Natural: ADD 1 TO #5498-FORM-CNT
                        pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Contrib.nadd(ldaTwrl9710.getForm_Tirf_Trad_Ira_Contrib());                                              //Natural: ADD TIRF-TRAD-IRA-CONTRIB TO #5498-TRAD-IRA-CONTRIB
                        pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Contrib.nadd(ldaTwrl9710.getForm_Tirf_Roth_Ira_Contrib());                                              //Natural: ADD TIRF-ROTH-IRA-CONTRIB TO #5498-ROTH-IRA-CONTRIB
                        pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rollover.nadd(ldaTwrl9710.getForm_Tirf_Trad_Ira_Rollover());                                            //Natural: ADD TIRF-TRAD-IRA-ROLLOVER TO #5498-TRAD-IRA-ROLLOVER
                        pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rechar.nadd(ldaTwrl9710.getForm_Tirf_Ira_Rechar_Amt());                                                 //Natural: ADD TIRF-IRA-RECHAR-AMT TO #5498-TRAD-IRA-RECHAR
                        pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Conversion.nadd(ldaTwrl9710.getForm_Tirf_Roth_Ira_Conversion());                                        //Natural: ADD TIRF-ROTH-IRA-CONVERSION TO #5498-ROTH-IRA-CONVERSION
                        pnd_5498_Control_Totals_Pnd_5498_Account_Fmv.nadd(ldaTwrl9710.getForm_Tirf_Account_Fmv());                                                        //Natural: ADD TIRF-ACCOUNT-FMV TO #5498-ACCOUNT-FMV
                        //*  BK
                        pnd_5498_Control_Totals_Pnd_5498_Sep_Amt.nadd(ldaTwrl9710.getForm_Tirf_Sep_Amt());                                                                //Natural: ADD TIRF-SEP-AMT TO #5498-SEP-AMT
                        //*  DASDH
                        pnd_5498_Control_Totals_Pnd_5498_Postpn_Amt.nadd(ldaTwrl9710.getForm_Tirf_Postpn_Amt());                                                          //Natural: ADD TIRF-POSTPN-AMT TO #5498-POSTPN-AMT
                        if (condition(pnd_E_Deliver.equals(true)))                                                                                                        //Natural: IF #E-DELIVER EQ TRUE
                        {
                            pnd_5498_Control_Totals_E_Pnd_5498_Form_Cnt_E.nadd(1);                                                                                        //Natural: ADD 1 TO #5498-FORM-CNT-E
                            pnd_5498_Control_Totals_E_Pnd_5498_Trad_Ira_Contrib_E.nadd(ldaTwrl9710.getForm_Tirf_Trad_Ira_Contrib());                                      //Natural: ADD TIRF-TRAD-IRA-CONTRIB TO #5498-TRAD-IRA-CONTRIB-E
                            pnd_5498_Control_Totals_E_Pnd_5498_Roth_Ira_Contrib_E.nadd(ldaTwrl9710.getForm_Tirf_Roth_Ira_Contrib());                                      //Natural: ADD TIRF-ROTH-IRA-CONTRIB TO #5498-ROTH-IRA-CONTRIB-E
                            pnd_5498_Control_Totals_E_Pnd_5498_Trad_Ira_Rollover_E.nadd(ldaTwrl9710.getForm_Tirf_Trad_Ira_Rollover());                                    //Natural: ADD TIRF-TRAD-IRA-ROLLOVER TO #5498-TRAD-IRA-ROLLOVER-E
                            pnd_5498_Control_Totals_E_Pnd_5498_Trad_Ira_Rechar_E.nadd(ldaTwrl9710.getForm_Tirf_Ira_Rechar_Amt());                                         //Natural: ADD TIRF-IRA-RECHAR-AMT TO #5498-TRAD-IRA-RECHAR-E
                            pnd_5498_Control_Totals_E_Pnd_5498_Roth_Ira_Conversion_E.nadd(ldaTwrl9710.getForm_Tirf_Roth_Ira_Conversion());                                //Natural: ADD TIRF-ROTH-IRA-CONVERSION TO #5498-ROTH-IRA-CONVERSION-E
                            pnd_5498_Control_Totals_E_Pnd_5498_Account_Fmv_E.nadd(ldaTwrl9710.getForm_Tirf_Account_Fmv());                                                //Natural: ADD TIRF-ACCOUNT-FMV TO #5498-ACCOUNT-FMV-E
                            //*  BK
                            pnd_5498_Control_Totals_E_Pnd_5498_Sep_Amt_E.nadd(ldaTwrl9710.getForm_Tirf_Sep_Amt());                                                        //Natural: ADD TIRF-SEP-AMT TO #5498-SEP-AMT-E
                            //*  DASDH
                            pnd_5498_Control_Totals_E_Pnd_5498_Postpn_Amt_E.nadd(ldaTwrl9710.getForm_Tirf_Postpn_Amt());                                                  //Natural: ADD TIRF-POSTPN-AMT TO #5498-POSTPN-AMT-E
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_5498_Control_Totals_E_No_Pnd_5498_Form_Cnt_E_No.nadd(1);                                                                                  //Natural: ADD 1 TO #5498-FORM-CNT-E-NO
                            pnd_5498_Control_Totals_E_No_Pnd_5498_Trad_Ira_Contrib_E_No.nadd(ldaTwrl9710.getForm_Tirf_Trad_Ira_Contrib());                                //Natural: ADD TIRF-TRAD-IRA-CONTRIB TO #5498-TRAD-IRA-CONTRIB-E-NO
                            pnd_5498_Control_Totals_E_No_Pnd_5498_Roth_Ira_Contrib_E_No.nadd(ldaTwrl9710.getForm_Tirf_Roth_Ira_Contrib());                                //Natural: ADD TIRF-ROTH-IRA-CONTRIB TO #5498-ROTH-IRA-CONTRIB-E-NO
                            pnd_5498_Control_Totals_E_No_Pnd_5498_Trad_Ira_Rollover_E_No.nadd(ldaTwrl9710.getForm_Tirf_Trad_Ira_Rollover());                              //Natural: ADD TIRF-TRAD-IRA-ROLLOVER TO #5498-TRAD-IRA-ROLLOVER-E-NO
                            pnd_5498_Control_Totals_E_No_Pnd_5498_Trad_Ira_Rechar_E_No.nadd(ldaTwrl9710.getForm_Tirf_Ira_Rechar_Amt());                                   //Natural: ADD TIRF-IRA-RECHAR-AMT TO #5498-TRAD-IRA-RECHAR-E-NO
                            pnd_5498_Control_Totals_E_No_Pnd_5498_Roth_Ira_Conversion_E_No.nadd(ldaTwrl9710.getForm_Tirf_Roth_Ira_Conversion());                          //Natural: ADD TIRF-ROTH-IRA-CONVERSION TO #5498-ROTH-IRA-CONVERSION-E-NO
                            pnd_5498_Control_Totals_E_No_Pnd_5498_Account_Fmv_E_No.nadd(ldaTwrl9710.getForm_Tirf_Account_Fmv());                                          //Natural: ADD TIRF-ACCOUNT-FMV TO #5498-ACCOUNT-FMV-E-NO
                            //*  BK
                            pnd_5498_Control_Totals_E_No_Pnd_5498_Sep_Amt_E_No.nadd(ldaTwrl9710.getForm_Tirf_Sep_Amt());                                                  //Natural: ADD TIRF-SEP-AMT TO #5498-SEP-AMT-E-NO
                            //*  DASDH
                            pnd_5498_Control_Totals_E_No_Pnd_5498_Postpn_Amt_E_No.nadd(ldaTwrl9710.getForm_Tirf_Postpn_Amt());                                            //Natural: ADD TIRF-POSTPN-AMT TO #5498-POSTPN-AMT-E-NO
                            //*  480.7C
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 5
                    else if (condition((ldaTwrl9710.getForm_Tirf_Form_Type().equals(5))))
                    {
                        decideConditionsMet1655++;
                        pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                              //Natural: ADD TIRF-GROSS-AMT TO #4807C-GROSS-AMT ( 1 )
                        pnd_4807c_Control_Totals_Pnd_4807c_Int_Amt.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                  //Natural: ADD TIRF-INT-AMT TO #4807C-INT-AMT ( 1 )
                        pnd_4807c_Control_Totals_Pnd_4807c_Taxable_Amt.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Taxable_Amt());                                          //Natural: ADD TIRF-TAXABLE-AMT TO #4807C-TAXABLE-AMT ( 1 )
                        pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Ivc_Amt());                                                  //Natural: ADD TIRF-IVC-AMT TO #4807C-IVC-AMT ( 1 )
                        pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt_Roll.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll());                                 //Natural: ADD TIRF-PR-GROSS-AMT-ROLL TO #4807C-GROSS-AMT-ROLL ( 1 )
                        pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt_Roll.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll());                                     //Natural: ADD TIRF-PR-IVC-AMT-ROLL TO #4807C-IVC-AMT-ROLL ( 1 )
                        if (condition(pnd_E_Deliver.equals(true)))                                                                                                        //Natural: IF #E-DELIVER EQ TRUE
                        {
                            pnd_4807c_Control_Totals_E_Pnd_4807c_Gross_Amt_E.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                      //Natural: ADD TIRF-GROSS-AMT TO #4807C-GROSS-AMT-E ( 1 )
                            pnd_4807c_Control_Totals_E_Pnd_4807c_Int_Amt_E.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                          //Natural: ADD TIRF-INT-AMT TO #4807C-INT-AMT-E ( 1 )
                            pnd_4807c_Control_Totals_E_Pnd_4807c_Taxable_Amt_E.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Taxable_Amt());                                  //Natural: ADD TIRF-TAXABLE-AMT TO #4807C-TAXABLE-AMT-E ( 1 )
                            pnd_4807c_Control_Totals_E_Pnd_4807c_Ivc_Amt_E.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Ivc_Amt());                                          //Natural: ADD TIRF-IVC-AMT TO #4807C-IVC-AMT-E ( 1 )
                            pnd_4807c_Control_Totals_E_Pnd_4807c_Gross_Amt_Roll_E.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll());                         //Natural: ADD TIRF-PR-GROSS-AMT-ROLL TO #4807C-GROSS-AMT-ROLL-E ( 1 )
                            pnd_4807c_Control_Totals_E_Pnd_4807c_Ivc_Amt_Roll_E.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll());                             //Natural: ADD TIRF-PR-IVC-AMT-ROLL TO #4807C-IVC-AMT-ROLL-E ( 1 )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_4807c_Control_Totals_E_No_Pnd_4807c_Gross_Amt_E_No.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                //Natural: ADD TIRF-GROSS-AMT TO #4807C-GROSS-AMT-E-NO ( 1 )
                            pnd_4807c_Control_Totals_E_No_Pnd_4807c_Int_Amt_E_No.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                    //Natural: ADD TIRF-INT-AMT TO #4807C-INT-AMT-E-NO ( 1 )
                            pnd_4807c_Control_Totals_E_No_Pnd_4807c_Taxable_Amt_E_No.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Taxable_Amt());                            //Natural: ADD TIRF-TAXABLE-AMT TO #4807C-TAXABLE-AMT-E-NO ( 1 )
                            pnd_4807c_Control_Totals_E_No_Pnd_4807c_Ivc_Amt_E_No.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Ivc_Amt());                                    //Natural: ADD TIRF-IVC-AMT TO #4807C-IVC-AMT-E-NO ( 1 )
                            pnd_4807c_Control_Totals_E_No_Pnd_4807c_Gross_Amt_Roll_E_No.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll());                   //Natural: ADD TIRF-PR-GROSS-AMT-ROLL TO #4807C-GROSS-AMT-ROLL-E-NO ( 1 )
                            pnd_4807c_Control_Totals_E_No_Pnd_4807c_Ivc_Amt_Roll_E_No.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll());                       //Natural: ADD TIRF-PR-IVC-AMT-ROLL TO #4807C-IVC-AMT-ROLL-E-NO ( 1 )
                            //*  NR4 (CANADIAN)
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 7
                    else if (condition((ldaTwrl9710.getForm_Tirf_Form_Type().equals(7))))
                    {
                        decideConditionsMet1655++;
                        //*  PERIODIC
                        //*  LUMP SUM & RTB
                        //*  INTEREST
                        short decideConditionsMet1862 = 0;                                                                                                                //Natural: DECIDE ON FIRST VALUE TIRF-NR4-INCOME-CODE;//Natural: VALUE '02'
                        if (condition((ldaTwrl9710.getForm_Tirf_Nr4_Income_Code().equals("02"))))
                        {
                            decideConditionsMet1862++;
                            pnd_Nr4_Ndx.setValue(1);                                                                                                                      //Natural: ASSIGN #NR4-NDX := 1
                        }                                                                                                                                                 //Natural: VALUE '03'
                        else if (condition((ldaTwrl9710.getForm_Tirf_Nr4_Income_Code().equals("03"))))
                        {
                            decideConditionsMet1862++;
                            pnd_Nr4_Ndx.setValue(2);                                                                                                                      //Natural: ASSIGN #NR4-NDX := 2
                        }                                                                                                                                                 //Natural: VALUE '62'
                        else if (condition((ldaTwrl9710.getForm_Tirf_Nr4_Income_Code().equals("62"))))
                        {
                            decideConditionsMet1862++;
                            pnd_Nr4_Ndx.setValue(3);                                                                                                                      //Natural: ASSIGN #NR4-NDX := 3
                        }                                                                                                                                                 //Natural: NONE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-DECIDE
                        pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt.getValue(pnd_Nr4_Ndx).nadd(1);                                                                            //Natural: ADD 1 TO #NR4-FORM-CNT ( #NR4-NDX )
                        pnd_Nr4_Control_Totals_Pnd_Nr4_Income_Code.getValue(pnd_Nr4_Ndx).setValue(ldaTwrl9710.getForm_Tirf_Nr4_Income_Code());                            //Natural: MOVE TIRF-NR4-INCOME-CODE TO #NR4-INCOME-CODE ( #NR4-NDX )
                        pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt.getValue(pnd_Nr4_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                        //Natural: ADD TIRF-GROSS-AMT TO #NR4-GROSS-AMT ( #NR4-NDX )
                        pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt.getValue(pnd_Nr4_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                       //Natural: ADD TIRF-INT-AMT TO #NR4-INTEREST-AMT ( #NR4-NDX )
                        pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld.getValue(pnd_Nr4_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                //Natural: ADD TIRF-FED-TAX-WTHLD TO #NR4-FED-TAX-WTHLD ( #NR4-NDX )
                        //*      INCREMENT TOTALS
                        pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt.getValue(4).nadd(1);                                                                                      //Natural: ADD 1 TO #NR4-FORM-CNT ( 4 )
                        pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt.getValue(4).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                  //Natural: ADD TIRF-GROSS-AMT TO #NR4-GROSS-AMT ( 4 )
                        pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt.getValue(4).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                 //Natural: ADD TIRF-INT-AMT TO #NR4-INTEREST-AMT ( 4 )
                        pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld.getValue(4).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                          //Natural: ADD TIRF-FED-TAX-WTHLD TO #NR4-FED-TAX-WTHLD ( 4 )
                        if (condition(pnd_E_Deliver.equals(true)))                                                                                                        //Natural: IF #E-DELIVER EQ TRUE
                        {
                            pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt_E.getValue(pnd_Nr4_Ndx).nadd(1);                                                                      //Natural: ADD 1 TO #NR4-FORM-CNT-E ( #NR4-NDX )
                            pnd_Nr4_Control_Totals_Pnd_Nr4_Income_Code_E.getValue(pnd_Nr4_Ndx).setValue(ldaTwrl9710.getForm_Tirf_Nr4_Income_Code());                      //Natural: MOVE TIRF-NR4-INCOME-CODE TO #NR4-INCOME-CODE-E ( #NR4-NDX )
                            pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt_E.getValue(pnd_Nr4_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                  //Natural: ADD TIRF-GROSS-AMT TO #NR4-GROSS-AMT-E ( #NR4-NDX )
                            pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt_E.getValue(pnd_Nr4_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                 //Natural: ADD TIRF-INT-AMT TO #NR4-INTEREST-AMT-E ( #NR4-NDX )
                            pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld_E.getValue(pnd_Nr4_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                          //Natural: ADD TIRF-FED-TAX-WTHLD TO #NR4-FED-TAX-WTHLD-E ( #NR4-NDX )
                            //*      INCREMENT TOTALS
                            pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt_E.getValue(4).nadd(1);                                                                                //Natural: ADD 1 TO #NR4-FORM-CNT-E ( 4 )
                            pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt_E.getValue(4).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                            //Natural: ADD TIRF-GROSS-AMT TO #NR4-GROSS-AMT-E ( 4 )
                            pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt_E.getValue(4).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                           //Natural: ADD TIRF-INT-AMT TO #NR4-INTEREST-AMT-E ( 4 )
                            pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld_E.getValue(4).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                    //Natural: ADD TIRF-FED-TAX-WTHLD TO #NR4-FED-TAX-WTHLD-E ( 4 )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt_E_No.getValue(pnd_Nr4_Ndx).nadd(1);                                                                   //Natural: ADD 1 TO #NR4-FORM-CNT-E-NO ( #NR4-NDX )
                            pnd_Nr4_Control_Totals_Pnd_Nr4_Income_Code_E_No.getValue(pnd_Nr4_Ndx).setValue(ldaTwrl9710.getForm_Tirf_Nr4_Income_Code());                   //Natural: MOVE TIRF-NR4-INCOME-CODE TO #NR4-INCOME-CODE-E-NO ( #NR4-NDX )
                            pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt_E_No.getValue(pnd_Nr4_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                               //Natural: ADD TIRF-GROSS-AMT TO #NR4-GROSS-AMT-E-NO ( #NR4-NDX )
                            pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt_E_No.getValue(pnd_Nr4_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                              //Natural: ADD TIRF-INT-AMT TO #NR4-INTEREST-AMT-E-NO ( #NR4-NDX )
                            pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld_E_No.getValue(pnd_Nr4_Ndx).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                       //Natural: ADD TIRF-FED-TAX-WTHLD TO #NR4-FED-TAX-WTHLD-E-NO ( #NR4-NDX )
                            //*      INCREMENT TOTALS
                            pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt_E_No.getValue(4).nadd(1);                                                                             //Natural: ADD 1 TO #NR4-FORM-CNT-E-NO ( 4 )
                            pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt_E_No.getValue(4).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                         //Natural: ADD TIRF-GROSS-AMT TO #NR4-GROSS-AMT-E-NO ( 4 )
                            pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt_E_No.getValue(4).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                        //Natural: ADD TIRF-INT-AMT TO #NR4-INTEREST-AMT-E-NO ( 4 )
                            pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld_E_No.getValue(4).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                 //Natural: ADD TIRF-FED-TAX-WTHLD TO #NR4-FED-TAX-WTHLD-E-NO ( 4 )
                            //*  SUNY
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 10
                    else if (condition((ldaTwrl9710.getForm_Tirf_Form_Type().equals(10))))
                    {
                        decideConditionsMet1655++;
                        if (condition(ldaTwrl9710.getForm_Tirf_Contract_Nbr().equals(" ")))                                                                               //Natural: IF TIRF-CONTRACT-NBR = ' '
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                        FOR02:                                                                                                                                            //Natural: FOR #M 1 C*TIRF-LTR-GRP
                        for (pnd_M.setValue(1); condition(pnd_M.lessOrEqual(ldaTwrl9710.getForm_Count_Casttirf_Ltr_Grp())); pnd_M.nadd(1))
                        {
                            if (condition(ldaTwrl9710.getForm_Tirf_Ltr_Gross_Amt().getValue(pnd_M).equals(getZero()) || ldaTwrl9710.getForm_Tirf_Ltr_Per().getValue(pnd_M).equals(getZero()))) //Natural: IF TIRF-LTR-GROSS-AMT ( #M ) = 0 OR TIRF-LTR-PER ( #M ) = 0
                            {
                                if (condition(true)) continue;                                                                                                            //Natural: ESCAPE TOP
                            }                                                                                                                                             //Natural: END-IF
                            pnd_Suny_Control_Totals_Pnd_Suny_Contract_Ctr.nadd(1);                                                                                        //Natural: ADD 1 TO #SUNY-CONTRACT-CTR
                            if (condition(pnd_E_Deliver.getBoolean()))                                                                                                    //Natural: IF #E-DELIVER
                            {
                                pnd_Suny_Control_Totals_E_Pnd_Suny_Contract_Ctr_E.nadd(1);                                                                                //Natural: ADD 1 TO #SUNY-CONTRACT-CTR-E
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Suny_Control_Totals_E_No_Pnd_Suny_Contract_Ctr_E_No.nadd(1);                                                                          //Natural: ADD 1 TO #SUNY-CONTRACT-CTR-E-NO
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*      ADD 1 TO #SUNY-CONTRACT-CTR
                        //*      IF #E-DELIVER
                        //*         ADD 1 TO #SUNY-CONTRACT-CTR-E
                        //*      ELSE
                        //*         ADD 1 TO #SUNY-CONTRACT-CTR-E-NO
                        //*      END-IF
                        pnd_Suny_Control_Totals_Pnd_Suny_Gross_Amt.nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                            //Natural: ADD TIRF-GROSS-AMT TO #SUNY-GROSS-AMT
                        pnd_Suny_Control_Totals_Pnd_Suny_Fed_Tax_Wthld.nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                                    //Natural: ADD TIRF-FED-TAX-WTHLD TO #SUNY-FED-TAX-WTHLD
                        pnd_Suny_Control_Totals_Pnd_Suny_State_Tax_Wthld.nadd(ldaTwrl9710.getForm_Tirf_Summ_State_Tax_Wthld());                                           //Natural: ADD TIRF-SUMM-STATE-TAX-WTHLD TO #SUNY-STATE-TAX-WTHLD
                        pnd_Suny_Control_Totals_Pnd_Suny_Loc_Tax_Wthld.nadd(ldaTwrl9710.getForm_Tirf_Summ_Local_Tax_Wthld());                                             //Natural: ADD TIRF-SUMM-LOCAL-TAX-WTHLD TO #SUNY-LOC-TAX-WTHLD
                        if (condition(pnd_E_Deliver.equals(true)))                                                                                                        //Natural: IF #E-DELIVER EQ TRUE
                        {
                            pnd_Suny_Control_Totals_E_Pnd_Suny_Gross_Amt_E.nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                    //Natural: ADD TIRF-GROSS-AMT TO #SUNY-GROSS-AMT-E
                            pnd_Suny_Control_Totals_E_Pnd_Suny_Fed_Tax_Wthld_E.nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                            //Natural: ADD TIRF-FED-TAX-WTHLD TO #SUNY-FED-TAX-WTHLD-E
                            pnd_Suny_Control_Totals_E_Pnd_Suny_State_Tax_Wthld_E.nadd(ldaTwrl9710.getForm_Tirf_Summ_State_Tax_Wthld());                                   //Natural: ADD TIRF-SUMM-STATE-TAX-WTHLD TO #SUNY-STATE-TAX-WTHLD-E
                            pnd_Suny_Control_Totals_E_Pnd_Suny_Loc_Tax_Wthld_E.nadd(ldaTwrl9710.getForm_Tirf_Summ_Local_Tax_Wthld());                                     //Natural: ADD TIRF-SUMM-LOCAL-TAX-WTHLD TO #SUNY-LOC-TAX-WTHLD-E
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Suny_Control_Totals_E_No_Pnd_Suny_Gross_Amt_E_No.nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                              //Natural: ADD TIRF-GROSS-AMT TO #SUNY-GROSS-AMT-E-NO
                            pnd_Suny_Control_Totals_E_No_Pnd_Suny_Fed_Tax_Wthld_E_No.nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                      //Natural: ADD TIRF-FED-TAX-WTHLD TO #SUNY-FED-TAX-WTHLD-E-NO
                            pnd_Suny_Control_Totals_E_No_Pnd_Suny_State_Tax_Wthld_E_No.nadd(ldaTwrl9710.getForm_Tirf_Summ_State_Tax_Wthld());                             //Natural: ADD TIRF-SUMM-STATE-TAX-WTHLD TO #SUNY-STATE-TAX-WTHLD-E-NO
                            pnd_Suny_Control_Totals_E_No_Pnd_Suny_Loc_Tax_Wthld_E_No.nadd(ldaTwrl9710.getForm_Tirf_Summ_Local_Tax_Wthld());                               //Natural: ADD TIRF-SUMM-LOCAL-TAX-WTHLD TO #SUNY-LOC-TAX-WTHLD-E-NO
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    rEAD_FORMTirf_Tax_YearOld.setValue(ldaTwrl9710.getForm_Tirf_Tax_Year());                                                                              //Natural: END-READ
                    rEAD_FORMTirf_Company_CdeOld.setValue(ldaTwrl9710.getForm_Tirf_Company_Cde());
                    rEAD_FORMTirf_Form_TypeOld.setValue(ldaTwrl9710.getForm_Tirf_Form_Type());
                }
                if (condition(ldaTwrl9710.getVw_form().getAstCOUNTER().greater(0)))
                {
                    atBreakEventRead_Form(endOfDataReadForm);
                }
                if (Global.isEscape()) return;
                if (condition(pnd_1099r_Loop.equals(getZero())))                                                                                                          //Natural: IF #1099R-LOOP EQ 0
                {
                    pnd_Form_Text.setValue("1099-R");                                                                                                                     //Natural: MOVE '1099-R' TO #FORM-TEXT
                    pnd_Form_Cnt.setValue(pnd_Form_Cnt_All_1099r);                                                                                                        //Natural: ASSIGN #FORM-CNT := #FORM-CNT-ALL-1099R
                    pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt.setValue(pnd_1099r_Gross_Amt_All_1099r);                                                           //Natural: ASSIGN #1099R-GROSS-AMT := #1099R-GROSS-AMT-ALL-1099R
                    pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt.setValue(pnd_1099r_Ivc_Amt_All_1099r);                                                               //Natural: ASSIGN #1099R-IVC-AMT := #1099R-IVC-AMT-ALL-1099R
                    pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt.setValue(pnd_1099r_Taxable_Amt_All_1099r);                                                       //Natural: ASSIGN #1099R-TAXABLE-AMT := #1099R-TAXABLE-AMT-ALL-1099R
                    pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld.setValue(pnd_1099r_State_Tax_Wthld_All_1099r);                                               //Natural: ASSIGN #1099R-STATE-TAX-WTHLD := #1099R-STATE-TAX-WTHLD-ALL-1099R
                    pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt.setValue(pnd_1099i_Int_Amt_All_1099r);                                                               //Natural: ASSIGN #1099I-INT-AMT := #1099I-INT-AMT-ALL-1099R
                    pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld.setValue(pnd_1099r_Loc_Tax_Wthld_All_1099r);                                                   //Natural: ASSIGN #1099R-LOC-TAX-WTHLD := #1099R-LOC-TAX-WTHLD-ALL-1099R
                    pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld.setValue(pnd_1099_Fed_Tax_Wthld_All_1099r);                                                     //Natural: ASSIGN #1099-FED-TAX-WTHLD := #1099-FED-TAX-WTHLD-ALL-1099R
                    pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt.setValue(pnd_1099r_Irr_Amt_All_1099r);                                                               //Natural: ASSIGN #1099R-IRR-AMT := #1099R-IRR-AMT-ALL-1099R
                    pnd_1099r_Loop.nadd(1);                                                                                                                               //Natural: ADD 1 TO #1099R-LOOP
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-GRAND-TOTAL-1099
                sub_Print_Grand_Total_1099();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                getReports().write(11, new TabSetting(1),"Email",new ColumnSpacing(4),pnd_Form_Cnt_E_1099r, new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_1099r_Gross_Amt_E_1099r,  //Natural: WRITE ( 11 ) 1T 'Email' 4X #FORM-CNT-E-1099R ( EM = Z,ZZZ,ZZ9 ) 1X #1099R-GROSS-AMT-E-1099R ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-IVC-AMT-E-1099R ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-TAXABLE-AMT-E-1099R ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099I-INT-AMT-E-1099R ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099-FED-TAX-WTHLD-E-1099R ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_Ivc_Amt_E_1099r, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(1),pnd_1099r_Taxable_Amt_E_1099r, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099i_Int_Amt_E_1099r, 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099_Fed_Tax_Wthld_E_1099r, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(11, new ColumnSpacing(57),pnd_1099r_State_Tax_Wthld_E_1099r, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_Loc_Tax_Wthld_E_1099r,  //Natural: WRITE ( 11 ) 57X #1099R-STATE-TAX-WTHLD-E-1099R ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-LOC-TAX-WTHLD-E-1099R ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-IRR-AMT-E-1099R ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_Irr_Amt_E_1099r, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(11, new TabSetting(1),"No-Email",new ColumnSpacing(1),pnd_Form_Cnt_E_No_1099r, new ReportEditMask ("Z,ZZZ,ZZ9"),new                    //Natural: WRITE ( 11 ) 1T 'No-Email' 1X #FORM-CNT-E-NO-1099R ( EM = Z,ZZZ,ZZ9 ) 1X #1099R-GROSS-AMT-E-NO-1099R ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-IVC-AMT-E-NO-1099R ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-TAXABLE-AMT-E-NO-1099R ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099I-INT-AMT-E-NO-1099R ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099-FED-TAX-WTHLD-E-NO-1099R ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                    ColumnSpacing(1),pnd_1099r_Gross_Amt_E_No_1099r, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_Ivc_Amt_E_No_1099r, 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_Taxable_Amt_E_No_1099r, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(1),pnd_1099i_Int_Amt_E_No_1099r, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099_Fed_Tax_Wthld_E_No_1099r, 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(11, new ColumnSpacing(57),pnd_1099r_State_Tax_Wthld_E_No_109r, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_Loc_Tax_Wthld_E_No_1099r,  //Natural: WRITE ( 11 ) 57X #1099R-STATE-TAX-WTHLD-E-NO-109R ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-LOC-TAX-WTHLD-E-NO-1099R ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-IRR-AMT-E-NO-1099R ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_Irr_Amt_E_No_1099r, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(11, NEWLINE,NEWLINE);                                                                                                                  //Natural: WRITE ( 11 ) //
                if (Global.isEscape()) return;
                if (condition(pnd_1099r_Loop.equals(1)))                                                                                                                  //Natural: IF #1099R-LOOP EQ 1
                {
                    pnd_Form_Text.setValue("1099-INT");                                                                                                                   //Natural: MOVE '1099-INT' TO #FORM-TEXT
                    pnd_Form_Cnt.setValue(pnd_Form_Cnt_All_1099int);                                                                                                      //Natural: ASSIGN #FORM-CNT := #FORM-CNT-ALL-1099INT
                    pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt.setValue(pnd_1099r_Gross_Amt_All_1099int);                                                         //Natural: ASSIGN #1099R-GROSS-AMT := #1099R-GROSS-AMT-ALL-1099INT
                    pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt.setValue(pnd_1099r_Ivc_Amt_All_1099int);                                                             //Natural: ASSIGN #1099R-IVC-AMT := #1099R-IVC-AMT-ALL-1099INT
                    pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt.setValue(pnd_1099r_Taxable_Amt_All_1099int);                                                     //Natural: ASSIGN #1099R-TAXABLE-AMT := #1099R-TAXABLE-AMT-ALL-1099INT
                    pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld.setValue(pnd_1099r_State_Tax_Wthld_All_1099int);                                             //Natural: ASSIGN #1099R-STATE-TAX-WTHLD := #1099R-STATE-TAX-WTHLD-ALL-1099INT
                    pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt.setValue(pnd_1099i_Int_Amt_All_1099int);                                                             //Natural: ASSIGN #1099I-INT-AMT := #1099I-INT-AMT-ALL-1099INT
                    pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld.setValue(pnd_1099r_Loc_Tax_Wthld_All_1099int);                                                 //Natural: ASSIGN #1099R-LOC-TAX-WTHLD := #1099R-LOC-TAX-WTHLD-ALL-1099INT
                    pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld.setValue(pnd_1099_Fed_Tax_Wthld_All_1099int);                                                   //Natural: ASSIGN #1099-FED-TAX-WTHLD := #1099-FED-TAX-WTHLD-ALL-1099INT
                    pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt.setValue(pnd_1099r_Irr_Amt_All_1099int);                                                             //Natural: ASSIGN #1099R-IRR-AMT := #1099R-IRR-AMT-ALL-1099INT
                    pnd_1099r_Loop.nadd(1);                                                                                                                               //Natural: ADD 1 TO #1099R-LOOP
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-GRAND-TOTAL-1099
                sub_Print_Grand_Total_1099();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                getReports().write(11, new TabSetting(1),"Email",new ColumnSpacing(4),pnd_Form_Cnt_E_1099i, new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_1099r_Gross_Amt_E_1099i,  //Natural: WRITE ( 11 ) 1T 'Email' 4X #FORM-CNT-E-1099I ( EM = Z,ZZZ,ZZ9 ) 1X #1099R-GROSS-AMT-E-1099I ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-IVC-AMT-E-1099I ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-TAXABLE-AMT-E-1099I ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099I-INT-AMT-E-1099I ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099-FED-TAX-WTHLD-E-1099I ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_Ivc_Amt_E_1099i, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(1),pnd_1099r_Taxable_Amt_E_1099i, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099i_Int_Amt_E_1099i, 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099_Fed_Tax_Wthld_E_1099i, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(11, new ColumnSpacing(57),pnd_1099r_State_Tax_Wthld_E_1099i, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_Loc_Tax_Wthld_E_1099i,  //Natural: WRITE ( 11 ) 57X #1099R-STATE-TAX-WTHLD-E-1099I ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-LOC-TAX-WTHLD-E-1099I ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-IRR-AMT-E-1099I ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_Irr_Amt_E_1099i, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(11, new TabSetting(1),"No-Email",new ColumnSpacing(1),pnd_Form_Cnt_E_No_1099i, new ReportEditMask ("Z,ZZZ,ZZ9"),new                    //Natural: WRITE ( 11 ) 1T 'No-Email' 1X #FORM-CNT-E-NO-1099I ( EM = Z,ZZZ,ZZ9 ) 1X #1099R-GROSS-AMT-E-NO-1099I ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-IVC-AMT-E-NO-1099I ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-TAXABLE-AMT-E-NO-1099I ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099I-INT-AMT-E-NO-1099I ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099-FED-TAX-WTHLD-E-NO-1099I ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                    ColumnSpacing(1),pnd_1099r_Gross_Amt_E_No_1099i, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_Ivc_Amt_E_No_1099i, 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_Taxable_Amt_E_No_1099i, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(1),pnd_1099i_Int_Amt_E_No_1099i, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099_Fed_Tax_Wthld_E_No_1099i, 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(11, new ColumnSpacing(57),pnd_1099r_State_Tax_Wthld_E_No_109i, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_Loc_Tax_Wthld_E_No_1099i,  //Natural: WRITE ( 11 ) 57X #1099R-STATE-TAX-WTHLD-E-NO-109I ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-LOC-TAX-WTHLD-E-NO-1099I ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-IRR-AMT-E-NO-1099I ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_Irr_Amt_E_No_1099i, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(11, NEWLINE,NEWLINE);                                                                                                                  //Natural: WRITE ( 11 ) //
                if (Global.isEscape()) return;
                if (condition(pnd_1099r_Loop.equals(2)))                                                                                                                  //Natural: IF #1099R-LOOP EQ 2
                {
                    pnd_Form_Text.setValue("TOTAL");                                                                                                                      //Natural: MOVE 'TOTAL' TO #FORM-TEXT
                    pnd_Form_Cnt.setValue(pnd_Form_Cnt_All_1099tot);                                                                                                      //Natural: ASSIGN #FORM-CNT := #FORM-CNT-ALL-1099TOT
                    pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt.setValue(pnd_1099r_Gross_Amt_All_1099tot);                                                         //Natural: ASSIGN #1099R-GROSS-AMT := #1099R-GROSS-AMT-ALL-1099TOT
                    pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt.setValue(pnd_1099r_Ivc_Amt_All_1099tot);                                                             //Natural: ASSIGN #1099R-IVC-AMT := #1099R-IVC-AMT-ALL-1099TOT
                    pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt.setValue(pnd_1099r_Taxable_Amt_All_1099tot);                                                     //Natural: ASSIGN #1099R-TAXABLE-AMT := #1099R-TAXABLE-AMT-ALL-1099TOT
                    pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld.setValue(pnd_1099r_State_Tax_Wthld_All_1099tot);                                             //Natural: ASSIGN #1099R-STATE-TAX-WTHLD := #1099R-STATE-TAX-WTHLD-ALL-1099TOT
                    pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt.setValue(pnd_1099i_Int_Amt_All_1099tot);                                                             //Natural: ASSIGN #1099I-INT-AMT := #1099I-INT-AMT-ALL-1099TOT
                    pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld.setValue(pnd_1099r_Loc_Tax_Wthld_All_1099tot);                                                 //Natural: ASSIGN #1099R-LOC-TAX-WTHLD := #1099R-LOC-TAX-WTHLD-ALL-1099TOT
                    pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld.setValue(pnd_1099_Fed_Tax_Wthld_All_1099tot);                                                   //Natural: ASSIGN #1099-FED-TAX-WTHLD := #1099-FED-TAX-WTHLD-ALL-1099TOT
                    pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt.setValue(pnd_1099r_Irr_Amt_All_1099tot);                                                             //Natural: ASSIGN #1099R-IRR-AMT := #1099R-IRR-AMT-ALL-1099TOT
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-GRAND-TOTAL-1099
                sub_Print_Grand_Total_1099();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                getReports().write(11, new TabSetting(1),"Email",new ColumnSpacing(4),pnd_Total_1099_Form_Cnt_E_Gr, new ReportEditMask ("Z,ZZZ,ZZ9"),new                  //Natural: WRITE ( 11 ) 1T 'Email' 4X #TOTAL-1099-FORM-CNT-E-GR ( EM = Z,ZZZ,ZZ9 ) 1X #TOTAL-1099-GROSS-AMT-E-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-IVC-AMT-E-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-TAXABLE-AMT-E-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-INT-AMT-E-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-FED-TAX-WTHLD-E-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                    ColumnSpacing(1),pnd_Total_1099_Gross_Amt_E_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Total_1099_Ivc_Amt_E_Gr, 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Total_1099_Taxable_Amt_E_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(1),pnd_Total_1099_Int_Amt_E_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Total_1099_Fed_Tax_Wthld_E_Gr, 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(11, new ColumnSpacing(57),pnd_Total_1099_State_Tax_Wthld_E_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Total_1099_Loc_Tax_Wthld_E_Gr,  //Natural: WRITE ( 11 ) 57X #TOTAL-1099-STATE-TAX-WTHLD-E-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-LOC-TAX-WTHLD-E-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-IRR-AMT-E-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Total_1099_Irr_Amt_E_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(11, new TabSetting(1),"No-Email",new ColumnSpacing(1),pnd_Total_1099_Form_Cnt_E_No_Gr, new ReportEditMask ("Z,ZZZ,ZZ9"),new            //Natural: WRITE ( 11 ) 1T 'No-Email' 1X #TOTAL-1099-FORM-CNT-E-NO-GR ( EM = Z,ZZZ,ZZ9 ) 1X #TOTAL-1099-GROSS-AMT-E-NO-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-IVC-AMT-E-NO-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-TAXABLE-AMT-E-NO-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-INT-AMT-E-NO-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-FED-TAX-WTHLD-E-NO-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                    ColumnSpacing(1),pnd_Total_1099_Gross_Amt_E_No_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Total_1099_Ivc_Amt_E_No_Gr, 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Total_1099_Taxable_Amt_E_No_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(1),pnd_Total_1099_Int_Amt_E_No_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Total_1099_Fed_Tax_Wthld_E_No_Gr, 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(11, new ColumnSpacing(57),pnd_Total_1099_State_Tax_E_No_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Total_1099_Loc_Tax_E_No_Gr,  //Natural: WRITE ( 11 ) 57X #TOTAL-1099-STATE-TAX-E-NO-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-LOC-TAX-E-NO-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-IRR-AMT-E-NO-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Total_1099_Irr_Amt_E_No_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                //* **********************************************************************
                                                                                                                                                                          //Natural: PERFORM PRINT-1042S-GR
                sub_Print_1042s_Gr();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-5498-GR
                sub_Print_5498_Gr();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-480-GR
                sub_Print_480_Gr();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-NR4-GR
                sub_Print_Nr4_Gr();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-5498-P2
                sub_Get_5498_P2();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  RCC
                                                                                                                                                                          //Natural: PERFORM PRINT-5498-P2
                sub_Print_5498_P2();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-SUNY
                sub_Print_Suny();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //* **********************************************************************
                getReports().write(10, "--------------------------------------",NEWLINE,NEWLINE,"Forms Grand Total:           ",pnd_Form_Cnt_Comp_Grand,                  //Natural: WRITE ( 10 ) '--------------------------------------' // 'Forms Grand Total:           ' #FORM-CNT-COMP-GRAND / 'Forms Grand Total Email:     ' #FORM-CNT-COMP-E-GRAND / 'Forms Grand Total No-Email:  ' #FORM-CNT-COMP-E-NO-GRAND
                    NEWLINE,"Forms Grand Total Email:     ",pnd_Form_Cnt_Comp_E_Grand,NEWLINE,"Forms Grand Total No-Email:  ",pnd_Form_Cnt_Comp_E_No_Grand);
                if (Global.isEscape()) return;
                //* ***************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-1099
                //* *************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INCREMENT-1099-TOTAL
                //* ***************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-1099-GRAND-TOTAL
                //* ***************************************
                //* ******************************************
                //* ***************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-5498
                //* ***************************
                //* ****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-1042S
                //* ****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-480-7
                //* **************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INCREMENT-4807-TOTAL
                //* ***************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-4807-GRAND-TOTAL
                //* ***************************************
                //* **************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-NR4
                //* **************************
                //* *******************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HOLD-RPT
                //* *********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-COMPANY-DESC
                //* **************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-REPORTED-TO-IRS
                //* ***************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-SYSTEM-HOLD-CODE
                //* ***************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-GRAND-TOTAL-1099
                //* ****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-1042S-GR
                //* ******************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-5498-GR
                //* ******************************
                //* *****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-480-GR
                //* *****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-NR4-GR
                //* *****************************
                //*  --------------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-5498-P2
                //*  ------------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SUNY
                //*  ------------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-5498-P2
                //*  ------------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ALL-CONTRACTS-P2
                //*  --------------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCESS-IRA-FILE
                //*  --------------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: E-DELIVERY-RECS
                //*  --------------------------------------------------------------------
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Print_1099() throws Exception                                                                                                                        //Natural: PRINT-1099
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        getReports().display(2, "///Type of/Form",                                                                                                                        //Natural: DISPLAY ( 2 ) '///Type of/Form' #FORM-TEXT '///Form/Count' #FORM-CNT ( EM = Z,ZZZ,ZZ9 ) '///Gross/Amount' #1099R-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '///Tax Free/IVC' #1099R-IVC-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) VERT AS 'Taxable/Amount/-----------/State Tax/Withholding' #1099R-TAXABLE-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #1099R-STATE-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) VERT AS 'Interest/Amount/-----------/Local Tax/Withholding' #1099I-INT-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #1099R-LOC-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) VERT AS 'Federal/Withholding/-----------/IRR Amount' #1099-FED-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #1099R-IRR-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) //
        		pnd_Form_Text,"///Form/Count",
        		pnd_Form_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"///Gross/Amount",
        		pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"///Tax Free/IVC",
        		pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, 
            "Taxable/Amount/-----------/State Tax/Withholding",
        		pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "Interest/Amount/-----------/Local Tax/Withholding",
            
        		pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "Federal/Withholding/-----------/IRR Amount",
            
        		pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().skip(2, 2);                                                                                                                                          //Natural: SKIP ( 2 ) 2
        //*  IF #PRINT-1099-LOOP EQ 0
        if (condition(pnd_Form_Text.equals("1099-R")))                                                                                                                    //Natural: IF #FORM-TEXT EQ '1099-R'
        {
            //*   ADD 1 TO #PRINT-1099-LOOP
            pnd_Form_Cnt_All_1099r.nadd(pnd_Form_Cnt);                                                                                                                    //Natural: ADD #FORM-CNT TO #FORM-CNT-ALL-1099R
            pnd_1099r_Gross_Amt_All_1099r.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt);                                                                       //Natural: ADD #1099R-GROSS-AMT TO #1099R-GROSS-AMT-ALL-1099R
            pnd_1099r_Ivc_Amt_All_1099r.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt);                                                                           //Natural: ADD #1099R-IVC-AMT TO #1099R-IVC-AMT-ALL-1099R
            pnd_1099r_Taxable_Amt_All_1099r.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt);                                                                   //Natural: ADD #1099R-TAXABLE-AMT TO #1099R-TAXABLE-AMT-ALL-1099R
            pnd_1099r_State_Tax_Wthld_All_1099r.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld);                                                           //Natural: ADD #1099R-STATE-TAX-WTHLD TO #1099R-STATE-TAX-WTHLD-ALL-1099R
            pnd_1099i_Int_Amt_All_1099r.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt);                                                                           //Natural: ADD #1099I-INT-AMT TO #1099I-INT-AMT-ALL-1099R
            pnd_1099r_Loc_Tax_Wthld_All_1099r.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld);                                                               //Natural: ADD #1099R-LOC-TAX-WTHLD TO #1099R-LOC-TAX-WTHLD-ALL-1099R
            pnd_1099_Fed_Tax_Wthld_All_1099r.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld);                                                                 //Natural: ADD #1099-FED-TAX-WTHLD TO #1099-FED-TAX-WTHLD-ALL-1099R
            pnd_1099r_Irr_Amt_All_1099r.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt);                                                                           //Natural: ADD #1099R-IRR-AMT TO #1099R-IRR-AMT-ALL-1099R
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE IMMEDIATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #PRINT-1099-LOOP EQ 1
        if (condition(pnd_Form_Text.equals("1099-INT")))                                                                                                                  //Natural: IF #FORM-TEXT EQ '1099-INT'
        {
            pnd_Form_Cnt_All_1099int.nadd(pnd_Form_Cnt);                                                                                                                  //Natural: ADD #FORM-CNT TO #FORM-CNT-ALL-1099INT
            pnd_1099r_Gross_Amt_All_1099int.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt);                                                                     //Natural: ADD #1099R-GROSS-AMT TO #1099R-GROSS-AMT-ALL-1099INT
            pnd_1099r_Ivc_Amt_All_1099int.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt);                                                                         //Natural: ADD #1099R-IVC-AMT TO #1099R-IVC-AMT-ALL-1099INT
            pnd_1099r_Taxable_Amt_All_1099int.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt);                                                                 //Natural: ADD #1099R-TAXABLE-AMT TO #1099R-TAXABLE-AMT-ALL-1099INT
            pnd_1099r_State_Tax_Wthld_All_1099int.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld);                                                         //Natural: ADD #1099R-STATE-TAX-WTHLD TO #1099R-STATE-TAX-WTHLD-ALL-1099INT
            pnd_1099i_Int_Amt_All_1099int.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt);                                                                         //Natural: ADD #1099I-INT-AMT TO #1099I-INT-AMT-ALL-1099INT
            pnd_1099r_Loc_Tax_Wthld_All_1099int.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld);                                                             //Natural: ADD #1099R-LOC-TAX-WTHLD TO #1099R-LOC-TAX-WTHLD-ALL-1099INT
            pnd_1099_Fed_Tax_Wthld_All_1099int.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld);                                                               //Natural: ADD #1099-FED-TAX-WTHLD TO #1099-FED-TAX-WTHLD-ALL-1099INT
            pnd_1099r_Irr_Amt_All_1099int.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt);                                                                         //Natural: ADD #1099R-IRR-AMT TO #1099R-IRR-AMT-ALL-1099INT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE IMMEDIATE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Form_Text.equals("TOTAL")))                                                                                                                     //Natural: IF #FORM-TEXT EQ 'TOTAL'
        {
            pnd_Form_Cnt_All_1099tot.nadd(pnd_Form_Cnt);                                                                                                                  //Natural: ADD #FORM-CNT TO #FORM-CNT-ALL-1099TOT
            pnd_1099r_Gross_Amt_All_1099tot.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt);                                                                     //Natural: ADD #1099R-GROSS-AMT TO #1099R-GROSS-AMT-ALL-1099TOT
            pnd_1099r_Ivc_Amt_All_1099tot.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt);                                                                         //Natural: ADD #1099R-IVC-AMT TO #1099R-IVC-AMT-ALL-1099TOT
            pnd_1099r_Taxable_Amt_All_1099tot.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt);                                                                 //Natural: ADD #1099R-TAXABLE-AMT TO #1099R-TAXABLE-AMT-ALL-1099TOT
            pnd_1099r_State_Tax_Wthld_All_1099tot.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld);                                                         //Natural: ADD #1099R-STATE-TAX-WTHLD TO #1099R-STATE-TAX-WTHLD-ALL-1099TOT
            pnd_1099i_Int_Amt_All_1099tot.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt);                                                                         //Natural: ADD #1099I-INT-AMT TO #1099I-INT-AMT-ALL-1099TOT
            pnd_1099r_Loc_Tax_Wthld_All_1099tot.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld);                                                             //Natural: ADD #1099R-LOC-TAX-WTHLD TO #1099R-LOC-TAX-WTHLD-ALL-1099TOT
            pnd_1099_Fed_Tax_Wthld_All_1099tot.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld);                                                               //Natural: ADD #1099-FED-TAX-WTHLD TO #1099-FED-TAX-WTHLD-ALL-1099TOT
            pnd_1099r_Irr_Amt_All_1099tot.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt);                                                                         //Natural: ADD #1099R-IRR-AMT TO #1099R-IRR-AMT-ALL-1099TOT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Increment_1099_Total() throws Exception                                                                                                              //Natural: INCREMENT-1099-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        pnd_1099_Grand_Totals_Pnd_Total_1099_Form_Cnt.nadd(pnd_Form_Cnt);                                                                                                 //Natural: ADD #FORM-CNT TO #TOTAL-1099-FORM-CNT
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_Form_Cnt_E.nadd(pnd_Form_Cnt_E);                                                                                           //Natural: ADD #FORM-CNT-E TO #TOTAL-1099-FORM-CNT-E
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Form_Cnt_E_No.nadd(pnd_Form_Cnt_E_No);                                                                                  //Natural: ADD #FORM-CNT-E-NO TO #TOTAL-1099-FORM-CNT-E-NO
        pnd_1099_Grand_Totals_Pnd_Total_1099_Hold_Cnt.nadd(pnd_Hold_Cnt);                                                                                                 //Natural: ADD #HOLD-CNT TO #TOTAL-1099-HOLD-CNT
        pnd_1099_Grand_Totals_Pnd_Total_1099_Empty_Form_Cnt.nadd(pnd_Empty_Form_Cnt);                                                                                     //Natural: ADD #EMPTY-FORM-CNT TO #TOTAL-1099-EMPTY-FORM-CNT
        pnd_1099_Grand_Totals_Pnd_Total_1099_Gross_Amt.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt);                                                          //Natural: ADD #1099R-GROSS-AMT TO #TOTAL-1099-GROSS-AMT
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_Gross_Amt_E.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Gross_Amt_E);                                                  //Natural: ADD #1099R-GROSS-AMT-E TO #TOTAL-1099-GROSS-AMT-E
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Gross_Amt_E_No.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Gross_Amt_E_No);                                      //Natural: ADD #1099R-GROSS-AMT-E-NO TO #TOTAL-1099-GROSS-AMT-E-NO
        pnd_1099_Grand_Totals_Pnd_Total_1099_Taxable_Amt.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt);                                                      //Natural: ADD #1099R-TAXABLE-AMT TO #TOTAL-1099-TAXABLE-AMT
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_Taxable_Amt_E.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Taxable_Amt_E);                                              //Natural: ADD #1099R-TAXABLE-AMT-E TO #TOTAL-1099-TAXABLE-AMT-E
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Taxable_Amt_E_No.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Taxable_Amt_E_No);                                  //Natural: ADD #1099R-TAXABLE-AMT-E-NO TO #TOTAL-1099-TAXABLE-AMT-E-NO
        pnd_1099_Grand_Totals_Pnd_Total_1099_Fed_Tax_Wthld.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld);                                                   //Natural: ADD #1099-FED-TAX-WTHLD TO #TOTAL-1099-FED-TAX-WTHLD
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_Fed_Tax_Wthld_E.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099_Fed_Tax_Wthld_E);                                           //Natural: ADD #1099-FED-TAX-WTHLD-E TO #TOTAL-1099-FED-TAX-WTHLD-E
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Fed_Tax_Wthld_E_No.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099_Fed_Tax_Wthld_E_No);                               //Natural: ADD #1099-FED-TAX-WTHLD-E-NO TO #TOTAL-1099-FED-TAX-WTHLD-E-NO
        pnd_1099_Grand_Totals_Pnd_Total_1099_Ivc_Amt.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt);                                                              //Natural: ADD #1099R-IVC-AMT TO #TOTAL-1099-IVC-AMT
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_Ivc_Amt_E.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Ivc_Amt_E);                                                      //Natural: ADD #1099R-IVC-AMT-E TO #TOTAL-1099-IVC-AMT-E
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Ivc_Amt_E_No.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Ivc_Amt_E_No);                                          //Natural: ADD #1099R-IVC-AMT-E-NO TO #TOTAL-1099-IVC-AMT-E-NO
        pnd_1099_Grand_Totals_Pnd_Total_1099_State_Tax_Wthld.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld);                                              //Natural: ADD #1099R-STATE-TAX-WTHLD TO #TOTAL-1099-STATE-TAX-WTHLD
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_State_Tax_Wthld_E.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_State_Tax_Wthld_E);                                      //Natural: ADD #1099R-STATE-TAX-WTHLD-E TO #TOTAL-1099-STATE-TAX-WTHLD-E
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_State_Tax_Wthld_E_No.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_State_Tax_Wthld_E_No);                          //Natural: ADD #1099R-STATE-TAX-WTHLD-E-NO TO #TOTAL-1099-STATE-TAX-WTHLD-E-NO
        pnd_1099_Grand_Totals_Pnd_Total_1099_Loc_Tax_Wthld.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld);                                                  //Natural: ADD #1099R-LOC-TAX-WTHLD TO #TOTAL-1099-LOC-TAX-WTHLD
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_Loc_Tax_Wthld_E.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Loc_Tax_Wthld_E);                                          //Natural: ADD #1099R-LOC-TAX-WTHLD-E TO #TOTAL-1099-LOC-TAX-WTHLD-E
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Loc_Tax_Wthld_E_No.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Loc_Tax_Wthld_E_No);                              //Natural: ADD #1099R-LOC-TAX-WTHLD-E-NO TO #TOTAL-1099-LOC-TAX-WTHLD-E-NO
        pnd_1099_Grand_Totals_Pnd_Total_1099_Int_Amt.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt);                                                              //Natural: ADD #1099I-INT-AMT TO #TOTAL-1099-INT-AMT
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_Int_Amt_E.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099i_Int_Amt_E);                                                      //Natural: ADD #1099I-INT-AMT-E TO #TOTAL-1099-INT-AMT-E
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Int_Amt_E_No.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099i_Int_Amt_E_No);                                          //Natural: ADD #1099I-INT-AMT-E-NO TO #TOTAL-1099-INT-AMT-E-NO
        pnd_1099_Grand_Totals_Pnd_Total_1099_Irr_Amt.nadd(pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt);                                                              //Natural: ADD #1099R-IRR-AMT TO #TOTAL-1099-IRR-AMT
        pnd_1099_Grand_Totals_E_Pnd_Total_1099_Irr_Amt_E.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Irr_Amt_E);                                                      //Natural: ADD #1099R-IRR-AMT-E TO #TOTAL-1099-IRR-AMT-E
        pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Irr_Amt_E_No.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Irr_Amt_E_No);                                          //Natural: ADD #1099R-IRR-AMT-E-NO TO #TOTAL-1099-IRR-AMT-E-NO
    }
    private void sub_Print_1099_Grand_Total() throws Exception                                                                                                            //Natural: PRINT-1099-GRAND-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Form_Text.setValue("TOTAL");                                                                                                                                  //Natural: ASSIGN #FORM-TEXT := 'TOTAL'
        pnd_Form_Cnt.setValue(pnd_1099_Grand_Totals_Pnd_Total_1099_Form_Cnt);                                                                                             //Natural: ASSIGN #FORM-CNT := #TOTAL-1099-FORM-CNT
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt.setValue(pnd_1099_Grand_Totals_Pnd_Total_1099_Gross_Amt);                                                      //Natural: ASSIGN #1099R-GROSS-AMT := #TOTAL-1099-GROSS-AMT
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt.setValue(pnd_1099_Grand_Totals_Pnd_Total_1099_Taxable_Amt);                                                  //Natural: ASSIGN #1099R-TAXABLE-AMT := #TOTAL-1099-TAXABLE-AMT
        pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld.setValue(pnd_1099_Grand_Totals_Pnd_Total_1099_Fed_Tax_Wthld);                                               //Natural: ASSIGN #1099-FED-TAX-WTHLD := #TOTAL-1099-FED-TAX-WTHLD
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt.setValue(pnd_1099_Grand_Totals_Pnd_Total_1099_Ivc_Amt);                                                          //Natural: ASSIGN #1099R-IVC-AMT := #TOTAL-1099-IVC-AMT
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld.setValue(pnd_1099_Grand_Totals_Pnd_Total_1099_State_Tax_Wthld);                                          //Natural: ASSIGN #1099R-STATE-TAX-WTHLD := #TOTAL-1099-STATE-TAX-WTHLD
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld.setValue(pnd_1099_Grand_Totals_Pnd_Total_1099_Loc_Tax_Wthld);                                              //Natural: ASSIGN #1099R-LOC-TAX-WTHLD := #TOTAL-1099-LOC-TAX-WTHLD
        pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt.setValue(pnd_1099_Grand_Totals_Pnd_Total_1099_Int_Amt);                                                          //Natural: ASSIGN #1099I-INT-AMT := #TOTAL-1099-INT-AMT
        pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt.setValue(pnd_1099_Grand_Totals_Pnd_Total_1099_Irr_Amt);                                                          //Natural: ASSIGN #1099R-IRR-AMT := #TOTAL-1099-IRR-AMT
                                                                                                                                                                          //Natural: PERFORM PRINT-1099
        sub_Print_1099();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        getReports().write(2, new TabSetting(1),"Email",new ColumnSpacing(4),pnd_1099_Grand_Totals_E_Pnd_Total_1099_Form_Cnt_E, new ReportEditMask ("Z,ZZZ,ZZ9"),new      //Natural: WRITE ( 2 ) 1T 'Email' 4X #TOTAL-1099-FORM-CNT-E ( EM = Z,ZZZ,ZZ9 ) 1X #TOTAL-1099-GROSS-AMT-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-IVC-AMT-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-TAXABLE-AMT-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-INT-AMT-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-FED-TAX-WTHLD-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(1),pnd_1099_Grand_Totals_E_Pnd_Total_1099_Gross_Amt_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099_Grand_Totals_E_Pnd_Total_1099_Ivc_Amt_E, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099_Grand_Totals_E_Pnd_Total_1099_Taxable_Amt_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_1099_Grand_Totals_E_Pnd_Total_1099_Int_Amt_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099_Grand_Totals_E_Pnd_Total_1099_Fed_Tax_Wthld_E, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, new ColumnSpacing(57),pnd_1099_Grand_Totals_E_Pnd_Total_1099_State_Tax_Wthld_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new               //Natural: WRITE ( 2 ) 57X #TOTAL-1099-STATE-TAX-WTHLD-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-LOC-TAX-WTHLD-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-IRR-AMT-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(1),pnd_1099_Grand_Totals_E_Pnd_Total_1099_Loc_Tax_Wthld_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099_Grand_Totals_E_Pnd_Total_1099_Irr_Amt_E, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(1),"No-Email",new ColumnSpacing(1),pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Form_Cnt_E_No, new ReportEditMask               //Natural: WRITE ( 2 ) 1T 'No-Email' 1X #TOTAL-1099-FORM-CNT-E-NO ( EM = Z,ZZZ,ZZ9 ) 1X #TOTAL-1099-GROSS-AMT-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-IVC-AMT-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-TAXABLE-AMT-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-INT-AMT-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-FED-TAX-WTHLD-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Gross_Amt_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Ivc_Amt_E_No, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Taxable_Amt_E_No, new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Int_Amt_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Fed_Tax_Wthld_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, new ColumnSpacing(57),pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_State_Tax_Wthld_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new         //Natural: WRITE ( 2 ) 57X #TOTAL-1099-STATE-TAX-WTHLD-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-LOC-TAX-WTHLD-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-1099-IRR-AMT-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(1),pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Loc_Tax_Wthld_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Irr_Amt_E_No, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, NEWLINE,NEWLINE,NEWLINE,"HELD CNT...:",pnd_1099_Grand_Totals_Pnd_Total_1099_Hold_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"EMPTY FORMS:",pnd_1099_Grand_Totals_Pnd_Total_1099_Empty_Form_Cnt,  //Natural: WRITE ( 2 ) // /'HELD CNT...:' #TOTAL-1099-HOLD-CNT ( EM = Z,ZZZ,ZZ9 ) /'EMPTY FORMS:' #TOTAL-1099-EMPTY-FORM-CNT ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Total_1099_Form_Cnt_E_Gr.nadd(pnd_1099_Grand_Totals_E_Pnd_Total_1099_Form_Cnt_E);                                                                             //Natural: ADD #TOTAL-1099-FORM-CNT-E TO #TOTAL-1099-FORM-CNT-E-GR
        pnd_Total_1099_Gross_Amt_E_Gr.nadd(pnd_1099_Grand_Totals_E_Pnd_Total_1099_Gross_Amt_E);                                                                           //Natural: ADD #TOTAL-1099-GROSS-AMT-E TO #TOTAL-1099-GROSS-AMT-E-GR
        pnd_Total_1099_Ivc_Amt_E_Gr.nadd(pnd_1099_Grand_Totals_E_Pnd_Total_1099_Ivc_Amt_E);                                                                               //Natural: ADD #TOTAL-1099-IVC-AMT-E TO #TOTAL-1099-IVC-AMT-E-GR
        pnd_Total_1099_Taxable_Amt_E_Gr.nadd(pnd_1099_Grand_Totals_E_Pnd_Total_1099_Taxable_Amt_E);                                                                       //Natural: ADD #TOTAL-1099-TAXABLE-AMT-E TO #TOTAL-1099-TAXABLE-AMT-E-GR
        pnd_Total_1099_Int_Amt_E_Gr.nadd(pnd_1099_Grand_Totals_E_Pnd_Total_1099_Int_Amt_E);                                                                               //Natural: ADD #TOTAL-1099-INT-AMT-E TO #TOTAL-1099-INT-AMT-E-GR
        pnd_Total_1099_Fed_Tax_Wthld_E_Gr.nadd(pnd_1099_Grand_Totals_E_Pnd_Total_1099_Fed_Tax_Wthld_E);                                                                   //Natural: ADD #TOTAL-1099-FED-TAX-WTHLD-E TO #TOTAL-1099-FED-TAX-WTHLD-E-GR
        pnd_Total_1099_State_Tax_Wthld_E_Gr.nadd(pnd_1099_Grand_Totals_E_Pnd_Total_1099_State_Tax_Wthld_E);                                                               //Natural: ADD #TOTAL-1099-STATE-TAX-WTHLD-E TO #TOTAL-1099-STATE-TAX-WTHLD-E-GR
        pnd_Total_1099_Loc_Tax_Wthld_E_Gr.nadd(pnd_1099_Grand_Totals_E_Pnd_Total_1099_Loc_Tax_Wthld_E);                                                                   //Natural: ADD #TOTAL-1099-LOC-TAX-WTHLD-E TO #TOTAL-1099-LOC-TAX-WTHLD-E-GR
        pnd_Total_1099_Irr_Amt_E_Gr.nadd(pnd_1099_Grand_Totals_E_Pnd_Total_1099_Irr_Amt_E);                                                                               //Natural: ADD #TOTAL-1099-IRR-AMT-E TO #TOTAL-1099-IRR-AMT-E-GR
        pnd_Total_1099_Form_Cnt_E_No_Gr.nadd(pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Form_Cnt_E_No);                                                                    //Natural: ADD #TOTAL-1099-FORM-CNT-E-NO TO #TOTAL-1099-FORM-CNT-E-NO-GR
        pnd_Total_1099_Gross_Amt_E_No_Gr.nadd(pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Gross_Amt_E_No);                                                                  //Natural: ADD #TOTAL-1099-GROSS-AMT-E-NO TO #TOTAL-1099-GROSS-AMT-E-NO-GR
        pnd_Total_1099_Ivc_Amt_E_No_Gr.nadd(pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Ivc_Amt_E_No);                                                                      //Natural: ADD #TOTAL-1099-IVC-AMT-E-NO TO #TOTAL-1099-IVC-AMT-E-NO-GR
        pnd_Total_1099_Taxable_Amt_E_No_Gr.nadd(pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Taxable_Amt_E_No);                                                              //Natural: ADD #TOTAL-1099-TAXABLE-AMT-E-NO TO #TOTAL-1099-TAXABLE-AMT-E-NO-GR
        pnd_Total_1099_Int_Amt_E_No_Gr.nadd(pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Int_Amt_E_No);                                                                      //Natural: ADD #TOTAL-1099-INT-AMT-E-NO TO #TOTAL-1099-INT-AMT-E-NO-GR
        pnd_Total_1099_Fed_Tax_Wthld_E_No_Gr.nadd(pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Fed_Tax_Wthld_E_No);                                                          //Natural: ADD #TOTAL-1099-FED-TAX-WTHLD-E-NO TO #TOTAL-1099-FED-TAX-WTHLD-E-NO-GR
        pnd_Total_1099_State_Tax_E_No_Gr.nadd(pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_State_Tax_Wthld_E_No);                                                            //Natural: ADD #TOTAL-1099-STATE-TAX-WTHLD-E-NO TO #TOTAL-1099-STATE-TAX-E-NO-GR
        pnd_Total_1099_Loc_Tax_E_No_Gr.nadd(pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Loc_Tax_Wthld_E_No);                                                                //Natural: ADD #TOTAL-1099-LOC-TAX-WTHLD-E-NO TO #TOTAL-1099-LOC-TAX-E-NO-GR
        pnd_Total_1099_Irr_Amt_E_No_Gr.nadd(pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Irr_Amt_E_No);                                                                      //Natural: ADD #TOTAL-1099-IRR-AMT-E-NO TO #TOTAL-1099-IRR-AMT-E-NO-GR
    }
    //*  01/30/08 - AY
    private void sub_Print_5498() throws Exception                                                                                                                        //Natural: PRINT-5498
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Form_Text.setValue(pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Old_Form_Type.getInt() + 1));                                                     //Natural: ASSIGN #FORM-TEXT := #TWRAFRMN.#FORM-NAME ( #OLD-FORM-TYPE )
        pnd_Form_Text.setValue(DbsUtil.compress(pnd_Form_Text, "P1"));                                                                                                    //Natural: COMPRESS #FORM-TEXT 'P1' INTO #FORM-TEXT
        //*  DASDH
        //*  DASDH
        //*  BK
        getReports().display(3, "Type of/Form",                                                                                                                           //Natural: DISPLAY ( 3 ) 'Type of/Form' #FORM-TEXT ( IS = ON ) 'Form/Count' #5498-FORM-CNT ( EM = Z,ZZZ,ZZ9 ) 'IRA Contribution/Classic Amount' #5498-TRAD-IRA-CONTRIB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'Late RO Amt' #5498-POSTPN-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'IRA Contribution/ROTH Amount' #5498-ROTH-IRA-CONTRIB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'SEP Amount' #5498-SEP-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'Rollover/Contribution' #5498-TRAD-IRA-ROLLOVER ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'Recharactorization/Amount' #5498-TRAD-IRA-RECHAR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '/Conversion Amount' #5498-ROTH-IRA-CONVERSION ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '/Fair-Mkt-Val Amt' #5498-ACCOUNT-FMV ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
        		pnd_Form_Text, new IdenticalSuppress(true),"Form/Count",
        		pnd_5498_Control_Totals_Pnd_5498_Form_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"IRA Contribution/Classic Amount",
        		pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Contrib, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Late RO Amt",
        		pnd_5498_Control_Totals_Pnd_5498_Postpn_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"IRA Contribution/ROTH Amount",
        		pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Contrib, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"SEP Amount",
        		pnd_5498_Control_Totals_Pnd_5498_Sep_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Rollover/Contribution",
        		pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rollover, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Recharactorization/Amount",
        		pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rechar, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Conversion Amount",
        		pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Conversion, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Fair-Mkt-Val Amt",
        		pnd_5498_Control_Totals_Pnd_5498_Account_Fmv, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  DASDH
        getReports().write(3, new TabSetting(1),"Email",new ColumnSpacing(4),pnd_5498_Control_Totals_E_Pnd_5498_Form_Cnt_E, new ReportEditMask ("Z,ZZZ,ZZ9"),new          //Natural: WRITE ( 3 ) 1T 'Email' 4X #5498-FORM-CNT-E ( EM = Z,ZZZ,ZZ9 ) 1X #5498-TRAD-IRA-CONTRIB-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-ROTH-IRA-CONTRIB-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-TRAD-IRA-ROLLOVER-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-TRAD-IRA-RECHAR-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-ROTH-IRA-CONVERSION-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-ACCOUNT-FMV-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 19X #5498-POSTPN-AMT-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-SEP-AMT-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(1),pnd_5498_Control_Totals_E_Pnd_5498_Trad_Ira_Contrib_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_Pnd_5498_Roth_Ira_Contrib_E, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_Pnd_5498_Trad_Ira_Rollover_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_5498_Control_Totals_E_Pnd_5498_Trad_Ira_Rechar_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_Pnd_5498_Roth_Ira_Conversion_E, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_Pnd_5498_Account_Fmv_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            ColumnSpacing(19),pnd_5498_Control_Totals_E_Pnd_5498_Postpn_Amt_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_Pnd_5498_Sep_Amt_E, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  DASDH
        getReports().write(3, new TabSetting(1),"No-Email",new ColumnSpacing(1),pnd_5498_Control_Totals_E_No_Pnd_5498_Form_Cnt_E_No, new ReportEditMask                   //Natural: WRITE ( 3 ) 1T 'No-Email' 1X #5498-FORM-CNT-E-NO ( EM = Z,ZZZ,ZZ9 ) 1X #5498-TRAD-IRA-CONTRIB-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-ROTH-IRA-CONTRIB-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-TRAD-IRA-ROLLOVER-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-TRAD-IRA-RECHAR-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-ROTH-IRA-CONVERSION-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-ACCOUNT-FMV-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 19X #5498-POSTPN-AMT-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-SEP-AMT-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_No_Pnd_5498_Trad_Ira_Contrib_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_5498_Control_Totals_E_No_Pnd_5498_Roth_Ira_Contrib_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_No_Pnd_5498_Trad_Ira_Rollover_E_No, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_No_Pnd_5498_Trad_Ira_Rechar_E_No, new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_No_Pnd_5498_Roth_Ira_Conversion_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_5498_Control_Totals_E_No_Pnd_5498_Account_Fmv_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(19),pnd_5498_Control_Totals_E_No_Pnd_5498_Postpn_Amt_E_No, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_No_Pnd_5498_Sep_Amt_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(3, NEWLINE,NEWLINE,NEWLINE,"Held Cnt...:",pnd_Hold_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Empty Forms:",pnd_Empty_Form_Cnt,            //Natural: WRITE ( 3 ) // /'Held Cnt...:' #HOLD-CNT ( EM = Z,ZZZ,ZZ9 ) /'Empty Forms:' #EMPTY-FORM-CNT ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_5498_Control_Totals_Gr_Pnd_5498_Form_Cnt_Gr.nadd(pnd_5498_Control_Totals_Pnd_5498_Form_Cnt);                                                                  //Natural: ADD #5498-FORM-CNT TO #5498-FORM-CNT-GR
        pnd_5498_Control_Totals_Gr_Pnd_5498_Trad_Ira_Contrib_Gr.nadd(pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Contrib);                                                  //Natural: ADD #5498-TRAD-IRA-CONTRIB TO #5498-TRAD-IRA-CONTRIB-GR
        pnd_5498_Control_Totals_Gr_Pnd_5498_Roth_Ira_Contrib_Gr.nadd(pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Contrib);                                                  //Natural: ADD #5498-ROTH-IRA-CONTRIB TO #5498-ROTH-IRA-CONTRIB-GR
        pnd_5498_Control_Totals_Gr_Pnd_5498_Sep_Amt_Gr.nadd(pnd_5498_Control_Totals_Pnd_5498_Sep_Amt);                                                                    //Natural: ADD #5498-SEP-AMT TO #5498-SEP-AMT-GR
        //*  DASDH
        pnd_5498_Control_Totals_Gr_Pnd_5498_Postpn_Amt_Gr.nadd(pnd_5498_Control_Totals_Pnd_5498_Postpn_Amt);                                                              //Natural: ADD #5498-POSTPN-AMT TO #5498-POSTPN-AMT-GR
        pnd_5498_Control_Totals_Gr_Pnd_5498_Trad_Ira_Rollover_Gr.nadd(pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rollover);                                                //Natural: ADD #5498-TRAD-IRA-ROLLOVER TO #5498-TRAD-IRA-ROLLOVER-GR
        pnd_5498_Control_Totals_Gr_Pnd_5498_Trad_Ira_Rechar_Gr.nadd(pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rechar);                                                    //Natural: ADD #5498-TRAD-IRA-RECHAR TO #5498-TRAD-IRA-RECHAR-GR
        pnd_5498_Control_Totals_Gr_Pnd_5498_Roth_Ira_Conversion_Gr.nadd(pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Conversion);                                            //Natural: ADD #5498-ROTH-IRA-CONVERSION TO #5498-ROTH-IRA-CONVERSION-GR
        pnd_5498_Control_Totals_Gr_Pnd_5498_Account_Fmv_Gr.nadd(pnd_5498_Control_Totals_Pnd_5498_Account_Fmv);                                                            //Natural: ADD #5498-ACCOUNT-FMV TO #5498-ACCOUNT-FMV-GR
        pnd_5498_Control_Totals_E_Gr_Pnd_5498_Form_Cnt_E_Gr.nadd(pnd_5498_Control_Totals_E_Pnd_5498_Form_Cnt_E);                                                          //Natural: ADD #5498-FORM-CNT-E TO #5498-FORM-CNT-E-GR
        pnd_5498_Control_Totals_E_Gr_Pnd_5498_Trad_Ira_Contrib_E_Gr.nadd(pnd_5498_Control_Totals_E_Pnd_5498_Trad_Ira_Contrib_E);                                          //Natural: ADD #5498-TRAD-IRA-CONTRIB-E TO #5498-TRAD-IRA-CONTRIB-E-GR
        pnd_5498_Control_Totals_E_Gr_Pnd_5498_Roth_Ira_Contrib_E_Gr.nadd(pnd_5498_Control_Totals_E_Pnd_5498_Roth_Ira_Contrib_E);                                          //Natural: ADD #5498-ROTH-IRA-CONTRIB-E TO #5498-ROTH-IRA-CONTRIB-E-GR
        pnd_5498_Control_Totals_E_Gr_Pnd_5498_Trad_Ira_Rollover_E_Gr.nadd(pnd_5498_Control_Totals_E_Pnd_5498_Trad_Ira_Rollover_E);                                        //Natural: ADD #5498-TRAD-IRA-ROLLOVER-E TO #5498-TRAD-IRA-ROLLOVER-E-GR
        pnd_5498_Control_Totals_E_Gr_Pnd_5498_Trad_Ira_Rechar_E_Gr.nadd(pnd_5498_Control_Totals_E_Pnd_5498_Trad_Ira_Rechar_E);                                            //Natural: ADD #5498-TRAD-IRA-RECHAR-E TO #5498-TRAD-IRA-RECHAR-E-GR
        pnd_5498_Control_Totals_E_Gr_Pnd_5498_Roth_Ira_Conversion_E_Gr.nadd(pnd_5498_Control_Totals_E_Pnd_5498_Roth_Ira_Conversion_E);                                    //Natural: ADD #5498-ROTH-IRA-CONVERSION-E TO #5498-ROTH-IRA-CONVERSION-E-GR
        pnd_5498_Control_Totals_E_Gr_Pnd_5498_Account_Fmv_E_Gr.nadd(pnd_5498_Control_Totals_E_Pnd_5498_Account_Fmv_E);                                                    //Natural: ADD #5498-ACCOUNT-FMV-E TO #5498-ACCOUNT-FMV-E-GR
        pnd_5498_Control_Totals_E_Gr_Pnd_5498_Sep_Amt_E_Gr.nadd(pnd_5498_Control_Totals_E_Pnd_5498_Sep_Amt_E);                                                            //Natural: ADD #5498-SEP-AMT-E TO #5498-SEP-AMT-E-GR
        pnd_5498_Control_Totals_E_Gr_Pnd_5498_Postpn_Amt_E_Gr.nadd(pnd_5498_Control_Totals_E_Pnd_5498_Postpn_Amt_E);                                                      //Natural: ADD #5498-POSTPN-AMT-E TO #5498-POSTPN-AMT-E-GR
        pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Form_Cnt_E_No_Gr.nadd(pnd_5498_Control_Totals_E_No_Pnd_5498_Form_Cnt_E_No);                                              //Natural: ADD #5498-FORM-CNT-E-NO TO #5498-FORM-CNT-E-NO-GR
        pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Trad_Ira_Contrib_E_No_Gr.nadd(pnd_5498_Control_Totals_E_No_Pnd_5498_Trad_Ira_Contrib_E_No);                              //Natural: ADD #5498-TRAD-IRA-CONTRIB-E-NO TO #5498-TRAD-IRA-CONTRIB-E-NO-GR
        pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Roth_Ira_Contrib_E_No_Gr.nadd(pnd_5498_Control_Totals_E_No_Pnd_5498_Roth_Ira_Contrib_E_No);                              //Natural: ADD #5498-ROTH-IRA-CONTRIB-E-NO TO #5498-ROTH-IRA-CONTRIB-E-NO-GR
        pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Trad_Ira_Rollover_E_No_Gr.nadd(pnd_5498_Control_Totals_E_No_Pnd_5498_Trad_Ira_Rollover_E_No);                            //Natural: ADD #5498-TRAD-IRA-ROLLOVER-E-NO TO #5498-TRAD-IRA-ROLLOVER-E-NO-GR
        pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Trad_Ira_Rechar_E_No_Gr.nadd(pnd_5498_Control_Totals_E_No_Pnd_5498_Trad_Ira_Rechar_E_No);                                //Natural: ADD #5498-TRAD-IRA-RECHAR-E-NO TO #5498-TRAD-IRA-RECHAR-E-NO-GR
        pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Roth_Ira_Conversion_E_No_Gr.nadd(pnd_5498_Control_Totals_E_No_Pnd_5498_Roth_Ira_Conversion_E_No);                        //Natural: ADD #5498-ROTH-IRA-CONVERSION-E-NO TO #5498-ROTH-IRA-CONVERSION-E-NO-GR
        pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Account_Fmv_E_No_Gr.nadd(pnd_5498_Control_Totals_E_No_Pnd_5498_Account_Fmv_E_No);                                        //Natural: ADD #5498-ACCOUNT-FMV-E-NO TO #5498-ACCOUNT-FMV-E-NO-GR
        pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Sep_Amt_E_No_Gr.nadd(pnd_5498_Control_Totals_E_No_Pnd_5498_Sep_Amt_E_No);                                                //Natural: ADD #5498-SEP-AMT-E-NO TO #5498-SEP-AMT-E-NO-GR
        //*  DASDH
        pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Postpn_Amt_E_No_Gr.nadd(pnd_5498_Control_Totals_E_No_Pnd_5498_Postpn_Amt_E_No);                                          //Natural: ADD #5498-POSTPN-AMT-E-NO TO #5498-POSTPN-AMT-E-NO-GR
    }
    private void sub_Print_1042s() throws Exception                                                                                                                       //Natural: PRINT-1042S
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        //*  01/30/08 - AY
        getReports().display(4, "Type of/Form",                                                                                                                           //Natural: DISPLAY ( 4 ) 'Type of/Form' #TWRAFRMN.#FORM-NAME ( #OLD-FORM-TYPE ) 'Form/Count' #FORM-CNT ( EM = Z,ZZZ,ZZ9 ) '/Gross Income' #1042S-GROSS-INCOME ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'Pension/Annuities' #1042S-PENSION-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '/Interest' #1042S-INTEREST-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'NRA Tax/Withheld' #1042S-NRA-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '/Amount Repaid' #1042S-REFUND-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
        		pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Old_Form_Type.getInt() + 1),"Form/Count",
        		pnd_Form_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"/Gross Income",
        		pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Pension/Annuities",
        		pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Interest",
        		pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"NRA Tax/Withheld",
        		pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Amount Repaid",
        		pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  FE141201 START
        //*  FE141201 END
        //*  FE141201 START
        //*  FE141201 END
        getReports().write(4, new TabSetting(1),"FATCA",new ColumnSpacing(4),pnd_Form_Cnt_Fatca.getValue(2), new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income_Fatca.getValue(2),  //Natural: WRITE ( 4 ) 1T 'FATCA' 4X #FORM-CNT-FATCA ( 2 ) ( EM = Z,ZZZ,ZZ9 ) 1X #1042S-GROSS-INCOME-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-PENSION-AMT-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-INTEREST-AMT-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-NRA-TAX-WTHLD-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-REFUND-AMT-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 1T 'NonFATCA' #FORM-CNT-FATCA ( 1 ) ( EM = Z,ZZZ,ZZ9 ) 1X #1042S-GROSS-INCOME-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-PENSION-AMT-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-INTEREST-AMT-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-NRA-TAX-WTHLD-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-REFUND-AMT-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) // 1T 'Email' 4X #FORM-CNT-E ( EM = Z,ZZZ,ZZ9 ) 1X #1042S-GROSS-INCOME-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-PENSION-AMT-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-INTEREST-AMT-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-NRA-TAX-WTHLD-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-REFUND-AMT-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 1T 'FATCA' 4X #FORM-CNT-E-FATCA ( 2 ) ( EM = Z,ZZZ,ZZ9 ) 1X #1042S-GROSS-INCOME-E-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-PENSION-AMT-E-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-INTEREST-AMT-E-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-NRA-TAX-WTHLD-E-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-REFUND-AMT-E-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 1T 'NonFATCA' #FORM-CNT-E-FATCA ( 1 ) ( EM = Z,ZZZ,ZZ9 ) 1X #1042S-GROSS-INCOME-E-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-PENSION-AMT-E-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-INTEREST-AMT-E-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-NRA-TAX-WTHLD-E-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-REFUND-AMT-E-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) /
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt_Fatca.getValue(2), new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt_Fatca.getValue(2), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld_Fatca.getValue(2), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt_Fatca.getValue(2), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(1),"NonFATCA",pnd_Form_Cnt_Fatca.getValue(1), new ReportEditMask ("Z,ZZZ,ZZ9"),new 
            ColumnSpacing(1),pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income_Fatca.getValue(1), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt_Fatca.getValue(1), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt_Fatca.getValue(1), new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld_Fatca.getValue(1), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt_Fatca.getValue(1), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,new 
            TabSetting(1),"Email",new ColumnSpacing(4),pnd_Form_Cnt_E, new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_Pnd_1042s_Gross_Income_E, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_Pnd_1042s_Pension_Amt_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_1042s_Control_Totals_E_Pnd_1042s_Interest_Amt_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_Pnd_1042s_Nra_Tax_Wthld_E, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_Pnd_1042s_Refund_Amt_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(1),"FATCA",new ColumnSpacing(4),pnd_Form_Cnt_E_Fatca.getValue(2), new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_Pnd_1042s_Gross_Income_E_Fatca.getValue(2), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_Pnd_1042s_Pension_Amt_E_Fatca.getValue(2), new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_Pnd_1042s_Interest_Amt_E_Fatca.getValue(2), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_1042s_Control_Totals_E_Pnd_1042s_Nra_Tax_Wthld_E_Fatca.getValue(2), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_Pnd_1042s_Refund_Amt_E_Fatca.getValue(2), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(1),"NonFATCA",pnd_Form_Cnt_E_Fatca.getValue(1), new ReportEditMask ("Z,ZZZ,ZZ9"),new 
            ColumnSpacing(1),pnd_1042s_Control_Totals_E_Pnd_1042s_Gross_Income_E_Fatca.getValue(1), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_Pnd_1042s_Pension_Amt_E_Fatca.getValue(1), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_Pnd_1042s_Interest_Amt_E_Fatca.getValue(1), new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_Pnd_1042s_Nra_Tax_Wthld_E_Fatca.getValue(1), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_1042s_Control_Totals_E_Pnd_1042s_Refund_Amt_E_Fatca.getValue(1), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE);
        if (Global.isEscape()) return;
        //*  FE141201 START
        //*  FE141201 END
        getReports().write(4, new TabSetting(1),"No-Email",new ColumnSpacing(1),pnd_Form_Cnt_E_No, new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_No_Pnd_1042s_Gross_Income_E_No,  //Natural: WRITE ( 4 ) 1T 'No-Email' 1X #FORM-CNT-E-NO ( EM = Z,ZZZ,ZZ9 ) 1X #1042S-GROSS-INCOME-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-PENSION-AMT-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-INTEREST-AMT-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-NRA-TAX-WTHLD-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-REFUND-AMT-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 1T 'FATCA' 4X #FORM-CNT-E-NO-FATCA ( 2 ) ( EM = Z,ZZZ,ZZ9 ) 1X #1042S-GROSS-INCOME-E-NO-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-PENSION-AMT-E-NO-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-INTEREST-AMT-E-NO-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-NRA-TAX-WTHLD-E-NO-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-REFUND-AMT-E-NO-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 1T 'NonFATCA' #FORM-CNT-E-NO-FATCA ( 1 ) ( EM = Z,ZZZ,ZZ9 ) 1X #1042S-GROSS-INCOME-E-NO-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-PENSION-AMT-E-NO-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-INTEREST-AMT-E-NO-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-NRA-TAX-WTHLD-E-NO-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-REFUND-AMT-E-NO-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) /
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_No_Pnd_1042s_Pension_Amt_E_No, new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_No_Pnd_1042s_Interest_Amt_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_1042s_Control_Totals_E_No_Pnd_1042s_Nra_Tax_Wthld_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_No_Pnd_1042s_Refund_Amt_E_No, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(1),"FATCA",new ColumnSpacing(4),pnd_Form_Cnt_E_No_Fatca.getValue(2), new ReportEditMask 
            ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_No_Pnd_1042s_Gross_Income_E_No_Fatca.getValue(2), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_1042s_Control_Totals_E_No_Pnd_1042s_Pension_Amt_E_No_Fatca.getValue(2), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_No_Pnd_1042s_Interest_Amt_E_No_Fatca.getValue(2), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_No_Pnd_1042s_Nra_Tax_Wthld_E_No_Fatca.getValue(2), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_No_Pnd_1042s_Refund_Amt_E_No_Fatca.getValue(2), new 
            ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(1),"NonFATCA",pnd_Form_Cnt_E_No_Fatca.getValue(1), new ReportEditMask ("Z,ZZZ,ZZ9"),new 
            ColumnSpacing(1),pnd_1042s_Control_Totals_E_No_Pnd_1042s_Gross_Income_E_No_Fatca.getValue(1), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_1042s_Control_Totals_E_No_Pnd_1042s_Pension_Amt_E_No_Fatca.getValue(1), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_No_Pnd_1042s_Interest_Amt_E_No_Fatca.getValue(1), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_No_Pnd_1042s_Nra_Tax_Wthld_E_No_Fatca.getValue(1), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Control_Totals_E_No_Pnd_1042s_Refund_Amt_E_No_Fatca.getValue(1), new 
            ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(4, NEWLINE,NEWLINE,NEWLINE,"Held Cnt...:",pnd_Hold_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Empty Forms:",pnd_Empty_Form_Cnt,            //Natural: WRITE ( 4 ) // /'Held Cnt...:' #HOLD-CNT ( EM = Z,ZZZ,ZZ9 ) /'Empty Forms:' #EMPTY-FORM-CNT ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pnd_Form_Cnt_1042s_Gr.nadd(pnd_Form_Cnt);                                                                                                                         //Natural: ADD #FORM-CNT TO #FORM-CNT-1042S-GR
        pnd_1042s_Gross_Income_1042s_Gr.nadd(pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income);                                                                            //Natural: ADD #1042S-GROSS-INCOME TO #1042S-GROSS-INCOME-1042S-GR
        pnd_1042s_Pension_Amt_1042s_Gr.nadd(pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt);                                                                              //Natural: ADD #1042S-PENSION-AMT TO #1042S-PENSION-AMT-1042S-GR
        pnd_1042s_Interest_Amt_1042s_Gr.nadd(pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt);                                                                            //Natural: ADD #1042S-INTEREST-AMT TO #1042S-INTEREST-AMT-1042S-GR
        pnd_1042s_Nra_Tax_Wthld_1042s_Gr.nadd(pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld);                                                                          //Natural: ADD #1042S-NRA-TAX-WTHLD TO #1042S-NRA-TAX-WTHLD-1042S-GR
        pnd_1042s_Refund_Amt_1042s_Gr.nadd(pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt);                                                                                //Natural: ADD #1042S-REFUND-AMT TO #1042S-REFUND-AMT-1042S-GR
        pnd_Form_Cnt_E_1042s_Gr.nadd(pnd_Form_Cnt_E);                                                                                                                     //Natural: ADD #FORM-CNT-E TO #FORM-CNT-E-1042S-GR
        pnd_1042s_Gross_Income_E_1042s_Gr.nadd(pnd_1042s_Control_Totals_E_Pnd_1042s_Gross_Income_E);                                                                      //Natural: ADD #1042S-GROSS-INCOME-E TO #1042S-GROSS-INCOME-E-1042S-GR
        pnd_1042s_Pension_Amt_E_1042s_Gr.nadd(pnd_1042s_Control_Totals_E_Pnd_1042s_Pension_Amt_E);                                                                        //Natural: ADD #1042S-PENSION-AMT-E TO #1042S-PENSION-AMT-E-1042S-GR
        pnd_1042s_Interest_Amt_E_1042s_Gr.nadd(pnd_1042s_Control_Totals_E_Pnd_1042s_Interest_Amt_E);                                                                      //Natural: ADD #1042S-INTEREST-AMT-E TO #1042S-INTEREST-AMT-E-1042S-GR
        pnd_1042s_Nra_Tax_Wthld_E_1042s_Gr.nadd(pnd_1042s_Control_Totals_E_Pnd_1042s_Nra_Tax_Wthld_E);                                                                    //Natural: ADD #1042S-NRA-TAX-WTHLD-E TO #1042S-NRA-TAX-WTHLD-E-1042S-GR
        pnd_1042s_Refund_Amt_E_1042s_Gr.nadd(pnd_1042s_Control_Totals_E_Pnd_1042s_Refund_Amt_E);                                                                          //Natural: ADD #1042S-REFUND-AMT-E TO #1042S-REFUND-AMT-E-1042S-GR
        pnd_Form_Cnt_E_No_1042s_Gr.nadd(pnd_Form_Cnt_E_No);                                                                                                               //Natural: ADD #FORM-CNT-E-NO TO #FORM-CNT-E-NO-1042S-GR
        pnd_1042s_Gross_Income_E_No_1042s_Gr.nadd(pnd_1042s_Control_Totals_E_No_Pnd_1042s_Gross_Income_E_No);                                                             //Natural: ADD #1042S-GROSS-INCOME-E-NO TO #1042S-GROSS-INCOME-E-NO-1042S-GR
        pnd_1042s_Pension_Amt_E_No_1042s_Gr.nadd(pnd_1042s_Control_Totals_E_No_Pnd_1042s_Pension_Amt_E_No);                                                               //Natural: ADD #1042S-PENSION-AMT-E-NO TO #1042S-PENSION-AMT-E-NO-1042S-GR
        pnd_1042s_Interest_Amt_E_No_1042s_Gr.nadd(pnd_1042s_Control_Totals_E_No_Pnd_1042s_Interest_Amt_E_No);                                                             //Natural: ADD #1042S-INTEREST-AMT-E-NO TO #1042S-INTEREST-AMT-E-NO-1042S-GR
        pnd_1042s_Nra_Tax_Wthld_E_No_1042s_Gr.nadd(pnd_1042s_Control_Totals_E_No_Pnd_1042s_Nra_Tax_Wthld_E_No);                                                           //Natural: ADD #1042S-NRA-TAX-WTHLD-E-NO TO #1042S-NRA-TAX-WTHLD-E-NO-1042S-GR
        pnd_1042s_Refund_Amt_E_No_1042s_Gr.nadd(pnd_1042s_Control_Totals_E_No_Pnd_1042s_Refund_Amt_E_No);                                                                 //Natural: ADD #1042S-REFUND-AMT-E-NO TO #1042S-REFUND-AMT-E-NO-1042S-GR
        //*  FE141201 START
        pnd_Form_Cnt_1042s_Gr_Fatca.getValue(1).nadd(pnd_Form_Cnt_Fatca.getValue(1));                                                                                     //Natural: ADD #FORM-CNT-FATCA ( 1 ) TO #FORM-CNT-1042S-GR-FATCA ( 1 )
        pnd_1042s_Gross_Income_1042s_Gr_Fatca.getValue(1).nadd(pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income_Fatca.getValue(1));                                        //Natural: ADD #1042S-GROSS-INCOME-FATCA ( 1 ) TO #1042S-GROSS-INCOME-1042S-GR-FATCA ( 1 )
        pnd_1042s_Pension_Amt_1042s_Gr_Fatca.getValue(1).nadd(pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt_Fatca.getValue(1));                                          //Natural: ADD #1042S-PENSION-AMT-FATCA ( 1 ) TO #1042S-PENSION-AMT-1042S-GR-FATCA ( 1 )
        pnd_1042s_Interest_Amt_1042s_Gr_Fatca.getValue(1).nadd(pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt_Fatca.getValue(1));                                        //Natural: ADD #1042S-INTEREST-AMT-FATCA ( 1 ) TO #1042S-INTEREST-AMT-1042S-GR-FATCA ( 1 )
        pnd_1042s_Nra_Tax_Wthld_1042s_Gr_Fatca.getValue(1).nadd(pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld_Fatca.getValue(1));                                      //Natural: ADD #1042S-NRA-TAX-WTHLD-FATCA ( 1 ) TO #1042S-NRA-TAX-WTHLD-1042S-GR-FATCA ( 1 )
        pnd_1042s_Refund_Amt_1042s_Gr_Fatca.getValue(1).nadd(pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt_Fatca.getValue(1));                                            //Natural: ADD #1042S-REFUND-AMT-FATCA ( 1 ) TO #1042S-REFUND-AMT-1042S-GR-FATCA ( 1 )
        pnd_Form_Cnt_E_1042s_Gr_Fatca.getValue(1).nadd(pnd_Form_Cnt_E_Fatca.getValue(1));                                                                                 //Natural: ADD #FORM-CNT-E-FATCA ( 1 ) TO #FORM-CNT-E-1042S-GR-FATCA ( 1 )
        pnd_1042s_Gross_Income_E_1042s_Gr_Fatca.getValue(1).nadd(pnd_1042s_Control_Totals_E_Pnd_1042s_Gross_Income_E_Fatca.getValue(1));                                  //Natural: ADD #1042S-GROSS-INCOME-E-FATCA ( 1 ) TO #1042S-GROSS-INCOME-E-1042S-GR-FATCA ( 1 )
        pnd_1042s_Pension_Amt_E_1042s_Gr_Fatca.getValue(1).nadd(pnd_1042s_Control_Totals_E_Pnd_1042s_Pension_Amt_E_Fatca.getValue(1));                                    //Natural: ADD #1042S-PENSION-AMT-E-FATCA ( 1 ) TO #1042S-PENSION-AMT-E-1042S-GR-FATCA ( 1 )
        pnd_1042s_Interest_Amt_E_1042s_Gr_Fatca.getValue(1).nadd(pnd_1042s_Control_Totals_E_Pnd_1042s_Interest_Amt_E_Fatca.getValue(1));                                  //Natural: ADD #1042S-INTEREST-AMT-E-FATCA ( 1 ) TO #1042S-INTEREST-AMT-E-1042S-GR-FATCA ( 1 )
        pnd_1042s_Nra_Tax_Wthld_E_1042s_Gr_Fatca.getValue(1).nadd(pnd_1042s_Control_Totals_E_Pnd_1042s_Nra_Tax_Wthld_E_Fatca.getValue(1));                                //Natural: ADD #1042S-NRA-TAX-WTHLD-E-FATCA ( 1 ) TO #1042S-NRA-TAX-WTHLD-E-1042S-GR-FATCA ( 1 )
        pnd_1042s_Refund_Amt_E_1042s_Gr_Fatca.getValue(1).nadd(pnd_1042s_Control_Totals_E_Pnd_1042s_Refund_Amt_E_Fatca.getValue(1));                                      //Natural: ADD #1042S-REFUND-AMT-E-FATCA ( 1 ) TO #1042S-REFUND-AMT-E-1042S-GR-FATCA ( 1 )
        pnd_Form_Cnt_E_No_1042s_Gr_Fatca.getValue(1).nadd(pnd_Form_Cnt_E_No_Fatca.getValue(1));                                                                           //Natural: ADD #FORM-CNT-E-NO-FATCA ( 1 ) TO #FORM-CNT-E-NO-1042S-GR-FATCA ( 1 )
        pnd_1042s_Gross_Income_E_No_Gr_Fatca.getValue(1).nadd(pnd_1042s_Control_Totals_E_No_Pnd_1042s_Gross_Income_E_No_Fatca.getValue(1));                               //Natural: ADD #1042S-GROSS-INCOME-E-NO-FATCA ( 1 ) TO #1042S-GROSS-INCOME-E-NO-GR-FATCA ( 1 )
        pnd_1042s_Pension_Amt_E_No_Gr_Fatca.getValue(1).nadd(pnd_1042s_Control_Totals_E_No_Pnd_1042s_Pension_Amt_E_No_Fatca.getValue(1));                                 //Natural: ADD #1042S-PENSION-AMT-E-NO-FATCA ( 1 ) TO #1042S-PENSION-AMT-E-NO-GR-FATCA ( 1 )
        pnd_1042s_Interest_Amt_E_No_Gr_Fatca.getValue(1).nadd(pnd_1042s_Control_Totals_E_No_Pnd_1042s_Interest_Amt_E_No_Fatca.getValue(1));                               //Natural: ADD #1042S-INTEREST-AMT-E-NO-FATCA ( 1 ) TO #1042S-INTEREST-AMT-E-NO-GR-FATCA ( 1 )
        pnd_1042s_Nra_Tax_Wthld_E_No_Gr_Fatca.getValue(1).nadd(pnd_1042s_Control_Totals_E_No_Pnd_1042s_Nra_Tax_Wthld_E_No_Fatca.getValue(1));                             //Natural: ADD #1042S-NRA-TAX-WTHLD-E-NO-FATCA ( 1 ) TO #1042S-NRA-TAX-WTHLD-E-NO-GR-FATCA ( 1 )
        pnd_1042s_Refund_Amt_E_No_Gr_Fatca.getValue(1).nadd(pnd_1042s_Control_Totals_E_No_Pnd_1042s_Refund_Amt_E_No_Fatca.getValue(1));                                   //Natural: ADD #1042S-REFUND-AMT-E-NO-FATCA ( 1 ) TO #1042S-REFUND-AMT-E-NO-GR-FATCA ( 1 )
        //*  FE141201 START
        pnd_Form_Cnt_1042s_Gr_Fatca.getValue(2).nadd(pnd_Form_Cnt_Fatca.getValue(2));                                                                                     //Natural: ADD #FORM-CNT-FATCA ( 2 ) TO #FORM-CNT-1042S-GR-FATCA ( 2 )
        pnd_1042s_Gross_Income_1042s_Gr_Fatca.getValue(2).nadd(pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income_Fatca.getValue(2));                                        //Natural: ADD #1042S-GROSS-INCOME-FATCA ( 2 ) TO #1042S-GROSS-INCOME-1042S-GR-FATCA ( 2 )
        pnd_1042s_Pension_Amt_1042s_Gr_Fatca.getValue(2).nadd(pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt_Fatca.getValue(2));                                          //Natural: ADD #1042S-PENSION-AMT-FATCA ( 2 ) TO #1042S-PENSION-AMT-1042S-GR-FATCA ( 2 )
        pnd_1042s_Interest_Amt_1042s_Gr_Fatca.getValue(2).nadd(pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt_Fatca.getValue(2));                                        //Natural: ADD #1042S-INTEREST-AMT-FATCA ( 2 ) TO #1042S-INTEREST-AMT-1042S-GR-FATCA ( 2 )
        pnd_1042s_Nra_Tax_Wthld_1042s_Gr_Fatca.getValue(2).nadd(pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld_Fatca.getValue(2));                                      //Natural: ADD #1042S-NRA-TAX-WTHLD-FATCA ( 2 ) TO #1042S-NRA-TAX-WTHLD-1042S-GR-FATCA ( 2 )
        pnd_1042s_Refund_Amt_1042s_Gr_Fatca.getValue(2).nadd(pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt_Fatca.getValue(2));                                            //Natural: ADD #1042S-REFUND-AMT-FATCA ( 2 ) TO #1042S-REFUND-AMT-1042S-GR-FATCA ( 2 )
        pnd_Form_Cnt_E_1042s_Gr_Fatca.getValue(2).nadd(pnd_Form_Cnt_E_Fatca.getValue(2));                                                                                 //Natural: ADD #FORM-CNT-E-FATCA ( 2 ) TO #FORM-CNT-E-1042S-GR-FATCA ( 2 )
        pnd_1042s_Gross_Income_E_1042s_Gr_Fatca.getValue(2).nadd(pnd_1042s_Control_Totals_E_Pnd_1042s_Gross_Income_E_Fatca.getValue(2));                                  //Natural: ADD #1042S-GROSS-INCOME-E-FATCA ( 2 ) TO #1042S-GROSS-INCOME-E-1042S-GR-FATCA ( 2 )
        pnd_1042s_Pension_Amt_E_1042s_Gr_Fatca.getValue(2).nadd(pnd_1042s_Control_Totals_E_Pnd_1042s_Pension_Amt_E_Fatca.getValue(2));                                    //Natural: ADD #1042S-PENSION-AMT-E-FATCA ( 2 ) TO #1042S-PENSION-AMT-E-1042S-GR-FATCA ( 2 )
        pnd_1042s_Interest_Amt_E_1042s_Gr_Fatca.getValue(2).nadd(pnd_1042s_Control_Totals_E_Pnd_1042s_Interest_Amt_E_Fatca.getValue(2));                                  //Natural: ADD #1042S-INTEREST-AMT-E-FATCA ( 2 ) TO #1042S-INTEREST-AMT-E-1042S-GR-FATCA ( 2 )
        pnd_1042s_Nra_Tax_Wthld_E_1042s_Gr_Fatca.getValue(2).nadd(pnd_1042s_Control_Totals_E_Pnd_1042s_Nra_Tax_Wthld_E_Fatca.getValue(2));                                //Natural: ADD #1042S-NRA-TAX-WTHLD-E-FATCA ( 2 ) TO #1042S-NRA-TAX-WTHLD-E-1042S-GR-FATCA ( 2 )
        pnd_1042s_Refund_Amt_E_1042s_Gr_Fatca.getValue(2).nadd(pnd_1042s_Control_Totals_E_Pnd_1042s_Refund_Amt_E_Fatca.getValue(2));                                      //Natural: ADD #1042S-REFUND-AMT-E-FATCA ( 2 ) TO #1042S-REFUND-AMT-E-1042S-GR-FATCA ( 2 )
        pnd_Form_Cnt_E_No_1042s_Gr_Fatca.getValue(2).nadd(pnd_Form_Cnt_E_No_Fatca.getValue(2));                                                                           //Natural: ADD #FORM-CNT-E-NO-FATCA ( 2 ) TO #FORM-CNT-E-NO-1042S-GR-FATCA ( 2 )
        pnd_1042s_Gross_Income_E_No_Gr_Fatca.getValue(2).nadd(pnd_1042s_Control_Totals_E_No_Pnd_1042s_Gross_Income_E_No_Fatca.getValue(2));                               //Natural: ADD #1042S-GROSS-INCOME-E-NO-FATCA ( 2 ) TO #1042S-GROSS-INCOME-E-NO-GR-FATCA ( 2 )
        pnd_1042s_Pension_Amt_E_No_Gr_Fatca.getValue(2).nadd(pnd_1042s_Control_Totals_E_No_Pnd_1042s_Pension_Amt_E_No_Fatca.getValue(2));                                 //Natural: ADD #1042S-PENSION-AMT-E-NO-FATCA ( 2 ) TO #1042S-PENSION-AMT-E-NO-GR-FATCA ( 2 )
        pnd_1042s_Interest_Amt_E_No_Gr_Fatca.getValue(2).nadd(pnd_1042s_Control_Totals_E_No_Pnd_1042s_Interest_Amt_E_No_Fatca.getValue(2));                               //Natural: ADD #1042S-INTEREST-AMT-E-NO-FATCA ( 2 ) TO #1042S-INTEREST-AMT-E-NO-GR-FATCA ( 2 )
        pnd_1042s_Nra_Tax_Wthld_E_No_Gr_Fatca.getValue(2).nadd(pnd_1042s_Control_Totals_E_No_Pnd_1042s_Nra_Tax_Wthld_E_No_Fatca.getValue(2));                             //Natural: ADD #1042S-NRA-TAX-WTHLD-E-NO-FATCA ( 2 ) TO #1042S-NRA-TAX-WTHLD-E-NO-GR-FATCA ( 2 )
        pnd_1042s_Refund_Amt_E_No_Gr_Fatca.getValue(2).nadd(pnd_1042s_Control_Totals_E_No_Pnd_1042s_Refund_Amt_E_No_Fatca.getValue(2));                                   //Natural: ADD #1042S-REFUND-AMT-E-NO-FATCA ( 2 ) TO #1042S-REFUND-AMT-E-NO-GR-FATCA ( 2 )
    }
    //*  PR
    private void sub_Print_480_7() throws Exception                                                                                                                       //Natural: PRINT-480-7
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        getReports().display(5, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Type of/Form",                                                       //Natural: DISPLAY ( 5 ) ( HC = R ) 'Type of/Form' #FORM-TEXT ( HC = L ) 'Form/Count' #4807C-FORM-CNT ( #K ) 'Gross/Proceeds' #4807C-GROSS-AMT ( #K ) 'Interest/Amount' #4807C-INT-AMT ( #K ) 'Distribution Amount' #4807C-DISTRIB-AMT ( #K ) / 'Rollover Amount' #4807C-GROSS-AMT-ROLL ( #K ) 'Taxable/Amount' #4807C-TAXABLE-AMT ( #K ) 'IVC Amount' #4807C-IVC-AMT ( #K ) / 'Rollover IVC Amount' #4807C-IVC-AMT-ROLL ( #K )
        		pnd_Form_Text, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"Form/Count",
        		pnd_4807c_Control_Totals_Pnd_4807c_Form_Cnt.getValue(pnd_K),"Gross/Proceeds",
        		pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt.getValue(pnd_K),"Interest/Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Int_Amt.getValue(pnd_K),"Distribution Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Distrib_Amt.getValue(pnd_K),NEWLINE,"Rollover Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt_Roll.getValue(pnd_K),"Taxable/Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Taxable_Amt.getValue(pnd_K),"IVC Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt.getValue(pnd_K),NEWLINE,"Rollover IVC Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt_Roll.getValue(pnd_K));
        if (Global.isEscape()) return;
        getReports().skip(5, 1);                                                                                                                                          //Natural: SKIP ( 5 ) 1
        pnd_4807c_Control_Totals_Gr_Pnd_4807c_Form_Cnt_Gr.nadd(pnd_4807c_Control_Totals_Pnd_4807c_Form_Cnt.getValue(pnd_K));                                              //Natural: ADD #4807C-FORM-CNT ( #K ) TO #4807C-FORM-CNT-GR
        pnd_4807c_Control_Totals_Gr_Pnd_4807c_Gross_Amt_Gr.nadd(pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt.getValue(pnd_K));                                            //Natural: ADD #4807C-GROSS-AMT ( #K ) TO #4807C-GROSS-AMT-GR
        pnd_4807c_Control_Totals_Gr_Pnd_4807c_Int_Amt_Gr.nadd(pnd_4807c_Control_Totals_Pnd_4807c_Int_Amt.getValue(pnd_K));                                                //Natural: ADD #4807C-INT-AMT ( #K ) TO #4807C-INT-AMT-GR
        pnd_4807c_Control_Totals_Gr_Pnd_4807c_Distrib_Amt_Gr.nadd(pnd_4807c_Control_Totals_Pnd_4807c_Distrib_Amt.getValue(pnd_K));                                        //Natural: ADD #4807C-DISTRIB-AMT ( #K ) TO #4807C-DISTRIB-AMT-GR
        pnd_4807c_Control_Totals_Gr_Pnd_4807c_Gross_Amt_Roll_Gr.nadd(pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt_Roll.getValue(pnd_K));                                  //Natural: ADD #4807C-GROSS-AMT-ROLL ( #K ) TO #4807C-GROSS-AMT-ROLL-GR
        pnd_4807c_Control_Totals_Gr_Pnd_4807c_Taxable_Amt_Gr.nadd(pnd_4807c_Control_Totals_Pnd_4807c_Taxable_Amt.getValue(pnd_K));                                        //Natural: ADD #4807C-TAXABLE-AMT ( #K ) TO #4807C-TAXABLE-AMT-GR
        pnd_4807c_Control_Totals_Gr_Pnd_4807c_Ivc_Amt_Gr.nadd(pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt.getValue(pnd_K));                                                //Natural: ADD #4807C-IVC-AMT ( #K ) TO #4807C-IVC-AMT-GR
        pnd_4807c_Control_Totals_Gr_Pnd_4807c_Ivc_Amt_Roll_Gr.nadd(pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt_Roll.getValue(pnd_K));                                      //Natural: ADD #4807C-IVC-AMT-ROLL ( #K ) TO #4807C-IVC-AMT-ROLL-GR
        pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Form_Cnt_E_Gr.nadd(pnd_4807c_Control_Totals_E_Pnd_4807c_Form_Cnt_E.getValue(pnd_K));                                      //Natural: ADD #4807C-FORM-CNT-E ( #K ) TO #4807C-FORM-CNT-E-GR
        pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Gross_Amt_E_Gr.nadd(pnd_4807c_Control_Totals_E_Pnd_4807c_Gross_Amt_E.getValue(pnd_K));                                    //Natural: ADD #4807C-GROSS-AMT-E ( #K ) TO #4807C-GROSS-AMT-E-GR
        pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Int_Amt_E_Gr.nadd(pnd_4807c_Control_Totals_E_Pnd_4807c_Int_Amt_E.getValue(pnd_K));                                        //Natural: ADD #4807C-INT-AMT-E ( #K ) TO #4807C-INT-AMT-E-GR
        pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Distrib_Amt_E_Gr.nadd(pnd_4807c_Control_Totals_E_Pnd_4807c_Distrib_Amt_E.getValue(pnd_K));                                //Natural: ADD #4807C-DISTRIB-AMT-E ( #K ) TO #4807C-DISTRIB-AMT-E-GR
        pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Taxable_Amt_E_Gr.nadd(pnd_4807c_Control_Totals_E_Pnd_4807c_Taxable_Amt_E.getValue(pnd_K));                                //Natural: ADD #4807C-TAXABLE-AMT-E ( #K ) TO #4807C-TAXABLE-AMT-E-GR
        pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Ivc_Amt_E_Gr.nadd(pnd_4807c_Control_Totals_E_Pnd_4807c_Ivc_Amt_E.getValue(pnd_K));                                        //Natural: ADD #4807C-IVC-AMT-E ( #K ) TO #4807C-IVC-AMT-E-GR
        pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Gross_Amt_Roll_E_Gr.nadd(pnd_4807c_Control_Totals_E_Pnd_4807c_Gross_Amt_Roll_E.getValue(pnd_K));                          //Natural: ADD #4807C-GROSS-AMT-ROLL-E ( #K ) TO #4807C-GROSS-AMT-ROLL-E-GR
        pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Ivc_Amt_Roll_E_Gr.nadd(pnd_4807c_Control_Totals_E_Pnd_4807c_Ivc_Amt_Roll_E.getValue(pnd_K));                              //Natural: ADD #4807C-IVC-AMT-ROLL-E ( #K ) TO #4807C-IVC-AMT-ROLL-E-GR
        pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Form_Cnt_E_No_Gr.nadd(pnd_4807c_Control_Totals_E_No_Pnd_4807c_Form_Cnt_E_No.getValue(pnd_K));                          //Natural: ADD #4807C-FORM-CNT-E-NO ( #K ) TO #4807C-FORM-CNT-E-NO-GR
        pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Gross_Amt_E_No_Gr.nadd(pnd_4807c_Control_Totals_E_No_Pnd_4807c_Gross_Amt_E_No.getValue(pnd_K));                        //Natural: ADD #4807C-GROSS-AMT-E-NO ( #K ) TO #4807C-GROSS-AMT-E-NO-GR
        pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Int_Amt_E_No_Gr.nadd(pnd_4807c_Control_Totals_E_No_Pnd_4807c_Int_Amt_E_No.getValue(pnd_K));                            //Natural: ADD #4807C-INT-AMT-E-NO ( #K ) TO #4807C-INT-AMT-E-NO-GR
        pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Distrib_Amt_E_No_Gr.nadd(pnd_4807c_Control_Totals_E_No_Pnd_4807c_Distrib_Amt_E_No.getValue(pnd_K));                    //Natural: ADD #4807C-DISTRIB-AMT-E-NO ( #K ) TO #4807C-DISTRIB-AMT-E-NO-GR
        pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Taxable_Amt_E_No_Gr.nadd(pnd_4807c_Control_Totals_E_No_Pnd_4807c_Taxable_Amt_E_No.getValue(pnd_K));                    //Natural: ADD #4807C-TAXABLE-AMT-E-NO ( #K ) TO #4807C-TAXABLE-AMT-E-NO-GR
        pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Ivc_Amt_E_No_Gr.nadd(pnd_4807c_Control_Totals_E_No_Pnd_4807c_Ivc_Amt_E_No.getValue(pnd_K));                            //Natural: ADD #4807C-IVC-AMT-E-NO ( #K ) TO #4807C-IVC-AMT-E-NO-GR
        pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Gross_Amt_Roll_E_No_Gr.nadd(pnd_4807c_Control_Totals_E_No_Pnd_4807c_Gross_Amt_Roll_E_No.getValue(pnd_K));              //Natural: ADD #4807C-GROSS-AMT-ROLL-E-NO ( #K ) TO #4807C-GROSS-AMT-ROLL-E-NO-GR
        pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Ivc_Amt_Roll_E_No_Gr.nadd(pnd_4807c_Control_Totals_E_No_Pnd_4807c_Ivc_Amt_Roll_E_No.getValue(pnd_K));                  //Natural: ADD #4807C-IVC-AMT-ROLL-E-NO ( #K ) TO #4807C-IVC-AMT-ROLL-E-NO-GR
    }
    private void sub_Increment_4807_Total() throws Exception                                                                                                              //Natural: INCREMENT-4807-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        pnd_4807c_Control_Totals_Pnd_4807c_Form_Cnt.getValue(2).nadd(pnd_4807c_Control_Totals_Pnd_4807c_Form_Cnt.getValue(1));                                            //Natural: ADD #4807C-FORM-CNT ( 1 ) TO #4807C-FORM-CNT ( 2 )
        pnd_4807c_Control_Totals_E_Pnd_4807c_Form_Cnt_E.getValue(2).nadd(pnd_4807c_Control_Totals_E_Pnd_4807c_Form_Cnt_E.getValue(1));                                    //Natural: ADD #4807C-FORM-CNT-E ( 1 ) TO #4807C-FORM-CNT-E ( 2 )
        pnd_4807c_Control_Totals_E_No_Pnd_4807c_Form_Cnt_E_No.getValue(2).nadd(pnd_4807c_Control_Totals_E_No_Pnd_4807c_Form_Cnt_E_No.getValue(1));                        //Natural: ADD #4807C-FORM-CNT-E-NO ( 1 ) TO #4807C-FORM-CNT-E-NO ( 2 )
        pnd_4807c_Control_Totals_Pnd_4807c_Empty_Form_Cnt.getValue(2).nadd(pnd_4807c_Control_Totals_Pnd_4807c_Empty_Form_Cnt.getValue(1));                                //Natural: ADD #4807C-EMPTY-FORM-CNT ( 1 ) TO #4807C-EMPTY-FORM-CNT ( 2 )
        pnd_4807c_Control_Totals_Pnd_4807c_Hold_Cnt.getValue(2).nadd(pnd_4807c_Control_Totals_Pnd_4807c_Hold_Cnt.getValue(1));                                            //Natural: ADD #4807C-HOLD-CNT ( 1 ) TO #4807C-HOLD-CNT ( 2 )
        pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt.getValue(2).nadd(pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt.getValue(1));                                          //Natural: ADD #4807C-GROSS-AMT ( 1 ) TO #4807C-GROSS-AMT ( 2 )
        pnd_4807c_Control_Totals_E_Pnd_4807c_Gross_Amt_E.getValue(2).nadd(pnd_4807c_Control_Totals_E_Pnd_4807c_Gross_Amt_E.getValue(1));                                  //Natural: ADD #4807C-GROSS-AMT-E ( 1 ) TO #4807C-GROSS-AMT-E ( 2 )
        pnd_4807c_Control_Totals_E_No_Pnd_4807c_Gross_Amt_E_No.getValue(2).nadd(pnd_4807c_Control_Totals_E_No_Pnd_4807c_Gross_Amt_E_No.getValue(1));                      //Natural: ADD #4807C-GROSS-AMT-E-NO ( 1 ) TO #4807C-GROSS-AMT-E-NO ( 2 )
        pnd_4807c_Control_Totals_Pnd_4807c_Int_Amt.getValue(2).nadd(pnd_4807c_Control_Totals_Pnd_4807c_Int_Amt.getValue(1));                                              //Natural: ADD #4807C-INT-AMT ( 1 ) TO #4807C-INT-AMT ( 2 )
        pnd_4807c_Control_Totals_E_Pnd_4807c_Int_Amt_E.getValue(2).nadd(pnd_4807c_Control_Totals_E_Pnd_4807c_Int_Amt_E.getValue(1));                                      //Natural: ADD #4807C-INT-AMT-E ( 1 ) TO #4807C-INT-AMT-E ( 2 )
        pnd_4807c_Control_Totals_E_No_Pnd_4807c_Int_Amt_E_No.getValue(2).nadd(pnd_4807c_Control_Totals_E_No_Pnd_4807c_Int_Amt_E_No.getValue(1));                          //Natural: ADD #4807C-INT-AMT-E-NO ( 1 ) TO #4807C-INT-AMT-E-NO ( 2 )
        pnd_4807c_Control_Totals_Pnd_4807c_Distrib_Amt.getValue(2).nadd(pnd_4807c_Control_Totals_Pnd_4807c_Distrib_Amt.getValue(1));                                      //Natural: ADD #4807C-DISTRIB-AMT ( 1 ) TO #4807C-DISTRIB-AMT ( 2 )
        pnd_4807c_Control_Totals_E_Pnd_4807c_Distrib_Amt_E.getValue(2).nadd(pnd_4807c_Control_Totals_E_Pnd_4807c_Distrib_Amt_E.getValue(1));                              //Natural: ADD #4807C-DISTRIB-AMT-E ( 1 ) TO #4807C-DISTRIB-AMT-E ( 2 )
        pnd_4807c_Control_Totals_E_No_Pnd_4807c_Distrib_Amt_E_No.getValue(2).nadd(pnd_4807c_Control_Totals_E_No_Pnd_4807c_Distrib_Amt_E_No.getValue(1));                  //Natural: ADD #4807C-DISTRIB-AMT-E-NO ( 1 ) TO #4807C-DISTRIB-AMT-E-NO ( 2 )
        pnd_4807c_Control_Totals_Pnd_4807c_Taxable_Amt.getValue(2).nadd(pnd_4807c_Control_Totals_Pnd_4807c_Taxable_Amt.getValue(1));                                      //Natural: ADD #4807C-TAXABLE-AMT ( 1 ) TO #4807C-TAXABLE-AMT ( 2 )
        pnd_4807c_Control_Totals_E_Pnd_4807c_Taxable_Amt_E.getValue(2).nadd(pnd_4807c_Control_Totals_E_Pnd_4807c_Taxable_Amt_E.getValue(1));                              //Natural: ADD #4807C-TAXABLE-AMT-E ( 1 ) TO #4807C-TAXABLE-AMT-E ( 2 )
        pnd_4807c_Control_Totals_E_No_Pnd_4807c_Taxable_Amt_E_No.getValue(2).nadd(pnd_4807c_Control_Totals_E_No_Pnd_4807c_Taxable_Amt_E_No.getValue(1));                  //Natural: ADD #4807C-TAXABLE-AMT-E-NO ( 1 ) TO #4807C-TAXABLE-AMT-E-NO ( 2 )
        pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt.getValue(2).nadd(pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt.getValue(1));                                              //Natural: ADD #4807C-IVC-AMT ( 1 ) TO #4807C-IVC-AMT ( 2 )
        pnd_4807c_Control_Totals_E_Pnd_4807c_Ivc_Amt_E.getValue(2).nadd(pnd_4807c_Control_Totals_E_Pnd_4807c_Ivc_Amt_E.getValue(1));                                      //Natural: ADD #4807C-IVC-AMT-E ( 1 ) TO #4807C-IVC-AMT-E ( 2 )
        pnd_4807c_Control_Totals_E_No_Pnd_4807c_Ivc_Amt_E_No.getValue(2).nadd(pnd_4807c_Control_Totals_E_No_Pnd_4807c_Ivc_Amt_E_No.getValue(1));                          //Natural: ADD #4807C-IVC-AMT-E-NO ( 1 ) TO #4807C-IVC-AMT-E-NO ( 2 )
        pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt_Roll.getValue(2).nadd(pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt_Roll.getValue(1));                                //Natural: ADD #4807C-GROSS-AMT-ROLL ( 1 ) TO #4807C-GROSS-AMT-ROLL ( 2 )
        pnd_4807c_Control_Totals_E_Pnd_4807c_Gross_Amt_Roll_E.getValue(2).nadd(pnd_4807c_Control_Totals_E_Pnd_4807c_Gross_Amt_Roll_E.getValue(1));                        //Natural: ADD #4807C-GROSS-AMT-ROLL-E ( 1 ) TO #4807C-GROSS-AMT-ROLL-E ( 2 )
        pnd_4807c_Control_Totals_E_No_Pnd_4807c_Gross_Amt_Roll_E_No.getValue(2).nadd(pnd_4807c_Control_Totals_E_No_Pnd_4807c_Gross_Amt_Roll_E_No.getValue(1));            //Natural: ADD #4807C-GROSS-AMT-ROLL-E-NO ( 1 ) TO #4807C-GROSS-AMT-ROLL-E-NO ( 2 )
        pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt_Roll.getValue(2).nadd(pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt_Roll.getValue(1));                                    //Natural: ADD #4807C-IVC-AMT-ROLL ( 1 ) TO #4807C-IVC-AMT-ROLL ( 2 )
        pnd_4807c_Control_Totals_E_Pnd_4807c_Ivc_Amt_Roll_E.getValue(2).nadd(pnd_4807c_Control_Totals_E_Pnd_4807c_Ivc_Amt_Roll_E.getValue(1));                            //Natural: ADD #4807C-IVC-AMT-ROLL-E ( 1 ) TO #4807C-IVC-AMT-ROLL-E ( 2 )
        pnd_4807c_Control_Totals_E_No_Pnd_4807c_Ivc_Amt_Roll_E_No.getValue(2).nadd(pnd_4807c_Control_Totals_E_No_Pnd_4807c_Ivc_Amt_Roll_E_No.getValue(1));                //Natural: ADD #4807C-IVC-AMT-ROLL-E-NO ( 1 ) TO #4807C-IVC-AMT-ROLL-E-NO ( 2 )
    }
    private void sub_Print_4807_Grand_Total() throws Exception                                                                                                            //Natural: PRINT-4807-GRAND-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Form_Text.setValue("Total");                                                                                                                                  //Natural: ASSIGN #FORM-TEXT := 'Total'
        pnd_K.setValue(2);                                                                                                                                                //Natural: ASSIGN #K := 2
        //*  PR
                                                                                                                                                                          //Natural: PERFORM PRINT-480-7
        sub_Print_480_7();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        getReports().write(5, new TabSetting(1),"Email",new ColumnSpacing(4),pnd_4807c_Control_Totals_E_Pnd_4807c_Form_Cnt_E.getValue(pnd_K), new ReportEditMask          //Natural: WRITE ( 5 ) 1T 'Email' 4X #4807C-FORM-CNT-E ( #K ) 1X #4807C-GROSS-AMT-E ( #K ) 1X #4807C-INT-AMT-E ( #K ) 1X #4807C-DISTRIB-AMT-E ( #K ) 2X #4807C-TAXABLE-AMT-E ( #K ) 1X #4807C-IVC-AMT-E ( #K ) / 57X #4807C-GROSS-AMT-ROLL-E ( #K ) 21X #4807C-IVC-AMT-ROLL-E ( #K )
            ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_4807c_Control_Totals_E_Pnd_4807c_Gross_Amt_E.getValue(pnd_K), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_4807c_Control_Totals_E_Pnd_4807c_Int_Amt_E.getValue(pnd_K), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_4807c_Control_Totals_E_Pnd_4807c_Distrib_Amt_E.getValue(pnd_K), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_4807c_Control_Totals_E_Pnd_4807c_Taxable_Amt_E.getValue(pnd_K), new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_4807c_Control_Totals_E_Pnd_4807c_Ivc_Amt_E.getValue(pnd_K), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            ColumnSpacing(57),pnd_4807c_Control_Totals_E_Pnd_4807c_Gross_Amt_Roll_E.getValue(pnd_K), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(21),pnd_4807c_Control_Totals_E_Pnd_4807c_Ivc_Amt_Roll_E.getValue(pnd_K), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(5, new TabSetting(1),"No-Email",new ColumnSpacing(1),pnd_4807c_Control_Totals_E_No_Pnd_4807c_Form_Cnt_E_No.getValue(pnd_K),                    //Natural: WRITE ( 5 ) 1T 'No-Email' 1X #4807C-FORM-CNT-E-NO ( #K ) 1X #4807C-GROSS-AMT-E-NO ( #K ) 1X #4807C-INT-AMT-E-NO ( #K ) 1X #4807C-DISTRIB-AMT-E-NO ( #K ) 2X #4807C-TAXABLE-AMT-E-NO ( #K ) 1X #4807C-IVC-AMT-E-NO ( #K ) / 57X #4807C-GROSS-AMT-ROLL-E-NO ( #K ) 21X #4807C-IVC-AMT-ROLL-E-NO ( #K )
            new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_4807c_Control_Totals_E_No_Pnd_4807c_Gross_Amt_E_No.getValue(pnd_K), new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_4807c_Control_Totals_E_No_Pnd_4807c_Int_Amt_E_No.getValue(pnd_K), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_4807c_Control_Totals_E_No_Pnd_4807c_Distrib_Amt_E_No.getValue(pnd_K), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_4807c_Control_Totals_E_No_Pnd_4807c_Taxable_Amt_E_No.getValue(pnd_K), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_4807c_Control_Totals_E_No_Pnd_4807c_Ivc_Amt_E_No.getValue(pnd_K), new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(57),pnd_4807c_Control_Totals_E_No_Pnd_4807c_Gross_Amt_Roll_E_No.getValue(pnd_K), new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(21),pnd_4807c_Control_Totals_E_No_Pnd_4807c_Ivc_Amt_Roll_E_No.getValue(pnd_K), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(5, NEWLINE,NEWLINE,NEWLINE,"HELD CNT...:",pnd_4807c_Control_Totals_Pnd_4807c_Hold_Cnt.getValue(pnd_K), new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Empty Forms:",pnd_4807c_Control_Totals_Pnd_4807c_Empty_Form_Cnt.getValue(pnd_K),  //Natural: WRITE ( 5 ) // /'HELD CNT...:' #4807C-HOLD-CNT ( #K ) /'Empty Forms:' #4807C-EMPTY-FORM-CNT ( #K )
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    //*  CANADIAN
    private void sub_Print_Nr4() throws Exception                                                                                                                         //Natural: PRINT-NR4
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #NR4-NDX = 1 TO 4
        for (pnd_Nr4_Ndx.setValue(1); condition(pnd_Nr4_Ndx.lessOrEqual(4)); pnd_Nr4_Ndx.nadd(1))
        {
            short decideConditionsMet2384 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #NR4-NDX;//Natural: VALUE 1
            if (condition((pnd_Nr4_Ndx.equals(1))))
            {
                decideConditionsMet2384++;
                pnd_Form_Text.setValue("NR4");                                                                                                                            //Natural: ASSIGN #FORM-TEXT := 'NR4'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Nr4_Ndx.equals(2))))
            {
                decideConditionsMet2384++;
                pnd_Form_Text.setValue("NR4");                                                                                                                            //Natural: ASSIGN #FORM-TEXT := 'NR4'
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Nr4_Ndx.equals(3))))
            {
                decideConditionsMet2384++;
                pnd_Form_Text.setValue("NR4");                                                                                                                            //Natural: ASSIGN #FORM-TEXT := 'NR4'
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pnd_Nr4_Ndx.equals(4))))
            {
                decideConditionsMet2384++;
                pnd_Form_Text.setValue("TOTAL");                                                                                                                          //Natural: ASSIGN #FORM-TEXT := 'TOTAL'
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(pnd_Nr4_Ndx.equals(1)))                                                                                                                         //Natural: IF #NR4-NDX = 1
            {
                pnd_Form_Text.setValue("NR4");                                                                                                                            //Natural: ASSIGN #FORM-TEXT := 'NR4'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Nr4_Ndx.equals(2)))                                                                                                                         //Natural: IF #NR4-NDX = 2
            {
                pnd_Form_Text.setValue("NR4");                                                                                                                            //Natural: ASSIGN #FORM-TEXT := 'NR4'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Nr4_Ndx.equals(3)))                                                                                                                         //Natural: IF #NR4-NDX = 3
            {
                pnd_Form_Text.setValue("NR4");                                                                                                                            //Natural: ASSIGN #FORM-TEXT := 'NR4'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Nr4_Ndx.equals(4)))                                                                                                                         //Natural: IF #NR4-NDX = 4
            {
                pnd_Form_Text.setValue("TOTAL");                                                                                                                          //Natural: ASSIGN #FORM-TEXT := 'TOTAL'
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet2409 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #NR4-NDX;//Natural: VALUE 1
            if (condition((pnd_Nr4_Ndx.equals(1))))
            {
                decideConditionsMet2409++;
                pnd_Nr4_Income_Desc.setValue("Periodic Payment");                                                                                                         //Natural: ASSIGN #NR4-INCOME-DESC := 'Periodic Payment'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Nr4_Ndx.equals(2))))
            {
                decideConditionsMet2409++;
                pnd_Nr4_Income_Desc.setValue("Lump Sum/RTB");                                                                                                             //Natural: ASSIGN #NR4-INCOME-DESC := 'Lump Sum/RTB'
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Nr4_Ndx.equals(3))))
            {
                decideConditionsMet2409++;
                pnd_Nr4_Income_Desc.setValue("Interest");                                                                                                                 //Natural: ASSIGN #NR4-INCOME-DESC := 'Interest'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Nr4_Income_Desc.reset();                                                                                                                              //Natural: RESET #NR4-INCOME-DESC
            }                                                                                                                                                             //Natural: END-DECIDE
            getReports().display(6, "Type of/Form",                                                                                                                       //Natural: DISPLAY ( 6 ) 'Type of/Form' #FORM-TEXT '/Income Desc' #NR4-INCOME-DESC 'Form/Count' #NR4-FORM-CNT ( #NR4-NDX ) ( EM = Z,ZZZ,ZZ9 ) '/Gross Amount' #NR4-GROSS-AMT ( #NR4-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '/Interest Amount' #NR4-INTEREST-AMT ( #NR4-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'Canadian Tax/Withheld' #NR4-FED-TAX-WTHLD ( #NR4-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            		pnd_Form_Text,"/Income Desc",
            		pnd_Nr4_Income_Desc,"Form/Count",
            		pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt.getValue(pnd_Nr4_Ndx), new ReportEditMask ("Z,ZZZ,ZZ9"),"/Gross Amount",
            		pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt.getValue(pnd_Nr4_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Interest Amount",
            		pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt.getValue(pnd_Nr4_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Canadian Tax/Withheld",
            		pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld.getValue(pnd_Nr4_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(6, new TabSetting(1),"Email",new ColumnSpacing(21),pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt_E.getValue(pnd_Nr4_Ndx), new                    //Natural: WRITE ( 6 ) 1T 'Email' 21X #NR4-FORM-CNT-E ( #NR4-NDX ) ( EM = Z,ZZZ,ZZ9 ) 1X #NR4-GROSS-AMT-E ( #NR4-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #NR4-INTEREST-AMT-E ( #NR4-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #NR4-FED-TAX-WTHLD-E ( #NR4-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt_E.getValue(pnd_Nr4_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(1),pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt_E.getValue(pnd_Nr4_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld_E.getValue(pnd_Nr4_Ndx), 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(6, new TabSetting(1),"No-Email",new ColumnSpacing(18),pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt_E_No.getValue(pnd_Nr4_Ndx),                  //Natural: WRITE ( 6 ) 1T 'No-Email' 18X #NR4-FORM-CNT-E-NO ( #NR4-NDX ) ( EM = Z,ZZZ,ZZ9 ) 1X #NR4-GROSS-AMT-E-NO ( #NR4-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #NR4-INTEREST-AMT-E-NO ( #NR4-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #NR4-FED-TAX-WTHLD-E-NO ( #NR4-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt_E_No.getValue(pnd_Nr4_Ndx), new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt_E_No.getValue(pnd_Nr4_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(1),pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld_E_No.getValue(pnd_Nr4_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Form_Cnt_Gr.getValue(pnd_Nr4_Ndx).nadd(pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt.getValue(pnd_Nr4_Ndx));                      //Natural: ADD #NR4-FORM-CNT ( #NR4-NDX ) TO #NR4-FORM-CNT-GR ( #NR4-NDX )
            pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Gross_Amt_Gr.getValue(pnd_Nr4_Ndx).nadd(pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt.getValue(pnd_Nr4_Ndx));                    //Natural: ADD #NR4-GROSS-AMT ( #NR4-NDX ) TO #NR4-GROSS-AMT-GR ( #NR4-NDX )
            pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Interest_Amt_Gr.getValue(pnd_Nr4_Ndx).nadd(pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt.getValue(pnd_Nr4_Ndx));              //Natural: ADD #NR4-INTEREST-AMT ( #NR4-NDX ) TO #NR4-INTEREST-AMT-GR ( #NR4-NDX )
            pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Fed_Tax_Wthld_Gr.getValue(pnd_Nr4_Ndx).nadd(pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld.getValue(pnd_Nr4_Ndx));            //Natural: ADD #NR4-FED-TAX-WTHLD ( #NR4-NDX ) TO #NR4-FED-TAX-WTHLD-GR ( #NR4-NDX )
            pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Form_Cnt_E_Gr.getValue(pnd_Nr4_Ndx).nadd(pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt_E.getValue(pnd_Nr4_Ndx));                  //Natural: ADD #NR4-FORM-CNT-E ( #NR4-NDX ) TO #NR4-FORM-CNT-E-GR ( #NR4-NDX )
            pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Gross_Amt_E_Gr.getValue(pnd_Nr4_Ndx).nadd(pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt_E.getValue(pnd_Nr4_Ndx));                //Natural: ADD #NR4-GROSS-AMT-E ( #NR4-NDX ) TO #NR4-GROSS-AMT-E-GR ( #NR4-NDX )
            pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Interest_Amt_E_Gr.getValue(pnd_Nr4_Ndx).nadd(pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt_E.getValue(pnd_Nr4_Ndx));          //Natural: ADD #NR4-INTEREST-AMT-E ( #NR4-NDX ) TO #NR4-INTEREST-AMT-E-GR ( #NR4-NDX )
            pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Fed_Tax_Wthld_E_Gr.getValue(pnd_Nr4_Ndx).nadd(pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld_E.getValue(pnd_Nr4_Ndx));        //Natural: ADD #NR4-FED-TAX-WTHLD-E ( #NR4-NDX ) TO #NR4-FED-TAX-WTHLD-E-GR ( #NR4-NDX )
            pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Form_Cnt_E_No_Gr.getValue(pnd_Nr4_Ndx).nadd(pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt_E_No.getValue(pnd_Nr4_Ndx));            //Natural: ADD #NR4-FORM-CNT-E-NO ( #NR4-NDX ) TO #NR4-FORM-CNT-E-NO-GR ( #NR4-NDX )
            pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Gross_Amt_E_No_Gr.getValue(pnd_Nr4_Ndx).nadd(pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt_E_No.getValue(pnd_Nr4_Ndx));          //Natural: ADD #NR4-GROSS-AMT-E-NO ( #NR4-NDX ) TO #NR4-GROSS-AMT-E-NO-GR ( #NR4-NDX )
            pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Interest_Amt_E_No_Gr.getValue(pnd_Nr4_Ndx).nadd(pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt_E_No.getValue(pnd_Nr4_Ndx));    //Natural: ADD #NR4-INTEREST-AMT-E-NO ( #NR4-NDX ) TO #NR4-INTEREST-AMT-E-NO-GR ( #NR4-NDX )
            pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Fed_Tax_Wthld_E_No_Gr.getValue(pnd_Nr4_Ndx).nadd(pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld_E_No.getValue(pnd_Nr4_Ndx));  //Natural: ADD #NR4-FED-TAX-WTHLD-E-NO ( #NR4-NDX ) TO #NR4-FED-TAX-WTHLD-E-NO-GR ( #NR4-NDX )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(6, NEWLINE,NEWLINE,NEWLINE,"Held Cnt...:",pnd_Hold_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Empty Forms:",pnd_Empty_Form_Cnt,            //Natural: WRITE ( 6 ) // /'Held Cnt...:' #HOLD-CNT ( EM = Z,ZZZ,ZZ9 ) /'Empty Forms:' #EMPTY-FORM-CNT ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Write_Hold_Rpt() throws Exception                                                                                                                    //Natural: WRITE-HOLD-RPT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        if (condition(ldaTwrl9710.getForm_Tirf_Moore_Hold_Ind().equals(" ")))                                                                                             //Natural: IF FORM.TIRF-MOORE-HOLD-IND EQ ' '
        {
            ldaTwrl5951.getTwrl5951_Pnd_Moore_Mail().setValue("Y");                                                                                                       //Natural: ASSIGN TWRL5951.#MOORE-MAIL := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl5951.getTwrl5951_Pnd_Moore_Mail().setValue("N");                                                                                                       //Natural: ASSIGN TWRL5951.#MOORE-MAIL := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        //*  EB
        if (condition(ldaTwrl9710.getForm_Tirf_Paper_Print_Hold_Ind().equals(" ")))                                                                                       //Natural: IF FORM.TIRF-PAPER-PRINT-HOLD-IND EQ ' '
        {
            ldaTwrl5951.getTwrl5951_Pnd_In_House_Mail().setValue("Y");                                                                                                    //Natural: ASSIGN TWRL5951.#IN-HOUSE-MAIL := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl5951.getTwrl5951_Pnd_In_House_Mail().setValue("N");                                                                                                    //Natural: ASSIGN TWRL5951.#IN-HOUSE-MAIL := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Print_Hold_Ind().equals(" ")))                                                                                             //Natural: IF FORM.TIRF-PRINT-HOLD-IND EQ ' '
        {
            ldaTwrl5951.getTwrl5951_Pnd_Vol_Printable().setValue("Y");                                                                                                    //Natural: ASSIGN TWRL5951.#VOL-PRINTABLE := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl5951.getTwrl5951_Pnd_Vol_Printable().setValue("N");                                                                                                    //Natural: ASSIGN TWRL5951.#VOL-PRINTABLE := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Irs_Reject_Ind().equals(" ")))                                                                                             //Natural: IF FORM.TIRF-IRS-REJECT-IND EQ ' '
        {
            ldaTwrl5951.getTwrl5951_Pnd_Irs_Reportable().setValue("Y");                                                                                                   //Natural: ASSIGN TWRL5951.#IRS-REPORTABLE := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl5951.getTwrl5951_Pnd_Irs_Reportable().setValue("N");                                                                                                   //Natural: ASSIGN TWRL5951.#IRS-REPORTABLE := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        FOR04:                                                                                                                                                            //Natural: FOR #SYS-ERR-NDX = 1 TO FORM.C*TIRF-SYS-ERR
        for (pnd_Sys_Err_Ndx.setValue(1); condition(pnd_Sys_Err_Ndx.lessOrEqual(ldaTwrl9710.getForm_Count_Casttirf_Sys_Err())); pnd_Sys_Err_Ndx.nadd(1))
        {
            pnd_Err_Ndx.setValue(ldaTwrl9710.getForm_Tirf_Sys_Err().getValue(pnd_Sys_Err_Ndx));                                                                           //Natural: ASSIGN #ERR-NDX := FORM.TIRF-SYS-ERR ( #SYS-ERR-NDX )
            //*  J.ROTHOLZ 12/28/01
            if (condition(pnd_Err_Ndx.equals(24)))                                                                                                                        //Natural: IF #ERR-NDX = 24
            {
                //*  BYPASS MISSING
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  WALKROUTE ERRORS
            }                                                                                                                                                             //Natural: END-IF
            //*  SYS HOLD COUNT
            pnd_Sys_Err_Cnt.getValue(pnd_Err_Ndx.getInt() + 1).nadd(1);                                                                                                   //Natural: ADD 1 TO #SYS-ERR-CNT ( #ERR-NDX )
            //*  SYS HOLD TOTAL
            pnd_Sys_Err_Cnt.getValue(0 + 1).nadd(1);                                                                                                                      //Natural: ADD 1 TO #SYS-ERR-CNT ( 0 )
            ldaTwrl5951.getTwrl5951_Pnd_Tax_Year().setValue(ldaTwrl9710.getForm_Tirf_Tax_Year());                                                                         //Natural: ASSIGN TWRL5951.#TAX-YEAR := FORM.TIRF-TAX-YEAR
            ldaTwrl5951.getTwrl5951_Pnd_Form_Type().setValue(ldaTwrl9710.getForm_Tirf_Form_Type());                                                                       //Natural: ASSIGN TWRL5951.#FORM-TYPE := FORM.TIRF-FORM-TYPE
            ldaTwrl5951.getTwrl5951_Pnd_Comp_Short_Name().setValue(ldaTwrlcomp.getPnd_Twrlcomp_Pnd_Comp_Short_Name().getValue(pnd_Company_Ndx.getInt()                    //Natural: ASSIGN TWRL5951.#COMP-SHORT-NAME := #TWRLCOMP.#COMP-SHORT-NAME ( #COMPANY-NDX )
                + 1));
            ldaTwrl5951.getTwrl5951_Pnd_Tin().setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                                   //Natural: ASSIGN TWRL5951.#TIN := FORM.TIRF-TIN
            ldaTwrl5951.getTwrl5951_Pnd_Contract_Nbr().setValue(ldaTwrl9710.getForm_Tirf_Contract_Nbr());                                                                 //Natural: ASSIGN TWRL5951.#CONTRACT-NBR := FORM.TIRF-CONTRACT-NBR
            ldaTwrl5951.getTwrl5951_Pnd_Payee_Cde().setValue(ldaTwrl9710.getForm_Tirf_Payee_Cde());                                                                       //Natural: ASSIGN TWRL5951.#PAYEE-CDE := FORM.TIRF-PAYEE-CDE
            ldaTwrl5951.getTwrl5951_Pnd_Err_Cde().setValue(pnd_Err_Ndx);                                                                                                  //Natural: ASSIGN TWRL5951.#ERR-CDE := #ERR-NDX
            //* **  WRITE WORK FILE 2 TWRL5951
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Get_Company_Desc() throws Exception                                                                                                                  //Natural: GET-COMPANY-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        DbsUtil.examine(new ExamineSource(ldaTwrlcomp.getPnd_Twrlcomp_Pnd_Comp_Code().getValue("*")), new ExamineSearch(pnd_Old_Company_Cde), new ExamineGivingIndex(pnd_Company_Ndx)); //Natural: EXAMINE #COMP-CODE ( * ) FOR #OLD-COMPANY-CDE GIVING INDEX #COMPANY-NDX
    }
    private void sub_Check_Reported_To_Irs() throws Exception                                                                                                             //Natural: CHECK-REPORTED-TO-IRS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        //*   CHECK IF PREVIOUSLY REPORTED TO IRS
        pnd_Reported_To_Irs.reset();                                                                                                                                      //Natural: RESET #REPORTED-TO-IRS
        pnd_Hist_Key_Pnd_Tax_Year.setValue(ldaTwrl9710.getForm_Tirf_Tax_Year());                                                                                          //Natural: ASSIGN #HIST-KEY.#TAX-YEAR := FORM.TIRF-TAX-YEAR
        pnd_Hist_Key_Pnd_Tin.setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                                                    //Natural: ASSIGN #HIST-KEY.#TIN := FORM.TIRF-TIN
        pnd_Hist_Key_Pnd_Form_Type.setValue(ldaTwrl9710.getForm_Tirf_Form_Type());                                                                                        //Natural: ASSIGN #HIST-KEY.#FORM-TYPE := FORM.TIRF-FORM-TYPE
        pnd_Hist_Key_Pnd_Contract_Nbr.setValue(ldaTwrl9710.getForm_Tirf_Contract_Nbr());                                                                                  //Natural: ASSIGN #HIST-KEY.#CONTRACT-NBR := FORM.TIRF-CONTRACT-NBR
        pnd_Hist_Key_Pnd_Payee.setValue(ldaTwrl9710.getForm_Tirf_Payee_Cde());                                                                                            //Natural: ASSIGN #HIST-KEY.#PAYEE := FORM.TIRF-PAYEE-CDE
        pnd_Hist_Key_Pnd_Key.setValue(ldaTwrl9710.getForm_Tirf_Key());                                                                                                    //Natural: ASSIGN #HIST-KEY.#KEY := FORM.TIRF-KEY
        pnd_Hist_Key_Pnd_Ts.reset();                                                                                                                                      //Natural: RESET #HIST-KEY.#TS #HIST-KEY.#IRS-RPT-IND
        pnd_Hist_Key_Pnd_Irs_Rpt_Ind.reset();
        vw_hist_Form.createHistogram                                                                                                                                      //Natural: HISTOGRAM HIST-FORM TIRF-SUPERDE-4 STARTING FROM #HIST-KEY
        (
        "HIST_IRS",
        "TIRF_SUPERDE_4",
        new Wc[] { new Wc("TIRF_SUPERDE_4", ">=", pnd_Hist_Key.getBinary(), WcType.WITH) }
        );
        HIST_IRS:
        while (condition(vw_hist_Form.readNextRow("HIST_IRS")))
        {
            if (condition(hist_Form_Pnd_Tirf_Tax_Year.notEquals(pnd_Hist_Key_Pnd_Tax_Year) || hist_Form_Pnd_Tirf_Tin.notEquals(pnd_Hist_Key_Pnd_Tin) ||                   //Natural: IF #TIRF-TAX-YEAR NE #HIST-KEY.#TAX-YEAR OR #TIRF-TIN NE #HIST-KEY.#TIN OR #TIRF-FORM-TYPE NE #HIST-KEY.#FORM-TYPE OR #TIRF-CONTRACT-NBR NE #HIST-KEY.#CONTRACT-NBR OR #TIRF-PAYEE-CDE NE #HIST-KEY.#PAYEE OR #TIRF-KEY NE #HIST-KEY.#KEY
                hist_Form_Pnd_Tirf_Form_Type.notEquals(pnd_Hist_Key_Pnd_Form_Type) || hist_Form_Pnd_Tirf_Contract_Nbr.notEquals(pnd_Hist_Key_Pnd_Contract_Nbr) 
                || hist_Form_Pnd_Tirf_Payee_Cde.notEquals(pnd_Hist_Key_Pnd_Payee) || hist_Form_Pnd_Tirf_Key.notEquals(pnd_Hist_Key_Pnd_Key)))
            {
                if (true) break HIST_IRS;                                                                                                                                 //Natural: ESCAPE BOTTOM ( HIST-IRS. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(hist_Form_Pnd_Tirf_Irs_Rpt_Ind.equals("H")))                                                                                                    //Natural: IF #TIRF-IRS-RPT-IND = 'H'
            {
                //*  BYPASS HELD FORMS
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Reported_To_Irs.setValue(true);                                                                                                                       //Natural: ASSIGN #REPORTED-TO-IRS := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        //*   CHECK IF REPORTED TO STATE
        //*  1099-R
        if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().equals(1)))                                                                                                    //Natural: IF FORM.TIRF-FORM-TYPE = 01
        {
            vw_form_1099_R.startDatabaseRead                                                                                                                              //Natural: READ FORM-1099-R BY TIRF-SUPERDE-1 = #HIST-KEY
            (
            "F_1099_R",
            new Wc[] { new Wc("TIRF_SUPERDE_1", ">=", pnd_Hist_Key.getBinary(), WcType.BY) },
            new Oc[] { new Oc("TIRF_SUPERDE_1", "ASC") }
            );
            F_1099_R:
            while (condition(vw_form_1099_R.readNextRow("F_1099_R")))
            {
                if (condition(form_1099_R_Tirf_Tax_Year.notEquals(pnd_Hist_Key_Pnd_Tax_Year) || form_1099_R_Tirf_Tin.notEquals(pnd_Hist_Key_Pnd_Tin) ||                   //Natural: IF FORM-1099-R.TIRF-TAX-YEAR NE #HIST-KEY.#TAX-YEAR OR FORM-1099-R.TIRF-TIN NE #HIST-KEY.#TIN OR FORM-1099-R.TIRF-FORM-TYPE NE #HIST-KEY.#FORM-TYPE OR FORM-1099-R.TIRF-CONTRACT-NBR NE #HIST-KEY.#CONTRACT-NBR OR FORM-1099-R.TIRF-PAYEE-CDE NE #HIST-KEY.#PAYEE OR FORM-1099-R.TIRF-KEY NE #HIST-KEY.#KEY
                    form_1099_R_Tirf_Form_Type.notEquals(pnd_Hist_Key_Pnd_Form_Type) || form_1099_R_Tirf_Contract_Nbr.notEquals(pnd_Hist_Key_Pnd_Contract_Nbr) 
                    || form_1099_R_Tirf_Payee_Cde.notEquals(pnd_Hist_Key_Pnd_Payee) || form_1099_R_Tirf_Key.notEquals(pnd_Hist_Key_Pnd_Key)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                FOR05:                                                                                                                                                    //Natural: FOR #STATE-NDX = 1 TO FORM-1099-R.C*TIRF-1099-R-STATE-GRP
                for (pnd_State_Ndx.setValue(1); condition(pnd_State_Ndx.lessOrEqual(form_1099_R_Count_Casttirf_1099_R_State_Grp)); pnd_State_Ndx.nadd(1))
                {
                    if (condition(form_1099_R_Tirf_State_Auth_Rpt_Ind.getValue(pnd_State_Ndx).equals(" ") || form_1099_R_Tirf_State_Auth_Rpt_Ind.getValue(pnd_State_Ndx).equals("H"))) //Natural: IF FORM-1099-R.TIRF-STATE-AUTH-RPT-IND ( #STATE-NDX ) = ' ' OR = 'H'
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Reported_To_Irs.setValue(true);                                                                                                               //Natural: ASSIGN #REPORTED-TO-IRS := TRUE
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F_1099_R"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F_1099_R"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_System_Hold_Code() throws Exception                                                                                                            //Natural: CHECK-SYSTEM-HOLD-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        if (condition(ldaTwrl9710.getForm_Count_Casttirf_Sys_Err().notEquals(getZero())))                                                                                 //Natural: IF FORM.C*TIRF-SYS-ERR NE 0
        {
            pnd_Hold_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #HOLD-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-HOLD-RPT
            sub_Write_Hold_Rpt();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Grand_Total_1099() throws Exception                                                                                                            //Natural: PRINT-GRAND-TOTAL-1099
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        getReports().display(11, "///Type of/Form",                                                                                                                       //Natural: DISPLAY ( 11 ) '///Type of/Form' #FORM-TEXT '///Form/Count' #FORM-CNT ( EM = Z,ZZZ,ZZ9 ) '///Gross/Amount' #1099R-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '///Tax Free/IVC' #1099R-IVC-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) VERT AS 'Taxable/Amount/-----------/State Tax/Withholding' #1099R-TAXABLE-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #1099R-STATE-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) VERT AS 'Interest/Amount/-----------/Local Tax/Withholding' #1099I-INT-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #1099R-LOC-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) VERT AS 'Federal/Withholding/-----------/IRR Amount' #1099-FED-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #1099R-IRR-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) //
        		pnd_Form_Text,"///Form/Count",
        		pnd_Form_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"///Gross/Amount",
        		pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"///Tax Free/IVC",
        		pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, 
            "Taxable/Amount/-----------/State Tax/Withholding",
        		pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "Interest/Amount/-----------/Local Tax/Withholding",
            
        		pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "Federal/Withholding/-----------/IRR Amount",
            
        		pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().skip(11, 2);                                                                                                                                         //Natural: SKIP ( 11 ) 2
    }
    private void sub_Print_1042s_Gr() throws Exception                                                                                                                    //Natural: PRINT-1042S-GR
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Old_Form_Type.getInt() + 1).setValue("1042-S");                                                          //Natural: MOVE '1042-S' TO #TWRAFRMN.#FORM-NAME ( #OLD-FORM-TYPE )
        getReports().display(12, "Type of/Form",                                                                                                                          //Natural: DISPLAY ( 12 ) 'Type of/Form' #TWRAFRMN.#FORM-NAME ( #OLD-FORM-TYPE ) 'Form/Count' #FORM-CNT-1042S-GR ( EM = Z,ZZZ,ZZ9 ) '/Gross Income' #1042S-GROSS-INCOME-1042S-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'Pension/Annuities' #1042S-PENSION-AMT-1042S-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '/Interest' #1042S-INTEREST-AMT-1042S-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'NRA Tax/Withheld' #1042S-NRA-TAX-WTHLD-1042S-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '/Amount Repaid' #1042S-REFUND-AMT-1042S-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
        		pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Old_Form_Type.getInt() + 1),"Form/Count",
        		pnd_Form_Cnt_1042s_Gr, new ReportEditMask ("Z,ZZZ,ZZ9"),"/Gross Income",
        		pnd_1042s_Gross_Income_1042s_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Pension/Annuities",
        		pnd_1042s_Pension_Amt_1042s_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Interest",
        		pnd_1042s_Interest_Amt_1042s_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"NRA Tax/Withheld",
        		pnd_1042s_Nra_Tax_Wthld_1042s_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Amount Repaid",
        		pnd_1042s_Refund_Amt_1042s_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  FE141205 START
        //*  FE141205 END
        getReports().write(12, new TabSetting(1),"FATCA",new ColumnSpacing(4),pnd_Form_Cnt_1042s_Gr_Fatca.getValue(2), new ReportEditMask ("Z,ZZZ,ZZ9"),new               //Natural: WRITE ( 12 ) 1T 'FATCA' 4X #FORM-CNT-1042S-GR-FATCA ( 2 ) ( EM = Z,ZZZ,ZZ9 ) 1X #1042S-GROSS-INCOME-1042S-GR-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-PENSION-AMT-1042S-GR-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-INTEREST-AMT-1042S-GR-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-NRA-TAX-WTHLD-1042S-GR-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-REFUND-AMT-1042S-GR-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 1T 'NonFATCA' #FORM-CNT-1042S-GR-FATCA ( 1 ) ( EM = Z,ZZZ,ZZ9 ) 1X #1042S-GROSS-INCOME-1042S-GR-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-PENSION-AMT-1042S-GR-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-INTEREST-AMT-1042S-GR-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-NRA-TAX-WTHLD-1042S-GR-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-REFUND-AMT-1042S-GR-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) /
            ColumnSpacing(1),pnd_1042s_Gross_Income_1042s_Gr_Fatca.getValue(2), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Pension_Amt_1042s_Gr_Fatca.getValue(2), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Interest_Amt_1042s_Gr_Fatca.getValue(2), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_1042s_Nra_Tax_Wthld_1042s_Gr_Fatca.getValue(2), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Refund_Amt_1042s_Gr_Fatca.getValue(2), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(1),"NonFATCA",pnd_Form_Cnt_1042s_Gr_Fatca.getValue(1), new ReportEditMask ("Z,ZZZ,ZZ9"),new 
            ColumnSpacing(1),pnd_1042s_Gross_Income_1042s_Gr_Fatca.getValue(1), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Pension_Amt_1042s_Gr_Fatca.getValue(1), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Interest_Amt_1042s_Gr_Fatca.getValue(1), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_1042s_Nra_Tax_Wthld_1042s_Gr_Fatca.getValue(1), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Refund_Amt_1042s_Gr_Fatca.getValue(1), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE);
        if (Global.isEscape()) return;
        //*  FE141205 START
        //*  FE141205 END
        getReports().write(12, new TabSetting(1),"Email",new ColumnSpacing(4),pnd_Form_Cnt_E_1042s_Gr, new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_1042s_Gross_Income_E_1042s_Gr,  //Natural: WRITE ( 12 ) 1T 'Email' 4X #FORM-CNT-E-1042S-GR ( EM = Z,ZZZ,ZZ9 ) 1X #1042S-GROSS-INCOME-E-1042S-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-PENSION-AMT-E-1042S-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-INTEREST-AMT-E-1042S-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-NRA-TAX-WTHLD-E-1042S-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-REFUND-AMT-E-1042S-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 1T 'FATCA' 4X #FORM-CNT-E-1042S-GR-FATCA ( 2 ) ( EM = Z,ZZZ,ZZ9 ) 1X #1042S-GROSS-INCOME-E-1042S-GR-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-PENSION-AMT-E-1042S-GR-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-INTEREST-AMT-E-1042S-GR-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-NRA-TAX-WTHLD-E-1042S-GR-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-REFUND-AMT-E-1042S-GR-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 1T 'NonFATCA' #FORM-CNT-E-1042S-GR-FATCA ( 1 ) ( EM = Z,ZZZ,ZZ9 ) 1X #1042S-GROSS-INCOME-1042S-GR-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-PENSION-AMT-1042S-GR-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-INTEREST-AMT-1042S-GR-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-NRA-TAX-WTHLD-1042S-GR-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-REFUND-AMT-1042S-GR-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) /
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Pension_Amt_E_1042s_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_1042s_Interest_Amt_E_1042s_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Nra_Tax_Wthld_E_1042s_Gr, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Refund_Amt_E_1042s_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(1),"FATCA",new ColumnSpacing(4),pnd_Form_Cnt_E_1042s_Gr_Fatca.getValue(2), new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_1042s_Gross_Income_E_1042s_Gr_Fatca.getValue(2), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Pension_Amt_E_1042s_Gr_Fatca.getValue(2), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_1042s_Interest_Amt_E_1042s_Gr_Fatca.getValue(2), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Nra_Tax_Wthld_E_1042s_Gr_Fatca.getValue(2), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Refund_Amt_E_1042s_Gr_Fatca.getValue(2), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(1),"NonFATCA",pnd_Form_Cnt_E_1042s_Gr_Fatca.getValue(1), new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_1042s_Gross_Income_1042s_Gr_Fatca.getValue(1), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Pension_Amt_1042s_Gr_Fatca.getValue(1), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_1042s_Interest_Amt_1042s_Gr_Fatca.getValue(1), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Nra_Tax_Wthld_1042s_Gr_Fatca.getValue(1), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Refund_Amt_1042s_Gr_Fatca.getValue(1), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),
            NEWLINE);
        if (Global.isEscape()) return;
        //*  FE141205 START
        //*  FE141205 END
        getReports().write(12, new TabSetting(1),"No-Email",new ColumnSpacing(1),pnd_Form_Cnt_E_No_1042s_Gr, new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_1042s_Gross_Income_E_No_1042s_Gr,  //Natural: WRITE ( 12 ) 1T 'No-Email' 1X #FORM-CNT-E-NO-1042S-GR ( EM = Z,ZZZ,ZZ9 ) 1X #1042S-GROSS-INCOME-E-NO-1042S-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-PENSION-AMT-E-NO-1042S-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-INTEREST-AMT-E-NO-1042S-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-NRA-TAX-WTHLD-E-NO-1042S-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-REFUND-AMT-E-NO-1042S-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 1T 'FATCA' 4X #FORM-CNT-E-NO-1042S-GR-FATCA ( 2 ) ( EM = Z,ZZZ,ZZ9 ) 1X #1042S-GROSS-INCOME-E-NO-GR-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-PENSION-AMT-E-NO-GR-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-INTEREST-AMT-E-NO-GR-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-NRA-TAX-WTHLD-E-NO-GR-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-REFUND-AMT-E-NO-GR-FATCA ( 2 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 1T 'NonFATCA' #FORM-CNT-E-NO-1042S-GR-FATCA ( 1 ) ( EM = Z,ZZZ,ZZ9 ) 1X #1042S-GROSS-INCOME-E-NO-GR-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-PENSION-AMT-E-NO-GR-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-INTEREST-AMT-E-NO-GR-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-NRA-TAX-WTHLD-E-NO-GR-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1042S-REFUND-AMT-E-NO-GR-FATCA ( 1 ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) /
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Pension_Amt_E_No_1042s_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_1042s_Interest_Amt_E_No_1042s_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Nra_Tax_Wthld_E_No_1042s_Gr, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Refund_Amt_E_No_1042s_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(1),"FATCA",new ColumnSpacing(4),pnd_Form_Cnt_E_No_1042s_Gr_Fatca.getValue(2), new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_1042s_Gross_Income_E_No_Gr_Fatca.getValue(2), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Pension_Amt_E_No_Gr_Fatca.getValue(2), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_1042s_Interest_Amt_E_No_Gr_Fatca.getValue(2), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Nra_Tax_Wthld_E_No_Gr_Fatca.getValue(2), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Refund_Amt_E_No_Gr_Fatca.getValue(2), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(1),"NonFATCA",pnd_Form_Cnt_E_No_1042s_Gr_Fatca.getValue(1), new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_1042s_Gross_Income_E_No_Gr_Fatca.getValue(1), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Pension_Amt_E_No_Gr_Fatca.getValue(1), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_1042s_Interest_Amt_E_No_Gr_Fatca.getValue(1), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Nra_Tax_Wthld_E_No_Gr_Fatca.getValue(1), 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1042s_Refund_Amt_E_No_Gr_Fatca.getValue(1), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),
            NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Print_5498_Gr() throws Exception                                                                                                                     //Natural: PRINT-5498-GR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Form_Text.setValue("5498 P1");                                                                                                                                //Natural: ASSIGN #FORM-TEXT := '5498 P1'
        //*  DASDH
        //*  DASDH
        //*  BK
        getReports().display(13, "Type of/Form",                                                                                                                          //Natural: DISPLAY ( 13 ) 'Type of/Form' #FORM-TEXT ( IS = ON ) 'Form/Count' #5498-FORM-CNT-GR ( EM = Z,ZZZ,ZZ9 ) 'IRA Contribution/Classic Amount' #5498-TRAD-IRA-CONTRIB-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'Late R/O Amt' #5498-POSTPN-AMT-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'IRA Contribution/ROTH Amount' #5498-ROTH-IRA-CONTRIB-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'SEP Amount' #5498-SEP-AMT-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'Rollover/Contribution' #5498-TRAD-IRA-ROLLOVER-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'Recharactorization/Amount' #5498-TRAD-IRA-RECHAR-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '/Conversion Amount' #5498-ROTH-IRA-CONVERSION-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '/Fair-Mkt-Val Amt' #5498-ACCOUNT-FMV-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
        		pnd_Form_Text, new IdenticalSuppress(true),"Form/Count",
        		pnd_5498_Control_Totals_Gr_Pnd_5498_Form_Cnt_Gr, new ReportEditMask ("Z,ZZZ,ZZ9"),"IRA Contribution/Classic Amount",
        		pnd_5498_Control_Totals_Gr_Pnd_5498_Trad_Ira_Contrib_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Late R/O Amt",
        		pnd_5498_Control_Totals_Gr_Pnd_5498_Postpn_Amt_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"IRA Contribution/ROTH Amount",
        		pnd_5498_Control_Totals_Gr_Pnd_5498_Roth_Ira_Contrib_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"SEP Amount",
        		pnd_5498_Control_Totals_Gr_Pnd_5498_Sep_Amt_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Rollover/Contribution",
        		pnd_5498_Control_Totals_Gr_Pnd_5498_Trad_Ira_Rollover_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Recharactorization/Amount",
        		pnd_5498_Control_Totals_Gr_Pnd_5498_Trad_Ira_Rechar_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Conversion Amount",
        		pnd_5498_Control_Totals_Gr_Pnd_5498_Roth_Ira_Conversion_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Fair-Mkt-Val Amt",
        		pnd_5498_Control_Totals_Gr_Pnd_5498_Account_Fmv_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  DASDH
        //*  DASDH
        getReports().write(13, new TabSetting(1),"Email",new ColumnSpacing(4),pnd_5498_Control_Totals_E_Gr_Pnd_5498_Form_Cnt_E_Gr, new ReportEditMask                     //Natural: WRITE ( 13 ) 1T 'Email' 4X #5498-FORM-CNT-E-GR ( EM = Z,ZZZ,ZZ9 ) 1X #5498-TRAD-IRA-CONTRIB-E-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-ROTH-IRA-CONTRIB-E-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-TRAD-IRA-ROLLOVER-E-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-TRAD-IRA-RECHAR-E-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-ROTH-IRA-CONVERSION-E-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-ACCOUNT-FMV-E-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 19X #5498-POSTPN-AMT-E-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-SEP-AMT-E-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_Gr_Pnd_5498_Trad_Ira_Contrib_E_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_5498_Control_Totals_E_Gr_Pnd_5498_Roth_Ira_Contrib_E_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_Gr_Pnd_5498_Trad_Ira_Rollover_E_Gr, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_Gr_Pnd_5498_Trad_Ira_Rechar_E_Gr, new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_Gr_Pnd_5498_Roth_Ira_Conversion_E_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_5498_Control_Totals_E_Gr_Pnd_5498_Account_Fmv_E_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(19),pnd_5498_Control_Totals_E_Gr_Pnd_5498_Postpn_Amt_E_Gr, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_Gr_Pnd_5498_Sep_Amt_E_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  DASDH
        //*  DASDH
        getReports().write(13, new TabSetting(1),"No-Email",new ColumnSpacing(1),pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Form_Cnt_E_No_Gr, new ReportEditMask            //Natural: WRITE ( 13 ) 1T 'No-Email' 1X #5498-FORM-CNT-E-NO-GR ( EM = Z,ZZZ,ZZ9 ) 1X #5498-TRAD-IRA-CONTRIB-E-NO-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-ROTH-IRA-CONTRIB-E-NO-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-TRAD-IRA-ROLLOVER-E-NO-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-TRAD-IRA-RECHAR-E-NO-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-ROTH-IRA-CONVERSION-E-NO-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-ACCOUNT-FMV-E-NO-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 19X #5498-POSTPN-AMT-E-NO-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-SEP-AMT-E-NO-GR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Trad_Ira_Contrib_E_No_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Roth_Ira_Contrib_E_No_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Trad_Ira_Rollover_E_No_Gr, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Trad_Ira_Rechar_E_No_Gr, new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Roth_Ira_Conversion_E_No_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Account_Fmv_E_No_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(19),pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Postpn_Amt_E_No_Gr, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_No_Gr_Pnd_5498_Sep_Amt_E_No_Gr, new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(13, NEWLINE,NEWLINE);                                                                                                                          //Natural: WRITE ( 13 ) //
        if (Global.isEscape()) return;
    }
    private void sub_Print_480_Gr() throws Exception                                                                                                                      //Natural: PRINT-480-GR
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        pnd_Form_Text.setValue("480.7C");                                                                                                                                 //Natural: MOVE '480.7C' TO #FORM-TEXT
        getReports().display(14, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Type of/Form",                                                      //Natural: DISPLAY ( 14 ) ( HC = R ) 'Type of/Form' #FORM-TEXT ( HC = L ) 'Form/Count' #4807C-FORM-CNT-GR 'Gross/Proceeds' #4807C-GROSS-AMT-GR 'Interest/Amount' #4807C-INT-AMT-GR 'Distribution Amount' #4807C-DISTRIB-AMT-GR / 'Rollover Amount' #4807C-GROSS-AMT-ROLL-GR 'Taxable/Amount' #4807C-TAXABLE-AMT-GR 'IVC Amount' #4807C-IVC-AMT-GR / 'Rollover IVC Amount' #4807C-IVC-AMT-ROLL-GR
        		pnd_Form_Text, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"Form/Count",
        		pnd_4807c_Control_Totals_Gr_Pnd_4807c_Form_Cnt_Gr,"Gross/Proceeds",
        		pnd_4807c_Control_Totals_Gr_Pnd_4807c_Gross_Amt_Gr,"Interest/Amount",
        		pnd_4807c_Control_Totals_Gr_Pnd_4807c_Int_Amt_Gr,"Distribution Amount",
        		pnd_4807c_Control_Totals_Gr_Pnd_4807c_Distrib_Amt_Gr,NEWLINE,"Rollover Amount",
        		pnd_4807c_Control_Totals_Gr_Pnd_4807c_Gross_Amt_Roll_Gr,"Taxable/Amount",
        		pnd_4807c_Control_Totals_Gr_Pnd_4807c_Taxable_Amt_Gr,"IVC Amount",
        		pnd_4807c_Control_Totals_Gr_Pnd_4807c_Ivc_Amt_Gr,NEWLINE,"Rollover IVC Amount",
        		pnd_4807c_Control_Totals_Gr_Pnd_4807c_Ivc_Amt_Roll_Gr);
        if (Global.isEscape()) return;
        getReports().skip(14, 1);                                                                                                                                         //Natural: SKIP ( 14 ) 1
        getReports().write(14, new TabSetting(1),"Email",new ColumnSpacing(4),pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Form_Cnt_E_Gr, new ReportEditMask                   //Natural: WRITE ( 14 ) 1T 'Email' 4X #4807C-FORM-CNT-E-GR 1X #4807C-GROSS-AMT-E-GR 1X #4807C-INT-AMT-E-GR 1X #4807C-DISTRIB-AMT-E-GR 2X #4807C-TAXABLE-AMT-E-GR 1X #4807C-IVC-AMT-E-GR / 57X #4807C-GROSS-AMT-ROLL-E-GR 21X #4807C-IVC-AMT-ROLL-E-GR
            ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Gross_Amt_E_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Int_Amt_E_Gr, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Distrib_Amt_E_Gr, new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Taxable_Amt_E_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Ivc_Amt_E_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(57),pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Gross_Amt_Roll_E_Gr, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(21),pnd_4807c_Control_Totals_E_Gr_Pnd_4807c_Ivc_Amt_Roll_E_Gr, new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(14, new TabSetting(1),"No-Email",new ColumnSpacing(1),pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Form_Cnt_E_No_Gr, new ReportEditMask          //Natural: WRITE ( 14 ) 1T 'No-Email' 1X #4807C-FORM-CNT-E-NO-GR 1X #4807C-GROSS-AMT-E-NO-GR 1X #4807C-INT-AMT-E-NO-GR 1X #4807C-DISTRIB-AMT-E-NO-GR 2X #4807C-TAXABLE-AMT-E-NO-GR 1X #4807C-IVC-AMT-E-NO-GR / 57X #4807C-GROSS-AMT-ROLL-E-NO-GR 21X #4807C-IVC-AMT-ROLL-E-NO-GR
            ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Gross_Amt_E_No_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Int_Amt_E_No_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Distrib_Amt_E_No_Gr, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Taxable_Amt_E_No_Gr, new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Ivc_Amt_E_No_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            ColumnSpacing(57),pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Gross_Amt_Roll_E_No_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(21),pnd_4807c_Control_Totals_E_No_Gr_Pnd_4807c_Ivc_Amt_Roll_E_No_Gr, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
    }
    //*  CANADIAN
    private void sub_Print_Nr4_Gr() throws Exception                                                                                                                      //Natural: PRINT-NR4-GR
    {
        if (BLNatReinput.isReinput()) return;

        FOR06:                                                                                                                                                            //Natural: FOR #NR4-NDX = 1 TO 4
        for (pnd_Nr4_Ndx.setValue(1); condition(pnd_Nr4_Ndx.lessOrEqual(4)); pnd_Nr4_Ndx.nadd(1))
        {
            short decideConditionsMet2598 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #NR4-NDX;//Natural: VALUE 1
            if (condition((pnd_Nr4_Ndx.equals(1))))
            {
                decideConditionsMet2598++;
                pnd_Form_Text.setValue("NR4");                                                                                                                            //Natural: ASSIGN #FORM-TEXT := 'NR4'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Nr4_Ndx.equals(2))))
            {
                decideConditionsMet2598++;
                pnd_Form_Text.setValue("NR4");                                                                                                                            //Natural: ASSIGN #FORM-TEXT := 'NR4'
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Nr4_Ndx.equals(3))))
            {
                decideConditionsMet2598++;
                pnd_Form_Text.setValue("NR4");                                                                                                                            //Natural: ASSIGN #FORM-TEXT := 'NR4'
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pnd_Nr4_Ndx.equals(4))))
            {
                decideConditionsMet2598++;
                pnd_Form_Text.setValue("TOTAL");                                                                                                                          //Natural: ASSIGN #FORM-TEXT := 'TOTAL'
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(pnd_Nr4_Ndx.equals(1)))                                                                                                                         //Natural: IF #NR4-NDX = 1
            {
                pnd_Form_Text.setValue("NR4");                                                                                                                            //Natural: ASSIGN #FORM-TEXT := 'NR4'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Nr4_Ndx.equals(2)))                                                                                                                         //Natural: IF #NR4-NDX = 2
            {
                pnd_Form_Text.setValue("NR4");                                                                                                                            //Natural: ASSIGN #FORM-TEXT := 'NR4'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Nr4_Ndx.equals(3)))                                                                                                                         //Natural: IF #NR4-NDX = 3
            {
                pnd_Form_Text.setValue("NR4");                                                                                                                            //Natural: ASSIGN #FORM-TEXT := 'NR4'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Nr4_Ndx.equals(4)))                                                                                                                         //Natural: IF #NR4-NDX = 4
            {
                pnd_Form_Text.setValue("TOTAL");                                                                                                                          //Natural: ASSIGN #FORM-TEXT := 'TOTAL'
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet2623 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #NR4-NDX;//Natural: VALUE 1
            if (condition((pnd_Nr4_Ndx.equals(1))))
            {
                decideConditionsMet2623++;
                pnd_Nr4_Income_Desc.setValue("Periodic Payment");                                                                                                         //Natural: ASSIGN #NR4-INCOME-DESC := 'Periodic Payment'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Nr4_Ndx.equals(2))))
            {
                decideConditionsMet2623++;
                pnd_Nr4_Income_Desc.setValue("Lump Sum/RTB");                                                                                                             //Natural: ASSIGN #NR4-INCOME-DESC := 'Lump Sum/RTB'
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Nr4_Ndx.equals(3))))
            {
                decideConditionsMet2623++;
                pnd_Nr4_Income_Desc.setValue("Interest");                                                                                                                 //Natural: ASSIGN #NR4-INCOME-DESC := 'Interest'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Nr4_Income_Desc.reset();                                                                                                                              //Natural: RESET #NR4-INCOME-DESC
            }                                                                                                                                                             //Natural: END-DECIDE
            getReports().display(15, "Type of/Form",                                                                                                                      //Natural: DISPLAY ( 15 ) 'Type of/Form' #FORM-TEXT '/Income Desc' #NR4-INCOME-DESC 'Form/Count' #NR4-FORM-CNT-GR ( #NR4-NDX ) ( EM = Z,ZZZ,ZZ9 ) '/Gross Amount' #NR4-GROSS-AMT-GR ( #NR4-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '/Interest Amount' #NR4-INTEREST-AMT-GR ( #NR4-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'Canadian Tax/Withheld' #NR4-FED-TAX-WTHLD-GR ( #NR4-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            		pnd_Form_Text,"/Income Desc",
            		pnd_Nr4_Income_Desc,"Form/Count",
            		pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Form_Cnt_Gr.getValue(pnd_Nr4_Ndx), new ReportEditMask ("Z,ZZZ,ZZ9"),"/Gross Amount",
            		pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Gross_Amt_Gr.getValue(pnd_Nr4_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Interest Amount",
            		pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Interest_Amt_Gr.getValue(pnd_Nr4_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Canadian Tax/Withheld",
                
            		pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Fed_Tax_Wthld_Gr.getValue(pnd_Nr4_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(15, new TabSetting(1),"Email",new ColumnSpacing(21),pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Form_Cnt_E_Gr.getValue(pnd_Nr4_Ndx),                 //Natural: WRITE ( 15 ) 1T 'Email' 21X #NR4-FORM-CNT-E-GR ( #NR4-NDX ) ( EM = Z,ZZZ,ZZ9 ) 1X #NR4-GROSS-AMT-E-GR ( #NR4-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #NR4-INTEREST-AMT-E-GR ( #NR4-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #NR4-FED-TAX-WTHLD-E-GR ( #NR4-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Gross_Amt_E_Gr.getValue(pnd_Nr4_Ndx), new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Interest_Amt_E_Gr.getValue(pnd_Nr4_Ndx), new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Fed_Tax_Wthld_E_Gr.getValue(pnd_Nr4_Ndx), new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(15, new TabSetting(1),"No-Email",new ColumnSpacing(18),pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Form_Cnt_E_No_Gr.getValue(pnd_Nr4_Ndx),           //Natural: WRITE ( 15 ) 1T 'No-Email' 18X #NR4-FORM-CNT-E-NO-GR ( #NR4-NDX ) ( EM = Z,ZZZ,ZZ9 ) 1X #NR4-GROSS-AMT-E-NO-GR ( #NR4-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #NR4-INTEREST-AMT-E-NO-GR ( #NR4-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #NR4-FED-TAX-WTHLD-E-NO-GR ( #NR4-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Gross_Amt_E_No_Gr.getValue(pnd_Nr4_Ndx), new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Interest_Amt_E_No_Gr.getValue(pnd_Nr4_Ndx), new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Fed_Tax_Wthld_E_No_Gr.getValue(pnd_Nr4_Ndx), new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Print_5498_P2() throws Exception                                                                                                                     //Natural: PRINT-5498-P2
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------------------------------
        pnd_Form_Text.setValue("5498 P2");                                                                                                                                //Natural: MOVE '5498 P2' TO #FORM-TEXT
        //*  DASDH
        getReports().display(16, "Type of/Form",                                                                                                                          //Natural: DISPLAY ( 16 ) 'Type of/Form' #FORM-TEXT ( IS = ON ) 'COUNT/Form' #5498-FORM-CNT-P2 ( EM = Z,ZZZ,ZZ9 ) / 'Part' #5498-PART-CNT-P2 ( EM = Z,ZZZ,ZZ9 ) 'IRA Contribution/Classic Amount' #5498-TRAD-IRA-CONTRIB-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'Late R/O Amt' #5498-POSTPN-AMT-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'IRA Contribution/ROTH Amount' #5498-ROTH-IRA-CONTRIB-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'SEP Amount' #5498-SEP-AMT-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'Rollover/Contribution' #5498-TRAD-IRA-ROLLOVER-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'Recharactorization/Amount' #5498-TRAD-IRA-RECHAR-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '/Conversion Amount' #5498-ROTH-IRA-CONVERSION-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '/Fair-Mkt-Val Amt' #5498-ACCOUNT-FMV-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
        		pnd_Form_Text, new IdenticalSuppress(true),"COUNT/Form",
        		pnd_5498_Control_Totals_P2_Pnd_5498_Form_Cnt_P2, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Part",
        		pnd_5498_Control_Totals_P2_Pnd_5498_Part_Cnt_P2, new ReportEditMask ("Z,ZZZ,ZZ9"),"IRA Contribution/Classic Amount",
        		pnd_5498_Control_Totals_P2_Pnd_5498_Trad_Ira_Contrib_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Late R/O Amt",
        		pnd_5498_Control_Totals_P2_Pnd_5498_Postpn_Amt_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"IRA Contribution/ROTH Amount",
        		pnd_5498_Control_Totals_P2_Pnd_5498_Roth_Ira_Contrib_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"SEP Amount",
        		pnd_5498_Control_Totals_P2_Pnd_5498_Sep_Amt_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Rollover/Contribution",
        		pnd_5498_Control_Totals_P2_Pnd_5498_Trad_Ira_Rollover_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Recharactorization/Amount",
        		pnd_5498_Control_Totals_P2_Pnd_5498_Trad_Ira_Rechar_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Conversion Amount",
        		pnd_5498_Control_Totals_P2_Pnd_5498_Roth_Ira_Conversion_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Fair-Mkt-Val Amt",
        		pnd_5498_Control_Totals_P2_Pnd_5498_Account_Fmv_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  DASDH
        //*  DASDH
        getReports().write(16, new TabSetting(1),"Email",new ColumnSpacing(4),pnd_5498_Control_Totals_E_P2_Pnd_5498_Form_Cnt_E_P2, new ReportEditMask                     //Natural: WRITE ( 16 ) 1T 'Email' 4X #5498-FORM-CNT-E-P2 ( EM = Z,ZZZ,ZZ9 ) 1X #5498-TRAD-IRA-CONTRIB-E-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-ROTH-IRA-CONTRIB-E-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-TRAD-IRA-ROLLOVER-E-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-TRAD-IRA-RECHAR-E-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-ROTH-IRA-CONVERSION-E-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-ACCOUNT-FMV-E-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 9X #5498-PART-CNT-E-P2 ( EM = Z,ZZZ,ZZ9 ) 1X #5498-POSTPN-AMT-E-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-SEP-AMT-E-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_P2_Pnd_5498_Trad_Ira_Contrib_E_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_5498_Control_Totals_E_P2_Pnd_5498_Roth_Ira_Contrib_E_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_P2_Pnd_5498_Trad_Ira_Rollover_E_P2, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_P2_Pnd_5498_Trad_Ira_Rechar_E_P2, new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_P2_Pnd_5498_Roth_Ira_Conversion_E_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_5498_Control_Totals_E_P2_Pnd_5498_Account_Fmv_E_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(9),pnd_5498_Control_Totals_E_P2_Pnd_5498_Part_Cnt_E_P2, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_P2_Pnd_5498_Postpn_Amt_E_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_5498_Control_Totals_E_P2_Pnd_5498_Sep_Amt_E_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  DASDH
        //*  DASDH
        getReports().write(16, new TabSetting(1),"No-Email",new ColumnSpacing(1),pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Form_Cnt_E_N_P2, new ReportEditMask              //Natural: WRITE ( 16 ) 1T 'No-Email' 1X #5498-FORM-CNT-E-N-P2 ( EM = Z,ZZZ,ZZ9 ) 1X #5498-TRAD-IRA-CONTRIB-E-N-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-ROTH-IRA-CONTRIB-E-N-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-TRAD-IRA-ROLLOVER-E-N-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-TRAD-IRA-RECHAR-E-N-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-ROTH-IRA-CONVERSION-E-N-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-ACCOUNT-FMV-E-N-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 9X #5498-PART-CNT-E-N-P2 ( EM = Z,ZZZ,ZZ9 ) 1X #5498-POSTPN-AMT-E-N-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #5498-SEP-AMT-E-N-P2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Trad_Ira_Contrib_E_N_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Roth_Ira_Contrib_E_N_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Trad_Ira_Rollover_E_N_P2, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Trad_Ira_Rechar_E_N_P2, new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Roth_Ira_Conversion_E_N_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Account_Fmv_E_N_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(9),pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Part_Cnt_E_N_P2, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Postpn_Amt_E_N_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Sep_Amt_E_N_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
    }
    private void sub_Print_Suny() throws Exception                                                                                                                        //Natural: PRINT-SUNY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Form_Text.setValue("SUNY");                                                                                                                                   //Natural: ASSIGN #FORM-TEXT := 'SUNY'
        getReports().display(18, "Type of/Form",                                                                                                                          //Natural: DISPLAY ( 18 ) 'Type of/Form' #FORM-TEXT ( IS = ON ) 'Number of/Letters' #LETTER-COUNT ( EM = Z,ZZZ,ZZ9 ) 'Number of/Details' #SUNY-CONTRACT-CTR ( EM = Z,ZZZ,ZZ9 ) 'Gross/Amount' #SUNY-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'Federal/Withholding' #SUNY-FED-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'State Tax/Withholding' #SUNY-STATE-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'Local Tax/Withholding' #SUNY-LOC-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
        		pnd_Form_Text, new IdenticalSuppress(true),"Number of/Letters",
        		pnd_Suny_Control_Totals_Pnd_Letter_Count, new ReportEditMask ("Z,ZZZ,ZZ9"),"Number of/Details",
        		pnd_Suny_Control_Totals_Pnd_Suny_Contract_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"),"Gross/Amount",
        		pnd_Suny_Control_Totals_Pnd_Suny_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Federal/Withholding",
        		pnd_Suny_Control_Totals_Pnd_Suny_Fed_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"State Tax/Withholding",
        		pnd_Suny_Control_Totals_Pnd_Suny_State_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Local Tax/Withholding",
        		pnd_Suny_Control_Totals_Pnd_Suny_Loc_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(18, new TabSetting(1),"Email",new ColumnSpacing(4),pnd_Suny_Control_Totals_E_Pnd_Letter_Count_E, new ReportEditMask ("Z,ZZZ,ZZ9"),new          //Natural: WRITE ( 18 ) 1T 'Email' 4X #LETTER-COUNT-E ( EM = Z,ZZZ,ZZ9 ) 1X #SUNY-CONTRACT-CTR-E ( EM = Z,ZZZ,ZZ9 ) 1X #SUNY-GROSS-AMT-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #SUNY-FED-TAX-WTHLD-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #SUNY-STATE-TAX-WTHLD-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #SUNY-LOC-TAX-WTHLD-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(1),pnd_Suny_Control_Totals_E_Pnd_Suny_Contract_Ctr_E, new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_Suny_Control_Totals_E_Pnd_Suny_Gross_Amt_E, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Suny_Control_Totals_E_Pnd_Suny_Fed_Tax_Wthld_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_Suny_Control_Totals_E_Pnd_Suny_State_Tax_Wthld_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Suny_Control_Totals_E_Pnd_Suny_Loc_Tax_Wthld_E, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(18, new TabSetting(1),"No-Email",new ColumnSpacing(1),pnd_Suny_Control_Totals_E_No_Pnd_Letter_Count_E_No, new ReportEditMask                   //Natural: WRITE ( 18 ) 1T 'No-Email' 1X #LETTER-COUNT-E-NO ( EM = Z,ZZZ,ZZ9 ) 1X #SUNY-CONTRACT-CTR-E-NO ( EM = Z,ZZZ,ZZ9 ) 1X #SUNY-GROSS-AMT-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #SUNY-FED-TAX-WTHLD-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #SUNY-STATE-TAX-WTHLD-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #SUNY-LOC-TAX-WTHLD-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_Suny_Control_Totals_E_No_Pnd_Suny_Contract_Ctr_E_No, new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_Suny_Control_Totals_E_No_Pnd_Suny_Gross_Amt_E_No, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Suny_Control_Totals_E_No_Pnd_Suny_Fed_Tax_Wthld_E_No, new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Suny_Control_Totals_E_No_Pnd_Suny_State_Tax_Wthld_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(1),pnd_Suny_Control_Totals_E_No_Pnd_Suny_Loc_Tax_Wthld_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
    }
    private void sub_Get_5498_P2() throws Exception                                                                                                                       //Natural: GET-5498-P2
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------------------------------------
        //*  #S3.#TAX-YEAR  := #INPUT-REC.#TAX-YEAR
        //*  #S3.#STATUS    := 'A'   /* ACTIVE
        //*  #S3.#FORM      :=  4    /* 5498
        //*  RD1.
        //*  READ FORM-X BY TIRF-SUPERDE-3 = #S3
        //*    WHERE FORM-X.TIRF-SRCE-CDE EQ 'IR'                           /*
        getReports().write(0, "================== ",pnd_Form_Key);                                                                                                        //Natural: WRITE '================== ' #FORM-KEY
        if (Global.isEscape()) return;
        vw_form_X.startDatabaseRead                                                                                                                                       //Natural: READ FORM-X BY TIRF-SUPERDE-6 = #FORM-KEY WHERE FORM-X.TIRF-SRCE-CDE EQ 'IR' AND FORM-X.TIRF-FORM-TYPE = 4
        (
        "RD1",
        new Wc[] { new Wc("TIRF_SRCE_CDE", "=", "IR", "And", WcType.WHERE) ,
        new Wc("TIRF_FORM_TYPE", "=", 4, WcType.WHERE) ,
        new Wc("TIRF_SUPERDE_6", ">=", pnd_Form_Key, WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_6", "ASC") }
        );
        boolean endOfDataRd1 = true;
        boolean firstRd1 = true;
        RD1:
        while (condition(vw_form_X.readNextRow("RD1")))
        {
            if (condition(vw_form_X.getAstCOUNTER().greater(0)))
            {
                atBreakEventRd1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataRd1 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF FORM-X.TIRF-SUPERDE-6 /18/
            if (condition(form_X_Tirf_Tax_Year.notEquals(pnd_Form_Key_Pnd_Tax_Year)))                                                                                     //Natural: IF FORM-X.TIRF-TAX-YEAR NE #FORM-KEY.#TAX-YEAR
            {
                if (true) break RD1;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RD1. )
            }                                                                                                                                                             //Natural: END-IF
            rD1Tirf_Superde_6Old.setValue(form_X_Tirf_Superde_6);                                                                                                         //Natural: END-READ
        }
        if (condition(vw_form_X.getAstCOUNTER().greater(0)))
        {
            atBreakEventRd1(endOfDataRd1);
        }
        if (Global.isEscape()) return;
    }
    private void sub_Get_All_Contracts_P2() throws Exception                                                                                                              //Natural: GET-ALL-CONTRACTS-P2
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------------------------------------
        vw_form_R.startDatabaseRead                                                                                                                                       //Natural: READ FORM-R BY TIRF-SUPERDE-6 = #TIRF-SUPERDE-3-S THRU #TIRF-SUPERDE-3-E
        (
        "RX1",
        new Wc[] { new Wc("TIRF_SUPERDE_6", ">=", pnd_Tirf_Superde_3_S, "And", WcType.BY) ,
        new Wc("TIRF_SUPERDE_6", "<=", pnd_Tirf_Superde_3_E, WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_6", "ASC") }
        );
        RX1:
        while (condition(vw_form_R.readNextRow("RX1")))
        {
            pnd_5498_Type_Pnd_5498_Type_N.compute(new ComputeParameters(false, pnd_5498_Type_Pnd_5498_Type_N), form_R_Tirf_Ira_Acct_Type.val().multiply(10));             //Natural: ASSIGN #5498-TYPE-N := VAL ( FORM-R.TIRF-IRA-ACCT-TYPE ) * 10
            if (condition(form_R_Tirf_Srce_Cde.equals("IR")))                                                                                                             //Natural: IF FORM-R.TIRF-SRCE-CDE = 'IR'
            {
                                                                                                                                                                          //Natural: PERFORM ACCESS-IRA-FILE
                sub_Access_Ira_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RX1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RX1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  -------------------------------------------------------
            if (condition(pnd_Customer_Id_Pnd_Pin.notEquals(form_R_Tirf_Pin)))                                                                                            //Natural: IF #PIN NE FORM-R.TIRF-PIN
            {
                pnd_Customer_Id_Pnd_Pin.setValue(form_R_Tirf_Pin);                                                                                                        //Natural: MOVE FORM-R.TIRF-PIN TO #PIN
                pnd_E_Deliver.setValue(false);                                                                                                                            //Natural: MOVE FALSE TO #E-DELIVER
                //*    #COMM-CPMN550.#COMM-CUSTOMER-PIN   := #CUSTOMER-ID
                //*    #COMM-CPMN550.#COMM-CUSTOMER-TYP   := 'PH'
                //*    #COMM-CPMN550.#COMM-CUSTOMER-FK    := 'PPPPPPPPPPPPPPP'
                //*    #COMM-CPMN550.#COMM-DOC-ID         := 'D35'   /* TAX
                //*    CALLNAT 'CPMN550' USING #COMM-CPMN550
                //*    IF #COMM-OUT-USER-ERROR-CODE        = 0 /* TAX EMAIL PREFERENCE
                pnd_Twrparti_Curr_Rec_Sd.reset();                                                                                                                         //Natural: RESET #TWRPARTI-CURR-REC-SD
                pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Tax_Id.setValue(form_R_Tirf_Tin);                                                                               //Natural: ASSIGN #KEY-TWRPARTI-TAX-ID := FORM-R.TIRF-TIN
                pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Status.setValue(" ");                                                                                           //Natural: ASSIGN #KEY-TWRPARTI-STATUS := ' '
                pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Part_Eff_End_Dte.setValue("99999999");                                                                          //Natural: ASSIGN #KEY-TWRPARTI-PART-EFF-END-DTE := '99999999'
                vw_twrparti_Read.startDatabaseRead                                                                                                                        //Natural: READ ( 1 ) TWRPARTI-READ WITH TWRPARTI-CURR-REC-SD EQ #TWRPARTI-CURR-REC-SD
                (
                "READ02",
                new Wc[] { new Wc("TWRPARTI_CURR_REC_SD", ">=", pnd_Twrparti_Curr_Rec_Sd, WcType.BY) },
                new Oc[] { new Oc("TWRPARTI_CURR_REC_SD", "ASC") },
                1
                );
                READ02:
                while (condition(vw_twrparti_Read.readNextRow("READ02")))
                {
                    if (condition(twrparti_Read_Twrparti_Tax_Id.notEquals(form_R_Tirf_Tin)))                                                                              //Natural: IF TWRPARTI-TAX-ID NE FORM-R.TIRF-TIN
                    {
                        getReports().write(0, "=",twrparti_Read_Twrparti_Tax_Id,"=",form_R_Tirf_Tin);                                                                     //Natural: WRITE '=' TWRPARTI-TAX-ID '=' FORM-R.TIRF-TIN
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "** SYSTEM ERROR: SSN NOT ON PARTICIPANT FILE **");                                                                         //Natural: WRITE '** SYSTEM ERROR: SSN NOT ON PARTICIPANT FILE **'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        DbsUtil.terminate(99);  if (true) return;                                                                                                         //Natural: TERMINATE 99
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(twrparti_Read_Twrparti_Rlup_Email_Pref.equals("Y") && form_R_Tirf_Deceased_Ind.notEquals("Y")))                                         //Natural: IF TWRPARTI-RLUP-EMAIL-PREF = 'Y' AND FORM-R.TIRF-DECEASED-IND NE 'Y'
                    {
                        pnd_E_Deliver.setValue(true);                                                                                                                     //Natural: MOVE TRUE TO #E-DELIVER
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RX1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RX1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_5498_Control_Totals_P2_Pnd_5498_Part_Cnt_P2.nadd(1);                                                                                                  //Natural: ADD 1 TO #5498-PART-CNT-P2
                if (condition(pnd_E_Deliver.getBoolean()))                                                                                                                //Natural: IF #E-DELIVER
                {
                    pnd_5498_Control_Totals_E_P2_Pnd_5498_Part_Cnt_E_P2.nadd(1);                                                                                          //Natural: ADD 1 TO #5498-PART-CNT-E-P2
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Part_Cnt_E_N_P2.nadd(1);                                                                                      //Natural: ADD 1 TO #5498-PART-CNT-E-N-P2
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_5498_Control_Totals_P2_Pnd_5498_Form_Cnt_P2.nadd(1);                                                                                                      //Natural: ADD 1 TO #5498-FORM-CNT-P2
            pnd_5498_Control_Totals_P2_Pnd_5498_Trad_Ira_Contrib_P2.nadd(form_R_Tirf_Trad_Ira_Contrib);                                                                   //Natural: ADD FORM-R.TIRF-TRAD-IRA-CONTRIB TO #5498-TRAD-IRA-CONTRIB-P2
            pnd_5498_Control_Totals_P2_Pnd_5498_Roth_Ira_Contrib_P2.nadd(form_R_Tirf_Roth_Ira_Contrib);                                                                   //Natural: ADD FORM-R.TIRF-ROTH-IRA-CONTRIB TO #5498-ROTH-IRA-CONTRIB-P2
            pnd_5498_Control_Totals_P2_Pnd_5498_Trad_Ira_Rollover_P2.nadd(form_R_Tirf_Trad_Ira_Rollover);                                                                 //Natural: ADD FORM-R.TIRF-TRAD-IRA-ROLLOVER TO #5498-TRAD-IRA-ROLLOVER-P2
            pnd_5498_Control_Totals_P2_Pnd_5498_Trad_Ira_Rechar_P2.nadd(form_R_Tirf_Ira_Rechar_Amt);                                                                      //Natural: ADD FORM-R.TIRF-IRA-RECHAR-AMT TO #5498-TRAD-IRA-RECHAR-P2
            pnd_5498_Control_Totals_P2_Pnd_5498_Roth_Ira_Conversion_P2.nadd(form_R_Tirf_Roth_Ira_Conversion);                                                             //Natural: ADD FORM-R.TIRF-ROTH-IRA-CONVERSION TO #5498-ROTH-IRA-CONVERSION-P2
            pnd_5498_Control_Totals_P2_Pnd_5498_Account_Fmv_P2.nadd(form_R_Tirf_Account_Fmv);                                                                             //Natural: ADD FORM-R.TIRF-ACCOUNT-FMV TO #5498-ACCOUNT-FMV-P2
            pnd_5498_Control_Totals_P2_Pnd_5498_Sep_Amt_P2.nadd(form_R_Tirf_Sep_Amt);                                                                                     //Natural: ADD FORM-R.TIRF-SEP-AMT TO #5498-SEP-AMT-P2
            //*  DASDH
            pnd_5498_Control_Totals_P2_Pnd_5498_Postpn_Amt_P2.nadd(form_R_Tirf_Postpn_Amt);                                                                               //Natural: ADD FORM-R.TIRF-POSTPN-AMT TO #5498-POSTPN-AMT-P2
            if (condition(pnd_E_Deliver.equals(true)))                                                                                                                    //Natural: IF #E-DELIVER EQ TRUE
            {
                pnd_5498_Control_Totals_E_P2_Pnd_5498_Form_Cnt_E_P2.nadd(1);                                                                                              //Natural: ADD 1 TO #5498-FORM-CNT-E-P2
                pnd_5498_Control_Totals_E_P2_Pnd_5498_Trad_Ira_Contrib_E_P2.nadd(form_R_Tirf_Trad_Ira_Contrib);                                                           //Natural: ADD FORM-R.TIRF-TRAD-IRA-CONTRIB TO #5498-TRAD-IRA-CONTRIB-E-P2
                pnd_5498_Control_Totals_E_P2_Pnd_5498_Roth_Ira_Contrib_E_P2.nadd(form_R_Tirf_Roth_Ira_Contrib);                                                           //Natural: ADD FORM-R.TIRF-ROTH-IRA-CONTRIB TO #5498-ROTH-IRA-CONTRIB-E-P2
                pnd_5498_Control_Totals_E_P2_Pnd_5498_Trad_Ira_Rollover_E_P2.nadd(form_R_Tirf_Trad_Ira_Rollover);                                                         //Natural: ADD FORM-R.TIRF-TRAD-IRA-ROLLOVER TO #5498-TRAD-IRA-ROLLOVER-E-P2
                pnd_5498_Control_Totals_E_P2_Pnd_5498_Trad_Ira_Rechar_E_P2.nadd(form_R_Tirf_Ira_Rechar_Amt);                                                              //Natural: ADD FORM-R.TIRF-IRA-RECHAR-AMT TO #5498-TRAD-IRA-RECHAR-E-P2
                pnd_5498_Control_Totals_E_P2_Pnd_5498_Roth_Ira_Conversion_E_P2.nadd(form_R_Tirf_Roth_Ira_Conversion);                                                     //Natural: ADD FORM-R.TIRF-ROTH-IRA-CONVERSION TO #5498-ROTH-IRA-CONVERSION-E-P2
                pnd_5498_Control_Totals_E_P2_Pnd_5498_Account_Fmv_E_P2.nadd(form_R_Tirf_Account_Fmv);                                                                     //Natural: ADD FORM-R.TIRF-ACCOUNT-FMV TO #5498-ACCOUNT-FMV-E-P2
                pnd_5498_Control_Totals_E_P2_Pnd_5498_Sep_Amt_E_P2.nadd(form_R_Tirf_Sep_Amt);                                                                             //Natural: ADD FORM-R.TIRF-SEP-AMT TO #5498-SEP-AMT-E-P2
                //* DASDH
                pnd_5498_Control_Totals_E_P2_Pnd_5498_Postpn_Amt_E_P2.nadd(form_R_Tirf_Postpn_Amt);                                                                       //Natural: ADD FORM-R.TIRF-POSTPN-AMT TO #5498-POSTPN-AMT-E-P2
                                                                                                                                                                          //Natural: PERFORM E-DELIVERY-RECS
                sub_E_Delivery_Recs();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RX1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RX1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Form_Cnt_E_N_P2.nadd(1);                                                                                          //Natural: ADD 1 TO #5498-FORM-CNT-E-N-P2
                pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Trad_Ira_Contrib_E_N_P2.nadd(form_R_Tirf_Trad_Ira_Contrib);                                                       //Natural: ADD FORM-R.TIRF-TRAD-IRA-CONTRIB TO #5498-TRAD-IRA-CONTRIB-E-N-P2
                pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Roth_Ira_Contrib_E_N_P2.nadd(form_R_Tirf_Roth_Ira_Contrib);                                                       //Natural: ADD FORM-R.TIRF-ROTH-IRA-CONTRIB TO #5498-ROTH-IRA-CONTRIB-E-N-P2
                pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Trad_Ira_Rollover_E_N_P2.nadd(form_R_Tirf_Trad_Ira_Rollover);                                                     //Natural: ADD FORM-R.TIRF-TRAD-IRA-ROLLOVER TO #5498-TRAD-IRA-ROLLOVER-E-N-P2
                pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Trad_Ira_Rechar_E_N_P2.nadd(form_R_Tirf_Ira_Rechar_Amt);                                                          //Natural: ADD FORM-R.TIRF-IRA-RECHAR-AMT TO #5498-TRAD-IRA-RECHAR-E-N-P2
                pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Roth_Ira_Conversion_E_N_P2.nadd(form_R_Tirf_Roth_Ira_Conversion);                                                 //Natural: ADD FORM-R.TIRF-ROTH-IRA-CONVERSION TO #5498-ROTH-IRA-CONVERSION-E-N-P2
                pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Account_Fmv_E_N_P2.nadd(form_R_Tirf_Account_Fmv);                                                                 //Natural: ADD FORM-R.TIRF-ACCOUNT-FMV TO #5498-ACCOUNT-FMV-E-N-P2
                pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Sep_Amt_E_N_P2.nadd(form_R_Tirf_Sep_Amt);                                                                         //Natural: ADD FORM-R.TIRF-SEP-AMT TO #5498-SEP-AMT-E-N-P2
                //*  DASDH
                pnd_5498_Control_Totals_E_N_P2_Pnd_5498_Postpn_Amt_E_N_P2.nadd(form_R_Tirf_Postpn_Amt);                                                                   //Natural: ADD FORM-R.TIRF-POSTPN-AMT TO #5498-POSTPN-AMT-E-N-P2
                //*  DASDH
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Access_Ira_File() throws Exception                                                                                                                   //Natural: ACCESS-IRA-FILE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ira_Start_Twrc_Tax_Year.setValue(form_R_Tirf_Tax_Year);                                                                                                       //Natural: ASSIGN #IRA-START.TWRC-TAX-YEAR := FORM-R.TIRF-TAX-YEAR
        pnd_Ira_Start_Twrc_Status.setValue(" ");                                                                                                                          //Natural: ASSIGN #IRA-START.TWRC-STATUS := ' '
        pnd_Ira_Start_Twrc_Tax_Id.setValue(form_R_Tirf_Tin);                                                                                                              //Natural: ASSIGN #IRA-START.TWRC-TAX-ID := FORM-R.TIRF-TIN
        pnd_Ira_Start_Twrc_Contract.setValue(form_R_Tirf_Contract_Nbr);                                                                                                   //Natural: ASSIGN #IRA-START.TWRC-CONTRACT := FORM-R.TIRF-CONTRACT-NBR
        pnd_Ira_Start_Twrc_Payee.setValue(form_R_Tirf_Payee_Cde);                                                                                                         //Natural: ASSIGN #IRA-START.TWRC-PAYEE := FORM-R.TIRF-PAYEE-CDE
        pnd_Ira_End.setValue(pnd_Ira_Start);                                                                                                                              //Natural: ASSIGN #IRA-END := #IRA-START
        setValueToSubstring(low_Values,pnd_Ira_Start,26,1);                                                                                                               //Natural: MOVE LOW-VALUES TO SUBSTR ( #IRA-START,26,1 )
        setValueToSubstring(high_Values,pnd_Ira_End,26,1);                                                                                                                //Natural: MOVE HIGH-VALUES TO SUBSTR ( #IRA-END ,26,1 )
        ldaTwrl9610.getVw_ira().startDatabaseRead                                                                                                                         //Natural: READ ( 1 ) IRA BY TWRC-S2-FORMS = #IRA-START THRU #IRA-END
        (
        "R_IRA",
        new Wc[] { new Wc("TWRC_S2_FORMS", ">=", pnd_Ira_Start, "And", WcType.BY) ,
        new Wc("TWRC_S2_FORMS", "<=", pnd_Ira_End, WcType.BY) },
        new Oc[] { new Oc("TWRC_S2_FORMS", "ASC") },
        1
        );
        R_IRA:
        while (condition(ldaTwrl9610.getVw_ira().readNextRow("R_IRA")))
        {
            if (condition(ldaTwrl9610.getIra_Twrc_Source().equals("P2") || ldaTwrl9610.getIra_Twrc_Update_Source().equals("P2")))                                         //Natural: IF IRA.TWRC-SOURCE = 'P2' OR IRA.TWRC-UPDATE-SOURCE = 'P2'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                form_R_Tirf_Srce_Cde.reset();                                                                                                                             //Natural: RESET FORM-R.TIRF-SRCE-CDE
                //*    DISPLAY     'RESET IR'
                //*      'TIN'     FORM-R.TIRF-TIN
                //*      'CONTRACT'FORM-R.TIRF-CONTRACT-NBR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaTwrl9610.getVw_ira().getAstCOUNTER().equals(getZero())))                                                                                         //Natural: IF *COUNTER ( R-IRA. ) = 0
        {
            getReports().write(0, "ERROR","TIN",form_R_Tirf_Tin,"CONTRACT",form_R_Tirf_Contract_Nbr);                                                                     //Natural: WRITE 'ERROR' 'TIN' FORM-R.TIRF-TIN 'CONTRACT' FORM-R.TIRF-CONTRACT-NBR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_E_Delivery_Recs() throws Exception                                                                                                                   //Natural: E-DELIVERY-RECS
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------------------------------
        getWorkFiles().write(2, false, pnd_5498_Type, form_R_Tirf_Tax_Year, form_R_Tirf_Form_Type, form_R_Tirf_Company_Cde, form_R_Tirf_Tin, form_R_Tirf_Contract_Nbr,    //Natural: WRITE WORK FILE 02 #5498-TYPE FORM-R.TIRF-TAX-YEAR FORM-R.TIRF-FORM-TYPE FORM-R.TIRF-COMPANY-CDE FORM-R.TIRF-TIN FORM-R.TIRF-CONTRACT-NBR FORM-R.TIRF-PAYEE-CDE FORM-R.TIRF-SRCE-CDE FORM-R.TIRF-IRA-ACCT-TYPE FORM-R.TIRF-CONTRACT-XREF FORM-R.TIRF-TRAD-IRA-CONTRIB FORM-R.TIRF-ROTH-IRA-CONTRIB FORM-R.TIRF-TRAD-IRA-ROLLOVER FORM-R.TIRF-ROTH-IRA-CONVERSION FORM-R.TIRF-IRA-RECHAR-AMT FORM-R.TIRF-ACCOUNT-FMV FORM-R.TIRF-SEP-AMT FORM-R.TIRF-POSTPN-AMT
            form_R_Tirf_Payee_Cde, form_R_Tirf_Srce_Cde, form_R_Tirf_Ira_Acct_Type, form_R_Tirf_Contract_Xref, form_R_Tirf_Trad_Ira_Contrib, form_R_Tirf_Roth_Ira_Contrib, 
            form_R_Tirf_Trad_Ira_Rollover, form_R_Tirf_Roth_Ira_Conversion, form_R_Tirf_Ira_Rechar_Amt, form_R_Tirf_Account_Fmv, form_R_Tirf_Sep_Amt, form_R_Tirf_Postpn_Amt);
    }

    //

    // Support Methods

    private void atBreakEventRead_Form() throws Exception {atBreakEventRead_Form(false);}
    private void atBreakEventRead_Form(boolean endOfData) throws Exception
    {
        boolean ldaTwrl9710_getForm_Tirf_Form_TypeIsBreak = ldaTwrl9710.getForm_Tirf_Form_Type().isBreak(endOfData);
        boolean ldaTwrl9710_getForm_Tirf_Company_CdeIsBreak = ldaTwrl9710.getForm_Tirf_Company_Cde().isBreak(endOfData);
        if (condition(ldaTwrl9710_getForm_Tirf_Form_TypeIsBreak || ldaTwrl9710_getForm_Tirf_Company_CdeIsBreak))
        {
            if (condition(rEAD_FORMTirf_Tax_YearOld.notEquals(pnd_Form_Key_Pnd_Tax_Year)))                                                                                //Natural: IF OLD ( TIRF-TAX-YEAR ) NE #FORM-KEY.#TAX-YEAR
            {
                if (!endOfData)                                                                                                                                           //Natural: ESCAPE BOTTOM ( READ-FORM. )
                {
                    Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom, "READ_FORM");
                }
                if (condition(true)) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_Old_Company_Cde.setValue(rEAD_FORMTirf_Company_CdeOld);                                                                                                   //Natural: ASSIGN #OLD-COMPANY-CDE := OLD ( TIRF-COMPANY-CDE )
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-DESC
            sub_Get_Company_Desc();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Old_Form_Type.setValue(rEAD_FORMTirf_Form_TypeOld);                                                                                                       //Natural: ASSIGN #OLD-FORM-TYPE := OLD ( TIRF-FORM-TYPE )
            //*  01/30/08 - AY
            short decideConditionsMet1394 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #OLD-FORM-TYPE;//Natural: VALUE 1
            if (condition((pnd_Old_Form_Type.equals(1))))
            {
                decideConditionsMet1394++;
                pnd_Form_Text.setValue(pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Old_Form_Type.getInt() + 1));                                             //Natural: ASSIGN #FORM-TEXT := #TWRAFRMN.#FORM-NAME ( #OLD-FORM-TYPE )
                                                                                                                                                                          //Natural: PERFORM PRINT-1099
                sub_Print_1099();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                getReports().write(2, new TabSetting(1),"Email",new ColumnSpacing(4),pnd_Form_Cnt_E, new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Gross_Amt_E,  //Natural: WRITE ( 2 ) 1T 'Email' 4X #FORM-CNT-E ( EM = Z,ZZZ,ZZ9 ) 1X #1099R-GROSS-AMT-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-IVC-AMT-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-TAXABLE-AMT-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099I-INT-AMT-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099-FED-TAX-WTHLD-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Ivc_Amt_E, new ReportEditMask 
                    ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Taxable_Amt_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_Pnd_1099i_Int_Amt_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_Pnd_1099_Fed_Tax_Wthld_E, 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape())) return;
                getReports().write(2, new ColumnSpacing(57),pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_State_Tax_Wthld_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new   //Natural: WRITE ( 2 ) 57X #1099R-STATE-TAX-WTHLD-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-LOC-TAX-WTHLD-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-IRR-AMT-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                    ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Loc_Tax_Wthld_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Irr_Amt_E, 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape())) return;
                getReports().write(2, new TabSetting(1),"No-Email",new ColumnSpacing(1),pnd_Form_Cnt_E_No, new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Gross_Amt_E_No,  //Natural: WRITE ( 2 ) 1T 'No-Email' 1X #FORM-CNT-E-NO ( EM = Z,ZZZ,ZZ9 ) 1X #1099R-GROSS-AMT-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-IVC-AMT-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-TAXABLE-AMT-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099I-INT-AMT-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099-FED-TAX-WTHLD-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Ivc_Amt_E_No, new ReportEditMask 
                    ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Taxable_Amt_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099i_Int_Amt_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099_Fed_Tax_Wthld_E_No, 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape())) return;
                getReports().write(2, new ColumnSpacing(57),pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_State_Tax_Wthld_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 2 ) 57X #1099R-STATE-TAX-WTHLD-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-LOC-TAX-WTHLD-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-IRR-AMT-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                    ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Loc_Tax_Wthld_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Irr_Amt_E_No, 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape())) return;
                getReports().write(2, NEWLINE,NEWLINE);                                                                                                                   //Natural: WRITE ( 2 ) //
                if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM INCREMENT-1099-TOTAL
                sub_Increment_1099_Total();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Form_Cnt_E_1099r.nadd(pnd_Form_Cnt_E);                                                                                                                //Natural: ADD #FORM-CNT-E TO #FORM-CNT-E-1099R
                pnd_1099r_Gross_Amt_E_1099r.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Gross_Amt_E);                                                                 //Natural: ADD #1099R-GROSS-AMT-E TO #1099R-GROSS-AMT-E-1099R
                pnd_1099r_Ivc_Amt_E_1099r.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Ivc_Amt_E);                                                                     //Natural: ADD #1099R-IVC-AMT-E TO #1099R-IVC-AMT-E-1099R
                pnd_1099r_Taxable_Amt_E_1099r.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Taxable_Amt_E);                                                             //Natural: ADD #1099R-TAXABLE-AMT-E TO #1099R-TAXABLE-AMT-E-1099R
                pnd_1099i_Int_Amt_E_1099r.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099i_Int_Amt_E);                                                                     //Natural: ADD #1099I-INT-AMT-E TO #1099I-INT-AMT-E-1099R
                pnd_1099_Fed_Tax_Wthld_E_1099r.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099_Fed_Tax_Wthld_E);                                                           //Natural: ADD #1099-FED-TAX-WTHLD-E TO #1099-FED-TAX-WTHLD-E-1099R
                pnd_1099r_State_Tax_Wthld_E_1099r.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_State_Tax_Wthld_E);                                                     //Natural: ADD #1099R-STATE-TAX-WTHLD-E TO #1099R-STATE-TAX-WTHLD-E-1099R
                pnd_1099r_Loc_Tax_Wthld_E_1099r.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Loc_Tax_Wthld_E);                                                         //Natural: ADD #1099R-LOC-TAX-WTHLD-E TO #1099R-LOC-TAX-WTHLD-E-1099R
                pnd_1099r_Irr_Amt_E_1099r.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Irr_Amt_E);                                                                     //Natural: ADD #1099R-IRR-AMT-E TO #1099R-IRR-AMT-E-1099R
                pnd_Form_Cnt_E_No_1099r.nadd(pnd_Form_Cnt_E_No);                                                                                                          //Natural: ADD #FORM-CNT-E-NO TO #FORM-CNT-E-NO-1099R
                pnd_1099r_Gross_Amt_E_No_1099r.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Gross_Amt_E_No);                                                        //Natural: ADD #1099R-GROSS-AMT-E-NO TO #1099R-GROSS-AMT-E-NO-1099R
                pnd_1099r_Ivc_Amt_E_No_1099r.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Ivc_Amt_E_No);                                                            //Natural: ADD #1099R-IVC-AMT-E-NO TO #1099R-IVC-AMT-E-NO-1099R
                pnd_1099r_Taxable_Amt_E_No_1099r.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Taxable_Amt_E_No);                                                    //Natural: ADD #1099R-TAXABLE-AMT-E-NO TO #1099R-TAXABLE-AMT-E-NO-1099R
                pnd_1099i_Int_Amt_E_No_1099r.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099i_Int_Amt_E_No);                                                            //Natural: ADD #1099I-INT-AMT-E-NO TO #1099I-INT-AMT-E-NO-1099R
                pnd_1099_Fed_Tax_Wthld_E_No_1099r.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099_Fed_Tax_Wthld_E_No);                                                  //Natural: ADD #1099-FED-TAX-WTHLD-E-NO TO #1099-FED-TAX-WTHLD-E-NO-1099R
                pnd_1099r_State_Tax_Wthld_E_No_109r.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_State_Tax_Wthld_E_No);                                             //Natural: ADD #1099R-STATE-TAX-WTHLD-E-NO TO #1099R-STATE-TAX-WTHLD-E-NO-109R
                pnd_1099r_Loc_Tax_Wthld_E_No_1099r.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Loc_Tax_Wthld_E_No);                                                //Natural: ADD #1099R-LOC-TAX-WTHLD-E-NO TO #1099R-LOC-TAX-WTHLD-E-NO-1099R
                pnd_1099r_Irr_Amt_E_No_1099r.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Irr_Amt_E_No);                                                            //Natural: ADD #1099R-IRR-AMT-E-NO TO #1099R-IRR-AMT-E-NO-1099R
                pnd_1099r_1099i_Control_Totals.reset();                                                                                                                   //Natural: RESET #1099R-1099I-CONTROL-TOTALS #1099R-1099I-CONTROL-TOTALS-E #1099R-1099I-CONTROL-TOTALS-E-NO
                pnd_1099r_1099i_Control_Totals_E.reset();
                pnd_1099r_1099i_Control_Totals_E_No.reset();
                if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().notEquals(2)))                                                                                         //Natural: IF TIRF-FORM-TYPE NE 2
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-1099-GRAND-TOTAL
                    sub_Print_1099_Grand_Total();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    pnd_Form_Cnt_Comp.nadd(pnd_1099_Grand_Totals_Pnd_Total_1099_Form_Cnt);                                                                                //Natural: ADD #TOTAL-1099-FORM-CNT TO #FORM-CNT-COMP
                    pnd_Form_Cnt_Comp_1099.nadd(pnd_1099_Grand_Totals_Pnd_Total_1099_Form_Cnt);                                                                           //Natural: ADD #TOTAL-1099-FORM-CNT TO #FORM-CNT-COMP-1099
                    pnd_Form_Cnt_Comp_E.nadd(pnd_1099_Grand_Totals_E_Pnd_Total_1099_Form_Cnt_E);                                                                          //Natural: ADD #TOTAL-1099-FORM-CNT-E TO #FORM-CNT-COMP-E
                    pnd_Form_Cnt_Comp_E_1099.nadd(pnd_1099_Grand_Totals_E_Pnd_Total_1099_Form_Cnt_E);                                                                     //Natural: ADD #TOTAL-1099-FORM-CNT-E TO #FORM-CNT-COMP-E-1099
                    pnd_Form_Cnt_Comp_E_No.nadd(pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Form_Cnt_E_No);                                                                 //Natural: ADD #TOTAL-1099-FORM-CNT-E-NO TO #FORM-CNT-COMP-E-NO
                    pnd_Form_Cnt_Comp_E_No_1099.nadd(pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Form_Cnt_E_No);                                                            //Natural: ADD #TOTAL-1099-FORM-CNT-E-NO TO #FORM-CNT-COMP-E-NO-1099
                    //*  01/30/08 - AY
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Old_Form_Type.equals(2))))
            {
                decideConditionsMet1394++;
                pnd_Form_Text.setValue(pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Old_Form_Type.getInt() + 1));                                             //Natural: ASSIGN #FORM-TEXT := #TWRAFRMN.#FORM-NAME ( #OLD-FORM-TYPE )
                                                                                                                                                                          //Natural: PERFORM PRINT-1099
                sub_Print_1099();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                getReports().write(2, new TabSetting(1),"Email",new ColumnSpacing(4),pnd_Form_Cnt_E, new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Gross_Amt_E,  //Natural: WRITE ( 2 ) 1T 'Email' 4X #FORM-CNT-E ( EM = Z,ZZZ,ZZ9 ) 1X #1099R-GROSS-AMT-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-IVC-AMT-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-TAXABLE-AMT-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099I-INT-AMT-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099-FED-TAX-WTHLD-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Ivc_Amt_E, new ReportEditMask 
                    ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Taxable_Amt_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_Pnd_1099i_Int_Amt_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_Pnd_1099_Fed_Tax_Wthld_E, 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape())) return;
                getReports().write(2, new ColumnSpacing(57),pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_State_Tax_Wthld_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new   //Natural: WRITE ( 2 ) 57X #1099R-STATE-TAX-WTHLD-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-LOC-TAX-WTHLD-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-IRR-AMT-E ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                    ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Loc_Tax_Wthld_E, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Irr_Amt_E, 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape())) return;
                getReports().write(2, new TabSetting(1),"No-Email",new ColumnSpacing(1),pnd_Form_Cnt_E_No, new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Gross_Amt_E_No,  //Natural: WRITE ( 2 ) 1T 'No-Email' 1X #FORM-CNT-E-NO ( EM = Z,ZZZ,ZZ9 ) 1X #1099R-GROSS-AMT-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-IVC-AMT-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-TAXABLE-AMT-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099I-INT-AMT-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099-FED-TAX-WTHLD-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Ivc_Amt_E_No, new ReportEditMask 
                    ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Taxable_Amt_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099i_Int_Amt_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099_Fed_Tax_Wthld_E_No, 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape())) return;
                getReports().write(2, new ColumnSpacing(57),pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_State_Tax_Wthld_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 2 ) 57X #1099R-STATE-TAX-WTHLD-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-LOC-TAX-WTHLD-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 1X #1099R-IRR-AMT-E-NO ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                    ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Loc_Tax_Wthld_E_No, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Irr_Amt_E_No, 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape())) return;
                getReports().write(2, NEWLINE,NEWLINE);                                                                                                                   //Natural: WRITE ( 2 ) //
                if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM INCREMENT-1099-TOTAL
                sub_Increment_1099_Total();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Form_Cnt_E_1099i.nadd(pnd_Form_Cnt_E);                                                                                                                //Natural: ADD #FORM-CNT-E TO #FORM-CNT-E-1099I
                pnd_1099r_Gross_Amt_E_1099i.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Gross_Amt_E);                                                                 //Natural: ADD #1099R-GROSS-AMT-E TO #1099R-GROSS-AMT-E-1099I
                pnd_1099r_Ivc_Amt_E_1099i.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Ivc_Amt_E);                                                                     //Natural: ADD #1099R-IVC-AMT-E TO #1099R-IVC-AMT-E-1099I
                pnd_1099r_Taxable_Amt_E_1099i.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Taxable_Amt_E);                                                             //Natural: ADD #1099R-TAXABLE-AMT-E TO #1099R-TAXABLE-AMT-E-1099I
                pnd_1099i_Int_Amt_E_1099i.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099i_Int_Amt_E);                                                                     //Natural: ADD #1099I-INT-AMT-E TO #1099I-INT-AMT-E-1099I
                pnd_1099_Fed_Tax_Wthld_E_1099i.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099_Fed_Tax_Wthld_E);                                                           //Natural: ADD #1099-FED-TAX-WTHLD-E TO #1099-FED-TAX-WTHLD-E-1099I
                pnd_1099r_State_Tax_Wthld_E_1099i.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_State_Tax_Wthld_E);                                                     //Natural: ADD #1099R-STATE-TAX-WTHLD-E TO #1099R-STATE-TAX-WTHLD-E-1099I
                pnd_1099r_Loc_Tax_Wthld_E_1099i.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Loc_Tax_Wthld_E);                                                         //Natural: ADD #1099R-LOC-TAX-WTHLD-E TO #1099R-LOC-TAX-WTHLD-E-1099I
                pnd_1099r_Irr_Amt_E_1099i.nadd(pnd_1099r_1099i_Control_Totals_E_Pnd_1099r_Irr_Amt_E);                                                                     //Natural: ADD #1099R-IRR-AMT-E TO #1099R-IRR-AMT-E-1099I
                pnd_Form_Cnt_E_No_1099i.nadd(pnd_Form_Cnt_E_No);                                                                                                          //Natural: ADD #FORM-CNT-E-NO TO #FORM-CNT-E-NO-1099I
                pnd_1099r_Gross_Amt_E_No_1099i.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Gross_Amt_E_No);                                                        //Natural: ADD #1099R-GROSS-AMT-E-NO TO #1099R-GROSS-AMT-E-NO-1099I
                pnd_1099r_Ivc_Amt_E_No_1099i.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Ivc_Amt_E_No);                                                            //Natural: ADD #1099R-IVC-AMT-E-NO TO #1099R-IVC-AMT-E-NO-1099I
                pnd_1099r_Taxable_Amt_E_No_1099i.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Taxable_Amt_E_No);                                                    //Natural: ADD #1099R-TAXABLE-AMT-E-NO TO #1099R-TAXABLE-AMT-E-NO-1099I
                pnd_1099i_Int_Amt_E_No_1099i.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099i_Int_Amt_E_No);                                                            //Natural: ADD #1099I-INT-AMT-E-NO TO #1099I-INT-AMT-E-NO-1099I
                pnd_1099_Fed_Tax_Wthld_E_No_1099i.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099_Fed_Tax_Wthld_E_No);                                                  //Natural: ADD #1099-FED-TAX-WTHLD-E-NO TO #1099-FED-TAX-WTHLD-E-NO-1099I
                pnd_1099r_State_Tax_Wthld_E_No_109i.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_State_Tax_Wthld_E_No);                                             //Natural: ADD #1099R-STATE-TAX-WTHLD-E-NO TO #1099R-STATE-TAX-WTHLD-E-NO-109I
                pnd_1099r_Loc_Tax_Wthld_E_No_1099i.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Loc_Tax_Wthld_E_No);                                                //Natural: ADD #1099R-LOC-TAX-WTHLD-E-NO TO #1099R-LOC-TAX-WTHLD-E-NO-1099I
                pnd_1099r_Irr_Amt_E_No_1099i.nadd(pnd_1099r_1099i_Control_Totals_E_No_Pnd_1099r_Irr_Amt_E_No);                                                            //Natural: ADD #1099R-IRR-AMT-E-NO TO #1099R-IRR-AMT-E-NO-1099I
                                                                                                                                                                          //Natural: PERFORM PRINT-1099-GRAND-TOTAL
                sub_Print_1099_Grand_Total();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Total_1099_Form_Cnt_1099r.nadd(pnd_1099_Grand_Totals_Pnd_Total_1099_Form_Cnt);                                                                        //Natural: ADD #TOTAL-1099-FORM-CNT TO #TOTAL-1099-FORM-CNT-1099R
                pnd_Total_1099_Gross_Amt_1099r.nadd(pnd_1099_Grand_Totals_Pnd_Total_1099_Gross_Amt);                                                                      //Natural: ADD #TOTAL-1099-GROSS-AMT TO #TOTAL-1099-GROSS-AMT-1099R
                pnd_Total_1099_Taxable_Amt_1099r.nadd(pnd_1099_Grand_Totals_Pnd_Total_1099_Taxable_Amt);                                                                  //Natural: ADD #TOTAL-1099-TAXABLE-AMT TO #TOTAL-1099-TAXABLE-AMT-1099R
                pnd_Total_1099_Fed_Tax_Wthld_1099r.nadd(pnd_1099_Grand_Totals_Pnd_Total_1099_Fed_Tax_Wthld);                                                              //Natural: ADD #TOTAL-1099-FED-TAX-WTHLD TO #TOTAL-1099-FED-TAX-WTHLD-1099R
                pnd_Total_1099_Ivc_Amt_1099r.nadd(pnd_1099_Grand_Totals_Pnd_Total_1099_Ivc_Amt);                                                                          //Natural: ADD #TOTAL-1099-IVC-AMT TO #TOTAL-1099-IVC-AMT-1099R
                pnd_Total_1099_State_Tax_Wthld_1099r.nadd(pnd_1099_Grand_Totals_Pnd_Total_1099_State_Tax_Wthld);                                                          //Natural: ADD #TOTAL-1099-STATE-TAX-WTHLD TO #TOTAL-1099-STATE-TAX-WTHLD-1099R
                pnd_Total_1099_Loc_Tax_Wthld_1099r.nadd(pnd_1099_Grand_Totals_Pnd_Total_1099_Loc_Tax_Wthld);                                                              //Natural: ADD #TOTAL-1099-LOC-TAX-WTHLD TO #TOTAL-1099-LOC-TAX-WTHLD-1099R
                pnd_Total_1099_Int_Amt_1099r.nadd(pnd_1099_Grand_Totals_Pnd_Total_1099_Int_Amt);                                                                          //Natural: ADD #TOTAL-1099-INT-AMT TO #TOTAL-1099-INT-AMT-1099R
                pnd_Total_1099_Irr_Amt_1099r.nadd(pnd_1099_Grand_Totals_Pnd_Total_1099_Irr_Amt);                                                                          //Natural: ADD #TOTAL-1099-IRR-AMT TO #TOTAL-1099-IRR-AMT-1099R
                pnd_Form_Cnt_Comp.nadd(pnd_1099_Grand_Totals_Pnd_Total_1099_Form_Cnt);                                                                                    //Natural: ADD #TOTAL-1099-FORM-CNT TO #FORM-CNT-COMP
                pnd_Form_Cnt_Comp_1099.nadd(pnd_1099_Grand_Totals_Pnd_Total_1099_Form_Cnt);                                                                               //Natural: ADD #TOTAL-1099-FORM-CNT TO #FORM-CNT-COMP-1099
                pnd_Form_Cnt_Comp_E.nadd(pnd_1099_Grand_Totals_E_Pnd_Total_1099_Form_Cnt_E);                                                                              //Natural: ADD #TOTAL-1099-FORM-CNT-E TO #FORM-CNT-COMP-E
                pnd_Form_Cnt_Comp_E_1099.nadd(pnd_1099_Grand_Totals_E_Pnd_Total_1099_Form_Cnt_E);                                                                         //Natural: ADD #TOTAL-1099-FORM-CNT-E TO #FORM-CNT-COMP-E-1099
                pnd_Form_Cnt_Comp_E_No.nadd(pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Form_Cnt_E_No);                                                                     //Natural: ADD #TOTAL-1099-FORM-CNT-E-NO TO #FORM-CNT-COMP-E-NO
                pnd_Form_Cnt_Comp_E_No_1099.nadd(pnd_1099_Grand_Totals_E_No_Pnd_Total_1099_Form_Cnt_E_No);                                                                //Natural: ADD #TOTAL-1099-FORM-CNT-E-NO TO #FORM-CNT-COMP-E-NO-1099
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Old_Form_Type.equals(3))))
            {
                decideConditionsMet1394++;
                                                                                                                                                                          //Natural: PERFORM PRINT-1042S
                sub_Print_1042s();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Form_Cnt_Comp.nadd(pnd_Form_Cnt);                                                                                                                     //Natural: ADD #FORM-CNT TO #FORM-CNT-COMP
                pnd_Form_Cnt_Comp_1042s.nadd(pnd_Form_Cnt);                                                                                                               //Natural: ADD #FORM-CNT TO #FORM-CNT-COMP-1042S
                pnd_Form_Cnt_Comp_E.nadd(pnd_Form_Cnt_E);                                                                                                                 //Natural: ADD #FORM-CNT-E TO #FORM-CNT-COMP-E
                pnd_Form_Cnt_Comp_E_1042s.nadd(pnd_Form_Cnt_E);                                                                                                           //Natural: ADD #FORM-CNT-E TO #FORM-CNT-COMP-E-1042S
                pnd_Form_Cnt_Comp_E_No.nadd(pnd_Form_Cnt_E_No);                                                                                                           //Natural: ADD #FORM-CNT-E-NO TO #FORM-CNT-COMP-E-NO
                pnd_Form_Cnt_Comp_E_No_1042s.nadd(pnd_Form_Cnt_E_No);                                                                                                     //Natural: ADD #FORM-CNT-E-NO TO #FORM-CNT-COMP-E-NO-1042S
                //*  FE1412
                pnd_Form_Cnt_Comp_1042s_Fatca.getValue(1).nadd(pnd_Form_Cnt_Fatca.getValue(1));                                                                           //Natural: ADD #FORM-CNT-FATCA ( 1 ) TO #FORM-CNT-COMP-1042S-FATCA ( 1 )
                //*  START
                pnd_Form_Cnt_Comp_1042s_Fatca.getValue(2).nadd(pnd_Form_Cnt_Fatca.getValue(2));                                                                           //Natural: ADD #FORM-CNT-FATCA ( 2 ) TO #FORM-CNT-COMP-1042S-FATCA ( 2 )
                pnd_Form_Cnt_Comp_E_1042s_Fatca.getValue(1).nadd(pnd_Form_Cnt_E_Fatca.getValue(1));                                                                       //Natural: ADD #FORM-CNT-E-FATCA ( 1 ) TO #FORM-CNT-COMP-E-1042S-FATCA ( 1 )
                pnd_Form_Cnt_Comp_E_1042s_Fatca.getValue(2).nadd(pnd_Form_Cnt_E_Fatca.getValue(2));                                                                       //Natural: ADD #FORM-CNT-E-FATCA ( 2 ) TO #FORM-CNT-COMP-E-1042S-FATCA ( 2 )
                pnd_Form_Cnt_Comp_E_No_1042s_Fatca.getValue(1).nadd(pnd_Form_Cnt_E_No_Fatca.getValue(1));                                                                 //Natural: ADD #FORM-CNT-E-NO-FATCA ( 1 ) TO #FORM-CNT-COMP-E-NO-1042S-FATCA ( 1 )
                //*  FE141201 END
                pnd_Form_Cnt_Comp_E_No_1042s_Fatca.getValue(2).nadd(pnd_Form_Cnt_E_No_Fatca.getValue(2));                                                                 //Natural: ADD #FORM-CNT-E-NO-FATCA ( 2 ) TO #FORM-CNT-COMP-E-NO-1042S-FATCA ( 2 )
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pnd_Old_Form_Type.equals(4))))
            {
                decideConditionsMet1394++;
                                                                                                                                                                          //Natural: PERFORM PRINT-5498
                sub_Print_5498();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Form_Cnt_Comp.nadd(pnd_5498_Control_Totals_Pnd_5498_Form_Cnt);                                                                                        //Natural: ADD #5498-FORM-CNT TO #FORM-CNT-COMP
                pnd_Form_Cnt_Comp_5498.nadd(pnd_5498_Control_Totals_Pnd_5498_Form_Cnt);                                                                                   //Natural: ADD #5498-FORM-CNT TO #FORM-CNT-COMP-5498
                pnd_Form_Cnt_Comp_E.nadd(pnd_5498_Control_Totals_E_Pnd_5498_Form_Cnt_E);                                                                                  //Natural: ADD #5498-FORM-CNT-E TO #FORM-CNT-COMP-E
                pnd_Form_Cnt_Comp_E_5498.nadd(pnd_5498_Control_Totals_E_Pnd_5498_Form_Cnt_E);                                                                             //Natural: ADD #5498-FORM-CNT-E TO #FORM-CNT-COMP-E-5498
                pnd_Form_Cnt_Comp_E_No.nadd(pnd_5498_Control_Totals_E_No_Pnd_5498_Form_Cnt_E_No);                                                                         //Natural: ADD #5498-FORM-CNT-E-NO TO #FORM-CNT-COMP-E-NO
                pnd_Form_Cnt_Comp_E_No_5498.nadd(pnd_5498_Control_Totals_E_No_Pnd_5498_Form_Cnt_E_No);                                                                    //Natural: ADD #5498-FORM-CNT-E-NO TO #FORM-CNT-COMP-E-NO-5498
            }                                                                                                                                                             //Natural: VALUE 5
            else if (condition((pnd_Old_Form_Type.equals(5))))
            {
                decideConditionsMet1394++;
                pnd_Form_Text.setValue(pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Old_Form_Type.getInt() + 1));                                             //Natural: ASSIGN #FORM-TEXT := #TWRAFRMN.#FORM-NAME ( #OLD-FORM-TYPE )
                pnd_4807c_Control_Totals_Pnd_4807c_Form_Cnt.getValue(1).setValue(pnd_Form_Cnt);                                                                           //Natural: ASSIGN #4807C-FORM-CNT ( 1 ) := #FORM-CNT
                pnd_4807c_Control_Totals_E_Pnd_4807c_Form_Cnt_E.getValue(1).setValue(pnd_Form_Cnt_E);                                                                     //Natural: ASSIGN #4807C-FORM-CNT-E ( 1 ) := #FORM-CNT-E
                pnd_4807c_Control_Totals_E_No_Pnd_4807c_Form_Cnt_E_No.getValue(1).setValue(pnd_Form_Cnt_E_No);                                                            //Natural: ASSIGN #4807C-FORM-CNT-E-NO ( 1 ) := #FORM-CNT-E-NO
                pnd_4807c_Control_Totals_Pnd_4807c_Hold_Cnt.getValue(1).setValue(pnd_Hold_Cnt);                                                                           //Natural: ASSIGN #4807C-HOLD-CNT ( 1 ) := #HOLD-CNT
                pnd_4807c_Control_Totals_Pnd_4807c_Empty_Form_Cnt.getValue(1).setValue(pnd_Empty_Form_Cnt);                                                               //Natural: ASSIGN #4807C-EMPTY-FORM-CNT ( 1 ) := #EMPTY-FORM-CNT
                pnd_4807c_Control_Totals_Pnd_4807c_Distrib_Amt.getValue(1).compute(new ComputeParameters(false, pnd_4807c_Control_Totals_Pnd_4807c_Distrib_Amt.getValue(1)),  //Natural: ASSIGN #4807C-DISTRIB-AMT ( 1 ) := #4807C-GROSS-AMT ( 1 ) + #4807C-INT-AMT ( 1 )
                    pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt.getValue(1).add(pnd_4807c_Control_Totals_Pnd_4807c_Int_Amt.getValue(1)));
                pnd_K.setValue(1);                                                                                                                                        //Natural: ASSIGN #K := 1
                if (condition(pnd_E_Deliver.equals(true)))                                                                                                                //Natural: IF #E-DELIVER EQ TRUE
                {
                    pnd_4807c_Control_Totals_E_Pnd_4807c_Distrib_Amt_E.getValue(1).compute(new ComputeParameters(false, pnd_4807c_Control_Totals_E_Pnd_4807c_Distrib_Amt_E.getValue(1)),  //Natural: ASSIGN #4807C-DISTRIB-AMT-E ( 1 ) := #4807C-GROSS-AMT ( 1 ) + #4807C-INT-AMT ( 1 )
                        pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt.getValue(1).add(pnd_4807c_Control_Totals_Pnd_4807c_Int_Amt.getValue(1)));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_4807c_Control_Totals_E_No_Pnd_4807c_Distrib_Amt_E_No.getValue(1).compute(new ComputeParameters(false, pnd_4807c_Control_Totals_E_No_Pnd_4807c_Distrib_Amt_E_No.getValue(1)),  //Natural: ASSIGN #4807C-DISTRIB-AMT-E-NO ( 1 ) := #4807C-GROSS-AMT ( 1 ) + #4807C-INT-AMT ( 1 )
                        pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt.getValue(1).add(pnd_4807c_Control_Totals_Pnd_4807c_Int_Amt.getValue(1)));
                }                                                                                                                                                         //Natural: END-IF
                //*  PR
                                                                                                                                                                          //Natural: PERFORM PRINT-480-7
                sub_Print_480_7();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                getReports().write(5, new TabSetting(1),"Email",new ColumnSpacing(4),pnd_4807c_Control_Totals_E_Pnd_4807c_Form_Cnt_E.getValue(pnd_K),                     //Natural: WRITE ( 5 ) 1T 'Email' 4X #4807C-FORM-CNT-E ( #K ) 1X #4807C-GROSS-AMT-E ( #K ) 1X #4807C-INT-AMT-E ( #K ) 1X #4807C-DISTRIB-AMT-E ( #K ) 2X #4807C-TAXABLE-AMT-E ( #K ) 1X #4807C-IVC-AMT-E ( #K ) / 57X #4807C-GROSS-AMT-ROLL-E ( #K ) 21X #4807C-IVC-AMT-ROLL-E ( #K )
                    new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_4807c_Control_Totals_E_Pnd_4807c_Gross_Amt_E.getValue(pnd_K), new ReportEditMask 
                    ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_4807c_Control_Totals_E_Pnd_4807c_Int_Amt_E.getValue(pnd_K), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(1),pnd_4807c_Control_Totals_E_Pnd_4807c_Distrib_Amt_E.getValue(pnd_K), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_4807c_Control_Totals_E_Pnd_4807c_Taxable_Amt_E.getValue(pnd_K), 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_4807c_Control_Totals_E_Pnd_4807c_Ivc_Amt_E.getValue(pnd_K), new ReportEditMask 
                    ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(57),pnd_4807c_Control_Totals_E_Pnd_4807c_Gross_Amt_Roll_E.getValue(pnd_K), new ReportEditMask 
                    ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(21),pnd_4807c_Control_Totals_E_Pnd_4807c_Ivc_Amt_Roll_E.getValue(pnd_K), new ReportEditMask 
                    ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape())) return;
                getReports().write(5, new TabSetting(1),"No-Email",new ColumnSpacing(1),pnd_4807c_Control_Totals_E_No_Pnd_4807c_Form_Cnt_E_No.getValue(pnd_K),            //Natural: WRITE ( 5 ) 1T 'No-Email' 1X #4807C-FORM-CNT-E-NO ( #K ) 1X #4807C-GROSS-AMT-E-NO ( #K ) 1X #4807C-INT-AMT-E-NO ( #K ) 1X #4807C-DISTRIB-AMT-E-NO ( #K ) 2X #4807C-TAXABLE-AMT-E-NO ( #K ) 1X #4807C-IVC-AMT-E-NO ( #K ) / 57X #4807C-GROSS-AMT-ROLL-E-NO ( #K ) 21X #4807C-IVC-AMT-ROLL-E-NO ( #K )
                    new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_4807c_Control_Totals_E_No_Pnd_4807c_Gross_Amt_E_No.getValue(pnd_K), new ReportEditMask 
                    ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_4807c_Control_Totals_E_No_Pnd_4807c_Int_Amt_E_No.getValue(pnd_K), new ReportEditMask 
                    ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_4807c_Control_Totals_E_No_Pnd_4807c_Distrib_Amt_E_No.getValue(pnd_K), new ReportEditMask 
                    ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_4807c_Control_Totals_E_No_Pnd_4807c_Taxable_Amt_E_No.getValue(pnd_K), new ReportEditMask 
                    ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_4807c_Control_Totals_E_No_Pnd_4807c_Ivc_Amt_E_No.getValue(pnd_K), new ReportEditMask 
                    ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(57),pnd_4807c_Control_Totals_E_No_Pnd_4807c_Gross_Amt_Roll_E_No.getValue(pnd_K), new 
                    ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(21),pnd_4807c_Control_Totals_E_No_Pnd_4807c_Ivc_Amt_Roll_E_No.getValue(pnd_K), 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM INCREMENT-4807-TOTAL
                sub_Increment_4807_Total();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*        PERFORM PRINT-4807-GRAND-TOTAL
                pnd_Form_Cnt_Comp.nadd(pnd_4807c_Control_Totals_Pnd_4807c_Form_Cnt.getValue(pnd_K));                                                                      //Natural: ADD #4807C-FORM-CNT ( #K ) TO #FORM-CNT-COMP
                pnd_Form_Cnt_Comp_E.nadd(pnd_4807c_Control_Totals_E_Pnd_4807c_Form_Cnt_E.getValue(pnd_K));                                                                //Natural: ADD #4807C-FORM-CNT-E ( #K ) TO #FORM-CNT-COMP-E
                pnd_Form_Cnt_Comp_E_No.nadd(pnd_4807c_Control_Totals_E_No_Pnd_4807c_Form_Cnt_E_No.getValue(pnd_K));                                                       //Natural: ADD #4807C-FORM-CNT-E-NO ( #K ) TO #FORM-CNT-COMP-E-NO
            }                                                                                                                                                             //Natural: VALUE 7
            else if (condition((pnd_Old_Form_Type.equals(7))))
            {
                decideConditionsMet1394++;
                //*  CANADIAN
                                                                                                                                                                          //Natural: PERFORM PRINT-NR4
                sub_Print_Nr4();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Form_Cnt_Comp.nadd(pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt.getValue(4));                                                                              //Natural: ADD #NR4-FORM-CNT ( 4 ) TO #FORM-CNT-COMP
                pnd_Form_Cnt_Comp_E.nadd(pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt_E.getValue(4));                                                                          //Natural: ADD #NR4-FORM-CNT-E ( 4 ) TO #FORM-CNT-COMP-E
                pnd_Form_Cnt_Comp_E_No.nadd(pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt_E_No.getValue(4));                                                                    //Natural: ADD #NR4-FORM-CNT-E-NO ( 4 ) TO #FORM-CNT-COMP-E-NO
                //*      VALUE 10
                //*        ADD #FORM-CNT                TO #LETTER-COUNT
                //*        ADD #FORM-CNT-E              TO #LETTER-COUNT-E
                //*        ADD #FORM-CNT-E-NO           TO #LETTER-COUNT-E-NO
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  FE141205
            //*  FE141205
            //*  FE141205
            pnd_Form_Cnt.reset();                                                                                                                                         //Natural: RESET #FORM-CNT #FORM-CNT-E #FORM-CNT-E-NO #FORM-CNT-FATCA ( 1:2 ) #FORM-CNT-E-FATCA ( 1:2 ) #FORM-CNT-E-NO-FATCA ( 1:2 ) #HOLD-CNT #EMPTY-FORM-CNT #SYS-ERR-CNT ( * ) #1099R-LOOP #PRINT-1099-LOOP
            pnd_Form_Cnt_E.reset();
            pnd_Form_Cnt_E_No.reset();
            pnd_Form_Cnt_Fatca.getValue(1,":",2).reset();
            pnd_Form_Cnt_E_Fatca.getValue(1,":",2).reset();
            pnd_Form_Cnt_E_No_Fatca.getValue(1,":",2).reset();
            pnd_Hold_Cnt.reset();
            pnd_Empty_Form_Cnt.reset();
            pnd_Sys_Err_Cnt.getValue("*").reset();
            pnd_1099r_Loop.reset();
            pnd_Print_1099_Loop.reset();
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaTwrl9710_getForm_Tirf_Company_CdeIsBreak))
        {
            pnd_Old_Company_Cde.setValue(ldaTwrl9710.getForm_Tirf_Company_Cde());                                                                                         //Natural: ASSIGN #OLD-COMPANY-CDE := TIRF-COMPANY-CDE
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-DESC
            sub_Get_Company_Desc();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Form_Cnt_Comp_Grand.nadd(pnd_Form_Cnt_Comp);                                                                                                              //Natural: ADD #FORM-CNT-COMP TO #FORM-CNT-COMP-GRAND
            pnd_Form_Cnt_Comp_E_Grand.nadd(pnd_Form_Cnt_Comp_E);                                                                                                          //Natural: ADD #FORM-CNT-COMP-E TO #FORM-CNT-COMP-E-GRAND
            pnd_Form_Cnt_Comp_E_No_Grand.nadd(pnd_Form_Cnt_Comp_E_No);                                                                                                    //Natural: ADD #FORM-CNT-COMP-E-NO TO #FORM-CNT-COMP-E-NO-GRAND
            if (condition(pnd_Form_Cnt_Comp.notEquals(getZero())))                                                                                                        //Natural: IF #FORM-CNT-COMP NE 0
            {
                getReports().write(10, NEWLINE,NEWLINE,"--------------------------------------",NEWLINE,NEWLINE,"Forms Company Total:         ",pnd_Form_Cnt_Comp,        //Natural: WRITE ( 10 ) // '--------------------------------------' // 'Forms Company Total:         ' #FORM-CNT-COMP / 'Forms Company Total Email:   ' #FORM-CNT-COMP-E / 'Forms Company Total No-Email:' #FORM-CNT-COMP-E-NO
                    NEWLINE,"Forms Company Total Email:   ",pnd_Form_Cnt_Comp_E,NEWLINE,"Forms Company Total No-Email:",pnd_Form_Cnt_Comp_E_No);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_Form_Cnt_Comp.reset();                                                                                                                                    //Natural: RESET #FORM-CNT-COMP #FORM-CNT-COMP-E #FORM-CNT-COMP-E-NO #1099R-1099I-CONTROL-TOTALS #1099R-1099I-CONTROL-TOTALS-E #1099R-1099I-CONTROL-TOTALS-E-NO #1099-GRAND-TOTALS #1099-GRAND-TOTALS-E #1099-GRAND-TOTALS-E-NO #1042S-CONTROL-TOTALS #1042S-CONTROL-TOTALS-E #1042S-CONTROL-TOTALS-E-NO #5498-CONTROL-TOTALS #5498-CONTROL-TOTALS-E #5498-CONTROL-TOTALS-E-NO #4807C-CONTROL-TOTALS ( * ) #4807C-CONTROL-TOTALS-E ( * ) #4807C-CONTROL-TOTALS-E-NO ( * ) #NR4-CONTROL-TOTALS ( * )
            pnd_Form_Cnt_Comp_E.reset();
            pnd_Form_Cnt_Comp_E_No.reset();
            pnd_1099r_1099i_Control_Totals.reset();
            pnd_1099r_1099i_Control_Totals_E.reset();
            pnd_1099r_1099i_Control_Totals_E_No.reset();
            pnd_1099_Grand_Totals.reset();
            pnd_1099_Grand_Totals_E.reset();
            pnd_1099_Grand_Totals_E_No.reset();
            pnd_1042s_Control_Totals.reset();
            pnd_1042s_Control_Totals_E.reset();
            pnd_1042s_Control_Totals_E_No.reset();
            pnd_5498_Control_Totals.reset();
            pnd_5498_Control_Totals_E.reset();
            pnd_5498_Control_Totals_E_No.reset();
            pnd_4807c_Control_Totals.getValue("*").reset();
            pnd_4807c_Control_Totals_E.getValue("*").reset();
            pnd_4807c_Control_Totals_E_No.getValue("*").reset();
            pnd_Nr4_Control_Totals.getValue("*").reset();
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(3));                                                                                                             //Natural: NEWPAGE ( 3 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(4));                                                                                                             //Natural: NEWPAGE ( 4 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(5));                                                                                                             //Natural: NEWPAGE ( 5 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(6));                                                                                                             //Natural: NEWPAGE ( 6 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(7));                                                                                                             //Natural: NEWPAGE ( 7 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(8));                                                                                                             //Natural: NEWPAGE ( 8 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(9));                                                                                                             //Natural: NEWPAGE ( 9 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }

    private void atBreakEventRd1() throws Exception {atBreakEventRd1(false);}
    private void atBreakEventRd1(boolean endOfData) throws Exception
    {
        boolean form_X_Tirf_Superde_618IsBreak = form_X_Tirf_Superde_6.isBreak(endOfData);
        if (condition(form_X_Tirf_Superde_618IsBreak))
        {
            pnd_Tirf_Superde_3_S.setValue(rD1Tirf_Superde_6Old);                                                                                                          //Natural: ASSIGN #TIRF-SUPERDE-3-S := OLD ( FORM-X.TIRF-SUPERDE-6 )
            pnd_Tirf_Superde_3_E.setValue(rD1Tirf_Superde_6Old);                                                                                                          //Natural: ASSIGN #TIRF-SUPERDE-3-E := OLD ( FORM-X.TIRF-SUPERDE-6 )
            setValueToSubstring(low_Values,pnd_Tirf_Superde_3_S,19,1);                                                                                                    //Natural: MOVE LOW-VALUES TO SUBSTR ( #TIRF-SUPERDE-3-S,19,1 )
            setValueToSubstring(high_Values,pnd_Tirf_Superde_3_E,19,1);                                                                                                   //Natural: MOVE HIGH-VALUES TO SUBSTR ( #TIRF-SUPERDE-3-E,19,1 )
                                                                                                                                                                          //Natural: PERFORM GET-ALL-CONTRACTS-P2
            sub_Get_All_Contracts_P2();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=132 ZP=ON");
        Global.format(1, "PS=58 LS=133 ZP=ON");
        Global.format(2, "PS=58 LS=133 ZP=ON");
        Global.format(3, "PS=58 LS=133 ZP=ON");
        Global.format(4, "PS=58 LS=133 ZP=ON");
        Global.format(5, "PS=58 LS=133 ZP=ON");
        Global.format(6, "PS=58 LS=133 ZP=ON");
        Global.format(7, "PS=58 LS=133 ZP=ON");
        Global.format(8, "PS=58 LS=133 ZP=ON");
        Global.format(9, "PS=58 LS=133 ZP=ON");
        Global.format(10, "PS=58 LS=133 ZP=ON");
        Global.format(11, "PS=58 LS=133 ZP=ON");
        Global.format(12, "PS=58 LS=133 ZP=ON");
        Global.format(13, "PS=58 LS=133 ZP=ON");
        Global.format(14, "PS=58 LS=133 ZP=ON");
        Global.format(15, "PS=58 LS=133 ZP=ON");
        Global.format(16, "PS=58 LS=133 ZP=ON");
        Global.format(18, "PS=58 LS=133 ZP=ON");

        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(2), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"YTD Form Database Control",new TabSetting(110),"Report: RPT2",NEWLINE,new 
            TabSetting(59),"Tax Year",pnd_Input_Rec_Pnd_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE,"Company: ",ldaTwrlcomp.getPnd_Twrlcomp_Pnd_Comp_Short_Name(),
            NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(3), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"YTD Form Database Control",new TabSetting(110),"Report: RPT3",NEWLINE,new 
            TabSetting(59),"Tax Year",pnd_Input_Rec_Pnd_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE,"Company: ",ldaTwrlcomp.getPnd_Twrlcomp_Pnd_Comp_Short_Name(),
            NEWLINE,NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(4), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"YTD Form Database Control",new TabSetting(110),"Report: RPT4",NEWLINE,new 
            TabSetting(59),"Tax Year",pnd_Input_Rec_Pnd_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE,"Company: ",ldaTwrlcomp.getPnd_Twrlcomp_Pnd_Comp_Short_Name(),
            NEWLINE,NEWLINE);
        getReports().write(5, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(5), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"YTD Form Database Control",new TabSetting(110),"Report: RPT5",NEWLINE,new 
            TabSetting(59),"Tax Year",pnd_Input_Rec_Pnd_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE,"Company: ",ldaTwrlcomp.getPnd_Twrlcomp_Pnd_Comp_Short_Name(),
            NEWLINE,NEWLINE);
        getReports().write(6, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(6), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"YTD Form Database Control",new TabSetting(110),"Report: RPT6",NEWLINE,new 
            TabSetting(59),"Tax Year",pnd_Input_Rec_Pnd_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE,"Company: ",ldaTwrlcomp.getPnd_Twrlcomp_Pnd_Comp_Short_Name(),
            NEWLINE,NEWLINE);
        getReports().write(11, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(11), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"YTD Form Database Control",new TabSetting(110),"Report: RPT11",NEWLINE,new 
            TabSetting(59),"Tax Year",pnd_Input_Rec_Pnd_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE,"Grand Total forms 1099-R and 1099-INT",NEWLINE,
            NEWLINE);
        getReports().write(12, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(12), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"YTD Form Database Control",new TabSetting(110),"Report: RPT12",NEWLINE,new 
            TabSetting(59),"Tax Year",pnd_Input_Rec_Pnd_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE,"Grand Total form 1042-S",NEWLINE,NEWLINE);
        getReports().write(13, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(13), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"YTD Form Database Control",new TabSetting(110),"Report: RPT13",NEWLINE,new 
            TabSetting(59),"Tax Year",pnd_Input_Rec_Pnd_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE,"Grand Total form 5498 P1",NEWLINE,NEWLINE);
        getReports().write(14, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(14), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"YTD Form Database Control",new TabSetting(110),"Report: RPT14",NEWLINE,new 
            TabSetting(59),"Tax Year",pnd_Input_Rec_Pnd_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE,"Grand Total form 480.7C",NEWLINE,NEWLINE);
        getReports().write(15, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(15), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"YTD Form Database Control",new TabSetting(110),"Report: RPT15",NEWLINE,new 
            TabSetting(59),"Tax Year",pnd_Input_Rec_Pnd_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE,"Grand Total form NR4",NEWLINE,NEWLINE);
        getReports().write(16, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(16), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"YTD Form Database Control",new TabSetting(110),"Report: RPT16",NEWLINE,new 
            TabSetting(59),"Tax Year",pnd_Input_Rec_Pnd_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE,"Grand Total form 5498 P2",NEWLINE,NEWLINE);
        getReports().write(18, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(110),"PAGE:",getReports().getPageNumberDbs(18), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"YTD Form Database Control",new TabSetting(110),"REPORT: RPT18",NEWLINE,new 
            TabSetting(59),"Tax Year",pnd_Input_Rec_Pnd_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE,"GRAND TOTAL SUNY        ",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(2, "///Type of/Form",
        		pnd_Form_Text,"///Form/Count",
        		pnd_Form_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"///Gross/Amount",
        		pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"///Tax Free/IVC",
        		pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, 
            "Taxable/Amount/-----------/State Tax/Withholding",
        		pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "Interest/Amount/-----------/Local Tax/Withholding",
            
        		pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "Federal/Withholding/-----------/IRR Amount",
            
        		pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE);
        getReports().setDisplayColumns(3, "Type of/Form",
        		pnd_Form_Text, new IdenticalSuppress(true),"Form/Count",
        		pnd_5498_Control_Totals_Pnd_5498_Form_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"IRA Contribution/Classic Amount",
        		pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Contrib, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Late RO Amt",
        		pnd_5498_Control_Totals_Pnd_5498_Postpn_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"IRA Contribution/ROTH Amount",
        		pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Contrib, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"SEP Amount",
        		pnd_5498_Control_Totals_Pnd_5498_Sep_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Rollover/Contribution",
        		pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rollover, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Recharactorization/Amount",
        		pnd_5498_Control_Totals_Pnd_5498_Trad_Ira_Rechar, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Conversion Amount",
        		pnd_5498_Control_Totals_Pnd_5498_Roth_Ira_Conversion, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Fair-Mkt-Val Amt",
        		pnd_5498_Control_Totals_Pnd_5498_Account_Fmv, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(4, "Type of/Form",
        		pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),"Form/Count",
        		pnd_Form_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"/Gross Income",
        		pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Pension/Annuities",
        		pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Interest",
        		pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"NRA Tax/Withheld",
        		pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Amount Repaid",
        		pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(5, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Type of/Form",
        		pnd_Form_Text, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"Form/Count",
        		pnd_4807c_Control_Totals_Pnd_4807c_Form_Cnt,"Gross/Proceeds",
        		pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt,"Interest/Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Int_Amt,"Distribution Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Distrib_Amt,NEWLINE,"Rollover Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Gross_Amt_Roll,"Taxable/Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Taxable_Amt,"IVC Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt,NEWLINE,"Rollover IVC Amount",
        		pnd_4807c_Control_Totals_Pnd_4807c_Ivc_Amt_Roll);
        getReports().setDisplayColumns(6, "Type of/Form",
        		pnd_Form_Text,"/Income Desc",
        		pnd_Nr4_Income_Desc,"Form/Count",
        		pnd_Nr4_Control_Totals_Pnd_Nr4_Form_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"/Gross Amount",
        		pnd_Nr4_Control_Totals_Pnd_Nr4_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Interest Amount",
        		pnd_Nr4_Control_Totals_Pnd_Nr4_Interest_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Canadian Tax/Withheld",
        		pnd_Nr4_Control_Totals_Pnd_Nr4_Fed_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(11, "///Type of/Form",
        		pnd_Form_Text,"///Form/Count",
        		pnd_Form_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"///Gross/Amount",
        		pnd_1099r_1099i_Control_Totals_Pnd_1099r_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"///Tax Free/IVC",
        		pnd_1099r_1099i_Control_Totals_Pnd_1099r_Ivc_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, 
            "Taxable/Amount/-----------/State Tax/Withholding",
        		pnd_1099r_1099i_Control_Totals_Pnd_1099r_Taxable_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099r_1099i_Control_Totals_Pnd_1099r_State_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "Interest/Amount/-----------/Local Tax/Withholding",
            
        		pnd_1099r_1099i_Control_Totals_Pnd_1099i_Int_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099r_1099i_Control_Totals_Pnd_1099r_Loc_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "Federal/Withholding/-----------/IRR Amount",
            
        		pnd_1099r_1099i_Control_Totals_Pnd_1099_Fed_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_1099r_1099i_Control_Totals_Pnd_1099r_Irr_Amt, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE);
        getReports().setDisplayColumns(12, "Type of/Form",
        		pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),"Form/Count",
        		pnd_Form_Cnt_1042s_Gr, new ReportEditMask ("Z,ZZZ,ZZ9"),"/Gross Income",
        		pnd_1042s_Gross_Income_1042s_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Pension/Annuities",
        		pnd_1042s_Pension_Amt_1042s_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Interest",
        		pnd_1042s_Interest_Amt_1042s_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"NRA Tax/Withheld",
        		pnd_1042s_Nra_Tax_Wthld_1042s_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Amount Repaid",
        		pnd_1042s_Refund_Amt_1042s_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(13, "Type of/Form",
        		pnd_Form_Text, new IdenticalSuppress(true),"Form/Count",
        		pnd_5498_Control_Totals_Gr_Pnd_5498_Form_Cnt_Gr, new ReportEditMask ("Z,ZZZ,ZZ9"),"IRA Contribution/Classic Amount",
        		pnd_5498_Control_Totals_Gr_Pnd_5498_Trad_Ira_Contrib_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Late R/O Amt",
        		pnd_5498_Control_Totals_Gr_Pnd_5498_Postpn_Amt_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"IRA Contribution/ROTH Amount",
        		pnd_5498_Control_Totals_Gr_Pnd_5498_Roth_Ira_Contrib_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"SEP Amount",
        		pnd_5498_Control_Totals_Gr_Pnd_5498_Sep_Amt_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Rollover/Contribution",
        		pnd_5498_Control_Totals_Gr_Pnd_5498_Trad_Ira_Rollover_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Recharactorization/Amount",
        		pnd_5498_Control_Totals_Gr_Pnd_5498_Trad_Ira_Rechar_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Conversion Amount",
        		pnd_5498_Control_Totals_Gr_Pnd_5498_Roth_Ira_Conversion_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Fair-Mkt-Val Amt",
        		pnd_5498_Control_Totals_Gr_Pnd_5498_Account_Fmv_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(14, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Type of/Form",
        		pnd_Form_Text, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"Form/Count",
        		pnd_4807c_Control_Totals_Gr_Pnd_4807c_Form_Cnt_Gr,"Gross/Proceeds",
        		pnd_4807c_Control_Totals_Gr_Pnd_4807c_Gross_Amt_Gr,"Interest/Amount",
        		pnd_4807c_Control_Totals_Gr_Pnd_4807c_Int_Amt_Gr,"Distribution Amount",
        		pnd_4807c_Control_Totals_Gr_Pnd_4807c_Distrib_Amt_Gr,NEWLINE,"Rollover Amount",
        		pnd_4807c_Control_Totals_Gr_Pnd_4807c_Gross_Amt_Roll_Gr,"Taxable/Amount",
        		pnd_4807c_Control_Totals_Gr_Pnd_4807c_Taxable_Amt_Gr,"IVC Amount",
        		pnd_4807c_Control_Totals_Gr_Pnd_4807c_Ivc_Amt_Gr,NEWLINE,"Rollover IVC Amount",
        		pnd_4807c_Control_Totals_Gr_Pnd_4807c_Ivc_Amt_Roll_Gr);
        getReports().setDisplayColumns(15, "Type of/Form",
        		pnd_Form_Text,"/Income Desc",
        		pnd_Nr4_Income_Desc,"Form/Count",
        		pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Form_Cnt_Gr, new ReportEditMask ("Z,ZZZ,ZZ9"),"/Gross Amount",
        		pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Gross_Amt_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Interest Amount",
        		pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Interest_Amt_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Canadian Tax/Withheld",
        		pnd_Nr4_Control_Totals_Gr_Pnd_Nr4_Fed_Tax_Wthld_Gr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(16, "Type of/Form",
        		pnd_Form_Text, new IdenticalSuppress(true),"COUNT/Form",
        		pnd_5498_Control_Totals_P2_Pnd_5498_Form_Cnt_P2, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Part",
        		pnd_5498_Control_Totals_P2_Pnd_5498_Part_Cnt_P2, new ReportEditMask ("Z,ZZZ,ZZ9"),"IRA Contribution/Classic Amount",
        		pnd_5498_Control_Totals_P2_Pnd_5498_Trad_Ira_Contrib_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Late R/O Amt",
        		pnd_5498_Control_Totals_P2_Pnd_5498_Postpn_Amt_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"IRA Contribution/ROTH Amount",
        		pnd_5498_Control_Totals_P2_Pnd_5498_Roth_Ira_Contrib_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"SEP Amount",
        		pnd_5498_Control_Totals_P2_Pnd_5498_Sep_Amt_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Rollover/Contribution",
        		pnd_5498_Control_Totals_P2_Pnd_5498_Trad_Ira_Rollover_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Recharactorization/Amount",
        		pnd_5498_Control_Totals_P2_Pnd_5498_Trad_Ira_Rechar_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Conversion Amount",
        		pnd_5498_Control_Totals_P2_Pnd_5498_Roth_Ira_Conversion_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Fair-Mkt-Val Amt",
        		pnd_5498_Control_Totals_P2_Pnd_5498_Account_Fmv_P2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(18, "Type of/Form",
        		pnd_Form_Text, new IdenticalSuppress(true),"Number of/Letters",
        		pnd_Suny_Control_Totals_Pnd_Letter_Count, new ReportEditMask ("Z,ZZZ,ZZ9"),"Number of/Details",
        		pnd_Suny_Control_Totals_Pnd_Suny_Contract_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"),"Gross/Amount",
        		pnd_Suny_Control_Totals_Pnd_Suny_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Federal/Withholding",
        		pnd_Suny_Control_Totals_Pnd_Suny_Fed_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"State Tax/Withholding",
        		pnd_Suny_Control_Totals_Pnd_Suny_State_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Local Tax/Withholding",
        		pnd_Suny_Control_Totals_Pnd_Suny_Loc_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
    }
    private void CheckAtStartofData1373() throws Exception
    {
        if (condition(ldaTwrl9710.getVw_form().getAtStartOfData()))
        {
            pnd_Old_Company_Cde.setValue(ldaTwrl9710.getForm_Tirf_Company_Cde());                                                                                         //Natural: ASSIGN #OLD-COMPANY-CDE := TIRF-COMPANY-CDE
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-DESC
            sub_Get_Company_Desc();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
