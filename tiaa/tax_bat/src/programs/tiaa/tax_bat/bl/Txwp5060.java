/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:46:29 PM
**        * FROM NATURAL PROGRAM : Txwp5060
************************************************************
**        * FILE NAME            : Txwp5060.java
**        * CLASS NAME           : Txwp5060
**        * INSTANCE NAME        : Txwp5060
************************************************************
************************************************************************
* SUBPROGRAM: TXWP5060
* FUNCTION  : UPDATE W8 OVERRIDE FLAG FOR FOREIGN CITIZENS
* PROGRAMMER: JONATHAN ROTHOLZ - 10/02/2001
* HISTORY...: J.ROTHOLZ 12/5/01  MODIFIED UPDATE CRITERIA
*    04/20/05 MS - NEW PROVINCE CODE 88 - NU
*    06/10/15 FENDAYA COR AND NAS SUNSET. FE201506
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Txwp5060 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaTxwa2410 pdaTxwa2410;
    private PdaMdma101 pdaMdma101;
    private LdaTxwl2425 ldaTxwl2425;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Txwp5060;
    private DbsField pnd_Txwp5060_Pnd_Debug_Ind;
    private DbsField pnd_Txwp5060_Pnd_Abend_Ind;
    private DbsField pnd_Txwp5060_Pnd_Display_Ind;
    private DbsField pnd_Txwp5060_Pnd_Ret_Msg;
    private DbsField pnd_Cnt_Foreign_Active;
    private DbsField pnd_Cnt_Recs_Upd;
    private DbsField pnd_Cnt_Et;
    private DbsField pnd_Cnt_Tot_Recs_Read;
    private DbsField pnd_Override;
    private DbsField pnd_Us_Possession;
    private DbsField pnd_Treaty_Rate;
    private DbsField pnd_Error;
    private DbsField pnd_Level_Ok;
    private DbsField pnd_Upd_Ind;
    private DbsField pnd_Hold_Isn;
    private DbsField pnd_Hold_Res_Cde;
    private DbsField pnd_Hold_Res_Type;
    private DbsField pnd_Hold_Loc_Cde;
    private DbsField pnd_Hold_W8_Ben_Country;
    private DbsField pnd_Key;

    private DbsGroup pnd_Key__R_Field_1;
    private DbsField pnd_Key_Tin;
    private DbsField pnd_Key_Tin_Type;
    private DbsField pnd_Key_Active_Ind;
    private DbsField pnd_Date_Temp;
    private DbsField pnd_Country_Temp;
    private DbsField pnd_Res_Comp;
    private DbsField pnd_Res_Temp;

    private DbsGroup pnd_Res_Temp__R_Field_2;
    private DbsField pnd_Res_Temp_Pnd_Res_Temp_Numeric;
    private DbsField pnd_Dt_Tm_A;
    private DbsField pnd_Rc;
    private DbsField pnd_I2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaTxwa2410 = new PdaTxwa2410(localVariables);
        pdaMdma101 = new PdaMdma101(localVariables);
        ldaTxwl2425 = new LdaTxwl2425();
        registerRecord(ldaTxwl2425);
        registerRecord(ldaTxwl2425.getVw_form_Receipt_View());

        // Local Variables

        pnd_Txwp5060 = localVariables.newGroupInRecord("pnd_Txwp5060", "#TXWP5060");
        pnd_Txwp5060_Pnd_Debug_Ind = pnd_Txwp5060.newFieldInGroup("pnd_Txwp5060_Pnd_Debug_Ind", "#DEBUG-IND", FieldType.BOOLEAN, 1);
        pnd_Txwp5060_Pnd_Abend_Ind = pnd_Txwp5060.newFieldInGroup("pnd_Txwp5060_Pnd_Abend_Ind", "#ABEND-IND", FieldType.BOOLEAN, 1);
        pnd_Txwp5060_Pnd_Display_Ind = pnd_Txwp5060.newFieldInGroup("pnd_Txwp5060_Pnd_Display_Ind", "#DISPLAY-IND", FieldType.BOOLEAN, 1);
        pnd_Txwp5060_Pnd_Ret_Msg = pnd_Txwp5060.newFieldInGroup("pnd_Txwp5060_Pnd_Ret_Msg", "#RET-MSG", FieldType.STRING, 70);
        pnd_Cnt_Foreign_Active = localVariables.newFieldInRecord("pnd_Cnt_Foreign_Active", "#CNT-FOREIGN-ACTIVE", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Recs_Upd = localVariables.newFieldInRecord("pnd_Cnt_Recs_Upd", "#CNT-RECS-UPD", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Et = localVariables.newFieldInRecord("pnd_Cnt_Et", "#CNT-ET", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Tot_Recs_Read = localVariables.newFieldInRecord("pnd_Cnt_Tot_Recs_Read", "#CNT-TOT-RECS-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Override = localVariables.newFieldInRecord("pnd_Override", "#OVERRIDE", FieldType.BOOLEAN, 1);
        pnd_Us_Possession = localVariables.newFieldInRecord("pnd_Us_Possession", "#US-POSSESSION", FieldType.BOOLEAN, 1);
        pnd_Treaty_Rate = localVariables.newFieldInRecord("pnd_Treaty_Rate", "#TREATY-RATE", FieldType.BOOLEAN, 1);
        pnd_Error = localVariables.newFieldInRecord("pnd_Error", "#ERROR", FieldType.BOOLEAN, 1);
        pnd_Level_Ok = localVariables.newFieldInRecord("pnd_Level_Ok", "#LEVEL-OK", FieldType.BOOLEAN, 1);
        pnd_Upd_Ind = localVariables.newFieldInRecord("pnd_Upd_Ind", "#UPD-IND", FieldType.BOOLEAN, 1);
        pnd_Hold_Isn = localVariables.newFieldInRecord("pnd_Hold_Isn", "#HOLD-ISN", FieldType.PACKED_DECIMAL, 12);
        pnd_Hold_Res_Cde = localVariables.newFieldInRecord("pnd_Hold_Res_Cde", "#HOLD-RES-CDE", FieldType.STRING, 3);
        pnd_Hold_Res_Type = localVariables.newFieldInRecord("pnd_Hold_Res_Type", "#HOLD-RES-TYPE", FieldType.STRING, 1);
        pnd_Hold_Loc_Cde = localVariables.newFieldInRecord("pnd_Hold_Loc_Cde", "#HOLD-LOC-CDE", FieldType.STRING, 3);
        pnd_Hold_W8_Ben_Country = localVariables.newFieldInRecord("pnd_Hold_W8_Ben_Country", "#HOLD-W8-BEN-COUNTRY", FieldType.STRING, 3);
        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 12);

        pnd_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Key__R_Field_1", "REDEFINE", pnd_Key);
        pnd_Key_Tin = pnd_Key__R_Field_1.newFieldInGroup("pnd_Key_Tin", "TIN", FieldType.STRING, 10);
        pnd_Key_Tin_Type = pnd_Key__R_Field_1.newFieldInGroup("pnd_Key_Tin_Type", "TIN-TYPE", FieldType.STRING, 1);
        pnd_Key_Active_Ind = pnd_Key__R_Field_1.newFieldInGroup("pnd_Key_Active_Ind", "ACTIVE-IND", FieldType.STRING, 1);
        pnd_Date_Temp = localVariables.newFieldInRecord("pnd_Date_Temp", "#DATE-TEMP", FieldType.STRING, 8);
        pnd_Country_Temp = localVariables.newFieldInRecord("pnd_Country_Temp", "#COUNTRY-TEMP", FieldType.STRING, 3);
        pnd_Res_Comp = localVariables.newFieldInRecord("pnd_Res_Comp", "#RES-COMP", FieldType.STRING, 3);
        pnd_Res_Temp = localVariables.newFieldInRecord("pnd_Res_Temp", "#RES-TEMP", FieldType.STRING, 3);

        pnd_Res_Temp__R_Field_2 = localVariables.newGroupInRecord("pnd_Res_Temp__R_Field_2", "REDEFINE", pnd_Res_Temp);
        pnd_Res_Temp_Pnd_Res_Temp_Numeric = pnd_Res_Temp__R_Field_2.newFieldInGroup("pnd_Res_Temp_Pnd_Res_Temp_Numeric", "#RES-TEMP-NUMERIC", FieldType.NUMERIC, 
            3);
        pnd_Dt_Tm_A = localVariables.newFieldInRecord("pnd_Dt_Tm_A", "#DT-TM-A", FieldType.STRING, 15);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTxwl2425.initializeValues();

        localVariables.reset();
        pnd_Txwp5060_Pnd_Debug_Ind.setInitialValue(false);
        pnd_Txwp5060_Pnd_Abend_Ind.setInitialValue(false);
        pnd_Txwp5060_Pnd_Display_Ind.setInitialValue(false);
        pnd_Override.setInitialValue(false);
        pnd_Us_Possession.setInitialValue(false);
        pnd_Treaty_Rate.setInitialValue(false);
        pnd_Error.setInitialValue(false);
        pnd_Level_Ok.setInitialValue(false);
        pnd_Upd_Ind.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Txwp5060() throws Exception
    {
        super("Txwp5060", true);
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* ********************************************************************
        //*  M A I N   P R O G R A M
        //* ********************************************************************
        //*  FE201506
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
        sub_Open_Mq();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean()))                                                                                                           //Natural: IF #TXWP5060.#DEBUG-IND
        {
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"Begin Subprogram Processing");                                                                //Natural: WRITE *PROGRAM 'Begin Subprogram Processing'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        setKeys(ControlKeys.PF5, "%W<80", (String)null);                                                                                                                  //Natural: FORMAT ( 1 ) PS = 56 LS = 132;//Natural: SET KEY PF5 = '%W<80' PF6 = '%W>80'
        setKeys(ControlKeys.PF6, "%W>80", (String)null);
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *TIMX ( EM = MM/DD/YYYY�HH:IIAP ) 95T 'Page  :' *PAGE-NUMBER ( 1 ) ( EM = Z,ZZ9 ) / *PROGRAM 22T 'W8 BEN OVERRIDE UPDATES - FLAGS SET TO "Y"' 95T 'Report:  RPT1' //
                                                                                                                                                                          //Natural: PERFORM READ-PARTICANT-RECORDS
        sub_Read_Particant_Records();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean()))                                                                                                           //Natural: IF #TXWP5060.#DEBUG-IND
        {
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"End Subprogram Processing");                                                                  //Natural: WRITE *PROGRAM 'End Subprogram Processing'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* **********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-PARTICANT-RECORDS
        //* **********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OVERRIDE-OR-NOT
        //* **********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-W8-OVERRIDE
        //* **********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LGO-PROCESS
        //* **********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESIDENCY-NAAD
        //* **********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESIDENCY-TAXWARS
        //* **********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NON-LGO-PROCESS
        //* **********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CNTRY-US-POSSESSION-CHECK
        //* **********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: W8-PROCESS
        //* **********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-PARTICIPANT-RECORD
        //*  'LU/USER'             FORM-RECEIPT-VIEW.LU-USER
        //*  '/LU-TS'              FORM-RECEIPT-VIEW.LU-TS (AL=8)
        //* **********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-CONDITION
        //*  **************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-MQ
        //*  FE201506 END
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
    }
    private void sub_Read_Particant_Records() throws Exception                                                                                                            //Natural: READ-PARTICANT-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************
        if (condition(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean()))                                                                                                           //Natural: IF #TXWP5060.#DEBUG-IND
        {
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"RETRIEVE-PARTICANT-RECORD <<< Subroutine");                                                   //Natural: WRITE *PROGRAM 'RETRIEVE-PARTICANT-RECORD <<< Subroutine'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        ldaTxwl2425.getVw_form_Receipt_View().startDatabaseRead                                                                                                           //Natural: READ FORM-RECEIPT-VIEW BY SUPER-4
        (
        "RD1",
        new Oc[] { new Oc("SUPER_4", "ASC") }
        );
        RD1:
        while (condition(ldaTxwl2425.getVw_form_Receipt_View().readNextRow("RD1")))
        {
            pnd_Cnt_Tot_Recs_Read.nadd(1);                                                                                                                                //Natural: ADD 1 TO #CNT-TOT-RECS-READ
            pnd_Override.resetInitial();                                                                                                                                  //Natural: RESET INITIAL #OVERRIDE
            pnd_Level_Ok.resetInitial();                                                                                                                                  //Natural: RESET INITIAL #LEVEL-OK
            pnd_Error.resetInitial();                                                                                                                                     //Natural: RESET INITIAL #ERROR
            pnd_Hold_Res_Type.reset();                                                                                                                                    //Natural: RESET #HOLD-RES-TYPE #HOLD-RES-CDE #HOLD-LOC-CDE
            pnd_Hold_Res_Cde.reset();
            pnd_Hold_Loc_Cde.reset();
            pnd_Cnt_Foreign_Active.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CNT-FOREIGN-ACTIVE
                                                                                                                                                                          //Natural: PERFORM OVERRIDE-OR-NOT
            sub_Override_Or_Not();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Override.getBoolean() && ldaTxwl2425.getForm_Receipt_View_W8_Ben_Override().notEquals("Y")))                                                //Natural: IF #OVERRIDE AND FORM-RECEIPT-VIEW.W8-BEN-OVERRIDE NE 'Y'
            {
                pnd_Upd_Ind.setValue(true);                                                                                                                               //Natural: ASSIGN #UPD-IND := TRUE
                pnd_Hold_Isn.setValue(ldaTxwl2425.getVw_form_Receipt_View().getAstISN("RD1"));                                                                            //Natural: ASSIGN #HOLD-ISN := *ISN ( RD1. )
                                                                                                                                                                          //Natural: PERFORM UPDATE-W8-OVERRIDE
                sub_Update_W8_Override();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Upd_Ind.setValue(false);                                                                                                                              //Natural: ASSIGN #UPD-IND := FALSE
                //*    PERFORM DISPLAY-PARTICIPANT-RECORD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,NEWLINE,new ColumnSpacing(2),"TOTAL RECORDS READ          :",pnd_Cnt_Tot_Recs_Read,NEWLINE,new ColumnSpacing(2),"TOTAL ACTIVE FOREIGN RECORDS:",pnd_Cnt_Foreign_Active,NEWLINE,new  //Natural: WRITE ( 1 ) // 2X 'TOTAL RECORDS READ          :' #CNT-TOT-RECS-READ / 2X 'TOTAL ACTIVE FOREIGN RECORDS:' #CNT-FOREIGN-ACTIVE / 2X 'TOTAL RECORDS UPDATED:       ' #CNT-RECS-UPD /
            ColumnSpacing(2),"TOTAL RECORDS UPDATED:       ",pnd_Cnt_Recs_Upd,NEWLINE);
        if (Global.isEscape()) return;
        //*  FINAL ET
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Override_Or_Not() throws Exception                                                                                                                   //Natural: OVERRIDE-OR-NOT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************
        if (condition(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean()))                                                                                                           //Natural: IF #TXWP5060.#DEBUG-IND
        {
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"OVERRIDE-OR-NOT           <<< Subroutine");                                                   //Natural: WRITE *PROGRAM 'OVERRIDE-OR-NOT           <<< Subroutine'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTxwl2425.getForm_Receipt_View_Lgo_Res_Type().notEquals(" ")))                                                                                    //Natural: IF FORM-RECEIPT-VIEW.LGO-RES-TYPE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM LGO-PROCESS
            sub_Lgo_Process();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Level_Ok.getBoolean())))                                                                                                                     //Natural: IF NOT #LEVEL-OK
        {
                                                                                                                                                                          //Natural: PERFORM RESIDENCY-NAAD
            sub_Residency_Naad();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Error.getBoolean()))                                                                                                                        //Natural: IF #ERROR
            {
                pnd_Txwp5060_Pnd_Ret_Msg.setValue(" Invalid Residency Cde");                                                                                              //Natural: ASSIGN #TXWP5060.#RET-MSG := ' Invalid Residency Cde'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Level_Ok.getBoolean())))                                                                                                                 //Natural: IF NOT #LEVEL-OK
            {
                                                                                                                                                                          //Natural: PERFORM RESIDENCY-TAXWARS
                sub_Residency_Taxwars();
                if (condition(Global.isEscape())) {return;}
                if (condition(! (pnd_Level_Ok.getBoolean())))                                                                                                             //Natural: IF NOT #LEVEL-OK
                {
                    pnd_Res_Temp.setValue(ldaTxwl2425.getForm_Receipt_View_Res_Cde());                                                                                    //Natural: ASSIGN #RES-TEMP := FORM-RECEIPT-VIEW.RES-CDE
                                                                                                                                                                          //Natural: PERFORM NON-LGO-PROCESS
                    sub_Non_Lgo_Process();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(pnd_Error.getBoolean()))                                                                                                                //Natural: IF #ERROR
                    {
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM W8-PROCESS
        sub_W8_Process();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Update_W8_Override() throws Exception                                                                                                                //Natural: UPDATE-W8-OVERRIDE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************
        if (condition(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean()))                                                                                                           //Natural: IF #TXWP5060.#DEBUG-IND
        {
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"UPDATE-W8-OVERRIDE        <<< Subroutine");                                                   //Natural: WRITE *PROGRAM 'UPDATE-W8-OVERRIDE        <<< Subroutine'
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"ISN OF UPDATED RECORD",pnd_Hold_Isn);                                                         //Natural: WRITE *PROGRAM 'ISN OF UPDATED RECORD' #HOLD-ISN
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        GT1:                                                                                                                                                              //Natural: GET FORM-RECEIPT-VIEW #HOLD-ISN
        ldaTxwl2425.getVw_form_Receipt_View().readByID(pnd_Hold_Isn.getLong(), "GT1");
        ldaTxwl2425.getForm_Receipt_View_Lu_User().setValue(Global.getINIT_USER());                                                                                       //Natural: ASSIGN FORM-RECEIPT-VIEW.LU-USER := *INIT-USER
        pnd_Dt_Tm_A.setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                                                               //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO #DT-TM-A
        ldaTxwl2425.getForm_Receipt_View_Lu_Ts().setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Dt_Tm_A);                                                       //Natural: MOVE EDITED #DT-TM-A TO FORM-RECEIPT-VIEW.LU-TS ( EM = YYYYMMDDHHIISST )
        ldaTxwl2425.getForm_Receipt_View_Active_Ind().setValue(" ");                                                                                                      //Natural: ASSIGN FORM-RECEIPT-VIEW.ACTIVE-IND := ' '
        ldaTxwl2425.getVw_form_Receipt_View().updateDBRow("GT1");                                                                                                         //Natural: UPDATE ( GT1. )
        ldaTxwl2425.getForm_Receipt_View_Active_Ind().setValue("A");                                                                                                      //Natural: ASSIGN FORM-RECEIPT-VIEW.ACTIVE-IND := 'A'
        ldaTxwl2425.getForm_Receipt_View_Create_Ts().setValue(ldaTxwl2425.getForm_Receipt_View_Lu_Ts());                                                                  //Natural: ASSIGN FORM-RECEIPT-VIEW.CREATE-TS := FORM-RECEIPT-VIEW.LU-TS
        ldaTxwl2425.getForm_Receipt_View_Invrse_Create_Ts().compute(new ComputeParameters(false, ldaTxwl2425.getForm_Receipt_View_Invrse_Create_Ts()),                    //Natural: ASSIGN FORM-RECEIPT-VIEW.INVRSE-CREATE-TS := 1000000000000 - *TIMX
            new DbsDecimal("1000000000000").subtract(Global.getTIMX()));
        ldaTxwl2425.getForm_Receipt_View_W8_Ben_Override().setValue("Y");                                                                                                 //Natural: ASSIGN FORM-RECEIPT-VIEW.W8-BEN-OVERRIDE := 'Y'
        ST1:                                                                                                                                                              //Natural: STORE FORM-RECEIPT-VIEW
        ldaTxwl2425.getVw_form_Receipt_View().insertDBRow("ST1");
        pnd_Cnt_Recs_Upd.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNT-RECS-UPD
        pnd_Cnt_Et.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #CNT-ET
        if (condition(pnd_Cnt_Et.greater(50)))                                                                                                                            //Natural: IF #CNT-ET > 50
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Cnt_Et.reset();                                                                                                                                           //Natural: RESET #CNT-ET
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DISPLAY-PARTICIPANT-RECORD
        sub_Display_Participant_Record();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Lgo_Process() throws Exception                                                                                                                       //Natural: LGO-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************
        if (condition(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean()))                                                                                                           //Natural: IF #TXWP5060.#DEBUG-IND
        {
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"LGO-PROCESS               <<< Subroutine");                                                   //Natural: WRITE *PROGRAM 'LGO-PROCESS               <<< Subroutine'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(Global.getDATX().greaterOrEqual(ldaTxwl2425.getForm_Receipt_View_Lgo_Eff_Dte()) && (Global.getDATX().lessOrEqual(ldaTxwl2425.getForm_Receipt_View_Lgo_Exp_Dte())  //Natural: IF *DATX >= FORM-RECEIPT-VIEW.LGO-EFF-DTE AND ( *DATX <= FORM-RECEIPT-VIEW.LGO-EXP-DTE OR FORM-RECEIPT-VIEW.LGO-EXP-DTE = 0 )
            || ldaTxwl2425.getForm_Receipt_View_Lgo_Exp_Dte().equals(getZero()))))
        {
            pnd_Hold_Res_Cde.setValue(ldaTxwl2425.getForm_Receipt_View_Lgo_Res_Cde());                                                                                    //Natural: ASSIGN #HOLD-RES-CDE := FORM-RECEIPT-VIEW.LGO-RES-CDE
            pnd_Hold_Res_Type.setValue(ldaTxwl2425.getForm_Receipt_View_Lgo_Res_Type());                                                                                  //Natural: ASSIGN #HOLD-RES-TYPE := FORM-RECEIPT-VIEW.LGO-RES-TYPE
            pnd_Hold_Loc_Cde.setValue(ldaTxwl2425.getForm_Receipt_View_Lgo_Loc_Cde());                                                                                    //Natural: ASSIGN #HOLD-LOC-CDE := FORM-RECEIPT-VIEW.LGO-LOC-CDE
            pnd_Level_Ok.setValue(true);                                                                                                                                  //Natural: ASSIGN #LEVEL-OK := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTxwl2425.getForm_Receipt_View_Lgo_Res_Type().equals("2")))                                                                                       //Natural: IF FORM-RECEIPT-VIEW.LGO-RES-TYPE = '2'
        {
            if (condition(ldaTxwl2425.getForm_Receipt_View_Lgo_Res_Cde().equals("42")))                                                                                   //Natural: IF FORM-RECEIPT-VIEW.LGO-RES-CDE = '42'
            {
                pnd_Us_Possession.setValue(true);                                                                                                                         //Natural: ASSIGN #US-POSSESSION := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Country_Temp.setValue(pnd_Hold_Res_Cde);                                                                                                              //Natural: ASSIGN #COUNTRY-TEMP := #HOLD-RES-CDE
                                                                                                                                                                          //Natural: PERFORM CNTRY-US-POSSESSION-CHECK
                sub_Cntry_Us_Possession_Check();
                if (condition(Global.isEscape())) {return;}
                if (condition(pdaTxwa2410.getTxwa2410_Pnd_Ret_Code().getBoolean()))                                                                                       //Natural: IF TXWA2410.#RET-CODE
                {
                    pnd_Us_Possession.setValue(pdaTxwa2410.getTxwa2410_Pnd_Us_Poss_Ind().getBoolean());                                                                   //Natural: ASSIGN #US-POSSESSION := TXWA2410.#US-POSS-IND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  TWRNPAR2
    private void sub_Residency_Naad() throws Exception                                                                                                                    //Natural: RESIDENCY-NAAD
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************
        if (condition(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean()))                                                                                                           //Natural: IF #TXWP5060.#DEBUG-IND
        {
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"RESIDENCY-NAAD            <<< Subroutine");                                                   //Natural: WRITE *PROGRAM 'RESIDENCY-NAAD            <<< Subroutine'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  #TWRAPAR2.#ABEND-IND   := #TXWP5060.#ABEND-IND /* FE201506 COMMENT ST
        //*  #TWRAPAR2.#DEBUG-IND   := #TXWP5060.#DEBUG-IND
        //*  #TWRAPAR2.#DISPLAY-IND := #TXWP5060.#DISPLAY-IND
        //*  #TWRAPAR2.#TIN   := FORM-RECEIPT-VIEW.TIN /* FE201506 COMMENT END
        //*  FE201506 START
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        pdaMdma101.getPnd_Mdma101_Pnd_I_Ssn_A9().setValue(ldaTxwl2425.getForm_Receipt_View_Tin());                                                                        //Natural: ASSIGN #MDMA101.#I-SSN-A9 := FORM-RECEIPT-VIEW.TIN
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' USING #MDMA101
        if (condition(Global.isEscape())) return;
        //*   #TWRAPAR2
        //*   #TWRACOR
        //*   #TWRACNAD
        if (condition(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean()))                                                                                                           //Natural: IF #TXWP5060.#DEBUG-IND
        {
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"RETURN CODE FROM MDMN101  IS -",pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code());               //Natural: WRITE *PROGRAM 'RETURN CODE FROM MDMN101  IS -' #MDMA101.#O-RETURN-CODE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA101.#O-RETURN-CODE EQ '0000'
        {
            if (condition(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean()))                                                                                                       //Natural: IF #TXWP5060.#DEBUG-IND
            {
                getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"COR LOOKUP RESULTS:","=",pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code());                  //Natural: WRITE *PROGRAM 'COR LOOKUP RESULTS:' '=' #MDMA101.#O-RETURN-CODE
                if (Global.isEscape()) return;
                getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"COR Lookup Results:","=",pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code()); //Natural: WRITE *PROGRAM 'COR Lookup Results:' '=' #MDMA101.#O-BASE-ADDRESS-GEOGRAPHIC-CODE
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().equals(" ") || pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().equals("00")  //Natural: IF #MDMA101.#O-BASE-ADDRESS-GEOGRAPHIC-CODE = ' ' OR = '00' OR = '0 ' OR = '97' OR = '98' OR = '99'
                || pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().equals("0 ") || pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().equals("97") 
                || pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().equals("98") || pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().equals("99")))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Res_Temp.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code());                                                                    //Natural: ASSIGN #RES-TEMP := #MDMA101.#O-BASE-ADDRESS-GEOGRAPHIC-CODE
                                                                                                                                                                          //Natural: PERFORM NON-LGO-PROCESS
                sub_Non_Lgo_Process();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Error.getBoolean()))                                                                                                                    //Natural: IF #ERROR
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Level_Ok.setValue(true);                                                                                                                              //Natural: ASSIGN #LEVEL-OK := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean()))                                                                                                       //Natural: IF #TXWP5060.#DEBUG-IND
            {
                getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM()," Warning: US Possession Lookup Failed");                                                  //Natural: WRITE *PROGRAM ' Warning: US Possession Lookup Failed'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Residency_Taxwars() throws Exception                                                                                                                 //Natural: RESIDENCY-TAXWARS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************
        if (condition(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean()))                                                                                                           //Natural: IF #TXWP5060.#DEBUG-IND
        {
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"RESIDENCY-TAXWARS         <<< Subroutine");                                                   //Natural: WRITE *PROGRAM 'RESIDENCY-TAXWARS         <<< Subroutine'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Hold_Res_Cde.setValue(ldaTxwl2425.getForm_Receipt_View_Res_Cde());                                                                                            //Natural: ASSIGN #HOLD-RES-CDE := FORM-RECEIPT-VIEW.RES-CDE
        pnd_Hold_Res_Type.setValue(ldaTxwl2425.getForm_Receipt_View_Res_Type());                                                                                          //Natural: ASSIGN #HOLD-RES-TYPE := FORM-RECEIPT-VIEW.RES-TYPE
        pnd_Hold_Loc_Cde.setValue(ldaTxwl2425.getForm_Receipt_View_Loc_Cde());                                                                                            //Natural: ASSIGN #HOLD-LOC-CDE := FORM-RECEIPT-VIEW.LOC-CDE
        if (condition(ldaTxwl2425.getForm_Receipt_View_Res_Cde().equals("42")))                                                                                           //Natural: IF FORM-RECEIPT-VIEW.RES-CDE = '42'
        {
            if (condition(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean()))                                                                                                       //Natural: IF #TXWP5060.#DEBUG-IND
            {
                getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"RES-CDE = 42 (RESIDENCY-TAXWARS)");                                                       //Natural: WRITE *PROGRAM 'RES-CDE = 42 (RESIDENCY-TAXWARS)'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_Us_Possession.setValue(true);                                                                                                                             //Natural: ASSIGN #US-POSSESSION := TRUE
            pnd_Level_Ok.setValue(true);                                                                                                                                  //Natural: ASSIGN #LEVEL-OK := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(! (DbsUtil.is(ldaTxwl2425.getForm_Receipt_View_Res_Cde().getText(),"N3"))))                                                                     //Natural: IF NOT ( FORM-RECEIPT-VIEW.RES-CDE IS ( N3 ) )
            {
                if (condition(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean()))                                                                                                   //Natural: IF #TXWP5060.#DEBUG-IND
                {
                    getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"RES-CDE is alpha (RESIDENCY-TAXWARS)");                                               //Natural: WRITE *PROGRAM 'RES-CDE is alpha (RESIDENCY-TAXWARS)'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                pnd_Country_Temp.setValue(pnd_Hold_Res_Cde);                                                                                                              //Natural: ASSIGN #COUNTRY-TEMP := #HOLD-RES-CDE
                                                                                                                                                                          //Natural: PERFORM CNTRY-US-POSSESSION-CHECK
                sub_Cntry_Us_Possession_Check();
                if (condition(Global.isEscape())) {return;}
                if (condition(pdaTxwa2410.getTxwa2410_Pnd_Ret_Code().getBoolean()))                                                                                       //Natural: IF TXWA2410.#RET-CODE
                {
                    pnd_Us_Possession.setValue(pdaTxwa2410.getTxwa2410_Pnd_Us_Poss_Ind().getBoolean());                                                                   //Natural: ASSIGN #US-POSSESSION := TXWA2410.#US-POSS-IND
                    pnd_Level_Ok.setValue(true);                                                                                                                          //Natural: ASSIGN #LEVEL-OK := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Non_Lgo_Process() throws Exception                                                                                                                   //Natural: NON-LGO-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************
        if (condition(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean()))                                                                                                           //Natural: IF #TXWP5060.#DEBUG-IND
        {
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"NON-LGO-PROCESS           <<< Subroutine");                                                   //Natural: WRITE *PROGRAM 'NON-LGO-PROCESS           <<< Subroutine'
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"RES-CDE/GEO used in NON-LGO-PROCESS:",pnd_Res_Temp);                                          //Natural: WRITE *PROGRAM 'RES-CDE/GEO used in NON-LGO-PROCESS:' #RES-TEMP
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.is(pnd_Res_Temp.getText(),"N3")))                                                                                                           //Natural: IF #RES-TEMP IS ( N3 )
        {
            pnd_Res_Temp.setValue(pnd_Res_Temp, MoveOption.RightJustified);                                                                                               //Natural: MOVE RIGHT JUSTIFIED #RES-TEMP TO #RES-TEMP
            pnd_Res_Comp.setValue(pnd_Res_Temp, MoveOption.LeftJustified);                                                                                                //Natural: MOVE LEFT JUSTIFIED #RES-TEMP TO #RES-COMP
            if (condition(ldaTxwl2425.getForm_Receipt_View_Res_Type().notEquals(" ") && ldaTxwl2425.getForm_Receipt_View_Res_Cde().notEquals(" ")))                       //Natural: IF FORM-RECEIPT-VIEW.RES-TYPE NE ' ' AND FORM-RECEIPT-VIEW.RES-CDE NE ' '
            {
                pnd_Hold_Res_Cde.setValue(ldaTxwl2425.getForm_Receipt_View_Res_Cde());                                                                                    //Natural: ASSIGN #HOLD-RES-CDE := FORM-RECEIPT-VIEW.RES-CDE
                pnd_Hold_Res_Type.setValue(ldaTxwl2425.getForm_Receipt_View_Res_Type());                                                                                  //Natural: ASSIGN #HOLD-RES-TYPE := FORM-RECEIPT-VIEW.RES-TYPE
                pnd_Hold_Loc_Cde.setValue(ldaTxwl2425.getForm_Receipt_View_Loc_Cde());                                                                                    //Natural: ASSIGN #HOLD-LOC-CDE := FORM-RECEIPT-VIEW.LOC-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                short decideConditionsMet572 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #RES-TEMP-NUMERIC = 42
                if (condition(pnd_Res_Temp_Pnd_Res_Temp_Numeric.equals(42)))
                {
                    decideConditionsMet572++;
                    pnd_Hold_Res_Type.setValue("3");                                                                                                                      //Natural: ASSIGN #HOLD-RES-TYPE := '3'
                }                                                                                                                                                         //Natural: WHEN #RES-TEMP-NUMERIC = 01 THRU 57
                else if (condition(pnd_Res_Temp_Pnd_Res_Temp_Numeric.greaterOrEqual(1) && pnd_Res_Temp_Pnd_Res_Temp_Numeric.lessOrEqual(57)))
                {
                    decideConditionsMet572++;
                    pnd_Hold_Res_Type.setValue("1");                                                                                                                      //Natural: ASSIGN #HOLD-RES-TYPE := '1'
                    pnd_Hold_Loc_Cde.setValue(ldaTxwl2425.getForm_Receipt_View_Loc_Cde());                                                                                //Natural: ASSIGN #HOLD-LOC-CDE := FORM-RECEIPT-VIEW.LOC-CDE
                }                                                                                                                                                         //Natural: WHEN #RES-TEMP-NUMERIC = 74 THRU 88
                else if (condition(pnd_Res_Temp_Pnd_Res_Temp_Numeric.greaterOrEqual(74) && pnd_Res_Temp_Pnd_Res_Temp_Numeric.lessOrEqual(88)))
                {
                    decideConditionsMet572++;
                    pnd_Hold_Res_Type.setValue("5");                                                                                                                      //Natural: ASSIGN #HOLD-RES-TYPE := '5'
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet572 > 0))
                {
                    pnd_Hold_Res_Cde.setValue(pnd_Res_Temp);                                                                                                              //Natural: ASSIGN #HOLD-RES-CDE := #RES-TEMP
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Error.setValue(true);                                                                                                                             //Natural: ASSIGN #ERROR := TRUE
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Res_Temp_Pnd_Res_Temp_Numeric.equals(42)))                                                                                                  //Natural: IF #RES-TEMP-NUMERIC = 42
            {
                pnd_Us_Possession.setValue(true);                                                                                                                         //Natural: ASSIGN #US-POSSESSION := TRUE
            }                                                                                                                                                             //Natural: END-IF
            //*  I.E., #RES-TEMP IS ALPHA
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Hold_Res_Cde.setValue(pnd_Res_Temp);                                                                                                                      //Natural: ASSIGN #HOLD-RES-CDE := #RES-TEMP
            pnd_Country_Temp.setValue(pnd_Res_Temp);                                                                                                                      //Natural: ASSIGN #COUNTRY-TEMP := #RES-TEMP
            if (condition(ldaTxwl2425.getForm_Receipt_View_Res_Type().notEquals(" ") && ldaTxwl2425.getForm_Receipt_View_Res_Cde().notEquals(" ")))                       //Natural: IF FORM-RECEIPT-VIEW.RES-TYPE NE ' ' AND FORM-RECEIPT-VIEW.RES-CDE NE ' '
            {
                pnd_Country_Temp.setValue(ldaTxwl2425.getForm_Receipt_View_Res_Cde());                                                                                    //Natural: ASSIGN #COUNTRY-TEMP := FORM-RECEIPT-VIEW.RES-CDE
                pnd_Hold_Res_Cde.setValue(ldaTxwl2425.getForm_Receipt_View_Res_Cde());                                                                                    //Natural: ASSIGN #HOLD-RES-CDE := FORM-RECEIPT-VIEW.RES-CDE
                pnd_Hold_Res_Type.setValue(ldaTxwl2425.getForm_Receipt_View_Res_Type());                                                                                  //Natural: ASSIGN #HOLD-RES-TYPE := FORM-RECEIPT-VIEW.RES-TYPE
                pnd_Hold_Loc_Cde.setValue(ldaTxwl2425.getForm_Receipt_View_Loc_Cde());                                                                                    //Natural: ASSIGN #HOLD-LOC-CDE := FORM-RECEIPT-VIEW.LOC-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                short decideConditionsMet600 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #RES-TEMP = 'CA'
                if (condition(pnd_Res_Temp.equals("CA")))
                {
                    decideConditionsMet600++;
                    pnd_Hold_Res_Type.setValue("4");                                                                                                                      //Natural: ASSIGN #HOLD-RES-TYPE := '4'
                }                                                                                                                                                         //Natural: WHEN #RES-TEMP = 'RQ'
                else if (condition(pnd_Res_Temp.equals("RQ")))
                {
                    decideConditionsMet600++;
                    pnd_Hold_Res_Type.setValue("3");                                                                                                                      //Natural: ASSIGN #HOLD-RES-TYPE := '3'
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    if (condition(pnd_Res_Temp.notEquals(" ")))                                                                                                           //Natural: IF #RES-TEMP NE ' '
                    {
                        pnd_Hold_Res_Type.setValue("2");                                                                                                                  //Natural: ASSIGN #HOLD-RES-TYPE := '2'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CNTRY-US-POSSESSION-CHECK
            sub_Cntry_Us_Possession_Check();
            if (condition(Global.isEscape())) {return;}
            if (condition(pdaTxwa2410.getTxwa2410_Pnd_Ret_Code().getBoolean()))                                                                                           //Natural: IF TXWA2410.#RET-CODE
            {
                pnd_Us_Possession.setValue(pdaTxwa2410.getTxwa2410_Pnd_Us_Poss_Ind().getBoolean());                                                                       //Natural: ASSIGN #US-POSSESSION := TXWA2410.#US-POSS-IND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  TXWN2410
    private void sub_Cntry_Us_Possession_Check() throws Exception                                                                                                         //Natural: CNTRY-US-POSSESSION-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************
        if (condition(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean()))                                                                                                           //Natural: IF #TXWP5060.#DEBUG-IND
        {
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"CNTRY-US-POSSESSION-CHECK <<< Subroutine");                                                   //Natural: WRITE *PROGRAM 'CNTRY-US-POSSESSION-CHECK <<< Subroutine'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaTxwa2410.getTxwa2410_Pnd_Function().setValue("1");                                                                                                             //Natural: ASSIGN TXWA2410.#FUNCTION := '1'
        pdaTxwa2410.getTxwa2410_Pnd_Abend_Ind().setValue(pnd_Txwp5060_Pnd_Abend_Ind.getBoolean());                                                                        //Natural: ASSIGN TXWA2410.#ABEND-IND := #TXWP5060.#ABEND-IND
        pdaTxwa2410.getTxwa2410_Pnd_Debug_Ind().setValue(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean());                                                                        //Natural: ASSIGN TXWA2410.#DEBUG-IND := #TXWP5060.#DEBUG-IND
        pdaTxwa2410.getTxwa2410_Pnd_Display_Ind().setValue(pnd_Txwp5060_Pnd_Display_Ind.getBoolean());                                                                    //Natural: ASSIGN TXWA2410.#DISPLAY-IND := #TXWP5060.#DISPLAY-IND
        pdaTxwa2410.getTxwa2410_Pnd_Country_Cde().setValue(pnd_Country_Temp);                                                                                             //Natural: ASSIGN TXWA2410.#COUNTRY-CDE := #COUNTRY-TEMP
        pdaTxwa2410.getTxwa2410_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTxwa2410.getTxwa2410_Pnd_Tax_Year()), Global.getDATN().divide(10000));             //Natural: ASSIGN TXWA2410.#TAX-YEAR := *DATN / 10000
        if (condition(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean()))                                                                                                           //Natural: IF #TXWP5060.#DEBUG-IND
        {
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"=",pdaTxwa2410.getTxwa2410_Pnd_Country_Cde());                                                //Natural: WRITE *PROGRAM '=' TXWA2410.#COUNTRY-CDE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Txwn2410.class , getCurrentProcessState(), pdaTxwa2410.getTxwa2410_Input_Parms(), pdaTxwa2410.getTxwa2410_Output_Data());                         //Natural: CALLNAT 'TXWN2410' USING TXWA2410.INPUT-PARMS TXWA2410.OUTPUT-DATA
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean()))                                                                                                           //Natural: IF #TXWP5060.#DEBUG-IND
        {
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),": Return Code from TXWN2410 is -",pdaTxwa2410.getTxwa2410_Pnd_Ret_Code());                    //Natural: WRITE *PROGRAM ': Return Code from TXWN2410 is -' TXWA2410.#RET-CODE
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"=",pdaTxwa2410.getTxwa2410_Pnd_Us_Poss_Ind());                                                //Natural: WRITE *PROGRAM '=' TXWA2410.#US-POSS-IND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaTxwa2410.getTxwa2410_Pnd_Ret_Code().getBoolean()))                                                                                               //Natural: IF TXWA2410.#RET-CODE
        {
            pnd_Us_Possession.setValue(pdaTxwa2410.getTxwa2410_Pnd_Us_Poss_Ind().getBoolean());                                                                           //Natural: ASSIGN #US-POSSESSION := TXWA2410.#US-POSS-IND
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean()))                                                                                                       //Natural: IF #TXWP5060.#DEBUG-IND
            {
                getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM()," Warning: Invalid Country Code ");                                                        //Natural: WRITE *PROGRAM ' Warning: Invalid Country Code '
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_W8_Process() throws Exception                                                                                                                        //Natural: W8-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************
        if (condition(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean()))                                                                                                           //Natural: IF #TXWP5060.#DEBUG-IND
        {
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"W8-PROCESS                <<< Subroutine");                                                   //Natural: WRITE *PROGRAM 'W8-PROCESS                <<< Subroutine'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Country_Temp.reset();                                                                                                                                         //Natural: RESET #COUNTRY-TEMP
        pnd_Treaty_Rate.resetInitial();                                                                                                                                   //Natural: RESET INITIAL #TREATY-RATE
        pnd_Hold_W8_Ben_Country.setValue(ldaTxwl2425.getForm_Receipt_View_W8_Ben_Country());                                                                              //Natural: ASSIGN #HOLD-W8-BEN-COUNTRY := FORM-RECEIPT-VIEW.W8-BEN-COUNTRY
        pnd_Hold_Res_Cde.setValue(pnd_Hold_Res_Cde, MoveOption.LeftJustified);                                                                                            //Natural: MOVE LEFT #HOLD-RES-CDE TO #HOLD-RES-CDE
        //*  NO UPDATE
        short decideConditionsMet659 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN FORM-RECEIPT-VIEW.W8-BEN-OVERRIDE = 'Y'
        if (condition(ldaTxwl2425.getForm_Receipt_View_W8_Ben_Override().equals("Y")))
        {
            decideConditionsMet659++;
            //*  NO UPDATE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: WHEN NOT ( FORM-RECEIPT-VIEW.W8-BEN-PERM-RES = 'Y' AND FORM-RECEIPT-VIEW.W8-BEN-TIN = 'Y' AND FORM-RECEIPT-VIEW.W8-BEN-TREATY = 'Y' AND FORM-RECEIPT-VIEW.W8-BEN-SIGNATURE = 'Y' AND FORM-RECEIPT-VIEW.W8-BEN-SIGNED-DATE > 0 )
        else if (condition(! ((ldaTxwl2425.getForm_Receipt_View_W8_Ben_Perm_Res().equals("Y") && ldaTxwl2425.getForm_Receipt_View_W8_Ben_Tin().equals("Y") 
            && ldaTxwl2425.getForm_Receipt_View_W8_Ben_Treaty().equals("Y") && ldaTxwl2425.getForm_Receipt_View_W8_Ben_Signature().equals("Y") && ldaTxwl2425.getForm_Receipt_View_W8_Ben_Signed_Date().greater(getZero())))))
        {
            decideConditionsMet659++;
            //*  NO UPDATE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: WHEN #HOLD-RES-CDE = #HOLD-W8-BEN-COUNTRY
        else if (condition(pnd_Hold_Res_Cde.equals(pnd_Hold_W8_Ben_Country)))
        {
            decideConditionsMet659++;
            //*  CHECK TREATY
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: WHEN #HOLD-RES-CDE = MASK ( AAN ) AND #HOLD-W8-BEN-COUNTRY = MASK ( AAN )
        else if (condition(DbsUtil.maskMatches(pnd_Hold_Res_Cde,"AAN") && DbsUtil.maskMatches(pnd_Hold_W8_Ben_Country,"AAN")))
        {
            decideConditionsMet659++;
            pnd_Country_Temp.setValue(pnd_Hold_W8_Ben_Country);                                                                                                           //Natural: ASSIGN #COUNTRY-TEMP := #HOLD-W8-BEN-COUNTRY
        }                                                                                                                                                                 //Natural: WHEN ( #HOLD-RES-CDE = MASK ( AAN ) AND #HOLD-W8-BEN-COUNTRY = MASK ( AA' ' ) ) OR ( #HOLD-RES-CDE = MASK ( AA' ' ) AND #HOLD-W8-BEN-COUNTRY = MASK ( AAN ) )
        else if (condition(((DbsUtil.maskMatches(pnd_Hold_Res_Cde,"AAN") && DbsUtil.maskMatches(pnd_Hold_W8_Ben_Country,"AA' '")) || (DbsUtil.maskMatches(pnd_Hold_Res_Cde,"AA' '") 
            && DbsUtil.maskMatches(pnd_Hold_W8_Ben_Country,"AAN")))))
        {
            decideConditionsMet659++;
            if (condition(pnd_Hold_Res_Cde.getSubstring(1,2).equals(pnd_Hold_W8_Ben_Country.getSubstring(1,2))))                                                          //Natural: IF SUBSTR ( #HOLD-RES-CDE,1,2 ) = SUBSTR ( #HOLD-W8-BEN-COUNTRY,1,2 )
            {
                //*  NO UPDATE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
                //*  CHECK TREATY
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Country_Temp.setValue(pnd_Hold_W8_Ben_Country);                                                                                                       //Natural: ASSIGN #COUNTRY-TEMP := #HOLD-W8-BEN-COUNTRY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #HOLD-RES-CDE IS ( N3 ) AND VAL ( #HOLD-RES-CDE ) = 74 THRU 88 AND #HOLD-RES-TYPE = '5'
        else if (condition(DbsUtil.is(pnd_Hold_Res_Cde.getText(),"N3") && pnd_Hold_Res_Cde.greaterOrEqual(74) && pnd_Hold_Res_Cde.lessOrEqual(88) && pnd_Hold_Res_Type.equals("5")))
        {
            decideConditionsMet659++;
            //*  NO UPDATE
            if (condition(pnd_Hold_W8_Ben_Country.equals("CA")))                                                                                                          //Natural: IF #HOLD-W8-BEN-COUNTRY = 'CA'
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
                //*  CHECK TREATY
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Country_Temp.setValue(pnd_Hold_W8_Ben_Country);                                                                                                       //Natural: ASSIGN #COUNTRY-TEMP := #HOLD-W8-BEN-COUNTRY
                //*  CHECK TREATY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Country_Temp.setValue(pnd_Hold_W8_Ben_Country);                                                                                                           //Natural: ASSIGN #COUNTRY-TEMP := #HOLD-W8-BEN-COUNTRY
        }                                                                                                                                                                 //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM CNTRY-US-POSSESSION-CHECK
        sub_Cntry_Us_Possession_Check();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaTxwa2410.getTxwa2410_Pnd_Ret_Code().getBoolean()))                                                                                               //Natural: IF TXWA2410.#RET-CODE
        {
            if (condition(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean()))                                                                                                       //Natural: IF #TXWP5060.#DEBUG-IND
            {
                getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"=",pdaTxwa2410.getTxwa2410_Tircntl_Cntry_Treaty_Ind());                                   //Natural: WRITE *PROGRAM '=' TXWA2410.TIRCNTL-CNTRY-TREATY-IND
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_Treaty_Rate.setValueEdited(new ReportEditMask("N/Y"),pdaTxwa2410.getTxwa2410_Tircntl_Cntry_Treaty_Ind());                                                 //Natural: MOVE EDITED TXWA2410.TIRCNTL-CNTRY-TREATY-IND TO #TREATY-RATE ( EM = N/Y )
            if (condition(pnd_Treaty_Rate.getBoolean()))                                                                                                                  //Natural: IF #TREATY-RATE
            {
                pnd_Override.setValue(true);                                                                                                                              //Natural: ASSIGN #OVERRIDE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Txwp5060_Pnd_Debug_Ind.getBoolean()))                                                                                                       //Natural: IF #TXWP5060.#DEBUG-IND
            {
                getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM()," Warning: County/Possession lookup failed");                                              //Natural: WRITE *PROGRAM ' Warning: County/Possession lookup failed'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Display_Participant_Record() throws Exception                                                                                                        //Natural: DISPLAY-PARTICIPANT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************
        getReports().display(1, "//TIN",                                                                                                                                  //Natural: DISPLAY ( 1 ) '//TIN' FORM-RECEIPT-VIEW.TIN '/TIN/TYP' FORM-RECEIPT-VIEW.TIN-TYPE '/CTZ/CDE' FORM-RECEIPT-VIEW.CITIZEN-CDE '/ACT/IND' FORM-RECEIPT-VIEW.ACTIVE-IND '/RES/TYP' #HOLD-RES-TYPE '/RES/CDE' #HOLD-RES-CDE '/LOC/CDE' #HOLD-LOC-CDE '/W8/CTRY' FORM-RECEIPT-VIEW.W8-BEN-COUNTRY 'LGO/RES/TYP' FORM-RECEIPT-VIEW.LGO-RES-TYPE 'LGO/RES/CDE' FORM-RECEIPT-VIEW.LGO-RES-CDE 'LGO/LOC/CDE' FORM-RECEIPT-VIEW.LGO-LOC-CDE 'LGO/EFF/DTE' FORM-RECEIPT-VIEW.LGO-EFF-DTE 'LGO/EXP/DTE' FORM-RECEIPT-VIEW.LGO-EXP-DTE '/W8/OVR' FORM-RECEIPT-VIEW.W8-BEN-OVERRIDE 'W8/PRM/RES' FORM-RECEIPT-VIEW.W8-BEN-PERM-RES '/W8/TIN' FORM-RECEIPT-VIEW.W8-BEN-TIN '/W8/TRTY' FORM-RECEIPT-VIEW.W8-BEN-TREATY '/W8/SIG' FORM-RECEIPT-VIEW.W8-BEN-SIGNATURE 'W8/SIG/DATE' FORM-RECEIPT-VIEW.W8-BEN-SIGNED-DATE
        		ldaTxwl2425.getForm_Receipt_View_Tin(),"/TIN/TYP",
        		ldaTxwl2425.getForm_Receipt_View_Tin_Type(),"/CTZ/CDE",
        		ldaTxwl2425.getForm_Receipt_View_Citizen_Cde(),"/ACT/IND",
        		ldaTxwl2425.getForm_Receipt_View_Active_Ind(),"/RES/TYP",
        		pnd_Hold_Res_Type,"/RES/CDE",
        		pnd_Hold_Res_Cde,"/LOC/CDE",
        		pnd_Hold_Loc_Cde,"/W8/CTRY",
        		ldaTxwl2425.getForm_Receipt_View_W8_Ben_Country(),"LGO/RES/TYP",
        		ldaTxwl2425.getForm_Receipt_View_Lgo_Res_Type(),"LGO/RES/CDE",
        		ldaTxwl2425.getForm_Receipt_View_Lgo_Res_Cde(),"LGO/LOC/CDE",
        		ldaTxwl2425.getForm_Receipt_View_Lgo_Loc_Cde(),"LGO/EFF/DTE",
        		ldaTxwl2425.getForm_Receipt_View_Lgo_Eff_Dte(),"LGO/EXP/DTE",
        		ldaTxwl2425.getForm_Receipt_View_Lgo_Exp_Dte(),"/W8/OVR",
        		ldaTxwl2425.getForm_Receipt_View_W8_Ben_Override(),"W8/PRM/RES",
        		ldaTxwl2425.getForm_Receipt_View_W8_Ben_Perm_Res(),"/W8/TIN",
        		ldaTxwl2425.getForm_Receipt_View_W8_Ben_Tin(),"/W8/TRTY",
        		ldaTxwl2425.getForm_Receipt_View_W8_Ben_Treaty(),"/W8/SIG",
        		ldaTxwl2425.getForm_Receipt_View_W8_Ben_Signature(),"W8/SIG/DATE",
        		ldaTxwl2425.getForm_Receipt_View_W8_Ben_Signed_Date());
        if (Global.isEscape()) return;
    }
    private void sub_Error_Condition() throws Exception                                                                                                                   //Natural: ERROR-CONDITION
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************
        if (condition(pnd_Txwp5060_Pnd_Abend_Ind.getBoolean() || pnd_Txwp5060_Pnd_Display_Ind.getBoolean()))                                                              //Natural: IF #TXWP5060.#ABEND-IND OR #TXWP5060.#DISPLAY-IND
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(1, "***",new TabSetting(25),pnd_Txwp5060_Pnd_Ret_Msg, new AlphanumericLength (45),new TabSetting(77),"***",NEWLINE,"***",new               //Natural: WRITE ( 1 ) '***' 25T #TXWP5060.#RET-MSG ( AL = 45 ) 77T '***' / '***' 25T 'TIN:         ' FORM-RECEIPT-VIEW.TIN 77T '***' / '***' 25T 'TIN Type:    ' FORM-RECEIPT-VIEW.TIN-TYPE 77T '***' / '***' 25T 'PIN:         ' FORM-RECEIPT-VIEW.PIN 77T '***' / '***' 25T 'Citizen Code:' FORM-RECEIPT-VIEW.CITIZEN-CDE 77T '***' / '***' 25T 'Res Code:    ' FORM-RECEIPT-VIEW.RES-CDE 77T '***' / '***' 25T 'Res Type:    ' FORM-RECEIPT-VIEW.RES-TYPE 77T '***' / '***' 25T 'Loc Code:    ' FORM-RECEIPT-VIEW.LOC-CDE 77T '***'
                TabSetting(25),"TIN:         ",ldaTxwl2425.getForm_Receipt_View_Tin(),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"TIN Type:    ",ldaTxwl2425.getForm_Receipt_View_Tin_Type(),new 
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"PIN:         ",ldaTxwl2425.getForm_Receipt_View_Pin(),new TabSetting(77),"***",NEWLINE,"***",new 
                TabSetting(25),"Citizen Code:",ldaTxwl2425.getForm_Receipt_View_Citizen_Cde(),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Res Code:    ",ldaTxwl2425.getForm_Receipt_View_Res_Cde(),new 
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Res Type:    ",ldaTxwl2425.getForm_Receipt_View_Res_Type(),new TabSetting(77),"***",NEWLINE,"***",new 
                TabSetting(25),"Loc Code:    ",ldaTxwl2425.getForm_Receipt_View_Loc_Cde(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Txwp5060_Pnd_Abend_Ind.getBoolean()))                                                                                                           //Natural: IF #TXWP5060.#ABEND-IND
        {
            DbsUtil.terminate(100);  if (true) return;                                                                                                                    //Natural: TERMINATE 100
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 0 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 0 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 0 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    //*  FE201506 START
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(2).equals("0")  //Natural: IF ##DATA-RESPONSE ( 1 ) = '0' AND ##DATA-RESPONSE ( 2 ) = '0' AND ##DATA-RESPONSE ( 3 ) = '0' AND ##DATA-RESPONSE ( 4 ) = '0'
            && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(3).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(4).equals("0")))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I2 = 1 TO 60
        for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(60)); pnd_I2.nadd(1))
        {
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_I2)));           //Natural: COMPRESS #RC ##DATA-RESPONSE ( #I2 ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,"=",pnd_Rc);                                                                                                           //Natural: WRITE '=' #RC
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,              //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE,"****************************",NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=56 LS=132");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:IIAP"),new TabSetting(95),"Page  :",getReports().getPageNumberDbs(1), 
            new ReportEditMask ("Z,ZZ9"),NEWLINE,Global.getPROGRAM(),new TabSetting(22),"W8 BEN OVERRIDE UPDATES - FLAGS SET TO 'Y'",new TabSetting(95),
            "Report:  RPT1",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, "//TIN",
        		ldaTxwl2425.getForm_Receipt_View_Tin(),"/TIN/TYP",
        		ldaTxwl2425.getForm_Receipt_View_Tin_Type(),"/CTZ/CDE",
        		ldaTxwl2425.getForm_Receipt_View_Citizen_Cde(),"/ACT/IND",
        		ldaTxwl2425.getForm_Receipt_View_Active_Ind(),"/RES/TYP",
        		pnd_Hold_Res_Type,"/RES/CDE",
        		pnd_Hold_Res_Cde,"/LOC/CDE",
        		pnd_Hold_Loc_Cde,"/W8/CTRY",
        		ldaTxwl2425.getForm_Receipt_View_W8_Ben_Country(),"LGO/RES/TYP",
        		ldaTxwl2425.getForm_Receipt_View_Lgo_Res_Type(),"LGO/RES/CDE",
        		ldaTxwl2425.getForm_Receipt_View_Lgo_Res_Cde(),"LGO/LOC/CDE",
        		ldaTxwl2425.getForm_Receipt_View_Lgo_Loc_Cde(),"LGO/EFF/DTE",
        		ldaTxwl2425.getForm_Receipt_View_Lgo_Eff_Dte(),"LGO/EXP/DTE",
        		ldaTxwl2425.getForm_Receipt_View_Lgo_Exp_Dte(),"/W8/OVR",
        		ldaTxwl2425.getForm_Receipt_View_W8_Ben_Override(),"W8/PRM/RES",
        		ldaTxwl2425.getForm_Receipt_View_W8_Ben_Perm_Res(),"/W8/TIN",
        		ldaTxwl2425.getForm_Receipt_View_W8_Ben_Tin(),"/W8/TRTY",
        		ldaTxwl2425.getForm_Receipt_View_W8_Ben_Treaty(),"/W8/SIG",
        		ldaTxwl2425.getForm_Receipt_View_W8_Ben_Signature(),"W8/SIG/DATE",
        		ldaTxwl2425.getForm_Receipt_View_W8_Ben_Signed_Date());
    }
}
