/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:33:27 PM
**        * FROM NATURAL PROGRAM : Twrp1920
************************************************************
**        * FILE NAME            : Twrp1920.java
**        * CLASS NAME           : Twrp1920
**        * INSTANCE NAME        : Twrp1920
************************************************************
***********************************************************************
* PROGRAM : TWRP1920
* FUNCTION: PRINTS ALL INACTIVATED RECORDS , CREATED RECORDS DUE TO
*                 UPDATE (OL) AND RECORDS CREATED ONLINE (ML).
* INPUTS  : CONTRIBUTION FLAT-FILE1 (CREATED BY TWRP1900)
*           CONTROL RECORD - FOR THE PARAMETER DATE
* OUTPUT  : CONTROL TOTAL AND
*           IRA5498 DAILY ONLINE MAINTENANCE DETAIL REPORT
* AUTHOR  : EDITH 7/26/99
* NOTE    : INACTIVATED RECORDS FROM CONTRIBUTION FILE HAVE (+)
*             AMOUNTS AND THE USER WANTS TO DISPLAY THESE AMOUNTS WITH
*             A (-) SIGN.
* UPDATES : TO ACCOUNT INACTIVATED RECORDS BASED ON UPDATE DATE. EDITH
*         : TO PROCESS 'ML' RECORDS , TO PUT SUBTOTAL AFTER EVERY
*            CHANGE IN SOURCE CODE AND TO ENCLOSE INACTIVATED RECORD'S
*            COUNT & AMT. IN PARENTHESIS TO REPRESENT NEGATIVE VALUES.
*                EDITH 8/31/99  (AS PER JOE)
*         : TO DISPLAY ORIGINAL 'ML' THAT WAS INACTIVATED.
*                EDITH 9/8/99 AS PER RICHARD
*         : TO DIPLAY ORIGINAL SOURCE CODE THAT WAS INACTIVATED ON THAT
*             DAY -OR- DISPLAY THE SOURCE CODE THAT WAS CREATED AND
*                      INACTIVATED ON THE SAME DAY. THIS IS INCONNECTION
*                      WITH SOLUTION TO BALANCE THE CONTRIBUTION CONTROL
*                      REPORT VS. CONTRIBUTION ROLL-UP CONTROL REPORT.
*            EDITH 9/15/99.
*
*    NOTE  : A RE-RUN MIGHT NOT GIVE YOU THE CURRENT STATUS OF THE
*             DATABASE BECAUSE RECORD MIGHT BE UPDATED (INACTIVATED)
*             SAY A DAY AFTER THE SUPPOSE TO BE RE-RUN DATE.
*             EX.  A RECORD CREATED ON 8/19/99, UPDATED ON 8/23/99
*                  RE-RUN ON 8/19/99 - SO, THIS RECORD WILL NO LONGER BE
*                  INCLUDED IN PROCESSING.
*         : 01/08/2000 EDITH
*           TO READ FLAT-FILE FOR THE TAX YEAR AND PROCESSING DATE TO BE
*                  USED BY THE PROGRAM
*         : 08/25/2005 J.ROTHOLZ - ADDED CODE FOR SEP IRA
*         : 09/07/2005 B.KARCHEV - UPDATE REPORT LAYOUT
*         : 10/19/2017 DASDH    -5498 BOX CHANGES
*         : 11/12/2020 SAIK     -5498 TAX COMPLIANCE CHANGES - 2020
***********************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp1920 extends BLNatBase
{
    // Data Areas
    private LdaTwrl190a ldaTwrl190a;
    private LdaTwrl0600 ldaTwrl0600;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Parm_Tyear;

    private DbsGroup pnd_Prt_Var;
    private DbsField pnd_Prt_Var_Pnd_Prt1;
    private DbsField pnd_Prt_Var_Pnd_Prt2;
    private DbsField pnd_Prt_Var_Pnd_Prt3;
    private DbsField pnd_Prt4;
    private DbsField pnd_Prt5;
    private DbsField pnd_A;
    private DbsField pnd_B;

    private DbsGroup pnd_Total_Var;
    private DbsField pnd_Total_Var_Pnd_Count;
    private DbsField pnd_Total_Var_Pnd_Classic_Amt;
    private DbsField pnd_Total_Var_Pnd_Roth_Amt;
    private DbsField pnd_Total_Var_Pnd_Sep_Amt;
    private DbsField pnd_Total_Var_Pnd_Rollovr_Amt;
    private DbsField pnd_Total_Var_Pnd_Rechar_Amt;
    private DbsField pnd_Total_Var_Pnd_Conv_Amt;
    private DbsField pnd_Total_Var_Pnd_Fmv_Amt;
    private DbsField pnd_Total_Var_Pnd_Postpn_Amt;
    private DbsField pnd_Total_Var_Pnd_Repayments_Amt;
    private DbsField pnd_Temp_Amt;
    private DbsField pnd_Amt;
    private DbsField pnd_Prt_Amt;
    private DbsField pnd_Ttl_Cnt;
    private DbsField pnd_Ttl_Amt;
    private DbsField pnd_Print_Run;
    private DbsField pnd_Print_Com;
    private DbsField pnd_Param_Freq;
    private DbsField pnd_Tax_Year;
    private DbsField pnd_Rec_Read;
    private DbsField pnd_Rec_Process;
    private DbsField pnd_Error_Rec;
    private DbsField pnd_Cnt;
    private DbsField pnd_I;
    private DbsField pnd_First_Rec;
    private DbsField pnd_New_Company;
    private DbsField pnd_Same_Source;
    private DbsField pnd_Wfslash_Orig_Sc;

    private DbsGroup pnd_Saver;
    private DbsField pnd_Saver_Pnd_Sv_Company;
    private DbsField pnd_Saver_Pnd_Sv_Source;
    private DbsField pnd_Saver_Pnd_Sv_Status;
    private DbsField pnd_Cref_Cnt;
    private DbsField pnd_Tiaa_Cnt;
    private DbsField pnd_Life_Cnt;
    private DbsField pnd_Othr_Cnt;
    private DbsField pnd_Prt_Source;

    private DbsGroup pnd_Prt_Source__R_Field_1;
    private DbsField pnd_Prt_Source_Pnd_Prt_Source_Org;
    private DbsField pnd_Prt_Source_Pnd_Prt_Source_Upd;
    private DbsField pnd_Contract;
    private DbsField pnd_Ind;

    private DbsGroup pnd_Print_Var;
    private DbsField pnd_Print_Var_Pnd_Print_Source;
    private DbsField pnd_Print_Var_Pnd_Print_Contract;
    private DbsField pnd_Print_Var_Pnd_Print_Tax_Id;
    private DbsField pnd_Print_Var_Pnd_Print_Classic;
    private DbsField pnd_Print_Var_Pnd_Print_Roth;
    private DbsField pnd_Print_Var_Pnd_Print_Sep;
    private DbsField pnd_Print_Var_Pnd_Print_Rollover;
    private DbsField pnd_Print_Var_Pnd_Print_Rechar;
    private DbsField pnd_Print_Var_Pnd_Print_Conv;
    private DbsField pnd_Print_Var_Pnd_Print_Fmv;
    private DbsField pnd_Print_Var_Pnd_Print_Postpn;
    private DbsField pnd_Print_Var_Pnd_Print_Repayments;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl190a = new LdaTwrl190a();
        registerRecord(ldaTwrl190a);
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Parm_Tyear = localVariables.newFieldInRecord("pnd_Parm_Tyear", "#PARM-TYEAR", FieldType.STRING, 4);

        pnd_Prt_Var = localVariables.newGroupInRecord("pnd_Prt_Var", "#PRT-VAR");
        pnd_Prt_Var_Pnd_Prt1 = pnd_Prt_Var.newFieldInGroup("pnd_Prt_Var_Pnd_Prt1", "#PRT1", FieldType.STRING, 4);
        pnd_Prt_Var_Pnd_Prt2 = pnd_Prt_Var.newFieldInGroup("pnd_Prt_Var_Pnd_Prt2", "#PRT2", FieldType.STRING, 14);
        pnd_Prt_Var_Pnd_Prt3 = pnd_Prt_Var.newFieldInGroup("pnd_Prt_Var_Pnd_Prt3", "#PRT3", FieldType.STRING, 21);
        pnd_Prt4 = localVariables.newFieldInRecord("pnd_Prt4", "#PRT4", FieldType.STRING, 21);
        pnd_Prt5 = localVariables.newFieldInRecord("pnd_Prt5", "#PRT5", FieldType.STRING, 21);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.STRING, 4);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.STRING, 21);

        pnd_Total_Var = localVariables.newGroupArrayInRecord("pnd_Total_Var", "#TOTAL-VAR", new DbsArrayController(1, 3));
        pnd_Total_Var_Pnd_Count = pnd_Total_Var.newFieldInGroup("pnd_Total_Var_Pnd_Count", "#COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Var_Pnd_Classic_Amt = pnd_Total_Var.newFieldInGroup("pnd_Total_Var_Pnd_Classic_Amt", "#CLASSIC-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Var_Pnd_Roth_Amt = pnd_Total_Var.newFieldInGroup("pnd_Total_Var_Pnd_Roth_Amt", "#ROTH-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Var_Pnd_Sep_Amt = pnd_Total_Var.newFieldInGroup("pnd_Total_Var_Pnd_Sep_Amt", "#SEP-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Var_Pnd_Rollovr_Amt = pnd_Total_Var.newFieldInGroup("pnd_Total_Var_Pnd_Rollovr_Amt", "#ROLLOVR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Var_Pnd_Rechar_Amt = pnd_Total_Var.newFieldInGroup("pnd_Total_Var_Pnd_Rechar_Amt", "#RECHAR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Var_Pnd_Conv_Amt = pnd_Total_Var.newFieldInGroup("pnd_Total_Var_Pnd_Conv_Amt", "#CONV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Var_Pnd_Fmv_Amt = pnd_Total_Var.newFieldInGroup("pnd_Total_Var_Pnd_Fmv_Amt", "#FMV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Var_Pnd_Postpn_Amt = pnd_Total_Var.newFieldInGroup("pnd_Total_Var_Pnd_Postpn_Amt", "#POSTPN-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Var_Pnd_Repayments_Amt = pnd_Total_Var.newFieldInGroup("pnd_Total_Var_Pnd_Repayments_Amt", "#REPAYMENTS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Temp_Amt = localVariables.newFieldArrayInRecord("pnd_Temp_Amt", "#TEMP-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 9));
        pnd_Amt = localVariables.newFieldArrayInRecord("pnd_Amt", "#AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 9));
        pnd_Prt_Amt = localVariables.newFieldArrayInRecord("pnd_Prt_Amt", "#PRT-AMT", FieldType.STRING, 16, new DbsArrayController(1, 9));
        pnd_Ttl_Cnt = localVariables.newFieldInRecord("pnd_Ttl_Cnt", "#TTL-CNT", FieldType.STRING, 11);
        pnd_Ttl_Amt = localVariables.newFieldArrayInRecord("pnd_Ttl_Amt", "#TTL-AMT", FieldType.STRING, 19, new DbsArrayController(1, 9));
        pnd_Print_Run = localVariables.newFieldInRecord("pnd_Print_Run", "#PRINT-RUN", FieldType.STRING, 11);
        pnd_Print_Com = localVariables.newFieldInRecord("pnd_Print_Com", "#PRINT-COM", FieldType.STRING, 4);
        pnd_Param_Freq = localVariables.newFieldInRecord("pnd_Param_Freq", "#PARAM-FREQ", FieldType.STRING, 1);
        pnd_Tax_Year = localVariables.newFieldInRecord("pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Rec_Read = localVariables.newFieldInRecord("pnd_Rec_Read", "#REC-READ", FieldType.NUMERIC, 9);
        pnd_Rec_Process = localVariables.newFieldInRecord("pnd_Rec_Process", "#REC-PROCESS", FieldType.NUMERIC, 9);
        pnd_Error_Rec = localVariables.newFieldInRecord("pnd_Error_Rec", "#ERROR-REC", FieldType.NUMERIC, 9);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_First_Rec = localVariables.newFieldInRecord("pnd_First_Rec", "#FIRST-REC", FieldType.BOOLEAN, 1);
        pnd_New_Company = localVariables.newFieldInRecord("pnd_New_Company", "#NEW-COMPANY", FieldType.BOOLEAN, 1);
        pnd_Same_Source = localVariables.newFieldInRecord("pnd_Same_Source", "#SAME-SOURCE", FieldType.BOOLEAN, 1);
        pnd_Wfslash_Orig_Sc = localVariables.newFieldInRecord("pnd_Wfslash_Orig_Sc", "#W/ORIG-SC", FieldType.BOOLEAN, 1);

        pnd_Saver = localVariables.newGroupInRecord("pnd_Saver", "#SAVER");
        pnd_Saver_Pnd_Sv_Company = pnd_Saver.newFieldInGroup("pnd_Saver_Pnd_Sv_Company", "#SV-COMPANY", FieldType.STRING, 1);
        pnd_Saver_Pnd_Sv_Source = pnd_Saver.newFieldInGroup("pnd_Saver_Pnd_Sv_Source", "#SV-SOURCE", FieldType.STRING, 6);
        pnd_Saver_Pnd_Sv_Status = pnd_Saver.newFieldInGroup("pnd_Saver_Pnd_Sv_Status", "#SV-STATUS", FieldType.STRING, 1);
        pnd_Cref_Cnt = localVariables.newFieldArrayInRecord("pnd_Cref_Cnt", "#CREF-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Tiaa_Cnt = localVariables.newFieldArrayInRecord("pnd_Tiaa_Cnt", "#TIAA-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Life_Cnt = localVariables.newFieldArrayInRecord("pnd_Life_Cnt", "#LIFE-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Othr_Cnt = localVariables.newFieldArrayInRecord("pnd_Othr_Cnt", "#OTHR-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Prt_Source = localVariables.newFieldInRecord("pnd_Prt_Source", "#PRT-SOURCE", FieldType.STRING, 6);

        pnd_Prt_Source__R_Field_1 = localVariables.newGroupInRecord("pnd_Prt_Source__R_Field_1", "REDEFINE", pnd_Prt_Source);
        pnd_Prt_Source_Pnd_Prt_Source_Org = pnd_Prt_Source__R_Field_1.newFieldInGroup("pnd_Prt_Source_Pnd_Prt_Source_Org", "#PRT-SOURCE-ORG", FieldType.STRING, 
            3);
        pnd_Prt_Source_Pnd_Prt_Source_Upd = pnd_Prt_Source__R_Field_1.newFieldInGroup("pnd_Prt_Source_Pnd_Prt_Source_Upd", "#PRT-SOURCE-UPD", FieldType.STRING, 
            3);
        pnd_Contract = localVariables.newFieldInRecord("pnd_Contract", "#CONTRACT", FieldType.STRING, 11);
        pnd_Ind = localVariables.newFieldInRecord("pnd_Ind", "#IND", FieldType.NUMERIC, 3);

        pnd_Print_Var = localVariables.newGroupArrayInRecord("pnd_Print_Var", "#PRINT-VAR", new DbsArrayController(1, 380));
        pnd_Print_Var_Pnd_Print_Source = pnd_Print_Var.newFieldInGroup("pnd_Print_Var_Pnd_Print_Source", "#PRINT-SOURCE", FieldType.STRING, 21);
        pnd_Print_Var_Pnd_Print_Contract = pnd_Print_Var.newFieldInGroup("pnd_Print_Var_Pnd_Print_Contract", "#PRINT-CONTRACT", FieldType.STRING, 11);
        pnd_Print_Var_Pnd_Print_Tax_Id = pnd_Print_Var.newFieldInGroup("pnd_Print_Var_Pnd_Print_Tax_Id", "#PRINT-TAX-ID", FieldType.STRING, 10);
        pnd_Print_Var_Pnd_Print_Classic = pnd_Print_Var.newFieldInGroup("pnd_Print_Var_Pnd_Print_Classic", "#PRINT-CLASSIC", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Print_Var_Pnd_Print_Roth = pnd_Print_Var.newFieldInGroup("pnd_Print_Var_Pnd_Print_Roth", "#PRINT-ROTH", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Print_Var_Pnd_Print_Sep = pnd_Print_Var.newFieldInGroup("pnd_Print_Var_Pnd_Print_Sep", "#PRINT-SEP", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Print_Var_Pnd_Print_Rollover = pnd_Print_Var.newFieldInGroup("pnd_Print_Var_Pnd_Print_Rollover", "#PRINT-ROLLOVER", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Print_Var_Pnd_Print_Rechar = pnd_Print_Var.newFieldInGroup("pnd_Print_Var_Pnd_Print_Rechar", "#PRINT-RECHAR", FieldType.PACKED_DECIMAL, 11, 
            2);
        pnd_Print_Var_Pnd_Print_Conv = pnd_Print_Var.newFieldInGroup("pnd_Print_Var_Pnd_Print_Conv", "#PRINT-CONV", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Print_Var_Pnd_Print_Fmv = pnd_Print_Var.newFieldInGroup("pnd_Print_Var_Pnd_Print_Fmv", "#PRINT-FMV", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Print_Var_Pnd_Print_Postpn = pnd_Print_Var.newFieldInGroup("pnd_Print_Var_Pnd_Print_Postpn", "#PRINT-POSTPN", FieldType.PACKED_DECIMAL, 11, 
            2);
        pnd_Print_Var_Pnd_Print_Repayments = pnd_Print_Var.newFieldInGroup("pnd_Print_Var_Pnd_Print_Repayments", "#PRINT-REPAYMENTS", FieldType.PACKED_DECIMAL, 
            11, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl190a.initializeValues();
        ldaTwrl0600.initializeValues();

        localVariables.reset();
        pnd_First_Rec.setInitialValue(true);
        pnd_New_Company.setInitialValue(false);
        pnd_Same_Source.setInitialValue(false);
        pnd_Wfslash_Orig_Sc.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp1920() throws Exception
    {
        super("Twrp1920");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        getWorkFiles().read(1, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                             //Natural: READ WORK FILE 01 ONCE #TWRP0600-CONTROL-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            getReports().write(0, " ************************************ ",NEWLINE," ***                              *** ",NEWLINE," ***    CONTROL RECORD IS EMPTY   *** ", //Natural: WRITE ' ************************************ ' / ' ***                              *** ' / ' ***    CONTROL RECORD IS EMPTY   *** ' / ' ***    PLEASE INFORM SYSTEMS !!  *** ' / ' ***                              *** ' / ' ************************************ '
                NEWLINE," ***    PLEASE INFORM SYSTEMS !!  *** ",NEWLINE," ***                              *** ",NEWLINE," ************************************ ");
            if (Global.isEscape()) return;
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-ENDFILE
        pnd_Ind.reset();                                                                                                                                                  //Natural: RESET #IND #CONTRACT
        pnd_Contract.reset();
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 02 RECORD #F96-FF1
        while (condition(getWorkFiles().read(2, ldaTwrl190a.getPnd_F96_Ff1())))
        {
            pnd_Rec_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-READ
            pnd_Cnt.setValue(1);                                                                                                                                          //Natural: ASSIGN #CNT := 1
                                                                                                                                                                          //Natural: PERFORM COMPANY-CNT
            sub_Company_Cnt();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(!((((ldaTwrl190a.getPnd_F96_Ff1_Twrc_Status().equals(" ") && (ldaTwrl190a.getPnd_F96_Ff1_Pnd_Twrc_Orig_Sc().equals("ZZ ") ||                    //Natural: ACCEPT IF #F96-FF1.TWRC-STATUS = ' ' AND ( #F96-FF1.#TWRC-ORIG-SC = 'ZZ ' OR #F96-FF1.#TWRC-UPDT-SC = 'OL ' ) AND #F96-FF1.TWRC-CREATE-DATE = #TWRP0600-FROM-CCYYMMDD THRU #TWRP0600-TO-CCYYMMDD OR #F96-FF1.TWRC-STATUS = 'I' AND #F96-FF1.TWRC-UPDATE-DATE = #TWRP0600-FROM-CCYYMMDD THRU #TWRP0600-TO-CCYYMMDD
                ldaTwrl190a.getPnd_F96_Ff1_Pnd_Twrc_Updt_Sc().equals("OL "))) && (ldaTwrl190a.getPnd_F96_Ff1_Twrc_Create_Date().greaterOrEqual(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd()) 
                && ldaTwrl190a.getPnd_F96_Ff1_Twrc_Create_Date().lessOrEqual(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd()))) || 
                (ldaTwrl190a.getPnd_F96_Ff1_Twrc_Status().equals("I") && (ldaTwrl190a.getPnd_F96_Ff1_Twrc_Update_Date().greaterOrEqual(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd()) 
                && ldaTwrl190a.getPnd_F96_Ff1_Twrc_Update_Date().lessOrEqual(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd())))))))
            {
                continue;
            }
            pnd_Rec_Process.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #REC-PROCESS
            pnd_Cnt.setValue(2);                                                                                                                                          //Natural: ASSIGN #CNT := 2
                                                                                                                                                                          //Natural: PERFORM COMPANY-CNT
            sub_Company_Cnt();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_First_Rec.equals(true)))                                                                                                                    //Natural: IF #FIRST-REC = TRUE
            {
                pnd_First_Rec.setValue(false);                                                                                                                            //Natural: ASSIGN #FIRST-REC := FALSE
                                                                                                                                                                          //Natural: PERFORM SAVE-FIELDS
                sub_Save_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Company_Cde().equals(pnd_Saver_Pnd_Sv_Company)))                                                                //Natural: IF #F96-FF1.TWRC-COMPANY-CDE = #SV-COMPANY
            {
                if (condition(ldaTwrl190a.getPnd_F96_Ff1_Pnd_Twrc_Source_Upd().equals(pnd_Saver_Pnd_Sv_Source)))                                                          //Natural: IF #F96-FF1.#TWRC-SOURCE-UPD = #SV-SOURCE
                {
                    if (condition(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Status().equals(pnd_Saver_Pnd_Sv_Status)))                                                              //Natural: IF #F96-FF1.TWRC-STATUS = #SV-STATUS
                    {
                        pnd_Same_Source.setValue(true);                                                                                                                   //Natural: ASSIGN #SAME-SOURCE := TRUE
                        //*  AT BREAK OF STATUS
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Saver_Pnd_Sv_Status.setValue(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Status());                                                                       //Natural: ASSIGN #SV-STATUS := #F96-FF1.TWRC-STATUS
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  AT BREAK OF SOURCE
                                                                                                                                                                          //Natural: PERFORM PRINT-SUBTOTAL
                    sub_Print_Subtotal();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Saver_Pnd_Sv_Source.setValue(ldaTwrl190a.getPnd_F96_Ff1_Pnd_Twrc_Source_Upd());                                                                   //Natural: ASSIGN #SV-SOURCE := #F96-FF1.#TWRC-SOURCE-UPD
                    pnd_Saver_Pnd_Sv_Status.setValue(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Status());                                                                           //Natural: ASSIGN #SV-STATUS := #F96-FF1.TWRC-STATUS
                    pnd_Same_Source.setValue(false);                                                                                                                      //Natural: ASSIGN #SAME-SOURCE := FALSE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  AT BREAK OF COMPANY
                                                                                                                                                                          //Natural: PERFORM PRINT-GRANDTOTAL
                sub_Print_Grandtotal();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM SAVE-FIELDS
                sub_Save_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Same_Source.setValue(false);                                                                                                                          //Natural: ASSIGN #SAME-SOURCE := FALSE
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 01 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-STATUS
            sub_Check_Status();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* *------
        if (condition(pnd_Rec_Process.equals(getZero())))                                                                                                                 //Natural: IF #REC-PROCESS = 0
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(40)," **** NO RECORDS PROCESSED **** ");                            //Natural: WRITE ( 01 ) //// 40T ' **** NO RECORDS PROCESSED **** '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-GRANDTOTAL
            sub_Print_Grandtotal();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 01 )
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),NEWLINE,"IRA5498 DAILY ONLINE MAINTENANCE DETAIL REPORT",NEWLINE,"DATE RANGE : ",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd()," THRU ",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd(),NEWLINE,"RUNDATE    : ",Global.getDATX(),  //Natural: WRITE ( 01 ) NOTITLE NOHDR *PROGRAM / 'IRA5498 DAILY ONLINE MAINTENANCE DETAIL REPORT' / 'DATE RANGE : ' #TWRP0600-FROM-CCYYMMDD ' THRU ' #TWRP0600-TO-CCYYMMDD / 'RUNDATE    : ' *DATX ( EM = MM/DD/YYYY ) / 'RUNTIME    : ' *TIMX / 'TAX YEAR   : ' #TWRP0600-TAX-YEAR-CCYY /// 'TOTAL RECORDS READ         : ' #REC-READ / '  1. CREF RECORDS          : ' #CREF-CNT ( 1 ) / '  2. TIAA RECORDS          : ' #TIAA-CNT ( 1 ) / '  3. TIAA-LIFE RECORDS     : ' #LIFE-CNT ( 1 ) / '  4. NON-CREF/TIAA RECORDS : ' #OTHR-CNT ( 1 ) // 'TOTAL RECORDS PROCESS      : ' #REC-PROCESS / '  1. CREF RECORDS          : ' #CREF-CNT ( 2 ) / '  2. TIAA RECORDS          : ' #TIAA-CNT ( 2 ) / '  3. TIAA-LIFE RECORDS     : ' #LIFE-CNT ( 2 ) / '  4. NON-CREF/TIAA RECORDS : ' #OTHR-CNT ( 2 ) ///
            new ReportEditMask ("MM/DD/YYYY"),NEWLINE,"RUNTIME    : ",Global.getTIMX(),NEWLINE,"TAX YEAR   : ",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy(),
            NEWLINE,NEWLINE,NEWLINE,"TOTAL RECORDS READ         : ",pnd_Rec_Read,NEWLINE,"  1. CREF RECORDS          : ",pnd_Cref_Cnt.getValue(1),NEWLINE,
            "  2. TIAA RECORDS          : ",pnd_Tiaa_Cnt.getValue(1),NEWLINE,"  3. TIAA-LIFE RECORDS     : ",pnd_Life_Cnt.getValue(1),NEWLINE,"  4. NON-CREF/TIAA RECORDS : ",
            pnd_Othr_Cnt.getValue(1),NEWLINE,NEWLINE,"TOTAL RECORDS PROCESS      : ",pnd_Rec_Process,NEWLINE,"  1. CREF RECORDS          : ",pnd_Cref_Cnt.getValue(2),
            NEWLINE,"  2. TIAA RECORDS          : ",pnd_Tiaa_Cnt.getValue(2),NEWLINE,"  3. TIAA-LIFE RECORDS     : ",pnd_Life_Cnt.getValue(2),NEWLINE,"  4. NON-CREF/TIAA RECORDS : ",
            pnd_Othr_Cnt.getValue(2),NEWLINE,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *------------
        //* *----------------------------
        //* *------------
        //* *------------
        //* *------------
        //* *----------------------------------
        //* *------------
        //* *------------
        //* *------------
        //*   --  ACCOUNT ORIG. SOURCE TO SUBTOTAL & GRANDTOTAL  --
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 01 )
    }
    private void sub_Check_Status() throws Exception                                                                                                                      //Natural: CHECK-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------
        if (condition(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Status().equals("I")))                                                                                              //Natural: IF #F96-FF1.TWRC-STATUS = 'I'
        {
            if (condition(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Create_Date().greaterOrEqual(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd())           //Natural: IF #F96-FF1.TWRC-CREATE-DATE = #TWRP0600-FROM-CCYYMMDD THRU #TWRP0600-TO-CCYYMMDD AND #F96-FF1.TWRC-UPDATE-DATE = #TWRP0600-FROM-CCYYMMDD THRU #TWRP0600-TO-CCYYMMDD
                && ldaTwrl190a.getPnd_F96_Ff1_Twrc_Create_Date().lessOrEqual(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd()) && ldaTwrl190a.getPnd_F96_Ff1_Twrc_Update_Date().greaterOrEqual(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd()) 
                && ldaTwrl190a.getPnd_F96_Ff1_Twrc_Update_Date().lessOrEqual(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd())))
            {
                                                                                                                                                                          //Natural: PERFORM PRINT-ORIG-SOURCE
                sub_Print_Orig_Source();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM KEEP-INACTIVE-RECORD
                sub_Keep_Inactive_Record();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM KEEP-INACTIVE-RECORD
                sub_Keep_Inactive_Record();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  ACTIVE RECORD
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-RECORD
            sub_Print_Detail_Record();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ACCOUNT-REC
            sub_Account_Rec();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Save_Fields() throws Exception                                                                                                                       //Natural: SAVE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Saver_Pnd_Sv_Company.setValue(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Company_Cde());                                                                                 //Natural: ASSIGN #SV-COMPANY := #F96-FF1.TWRC-COMPANY-CDE
        pnd_Saver_Pnd_Sv_Source.setValue(ldaTwrl190a.getPnd_F96_Ff1_Pnd_Twrc_Source_Upd());                                                                               //Natural: ASSIGN #SV-SOURCE := #F96-FF1.#TWRC-SOURCE-UPD
        pnd_Saver_Pnd_Sv_Status.setValue(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Status());                                                                                       //Natural: ASSIGN #SV-STATUS := #F96-FF1.TWRC-STATUS
    }
    private void sub_Account_Rec() throws Exception                                                                                                                       //Natural: ACCOUNT-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------
        if (condition(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Status().equals(" ")))                                                                                              //Natural: IF #F96-FF1.TWRC-STATUS = ' '
        {
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 2
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(2)); pnd_I.nadd(1))
            {
                pnd_Total_Var_Pnd_Count.getValue(pnd_I).nadd(1);                                                                                                          //Natural: ADD 1 TO #COUNT ( #I )
                pnd_Total_Var_Pnd_Classic_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Classic_Amt());                                                        //Natural: ADD #F96-FF1.TWRC-CLASSIC-AMT TO #CLASSIC-AMT ( #I )
                pnd_Total_Var_Pnd_Roth_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Amt());                                                              //Natural: ADD #F96-FF1.TWRC-ROTH-AMT TO #ROTH-AMT ( #I )
                pnd_Total_Var_Pnd_Sep_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Sep_Amt());                                                                //Natural: ADD #F96-FF1.TWRC-SEP-AMT TO #SEP-AMT ( #I )
                pnd_Total_Var_Pnd_Rollovr_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rollover_Amt());                                                       //Natural: ADD #F96-FF1.TWRC-ROLLOVER-AMT TO #ROLLOVR-AMT ( #I )
                pnd_Total_Var_Pnd_Rechar_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rechar_Amt());                                                          //Natural: ADD #F96-FF1.TWRC-RECHAR-AMT TO #RECHAR-AMT ( #I )
                pnd_Total_Var_Pnd_Conv_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Conversion_Amt());                                                   //Natural: ADD #F96-FF1.TWRC-ROTH-CONVERSION-AMT TO #CONV-AMT ( #I )
                pnd_Total_Var_Pnd_Fmv_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Fair_Mkt_Val_Amt());                                                       //Natural: ADD #F96-FF1.TWRC-FAIR-MKT-VAL-AMT TO #FMV-AMT ( #I )
                //* DASDH
                pnd_Total_Var_Pnd_Postpn_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Postpn_Amt());                                                          //Natural: ADD #F96-FF1.TWRC-POSTPN-AMT TO #POSTPN-AMT ( #I )
                //*  SAIK
                pnd_Total_Var_Pnd_Repayments_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Repayments_Amt());                                                  //Natural: ADD #F96-FF1.TWRC-REPAYMENTS-AMT TO #REPAYMENTS-AMT ( #I )
                //*  SAIK
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR02:                                                                                                                                                        //Natural: FOR #I 1 2
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(2)); pnd_I.nadd(1))
            {
                pnd_Total_Var_Pnd_Count.getValue(pnd_I).nsubtract(1);                                                                                                     //Natural: SUBTRACT 1 FROM #COUNT ( #I )
                pnd_Total_Var_Pnd_Classic_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Classic_Amt());                                                   //Natural: SUBTRACT #F96-FF1.TWRC-CLASSIC-AMT FROM #CLASSIC-AMT ( #I )
                pnd_Total_Var_Pnd_Roth_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Amt());                                                         //Natural: SUBTRACT #F96-FF1.TWRC-ROTH-AMT FROM #ROTH-AMT ( #I )
                pnd_Total_Var_Pnd_Sep_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Sep_Amt());                                                           //Natural: SUBTRACT #F96-FF1.TWRC-SEP-AMT FROM #SEP-AMT ( #I )
                pnd_Total_Var_Pnd_Rollovr_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rollover_Amt());                                                  //Natural: SUBTRACT #F96-FF1.TWRC-ROLLOVER-AMT FROM #ROLLOVR-AMT ( #I )
                pnd_Total_Var_Pnd_Rechar_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rechar_Amt());                                                     //Natural: SUBTRACT #F96-FF1.TWRC-RECHAR-AMT FROM #RECHAR-AMT ( #I )
                pnd_Total_Var_Pnd_Conv_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Conversion_Amt());                                              //Natural: SUBTRACT #F96-FF1.TWRC-ROTH-CONVERSION-AMT FROM #CONV-AMT ( #I )
                pnd_Total_Var_Pnd_Fmv_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Fair_Mkt_Val_Amt());                                                  //Natural: SUBTRACT #F96-FF1.TWRC-FAIR-MKT-VAL-AMT FROM #FMV-AMT ( #I )
                //*  DASDH
                pnd_Total_Var_Pnd_Postpn_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Postpn_Amt());                                                     //Natural: SUBTRACT #F96-FF1.TWRC-POSTPN-AMT FROM #POSTPN-AMT ( #I )
                //*  DASDH
                //*  SAIK
                pnd_Total_Var_Pnd_Repayments_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Repayments_Amt());                                             //Natural: SUBTRACT #F96-FF1.TWRC-REPAYMENTS-AMT FROM #REPAYMENTS-AMT ( #I )
                //*  SAIK
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Company_Cnt() throws Exception                                                                                                                       //Natural: COMPANY-CNT
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------
        short decideConditionsMet354 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #F96-FF1.TWRC-COMPANY-CDE;//Natural: VALUE 'C'
        if (condition((ldaTwrl190a.getPnd_F96_Ff1_Twrc_Company_Cde().equals("C"))))
        {
            decideConditionsMet354++;
            pnd_Cref_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #CREF-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((ldaTwrl190a.getPnd_F96_Ff1_Twrc_Company_Cde().equals("T"))))
        {
            decideConditionsMet354++;
            pnd_Tiaa_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #TIAA-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((ldaTwrl190a.getPnd_F96_Ff1_Twrc_Company_Cde().equals("L"))))
        {
            decideConditionsMet354++;
            pnd_Life_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #LIFE-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Othr_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #OTHR-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Check_Source_Desc() throws Exception                                                                                                                 //Natural: CHECK-SOURCE-DESC
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Prt_Source.setValue(ldaTwrl190a.getPnd_F96_Ff1_Pnd_Twrc_Source_Upd());                                                                                        //Natural: ASSIGN #PRT-SOURCE := #F96-FF1.#TWRC-SOURCE-UPD
        if (condition(pnd_Prt_Source_Pnd_Prt_Source_Org.equals("ZZ ")))                                                                                                   //Natural: IF #PRT-SOURCE-ORG = 'ZZ '
        {
            pnd_Prt_Source_Pnd_Prt_Source_Org.setValue("ML ");                                                                                                            //Natural: ASSIGN #PRT-SOURCE-ORG := 'ML '
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Prt_Source_Pnd_Prt_Source_Upd.equals(" ")))                                                                                                     //Natural: IF #PRT-SOURCE-UPD = ' '
        {
            pnd_Prt_Var_Pnd_Prt1.setValue(" ");                                                                                                                           //Natural: ASSIGN #PRT1 := ' '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Prt_Var_Pnd_Prt1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "/", pnd_Prt_Source_Pnd_Prt_Source_Upd));                                       //Natural: COMPRESS '/' #PRT-SOURCE-UPD TO #PRT1 LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Status().equals("I")))                                                                                              //Natural: IF #F96-FF1.TWRC-STATUS = 'I'
        {
            pnd_Prt_Var_Pnd_Prt2.setValue(" - INACTIVATED");                                                                                                              //Natural: ASSIGN #PRT2 := ' - INACTIVATED'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Prt_Var_Pnd_Prt2.setValue(" ");                                                                                                                           //Natural: ASSIGN #PRT2 := ' '
        }                                                                                                                                                                 //Natural: END-IF
        //*   COMPRESS  #PRT-SOURCE-ORG #PRT1 #PRT2  TO  #PRT3  LEAVING NO SPACE
    }
    private void sub_Check_Var_To_Prt() throws Exception                                                                                                                  //Natural: CHECK-VAR-TO-PRT
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        //*  IF #FIRST-REC   =  TRUE
        //*     #FIRST-REC  :=  FALSE
        //*     PERFORM  CHECK-SOURCE-DESC
        //*     COMPRESS  #PRT-SOURCE-ORG #PRT1 #PRT2  TO  #PRT3  LEAVING NO SPACE
        //*  END-IF
        //*  SAME CO,SOURCE
        if (condition(pnd_Same_Source.equals(true)))                                                                                                                      //Natural: IF #SAME-SOURCE = TRUE
        {
            pnd_Same_Source.setValue(false);                                                                                                                              //Natural: ASSIGN #SAME-SOURCE := FALSE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM CHECK-SOURCE-DESC
            sub_Check_Source_Desc();
            if (condition(Global.isEscape())) {return;}
            pnd_Prt_Var_Pnd_Prt3.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Prt_Source_Pnd_Prt_Source_Org, pnd_Prt_Var_Pnd_Prt1, pnd_Prt_Var_Pnd_Prt2)); //Natural: COMPRESS #PRT-SOURCE-ORG #PRT1 #PRT2 TO #PRT3 LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Contract.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl190a.getPnd_F96_Ff1_Twrc_Contract(), "-", ldaTwrl190a.getPnd_F96_Ff1_Twrc_Payee())); //Natural: COMPRESS #F96-FF1.TWRC-CONTRACT '-' #F96-FF1.TWRC-PAYEE INTO #CONTRACT LEAVING NO SPACE
    }
    private void sub_Print_Detail_Record() throws Exception                                                                                                               //Natural: PRINT-DETAIL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
                                                                                                                                                                          //Natural: PERFORM CHECK-VAR-TO-PRT
        sub_Check_Var_To_Prt();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Status().equals(" ")))                                                                                              //Natural: IF #F96-FF1.TWRC-STATUS = ' '
        {
            //*  BK
            //*  DASDH
            //*  SAIK
            getReports().write(1, ReportOption.NOTITLE,pnd_Prt_Var_Pnd_Prt3,new ColumnSpacing(1),pnd_Contract,new ColumnSpacing(7),ldaTwrl190a.getPnd_F96_Ff1_Twrc_Classic_Amt(),  //Natural: WRITE ( 01 ) #PRT3 1X #CONTRACT 7X #F96-FF1.TWRC-CLASSIC-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #F96-FF1.TWRC-ROTH-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #F96-FF1.TWRC-SEP-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #F96-FF1.TWRC-ROLLOVER-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) / 23T #F96-FF1.TWRC-TAX-ID 40T #F96-FF1.TWRC-POSTPN-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) 62T #F96-FF1.TWRC-RECHAR-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #F96-FF1.TWRC-ROTH-CONVERSION-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #F96-FF1.TWRC-FAIR-MKT-VAL-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) / 40T #F96-FF1.TWRC-REPAYMENTS-AMT ( EM = ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(6),ldaTwrl190a.getPnd_F96_Ff1_Twrc_Sep_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rollover_Amt(), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(23),ldaTwrl190a.getPnd_F96_Ff1_Twrc_Tax_Id(),new TabSetting(40),ldaTwrl190a.getPnd_F96_Ff1_Twrc_Postpn_Amt(), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(62),ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rechar_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(6),ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Conversion_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaTwrl190a.getPnd_F96_Ff1_Twrc_Fair_Mkt_Val_Amt(), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(40),ldaTwrl190a.getPnd_F96_Ff1_Twrc_Repayments_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            //*  INACTIVATED RECORD
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Prt_Amt.getValue(1).setValueEdited(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Classic_Amt(),new ReportEditMask("' ('ZZZ,ZZZ,ZZ9.99')'"));                            //Natural: MOVE EDITED #F96-FF1.TWRC-CLASSIC-AMT ( EM = ' ('ZZZ,ZZZ,ZZ9.99')' ) TO #PRT-AMT ( 1 )
            pnd_Prt_Amt.getValue(2).setValueEdited(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Amt(),new ReportEditMask("' ('ZZZ,ZZZ,ZZ9.99')'"));                               //Natural: MOVE EDITED #F96-FF1.TWRC-ROTH-AMT ( EM = ' ('ZZZ,ZZZ,ZZ9.99')' ) TO #PRT-AMT ( 2 )
            pnd_Prt_Amt.getValue(3).setValueEdited(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Sep_Amt(),new ReportEditMask("' ('ZZZ,ZZZ,ZZ9.99')'"));                                //Natural: MOVE EDITED #F96-FF1.TWRC-SEP-AMT ( EM = ' ('ZZZ,ZZZ,ZZ9.99')' ) TO #PRT-AMT ( 3 )
            pnd_Prt_Amt.getValue(4).setValueEdited(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rollover_Amt(),new ReportEditMask("' ('ZZZ,ZZZ,ZZ9.99')'"));                           //Natural: MOVE EDITED #F96-FF1.TWRC-ROLLOVER-AMT ( EM = ' ('ZZZ,ZZZ,ZZ9.99')' ) TO #PRT-AMT ( 4 )
            pnd_Prt_Amt.getValue(5).setValueEdited(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rechar_Amt(),new ReportEditMask("' ('ZZZ,ZZZ,ZZ9.99')'"));                             //Natural: MOVE EDITED #F96-FF1.TWRC-RECHAR-AMT ( EM = ' ('ZZZ,ZZZ,ZZ9.99')' ) TO #PRT-AMT ( 5 )
            pnd_Prt_Amt.getValue(6).setValueEdited(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Conversion_Amt(),new ReportEditMask("' ('ZZZ,ZZZ,ZZ9.99')'"));                    //Natural: MOVE EDITED #F96-FF1.TWRC-ROTH-CONVERSION-AMT ( EM = ' ('ZZZ,ZZZ,ZZ9.99')' ) TO #PRT-AMT ( 6 )
            pnd_Prt_Amt.getValue(7).setValueEdited(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Fair_Mkt_Val_Amt(),new ReportEditMask("' ('ZZZ,ZZZ,ZZ9.99')'"));                       //Natural: MOVE EDITED #F96-FF1.TWRC-FAIR-MKT-VAL-AMT ( EM = ' ('ZZZ,ZZZ,ZZ9.99')' ) TO #PRT-AMT ( 7 )
            //*  DASDH
            pnd_Prt_Amt.getValue(8).setValueEdited(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Postpn_Amt(),new ReportEditMask("' ('ZZZ,ZZZ,ZZ9.99')'"));                             //Natural: MOVE EDITED #F96-FF1.TWRC-POSTPN-AMT ( EM = ' ('ZZZ,ZZZ,ZZ9.99')' ) TO #PRT-AMT ( 8 )
            //*  SAIK
            pnd_Prt_Amt.getValue(9).setValueEdited(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Repayments_Amt(),new ReportEditMask("' ('ZZZ,ZZZ,ZZ9.99')'"));                         //Natural: MOVE EDITED #F96-FF1.TWRC-REPAYMENTS-AMT ( EM = ' ('ZZZ,ZZZ,ZZ9.99')' ) TO #PRT-AMT ( 9 )
            //*  BK
            //*  DASDH
            //*  SAIK
            getReports().write(1, ReportOption.NOTITLE,pnd_Prt_Var_Pnd_Prt3,new ColumnSpacing(1),pnd_Contract,new ColumnSpacing(6),pnd_Prt_Amt.getValue(1),new            //Natural: WRITE ( 01 ) #PRT3 1X #CONTRACT 6X #PRT-AMT ( 1 ) 5X #PRT-AMT ( 2 ) 4X #PRT-AMT ( 3 ) 3X #PRT-AMT ( 4 ) / 23T #F96-FF1.TWRC-TAX-ID 40T #PRT-AMT ( 8 ) 61T #PRT-AMT ( 5 ) 4X #PRT-AMT ( 6 ) 3X #PRT-AMT ( 7 ) / 40T #PRT-AMT ( 9 )
                ColumnSpacing(5),pnd_Prt_Amt.getValue(2),new ColumnSpacing(4),pnd_Prt_Amt.getValue(3),new ColumnSpacing(3),pnd_Prt_Amt.getValue(4),NEWLINE,new 
                TabSetting(23),ldaTwrl190a.getPnd_F96_Ff1_Twrc_Tax_Id(),new TabSetting(40),pnd_Prt_Amt.getValue(8),new TabSetting(61),pnd_Prt_Amt.getValue(5),new 
                ColumnSpacing(4),pnd_Prt_Amt.getValue(6),new ColumnSpacing(3),pnd_Prt_Amt.getValue(7),NEWLINE,new TabSetting(40),pnd_Prt_Amt.getValue(9));
            if (Global.isEscape()) return;
            pnd_Prt_Amt.getValue("*").reset();                                                                                                                            //Natural: RESET #PRT-AMT ( * )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Contract.reset();                                                                                                                                             //Natural: RESET #CONTRACT
    }
    private void sub_Print_Orig_Source() throws Exception                                                                                                                 //Natural: PRINT-ORIG-SOURCE
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        //*   --  PRINT ORIGINAL SOURCE THAT WAS INACTIVATED  --
                                                                                                                                                                          //Natural: PERFORM CHECK-SOURCE-DESC
        sub_Check_Source_Desc();
        if (condition(Global.isEscape())) {return;}
        pnd_Prt_Var_Pnd_Prt3.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Prt_Source_Pnd_Prt_Source_Org, pnd_Prt_Var_Pnd_Prt1));                          //Natural: COMPRESS #PRT-SOURCE-ORG #PRT1 INTO #PRT3 LEAVING NO SPACE
        pnd_Wfslash_Orig_Sc.setValue(true);                                                                                                                               //Natural: ASSIGN #W/ORIG-SC := TRUE
        pnd_Contract.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl190a.getPnd_F96_Ff1_Twrc_Contract(), "-", ldaTwrl190a.getPnd_F96_Ff1_Twrc_Payee())); //Natural: COMPRESS #F96-FF1.TWRC-CONTRACT '-' #F96-FF1.TWRC-PAYEE INTO #CONTRACT LEAVING NO SPACE
        //*  BK
        //*  DASDH
        //*  SAIK
        getReports().write(1, ReportOption.NOTITLE,pnd_Prt_Var_Pnd_Prt3,new ColumnSpacing(1),pnd_Contract,new ColumnSpacing(7),ldaTwrl190a.getPnd_F96_Ff1_Twrc_Classic_Amt(),  //Natural: WRITE ( 01 ) #PRT3 1X #CONTRACT 7X #F96-FF1.TWRC-CLASSIC-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #F96-FF1.TWRC-ROTH-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #F96-FF1.TWRC-SEP-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) 5X #F96-FF1.TWRC-ROLLOVER-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) / 23T #F96-FF1.TWRC-TAX-ID 40T #F96-FF1.TWRC-POSTPN-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) 62T #F96-FF1.TWRC-RECHAR-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #F96-FF1.TWRC-ROTH-CONVERSION-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) 5X #F96-FF1.TWRC-FAIR-MKT-VAL-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) / 40T #F96-FF1.TWRC-REPAYMENTS-AMT ( EM = ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(6),ldaTwrl190a.getPnd_F96_Ff1_Twrc_Sep_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(5),ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rollover_Amt(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(23),ldaTwrl190a.getPnd_F96_Ff1_Twrc_Tax_Id(),new TabSetting(40),ldaTwrl190a.getPnd_F96_Ff1_Twrc_Postpn_Amt(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(62),ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rechar_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(6),ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Conversion_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(5),ldaTwrl190a.getPnd_F96_Ff1_Twrc_Fair_Mkt_Val_Amt(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(40),ldaTwrl190a.getPnd_F96_Ff1_Twrc_Repayments_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #I 1 2
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(2)); pnd_I.nadd(1))
        {
            pnd_Total_Var_Pnd_Count.getValue(pnd_I).nadd(1);                                                                                                              //Natural: ADD 1 TO #COUNT ( #I )
            pnd_Total_Var_Pnd_Classic_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Classic_Amt());                                                            //Natural: ADD #F96-FF1.TWRC-CLASSIC-AMT TO #CLASSIC-AMT ( #I )
            pnd_Total_Var_Pnd_Roth_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Amt());                                                                  //Natural: ADD #F96-FF1.TWRC-ROTH-AMT TO #ROTH-AMT ( #I )
            pnd_Total_Var_Pnd_Sep_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Sep_Amt());                                                                    //Natural: ADD #F96-FF1.TWRC-SEP-AMT TO #SEP-AMT ( #I )
            pnd_Total_Var_Pnd_Rollovr_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rollover_Amt());                                                           //Natural: ADD #F96-FF1.TWRC-ROLLOVER-AMT TO #ROLLOVR-AMT ( #I )
            pnd_Total_Var_Pnd_Rechar_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rechar_Amt());                                                              //Natural: ADD #F96-FF1.TWRC-RECHAR-AMT TO #RECHAR-AMT ( #I )
            pnd_Total_Var_Pnd_Conv_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Conversion_Amt());                                                       //Natural: ADD #F96-FF1.TWRC-ROTH-CONVERSION-AMT TO #CONV-AMT ( #I )
            pnd_Total_Var_Pnd_Fmv_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Fair_Mkt_Val_Amt());                                                           //Natural: ADD #F96-FF1.TWRC-FAIR-MKT-VAL-AMT TO #FMV-AMT ( #I )
            //*  DASDH
            pnd_Total_Var_Pnd_Postpn_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Postpn_Amt());                                                              //Natural: ADD #F96-FF1.TWRC-POSTPN-AMT TO #POSTPN-AMT ( #I )
            //*  SAIK
            pnd_Total_Var_Pnd_Repayments_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Repayments_Amt());                                                      //Natural: ADD #F96-FF1.TWRC-REPAYMENTS-AMT TO #REPAYMENTS-AMT ( #I )
            //*  SAIK
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Keep_Inactive_Record() throws Exception                                                                                                              //Natural: KEEP-INACTIVE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------
        //*   --  STORE INACTIVATED RECORD FOR PRINTING  --
        if (condition(pnd_Wfslash_Orig_Sc.equals(true)))                                                                                                                  //Natural: IF #W/ORIG-SC = TRUE
        {
            pnd_Wfslash_Orig_Sc.setValue(false);                                                                                                                          //Natural: ASSIGN #W/ORIG-SC := FALSE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM CHECK-SOURCE-DESC
            sub_Check_Source_Desc();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Prt4.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Prt_Source_Pnd_Prt_Source_Org, pnd_Prt_Var_Pnd_Prt1, pnd_Prt_Var_Pnd_Prt2));                //Natural: COMPRESS #PRT-SOURCE-ORG #PRT1 #PRT2 INTO #PRT4 LEAVING NO SPACE
        if (condition(pnd_Contract.equals(" ")))                                                                                                                          //Natural: IF #CONTRACT = ' '
        {
            pnd_Contract.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl190a.getPnd_F96_Ff1_Twrc_Contract(), "-", ldaTwrl190a.getPnd_F96_Ff1_Twrc_Payee())); //Natural: COMPRESS #F96-FF1.TWRC-CONTRACT '-' #F96-FF1.TWRC-PAYEE INTO #CONTRACT LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Prt_Source_Pnd_Prt_Source_Org.equals("P1 ") || pnd_Prt_Source_Pnd_Prt_Source_Org.equals("P2 ") || pnd_Prt_Source_Pnd_Prt_Source_Org.equals("P11")  //Natural: IF #PRT-SOURCE-ORG = 'P1 ' OR = 'P2 ' OR = 'P11' OR = 'P22'
            || pnd_Prt_Source_Pnd_Prt_Source_Org.equals("P22")))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ind.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #IND
            getReports().display(0, pnd_Ind,pnd_Prt4,pnd_Contract,ldaTwrl190a.getPnd_F96_Ff1_Twrc_Tax_Id());                                                              //Natural: DISPLAY #IND #PRT4 #CONTRACT #F96-FF1.TWRC-TAX-ID
            if (Global.isEscape()) return;
            pnd_Print_Var_Pnd_Print_Source.getValue(pnd_Ind).setValue(pnd_Prt4);                                                                                          //Natural: MOVE #PRT4 TO #PRINT-SOURCE ( #IND )
            pnd_Print_Var_Pnd_Print_Contract.getValue(pnd_Ind).setValue(pnd_Contract);                                                                                    //Natural: MOVE #CONTRACT TO #PRINT-CONTRACT ( #IND )
            pnd_Print_Var_Pnd_Print_Tax_Id.getValue(pnd_Ind).setValue(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Tax_Id());                                                          //Natural: MOVE #F96-FF1.TWRC-TAX-ID TO #PRINT-TAX-ID ( #IND )
            pnd_Print_Var_Pnd_Print_Classic.getValue(pnd_Ind).setValue(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Classic_Amt());                                                    //Natural: MOVE #F96-FF1.TWRC-CLASSIC-AMT TO #PRINT-CLASSIC ( #IND )
            pnd_Print_Var_Pnd_Print_Roth.getValue(pnd_Ind).setValue(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Amt());                                                          //Natural: MOVE #F96-FF1.TWRC-ROTH-AMT TO #PRINT-ROTH ( #IND )
            pnd_Print_Var_Pnd_Print_Sep.getValue(pnd_Ind).setValue(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Sep_Amt());                                                            //Natural: MOVE #F96-FF1.TWRC-SEP-AMT TO #PRINT-SEP ( #IND )
            pnd_Print_Var_Pnd_Print_Rollover.getValue(pnd_Ind).setValue(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rollover_Amt());                                                  //Natural: MOVE #F96-FF1.TWRC-ROLLOVER-AMT TO #PRINT-ROLLOVER ( #IND )
            pnd_Print_Var_Pnd_Print_Rechar.getValue(pnd_Ind).setValue(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rechar_Amt());                                                      //Natural: MOVE #F96-FF1.TWRC-RECHAR-AMT TO #PRINT-RECHAR ( #IND )
            pnd_Print_Var_Pnd_Print_Conv.getValue(pnd_Ind).setValue(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Conversion_Amt());                                               //Natural: MOVE #F96-FF1.TWRC-ROTH-CONVERSION-AMT TO #PRINT-CONV ( #IND )
            pnd_Print_Var_Pnd_Print_Fmv.getValue(pnd_Ind).setValue(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Fair_Mkt_Val_Amt());                                                   //Natural: MOVE #F96-FF1.TWRC-FAIR-MKT-VAL-AMT TO #PRINT-FMV ( #IND )
            //*  DASDH
            pnd_Print_Var_Pnd_Print_Postpn.getValue(pnd_Ind).setValue(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Postpn_Amt());                                                      //Natural: MOVE #F96-FF1.TWRC-POSTPN-AMT TO #PRINT-POSTPN ( #IND )
            //*  DASDH
            //*  SAIK
            pnd_Print_Var_Pnd_Print_Repayments.getValue(pnd_Ind).setValue(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Repayments_Amt());                                              //Natural: MOVE #F96-FF1.TWRC-REPAYMENTS-AMT TO #PRINT-REPAYMENTS ( #IND )
            //*  SAIK
            pnd_Contract.reset();                                                                                                                                         //Natural: RESET #CONTRACT #PRT4
            pnd_Prt4.reset();
        }                                                                                                                                                                 //Natural: END-IF
        FOR04:                                                                                                                                                            //Natural: FOR #I 2 3
        for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(3)); pnd_I.nadd(1))
        {
            pnd_Total_Var_Pnd_Count.getValue(pnd_I).nsubtract(1);                                                                                                         //Natural: SUBTRACT 1 FROM #COUNT ( #I )
            pnd_Total_Var_Pnd_Classic_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Classic_Amt());                                                       //Natural: SUBTRACT #F96-FF1.TWRC-CLASSIC-AMT FROM #CLASSIC-AMT ( #I )
            pnd_Total_Var_Pnd_Roth_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Amt());                                                             //Natural: SUBTRACT #F96-FF1.TWRC-ROTH-AMT FROM #ROTH-AMT ( #I )
            pnd_Total_Var_Pnd_Sep_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Sep_Amt());                                                               //Natural: SUBTRACT #F96-FF1.TWRC-SEP-AMT FROM #SEP-AMT ( #I )
            pnd_Total_Var_Pnd_Rollovr_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rollover_Amt());                                                      //Natural: SUBTRACT #F96-FF1.TWRC-ROLLOVER-AMT FROM #ROLLOVR-AMT ( #I )
            pnd_Total_Var_Pnd_Rechar_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rechar_Amt());                                                         //Natural: SUBTRACT #F96-FF1.TWRC-RECHAR-AMT FROM #RECHAR-AMT ( #I )
            pnd_Total_Var_Pnd_Conv_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Conversion_Amt());                                                  //Natural: SUBTRACT #F96-FF1.TWRC-ROTH-CONVERSION-AMT FROM #CONV-AMT ( #I )
            pnd_Total_Var_Pnd_Fmv_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Fair_Mkt_Val_Amt());                                                      //Natural: SUBTRACT #F96-FF1.TWRC-FAIR-MKT-VAL-AMT FROM #FMV-AMT ( #I )
            //*  DASDH
            pnd_Total_Var_Pnd_Postpn_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Postpn_Amt());                                                         //Natural: SUBTRACT #F96-FF1.TWRC-POSTPN-AMT FROM #POSTPN-AMT ( #I )
            //*  DASDH
            //*  SAIK
            pnd_Total_Var_Pnd_Repayments_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Repayments_Amt());                                                 //Natural: SUBTRACT #F96-FF1.TWRC-REPAYMENTS-AMT FROM #REPAYMENTS-AMT ( #I )
            //*  SAIK
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Print_Subtotal() throws Exception                                                                                                                    //Natural: PRINT-SUBTOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------
        if (condition(pnd_Saver_Pnd_Sv_Status.equals("I")))                                                                                                               //Natural: IF #SV-STATUS = 'I'
        {
            if (condition(pnd_Total_Var_Pnd_Count.getValue(3).equals(getZero())))                                                                                         //Natural: IF #COUNT ( 3 ) = 0
            {
                //*             ------- INACTIVE BUT WITH NO SAVED INACTIVE RECORDS --
                pnd_Prt_Var_Pnd_Prt3.reset();                                                                                                                             //Natural: RESET #PRT3
                pnd_Prt_Var_Pnd_Prt3.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "SUBTOTAL ( ", pnd_Prt_Source_Pnd_Prt_Source_Org, pnd_Prt_Var_Pnd_Prt1,     //Natural: COMPRESS 'SUBTOTAL ( ' #PRT-SOURCE-ORG #PRT1 ' - I)' INTO #PRT3 LEAVING NO SPACE
                    " - I)"));
                pnd_Ttl_Cnt.setValueEdited(pnd_Total_Var_Pnd_Count.getValue(1),new ReportEditMask("' ('Z,ZZZ,ZZ9')'"));                                                   //Natural: MOVE EDITED #COUNT ( 1 ) ( EM = ' ('Z,ZZZ,ZZ9')' ) TO #TTL-CNT
                pnd_Ttl_Amt.getValue(1).setValueEdited(pnd_Total_Var_Pnd_Classic_Amt.getValue(1),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                         //Natural: MOVE EDITED #CLASSIC-AMT ( 1 ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #TTL-AMT ( 1 )
                pnd_Ttl_Amt.getValue(2).setValueEdited(pnd_Total_Var_Pnd_Roth_Amt.getValue(1),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                            //Natural: MOVE EDITED #ROTH-AMT ( 1 ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #TTL-AMT ( 2 )
                pnd_Ttl_Amt.getValue(3).setValueEdited(pnd_Total_Var_Pnd_Sep_Amt.getValue(1),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                             //Natural: MOVE EDITED #SEP-AMT ( 1 ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #TTL-AMT ( 3 )
                pnd_Ttl_Amt.getValue(4).setValueEdited(pnd_Total_Var_Pnd_Rollovr_Amt.getValue(1),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                         //Natural: MOVE EDITED #ROLLOVR-AMT ( 1 ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #TTL-AMT ( 4 )
                pnd_Ttl_Amt.getValue(5).setValueEdited(pnd_Total_Var_Pnd_Rechar_Amt.getValue(1),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                          //Natural: MOVE EDITED #RECHAR-AMT ( 1 ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #TTL-AMT ( 5 )
                pnd_Ttl_Amt.getValue(6).setValueEdited(pnd_Total_Var_Pnd_Conv_Amt.getValue(1),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                            //Natural: MOVE EDITED #CONV-AMT ( 1 ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #TTL-AMT ( 6 )
                pnd_Ttl_Amt.getValue(7).setValueEdited(pnd_Total_Var_Pnd_Fmv_Amt.getValue(1),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                             //Natural: MOVE EDITED #FMV-AMT ( 1 ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #TTL-AMT ( 7 )
                //*  DASDH
                pnd_Ttl_Amt.getValue(8).setValueEdited(pnd_Total_Var_Pnd_Postpn_Amt.getValue(1),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                          //Natural: MOVE EDITED #POSTPN-AMT ( 1 ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #TTL-AMT ( 8 )
                //*  SAIK
                pnd_Ttl_Amt.getValue(9).setValueEdited(pnd_Total_Var_Pnd_Repayments_Amt.getValue(1),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                      //Natural: MOVE EDITED #REPAYMENTS-AMT ( 1 ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #TTL-AMT ( 9 )
                //*  BK
                //*  DASDH
                //*  SAIK
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Prt_Var_Pnd_Prt3,new TabSetting(23),pnd_Ttl_Cnt,new TabSetting(37),pnd_Ttl_Amt.getValue(1),new     //Natural: WRITE ( 01 ) / #PRT3 23T #TTL-CNT 37T #TTL-AMT ( 1 ) 2X #TTL-AMT ( 2 ) 1X #TTL-AMT ( 3 ) 1X #TTL-AMT ( 4 ) /35T #TTL-AMT ( 8 ) 58T #TTL-AMT ( 5 ) 1X #TTL-AMT ( 6 ) 1X #TTL-AMT ( 7 ) /35T #TTL-AMT ( 9 ) /
                    ColumnSpacing(2),pnd_Ttl_Amt.getValue(2),new ColumnSpacing(1),pnd_Ttl_Amt.getValue(3),new ColumnSpacing(1),pnd_Ttl_Amt.getValue(4),NEWLINE,new 
                    TabSetting(35),pnd_Ttl_Amt.getValue(8),new TabSetting(58),pnd_Ttl_Amt.getValue(5),new ColumnSpacing(1),pnd_Ttl_Amt.getValue(6),new ColumnSpacing(1),pnd_Ttl_Amt.getValue(7),NEWLINE,new 
                    TabSetting(35),pnd_Ttl_Amt.getValue(9),NEWLINE);
                if (Global.isEscape()) return;
                pnd_Total_Var.getValue(1).reset();                                                                                                                        //Natural: RESET #TOTAL-VAR ( 1 ) #TTL-CNT #TTL-AMT ( * )
                pnd_Ttl_Cnt.reset();
                pnd_Ttl_Amt.getValue("*").reset();
                //*      ------  WITH SAVED INACTIVED RECORDS  -----
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Total_Var_Pnd_Count.getValue(1).notEquals(getZero())))                                                                                  //Natural: IF #COUNT ( 1 ) NE 0
                {
                    //*  1) PRINT ACTIVE SUBTOTAL
                                                                                                                                                                          //Natural: PERFORM PRINT-SUBTOTAL-ACTIVE
                    sub_Print_Subtotal_Active();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                FOR05:                                                                                                                                                    //Natural: FOR #I 1 #IND
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Ind)); pnd_I.nadd(1))
                {
                    pnd_Prt_Amt.getValue(1).setValueEdited(pnd_Print_Var_Pnd_Print_Classic.getValue(pnd_I),new ReportEditMask("' ('ZZZ,ZZZ,ZZ9.99')'"));                  //Natural: MOVE EDITED #PRINT-CLASSIC ( #I ) ( EM = ' ('ZZZ,ZZZ,ZZ9.99')' ) TO #PRT-AMT ( 1 )
                    pnd_Prt_Amt.getValue(2).setValueEdited(pnd_Print_Var_Pnd_Print_Roth.getValue(pnd_I),new ReportEditMask("' ('ZZZ,ZZZ,ZZ9.99')'"));                     //Natural: MOVE EDITED #PRINT-ROTH ( #I ) ( EM = ' ('ZZZ,ZZZ,ZZ9.99')' ) TO #PRT-AMT ( 2 )
                    pnd_Prt_Amt.getValue(3).setValueEdited(pnd_Print_Var_Pnd_Print_Sep.getValue(pnd_I),new ReportEditMask("' ('ZZZ,ZZZ,ZZ9.99')'"));                      //Natural: MOVE EDITED #PRINT-SEP ( #I ) ( EM = ' ('ZZZ,ZZZ,ZZ9.99')' ) TO #PRT-AMT ( 3 )
                    pnd_Prt_Amt.getValue(4).setValueEdited(pnd_Print_Var_Pnd_Print_Rollover.getValue(pnd_I),new ReportEditMask("' ('ZZZ,ZZZ,ZZ9.99')'"));                 //Natural: MOVE EDITED #PRINT-ROLLOVER ( #I ) ( EM = ' ('ZZZ,ZZZ,ZZ9.99')' ) TO #PRT-AMT ( 4 )
                    pnd_Prt_Amt.getValue(5).setValueEdited(pnd_Print_Var_Pnd_Print_Rechar.getValue(pnd_I),new ReportEditMask("' ('ZZZ,ZZZ,ZZ9.99')'"));                   //Natural: MOVE EDITED #PRINT-RECHAR ( #I ) ( EM = ' ('ZZZ,ZZZ,ZZ9.99')' ) TO #PRT-AMT ( 5 )
                    pnd_Prt_Amt.getValue(6).setValueEdited(pnd_Print_Var_Pnd_Print_Conv.getValue(pnd_I),new ReportEditMask("' ('ZZZ,ZZZ,ZZ9.99')'"));                     //Natural: MOVE EDITED #PRINT-CONV ( #I ) ( EM = ' ('ZZZ,ZZZ,ZZ9.99')' ) TO #PRT-AMT ( 6 )
                    pnd_Prt_Amt.getValue(7).setValueEdited(pnd_Print_Var_Pnd_Print_Fmv.getValue(pnd_I),new ReportEditMask("' ('ZZZ,ZZZ,ZZ9.99')'"));                      //Natural: MOVE EDITED #PRINT-FMV ( #I ) ( EM = ' ('ZZZ,ZZZ,ZZ9.99')' ) TO #PRT-AMT ( 7 )
                    //*  DASDH
                    pnd_Prt_Amt.getValue(8).setValueEdited(pnd_Print_Var_Pnd_Print_Postpn.getValue(pnd_I),new ReportEditMask("' ('ZZZ,ZZZ,ZZ9.99')'"));                   //Natural: MOVE EDITED #PRINT-POSTPN ( #I ) ( EM = ' ('ZZZ,ZZZ,ZZ9.99')' ) TO #PRT-AMT ( 8 )
                    //*  SAIK
                    pnd_Prt_Amt.getValue(9).setValueEdited(pnd_Print_Var_Pnd_Print_Repayments.getValue(pnd_I),new ReportEditMask("' ('ZZZ,ZZZ,ZZ9.99')'"));               //Natural: MOVE EDITED #PRINT-REPAYMENTS ( #I ) ( EM = ' ('ZZZ,ZZZ,ZZ9.99')' ) TO #PRT-AMT ( 9 )
                    //*  BK
                    //*  DASDH
                    //*  SAIK
                    getReports().write(1, ReportOption.NOTITLE,pnd_Print_Var_Pnd_Print_Source.getValue(pnd_I),new ColumnSpacing(1),pnd_Print_Var_Pnd_Print_Contract.getValue(pnd_I),new  //Natural: WRITE ( 01 ) #PRINT-SOURCE ( #I ) 1X #PRINT-CONTRACT ( #I ) 6X #PRT-AMT ( 1 ) 5X #PRT-AMT ( 2 ) 4X #PRT-AMT ( 3 ) 3X #PRT-AMT ( 4 ) / 22T #PRINT-TAX-ID ( #I ) 40T #PRT-AMT ( 8 ) 61T #PRT-AMT ( 5 ) 4X #PRT-AMT ( 6 ) 3X #PRT-AMT ( 7 ) / 40T #PRT-AMT ( 9 )
                        ColumnSpacing(6),pnd_Prt_Amt.getValue(1),new ColumnSpacing(5),pnd_Prt_Amt.getValue(2),new ColumnSpacing(4),pnd_Prt_Amt.getValue(3),new 
                        ColumnSpacing(3),pnd_Prt_Amt.getValue(4),NEWLINE,new TabSetting(22),pnd_Print_Var_Pnd_Print_Tax_Id.getValue(pnd_I),new TabSetting(40),pnd_Prt_Amt.getValue(8),new 
                        TabSetting(61),pnd_Prt_Amt.getValue(5),new ColumnSpacing(4),pnd_Prt_Amt.getValue(6),new ColumnSpacing(3),pnd_Prt_Amt.getValue(7),NEWLINE,new 
                        TabSetting(40),pnd_Prt_Amt.getValue(9));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Prt_Amt.getValue("*").reset();                                                                                                                    //Natural: RESET #PRT-AMT ( * )
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*                                    3) PRINT SUBTOTAL OF SAVED I RECORDS
                pnd_Prt_Var_Pnd_Prt3.reset();                                                                                                                             //Natural: RESET #PRT3
                pnd_Prt_Var_Pnd_Prt3.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "SUBTOTAL ( ", pnd_Prt_Source_Pnd_Prt_Source_Org, pnd_Prt_Var_Pnd_Prt1,     //Natural: COMPRESS 'SUBTOTAL ( ' #PRT-SOURCE-ORG #PRT1 ' - I)' INTO #PRT3 LEAVING NO SPACE
                    " - I)"));
                pnd_Ttl_Cnt.setValueEdited(pnd_Total_Var_Pnd_Count.getValue(3),new ReportEditMask("' ('Z,ZZZ,ZZ9')'"));                                                   //Natural: MOVE EDITED #COUNT ( 3 ) ( EM = ' ('Z,ZZZ,ZZ9')' ) TO #TTL-CNT
                pnd_Ttl_Amt.getValue(1).setValueEdited(pnd_Total_Var_Pnd_Classic_Amt.getValue(3),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                         //Natural: MOVE EDITED #CLASSIC-AMT ( 3 ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #TTL-AMT ( 1 )
                pnd_Ttl_Amt.getValue(2).setValueEdited(pnd_Total_Var_Pnd_Roth_Amt.getValue(3),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                            //Natural: MOVE EDITED #ROTH-AMT ( 3 ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #TTL-AMT ( 2 )
                pnd_Ttl_Amt.getValue(3).setValueEdited(pnd_Total_Var_Pnd_Sep_Amt.getValue(3),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                             //Natural: MOVE EDITED #SEP-AMT ( 3 ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #TTL-AMT ( 3 )
                pnd_Ttl_Amt.getValue(4).setValueEdited(pnd_Total_Var_Pnd_Rollovr_Amt.getValue(3),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                         //Natural: MOVE EDITED #ROLLOVR-AMT ( 3 ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #TTL-AMT ( 4 )
                pnd_Ttl_Amt.getValue(5).setValueEdited(pnd_Total_Var_Pnd_Rechar_Amt.getValue(3),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                          //Natural: MOVE EDITED #RECHAR-AMT ( 3 ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #TTL-AMT ( 5 )
                pnd_Ttl_Amt.getValue(6).setValueEdited(pnd_Total_Var_Pnd_Conv_Amt.getValue(3),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                            //Natural: MOVE EDITED #CONV-AMT ( 3 ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #TTL-AMT ( 6 )
                pnd_Ttl_Amt.getValue(7).setValueEdited(pnd_Total_Var_Pnd_Fmv_Amt.getValue(3),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                             //Natural: MOVE EDITED #FMV-AMT ( 3 ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #TTL-AMT ( 7 )
                //*  DASDH
                pnd_Ttl_Amt.getValue(8).setValueEdited(pnd_Total_Var_Pnd_Postpn_Amt.getValue(3),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                          //Natural: MOVE EDITED #POSTPN-AMT ( 3 ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #TTL-AMT ( 8 )
                //*  SAIK
                pnd_Ttl_Amt.getValue(9).setValueEdited(pnd_Total_Var_Pnd_Repayments_Amt.getValue(3),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                      //Natural: MOVE EDITED #REPAYMENTS-AMT ( 3 ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #TTL-AMT ( 9 )
                //*  BK
                //*  DASDH
                //*  SAIK
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Prt_Var_Pnd_Prt3,new TabSetting(23),pnd_Ttl_Cnt,new TabSetting(37),pnd_Ttl_Amt.getValue(1),new     //Natural: WRITE ( 01 ) / #PRT3 23T #TTL-CNT 37T #TTL-AMT ( 1 ) 2X #TTL-AMT ( 2 ) 1X #TTL-AMT ( 3 ) 1X #TTL-AMT ( 4 ) / 35T #TTL-AMT ( 8 ) 58T #TTL-AMT ( 5 ) 1X #TTL-AMT ( 6 ) 1X #TTL-AMT ( 7 ) / 35T #TTL-AMT ( 9 ) /
                    ColumnSpacing(2),pnd_Ttl_Amt.getValue(2),new ColumnSpacing(1),pnd_Ttl_Amt.getValue(3),new ColumnSpacing(1),pnd_Ttl_Amt.getValue(4),NEWLINE,new 
                    TabSetting(35),pnd_Ttl_Amt.getValue(8),new TabSetting(58),pnd_Ttl_Amt.getValue(5),new ColumnSpacing(1),pnd_Ttl_Amt.getValue(6),new ColumnSpacing(1),pnd_Ttl_Amt.getValue(7),NEWLINE,new 
                    TabSetting(35),pnd_Ttl_Amt.getValue(9),NEWLINE);
                if (Global.isEscape()) return;
                pnd_Print_Var.getValue("*").reset();                                                                                                                      //Natural: RESET #PRINT-VAR ( * ) #IND #TOTAL-VAR ( 3 )
                pnd_Ind.reset();
                pnd_Total_Var.getValue(3).reset();
            }                                                                                                                                                             //Natural: END-IF
            //*  #SV-STATUS = ' '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUBTOTAL-ACTIVE
            sub_Print_Subtotal_Active();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Prt_Source.reset();                                                                                                                                           //Natural: RESET #PRT-SOURCE #PRT-VAR
        pnd_Prt_Var.reset();
    }
    private void sub_Print_Subtotal_Active() throws Exception                                                                                                             //Natural: PRINT-SUBTOTAL-ACTIVE
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------
        pnd_Prt5.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "SUBTOTAL ( ", pnd_Prt_Source_Pnd_Prt_Source_Org, pnd_Prt_Var_Pnd_Prt1, ")"));                  //Natural: COMPRESS 'SUBTOTAL ( ' #PRT-SOURCE-ORG #PRT1 ')' INTO #PRT5 LEAVING NO SPACE
        //*  BK
        //*  DASDH
        //*  SAIK
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Prt5,new TabSetting(24),pnd_Total_Var_Pnd_Count.getValue(1), new ReportEditMask ("Z,ZZZ,ZZ9"),new          //Natural: WRITE ( 01 ) / #PRT5 24T #COUNT ( 1 ) ( EM = Z,ZZZ,ZZ9 ) 38T #CLASSIC-AMT ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 4X #ROTH-AMT ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 3X #SEP-AMT ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 3X #ROLLOVR-AMT ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 40T #POSTPN-AMT ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 59T #RECHAR-AMT ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 3X #CONV-AMT ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 3X #FMV-AMT ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 40T #REPAYMENTS-AMT ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) /
            TabSetting(38),pnd_Total_Var_Pnd_Classic_Amt.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(4),pnd_Total_Var_Pnd_Roth_Amt.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Total_Var_Pnd_Sep_Amt.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(3),pnd_Total_Var_Pnd_Rollovr_Amt.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(40),pnd_Total_Var_Pnd_Postpn_Amt.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(59),pnd_Total_Var_Pnd_Rechar_Amt.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(3),pnd_Total_Var_Pnd_Conv_Amt.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Total_Var_Pnd_Fmv_Amt.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(40),pnd_Total_Var_Pnd_Repayments_Amt.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),
            NEWLINE);
        if (Global.isEscape()) return;
        pnd_Total_Var.getValue(1).reset();                                                                                                                                //Natural: RESET #TOTAL-VAR ( 1 ) #PRT5
        pnd_Prt5.reset();
    }
    private void sub_Print_Grandtotal() throws Exception                                                                                                                  //Natural: PRINT-GRANDTOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
                                                                                                                                                                          //Natural: PERFORM PRINT-SUBTOTAL
        sub_Print_Subtotal();
        if (condition(Global.isEscape())) {return;}
        //*  DASDH
        //*  SAIK
        pnd_Prt_Var_Pnd_Prt3.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "GRANDTOTAL ( ", pnd_Print_Com, ")"));                                              //Natural: COMPRESS 'GRANDTOTAL ( ' #PRINT-COM ')' INTO #PRT3 LEAVING NO SPACE
        pnd_Temp_Amt.getValue(1).setValue(pnd_Total_Var_Pnd_Classic_Amt.getValue(2));                                                                                     //Natural: ASSIGN #TEMP-AMT ( 1 ) := #CLASSIC-AMT ( 2 )
        pnd_Temp_Amt.getValue(2).setValue(pnd_Total_Var_Pnd_Roth_Amt.getValue(2));                                                                                        //Natural: ASSIGN #TEMP-AMT ( 2 ) := #ROTH-AMT ( 2 )
        pnd_Temp_Amt.getValue(3).setValue(pnd_Total_Var_Pnd_Sep_Amt.getValue(2));                                                                                         //Natural: ASSIGN #TEMP-AMT ( 3 ) := #SEP-AMT ( 2 )
        pnd_Temp_Amt.getValue(4).setValue(pnd_Total_Var_Pnd_Rollovr_Amt.getValue(2));                                                                                     //Natural: ASSIGN #TEMP-AMT ( 4 ) := #ROLLOVR-AMT ( 2 )
        pnd_Temp_Amt.getValue(5).setValue(pnd_Total_Var_Pnd_Rechar_Amt.getValue(2));                                                                                      //Natural: ASSIGN #TEMP-AMT ( 5 ) := #RECHAR-AMT ( 2 )
        pnd_Temp_Amt.getValue(6).setValue(pnd_Total_Var_Pnd_Conv_Amt.getValue(2));                                                                                        //Natural: ASSIGN #TEMP-AMT ( 6 ) := #CONV-AMT ( 2 )
        pnd_Temp_Amt.getValue(7).setValue(pnd_Total_Var_Pnd_Fmv_Amt.getValue(2));                                                                                         //Natural: ASSIGN #TEMP-AMT ( 7 ) := #FMV-AMT ( 2 )
        pnd_Temp_Amt.getValue(8).setValue(pnd_Total_Var_Pnd_Postpn_Amt.getValue(2));                                                                                      //Natural: ASSIGN #TEMP-AMT ( 8 ) := #POSTPN-AMT ( 2 )
        pnd_Temp_Amt.getValue(9).setValue(pnd_Total_Var_Pnd_Repayments_Amt.getValue(2));                                                                                  //Natural: ASSIGN #TEMP-AMT ( 9 ) := #REPAYMENTS-AMT ( 2 )
        if (condition(pnd_Total_Var_Pnd_Count.getValue(2).less(getZero())))                                                                                               //Natural: IF #COUNT ( 2 ) < 0
        {
            pnd_Ttl_Cnt.setValueEdited(pnd_Total_Var_Pnd_Count.getValue(2),new ReportEditMask("' ('Z,ZZZ,ZZ9')'"));                                                       //Natural: MOVE EDITED #COUNT ( 2 ) ( EM = ' ('Z,ZZZ,ZZ9')' ) TO #TTL-CNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ttl_Cnt.setValueEdited(pnd_Total_Var_Pnd_Count.getValue(2),new ReportEditMask("'  'Z,ZZZ,ZZ9"));                                                          //Natural: MOVE EDITED #COUNT ( 2 ) ( EM = '  'Z,ZZZ,ZZ9 ) TO #TTL-CNT
        }                                                                                                                                                                 //Natural: END-IF
        FOR06:                                                                                                                                                            //Natural: FOR #I 1 7
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(7)); pnd_I.nadd(1))
        {
            if (condition(pnd_Temp_Amt.getValue(pnd_I).less(getZero())))                                                                                                  //Natural: IF #TEMP-AMT ( #I ) < 0
            {
                pnd_Ttl_Amt.getValue(pnd_I).setValueEdited(pnd_Temp_Amt.getValue(pnd_I),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                                  //Natural: MOVE EDITED #TEMP-AMT ( #I ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #TTL-AMT ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ttl_Amt.getValue(pnd_I).setValueEdited(pnd_Temp_Amt.getValue(pnd_I),new ReportEditMask("'  'ZZ,ZZZ,ZZZ,ZZ9.99"));                                     //Natural: MOVE EDITED #TEMP-AMT ( #I ) ( EM = '  'ZZ,ZZZ,ZZZ,ZZ9.99 ) TO #TTL-AMT ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  BK
        //*  DASDH
        //*  SAIK
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Prt_Var_Pnd_Prt3,new TabSetting(23),pnd_Ttl_Cnt,new TabSetting(37),pnd_Ttl_Amt.getValue(1),new             //Natural: WRITE ( 01 ) / #PRT3 23T #TTL-CNT 37T #TTL-AMT ( 1 ) 2X #TTL-AMT ( 2 ) 1X #TTL-AMT ( 3 ) 1X #TTL-AMT ( 4 ) /35T #TTL-AMT ( 8 ) 58T #TTL-AMT ( 5 ) 1X #TTL-AMT ( 6 ) 1X #TTL-AMT ( 7 ) /35T #TTL-AMT ( 9 )
            ColumnSpacing(2),pnd_Ttl_Amt.getValue(2),new ColumnSpacing(1),pnd_Ttl_Amt.getValue(3),new ColumnSpacing(1),pnd_Ttl_Amt.getValue(4),NEWLINE,new 
            TabSetting(35),pnd_Ttl_Amt.getValue(8),new TabSetting(58),pnd_Ttl_Amt.getValue(5),new ColumnSpacing(1),pnd_Ttl_Amt.getValue(6),new ColumnSpacing(1),pnd_Ttl_Amt.getValue(7),NEWLINE,new 
            TabSetting(35),pnd_Ttl_Amt.getValue(9));
        if (Global.isEscape()) return;
        pnd_Total_Var.getValue(2).reset();                                                                                                                                //Natural: RESET #TOTAL-VAR ( 2 ) #TTL-AMT ( * ) #PRT-VAR #TEMP-AMT ( * ) #PRINT-VAR ( * )
        pnd_Ttl_Amt.getValue("*").reset();
        pnd_Prt_Var.reset();
        pnd_Temp_Amt.getValue("*").reset();
        pnd_Print_Var.getValue("*").reset();
    }
    private void sub_Co_Desc() throws Exception                                                                                                                           //Natural: CO-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------
        short decideConditionsMet683 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SV-COMPANY;//Natural: VALUE 'C'
        if (condition((pnd_Saver_Pnd_Sv_Company.equals("C"))))
        {
            decideConditionsMet683++;
            pnd_Print_Com.setValue("CREF");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'CREF'
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pnd_Saver_Pnd_Sv_Company.equals("L"))))
        {
            decideConditionsMet683++;
            pnd_Print_Com.setValue("LIFE");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'LIFE'
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pnd_Saver_Pnd_Sv_Company.equals("T"))))
        {
            decideConditionsMet683++;
            pnd_Print_Com.setValue("TIAA");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'TIAA'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                                                                                                                                                                          //Natural: PERFORM CO-DESC
                    sub_Co_Desc();
                    if (condition(Global.isEscape())) {return;}
                    //*  BK
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 01 ) NOTITLE NOHDR *INIT-USER '-' *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 114T 'PAGE' *PAGE-NUMBER ( 01 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 39T 'IRA5498 DAILY ONLINE MAINTENANCE DETAIL REPORT' / 'RUNTIME : ' *TIMX 54T 'TAX YEAR ' #TWRP0600-TAX-YEAR-CCYY 90T 'DATE RANGE : ' #TWRP0600-FROM-CCYYMMDD ' THRU ' #TWRP0600-TO-CCYYMMDD /// 'COMPANY : ' #PRINT-COM // 23T 'CONTRACT-PY' 39T 'IRA CONTRIBUTION ' 3X 'IRA CONTRIBUTION' / 'SYSTEM SOURCE' 23T 'TAX-ID-NBR' 7X 'CLASSIC AMOUNT' 6X 'ROTH  AMOUNT' 8X 'SEP  AMOUNT' 9X 'R/O CONTRIBUTION' /40T 'LATE R/O AMT' 59T '-----------------' 4X '----------------' 4X '----------------' / 40T 'REPAYMENTS' 63T 'RECHAR-AMOUNT' 3X 'CONVERSION AMOUNT' 4X 'FAIR-MKT-VAL AMT' //
                        TabSetting(114),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(39),"IRA5498 DAILY ONLINE MAINTENANCE DETAIL REPORT",NEWLINE,"RUNTIME : ",Global.getTIMX(),new TabSetting(54),"TAX YEAR ",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy(),new 
                        TabSetting(90),"DATE RANGE : ",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd()," THRU ",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd(),NEWLINE,NEWLINE,NEWLINE,"COMPANY : ",pnd_Print_Com,NEWLINE,NEWLINE,new 
                        TabSetting(23),"CONTRACT-PY",new TabSetting(39),"IRA CONTRIBUTION ",new ColumnSpacing(3),"IRA CONTRIBUTION",NEWLINE,"SYSTEM SOURCE",new 
                        TabSetting(23),"TAX-ID-NBR",new ColumnSpacing(7),"CLASSIC AMOUNT",new ColumnSpacing(6),"ROTH  AMOUNT",new ColumnSpacing(8),"SEP  AMOUNT",new 
                        ColumnSpacing(9),"R/O CONTRIBUTION",NEWLINE,new TabSetting(40),"LATE R/O AMT",new TabSetting(59),"-----------------",new ColumnSpacing(4),"----------------",new 
                        ColumnSpacing(4),"----------------",NEWLINE,new TabSetting(40),"REPAYMENTS",new TabSetting(63),"RECHAR-AMOUNT",new ColumnSpacing(3),"CONVERSION AMOUNT",new 
                        ColumnSpacing(4),"FAIR-MKT-VAL AMT",NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");

        getReports().setDisplayColumns(0, pnd_Ind,pnd_Prt4,pnd_Contract,ldaTwrl190a.getPnd_F96_Ff1_Twrc_Tax_Id());
    }
}
