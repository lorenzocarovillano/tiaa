/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:44:54 PM
**        * FROM NATURAL PROGRAM : Twrp8162
************************************************************
**        * FILE NAME            : Twrp8162.java
**        * CLASS NAME           : Twrp8162
**        * INSTANCE NAME        : Twrp8162
************************************************************
************************************************************************
** PROGRAM : TWRP8161
** SYSTEM  : TAXWARS
** AUTHOR  : SAI
** FUNCTION: INITIALIZE TBL4 RECORDS W/1042-S  SERIAL NUMBER RANGES
** HISTORY.....:
**    02/03/2011 - SAI        -  CLONED FROM TWRP8161
**
**                   PARM WITH NEW CONTROL NUMBER RANGE PROVIDED BY
**                   BUSINESS    WILL HAVE TO BE MIGRATED EACH YEAR
**                   BEFORE RUNNING THE JOB (PTW1427R).
**
**                   NOTE:  WHEN ENTERING NEW RANGE IN PARM, SET "CURR"
**                   NUMBER TO "START" VALUE -1 (MINUS 1).
**
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp8162 extends BLNatBase
{
    // Data Areas
    private PdaTwratbl4 pdaTwratbl4;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I;

    private DbsGroup pnd_Input_Parameters;
    private DbsField pnd_Input_Parameters_Pnd_In_1042s_Year;
    private DbsField pnd_Input_Parameters_Pnd_In_1042s_Start;
    private DbsField pnd_Input_Parameters_Pnd_In_1042s_End;
    private DbsField pnd_Input_Parameters_Pnd_In_1042s_Curr;
    private DbsField pnd_Input_Parameters_Pnd_In_Orig_Corr_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwratbl4 = new PdaTwratbl4(localVariables);

        // Local Variables
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);

        pnd_Input_Parameters = localVariables.newGroupInRecord("pnd_Input_Parameters", "#INPUT-PARAMETERS");
        pnd_Input_Parameters_Pnd_In_1042s_Year = pnd_Input_Parameters.newFieldInGroup("pnd_Input_Parameters_Pnd_In_1042s_Year", "#IN-1042S-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Input_Parameters_Pnd_In_1042s_Start = pnd_Input_Parameters.newFieldInGroup("pnd_Input_Parameters_Pnd_In_1042s_Start", "#IN-1042S-START", FieldType.STRING, 
            10);
        pnd_Input_Parameters_Pnd_In_1042s_End = pnd_Input_Parameters.newFieldInGroup("pnd_Input_Parameters_Pnd_In_1042s_End", "#IN-1042S-END", FieldType.STRING, 
            10);
        pnd_Input_Parameters_Pnd_In_1042s_Curr = pnd_Input_Parameters.newFieldInGroup("pnd_Input_Parameters_Pnd_In_1042s_Curr", "#IN-1042S-CURR", FieldType.STRING, 
            10);
        pnd_Input_Parameters_Pnd_In_Orig_Corr_Ind = pnd_Input_Parameters.newFieldInGroup("pnd_Input_Parameters_Pnd_In_Orig_Corr_Ind", "#IN-ORIG-CORR-IND", 
            FieldType.STRING, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp8162() throws Exception
    {
        super("Twrp8162");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp8162|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: IF *DEVICE = 'BATCH'
                {
                    //*  SET DELIMITER MODE FOR BATCH INPUT
                    setControl("D");                                                                                                                                      //Natural: SET CONTROL 'D'
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parameters_Pnd_In_1042s_Year,pnd_Input_Parameters_Pnd_In_1042s_Start,pnd_Input_Parameters_Pnd_In_1042s_End, //Natural: INPUT #IN-1042S-YEAR #IN-1042S-START #IN-1042S-END #IN-1042S-CURR #IN-ORIG-CORR-IND
                    pnd_Input_Parameters_Pnd_In_1042s_Curr,pnd_Input_Parameters_Pnd_In_Orig_Corr_Ind);
                getReports().write(0, ReportOption.NOTITLE,"=",pnd_Input_Parameters_Pnd_In_1042s_Year,NEWLINE,"=",pnd_Input_Parameters_Pnd_In_1042s_Start,                //Natural: WRITE '=' #IN-1042S-YEAR / '=' #IN-1042S-START / '=' #IN-1042S-END / '=' #IN-1042S-CURR / '=' #IN-ORIG-CORR-IND /
                    NEWLINE,"=",pnd_Input_Parameters_Pnd_In_1042s_End,NEWLINE,"=",pnd_Input_Parameters_Pnd_In_1042s_Curr,NEWLINE,"=",pnd_Input_Parameters_Pnd_In_Orig_Corr_Ind,
                    NEWLINE);
                if (Global.isEscape()) return;
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 00 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 37T 'TaxWaRS' 68T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 23T '1042 S Control Records - Ser.#s Initialized ' 68T 'Report: RPT1' / //
                //* ***************
                //*  MAIN PROGRAM *
                //* ***************
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(3);                                                                                                   //Natural: ASSIGN #TWRATBL4.#FORM-IND := 03
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Input_Parameters_Pnd_In_1042s_Year);                                                              //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #IN-1042S-YEAR
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Display_Ind().setValue(true);                                                                                             //Natural: ASSIGN #TWRATBL4.#DISPLAY-IND := TRUE
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(true);                                                                                               //Natural: ASSIGN #TWRATBL4.#ABEND-IND := TRUE
                DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNTB4R' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                pdaTwratbl4.getPnd_Twratbl4_Tircntl_1042s_Cntl_Start().setValue(pnd_Input_Parameters_Pnd_In_1042s_Start);                                                 //Natural: ASSIGN TIRCNTL-1042S-CNTL-START := #IN-1042S-START
                pdaTwratbl4.getPnd_Twratbl4_Tircntl_1042s_Cntl_End().setValue(pnd_Input_Parameters_Pnd_In_1042s_End);                                                     //Natural: ASSIGN TIRCNTL-1042S-CNTL-END := #IN-1042S-END
                pdaTwratbl4.getPnd_Twratbl4_Tircntl_1042s_Cntl_Curr().setValue(pnd_Input_Parameters_Pnd_In_1042s_Curr);                                                   //Natural: ASSIGN TIRCNTL-1042S-CNTL-CURR := #IN-1042S-CURR
                getReports().write(0, ReportOption.NOTITLE," before update ");                                                                                            //Natural: WRITE ' before update '
                if (Global.isEscape()) return;
                DbsUtil.callnat(Twrntb4u.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNTB4U' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                getReports().write(0, ReportOption.NOTITLE,"*** 1042-S  Form Serial Numbers Initialized ***",NEWLINE,"in",pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(),  //Natural: WRITE '*** 1042-S  Form Serial Numbers Initialized ***' / 'in' #TWRATBL4.#INPUT-PARMS / 'out' #TWRATBL4.TIRCNTL-1042S-CNTL-START #TWRATBL4.TIRCNTL-1042S-CNTL-END #TWRATBL4.TIRCNTL-1042S-CNTL-CURR
                    NEWLINE,"out",pdaTwratbl4.getPnd_Twratbl4_Tircntl_1042s_Cntl_Start(),pdaTwratbl4.getPnd_Twratbl4_Tircntl_1042s_Cntl_End(),pdaTwratbl4.getPnd_Twratbl4_Tircntl_1042s_Cntl_Curr());
                if (Global.isEscape()) return;
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 0 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 0 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 0 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");

        getReports().write(0, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(37),"TaxWaRS",new TabSetting(68),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(23),"1042 S Control Records - Ser.#s Initialized ",new TabSetting(68),"Report: RPT1",NEWLINE,NEWLINE,NEWLINE);
    }
}
