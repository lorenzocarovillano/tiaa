/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:39:47 PM
**        * FROM NATURAL PROGRAM : Twrp4580
************************************************************
**        * FILE NAME            : Twrp4580.java
**        * CLASS NAME           : Twrp4580
**        * INSTANCE NAME        : Twrp4580
************************************************************
************************************************************************
*
* PROGRAM  : TWRP4580  (IRS - 5498  CORRECTION REPORTING).
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : PRODUCES IRS 5498 EMPTY FORMS DETAIL REPORT.
* CREATED  : 07 / 12 / 2000.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM PRODUCES A REPORT OF ALL EMPTY 5498 RECORDS NOT
*            REPORTED TO THE 'IRS' (ORIGINAL REPORTING).
*            (CORRECTION REPORTING).
* 07/06/06  RM  RESTOW FOR TWRL452A UPDATES.
* 23-11-20 : PALDE           - RESTOW FOR TWRL452A UPDATES
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp4580 extends BLNatBase
{
    // Data Areas
    private LdaTwrl452a ldaTwrl452a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_City_State_Zip_Code;
    private DbsField pnd_Total_B_Recs_Record;
    private DbsField pnd_Prev_A_Rec_Company;
    private DbsField pnd_Prev_C_Rec_Company;
    private DbsField pnd_Isn;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_F_Name;
    private DbsField pnd_T_Trans_Count;
    private DbsField pnd_T_Classic_Amt;
    private DbsField pnd_T_Roth_Amt;
    private DbsField pnd_T_Rollover_Amt;
    private DbsField pnd_T_Rechar_Amt;
    private DbsField pnd_T_Education_Amt;
    private DbsField pnd_T_Fmv_Amt;
    private DbsField pnd_T_Roth_Conv_Amt;
    private DbsField pnd_C_Trans_Count;
    private DbsField pnd_C_Classic_Amt;
    private DbsField pnd_C_Roth_Amt;
    private DbsField pnd_C_Rollover_Amt;
    private DbsField pnd_C_Rechar_Amt;
    private DbsField pnd_C_Education_Amt;
    private DbsField pnd_C_Fmv_Amt;
    private DbsField pnd_C_Roth_Conv_Amt;
    private DbsField pnd_L_Trans_Count;
    private DbsField pnd_L_Classic_Amt;
    private DbsField pnd_L_Roth_Amt;
    private DbsField pnd_L_Rollover_Amt;
    private DbsField pnd_L_Rechar_Amt;
    private DbsField pnd_L_Education_Amt;
    private DbsField pnd_L_Fmv_Amt;
    private DbsField pnd_L_Roth_Conv_Amt;
    private DbsField i1;
    private DbsField i2;
    private DbsField i3;
    private DbsField i4;
    private DbsField i;
    private DbsField j;
    private DbsField k;
    private DbsField l;
    private DbsField t;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl452a = new LdaTwrl452a();
        registerRecord(ldaTwrl452a);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_City_State_Zip_Code = localVariables.newFieldInRecord("pnd_City_State_Zip_Code", "#CITY-STATE-ZIP-CODE", FieldType.STRING, 40);
        pnd_Total_B_Recs_Record = localVariables.newFieldInRecord("pnd_Total_B_Recs_Record", "#TOTAL-B-RECS-RECORD", FieldType.NUMERIC, 8);
        pnd_Prev_A_Rec_Company = localVariables.newFieldInRecord("pnd_Prev_A_Rec_Company", "#PREV-A-REC-COMPANY", FieldType.STRING, 1);
        pnd_Prev_C_Rec_Company = localVariables.newFieldInRecord("pnd_Prev_C_Rec_Company", "#PREV-C-REC-COMPANY", FieldType.STRING, 1);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Name = localVariables.newFieldInRecord("pnd_F_Name", "#F-NAME", FieldType.STRING, 40);
        pnd_T_Trans_Count = localVariables.newFieldArrayInRecord("pnd_T_Trans_Count", "#T-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            8));
        pnd_T_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_T_Classic_Amt", "#T-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_T_Roth_Amt", "#T-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_T_Rollover_Amt", "#T-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_T_Rechar_Amt", "#T-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Education_Amt = localVariables.newFieldArrayInRecord("pnd_T_Education_Amt", "#T-EDUCATION-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_T_Fmv_Amt", "#T-FMV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_T_Roth_Conv_Amt", "#T-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Trans_Count = localVariables.newFieldArrayInRecord("pnd_C_Trans_Count", "#C-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            8));
        pnd_C_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_C_Classic_Amt", "#C-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_C_Roth_Amt", "#C-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_C_Rollover_Amt", "#C-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_C_Rechar_Amt", "#C-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Education_Amt = localVariables.newFieldArrayInRecord("pnd_C_Education_Amt", "#C-EDUCATION-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_C_Fmv_Amt", "#C-FMV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_C_Roth_Conv_Amt", "#C-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Trans_Count = localVariables.newFieldArrayInRecord("pnd_L_Trans_Count", "#L-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            8));
        pnd_L_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_L_Classic_Amt", "#L-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_L_Roth_Amt", "#L-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_L_Rollover_Amt", "#L-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_L_Rechar_Amt", "#L-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Education_Amt = localVariables.newFieldArrayInRecord("pnd_L_Education_Amt", "#L-EDUCATION-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_L_Fmv_Amt", "#L-FMV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_L_Roth_Conv_Amt", "#L-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        i1 = localVariables.newFieldInRecord("i1", "I1", FieldType.PACKED_DECIMAL, 4);
        i2 = localVariables.newFieldInRecord("i2", "I2", FieldType.PACKED_DECIMAL, 4);
        i3 = localVariables.newFieldInRecord("i3", "I3", FieldType.PACKED_DECIMAL, 4);
        i4 = localVariables.newFieldInRecord("i4", "I4", FieldType.PACKED_DECIMAL, 4);
        i = localVariables.newFieldInRecord("i", "I", FieldType.PACKED_DECIMAL, 4);
        j = localVariables.newFieldInRecord("j", "J", FieldType.PACKED_DECIMAL, 4);
        k = localVariables.newFieldInRecord("k", "K", FieldType.PACKED_DECIMAL, 4);
        l = localVariables.newFieldInRecord("l", "L", FieldType.PACKED_DECIMAL, 4);
        t = localVariables.newFieldInRecord("t", "T", FieldType.PACKED_DECIMAL, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl452a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp4580() throws Exception
    {
        super("Twrp4580");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP4580", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133;//Natural: FORMAT ( 02 ) PS = 60 LS = 133;//Natural: FORMAT ( 03 ) PS = 60 LS = 133;//Natural: FORMAT ( 04 ) PS = 60 LS = 133;//Natural: FORMAT ( 05 ) PS = 60 LS = 133
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        RD1:                                                                                                                                                              //Natural: READ WORK FILE 01 RECORD #FORM
        while (condition(getWorkFiles().read(1, ldaTwrl452a.getPnd_Form())))
        {
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            pnd_F_Name.setValue(DbsUtil.compress(ldaTwrl452a.getPnd_Form_Pnd_F_Part_First_Nme(), ldaTwrl452a.getPnd_Form_Pnd_F_Part_Mddle_Nme(), ldaTwrl452a.getPnd_Form_Pnd_F_Part_Last_Nme())); //Natural: COMPRESS #F-PART-FIRST-NME #F-PART-MDDLE-NME #F-PART-LAST-NME INTO #F-NAME
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),ldaTwrl452a.getPnd_Form_Pnd_F_Tin(), new ReportEditMask ("XXX-XX-XXXXX"),new  //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T #F-TIN ( EM = XXX-XX-XXXXX ) 15T #F-TAX-ID-TYPE 18T #F-CONTRACT-NBR 28T #F-PAYEE-CDE 34T #F-IRA-TYPE 38T #F-COMPANY-CDE 40T #F-NAME
                TabSetting(15),ldaTwrl452a.getPnd_Form_Pnd_F_Tax_Id_Type(),new TabSetting(18),ldaTwrl452a.getPnd_Form_Pnd_F_Contract_Nbr(),new TabSetting(28),ldaTwrl452a.getPnd_Form_Pnd_F_Payee_Cde(),new 
                TabSetting(34),ldaTwrl452a.getPnd_Form_Pnd_F_Ira_Type(),new TabSetting(38),ldaTwrl452a.getPnd_Form_Pnd_F_Company_Cde(),new TabSetting(40),
                pnd_F_Name);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        RD1_Exit:
        if (Global.isEscape()) return;
        //* *------
        //*   #READ-CTR  =  0
        //*   PERFORM  ERROR-DISPLAY-START
        //*   WRITE (00)
        //*         '***' 06T 'Form (5498) Extract File (Work File 01) Is Empty'
        //*     77T '***'
        //*   /     '***' 06T 'PROGRAM...:' *PROGRAM
        //*     77T '***'
        //*   PERFORM  ERROR-DISPLAY-END
        //*   TERMINATE 90
        //*  D-IF
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *------------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 01 )
        //* *-------                                                                                                                                                      //Natural: ON ERROR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        getReports().write(0, NEWLINE,new TabSetting(1),"IRS 5498 Empty Form Records Found..........",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                    //Natural: WRITE ( 00 ) / 01T 'IRS 5498 Empty Form Records Found..........' #READ-CTR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"IRS 5498 Empty Form Records Found..........",pnd_Read_Ctr, new ReportEditMask               //Natural: WRITE ( 01 ) / 01T 'IRS 5498 Empty Form Records Found..........' #READ-CTR
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(49),"IRS 5498 Empty Forms Detail Report",new  //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 49T 'IRS 5498 Empty Forms Detail Report' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"Tax Year......",ldaTwrl452a.getPnd_Form_Pnd_F_Tax_Year());                              //Natural: WRITE ( 01 ) NOTITLE 01T 'Tax Year......' #F-TAX-YEAR
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1 LINES
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(14),"ID ",new TabSetting(33),"IRA ",new TabSetting(38),"C");                                //Natural: WRITE ( 01 ) NOTITLE 14T 'ID ' 33T 'IRA ' 38T 'C'
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"   Tax ID   ",new TabSetting(14),"Typ",new TabSetting(18),"Contract",new                //Natural: WRITE ( 01 ) NOTITLE 01T '   Tax ID   ' 14T 'Typ' 18T 'Contract' 27T 'Payee' 33T 'Type' 38T 'C' 40T '       Name Of contributor              '
                        TabSetting(27),"Payee",new TabSetting(33),"Type",new TabSetting(38),"C",new TabSetting(40),"       Name Of contributor              ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"============",new TabSetting(14),"===",new TabSetting(18),"========",new                //Natural: WRITE ( 01 ) NOTITLE 01T '============' 14T '===' 18T '========' 27T '=====' 33T '====' 38T '=' 40T '========================================'
                        TabSetting(27),"=====",new TabSetting(33),"====",new TabSetting(38),"=",new TabSetting(40),"========================================");
                    //*  SKIP (01) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
        sub_Error_Display_Start();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
        sub_Error_Display_End();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
        Global.format(2, "PS=60 LS=133");
        Global.format(3, "PS=60 LS=133");
        Global.format(4, "PS=60 LS=133");
        Global.format(5, "PS=60 LS=133");
    }
}
