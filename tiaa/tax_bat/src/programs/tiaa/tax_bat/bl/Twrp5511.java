/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:41:00 PM
**        * FROM NATURAL PROGRAM : Twrp5511
************************************************************
**        * FILE NAME            : Twrp5511.java
**        * CLASS NAME           : Twrp5511
**        * INSTANCE NAME        : Twrp5511
************************************************************
************************************************************************
** PROGRAM : TWRP5511
** SYSTEM  : TAXWARS
** AUTHOR  : MICHAEL SUPONITSKY
** FUNCTION: RECONCILIATION REPORT FOR 1099-R AND 1099-INT FROM THE
**           PAYMENT FILE. (TAX-CITIZENSHIP = U.
** HISTORY :
** ----------------------------------------------
** 06/30/2000 - M. SUPONITSKY - HARDCOPY IND NOW INCLUDES MAG STATES.
** 06/11/2002 - JH - ALLOW IVC AMOUNTS FOR ROLLOVERS - 2002 AND LATER
** 08/13/02   - J.ROTHOLZ - RECOMPILED DUE TO INCREASE IN #COMP-NAME
**              LENGTH IN TWRLCOMP & TWRACOMP
** 10/08/02 JH DISTR CODE 6 (TAXFREE EXCHANGE) IS TREATED LIKE ROLLOVER
** 09/29/03 RM - 2003 1099 & 5498 SDR
**               DISTRIBUTION CODE 'X' REMOVED.
* 10/14/03:STOWED DUE TO UPDATE ON TWRL0900-ADDED CONTRACT TYPE & IRC-TO
** 11/03/2003 - M. SUPONITSKY - COPY OF TWRP5501. USED FOR TAX YEARS
**                              LESS THAN 2003.
** 12/06/05 - BK. TAX YEAR ADDED TO TWRNCOMP PARMS
** 02/18/15: OS - RECOMPILED FOR UPDATED TWRL0900
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5511 extends BLNatBase
{
    // Data Areas
    private GdaTwrg5501 gdaTwrg5501;
    private PdaTwracomp pdaTwracomp;
    private PdaTwratbl2 pdaTwratbl2;
    private LdaTwrl0900 ldaTwrl0900;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Pnd_Cntl_Max;
    private DbsField pnd_Ws_Const_Pnd_St_To1_Max;
    private DbsField pnd_Ws_Const_Pnd_1099r_Const;

    private DbsGroup pnd_Key_Detail;
    private DbsField pnd_Key_Detail_Twrpymnt_Tax_Year;
    private DbsField pnd_Key_Detail_Twrpymnt_Company_Cde_Form;
    private DbsField pnd_Key_Detail_Twrpymnt_Tax_Id_Nbr;
    private DbsField pnd_Key_Detail_Twrpymnt_Contract_Nbr;
    private DbsField pnd_Key_Detail_Twrpymnt_Payee_Cde;
    private DbsField pnd_Key_Detail_Twrpymnt_Distribution_Cde;
    private DbsField pnd_Key_Detail_Twrpymnt_Paymt_Category;
    private DbsField pnd_Key_Detail_Twrpymnt_Residency_Type;
    private DbsField pnd_Key_Detail_Twrpymnt_Residency_Code;

    private DbsGroup pnd_Key_Detail__R_Field_1;
    private DbsField pnd_Key_Detail_Pnd_Key;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_1099r_Max;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_Company_Line;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_Idx;
    private DbsField pnd_Ws_Pnd_S1;
    private DbsField pnd_Ws_Pnd_S2;
    private DbsField pnd_Ws_Pnd_Known_Payment;
    private DbsField pnd_Ws_Pnd_Tran_Cv;
    private DbsField pnd_Ws_Pnd_Gross_Cv;
    private DbsField pnd_Ws_Pnd_Ivc_Cv;
    private DbsField pnd_Ws_Pnd_Taxable_Cv;
    private DbsField pnd_Ws_Pnd_Fed_Cv;
    private DbsField pnd_Ws_Pnd_Sta_Cv;
    private DbsField pnd_Ws_Pnd_Loc_Cv;
    private DbsField pnd_Ws_Pnd_Int_Cv;
    private DbsField pnd_Ws_Pnd_Int_Back_Cv;
    private DbsField pnd_Ws_Pnd_1099;

    private DbsGroup pnd_Case_Fields;
    private DbsField pnd_Case_Fields_Pnd_Roll;
    private DbsField pnd_Case_Fields_Pnd_Rechar;

    private DbsGroup pnd_Case_Fields_Pnd_1099_Cases;
    private DbsField pnd_Case_Fields_Pnd_Payment_Type;
    private DbsField pnd_Case_Fields_Pnd_Settle_Type;
    private DbsField pnd_Case_Fields_Pnd_Case_Present;
    private DbsField pnd_Case_Fields_Pnd_Tnd;
    private DbsField pnd_Case_Fields_Pnd_Ivc_Amt;
    private DbsField pnd_Case_Fields_Pnd_Taxable_Amt;

    private DbsGroup pnd_St_Case;
    private DbsField pnd_St_Case_Pnd_Case_Present;
    private DbsField pnd_St_Case_Pnd_Tnd;
    private DbsField pnd_St_Case_Pnd_Tran_Cnt;
    private DbsField pnd_St_Case_Pnd_Gross_Amt;
    private DbsField pnd_St_Case_Pnd_Ivc_Amt;
    private DbsField pnd_St_Case_Pnd_Ivc_Adj;
    private DbsField pnd_St_Case_Pnd_Taxable_Amt;
    private DbsField pnd_St_Case_Pnd_Taxable_Adj;
    private DbsField pnd_St_Case_Pnd_Sta_Tax;
    private DbsField pnd_St_Case_Pnd_Loc_Tax;
    private DbsField pnd_St_Case_Pnd_State_Distr;
    private DbsField pnd_St_Case_Pnd_Reportable_Distr;

    private DbsGroup pnd_Prev_Res;
    private DbsField pnd_Prev_Res_Pnd_Distr_Cde;
    private DbsField pnd_Prev_Res_Pnd_Res_Type;
    private DbsField pnd_Prev_Res_Pnd_Res_Code;

    private DbsGroup pnd_Cntl;
    private DbsField pnd_Cntl_Pnd_Cntl_Text;
    private DbsField pnd_Cntl_Pnd_Cntl_Text1;
    private DbsField pnd_Cntl_Pnd_Tran_Cnt_Cv;
    private DbsField pnd_Cntl_Pnd_Gross_Amt_Cv;
    private DbsField pnd_Cntl_Pnd_Ivc_Amt_Cv;
    private DbsField pnd_Cntl_Pnd_Taxable_Amt_Cv;
    private DbsField pnd_Cntl_Pnd_Fed_Tax_Cv;
    private DbsField pnd_Cntl_Pnd_Sta_Tax_Cv;
    private DbsField pnd_Cntl_Pnd_Loc_Tax_Cv;
    private DbsField pnd_Cntl_Pnd_Int_Amt_Cv;
    private DbsField pnd_Cntl_Pnd_Int_Back_Tax_Cv;

    private DbsGroup pnd_Cntl_Pnd_Totals;
    private DbsField pnd_Cntl_Pnd_Tran_Cnt;
    private DbsField pnd_Cntl_Pnd_Gross_Amt;
    private DbsField pnd_Cntl_Pnd_Ivc_Amt;
    private DbsField pnd_Cntl_Pnd_Taxable_Amt;
    private DbsField pnd_Cntl_Pnd_Fed_Tax;
    private DbsField pnd_Cntl_Pnd_Sta_Tax;
    private DbsField pnd_Cntl_Pnd_Loc_Tax;
    private DbsField pnd_Cntl_Pnd_Int_Amt;
    private DbsField pnd_Cntl_Pnd_Int_Tax;

    private DbsGroup pnd_St_To1;
    private DbsField pnd_St_To1_Pnd_Pmnt_Taxable;
    private DbsField pnd_St_To1_Pnd_Taxable_Adj;
    private DbsField pnd_St_To1_Pnd_Form_Taxable;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Twrpymnt_Distribution_CdeOld;
    private DbsField readWork01Twrpymnt_Residency_TypeOld;
    private DbsField readWork01Twrpymnt_Residency_CodeOld;
    private DbsField readWork01Twrpymnt_Company_Cde_FormOld;
    private DbsField readWork01Twrpymnt_Tax_YearOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaTwrg5501 = GdaTwrg5501.getInstance(getCallnatLevel());
        registerRecord(gdaTwrg5501);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaTwracomp = new PdaTwracomp(localVariables);
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        ldaTwrl0900 = new LdaTwrl0900();
        registerRecord(ldaTwrl0900);

        // Local Variables

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Pnd_Cntl_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_Cntl_Max", "#CNTL-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Const_Pnd_St_To1_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_St_To1_Max", "#ST-TO1-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Const_Pnd_1099r_Const = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_1099r_Const", "#1099R-CONST", FieldType.PACKED_DECIMAL, 3);

        pnd_Key_Detail = localVariables.newGroupInRecord("pnd_Key_Detail", "#KEY-DETAIL");
        pnd_Key_Detail_Twrpymnt_Tax_Year = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Twrpymnt_Tax_Year", "TWRPYMNT-TAX-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Key_Detail_Twrpymnt_Company_Cde_Form = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Twrpymnt_Company_Cde_Form", "TWRPYMNT-COMPANY-CDE-FORM", 
            FieldType.STRING, 1);
        pnd_Key_Detail_Twrpymnt_Tax_Id_Nbr = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Twrpymnt_Tax_Id_Nbr", "TWRPYMNT-TAX-ID-NBR", FieldType.STRING, 
            10);
        pnd_Key_Detail_Twrpymnt_Contract_Nbr = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Twrpymnt_Contract_Nbr", "TWRPYMNT-CONTRACT-NBR", FieldType.STRING, 
            8);
        pnd_Key_Detail_Twrpymnt_Payee_Cde = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Twrpymnt_Payee_Cde", "TWRPYMNT-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Key_Detail_Twrpymnt_Distribution_Cde = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Twrpymnt_Distribution_Cde", "TWRPYMNT-DISTRIBUTION-CDE", 
            FieldType.STRING, 2);
        pnd_Key_Detail_Twrpymnt_Paymt_Category = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Twrpymnt_Paymt_Category", "TWRPYMNT-PAYMT-CATEGORY", FieldType.STRING, 
            1);
        pnd_Key_Detail_Twrpymnt_Residency_Type = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Twrpymnt_Residency_Type", "TWRPYMNT-RESIDENCY-TYPE", FieldType.STRING, 
            1);
        pnd_Key_Detail_Twrpymnt_Residency_Code = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Twrpymnt_Residency_Code", "TWRPYMNT-RESIDENCY-CODE", FieldType.STRING, 
            2);

        pnd_Key_Detail__R_Field_1 = localVariables.newGroupInRecord("pnd_Key_Detail__R_Field_1", "REDEFINE", pnd_Key_Detail);
        pnd_Key_Detail_Pnd_Key = pnd_Key_Detail__R_Field_1.newFieldInGroup("pnd_Key_Detail_Pnd_Key", "#KEY", FieldType.STRING, 31);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_1099r_Max = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_1099r_Max", "#1099R-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Company_Line = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Line", "#COMPANY-LINE", FieldType.STRING, 59);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Idx", "#IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S1", "#S1", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S2", "#S2", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Known_Payment = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Known_Payment", "#KNOWN-PAYMENT", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Tran_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tran_Cv", "#TRAN-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Gross_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Gross_Cv", "#GROSS-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Ivc_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ivc_Cv", "#IVC-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Taxable_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Taxable_Cv", "#TAXABLE-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Fed_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Fed_Cv", "#FED-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Sta_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Sta_Cv", "#STA-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Loc_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Loc_Cv", "#LOC-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Int_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Int_Cv", "#INT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Int_Back_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Int_Back_Cv", "#INT-BACK-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_1099 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_1099", "#1099", FieldType.PACKED_DECIMAL, 3);

        pnd_Case_Fields = localVariables.newGroupInRecord("pnd_Case_Fields", "#CASE-FIELDS");
        pnd_Case_Fields_Pnd_Roll = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Roll", "#ROLL", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Rechar = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Rechar", "#RECHAR", FieldType.BOOLEAN, 1);

        pnd_Case_Fields_Pnd_1099_Cases = pnd_Case_Fields.newGroupArrayInGroup("pnd_Case_Fields_Pnd_1099_Cases", "#1099-CASES", new DbsArrayController(1, 
            4));
        pnd_Case_Fields_Pnd_Payment_Type = pnd_Case_Fields_Pnd_1099_Cases.newFieldInGroup("pnd_Case_Fields_Pnd_Payment_Type", "#PAYMENT-TYPE", FieldType.STRING, 
            1);
        pnd_Case_Fields_Pnd_Settle_Type = pnd_Case_Fields_Pnd_1099_Cases.newFieldInGroup("pnd_Case_Fields_Pnd_Settle_Type", "#SETTLE-TYPE", FieldType.STRING, 
            1);
        pnd_Case_Fields_Pnd_Case_Present = pnd_Case_Fields_Pnd_1099_Cases.newFieldInGroup("pnd_Case_Fields_Pnd_Case_Present", "#CASE-PRESENT", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Tnd = pnd_Case_Fields_Pnd_1099_Cases.newFieldInGroup("pnd_Case_Fields_Pnd_Tnd", "#TND", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Ivc_Amt = pnd_Case_Fields_Pnd_1099_Cases.newFieldInGroup("pnd_Case_Fields_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Case_Fields_Pnd_Taxable_Amt = pnd_Case_Fields_Pnd_1099_Cases.newFieldInGroup("pnd_Case_Fields_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);

        pnd_St_Case = localVariables.newGroupArrayInRecord("pnd_St_Case", "#ST-CASE", new DbsArrayController(1, 4));
        pnd_St_Case_Pnd_Case_Present = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_Case_Present", "#CASE-PRESENT", FieldType.BOOLEAN, 1);
        pnd_St_Case_Pnd_Tnd = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_Tnd", "#TND", FieldType.BOOLEAN, 1);
        pnd_St_Case_Pnd_Tran_Cnt = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_Tran_Cnt", "#TRAN-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_St_Case_Pnd_Gross_Amt = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_St_Case_Pnd_Ivc_Amt = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_St_Case_Pnd_Ivc_Adj = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_Ivc_Adj", "#IVC-ADJ", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_St_Case_Pnd_Taxable_Amt = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_St_Case_Pnd_Taxable_Adj = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_Taxable_Adj", "#TAXABLE-ADJ", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_St_Case_Pnd_Sta_Tax = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_Sta_Tax", "#STA-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_St_Case_Pnd_Loc_Tax = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_Loc_Tax", "#LOC-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_St_Case_Pnd_State_Distr = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_State_Distr", "#STATE-DISTR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_St_Case_Pnd_Reportable_Distr = pnd_St_Case.newFieldInGroup("pnd_St_Case_Pnd_Reportable_Distr", "#REPORTABLE-DISTR", FieldType.BOOLEAN, 1);

        pnd_Prev_Res = localVariables.newGroupInRecord("pnd_Prev_Res", "#PREV-RES");
        pnd_Prev_Res_Pnd_Distr_Cde = pnd_Prev_Res.newFieldInGroup("pnd_Prev_Res_Pnd_Distr_Cde", "#DISTR-CDE", FieldType.STRING, 2);
        pnd_Prev_Res_Pnd_Res_Type = pnd_Prev_Res.newFieldInGroup("pnd_Prev_Res_Pnd_Res_Type", "#RES-TYPE", FieldType.STRING, 1);
        pnd_Prev_Res_Pnd_Res_Code = pnd_Prev_Res.newFieldInGroup("pnd_Prev_Res_Pnd_Res_Code", "#RES-CODE", FieldType.STRING, 2);

        pnd_Cntl = localVariables.newGroupArrayInRecord("pnd_Cntl", "#CNTL", new DbsArrayController(1, 9));
        pnd_Cntl_Pnd_Cntl_Text = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Cntl_Text", "#CNTL-TEXT", FieldType.STRING, 26);
        pnd_Cntl_Pnd_Cntl_Text1 = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Cntl_Text1", "#CNTL-TEXT1", FieldType.STRING, 18);
        pnd_Cntl_Pnd_Tran_Cnt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Tran_Cnt_Cv", "#TRAN-CNT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Gross_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Gross_Amt_Cv", "#GROSS-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Ivc_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Ivc_Amt_Cv", "#IVC-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Taxable_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Taxable_Amt_Cv", "#TAXABLE-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Fed_Tax_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Fed_Tax_Cv", "#FED-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Sta_Tax_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Sta_Tax_Cv", "#STA-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Loc_Tax_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Loc_Tax_Cv", "#LOC-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Int_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Int_Amt_Cv", "#INT-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Int_Back_Tax_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Int_Back_Tax_Cv", "#INT-BACK-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 2);

        pnd_Cntl_Pnd_Totals = pnd_Cntl.newGroupArrayInGroup("pnd_Cntl_Pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Cntl_Pnd_Tran_Cnt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Tran_Cnt", "#TRAN-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Cntl_Pnd_Gross_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Cntl_Pnd_Ivc_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Taxable_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Cntl_Pnd_Fed_Tax = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Fed_Tax", "#FED-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Sta_Tax = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Sta_Tax", "#STA-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Loc_Tax = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Loc_Tax", "#LOC-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Int_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Int_Tax = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Int_Tax", "#INT-TAX", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_St_To1 = localVariables.newGroupArrayInRecord("pnd_St_To1", "#ST-TO1", new DbsArrayController(1, 63, 1, 2, 1, 5));
        pnd_St_To1_Pnd_Pmnt_Taxable = pnd_St_To1.newFieldInGroup("pnd_St_To1_Pnd_Pmnt_Taxable", "#PMNT-TAXABLE", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_St_To1_Pnd_Taxable_Adj = pnd_St_To1.newFieldInGroup("pnd_St_To1_Pnd_Taxable_Adj", "#TAXABLE-ADJ", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_St_To1_Pnd_Form_Taxable = pnd_St_To1.newFieldInGroup("pnd_St_To1_Pnd_Form_Taxable", "#FORM-TAXABLE", FieldType.PACKED_DECIMAL, 13, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Twrpymnt_Distribution_CdeOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrpymnt_Distribution_Cde_OLD", "Twrpymnt_Distribution_Cde_OLD", 
            FieldType.STRING, 2);
        readWork01Twrpymnt_Residency_TypeOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrpymnt_Residency_Type_OLD", "Twrpymnt_Residency_Type_OLD", 
            FieldType.STRING, 1);
        readWork01Twrpymnt_Residency_CodeOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrpymnt_Residency_Code_OLD", "Twrpymnt_Residency_Code_OLD", 
            FieldType.STRING, 2);
        readWork01Twrpymnt_Company_Cde_FormOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrpymnt_Company_Cde_Form_OLD", "Twrpymnt_Company_Cde_Form_OLD", 
            FieldType.STRING, 1);
        readWork01Twrpymnt_Tax_YearOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrpymnt_Tax_Year_OLD", "Twrpymnt_Tax_Year_OLD", FieldType.NUMERIC, 
            4);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaTwrl0900.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Pnd_Cntl_Max.setInitialValue(9);
        pnd_Ws_Const_Pnd_St_To1_Max.setInitialValue(62);
        pnd_Ws_Const_Pnd_1099r_Const.setInitialValue(4);
        pnd_Ws_Pnd_1099.setInitialValue(1);
        pnd_Case_Fields_Pnd_Payment_Type.getValue(2).setInitialValue("N");
        pnd_Case_Fields_Pnd_Payment_Type.getValue(3).setInitialValue("N");
        pnd_Case_Fields_Pnd_Payment_Type.getValue(4).setInitialValue("N");
        pnd_Case_Fields_Pnd_Settle_Type.getValue(2).setInitialValue("X");
        pnd_Case_Fields_Pnd_Settle_Type.getValue(3).setInitialValue("I");
        pnd_Case_Fields_Pnd_Settle_Type.getValue(4).setInitialValue("H");
        pnd_Cntl_Pnd_Cntl_Text.getValue(1).setInitialValue("* All Payments");
        pnd_Cntl_Pnd_Cntl_Text.getValue(2).setInitialValue("Active Payments");
        pnd_Cntl_Pnd_Cntl_Text.getValue(3).setInitialValue("- Rollovers with 'K' $ IVC");
        pnd_Cntl_Pnd_Cntl_Text.getValue(4).setInitialValue("- IRA Excess Contributions");
        pnd_Cntl_Pnd_Cntl_Text.getValue(5).setInitialValue("- 'U' IVC with $");
        pnd_Cntl_Pnd_Cntl_Text.getValue(6).setInitialValue("- Taxable not Determined");
        pnd_Cntl_Pnd_Cntl_Text.getValue(7).setInitialValue("- Puerto Rico");
        pnd_Cntl_Pnd_Cntl_Text.getValue(8).setInitialValue("- Foreign Residency");
        pnd_Cntl_Pnd_Cntl_Text.getValue(9).setInitialValue("= 1099-R & 1099-INT forms");
        pnd_Cntl_Pnd_Cntl_Text1.getValue(4).setInitialValue("  & REAC");
        pnd_Cntl_Pnd_Cntl_Text1.getValue(6).setInitialValue("  (Not Rollovers)");
        pnd_Cntl_Pnd_Tran_Cnt_Cv.setInitialAttributeValue("AD=I)#CNTL.#TRAN-CNT-CV(2)	(AD=I)#CNTL.#TRAN-CNT-CV(3)	(AD=N)#CNTL.#TRAN-CNT-CV(4)	(AD=N)#CNTL.#TRAN-CNT-CV(5)	(AD=N)#CNTL.#TRAN-CNT-CV(6)	(AD=N)#CNTL.#TRAN-CNT-CV(7)	(AD=N)#CNTL.#TRAN-CNT-CV(8)	(AD=N)#CNTL.#TRAN-CNT-CV(9)	(AD=N");
        pnd_Cntl_Pnd_Gross_Amt_Cv.setInitialAttributeValue("AD=I)#CNTL.#GROSS-AMT-CV(2)	(AD=I)#CNTL.#GROSS-AMT-CV(3)	(AD=N)#CNTL.#GROSS-AMT-CV(4)	(AD=N)#CNTL.#GROSS-AMT-CV(5)	(AD=N)#CNTL.#GROSS-AMT-CV(6)	(AD=N)#CNTL.#GROSS-AMT-CV(7)	(AD=N)#CNTL.#GROSS-AMT-CV(8)	(AD=N)#CNTL.#GROSS-AMT-CV(9)	(AD=I");
        pnd_Cntl_Pnd_Ivc_Amt_Cv.setInitialAttributeValue("AD=I)#CNTL.#IVC-AMT-CV(2)	(AD=I)#CNTL.#IVC-AMT-CV(3)	(AD=I)#CNTL.#IVC-AMT-CV(4)	(AD=I)#CNTL.#IVC-AMT-CV(5)	(AD=I)#CNTL.#IVC-AMT-CV(6)	(AD=I)#CNTL.#IVC-AMT-CV(7)	(AD=N)#CNTL.#IVC-AMT-CV(8)	(AD=N)#CNTL.#IVC-AMT-CV(9)	(AD=I");
        pnd_Cntl_Pnd_Taxable_Amt_Cv.setInitialAttributeValue("AD=N)#CNTL.#TAXABLE-AMT-CV(2)	(AD=I)#CNTL.#TAXABLE-AMT-CV(3)	(AD=N)#CNTL.#TAXABLE-AMT-CV(4)	(AD=N)#CNTL.#TAXABLE-AMT-CV(5)	(AD=N)#CNTL.#TAXABLE-AMT-CV(6)	(AD=I)#CNTL.#TAXABLE-AMT-CV(7)	(AD=N)#CNTL.#TAXABLE-AMT-CV(8)	(AD=N)#CNTL.#TAXABLE-AMT-CV(9)	(AD=I");
        pnd_Cntl_Pnd_Fed_Tax_Cv.setInitialAttributeValue("AD=I)#CNTL.#FED-TAX-CV(2)	(AD=I)#CNTL.#FED-TAX-CV(3)	(AD=N)#CNTL.#FED-TAX-CV(4)	(AD=N)#CNTL.#FED-TAX-CV(5)	(AD=N)#CNTL.#FED-TAX-CV(6)	(AD=N)#CNTL.#FED-TAX-CV(7)	(AD=N)#CNTL.#FED-TAX-CV(8)	(AD=N)#CNTL.#FED-TAX-CV(9)	(AD=I");
        pnd_Cntl_Pnd_Sta_Tax_Cv.setInitialAttributeValue("AD=I)#CNTL.#STA-TAX-CV(2)	(AD=I)#CNTL.#STA-TAX-CV(3)	(AD=N)#CNTL.#STA-TAX-CV(4)	(AD=N)#CNTL.#STA-TAX-CV(5)	(AD=N)#CNTL.#STA-TAX-CV(6)	(AD=N)#CNTL.#STA-TAX-CV(7)	(AD=I)#CNTL.#STA-TAX-CV(8)	(AD=I)#CNTL.#STA-TAX-CV(9)	(AD=I");
        pnd_Cntl_Pnd_Loc_Tax_Cv.setInitialAttributeValue("AD=I)#CNTL.#LOC-TAX-CV(2)	(AD=I)#CNTL.#LOC-TAX-CV(3)	(AD=N)#CNTL.#LOC-TAX-CV(4)	(AD=N)#CNTL.#LOC-TAX-CV(5)	(AD=N)#CNTL.#LOC-TAX-CV(6)	(AD=N)#CNTL.#LOC-TAX-CV(7)	(AD=I)#CNTL.#LOC-TAX-CV(8)	(AD=I)#CNTL.#LOC-TAX-CV(9)	(AD=I");
        pnd_Cntl_Pnd_Int_Amt_Cv.setInitialAttributeValue("AD=I)#CNTL.#INT-AMT-CV(2)	(AD=I)#CNTL.#INT-AMT-CV(3)	(AD=N)#CNTL.#INT-AMT-CV(4)	(AD=N)#CNTL.#INT-AMT-CV(5)	(AD=N)#CNTL.#INT-AMT-CV(6)	(AD=N)#CNTL.#INT-AMT-CV(7)	(AD=N)#CNTL.#INT-AMT-CV(8)	(AD=N)#CNTL.#INT-AMT-CV(9)	(AD=I");
        pnd_Cntl_Pnd_Int_Back_Tax_Cv.setInitialAttributeValue("AD=I)#CNTL.#INT-BACK-TAX-CV(2)	(AD=I)#CNTL.#INT-BACK-TAX-CV(3)	(AD=N)#CNTL.#INT-BACK-TAX-CV(4)	(AD=N)#CNTL.#INT-BACK-TAX-CV(5)	(AD=N)#CNTL.#INT-BACK-TAX-CV(6)	(AD=N)#CNTL.#INT-BACK-TAX-CV(7)	(AD=N)#CNTL.#INT-BACK-TAX-CV(8)	(AD=N)#CNTL.#INT-BACK-TAX-CV(9)	(AD=I");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5511() throws Exception
    {
        super("Twrp5511");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 02 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 03 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 04 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 05 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 06 ) PS = 58 LS = 133 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T 'Summary of Federal Payment Transactions' 120T 'Report: RPT1' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / #WS.#COMPANY-LINE //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 02 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 57T 'State Summary Totals' 120T 'Report: RPT2' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / #WS.#COMPANY-LINE //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 03 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 48T 'State Summary Totals - Non-Reportable' 120T 'Report: RPT3' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / #WS.#COMPANY-LINE //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 04 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T 'State Summary Totals - Gross reportable' 120T 'Report: RPT4' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / #WS.#COMPANY-LINE //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 05 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 05 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'State Summary Totals - Taxable reportable' 120T 'Report: RPT5' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / #WS.#COMPANY-LINE //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 06 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 06 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'State Summary Totals - HardCopy reportable' 120T 'Report: RPT6' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / #WS.#COMPANY-LINE //
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 RECORD #XTAXYR-F94
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaTwrl0900.getPnd_Xtaxyr_F94())))
        {
            CheckAtStartofData327();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: AT START OF DATA;//Natural: BEFORE BREAK PROCESSING
            pnd_Key_Detail.setValuesByName(ldaTwrl0900.getPnd_Xtaxyr_F94());                                                                                              //Natural: MOVE BY NAME #XTAXYR-F94 TO #KEY-DETAIL
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            FOR03:                                                                                                                                                        //Natural: AT BREAK OF #KEY;//Natural: AT BREAK OF #KEY /28/;//Natural: AT BREAK OF #XTAXYR-F94.TWRPYMNT-COMPANY-CDE-FORM;//Natural: AT END OF DATA;//Natural: FOR #I = 1 TO #XTAXYR-F94.#C-TWRPYMNT-PAYMENTS
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments())); pnd_Ws_Pnd_I.nadd(1))
            {
                //*                                                   ALL PAYMENTS
                //*  ------------------------------            JH 6/11/02 BEGIN
                //* *  IF #XTAXYR-F94.TWRPYMNT-COMPANY-CDE-FORM = 'C'
                //* *      AND #XTAXYR-F94.TWRPYMNT-RESIDENCY-CODE = '00'
                //* *    DISPLAY
                //* *      'RES'        #XTAXYR-F94.TWRPYMNT-RESIDENCY-CODE
                //* *      'TAX ID'     #XTAXYR-F94.TWRPYMNT-TAX-ID-NBR
                //* *      'DIST'       #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE
                //* *      'CONTRACT'   #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR
                //* *      'PAYEE'      #XTAXYR-F94.TWRPYMNT-PAYEE-CDE
                //* *      'TAX CIT'    #XTAXYR-F94.TWRPYMNT-TAX-CITIZENSHIP
                //* *      'PYMT/TYPE' #XTAXYR-F94.TWRPYMNT-PAYMENT-TYPE(#I)
                //* *      'PYMT/TYPE' #XTAXYR-F94.TWRPYMNT-SETTLE-TYPE(#I)
                //* *      'GROSS'      #XTAXYR-F94.TWRPYMNT-GROSS-AMT(#I)
                //* *      'IVC'        #XTAXYR-F94.TWRPYMNT-IVC-AMT(#I)
                //* *      'INTEREST'   #XTAXYR-F94.TWRPYMNT-INT-AMT(#I)
                //* *      'FEDERAL'    #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT(#I)
                //* *      'NRA'        #XTAXYR-F94.TWRPYMNT-NRA-WTHLD-AMT(#I)
                //* *  END-IF
                //*  ------------------------------            JH 6/11/02 END
                pnd_Cntl_Pnd_Tran_Cnt.getValue(1,1).nadd(1);                                                                                                              //Natural: ADD 1 TO #CNTL.#TRAN-CNT ( 1,1 )
                pnd_Cntl_Pnd_Gross_Amt.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 1,1 )
                pnd_Cntl_Pnd_Ivc_Amt.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                                         //Natural: ADD #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) TO #CNTL.#IVC-AMT ( 1,1 )
                pnd_Cntl_Pnd_Fed_Tax.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                                   //Natural: ADD #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #CNTL.#FED-TAX ( 1,1 )
                pnd_Cntl_Pnd_Sta_Tax.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) TO #CNTL.#STA-TAX ( 1,1 )
                pnd_Cntl_Pnd_Loc_Tax.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Local_Wthld().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-LOCAL-WTHLD ( #I ) TO #CNTL.#LOC-TAX ( 1,1 )
                pnd_Cntl_Pnd_Int_Amt.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                         //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#INT-AMT ( 1,1 )
                pnd_Cntl_Pnd_Int_Tax.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Bkup_Wthld().getValue(pnd_Ws_Pnd_I));                                  //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-BKUP-WTHLD ( #I ) TO #CNTL.#INT-TAX ( 1,1 )
                if (condition(! (ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals(" ") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals("C")))) //Natural: IF NOT #XTAXYR-F94.TWRPYMNT-PYMNT-STATUS ( #I ) = ' ' OR = 'C'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*                                                   ACTIVE PAYMENTS
                pnd_Cntl_Pnd_Fed_Tax.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                                   //Natural: ADD #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #CNTL.#FED-TAX ( 2,1 )
                pnd_Cntl_Pnd_Sta_Tax.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) TO #CNTL.#STA-TAX ( 2,1 )
                pnd_Cntl_Pnd_Loc_Tax.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Local_Wthld().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-LOCAL-WTHLD ( #I ) TO #CNTL.#LOC-TAX ( 2,1 )
                pnd_Cntl_Pnd_Int_Amt.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                         //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#INT-AMT ( 2,1 )
                pnd_Cntl_Pnd_Int_Tax.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Bkup_Wthld().getValue(pnd_Ws_Pnd_I));                                  //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-BKUP-WTHLD ( #I ) TO #CNTL.#INT-TAX ( 2,1 )
                //*  INTEREST
                if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I).equals(new DbsDecimal("0.00")) && (ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new  //Natural: IF #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) = 0.00 AND ( #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) NE 0.00 OR #XTAXYR-F94.TWRPYMNT-INT-BKUP-WTHLD ( #I ) NE 0.00 )
                    DbsDecimal("0.00")) || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Bkup_Wthld().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00")))))
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_1099.resetInitial();                                                                                                                           //Natural: RESET INITIAL #WS.#1099
                FOR04:                                                                                                                                                    //Natural: FOR #IDX = 2 TO #1099R-MAX
                for (pnd_Ws_Pnd_Idx.setValue(2); condition(pnd_Ws_Pnd_Idx.lessOrEqual(pnd_Ws_Pnd_1099r_Max)); pnd_Ws_Pnd_Idx.nadd(1))
                {
                    if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payment_Type().getValue(pnd_Ws_Pnd_I).equals(pnd_Case_Fields_Pnd_Payment_Type.getValue(pnd_Ws_Pnd_Idx))  //Natural: IF #XTAXYR-F94.TWRPYMNT-PAYMENT-TYPE ( #I ) = #CASE-FIELDS.#PAYMENT-TYPE ( #IDX ) AND #XTAXYR-F94.TWRPYMNT-SETTLE-TYPE ( #I ) = #CASE-FIELDS.#SETTLE-TYPE ( #IDX )
                        && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals(pnd_Case_Fields_Pnd_Settle_Type.getValue(pnd_Ws_Pnd_Idx))))
                    {
                        pnd_Ws_Pnd_1099.setValue(pnd_Ws_Pnd_Idx);                                                                                                         //Natural: ASSIGN #WS.#1099 := #IDX
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Case_Fields_Pnd_Case_Present.getValue(pnd_Ws_Pnd_1099).setValue(true);                                                                                //Natural: ASSIGN #CASE-FIELDS.#CASE-PRESENT ( #1099 ) := TRUE
                //*  KNOWN IVC
                if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Ind().getValue(pnd_Ws_Pnd_I).equals("K") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Protect().getValue(pnd_Ws_Pnd_I).getBoolean()))) //Natural: IF ( #XTAXYR-F94.TWRPYMNT-IVC-IND ( #I ) = 'K' OR #XTAXYR-F94.TWRPYMNT-IVC-PROTECT ( #I ) )
                {
                    pnd_Ws_Pnd_Known_Payment.setValue(true);                                                                                                              //Natural: ASSIGN #WS.#KNOWN-PAYMENT := TRUE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_Known_Payment.reset();                                                                                                                     //Natural: RESET #WS.#KNOWN-PAYMENT
                }                                                                                                                                                         //Natural: END-IF
                pnd_Cntl_Pnd_Tran_Cnt.getValue(2,1).nadd(1);                                                                                                              //Natural: ADD 1 TO #CNTL.#TRAN-CNT ( 2,1 )
                pnd_Cntl_Pnd_Gross_Amt.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 2,1 )
                pnd_Cntl_Pnd_Ivc_Amt.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                                         //Natural: ADD #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) TO #CNTL.#IVC-AMT ( 2,1 )
                if (condition(pnd_Ws_Pnd_Known_Payment.getBoolean()))                                                                                                     //Natural: IF #WS.#KNOWN-PAYMENT
                {
                    //*  ------------------------------            JH 6/11/02
                    if (condition(pnd_Case_Fields_Pnd_Rechar.getBoolean() || pnd_Case_Fields_Pnd_Roll.getBoolean()))                                                      //Natural: IF #CASE-FIELDS.#RECHAR OR #CASE-FIELDS.#ROLL
                    {
                        pnd_Cntl_Pnd_Ivc_Amt.getValue(3,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) TO #CNTL.#IVC-AMT ( 3,1 )
                        if (condition(pnd_Case_Fields_Pnd_Roll.getBoolean() && pnd_Ws_Pnd_Tax_Year.greater(2001)))                                                        //Natural: IF #CASE-FIELDS.#ROLL AND #WS.#TAX-YEAR > 2001
                        {
                            pnd_Cntl_Pnd_Ivc_Amt.getValue(3,1).nsubtract(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                        //Natural: SUBTRACT #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) FROM #CNTL.#IVC-AMT ( 3,1 )
                        }                                                                                                                                                 //Natural: END-IF
                        //*  ------------------------------
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Case_Fields_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099).compute(new ComputeParameters(false, pnd_Case_Fields_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099)),  //Natural: COMPUTE #CASE-FIELDS.#TAXABLE-AMT ( #1099 ) = #CASE-FIELDS.#TAXABLE-AMT ( #1099 ) + #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) - #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I )
                            pnd_Case_Fields_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099).add(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I)).subtract(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I)));
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Case_Fields_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                  //Natural: ADD #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) TO #CASE-FIELDS.#IVC-AMT ( #1099 )
                    //*  UNKNOWN IVC
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cntl_Pnd_Ivc_Amt.getValue(5,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) TO #CNTL.#IVC-AMT ( 5,1 )
                    pnd_Case_Fields_Pnd_Tnd.getValue(pnd_Ws_Pnd_1099).setValue(true);                                                                                     //Natural: ASSIGN #CASE-FIELDS.#TND ( #1099 ) := TRUE
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet613 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #XTAXYR-F94.TWRPYMNT-RESIDENCY-CODE = '42' OR = 'RQ'
                if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code().equals("42") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code().equals("RQ")))
                {
                    decideConditionsMet613++;
                    pnd_Cntl_Pnd_Sta_Tax.getValue(7,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) TO #CNTL.#STA-TAX ( 7,1 )
                    pnd_Cntl_Pnd_Loc_Tax.getValue(7,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Local_Wthld().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-LOCAL-WTHLD ( #I ) TO #CNTL.#LOC-TAX ( 7,1 )
                }                                                                                                                                                         //Natural: WHEN #XTAXYR-F94.TWRPYMNT-RESIDENCY-TYPE NE '1'
                else if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Type().notEquals("1")))
                {
                    decideConditionsMet613++;
                    pnd_Cntl_Pnd_Sta_Tax.getValue(8,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) TO #CNTL.#STA-TAX ( 8,1 )
                    pnd_Cntl_Pnd_Loc_Tax.getValue(8,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Local_Wthld().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-LOCAL-WTHLD ( #I ) TO #CNTL.#LOC-TAX ( 8,1 )
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //*    STATE REPORTS
                pnd_St_Case_Pnd_Case_Present.getValue(pnd_Ws_Pnd_1099).setValue(true);                                                                                    //Natural: ASSIGN #ST-CASE.#CASE-PRESENT ( #1099 ) := TRUE
                pnd_St_Case_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_1099).nadd(1);                                                                                               //Natural: ADD 1 TO #ST-CASE.#TRAN-CNT ( #1099 )
                pnd_St_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_1099).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                      //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #ST-CASE.#GROSS-AMT ( #1099 )
                pnd_St_Case_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                          //Natural: ADD #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) TO #ST-CASE.#IVC-AMT ( #1099 )
                pnd_St_Case_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_1099).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I));                      //Natural: ADD #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) TO #ST-CASE.#STA-TAX ( #1099 )
                pnd_St_Case_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_1099).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Local_Wthld().getValue(pnd_Ws_Pnd_I));                      //Natural: ADD #XTAXYR-F94.TWRPYMNT-LOCAL-WTHLD ( #I ) TO #ST-CASE.#LOC-TAX ( #1099 )
                if (condition(pnd_Ws_Pnd_Known_Payment.getBoolean()))                                                                                                     //Natural: IF #WS.#KNOWN-PAYMENT
                {
                    pnd_St_Case_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099).compute(new ComputeParameters(false, pnd_St_Case_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099)),    //Natural: COMPUTE #ST-CASE.#TAXABLE-AMT ( #1099 ) = #ST-CASE.#TAXABLE-AMT ( #1099 ) + #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) - #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I )
                        pnd_St_Case_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099).add(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I)).subtract(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I)));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_St_Case_Pnd_Tnd.getValue(pnd_Ws_Pnd_1099).setValue(true);                                                                                         //Natural: ASSIGN #ST-CASE.#TND ( #1099 ) := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            readWork01Twrpymnt_Distribution_CdeOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde());                                                   //Natural: END-WORK
            readWork01Twrpymnt_Residency_TypeOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Type());
            readWork01Twrpymnt_Residency_CodeOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code());
            readWork01Twrpymnt_Company_Cde_FormOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form());
            readWork01Twrpymnt_Tax_YearOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            pnd_Ws_Pnd_Company_Line.setValue("Grand Totals");                                                                                                             //Natural: ASSIGN #WS.#COMPANY-LINE := 'Grand Totals'
            pnd_Ws_Pnd_S2.setValue(2);                                                                                                                                    //Natural: ASSIGN #WS.#S2 := 2
                                                                                                                                                                          //Natural: PERFORM WRITE-FED-REPORT
            sub_Write_Fed_Report();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //* **********************
        //*  S U B R O U T I N E S
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-FED-REPORT
        //* *********************************
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COMPANY
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-STATE-TABLE
        //* *********************************
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-STATE-ENTRY
    }
    private void sub_Write_Fed_Report() throws Exception                                                                                                                  //Natural: WRITE-FED-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Cntl_Pnd_Gross_Amt.getValue(9,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Gross_Amt.getValue(2,pnd_Ws_Pnd_S2));                                                      //Natural: ASSIGN #CNTL.#GROSS-AMT ( 9,#S2 ) := #CNTL.#GROSS-AMT ( 2,#S2 )
        pnd_Cntl_Pnd_Int_Amt.getValue(9,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Int_Amt.getValue(2,pnd_Ws_Pnd_S2));                                                          //Natural: ASSIGN #CNTL.#INT-AMT ( 9,#S2 ) := #CNTL.#INT-AMT ( 2,#S2 )
        pnd_Cntl_Pnd_Ivc_Amt.getValue(9,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Ivc_Amt.getValue(9,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Ivc_Amt.getValue(2, //Natural: ASSIGN #CNTL.#IVC-AMT ( 9,#S2 ) := #CNTL.#IVC-AMT ( 2,#S2 ) - #CNTL.#IVC-AMT ( 3:8,#S2 )
            pnd_Ws_Pnd_S2).subtract(pnd_Cntl_Pnd_Ivc_Amt.getValue(3,":",8,pnd_Ws_Pnd_S2)));
        pnd_Cntl_Pnd_Taxable_Amt.getValue(9,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Taxable_Amt.getValue(9,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Taxable_Amt.getValue(2, //Natural: ASSIGN #CNTL.#TAXABLE-AMT ( 9,#S2 ) := #CNTL.#TAXABLE-AMT ( 2,#S2 ) - #CNTL.#TAXABLE-AMT ( 3:8,#S2 )
            pnd_Ws_Pnd_S2).subtract(pnd_Cntl_Pnd_Taxable_Amt.getValue(3,":",8,pnd_Ws_Pnd_S2)));
        pnd_Cntl_Pnd_Fed_Tax.getValue(9,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Fed_Tax.getValue(2,pnd_Ws_Pnd_S2));                                                          //Natural: ASSIGN #CNTL.#FED-TAX ( 9,#S2 ) := #CNTL.#FED-TAX ( 2,#S2 )
        pnd_Cntl_Pnd_Int_Tax.getValue(9,pnd_Ws_Pnd_S2).setValue(pnd_Cntl_Pnd_Int_Tax.getValue(2,pnd_Ws_Pnd_S2));                                                          //Natural: ASSIGN #CNTL.#INT-TAX ( 9,#S2 ) := #CNTL.#INT-TAX ( 2,#S2 )
        pnd_Cntl_Pnd_Sta_Tax.getValue(9,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Sta_Tax.getValue(9,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Sta_Tax.getValue(2, //Natural: ASSIGN #CNTL.#STA-TAX ( 9,#S2 ) := #CNTL.#STA-TAX ( 2,#S2 ) - #CNTL.#STA-TAX ( 3:8,#S2 )
            pnd_Ws_Pnd_S2).subtract(pnd_Cntl_Pnd_Sta_Tax.getValue(3,":",8,pnd_Ws_Pnd_S2)));
        pnd_Cntl_Pnd_Loc_Tax.getValue(9,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Loc_Tax.getValue(9,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Loc_Tax.getValue(2, //Natural: ASSIGN #CNTL.#LOC-TAX ( 9,#S2 ) := #CNTL.#LOC-TAX ( 2,#S2 ) - #CNTL.#LOC-TAX ( 3:8,#S2 )
            pnd_Ws_Pnd_S2).subtract(pnd_Cntl_Pnd_Loc_Tax.getValue(3,":",8,pnd_Ws_Pnd_S2)));
        FOR05:                                                                                                                                                            //Natural: FOR #WS.#S1 = 1 TO #CNTL-MAX
        for (pnd_Ws_Pnd_S1.setValue(1); condition(pnd_Ws_Pnd_S1.lessOrEqual(pnd_Ws_Const_Pnd_Cntl_Max)); pnd_Ws_Pnd_S1.nadd(1))
        {
            pnd_Ws_Pnd_Tran_Cv.setValue(pnd_Cntl_Pnd_Tran_Cnt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                //Natural: ASSIGN #WS.#TRAN-CV := #CNTL.#TRAN-CNT-CV ( #S1 )
            pnd_Ws_Pnd_Gross_Cv.setValue(pnd_Cntl_Pnd_Gross_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                              //Natural: ASSIGN #WS.#GROSS-CV := #CNTL.#GROSS-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Ivc_Cv.setValue(pnd_Cntl_Pnd_Ivc_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#IVC-CV := #CNTL.#IVC-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Taxable_Cv.setValue(pnd_Cntl_Pnd_Taxable_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                          //Natural: ASSIGN #WS.#TAXABLE-CV := #CNTL.#TAXABLE-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Fed_Cv.setValue(pnd_Cntl_Pnd_Fed_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#FED-CV := #CNTL.#FED-TAX-CV ( #S1 )
            pnd_Ws_Pnd_Sta_Cv.setValue(pnd_Cntl_Pnd_Sta_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#STA-CV := #CNTL.#STA-TAX-CV ( #S1 )
            pnd_Ws_Pnd_Loc_Cv.setValue(pnd_Cntl_Pnd_Loc_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#LOC-CV := #CNTL.#LOC-TAX-CV ( #S1 )
            pnd_Ws_Pnd_Int_Cv.setValue(pnd_Cntl_Pnd_Int_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#INT-CV := #CNTL.#INT-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Int_Back_Cv.setValue(pnd_Cntl_Pnd_Int_Back_Tax_Cv.getValue(pnd_Ws_Pnd_S1));                                                                        //Natural: ASSIGN #WS.#INT-BACK-CV := #CNTL.#INT-BACK-TAX-CV ( #S1 )
            getReports().display(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",                         //Natural: DISPLAY ( 1 ) ( HC = R ES = ON ) '/' #CNTL-TEXT ( #S1 ) / '/' #CNTL-TEXT1 ( #S1 ) '/Trans Count' #CNTL.#TRAN-CNT ( #S1,#S2 ) ( CV = #TRAN-CV ) 'Gross Amount' #CNTL.#GROSS-AMT ( #S1,#S2 ) ( CV = #GROSS-CV ) / 3X 'Interest' #CNTL.#INT-AMT ( #S1,#S2 ) ( CV = #INT-CV ) '/IVC Amount' #CNTL.#IVC-AMT ( #S1,#S2 ) ( CV = #IVC-CV ) '/Taxable Amount' #CNTL.#TAXABLE-AMT ( #S1,#S2 ) ( CV = #TAXABLE-CV ) 'Federal Tax' #CNTL.#FED-TAX ( #S1,#S2 ) ( CV = #FED-CV ) / 'Backup Tax' #CNTL.#INT-TAX ( #S1,#S2 ) ( CV = #INT-BACK-CV ) 'State   Tax' #CNTL.#STA-TAX ( #S1,#S2 ) ( CV = #STA-CV ) / 'Local   Tax' #CNTL.#LOC-TAX ( #S1,#S2 ) ( CV = #LOC-CV )
            		pnd_Cntl_Pnd_Cntl_Text.getValue(pnd_Ws_Pnd_S1),NEWLINE,"/",
            		pnd_Cntl_Pnd_Cntl_Text1.getValue(pnd_Ws_Pnd_S1),"/Trans Count",
            		pnd_Cntl_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Tran_Cv,"Gross Amount",
            		pnd_Cntl_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Gross_Cv,NEWLINE,new ColumnSpacing(3),"Interest",
            		pnd_Cntl_Pnd_Int_Amt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Int_Cv,"/IVC Amount",
            		pnd_Cntl_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Ivc_Cv,"/Taxable Amount",
            		pnd_Cntl_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Taxable_Cv,"Federal Tax",
            		pnd_Cntl_Pnd_Fed_Tax.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Fed_Cv,NEWLINE,"Backup Tax",
            		pnd_Cntl_Pnd_Int_Tax.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Int_Back_Cv,"State   Tax",
            		pnd_Cntl_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Sta_Cv,NEWLINE,"Local   Tax",
            		pnd_Cntl_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Loc_Cv);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1 LINES
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR06:                                                                                                                                                            //Natural: FOR #I = 1 TO 5
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(5)); pnd_Ws_Pnd_I.nadd(1))
        {
            gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt().getValue(60 + 1,1,pnd_Ws_Pnd_I).nadd(gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt().getValue(0 + 1,":",                    //Natural: ADD #ST-TOT.#TRAN-CNT ( 0:59,1,#I ) TO #ST-TOT.#TRAN-CNT ( 60,1,#I )
                59 + 1,1,pnd_Ws_Pnd_I));
            gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt().getValue(62 + 1,1,pnd_Ws_Pnd_I).nadd(gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt().getValue(60 + 1,":",                   //Natural: ADD #ST-TOT.#TRAN-CNT ( 60:61,1,#I ) TO #ST-TOT.#TRAN-CNT ( 62,1,#I )
                61 + 1,1,pnd_Ws_Pnd_I));
            gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt().getValue(60 + 1,1,pnd_Ws_Pnd_I).nadd(gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt().getValue(0 + 1,":",                  //Natural: ADD #ST-TOT.#GROSS-AMT ( 0:59,1,#I ) TO #ST-TOT.#GROSS-AMT ( 60,1,#I )
                59 + 1,1,pnd_Ws_Pnd_I));
            gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt().getValue(62 + 1,1,pnd_Ws_Pnd_I).nadd(gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt().getValue(60 + 1,":",                 //Natural: ADD #ST-TOT.#GROSS-AMT ( 60:61,1,#I ) TO #ST-TOT.#GROSS-AMT ( 62,1,#I )
                61 + 1,1,pnd_Ws_Pnd_I));
            gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc().getValue(60 + 1,1,pnd_Ws_Pnd_I).nadd(gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc().getValue(0 + 1,":",                  //Natural: ADD #ST-TOT.#PYMNT-IVC ( 0:59,1,#I ) TO #ST-TOT.#PYMNT-IVC ( 60,1,#I )
                59 + 1,1,pnd_Ws_Pnd_I));
            gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc().getValue(62 + 1,1,pnd_Ws_Pnd_I).nadd(gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc().getValue(60 + 1,":",                 //Natural: ADD #ST-TOT.#PYMNT-IVC ( 60:61,1,#I ) TO #ST-TOT.#PYMNT-IVC ( 62,1,#I )
                61 + 1,1,pnd_Ws_Pnd_I));
            gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj().getValue(60 + 1,1,pnd_Ws_Pnd_I).nadd(gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj().getValue(0 + 1,":",59 + 1,               //Natural: ADD #ST-TOT.#IVC-ADJ ( 0:59,1,#I ) TO #ST-TOT.#IVC-ADJ ( 60,1,#I )
                1,pnd_Ws_Pnd_I));
            gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj().getValue(62 + 1,1,pnd_Ws_Pnd_I).nadd(gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj().getValue(60 + 1,":",61 + 1,              //Natural: ADD #ST-TOT.#IVC-ADJ ( 60:61,1,#I ) TO #ST-TOT.#IVC-ADJ ( 62,1,#I )
                1,pnd_Ws_Pnd_I));
            gdaTwrg5501.getPnd_St_Tot_Pnd_Form_Ivc().getValue(0 + 1,":",59 + 1,1,pnd_Ws_Pnd_I).compute(new ComputeParameters(false, gdaTwrg5501.getPnd_St_Tot_Pnd_Form_Ivc().getValue(0  //Natural: ASSIGN #ST-TOT.#FORM-IVC ( 0:59,1,#I ) := #ST-TOT.#PYMNT-IVC ( 0:59,1,#I ) - #ST-TOT.#IVC-ADJ ( 0:59,1,#I )
                + 1,":",59 + 1,1,pnd_Ws_Pnd_I)), gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc().getValue(0 + 1,":",59 + 1,1,pnd_Ws_Pnd_I).subtract(gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj().getValue(0 
                + 1,":",59 + 1,1,pnd_Ws_Pnd_I)));
            gdaTwrg5501.getPnd_St_Tot_Pnd_Form_Ivc().getValue(61 + 1,1,pnd_Ws_Pnd_I).compute(new ComputeParameters(false, gdaTwrg5501.getPnd_St_Tot_Pnd_Form_Ivc().getValue(61  //Natural: ASSIGN #ST-TOT.#FORM-IVC ( 61,1,#I ) := #ST-TOT.#PYMNT-IVC ( 61,1,#I ) - #ST-TOT.#IVC-ADJ ( 61,1,#I )
                + 1,1,pnd_Ws_Pnd_I)), gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc().getValue(61 + 1,1,pnd_Ws_Pnd_I).subtract(gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj().getValue(61 
                + 1,1,pnd_Ws_Pnd_I)));
            gdaTwrg5501.getPnd_St_Tot_Pnd_Form_Ivc().getValue(60 + 1,1,pnd_Ws_Pnd_I).nadd(gdaTwrg5501.getPnd_St_Tot_Pnd_Form_Ivc().getValue(0 + 1,":",                    //Natural: ADD #ST-TOT.#FORM-IVC ( 0:59,1,#I ) TO #ST-TOT.#FORM-IVC ( 60,1,#I )
                59 + 1,1,pnd_Ws_Pnd_I));
            gdaTwrg5501.getPnd_St_Tot_Pnd_Form_Ivc().getValue(62 + 1,1,pnd_Ws_Pnd_I).nadd(gdaTwrg5501.getPnd_St_Tot_Pnd_Form_Ivc().getValue(60 + 1,":",                   //Natural: ADD #ST-TOT.#FORM-IVC ( 60:61,1,#I ) TO #ST-TOT.#FORM-IVC ( 62,1,#I )
                61 + 1,1,pnd_Ws_Pnd_I));
            pnd_St_To1_Pnd_Pmnt_Taxable.getValue(60 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_To1_Pnd_Pmnt_Taxable.getValue(0 + 1,":",59 + 1,1,pnd_Ws_Pnd_I));                      //Natural: ADD #ST-TO1.#PMNT-TAXABLE ( 0:59,1,#I ) TO #ST-TO1.#PMNT-TAXABLE ( 60,1,#I )
            pnd_St_To1_Pnd_Pmnt_Taxable.getValue(62 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_To1_Pnd_Pmnt_Taxable.getValue(60 + 1,":",61 + 1,1,pnd_Ws_Pnd_I));                     //Natural: ADD #ST-TO1.#PMNT-TAXABLE ( 60:61,1,#I ) TO #ST-TO1.#PMNT-TAXABLE ( 62,1,#I )
            pnd_St_To1_Pnd_Taxable_Adj.getValue(60 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_To1_Pnd_Taxable_Adj.getValue(0 + 1,":",59 + 1,1,pnd_Ws_Pnd_I));                        //Natural: ADD #ST-TO1.#TAXABLE-ADJ ( 0:59,1,#I ) TO #ST-TO1.#TAXABLE-ADJ ( 60,1,#I )
            pnd_St_To1_Pnd_Taxable_Adj.getValue(62 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_To1_Pnd_Taxable_Adj.getValue(60 + 1,":",61 + 1,1,pnd_Ws_Pnd_I));                       //Natural: ADD #ST-TO1.#TAXABLE-ADJ ( 60:61,1,#I ) TO #ST-TO1.#TAXABLE-ADJ ( 62,1,#I )
            pnd_St_To1_Pnd_Form_Taxable.getValue(0 + 1,":",59 + 1,1,pnd_Ws_Pnd_I).compute(new ComputeParameters(false, pnd_St_To1_Pnd_Form_Taxable.getValue(0             //Natural: ASSIGN #ST-TO1.#FORM-TAXABLE ( 0:59,1,#I ) := #ST-TO1.#PMNT-TAXABLE ( 0:59,1,#I ) + #ST-TO1.#TAXABLE-ADJ ( 0:59,1,#I )
                + 1,":",59 + 1,1,pnd_Ws_Pnd_I)), pnd_St_To1_Pnd_Pmnt_Taxable.getValue(0 + 1,":",59 + 1,1,pnd_Ws_Pnd_I).add(pnd_St_To1_Pnd_Taxable_Adj.getValue(0 
                + 1,":",59 + 1,1,pnd_Ws_Pnd_I)));
            pnd_St_To1_Pnd_Form_Taxable.getValue(61 + 1,1,pnd_Ws_Pnd_I).compute(new ComputeParameters(false, pnd_St_To1_Pnd_Form_Taxable.getValue(61 +                    //Natural: ASSIGN #ST-TO1.#FORM-TAXABLE ( 61,1,#I ) := #ST-TO1.#PMNT-TAXABLE ( 61,1,#I ) + #ST-TO1.#TAXABLE-ADJ ( 61,1,#I )
                1,1,pnd_Ws_Pnd_I)), pnd_St_To1_Pnd_Pmnt_Taxable.getValue(61 + 1,1,pnd_Ws_Pnd_I).add(pnd_St_To1_Pnd_Taxable_Adj.getValue(61 + 1,1,pnd_Ws_Pnd_I)));
            pnd_St_To1_Pnd_Form_Taxable.getValue(60 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_To1_Pnd_Form_Taxable.getValue(0 + 1,":",59 + 1,1,pnd_Ws_Pnd_I));                      //Natural: ADD #ST-TO1.#FORM-TAXABLE ( 0:59,1,#I ) TO #ST-TO1.#FORM-TAXABLE ( 60,1,#I )
            pnd_St_To1_Pnd_Form_Taxable.getValue(62 + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_To1_Pnd_Form_Taxable.getValue(60 + 1,":",61 + 1,1,pnd_Ws_Pnd_I));                     //Natural: ADD #ST-TO1.#FORM-TAXABLE ( 60:61,1,#I ) TO #ST-TO1.#FORM-TAXABLE ( 62,1,#I )
            gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax().getValue(60 + 1,1,pnd_Ws_Pnd_I).nadd(gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax().getValue(0 + 1,":",59 + 1,               //Natural: ADD #ST-TOT.#STA-TAX ( 0:59,1,#I ) TO #ST-TOT.#STA-TAX ( 60,1,#I )
                1,pnd_Ws_Pnd_I));
            gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax().getValue(62 + 1,1,pnd_Ws_Pnd_I).nadd(gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax().getValue(60 + 1,":",61 + 1,              //Natural: ADD #ST-TOT.#STA-TAX ( 60:61,1,#I ) TO #ST-TOT.#STA-TAX ( 62,1,#I )
                1,pnd_Ws_Pnd_I));
            gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax().getValue(60 + 1,1,pnd_Ws_Pnd_I).nadd(gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax().getValue(0 + 1,":",59 + 1,               //Natural: ADD #ST-TOT.#LOC-TAX ( 0:59,1,#I ) TO #ST-TOT.#LOC-TAX ( 60,1,#I )
                1,pnd_Ws_Pnd_I));
            gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax().getValue(62 + 1,1,pnd_Ws_Pnd_I).nadd(gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax().getValue(60 + 1,":",61 + 1,              //Natural: ADD #ST-TOT.#LOC-TAX ( 60:61,1,#I ) TO #ST-TOT.#LOC-TAX ( 62,1,#I )
                1,pnd_Ws_Pnd_I));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(3));                                                                                                                 //Natural: NEWPAGE ( 3 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(4));                                                                                                                 //Natural: NEWPAGE ( 4 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(5));                                                                                                                 //Natural: NEWPAGE ( 5 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(6));                                                                                                                 //Natural: NEWPAGE ( 6 )
        if (condition(Global.isEscape())){return;}
        FOR07:                                                                                                                                                            //Natural: FOR #WS.#S1 = 0 TO #ST-TOT-MAX
        for (pnd_Ws_Pnd_S1.setValue(0); condition(pnd_Ws_Pnd_S1.lessOrEqual(gdaTwrg5501.getPnd_St_Tot_Max())); pnd_Ws_Pnd_S1.nadd(1))
        {
            if (condition(gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Desc().getValue(pnd_Ws_Pnd_S1.getInt() + 1).notEquals(" ")))                                               //Natural: IF #ST-CNTL.#STATE-DESC ( #S1 ) NE ' '
            {
                getReports().display(2, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",                     //Natural: DISPLAY ( 2 ) ( HC = R ES = ON ) '/' #ST-CNTL.#STATE-DESC ( #S1 ) '//Trans Count' #ST-TOT.#TRAN-CNT ( #S1,#S2,1 ) '//Gross Amount' #ST-TOT.#GROSS-AMT ( #S1,#S2,1 ) 'Payment IVC' #ST-TOT.#PYMNT-IVC ( #S1,#S2,1 ) / 'Adjustment IVC' #ST-TOT.#IVC-ADJ ( #S1,#S2,1 ) / 'Form IVC' #ST-TOT.#FORM-IVC ( #S1,#S2,1 ) 'Payment Taxable' #ST-TO1.#PMNT-TAXABLE ( #S1,#S2,1 ) / 'Adjustment Taxable' #ST-TO1.#TAXABLE-ADJ ( #S1,#S2,1 ) / 'Form Taxable' #ST-TO1.#FORM-TAXABLE ( #S1,#S2,1 ) '//State Tax' #ST-TOT.#STA-TAX ( #S1,#S2,1 ) '//Local Tax' #ST-TOT.#LOC-TAX ( #S1,#S2,1 )
                		gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Desc().getValue(pnd_Ws_Pnd_S1.getInt() + 1),"//Trans Count",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1),"//Gross Amount",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1),"Payment IVC",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1),NEWLINE,"Adjustment IVC",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1),NEWLINE,"Form IVC",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Form_Ivc().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1),"Payment Taxable",
                		pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1),NEWLINE,"Adjustment Taxable",
                		pnd_St_To1_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1),NEWLINE,"Form Taxable",
                		pnd_St_To1_Pnd_Form_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1),"//State Tax",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1),"//Local Tax",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_S1.equals(59) || pnd_Ws_Pnd_S1.equals(61)))                                                                                      //Natural: IF #WS.#S1 = 59 OR = 61
                {
                    getReports().write(2, new ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1)),"-",new      //Natural: WRITE ( 2 ) T*#ST-TOT.#TRAN-CNT ( #S1,#S2,1 ) '-' ( 12 ) T*#ST-TOT.#GROSS-AMT ( #S1,#S2,1 ) '-' ( 18 ) T*#ST-TOT.#PYMNT-IVC ( #S1,#S2,1 ) '-' ( 15 ) T*#ST-TO1.#PMNT-TAXABLE ( #S1,#S2,1 ) '-' ( 18 ) T*#ST-TOT.#STA-TAX ( #S1,#S2,1 ) '-' ( 15 ) T*#ST-TOT.#LOC-TAX ( #S1,#S2,1 ) '-' ( 15 )
                        RepeatItem(12),new ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1)),"-",new 
                        RepeatItem(18),new ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1)),"-",new 
                        RepeatItem(15),new ReportTAsterisk(pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1)),"-",new RepeatItem(18),new 
                        ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1)),"-",new RepeatItem(15),new 
                        ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,1)),"-",new RepeatItem(15));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Desc().getValue(pnd_Ws_Pnd_S1.getInt() + 1).notEquals(" ")))                                               //Natural: IF #ST-CNTL.#STATE-DESC ( #S1 ) NE ' '
            {
                getReports().display(3, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",                     //Natural: DISPLAY ( 3 ) ( HC = R ES = ON ) '/' #ST-CNTL.#STATE-DESC ( #S1 ) '//Trans Count' #ST-TOT.#TRAN-CNT ( #S1,#S2,2 ) '//Gross Amount' #ST-TOT.#GROSS-AMT ( #S1,#S2,2 ) 'Payment IVC' #ST-TOT.#PYMNT-IVC ( #S1,#S2,2 ) / 'Adjustment IVC' #ST-TOT.#IVC-ADJ ( #S1,#S2,2 ) / 'Form IVC' #ST-TOT.#FORM-IVC ( #S1,#S2,2 ) 'Payment Taxable' #ST-TO1.#PMNT-TAXABLE ( #S1,#S2,2 ) / 'Adjustment Taxable' #ST-TO1.#TAXABLE-ADJ ( #S1,#S2,2 ) / 'Form Taxable' #ST-TO1.#FORM-TAXABLE ( #S1,#S2,2 ) '//State Tax' #ST-TOT.#STA-TAX ( #S1,#S2,2 ) '//Local Tax' #ST-TOT.#LOC-TAX ( #S1,#S2,2 )
                		gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Desc().getValue(pnd_Ws_Pnd_S1.getInt() + 1),"//Trans Count",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2),"//Gross Amount",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2),"Payment IVC",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2),NEWLINE,"Adjustment IVC",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2),NEWLINE,"Form IVC",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Form_Ivc().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2),"Payment Taxable",
                		pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2),NEWLINE,"Adjustment Taxable",
                		pnd_St_To1_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2),NEWLINE,"Form Taxable",
                		pnd_St_To1_Pnd_Form_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2),"//State Tax",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2),"//Local Tax",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_S1.equals(59) || pnd_Ws_Pnd_S1.equals(61)))                                                                                      //Natural: IF #WS.#S1 = 59 OR = 61
                {
                    getReports().write(3, new ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2)),"-",new      //Natural: WRITE ( 3 ) T*#ST-TOT.#TRAN-CNT ( #S1,#S2,2 ) '-' ( 12 ) T*#ST-TOT.#GROSS-AMT ( #S1,#S2,2 ) '-' ( 18 ) T*#ST-TOT.#PYMNT-IVC ( #S1,#S2,2 ) '-' ( 15 ) T*#ST-TO1.#PMNT-TAXABLE ( #S1,#S2,2 ) '-' ( 18 ) T*#ST-TOT.#STA-TAX ( #S1,#S2,2 ) '-' ( 15 ) T*#ST-TOT.#LOC-TAX ( #S1,#S2,2 ) '-' ( 15 )
                        RepeatItem(12),new ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2)),"-",new 
                        RepeatItem(18),new ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2)),"-",new 
                        RepeatItem(15),new ReportTAsterisk(pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2)),"-",new RepeatItem(18),new 
                        ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2)),"-",new RepeatItem(15),new 
                        ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,2)),"-",new RepeatItem(15));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Desc().getValue(pnd_Ws_Pnd_S1.getInt() + 1).notEquals(" ") && gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Gross_Rpt().getValue(pnd_Ws_Pnd_S1.getInt()  //Natural: IF #ST-CNTL.#STATE-DESC ( #S1 ) NE ' ' AND #ST-CNTL.#STATE-GROSS-RPT ( #S1 )
                + 1).getBoolean()))
            {
                getReports().display(4, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",                     //Natural: DISPLAY ( 4 ) ( HC = R ES = ON ) '/' #ST-CNTL.#STATE-DESC ( #S1 ) '//Trans Count' #ST-TOT.#TRAN-CNT ( #S1,#S2,3 ) '//Gross Amount' #ST-TOT.#GROSS-AMT ( #S1,#S2,3 ) 'Payment IVC' #ST-TOT.#PYMNT-IVC ( #S1,#S2,3 ) / 'Adjustment IVC' #ST-TOT.#IVC-ADJ ( #S1,#S2,3 ) / 'Form IVC' #ST-TOT.#FORM-IVC ( #S1,#S2,3 ) 'Payment Taxable' #ST-TO1.#PMNT-TAXABLE ( #S1,#S2,3 ) / 'Adjustment Taxable' #ST-TO1.#TAXABLE-ADJ ( #S1,#S2,3 ) / 'Form Taxable' #ST-TO1.#FORM-TAXABLE ( #S1,#S2,3 ) '//State Tax' #ST-TOT.#STA-TAX ( #S1,#S2,3 ) '//Local Tax' #ST-TOT.#LOC-TAX ( #S1,#S2,3 )
                		gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Desc().getValue(pnd_Ws_Pnd_S1.getInt() + 1),"//Trans Count",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3),"//Gross Amount",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3),"Payment IVC",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3),NEWLINE,"Adjustment IVC",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3),NEWLINE,"Form IVC",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Form_Ivc().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3),"Payment Taxable",
                		pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3),NEWLINE,"Adjustment Taxable",
                		pnd_St_To1_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3),NEWLINE,"Form Taxable",
                		pnd_St_To1_Pnd_Form_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3),"//State Tax",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3),"//Local Tax",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_S1.equals(59) || pnd_Ws_Pnd_S1.equals(61)))                                                                                          //Natural: IF #WS.#S1 = 59 OR = 61
            {
                getReports().write(4, new ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3)),"-",new          //Natural: WRITE ( 4 ) T*#ST-TOT.#TRAN-CNT ( #S1,#S2,3 ) '-' ( 12 ) T*#ST-TOT.#GROSS-AMT ( #S1,#S2,3 ) '-' ( 18 ) T*#ST-TOT.#PYMNT-IVC ( #S1,#S2,3 ) '-' ( 15 ) T*#ST-TO1.#PMNT-TAXABLE ( #S1,#S2,3 ) '-' ( 18 ) T*#ST-TOT.#STA-TAX ( #S1,#S2,3 ) '-' ( 15 ) T*#ST-TOT.#LOC-TAX ( #S1,#S2,3 ) '-' ( 15 )
                    RepeatItem(12),new ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3)),"-",new 
                    RepeatItem(18),new ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3)),"-",new 
                    RepeatItem(15),new ReportTAsterisk(pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3)),"-",new RepeatItem(18),new 
                    ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3)),"-",new RepeatItem(15),new 
                    ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,3)),"-",new RepeatItem(15));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Desc().getValue(pnd_Ws_Pnd_S1.getInt() + 1).notEquals(" ") && ! (gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Gross_Rpt().getValue(pnd_Ws_Pnd_S1.getInt()  //Natural: IF #ST-CNTL.#STATE-DESC ( #S1 ) NE ' ' AND NOT #ST-CNTL.#STATE-GROSS-RPT ( #S1 ) OR #WS.#S1 = 60
                + 1).equals(true))) || pnd_Ws_Pnd_S1.equals(60))))
            {
                getReports().display(5, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",                     //Natural: DISPLAY ( 5 ) ( HC = R ES = ON ) '/' #ST-CNTL.#STATE-DESC ( #S1 ) '//Trans Count' #ST-TOT.#TRAN-CNT ( #S1,#S2,4 ) '//Gross Amount' #ST-TOT.#GROSS-AMT ( #S1,#S2,4 ) 'Payment IVC' #ST-TOT.#PYMNT-IVC ( #S1,#S2,4 ) / 'Adjustment IVC' #ST-TOT.#IVC-ADJ ( #S1,#S2,4 ) / 'Form IVC' #ST-TOT.#FORM-IVC ( #S1,#S2,4 ) 'Payment Taxable' #ST-TO1.#PMNT-TAXABLE ( #S1,#S2,4 ) / 'Adjustment Taxable' #ST-TO1.#TAXABLE-ADJ ( #S1,#S2,4 ) / 'Form Taxable' #ST-TO1.#FORM-TAXABLE ( #S1,#S2,4 ) '//State Tax' #ST-TOT.#STA-TAX ( #S1,#S2,4 ) '//Local Tax' #ST-TOT.#LOC-TAX ( #S1,#S2,4 )
                		gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Desc().getValue(pnd_Ws_Pnd_S1.getInt() + 1),"//Trans Count",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4),"//Gross Amount",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4),"Payment IVC",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4),NEWLINE,"Adjustment IVC",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4),NEWLINE,"Form IVC",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Form_Ivc().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4),"Payment Taxable",
                		pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4),NEWLINE,"Adjustment Taxable",
                		pnd_St_To1_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4),NEWLINE,"Form Taxable",
                		pnd_St_To1_Pnd_Form_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4),"//State Tax",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4),"//Local Tax",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_S1.equals(59)))                                                                                                                      //Natural: IF #WS.#S1 = 59
            {
                getReports().write(5, new ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4)),"-",new          //Natural: WRITE ( 5 ) T*#ST-TOT.#TRAN-CNT ( #S1,#S2,4 ) '-' ( 12 ) T*#ST-TOT.#GROSS-AMT ( #S1,#S2,4 ) '-' ( 18 ) T*#ST-TOT.#PYMNT-IVC ( #S1,#S2,4 ) '-' ( 15 ) T*#ST-TO1.#PMNT-TAXABLE ( #S1,#S2,4 ) '-' ( 18 ) T*#ST-TOT.#STA-TAX ( #S1,#S2,4 ) '-' ( 15 ) T*#ST-TOT.#LOC-TAX ( #S1,#S2,4 ) '-' ( 15 )
                    RepeatItem(12),new ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4)),"-",new 
                    RepeatItem(18),new ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4)),"-",new 
                    RepeatItem(15),new ReportTAsterisk(pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4)),"-",new RepeatItem(18),new 
                    ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4)),"-",new RepeatItem(15),new 
                    ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,4)),"-",new RepeatItem(15));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Desc().getValue(pnd_Ws_Pnd_S1.getInt() + 1).notEquals(" ") && gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Med().getValue(pnd_Ws_Pnd_S1.getInt()  //Natural: IF #ST-CNTL.#STATE-DESC ( #S1 ) NE ' ' AND #ST-CNTL.#STATE-MED ( #S1 ) NE ' ' OR #WS.#S1 = 60
                + 1).notEquals(" ") || pnd_Ws_Pnd_S1.equals(60)))
            {
                getReports().display(6, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",                     //Natural: DISPLAY ( 6 ) ( HC = R ES = ON ) '/' #ST-CNTL.#STATE-DESC ( #S1 ) '//Trans Count' #ST-TOT.#TRAN-CNT ( #S1,#S2,5 ) '//Gross Amount' #ST-TOT.#GROSS-AMT ( #S1,#S2,5 ) 'Payment IVC' #ST-TOT.#PYMNT-IVC ( #S1,#S2,5 ) / 'Adjustment IVC' #ST-TOT.#IVC-ADJ ( #S1,#S2,5 ) / 'Form IVC' #ST-TOT.#FORM-IVC ( #S1,#S2,5 ) 'Payment Taxable' #ST-TO1.#PMNT-TAXABLE ( #S1,#S2,5 ) / 'Adjustment Taxable' #ST-TO1.#TAXABLE-ADJ ( #S1,#S2,5 ) / 'Form Taxable' #ST-TO1.#FORM-TAXABLE ( #S1,#S2,5 ) '//State Tax' #ST-TOT.#STA-TAX ( #S1,#S2,5 ) '//Local Tax' #ST-TOT.#LOC-TAX ( #S1,#S2,5 )
                		gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Desc().getValue(pnd_Ws_Pnd_S1.getInt() + 1),"//Trans Count",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5),"//Gross Amount",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5),"Payment IVC",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5),NEWLINE,"Adjustment IVC",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5),NEWLINE,"Form IVC",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Form_Ivc().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5),"Payment Taxable",
                		pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5),NEWLINE,"Adjustment Taxable",
                		pnd_St_To1_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5),NEWLINE,"Form Taxable",
                		pnd_St_To1_Pnd_Form_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5),"//State Tax",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5),"//Local Tax",
                		gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_S1.equals(59)))                                                                                                                      //Natural: IF #WS.#S1 = 59
            {
                getReports().write(6, new ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5)),"-",new          //Natural: WRITE ( 6 ) T*#ST-TOT.#TRAN-CNT ( #S1,#S2,5 ) '-' ( 12 ) T*#ST-TOT.#GROSS-AMT ( #S1,#S2,5 ) '-' ( 18 ) T*#ST-TOT.#PYMNT-IVC ( #S1,#S2,5 ) '-' ( 15 ) T*#ST-TO1.#PMNT-TAXABLE ( #S1,#S2,5 ) '-' ( 18 ) T*#ST-TOT.#STA-TAX ( #S1,#S2,5 ) '-' ( 15 ) T*#ST-TOT.#LOC-TAX ( #S1,#S2,5 ) '-' ( 15 )
                    RepeatItem(12),new ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5)),"-",new 
                    RepeatItem(18),new ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5)),"-",new 
                    RepeatItem(15),new ReportTAsterisk(pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5)),"-",new RepeatItem(18),new 
                    ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5)),"-",new RepeatItem(15),new 
                    ReportTAsterisk(gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax().getValue(pnd_Ws_Pnd_S1.getInt() + 1,pnd_Ws_Pnd_S2,5)),"-",new RepeatItem(15));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Process_Company() throws Exception                                                                                                                   //Natural: PROCESS-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        DbsUtil.callnat(Twrncomp.class , getCurrentProcessState(), pdaTwracomp.getPnd_Twracomp_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracomp.getPnd_Twracomp_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOMP' USING #TWRACOMP.#INPUT-PARMS ( AD = O ) #TWRACOMP.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Load_State_Table() throws Exception                                                                                                                  //Natural: LOAD-STATE-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                             //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
        pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().setValue(false);                                                                                                          //Natural: ASSIGN TWRATBL2.#ABEND-IND := TWRATBL2.#DISPLAY-IND := FALSE
        pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().setValue(false);
        pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                                             //Natural: ASSIGN TWRATBL2.#TAX-YEAR := #WS.#TAX-YEAR
        gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Gross_Rpt().getValue("*").setValue(true);                                                                                    //Natural: ASSIGN #ST-CNTL.#STATE-GROSS-RPT ( * ) := TRUE
        FOR08:                                                                                                                                                            //Natural: FOR #WS.#I = 0 TO 57
        for (pnd_Ws_Pnd_I.setValue(0); condition(pnd_Ws_Pnd_I.lessOrEqual(57)); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(pnd_Ws_Pnd_I.equals(42)))                                                                                                                       //Natural: IF #WS.#I = 42
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Idx.setValue(pnd_Ws_Pnd_I);                                                                                                                        //Natural: ASSIGN #WS.#IDX := #WS.#I
                                                                                                                                                                          //Natural: PERFORM LOAD-STATE-ENTRY
            sub_Load_State_Entry();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_I.setValue(97);                                                                                                                                        //Natural: ASSIGN #WS.#I := 97
        pnd_Ws_Pnd_Idx.setValue(58);                                                                                                                                      //Natural: ASSIGN #WS.#IDX := 58
                                                                                                                                                                          //Natural: PERFORM LOAD-STATE-ENTRY
        sub_Load_State_Entry();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Load_State_Entry() throws Exception                                                                                                                  //Natural: LOAD-STATE-ENTRY
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValueEdited(pnd_Ws_Pnd_I,new ReportEditMask("999"));                                                                   //Natural: MOVE EDITED #WS.#I ( EM = 999 ) TO TWRATBL2.#STATE-CDE
        DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        if (condition(pdaTwratbl2.getTwratbl2_Pnd_Return_Cde().getBoolean()))                                                                                             //Natural: IF TWRATBL2.#RETURN-CDE
        {
            gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Desc().getValue(pnd_Ws_Pnd_Idx.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Full_Name());                //Natural: ASSIGN #ST-CNTL.#STATE-DESC ( #IDX ) := TIRCNTL-STATE-FULL-NAME
            if (condition(pdaTwratbl2.getTwratbl2_Tircntl_State_Ind().equals(" ")))                                                                                       //Natural: IF TWRATBL2.TIRCNTL-STATE-IND = ' '
            {
                gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Rule().getValue(pnd_Ws_Pnd_Idx.getInt() + 1).setValue("9");                                                          //Natural: ASSIGN #ST-CNTL.#STATE-RULE ( #IDX ) := '9'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Rule().getValue(pnd_Ws_Pnd_Idx.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Ind());                  //Natural: ASSIGN #ST-CNTL.#STATE-RULE ( #IDX ) := TWRATBL2.TIRCNTL-STATE-IND
            }                                                                                                                                                             //Natural: END-IF
            gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Cmb().getValue(pnd_Ws_Pnd_Idx.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_Comb_Fed_Ind());                    //Natural: ASSIGN #ST-CNTL.#STATE-CMB ( #IDX ) := TWRATBL2.TIRCNTL-COMB-FED-IND
            gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Med().getValue(pnd_Ws_Pnd_Idx.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_Media_Code());                      //Natural: ASSIGN #ST-CNTL.#STATE-MED ( #IDX ) := TWRATBL2.TIRCNTL-MEDIA-CODE
            if (condition(gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Rule().getValue(pnd_Ws_Pnd_Idx.getInt() + 1).equals("1") || gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Rule().getValue(pnd_Ws_Pnd_Idx.getInt()  //Natural: IF #ST-CNTL.#STATE-RULE ( #IDX ) = '1' OR = '2' OR = '5' OR = '6'
                + 1).equals("2") || gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Rule().getValue(pnd_Ws_Pnd_Idx.getInt() + 1).equals("5") || gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Rule().getValue(pnd_Ws_Pnd_Idx.getInt() 
                + 1).equals("6")))
            {
                gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Gross_Rpt().getValue(pnd_Ws_Pnd_Idx.getInt() + 1).reset();                                                           //Natural: RESET #ST-CNTL.#STATE-GROSS-RPT ( #IDX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Key_Detail_Pnd_KeyIsBreak = pnd_Key_Detail_Pnd_Key.isBreak(endOfData);
        boolean pnd_Key_Detail_Pnd_Key28IsBreak = pnd_Key_Detail_Pnd_Key.isBreak(endOfData);
        boolean ldaTwrl0900_getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_FormIsBreak = ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form().isBreak(endOfData);
        if (condition(pnd_Key_Detail_Pnd_KeyIsBreak || pnd_Key_Detail_Pnd_Key28IsBreak || ldaTwrl0900_getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_FormIsBreak))
        {
            pnd_Prev_Res_Pnd_Distr_Cde.setValue(readWork01Twrpymnt_Distribution_CdeOld);                                                                                  //Natural: ASSIGN #PREV-RES.#DISTR-CDE := OLD ( #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE )
            pnd_Prev_Res_Pnd_Res_Type.setValue(readWork01Twrpymnt_Residency_TypeOld);                                                                                     //Natural: ASSIGN #PREV-RES.#RES-TYPE := OLD ( #XTAXYR-F94.TWRPYMNT-RESIDENCY-TYPE )
            pnd_Prev_Res_Pnd_Res_Code.setValue(readWork01Twrpymnt_Residency_CodeOld);                                                                                     //Natural: ASSIGN #PREV-RES.#RES-CODE := OLD ( #XTAXYR-F94.TWRPYMNT-RESIDENCY-CODE )
            if (condition(pnd_Prev_Res_Pnd_Res_Type.equals("1")))                                                                                                         //Natural: IF #PREV-RES.#RES-TYPE = '1'
            {
                short decideConditionsMet357 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #PREV-RES.#RES-CODE;//Natural: VALUE '00' : '57'
                if (condition((pnd_Prev_Res_Pnd_Res_Code.equals("00' : '57"))))
                {
                    decideConditionsMet357++;
                    if (condition(DbsUtil.maskMatches(pnd_Prev_Res_Pnd_Res_Code,"99")))                                                                                   //Natural: IF #PREV-RES.#RES-CODE = MASK ( 99 )
                    {
                        pnd_Ws_Pnd_Idx.compute(new ComputeParameters(false, pnd_Ws_Pnd_Idx), pnd_Prev_Res_Pnd_Res_Code.val());                                            //Natural: ASSIGN #WS.#IDX := VAL ( #PREV-RES.#RES-CODE )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE '97'
                else if (condition((pnd_Prev_Res_Pnd_Res_Code.equals("97"))))
                {
                    decideConditionsMet357++;
                    pnd_Ws_Pnd_Idx.setValue(58);                                                                                                                          //Natural: ASSIGN #WS.#IDX := 58
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    pnd_Ws_Pnd_Idx.setValue(59);                                                                                                                          //Natural: ASSIGN #WS.#IDX := 59
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Idx.setValue(61);                                                                                                                              //Natural: ASSIGN #WS.#IDX := 61
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #WS.#1099 = 1 TO #1099R-MAX
            for (pnd_Ws_Pnd_1099.setValue(1); condition(pnd_Ws_Pnd_1099.lessOrEqual(pnd_Ws_Pnd_1099r_Max)); pnd_Ws_Pnd_1099.nadd(1))
            {
                if (condition(! (pnd_St_Case_Pnd_Case_Present.getValue(pnd_Ws_Pnd_1099).equals(true))))                                                                   //Natural: IF NOT #ST-CASE.#CASE-PRESENT ( #1099 )
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt().getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,1).nadd(pnd_St_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_1099));            //Natural: ADD #ST-CASE.#GROSS-AMT ( #1099 ) TO #ST-TOT.#GROSS-AMT ( #IDX,1,1 )
                gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc().getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,1).nadd(pnd_St_Case_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099));              //Natural: ADD #ST-CASE.#IVC-AMT ( #1099 ) TO #ST-TOT.#PYMNT-IVC ( #IDX,1,1 )
                gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt().getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,1).nadd(pnd_St_Case_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_1099));              //Natural: ADD #ST-CASE.#TRAN-CNT ( #1099 ) TO #ST-TOT.#TRAN-CNT ( #IDX,1,1 )
                gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax().getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,1).nadd(pnd_St_Case_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_1099));                //Natural: ADD #ST-CASE.#STA-TAX ( #1099 ) TO #ST-TOT.#STA-TAX ( #IDX,1,1 )
                gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax().getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,1).nadd(pnd_St_Case_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_1099));                //Natural: ADD #ST-CASE.#LOC-TAX ( #1099 ) TO #ST-TOT.#LOC-TAX ( #IDX,1,1 )
                //*  ------------------------------            JH 6/11/02 BEGIN                                                                                           //Natural: DECIDE FOR FIRST CONDITION
                short decideConditionsMet379 = 0;                                                                                                                         //Natural: WHEN #CASE-FIELDS.#ROLL
                if (condition(pnd_Case_Fields_Pnd_Roll.getBoolean()))
                {
                    decideConditionsMet379++;
                    if (condition(pnd_Ws_Pnd_Tax_Year.less(2002)))                                                                                                        //Natural: IF #WS.#TAX-YEAR < 2002
                    {
                        pnd_St_Case_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_1099).setValue(pnd_St_Case_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099));                                    //Natural: ASSIGN #ST-CASE.#IVC-ADJ ( #1099 ) := #ST-CASE.#IVC-AMT ( #1099 )
                        gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj().getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,1).nadd(pnd_St_Case_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_1099));        //Natural: ADD #ST-CASE.#IVC-ADJ ( #1099 ) TO #ST-TOT.#IVC-ADJ ( #IDX,1,1 )
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_St_Case_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099).reset();                                                                                        //Natural: RESET #ST-CASE.#TAXABLE-AMT ( #1099 )
                }                                                                                                                                                         //Natural: WHEN #CASE-FIELDS.#RECHAR
                else if (condition(pnd_Case_Fields_Pnd_Rechar.getBoolean()))
                {
                    decideConditionsMet379++;
                    pnd_St_Case_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_1099).setValue(pnd_St_Case_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099));                                        //Natural: ASSIGN #ST-CASE.#IVC-ADJ ( #1099 ) := #ST-CASE.#IVC-AMT ( #1099 )
                    gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj().getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,1).nadd(pnd_St_Case_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_1099));            //Natural: ADD #ST-CASE.#IVC-ADJ ( #1099 ) TO #ST-TOT.#IVC-ADJ ( #IDX,1,1 )
                    pnd_St_Case_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099).reset();                                                                                        //Natural: RESET #ST-CASE.#TAXABLE-AMT ( #1099 )
                    //*  ------------------------------            JH 6/11/02 END
                }                                                                                                                                                         //Natural: WHEN #ST-CASE.#TND ( #1099 )
                else if (condition(pnd_St_Case_Pnd_Tnd.getValue(pnd_Ws_Pnd_1099).equals(true)))
                {
                    decideConditionsMet379++;
                    pnd_St_Case_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_1099).setValue(pnd_St_Case_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099));                                        //Natural: ASSIGN #ST-CASE.#IVC-ADJ ( #1099 ) := #ST-CASE.#IVC-AMT ( #1099 )
                    gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj().getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,1).nadd(pnd_St_Case_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_1099));            //Natural: ADD #ST-CASE.#IVC-ADJ ( #1099 ) TO #ST-TOT.#IVC-ADJ ( #IDX,1,1 )
                    pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,1).nadd(pnd_St_Case_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099));                    //Natural: ADD #ST-CASE.#TAXABLE-AMT ( #1099 ) TO #ST-TO1.#PMNT-TAXABLE ( #IDX,1,1 )
                    pnd_St_Case_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_1099).compute(new ComputeParameters(false, pnd_St_Case_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_1099)),    //Natural: COMPUTE #ST-CASE.#TAXABLE-ADJ ( #1099 ) = #ST-CASE.#GROSS-AMT ( #1099 ) - #ST-CASE.#TAXABLE-AMT ( #1099 )
                        pnd_St_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_1099).subtract(pnd_St_Case_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099)));
                    pnd_St_To1_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,1).nadd(pnd_St_Case_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_1099));                     //Natural: ADD #ST-CASE.#TAXABLE-ADJ ( #1099 ) TO #ST-TO1.#TAXABLE-ADJ ( #IDX,1,1 )
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    //*  IRA EXCESS CONTRIBUTION & REAC
                    if (condition(pnd_Ws_Pnd_1099.equals(3) || pnd_Ws_Pnd_1099.equals(4)))                                                                                //Natural: IF #1099 = 3 OR = 4
                    {
                        gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj().getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,1).nadd(pnd_St_Case_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099));        //Natural: ADD #ST-CASE.#IVC-AMT ( #1099 ) TO #ST-TOT.#IVC-ADJ ( #IDX,1,1 )
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,1).nadd(pnd_St_Case_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099));                    //Natural: ADD #ST-CASE.#TAXABLE-AMT ( #1099 ) TO #ST-TO1.#PMNT-TAXABLE ( #IDX,1,1 )
                }                                                                                                                                                         //Natural: END-DECIDE
                if (condition(gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Gross_Rpt().getValue(pnd_Ws_Pnd_Idx.getInt() + 1).getBoolean()))                                       //Natural: IF #ST-CNTL.#STATE-GROSS-RPT ( #IDX )
                {
                    pnd_St_Case_Pnd_State_Distr.getValue(pnd_Ws_Pnd_1099).setValue(pnd_St_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_1099));                                  //Natural: ASSIGN #ST-CASE.#STATE-DISTR ( #1099 ) := #ST-CASE.#GROSS-AMT ( #1099 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_St_Case_Pnd_State_Distr.getValue(pnd_Ws_Pnd_1099).compute(new ComputeParameters(false, pnd_St_Case_Pnd_State_Distr.getValue(pnd_Ws_Pnd_1099)),    //Natural: ASSIGN #ST-CASE.#STATE-DISTR ( #1099 ) := #ST-CASE.#TAXABLE-AMT ( #1099 ) + #ST-CASE.#TAXABLE-ADJ ( #1099 )
                        pnd_St_Case_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099).add(pnd_St_Case_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_1099)));
                }                                                                                                                                                         //Natural: END-IF
                //*  EMPTY STATE
                //* PA PENNSYLVANIA
                short decideConditionsMet412 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ST-CASE.#STATE-DISTR ( #1099 ) = 0.00 AND #ST-CASE.#STA-TAX ( #1099 ) = 0.00 AND #ST-CASE.#LOC-TAX ( #1099 ) = 0.00
                if (condition(pnd_St_Case_Pnd_State_Distr.getValue(pnd_Ws_Pnd_1099).equals(new DbsDecimal("0.00")) && pnd_St_Case_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_1099).equals(new 
                    DbsDecimal("0.00")) && pnd_St_Case_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_1099).equals(new DbsDecimal("0.00"))))
                {
                    decideConditionsMet412++;
                    ignore();
                }                                                                                                                                                         //Natural: WHEN #WS.#IDX = 41
                else if (condition(pnd_Ws_Pnd_Idx.equals(41)))
                {
                    decideConditionsMet412++;
                    if (condition(pnd_Prev_Res_Pnd_Distr_Cde.equals("1") || pnd_St_Case_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_1099).notEquals(new DbsDecimal("0.00"))           //Natural: IF #PREV-RES.#DISTR-CDE = '1' OR #ST-CASE.#STA-TAX ( #1099 ) NE 0.00 OR #ST-CASE.#LOC-TAX ( #1099 ) NE 0.00
                        || pnd_St_Case_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_1099).notEquals(new DbsDecimal("0.00"))))
                    {
                        pnd_St_Case_Pnd_Reportable_Distr.getValue(pnd_Ws_Pnd_1099).setValue(true);                                                                        //Natural: ASSIGN #ST-CASE.#REPORTABLE-DISTR ( #1099 ) := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #ST-CNTL.#STATE-RULE ( #IDX ) = ' ' OR = '2' OR = '4' OR = '6' OR = '8' OR = '9'
                else if (condition(gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Rule().getValue(pnd_Ws_Pnd_Idx.getInt() + 1).equals(" ") || gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Rule().getValue(pnd_Ws_Pnd_Idx.getInt() 
                    + 1).equals("2") || gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Rule().getValue(pnd_Ws_Pnd_Idx.getInt() + 1).equals("4") || gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Rule().getValue(pnd_Ws_Pnd_Idx.getInt() 
                    + 1).equals("6") || gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Rule().getValue(pnd_Ws_Pnd_Idx.getInt() + 1).equals("8") || gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Rule().getValue(pnd_Ws_Pnd_Idx.getInt() 
                    + 1).equals("9")))
                {
                    decideConditionsMet412++;
                    if (condition(pnd_St_Case_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_1099).notEquals(new DbsDecimal("0.00")) || pnd_St_Case_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_1099).notEquals(new  //Natural: IF #ST-CASE.#STA-TAX ( #1099 ) NE 0.00 OR #ST-CASE.#LOC-TAX ( #1099 ) NE 0.00
                        DbsDecimal("0.00"))))
                    {
                        pnd_St_Case_Pnd_Reportable_Distr.getValue(pnd_Ws_Pnd_1099).setValue(true);                                                                        //Natural: ASSIGN #ST-CASE.#REPORTABLE-DISTR ( #1099 ) := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_St_Case_Pnd_Reportable_Distr.getValue(pnd_Ws_Pnd_1099).setValue(true);                                                                            //Natural: ASSIGN #ST-CASE.#REPORTABLE-DISTR ( #1099 ) := TRUE
                }                                                                                                                                                         //Natural: END-DECIDE
                if (condition(pnd_St_Case_Pnd_Reportable_Distr.getValue(pnd_Ws_Pnd_1099).getBoolean()))                                                                   //Natural: IF #ST-CASE.#REPORTABLE-DISTR ( #1099 )
                {
                    if (condition(gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Gross_Rpt().getValue(pnd_Ws_Pnd_Idx.getInt() + 1).getBoolean()))                                   //Natural: IF #ST-CNTL.#STATE-GROSS-RPT ( #IDX )
                    {
                        pnd_Ws_Pnd_I.setValue(3);                                                                                                                         //Natural: ASSIGN #WS.#I := 3
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Pnd_I.setValue(4);                                                                                                                         //Natural: ASSIGN #WS.#I := 4
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_I.setValue(2);                                                                                                                             //Natural: ASSIGN #WS.#I := 2
                }                                                                                                                                                         //Natural: END-IF
                gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt().getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Case_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_1099));   //Natural: ADD #ST-CASE.#TRAN-CNT ( #1099 ) TO #ST-TOT.#TRAN-CNT ( #IDX,1,#I )
                gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt().getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_1099)); //Natural: ADD #ST-CASE.#GROSS-AMT ( #1099 ) TO #ST-TOT.#GROSS-AMT ( #IDX,1,#I )
                gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc().getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Case_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099));   //Natural: ADD #ST-CASE.#IVC-AMT ( #1099 ) TO #ST-TOT.#PYMNT-IVC ( #IDX,1,#I )
                gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj().getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Case_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_1099));     //Natural: ADD #ST-CASE.#IVC-ADJ ( #1099 ) TO #ST-TOT.#IVC-ADJ ( #IDX,1,#I )
                pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Case_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099));             //Natural: ADD #ST-CASE.#TAXABLE-AMT ( #1099 ) TO #ST-TO1.#PMNT-TAXABLE ( #IDX,1,#I )
                pnd_St_To1_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Case_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_1099));              //Natural: ADD #ST-CASE.#TAXABLE-ADJ ( #1099 ) TO #ST-TO1.#TAXABLE-ADJ ( #IDX,1,#I )
                gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax().getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Case_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_1099));     //Natural: ADD #ST-CASE.#STA-TAX ( #1099 ) TO #ST-TOT.#STA-TAX ( #IDX,1,#I )
                gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax().getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,pnd_Ws_Pnd_I).nadd(pnd_St_Case_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_1099));     //Natural: ADD #ST-CASE.#LOC-TAX ( #1099 ) TO #ST-TOT.#LOC-TAX ( #IDX,1,#I )
                if (condition(pnd_St_Case_Pnd_Reportable_Distr.getValue(pnd_Ws_Pnd_1099).getBoolean()))                                                                   //Natural: IF #ST-CASE.#REPORTABLE-DISTR ( #1099 )
                {
                    //*  PAPER OR MAG
                    if (condition(gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Med().getValue(pnd_Ws_Pnd_Idx.getInt() + 1).notEquals(" ")))                                       //Natural: IF #ST-CNTL.#STATE-MED ( #IDX ) NE ' '
                    {
                        short decideConditionsMet446 = 0;                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ST-CNTL.#STATE-RULE ( #IDX ) = '5' OR = '7' AND #ST-CNTL.#STATE-CMB ( #IDX ) NE 'C'
                        if (condition(((gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Rule().getValue(pnd_Ws_Pnd_Idx.getInt() + 1).equals("5") || gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Rule().getValue(pnd_Ws_Pnd_Idx.getInt() 
                            + 1).equals("7")) && gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Cmb().getValue(pnd_Ws_Pnd_Idx.getInt() + 1).notEquals("C"))))
                        {
                            decideConditionsMet446++;
                            ignore();
                        }                                                                                                                                                 //Natural: WHEN #ST-CASE.#STA-TAX ( #1099 ) NE 0.00 OR #ST-CASE.#LOC-TAX ( #1099 ) NE 0.00
                        else if (condition(pnd_St_Case_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_1099).notEquals(new DbsDecimal("0.00")) || pnd_St_Case_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_1099).notEquals(new 
                            DbsDecimal("0.00"))))
                        {
                            decideConditionsMet446++;
                            ignore();
                        }                                                                                                                                                 //Natural: WHEN ANY
                        if (condition(decideConditionsMet446 > 0))
                        {
                            gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt().getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,5).nadd(pnd_St_Case_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_1099));  //Natural: ADD #ST-CASE.#TRAN-CNT ( #1099 ) TO #ST-TOT.#TRAN-CNT ( #IDX,1,5 )
                            gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt().getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,5).nadd(pnd_St_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_1099)); //Natural: ADD #ST-CASE.#GROSS-AMT ( #1099 ) TO #ST-TOT.#GROSS-AMT ( #IDX,1,5 )
                            gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc().getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,5).nadd(pnd_St_Case_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099));  //Natural: ADD #ST-CASE.#IVC-AMT ( #1099 ) TO #ST-TOT.#PYMNT-IVC ( #IDX,1,5 )
                            gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj().getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,5).nadd(pnd_St_Case_Pnd_Ivc_Adj.getValue(pnd_Ws_Pnd_1099));    //Natural: ADD #ST-CASE.#IVC-ADJ ( #1099 ) TO #ST-TOT.#IVC-ADJ ( #IDX,1,5 )
                            pnd_St_To1_Pnd_Pmnt_Taxable.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,5).nadd(pnd_St_Case_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099));            //Natural: ADD #ST-CASE.#TAXABLE-AMT ( #1099 ) TO #ST-TO1.#PMNT-TAXABLE ( #IDX,1,5 )
                            pnd_St_To1_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,5).nadd(pnd_St_Case_Pnd_Taxable_Adj.getValue(pnd_Ws_Pnd_1099));             //Natural: ADD #ST-CASE.#TAXABLE-ADJ ( #1099 ) TO #ST-TO1.#TAXABLE-ADJ ( #IDX,1,5 )
                            gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax().getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,5).nadd(pnd_St_Case_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_1099));    //Natural: ADD #ST-CASE.#STA-TAX ( #1099 ) TO #ST-TOT.#STA-TAX ( #IDX,1,5 )
                            gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax().getValue(pnd_Ws_Pnd_Idx.getInt() + 1,1,5).nadd(pnd_St_Case_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_1099));    //Natural: ADD #ST-CASE.#LOC-TAX ( #1099 ) TO #ST-TOT.#LOC-TAX ( #IDX,1,5 )
                        }                                                                                                                                                 //Natural: WHEN NONE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape())) return;
            pnd_St_Case.getValue("*").reset();                                                                                                                            //Natural: RESET #ST-CASE ( * )
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Key_Detail_Pnd_KeyIsBreak || pnd_Key_Detail_Pnd_Key28IsBreak || ldaTwrl0900_getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_FormIsBreak))
        {
            FOR02:                                                                                                                                                        //Natural: FOR #WS.#1099 = 1 TO #1099R-MAX
            for (pnd_Ws_Pnd_1099.setValue(1); condition(pnd_Ws_Pnd_1099.lessOrEqual(pnd_Ws_Pnd_1099r_Max)); pnd_Ws_Pnd_1099.nadd(1))
            {
                if (condition(! (pnd_Case_Fields_Pnd_Case_Present.getValue(pnd_Ws_Pnd_1099).equals(true))))                                                               //Natural: IF NOT #CASE-FIELDS.#CASE-PRESENT ( #1099 )
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Cntl_Pnd_Taxable_Amt.getValue(2,1).nadd(pnd_Case_Fields_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099));                                                   //Natural: ADD #CASE-FIELDS.#TAXABLE-AMT ( #1099 ) TO #CNTL.#TAXABLE-AMT ( 2,1 )
                //*  JH 6/11/2002
                if (condition(pnd_Case_Fields_Pnd_Tnd.getValue(pnd_Ws_Pnd_1099).equals(true) && ! (pnd_Case_Fields_Pnd_Roll.getBoolean()) && ! (pnd_Case_Fields_Pnd_Rechar.getBoolean()))) //Natural: IF #CASE-FIELDS.#TND ( #1099 ) AND NOT #CASE-FIELDS.#ROLL AND NOT #CASE-FIELDS.#RECHAR
                {
                    pnd_Cntl_Pnd_Ivc_Amt.getValue(6,1).nadd(pnd_Case_Fields_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099));                                                       //Natural: ASSIGN #CNTL.#IVC-AMT ( 6,1 ) := #CNTL.#IVC-AMT ( 6,1 ) + #CASE-FIELDS.#IVC-AMT ( #1099 )
                    pnd_Cntl_Pnd_Taxable_Amt.getValue(6,1).nadd(pnd_Case_Fields_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_1099));                                               //Natural: ASSIGN #CNTL.#TAXABLE-AMT ( 6,1 ) := #CNTL.#TAXABLE-AMT ( 6,1 ) + #CASE-FIELDS.#TAXABLE-AMT ( #1099 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IRA EXCESS CONTRIBUTION & REAC
                    if (condition(pnd_Ws_Pnd_1099.equals(3) || pnd_Ws_Pnd_1099.equals(4)))                                                                                //Natural: IF #1099 = 3 OR = 4
                    {
                        pnd_Cntl_Pnd_Ivc_Amt.getValue(4,1).nadd(pnd_Case_Fields_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_1099));                                                   //Natural: ASSIGN #CNTL.#IVC-AMT ( 4,1 ) := #CNTL.#IVC-AMT ( 4,1 ) + #CASE-FIELDS.#IVC-AMT ( #1099 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape())) return;
            pnd_Case_Fields.resetInitial();                                                                                                                               //Natural: RESET INITIAL #CASE-FIELDS
            //*  ------------------------------            JH 6/11/02 BEGIN
            //*  ROLLOVERS
            //*  TAX-FREE EXCHANGE  10/8/02
            if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("G") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("H")  //Natural: IF #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE = 'G' OR = 'H' OR = 'Z' OR = '6'
                || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("Z") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("6")))
            {
                //*      OR = 'X'
                pnd_Case_Fields_Pnd_Roll.setValue(true);                                                                                                                  //Natural: ASSIGN #CASE-FIELDS.#ROLL := TRUE
            }                                                                                                                                                             //Natural: END-IF
            //*  RECHARACTERIZATIONS
            if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("N") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("R"))) //Natural: IF #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE = 'N' OR = 'R'
            {
                pnd_Case_Fields_Pnd_Rechar.setValue(true);                                                                                                                //Natural: ASSIGN #CASE-FIELDS.#RECHAR := TRUE
            }                                                                                                                                                             //Natural: END-IF
            //*  ------------------------------            JH 6/11/02 END
            //*  BK
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaTwrl0900_getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_FormIsBreak))
        {
            pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(readWork01Twrpymnt_Company_Cde_FormOld);                                                                 //Natural: ASSIGN #TWRACOMP.#COMP-CODE := OLD ( #XTAXYR-F94.TWRPYMNT-COMPANY-CDE-FORM )
            pdaTwracomp.getPnd_Twracomp_Pnd_Tax_Year().setValue(readWork01Twrpymnt_Tax_YearOld);                                                                          //Natural: ASSIGN #TWRACOMP.#TAX-YEAR := OLD ( #XTAXYR-F94.TWRPYMNT-TAX-YEAR )
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMPANY
            sub_Process_Company();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Pnd_Company_Line.setValue("Company:");                                                                                                                 //Natural: ASSIGN #WS.#COMPANY-LINE := 'Company:'
            setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),pnd_Ws_Pnd_Company_Line,11,4);                                                          //Natural: MOVE #TWRACOMP.#COMP-SHORT-NAME TO SUBSTR ( #WS.#COMPANY-LINE,11,4 )
            setValueToSubstring("-",pnd_Ws_Pnd_Company_Line,16,1);                                                                                                        //Natural: MOVE '-' TO SUBSTR ( #WS.#COMPANY-LINE,16,1 )
            setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Name(),pnd_Ws_Pnd_Company_Line,18,37);                                                               //Natural: MOVE #TWRACOMP.#COMP-NAME TO SUBSTR ( #WS.#COMPANY-LINE,18,37 )
            pnd_Ws_Pnd_S2.setValue(1);                                                                                                                                    //Natural: ASSIGN #WS.#S2 := 1
                                                                                                                                                                          //Natural: PERFORM WRITE-FED-REPORT
            sub_Write_Fed_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Cntl_Pnd_Tran_Cnt.getValue("*",2).nadd(pnd_Cntl_Pnd_Tran_Cnt.getValue("*",1));                                                                            //Natural: ADD #CNTL.#TRAN-CNT ( *,1 ) TO #CNTL.#TRAN-CNT ( *,2 )
            pnd_Cntl_Pnd_Gross_Amt.getValue("*",2).nadd(pnd_Cntl_Pnd_Gross_Amt.getValue("*",1));                                                                          //Natural: ADD #CNTL.#GROSS-AMT ( *,1 ) TO #CNTL.#GROSS-AMT ( *,2 )
            pnd_Cntl_Pnd_Ivc_Amt.getValue("*",2).nadd(pnd_Cntl_Pnd_Ivc_Amt.getValue("*",1));                                                                              //Natural: ADD #CNTL.#IVC-AMT ( *,1 ) TO #CNTL.#IVC-AMT ( *,2 )
            pnd_Cntl_Pnd_Taxable_Amt.getValue("*",2).nadd(pnd_Cntl_Pnd_Taxable_Amt.getValue("*",1));                                                                      //Natural: ADD #CNTL.#TAXABLE-AMT ( *,1 ) TO #CNTL.#TAXABLE-AMT ( *,2 )
            pnd_Cntl_Pnd_Fed_Tax.getValue("*",2).nadd(pnd_Cntl_Pnd_Fed_Tax.getValue("*",1));                                                                              //Natural: ADD #CNTL.#FED-TAX ( *,1 ) TO #CNTL.#FED-TAX ( *,2 )
            pnd_Cntl_Pnd_Sta_Tax.getValue("*",2).nadd(pnd_Cntl_Pnd_Sta_Tax.getValue("*",1));                                                                              //Natural: ADD #CNTL.#STA-TAX ( *,1 ) TO #CNTL.#STA-TAX ( *,2 )
            pnd_Cntl_Pnd_Loc_Tax.getValue("*",2).nadd(pnd_Cntl_Pnd_Loc_Tax.getValue("*",1));                                                                              //Natural: ADD #CNTL.#LOC-TAX ( *,1 ) TO #CNTL.#LOC-TAX ( *,2 )
            pnd_Cntl_Pnd_Int_Amt.getValue("*",2).nadd(pnd_Cntl_Pnd_Int_Amt.getValue("*",1));                                                                              //Natural: ADD #CNTL.#INT-AMT ( *,1 ) TO #CNTL.#INT-AMT ( *,2 )
            pnd_Cntl_Pnd_Int_Tax.getValue("*",2).nadd(pnd_Cntl_Pnd_Int_Tax.getValue("*",1));                                                                              //Natural: ADD #CNTL.#INT-TAX ( *,1 ) TO #CNTL.#INT-TAX ( *,2 )
            gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt().getValue("*",2,"*").nadd(gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt().getValue("*",1,"*"));                              //Natural: ADD #ST-TOT.#TRAN-CNT ( *,1,* ) TO #ST-TOT.#TRAN-CNT ( *,2,* )
            gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt().getValue("*",2,"*").nadd(gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt().getValue("*",1,"*"));                            //Natural: ADD #ST-TOT.#GROSS-AMT ( *,1,* ) TO #ST-TOT.#GROSS-AMT ( *,2,* )
            gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc().getValue("*",2,"*").nadd(gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc().getValue("*",1,"*"));                            //Natural: ADD #ST-TOT.#PYMNT-IVC ( *,1,* ) TO #ST-TOT.#PYMNT-IVC ( *,2,* )
            gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj().getValue("*",2,"*").nadd(gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj().getValue("*",1,"*"));                                //Natural: ADD #ST-TOT.#IVC-ADJ ( *,1,* ) TO #ST-TOT.#IVC-ADJ ( *,2,* )
            gdaTwrg5501.getPnd_St_Tot_Pnd_Form_Ivc().getValue("*",2,"*").nadd(gdaTwrg5501.getPnd_St_Tot_Pnd_Form_Ivc().getValue("*",1,"*"));                              //Natural: ADD #ST-TOT.#FORM-IVC ( *,1,* ) TO #ST-TOT.#FORM-IVC ( *,2,* )
            pnd_St_To1_Pnd_Pmnt_Taxable.getValue("*",2,"*").nadd(pnd_St_To1_Pnd_Pmnt_Taxable.getValue("*",1,"*"));                                                        //Natural: ADD #ST-TO1.#PMNT-TAXABLE ( *,1,* ) TO #ST-TO1.#PMNT-TAXABLE ( *,2,* )
            pnd_St_To1_Pnd_Taxable_Adj.getValue("*",2,"*").nadd(pnd_St_To1_Pnd_Taxable_Adj.getValue("*",1,"*"));                                                          //Natural: ADD #ST-TO1.#TAXABLE-ADJ ( *,1,* ) TO #ST-TO1.#TAXABLE-ADJ ( *,2,* )
            pnd_St_To1_Pnd_Form_Taxable.getValue("*",2,"*").nadd(pnd_St_To1_Pnd_Form_Taxable.getValue("*",1,"*"));                                                        //Natural: ADD #ST-TO1.#FORM-TAXABLE ( *,1,* ) TO #ST-TO1.#FORM-TAXABLE ( *,2,* )
            gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax().getValue("*",2,"*").nadd(gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax().getValue("*",1,"*"));                                //Natural: ADD #ST-TOT.#STA-TAX ( *,1,* ) TO #ST-TOT.#STA-TAX ( *,2,* )
            gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax().getValue("*",2,"*").nadd(gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax().getValue("*",1,"*"));                                //Natural: ADD #ST-TOT.#LOC-TAX ( *,1,* ) TO #ST-TOT.#LOC-TAX ( *,2,* )
            pnd_Cntl_Pnd_Totals.getValue("*",1).reset();                                                                                                                  //Natural: RESET #CNTL.#TOTALS ( *,1 ) #ST-TOT ( *,1,* ) #ST-TO1 ( *,1,* )
            gdaTwrg5501.getPnd_St_Tot().getValue("*",1,"*").reset();
            pnd_St_To1.getValue("*",1,"*").reset();
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=133 ZP=ON");
        Global.format(2, "PS=58 LS=133 ZP=ON");
        Global.format(3, "PS=58 LS=133 ZP=ON");
        Global.format(4, "PS=58 LS=133 ZP=ON");
        Global.format(5, "PS=58 LS=133 ZP=ON");
        Global.format(6, "PS=58 LS=133 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(47),"Summary of Federal Payment Transactions",new TabSetting(120),"Report: RPT1",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(57),"State Summary Totals",new TabSetting(120),"Report: RPT2",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, new ReportEditMask 
            ("9999"), new SignPosition (false),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(48),"State Summary Totals - Non-Reportable",new TabSetting(120),"Report: RPT3",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(4), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(47),"State Summary Totals - Gross reportable",new TabSetting(120),"Report: RPT4",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(5, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(5), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"State Summary Totals - Taxable reportable",new TabSetting(120),"Report: RPT5",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);
        getReports().write(6, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(6), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"State Summary Totals - HardCopy reportable",new TabSetting(120),"Report: RPT6",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",
            
        		pnd_Cntl_Pnd_Cntl_Text,NEWLINE,"/",
        		pnd_Cntl_Pnd_Cntl_Text1,"/Trans Count",
        		pnd_Cntl_Pnd_Tran_Cnt, pnd_Ws_Pnd_Tran_Cv,"Gross Amount",
        		pnd_Cntl_Pnd_Gross_Amt, pnd_Ws_Pnd_Gross_Cv,NEWLINE,new ColumnSpacing(3),"Interest",
        		pnd_Cntl_Pnd_Int_Amt, pnd_Ws_Pnd_Int_Cv,"/IVC Amount",
        		pnd_Cntl_Pnd_Ivc_Amt, pnd_Ws_Pnd_Ivc_Cv,"/Taxable Amount",
        		pnd_Cntl_Pnd_Taxable_Amt, pnd_Ws_Pnd_Taxable_Cv,"Federal Tax",
        		pnd_Cntl_Pnd_Fed_Tax, pnd_Ws_Pnd_Fed_Cv,NEWLINE,"Backup Tax",
        		pnd_Cntl_Pnd_Int_Tax, pnd_Ws_Pnd_Int_Back_Cv,"State   Tax",
        		pnd_Cntl_Pnd_Sta_Tax, pnd_Ws_Pnd_Sta_Cv,NEWLINE,"Local   Tax",
        		pnd_Cntl_Pnd_Loc_Tax, pnd_Ws_Pnd_Loc_Cv);
        getReports().setDisplayColumns(2, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",
            
        		gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Desc(),"//Trans Count",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt(),"//Gross Amount",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt(),"Payment IVC",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc(),NEWLINE,"Adjustment IVC",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj(),NEWLINE,"Form IVC",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Form_Ivc(),"Payment Taxable",
        		pnd_St_To1_Pnd_Pmnt_Taxable,NEWLINE,"Adjustment Taxable",
        		pnd_St_To1_Pnd_Taxable_Adj,NEWLINE,"Form Taxable",
        		pnd_St_To1_Pnd_Form_Taxable,"//State Tax",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax(),"//Local Tax",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax());
        getReports().setDisplayColumns(3, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",
            
        		gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Desc(),"//Trans Count",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt(),"//Gross Amount",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt(),"Payment IVC",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc(),NEWLINE,"Adjustment IVC",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj(),NEWLINE,"Form IVC",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Form_Ivc(),"Payment Taxable",
        		pnd_St_To1_Pnd_Pmnt_Taxable,NEWLINE,"Adjustment Taxable",
        		pnd_St_To1_Pnd_Taxable_Adj,NEWLINE,"Form Taxable",
        		pnd_St_To1_Pnd_Form_Taxable,"//State Tax",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax(),"//Local Tax",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax());
        getReports().setDisplayColumns(4, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",
            
        		gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Desc(),"//Trans Count",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt(),"//Gross Amount",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt(),"Payment IVC",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc(),NEWLINE,"Adjustment IVC",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj(),NEWLINE,"Form IVC",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Form_Ivc(),"Payment Taxable",
        		pnd_St_To1_Pnd_Pmnt_Taxable,NEWLINE,"Adjustment Taxable",
        		pnd_St_To1_Pnd_Taxable_Adj,NEWLINE,"Form Taxable",
        		pnd_St_To1_Pnd_Form_Taxable,"//State Tax",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax(),"//Local Tax",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax());
        getReports().setDisplayColumns(5, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",
            
        		gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Desc(),"//Trans Count",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt(),"//Gross Amount",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt(),"Payment IVC",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc(),NEWLINE,"Adjustment IVC",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj(),NEWLINE,"Form IVC",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Form_Ivc(),"Payment Taxable",
        		pnd_St_To1_Pnd_Pmnt_Taxable,NEWLINE,"Adjustment Taxable",
        		pnd_St_To1_Pnd_Taxable_Adj,NEWLINE,"Form Taxable",
        		pnd_St_To1_Pnd_Form_Taxable,"//State Tax",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax(),"//Local Tax",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax());
        getReports().setDisplayColumns(6, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",
            
        		gdaTwrg5501.getPnd_St_Cntl_Pnd_State_Desc(),"//Trans Count",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Tran_Cnt(),"//Gross Amount",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Gross_Amt(),"Payment IVC",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Pymnt_Ivc(),NEWLINE,"Adjustment IVC",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Ivc_Adj(),NEWLINE,"Form IVC",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Form_Ivc(),"Payment Taxable",
        		pnd_St_To1_Pnd_Pmnt_Taxable,NEWLINE,"Adjustment Taxable",
        		pnd_St_To1_Pnd_Taxable_Adj,NEWLINE,"Form Taxable",
        		pnd_St_To1_Pnd_Form_Taxable,"//State Tax",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Sta_Tax(),"//Local Tax",
        		gdaTwrg5501.getPnd_St_Tot_Pnd_Loc_Tax());
    }
    private void CheckAtStartofData327() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Ws_Pnd_Tax_Year.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());                                                                              //Natural: ASSIGN #WS.#TAX-YEAR := #XTAXYR-F94.TWRPYMNT-TAX-YEAR
            if (condition(pnd_Ws_Pnd_Tax_Year.lessOrEqual(2000)))                                                                                                         //Natural: IF #WS.#TAX-YEAR LE 2000
            {
                pnd_Ws_Pnd_1099r_Max.setValue(3);                                                                                                                         //Natural: ASSIGN #WS.#1099R-MAX := 3
                pnd_Cntl_Pnd_Cntl_Text1.getValue(4).reset();                                                                                                              //Natural: RESET #CNTL-TEXT1 ( 4 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_1099r_Max.setValue(pnd_Ws_Const_Pnd_1099r_Const);                                                                                              //Natural: ASSIGN #WS.#1099R-MAX := #1099R-CONST
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM LOAD-STATE-TABLE
            sub_Load_State_Table();
            if (condition(Global.isEscape())) {return;}
            //*  ------------------------------            JH 6/11/02 BEGIN
            //*  ROLLOVERS
            //*  TAX-FREE EXCHANGE 10/8/02
            if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("G") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("H")  //Natural: IF #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE = 'G' OR = 'H' OR = 'Z' OR = '6'
                || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("Z") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("6")))
            {
                //*      OR = 'X'
                pnd_Case_Fields_Pnd_Roll.setValue(true);                                                                                                                  //Natural: ASSIGN #CASE-FIELDS.#ROLL := TRUE
            }                                                                                                                                                             //Natural: END-IF
            //*  RECHARACTERIZATIONS
            if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("N") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("R"))) //Natural: IF #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE = 'N' OR = 'R'
            {
                pnd_Case_Fields_Pnd_Rechar.setValue(true);                                                                                                                //Natural: ASSIGN #CASE-FIELDS.#RECHAR := TRUE
            }                                                                                                                                                             //Natural: END-IF
            //*  ------------------------------            JH 6/11/02 END
        }                                                                                                                                                                 //Natural: END-START
    }
}
