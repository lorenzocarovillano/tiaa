/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:40:03 PM
**        * FROM NATURAL PROGRAM : Twrp4630
************************************************************
**        * FILE NAME            : Twrp4630.java
**        * CLASS NAME           : Twrp4630
**        * INSTANCE NAME        : Twrp4630
************************************************************
************************************************************************
*
* PROGRAM  : TWRP4630
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : UPDATES THE IRA CONTRIBUTION TRANSACTIONS WITH UNIQUE NO.
* CREATED  : 07 / 24 / 1999.
*   BY     : RIAD LOUTFI.
* FUNCTION : ASSINGNS A UNIQUE NUMBERS TO EACH IRA CONTRIBUTON
*            TRANSACTION RECORD.
*
*
* 04/27/2017  WEBBJ - PIN EXPANSION STOW ONLY
*
* 11/24/2020 - PALDE - RESTOW FOR IRS REPORTING 2020 - 5498 CHANGES
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp4630 extends BLNatBase
{
    // Data Areas
    private LdaTwrl463a ldaTwrl463a;
    private LdaTwrl463b ldaTwrl463b;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Isn;
    private DbsField pnd_Un;

    private DbsGroup pnd_Un__R_Field_1;
    private DbsField pnd_Un_Pnd_Un_A;
    private DbsField pnd_Super;

    private DbsGroup pnd_Super__R_Field_2;
    private DbsField pnd_Super_Pnd_Tbl_No;
    private DbsField pnd_Super_Pnd_Tax_Year;
    private DbsField pnd_Super_Pnd_Source_Cd;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl463a = new LdaTwrl463a();
        registerRecord(ldaTwrl463a);
        ldaTwrl463b = new LdaTwrl463b();
        registerRecord(ldaTwrl463b);
        registerRecord(ldaTwrl463b.getVw_cntl());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_Un = localVariables.newFieldInRecord("pnd_Un", "#UN", FieldType.NUMERIC, 15);

        pnd_Un__R_Field_1 = localVariables.newGroupInRecord("pnd_Un__R_Field_1", "REDEFINE", pnd_Un);
        pnd_Un_Pnd_Un_A = pnd_Un__R_Field_1.newFieldInGroup("pnd_Un_Pnd_Un_A", "#UN-A", FieldType.STRING, 15);
        pnd_Super = localVariables.newFieldInRecord("pnd_Super", "#SUPER", FieldType.STRING, 7);

        pnd_Super__R_Field_2 = localVariables.newGroupInRecord("pnd_Super__R_Field_2", "REDEFINE", pnd_Super);
        pnd_Super_Pnd_Tbl_No = pnd_Super__R_Field_2.newFieldInGroup("pnd_Super_Pnd_Tbl_No", "#TBL-NO", FieldType.NUMERIC, 1);
        pnd_Super_Pnd_Tax_Year = pnd_Super__R_Field_2.newFieldInGroup("pnd_Super_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Super_Pnd_Source_Cd = pnd_Super__R_Field_2.newFieldInGroup("pnd_Super_Pnd_Source_Cd", "#SOURCE-CD", FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl463a.initializeValues();
        ldaTwrl463b.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp4630() throws Exception
    {
        super("Twrp4630");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                                                                                                                                                                          //Natural: PERFORM GET-STARTING-UNIQUE-NUMBER
        sub_Get_Starting_Unique_Number();
        if (condition(Global.isEscape())) {return;}
        //* * --  READ THE INPUT FILE (COMBINED CITED & ACCEPTED RECS)
        RD1:                                                                                                                                                              //Natural: READ WORK FILE 01 RECORD IRA-RECORD
        while (condition(getWorkFiles().read(1, ldaTwrl463a.getIra_Record())))
        {
            //*    INCREMENT THE VALUE OF #UN BY ONE.
            pnd_Un.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #UN
            ldaTwrl463a.getIra_Record_Ira_Unique_Id().setValue(pnd_Un_Pnd_Un_A);                                                                                          //Natural: ASSIGN IRA-RECORD.IRA-UNIQUE-ID := #UN-A
            getWorkFiles().write(2, false, ldaTwrl463a.getIra_Record());                                                                                                  //Natural: WRITE WORK FILE 02 IRA-RECORD
        }                                                                                                                                                                 //Natural: END-WORK
        RD1_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STARTING-UNIQUE-NUMBER
        //* *-------------------------------------------
        //* * --  FIND UNIQUE NUMBER COUNTER (SOURCE CODE 'XX')
        //* * --  ONLY ONE SOURCE CODE "XX" RECORD IS NEEDED ON TABLE NO. 5.
        //* * --  ONLY ONE SOURCE CODE "XX" RECORD IS NEEDED FOR THE LIFE TIME
        //* * --  OF THE SYSTEM
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-PROGRAM-PROCESSING
        //* *------------
    }
    //*  DO NOT CHANGE THIS VALUE, EVEN IF THE YEAR CHG.
    private void sub_Get_Starting_Unique_Number() throws Exception                                                                                                        //Natural: GET-STARTING-UNIQUE-NUMBER
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Super_Pnd_Tbl_No.setValue(5);                                                                                                                                 //Natural: ASSIGN #TBL-NO := 5
        pnd_Super_Pnd_Tax_Year.setValue(1998);                                                                                                                            //Natural: ASSIGN #TAX-YEAR := 1998
        pnd_Super_Pnd_Source_Cd.setValue("XX");                                                                                                                           //Natural: ASSIGN #SOURCE-CD := 'XX'
        ldaTwrl463b.getVw_cntl().startDatabaseFind                                                                                                                        //Natural: FIND CNTL WITH TIRCNTL-NBR-YEAR-FEEDER-CDE = #SUPER
        (
        "FD1",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_FEEDER_CDE", "=", pnd_Super, WcType.WITH) }
        );
        FD1:
        while (condition(ldaTwrl463b.getVw_cntl().readNextRow("FD1")))
        {
            ldaTwrl463b.getVw_cntl().setIfNotFoundControlFlag(false);
            pnd_Isn.setValue(ldaTwrl463b.getVw_cntl().getAstISN("FD1"));                                                                                                  //Natural: ASSIGN #ISN := *ISN ( FD1. )
            pnd_Un.setValue(ldaTwrl463b.getCntl_Tircntl_Rpt_Update_Dte_Time());                                                                                           //Natural: ASSIGN #UN := CNTL.TIRCNTL-RPT-UPDATE-DTE-TIME
            //*  NEWPAGE (00)
            //*  PRINT (00)
            //*        'ORIGIN CODE "XX" RECORD IS FOUND ON FEEDER TABLE NO. 5'
            getReports().skip(0, 2);                                                                                                                                      //Natural: SKIP ( 00 ) 2
            getReports().write(0, new TabSetting(1),"Unique Number value before update......",pnd_Un);                                                                    //Natural: WRITE ( 00 ) 01T 'Unique Number value before update......' #UN
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(ldaTwrl463b.getVw_cntl().getAstNUMBER().equals(getZero())))                                                                                         //Natural: IF *NUMBER ( FD1. ) = 0
        {
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( 00 )
            if (condition(Global.isEscape())){return;}
            getReports().print(0, "ERROR - ORIGIN CODE 'XX' RECORD IS MISSING FROM","FEEDER TABLE NO. 5");                                                                //Natural: PRINT ( 00 ) 'ERROR - ORIGIN CODE "XX" RECORD IS MISSING FROM' 'FEEDER TABLE NO. 5'
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 0090
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        //* * --  UPDATE THE UN COUNTER ON THE FEEDER TABLE.
        G1:                                                                                                                                                               //Natural: GET CNTL #ISN
        ldaTwrl463b.getVw_cntl().readByID(pnd_Isn.getLong(), "G1");
        ldaTwrl463b.getCntl_Tircntl_Rpt_Update_Dte_Time().setValue(pnd_Un);                                                                                               //Natural: ASSIGN CNTL.TIRCNTL-RPT-UPDATE-DTE-TIME := #UN
        ldaTwrl463b.getVw_cntl().updateDBRow("G1");                                                                                                                       //Natural: UPDATE ( G1. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(0, new TabSetting(1),"Unique Number value  after update......",pnd_Un);                                                                        //Natural: WRITE ( 00 ) 01T 'Unique Number value  after update......' #UN
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
    }
}
