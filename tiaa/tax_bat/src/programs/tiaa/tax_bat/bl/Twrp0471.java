/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:30:24 PM
**        * FROM NATURAL PROGRAM : Twrp0471
************************************************************
**        * FILE NAME            : Twrp0471.java
**        * CLASS NAME           : Twrp0471
**        * INSTANCE NAME        : Twrp0471
************************************************************
************************************************************************
** PROGRAM : TWRP0471
** SYSTEM  : TAXWARS
** AUTHOR  : RITA SALGADO
** FUNCTION: LOAD SUBPLAN TABLE WITH DATA FROM EDW.
** HISTORY : 01/2012  SUNY/CUNY
**
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0471 extends BLNatBase
{
    // Data Areas
    private LdaTwrlsprd ldaTwrlsprd;
    private LdaTwrlspup ldaTwrlspup;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup edw_Subplan_Record;
    private DbsField edw_Subplan_Record_Edw_Subplan;
    private DbsField edw_Subplan_Record_Edw_Subplan_Name;
    private DbsField pnd_Timx;
    private DbsField pnd_Isn;
    private DbsField pnd_Update;
    private DbsField pnd_Add;
    private DbsField pnd_Inverse_Lu_Ts;
    private DbsField pnd_Action;
    private DbsField pnd_Total_Read;
    private DbsField pnd_Total_Loaded;
    private DbsField pnd_Total_Not_Loaded;
    private DbsField pnd_Total_No_Info;
    private DbsField pnd_Total_Dup_Feed;
    private DbsField pnd_Rt_Super1;

    private DbsGroup pnd_Rt_Super1__R_Field_1;
    private DbsField pnd_Rt_Super1_Pnd_Rt_A_I_Ind;
    private DbsField pnd_Rt_Super1_Pnd_Rt_Table_Id;
    private DbsField pnd_Rt_Super1_Pnd_Rt_Plan;
    private DbsField pnd_Rt_Data;

    private DbsGroup pnd_Rt_Data__R_Field_2;
    private DbsField pnd_Rt_Data_Pnd_Rt_Sp_Name;
    private DbsField pnd_Prior_Subplan;
    private DbsField pnd_Edw_Data;

    private DbsGroup pnd_Edw_Data__R_Field_3;
    private DbsField pnd_Edw_Data_Pnd_Edw_Subplan_Name;
    private DbsField pnd_Rt_Sp_Comm_Ind;
    private DbsField pnd_Rt_Sp_Excl_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrlsprd = new LdaTwrlsprd();
        registerRecord(ldaTwrlsprd);
        registerRecord(ldaTwrlsprd.getVw_twrlspln());
        ldaTwrlspup = new LdaTwrlspup();
        registerRecord(ldaTwrlspup);
        registerRecord(ldaTwrlspup.getVw_twrlspln_U());

        // Local Variables
        localVariables = new DbsRecord();

        edw_Subplan_Record = localVariables.newGroupInRecord("edw_Subplan_Record", "EDW-SUBPLAN-RECORD");
        edw_Subplan_Record_Edw_Subplan = edw_Subplan_Record.newFieldInGroup("edw_Subplan_Record_Edw_Subplan", "EDW-SUBPLAN", FieldType.STRING, 6);
        edw_Subplan_Record_Edw_Subplan_Name = edw_Subplan_Record.newFieldInGroup("edw_Subplan_Record_Edw_Subplan_Name", "EDW-SUBPLAN-NAME", FieldType.STRING, 
            32);
        pnd_Timx = localVariables.newFieldInRecord("pnd_Timx", "#TIMX", FieldType.TIME);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Update = localVariables.newFieldInRecord("pnd_Update", "#UPDATE", FieldType.BOOLEAN, 1);
        pnd_Add = localVariables.newFieldInRecord("pnd_Add", "#ADD", FieldType.BOOLEAN, 1);
        pnd_Inverse_Lu_Ts = localVariables.newFieldInRecord("pnd_Inverse_Lu_Ts", "#INVERSE-LU-TS", FieldType.PACKED_DECIMAL, 13);
        pnd_Action = localVariables.newFieldInRecord("pnd_Action", "#ACTION", FieldType.STRING, 6);
        pnd_Total_Read = localVariables.newFieldInRecord("pnd_Total_Read", "#TOTAL-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Loaded = localVariables.newFieldInRecord("pnd_Total_Loaded", "#TOTAL-LOADED", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Not_Loaded = localVariables.newFieldInRecord("pnd_Total_Not_Loaded", "#TOTAL-NOT-LOADED", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_No_Info = localVariables.newFieldInRecord("pnd_Total_No_Info", "#TOTAL-NO-INFO", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Dup_Feed = localVariables.newFieldInRecord("pnd_Total_Dup_Feed", "#TOTAL-DUP-FEED", FieldType.PACKED_DECIMAL, 7);
        pnd_Rt_Super1 = localVariables.newFieldInRecord("pnd_Rt_Super1", "#RT-SUPER1", FieldType.STRING, 66);

        pnd_Rt_Super1__R_Field_1 = localVariables.newGroupInRecord("pnd_Rt_Super1__R_Field_1", "REDEFINE", pnd_Rt_Super1);
        pnd_Rt_Super1_Pnd_Rt_A_I_Ind = pnd_Rt_Super1__R_Field_1.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt_A_I_Ind", "#RT-A-I-IND", FieldType.STRING, 1);
        pnd_Rt_Super1_Pnd_Rt_Table_Id = pnd_Rt_Super1__R_Field_1.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt_Table_Id", "#RT-TABLE-ID", FieldType.STRING, 5);
        pnd_Rt_Super1_Pnd_Rt_Plan = pnd_Rt_Super1__R_Field_1.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt_Plan", "#RT-PLAN", FieldType.STRING, 6);
        pnd_Rt_Data = localVariables.newFieldInRecord("pnd_Rt_Data", "#RT-DATA", FieldType.STRING, 32);

        pnd_Rt_Data__R_Field_2 = localVariables.newGroupInRecord("pnd_Rt_Data__R_Field_2", "REDEFINE", pnd_Rt_Data);
        pnd_Rt_Data_Pnd_Rt_Sp_Name = pnd_Rt_Data__R_Field_2.newFieldInGroup("pnd_Rt_Data_Pnd_Rt_Sp_Name", "#RT-SP-NAME", FieldType.STRING, 32);
        pnd_Prior_Subplan = localVariables.newFieldInRecord("pnd_Prior_Subplan", "#PRIOR-SUBPLAN", FieldType.STRING, 6);
        pnd_Edw_Data = localVariables.newFieldInRecord("pnd_Edw_Data", "#EDW-DATA", FieldType.STRING, 32);

        pnd_Edw_Data__R_Field_3 = localVariables.newGroupInRecord("pnd_Edw_Data__R_Field_3", "REDEFINE", pnd_Edw_Data);
        pnd_Edw_Data_Pnd_Edw_Subplan_Name = pnd_Edw_Data__R_Field_3.newFieldInGroup("pnd_Edw_Data_Pnd_Edw_Subplan_Name", "#EDW-SUBPLAN-NAME", FieldType.STRING, 
            32);
        pnd_Rt_Sp_Comm_Ind = localVariables.newFieldInRecord("pnd_Rt_Sp_Comm_Ind", "#RT-SP-COMM-IND", FieldType.BOOLEAN, 1);
        pnd_Rt_Sp_Excl_Ind = localVariables.newFieldInRecord("pnd_Rt_Sp_Excl_Ind", "#RT-SP-EXCL-IND", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrlsprd.initializeValues();
        ldaTwrlspup.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0471() throws Exception
    {
        super("Twrp0471");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 133 ZP = ON
        //*  ONE TIME STAMP FOR THE ENTIRE LOAD
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *INIT-USER '-' *PROGRAM 45T 'TAX WITHHOLDING AND REPORTING SYSTEM' 120T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / 'RUNDATE :' *DATU 49T 'SUBPLAN TABLE CONTROL REPORT' / 'RUNTIME :' *TIMX ( EM = HH:IIAP ) // 'TAX YEAR:' //
        pnd_Timx.setValue(Global.getTIMX());                                                                                                                              //Natural: ASSIGN #TIMX := *TIMX
        R_EDW:                                                                                                                                                            //Natural: READ WORK FILE 1 EDW-SUBPLAN-RECORD
        while (condition(getWorkFiles().read(1, edw_Subplan_Record)))
        {
            pnd_Total_Read.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #TOTAL-READ
            if (condition(edw_Subplan_Record_Edw_Subplan.equals(" ") && edw_Subplan_Record_Edw_Subplan_Name.equals(" ")))                                                 //Natural: IF EDW-SUBPLAN = ' ' AND EDW-SUBPLAN-NAME = ' '
            {
                pnd_Total_No_Info.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TOTAL-NO-INFO
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(edw_Subplan_Record_Edw_Subplan.equals(pnd_Prior_Subplan)))                                                                                      //Natural: IF EDW-SUBPLAN = #PRIOR-SUBPLAN
            {
                pnd_Total_Dup_Feed.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOTAL-DUP-FEED
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Prior_Subplan.setValue(edw_Subplan_Record_Edw_Subplan);                                                                                               //Natural: ASSIGN #PRIOR-SUBPLAN := EDW-SUBPLAN
            }                                                                                                                                                             //Natural: END-IF
            //*  #EDW-DATA
            pnd_Edw_Data_Pnd_Edw_Subplan_Name.setValue(edw_Subplan_Record_Edw_Subplan_Name);                                                                              //Natural: ASSIGN #EDW-SUBPLAN-NAME := EDW-SUBPLAN-NAME
            pnd_Rt_Super1.setValue(" ");                                                                                                                                  //Natural: ASSIGN #RT-SUPER1 := ' '
            pnd_Rt_Super1_Pnd_Rt_A_I_Ind.setValue("A");                                                                                                                   //Natural: ASSIGN #RT-A-I-IND := 'A'
            pnd_Rt_Super1_Pnd_Rt_Table_Id.setValue("TX012");                                                                                                              //Natural: ASSIGN #RT-TABLE-ID := 'TX012'
            if (condition(edw_Subplan_Record_Edw_Subplan.notEquals(" ")))                                                                                                 //Natural: IF EDW-SUBPLAN NE ' '
            {
                pnd_Rt_Super1_Pnd_Rt_Plan.setValue(edw_Subplan_Record_Edw_Subplan);                                                                                       //Natural: ASSIGN #RT-PLAN := EDW-SUBPLAN
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Rt_Super1_Pnd_Rt_Plan.setValue("Name not supplied by EDW");                                                                                           //Natural: ASSIGN #RT-PLAN := 'Name not supplied by EDW'
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrlsprd.getVw_twrlspln().startDatabaseRead                                                                                                                //Natural: READ ( 1 ) TWRLSPLN BY RT-SUPER1 STARTING FROM #RT-SUPER1
            (
            "R_PLAN",
            new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Rt_Super1, WcType.BY) },
            new Oc[] { new Oc("RT_SUPER1", "ASC") },
            1
            );
            R_PLAN:
            while (condition(ldaTwrlsprd.getVw_twrlspln().readNextRow("R_PLAN")))
            {
                pnd_Isn.setValue(ldaTwrlsprd.getVw_twrlspln().getAstISN("R_PLAN"));                                                                                       //Natural: ASSIGN #ISN := *ISN
                //*    #RT-DATA
                pnd_Rt_Data_Pnd_Rt_Sp_Name.setValue(ldaTwrlsprd.getTwrlspln_Rt_Sp_Name());                                                                                //Natural: ASSIGN #RT-SP-NAME := TWRLSPLN.RT-SP-NAME
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R_EDW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R_EDW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Add.setValue(false);                                                                                                                                      //Natural: ASSIGN #ADD := FALSE
            pnd_Update.setValue(false);                                                                                                                                   //Natural: ASSIGN #UPDATE := FALSE
            short decideConditionsMet166 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN TWRLSPLN.RT-TABLE-ID NE 'TX012' OR TWRLSPLN.RT-A-I-IND NE 'A' OR TWRLSPLN.RT-SUBPLAN NE EDW-SUBPLAN
            if (condition(ldaTwrlsprd.getTwrlspln_Rt_Table_Id().notEquals("TX012") || ldaTwrlsprd.getTwrlspln_Rt_A_I_Ind().notEquals("A") || ldaTwrlsprd.getTwrlspln_Rt_Subplan().notEquals(edw_Subplan_Record_Edw_Subplan)))
            {
                decideConditionsMet166++;
                pnd_Add.setValue(true);                                                                                                                                   //Natural: ASSIGN #ADD := TRUE
                                                                                                                                                                          //Natural: PERFORM ADD-NEW-SUBPLAN
                sub_Add_New_Subplan();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R_EDW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R_EDW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN TWRLSPLN.RT-TABLE-ID = 'TX012' AND TWRLSPLN.RT-A-I-IND = 'A' AND TWRLSPLN.RT-SUBPLAN = EDW-SUBPLAN AND #RT-DATA NE #EDW-DATA
            else if (condition(ldaTwrlsprd.getTwrlspln_Rt_Table_Id().equals("TX012") && ldaTwrlsprd.getTwrlspln_Rt_A_I_Ind().equals("A") && ldaTwrlsprd.getTwrlspln_Rt_Subplan().equals(edw_Subplan_Record_Edw_Subplan) 
                && pnd_Rt_Data.notEquals(pnd_Edw_Data)))
            {
                decideConditionsMet166++;
                pnd_Update.setValue(true);                                                                                                                                //Natural: ASSIGN #UPDATE := TRUE
                                                                                                                                                                          //Natural: PERFORM UPDATE-SUBPLAN
                sub_Update_Subplan();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R_EDW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R_EDW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet166 > 0))
            {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUBPLAN
                sub_Print_Subplan();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R_EDW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R_EDW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Total_Not_Loaded.nadd(1);                                                                                                                             //Natural: ADD 1 TO #TOTAL-NOT-LOADED
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        R_EDW_Exit:
        if (Global.isEscape()) return;
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-SUBPLAN
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-NEW-SUBPLAN
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SUBPLAN
        //* ********************************************************************
        getReports().write(1, NEWLINE,"Total Records from file  :",pnd_Total_Read,NEWLINE,"Total Records with Blanks:",pnd_Total_No_Info,NEWLINE,"Total Duplicate EDW Recds:", //Natural: WRITE ( 01 ) / 'Total Records from file  :' #TOTAL-READ / 'Total Records with Blanks:' #TOTAL-NO-INFO / 'Total Duplicate EDW Recds:' #TOTAL-DUP-FEED / 'Total Records not Loaded :' #TOTAL-NOT-LOADED / 'Total Records Loaded     :' #TOTAL-LOADED
            pnd_Total_Dup_Feed,NEWLINE,"Total Records not Loaded :",pnd_Total_Not_Loaded,NEWLINE,"Total Records Loaded     :",pnd_Total_Loaded);
        if (Global.isEscape()) return;
    }
    private void sub_Update_Subplan() throws Exception                                                                                                                    //Natural: UPDATE-SUBPLAN
    {
        if (BLNatReinput.isReinput()) return;

        UPDATE_SUBPLAN:                                                                                                                                                   //Natural: GET TWRLSPLN-U #ISN
        ldaTwrlspup.getVw_twrlspln_U().readByID(pnd_Isn.getLong(), "UPDATE_SUBPLAN");
        ldaTwrlspup.getTwrlspln_U_Rt_A_I_Ind().setValue("I");                                                                                                             //Natural: ASSIGN TWRLSPLN-U.RT-A-I-IND := 'I'
        ldaTwrlspup.getTwrlspln_U_Rt_Sp_Lu_Ts().setValue(pnd_Timx);                                                                                                       //Natural: ASSIGN TWRLSPLN-U.RT-SP-LU-TS := #TIMX
        ldaTwrlspup.getTwrlspln_U_Rt_Sp_Inverse_Lu_Ts().compute(new ComputeParameters(false, ldaTwrlspup.getTwrlspln_U_Rt_Sp_Inverse_Lu_Ts()), new DbsDecimal("1000000000000").subtract(ldaTwrlspup.getTwrlspln_U_Rt_Sp_Lu_Ts())); //Natural: ASSIGN TWRLSPLN-U.RT-SP-INVERSE-LU-TS := 1000000000000 - TWRLSPLN-U.RT-SP-LU-TS
        ldaTwrlspup.getTwrlspln_U_Rt_Sp_Lu_User_Id().setValue(Global.getINIT_USER());                                                                                     //Natural: ASSIGN TWRLSPLN-U.RT-SP-LU-USER-ID := *INIT-USER
        pnd_Rt_Sp_Comm_Ind.setValue(ldaTwrlspup.getTwrlspln_U_Rt_Sp_Comm_Ind().getBoolean());                                                                             //Natural: ASSIGN #RT-SP-COMM-IND := TWRLSPLN-U.RT-SP-COMM-IND
        pnd_Rt_Sp_Excl_Ind.setValue(ldaTwrlspup.getTwrlspln_U_Rt_Sp_Excl_Ind().getBoolean());                                                                             //Natural: ASSIGN #RT-SP-EXCL-IND := TWRLSPLN-U.RT-SP-EXCL-IND
        ldaTwrlspup.getVw_twrlspln_U().updateDBRow("UPDATE_SUBPLAN");                                                                                                     //Natural: UPDATE ( UPDATE-SUBPLAN. )
                                                                                                                                                                          //Natural: PERFORM ADD-NEW-SUBPLAN
        sub_Add_New_Subplan();
        if (condition(Global.isEscape())) {return;}
        //*  UPDATE-PLAN
    }
    private void sub_Add_New_Subplan() throws Exception                                                                                                                   //Natural: ADD-NEW-SUBPLAN
    {
        if (BLNatReinput.isReinput()) return;

        ldaTwrlspup.getTwrlspln_U_Rt_A_I_Ind().setValue("A");                                                                                                             //Natural: ASSIGN TWRLSPLN-U.RT-A-I-IND := 'A'
        ldaTwrlspup.getTwrlspln_U_Rt_Table_Id().setValue("TX012");                                                                                                        //Natural: ASSIGN TWRLSPLN-U.RT-TABLE-ID := 'TX012'
        ldaTwrlspup.getTwrlspln_U_Rt_Subplan().setValue(edw_Subplan_Record_Edw_Subplan);                                                                                  //Natural: ASSIGN TWRLSPLN-U.RT-SUBPLAN := EDW-SUBPLAN
        ldaTwrlspup.getTwrlspln_U_Rt_Sp_Cr_User_Id().setValue(Global.getINIT_USER());                                                                                     //Natural: ASSIGN TWRLSPLN-U.RT-SP-CR-USER-ID := *INIT-USER
        ldaTwrlspup.getTwrlspln_U_Rt_Sp_Cr_Ts().setValue(pnd_Timx);                                                                                                       //Natural: ASSIGN TWRLSPLN-U.RT-SP-CR-TS := #TIMX
        ldaTwrlspup.getTwrlspln_U_Rt_Sp_Lu_User_Id().setValue(Global.getINIT_USER());                                                                                     //Natural: ASSIGN TWRLSPLN-U.RT-SP-LU-USER-ID := *INIT-USER
        ldaTwrlspup.getTwrlspln_U_Rt_Sp_Lu_Ts().setValue(ldaTwrlspup.getTwrlspln_U_Rt_Sp_Cr_Ts());                                                                        //Natural: ASSIGN TWRLSPLN-U.RT-SP-LU-TS := TWRLSPLN-U.RT-SP-CR-TS
        ldaTwrlspup.getTwrlspln_U_Rt_Sp_Inverse_Lu_Ts().compute(new ComputeParameters(false, ldaTwrlspup.getTwrlspln_U_Rt_Sp_Inverse_Lu_Ts()), new DbsDecimal("1000000000000").subtract(ldaTwrlspup.getTwrlspln_U_Rt_Sp_Cr_Ts())); //Natural: ASSIGN TWRLSPLN-U.RT-SP-INVERSE-LU-TS := 1000000000000 - TWRLSPLN-U.RT-SP-CR-TS
        ldaTwrlspup.getTwrlspln_U_Rt_Sp_Srce().setValue("B");                                                                                                             //Natural: ASSIGN TWRLSPLN-U.RT-SP-SRCE := 'B'
        if (condition(pnd_Add.getBoolean()))                                                                                                                              //Natural: IF #ADD
        {
            ldaTwrlspup.getTwrlspln_U_Rt_Sp_Comm_Ind().reset();                                                                                                           //Natural: RESET TWRLSPLN-U.RT-SP-COMM-IND TWRLSPLN-U.RT-SP-EXCL-IND
            ldaTwrlspup.getTwrlspln_U_Rt_Sp_Excl_Ind().reset();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrlspup.getTwrlspln_U_Rt_Sp_Comm_Ind().setValue(pnd_Rt_Sp_Comm_Ind.getBoolean());                                                                         //Natural: ASSIGN TWRLSPLN-U.RT-SP-COMM-IND := #RT-SP-COMM-IND
            ldaTwrlspup.getTwrlspln_U_Rt_Sp_Excl_Ind().setValue(pnd_Rt_Sp_Excl_Ind.getBoolean());                                                                         //Natural: ASSIGN TWRLSPLN-U.RT-SP-EXCL-IND := #RT-SP-EXCL-IND
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrlspup.getTwrlspln_U_Rt_Sp_Name().setValue(edw_Subplan_Record_Edw_Subplan_Name);                                                                             //Natural: ASSIGN TWRLSPLN-U.RT-SP-NAME := EDW-SUBPLAN-NAME
        ldaTwrlspup.getTwrlspln_U_Rt_Long_Key().setValue(edw_Subplan_Record_Edw_Subplan_Name);                                                                            //Natural: ASSIGN TWRLSPLN-U.RT-LONG-KEY := EDW-SUBPLAN-NAME
        ldaTwrlspup.getVw_twrlspln_U().insertDBRow();                                                                                                                     //Natural: STORE TWRLSPLN-U
        //*  ADD-NEW-SUBPLAN
    }
    private void sub_Print_Subplan() throws Exception                                                                                                                     //Natural: PRINT-SUBPLAN
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Update.getBoolean()))                                                                                                                           //Natural: IF #UPDATE
        {
            pnd_Action.setValue("Update");                                                                                                                                //Natural: ASSIGN #ACTION := 'Update'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Add.getBoolean()))                                                                                                                              //Natural: IF #ADD
        {
            pnd_Action.setValue("Add");                                                                                                                                   //Natural: ASSIGN #ACTION := 'Add'
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(1, "Sub Plan",                                                                                                                               //Natural: DISPLAY ( 01 ) 'Sub Plan' EDW-SUBPLAN 'Sub Plan Name' EDW-SUBPLAN-NAME 'Action' #ACTION
        		edw_Subplan_Record_Edw_Subplan,"Sub Plan Name",
        		edw_Subplan_Record_Edw_Subplan_Name,"Action",
        		pnd_Action);
        if (Global.isEscape()) return;
        pnd_Total_Loaded.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TOTAL-LOADED
        //*  PRINT-PLAN
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=133 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(45),"TAX WITHHOLDING AND REPORTING SYSTEM",new 
            TabSetting(120),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,"RUNDATE :",Global.getDATU(),new TabSetting(49),"SUBPLAN TABLE CONTROL REPORT",NEWLINE,"RUNTIME :",Global.getTIMX(), 
            new ReportEditMask ("HH:IIAP"),NEWLINE,NEWLINE,"TAX YEAR:",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, "Sub Plan",
        		edw_Subplan_Record_Edw_Subplan,"Sub Plan Name",
        		edw_Subplan_Record_Edw_Subplan_Name,"Action",
        		pnd_Action);
    }
}
