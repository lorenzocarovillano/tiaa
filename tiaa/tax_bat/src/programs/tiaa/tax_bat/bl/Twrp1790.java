/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:33:21 PM
**        * FROM NATURAL PROGRAM : Twrp1790
************************************************************
**        * FILE NAME            : Twrp1790.java
**        * CLASS NAME           : Twrp1790
**        * INSTANCE NAME        : Twrp1790
************************************************************
************************************************************************
** PROGRAM     : TWRP1790
** SYSTEM      : TAXWARS
** AUTHOR      : MICHAEL SUPONITSKY
** FUNCTION    : REPORT OF EXCESS INSURANCE DIVIDENDS.
** HISTORY.....:
** 08-20-2004  : ALTHEA YOUNG  - REVISED TO PRINT APPLICABLE GROSS,
**             :                 INTEREST, AND IVC GRAND TOTAL AMOUNTS
**             :                 AT THE END OF EACH REPORT.
**             :               - MOVED TOTAL PRINTING AFTER SUBROUTINES
**             :                 IN ORDER TO UTILIZE T* POSITIONS.
**             :               - SUBTRACT 1 FROM *DATN / 10000 TAX YEAR
**             :                 CALCULATION IF CURRENT MONTH IS JAN.
**             :               - ADD '//' TO COLUMN HEADING FOR BACK-UP
**             :                 WITHHOLDING IN REPORT 2.
** 11-02-2004  : ALTHEA YOUNG  - REVISED TO INCLUDE DISTRIBUTION CODES
**             :                 FOR DCI / DPI & GROUP INTEREST
**             :                 (PAYMENT TYPE 'N', SETTL. TYPE 'T').
**             :               - CHANGED SUPER TWRPYMNT-FORM-SD-1 TO
**             :                 TWRPYMNT-FORM-SD-2 WHEREVER APPLICABLE.
** 01-06-2005  : ALTHEA YOUNG  - RE-COMPILED DUE TO PAYMENT FILE SUPER-
**             :                 DESCRIPTOR CHANGES.  USER P. RIVERA
**             :                 REPORTED INCONSISTENCIES IN 01/03/05
**             :                 REPORT.
** 10-28-2005  : ALTHEA YOUNG  - REVISED TO INCLUDE TAX YEAR AS THE
**             :                 FIRST FIELD IN ALL REPORTS, AND
**             :                 ELIMINATE ALL FIELDS WITH (IS=ON).
**             :                 THIS IS BEING DONE TO FACILITATE MOBIUS
**             :                 REPORT POLICIES TO CREATE EXCEL SPREAD-
**             :                 SHEETS FROM REPORT FIELDS.
** 12-19-2006  : ALTHEA YOUNG  - REVISED TO INCLUDE FIELD
**             :                 PAY.TWRPYMNT-FED-WTHLD-AMT IN REPORT 1
**             :                 AND EXCEL SPREADSHEET.
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp1790 extends BLNatBase
{
    // Data Areas
    private LdaTwrl9415 ldaTwrl9415;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Input_Parms;
    private DbsField pnd_Ws_Pnd_Text;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_Datn;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Pnd_Datn_Yyyy;
    private DbsField pnd_Ws_Pnd_Datn_Mm;
    private DbsField pnd_Ws_Pnd_Datn_Dd;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Int_Amt;
    private DbsField pnd_Ws_Pnd_Ivc_Amt;
    private DbsField pnd_Ws_Pnd_Fed_Wh_Amt;

    private DbsGroup pnd_Gt;
    private DbsField pnd_Gt_Pnd_Insu_Cnt;
    private DbsField pnd_Gt_Pnd_Insu_Int_Cnt;
    private DbsField pnd_Gt_Pnd_Insu_Oth_Cnt;
    private DbsField pnd_Gt_Pnd_Gt_Ins_Gross_Amt;
    private DbsField pnd_Gt_Pnd_Gt_Ins_Int_Amt;
    private DbsField pnd_Gt_Pnd_Gt_Ins_Ivc_Amt;
    private DbsField pnd_Gt_Pnd_Gt_Ins_Fed_Wh_Amt;
    private DbsField pnd_Gt_Pnd_Gt_Int_Amt;
    private DbsField pnd_Gt_Pnd_Gt_Int_Bkup_Amt;
    private DbsField pnd_Gt_Pnd_Gt_Oth_Amt;
    private DbsField pnd_Gt_Pnd_Gt_Oth_Bkup_Amt;
    private DbsField pnd_Pay_Super_Start;
    private DbsField pnd_Pay_Super_End;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl9415 = new LdaTwrl9415();
        registerRecord(ldaTwrl9415);
        registerRecord(ldaTwrl9415.getVw_pay());

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Input_Parms = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Ws_Pnd_Text = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Text", "#TEXT", FieldType.STRING, 4);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Datn = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Datn", "#DATN", FieldType.NUMERIC, 8);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Datn);
        pnd_Ws_Pnd_Datn_Yyyy = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Datn_Yyyy", "#DATN-YYYY", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Datn_Mm = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Datn_Mm", "#DATN-MM", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_Datn_Dd = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Datn_Dd", "#DATN-DD", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Gross_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.STRING, 12);
        pnd_Ws_Pnd_Int_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Int_Amt", "#INT-AMT", FieldType.STRING, 10);
        pnd_Ws_Pnd_Ivc_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.STRING, 10);
        pnd_Ws_Pnd_Fed_Wh_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Fed_Wh_Amt", "#FED-WH-AMT", FieldType.STRING, 10);

        pnd_Gt = localVariables.newGroupInRecord("pnd_Gt", "#GT");
        pnd_Gt_Pnd_Insu_Cnt = pnd_Gt.newFieldInGroup("pnd_Gt_Pnd_Insu_Cnt", "#INSU-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Gt_Pnd_Insu_Int_Cnt = pnd_Gt.newFieldInGroup("pnd_Gt_Pnd_Insu_Int_Cnt", "#INSU-INT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Gt_Pnd_Insu_Oth_Cnt = pnd_Gt.newFieldInGroup("pnd_Gt_Pnd_Insu_Oth_Cnt", "#INSU-OTH-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Gt_Pnd_Gt_Ins_Gross_Amt = pnd_Gt.newFieldInGroup("pnd_Gt_Pnd_Gt_Ins_Gross_Amt", "#GT-INS-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Gt_Pnd_Gt_Ins_Int_Amt = pnd_Gt.newFieldInGroup("pnd_Gt_Pnd_Gt_Ins_Int_Amt", "#GT-INS-INT-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Gt_Pnd_Gt_Ins_Ivc_Amt = pnd_Gt.newFieldInGroup("pnd_Gt_Pnd_Gt_Ins_Ivc_Amt", "#GT-INS-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Gt_Pnd_Gt_Ins_Fed_Wh_Amt = pnd_Gt.newFieldInGroup("pnd_Gt_Pnd_Gt_Ins_Fed_Wh_Amt", "#GT-INS-FED-WH-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Gt_Pnd_Gt_Int_Amt = pnd_Gt.newFieldInGroup("pnd_Gt_Pnd_Gt_Int_Amt", "#GT-INT-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Gt_Pnd_Gt_Int_Bkup_Amt = pnd_Gt.newFieldInGroup("pnd_Gt_Pnd_Gt_Int_Bkup_Amt", "#GT-INT-BKUP-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Gt_Pnd_Gt_Oth_Amt = pnd_Gt.newFieldInGroup("pnd_Gt_Pnd_Gt_Oth_Amt", "#GT-OTH-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Gt_Pnd_Gt_Oth_Bkup_Amt = pnd_Gt.newFieldInGroup("pnd_Gt_Pnd_Gt_Oth_Bkup_Amt", "#GT-OTH-BKUP-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Pay_Super_Start = localVariables.newFieldInRecord("pnd_Pay_Super_Start", "#PAY-SUPER-START", FieldType.STRING, 5);
        pnd_Pay_Super_End = localVariables.newFieldInRecord("pnd_Pay_Super_End", "#PAY-SUPER-END", FieldType.STRING, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl9415.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp1790() throws Exception
    {
        super("Twrp1790");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp1790|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*  11-02-2004
                //*                                                                                                                                                       //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 02 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 03 ) PS = 58 LS = 133 ZP = ON
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: IF *DEVICE = 'BATCH'
                {
                    //*  SET DELIMITER MODE FOR BATCH INPUT
                    setControl("D");                                                                                                                                      //Natural: SET CONTROL 'D'
                    //*  11-02-2004
                }                                                                                                                                                         //Natural: END-IF
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 48T #WS.#TAX-YEAR ( SG = OFF ) 'Ins Cash Surr, Mat Endowments Cases' 120T 'Report: RPT1' //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 02 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 54T #WS.#TAX-YEAR ( SG = OFF ) 'Insurance Interest' 120T 'Report: RPT2' //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 03 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 50T #WS.#TAX-YEAR ( SG = OFF ) 'DCI, DPI & Group Interest' 120T 'Report: RPT3' //
                //* ***************
                //*  MAIN PROGRAM *
                //* ***************
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_Input_Parms);                                                                               //Natural: INPUT #WS.#INPUT-PARMS
                //*  08-20-2004
                if (condition(pnd_Ws_Pnd_Tax_Year.equals(getZero())))                                                                                                     //Natural: IF #WS.#TAX-YEAR = 0
                {
                    pnd_Ws_Pnd_Datn.setValue(Global.getDATN());                                                                                                           //Natural: ASSIGN #DATN := *DATN
                    //*  08-20-2004
                    //*  08-20-2004
                    if (condition(pnd_Ws_Pnd_Datn_Mm.equals(1)))                                                                                                          //Natural: IF #DATN-MM = 1
                    {
                        pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), (Global.getDATN().divide(10000)).subtract(1));                     //Natural: ASSIGN #WS.#TAX-YEAR := ( *DATN / 10000 ) - 1
                        //*  08-20-2004
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), Global.getDATN().divide(10000));                                   //Natural: ASSIGN #WS.#TAX-YEAR := *DATN / 10000
                        //*  08-20-2004
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Pay_Super_Start.setValueEdited(pnd_Ws_Pnd_Tax_Year,new ReportEditMask("9999"));                                                                       //Natural: MOVE EDITED #WS.#TAX-YEAR ( EM = 9999 ) TO #PAY-SUPER-START
                pnd_Pay_Super_End.setValue(pnd_Pay_Super_Start);                                                                                                          //Natural: ASSIGN #PAY-SUPER-END := #PAY-SUPER-START
                setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Pay_Super_Start,5,1);                                                                                     //Natural: MOVE LOW-VALUES TO SUBSTR ( #PAY-SUPER-START,5,1 )
                setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Pay_Super_End,5,1);                                                                                      //Natural: MOVE HIGH-VALUES TO SUBSTR ( #PAY-SUPER-END,5,1 )
                //*  11-02-2004
                //*  11-02-2004
                ldaTwrl9415.getVw_pay().startDatabaseRead                                                                                                                 //Natural: READ PAY BY TWRPYMNT-FORM-SD-2 = #PAY-SUPER-START THRU #PAY-SUPER-END
                (
                "RD_PAY",
                new Wc[] { new Wc("TWRPYMNT_FORM_SD_2", ">=", pnd_Pay_Super_Start, "And", WcType.BY) ,
                new Wc("TWRPYMNT_FORM_SD_2", "<=", pnd_Pay_Super_End, WcType.BY) },
                new Oc[] { new Oc("TWRPYMNT_FORM_SD_2", "ASC") }
                );
                boolean endOfDataRdPay = true;
                boolean firstRdPay = true;
                RD_PAY:
                while (condition(ldaTwrl9415.getVw_pay().readNextRow("RD_PAY")))
                {
                    if (condition(ldaTwrl9415.getVw_pay().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventRd_Pay();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataRdPay = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    FOR01:                                                                                                                                                //Natural: AT BREAK OF TWRPYMNT-FORM-SD-2;//Natural: FOR #WS.#I = 1 TO C*TWRPYMNT-PAYMENTS
                    for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl9415.getPay_Count_Casttwrpymnt_Payments())); pnd_Ws_Pnd_I.nadd(1))
                    {
                        if (condition(! (ldaTwrl9415.getPay_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals(" ") || ldaTwrl9415.getPay_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals("C")))) //Natural: IF NOT PAY.TWRPYMNT-PYMNT-STATUS ( #I ) = ' ' OR = 'C'
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                        short decideConditionsMet282 = 0;                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'N' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'K'
                        if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_Ws_Pnd_I).equals("N") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("K")))
                        {
                            decideConditionsMet282++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-INSU
                            sub_Process_Insu();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'I' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'T'
                        else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_Ws_Pnd_I).equals("I") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("T")))
                        {
                            decideConditionsMet282++;
                            //*  11-02-2004
                                                                                                                                                                          //Natural: PERFORM PROCESS-INSU-INT
                            sub_Process_Insu_Int();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: WHEN PAY.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'N' AND PAY.TWRPYMNT-SETTLE-TYPE ( #I ) = 'T'
                        else if (condition(ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_Ws_Pnd_I).equals("N") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("T")))
                        {
                            decideConditionsMet282++;
                            //*  11-02-2004
                                                                                                                                                                          //Natural: PERFORM PROCESS-DCI-DPI-GRP-INT
                            sub_Process_Dci_Dpi_Grp_Int();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: WHEN NONE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_PAY"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_PAY"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-READ
                if (condition(ldaTwrl9415.getVw_pay().getAstCOUNTER().greater(0)))
                {
                    atBreakEventRd_Pay(endOfDataRdPay);
                }
                if (Global.isEscape()) return;
                //* **********************
                //*  S U B R O U T I N E S
                //* *****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INSU
                //*  'Prt'                 PAY.TWRPYMNT-IVC-PROTECT      (#I)
                //* *********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INSU-INT
                //* ****************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-DCI-DPI-GRP-INT
                getReports().skip(1, 4);                                                                                                                                  //Natural: SKIP ( 1 ) 4 LINES
                getReports().skip(2, 4);                                                                                                                                  //Natural: SKIP ( 2 ) 4 LINES
                //*  11-02-2004
                //*  10-28-2005
                //* 08-20-2004
                //* 08-20-2004
                //* 08-20-2004
                //* 12-19-2006
                //*  10-28-2005
                //*  08-20-2004
                //* 08-20-2004
                //*  11-02-2004
                //*  10-28-2005
                getReports().skip(3, 4);                                                                                                                                  //Natural: SKIP ( 3 ) 4 LINES
                getReports().write(1, ReportOption.NOTITLE,pnd_Ws_Pnd_Tax_Year, new SignPosition (false),"Ins Cash Surr, Mat Endowments Cases:",new ColumnSpacing(4),pnd_Gt_Pnd_Insu_Cnt,  //Natural: WRITE ( 1 ) #WS.#TAX-YEAR ( SG = OFF ) 'Ins Cash Surr, Mat Endowments Cases:' 4X #GT.#INSU-CNT T*PAY.TWRPYMNT-GROSS-AMT ( #I ) 2X #GT.#GT-INS-GROSS-AMT T*PAY.TWRPYMNT-INT-AMT ( #I ) 2X #GT.#GT-INS-INT-AMT T*PAY.TWRPYMNT-IVC-AMT ( #I ) 2X #GT.#GT-INS-IVC-AMT T*PAY.TWRPYMNT-FED-WTHLD-AMT ( #I ) 2X #GT.#GT-INS-FED-WH-AMT
                    new ReportEditMask ("-Z,ZZZ,ZZ9"),new ReportTAsterisk(ldaTwrl9415.getPay_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I)),new ColumnSpacing(2),pnd_Gt_Pnd_Gt_Ins_Gross_Amt, 
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ReportTAsterisk(ldaTwrl9415.getPay_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I)),new ColumnSpacing(2),pnd_Gt_Pnd_Gt_Ins_Int_Amt, 
                    new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ReportTAsterisk(ldaTwrl9415.getPay_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I)),new ColumnSpacing(2),pnd_Gt_Pnd_Gt_Ins_Ivc_Amt, 
                    new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ReportTAsterisk(ldaTwrl9415.getPay_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I)),new ColumnSpacing(2),pnd_Gt_Pnd_Gt_Ins_Fed_Wh_Amt, 
                    new ReportEditMask ("Z,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(2, pnd_Ws_Pnd_Tax_Year, new SignPosition (false),"Insurance Interest Cases:",new ColumnSpacing(12),pnd_Gt_Pnd_Insu_Int_Cnt,            //Natural: WRITE ( 2 ) #WS.#TAX-YEAR ( SG = OFF ) 'Insurance Interest Cases:' 12X #GT.#INSU-INT-CNT T*PAY.TWRPYMNT-INT-AMT ( #I ) 2X #GT.#GT-INT-AMT T*PAY.TWRPYMNT-INT-BKUP-WTHLD ( #I ) 2X #GT.#GT-INT-BKUP-AMT
                    new ReportEditMask ("-Z,ZZZ,ZZ9"),new ReportTAsterisk(ldaTwrl9415.getPay_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I)),new ColumnSpacing(2),pnd_Gt_Pnd_Gt_Int_Amt, 
                    new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ReportTAsterisk(ldaTwrl9415.getPay_Twrpymnt_Int_Bkup_Wthld().getValue(pnd_Ws_Pnd_I)),new ColumnSpacing(2),pnd_Gt_Pnd_Gt_Int_Bkup_Amt, 
                    new ReportEditMask ("Z,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(3, pnd_Ws_Pnd_Tax_Year, new SignPosition (false),"DCI, DPI & Group Interest Cases:",new ColumnSpacing(5),pnd_Gt_Pnd_Insu_Oth_Cnt,      //Natural: WRITE ( 3 ) #WS.#TAX-YEAR ( SG = OFF ) 'DCI, DPI & Group Interest Cases:'5X #GT.#INSU-OTH-CNT T*PAY.TWRPYMNT-INT-AMT ( #I ) 2X #GT.#GT-OTH-AMT T*PAY.TWRPYMNT-INT-BKUP-WTHLD ( #I ) 2X #GT.#GT-OTH-BKUP-AMT
                    new ReportEditMask ("-Z,ZZZ,ZZ9"),new ReportTAsterisk(ldaTwrl9415.getPay_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I)),new ColumnSpacing(2),pnd_Gt_Pnd_Gt_Oth_Amt, 
                    new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ReportTAsterisk(ldaTwrl9415.getPay_Twrpymnt_Int_Bkup_Wthld().getValue(pnd_Ws_Pnd_I)),new ColumnSpacing(2),pnd_Gt_Pnd_Gt_Oth_Bkup_Amt, 
                    new ReportEditMask ("Z,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Process_Insu() throws Exception                                                                                                                      //Natural: PROCESS-INSU
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        //*  08-20-2004
        //*  10-28-2005
        //*  10-28-2005
        //*  10-28-2005
        //*  10-28-2005
        //*  10-28-2005
        //*  10-28-2005
        //*  12-19-2006
        pnd_Gt_Pnd_Insu_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #GT.#INSU-CNT
        getReports().display(1, new ReportEmptyLineSuppression(true),"/Tax/Year",                                                                                         //Natural: DISPLAY ( 1 ) ( ES = ON ) '/Tax/Year' PAY.TWRPYMNT-TAX-YEAR ( EM = 9999 ) '/Co/mp' PAY.TWRPYMNT-COMPANY-CDE '//TIN' PAY.TWRPYMNT-TAX-ID-NBR '//Contract' PAY.TWRPYMNT-CONTRACT-NBR 'P/a/y' PAY.TWRPYMNT-PAYEE-CDE '//Res' PAY.TWRPYMNT-RESIDENCY-CODE '/Di/st' PAY.TWRPYMNT-DISTRIBUTION-CDE '//RACF' PAY.TWRPYMNT-UPDTE-USER ( #I ) '/Pymnt/Date' PAY.TWRPYMNT-PYMNT-DTE ( #I ) '/Srce/Orig' PAY.TWRPYMNT-ORGN-SRCE-CDE ( #I ) '/Srce/Updt' PAY.TWRPYMNT-UPDTE-SRCE-CDE ( #I ) '/IVC/Ind' PAY.TWRPYMNT-IVC-IND ( #I ) '//Gross' PAY.TWRPYMNT-GROSS-AMT ( #I ) ( HC = R EM = -ZZZ,ZZZ,ZZ9.99 ) '//Interest' PAY.TWRPYMNT-INT-AMT ( #I ) ( HC = R EM = -Z,ZZZ,ZZ9.99 ) '/IVC/Amt' PAY.TWRPYMNT-IVC-AMT ( #I ) ( HC = R EM = -Z,ZZZ,ZZ9.99 ) '/Fed Wthld/Tax Amt' PAY.TWRPYMNT-FED-WTHLD-AMT ( #I ) ( HC = R EM = -Z,ZZZ,ZZ9.99 )
        		ldaTwrl9415.getPay_Twrpymnt_Tax_Year(), new ReportEditMask ("9999"),"/Co/mp",
        		ldaTwrl9415.getPay_Twrpymnt_Company_Cde(),"//TIN",
        		ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr(),"//Contract",
        		ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr(),"P/a/y",
        		ldaTwrl9415.getPay_Twrpymnt_Payee_Cde(),"//Res",
        		ldaTwrl9415.getPay_Twrpymnt_Residency_Code(),"/Di/st",
        		ldaTwrl9415.getPay_Twrpymnt_Distribution_Cde(),"//RACF",
        		ldaTwrl9415.getPay_Twrpymnt_Updte_User().getValue(pnd_Ws_Pnd_I),"/Pymnt/Date",
        		ldaTwrl9415.getPay_Twrpymnt_Pymnt_Dte().getValue(pnd_Ws_Pnd_I),"/Srce/Orig",
        		ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_Ws_Pnd_I),"/Srce/Updt",
        		ldaTwrl9415.getPay_Twrpymnt_Updte_Srce_Cde().getValue(pnd_Ws_Pnd_I),"/IVC/Ind",
        		ldaTwrl9415.getPay_Twrpymnt_Ivc_Ind().getValue(pnd_Ws_Pnd_I),"//Gross",
        		ldaTwrl9415.getPay_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask 
            ("-ZZZ,ZZZ,ZZ9.99"),"//Interest",
        		ldaTwrl9415.getPay_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask 
            ("-Z,ZZZ,ZZ9.99"),"/IVC/Amt",
        		ldaTwrl9415.getPay_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask 
            ("-Z,ZZZ,ZZ9.99"),"/Fed Wthld/Tax Amt",
        		ldaTwrl9415.getPay_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new 
            ReportEditMask ("-Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Gross_Amt.setValueEdited(ldaTwrl9415.getPay_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I),new ReportEditMask("999999999.99"));                           //Natural: MOVE EDITED PAY.TWRPYMNT-GROSS-AMT ( #I ) ( EM = 999999999.99 ) TO #WS.#GROSS-AMT
        pnd_Ws_Pnd_Ivc_Amt.setValueEdited(ldaTwrl9415.getPay_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I),new ReportEditMask("9999999.99"));                                 //Natural: MOVE EDITED PAY.TWRPYMNT-IVC-AMT ( #I ) ( EM = 9999999.99 ) TO #WS.#IVC-AMT
        //*  12-19-2006
        pnd_Ws_Pnd_Fed_Wh_Amt.setValueEdited(ldaTwrl9415.getPay_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I),new ReportEditMask("9999999.99"));                        //Natural: MOVE EDITED PAY.TWRPYMNT-FED-WTHLD-AMT ( #I ) ( EM = 9999999.99 ) TO #WS.#FED-WH-AMT
        //*  08-20-2004
        pnd_Gt_Pnd_Gt_Ins_Gross_Amt.nadd(ldaTwrl9415.getPay_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                                                 //Natural: ADD PAY.TWRPYMNT-GROSS-AMT ( #I ) TO #GT.#GT-INS-GROSS-AMT
        //*  08-20-2004
        pnd_Gt_Pnd_Gt_Ins_Int_Amt.nadd(ldaTwrl9415.getPay_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                                                     //Natural: ADD PAY.TWRPYMNT-INT-AMT ( #I ) TO #GT.#GT-INS-INT-AMT
        //*  08-20-2004
        pnd_Gt_Pnd_Gt_Ins_Ivc_Amt.nadd(ldaTwrl9415.getPay_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                                                                     //Natural: ADD PAY.TWRPYMNT-IVC-AMT ( #I ) TO #GT.#GT-INS-IVC-AMT
        //*  12-19-2006
        pnd_Gt_Pnd_Gt_Ins_Fed_Wh_Amt.nadd(ldaTwrl9415.getPay_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                                                            //Natural: ADD PAY.TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #GT.#GT-INS-FED-WH-AMT
        //*  12-19-2006
        getWorkFiles().write(1, false, ldaTwrl9415.getPay_Twrpymnt_Tax_Year(), ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr(), ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr(),      //Natural: WRITE WORK FILE 1 PAY.TWRPYMNT-TAX-YEAR PAY.TWRPYMNT-TAX-ID-NBR PAY.TWRPYMNT-CONTRACT-NBR PAY.TWRPYMNT-PAYEE-CDE PAY.TWRPYMNT-DISTRIBUTION-CDE PAY.TWRPYMNT-UPDTE-USER ( #I ) PAY.TWRPYMNT-PYMNT-DTE ( #I ) PAY.TWRPYMNT-IVC-IND ( #I ) #WS.#GROSS-AMT #WS.#IVC-AMT #WS.#FED-WH-AMT
            ldaTwrl9415.getPay_Twrpymnt_Payee_Cde(), ldaTwrl9415.getPay_Twrpymnt_Distribution_Cde(), ldaTwrl9415.getPay_Twrpymnt_Updte_User().getValue(pnd_Ws_Pnd_I), 
            ldaTwrl9415.getPay_Twrpymnt_Pymnt_Dte().getValue(pnd_Ws_Pnd_I), ldaTwrl9415.getPay_Twrpymnt_Ivc_Ind().getValue(pnd_Ws_Pnd_I), pnd_Ws_Pnd_Gross_Amt, 
            pnd_Ws_Pnd_Ivc_Amt, pnd_Ws_Pnd_Fed_Wh_Amt);
    }
    private void sub_Process_Insu_Int() throws Exception                                                                                                                  //Natural: PROCESS-INSU-INT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  08-20-2004
        //*  10-28-2005
        //*  10-28-2005
        //*  10-28-2005
        //*  10-28-2005
        //*  10-28-2005
        //*  10-28-2005
        //*  10-28-2005
        //*  08-20-2004
        pnd_Gt_Pnd_Insu_Int_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #GT.#INSU-INT-CNT
        getReports().display(2, new ReportEmptyLineSuppression(true),"/Tax/Year",                                                                                         //Natural: DISPLAY ( 2 ) ( ES = ON ) '/Tax/Year' PAY.TWRPYMNT-TAX-YEAR ( EM = 9999 ) '/Co/mp' PAY.TWRPYMNT-COMPANY-CDE '//TIN' PAY.TWRPYMNT-TAX-ID-NBR '//Contract' PAY.TWRPYMNT-CONTRACT-NBR 'P/a/y' PAY.TWRPYMNT-PAYEE-CDE '//Res' PAY.TWRPYMNT-RESIDENCY-CODE '//RACF' PAY.TWRPYMNT-UPDTE-USER ( #I ) '/Pymnt/Date' PAY.TWRPYMNT-PYMNT-DTE ( #I ) '/Srce/Orig' PAY.TWRPYMNT-ORGN-SRCE-CDE ( #I ) '/Srce/Updt' PAY.TWRPYMNT-UPDTE-SRCE-CDE ( #I ) '/Insur/Interest' PAY.TWRPYMNT-INT-AMT ( #I ) ( HC = R EM = -Z,ZZZ,ZZ9.99 ) '/Backup/Withholding' PAY.TWRPYMNT-INT-BKUP-WTHLD ( #I ) ( HC = R EM = -Z,ZZZ,ZZ9.99 )
        		ldaTwrl9415.getPay_Twrpymnt_Tax_Year(), new ReportEditMask ("9999"),"/Co/mp",
        		ldaTwrl9415.getPay_Twrpymnt_Company_Cde(),"//TIN",
        		ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr(),"//Contract",
        		ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr(),"P/a/y",
        		ldaTwrl9415.getPay_Twrpymnt_Payee_Cde(),"//Res",
        		ldaTwrl9415.getPay_Twrpymnt_Residency_Code(),"//RACF",
        		ldaTwrl9415.getPay_Twrpymnt_Updte_User().getValue(pnd_Ws_Pnd_I),"/Pymnt/Date",
        		ldaTwrl9415.getPay_Twrpymnt_Pymnt_Dte().getValue(pnd_Ws_Pnd_I),"/Srce/Orig",
        		ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_Ws_Pnd_I),"/Srce/Updt",
        		ldaTwrl9415.getPay_Twrpymnt_Updte_Srce_Cde().getValue(pnd_Ws_Pnd_I),"/Insur/Interest",
        		ldaTwrl9415.getPay_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask 
            ("-Z,ZZZ,ZZ9.99"),"/Backup/Withholding",
        		ldaTwrl9415.getPay_Twrpymnt_Int_Bkup_Wthld().getValue(pnd_Ws_Pnd_I), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new 
            ReportEditMask ("-Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Int_Amt.setValueEdited(ldaTwrl9415.getPay_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I),new ReportEditMask("9999999.99"));                                 //Natural: MOVE EDITED PAY.TWRPYMNT-INT-AMT ( #I ) ( EM = 9999999.99 ) TO #WS.#INT-AMT
        //*  08-20-2004
        pnd_Gt_Pnd_Gt_Int_Amt.nadd(ldaTwrl9415.getPay_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                                                         //Natural: ADD PAY.TWRPYMNT-INT-AMT ( #I ) TO #GT.#GT-INT-AMT
        //*  08-20-2004
        pnd_Gt_Pnd_Gt_Int_Bkup_Amt.nadd(ldaTwrl9415.getPay_Twrpymnt_Int_Bkup_Wthld().getValue(pnd_Ws_Pnd_I));                                                             //Natural: ADD PAY.TWRPYMNT-INT-BKUP-WTHLD ( #I ) TO #GT.#GT-INT-BKUP-AMT
        getWorkFiles().write(2, false, ldaTwrl9415.getPay_Twrpymnt_Tax_Year(), ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr(), ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr(),      //Natural: WRITE WORK FILE 2 PAY.TWRPYMNT-TAX-YEAR PAY.TWRPYMNT-TAX-ID-NBR PAY.TWRPYMNT-CONTRACT-NBR PAY.TWRPYMNT-PAYEE-CDE PAY.TWRPYMNT-UPDTE-USER ( #I ) PAY.TWRPYMNT-PYMNT-DTE ( #I ) #WS.#INT-AMT
            ldaTwrl9415.getPay_Twrpymnt_Payee_Cde(), ldaTwrl9415.getPay_Twrpymnt_Updte_User().getValue(pnd_Ws_Pnd_I), ldaTwrl9415.getPay_Twrpymnt_Pymnt_Dte().getValue(pnd_Ws_Pnd_I), 
            pnd_Ws_Pnd_Int_Amt);
    }
    //*  11-02-2004
    private void sub_Process_Dci_Dpi_Grp_Int() throws Exception                                                                                                           //Natural: PROCESS-DCI-DPI-GRP-INT
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        //*  10-28-2005
        //*  10-28-2005
        //*  10-28-2005
        //*  10-28-2005
        //*  10-28-2005
        //*  10-28-2005
        //*  10-28-2005
        pnd_Gt_Pnd_Insu_Oth_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #GT.#INSU-OTH-CNT
        getReports().display(3, new ReportEmptyLineSuppression(true),"/Tax/Year",                                                                                         //Natural: DISPLAY ( 3 ) ( ES = ON ) '/Tax/Year' PAY.TWRPYMNT-TAX-YEAR ( EM = 9999 ) '/Co/mp' PAY.TWRPYMNT-COMPANY-CDE '//TIN' PAY.TWRPYMNT-TAX-ID-NBR '//Contract' PAY.TWRPYMNT-CONTRACT-NBR 'P/a/y' PAY.TWRPYMNT-PAYEE-CDE '//Res' PAY.TWRPYMNT-RESIDENCY-CODE '//RACF' PAY.TWRPYMNT-UPDTE-USER ( #I ) '/Pymnt/Date' PAY.TWRPYMNT-PYMNT-DTE ( #I ) '/Srce/Orig' PAY.TWRPYMNT-ORGN-SRCE-CDE ( #I ) '/Srce/Updt' PAY.TWRPYMNT-UPDTE-SRCE-CDE ( #I ) 'DCI-DPI/& Group/Interest' PAY.TWRPYMNT-INT-AMT ( #I ) ( HC = R EM = -Z,ZZZ,ZZ9.99 ) '/Backup/Withholding' PAY.TWRPYMNT-INT-BKUP-WTHLD ( #I ) ( HC = R EM = -Z,ZZZ,ZZ9.99 )
        		ldaTwrl9415.getPay_Twrpymnt_Tax_Year(), new ReportEditMask ("9999"),"/Co/mp",
        		ldaTwrl9415.getPay_Twrpymnt_Company_Cde(),"//TIN",
        		ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr(),"//Contract",
        		ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr(),"P/a/y",
        		ldaTwrl9415.getPay_Twrpymnt_Payee_Cde(),"//Res",
        		ldaTwrl9415.getPay_Twrpymnt_Residency_Code(),"//RACF",
        		ldaTwrl9415.getPay_Twrpymnt_Updte_User().getValue(pnd_Ws_Pnd_I),"/Pymnt/Date",
        		ldaTwrl9415.getPay_Twrpymnt_Pymnt_Dte().getValue(pnd_Ws_Pnd_I),"/Srce/Orig",
        		ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_Ws_Pnd_I),"/Srce/Updt",
        		ldaTwrl9415.getPay_Twrpymnt_Updte_Srce_Cde().getValue(pnd_Ws_Pnd_I),"DCI-DPI/& Group/Interest",
        		ldaTwrl9415.getPay_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask 
            ("-Z,ZZZ,ZZ9.99"),"/Backup/Withholding",
        		ldaTwrl9415.getPay_Twrpymnt_Int_Bkup_Wthld().getValue(pnd_Ws_Pnd_I), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new 
            ReportEditMask ("-Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Int_Amt.setValueEdited(ldaTwrl9415.getPay_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I),new ReportEditMask("9999999.99"));                                 //Natural: MOVE EDITED PAY.TWRPYMNT-INT-AMT ( #I ) ( EM = 9999999.99 ) TO #WS.#INT-AMT
        pnd_Gt_Pnd_Gt_Oth_Amt.nadd(ldaTwrl9415.getPay_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                                                         //Natural: ADD PAY.TWRPYMNT-INT-AMT ( #I ) TO #GT.#GT-OTH-AMT
        pnd_Gt_Pnd_Gt_Oth_Bkup_Amt.nadd(ldaTwrl9415.getPay_Twrpymnt_Int_Bkup_Wthld().getValue(pnd_Ws_Pnd_I));                                                             //Natural: ADD PAY.TWRPYMNT-INT-BKUP-WTHLD ( #I ) TO #GT.#GT-OTH-BKUP-AMT
        getWorkFiles().write(3, false, ldaTwrl9415.getPay_Twrpymnt_Tax_Year(), ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr(), ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr(),      //Natural: WRITE WORK FILE 3 PAY.TWRPYMNT-TAX-YEAR PAY.TWRPYMNT-TAX-ID-NBR PAY.TWRPYMNT-CONTRACT-NBR PAY.TWRPYMNT-PAYEE-CDE PAY.TWRPYMNT-UPDTE-USER ( #I ) PAY.TWRPYMNT-PYMNT-DTE ( #I ) #WS.#INT-AMT
            ldaTwrl9415.getPay_Twrpymnt_Payee_Cde(), ldaTwrl9415.getPay_Twrpymnt_Updte_User().getValue(pnd_Ws_Pnd_I), ldaTwrl9415.getPay_Twrpymnt_Pymnt_Dte().getValue(pnd_Ws_Pnd_I), 
            pnd_Ws_Pnd_Int_Amt);
        //*  PROCESS-DCI-DPI-GRP-INT                  /* 11-02-2004
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventRd_Pay() throws Exception {atBreakEventRd_Pay(false);}
    private void atBreakEventRd_Pay(boolean endOfData) throws Exception
    {
        boolean ldaTwrl9415_getPay_Twrpymnt_Form_Sd_2IsBreak = ldaTwrl9415.getPay_Twrpymnt_Form_Sd_2().isBreak(endOfData);
        if (condition(ldaTwrl9415_getPay_Twrpymnt_Form_Sd_2IsBreak))
        {
            Global.format("IS=OFF");                                                                                                                                      //Natural: SUSPEND IDENTICAL SUPPRESS ( 1 )
            Global.format("IS=OFF");                                                                                                                                      //Natural: SUSPEND IDENTICAL SUPPRESS ( 2 )
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=133 ZP=ON");
        Global.format(2, "PS=58 LS=133 ZP=ON");
        Global.format(3, "PS=58 LS=133 ZP=ON");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(48),pnd_Ws_Pnd_Tax_Year, new SignPosition (false),"Ins Cash Surr, Mat Endowments Cases",new TabSetting(120),"Report: RPT1",NEWLINE,
            NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(54),pnd_Ws_Pnd_Tax_Year, new SignPosition (false),"Insurance Interest",new TabSetting(120),"Report: RPT2",NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(50),pnd_Ws_Pnd_Tax_Year, new SignPosition (false),"DCI, DPI & Group Interest",new TabSetting(120),"Report: RPT3",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new ReportEmptyLineSuppression(true),"/Tax/Year",
        		ldaTwrl9415.getPay_Twrpymnt_Tax_Year(), new ReportEditMask ("9999"),"/Co/mp",
        		ldaTwrl9415.getPay_Twrpymnt_Company_Cde(),"//TIN",
        		ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr(),"//Contract",
        		ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr(),"P/a/y",
        		ldaTwrl9415.getPay_Twrpymnt_Payee_Cde(),"//Res",
        		ldaTwrl9415.getPay_Twrpymnt_Residency_Code(),"/Di/st",
        		ldaTwrl9415.getPay_Twrpymnt_Distribution_Cde(),"//RACF",
        		ldaTwrl9415.getPay_Twrpymnt_Updte_User(),"/Pymnt/Date",
        		ldaTwrl9415.getPay_Twrpymnt_Pymnt_Dte(),"/Srce/Orig",
        		ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde(),"/Srce/Updt",
        		ldaTwrl9415.getPay_Twrpymnt_Updte_Srce_Cde(),"/IVC/Ind",
        		ldaTwrl9415.getPay_Twrpymnt_Ivc_Ind(),"//Gross",
        		ldaTwrl9415.getPay_Twrpymnt_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),
            "//Interest",
        		ldaTwrl9415.getPay_Twrpymnt_Int_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-Z,ZZZ,ZZ9.99"),
            "/IVC/Amt",
        		ldaTwrl9415.getPay_Twrpymnt_Ivc_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-Z,ZZZ,ZZ9.99"),
            "/Fed Wthld/Tax Amt",
        		ldaTwrl9415.getPay_Twrpymnt_Fed_Wthld_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(2, new ReportEmptyLineSuppression(true),"/Tax/Year",
        		ldaTwrl9415.getPay_Twrpymnt_Tax_Year(), new ReportEditMask ("9999"),"/Co/mp",
        		ldaTwrl9415.getPay_Twrpymnt_Company_Cde(),"//TIN",
        		ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr(),"//Contract",
        		ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr(),"P/a/y",
        		ldaTwrl9415.getPay_Twrpymnt_Payee_Cde(),"//Res",
        		ldaTwrl9415.getPay_Twrpymnt_Residency_Code(),"//RACF",
        		ldaTwrl9415.getPay_Twrpymnt_Updte_User(),"/Pymnt/Date",
        		ldaTwrl9415.getPay_Twrpymnt_Pymnt_Dte(),"/Srce/Orig",
        		ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde(),"/Srce/Updt",
        		ldaTwrl9415.getPay_Twrpymnt_Updte_Srce_Cde(),"/Insur/Interest",
        		ldaTwrl9415.getPay_Twrpymnt_Int_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-Z,ZZZ,ZZ9.99"),
            "/Backup/Withholding",
        		ldaTwrl9415.getPay_Twrpymnt_Int_Bkup_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(3, new ReportEmptyLineSuppression(true),"/Tax/Year",
        		ldaTwrl9415.getPay_Twrpymnt_Tax_Year(), new ReportEditMask ("9999"),"/Co/mp",
        		ldaTwrl9415.getPay_Twrpymnt_Company_Cde(),"//TIN",
        		ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr(),"//Contract",
        		ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr(),"P/a/y",
        		ldaTwrl9415.getPay_Twrpymnt_Payee_Cde(),"//Res",
        		ldaTwrl9415.getPay_Twrpymnt_Residency_Code(),"//RACF",
        		ldaTwrl9415.getPay_Twrpymnt_Updte_User(),"/Pymnt/Date",
        		ldaTwrl9415.getPay_Twrpymnt_Pymnt_Dte(),"/Srce/Orig",
        		ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde(),"/Srce/Updt",
        		ldaTwrl9415.getPay_Twrpymnt_Updte_Srce_Cde(),"DCI-DPI/& Group/Interest",
        		ldaTwrl9415.getPay_Twrpymnt_Int_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-Z,ZZZ,ZZ9.99"),
            "/Backup/Withholding",
        		ldaTwrl9415.getPay_Twrpymnt_Int_Bkup_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
    }
}
