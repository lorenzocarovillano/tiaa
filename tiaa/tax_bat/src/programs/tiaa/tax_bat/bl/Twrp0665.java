/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:30:50 PM
**        * FROM NATURAL PROGRAM : Twrp0665
************************************************************
**        * FILE NAME            : Twrp0665.java
**        * CLASS NAME           : Twrp0665
**        * INSTANCE NAME        : Twrp0665
************************************************************
***********************************************************************
** PROGRAM NAME:  TWRP0665
** SYSTEM      :  TAX REPORTING SYSTEM
** AUTHOR      :  ANATOLY
** PURPOSE     :  TRANSACTION FILE EDIT\VALIDATION
**             :  REFORMAT
** INPUT       :  1.TRANSACTION FILE EXTRACT FROM FEEDERS
**             :    (DETAIL)
**             :  2.THE CONTROL RECORDS EXTRACT
**             :
** OUTPUT      :  3.TRANSACTION FILE EXTRACT FROM FEEDERS
**             :    (SUMMARY)
** DATE        :  09/09/98
**
** UPDATES -
**
** 09/09/98 - RIAD LOUTFI - UPDATED TO PROCESS THE LAST ORIGIN CODE
**            SUMMARY RECORD, AND TO PROCESS THE CONTROL RECORDS
**            EXTRACT TO POPULATE FROM/TO DATES, AND TAX YEAR FIELDS.
**
** 09/11/98 - ANATOLY     - UPDATED TO PROCESS CORRECTLY THE LAST
**            SUMMARY RECORD.
**
** 10/15/98 - ANATOLY     - INCREASED THE FORMAT OF SUMMARIZED FIELDS.
**
** 11/18/03 - ROSE MA     - SI/SSS-AUTOMATION PROJECT
**                          CORRECT ERROR TO WRITE OUT COMP CODES OTHER
**                          THAN 'T' TO SUMMARY RECORD. SO TO FIX
**                          PROBLEM OF MISSING SUMMARY REPORTS IN
**                          TWRP0700.
** 09/20/04 - MS          - TRUST COMPANY FOR TOPS.
** 05/07/08 - ROSE MA     - ROTH 401K/403B PROJECT
**                          RECOMPILED DUE TO UPDATED TWRL0700.
** 10/17/11 - R CARREON   - NON ERISA TDA CHANGES
** 01/30/12 - R CARREON   - SUNY/CUNY
** 04/10/17 - J BREMER    - STOW FOR PIN EXP TWRL0700
**---------------------------------------------------------------------
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0665 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0600 ldaTwrl0600;
    private LdaTwrl0700 ldaTwrl0700;
    private LdaTwrl0710 ldaTwrl0710;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Comp_Code;
    private DbsField pnd_Twrt_Gross_A;
    private DbsField pnd_Twrt_Ivc_A;
    private DbsField pnd_Twrt_Int_A;
    private DbsField pnd_Twrt_Fed_A;
    private DbsField pnd_Twrt_Nra_A;
    private DbsField pnd_Twrt_State_A;
    private DbsField pnd_Twrt_Local_A;
    private DbsField pnd_Twrt_Can_A;
    private DbsField pnd_Tbl_Origin_Code;
    private DbsField pnd_Tbl_From_Ccyymmdd;
    private DbsField pnd_Tbl_To_Ccyymmdd;
    private DbsField pnd_Tbl_Tax_Year_Ccyy;
    private DbsField pnd_Tbl_Interface_Dte;
    private DbsField pnd_I;
    private DbsField pnd_J;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Twrt_Company_CdeOld;
    private DbsField readWork01Twrt_Company_CdeNcount360;
    private DbsField readWork01Twrt_Company_CdeNcount;
    private DbsField readWork01Pnd_Twrt_Gross_ASum360;
    private DbsField readWork01Pnd_Twrt_Gross_ASum;
    private DbsField readWork01Pnd_Twrt_Ivc_ASum360;
    private DbsField readWork01Pnd_Twrt_Ivc_ASum;
    private DbsField readWork01Pnd_Twrt_Int_ASum360;
    private DbsField readWork01Pnd_Twrt_Int_ASum;
    private DbsField readWork01Pnd_Twrt_Fed_ASum360;
    private DbsField readWork01Pnd_Twrt_Fed_ASum;
    private DbsField readWork01Pnd_Twrt_Nra_ASum360;
    private DbsField readWork01Pnd_Twrt_Nra_ASum;
    private DbsField readWork01Pnd_Twrt_State_ASum360;
    private DbsField readWork01Pnd_Twrt_State_ASum;
    private DbsField readWork01Pnd_Twrt_Local_ASum360;
    private DbsField readWork01Pnd_Twrt_Local_ASum;
    private DbsField readWork01Pnd_Twrt_Can_ASum360;
    private DbsField readWork01Pnd_Twrt_Can_ASum;
    private DbsField readWork01Twrt_SourceOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);
        ldaTwrl0700 = new LdaTwrl0700();
        registerRecord(ldaTwrl0700);
        ldaTwrl0710 = new LdaTwrl0710();
        registerRecord(ldaTwrl0710);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Comp_Code = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Comp_Code", "#COMP-CODE", FieldType.STRING, 1);
        pnd_Twrt_Gross_A = localVariables.newFieldInRecord("pnd_Twrt_Gross_A", "#TWRT-GROSS-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Twrt_Ivc_A = localVariables.newFieldInRecord("pnd_Twrt_Ivc_A", "#TWRT-IVC-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Twrt_Int_A = localVariables.newFieldInRecord("pnd_Twrt_Int_A", "#TWRT-INT-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Twrt_Fed_A = localVariables.newFieldInRecord("pnd_Twrt_Fed_A", "#TWRT-FED-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Twrt_Nra_A = localVariables.newFieldInRecord("pnd_Twrt_Nra_A", "#TWRT-NRA-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Twrt_State_A = localVariables.newFieldInRecord("pnd_Twrt_State_A", "#TWRT-STATE-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Twrt_Local_A = localVariables.newFieldInRecord("pnd_Twrt_Local_A", "#TWRT-LOCAL-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Twrt_Can_A = localVariables.newFieldInRecord("pnd_Twrt_Can_A", "#TWRT-CAN-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tbl_Origin_Code = localVariables.newFieldArrayInRecord("pnd_Tbl_Origin_Code", "#TBL-ORIGIN-CODE", FieldType.STRING, 2, new DbsArrayController(1, 
            100));
        pnd_Tbl_From_Ccyymmdd = localVariables.newFieldArrayInRecord("pnd_Tbl_From_Ccyymmdd", "#TBL-FROM-CCYYMMDD", FieldType.STRING, 8, new DbsArrayController(1, 
            100));
        pnd_Tbl_To_Ccyymmdd = localVariables.newFieldArrayInRecord("pnd_Tbl_To_Ccyymmdd", "#TBL-TO-CCYYMMDD", FieldType.STRING, 8, new DbsArrayController(1, 
            100));
        pnd_Tbl_Tax_Year_Ccyy = localVariables.newFieldArrayInRecord("pnd_Tbl_Tax_Year_Ccyy", "#TBL-TAX-YEAR-CCYY", FieldType.STRING, 4, new DbsArrayController(1, 
            100));
        pnd_Tbl_Interface_Dte = localVariables.newFieldArrayInRecord("pnd_Tbl_Interface_Dte", "#TBL-INTERFACE-DTE", FieldType.STRING, 8, new DbsArrayController(1, 
            100));
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 7);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Twrt_Company_CdeOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Company_Cde_OLD", "Twrt_Company_Cde_OLD", FieldType.STRING, 
            1);
        readWork01Twrt_Company_CdeNcount360 = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Company_Cde_NCOUNT_360", "Twrt_Company_Cde_NCOUNT_360", 
            FieldType.STRING, 1);
        readWork01Twrt_Company_CdeNcount = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Company_Cde_NCOUNT", "Twrt_Company_Cde_NCOUNT", FieldType.STRING, 
            1);
        readWork01Pnd_Twrt_Gross_ASum360 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Gross_A_SUM_360", "Pnd_Twrt_Gross_A_SUM_360", FieldType.PACKED_DECIMAL, 
            13, 2);
        readWork01Pnd_Twrt_Gross_ASum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Gross_A_SUM", "Pnd_Twrt_Gross_A_SUM", FieldType.PACKED_DECIMAL, 
            13, 2);
        readWork01Pnd_Twrt_Ivc_ASum360 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Ivc_A_SUM_360", "Pnd_Twrt_Ivc_A_SUM_360", FieldType.PACKED_DECIMAL, 
            13, 2);
        readWork01Pnd_Twrt_Ivc_ASum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Ivc_A_SUM", "Pnd_Twrt_Ivc_A_SUM", FieldType.PACKED_DECIMAL, 
            13, 2);
        readWork01Pnd_Twrt_Int_ASum360 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Int_A_SUM_360", "Pnd_Twrt_Int_A_SUM_360", FieldType.PACKED_DECIMAL, 
            13, 2);
        readWork01Pnd_Twrt_Int_ASum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Int_A_SUM", "Pnd_Twrt_Int_A_SUM", FieldType.PACKED_DECIMAL, 
            13, 2);
        readWork01Pnd_Twrt_Fed_ASum360 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Fed_A_SUM_360", "Pnd_Twrt_Fed_A_SUM_360", FieldType.PACKED_DECIMAL, 
            13, 2);
        readWork01Pnd_Twrt_Fed_ASum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Fed_A_SUM", "Pnd_Twrt_Fed_A_SUM", FieldType.PACKED_DECIMAL, 
            13, 2);
        readWork01Pnd_Twrt_Nra_ASum360 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Nra_A_SUM_360", "Pnd_Twrt_Nra_A_SUM_360", FieldType.PACKED_DECIMAL, 
            13, 2);
        readWork01Pnd_Twrt_Nra_ASum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Nra_A_SUM", "Pnd_Twrt_Nra_A_SUM", FieldType.PACKED_DECIMAL, 
            13, 2);
        readWork01Pnd_Twrt_State_ASum360 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_State_A_SUM_360", "Pnd_Twrt_State_A_SUM_360", FieldType.PACKED_DECIMAL, 
            13, 2);
        readWork01Pnd_Twrt_State_ASum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_State_A_SUM", "Pnd_Twrt_State_A_SUM", FieldType.PACKED_DECIMAL, 
            13, 2);
        readWork01Pnd_Twrt_Local_ASum360 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Local_A_SUM_360", "Pnd_Twrt_Local_A_SUM_360", FieldType.PACKED_DECIMAL, 
            13, 2);
        readWork01Pnd_Twrt_Local_ASum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Local_A_SUM", "Pnd_Twrt_Local_A_SUM", FieldType.PACKED_DECIMAL, 
            13, 2);
        readWork01Pnd_Twrt_Can_ASum360 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Can_A_SUM_360", "Pnd_Twrt_Can_A_SUM_360", FieldType.PACKED_DECIMAL, 
            13, 2);
        readWork01Pnd_Twrt_Can_ASum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Can_A_SUM", "Pnd_Twrt_Can_A_SUM", FieldType.PACKED_DECIMAL, 
            13, 2);
        readWork01Twrt_SourceOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Source_OLD", "Twrt_Source_OLD", FieldType.STRING, 2);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaTwrl0600.initializeValues();
        ldaTwrl0700.initializeValues();
        ldaTwrl0710.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0665() throws Exception
    {
        super("Twrp0665");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                                                                                                                                                                          //Natural: PERFORM LOAD-CONTROL-RECS-TABLE
        sub_Load_Control_Recs_Table();
        if (condition(Global.isEscape())) {return;}
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_Z().setValue("Z");                                                                                                             //Natural: ASSIGN #Z := 'Z'
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 01 RECORD TWRT-RECORD
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaTwrl0700.getTwrt_Record())))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF TWRT-COMPANY-CDE;//Natural: AT BREAK OF TWRT-SOURCE
            pnd_Twrt_Gross_A.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                                                                                       //Natural: ASSIGN #TWRT-GROSS-A := TWRT-GROSS-AMT
            pnd_Twrt_Ivc_A.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt());                                                                                           //Natural: ASSIGN #TWRT-IVC-A := TWRT-IVC-AMT
            pnd_Twrt_Int_A.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt());                                                                                      //Natural: ASSIGN #TWRT-INT-A := TWRT-INTEREST-AMT
            pnd_Twrt_Fed_A.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt());                                                                                     //Natural: ASSIGN #TWRT-FED-A := TWRT-FED-WHHLD-AMT
            pnd_Twrt_Nra_A.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt());                                                                                     //Natural: ASSIGN #TWRT-NRA-A := TWRT-NRA-WHHLD-AMT
            pnd_Twrt_State_A.setValue(ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt());                                                                                 //Natural: ASSIGN #TWRT-STATE-A := TWRT-STATE-WHHLD-AMT
            pnd_Twrt_Local_A.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt());                                                                                 //Natural: ASSIGN #TWRT-LOCAL-A := TWRT-LOCAL-WHHLD-AMT
            pnd_Twrt_Can_A.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Amt());                                                                                     //Natural: ASSIGN #TWRT-CAN-A := TWRT-CAN-WHHLD-AMT
            readWork01Twrt_Company_CdeOld.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde());                                                                        //Natural: END-WORK
            readWork01Pnd_Twrt_Gross_ASum360.nadd(readWork01Pnd_Twrt_Gross_ASum360,pnd_Twrt_Gross_A);
            readWork01Pnd_Twrt_Gross_ASum.nadd(readWork01Pnd_Twrt_Gross_ASum,pnd_Twrt_Gross_A);
            readWork01Pnd_Twrt_Ivc_ASum360.nadd(readWork01Pnd_Twrt_Ivc_ASum360,pnd_Twrt_Ivc_A);
            readWork01Pnd_Twrt_Ivc_ASum.nadd(readWork01Pnd_Twrt_Ivc_ASum,pnd_Twrt_Ivc_A);
            readWork01Pnd_Twrt_Int_ASum360.nadd(readWork01Pnd_Twrt_Int_ASum360,pnd_Twrt_Int_A);
            readWork01Pnd_Twrt_Int_ASum.nadd(readWork01Pnd_Twrt_Int_ASum,pnd_Twrt_Int_A);
            readWork01Pnd_Twrt_Fed_ASum360.nadd(readWork01Pnd_Twrt_Fed_ASum360,pnd_Twrt_Fed_A);
            readWork01Pnd_Twrt_Fed_ASum.nadd(readWork01Pnd_Twrt_Fed_ASum,pnd_Twrt_Fed_A);
            readWork01Pnd_Twrt_Nra_ASum360.nadd(readWork01Pnd_Twrt_Nra_ASum360,pnd_Twrt_Nra_A);
            readWork01Pnd_Twrt_Nra_ASum.nadd(readWork01Pnd_Twrt_Nra_ASum,pnd_Twrt_Nra_A);
            readWork01Pnd_Twrt_State_ASum360.nadd(readWork01Pnd_Twrt_State_ASum360,pnd_Twrt_State_A);
            readWork01Pnd_Twrt_State_ASum.nadd(readWork01Pnd_Twrt_State_ASum,pnd_Twrt_State_A);
            readWork01Pnd_Twrt_Local_ASum360.nadd(readWork01Pnd_Twrt_Local_ASum360,pnd_Twrt_Local_A);
            readWork01Pnd_Twrt_Local_ASum.nadd(readWork01Pnd_Twrt_Local_ASum,pnd_Twrt_Local_A);
            readWork01Pnd_Twrt_Can_ASum360.nadd(readWork01Pnd_Twrt_Can_ASum360,pnd_Twrt_Can_A);
            readWork01Pnd_Twrt_Can_ASum.nadd(readWork01Pnd_Twrt_Can_ASum,pnd_Twrt_Can_A);
            readWork01Twrt_SourceOld.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Source());
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            readWork01Twrt_Company_CdeNcount360.resetBreak();
            readWork01Twrt_Company_CdeNcount.resetBreak();
            readWork01Twrt_Company_CdeNcount360.resetBreak();
            readWork01Twrt_Company_CdeNcount360.resetBreak();
            readWork01Twrt_Company_CdeNcount360.resetBreak();
            readWork01Twrt_Company_CdeNcount360.resetBreak();
            readWork01Twrt_Company_CdeNcount360.resetBreak();
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        //* *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUMMARY-CONTROL-RECORD
        //* *********************************************
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-CONTROL-RECS-TABLE
    }
    private void sub_Write_Summary_Control_Record() throws Exception                                                                                                      //Natural: WRITE-SUMMARY-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #J = 1 TO #I
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_I)); pnd_J.nadd(1))
        {
            if (condition(ldaTwrl0710.getPnd_Inp_Summary_Pnd_Source_Code().equals(pnd_Tbl_Origin_Code.getValue(pnd_J))))                                                  //Natural: IF #SOURCE-CODE = #TBL-ORIGIN-CODE ( #J )
            {
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_From_Dte().setValue(pnd_Tbl_From_Ccyymmdd.getValue(pnd_J));                                                            //Natural: ASSIGN #FROM-DTE := #TBL-FROM-CCYYMMDD ( #J )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_To_Dte().setValue(pnd_Tbl_To_Ccyymmdd.getValue(pnd_J));                                                                //Natural: ASSIGN #TO-DTE := #TBL-TO-CCYYMMDD ( #J )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_Tx_Year().setValue(pnd_Tbl_Tax_Year_Ccyy.getValue(pnd_J));                                                             //Natural: ASSIGN #TX-YEAR := #TBL-TAX-YEAR-CCYY ( #J )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_Interface_Dte().setValue(pnd_Tbl_Interface_Dte.getValue(pnd_J));                                                       //Natural: ASSIGN #INTERFACE-DTE := #TBL-INTERFACE-DTE ( #J )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getWorkFiles().write(3, false, ldaTwrl0710.getPnd_Inp_Summary());                                                                                                 //Natural: WRITE WORK FILE 03 #INP-SUMMARY
        ldaTwrl0710.getPnd_Inp_Summary().reset();                                                                                                                         //Natural: RESET #INP-SUMMARY
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_Z().setValue("Z");                                                                                                             //Natural: ASSIGN #Z := 'Z'
    }
    private void sub_Load_Control_Recs_Table() throws Exception                                                                                                           //Natural: LOAD-CONTROL-RECS-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 02 #TWRP0600-CONTROL-RECORD
        while (condition(getWorkFiles().read(2, ldaTwrl0600.getPnd_Twrp0600_Control_Record())))
        {
            pnd_I.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #I
            pnd_Tbl_Origin_Code.getValue(pnd_I).setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code());                                          //Natural: ASSIGN #TBL-ORIGIN-CODE ( #I ) := #TWRP0600-ORIGIN-CODE
            pnd_Tbl_From_Ccyymmdd.getValue(pnd_I).setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd());                                      //Natural: ASSIGN #TBL-FROM-CCYYMMDD ( #I ) := #TWRP0600-FROM-CCYYMMDD
            pnd_Tbl_To_Ccyymmdd.getValue(pnd_I).setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd());                                          //Natural: ASSIGN #TBL-TO-CCYYMMDD ( #I ) := #TWRP0600-TO-CCYYMMDD
            pnd_Tbl_Tax_Year_Ccyy.getValue(pnd_I).setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy());                                      //Natural: ASSIGN #TBL-TAX-YEAR-CCYY ( #I ) := #TWRP0600-TAX-YEAR-CCYY
            pnd_Tbl_Interface_Dte.getValue(pnd_I).setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd());                                 //Natural: ASSIGN #TBL-INTERFACE-DTE ( #I ) := #TWRP0600-INTERFACE-CCYYMMDD
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaTwrl0700_getTwrt_Record_Twrt_Company_CdeIsBreak = ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().isBreak(endOfData);
        boolean ldaTwrl0700_getTwrt_Record_Twrt_SourceIsBreak = ldaTwrl0700.getTwrt_Record_Twrt_Source().isBreak(endOfData);
        if (condition(ldaTwrl0700_getTwrt_Record_Twrt_Company_CdeIsBreak || ldaTwrl0700_getTwrt_Record_Twrt_SourceIsBreak))
        {
            pnd_Ws_Pnd_Comp_Code.setValue(readWork01Twrt_Company_CdeOld);                                                                                                 //Natural: ASSIGN #WS.#COMP-CODE := OLD ( TWRT-COMPANY-CDE )
            short decideConditionsMet362 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WS.#COMP-CODE;//Natural: VALUE 'C'
            if (condition((pnd_Ws_Pnd_Comp_Code.equals("C"))))
            {
                decideConditionsMet362++;
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Comp_Code().setValue("C");                                                                                           //Natural: ASSIGN #C-COMP-CODE := 'C'
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Rec_Cnt().setValue(readWork01Twrt_Company_CdeNcount360);                                                             //Natural: ASSIGN #C-REC-CNT := NCOUNT ( TWRT-COMPANY-CDE )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Gross_A().setValue(readWork01Pnd_Twrt_Gross_ASum360);                                                                //Natural: ASSIGN #C-GROSS-A := SUM ( #TWRT-GROSS-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Ivc_A().setValue(readWork01Pnd_Twrt_Ivc_ASum360);                                                                    //Natural: ASSIGN #C-IVC-A := SUM ( #TWRT-IVC-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Int_A().setValue(readWork01Pnd_Twrt_Int_ASum360);                                                                    //Natural: ASSIGN #C-INT-A := SUM ( #TWRT-INT-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Fed_A().setValue(readWork01Pnd_Twrt_Fed_ASum360);                                                                    //Natural: ASSIGN #C-FED-A := SUM ( #TWRT-FED-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Nra_A().setValue(readWork01Pnd_Twrt_Nra_ASum360);                                                                    //Natural: ASSIGN #C-NRA-A := SUM ( #TWRT-NRA-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_State_A().setValue(readWork01Pnd_Twrt_State_ASum360);                                                                //Natural: ASSIGN #C-STATE-A := SUM ( #TWRT-STATE-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Local_A().setValue(readWork01Pnd_Twrt_Local_ASum360);                                                                //Natural: ASSIGN #C-LOCAL-A := SUM ( #TWRT-LOCAL-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Canadian_A().setValue(readWork01Pnd_Twrt_Can_ASum360);                                                               //Natural: ASSIGN #C-CANADIAN-A := SUM ( #TWRT-CAN-A )
            }                                                                                                                                                             //Natural: VALUE 'L'
            else if (condition((pnd_Ws_Pnd_Comp_Code.equals("L"))))
            {
                decideConditionsMet362++;
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Comp_Code().setValue("L");                                                                                           //Natural: ASSIGN #L-COMP-CODE := 'L'
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Rec_Cnt().setValue(readWork01Twrt_Company_CdeNcount360);                                                             //Natural: ASSIGN #L-REC-CNT := NCOUNT ( TWRT-COMPANY-CDE )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Gross_A().setValue(readWork01Pnd_Twrt_Gross_ASum360);                                                                //Natural: ASSIGN #L-GROSS-A := SUM ( #TWRT-GROSS-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Ivc_A().setValue(readWork01Pnd_Twrt_Ivc_ASum360);                                                                    //Natural: ASSIGN #L-IVC-A := SUM ( #TWRT-IVC-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Int_A().setValue(readWork01Pnd_Twrt_Int_ASum360);                                                                    //Natural: ASSIGN #L-INT-A := SUM ( #TWRT-INT-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Fed_A().setValue(readWork01Pnd_Twrt_Fed_ASum360);                                                                    //Natural: ASSIGN #L-FED-A := SUM ( #TWRT-FED-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Nra_A().setValue(readWork01Pnd_Twrt_Nra_ASum360);                                                                    //Natural: ASSIGN #L-NRA-A := SUM ( #TWRT-NRA-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_State_A().setValue(readWork01Pnd_Twrt_State_ASum360);                                                                //Natural: ASSIGN #L-STATE-A := SUM ( #TWRT-STATE-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Local_A().setValue(readWork01Pnd_Twrt_Local_ASum360);                                                                //Natural: ASSIGN #L-LOCAL-A := SUM ( #TWRT-LOCAL-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Canadian_A().setValue(readWork01Pnd_Twrt_Can_ASum360);                                                               //Natural: ASSIGN #L-CANADIAN-A := SUM ( #TWRT-CAN-A )
            }                                                                                                                                                             //Natural: VALUE 'S'
            else if (condition((pnd_Ws_Pnd_Comp_Code.equals("S"))))
            {
                decideConditionsMet362++;
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Comp_Code().setValue("S");                                                                                           //Natural: ASSIGN #S-COMP-CODE := 'S'
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Rec_Cnt().setValue(readWork01Twrt_Company_CdeNcount360);                                                             //Natural: ASSIGN #S-REC-CNT := NCOUNT ( TWRT-COMPANY-CDE )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Gross_A().setValue(readWork01Pnd_Twrt_Gross_ASum360);                                                                //Natural: ASSIGN #S-GROSS-A := SUM ( #TWRT-GROSS-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Ivc_A().setValue(readWork01Pnd_Twrt_Ivc_ASum360);                                                                    //Natural: ASSIGN #S-IVC-A := SUM ( #TWRT-IVC-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Int_A().setValue(readWork01Pnd_Twrt_Int_ASum360);                                                                    //Natural: ASSIGN #S-INT-A := SUM ( #TWRT-INT-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Fed_A().setValue(readWork01Pnd_Twrt_Fed_ASum360);                                                                    //Natural: ASSIGN #S-FED-A := SUM ( #TWRT-FED-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Nra_A().setValue(readWork01Pnd_Twrt_Nra_ASum360);                                                                    //Natural: ASSIGN #S-NRA-A := SUM ( #TWRT-NRA-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_State_A().setValue(readWork01Pnd_Twrt_State_ASum360);                                                                //Natural: ASSIGN #S-STATE-A := SUM ( #TWRT-STATE-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Local_A().setValue(readWork01Pnd_Twrt_Local_ASum360);                                                                //Natural: ASSIGN #S-LOCAL-A := SUM ( #TWRT-LOCAL-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Canadian_A().setValue(readWork01Pnd_Twrt_Can_ASum360);                                                               //Natural: ASSIGN #S-CANADIAN-A := SUM ( #TWRT-CAN-A )
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pnd_Ws_Pnd_Comp_Code.equals("T"))))
            {
                decideConditionsMet362++;
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Comp_Code().setValue("T");                                                                                           //Natural: ASSIGN #T-COMP-CODE := 'T'
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Rec_Cnt().setValue(readWork01Twrt_Company_CdeNcount360);                                                             //Natural: ASSIGN #T-REC-CNT := NCOUNT ( TWRT-COMPANY-CDE )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Gross_A().setValue(readWork01Pnd_Twrt_Gross_ASum360);                                                                //Natural: ASSIGN #T-GROSS-A := SUM ( #TWRT-GROSS-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Ivc_A().setValue(readWork01Pnd_Twrt_Ivc_ASum360);                                                                    //Natural: ASSIGN #T-IVC-A := SUM ( #TWRT-IVC-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Int_A().setValue(readWork01Pnd_Twrt_Int_ASum360);                                                                    //Natural: ASSIGN #T-INT-A := SUM ( #TWRT-INT-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Fed_A().setValue(readWork01Pnd_Twrt_Fed_ASum360);                                                                    //Natural: ASSIGN #T-FED-A := SUM ( #TWRT-FED-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Nra_A().setValue(readWork01Pnd_Twrt_Nra_ASum360);                                                                    //Natural: ASSIGN #T-NRA-A := SUM ( #TWRT-NRA-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_State_A().setValue(readWork01Pnd_Twrt_State_ASum360);                                                                //Natural: ASSIGN #T-STATE-A := SUM ( #TWRT-STATE-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Local_A().setValue(readWork01Pnd_Twrt_Local_ASum360);                                                                //Natural: ASSIGN #T-LOCAL-A := SUM ( #TWRT-LOCAL-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Canadian_A().setValue(readWork01Pnd_Twrt_Can_ASum360);                                                               //Natural: ASSIGN #T-CANADIAN-A := SUM ( #TWRT-CAN-A )
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pnd_Ws_Pnd_Comp_Code.equals("X"))))
            {
                decideConditionsMet362++;
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Comp_Code().setValue("X");                                                                                           //Natural: ASSIGN #C-COMP-CODE := 'X'
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Rec_Cnt().setValue(readWork01Twrt_Company_CdeNcount360);                                                             //Natural: ASSIGN #C-REC-CNT := NCOUNT ( TWRT-COMPANY-CDE )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Gross_A().setValue(readWork01Pnd_Twrt_Gross_ASum360);                                                                //Natural: ASSIGN #C-GROSS-A := SUM ( #TWRT-GROSS-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Ivc_A().setValue(readWork01Pnd_Twrt_Ivc_ASum360);                                                                    //Natural: ASSIGN #C-IVC-A := SUM ( #TWRT-IVC-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Int_A().setValue(readWork01Pnd_Twrt_Int_ASum360);                                                                    //Natural: ASSIGN #C-INT-A := SUM ( #TWRT-INT-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Fed_A().setValue(readWork01Pnd_Twrt_Fed_ASum360);                                                                    //Natural: ASSIGN #C-FED-A := SUM ( #TWRT-FED-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Nra_A().setValue(readWork01Pnd_Twrt_Nra_ASum360);                                                                    //Natural: ASSIGN #C-NRA-A := SUM ( #TWRT-NRA-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_State_A().setValue(readWork01Pnd_Twrt_State_ASum360);                                                                //Natural: ASSIGN #C-STATE-A := SUM ( #TWRT-STATE-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Local_A().setValue(readWork01Pnd_Twrt_Local_ASum360);                                                                //Natural: ASSIGN #C-LOCAL-A := SUM ( #TWRT-LOCAL-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Canadian_A().setValue(readWork01Pnd_Twrt_Can_ASum360);                                                               //Natural: ASSIGN #C-CANADIAN-A := SUM ( #TWRT-CAN-A )
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pnd_Ws_Pnd_Comp_Code.equals("F"))))
            {
                decideConditionsMet362++;
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Comp_Code().setValue("F");                                                                                           //Natural: ASSIGN #F-COMP-CODE := 'F'
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Rec_Cnt().setValue(readWork01Twrt_Company_CdeNcount360);                                                             //Natural: ASSIGN #F-REC-CNT := NCOUNT ( TWRT-COMPANY-CDE )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Gross_A().setValue(readWork01Pnd_Twrt_Gross_ASum360);                                                                //Natural: ASSIGN #F-GROSS-A := SUM ( #TWRT-GROSS-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Ivc_A().setValue(readWork01Pnd_Twrt_Ivc_ASum360);                                                                    //Natural: ASSIGN #F-IVC-A := SUM ( #TWRT-IVC-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Int_A().setValue(readWork01Pnd_Twrt_Int_ASum360);                                                                    //Natural: ASSIGN #F-INT-A := SUM ( #TWRT-INT-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Fed_A().setValue(readWork01Pnd_Twrt_Fed_ASum360);                                                                    //Natural: ASSIGN #F-FED-A := SUM ( #TWRT-FED-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Nra_A().setValue(readWork01Pnd_Twrt_Nra_ASum360);                                                                    //Natural: ASSIGN #F-NRA-A := SUM ( #TWRT-NRA-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_State_A().setValue(readWork01Pnd_Twrt_State_ASum360);                                                                //Natural: ASSIGN #F-STATE-A := SUM ( #TWRT-STATE-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Local_A().setValue(readWork01Pnd_Twrt_Local_ASum360);                                                                //Natural: ASSIGN #F-LOCAL-A := SUM ( #TWRT-LOCAL-A )
                ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Canadian_A().setValue(readWork01Pnd_Twrt_Can_ASum360);                                                               //Natural: ASSIGN #F-CANADIAN-A := SUM ( #TWRT-CAN-A )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            readWork01Twrt_Company_CdeNcount360.setDec(new DbsDecimal(0));                                                                                                //Natural: END-BREAK
            readWork01Pnd_Twrt_Gross_ASum360.setDec(new DbsDecimal(0));
            readWork01Pnd_Twrt_Ivc_ASum360.setDec(new DbsDecimal(0));
            readWork01Pnd_Twrt_Int_ASum360.setDec(new DbsDecimal(0));
            readWork01Pnd_Twrt_Fed_ASum360.setDec(new DbsDecimal(0));
            readWork01Pnd_Twrt_Nra_ASum360.setDec(new DbsDecimal(0));
            readWork01Pnd_Twrt_State_ASum360.setDec(new DbsDecimal(0));
            readWork01Pnd_Twrt_Local_ASum360.setDec(new DbsDecimal(0));
            readWork01Pnd_Twrt_Can_ASum360.setDec(new DbsDecimal(0));
        }
        if (condition(ldaTwrl0700_getTwrt_Record_Twrt_SourceIsBreak))
        {
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_Source_Code().setValue(readWork01Twrt_SourceOld);                                                                          //Natural: ASSIGN #SOURCE-CODE := OLD ( TWRT-SOURCE )
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-CONTROL-RECORD
            sub_Write_Summary_Control_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
}
