/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:25:46 PM
**        * FROM NATURAL PROGRAM : Twrp0117
************************************************************
**        * FILE NAME            : Twrp0117.java
**        * CLASS NAME           : Twrp0117
**        * INSTANCE NAME        : Twrp0117
************************************************************
************************************************************************
**
** PROGRAM:  TWRP0117 - FORM SELECTION
** SYSTEM :  TAX WITHHOLDING & REPORTING SYSTEM
** AUTHOR :  A.WILNER
** PURPOSE:  MAGNETIC REPORT TO PUERTO RICO
**                '480.6A'  =  FORM NUMBER 5   <---- REPLACED BY 480.7C
**                '480.6B'  =  FORM NUMBER 6              01/28/08   RM
**
** SPECIAL:  MUST USE INPUT PARAMETERS TO WHICH PROCESSING FORM
**
** HISTORY.....:
** --------------------------------------------------------------------
** 02/13/13 - RC - NEW SU RECORD
** 02/07/13 - RC - PR IS NOW COMPANY TIAA. CHANGE TCII TO TIAA
** 03/05/12 - MB - PROD FIX FOR THE STATE CODE FOR FOREIGN ADDRESSES
** 01/26/12 - MB - REPLACES PROGRAM OLDP0117 WITH TWRP0124
** 02/07/11 - JR - REVISED TO ACCOMMODATE 2010 LAYOUT CHANGES
** 02/09/10 - AY - REVISED TO ACCOMMODATE 2009 LAYOUT CHANGES:
**                 #TWRL117A-CONTROL-NUMBER   (A10) RE-DEFINED
**                   #TWRL117A-FILLER         (A02) SPACES ONLY
**                   #TWRL117A-CONTROL-NBR    (N08) 00008266
**                 #TWRL117A-S-CONTROL-NUMBER (A10) RE-DEFINED
**                   #TWRL117A-S-FILLER       (A02) SPACES ONLY
**                   #TWRL117A-S-CONTROL-NBR  (N08) 00008266
**                 #TWRL117A-TRANS-CODE       (A01) EXCLUDE 'X'
**                 #TWRL117A-PLAN-OF-ANN-TYPE (A01) INCLUDE 'N'
** 02/10/09 - AY - PER USER REQUEST, REVISED CONTROL RECORD FIELD
**                 POSITION 199 FROM 'C' TO 'I'.
** 02/04/09 - AY - REVISED TO ACCOMMODATE 2008 LAYOUT CHANGES:
**                 480.7C FIELD POSITIONS DELETED:  455 -  466
**                                                  467 -  478
**                 480.7C FIELD POSITIONS ADDED:    528 -  539
**                                                  540 -  551
**                 480.5  FIELD POSITIONS ADDED:   2495 - 2500
**               - POPULATED & DISPLAYED #TWRL117A-ROLLOVER-CONTR
**                                       #TWRL117A-ROLLOVER-DISTR
** 06/17/08 - RM - UPDATE CONTROL TABLE FOR FORM 6 WHEN UPDATING FORM 5.
** 01/28/08 - RM - FOR TAX YEAR 2007, FORM 480.7C REPLACES 480.6A.
**                 LDA TWRL117C HAS BEEN CREATED FOR THE NEW FORM.
**                 UPDATE THIS PGM TO ACCORMODATE THE CHANGES. (THE OLD
**                 VERSION HAS BEEN SAVED AS OLDP0117).
**                 ENLARGE WS-TOT FIELDS AND REPORTING FIELDS.
** 02/20/07 - RM - CHANGES WERE DONE FOR 2006 TO ACCORMODATE PR REQUIRE-
**                 MENT OF E-FILE VIA MOBIUS. BUT USER LATER FOUND THAT
**                 WAS NOT APPLICABLE FOR TIAA. THEREFORE, PROCESS AND
**                 PROGRAMS HAD TO BE REVERTED BACK TO 2005 VERSION.
**                 CHANGED MODULES SAVED TO 'TEM*'.
**                 MINOR CHANGES DONE IN TWRL117A AND THIS PGM FOR 2006.
**                 NO CHANGES FOR FORM 480.6B YET.
** 02/01/06 - RM - UPDATE TWRL117A TO ADD NEW FIELDS AND EXPAND RECORD
**                 LENGTH FROM 700 TO 2000, FOR 2005 NEW 480.6A & 480.5
**                 LAYOUT.
**                 CHANGE RELATED CODING TO ACCORMODATE THE NEW OR
**                 CHANGED FIELDS.
**                 ALTHOUGH THERE ARE NO 480.6B FORMS BEING PRODUCED
**                 CURRENTLY, TWRL117B IS UPDATED FOR THE NEW FIELDS
**                 AND NEW RECORD LENGTH.
** 11/11/05 - MS - REMOVE UNUSED LDAS.
** 02/08/05 - AY - REVISED TO POPULATE #TWRACOM2.#TAX-YEAR BEFORE
**                 CALLING COMPANY CODE TABLE.
**               - REVISED TO CALL COMPANY CODE TABLE (ONCE) WITH 'S'
**                 VALUE TO GET AND STORE 'Services' DATA.
**               - REVISED TO PRINT REPORTS WITH FORM FILE COMPANY DATA,
**                 WHILE WRITING OUTPUT DETAIL AND SUMMARY RECORDS
**                 WITH 'S' (SERVICES) DATA.  COMPANY CODE TABLE VALUES
**                 REPLACED HARD-CODING WHEREVER APPLICABLE.
** 02/05/03 - RM - CHANGE FIELD IN 480.5, UPDATE TWRL117A AND CHANGE
**                 COMPANY NAME INDIRECTLY VIA COMPANY TABLE.
** 12/09/02 - EB - RECOMPILED DUE TO FIELDS ADDED TO TWRL117A & 117B,
**                 ALSO RENAMED COLUMN 'Gross' TO 'Pension'.
** 11/19/02 - MN - RECOMPILED DUE TO FIELDS ADDED TO TWRL5001
** 09/10/02  BOTTERI - CHANGE CALL TWRACOMP TO TWRACOM2 & ADD BREAK
**           BY COMPANY CODE AND ACCUMULATORS/DISPLAYS FOR EACH.
** 08/13/02  J.ROTHOLZ - RECOMPILED DUE TO INCREASE IN #COMP-NAME
**           LENGTH IN TWRLCOMP & TWRACOMP
** 02/08/02  JHH - ADD DETAIL REPORT AND TOTALS
** 02/15/01  FELIX ORTIZ
**           TAX YEAR IS NOW AN OPTIONAL INPUT PARAMETER. SYSTEM
**           CALCULATES TAX YEAR WHEN PARAMETER IS NOT PASSED.
**           THIS DATE IS USED TO POPULATE THE OUTPUT FILE.
** 02/02/15  MUKHERR
**           CHANGED THE LAYOUT TWRL117C, TO ACCOMODATE "SU" & "PA"
**           RECORDS. ADDED NEW CODE TO POPULATE PA RECORDS, EARLIER
**           BUSINESS WAS MANUALLY UPDATING THE RECORDS.
** 01/28/2016 - PAULS - CHANGED THE TAXYEAR FROM 2014 TO 2015.
**           ADDED NEW FIELD DOC-TYPE IN PLACE OF TRANS-CODE.
**           CHANGES TAGGED WITH PAULS
** 01/17/17  SINHASN - UPDATE IRS 480.7C LAYOUT (TWRL117C) FOR 2016.
**                   - CHANGED #TWRL117A-FILLER1 IN TWRL117C
**                   - ADDED #TWRL117A-TYPE-ID-PAYEE IN TWRL117C
**                   - CHANGED THE TAXYEAR FROM 2015 TO 2016
**                   - CHANGED PHONE NUMBER & EMAIL ADDRESS
**                   - CHANGED FORM OF DISTRIBUTION POPULATION LOGIC
**                   - CHANGED PLAN OR ANNUITY TYPE POPULATION LOGIC
**                   - PAYEE-NAME POPULATED FOR CORPORATIONS
**                   - PAYEES FIRST NAME POPULATED FOR INDIVIDUALS
**                   - PAYEES LAST  NAME POPULATED FOR INDIVIDUALS
**                                                      TAG - SINHASN
** 2/17/17 RUPOLEENA - COST OF PENSION OR ANNUITY WILL BE RESET FOR
**                     PARTIAL PAYMENTS.
**                     POPULATE EIN,PLAN-NAME AND PLAN-SPONSOR-NAME FOR
**                     FILING WITH IRS.
**                                                      TAG - MUKHERR1
**
** 4/7/2017 - WEBBJ - PIN EXPANSION. RESTOW ONLY
** 01/29/2018 - DASDH1 - 480.7C BOX CHANGES FOR IRS REPORTING
** 01/22/2018 - VIKRAM - 480.7C.1 : NEW LAYOUT ADDED AND
**     CORRESPONDING  CHANGES IN PA ,SU , INFORMATIVE DETAIL AND SUMMARY
**     RECORD FIELDS (LDA TWRL117C) ADDED FOR FEIN=1 . TAG : VIKRAM
** 02/04/2020 - VIKRAM - 480.7C.1 : NEW LAYOUT ADDED AND
**     CORRESPONDING  CHANGES IN INFORMATIVE DETAIL AND SUMMARY
**     RECORD FIELDS (LDA TWRL117C) ADDED FOR RESIDENT=1 . TAG : VIKRAM1
** 02/04/2020 - GHOSHJ - 480.7C   : POPULATE DISASTER RELATED
**     DISTRIBUTIONS.                          /* 480.7C DISASTER
***********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0117 extends BLNatBase
{
    // Data Areas
    private LdaTwrl117c ldaTwrl117c;
    private LdaTwrl9710 ldaTwrl9710;
    private PdaTwracom2 pdaTwracom2;
    private PdaTwratbl2 pdaTwratbl2;
    private PdaTwratbl4 pdaTwratbl4;
    private LdaTwrltb4r ldaTwrltb4r;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_form1;
    private DbsField form1_Tirf_Irs_Rpt_Ind;
    private DbsField form1_Tirf_Irs_Rpt_Date;
    private DbsField form1_Tirf_Lu_User;
    private DbsField form1_Tirf_Lu_Ts;

    private DataAccessProgramView vw_pymnt;
    private DbsField pymnt_Twrpymnt_Tax_Year;
    private DbsField pymnt_Twrpymnt_Tax_Id_Nbr;
    private DbsField pymnt_Twrpymnt_Contract_Nbr;
    private DbsField pymnt_Twrpymnt_Payee_Cde;
    private DbsField pymnt_Count_Casttwrpymnt_Payments;

    private DbsGroup pymnt_Twrpymnt_Payments;
    private DbsField pymnt_Twrpymnt_Payment_Type;
    private DbsField pnd_S_Twrpymnt_Form_Sd;

    private DbsGroup pnd_S_Twrpymnt_Form_Sd__R_Field_1;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde;
    private DbsField pnd_In_Parm;

    private DbsGroup pnd_In_Parm__R_Field_2;
    private DbsField pnd_In_Parm_Pnd_In_Active_Ind;
    private DbsField pnd_In_Parm_Pnd_In_Form_Type;
    private DbsField pnd_In_Parm_Pnd_In_Inc_Old;
    private DbsField pnd_In_Parm_Pnd_In_Update;
    private DbsField pnd_In_Parm_Pnd_In_Tax_Year;

    private DbsGroup pnd_In_Parm__R_Field_3;
    private DbsField pnd_In_Parm_Pnd_In_Tax_Yearn;
    private DbsField pnd_Tirf_Superde_3;

    private DbsGroup pnd_Tirf_Superde_3__R_Field_4;
    private DbsField pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year;

    private DbsGroup pnd_Tirf_Superde_3__R_Field_5;
    private DbsField pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn;
    private DbsField pnd_Tirf_Superde_3_Pnd_Tirf_Active_Ind;
    private DbsField pnd_Tirf_Superde_3_Pnd_Tirf_Form_Type;

    private DbsGroup pnd_Work_Area;
    private DbsField pnd_Work_Area_Pnd_Ws_Et;
    private DbsField pnd_Work_Area_Pnd_Ws_Isn;
    private DbsField pnd_Work_Area_Pnd_Temp_Tax_Year;

    private DbsGroup pnd_Work_Area__R_Field_6;
    private DbsField pnd_Work_Area_Pnd_Temp_Tax_Yearcc;
    private DbsField pnd_Work_Area_Pnd_Temp_Tax_Yearyy;
    private DbsField pnd_Work_Area_Pnd_Temp_Cntl_Num;
    private DbsField pnd_Work_Area_Pnd_Ws_Num_Of_Doc;
    private DbsField pnd_Work_Area_Pnd_Ws_Amt_Withld;
    private DbsField pnd_Work_Area_Pnd_Ws_Amout_Paid;
    private DbsField pnd_Work_Area_Pnd_Ws_Amount;
    private DbsField pnd_Work_Area_Pnd_Empty_Form_Cnt;
    private DbsField pnd_Work_Area_Pnd_Reject;
    private DbsField pnd_Work_Area_Pnd_Rej_Count;
    private DbsField pnd_Work_Area_Pnd_Old_Cmpny;
    private DbsField pnd_Work_Area_Pnd_Counter;
    private DbsField pnd_Work_Area_Pnd_Cmpnys;
    private DbsField pnd_Work_Area_Pnd_I;
    private DbsField pnd_Work_Area_Pnd_First;
    private DbsField pnd_Work_Area_Pnd_Last;

    private DbsGroup pnd_Work_Area_Pnd_Ws_Totales;
    private DbsField pnd_Work_Area_Pnd_Ws_Cnt_Tot;
    private DbsField pnd_Work_Area_Pnd_Ws_Gross_Tot;
    private DbsField pnd_Work_Area_Pnd_Ws_Int_Tot;
    private DbsField pnd_Work_Area_Pnd_Ws_Comb_Tot;
    private DbsField pnd_Work_Area_Pnd_Ws_Roll_Cont_Tot;
    private DbsField pnd_Work_Area_Pnd_Ws_Roll_Dist_Tot;
    private DbsField pnd_Work_Area_Pnd_Ws_Cnt;
    private DbsField pnd_Work_Area_Pnd_Ws_Gross;
    private DbsField pnd_Work_Area_Pnd_Ws_Int;
    private DbsField pnd_Work_Area_Pnd_Ws_Roll_Cont;
    private DbsField pnd_Work_Area_Pnd_Ws_Roll_Dist;

    private DbsGroup pnd_S_Output_Data;
    private DbsField pnd_S_Output_Data_Pnd_Ret_Code;
    private DbsField pnd_S_Output_Data_Pnd_Ret_Msg;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Short_Name;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Name;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Corr_Name;
    private DbsField pnd_S_Output_Data_Pnd_Fed_Id;
    private DbsField pnd_S_Output_Data_Pnd_Nr4_Id;
    private DbsField pnd_S_Output_Data_Pnd_Contact_Name;
    private DbsField pnd_S_Output_Data_Pnd_Contact_Email_Addr;
    private DbsField pnd_S_Output_Data_Pnd_Phone;
    private DbsField pnd_S_Output_Data_Pnd_Address;
    private DbsField pnd_S_Output_Data_Pnd_City;
    private DbsField pnd_S_Output_Data_Pnd_State;
    private DbsField pnd_S_Output_Data_Pnd_Zip_9;

    private DbsGroup pnd_S_Output_Data__R_Field_7;
    private DbsField pnd_S_Output_Data_Pnd_Zip;
    private DbsField pnd_S_Output_Data_Pnd_Zip_4;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Tcc;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Trans_A;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Trans_B;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Payer_A;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Payer_B;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Agent_A;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Agent_B;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Retrn_A;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Retrn_B;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Futr1_A;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Futr1_B;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Futr2_A;
    private DbsField pnd_S_Output_Data_Pnd_Comp_Futr2_B;
    private DbsField pnd_Temp_Contact_Phone;

    private DbsGroup pnd_Temp_Contact_Phone__R_Field_8;
    private DbsField pnd_Temp_Contact_Phone_Pnd_Temp_Contact_Phone_N;
    private DbsField pnd_Spaces;
    private DbsField pnd_Maxm_Exempt_Amt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl117c = new LdaTwrl117c();
        registerRecord(ldaTwrl117c);
        ldaTwrl9710 = new LdaTwrl9710();
        registerRecord(ldaTwrl9710);
        registerRecord(ldaTwrl9710.getVw_form());
        localVariables = new DbsRecord();
        pdaTwracom2 = new PdaTwracom2(localVariables);
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        pdaTwratbl4 = new PdaTwratbl4(localVariables);
        ldaTwrltb4r = new LdaTwrltb4r();
        registerRecord(ldaTwrltb4r);
        registerRecord(ldaTwrltb4r.getVw_tircntl_Rpt());

        // Local Variables

        vw_form1 = new DataAccessProgramView(new NameInfo("vw_form1", "FORM1"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form1_Tirf_Irs_Rpt_Ind = vw_form1.getRecord().newFieldInGroup("form1_Tirf_Irs_Rpt_Ind", "TIRF-IRS-RPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_IND");
        form1_Tirf_Irs_Rpt_Ind.setDdmHeader("IRS/RPT/IND");
        form1_Tirf_Irs_Rpt_Date = vw_form1.getRecord().newFieldInGroup("form1_Tirf_Irs_Rpt_Date", "TIRF-IRS-RPT-DATE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_DATE");
        form1_Tirf_Irs_Rpt_Date.setDdmHeader("IRS/RPT/DATE");
        form1_Tirf_Lu_User = vw_form1.getRecord().newFieldInGroup("form1_Tirf_Lu_User", "TIRF-LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_LU_USER");
        form1_Tirf_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        form1_Tirf_Lu_Ts = vw_form1.getRecord().newFieldInGroup("form1_Tirf_Lu_Ts", "TIRF-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "TIRF_LU_TS");
        form1_Tirf_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        registerRecord(vw_form1);

        vw_pymnt = new DataAccessProgramView(new NameInfo("vw_pymnt", "PYMNT"), "TWRPYMNT_PAYMENT_FILE", "TIR_PAYMENT", DdmPeriodicGroups.getInstance().getGroups("TWRPYMNT_PAYMENT_FILE"));
        pymnt_Twrpymnt_Tax_Year = vw_pymnt.getRecord().newFieldInGroup("pymnt_Twrpymnt_Tax_Year", "TWRPYMNT-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TWRPYMNT_TAX_YEAR");
        pymnt_Twrpymnt_Tax_Id_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Twrpymnt_Tax_Id_Nbr", "TWRPYMNT-TAX-ID-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TWRPYMNT_TAX_ID_NBR");
        pymnt_Twrpymnt_Contract_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Twrpymnt_Contract_Nbr", "TWRPYMNT-CONTRACT-NBR", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "TWRPYMNT_CONTRACT_NBR");
        pymnt_Twrpymnt_Payee_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Twrpymnt_Payee_Cde", "TWRPYMNT-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRPYMNT_PAYEE_CDE");
        pymnt_Count_Casttwrpymnt_Payments = vw_pymnt.getRecord().newFieldInGroup("pymnt_Count_Casttwrpymnt_Payments", "C*TWRPYMNT-PAYMENTS", RepeatingFieldStrategy.CAsteriskVariable, 
            "TIR_PAYMENT_TWRPYMNT_PAYMENTS");

        pymnt_Twrpymnt_Payments = vw_pymnt.getRecord().newGroupInGroup("pymnt_Twrpymnt_Payments", "TWRPYMNT-PAYMENTS", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        pymnt_Twrpymnt_Payment_Type = pymnt_Twrpymnt_Payments.newFieldArrayInGroup("pymnt_Twrpymnt_Payment_Type", "TWRPYMNT-PAYMENT-TYPE", FieldType.STRING, 
            1, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_PAYMENT_TYPE", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        registerRecord(vw_pymnt);

        pnd_S_Twrpymnt_Form_Sd = localVariables.newFieldInRecord("pnd_S_Twrpymnt_Form_Sd", "#S-TWRPYMNT-FORM-SD", FieldType.STRING, 37);

        pnd_S_Twrpymnt_Form_Sd__R_Field_1 = localVariables.newGroupInRecord("pnd_S_Twrpymnt_Form_Sd__R_Field_1", "REDEFINE", pnd_S_Twrpymnt_Form_Sd);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year = pnd_S_Twrpymnt_Form_Sd__R_Field_1.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year", 
            "#S-TWRPYMNT-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr = pnd_S_Twrpymnt_Form_Sd__R_Field_1.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr", 
            "#S-TWRPYMNT-TAX-ID-NBR", FieldType.STRING, 10);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr = pnd_S_Twrpymnt_Form_Sd__R_Field_1.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr", 
            "#S-TWRPYMNT-CONTRACT-NBR", FieldType.STRING, 8);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde = pnd_S_Twrpymnt_Form_Sd__R_Field_1.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde", 
            "#S-TWRPYMNT-PAYEE-CDE", FieldType.STRING, 2);
        pnd_In_Parm = localVariables.newFieldInRecord("pnd_In_Parm", "#IN-PARM", FieldType.STRING, 79);

        pnd_In_Parm__R_Field_2 = localVariables.newGroupInRecord("pnd_In_Parm__R_Field_2", "REDEFINE", pnd_In_Parm);
        pnd_In_Parm_Pnd_In_Active_Ind = pnd_In_Parm__R_Field_2.newFieldInGroup("pnd_In_Parm_Pnd_In_Active_Ind", "#IN-ACTIVE-IND", FieldType.STRING, 1);
        pnd_In_Parm_Pnd_In_Form_Type = pnd_In_Parm__R_Field_2.newFieldInGroup("pnd_In_Parm_Pnd_In_Form_Type", "#IN-FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_In_Parm_Pnd_In_Inc_Old = pnd_In_Parm__R_Field_2.newFieldInGroup("pnd_In_Parm_Pnd_In_Inc_Old", "#IN-INC-OLD", FieldType.STRING, 1);
        pnd_In_Parm_Pnd_In_Update = pnd_In_Parm__R_Field_2.newFieldInGroup("pnd_In_Parm_Pnd_In_Update", "#IN-UPDATE", FieldType.STRING, 1);
        pnd_In_Parm_Pnd_In_Tax_Year = pnd_In_Parm__R_Field_2.newFieldInGroup("pnd_In_Parm_Pnd_In_Tax_Year", "#IN-TAX-YEAR", FieldType.STRING, 4);

        pnd_In_Parm__R_Field_3 = pnd_In_Parm__R_Field_2.newGroupInGroup("pnd_In_Parm__R_Field_3", "REDEFINE", pnd_In_Parm_Pnd_In_Tax_Year);
        pnd_In_Parm_Pnd_In_Tax_Yearn = pnd_In_Parm__R_Field_3.newFieldInGroup("pnd_In_Parm_Pnd_In_Tax_Yearn", "#IN-TAX-YEARN", FieldType.NUMERIC, 4);
        pnd_Tirf_Superde_3 = localVariables.newFieldInRecord("pnd_Tirf_Superde_3", "#TIRF-SUPERDE-3", FieldType.STRING, 33);

        pnd_Tirf_Superde_3__R_Field_4 = localVariables.newGroupInRecord("pnd_Tirf_Superde_3__R_Field_4", "REDEFINE", pnd_Tirf_Superde_3);
        pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year = pnd_Tirf_Superde_3__R_Field_4.newFieldInGroup("pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year", "#TIRF-TAX-YEAR", 
            FieldType.STRING, 4);

        pnd_Tirf_Superde_3__R_Field_5 = pnd_Tirf_Superde_3__R_Field_4.newGroupInGroup("pnd_Tirf_Superde_3__R_Field_5", "REDEFINE", pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year);
        pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn = pnd_Tirf_Superde_3__R_Field_5.newFieldInGroup("pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn", "#TIRF-TAX-YEARN", 
            FieldType.NUMERIC, 4);
        pnd_Tirf_Superde_3_Pnd_Tirf_Active_Ind = pnd_Tirf_Superde_3__R_Field_4.newFieldInGroup("pnd_Tirf_Superde_3_Pnd_Tirf_Active_Ind", "#TIRF-ACTIVE-IND", 
            FieldType.STRING, 1);
        pnd_Tirf_Superde_3_Pnd_Tirf_Form_Type = pnd_Tirf_Superde_3__R_Field_4.newFieldInGroup("pnd_Tirf_Superde_3_Pnd_Tirf_Form_Type", "#TIRF-FORM-TYPE", 
            FieldType.NUMERIC, 2);

        pnd_Work_Area = localVariables.newGroupInRecord("pnd_Work_Area", "#WORK-AREA");
        pnd_Work_Area_Pnd_Ws_Et = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Et", "#WS-ET", FieldType.PACKED_DECIMAL, 5);
        pnd_Work_Area_Pnd_Ws_Isn = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Isn", "#WS-ISN", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Temp_Tax_Year = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Temp_Tax_Year", "#TEMP-TAX-YEAR", FieldType.NUMERIC, 4);

        pnd_Work_Area__R_Field_6 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area__R_Field_6", "REDEFINE", pnd_Work_Area_Pnd_Temp_Tax_Year);
        pnd_Work_Area_Pnd_Temp_Tax_Yearcc = pnd_Work_Area__R_Field_6.newFieldInGroup("pnd_Work_Area_Pnd_Temp_Tax_Yearcc", "#TEMP-TAX-YEARCC", FieldType.NUMERIC, 
            2);
        pnd_Work_Area_Pnd_Temp_Tax_Yearyy = pnd_Work_Area__R_Field_6.newFieldInGroup("pnd_Work_Area_Pnd_Temp_Tax_Yearyy", "#TEMP-TAX-YEARYY", FieldType.NUMERIC, 
            2);
        pnd_Work_Area_Pnd_Temp_Cntl_Num = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Temp_Cntl_Num", "#TEMP-CNTL-NUM", FieldType.NUMERIC, 10);
        pnd_Work_Area_Pnd_Ws_Num_Of_Doc = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Num_Of_Doc", "#WS-NUM-OF-DOC", FieldType.NUMERIC, 10);
        pnd_Work_Area_Pnd_Ws_Amt_Withld = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Amt_Withld", "#WS-AMT-WITHLD", FieldType.NUMERIC, 15, 2);
        pnd_Work_Area_Pnd_Ws_Amout_Paid = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Amout_Paid", "#WS-AMOUT-PAID", FieldType.NUMERIC, 15, 2);
        pnd_Work_Area_Pnd_Ws_Amount = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Amount", "#WS-AMOUNT", FieldType.NUMERIC, 12);
        pnd_Work_Area_Pnd_Empty_Form_Cnt = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Empty_Form_Cnt", "#EMPTY-FORM-CNT", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Work_Area_Pnd_Reject = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Reject", "#REJECT", FieldType.BOOLEAN, 1);
        pnd_Work_Area_Pnd_Rej_Count = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Rej_Count", "#REJ-COUNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Work_Area_Pnd_Old_Cmpny = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Old_Cmpny", "#OLD-CMPNY", FieldType.STRING, 4);
        pnd_Work_Area_Pnd_Counter = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Counter", "#COUNTER", FieldType.PACKED_DECIMAL, 9);
        pnd_Work_Area_Pnd_Cmpnys = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Cmpnys", "#CMPNYS", FieldType.NUMERIC, 2);
        pnd_Work_Area_Pnd_I = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 9);
        pnd_Work_Area_Pnd_First = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Work_Area_Pnd_Last = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Last", "#LAST", FieldType.BOOLEAN, 1);

        pnd_Work_Area_Pnd_Ws_Totales = pnd_Work_Area.newGroupArrayInGroup("pnd_Work_Area_Pnd_Ws_Totales", "#WS-TOTALES", new DbsArrayController(1, 3));
        pnd_Work_Area_Pnd_Ws_Cnt_Tot = pnd_Work_Area_Pnd_Ws_Totales.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Cnt_Tot", "#WS-CNT-TOT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Work_Area_Pnd_Ws_Gross_Tot = pnd_Work_Area_Pnd_Ws_Totales.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Gross_Tot", "#WS-GROSS-TOT", FieldType.PACKED_DECIMAL, 
            15, 2);
        pnd_Work_Area_Pnd_Ws_Int_Tot = pnd_Work_Area_Pnd_Ws_Totales.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Int_Tot", "#WS-INT-TOT", FieldType.PACKED_DECIMAL, 
            15, 2);
        pnd_Work_Area_Pnd_Ws_Comb_Tot = pnd_Work_Area_Pnd_Ws_Totales.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Comb_Tot", "#WS-COMB-TOT", FieldType.PACKED_DECIMAL, 
            15, 2);
        pnd_Work_Area_Pnd_Ws_Roll_Cont_Tot = pnd_Work_Area_Pnd_Ws_Totales.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Roll_Cont_Tot", "#WS-ROLL-CONT-TOT", FieldType.PACKED_DECIMAL, 
            15, 2);
        pnd_Work_Area_Pnd_Ws_Roll_Dist_Tot = pnd_Work_Area_Pnd_Ws_Totales.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Roll_Dist_Tot", "#WS-ROLL-DIST-TOT", FieldType.PACKED_DECIMAL, 
            15, 2);
        pnd_Work_Area_Pnd_Ws_Cnt = pnd_Work_Area_Pnd_Ws_Totales.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Cnt", "#WS-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Work_Area_Pnd_Ws_Gross = pnd_Work_Area_Pnd_Ws_Totales.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Gross", "#WS-GROSS", FieldType.PACKED_DECIMAL, 
            12, 2);
        pnd_Work_Area_Pnd_Ws_Int = pnd_Work_Area_Pnd_Ws_Totales.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Int", "#WS-INT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Work_Area_Pnd_Ws_Roll_Cont = pnd_Work_Area_Pnd_Ws_Totales.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Roll_Cont", "#WS-ROLL-CONT", FieldType.PACKED_DECIMAL, 
            12, 2);
        pnd_Work_Area_Pnd_Ws_Roll_Dist = pnd_Work_Area_Pnd_Ws_Totales.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Roll_Dist", "#WS-ROLL-DIST", FieldType.PACKED_DECIMAL, 
            12, 2);

        pnd_S_Output_Data = localVariables.newGroupInRecord("pnd_S_Output_Data", "#S-OUTPUT-DATA");
        pnd_S_Output_Data_Pnd_Ret_Code = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Ret_Code", "#RET-CODE", FieldType.BOOLEAN, 1);
        pnd_S_Output_Data_Pnd_Ret_Msg = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Ret_Msg", "#RET-MSG", FieldType.STRING, 35);
        pnd_S_Output_Data_Pnd_Comp_Short_Name = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Short_Name", "#COMP-SHORT-NAME", FieldType.STRING, 
            4);
        pnd_S_Output_Data_Pnd_Comp_Name = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Name", "#COMP-NAME", FieldType.STRING, 55);
        pnd_S_Output_Data_Pnd_Comp_Corr_Name = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Corr_Name", "#COMP-CORR-NAME", FieldType.STRING, 
            18);
        pnd_S_Output_Data_Pnd_Fed_Id = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Fed_Id", "#FED-ID", FieldType.STRING, 9);
        pnd_S_Output_Data_Pnd_Nr4_Id = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Nr4_Id", "#NR4-ID", FieldType.STRING, 9);
        pnd_S_Output_Data_Pnd_Contact_Name = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Contact_Name", "#CONTACT-NAME", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Contact_Email_Addr = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Contact_Email_Addr", "#CONTACT-EMAIL-ADDR", 
            FieldType.STRING, 35);
        pnd_S_Output_Data_Pnd_Phone = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Phone", "#PHONE", FieldType.STRING, 14);
        pnd_S_Output_Data_Pnd_Address = pnd_S_Output_Data.newFieldArrayInGroup("pnd_S_Output_Data_Pnd_Address", "#ADDRESS", FieldType.STRING, 30, new 
            DbsArrayController(1, 2));
        pnd_S_Output_Data_Pnd_City = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_City", "#CITY", FieldType.STRING, 9);
        pnd_S_Output_Data_Pnd_State = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_S_Output_Data_Pnd_Zip_9 = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Zip_9", "#ZIP-9", FieldType.STRING, 9);

        pnd_S_Output_Data__R_Field_7 = pnd_S_Output_Data.newGroupInGroup("pnd_S_Output_Data__R_Field_7", "REDEFINE", pnd_S_Output_Data_Pnd_Zip_9);
        pnd_S_Output_Data_Pnd_Zip = pnd_S_Output_Data__R_Field_7.newFieldInGroup("pnd_S_Output_Data_Pnd_Zip", "#ZIP", FieldType.STRING, 5);
        pnd_S_Output_Data_Pnd_Zip_4 = pnd_S_Output_Data__R_Field_7.newFieldInGroup("pnd_S_Output_Data_Pnd_Zip_4", "#ZIP-4", FieldType.STRING, 4);
        pnd_S_Output_Data_Pnd_Comp_Tcc = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Tcc", "#COMP-TCC", FieldType.STRING, 10);
        pnd_S_Output_Data_Pnd_Comp_Trans_A = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Trans_A", "#COMP-TRANS-A", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Trans_B = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Trans_B", "#COMP-TRANS-B", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Payer_A = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Payer_A", "#COMP-PAYER-A", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Payer_B = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Payer_B", "#COMP-PAYER-B", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Agent_A = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Agent_A", "#COMP-AGENT-A", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Agent_B = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Agent_B", "#COMP-AGENT-B", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Retrn_A = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Retrn_A", "#COMP-RETRN-A", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Retrn_B = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Retrn_B", "#COMP-RETRN-B", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Futr1_A = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Futr1_A", "#COMP-FUTR1-A", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Futr1_B = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Futr1_B", "#COMP-FUTR1-B", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Futr2_A = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Futr2_A", "#COMP-FUTR2-A", FieldType.STRING, 
            40);
        pnd_S_Output_Data_Pnd_Comp_Futr2_B = pnd_S_Output_Data.newFieldInGroup("pnd_S_Output_Data_Pnd_Comp_Futr2_B", "#COMP-FUTR2-B", FieldType.STRING, 
            40);
        pnd_Temp_Contact_Phone = localVariables.newFieldInRecord("pnd_Temp_Contact_Phone", "#TEMP-CONTACT-PHONE", FieldType.STRING, 15);

        pnd_Temp_Contact_Phone__R_Field_8 = localVariables.newGroupInRecord("pnd_Temp_Contact_Phone__R_Field_8", "REDEFINE", pnd_Temp_Contact_Phone);
        pnd_Temp_Contact_Phone_Pnd_Temp_Contact_Phone_N = pnd_Temp_Contact_Phone__R_Field_8.newFieldInGroup("pnd_Temp_Contact_Phone_Pnd_Temp_Contact_Phone_N", 
            "#TEMP-CONTACT-PHONE-N", FieldType.NUMERIC, 15);
        pnd_Spaces = localVariables.newFieldInRecord("pnd_Spaces", "#SPACES", FieldType.STRING, 250);
        pnd_Maxm_Exempt_Amt = localVariables.newFieldInRecord("pnd_Maxm_Exempt_Amt", "#MAXM-EXEMPT-AMT", FieldType.NUMERIC, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_form1.reset();
        vw_pymnt.reset();

        ldaTwrl117c.initializeValues();
        ldaTwrl9710.initializeValues();
        ldaTwrltb4r.initializeValues();

        localVariables.reset();
        pnd_Work_Area_Pnd_First.setInitialValue(true);
        pnd_Maxm_Exempt_Amt.setInitialValue(10000);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0117() throws Exception
    {
        super("Twrp0117");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp0117|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*  -----------------------------------------
                //*  FO 02/01
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                //*  TOTALS
                //*  ACCEPTED
                //*  REJECTED
                //*                                                                                                                                                       //Natural: FORMAT PS = 56 LS = 133 ES = ON;//Natural: FORMAT ( 1 ) PS = 60 LS = 133 ES = ON;//Natural: FORMAT ( 2 ) PS = 60 LS = 133 ES = ON;//Natural: FORMAT ( 3 ) PS = 60 LS = 133 ES = ON
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_In_Parm);                                                                                          //Natural: INPUT #IN-PARM
                //*  FO 02/01
                //*  TAX YEAR IS ALWAYS CALENDAR
                if (condition(pnd_In_Parm_Pnd_In_Tax_Year.equals(" ")))                                                                                                   //Natural: IF #IN-TAX-YEAR = ' '
                {
                    pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year.compute(new ComputeParameters(false, pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year), Global.getDATN().divide(10000).subtract(1)); //Natural: ASSIGN #TIRF-TAX-YEAR := *DATN / 10000 - 1
                    //*  YEAR MINUS 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year.setValue(pnd_In_Parm_Pnd_In_Tax_Year);                                                                           //Natural: ASSIGN #TIRF-TAX-YEAR := #IN-TAX-YEAR
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year.less("2007")))                                                                                         //Natural: IF #TIRF-TAX-YEAR LT '2007'
                {
                    //*  FOR FORM 480.6A  01/28/08  RM
                    Global.setFetchProgram(DbsUtil.getBlType("TWRP0124"));                                                                                                //Natural: FETCH 'TWRP0124'
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
                //*  GET 'S' (SERVICES) COMPANY CODE DATA.
                //*  02-08-2005
                                                                                                                                                                          //Natural: PERFORM COMPANY-BREAK
                sub_Company_Break();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  02-08-2005
                pnd_S_Output_Data.setValuesByName(pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data());                                                                         //Natural: MOVE BY NAME #TWRACOM2.#OUTPUT-DATA TO #S-OUTPUT-DATA
                //*  02/13/13
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADER-REC
                sub_Write_Header_Rec();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //* 01/29/15 MUKHERR
                                                                                                                                                                          //Natural: PERFORM WRITE-PA-REC
                sub_Write_Pa_Rec();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  ==============================
                //*  01/28/08  RM
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 1 ) TITLE LEFT *TIMX ( EM = MM/DD/YYYY�HH:IIAP ) 28T 'TaxWaRS - Annual Original Tax Report - Totals  ' 92T 'Page:' *PAGE-NUMBER ( 1 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 34T #TIRF-TAX-YEAR '- Puerto Rico - Form 480.7C' /// 26T '  Accepted       Rejected            Total' / 26T '  --------       --------            -----'
                //*  02-08-2005
                //*  01/28/08  RM
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 2 ) TITLE LEFT *TIMX ( EM = MM/DD/YYYY�HH:IIAP ) 28T 'TaxWaRS - Annual Original Tax Report - Accepted' 92T 'Page:' *PAGE-NUMBER ( 2 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 32T #TIRF-TAX-YEAR '-' #TWRACOM2.#COMP-SHORT-NAME '- Puerto Rico - Form 480.7C' /
                //*  02-08-2005
                //*  01/28/08  RM
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 3 ) TITLE LEFT *TIMX ( EM = MM/DD/YYYY�HH:IIAP ) 28T 'TaxWaRS - Annual Original Tax Report - Rejected' 92T 'Page:' *PAGE-NUMBER ( 3 ) ( EM = ZZ9 ) / *INIT-USER '-' *PROGRAM 32T #TIRF-TAX-YEAR '-' #TWRACOM2.#COMP-SHORT-NAME '- Puerto Rico - Form 480.7C' /
                //*  -----------------------------------------------
                pnd_Tirf_Superde_3_Pnd_Tirf_Active_Ind.setValue(pnd_In_Parm_Pnd_In_Active_Ind);                                                                           //Natural: ASSIGN #TIRF-ACTIVE-IND := #IN-ACTIVE-IND
                pnd_Tirf_Superde_3_Pnd_Tirf_Form_Type.setValue(pnd_In_Parm_Pnd_In_Form_Type);                                                                             //Natural: ASSIGN #TIRF-FORM-TYPE := #IN-FORM-TYPE
                ldaTwrl9710.getVw_form().startDatabaseRead                                                                                                                //Natural: READ FORM WITH TIRF-SUPERDE-3 = #TIRF-SUPERDE-3
                (
                "READ01",
                new Wc[] { new Wc("TIRF_SUPERDE_3", ">=", pnd_Tirf_Superde_3, WcType.BY) },
                new Oc[] { new Oc("TIRF_SUPERDE_3", "ASC") }
                );
                READ01:
                while (condition(ldaTwrl9710.getVw_form().readNextRow("READ01")))
                {
                    if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().notEquals(pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year) || ldaTwrl9710.getForm_Tirf_Active_Ind().notEquals(pnd_Tirf_Superde_3_Pnd_Tirf_Active_Ind)  //Natural: IF TIRF-TAX-YEAR NE #TIRF-TAX-YEAR OR TIRF-ACTIVE-IND NE #TIRF-ACTIVE-IND OR TIRF-FORM-TYPE NE #TIRF-FORM-TYPE
                        || ldaTwrl9710.getForm_Tirf_Form_Type().notEquals(pnd_Tirf_Superde_3_Pnd_Tirf_Form_Type)))
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl9710.getForm_Tirf_Empty_Form().getBoolean()))                                                                                    //Natural: IF FORM.TIRF-EMPTY-FORM
                    {
                        pnd_Work_Area_Pnd_Empty_Form_Cnt.nadd(1);                                                                                                         //Natural: ADD 1 TO #EMPTY-FORM-CNT
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_In_Parm_Pnd_In_Inc_Old.equals("N") && ldaTwrl9710.getForm_Tirf_Irs_Rpt_Ind().notEquals(" ")))                                       //Natural: IF #IN-INC-OLD EQ 'N' AND FORM.TIRF-IRS-RPT-IND NE ' '
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Work_Area_Pnd_Counter.nadd(1);                                                                                                                    //Natural: ADD 1 TO #COUNTER
                    if (condition(ldaTwrl9710.getForm_Tirf_Company_Cde().notEquals(pnd_Work_Area_Pnd_Old_Cmpny)))                                                         //Natural: IF FORM.TIRF-COMPANY-CDE NE #OLD-CMPNY
                    {
                                                                                                                                                                          //Natural: PERFORM COMPANY-BREAK
                        sub_Company_Break();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        getReports().newPage(new ReportSpecification(2));                                                                                                 //Natural: NEWPAGE ( 2 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().newPage(new ReportSpecification(3));                                                                                                 //Natural: NEWPAGE ( 3 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Work_Area_Pnd_Reject.reset();                                                                                                                     //Natural: RESET #REJECT
                    pnd_Work_Area_Pnd_Ws_Isn.setValue(ldaTwrl9710.getVw_form().getAstISN("Read01"));                                                                      //Natural: MOVE *ISN TO #WS-ISN
                    if (condition(pnd_In_Parm_Pnd_In_Update.equals("Y")))                                                                                                 //Natural: IF #IN-UPDATE EQ 'Y'
                    {
                        GET1:                                                                                                                                             //Natural: GET FORM1 #WS-ISN
                        vw_form1.readByID(pnd_Work_Area_Pnd_Ws_Isn.getLong(), "GET1");
                        //*  FO 02/01 FLAG REJECTED RECORDS
                        //*  FO 02/01 CHANGED TO HELD
                        if (condition(ldaTwrl9710.getForm_Tirf_Irs_Reject_Ind().greater(" ")))                                                                            //Natural: IF TIRF-IRS-REJECT-IND GT ' '
                        {
                            form1_Tirf_Irs_Rpt_Ind.setValue("H");                                                                                                         //Natural: ASSIGN FORM1.TIRF-IRS-RPT-IND := 'H'
                                                                                                                                                                          //Natural: PERFORM WRITE-REJECT-REPORT
                            sub_Write_Reject_Report();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            pnd_Work_Area_Pnd_Reject.setValue(true);                                                                                                      //Natural: ASSIGN #REJECT := TRUE
                            //*  FO 02/01 CHANGED TO ORIGINAL
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            form1_Tirf_Irs_Rpt_Ind.setValue("O");                                                                                                         //Natural: ASSIGN FORM1.TIRF-IRS-RPT-IND := 'O'
                        }                                                                                                                                                 //Natural: END-IF
                        form1_Tirf_Lu_User.setValue(Global.getINIT_USER());                                                                                               //Natural: ASSIGN FORM1.TIRF-LU-USER := *INIT-USER
                        form1_Tirf_Lu_Ts.setValue(Global.getDATX());                                                                                                      //Natural: ASSIGN FORM1.TIRF-LU-TS := *DATX
                        form1_Tirf_Irs_Rpt_Date.setValue(Global.getDATX());                                                                                               //Natural: ASSIGN FORM1.TIRF-IRS-RPT-DATE := *DATX
                        vw_form1.updateDBRow("GET1");                                                                                                                     //Natural: UPDATE ( GET1. )
                        pnd_Work_Area_Pnd_Ws_Et.nadd(1);                                                                                                                  //Natural: ADD 1 TO #WS-ET
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Work_Area_Pnd_Ws_Et.greater(250)))                                                                                                  //Natural: IF #WS-ET GT 250
                    {
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                        pnd_Work_Area_Pnd_Ws_Et.reset();                                                                                                                  //Natural: RESET #WS-ET
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Work_Area_Pnd_Reject.getBoolean()))                                                                                                 //Natural: IF #REJECT
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ADD 1 TO #WS-SEQ-NUM
                    if (condition(pnd_Tirf_Superde_3_Pnd_Tirf_Form_Type.equals(5)))                                                                                       //Natural: IF #TIRF-FORM-TYPE = 5
                    {
                        //*  01/28/08  RM
                                                                                                                                                                          //Natural: PERFORM WRITE-480-7C-REC
                        sub_Write_480_7c_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    //*  IF  #TIRF-FORM-TYPE  =  6
                    //*    PERFORM WRITE-480-6B-REC                          /* 01/28/08  RM
                    //*  END-IF
                                                                                                                                                                          //Natural: PERFORM DISPLAY-ACCEPTED-FORM
                    sub_Display_Accepted_Form();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                //*  02/01/06  RM
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_Work_Area_Pnd_Last.setValue(true);                                                                                                                    //Natural: ASSIGN #LAST := TRUE
                                                                                                                                                                          //Natural: PERFORM COMPANY-BREAK
                sub_Company_Break();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  ----------------------------------------
                //*  #WS-COMB-TOT (*) ADDED AS NEW TOTAL AMOUNT FIELDS        01/28/08  RM
                pnd_Work_Area_Pnd_Ws_Comb_Tot.getValue(1).compute(new ComputeParameters(false, pnd_Work_Area_Pnd_Ws_Comb_Tot.getValue(1)), pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(1).add(pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(1))); //Natural: COMPUTE #WS-COMB-TOT ( 1 ) = #WS-GROSS-TOT ( 1 ) + #WS-INT-TOT ( 1 )
                pnd_Work_Area_Pnd_Ws_Comb_Tot.getValue(2).compute(new ComputeParameters(false, pnd_Work_Area_Pnd_Ws_Comb_Tot.getValue(2)), pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(2).add(pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(2))); //Natural: COMPUTE #WS-COMB-TOT ( 2 ) = #WS-GROSS-TOT ( 2 ) + #WS-INT-TOT ( 2 )
                pnd_Work_Area_Pnd_Ws_Comb_Tot.getValue(3).compute(new ComputeParameters(false, pnd_Work_Area_Pnd_Ws_Comb_Tot.getValue(3)), pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(3).add(pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(3))); //Natural: COMPUTE #WS-COMB-TOT ( 3 ) = #WS-GROSS-TOT ( 3 ) + #WS-INT-TOT ( 3 )
                getReports().write(1, "Total Gross         ",pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(1), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(2),  //Natural: WRITE ( 1 ) 'Total Gross         ' #WS-GROSS-TOT ( 1 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-GROSS-TOT ( 2 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-GROSS-TOT ( 3 ) ( EM = ZZZZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(3), new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(1, "Total Interest      ",pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(1), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(2),  //Natural: WRITE ( 1 ) 'Total Interest      ' #WS-INT-TOT ( 1 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-INT-TOT ( 2 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-INT-TOT ( 3 ) ( EM = ZZZZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(3), new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                //*  01/28/08  RM
                getReports().write(1, "Total Amount Paid   ",pnd_Work_Area_Pnd_Ws_Comb_Tot.getValue(1), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Comb_Tot.getValue(2),  //Natural: WRITE ( 1 ) 'Total Amount Paid   ' #WS-COMB-TOT ( 1 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-COMB-TOT ( 2 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-COMB-TOT ( 3 ) ( EM = ZZZZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Comb_Tot.getValue(3), new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                //*  02/04/09
                //*  02/04/09
                //*  02/04/09
                //*  02/04/09
                getReports().write(1, "Total Contributions ",pnd_Work_Area_Pnd_Ws_Roll_Cont_Tot.getValue(1), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Roll_Cont_Tot.getValue(2),  //Natural: WRITE ( 1 ) 'Total Contributions ' #WS-ROLL-CONT-TOT ( 1 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-ROLL-CONT-TOT ( 2 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-ROLL-CONT-TOT ( 3 ) ( EM = ZZZZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Roll_Cont_Tot.getValue(3), new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                //*  02/04/09
                //*  02/04/09
                //*  02/04/09
                //*  02/04/09
                getReports().write(1, "Total Distributions ",pnd_Work_Area_Pnd_Ws_Roll_Dist_Tot.getValue(1), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Roll_Dist_Tot.getValue(2),  //Natural: WRITE ( 1 ) 'Total Distributions ' #WS-ROLL-DIST-TOT ( 1 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-ROLL-DIST-TOT ( 2 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-ROLL-DIST-TOT ( 3 ) ( EM = ZZZZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Roll_Dist_Tot.getValue(3), new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                getReports().write(1, "Total Forms       ",new TabSetting(26),pnd_Work_Area_Pnd_Ws_Cnt_Tot.getValue(1),new TabSetting(42),pnd_Work_Area_Pnd_Ws_Cnt_Tot.getValue(2),new  //Natural: WRITE ( 1 ) 'Total Forms       ' 26T #WS-CNT-TOT ( 1 ) 42T #WS-CNT-TOT ( 2 ) 60T #WS-CNT-TOT ( 3 )
                    TabSetting(60),pnd_Work_Area_Pnd_Ws_Cnt_Tot.getValue(3));
                if (Global.isEscape()) return;
                getReports().write(1, "* -","-",new RepeatItem(70));                                                                                                      //Natural: WRITE ( 1 ) '* -' '-' ( 70 )
                if (Global.isEscape()) return;
                //*  --------------------------------------------
                //*  6 = 6B  06/17/08  RM
                //*  UPDT 7C CNTRL REC 01/28/08  RM
                if (condition(pnd_In_Parm_Pnd_In_Update.equals("Y")))                                                                                                     //Natural: IF #IN-UPDATE = 'Y'
                {
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn);                                                           //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #TIRF-TAX-YEARN
                    //*  #TWRATBL4.#FORM-IND  := #IN-FORM-TYPE
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(false);                                                                                          //Natural: ASSIGN #TWRATBL4.#ABEND-IND := FALSE
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Rec_Type().setValue(" ");                                                                                             //Natural: ASSIGN #TWRATBL4.#REC-TYPE := ' '
                    FOR01:                                                                                                                                                //Natural: FOR #TWRATBL4.#FORM-IND = 5 TO 6
                    for (pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(5); condition(pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().lessOrEqual(6)); 
                        pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().nadd(1))
                    {
                        //*  ALSO UPDT 6B      06/17/08  RM
                        DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4());                                                        //Natural: CALLNAT 'TWRNTB4R' #TWRATBL4
                        if (condition(Global.isEscape())) return;
                        if (condition(! (pdaTwratbl4.getPnd_Twratbl4_Pnd_Ret_Code().getBoolean())))                                                                       //Natural: IF NOT #TWRATBL4.#RET-CODE
                        {
                            DbsUtil.terminate(112);  if (true) return;                                                                                                    //Natural: TERMINATE 112
                        }                                                                                                                                                 //Natural: END-IF
                        pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(1).setValue(Global.getDATX());                                                             //Natural: ASSIGN #TWRATBL4.TIRCNTL-RPT-DTE ( 1 ) := *DATX
                        DbsUtil.callnat(Twrntb4u.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4());                                                        //Natural: CALLNAT 'TWRNTB4U' #TWRATBL4
                        if (condition(Global.isEscape())) return;
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //*  VIKRAM
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAILER-RECORD
                sub_Write_Trailer_Record();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  480.5  SUMMARY RECORD  02/01/06 RM
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-REC
                sub_Write_Summary_Rec();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                //*  -----------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-480-7C-REC
                //* *#TWRL117A-CONTROL-NUMBER   := '     7X216'
                //* *#TWRL117A-CONTROL-NUMBER   := '  00008266           /*   02/09/10  AY
                //*  #TWRL117A-CONTROL-NUMBER   := TIRF-PR-CNTL-NUM
                //*  >> SINHASN
                //*  #TWRL117A-TRANS-CODE       := 'O'
                //*  POPULATE REPORTING RECORD WITH 'SERVICES' VALUES.
                //*  #TWRL117A-PAYERS-NAME   := #S-OUTPUT-DATA.#COMP-PAYER-A
                //*  #ADDRESS(2) CONTAINS CITY, STATE & ZIP CODE, SHOULD NOT BE USE TO
                //*  POPULATE THE ADDRESS-2. LEAVE IT BLANK.                   01/28/08  RM
                //*  #TWRL117A-PAYERS-ADDRESS-2 := #S-OUTPUT-DATA.#ADDRESS(2)
                //*  >> SINHASN
                //* *    IF TWRPYMNT-PAYMENT-TYPE(*) = 'A' OR= 'C' OR= 'E'
                //* *      #TWRL117A-PLAN-OR-ANN-TYPE    := 'A'
                //* *    ELSE
                //*  #TWRL117A-PLAN-NM            := 'TIAA CLIENT PLAN'
                //*  #TWRL117A-PLAN-SPNSR         := 'TIAA CLIENT PLAN SPONSOR'
                //* *************DASDH1 - 480.7C BOX CHANGES********
                //*  -----------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPANY-BREAK
                //*  ------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-ACCEPTED-FORM
                //*  =====================================
                //*   FORM.TIRF-PR-CNTL-NUM
                //*  ------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REJECT-REPORT
                //*  FORM.TIRF-PR-CNTL-NUM
                //*  -----------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUMMARY-REC
                //*  ADD 1 TO #WS-SEQ-NUM
                //* *#TWRL117A-S-CONTROL-NUMBER := '  00008266'
                //*  #TWRL117A-S-FORM-TYPE      := 2
                //*  #TWRL117A-S-TRANS-CODE       := 'O'
                //*  #TWRL117A-S-PAYERS-NAME  := #S-OUTPUT-DATA.#COMP-PAYER-A
                //*  #ADDRESS(2) CONTAINS CITY, STATE & ZIP CODE, SHOULD NOT BE USE TO
                //*  POPULATE THE ADDRESS-2. LEAVE IT BLANK.                   01/28/08  RM
                //*  #TWRL117A-PAYERS-ADDRESS-2 := #S-OUTPUT-DATA.#ADDRESS(2)
                //* *#TWRL117A-S-TAXPAYER-TYPE    := 'C'
                //* *#TWRL117A-S-TAXPAYER-TYPE    := 'I'
                //*  ----------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HEADER-REC
                //*  >> SINHASN
                //*  -------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PA-REC
                //*  #TWRL117P-TAX-YEAR           := 2014
                //*  #TWRL117P-TAX-YEAR           := 2015
                //*  #TWRL117P-TAX-YEAR           := 2016
                //*  #TWRL117P-TAX-YEAR           := 2017
                //*  << SINHASN
                //*  #TWRL117P-CNTCT-EMAIL      := 'AFRIAS@TIAA-CREF.ORG'
                //*  >> SINHASN
                //* *********************************************************************
                //*  ----------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TRAILER-RECORD
                //* ***********************************************************************
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    //*    01/28/08  RM
    private void sub_Write_480_7c_Rec() throws Exception                                                                                                                  //Natural: WRITE-480-7C-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        //*  PERFORM GET-COMPANY-INFO  /* EB, NOW CALLED FROM AT BREAK OF CMPNY
        //*    03/05/12  MB
        if (condition(DbsUtil.maskMatches(ldaTwrl9710.getForm_Tirf_Geo_Cde(),"NN")))                                                                                      //Natural: IF FORM.TIRF-GEO-CDE EQ MASK ( NN )
        {
            pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                         //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
            pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwratbl2.getTwratbl2_Pnd_Tax_Year()), ldaTwrl9710.getForm_Tirf_Tax_Year().val()); //Natural: ASSIGN TWRATBL2.#TAX-YEAR := VAL ( FORM.TIRF-TAX-YEAR )
            pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().setValue(false);                                                                                                      //Natural: ASSIGN TWRATBL2.#ABEND-IND := FALSE
            pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().setValue(false);                                                                                                    //Natural: ASSIGN TWRATBL2.#DISPLAY-IND := FALSE
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "0", ldaTwrl9710.getForm_Tirf_Geo_Cde()));                   //Natural: COMPRESS '0' FORM.TIRF-GEO-CDE INTO TWRATBL2.#STATE-CDE LEAVING NO
            DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
            //*    03/05/12  MB
        }                                                                                                                                                                 //Natural: END-IF
        //*    01/28/08  RM
        ldaTwrl117c.getPnd_Twrl117c().reset();                                                                                                                            //Natural: RESET #TWRL117C
        pnd_Work_Area_Pnd_Temp_Cntl_Num.compute(new ComputeParameters(false, pnd_Work_Area_Pnd_Temp_Cntl_Num), ldaTwrl9710.getForm_Tirf_Pr_Cntl_Num().val());             //Natural: ASSIGN #TEMP-CNTL-NUM := VAL ( TIRF-PR-CNTL-NUM )
        //*    01/28/08  RM
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Control_Number().setValueEdited(pnd_Work_Area_Pnd_Temp_Cntl_Num,new ReportEditMask("Z999999999"));                       //Natural: MOVE EDITED #TEMP-CNTL-NUM ( EM = Z999999999 ) TO #TWRL117A-CONTROL-NUMBER
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Form_Type().setValue("Y");                                                                                               //Natural: ASSIGN #TWRL117A-FORM-TYPE := 'Y'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Rec_Type().setValue(1);                                                                                                  //Natural: ASSIGN #TWRL117A-REC-TYPE := 1
        //*   VIKRAM1 STARTS
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().greaterOrEqual("2019")))                                                                                        //Natural: IF FORM.TIRF-TAX-YEAR GE '2019'
        {
            ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Resident_Type().setValue("1");                                                                                 //Natural: ASSIGN #TWRL117A-PAYEE-RESIDENT-TYPE := '1'
            //*   VIKRAM1 ENDS
        }                                                                                                                                                                 //Natural: END-IF
        //*  #TWRL117A-TRANS-CODE VALUES: 'O' = ORIGINAL
        //*     (DOCUMENT TYPE)           'C' = CORRECTED
        //*                               'A' = AMENDED
        //*  EXCLUDED AS OF 2009!         'X' = DELETED          /*   02/09/10  AY
        //*  << SINHASN
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().greaterOrEqual("2016")))                                                                                        //Natural: IF FORM.TIRF-TAX-YEAR GE '2016'
        {
            //*  INDIVIDUAL
            if (condition(ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("1")))                                                                                            //Natural: IF TIRF-TAX-ID-TYPE = '1'
            {
                ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Type_Id_Payee().setValue("2");                                                                                   //Natural: ASSIGN #TWRL117A-TYPE-ID-PAYEE := '2'
                //*  CORPORATION
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("2")))                                                                                        //Natural: IF TIRF-TAX-ID-TYPE = '2'
                {
                    ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Type_Id_Payee().setValue("1");                                                                               //Natural: ASSIGN #TWRL117A-TYPE-ID-PAYEE := '1'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Type_Id_Payee().equals("2")))                                                                          //Natural: IF #TWRL117A-TYPE-ID-PAYEE = '2'
            {
                ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payeee_First_Name().setValue(ldaTwrl9710.getForm_Tirf_Part_First_Nme());                                         //Natural: ASSIGN #TWRL117A-PAYEEE-FIRST-NAME := FORM.TIRF-PART-FIRST-NME
                ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payeee_Last_Name().setValue(ldaTwrl9710.getForm_Tirf_Part_Last_Nme());                                           //Natural: ASSIGN #TWRL117A-PAYEEE-LAST-NAME := FORM.TIRF-PART-LAST-NME
            }                                                                                                                                                             //Natural: END-IF
            //*   #TWRL117A-ANNUITY-COST  := FORM.TIRF-IVC-AMT /* MUKHERR1
            //* PAULS
            //*  02-08-2005
            //*  VIKRAM
            //*  02-08-2005
            //*  02-2013
            //*  02-08-2005
            //*  02-08-2005
            //*  02-08-2005
            //*  02-08-2005
            //*  02-08-2005
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Doc_Type().setValue("O");                                                                                                //Natural: ASSIGN #TWRL117A-DOC-TYPE := 'O'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Tax_Year().compute(new ComputeParameters(false, ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Tax_Year()),                    //Natural: ASSIGN #TWRL117A-TAX-YEAR := VAL ( FORM.TIRF-TAX-YEAR )
            ldaTwrl9710.getForm_Tirf_Tax_Year().val());
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payers_Id_Type().setValue("1");                                                                                          //Natural: ASSIGN #TWRL117A-PAYERS-ID-TYPE := '1'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payers_Id().compute(new ComputeParameters(false, ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payers_Id()),                  //Natural: ASSIGN #TWRL117A-PAYERS-ID := VAL ( #S-OUTPUT-DATA.#FED-ID )
            pnd_S_Output_Data_Pnd_Fed_Id.val());
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payers_Name().setValue(pnd_S_Output_Data_Pnd_Comp_Name);                                                                 //Natural: ASSIGN #TWRL117A-PAYERS-NAME := #S-OUTPUT-DATA.#COMP-NAME
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payers_Address_1().setValue(pnd_S_Output_Data_Pnd_Address.getValue(1));                                                  //Natural: ASSIGN #TWRL117A-PAYERS-ADDRESS-1 := #S-OUTPUT-DATA.#ADDRESS ( 1 )
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payers_Town().setValue(pnd_S_Output_Data_Pnd_City);                                                                      //Natural: ASSIGN #TWRL117A-PAYERS-TOWN := #S-OUTPUT-DATA.#CITY
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payers_State().setValue(pnd_S_Output_Data_Pnd_State);                                                                    //Natural: ASSIGN #TWRL117A-PAYERS-STATE := #S-OUTPUT-DATA.#STATE
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payers_Zip_Full().setValue(pnd_S_Output_Data_Pnd_Zip_9);                                                                 //Natural: ASSIGN #TWRL117A-PAYERS-ZIP-FULL := #S-OUTPUT-DATA.#ZIP-9
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Ssn().compute(new ComputeParameters(false, ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Ssn()),                  //Natural: ASSIGN #TWRL117A-PAYEE-SSN := VAL ( FORM.TIRF-TIN )
            ldaTwrl9710.getForm_Tirf_Tin().val());
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Acct().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl9710.getForm_Tirf_Contract_Nbr(),           //Natural: COMPRESS TIRF-CONTRACT-NBR TIRF-PAYEE-CDE INTO #TWRL117A-PAYEE-ACCT LEAVING NO
            ldaTwrl9710.getForm_Tirf_Payee_Cde()));
        //*  << SINHASN
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().greaterOrEqual("2016")))                                                                                        //Natural: IF FORM.TIRF-TAX-YEAR GE '2016'
        {
            //*  CORPORATION
            if (condition(ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Type_Id_Payee().equals("1")))                                                                          //Natural: IF #TWRL117A-TYPE-ID-PAYEE = '1'
            {
                ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Name().setValue(DbsUtil.compress(ldaTwrl9710.getForm_Tirf_Part_First_Nme(), ldaTwrl9710.getForm_Tirf_Part_Mddle_Nme(),  //Natural: COMPRESS FORM.TIRF-PART-FIRST-NME FORM.TIRF-PART-MDDLE-NME FORM.TIRF-PART-LAST-NME INTO #TWRL117A-PAYEE-NAME
                    ldaTwrl9710.getForm_Tirf_Part_Last_Nme()));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Name().setValue(DbsUtil.compress(ldaTwrl9710.getForm_Tirf_Part_First_Nme(), ldaTwrl9710.getForm_Tirf_Part_Mddle_Nme(),  //Natural: COMPRESS FORM.TIRF-PART-FIRST-NME FORM.TIRF-PART-MDDLE-NME FORM.TIRF-PART-LAST-NME INTO #TWRL117A-PAYEE-NAME
                ldaTwrl9710.getForm_Tirf_Part_Last_Nme()));
            //*  02-20-07 RM
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Address_1().setValue(ldaTwrl9710.getForm_Tirf_Street_Addr());                                                      //Natural: ASSIGN #TWRL117A-PAYEE-ADDRESS-1 := FORM.TIRF-STREET-ADDR
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Address_2().setValue(ldaTwrl9710.getForm_Tirf_Street_Addr_Cont_1());                                               //Natural: ASSIGN #TWRL117A-PAYEE-ADDRESS-2 := FORM.TIRF-STREET-ADDR-CONT-1
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Town().setValue(ldaTwrl9710.getForm_Tirf_City());                                                                  //Natural: ASSIGN #TWRL117A-PAYEE-TOWN := FORM.TIRF-CITY
        //*    03/05/12 MB
        //*    03/05/12 MB
        if (condition(DbsUtil.maskMatches(ldaTwrl9710.getForm_Tirf_Geo_Cde(),"NN")))                                                                                      //Natural: IF FORM.TIRF-GEO-CDE EQ MASK ( NN )
        {
            ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_State().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code());                                          //Natural: ASSIGN #TWRL117A-PAYEE-STATE := TIRCNTL-STATE-ALPHA-CODE
            //*    03/05/12 MB
            //*    03/05/12 MB
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_State().setValue(ldaTwrl9710.getForm_Tirf_Geo_Cde());                                                          //Natural: ASSIGN #TWRL117A-PAYEE-STATE := FORM.TIRF-GEO-CDE
            //*    03/05/12 MB
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Zip_Full().setValue(ldaTwrl9710.getForm_Tirf_Zip());                                                               //Natural: ASSIGN #TWRL117A-PAYEE-ZIP-FULL := FORM.TIRF-ZIP
        //*  THE FOLLOWING LINES ARE NEW FOR 2007 480.7C FORM      01/28/08  RM
        //*          START 01/28/08  RM
        //*  << SINHASN - NEW PLAN-TYPE IND & FORM-OF-DISTRIB - 2016
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().greaterOrEqual("2016")))                                                                                        //Natural: IF FORM.TIRF-TAX-YEAR GE '2016'
        {
            //*  ANNUITY
            //*  PARTIAL
            //*  LUMP SUM
            short decideConditionsMet1647 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF FORM.TIRF-DISTR-CATEGORY;//Natural: VALUE '1'
            if (condition((ldaTwrl9710.getForm_Tirf_Distr_Category().equals("1"))))
            {
                decideConditionsMet1647++;
                ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Form_Of_Distr().setValue("E");                                                                                   //Natural: ASSIGN #TWRL117A-FORM-OF-DISTR := 'E'
            }                                                                                                                                                             //Natural: VALUE '2'
            else if (condition((ldaTwrl9710.getForm_Tirf_Distr_Category().equals("2"))))
            {
                decideConditionsMet1647++;
                ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Form_Of_Distr().setValue("P");                                                                                   //Natural: ASSIGN #TWRL117A-FORM-OF-DISTR := 'P'
            }                                                                                                                                                             //Natural: VALUE '3'
            else if (condition((ldaTwrl9710.getForm_Tirf_Distr_Category().equals("3"))))
            {
                decideConditionsMet1647++;
                ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Form_Of_Distr().setValue("L");                                                                                   //Natural: ASSIGN #TWRL117A-FORM-OF-DISTR := 'L'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  MUKHERR1
            //*  DASDH1
            if (condition(ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Form_Of_Distr().equals("E") || ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Form_Of_Distr().equals("P")))  //Natural: IF #TWRL117A-FORM-OF-DISTR = 'E' OR = 'P'
            {
                ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Annuity_Cost().setValue(ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll());                                             //Natural: ASSIGN #TWRL117A-ANNUITY-COST := FORM.TIRF-PR-IVC-AMT-ROLL
                //*  MUKHERR1
            }                                                                                                                                                             //Natural: END-IF
            pnd_S_Twrpymnt_Form_Sd.reset();                                                                                                                               //Natural: RESET #S-TWRPYMNT-FORM-SD
            pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year.setValue(pnd_In_Parm_Pnd_In_Tax_Yearn);                                                                        //Natural: MOVE #IN-TAX-YEARN TO #S-TWRPYMNT-TAX-YEAR
            pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr.setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                    //Natural: MOVE FORM.TIRF-TIN TO #S-TWRPYMNT-TAX-ID-NBR
            pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr.setValue(ldaTwrl9710.getForm_Tirf_Contract_Nbr());                                                         //Natural: MOVE FORM.TIRF-CONTRACT-NBR TO #S-TWRPYMNT-CONTRACT-NBR
            pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde.setValue(ldaTwrl9710.getForm_Tirf_Payee_Cde());                                                               //Natural: MOVE FORM.TIRF-PAYEE-CDE TO #S-TWRPYMNT-PAYEE-CDE
            vw_pymnt.startDatabaseRead                                                                                                                                    //Natural: READ PYMNT BY TWRPYMNT-FORM-SD STARTING FROM #S-TWRPYMNT-FORM-SD
            (
            "READ02",
            new Wc[] { new Wc("TWRPYMNT_FORM_SD", ">=", pnd_S_Twrpymnt_Form_Sd, WcType.BY) },
            new Oc[] { new Oc("TWRPYMNT_FORM_SD", "ASC") }
            );
            READ02:
            while (condition(vw_pymnt.readNextRow("READ02")))
            {
                if (condition(pymnt_Twrpymnt_Tax_Year.notEquals(pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year) || pymnt_Twrpymnt_Tax_Id_Nbr.notEquals(pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr)  //Natural: IF PYMNT.TWRPYMNT-TAX-YEAR NE #S-TWRPYMNT-TAX-YEAR OR PYMNT.TWRPYMNT-TAX-ID-NBR NE #S-TWRPYMNT-TAX-ID-NBR OR PYMNT.TWRPYMNT-CONTRACT-NBR NE #S-TWRPYMNT-CONTRACT-NBR OR PYMNT.TWRPYMNT-PAYEE-CDE NE #S-TWRPYMNT-PAYEE-CDE
                    || pymnt_Twrpymnt_Contract_Nbr.notEquals(pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr) || pymnt_Twrpymnt_Payee_Cde.notEquals(pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde)))
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //*  PRIVATE
                }                                                                                                                                                         //Natural: END-IF
                ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Plan_Or_Ann_Type().setValue("P");                                                                                //Natural: ASSIGN #TWRL117A-PLAN-OR-ANN-TYPE := 'P'
                //* *    END-IF
                //*  << MUKHERR1
                //*  NEW REQUIREMENT TO NOT POPULATE ANNUITY COST FOR PVT PLANS WITH
                //*  PARTIAL PAYMENTS
                if (condition(ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Form_Of_Distr().equals("P") && ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Plan_Or_Ann_Type().equals("P"))) //Natural: IF #TWRL117A-FORM-OF-DISTR = 'P' AND #TWRL117A-PLAN-OR-ANN-TYPE EQ 'P'
                {
                    //*  DASDH1
                    ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Annuity_Cost().reset();                                                                                      //Natural: RESET #TWRL117A-ANNUITY-COST
                    //*  >> MUKHERR1                             /* DASDH1
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  ANNUITY
            //*  LUMP SUM
            short decideConditionsMet1693 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF FORM.TIRF-DISTR-CATEGORY;//Natural: VALUE '1'
            if (condition((ldaTwrl9710.getForm_Tirf_Distr_Category().equals("1"))))
            {
                decideConditionsMet1693++;
                ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Form_Of_Distr().setValue("A");                                                                                   //Natural: ASSIGN #TWRL117A-FORM-OF-DISTR := 'A'
            }                                                                                                                                                             //Natural: VALUE '3'
            else if (condition((ldaTwrl9710.getForm_Tirf_Distr_Category().equals("3"))))
            {
                decideConditionsMet1693++;
                ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Form_Of_Distr().setValue("L");                                                                                   //Natural: ASSIGN #TWRL117A-FORM-OF-DISTR := 'L'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
                //*  PRIVATE
            }                                                                                                                                                             //Natural: END-DECIDE
            ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Plan_Or_Ann_Type().setValue("P");                                                                                    //Natural: ASSIGN #TWRL117A-PLAN-OR-ANN-TYPE := 'P'
        }                                                                                                                                                                 //Natural: END-IF
        //*  >> SINHASN
        //*  02/09/10 'N' ALSO AS OF 2009, BUT HOW TO DETERMINE? /* NON-QUALIFIED
        if (condition(ldaTwrl9710.getForm_Tirf_Distribution_Cde().equals("J")))                                                                                           //Natural: IF FORM.TIRF-DISTRIBUTION-CDE = 'J'
        {
            ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Distr_Amt().setValue(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                            //Natural: ASSIGN #TWRL117A-DISTR-AMT := FORM.TIRF-INT-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Distr_Amt().setValue(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                          //Natural: ASSIGN #TWRL117A-DISTR-AMT := FORM.TIRF-GROSS-AMT
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Taxable_Amt().setValue(ldaTwrl9710.getForm_Tirf_Taxable_Amt());                                                          //Natural: ASSIGN #TWRL117A-TAXABLE-AMT := FORM.TIRF-TAXABLE-AMT
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Contr_Aft_Tax().setValue(ldaTwrl9710.getForm_Tirf_Ivc_Amt());                                                            //Natural: ASSIGN #TWRL117A-CONTR-AFT-TAX := FORM.TIRF-IVC-AMT
        //*  480.7C DISASTER   >>
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().greaterOrEqual("2020")))                                                                                        //Natural: IF FORM.TIRF-TAX-YEAR GE '2020'
        {
            //*  DISTRIB CODE FOR DISASTER
            if (condition(ldaTwrl9710.getForm_Tirf_Distribution_Cde().equals("N")))                                                                                       //Natural: IF FORM.TIRF-DISTRIBUTION-CDE = 'N'
            {
                ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Distr_Amt().reset();                                                                                             //Natural: RESET #TWRL117A-DISTR-AMT #TWRL117A-TAXABLE-AMT #TWRL117A-CONTR-AFT-TAX #TWRL117A-PREPAID-AMT
                ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Taxable_Amt().reset();
                ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Contr_Aft_Tax().reset();
                ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Prepaid_Amt().reset();
                //*  MAXIMUM EXEMPT $10000
                if (condition(ldaTwrl9710.getForm_Tirf_Taxable_Amt().greaterOrEqual(pnd_Maxm_Exempt_Amt)))                                                                //Natural: IF FORM.TIRF-TAXABLE-AMT GE #MAXM-EXEMPT-AMT
                {
                    ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_A_Exempt().setValue(pnd_Maxm_Exempt_Amt);                                                                    //Natural: ASSIGN #TWRL117A-A-EXEMPT := #MAXM-EXEMPT-AMT
                    ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_B_Taxable().compute(new ComputeParameters(false, ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_B_Taxable()),      //Natural: ASSIGN #TWRL117A-B-TAXABLE := FORM.TIRF-TAXABLE-AMT - #MAXM-EXEMPT-AMT
                        ldaTwrl9710.getForm_Tirf_Taxable_Amt().subtract(pnd_Maxm_Exempt_Amt));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_A_Exempt().setValue(ldaTwrl9710.getForm_Tirf_Taxable_Amt());                                                 //Natural: ASSIGN #TWRL117A-A-EXEMPT := FORM.TIRF-TAXABLE-AMT
                    ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_B_Taxable().setValue(0);                                                                                     //Natural: ASSIGN #TWRL117A-B-TAXABLE := 0
                }                                                                                                                                                         //Natural: END-IF
                ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_C_Amt_Prep().setValue(0);                                                                                        //Natural: ASSIGN #TWRL117A-C-AMT-PREP := 0
                ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_D_Aft_Tax_Cntr().setValue(ldaTwrl9710.getForm_Tirf_Ivc_Amt());                                                   //Natural: ASSIGN #TWRL117A-D-AFT-TAX-CNTR := FORM.TIRF-IVC-AMT
                ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_E_Total().compute(new ComputeParameters(false, ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_E_Total()),              //Natural: ADD #TWRL117A-A-EXEMPT #TWRL117A-B-TAXABLE #TWRL117A-D-AFT-TAX-CNTR TO #TWRL117A-E-TOTAL
                    ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_E_Total().add(ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_A_Exempt()).add(ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_B_Taxable()).add(ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_D_Aft_Tax_Cntr()));
                ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Tax_Wth_Elg_Dist().setValue(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                           //Natural: ASSIGN #TWRL117A-TAX-WTH-ELG-DIST := FORM.TIRF-FED-TAX-WTHLD
            }                                                                                                                                                             //Natural: END-IF
            //*  02/04/09
            //* MUKHERR1
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Distr_Cde().setValue(ldaTwrl9710.getForm_Tirf_Distribution_Cde());                                                       //Natural: ASSIGN #TWRL117A-DISTR-CDE := FORM.TIRF-DISTRIBUTION-CDE
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Rollover_Distr().setValue(ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll());                                                 //Natural: ASSIGN #TWRL117A-ROLLOVER-DISTR := FORM.TIRF-PR-GROSS-AMT-ROLL
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Ein().setValue("000000000");                                                                                             //Natural: ASSIGN #TWRL117A-EIN := '000000000'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Plan_Nm().setValue(ldaTwrl9710.getForm_Tirf_Plan_Name());                                                                //Natural: ASSIGN #TWRL117A-PLAN-NM := TIRF-PLAN-NAME
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Plan_Spnsr().setValue(ldaTwrl9710.getForm_Tirf_Plan_Spnsr_Name());                                                       //Natural: ASSIGN #TWRL117A-PLAN-SPNSR := TIRF-PLAN-SPNSR-NAME
        if (condition(ldaTwrl9710.getForm_Tirf_Plan_Name().equals(" ")))                                                                                                  //Natural: IF FORM.TIRF-PLAN-NAME = ' '
        {
            ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Plan_Nm().setValue("TIAA CLIENT PLAN");                                                                              //Natural: ASSIGN #TWRL117A-PLAN-NM := 'TIAA CLIENT PLAN'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Plan().getValue(1).equals("MLTPLN")))                                                                                      //Natural: IF FORM.TIRF-PLAN ( 1 ) = 'MLTPLN'
        {
            ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Plan_Nm().setValue("TIAA CLIENT SPONSOR PLAN");                                                                      //Natural: ASSIGN #TWRL117A-PLAN-NM := 'TIAA CLIENT SPONSOR PLAN'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Plan_Spnsr_Name().equals(" ")))                                                                                            //Natural: IF FORM.TIRF-PLAN-SPNSR-NAME = ' '
        {
            ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Plan_Spnsr().setValue("TIAA CLIENT PLAN SPONSOR");                                                                   //Natural: ASSIGN #TWRL117A-PLAN-SPNSR := 'TIAA CLIENT PLAN SPONSOR'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().greaterOrEqual("2017")))                                                                                        //Natural: IF FORM.TIRF-TAX-YEAR GE '2017'
        {
            ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Plan_Or_Ann_Type().setValue("P");                                                                                    //Natural: ASSIGN #TWRL117A-PLAN-OR-ANN-TYPE := 'P'
        }                                                                                                                                                                 //Natural: END-IF
        //* *************** DASDH1 CHANGES END **************************
        getWorkFiles().write(1, false, ldaTwrl117c.getPnd_Twrl117c());                                                                                                    //Natural: WRITE WORK FILE 1 #TWRL117C
        pnd_Work_Area_Pnd_Ws_Num_Of_Doc.nadd(1);                                                                                                                          //Natural: ADD 1 TO #WS-NUM-OF-DOC
        //*  NO WITHLD FOR TIAA
        pnd_Work_Area_Pnd_Ws_Amt_Withld.reset();                                                                                                                          //Natural: RESET #WS-AMT-WITHLD
        pnd_Work_Area_Pnd_Ws_Amout_Paid.nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                                       //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-AMOUT-PAID
        pnd_Work_Area_Pnd_Ws_Amout_Paid.nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                         //Natural: ADD FORM.TIRF-INT-AMT TO #WS-AMOUT-PAID
        //*                                                    END 01/28/08  RM
    }
    private void sub_Company_Break() throws Exception                                                                                                                     //Natural: COMPANY-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        //*  ================================
        if (condition(pnd_Work_Area_Pnd_Counter.greater(1)))                                                                                                              //Natural: IF #COUNTER GT 1
        {
            //*  02-08-2005
            getReports().write(1, pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name()," Gross:        ",pnd_Work_Area_Pnd_Ws_Gross.getValue(1), new ReportEditMask          //Natural: WRITE ( 1 ) #TWRACOM2.#COMP-SHORT-NAME ' Gross:        ' #WS-GROSS ( 1 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-GROSS ( 2 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) 3X #WS-GROSS ( 3 ) ( EM = ZZZZ,ZZZ,ZZ9.99 )
                ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Gross.getValue(2), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Work_Area_Pnd_Ws_Gross.getValue(3), 
                new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            //*  02-08-2005
            getReports().write(1, pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name()," Interest:     ",pnd_Work_Area_Pnd_Ws_Int.getValue(1), new ReportEditMask            //Natural: WRITE ( 1 ) #TWRACOM2.#COMP-SHORT-NAME ' Interest:     ' #WS-INT ( 1 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-INT ( 2 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) 3X #WS-INT ( 3 ) ( EM = ZZZZ,ZZZ,ZZ9.99 )
                ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Int.getValue(2), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Work_Area_Pnd_Ws_Int.getValue(3), 
                new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            //*  02/04/09
            //*  02/04/09
            //*  02/04/09
            //*  02/04/09
            getReports().write(1, pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name()," Contributions:",pnd_Work_Area_Pnd_Ws_Roll_Cont.getValue(1), new                     //Natural: WRITE ( 1 ) #TWRACOM2.#COMP-SHORT-NAME ' Contributions:' #WS-ROLL-CONT ( 1 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-ROLL-CONT ( 2 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) 3X #WS-ROLL-CONT ( 3 ) ( EM = ZZZZ,ZZZ,ZZ9.99 )
                ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Roll_Cont.getValue(2), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Work_Area_Pnd_Ws_Roll_Cont.getValue(3), 
                new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            //*  02/04/09
            //*  02/04/09
            //*  02/04/09
            //*  02/04/09
            getReports().write(1, pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name()," Distributions:",pnd_Work_Area_Pnd_Ws_Roll_Dist.getValue(1), new                     //Natural: WRITE ( 1 ) #TWRACOM2.#COMP-SHORT-NAME ' Distributions:' #WS-ROLL-DIST ( 1 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-ROLL-DIST ( 2 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) 3X #WS-ROLL-DIST ( 3 ) ( EM = ZZZZ,ZZZ,ZZ9.99 )
                ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Roll_Dist.getValue(2), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Work_Area_Pnd_Ws_Roll_Dist.getValue(3), 
                new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            //*  02-08-2005
            getReports().write(1, pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name()," Forms:        ",new TabSetting(26),pnd_Work_Area_Pnd_Ws_Cnt.getValue(1),new         //Natural: WRITE ( 1 ) #TWRACOM2.#COMP-SHORT-NAME ' Forms:        ' 26T #WS-CNT ( 1 ) 42T #WS-CNT ( 2 ) 60T #WS-CNT ( 3 )
                TabSetting(42),pnd_Work_Area_Pnd_Ws_Cnt.getValue(2),new TabSetting(60),pnd_Work_Area_Pnd_Ws_Cnt.getValue(3));
            if (Global.isEscape()) return;
            getReports().write(1, "* -","-",new RepeatItem(70));                                                                                                          //Natural: WRITE ( 1 ) '* -' '-' ( 70 )
            if (Global.isEscape()) return;
            //*  02/04/09
            //*  02/04/09
            getReports().write(2, NEWLINE,NEWLINE,"**Total",pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),"Accepted:",pnd_Work_Area_Pnd_Ws_Cnt.getValue(1),pnd_Work_Area_Pnd_Ws_Gross.getValue(1),  //Natural: WRITE ( 2 ) // '**Total' #TWRACOM2.#COMP-SHORT-NAME 'Accepted:' #WS-CNT ( 1 ) #WS-GROSS ( 1 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-INT ( 1 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-ROLL-CONT ( 1 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-ROLL-DIST ( 1 ) ( EM = ZZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Int.getValue(1), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Roll_Cont.getValue(1), 
                new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Roll_Dist.getValue(1), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            //*  02/04/09
            //*  02/04/09
            getReports().write(3, NEWLINE,NEWLINE,"**Total",pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),"Rejected:",pnd_Work_Area_Pnd_Ws_Cnt.getValue(2),pnd_Work_Area_Pnd_Ws_Gross.getValue(2),  //Natural: WRITE ( 3 ) // '**Total' #TWRACOM2.#COMP-SHORT-NAME 'Rejected:' #WS-CNT ( 2 ) #WS-GROSS ( 2 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-INT ( 2 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-ROLL-CONT ( 2 ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) #WS-ROLL-DIST ( 2 ) ( EM = ZZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Int.getValue(2), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Roll_Cont.getValue(2), 
                new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),pnd_Work_Area_Pnd_Ws_Roll_Dist.getValue(2), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            //*  02/04/09
            pnd_Work_Area_Pnd_Ws_Cnt.getValue("*").reset();                                                                                                               //Natural: RESET #WS-CNT ( * ) #WS-GROSS ( * ) #WS-INT ( * ) #WS-ROLL-CONT ( * ) #WS-ROLL-DIST ( * )
            pnd_Work_Area_Pnd_Ws_Gross.getValue("*").reset();
            pnd_Work_Area_Pnd_Ws_Int.getValue("*").reset();
            pnd_Work_Area_Pnd_Ws_Roll_Cont.getValue("*").reset();
            pnd_Work_Area_Pnd_Ws_Roll_Dist.getValue("*").reset();
        }                                                                                                                                                                 //Natural: END-IF
        //*  02-08-2005
        if (condition(! (pnd_Work_Area_Pnd_Last.getBoolean())))                                                                                                           //Natural: IF NOT #LAST
        {
            pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().setValue(pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn);                                                                   //Natural: ASSIGN #TWRACOM2.#TAX-YEAR := #TIRF-TAX-YEARN
            //*  02-08-2005
            //*  TIAA          /* RC 02/07/2013
            //*  02-08-2005
            if (condition(pnd_Work_Area_Pnd_First.getBoolean()))                                                                                                          //Natural: IF #FIRST
            {
                pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue("T");                                                                                                //Natural: ASSIGN #TWRACOM2.#COMP-CODE := 'T'
                pnd_Work_Area_Pnd_First.setValue(false);                                                                                                                  //Natural: ASSIGN #FIRST := FALSE
                //*  02-08-2005
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "=",ldaTwrl9710.getForm_Tirf_Company_Cde());                                                                                        //Natural: WRITE '=' FORM.TIRF-COMPANY-CDE
                if (Global.isEscape()) return;
                pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue(ldaTwrl9710.getForm_Tirf_Company_Cde());                                                             //Natural: ASSIGN #TWRACOM2.#COMP-CODE := FORM.TIRF-COMPANY-CDE
            }                                                                                                                                                             //Natural: END-IF
            pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(5);                                                                                                      //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 5
            DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOM2' #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
            pnd_Work_Area_Pnd_Old_Cmpny.setValue(ldaTwrl9710.getForm_Tirf_Company_Cde());                                                                                 //Natural: ASSIGN #OLD-CMPNY := FORM.TIRF-COMPANY-CDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  01/28/08 RM
    //*  02/04/09
    //*  02/04/09
    private void sub_Display_Accepted_Form() throws Exception                                                                                                             //Natural: DISPLAY-ACCEPTED-FORM
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(2, "/ TIN ",                                                                                                                                 //Natural: DISPLAY ( 2 ) '/ TIN ' FORM.TIRF-TIN 'TIN/Typ' FORM.TIRF-TAX-ID-TYPE '/Contract' FORM.TIRF-CONTRACT-NBR '/Payee' FORM.TIRF-PAYEE-CDE '  Gross/  Amt' FORM.TIRF-GROSS-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) '/ Interest' FORM.TIRF-INT-AMT ( EM = ZZZ,ZZ9.99 ) 'Rollover/Distribution' FORM.TIRF-PR-GROSS-AMT-ROLL ( EM = -ZZZ,ZZZ,ZZ9.99 ) 'Rollover/Contribution' FORM.TIRF-PR-IVC-AMT-ROLL ( EM = -ZZZ,ZZZ,ZZ9.99 ) 'Control/Number' #TWRL117A-CONTROL-NUMBER
        		ldaTwrl9710.getForm_Tirf_Tin(),"TIN/Typ",
        		ldaTwrl9710.getForm_Tirf_Tax_Id_Type(),"/Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"/Payee",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"  Gross/  Amt",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"/ Interest",
        		ldaTwrl9710.getForm_Tirf_Int_Amt(), new ReportEditMask ("ZZZ,ZZ9.99"),"Rollover/Distribution",
        		ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"Rollover/Contribution",
        		ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"Control/Number",
        		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Control_Number());
        if (Global.isEscape()) return;
        pnd_Work_Area_Pnd_Ws_Cnt.getValue(1).nadd(1);                                                                                                                     //Natural: ADD 1 TO #WS-CNT ( 1 )
        pnd_Work_Area_Pnd_Ws_Cnt.getValue(3).nadd(1);                                                                                                                     //Natural: ADD 1 TO #WS-CNT ( 3 )
        pnd_Work_Area_Pnd_Ws_Gross.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                                //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS ( 1 )
        pnd_Work_Area_Pnd_Ws_Gross.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                                //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS ( 3 )
        pnd_Work_Area_Pnd_Ws_Int.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                    //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT ( 1 )
        pnd_Work_Area_Pnd_Ws_Int.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                    //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT ( 3 )
        //*  02/04/09
        pnd_Work_Area_Pnd_Ws_Roll_Cont.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll());                                                                      //Natural: ADD FORM.TIRF-PR-IVC-AMT-ROLL TO #WS-ROLL-CONT ( 1 )
        //*  02/04/09
        pnd_Work_Area_Pnd_Ws_Roll_Cont.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll());                                                                      //Natural: ADD FORM.TIRF-PR-IVC-AMT-ROLL TO #WS-ROLL-CONT ( 3 )
        //*  02/04/09
        pnd_Work_Area_Pnd_Ws_Roll_Dist.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll());                                                                    //Natural: ADD FORM.TIRF-PR-GROSS-AMT-ROLL TO #WS-ROLL-DIST ( 1 )
        //*  02/04/09
        pnd_Work_Area_Pnd_Ws_Roll_Dist.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll());                                                                    //Natural: ADD FORM.TIRF-PR-GROSS-AMT-ROLL TO #WS-ROLL-DIST ( 3 )
        pnd_Work_Area_Pnd_Ws_Cnt_Tot.getValue(1).nadd(1);                                                                                                                 //Natural: ADD 1 TO #WS-CNT-TOT ( 1 )
        pnd_Work_Area_Pnd_Ws_Cnt_Tot.getValue(3).nadd(1);                                                                                                                 //Natural: ADD 1 TO #WS-CNT-TOT ( 3 )
        pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                            //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS-TOT ( 1 )
        pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                            //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS-TOT ( 3 )
        pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT-TOT ( 1 )
        pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT-TOT ( 3 )
        //*  02/04/09
        pnd_Work_Area_Pnd_Ws_Roll_Cont_Tot.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll());                                                                  //Natural: ADD FORM.TIRF-PR-IVC-AMT-ROLL TO #WS-ROLL-CONT-TOT ( 1 )
        //*  02/04/09
        pnd_Work_Area_Pnd_Ws_Roll_Cont_Tot.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll());                                                                  //Natural: ADD FORM.TIRF-PR-IVC-AMT-ROLL TO #WS-ROLL-CONT-TOT ( 3 )
        //*  02/04/09
        pnd_Work_Area_Pnd_Ws_Roll_Dist_Tot.getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll());                                                                //Natural: ADD FORM.TIRF-PR-GROSS-AMT-ROLL TO #WS-ROLL-DIST-TOT ( 1 )
        //*  02/04/09
        pnd_Work_Area_Pnd_Ws_Roll_Dist_Tot.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll());                                                                //Natural: ADD FORM.TIRF-PR-GROSS-AMT-ROLL TO #WS-ROLL-DIST-TOT ( 3 )
        //*  DISPLAY-ACCEPTED
    }
    private void sub_Write_Reject_Report() throws Exception                                                                                                               //Natural: WRITE-REJECT-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ===================================
        pnd_Work_Area_Pnd_Rej_Count.nadd(1);                                                                                                                              //Natural: ADD 1 TO #REJ-COUNT
        pnd_Work_Area_Pnd_Temp_Cntl_Num.compute(new ComputeParameters(false, pnd_Work_Area_Pnd_Temp_Cntl_Num), ldaTwrl9710.getForm_Tirf_Pr_Cntl_Num().val());             //Natural: ASSIGN #TEMP-CNTL-NUM := VAL ( TIRF-PR-CNTL-NUM )
        //*  01/28/08 RM
        //*  02/04/09
        //*  02/04/09
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Control_Number().setValueEdited(pnd_Work_Area_Pnd_Temp_Cntl_Num,new ReportEditMask("Z999999999"));                       //Natural: MOVE EDITED #TEMP-CNTL-NUM ( EM = Z999999999 ) TO #TWRL117A-CONTROL-NUMBER
        getReports().display(3, "/ TIN ",                                                                                                                                 //Natural: DISPLAY ( 3 ) '/ TIN ' FORM.TIRF-TIN 'TIN/Typ' FORM.TIRF-TAX-ID-TYPE '/Contract' FORM.TIRF-CONTRACT-NBR '/Payee' FORM.TIRF-PAYEE-CDE '  Gross/  Amt' FORM.TIRF-GROSS-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) '/ Interest' FORM.TIRF-INT-AMT ( EM = ZZZ,ZZ9.99 ) 'Rollover/Distribution' FORM.TIRF-PR-GROSS-AMT-ROLL ( EM = -ZZZ,ZZZ,ZZ9.99 ) 'Rollover/Contribution' FORM.TIRF-PR-IVC-AMT-ROLL ( EM = -ZZZ,ZZZ,ZZ9.99 ) 'Control/Number' #TWRL117A-CONTROL-NUMBER
        		ldaTwrl9710.getForm_Tirf_Tin(),"TIN/Typ",
        		ldaTwrl9710.getForm_Tirf_Tax_Id_Type(),"/Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"/Payee",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"  Gross/  Amt",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"/ Interest",
        		ldaTwrl9710.getForm_Tirf_Int_Amt(), new ReportEditMask ("ZZZ,ZZ9.99"),"Rollover/Distribution",
        		ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"Rollover/Contribution",
        		ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"Control/Number",
        		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Control_Number());
        if (Global.isEscape()) return;
        pnd_Work_Area_Pnd_Ws_Cnt.getValue(2).nadd(1);                                                                                                                     //Natural: ADD 1 TO #WS-CNT ( 2 )
        pnd_Work_Area_Pnd_Ws_Cnt.getValue(3).nadd(1);                                                                                                                     //Natural: ADD 1 TO #WS-CNT ( 3 )
        pnd_Work_Area_Pnd_Ws_Gross.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                                //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS ( 2 )
        pnd_Work_Area_Pnd_Ws_Gross.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                                //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS ( 3 )
        pnd_Work_Area_Pnd_Ws_Int.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                    //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT ( 2 )
        pnd_Work_Area_Pnd_Ws_Int.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                    //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT ( 3 )
        //*  02/04/09
        pnd_Work_Area_Pnd_Ws_Roll_Cont.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll());                                                                      //Natural: ADD FORM.TIRF-PR-IVC-AMT-ROLL TO #WS-ROLL-CONT ( 2 )
        //*  02/04/09
        pnd_Work_Area_Pnd_Ws_Roll_Cont.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll());                                                                      //Natural: ADD FORM.TIRF-PR-IVC-AMT-ROLL TO #WS-ROLL-CONT ( 3 )
        //*  02/04/09
        pnd_Work_Area_Pnd_Ws_Roll_Dist.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll());                                                                    //Natural: ADD FORM.TIRF-PR-GROSS-AMT-ROLL TO #WS-ROLL-DIST ( 2 )
        //*  02/04/09
        pnd_Work_Area_Pnd_Ws_Roll_Dist.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll());                                                                    //Natural: ADD FORM.TIRF-PR-GROSS-AMT-ROLL TO #WS-ROLL-DIST ( 3 )
        pnd_Work_Area_Pnd_Ws_Cnt_Tot.getValue(2).nadd(1);                                                                                                                 //Natural: ADD 1 TO #WS-CNT-TOT ( 2 )
        pnd_Work_Area_Pnd_Ws_Cnt_Tot.getValue(3).nadd(1);                                                                                                                 //Natural: ADD 1 TO #WS-CNT-TOT ( 3 )
        pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                            //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS-TOT ( 2 )
        pnd_Work_Area_Pnd_Ws_Gross_Tot.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                            //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-GROSS-TOT ( 3 )
        pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT-TOT ( 2 )
        pnd_Work_Area_Pnd_Ws_Int_Tot.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                //Natural: ADD FORM.TIRF-INT-AMT TO #WS-INT-TOT ( 3 )
        //*  02/04/09
        pnd_Work_Area_Pnd_Ws_Roll_Cont_Tot.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll());                                                                  //Natural: ADD FORM.TIRF-PR-IVC-AMT-ROLL TO #WS-ROLL-CONT-TOT ( 2 )
        //*  02/04/09
        pnd_Work_Area_Pnd_Ws_Roll_Cont_Tot.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll());                                                                  //Natural: ADD FORM.TIRF-PR-IVC-AMT-ROLL TO #WS-ROLL-CONT-TOT ( 3 )
        //*  02/04/09
        pnd_Work_Area_Pnd_Ws_Roll_Dist_Tot.getValue(2).nadd(ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll());                                                                //Natural: ADD FORM.TIRF-PR-GROSS-AMT-ROLL TO #WS-ROLL-DIST-TOT ( 2 )
        //*  02/04/09
        pnd_Work_Area_Pnd_Ws_Roll_Dist_Tot.getValue(3).nadd(ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll());                                                                //Natural: ADD FORM.TIRF-PR-GROSS-AMT-ROLL TO #WS-ROLL-DIST-TOT ( 3 )
        //*  WRITE-REJECT-REPORT
    }
    private void sub_Write_Summary_Rec() throws Exception                                                                                                                 //Natural: WRITE-SUMMARY-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*  =================================
        //*    01/28/08  RM
        ldaTwrl117c.getPnd_Twrl117c().reset();                                                                                                                            //Natural: RESET #TWRL117C
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Control_Number().setValue(" 000000000");                                                                               //Natural: ASSIGN #TWRL117A-S-CONTROL-NUMBER := ' 000000000'
        //*  480.7C  01/28/08  RM
        if (condition(pnd_Tirf_Superde_3_Pnd_Tirf_Form_Type.equals(5)))                                                                                                   //Natural: IF #TIRF-FORM-TYPE = 5
        {
            ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Form_Type().setValue("Y");                                                                                         //Natural: ASSIGN #TWRL117A-S-FORM-TYPE := 'Y'
            //*  ELSE
            //*   #TWRL117A-S-FORM-TYPE      := 3              /* 480.6B  01/28/08  RM
            //*  SUMMARY
            //*  PAULS
            //*  02/20/07  RM
            //*  FEIN = 1, VIKRAM
            //*  02-08-2005
            //*  02-2013
            //*  02-08-2005
            //*  02-08-2005
            //*  02-08-2005
            //*  02-08-2005
            //*  CORPORATION = C, VIKRAM 2018
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Rec_Type().setValue(2);                                                                                                //Natural: ASSIGN #TWRL117A-S-REC-TYPE := 2
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Doc_Type().setValue("O");                                                                                              //Natural: ASSIGN #TWRL117A-S-DOC-TYPE := 'O'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Tax_Year().compute(new ComputeParameters(false, ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Tax_Year()),                //Natural: ASSIGN #TWRL117A-S-TAX-YEAR := VAL ( FORM.TIRF-TAX-YEAR )
            ldaTwrl9710.getForm_Tirf_Tax_Year().val());
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Payers_Id_Typ().setValue("1");                                                                                         //Natural: ASSIGN #TWRL117A-S-PAYERS-ID-TYP := '1'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Payers_Id().compute(new ComputeParameters(false, ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Payers_Id()),              //Natural: ASSIGN #TWRL117A-S-PAYERS-ID := VAL ( #S-OUTPUT-DATA.#FED-ID )
            pnd_S_Output_Data_Pnd_Fed_Id.val());
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Payers_Name().setValue(pnd_S_Output_Data_Pnd_Comp_Name);                                                               //Natural: ASSIGN #TWRL117A-S-PAYERS-NAME := #S-OUTPUT-DATA.#COMP-NAME
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Payers_Address_1().setValue(pnd_S_Output_Data_Pnd_Address.getValue(1));                                                //Natural: ASSIGN #TWRL117A-S-PAYERS-ADDRESS-1 := #S-OUTPUT-DATA.#ADDRESS ( 1 )
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Payers_Town().setValue(pnd_S_Output_Data_Pnd_City);                                                                    //Natural: ASSIGN #TWRL117A-S-PAYERS-TOWN := #S-OUTPUT-DATA.#CITY
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Payers_State().setValue(pnd_S_Output_Data_Pnd_State);                                                                  //Natural: ASSIGN #TWRL117A-S-PAYERS-STATE := #S-OUTPUT-DATA.#STATE
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip_Full().setValue(pnd_S_Output_Data_Pnd_Zip_9);                                                               //Natural: ASSIGN #TWRL117A-S-PAYERS-ZIP-FULL := #S-OUTPUT-DATA.#ZIP-9
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Num_Of_Doc().setValue(pnd_Work_Area_Pnd_Ws_Num_Of_Doc);                                                                //Natural: ASSIGN #TWRL117A-S-NUM-OF-DOC := #WS-NUM-OF-DOC
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Amt_Withld().setValue(pnd_Work_Area_Pnd_Ws_Amt_Withld);                                                                //Natural: ASSIGN #TWRL117A-S-AMT-WITHLD := #WS-AMT-WITHLD
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Amout_Paid().setValue(pnd_Work_Area_Pnd_Ws_Amout_Paid);                                                                //Natural: ASSIGN #TWRL117A-S-AMOUT-PAID := #WS-AMOUT-PAID
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Taxpayer_Type().setValue("C");                                                                                         //Natural: ASSIGN #TWRL117A-S-TAXPAYER-TYPE := 'C'
        //*  SPACES          02/28/08  RM
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Fill5().reset();                                                                                                       //Natural: RESET #TWRL117A-S-FILL5 #TWRL117A-S-FILL6 #TWRL117A-S-FILL7 #TWRL117A-S-FILL8 #TWRL117A-S-FILL9 #TWRL117A-S-FILL10 #TWRL117A-S-FILL11 #TWRL117A-S-FILL12 #TWRL117A-S-FILL13
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Fill6().reset();
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Fill7().reset();
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Fill8().reset();
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Fill9().reset();
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Fill10().reset();
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Fill11().reset();
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Fill12().reset();
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Fill13().reset();
        //*  #TWRL117A-S-FILL14                                  /* PAULS
        //*  << SINHASN
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().greaterOrEqual("2016")))                                                                                        //Natural: IF FORM.TIRF-TAX-YEAR GE '2016'
        {
            ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_S_Cntl_Nbr().setValue(0);                                                                                            //Natural: ASSIGN #TWRL117A-S-CNTL-NBR := 000000000
        }                                                                                                                                                                 //Natural: END-IF
        //*  >> SINHASN
        //*     01/28/08  RM
        getWorkFiles().write(1, false, ldaTwrl117c.getPnd_Twrl117c());                                                                                                    //Natural: WRITE WORK FILE 1 #TWRL117C
    }
    private void sub_Write_Header_Rec() throws Exception                                                                                                                  //Natural: WRITE-HEADER-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaTwrl117c.getPnd_Twrl117c().reset();                                                                                                                            //Natural: RESET #TWRL117C
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Rec_Id().setValue("SU");                                                                                                 //Natural: ASSIGN #TWRL117H-REC-ID := 'SU'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Submtr_Ein().compute(new ComputeParameters(false, ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Submtr_Ein()),                //Natural: ASSIGN #TWRL117H-SUBMTR-EIN := VAL ( #S-OUTPUT-DATA.#FED-ID )
            pnd_S_Output_Data_Pnd_Fed_Id.val());
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Resub_Ind().setValue(0);                                                                                                 //Natural: ASSIGN #TWRL117H-RESUB-IND := 0
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Software_Cde().setValue(98);                                                                                             //Natural: ASSIGN #TWRL117H-SOFTWARE-CDE := 98
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Comp_Name().setValue(pnd_S_Output_Data_Pnd_Comp_Name);                                                                   //Natural: ASSIGN #TWRL117H-COMP-NAME := #S-OUTPUT-DATA.#COMP-NAME
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Comp_Loc().setValue(pnd_S_Output_Data_Pnd_Address.getValue(1));                                                          //Natural: ASSIGN #TWRL117H-COMP-LOC := #S-OUTPUT-DATA.#ADDRESS ( 1 )
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Delivery_Addr().setValue(pnd_S_Output_Data_Pnd_Address.getValue(1));                                                     //Natural: ASSIGN #TWRL117H-DELIVERY-ADDR := #S-OUTPUT-DATA.#ADDRESS ( 1 )
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_City().setValue(pnd_S_Output_Data_Pnd_City);                                                                             //Natural: ASSIGN #TWRL117H-CITY := #S-OUTPUT-DATA.#CITY
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_State().setValue(pnd_S_Output_Data_Pnd_State);                                                                           //Natural: ASSIGN #TWRL117H-STATE := #S-OUTPUT-DATA.#STATE
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Zip_Full().setValue(pnd_S_Output_Data_Pnd_Zip_9);                                                                        //Natural: ASSIGN #TWRL117H-ZIP-FULL := #S-OUTPUT-DATA.#ZIP-9
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Name().setValue(pnd_S_Output_Data_Pnd_Comp_Name);                                                                 //Natural: ASSIGN #TWRL117H-SBMTER-NAME := #S-OUTPUT-DATA.#COMP-NAME
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Loc().setValue(pnd_S_Output_Data_Pnd_Address.getValue(1));                                                        //Natural: ASSIGN #TWRL117H-SBMTER-LOC := #S-OUTPUT-DATA.#ADDRESS ( 1 )
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Addr().setValue(pnd_S_Output_Data_Pnd_Address.getValue(1));                                                       //Natural: ASSIGN #TWRL117H-SBMTER-ADDR := #S-OUTPUT-DATA.#ADDRESS ( 1 )
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_City().setValue(pnd_S_Output_Data_Pnd_City);                                                                      //Natural: ASSIGN #TWRL117H-SBMTER-CITY := #S-OUTPUT-DATA.#CITY
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_State().setValue(pnd_S_Output_Data_Pnd_State);                                                                    //Natural: ASSIGN #TWRL117H-SBMTER-STATE := #S-OUTPUT-DATA.#STATE
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip_Full().setValue(pnd_S_Output_Data_Pnd_Zip_9);                                                                 //Natural: ASSIGN #TWRL117H-SBMTER-ZIP-FULL := #S-OUTPUT-DATA.#ZIP-9
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Contact_Name().setValue(pnd_S_Output_Data_Pnd_Contact_Name);                                                             //Natural: ASSIGN #TWRL117H-CONTACT-NAME := #S-OUTPUT-DATA.#CONTACT-NAME
        //* #TWRL117H-CONTACT-PHONE        := #S-OUTPUT-DATA.#PHONE
        //* *#TWRL117H-CONTACT-PHONE        := 7049884307          /* 2/3/15 DUTTAD
        //*  << SINHASN
        //*  MOVE '7049884307' TO #TEMP-CONTACT-PHONE  /* 2/3/15 DUTTAD
        //*  MOVE '87753539104654' TO #TEMP-CONTACT-PHONE  /* SK
        //*  SK
        //*  2/3/15 DUTTAD
        //*  FEIN = 1 ,  480.7C.1,  VIKRAM
        //*  02/02/15 MUKHERR
        //*  02/02/15 MUKHERR
        //*  02/02/15 MUKHERR
        pnd_Temp_Contact_Phone.setValue("87753539102344");                                                                                                                //Natural: MOVE '87753539102344' TO #TEMP-CONTACT-PHONE
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Contact_Phone().setValue(pnd_Temp_Contact_Phone_Pnd_Temp_Contact_Phone_N);                                               //Natural: ASSIGN #TWRL117H-CONTACT-PHONE := #TEMP-CONTACT-PHONE-N
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Contact_Email().setValue(pnd_S_Output_Data_Pnd_Contact_Email_Addr);                                                      //Natural: ASSIGN #TWRL117H-CONTACT-EMAIL := #S-OUTPUT-DATA.#CONTACT-EMAIL-ADDR
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Mthd_Prblm_Notice().setValue(2);                                                                                         //Natural: ASSIGN #TWRL117H-MTHD-PRBLM-NOTICE := 2
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Prepares_Code().setValue("P");                                                                                           //Natural: ASSIGN #TWRL117H-PREPARES-CODE := 'P'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Submtr_Iden_No_Typ().setValue("1");                                                                                      //Natural: ASSIGN #TWRL117H-SUBMTR-IDEN-NO-TYP := '1'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_St().setValue(" ");                                                                                         //Natural: ASSIGN #TWRL117H-SBMTER-FORGN-ST := ' '
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_Post().setValue(" ");                                                                                       //Natural: ASSIGN #TWRL117H-SBMTER-FORGN-POST := ' '
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_St_1().setValue(" ");                                                                                       //Natural: ASSIGN #TWRL117H-SBMTER-FORGN-ST-1 := ' '
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_Post_1().setValue(" ");                                                                                     //Natural: ASSIGN #TWRL117H-SBMTER-FORGN-POST-1 := ' '
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Country_Code_1().setValue(" ");                                                                                   //Natural: ASSIGN #TWRL117H-SBMTER-COUNTRY-CODE-1 := ' '
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Country_Code().setValue(" ");                                                                                     //Natural: ASSIGN #TWRL117H-SBMTER-COUNTRY-CODE := ' '
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Contact_Phone_Ext().setValue(" ");                                                                                       //Natural: ASSIGN #TWRL117H-CONTACT-PHONE-EXT := ' '
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Contact_Fax().setValue(" ");                                                                                             //Natural: ASSIGN #TWRL117H-CONTACT-FAX := ' '
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Fill2().setValue(" ");                                                                                                   //Natural: ASSIGN #TWRL117H-FILL2 := ' '
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Fill3().setValue(" ");                                                                                                   //Natural: ASSIGN #TWRL117H-FILL3 := ' '
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Fill4().setValue(" ");                                                                                                   //Natural: ASSIGN #TWRL117H-FILL4 := ' '
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Fill5().setValue(" ");                                                                                                   //Natural: ASSIGN #TWRL117H-FILL5 := ' '
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Fill6().setValue(" ");                                                                                                   //Natural: ASSIGN #TWRL117H-FILL6 := ' '
        getWorkFiles().write(1, false, ldaTwrl117c.getPnd_Twrl117c());                                                                                                    //Natural: WRITE WORK FILE 1 #TWRL117C
    }
    //*  << 01/29/2015 MUKHERR
    private void sub_Write_Pa_Rec() throws Exception                                                                                                                      //Natural: WRITE-PA-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------
        //*  VIKRAM 2018
        //*  NEEDS TO BE CONFIRMED
        //*  VIKRAM , FEIN = 1
        ldaTwrl117c.getPnd_Twrl117c().reset();                                                                                                                            //Natural: RESET #TWRL117C
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_Rec_Id().setValue("PA");                                                                                                 //Natural: ASSIGN #TWRL117P-REC-ID := 'PA'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_Tax_Year().setValue(pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn);                                                              //Natural: ASSIGN #TWRL117P-TAX-YEAR := #TIRF-TAX-YEARN
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_Agent_Ind_Cde().setValue(1);                                                                                             //Natural: ASSIGN #TWRL117P-AGENT-IND-CDE := 1
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_Ein().setValue(131624203);                                                                                               //Natural: ASSIGN #TWRL117P-EIN := 131624203
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_Frm_Type().setValue("Y");                                                                                                //Natural: ASSIGN #TWRL117P-FRM-TYPE := 'Y'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_Est_Num().setValue(" ");                                                                                                 //Natural: ASSIGN #TWRL117P-EST-NUM := ' '
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_File_Type().setValue("O");                                                                                               //Natural: ASSIGN #TWRL117P-FILE-TYPE := 'O'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_Filler1().setValue(" ");                                                                                                 //Natural: ASSIGN #TWRL117P-FILLER1 := ' '
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_Emp_Name().setValue("TEACHERS INSURANCE AND ANNUITY ASSOC");                                                             //Natural: ASSIGN #TWRL117P-EMP-NAME := 'TEACHERS INSURANCE AND ANNUITY ASSOC'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_Loc_Add().setValue("8500 ANDREW CARNEGIE BLVD");                                                                         //Natural: ASSIGN #TWRL117P-LOC-ADD := '8500 ANDREW CARNEGIE BLVD'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_Del_Add().setValue("8500 ANDREW CARNEGIE BLVD");                                                                         //Natural: ASSIGN #TWRL117P-DEL-ADD := '8500 ANDREW CARNEGIE BLVD'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_City().setValue("CHARLOTTE");                                                                                            //Natural: ASSIGN #TWRL117P-CITY := 'CHARLOTTE'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_State().setValue("NC");                                                                                                  //Natural: ASSIGN #TWRL117P-STATE := 'NC'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_Zip1().setValue(28262);                                                                                                  //Natural: ASSIGN #TWRL117P-ZIP1 := 28262
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_Zip2().setValue(8500);                                                                                                   //Natural: ASSIGN #TWRL117P-ZIP2 := 8500
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_Filler2().setValue(" ");                                                                                                 //Natural: ASSIGN #TWRL117P-FILLER2 := ' '
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_Frgn_State_Prvnc().setValue(" ");                                                                                        //Natural: ASSIGN #TWRL117P-FRGN-STATE-PRVNC := ' '
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_Frgn_Postal_Cde().setValue(" ");                                                                                         //Natural: ASSIGN #TWRL117P-FRGN-POSTAL-CDE := ' '
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_Cntry_Cde().setValue(" ");                                                                                               //Natural: ASSIGN #TWRL117P-CNTRY-CDE := ' '
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_Cntct_Email().setValue("Kristine.Levine@tiaa.org");                                                                      //Natural: ASSIGN #TWRL117P-CNTCT-EMAIL := 'Kristine.Levine@tiaa.org'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_Filler3().setValue(" ");                                                                                                 //Natural: ASSIGN #TWRL117P-FILLER3 := ' '
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117p_Agent_Type_Id().setValue("1");                                                                                           //Natural: ASSIGN #TWRL117P-AGENT-TYPE-ID := '1'
        getWorkFiles().write(1, false, ldaTwrl117c.getPnd_Twrl117c());                                                                                                    //Natural: WRITE WORK FILE 1 #TWRL117C
        //*  >> 01/29/2015 MUKHERR
    }
    //*  VIKRAM ADDED
    private void sub_Write_Trailer_Record() throws Exception                                                                                                              //Natural: WRITE-TRAILER-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------
        //*  FILLER+CONTROL N0
        //*  480.7C.1
        //*  1 = DETAIL RECORD
        //*  O = ORIGINAL
        //*  1 = FEIN
        //*  BLANK
        ldaTwrl117c.getPnd_Twrl117c().reset();                                                                                                                            //Natural: RESET #TWRL117C
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Control_Nbr().setValue(0);                                                                                               //Natural: ASSIGN #TWRL117T-CONTROL-NBR := 0
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Form_Type().setValue("R");                                                                                               //Natural: ASSIGN #TWRL117T-FORM-TYPE := 'R'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Rec_Type().setValue("1");                                                                                                //Natural: ASSIGN #TWRL117T-REC-TYPE := '1'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Doc_Type().setValue("O");                                                                                                //Natural: ASSIGN #TWRL117T-DOC-TYPE := 'O'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Tax_Year().setValue(pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Yearn);                                                              //Natural: ASSIGN #TWRL117T-TAX-YEAR := #TIRF-TAX-YEARN
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Payers_Id_Type().setValue("1");                                                                                          //Natural: ASSIGN #TWRL117T-PAYERS-ID-TYPE := '1'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Typ_Industry_Busins().setValue(" ");                                                                                     //Natural: ASSIGN #TWRL117T-TYP-INDUSTRY-BUSINS := ' '
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Indentification_No().compute(new ComputeParameters(false, ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Indentification_No()),  //Natural: ASSIGN #TWRL117T-INDENTIFICATION-NO := VAL ( #S-OUTPUT-DATA.#FED-ID )
            pnd_S_Output_Data_Pnd_Fed_Id.val());
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Busins_Name().setValue(pnd_S_Output_Data_Pnd_Comp_Name);                                                                 //Natural: ASSIGN #TWRL117T-BUSINS-NAME := #S-OUTPUT-DATA.#COMP-NAME
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Withhld_Agent_Nme().setValue(pnd_S_Output_Data_Pnd_Comp_Name);                                                           //Natural: ASSIGN #TWRL117T-WITHHLD-AGENT-NME := #S-OUTPUT-DATA.#COMP-NAME
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Agent_Telephone_A().setValue(pnd_S_Output_Data_Pnd_Phone.getSubstring(1,10));                                            //Natural: ASSIGN #TWRL117T-AGENT-TELEPHONE-A := SUBSTR ( #S-OUTPUT-DATA.#PHONE,1,10 )
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Agent_Address_1().setValue(pnd_S_Output_Data_Pnd_Address.getValue(1));                                                   //Natural: ASSIGN #TWRL117T-AGENT-ADDRESS-1 := #S-OUTPUT-DATA.#ADDRESS ( 1 )
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Agent_Address_2().setValue(pnd_S_Output_Data_Pnd_Address.getValue(2));                                                   //Natural: ASSIGN #TWRL117T-AGENT-ADDRESS-2 := #S-OUTPUT-DATA.#ADDRESS ( 2 )
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Agent_Town().setValue(pnd_S_Output_Data_Pnd_City);                                                                       //Natural: ASSIGN #TWRL117T-AGENT-TOWN := #S-OUTPUT-DATA.#CITY
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Agent_State().setValue(pnd_S_Output_Data_Pnd_State);                                                                     //Natural: ASSIGN #TWRL117T-AGENT-STATE := #S-OUTPUT-DATA.#STATE
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Agent_Zip_Full().compute(new ComputeParameters(false, ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Agent_Zip_Full()),        //Natural: ASSIGN #TWRL117T-AGENT-ZIP-FULL := VAL ( #S-OUTPUT-DATA.#ZIP-9 )
            pnd_S_Output_Data_Pnd_Zip_9.val());
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Physical_Address_1().setValue(pnd_S_Output_Data_Pnd_Address.getValue(1));                                                //Natural: ASSIGN #TWRL117T-PHYSICAL-ADDRESS-1 := #S-OUTPUT-DATA.#ADDRESS ( 1 )
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Physical_Address_2().setValue(pnd_S_Output_Data_Pnd_Address.getValue(2));                                                //Natural: ASSIGN #TWRL117T-PHYSICAL-ADDRESS-2 := #S-OUTPUT-DATA.#ADDRESS ( 2 )
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_A_Town().setValue(pnd_S_Output_Data_Pnd_City);                                                                           //Natural: ASSIGN #TWRL117T-A-TOWN := #S-OUTPUT-DATA.#CITY
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_A_State().setValue(pnd_S_Output_Data_Pnd_State);                                                                         //Natural: ASSIGN #TWRL117T-A-STATE := #S-OUTPUT-DATA.#STATE
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_A_Zip_Full().compute(new ComputeParameters(false, ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_A_Zip_Full()),                //Natural: ASSIGN #TWRL117T-A-ZIP-FULL := VAL ( #S-OUTPUT-DATA.#ZIP-9 )
            pnd_S_Output_Data_Pnd_Zip_9.val());
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Chng_Of_Address().setValue("N");                                                                                         //Natural: ASSIGN #TWRL117T-CHNG-OF-ADDRESS := 'N'
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Agent_Email().setValue(pnd_S_Output_Data_Pnd_Contact_Email_Addr);                                                        //Natural: ASSIGN #TWRL117T-AGENT-EMAIL := #S-OUTPUT-DATA.#CONTACT-EMAIL-ADDR
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Prdc_Pymnt_Quali_Gov().setValue(0);                                                                                      //Natural: ASSIGN #TWRL117T-PRDC-PYMNT-QUALI-GOV := 0.00
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Lumpsum_Distri20().setValue(0);                                                                                          //Natural: ASSIGN #TWRL117T-LUMPSUM-DISTRI20 := 0.00
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Lumpsum_Distri10().setValue(0);                                                                                          //Natural: ASSIGN #TWRL117T-LUMPSUM-DISTRI10 := 0.00
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Distri_Nonqualified().setValue(0);                                                                                       //Natural: ASSIGN #TWRL117T-DISTRI-NONQUALIFIED := 0.00
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Othr_Incm_Qualifd10().setValue(0);                                                                                       //Natural: ASSIGN #TWRL117T-OTHR-INCM-QUALIFD10 := 0.00
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Annuity_Cost().setValue(0);                                                                                              //Natural: ASSIGN #TWRL117T-ANNUITY-COST := 0.00
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Rolovr_Qual_Nondtira().setValue(0);                                                                                      //Natural: ASSIGN #TWRL117T-ROLOVR-QUAL-NONDTIRA := 0.00
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Distri_Rtrmnt_Sav10().setValue(0);                                                                                       //Natural: ASSIGN #TWRL117T-DISTRI-RTRMNT-SAV10 := 0.00
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Rlvr_Non_Dtira10().setValue(0);                                                                                          //Natural: ASSIGN #TWRL117T-RLVR-NON-DTIRA10 := 0.00
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Nonres_Dsitri().setValue(0);                                                                                             //Natural: ASSIGN #TWRL117T-NONRES-DSITRI := 0.00
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Other_Distribution().setValue(0);                                                                                        //Natural: ASSIGN #TWRL117T-OTHER-DISTRIBUTION := 0.00
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Econo_Ergncy_Maria().setValue(0);                                                                                        //Natural: ASSIGN #TWRL117T-ECONO-ERGNCY-MARIA := 0.00
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Total().setValue(0);                                                                                                     //Natural: ASSIGN #TWRL117T-TOTAL := 0
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Total_Forms().setValue(pnd_Work_Area_Pnd_Counter);                                                                       //Natural: ASSIGN #TWRL117T-TOTAL-FORMS := #COUNTER
        pnd_Spaces.setValue(" ");                                                                                                                                         //Natural: ASSIGN #SPACES := ' '
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Fill1().setValue(pnd_Spaces);                                                                                            //Natural: MOVE #SPACES TO #TWRL117T-FILL1
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Fill2().setValue(pnd_Spaces);                                                                                            //Natural: MOVE #SPACES TO #TWRL117T-FILL2
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Fill3().setValue(pnd_Spaces);                                                                                            //Natural: MOVE #SPACES TO #TWRL117T-FILL3
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Fill4().setValue(pnd_Spaces);                                                                                            //Natural: MOVE #SPACES TO #TWRL117T-FILL4
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Fill5().setValue(pnd_Spaces);                                                                                            //Natural: MOVE #SPACES TO #TWRL117T-FILL5
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Fill6().setValue(pnd_Spaces);                                                                                            //Natural: MOVE #SPACES TO #TWRL117T-FILL6
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Fill7().setValue(pnd_Spaces);                                                                                            //Natural: MOVE #SPACES TO #TWRL117T-FILL7
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Fill8().setValue(pnd_Spaces);                                                                                            //Natural: MOVE #SPACES TO #TWRL117T-FILL8
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Reason_For_Chg().setValue(pnd_Spaces);                                                                                   //Natural: MOVE #SPACES TO #TWRL117T-REASON-FOR-CHG
        ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Fillr1a().setValue(0);                                                                                                   //Natural: ASSIGN #TWRL117T-FILLR1A := 0
        getWorkFiles().write(1, false, ldaTwrl117c.getPnd_Twrl117c());                                                                                                    //Natural: WRITE WORK FILE 1 #TWRL117C
        //*  WRITE-TRAILER-RECORD  ENDS
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=56 LS=133 ES=ON");
        Global.format(1, "PS=60 LS=133 ES=ON");
        Global.format(2, "PS=60 LS=133 ES=ON");
        Global.format(3, "PS=60 LS=133 ES=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:IIAP"),new TabSetting(28),"TaxWaRS - Annual Original Tax Report - Totals  ",new 
            TabSetting(92),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(34),pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year,"- Puerto Rico - Form 480.7C",NEWLINE,NEWLINE,NEWLINE,new TabSetting(26),"  Accepted       Rejected            Total",NEWLINE,new 
            TabSetting(26),"  --------       --------            -----");
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:IIAP"),new TabSetting(28),"TaxWaRS - Annual Original Tax Report - Accepted",new 
            TabSetting(92),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(32),pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year,"-",pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),"- Puerto Rico - Form 480.7C",NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:IIAP"),new TabSetting(28),"TaxWaRS - Annual Original Tax Report - Rejected",new 
            TabSetting(92),"Page:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(32),pnd_Tirf_Superde_3_Pnd_Tirf_Tax_Year,"-",pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),"- Puerto Rico - Form 480.7C",NEWLINE);

        getReports().setDisplayColumns(2, "/ TIN ",
        		ldaTwrl9710.getForm_Tirf_Tin(),"TIN/Typ",
        		ldaTwrl9710.getForm_Tirf_Tax_Id_Type(),"/Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"/Payee",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"  Gross/  Amt",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"/ Interest",
        		ldaTwrl9710.getForm_Tirf_Int_Amt(), new ReportEditMask ("ZZZ,ZZ9.99"),"Rollover/Distribution",
        		ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"Rollover/Contribution",
        		ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"Control/Number",
        		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Control_Number());
        getReports().setDisplayColumns(3, "/ TIN ",
        		ldaTwrl9710.getForm_Tirf_Tin(),"TIN/Typ",
        		ldaTwrl9710.getForm_Tirf_Tax_Id_Type(),"/Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"/Payee",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"  Gross/  Amt",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"/ Interest",
        		ldaTwrl9710.getForm_Tirf_Int_Amt(), new ReportEditMask ("ZZZ,ZZ9.99"),"Rollover/Distribution",
        		ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"Rollover/Contribution",
        		ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"Control/Number",
        		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Control_Number());
    }
}
