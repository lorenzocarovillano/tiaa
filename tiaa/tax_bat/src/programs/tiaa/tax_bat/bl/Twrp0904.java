/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:31:44 PM
**        * FROM NATURAL PROGRAM : Twrp0904
************************************************************
**        * FILE NAME            : Twrp0904.java
**        * CLASS NAME           : Twrp0904
**        * INSTANCE NAME        : Twrp0904
************************************************************
********************************************************************
* PROGRAM : TWRP0904
* FUNCTION: PRODUCE MONTHLY MANAGEMENT REPORT (MMR)
* INPUT   : FLAT FILE CREATED BY TWRP0901
* OUTPUT  : MMR REPORT
* AUTHOR  :
* HISTORY :
*         :
* 12/02/14: O SOTTO FATCA CHANGES MARKED 11/2014.
********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0904 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0901 ldaTwrl0901;
    private LdaTwrl0902 ldaTwrl0902;
    private PdaTwratbl2 pdaTwratbl2;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Sort_Rec;
    private DbsField pnd_Sort_Rec_Pnd_Sort_Rec_Key;

    private DbsGroup pnd_Sort_Rec__R_Field_1;
    private DbsField pnd_Sort_Rec_Pnd_Ff3_Residency_Type;
    private DbsField pnd_Sort_Rec_Pnd_Ff3_Residency_Code;
    private DbsField pnd_Sort_Rec_Pnd_Ff3_Pymnt_Date;

    private DbsGroup pnd_Sort_Rec__R_Field_2;
    private DbsField pnd_Sort_Rec_Pnd_Ff3_Pymnt_Yyyymm;

    private DbsGroup pnd_Sort_Rec__R_Field_3;
    private DbsField pnd_Sort_Rec_Pnd_Ff3_Pymnt_Yyyy;
    private DbsField pnd_Sort_Rec_Pnd_Ff3_Company_Code;

    private DbsGroup pnd_Sort_Rec__R_Field_4;
    private DbsField pnd_Sort_Rec__Filler1;
    private DbsField pnd_Sort_Rec_Pnd_Ff3_Resdnce_Yyyy;
    private DbsField pnd_Sort_Rec_Pnd_Ff3_Distribution_Cde;
    private DbsField pnd_Sort_Rec_Pnd_Ff3_Intfce_Dte;
    private DbsField pnd_Sort_Rec_Pnd_Ff3_Source_Code;
    private DbsField pnd_Sort_Rec_Pnd_Ff3_Gross_Amt;
    private DbsField pnd_Sort_Rec_Pnd_Ff3_Fed_Wthld_Amt;
    private DbsField pnd_Sort_Rec_Pnd_Ff3_Nra_Wthld_Amt;
    private DbsField pnd_Sort_Rec_Pnd_Ff3_Can_Wthld_Amt;
    private DbsField pnd_Sort_Rec_Pnd_Ff3_State_Wthld;
    private DbsField pnd_Sort_Rec_Pnd_Ff3_Twrpymnt_Fatca_Ind;
    private DbsField pnd_Comp;
    private DbsField pnd_I_Tiaa;
    private DbsField pnd_I_Total;
    private DbsField pnd_Federal_Tax;
    private DbsField pnd_Nra_Tax;
    private DbsField pnd_Fatca_Tax;
    private DbsField pnd_Canadian_Tax;
    private DbsField pnd_State_Tax;
    private DbsField pnd_Puerto_Rico_Tax;
    private DbsField pnd_Pay_Date;

    private DbsGroup pnd_St_Pay_Date;
    private DbsField pnd_St_Pay_Date_Pnd_St_Pay_Yyyymm;
    private DbsField pnd_St_Federal_Tax;
    private DbsField pnd_St_Nra_Tax;
    private DbsField pnd_St_Fatca_Tax;
    private DbsField pnd_St_Canadian_Tax;
    private DbsField pnd_St_State_Tax;
    private DbsField pnd_St_Puerto_Rico_Tax;
    private DbsField pnd_Company_Lit;
    private DbsField pnd_Pay_Yyyy;
    private DbsField pnd_State_Code;
    private DbsField pnd_State_Name;
    private DbsField pnd_Pay_D;
    private DbsField pnd_I;
    private DbsField pnd_C;
    private DbsField pnd_Rec_Ritten;
    private DbsField pnd_Rec_Read;
    private DbsField pnd_Canadian_Found;
    private DbsField pnd_Trade_Date_A;

    private DbsGroup pnd_Trade_Date_A__R_Field_5;
    private DbsField pnd_Trade_Date_A_Pnd_Trade_Date_D;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Pnd_Ff3_Pymnt_DateOld;
    private DbsField sort01Pnd_Ff3_Pymnt_YyyyOld;
    private DbsField sort01Pnd_Ff3_Residency_CodeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0901 = new LdaTwrl0901();
        registerRecord(ldaTwrl0901);
        ldaTwrl0902 = new LdaTwrl0902();
        registerRecord(ldaTwrl0902);
        localVariables = new DbsRecord();
        pdaTwratbl2 = new PdaTwratbl2(localVariables);

        // Local Variables

        pnd_Sort_Rec = localVariables.newGroupInRecord("pnd_Sort_Rec", "#SORT-REC");
        pnd_Sort_Rec_Pnd_Sort_Rec_Key = pnd_Sort_Rec.newFieldInGroup("pnd_Sort_Rec_Pnd_Sort_Rec_Key", "#SORT-REC-KEY", FieldType.STRING, 12);

        pnd_Sort_Rec__R_Field_1 = pnd_Sort_Rec.newGroupInGroup("pnd_Sort_Rec__R_Field_1", "REDEFINE", pnd_Sort_Rec_Pnd_Sort_Rec_Key);
        pnd_Sort_Rec_Pnd_Ff3_Residency_Type = pnd_Sort_Rec__R_Field_1.newFieldInGroup("pnd_Sort_Rec_Pnd_Ff3_Residency_Type", "#FF3-RESIDENCY-TYPE", FieldType.STRING, 
            1);
        pnd_Sort_Rec_Pnd_Ff3_Residency_Code = pnd_Sort_Rec__R_Field_1.newFieldInGroup("pnd_Sort_Rec_Pnd_Ff3_Residency_Code", "#FF3-RESIDENCY-CODE", FieldType.STRING, 
            2);
        pnd_Sort_Rec_Pnd_Ff3_Pymnt_Date = pnd_Sort_Rec__R_Field_1.newFieldInGroup("pnd_Sort_Rec_Pnd_Ff3_Pymnt_Date", "#FF3-PYMNT-DATE", FieldType.STRING, 
            8);

        pnd_Sort_Rec__R_Field_2 = pnd_Sort_Rec__R_Field_1.newGroupInGroup("pnd_Sort_Rec__R_Field_2", "REDEFINE", pnd_Sort_Rec_Pnd_Ff3_Pymnt_Date);
        pnd_Sort_Rec_Pnd_Ff3_Pymnt_Yyyymm = pnd_Sort_Rec__R_Field_2.newFieldInGroup("pnd_Sort_Rec_Pnd_Ff3_Pymnt_Yyyymm", "#FF3-PYMNT-YYYYMM", FieldType.NUMERIC, 
            6);

        pnd_Sort_Rec__R_Field_3 = pnd_Sort_Rec__R_Field_1.newGroupInGroup("pnd_Sort_Rec__R_Field_3", "REDEFINE", pnd_Sort_Rec_Pnd_Ff3_Pymnt_Date);
        pnd_Sort_Rec_Pnd_Ff3_Pymnt_Yyyy = pnd_Sort_Rec__R_Field_3.newFieldInGroup("pnd_Sort_Rec_Pnd_Ff3_Pymnt_Yyyy", "#FF3-PYMNT-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Sort_Rec_Pnd_Ff3_Company_Code = pnd_Sort_Rec__R_Field_1.newFieldInGroup("pnd_Sort_Rec_Pnd_Ff3_Company_Code", "#FF3-COMPANY-CODE", FieldType.STRING, 
            1);

        pnd_Sort_Rec__R_Field_4 = pnd_Sort_Rec.newGroupInGroup("pnd_Sort_Rec__R_Field_4", "REDEFINE", pnd_Sort_Rec_Pnd_Sort_Rec_Key);
        pnd_Sort_Rec__Filler1 = pnd_Sort_Rec__R_Field_4.newFieldInGroup("pnd_Sort_Rec__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Sort_Rec_Pnd_Ff3_Resdnce_Yyyy = pnd_Sort_Rec__R_Field_4.newFieldInGroup("pnd_Sort_Rec_Pnd_Ff3_Resdnce_Yyyy", "#FF3-RESDNCE-YYYY", FieldType.STRING, 
            6);
        pnd_Sort_Rec_Pnd_Ff3_Distribution_Cde = pnd_Sort_Rec.newFieldInGroup("pnd_Sort_Rec_Pnd_Ff3_Distribution_Cde", "#FF3-DISTRIBUTION-CDE", FieldType.STRING, 
            2);
        pnd_Sort_Rec_Pnd_Ff3_Intfce_Dte = pnd_Sort_Rec.newFieldInGroup("pnd_Sort_Rec_Pnd_Ff3_Intfce_Dte", "#FF3-INTFCE-DTE", FieldType.STRING, 8);
        pnd_Sort_Rec_Pnd_Ff3_Source_Code = pnd_Sort_Rec.newFieldInGroup("pnd_Sort_Rec_Pnd_Ff3_Source_Code", "#FF3-SOURCE-CODE", FieldType.STRING, 4);
        pnd_Sort_Rec_Pnd_Ff3_Gross_Amt = pnd_Sort_Rec.newFieldInGroup("pnd_Sort_Rec_Pnd_Ff3_Gross_Amt", "#FF3-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 
            2);
        pnd_Sort_Rec_Pnd_Ff3_Fed_Wthld_Amt = pnd_Sort_Rec.newFieldInGroup("pnd_Sort_Rec_Pnd_Ff3_Fed_Wthld_Amt", "#FF3-FED-WTHLD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Sort_Rec_Pnd_Ff3_Nra_Wthld_Amt = pnd_Sort_Rec.newFieldInGroup("pnd_Sort_Rec_Pnd_Ff3_Nra_Wthld_Amt", "#FF3-NRA-WTHLD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Sort_Rec_Pnd_Ff3_Can_Wthld_Amt = pnd_Sort_Rec.newFieldInGroup("pnd_Sort_Rec_Pnd_Ff3_Can_Wthld_Amt", "#FF3-CAN-WTHLD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Sort_Rec_Pnd_Ff3_State_Wthld = pnd_Sort_Rec.newFieldInGroup("pnd_Sort_Rec_Pnd_Ff3_State_Wthld", "#FF3-STATE-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Sort_Rec_Pnd_Ff3_Twrpymnt_Fatca_Ind = pnd_Sort_Rec.newFieldInGroup("pnd_Sort_Rec_Pnd_Ff3_Twrpymnt_Fatca_Ind", "#FF3-TWRPYMNT-FATCA-IND", FieldType.STRING, 
            1);
        pnd_Comp = localVariables.newFieldInRecord("pnd_Comp", "#COMP", FieldType.NUMERIC, 1);
        pnd_I_Tiaa = localVariables.newFieldInRecord("pnd_I_Tiaa", "#I-TIAA", FieldType.NUMERIC, 2);
        pnd_I_Total = localVariables.newFieldInRecord("pnd_I_Total", "#I-TOTAL", FieldType.NUMERIC, 2);
        pnd_Federal_Tax = localVariables.newFieldArrayInRecord("pnd_Federal_Tax", "#FEDERAL-TAX", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            3, 1, 8));
        pnd_Nra_Tax = localVariables.newFieldArrayInRecord("pnd_Nra_Tax", "#NRA-TAX", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 3, 1, 
            8));
        pnd_Fatca_Tax = localVariables.newFieldArrayInRecord("pnd_Fatca_Tax", "#FATCA-TAX", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            3, 1, 8));
        pnd_Canadian_Tax = localVariables.newFieldArrayInRecord("pnd_Canadian_Tax", "#CANADIAN-TAX", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            3, 1, 8));
        pnd_State_Tax = localVariables.newFieldArrayInRecord("pnd_State_Tax", "#STATE-TAX", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            3, 1, 8));
        pnd_Puerto_Rico_Tax = localVariables.newFieldArrayInRecord("pnd_Puerto_Rico_Tax", "#PUERTO-RICO-TAX", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            3, 1, 8));
        pnd_Pay_Date = localVariables.newFieldInRecord("pnd_Pay_Date", "#PAY-DATE", FieldType.STRING, 8);

        pnd_St_Pay_Date = localVariables.newGroupArrayInRecord("pnd_St_Pay_Date", "#ST-PAY-DATE", new DbsArrayController(1, 24));
        pnd_St_Pay_Date_Pnd_St_Pay_Yyyymm = pnd_St_Pay_Date.newFieldInGroup("pnd_St_Pay_Date_Pnd_St_Pay_Yyyymm", "#ST-PAY-YYYYMM", FieldType.NUMERIC, 
            6);
        pnd_St_Federal_Tax = localVariables.newFieldArrayInRecord("pnd_St_Federal_Tax", "#ST-FEDERAL-TAX", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            25, 1, 8));
        pnd_St_Nra_Tax = localVariables.newFieldArrayInRecord("pnd_St_Nra_Tax", "#ST-NRA-TAX", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            25, 1, 8));
        pnd_St_Fatca_Tax = localVariables.newFieldArrayInRecord("pnd_St_Fatca_Tax", "#ST-FATCA-TAX", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            25, 1, 8));
        pnd_St_Canadian_Tax = localVariables.newFieldArrayInRecord("pnd_St_Canadian_Tax", "#ST-CANADIAN-TAX", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            25, 1, 8));
        pnd_St_State_Tax = localVariables.newFieldArrayInRecord("pnd_St_State_Tax", "#ST-STATE-TAX", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            25, 1, 8));
        pnd_St_Puerto_Rico_Tax = localVariables.newFieldArrayInRecord("pnd_St_Puerto_Rico_Tax", "#ST-PUERTO-RICO-TAX", FieldType.PACKED_DECIMAL, 13, 2, 
            new DbsArrayController(1, 25, 1, 8));
        pnd_Company_Lit = localVariables.newFieldArrayInRecord("pnd_Company_Lit", "#COMPANY-LIT", FieldType.STRING, 8, new DbsArrayController(1, 8));
        pnd_Pay_Yyyy = localVariables.newFieldInRecord("pnd_Pay_Yyyy", "#PAY-YYYY", FieldType.NUMERIC, 4);
        pnd_State_Code = localVariables.newFieldInRecord("pnd_State_Code", "#STATE-CODE", FieldType.STRING, 19);
        pnd_State_Name = localVariables.newFieldInRecord("pnd_State_Name", "#STATE-NAME", FieldType.STRING, 19);
        pnd_Pay_D = localVariables.newFieldInRecord("pnd_Pay_D", "#PAY-D", FieldType.DATE);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.PACKED_DECIMAL, 3);
        pnd_Rec_Ritten = localVariables.newFieldInRecord("pnd_Rec_Ritten", "#REC-RITTEN", FieldType.NUMERIC, 9);
        pnd_Rec_Read = localVariables.newFieldInRecord("pnd_Rec_Read", "#REC-READ", FieldType.NUMERIC, 9);
        pnd_Canadian_Found = localVariables.newFieldInRecord("pnd_Canadian_Found", "#CANADIAN-FOUND", FieldType.BOOLEAN, 1);
        pnd_Trade_Date_A = localVariables.newFieldInRecord("pnd_Trade_Date_A", "#TRADE-DATE-A", FieldType.STRING, 4);

        pnd_Trade_Date_A__R_Field_5 = localVariables.newGroupInRecord("pnd_Trade_Date_A__R_Field_5", "REDEFINE", pnd_Trade_Date_A);
        pnd_Trade_Date_A_Pnd_Trade_Date_D = pnd_Trade_Date_A__R_Field_5.newFieldInGroup("pnd_Trade_Date_A_Pnd_Trade_Date_D", "#TRADE-DATE-D", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Pnd_Ff3_Pymnt_DateOld = internalLoopRecord.newFieldInRecord("Sort01_Pnd_Ff3_Pymnt_Date_OLD", "Pnd_Ff3_Pymnt_Date_OLD", FieldType.STRING, 
            8);
        sort01Pnd_Ff3_Pymnt_YyyyOld = internalLoopRecord.newFieldInRecord("Sort01_Pnd_Ff3_Pymnt_Yyyy_OLD", "Pnd_Ff3_Pymnt_Yyyy_OLD", FieldType.NUMERIC, 
            4);
        sort01Pnd_Ff3_Residency_CodeOld = internalLoopRecord.newFieldInRecord("Sort01_Pnd_Ff3_Residency_Code_OLD", "Pnd_Ff3_Residency_Code_OLD", FieldType.STRING, 
            2);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaTwrl0901.initializeValues();
        ldaTwrl0902.initializeValues();

        localVariables.reset();
        pnd_I_Tiaa.setInitialValue(6);
        pnd_I_Total.setInitialValue(8);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Twrp0904() throws Exception
    {
        super("Twrp0904");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: FORMAT ( 2 ) PS = 60 LS = 132
        getWorkFiles().read(2, ldaTwrl0901.getPnd_Flat_File1());                                                                                                          //Natural: READ WORK FILE 2 ONCE #FLAT-FILE1
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            getReports().write(0, " ************************************ ",NEWLINE," ***                              *** ",NEWLINE," ***    CONTROL RECORD IS EMPTY   *** ", //Natural: WRITE ' ************************************ ' / ' ***                              *** ' / ' ***    CONTROL RECORD IS EMPTY   *** ' / ' ***    PLEASE INFORM SYSTEMS !!  *** ' / ' ***                              *** ' / ' ************************************ '
                NEWLINE," ***    PLEASE INFORM SYSTEMS !!  *** ",NEWLINE," ***                              *** ",NEWLINE," ************************************ ");
            if (Global.isEscape()) return;
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-ENDFILE
        pnd_Company_Lit.getValue(1).setValue("CREF");                                                                                                                     //Natural: ASSIGN #COMPANY-LIT ( 1 ) := 'CREF'
        pnd_Company_Lit.getValue(2).setValue("FSB");                                                                                                                      //Natural: ASSIGN #COMPANY-LIT ( 2 ) := 'FSB'
        pnd_Company_Lit.getValue(3).setValue("LIFE");                                                                                                                     //Natural: ASSIGN #COMPANY-LIT ( 3 ) := 'LIFE'
        pnd_Company_Lit.getValue(4).setValue("TCII");                                                                                                                     //Natural: ASSIGN #COMPANY-LIT ( 4 ) := 'TCII'
        pnd_Company_Lit.getValue(5).setValue("TIAA");                                                                                                                     //Natural: ASSIGN #COMPANY-LIT ( 5 ) := 'TIAA'
        pnd_Company_Lit.getValue(6).setValue("SubTotal");                                                                                                                 //Natural: ASSIGN #COMPANY-LIT ( 6 ) := 'SubTotal'
        pnd_Company_Lit.getValue(7).setValue("TRST");                                                                                                                     //Natural: ASSIGN #COMPANY-LIT ( 7 ) := 'TRST'
        pnd_Company_Lit.getValue(8).setValue("Total");                                                                                                                    //Natural: ASSIGN #COMPANY-LIT ( 8 ) := 'Total'
        //*  11/2014
        //*  11/2014
        pnd_Federal_Tax.getValue("*","*").reset();                                                                                                                        //Natural: RESET #FEDERAL-TAX ( *,* ) #NRA-TAX ( *,* ) #FATCA-TAX ( *,* ) #CANADIAN-TAX ( *,* ) #STATE-TAX ( *,* ) #PUERTO-RICO-TAX ( *,* ) #ST-PAY-YYYYMM ( * ) #ST-FEDERAL-TAX ( *,* ) #ST-NRA-TAX ( *,* ) #ST-FATCA-TAX ( *,* ) #ST-CANADIAN-TAX ( *,* ) #ST-STATE-TAX ( *,* ) #ST-PUERTO-RICO-TAX ( *,* )
        pnd_Nra_Tax.getValue("*","*").reset();
        pnd_Fatca_Tax.getValue("*","*").reset();
        pnd_Canadian_Tax.getValue("*","*").reset();
        pnd_State_Tax.getValue("*","*").reset();
        pnd_Puerto_Rico_Tax.getValue("*","*").reset();
        pnd_St_Pay_Date_Pnd_St_Pay_Yyyymm.getValue("*").reset();
        pnd_St_Federal_Tax.getValue("*","*").reset();
        pnd_St_Nra_Tax.getValue("*","*").reset();
        pnd_St_Fatca_Tax.getValue("*","*").reset();
        pnd_St_Canadian_Tax.getValue("*","*").reset();
        pnd_St_State_Tax.getValue("*","*").reset();
        pnd_St_Puerto_Rico_Tax.getValue("*","*").reset();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT JUSTIFIED / *PROGRAM 34T 'TAX WITHHOLDING AND REPORTING SYSTEM' 119T 'PAGE' *PAGE-NUMBER ( 1 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 39T 'Monthly Management Report' / 'RUNTIME : ' *TIMX 45T 'Tax Year' #FLAT-FILE1.#FF1-TAX-YEAR // 18X 'Federal        Non-FATCA         Canadian' 12X 'State      Puerto Rico            FATCA' //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT JUSTIFIED / *PROGRAM 34T 'TAX WITHHOLDING AND REPORTING SYSTEM' 119T 'PAGE' *PAGE-NUMBER ( 2 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 39T 'Monthly Management Report' / 'RUNTIME : ' *TIMX 44T 'Monthly Summary' / 45T 'Tax Year' #FLAT-FILE1.#FF1-TAX-YEAR // 18X 'Federal        Non-FATCA         Canadian' 12X 'State      Puerto Rico            FATCA' //
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 #FLAT-FILE3
        while (condition(getWorkFiles().read(1, ldaTwrl0902.getPnd_Flat_File3())))
        {
            pnd_Sort_Rec.setValuesByName(ldaTwrl0902.getPnd_Flat_File3());                                                                                                //Natural: MOVE BY NAME #FLAT-FILE3 TO #SORT-REC
            pnd_Rec_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-READ
            //*   'CREF'
            //*   'FSB'
            //*   'LIFE'
            //*   'TCII'
            //*   'TIAA'
            //*   'TRST'
            //* ****** ??????????
            short decideConditionsMet209 = 0;                                                                                                                             //Natural: DECIDE ON FIRST #SORT-REC.#FF3-COMPANY-CODE;//Natural: VALUE 'C'
            if (condition((pnd_Sort_Rec_Pnd_Ff3_Company_Code.equals("C"))))
            {
                decideConditionsMet209++;
                pnd_Comp.setValue(1);                                                                                                                                     //Natural: ASSIGN #COMP := 1
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pnd_Sort_Rec_Pnd_Ff3_Company_Code.equals("F"))))
            {
                decideConditionsMet209++;
                pnd_Comp.setValue(2);                                                                                                                                     //Natural: ASSIGN #COMP := 2
            }                                                                                                                                                             //Natural: VALUE 'L'
            else if (condition((pnd_Sort_Rec_Pnd_Ff3_Company_Code.equals("L"))))
            {
                decideConditionsMet209++;
                pnd_Comp.setValue(3);                                                                                                                                     //Natural: ASSIGN #COMP := 3
            }                                                                                                                                                             //Natural: VALUE 'S'
            else if (condition((pnd_Sort_Rec_Pnd_Ff3_Company_Code.equals("S"))))
            {
                decideConditionsMet209++;
                pnd_Comp.setValue(4);                                                                                                                                     //Natural: ASSIGN #COMP := 4
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pnd_Sort_Rec_Pnd_Ff3_Company_Code.equals("T"))))
            {
                decideConditionsMet209++;
                pnd_Comp.setValue(5);                                                                                                                                     //Natural: ASSIGN #COMP := 5
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pnd_Sort_Rec_Pnd_Ff3_Company_Code.equals("X"))))
            {
                decideConditionsMet209++;
                pnd_Comp.setValue(7);                                                                                                                                     //Natural: ASSIGN #COMP := 7
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition((pnd_Sort_Rec_Pnd_Ff3_Residency_Type.notEquals("1") || ! ((pnd_Sort_Rec_Pnd_Ff3_Residency_Code.greaterOrEqual("01") && pnd_Sort_Rec_Pnd_Ff3_Residency_Code.lessOrEqual("57")))))) //Natural: IF #SORT-REC.#FF3-RESIDENCY-TYPE NE '1' OR NOT ( #SORT-REC.#FF3-RESIDENCY-CODE = '01' THRU '57' )
            {
                pnd_Federal_Tax.getValue(3,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_Fed_Wthld_Amt);                                                                            //Natural: ADD #SORT-REC.#FF3-FED-WTHLD-AMT TO #FEDERAL-TAX ( 3,#COMP )
                //*  11/2014 START
                if (condition(pnd_Sort_Rec_Pnd_Ff3_Twrpymnt_Fatca_Ind.equals("Y")))                                                                                       //Natural: IF #SORT-REC.#FF3-TWRPYMNT-FATCA-IND = 'Y'
                {
                    pnd_Fatca_Tax.getValue(3,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_Nra_Wthld_Amt);                                                                          //Natural: ADD #SORT-REC.#FF3-NRA-WTHLD-AMT TO #FATCA-TAX ( 3,#COMP )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Nra_Tax.getValue(3,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_Nra_Wthld_Amt);                                                                            //Natural: ADD #SORT-REC.#FF3-NRA-WTHLD-AMT TO #NRA-TAX ( 3,#COMP )
                }                                                                                                                                                         //Natural: END-IF
                //*  11/2014 END
                pnd_Canadian_Tax.getValue(3,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_Can_Wthld_Amt);                                                                           //Natural: ADD #SORT-REC.#FF3-CAN-WTHLD-AMT TO #CANADIAN-TAX ( 3,#COMP )
                pnd_State_Tax.getValue(3,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_State_Wthld);                                                                                //Natural: ADD #SORT-REC.#FF3-STATE-WTHLD TO #STATE-TAX ( 3,#COMP )
                //*                                       #PUERTO-RICO-TAX   (3,#COMP)
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(pnd_Sort_Rec_Pnd_Ff3_Residency_Type.equals("1") && pnd_Sort_Rec_Pnd_Ff3_Residency_Code.greaterOrEqual("01") && pnd_Sort_Rec_Pnd_Ff3_Residency_Code.lessOrEqual("57")))) //Natural: ACCEPT IF #SORT-REC.#FF3-RESIDENCY-TYPE = '1' AND #SORT-REC.#FF3-RESIDENCY-CODE GE '01' AND #SORT-REC.#FF3-RESIDENCY-CODE LE '57'
            {
                continue;
            }
            getSort().writeSortInData(pnd_Sort_Rec_Pnd_Sort_Rec_Key, pnd_Sort_Rec_Pnd_Ff3_Fed_Wthld_Amt, pnd_Sort_Rec_Pnd_Ff3_Nra_Wthld_Amt, pnd_Sort_Rec_Pnd_Ff3_Can_Wthld_Amt,  //Natural: END-ALL
                pnd_Sort_Rec_Pnd_Ff3_State_Wthld, pnd_Sort_Rec_Pnd_Ff3_Twrpymnt_Fatca_Ind);
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  11/2014
        getSort().sortData(pnd_Sort_Rec_Pnd_Sort_Rec_Key);                                                                                                                //Natural: SORT BY #SORT-REC-KEY USING #SORT-REC.#FF3-FED-WTHLD-AMT #SORT-REC.#FF3-NRA-WTHLD-AMT #SORT-REC.#FF3-CAN-WTHLD-AMT #SORT-REC.#FF3-STATE-WTHLD #SORT-REC.#FF3-TWRPYMNT-FATCA-IND
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Sort_Rec_Pnd_Sort_Rec_Key, pnd_Sort_Rec_Pnd_Ff3_Fed_Wthld_Amt, pnd_Sort_Rec_Pnd_Ff3_Nra_Wthld_Amt, 
            pnd_Sort_Rec_Pnd_Ff3_Can_Wthld_Amt, pnd_Sort_Rec_Pnd_Ff3_State_Wthld, pnd_Sort_Rec_Pnd_Ff3_Twrpymnt_Fatca_Ind)))
        {
            CheckAtStartofData246();

            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //*                                                                                                                                                           //Natural: AT END OF DATA
            //*                                                                                                                                                           //Natural: AT BREAK OF #SORT-REC.#FF3-PYMNT-DATE /6/
            //*                                                                                                                                                           //Natural: AT BREAK OF #SORT-REC.#FF3-RESIDENCY-CODE
            pnd_Rec_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-READ
            //*   'CREF'
            //*   'FSB'
            //*   'LIFE'
            //*   'TCII'
            //*   'TIAA'
            //*   'TRST'
            //* ****** ??????????
            short decideConditionsMet336 = 0;                                                                                                                             //Natural: DECIDE ON FIRST #SORT-REC.#FF3-COMPANY-CODE;//Natural: VALUE 'C'
            if (condition((pnd_Sort_Rec_Pnd_Ff3_Company_Code.equals("C"))))
            {
                decideConditionsMet336++;
                pnd_Comp.setValue(1);                                                                                                                                     //Natural: ASSIGN #COMP := 1
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pnd_Sort_Rec_Pnd_Ff3_Company_Code.equals("F"))))
            {
                decideConditionsMet336++;
                pnd_Comp.setValue(2);                                                                                                                                     //Natural: ASSIGN #COMP := 2
            }                                                                                                                                                             //Natural: VALUE 'L'
            else if (condition((pnd_Sort_Rec_Pnd_Ff3_Company_Code.equals("L"))))
            {
                decideConditionsMet336++;
                pnd_Comp.setValue(3);                                                                                                                                     //Natural: ASSIGN #COMP := 3
            }                                                                                                                                                             //Natural: VALUE 'S'
            else if (condition((pnd_Sort_Rec_Pnd_Ff3_Company_Code.equals("S"))))
            {
                decideConditionsMet336++;
                pnd_Comp.setValue(4);                                                                                                                                     //Natural: ASSIGN #COMP := 4
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pnd_Sort_Rec_Pnd_Ff3_Company_Code.equals("T"))))
            {
                decideConditionsMet336++;
                pnd_Comp.setValue(5);                                                                                                                                     //Natural: ASSIGN #COMP := 5
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pnd_Sort_Rec_Pnd_Ff3_Company_Code.equals("X"))))
            {
                decideConditionsMet336++;
                pnd_Comp.setValue(7);                                                                                                                                     //Natural: ASSIGN #COMP := 7
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Federal_Tax.getValue(1,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_Fed_Wthld_Amt);                                                                                //Natural: ADD #SORT-REC.#FF3-FED-WTHLD-AMT TO #FEDERAL-TAX ( 1,#COMP )
            //*  11/2014 START
            if (condition(pnd_Sort_Rec_Pnd_Ff3_Twrpymnt_Fatca_Ind.equals("Y")))                                                                                           //Natural: IF #SORT-REC.#FF3-TWRPYMNT-FATCA-IND = 'Y'
            {
                pnd_Fatca_Tax.getValue(1,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_Nra_Wthld_Amt);                                                                              //Natural: ADD #SORT-REC.#FF3-NRA-WTHLD-AMT TO #FATCA-TAX ( 1,#COMP )
                pnd_Fatca_Tax.getValue(3,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_Nra_Wthld_Amt);                                                                              //Natural: ADD #SORT-REC.#FF3-NRA-WTHLD-AMT TO #FATCA-TAX ( 3,#COMP )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Nra_Tax.getValue(1,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_Nra_Wthld_Amt);                                                                                //Natural: ADD #SORT-REC.#FF3-NRA-WTHLD-AMT TO #NRA-TAX ( 1,#COMP )
                pnd_Nra_Tax.getValue(3,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_Nra_Wthld_Amt);                                                                                //Natural: ADD #SORT-REC.#FF3-NRA-WTHLD-AMT TO #NRA-TAX ( 3,#COMP )
            }                                                                                                                                                             //Natural: END-IF
            //*  11/2014 END
            pnd_Canadian_Tax.getValue(1,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_Can_Wthld_Amt);                                                                               //Natural: ADD #SORT-REC.#FF3-CAN-WTHLD-AMT TO #CANADIAN-TAX ( 1,#COMP )
            pnd_State_Tax.getValue(1,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_State_Wthld);                                                                                    //Natural: ADD #SORT-REC.#FF3-STATE-WTHLD TO #STATE-TAX ( 1,#COMP )
            //*                                       #PUERTO-RICO-TAX (1,#COMP)
            pnd_Federal_Tax.getValue(3,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_Fed_Wthld_Amt);                                                                                //Natural: ADD #SORT-REC.#FF3-FED-WTHLD-AMT TO #FEDERAL-TAX ( 3,#COMP )
            pnd_Canadian_Tax.getValue(3,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_Can_Wthld_Amt);                                                                               //Natural: ADD #SORT-REC.#FF3-CAN-WTHLD-AMT TO #CANADIAN-TAX ( 3,#COMP )
            pnd_State_Tax.getValue(3,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_State_Wthld);                                                                                    //Natural: ADD #SORT-REC.#FF3-STATE-WTHLD TO #STATE-TAX ( 3,#COMP )
            //*                                    TO #PUERTO-RICO-TAX (3,#COMP)
            FOR04:                                                                                                                                                        //Natural: FOR #I 1 24
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(24)); pnd_I.nadd(1))
            {
                if (condition(pnd_St_Pay_Date_Pnd_St_Pay_Yyyymm.getValue(pnd_I).equals(getZero())))                                                                       //Natural: IF #ST-PAY-YYYYMM ( #I ) = 0
                {
                    pnd_St_Pay_Date_Pnd_St_Pay_Yyyymm.getValue(pnd_I).setValue(pnd_Sort_Rec_Pnd_Ff3_Pymnt_Yyyymm);                                                        //Natural: ASSIGN #ST-PAY-YYYYMM ( #I ) := #SORT-REC.#FF3-PYMNT-YYYYMM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_St_Pay_Date_Pnd_St_Pay_Yyyymm.getValue(pnd_I).notEquals(pnd_Sort_Rec_Pnd_Ff3_Pymnt_Yyyymm)))                                            //Natural: IF #ST-PAY-YYYYMM ( #I ) NE #SORT-REC.#FF3-PYMNT-YYYYMM
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_St_Federal_Tax.getValue(pnd_I,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_Fed_Wthld_Amt);                                                                     //Natural: ADD #SORT-REC.#FF3-FED-WTHLD-AMT TO #ST-FEDERAL-TAX ( #I,#COMP )
                //*  11/2014 START
                if (condition(pnd_Sort_Rec_Pnd_Ff3_Twrpymnt_Fatca_Ind.equals("Y")))                                                                                       //Natural: IF #SORT-REC.#FF3-TWRPYMNT-FATCA-IND = 'Y'
                {
                    pnd_St_Fatca_Tax.getValue(pnd_I,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_Nra_Wthld_Amt);                                                                   //Natural: ADD #SORT-REC.#FF3-NRA-WTHLD-AMT TO #ST-FATCA-TAX ( #I,#COMP )
                    pnd_St_Fatca_Tax.getValue(25,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_Nra_Wthld_Amt);                                                                      //Natural: ADD #SORT-REC.#FF3-NRA-WTHLD-AMT TO #ST-FATCA-TAX ( 25,#COMP )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_St_Nra_Tax.getValue(pnd_I,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_Nra_Wthld_Amt);                                                                     //Natural: ADD #SORT-REC.#FF3-NRA-WTHLD-AMT TO #ST-NRA-TAX ( #I,#COMP )
                    pnd_St_Nra_Tax.getValue(25,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_Nra_Wthld_Amt);                                                                        //Natural: ADD #SORT-REC.#FF3-NRA-WTHLD-AMT TO #ST-NRA-TAX ( 25,#COMP )
                }                                                                                                                                                         //Natural: END-IF
                //*  11/2014 END
                pnd_St_Canadian_Tax.getValue(pnd_I,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_Can_Wthld_Amt);                                                                    //Natural: ADD #SORT-REC.#FF3-CAN-WTHLD-AMT TO #ST-CANADIAN-TAX ( #I,#COMP )
                pnd_St_State_Tax.getValue(pnd_I,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_State_Wthld);                                                                         //Natural: ADD #SORT-REC.#FF3-STATE-WTHLD TO #ST-STATE-TAX ( #I,#COMP )
                //*                                      TO #ST-PUERTO-RICO-TAX (#I,#COMP)
                //*    YTD TOTALS
                pnd_St_Federal_Tax.getValue(25,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_Fed_Wthld_Amt);                                                                        //Natural: ADD #SORT-REC.#FF3-FED-WTHLD-AMT TO #ST-FEDERAL-TAX ( 25,#COMP )
                pnd_St_Canadian_Tax.getValue(25,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_Can_Wthld_Amt);                                                                       //Natural: ADD #SORT-REC.#FF3-CAN-WTHLD-AMT TO #ST-CANADIAN-TAX ( 25,#COMP )
                pnd_St_State_Tax.getValue(25,pnd_Comp).nadd(pnd_Sort_Rec_Pnd_Ff3_State_Wthld);                                                                            //Natural: ADD #SORT-REC.#FF3-STATE-WTHLD TO #ST-STATE-TAX ( 25,#COMP )
                //*                                      TO #ST-PUERTO-RICO-TAX (25,#COMP)
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  25TH OCCUR IS YTD
            sort01Pnd_Ff3_Pymnt_DateOld.setValue(pnd_Sort_Rec_Pnd_Ff3_Pymnt_Date);                                                                                        //Natural: END-SORT
            sort01Pnd_Ff3_Pymnt_YyyyOld.setValue(pnd_Sort_Rec_Pnd_Ff3_Pymnt_Yyyy);
            sort01Pnd_Ff3_Residency_CodeOld.setValue(pnd_Sort_Rec_Pnd_Ff3_Residency_Code);
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        if (condition(getSort().getAtEndOfData()))
        {
            //* *  PERFORM WRITE-TOTAL-PAGES
            pnd_Federal_Tax.getValue(3,pnd_I_Tiaa).nadd(pnd_Federal_Tax.getValue(3,1,":",5));                                                                             //Natural: ADD #FEDERAL-TAX ( 3,1:5 ) TO #FEDERAL-TAX ( 3,#I-TIAA )
            pnd_Nra_Tax.getValue(3,pnd_I_Tiaa).nadd(pnd_Nra_Tax.getValue(3,1,":",5));                                                                                     //Natural: ADD #NRA-TAX ( 3,1:5 ) TO #NRA-TAX ( 3,#I-TIAA )
            //*  11/2014
            pnd_Fatca_Tax.getValue(3,pnd_I_Tiaa).nadd(pnd_Fatca_Tax.getValue(3,1,":",5));                                                                                 //Natural: ADD #FATCA-TAX ( 3,1:5 ) TO #FATCA-TAX ( 3,#I-TIAA )
            pnd_Canadian_Tax.getValue(3,pnd_I_Tiaa).nadd(pnd_Canadian_Tax.getValue(3,1,":",5));                                                                           //Natural: ADD #CANADIAN-TAX ( 3,1:5 ) TO #CANADIAN-TAX ( 3,#I-TIAA )
            pnd_State_Tax.getValue(3,pnd_I_Tiaa).nadd(pnd_State_Tax.getValue(3,1,":",5));                                                                                 //Natural: ADD #STATE-TAX ( 3,1:5 ) TO #STATE-TAX ( 3,#I-TIAA )
            pnd_Puerto_Rico_Tax.getValue(3,pnd_I_Tiaa).nadd(pnd_Puerto_Rico_Tax.getValue(3,1,":",5));                                                                     //Natural: ADD #PUERTO-RICO-TAX ( 3,1:5 ) TO #PUERTO-RICO-TAX ( 3,#I-TIAA )
            pnd_Federal_Tax.getValue(3,pnd_I_Total).compute(new ComputeParameters(false, pnd_Federal_Tax.getValue(3,pnd_I_Total)), pnd_Federal_Tax.getValue(3,            //Natural: COMPUTE #FEDERAL-TAX ( 3,#I-TOTAL ) := #FEDERAL-TAX ( 3,7 ) + #FEDERAL-TAX ( 3,#I-TIAA )
                7).add(pnd_Federal_Tax.getValue(3,pnd_I_Tiaa)));
            pnd_Nra_Tax.getValue(3,pnd_I_Total).compute(new ComputeParameters(false, pnd_Nra_Tax.getValue(3,pnd_I_Total)), pnd_Nra_Tax.getValue(3,7).add(pnd_Nra_Tax.getValue(3, //Natural: COMPUTE #NRA-TAX ( 3,#I-TOTAL ) := #NRA-TAX ( 3,7 ) + #NRA-TAX ( 3,#I-TIAA )
                pnd_I_Tiaa)));
            //*  11/2014 START
            pnd_Fatca_Tax.getValue(3,pnd_I_Total).compute(new ComputeParameters(false, pnd_Fatca_Tax.getValue(3,pnd_I_Total)), pnd_Fatca_Tax.getValue(3,                  //Natural: COMPUTE #FATCA-TAX ( 3,#I-TOTAL ) := #FATCA-TAX ( 3,7 ) + #FATCA-TAX ( 3,#I-TIAA )
                7).add(pnd_Fatca_Tax.getValue(3,pnd_I_Tiaa)));
            //*  11/2014 END
            pnd_Canadian_Tax.getValue(3,pnd_I_Total).compute(new ComputeParameters(false, pnd_Canadian_Tax.getValue(3,pnd_I_Total)), pnd_Canadian_Tax.getValue(3,         //Natural: COMPUTE #CANADIAN-TAX ( 3,#I-TOTAL ) := #CANADIAN-TAX ( 3,7 ) + #CANADIAN-TAX ( 3,#I-TIAA )
                7).add(pnd_Canadian_Tax.getValue(3,pnd_I_Tiaa)));
            pnd_State_Tax.getValue(3,pnd_I_Total).compute(new ComputeParameters(false, pnd_State_Tax.getValue(3,pnd_I_Total)), pnd_State_Tax.getValue(3,                  //Natural: COMPUTE #STATE-TAX ( 3,#I-TOTAL ) := #STATE-TAX ( 3,7 ) + #STATE-TAX ( 3,#I-TIAA )
                7).add(pnd_State_Tax.getValue(3,pnd_I_Tiaa)));
            pnd_Puerto_Rico_Tax.getValue(3,pnd_I_Total).compute(new ComputeParameters(false, pnd_Puerto_Rico_Tax.getValue(3,pnd_I_Total)), pnd_Puerto_Rico_Tax.getValue(3, //Natural: COMPUTE #PUERTO-RICO-TAX ( 3,#I-TOTAL ) := #PUERTO-RICO-TAX ( 3,7 ) + #PUERTO-RICO-TAX ( 3,#I-TIAA )
                7).add(pnd_Puerto_Rico_Tax.getValue(3,pnd_I_Tiaa)));
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,"YTD - All Residences",NEWLINE);                                                                                //Natural: WRITE ( 1 ) /// 'YTD - All Residences' /
            if (condition(Global.isEscape())) return;
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 #I-TOTAL
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_I_Total)); pnd_I.nadd(1))
            {
                //*  11/2014
                getReports().write(1, pnd_Company_Lit.getValue(pnd_I),pnd_Federal_Tax.getValue(3,pnd_I), new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),pnd_Nra_Tax.getValue(3,pnd_I),  //Natural: WRITE ( 1 ) #COMPANY-LIT ( #I ) #FEDERAL-TAX ( 3,#I ) #NRA-TAX ( 3,#I ) #CANADIAN-TAX ( 3,#I ) #STATE-TAX ( 3,#I ) #PUERTO-RICO-TAX ( 3,#I ) #FATCA-TAX ( 3,#I )
                    new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),pnd_Canadian_Tax.getValue(3,pnd_I), new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),pnd_State_Tax.getValue(3,pnd_I), 
                    new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),pnd_Puerto_Rico_Tax.getValue(3,pnd_I), new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),pnd_Fatca_Tax.getValue(3,pnd_I), 
                    new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        endSort();
        //* ***********************************************************************
        //* ***********************************************************************
        //*  OVERALL SUMMARY TOTALS BY MONTH
        FOR05:                                                                                                                                                            //Natural: FOR #I 1 25
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(25)); pnd_I.nadd(1))
        {
            if (condition(pnd_I.equals(25)))                                                                                                                              //Natural: IF #I = 25
            {
                getReports().write(2, NEWLINE,NEWLINE,NEWLINE,"YTD - All States",NEWLINE);                                                                                //Natural: WRITE ( 2 ) /// 'YTD - All States' /
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_St_Pay_Date_Pnd_St_Pay_Yyyymm.getValue(pnd_I).equals(getZero())))                                                                       //Natural: IF #ST-PAY-YYYYMM ( #I ) = 0
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Pay_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_St_Pay_Date_Pnd_St_Pay_Yyyymm.getValue(pnd_I), "01"));                          //Natural: COMPRESS #ST-PAY-YYYYMM ( #I ) '01' INTO #PAY-DATE LEAVING NO
                pnd_Pay_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Pay_Date);                                                                                    //Natural: MOVE EDITED #PAY-DATE TO #PAY-D ( EM = YYYYMMDD )
                getReports().write(2, NEWLINE,pnd_Pay_D, new ReportEditMask ("LLLLLLLLLL,' 'YYYY"));                                                                      //Natural: WRITE ( 2 ) / #PAY-D ( EM = LLLLLLLLLL,' 'YYYY )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_St_Federal_Tax.getValue(pnd_I,pnd_I_Tiaa).nadd(pnd_St_Federal_Tax.getValue(pnd_I,1,":",5));                                                               //Natural: ADD #ST-FEDERAL-TAX ( #I,1:5 ) TO #ST-FEDERAL-TAX ( #I,#I-TIAA )
            pnd_St_Nra_Tax.getValue(pnd_I,pnd_I_Tiaa).nadd(pnd_St_Nra_Tax.getValue(pnd_I,1,":",5));                                                                       //Natural: ADD #ST-NRA-TAX ( #I,1:5 ) TO #ST-NRA-TAX ( #I,#I-TIAA )
            //*  11/2014 START
            pnd_St_Fatca_Tax.getValue(pnd_I,pnd_I_Tiaa).nadd(pnd_St_Fatca_Tax.getValue(pnd_I,1,":",5));                                                                   //Natural: ADD #ST-FATCA-TAX ( #I,1:5 ) TO #ST-FATCA-TAX ( #I,#I-TIAA )
            //*  11/2014 END
            pnd_St_Canadian_Tax.getValue(pnd_I,pnd_I_Tiaa).nadd(pnd_St_Canadian_Tax.getValue(pnd_I,1,":",5));                                                             //Natural: ADD #ST-CANADIAN-TAX ( #I,1:5 ) TO #ST-CANADIAN-TAX ( #I,#I-TIAA )
            pnd_St_State_Tax.getValue(pnd_I,pnd_I_Tiaa).nadd(pnd_St_State_Tax.getValue(pnd_I,1,":",5));                                                                   //Natural: ADD #ST-STATE-TAX ( #I,1:5 ) TO #ST-STATE-TAX ( #I,#I-TIAA )
            pnd_St_Puerto_Rico_Tax.getValue(pnd_I,pnd_I_Tiaa).nadd(pnd_St_Puerto_Rico_Tax.getValue(pnd_I,1,":",5));                                                       //Natural: ADD #ST-PUERTO-RICO-TAX ( #I,1:5 ) TO #ST-PUERTO-RICO-TAX ( #I,#I-TIAA )
            pnd_St_Federal_Tax.getValue(pnd_I,pnd_I_Total).compute(new ComputeParameters(false, pnd_St_Federal_Tax.getValue(pnd_I,pnd_I_Total)), pnd_St_Federal_Tax.getValue(pnd_I, //Natural: COMPUTE #ST-FEDERAL-TAX ( #I,#I-TOTAL ) := #ST-FEDERAL-TAX ( #I,7 ) + #ST-FEDERAL-TAX ( #I,#I-TIAA )
                7).add(pnd_St_Federal_Tax.getValue(pnd_I,pnd_I_Tiaa)));
            pnd_St_Nra_Tax.getValue(pnd_I,pnd_I_Total).compute(new ComputeParameters(false, pnd_St_Nra_Tax.getValue(pnd_I,pnd_I_Total)), pnd_St_Nra_Tax.getValue(pnd_I,   //Natural: COMPUTE #ST-NRA-TAX ( #I,#I-TOTAL ) := #ST-NRA-TAX ( #I,7 ) + #ST-NRA-TAX ( #I,#I-TIAA )
                7).add(pnd_St_Nra_Tax.getValue(pnd_I,pnd_I_Tiaa)));
            //*  11/2014 START
            pnd_St_Fatca_Tax.getValue(pnd_I,pnd_I_Total).compute(new ComputeParameters(false, pnd_St_Fatca_Tax.getValue(pnd_I,pnd_I_Total)), pnd_St_Fatca_Tax.getValue(pnd_I, //Natural: COMPUTE #ST-FATCA-TAX ( #I,#I-TOTAL ) := #ST-FATCA-TAX ( #I,7 ) + #ST-FATCA-TAX ( #I,#I-TIAA )
                7).add(pnd_St_Fatca_Tax.getValue(pnd_I,pnd_I_Tiaa)));
            //*  11/2014 END
            pnd_St_Canadian_Tax.getValue(pnd_I,pnd_I_Total).compute(new ComputeParameters(false, pnd_St_Canadian_Tax.getValue(pnd_I,pnd_I_Total)), pnd_St_Canadian_Tax.getValue(pnd_I, //Natural: COMPUTE #ST-CANADIAN-TAX ( #I,#I-TOTAL ) := #ST-CANADIAN-TAX ( #I,7 ) + #ST-CANADIAN-TAX ( #I,#I-TIAA )
                7).add(pnd_St_Canadian_Tax.getValue(pnd_I,pnd_I_Tiaa)));
            pnd_St_State_Tax.getValue(pnd_I,pnd_I_Total).compute(new ComputeParameters(false, pnd_St_State_Tax.getValue(pnd_I,pnd_I_Total)), pnd_St_State_Tax.getValue(pnd_I, //Natural: COMPUTE #ST-STATE-TAX ( #I,#I-TOTAL ) := #ST-STATE-TAX ( #I,7 ) + #ST-STATE-TAX ( #I,#I-TIAA )
                7).add(pnd_St_State_Tax.getValue(pnd_I,pnd_I_Tiaa)));
            pnd_St_Puerto_Rico_Tax.getValue(pnd_I,pnd_I_Total).compute(new ComputeParameters(false, pnd_St_Puerto_Rico_Tax.getValue(pnd_I,pnd_I_Total)),                  //Natural: COMPUTE #ST-PUERTO-RICO-TAX ( #I,#I-TOTAL ) := #ST-PUERTO-RICO-TAX ( #I,7 ) + #ST-PUERTO-RICO-TAX ( #I,#I-TIAA )
                pnd_St_Puerto_Rico_Tax.getValue(pnd_I,7).add(pnd_St_Puerto_Rico_Tax.getValue(pnd_I,pnd_I_Tiaa)));
            FOR06:                                                                                                                                                        //Natural: FOR #C 1 #I-TOTAL
            for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(pnd_I_Total)); pnd_C.nadd(1))
            {
                //*  11/2014
                getReports().write(2, pnd_Company_Lit.getValue(pnd_C),pnd_St_Federal_Tax.getValue(pnd_I,pnd_C), new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),pnd_St_Nra_Tax.getValue(pnd_I,pnd_C),  //Natural: WRITE ( 2 ) #COMPANY-LIT ( #C ) #ST-FEDERAL-TAX ( #I,#C ) #ST-NRA-TAX ( #I,#C ) #ST-CANADIAN-TAX ( #I,#C ) #ST-STATE-TAX ( #I,#C ) #ST-PUERTO-RICO-TAX ( #I,#C ) #ST-FATCA-TAX ( #I,#C )
                    new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),pnd_St_Canadian_Tax.getValue(pnd_I,pnd_C), new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),pnd_St_State_Tax.getValue(pnd_I,pnd_C), 
                    new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),pnd_St_Puerto_Rico_Tax.getValue(pnd_I,pnd_C), new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),pnd_St_Fatca_Tax.getValue(pnd_I,pnd_C), 
                    new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Get_State_Name() throws Exception                                                                                                                    //Natural: GET-STATE-NAME
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                             //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
        pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().setValue(pnd_Pay_Yyyy);                                                                                                    //Natural: ASSIGN TWRATBL2.#TAX-YEAR := #PAY-YYYY
        pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().setValue(false);                                                                                                          //Natural: ASSIGN TWRATBL2.#ABEND-IND := FALSE
        pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().setValue(false);                                                                                                        //Natural: ASSIGN TWRATBL2.#DISPLAY-IND := FALSE
        pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "0", pnd_State_Code));                                           //Natural: COMPRESS '0' #STATE-CODE INTO TWRATBL2.#STATE-CDE LEAVING NO
        DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), pdaTwratbl2.getTwratbl2_Output_Data());                         //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS TWRATBL2.OUTPUT-DATA
        if (condition(Global.isEscape())) return;
        pnd_State_Name.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Full_Name());                                                                                       //Natural: ASSIGN #STATE-NAME := TIRCNTL-STATE-FULL-NAME
        //*  GET-STATE-NAME
    }

    //

    // Support Methods

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Sort_Rec_Pnd_Ff3_Pymnt_Date6IsBreak = pnd_Sort_Rec_Pnd_Ff3_Pymnt_Date.isBreak(endOfData);
        boolean pnd_Sort_Rec_Pnd_Ff3_Residency_CodeIsBreak = pnd_Sort_Rec_Pnd_Ff3_Residency_Code.isBreak(endOfData);
        if (condition(pnd_Sort_Rec_Pnd_Ff3_Pymnt_Date6IsBreak || pnd_Sort_Rec_Pnd_Ff3_Residency_CodeIsBreak))
        {
            pnd_Federal_Tax.getValue(1,pnd_I_Tiaa).nadd(pnd_Federal_Tax.getValue(1,1,":",5));                                                                             //Natural: ADD #FEDERAL-TAX ( 1,1:5 ) TO #FEDERAL-TAX ( 1,#I-TIAA )
            pnd_Nra_Tax.getValue(1,pnd_I_Tiaa).nadd(pnd_Nra_Tax.getValue(1,1,":",5));                                                                                     //Natural: ADD #NRA-TAX ( 1,1:5 ) TO #NRA-TAX ( 1,#I-TIAA )
            //*  11/2014
            pnd_Fatca_Tax.getValue(1,pnd_I_Tiaa).nadd(pnd_Fatca_Tax.getValue(1,1,":",5));                                                                                 //Natural: ADD #FATCA-TAX ( 1,1:5 ) TO #FATCA-TAX ( 1,#I-TIAA )
            pnd_Canadian_Tax.getValue(1,pnd_I_Tiaa).nadd(pnd_Canadian_Tax.getValue(1,1,":",5));                                                                           //Natural: ADD #CANADIAN-TAX ( 1,1:5 ) TO #CANADIAN-TAX ( 1,#I-TIAA )
            pnd_State_Tax.getValue(1,pnd_I_Tiaa).nadd(pnd_State_Tax.getValue(1,1,":",5));                                                                                 //Natural: ADD #STATE-TAX ( 1,1:5 ) TO #STATE-TAX ( 1,#I-TIAA )
            pnd_Puerto_Rico_Tax.getValue(1,pnd_I_Tiaa).nadd(pnd_Puerto_Rico_Tax.getValue(1,1,":",5));                                                                     //Natural: ADD #PUERTO-RICO-TAX ( 1,1:5 ) TO #PUERTO-RICO-TAX ( 1,#I-TIAA )
            pnd_Federal_Tax.getValue(1,pnd_I_Total).compute(new ComputeParameters(false, pnd_Federal_Tax.getValue(1,pnd_I_Total)), pnd_Federal_Tax.getValue(1,            //Natural: COMPUTE #FEDERAL-TAX ( 1,#I-TOTAL ) := #FEDERAL-TAX ( 1,7 ) + #FEDERAL-TAX ( 1,#I-TIAA )
                7).add(pnd_Federal_Tax.getValue(1,pnd_I_Tiaa)));
            pnd_Nra_Tax.getValue(1,pnd_I_Total).compute(new ComputeParameters(false, pnd_Nra_Tax.getValue(1,pnd_I_Total)), pnd_Nra_Tax.getValue(1,7).add(pnd_Nra_Tax.getValue(1, //Natural: COMPUTE #NRA-TAX ( 1,#I-TOTAL ) := #NRA-TAX ( 1,7 ) + #NRA-TAX ( 1,#I-TIAA )
                pnd_I_Tiaa)));
            //*  11/2014 START
            pnd_Fatca_Tax.getValue(1,pnd_I_Total).compute(new ComputeParameters(false, pnd_Fatca_Tax.getValue(1,pnd_I_Total)), pnd_Fatca_Tax.getValue(1,                  //Natural: COMPUTE #FATCA-TAX ( 1,#I-TOTAL ) := #FATCA-TAX ( 1,7 ) + #FATCA-TAX ( 1,#I-TIAA )
                7).add(pnd_Fatca_Tax.getValue(1,pnd_I_Tiaa)));
            //*  11/2014 END
            pnd_Canadian_Tax.getValue(1,pnd_I_Total).compute(new ComputeParameters(false, pnd_Canadian_Tax.getValue(1,pnd_I_Total)), pnd_Canadian_Tax.getValue(1,         //Natural: COMPUTE #CANADIAN-TAX ( 1,#I-TOTAL ) := #CANADIAN-TAX ( 1,7 ) + #CANADIAN-TAX ( 1,#I-TIAA )
                7).add(pnd_Canadian_Tax.getValue(1,pnd_I_Tiaa)));
            pnd_State_Tax.getValue(1,pnd_I_Total).compute(new ComputeParameters(false, pnd_State_Tax.getValue(1,pnd_I_Total)), pnd_State_Tax.getValue(1,                  //Natural: COMPUTE #STATE-TAX ( 1,#I-TOTAL ) := #STATE-TAX ( 1,7 ) + #STATE-TAX ( 1,#I-TIAA )
                7).add(pnd_State_Tax.getValue(1,pnd_I_Tiaa)));
            pnd_Puerto_Rico_Tax.getValue(1,pnd_I_Total).compute(new ComputeParameters(false, pnd_Puerto_Rico_Tax.getValue(1,pnd_I_Total)), pnd_Puerto_Rico_Tax.getValue(1, //Natural: COMPUTE #PUERTO-RICO-TAX ( 1,#I-TOTAL ) := #PUERTO-RICO-TAX ( 1,7 ) + #PUERTO-RICO-TAX ( 1,#I-TIAA )
                7).add(pnd_Puerto_Rico_Tax.getValue(1,pnd_I_Tiaa)));
            pnd_Pay_D.setValueEdited(new ReportEditMask("YYYYMMDD"),sort01Pnd_Ff3_Pymnt_DateOld);                                                                         //Natural: MOVE EDITED OLD ( #SORT-REC.#FF3-PYMNT-DATE ) TO #PAY-D ( EM = YYYYMMDD )
            pnd_Pay_Yyyy.setValue(sort01Pnd_Ff3_Pymnt_YyyyOld);                                                                                                           //Natural: ASSIGN #PAY-YYYY := OLD ( #SORT-REC.#FF3-PYMNT-YYYY )
            pnd_State_Code.setValue(sort01Pnd_Ff3_Residency_CodeOld);                                                                                                     //Natural: ASSIGN #STATE-CODE := OLD ( #SORT-REC.#FF3-RESIDENCY-CODE )
                                                                                                                                                                          //Natural: PERFORM GET-STATE-NAME
            sub_Get_State_Name();
            if (condition(Global.isEscape())) {return;}
            getReports().write(1, NEWLINE,pnd_Pay_D, new ReportEditMask ("YYYY"),"-",pnd_Pay_D, new ReportEditMask ("LLL"),"-",pnd_State_Name);                           //Natural: WRITE ( 1 ) / #PAY-D ( EM = YYYY ) '-' #PAY-D ( EM = LLL ) '-' #STATE-NAME
            if (condition(Global.isEscape())) return;
            FOR02:                                                                                                                                                        //Natural: FOR #I 1 #I-TOTAL
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_I_Total)); pnd_I.nadd(1))
            {
                //*  11/2014
                getReports().write(1, pnd_Company_Lit.getValue(pnd_I),pnd_Federal_Tax.getValue(1,pnd_I), new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),pnd_Nra_Tax.getValue(1,pnd_I),  //Natural: WRITE ( 1 ) #COMPANY-LIT ( #I ) #FEDERAL-TAX ( 1,#I ) #NRA-TAX ( 1,#I ) #CANADIAN-TAX ( 1,#I ) #STATE-TAX ( 1,#I ) #PUERTO-RICO-TAX ( 1,#I ) #FATCA-TAX ( 1,#I )
                    new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),pnd_Canadian_Tax.getValue(1,pnd_I), new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),pnd_State_Tax.getValue(1,pnd_I), 
                    new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),pnd_Puerto_Rico_Tax.getValue(1,pnd_I), new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),pnd_Fatca_Tax.getValue(1,pnd_I), 
                    new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Federal_Tax.getValue(2,pnd_I).nadd(pnd_Federal_Tax.getValue(1,pnd_I));                                                                                //Natural: ADD #FEDERAL-TAX ( 1,#I ) TO #FEDERAL-TAX ( 2,#I )
                pnd_Nra_Tax.getValue(2,pnd_I).nadd(pnd_Nra_Tax.getValue(1,pnd_I));                                                                                        //Natural: ADD #NRA-TAX ( 1,#I ) TO #NRA-TAX ( 2,#I )
                //*  11/2014
                pnd_Fatca_Tax.getValue(2,pnd_I).nadd(pnd_Fatca_Tax.getValue(1,pnd_I));                                                                                    //Natural: ADD #FATCA-TAX ( 1,#I ) TO #FATCA-TAX ( 2,#I )
                pnd_Canadian_Tax.getValue(2,pnd_I).nadd(pnd_Canadian_Tax.getValue(1,pnd_I));                                                                              //Natural: ADD #CANADIAN-TAX ( 1,#I ) TO #CANADIAN-TAX ( 2,#I )
                pnd_State_Tax.getValue(2,pnd_I).nadd(pnd_State_Tax.getValue(1,pnd_I));                                                                                    //Natural: ADD #STATE-TAX ( 1,#I ) TO #STATE-TAX ( 2,#I )
                pnd_Puerto_Rico_Tax.getValue(2,pnd_I).nadd(pnd_Puerto_Rico_Tax.getValue(1,pnd_I));                                                                        //Natural: ADD #PUERTO-RICO-TAX ( 1,#I ) TO #PUERTO-RICO-TAX ( 2,#I )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape())) return;
            //*  11/2014
            pnd_Federal_Tax.getValue(1,"*").reset();                                                                                                                      //Natural: RESET #FEDERAL-TAX ( 1,* ) #NRA-TAX ( 1,* ) #FATCA-TAX ( 1,* ) #CANADIAN-TAX ( 1,* ) #STATE-TAX ( 1,* ) #PUERTO-RICO-TAX ( 1,* )
            pnd_Nra_Tax.getValue(1,"*").reset();
            pnd_Fatca_Tax.getValue(1,"*").reset();
            pnd_Canadian_Tax.getValue(1,"*").reset();
            pnd_State_Tax.getValue(1,"*").reset();
            pnd_Puerto_Rico_Tax.getValue(1,"*").reset();
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Sort_Rec_Pnd_Ff3_Residency_CodeIsBreak))
        {
            pnd_Pay_Yyyy.setValue(sort01Pnd_Ff3_Pymnt_YyyyOld);                                                                                                           //Natural: ASSIGN #PAY-YYYY := OLD ( #SORT-REC.#FF3-PYMNT-YYYY )
            pnd_State_Code.setValue(sort01Pnd_Ff3_Residency_CodeOld);                                                                                                     //Natural: ASSIGN #STATE-CODE := OLD ( #SORT-REC.#FF3-RESIDENCY-CODE )
                                                                                                                                                                          //Natural: PERFORM GET-STATE-NAME
            sub_Get_State_Name();
            if (condition(Global.isEscape())) {return;}
            getReports().write(1, NEWLINE,"YTD -",pnd_State_Name);                                                                                                        //Natural: WRITE ( 1 ) / 'YTD -' #STATE-NAME
            if (condition(Global.isEscape())) return;
            FOR03:                                                                                                                                                        //Natural: FOR #I 1 #I-TOTAL
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_I_Total)); pnd_I.nadd(1))
            {
                //*  11/2014
                getReports().write(1, pnd_Company_Lit.getValue(pnd_I),pnd_Federal_Tax.getValue(2,pnd_I), new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),pnd_Nra_Tax.getValue(2,pnd_I),  //Natural: WRITE ( 1 ) #COMPANY-LIT ( #I ) #FEDERAL-TAX ( 2,#I ) #NRA-TAX ( 2,#I ) #CANADIAN-TAX ( 2,#I ) #STATE-TAX ( 2,#I ) #PUERTO-RICO-TAX ( 2,#I ) #FATCA-TAX ( 2,#I )
                    new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),pnd_Canadian_Tax.getValue(2,pnd_I), new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),pnd_State_Tax.getValue(2,pnd_I), 
                    new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),pnd_Puerto_Rico_Tax.getValue(2,pnd_I), new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),pnd_Fatca_Tax.getValue(2,pnd_I), 
                    new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape())) return;
            //*  11/2014
            pnd_Federal_Tax.getValue(2,"*").reset();                                                                                                                      //Natural: RESET #FEDERAL-TAX ( 2,* ) #NRA-TAX ( 2,* ) #FATCA-TAX ( 2,* ) #CANADIAN-TAX ( 2,* ) #STATE-TAX ( 2,* ) #PUERTO-RICO-TAX ( 2,* )
            pnd_Nra_Tax.getValue(2,"*").reset();
            pnd_Fatca_Tax.getValue(2,"*").reset();
            pnd_Canadian_Tax.getValue(2,"*").reset();
            pnd_State_Tax.getValue(2,"*").reset();
            pnd_Puerto_Rico_Tax.getValue(2,"*").reset();
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");
        Global.format(2, "PS=60 LS=132");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,NEWLINE,Global.getPROGRAM(),new TabSetting(34),"TAX WITHHOLDING AND REPORTING SYSTEM",new 
            TabSetting(119),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new TabSetting(39),"Monthly Management Report",NEWLINE,"RUNTIME : ",Global.getTIMX(),new 
            TabSetting(45),"Tax Year",ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Tax_Year(),NEWLINE,NEWLINE,new ColumnSpacing(18),"Federal        Non-FATCA         Canadian",new 
            ColumnSpacing(12),"State      Puerto Rico            FATCA",NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,NEWLINE,Global.getPROGRAM(),new TabSetting(34),"TAX WITHHOLDING AND REPORTING SYSTEM",new 
            TabSetting(119),"PAGE",getReports().getPageNumberDbs(2),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new TabSetting(39),"Monthly Management Report",NEWLINE,"RUNTIME : ",Global.getTIMX(),new 
            TabSetting(44),"Monthly Summary",NEWLINE,new TabSetting(45),"Tax Year",ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Tax_Year(),NEWLINE,NEWLINE,new 
            ColumnSpacing(18),"Federal        Non-FATCA         Canadian",new ColumnSpacing(12),"State      Puerto Rico            FATCA",NEWLINE,NEWLINE);
    }
    private void CheckAtStartofData246() throws Exception
    {
        if (condition(getSort().getAtStartOfData()))
        {
            pnd_Pay_Yyyy.setValue(pnd_Sort_Rec_Pnd_Ff3_Pymnt_Yyyy);                                                                                                       //Natural: ASSIGN #PAY-YYYY := #SORT-REC.#FF3-PYMNT-YYYY
            pnd_State_Code.setValue(pnd_Sort_Rec_Pnd_Ff3_Residency_Code);                                                                                                 //Natural: ASSIGN #STATE-CODE := #SORT-REC.#FF3-RESIDENCY-CODE
                                                                                                                                                                          //Natural: PERFORM GET-STATE-NAME
            sub_Get_State_Name();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
