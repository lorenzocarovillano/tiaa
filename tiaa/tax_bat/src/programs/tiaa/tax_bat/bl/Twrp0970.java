/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:32:11 PM
**        * FROM NATURAL PROGRAM : Twrp0970
************************************************************
**        * FILE NAME            : Twrp0970.java
**        * CLASS NAME           : Twrp0970
**        * INSTANCE NAME        : Twrp0970
************************************************************
* PROGRAM  : TWRP0970
* FUNCTION : PRINTS STATE SUMMARY TOTALS/GROSS-REPORTABLE
* INPUTS   : FLAT-FILE1 AND FLAT-FILE4
* SORTED BY: COMPANY, RESIDENCY CODE AND CONTRACT NO
* WRITTEN BY: EDITH
* HISTORY
* --------------------------------------------------
* 10/01/99 -INCLUDED PROCESSING OF APO AND UNKNOWN US STATES
*          -SPECIAL PROCESSING FOR PENNSYLVANIA (STATE IND=3 RES-CDE=41)
*               - SHLD. BE REPORTED AS GROSS REPTBLE IF
*              * DISTRIBUTION CODE = 1
*              * DISTRIBUTION CODE NE 1 BUT WITH STATE TAX
*          -SPECIAL CONDITION FOR NY/NYC :
*                RESIDENTS OF NY/NYC MUST BE REPORTED AS GROSS-REPORTBLE
*                 IF HE HAS NO  STATE TAX  BUT WITH LOCAL TAX
* 10/25/99 -PROCESS NEW COMPANY -TIAA LIFE (EDITH)
*          - NEW DEFINITION OF ROLLOVER BASED ON DISTRIBUTION CODE
*            NO ROLLOVER FOR NRA
* 02/28/02 -ELIMINATE CHECK OF STATE TABLE RULES (J.ROTHOLZ)
* 04/02/02 -ADD NEW COMPANY CODE - "S" FOR TCII (J.ROTHOLZ)
* 05/30/02 -ALLOW IVC AMOUNTS WITH ROLLOVERS (JH)
* 10/08/02 JH DISTR CODE 6 (TAXFREE EXCHANGE) IS TREATED LIKE ROLLOVER
* 01/09/03 MN INCREASE THE SIZE OF SOME COUNTERS AND USE TWRL0905
*             INSTEAD OF TWRL0904
* 09/29/03 RM - 2003 1099 & 5498 SDR
*               DISTRIBUTION CODE 'X' REMOVED.
* 09/22/04 RM TOPS RELEASE 3 CHANGES
*             ADD NEW COMPANY CODE 'X' FOR TRUST COMPANY.
* 07/19/11  RS ADDED TEST FOR DIST CODE '=' (H4)    SCAN RS0711
*              'Roll Roth 403 to Roth IRA - Death'
* 10/19/11: ADD NEW COMPANY CODE "F" FOR TIAA-CREF TRUST COMPANY,FSB;
*           CHANGE CAN BE REFERENCED AS DY1               (D.YHUN)
* ----------------------------------------------------------

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0970 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0901 ldaTwrl0901;
    private LdaTwrl0905 ldaTwrl0905;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Tax_Year;
    private DbsField pnd_Res_Code;
    private DbsField pnd_Sv_Search_Rc;
    private DbsField pnd_State_Ind;
    private DbsField pnd_State_Desc;
    private DbsField pnd_Print_Com;
    private DbsField pnd_I;
    private DbsField pnd_K;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Rec_Cnt;
    private DbsField pnd_Counters_Pnd_Trans_Cnt;
    private DbsField pnd_Counters_Pnd_Gross_Amt;
    private DbsField pnd_Counters_Pnd_Tax_Free_Ivc;
    private DbsField pnd_Counters_Pnd_Taxable_Amt;
    private DbsField pnd_Counters_Pnd_Int_Amt;
    private DbsField pnd_Counters_Pnd_State_Amt;
    private DbsField pnd_Counters_Pnd_Local_Amt;
    private DbsField pnd_Tax_Amt;
    private DbsField pnd_Rec_Read;
    private DbsField pnd_Rec_Process;
    private DbsField pnd_First;

    private DbsGroup pnd_Saver;
    private DbsField pnd_Saver_Pnd_Sv_Company;
    private DbsField pnd_Saver_Pnd_Sv_Contract;
    private DbsField pnd_Saver_Pnd_Sv_Res_Code;
    private DbsField pnd_Saver_Pnd_Sv_State_Desc;
    private DbsField pnd_Start_Date;
    private DbsField pnd_End_Date;

    private DbsGroup pnd_State_Table;
    private DbsField pnd_State_Table_Pnd_Residency_Code;
    private DbsField pnd_State_Table_Pnd_State_Indicator;
    private DbsField pnd_State_Table_Pnd_State_Description;
    private DbsField pnd_Found;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0901 = new LdaTwrl0901();
        registerRecord(ldaTwrl0901);
        ldaTwrl0905 = new LdaTwrl0905();
        registerRecord(ldaTwrl0905);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Tax_Year = localVariables.newFieldInRecord("pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);
        pnd_Res_Code = localVariables.newFieldInRecord("pnd_Res_Code", "#RES-CODE", FieldType.STRING, 2);
        pnd_Sv_Search_Rc = localVariables.newFieldInRecord("pnd_Sv_Search_Rc", "#SV-SEARCH-RC", FieldType.STRING, 2);
        pnd_State_Ind = localVariables.newFieldInRecord("pnd_State_Ind", "#STATE-IND", FieldType.STRING, 1);
        pnd_State_Desc = localVariables.newFieldInRecord("pnd_State_Desc", "#STATE-DESC", FieldType.STRING, 19);
        pnd_Print_Com = localVariables.newFieldInRecord("pnd_Print_Com", "#PRINT-COM", FieldType.STRING, 4);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 1);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 3);

        pnd_Counters = localVariables.newGroupArrayInRecord("pnd_Counters", "#COUNTERS", new DbsArrayController(1, 2));
        pnd_Counters_Pnd_Rec_Cnt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Rec_Cnt", "#REC-CNT", FieldType.NUMERIC, 7);
        pnd_Counters_Pnd_Trans_Cnt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Trans_Cnt", "#TRANS-CNT", FieldType.NUMERIC, 7);
        pnd_Counters_Pnd_Gross_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Counters_Pnd_Tax_Free_Ivc = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Tax_Free_Ivc", "#TAX-FREE-IVC", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Counters_Pnd_Taxable_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Counters_Pnd_Int_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Counters_Pnd_State_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_State_Amt", "#STATE-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Counters_Pnd_Local_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Local_Amt", "#LOCAL-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tax_Amt = localVariables.newFieldInRecord("pnd_Tax_Amt", "#TAX-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Rec_Read = localVariables.newFieldInRecord("pnd_Rec_Read", "#REC-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Rec_Process = localVariables.newFieldInRecord("pnd_Rec_Process", "#REC-PROCESS", FieldType.PACKED_DECIMAL, 7);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);

        pnd_Saver = localVariables.newGroupInRecord("pnd_Saver", "#SAVER");
        pnd_Saver_Pnd_Sv_Company = pnd_Saver.newFieldInGroup("pnd_Saver_Pnd_Sv_Company", "#SV-COMPANY", FieldType.STRING, 1);
        pnd_Saver_Pnd_Sv_Contract = pnd_Saver.newFieldInGroup("pnd_Saver_Pnd_Sv_Contract", "#SV-CONTRACT", FieldType.STRING, 8);
        pnd_Saver_Pnd_Sv_Res_Code = pnd_Saver.newFieldInGroup("pnd_Saver_Pnd_Sv_Res_Code", "#SV-RES-CODE", FieldType.STRING, 2);
        pnd_Saver_Pnd_Sv_State_Desc = pnd_Saver.newFieldInGroup("pnd_Saver_Pnd_Sv_State_Desc", "#SV-STATE-DESC", FieldType.STRING, 15);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);
        pnd_End_Date = localVariables.newFieldInRecord("pnd_End_Date", "#END-DATE", FieldType.STRING, 8);

        pnd_State_Table = localVariables.newGroupInRecord("pnd_State_Table", "#STATE-TABLE");
        pnd_State_Table_Pnd_Residency_Code = pnd_State_Table.newFieldArrayInGroup("pnd_State_Table_Pnd_Residency_Code", "#RESIDENCY-CODE", FieldType.STRING, 
            2, new DbsArrayController(1, 100));
        pnd_State_Table_Pnd_State_Indicator = pnd_State_Table.newFieldArrayInGroup("pnd_State_Table_Pnd_State_Indicator", "#STATE-INDICATOR", FieldType.STRING, 
            1, new DbsArrayController(1, 100));
        pnd_State_Table_Pnd_State_Description = pnd_State_Table.newFieldArrayInGroup("pnd_State_Table_Pnd_State_Description", "#STATE-DESCRIPTION", FieldType.STRING, 
            19, new DbsArrayController(1, 100));
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0901.initializeValues();
        ldaTwrl0905.initializeValues();

        localVariables.reset();
        pnd_First.setInitialValue(true);
        pnd_Found.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0970() throws Exception
    {
        super("Twrp0970");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*  ----------------------------------------------------------
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: FORMAT ( 2 ) PS = 60 LS = 132
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #FLAT-FILE1
        while (condition(getWorkFiles().read(1, ldaTwrl0901.getPnd_Flat_File1())))
        {
            pnd_Tax_Year.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Tax_Year());                                                                                      //Natural: ASSIGN #TAX-YEAR := #FF1-TAX-YEAR
            pnd_Start_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Start_Date());                                                                                  //Natural: ASSIGN #START-DATE := #FF1-START-DATE
            pnd_End_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_End_Date());                                                                                      //Natural: ASSIGN #END-DATE := #FF1-END-DATE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*                 /* CREATE A STATE TABLE BASED ON FILE98-TABLE2
        DbsUtil.callnat(Twrn0902.class , getCurrentProcessState(), pnd_Tax_Year, pnd_State_Table_Pnd_Residency_Code.getValue("*"), pnd_State_Table_Pnd_State_Indicator.getValue("*"),  //Natural: CALLNAT 'TWRN0902' #TAX-YEAR #RESIDENCY-CODE ( * ) #STATE-INDICATOR ( * ) #STATE-DESCRIPTION ( * )
            pnd_State_Table_Pnd_State_Description.getValue("*"));
        if (condition(Global.isEscape())) return;
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 2 RECORD #FLAT-FILE4
        while (condition(getWorkFiles().read(2, ldaTwrl0905.getPnd_Flat_File4())))
        {
            pnd_Rec_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-READ
            //*  BYPASS FOREIGN TAX CITIZENSHIP
            //*  BYPASS PUERTO RICO RESIDENCY
            if (condition(ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Tax_Citizenship().equals("F") || ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Residency_Code().equals("42")))    //Natural: IF #FF4-TAX-CITIZENSHIP = 'F' OR #FF4-RESIDENCY-CODE = '42'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Residency_Code().greaterOrEqual("00") && ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Residency_Code().lessOrEqual("57"))  //Natural: IF #FF4-RESIDENCY-CODE = '00' THRU '57' OR #FF4-RESIDENCY-CODE = '97'
                || ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Residency_Code().equals("97"))))
            {
                pnd_Res_Code.setValue(ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Residency_Code());                                                                            //Natural: ASSIGN #RES-CODE := #FF4-RESIDENCY-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Found.setValue(false);                                                                                                                                    //Natural: ASSIGN #FOUND := FALSE
            PND_PND_L1060:                                                                                                                                                //Natural: FOR #K 1 100
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(100)); pnd_K.nadd(1))
            {
                if (condition(pnd_State_Table_Pnd_Residency_Code.getValue(pnd_K).equals("  ")))                                                                           //Natural: IF #RESIDENCY-CODE ( #K ) = '  '
                {
                    if (true) break PND_PND_L1060;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L1060. )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Res_Code.equals(pnd_State_Table_Pnd_Residency_Code.getValue(pnd_K))))                                                               //Natural: IF #RES-CODE = #RESIDENCY-CODE ( #K )
                    {
                        pnd_State_Ind.setValue(pnd_State_Table_Pnd_State_Indicator.getValue(pnd_K));                                                                      //Natural: ASSIGN #STATE-IND := #STATE-INDICATOR ( #K )
                        pnd_State_Desc.setValue(pnd_State_Table_Pnd_State_Description.getValue(pnd_K));                                                                   //Natural: ASSIGN #STATE-DESC := #STATE-DESCRIPTION ( #K )
                        pnd_Found.setValue(true);                                                                                                                         //Natural: ASSIGN #FOUND := TRUE
                        if (true) break PND_PND_L1060;                                                                                                                    //Natural: ESCAPE BOTTOM ( ##L1060. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Found.equals(false)))                                                                                                                       //Natural: IF #FOUND = FALSE
            {
                if (condition(pnd_Res_Code.equals(pnd_Sv_Search_Rc)))                                                                                                     //Natural: IF #RES-CODE = #SV-SEARCH-RC
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(4),ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Company_Code(),new ColumnSpacing(8),            //Natural: WRITE ( 2 ) 4X #FF4-COMPANY-CODE 8X #RES-CODE
                        pnd_Res_Code);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Sv_Search_Rc.setValue(pnd_Res_Code);                                                                                                              //Natural: ASSIGN #SV-SEARCH-RC := #RES-CODE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*   ACCEPT IF #STATE-IND = '7'                    /* JRR 02/22/2002
            //*     OR ((#STATE-IND = '4' OR= '8') AND #FF4-SUM-STATE-WTHLD > 0 )
            //*     OR ( #STATE-IND = '4'                      AND
            //*     (#FF4-RESIDENCY-CODE = '35' OR= '3E')  AND
            //*   #FF4-SUM-STATE-WTHLD = 0              AND
            //*     #FF4-SUM-LOCAL-WTHLD > 0                 )
            //* *        OR (#STATE-IND = '3' AND #FF4-RESIDENCY-CODE = '41'  AND
            //* *           (#FF4-DISTRIBUTION-CDE  = '01'  OR
            //* *           (#FF4-DISTRIBUTION-CDE NE '01' AND
            //* *            #FF4-SUM-STATE-WTHLD > 0 )))
            //*     OR (#STATE-IND = '3' AND #FF4-RESIDENCY-CODE = '41'  AND
            //*     (#FF4-DISTRIBUTION-CDE  = '1'  OR
            //*     (#FF4-DISTRIBUTION-CDE NE '1' AND
            //*     #FF4-SUM-STATE-WTHLD > 0 )))
            //*     OR ( #STATE-IND = '3'  AND #FF4-RESIDENCY-CODE NE '41' )
            pnd_Rec_Process.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #REC-PROCESS
            if (condition(pnd_First.getBoolean()))                                                                                                                        //Natural: IF #FIRST
            {
                pnd_Saver_Pnd_Sv_Company.setValue(ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Company_Code());                                                                  //Natural: ASSIGN #SV-COMPANY := #FF4-COMPANY-CODE
                                                                                                                                                                          //Natural: PERFORM SAVE-DESC-RES
                sub_Save_Desc_Res();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_First.setValue(false);                                                                                                                                //Natural: ASSIGN #FIRST := FALSE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Company_Code().equals(pnd_Saver_Pnd_Sv_Company)))                                                         //Natural: IF #FF4-COMPANY-CODE = #SV-COMPANY
            {
                if (condition(ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Residency_Code().equals(pnd_Saver_Pnd_Sv_Res_Code)))                                                  //Natural: IF #FF4-RESIDENCY-CODE = #SV-RES-CODE
                {
                                                                                                                                                                          //Natural: PERFORM ACCOUNT-REC
                    sub_Account_Rec();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL
                    sub_Print_Detail();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM SAVE-DESC-RES
                    sub_Save_Desc_Res();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Saver_Pnd_Sv_Contract.reset();                                                                                                                    //Natural: RESET #SV-CONTRACT #COUNTERS ( 1 )
                    pnd_Counters.getValue(1).reset();
                                                                                                                                                                          //Natural: PERFORM ACCOUNT-REC
                    sub_Account_Rec();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  DIFFERENT COMPANY
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL
                sub_Print_Detail();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PRINT-COL-TOTAL
                sub_Print_Col_Total();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Saver_Pnd_Sv_Contract.reset();                                                                                                                        //Natural: RESET #SV-CONTRACT #COUNTERS ( * )
                pnd_Counters.getValue("*").reset();
                                                                                                                                                                          //Natural: PERFORM SAVE-DESC-RES
                sub_Save_Desc_Res();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Saver_Pnd_Sv_Company.setValue(ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Company_Code());                                                                  //Natural: ASSIGN #SV-COMPANY := #FF4-COMPANY-CODE
                                                                                                                                                                          //Natural: PERFORM ACCOUNT-REC
                sub_Account_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Rec_Process.equals(getZero())))                                                                                                                 //Natural: IF #REC-PROCESS = 0
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(40)," **** NO RECORDS PROCESSED **** ");                    //Natural: WRITE ( 1 ) ///// 40T ' **** NO RECORDS PROCESSED **** '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL
            sub_Print_Detail();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-COL-TOTAL
            sub_Print_Col_Total();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 1 )
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"TWRP0970 - CONTROL TOTAL",NEWLINE,NEWLINE,NEWLINE,"RUNDATE    : ",Global.getDATX(),                //Natural: WRITE ( 1 ) NOTITLE NOHDR 'TWRP0970 - CONTROL TOTAL' // / 'RUNDATE    : ' *DATX ( EM = MM/DD/YYYY ) / 'RUNTIME    : ' *TIMX / 'TAX YEAR   : ' #TAX-YEAR / 'DATE RANGE : ' #START-DATE 'THRU' #END-DATE /// '1) NO. OF RECORDS READ      : ' #REC-READ / '2) NO. OF RECORDS PROCESSED : ' #REC-PROCESS
            new ReportEditMask ("MM/DD/YYYY"),NEWLINE,"RUNTIME    : ",Global.getTIMX(),NEWLINE,"TAX YEAR   : ",pnd_Tax_Year,NEWLINE,"DATE RANGE : ",pnd_Start_Date,
            "THRU",pnd_End_Date,NEWLINE,NEWLINE,NEWLINE,"1) NO. OF RECORDS READ      : ",pnd_Rec_Read,NEWLINE,"2) NO. OF RECORDS PROCESSED : ",pnd_Rec_Process);
        if (Global.isEscape()) return;
        //*  ----------------------------------------------------------
        //*                  -------------
        //*  ----------------------------------------------------------
        //*                                         BG
        //*      OR = '6')     H4
        //*  ----------------------------------------------------------
        //*  ----------------------------------------------------------
        //*  ----------------------------------------------------------
        //*                  ---------------
        //*  ----------------------------------------------------------
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
    }
    private void sub_Save_Desc_Res() throws Exception                                                                                                                     //Natural: SAVE-DESC-RES
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Saver_Pnd_Sv_Res_Code.setValue(ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Residency_Code());                                                                       //Natural: ASSIGN #SV-RES-CODE := #FF4-RESIDENCY-CODE
        pnd_Saver_Pnd_Sv_State_Desc.setValue(pnd_State_Desc);                                                                                                             //Natural: ASSIGN #SV-STATE-DESC := #STATE-DESC
    }
    private void sub_Account_Rec() throws Exception                                                                                                                       //Natural: ACCOUNT-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*                  -----------
        if (condition(ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Contract_Nbr().equals(pnd_Saver_Pnd_Sv_Contract)))                                                            //Natural: IF #FF4-CONTRACT-NBR = #SV-CONTRACT
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Counters_Pnd_Rec_Cnt.getValue(1).nadd(1);                                                                                                                 //Natural: ADD 1 TO #REC-CNT ( 1 )
            pnd_Counters_Pnd_Rec_Cnt.getValue(2).nadd(1);                                                                                                                 //Natural: ADD 1 TO #REC-CNT ( 2 )
            pnd_Saver_Pnd_Sv_Contract.setValue(ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Contract_Nbr());                                                                     //Natural: ASSIGN #SV-CONTRACT := #FF4-CONTRACT-NBR
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 2
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(2)); pnd_I.nadd(1))
        {
            pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_I).nadd(ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Trans_Cnt());                                                           //Natural: ADD #FF4-TRANS-CNT TO #TRANS-CNT ( #I )
            pnd_Counters_Pnd_Gross_Amt.getValue(pnd_I).nadd(ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Sum_Gross_Amt());                                                       //Natural: ADD #FF4-SUM-GROSS-AMT TO #GROSS-AMT ( #I )
            pnd_Counters_Pnd_Tax_Free_Ivc.getValue(pnd_I).nadd(ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Sum_Ivc_Amt());                                                      //Natural: ADD #FF4-SUM-IVC-AMT TO #TAX-FREE-IVC ( #I )
            pnd_Counters_Pnd_Int_Amt.getValue(pnd_I).nadd(ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Sum_Int_Amt());                                                           //Natural: ADD #FF4-SUM-INT-AMT TO #INT-AMT ( #I )
            pnd_Counters_Pnd_State_Amt.getValue(pnd_I).nadd(ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Sum_State_Wthld());                                                     //Natural: ADD #FF4-SUM-STATE-WTHLD TO #STATE-AMT ( #I )
            pnd_Counters_Pnd_Local_Amt.getValue(pnd_I).nadd(ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Sum_Local_Wthld());                                                     //Natural: ADD #FF4-SUM-LOCAL-WTHLD TO #LOCAL-AMT ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Tax_Amt.reset();                                                                                                                                              //Natural: RESET #TAX-AMT
        //*  10/25/99
        //*  RECHAR  5/30/2002
        //*  ROLLOVER
        //*  RS0711
        short decideConditionsMet277 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FF4-TAX-CITIZENSHIP = 'U' AND ( #FF4-DISTRIBUTION-CDE = 'N' OR = 'R' OR = 'G' OR = 'H' OR = 'Z' OR = '[' OR = '6' OR = '=' )
        if (condition((ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Tax_Citizenship().equals("U") && (((((((ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Distribution_Cde().equals("N") 
            || ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Distribution_Cde().equals("R")) || ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Distribution_Cde().equals("G")) 
            || ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Distribution_Cde().equals("H")) || ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Distribution_Cde().equals("Z")) 
            || ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Distribution_Cde().equals("[")) || ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Distribution_Cde().equals("6")) 
            || ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Distribution_Cde().equals("=")))))
        {
            decideConditionsMet277++;
            pnd_Tax_Amt.setValue(0);                                                                                                                                      //Natural: ASSIGN #TAX-AMT := 0
        }                                                                                                                                                                 //Natural: WHEN #FF4-IVC-IND = 'K'
        else if (condition(ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Ivc_Ind().equals("K")))
        {
            decideConditionsMet277++;
            pnd_Tax_Amt.compute(new ComputeParameters(false, pnd_Tax_Amt), ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Sum_Gross_Amt().subtract(ldaTwrl0905.getPnd_Flat_File4_Pnd_Ff4_Sum_Ivc_Amt())); //Natural: ASSIGN #TAX-AMT := #FF4-SUM-GROSS-AMT - #FF4-SUM-IVC-AMT
            pnd_Counters_Pnd_Taxable_Amt.getValue(1).nadd(pnd_Tax_Amt);                                                                                                   //Natural: ADD #TAX-AMT TO #TAXABLE-AMT ( 1 )
            pnd_Counters_Pnd_Taxable_Amt.getValue(2).nadd(pnd_Tax_Amt);                                                                                                   //Natural: ADD #TAX-AMT TO #TAXABLE-AMT ( 2 )
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Tax_Amt.setValue(0);                                                                                                                                      //Natural: ASSIGN #TAX-AMT := 0
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Co_Desc() throws Exception                                                                                                                           //Natural: CO-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //*                  -------
        //*   DY1
        short decideConditionsMet291 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SV-COMPANY;//Natural: VALUE 'C'
        if (condition((pnd_Saver_Pnd_Sv_Company.equals("C"))))
        {
            decideConditionsMet291++;
            pnd_Print_Com.setValue("CREF");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'CREF'
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pnd_Saver_Pnd_Sv_Company.equals("L"))))
        {
            decideConditionsMet291++;
            pnd_Print_Com.setValue("LIFE");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'LIFE'
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pnd_Saver_Pnd_Sv_Company.equals("T"))))
        {
            decideConditionsMet291++;
            pnd_Print_Com.setValue("TIAA");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'TIAA'
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Saver_Pnd_Sv_Company.equals("S"))))
        {
            decideConditionsMet291++;
            pnd_Print_Com.setValue("TCII");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'TCII'
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((pnd_Saver_Pnd_Sv_Company.equals("F"))))
        {
            decideConditionsMet291++;
            pnd_Print_Com.setValue("FSBC");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'FSBC'
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((pnd_Saver_Pnd_Sv_Company.equals("X"))))
        {
            decideConditionsMet291++;
            pnd_Print_Com.setValue("TRST");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'TRST'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Print_Com.setValue("OTHR");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'OTHR'
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Print_Detail() throws Exception                                                                                                                      //Natural: PRINT-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ------------
        getReports().write(1, ReportOption.NOTITLE,pnd_Saver_Pnd_Sv_State_Desc,new ColumnSpacing(1),pnd_Counters_Pnd_Rec_Cnt.getValue(1), new ReportEditMask              //Natural: WRITE ( 1 ) #SV-STATE-DESC 1X #REC-CNT ( 1 ) 1X #TRANS-CNT ( 1 ) 1X #GROSS-AMT ( 1 ) 1X #TAX-FREE-IVC ( 1 ) 1X #TAXABLE-AMT ( 1 ) 1X #INT-AMT ( 1 ) 1X #STATE-AMT ( 1 ) 1X #LOCAL-AMT ( 1 )
            ("ZZZZ,ZZ9"),new ColumnSpacing(1),pnd_Counters_Pnd_Trans_Cnt.getValue(1), new ReportEditMask ("ZZZZ,ZZ9"),new ColumnSpacing(1),pnd_Counters_Pnd_Gross_Amt.getValue(1), 
            new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Counters_Pnd_Tax_Free_Ivc.getValue(1), new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new 
            ColumnSpacing(1),pnd_Counters_Pnd_Taxable_Amt.getValue(1), new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Counters_Pnd_Int_Amt.getValue(1), 
            new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Counters_Pnd_State_Amt.getValue(1), new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new 
            ColumnSpacing(1),pnd_Counters_Pnd_Local_Amt.getValue(1), new ReportEditMask ("ZZZZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
    }
    private void sub_Print_Col_Total() throws Exception                                                                                                                   //Natural: PRINT-COL-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(2),"TOTAL -->",new ColumnSpacing(5),pnd_Counters_Pnd_Rec_Cnt.getValue(2), new ReportEditMask         //Natural: WRITE ( 1 ) 2X 'TOTAL -->' 5X #REC-CNT ( 2 ) 1X #TRANS-CNT ( 2 ) 1X #GROSS-AMT ( 2 ) 1X #TAX-FREE-IVC ( 2 ) 1X #TAXABLE-AMT ( 2 ) 1X #INT-AMT ( 2 ) 1X #STATE-AMT ( 2 ) 1X #LOCAL-AMT ( 2 )
            ("ZZZZ,ZZ9"),new ColumnSpacing(1),pnd_Counters_Pnd_Trans_Cnt.getValue(2), new ReportEditMask ("ZZZZ,ZZ9"),new ColumnSpacing(1),pnd_Counters_Pnd_Gross_Amt.getValue(2), 
            new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Counters_Pnd_Tax_Free_Ivc.getValue(2), new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new 
            ColumnSpacing(1),pnd_Counters_Pnd_Taxable_Amt.getValue(2), new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Counters_Pnd_Int_Amt.getValue(2), 
            new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Counters_Pnd_State_Amt.getValue(2), new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new 
            ColumnSpacing(1),pnd_Counters_Pnd_Local_Amt.getValue(2), new ReportEditMask ("ZZZZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM CO-DESC
                    sub_Co_Desc();
                    if (condition(Global.isEscape())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"TWRP0970",new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new        //Natural: WRITE ( 1 ) NOTITLE NOHDR / 'TWRP0970' 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 116T 'PAGE' *PAGE-NUMBER ( 1 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 52T 'STATE SUMMARY TOTALS' 92T 'DATE RANGE :' #START-DATE 'THRU' #END-DATE / 'RUNTIME : ' *TIMX 54T 'TAX YEAR ' #TAX-YEAR // / 'COMPANY : ' #PRINT-COM /// 18X 'RECORD' 3X 'TRANS' 10X 'GROSS' 10X 'PAYMENT ' 10X 'TAXABLE' 8X 'INTEREST' 8X 'STATE' 10X 'LOCAL' / 5X 'STATES' 7X 'COUNT' 4X 'COUNT' 10X 'AMOUNT' 11X 'IVC ' 13X 'AMOUNT' 9X 'AMOUNT' 6X 'WITHHOLDING' 4X 'WITHHOLDING' //
                        TabSetting(116),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(52),"STATE SUMMARY TOTALS",new TabSetting(92),"DATE RANGE :",pnd_Start_Date,"THRU",pnd_End_Date,NEWLINE,"RUNTIME : ",Global.getTIMX(),new 
                        TabSetting(54),"TAX YEAR ",pnd_Tax_Year,NEWLINE,NEWLINE,NEWLINE,"COMPANY : ",pnd_Print_Com,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(18),"RECORD",new 
                        ColumnSpacing(3),"TRANS",new ColumnSpacing(10),"GROSS",new ColumnSpacing(10),"PAYMENT ",new ColumnSpacing(10),"TAXABLE",new ColumnSpacing(8),"INTEREST",new 
                        ColumnSpacing(8),"STATE",new ColumnSpacing(10),"LOCAL",NEWLINE,new ColumnSpacing(5),"STATES",new ColumnSpacing(7),"COUNT",new ColumnSpacing(4),"COUNT",new 
                        ColumnSpacing(10),"AMOUNT",new ColumnSpacing(11),"IVC ",new ColumnSpacing(13),"AMOUNT",new ColumnSpacing(9),"AMOUNT",new ColumnSpacing(6),"WITHHOLDING",new 
                        ColumnSpacing(4),"WITHHOLDING",NEWLINE,NEWLINE);
                    //*                        51T 'AS OF ' *DATX (EM=ZD' 'L(9)' 'YYYY)
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"TWRP0970",new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new        //Natural: WRITE ( 2 ) NOTITLE NOHDR / 'TWRP0970' 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 116T 'PAGE' *PAGE-NUMBER ( 2 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 52T 'STATE SUMMARY TOTALS' 92T 'DATE RANGE :' #START-DATE 'THRU' #END-DATE / 'RUNTIME : ' *TIMX 41T 'EXCEPTION REPORT - STATE CODES NOT IN TABLE' / 54T 'TAX YEAR ' #TAX-YEAR // 1X 'COMPANY' 3X 'STATE' / 3X 'CODE' 5X 'CODE'
                        TabSetting(116),"PAGE",getReports().getPageNumberDbs(2),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(52),"STATE SUMMARY TOTALS",new TabSetting(92),"DATE RANGE :",pnd_Start_Date,"THRU",pnd_End_Date,NEWLINE,"RUNTIME : ",Global.getTIMX(),new 
                        TabSetting(41),"EXCEPTION REPORT - STATE CODES NOT IN TABLE",NEWLINE,new TabSetting(54),"TAX YEAR ",pnd_Tax_Year,NEWLINE,NEWLINE,new 
                        ColumnSpacing(1),"COMPANY",new ColumnSpacing(3),"STATE",NEWLINE,new ColumnSpacing(3),"CODE",new ColumnSpacing(5),"CODE");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");
        Global.format(2, "PS=60 LS=132");
    }
}
