/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:34:40 PM
**        * FROM NATURAL PROGRAM : Twrp3145
************************************************************
**        * FILE NAME            : Twrp3145.java
**        * CLASS NAME           : Twrp3145
**        * INSTANCE NAME        : Twrp3145
************************************************************
************************************************************************
*
* PROGRAM  : TWRP3145 (COPY VERSION OF TWRP0945)
* FUNCTION : PRINTS CANADIAN STATE TRANSACTIONS
* INPUT    : FLAT-FILE1 & 3
*          : EDITH 4/26/99
*          : TO COMPUTE TAXABLE AMOUNT IF IVC-PROTECT IS ON OR
*              IVC-IND = K BUT NOT ROLL-OVER  (EDITH 7/13/99)
*            INCLUDED NEW COMPANY CODE 'L' (TIAA-LIFE)
* 12/14/99 : NEW DEFINITION OF ROLLOVER BASED ON DISTRIBUTION CODE
*            UPDATED DUE TO DELETION OF IA MAINTENANCE (TMAL,TMIL)
* 10/08/02 JH DISTR CODE 6 (TAXFREE EXCHANGE) IS TREATED LIKE ROLLOVER
* 11/08/04 : MS - TOPS RELEASE 3 - TRUST
* 01/11/05 RM TOPS RELEASE 3 CHANGES
*             ADD NEW COMPANY CODE 'X' FOR TRUST COMPANY AND NEW SOURCE
*             CODES 'OP', 'NL', 'PL'. UPDATE ALL RELATED LOGIC.
* 06/06/06: RM - NAVISYS CHANGES
*           THE NEW SOURCE CODE 'NV' HAS BEEN ADDED FOR NAVISYS PAYMENTS
*           AND THE EXISTING 'NL' AND 'PL'ONLINE MODIFICATION SOURCE
*           CODE WILL BE USED FOR BOTH TOPS AND NAVISYS. SINCE SOURCE
*           CODES ARE FOR SORTING PURPOSES ONLY IN THIS PROGRAM, THERE
*           IS NO NEED FOR ANY CODING CHANGES.
* 07/19/11  RS ADDED TEST FOR DIST CODE '=' (H4)    SCAN RS0711
*              'Roll Roth 403 to Roth IRA - Death'
* 10/18/11  JB NON-ERISA TDA - ADD COMPANY CODE 'F' SCAN JB1011
*
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3145 extends BLNatBase
{
    // Data Areas
    private LdaTwrl3001 ldaTwrl3001;
    private LdaTwrl0902 ldaTwrl0902;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Trans_Cnt;
    private DbsField pnd_Counters_Pnd_Gross_Amt;
    private DbsField pnd_Counters_Pnd_Ivc_Amt;
    private DbsField pnd_Counters_Pnd_Taxable_Amt;
    private DbsField pnd_Counters_Pnd_Int_Amt;
    private DbsField pnd_Counters_Pnd_Fed_Amt;
    private DbsField pnd_Counters_Pnd_Nra_Amt;
    private DbsField pnd_Counters_Pnd_Can_Amt;
    private DbsField pnd_Counters_Pnd_Pr_Amt;

    private DbsGroup pnd_Print_Fields;
    private DbsField pnd_Print_Fields_Pnd_Prtfld1;
    private DbsField pnd_Print_Fields_Pnd_Prtfld2;
    private DbsField pnd_Print_Fields_Pnd_Prtfld3;
    private DbsField pnd_Print_Fields_Pnd_Prtfld4;
    private DbsField pnd_Print_Fields_Pnd_Prtfld5;
    private DbsField pnd_Print_Fields_Pnd_Prtfld6;
    private DbsField pnd_Print_Fields_Pnd_Prtfld7;
    private DbsField pnd_Print_Fields_Pnd_Prtfld8;

    private DbsGroup pnd_Print0_Fields;
    private DbsField pnd_Print0_Fields_Pnd_Prtfld9;
    private DbsField pnd_Print0_Fields_Pnd_Prtfld10;
    private DbsField pnd_Rec_Read;
    private DbsField pnd_Rec_Process;
    private DbsField pnd_Print_Com;
    private DbsField pnd_Source;
    private DbsField pnd_Sv_Source;
    private DbsField pnd_New_Source;
    private DbsField pnd_Occ;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_First_Rec;

    private DbsGroup pnd_Var_File1;
    private DbsField pnd_Var_File1_Pnd_Tax_Year;
    private DbsField pnd_Var_File1_Pnd_Intf_Date_Fr;
    private DbsField pnd_Var_File1_Pnd_Intf_Date_To;
    private DbsField pnd_Var_File1_Pnd_Pymt_Date_Fr;
    private DbsField pnd_Var_File1_Pnd_Pymt_Date_To;
    private DbsField pnd_A;
    private DbsField pnd_Page;
    private DbsField pnd_Sub_Skip;
    private DbsField pnd_Ctr;
    private DbsField pnd_Sv_Srce12;
    private DbsField pnd_Source12;
    private DbsField pnd_Source34;

    private DbsGroup pnd_Savers;
    private DbsField pnd_Savers_Pnd_Sv_Company;
    private DbsField pnd_Savers_Pnd_Sv_Srce_Code;
    private DbsField pnd_Tot_Prt;
    private DbsField pnd_Tax_Amt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl3001 = new LdaTwrl3001();
        registerRecord(ldaTwrl3001);
        ldaTwrl0902 = new LdaTwrl0902();
        registerRecord(ldaTwrl0902);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Counters = localVariables.newGroupArrayInRecord("pnd_Counters", "#COUNTERS", new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Trans_Cnt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Trans_Cnt", "#TRANS-CNT", FieldType.NUMERIC, 7);
        pnd_Counters_Pnd_Gross_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Counters_Pnd_Ivc_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Counters_Pnd_Taxable_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Counters_Pnd_Int_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Counters_Pnd_Fed_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Fed_Amt", "#FED-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Counters_Pnd_Nra_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Nra_Amt", "#NRA-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Counters_Pnd_Can_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Can_Amt", "#CAN-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Counters_Pnd_Pr_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Pr_Amt", "#PR-AMT", FieldType.PACKED_DECIMAL, 12, 2);

        pnd_Print_Fields = localVariables.newGroupInRecord("pnd_Print_Fields", "#PRINT-FIELDS");
        pnd_Print_Fields_Pnd_Prtfld1 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld1", "#PRTFLD1", FieldType.STRING, 17);
        pnd_Print_Fields_Pnd_Prtfld2 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld2", "#PRTFLD2", FieldType.STRING, 8);
        pnd_Print_Fields_Pnd_Prtfld3 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld3", "#PRTFLD3", FieldType.STRING, 18);
        pnd_Print_Fields_Pnd_Prtfld4 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld4", "#PRTFLD4", FieldType.STRING, 15);
        pnd_Print_Fields_Pnd_Prtfld5 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld5", "#PRTFLD5", FieldType.STRING, 18);
        pnd_Print_Fields_Pnd_Prtfld6 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld6", "#PRTFLD6", FieldType.STRING, 15);
        pnd_Print_Fields_Pnd_Prtfld7 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld7", "#PRTFLD7", FieldType.STRING, 15);
        pnd_Print_Fields_Pnd_Prtfld8 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld8", "#PRTFLD8", FieldType.STRING, 15);

        pnd_Print0_Fields = localVariables.newGroupInRecord("pnd_Print0_Fields", "#PRINT0-FIELDS");
        pnd_Print0_Fields_Pnd_Prtfld9 = pnd_Print0_Fields.newFieldInGroup("pnd_Print0_Fields_Pnd_Prtfld9", "#PRTFLD9", FieldType.STRING, 15);
        pnd_Print0_Fields_Pnd_Prtfld10 = pnd_Print0_Fields.newFieldInGroup("pnd_Print0_Fields_Pnd_Prtfld10", "#PRTFLD10", FieldType.STRING, 15);
        pnd_Rec_Read = localVariables.newFieldInRecord("pnd_Rec_Read", "#REC-READ", FieldType.NUMERIC, 9);
        pnd_Rec_Process = localVariables.newFieldInRecord("pnd_Rec_Process", "#REC-PROCESS", FieldType.NUMERIC, 9);
        pnd_Print_Com = localVariables.newFieldInRecord("pnd_Print_Com", "#PRINT-COM", FieldType.STRING, 4);
        pnd_Source = localVariables.newFieldInRecord("pnd_Source", "#SOURCE", FieldType.STRING, 2);
        pnd_Sv_Source = localVariables.newFieldInRecord("pnd_Sv_Source", "#SV-SOURCE", FieldType.STRING, 2);
        pnd_New_Source = localVariables.newFieldInRecord("pnd_New_Source", "#NEW-SOURCE", FieldType.STRING, 4);
        pnd_Occ = localVariables.newFieldInRecord("pnd_Occ", "#OCC", FieldType.NUMERIC, 1);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 1);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 1);
        pnd_First_Rec = localVariables.newFieldInRecord("pnd_First_Rec", "#FIRST-REC", FieldType.BOOLEAN, 1);

        pnd_Var_File1 = localVariables.newGroupInRecord("pnd_Var_File1", "#VAR-FILE1");
        pnd_Var_File1_Pnd_Tax_Year = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);
        pnd_Var_File1_Pnd_Intf_Date_Fr = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Intf_Date_Fr", "#INTF-DATE-FR", FieldType.STRING, 8);
        pnd_Var_File1_Pnd_Intf_Date_To = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Intf_Date_To", "#INTF-DATE-TO", FieldType.STRING, 8);
        pnd_Var_File1_Pnd_Pymt_Date_Fr = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Pymt_Date_Fr", "#PYMT-DATE-FR", FieldType.STRING, 8);
        pnd_Var_File1_Pnd_Pymt_Date_To = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Pymt_Date_To", "#PYMT-DATE-TO", FieldType.STRING, 8);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.BOOLEAN, 1);
        pnd_Page = localVariables.newFieldInRecord("pnd_Page", "#PAGE", FieldType.BOOLEAN, 1);
        pnd_Sub_Skip = localVariables.newFieldInRecord("pnd_Sub_Skip", "#SUB-SKIP", FieldType.BOOLEAN, 1);
        pnd_Ctr = localVariables.newFieldInRecord("pnd_Ctr", "#CTR", FieldType.NUMERIC, 2);
        pnd_Sv_Srce12 = localVariables.newFieldInRecord("pnd_Sv_Srce12", "#SV-SRCE12", FieldType.STRING, 2);
        pnd_Source12 = localVariables.newFieldInRecord("pnd_Source12", "#SOURCE12", FieldType.STRING, 2);
        pnd_Source34 = localVariables.newFieldInRecord("pnd_Source34", "#SOURCE34", FieldType.STRING, 2);

        pnd_Savers = localVariables.newGroupInRecord("pnd_Savers", "#SAVERS");
        pnd_Savers_Pnd_Sv_Company = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Company", "#SV-COMPANY", FieldType.STRING, 1);
        pnd_Savers_Pnd_Sv_Srce_Code = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Srce_Code", "#SV-SRCE-CODE", FieldType.STRING, 4);
        pnd_Tot_Prt = localVariables.newFieldInRecord("pnd_Tot_Prt", "#TOT-PRT", FieldType.BOOLEAN, 1);
        pnd_Tax_Amt = localVariables.newFieldInRecord("pnd_Tax_Amt", "#TAX-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl3001.initializeValues();
        ldaTwrl0902.initializeValues();

        localVariables.reset();
        pnd_First_Rec.setInitialValue(true);
        pnd_A.setInitialValue(true);
        pnd_Page.setInitialValue(false);
        pnd_Sub_Skip.setInitialValue(false);
        pnd_Tot_Prt.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3145() throws Exception
    {
        super("Twrp3145");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #INFO-FF1
        while (condition(getWorkFiles().read(1, ldaTwrl3001.getPnd_Info_Ff1())))
        {
            pnd_Var_File1_Pnd_Tax_Year.setValue(ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Tax_Yr());                                                                            //Natural: ASSIGN #TAX-YEAR := #FF1-TAX-YR
            pnd_Var_File1_Pnd_Intf_Date_Fr.setValue(ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Intf_Date_Fr());                                                                  //Natural: ASSIGN #INTF-DATE-FR := #FF1-INTF-DATE-FR
            pnd_Var_File1_Pnd_Intf_Date_To.setValue(ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Intf_Date_To());                                                                  //Natural: ASSIGN #INTF-DATE-TO := #FF1-INTF-DATE-TO
            pnd_Var_File1_Pnd_Pymt_Date_Fr.setValue(ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Pymt_Date_Fr());                                                                  //Natural: ASSIGN #PYMT-DATE-FR := #FF1-PYMT-DATE-FR
            pnd_Var_File1_Pnd_Pymt_Date_To.setValue(ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Pymt_Date_To());                                                                  //Natural: ASSIGN #PYMT-DATE-TO := #FF1-PYMT-DATE-TO
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 2 RECORD #FLAT-FILE3
        while (condition(getWorkFiles().read(2, ldaTwrl0902.getPnd_Flat_File3())))
        {
            pnd_Rec_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-READ
            if (condition(!(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt().greater(getZero()))))                                                                   //Natural: ACCEPT IF #FF3-CAN-WTHLD-AMT > 0
            {
                continue;
            }
            pnd_Rec_Process.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #REC-PROCESS
            if (condition(pnd_First_Rec.getBoolean()))                                                                                                                    //Natural: IF #FIRST-REC
            {
                pnd_Savers_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                                 //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
                pnd_Savers_Pnd_Sv_Srce_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                                //Natural: ASSIGN #SV-SRCE-CODE := #FF3-SOURCE-CODE
                pnd_First_Rec.setValue(false);                                                                                                                            //Natural: ASSIGN #FIRST-REC := FALSE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code().equals(pnd_Savers_Pnd_Sv_Company)))                                                        //Natural: IF #FF3-COMPANY-CODE = #SV-COMPANY
            {
                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().equals(pnd_Savers_Pnd_Sv_Srce_Code)))                                                   //Natural: IF #FF3-SOURCE-CODE = #SV-SRCE-CODE
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM SOURCE-BRK
                    sub_Source_Brk();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                    sub_Reset_Ctrs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM COMPANY-BRK
                sub_Company_Brk();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  --------------- PROCESS-RECORD
            //*                  -------------
            pnd_Tax_Amt.reset();                                                                                                                                          //Natural: RESET #TAX-AMT
            //*  RECHAR  5/30/2002
            //*  ROLLOVER
            //*  RS0711
            short decideConditionsMet168 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FF3-TAX-CITIZENSHIP = 'U' AND ( #FF3-DISTRIBUTION-CDE = 'N' OR = 'R' OR = 'G' OR = 'H' OR = 'Z' OR = '[' OR = '6' OR = '=' )
            if (condition((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship().equals("U") && (((((((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("N") 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("R")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("G")) 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("H")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("Z")) 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("[")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("6")) 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("=")))))
            {
                decideConditionsMet168++;
                //*                                           BG
                //*        OR= '6')      H4
                pnd_Tax_Amt.setValue(0);                                                                                                                                  //Natural: ASSIGN #TAX-AMT := 0
            }                                                                                                                                                             //Natural: WHEN #FF3-IVC-PROTECT OR #FF3-IVC-IND = 'K'
            else if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Protect().getBoolean() || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Ind().equals("K")))
            {
                decideConditionsMet168++;
                pnd_Tax_Amt.compute(new ComputeParameters(false, pnd_Tax_Amt), ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt().subtract(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt())); //Natural: ASSIGN #TAX-AMT := #FF3-GROSS-AMT - #FF3-IVC-AMT
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Tax_Amt.setValue(0);                                                                                                                                  //Natural: ASSIGN #TAX-AMT := 0
            }                                                                                                                                                             //Natural: END-DECIDE
            FOR01:                                                                                                                                                        //Natural: FOR #K 1 3
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(3)); pnd_K.nadd(1))
            {
                pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_K).nadd(1);                                                                                                       //Natural: ADD 1 TO #TRANS-CNT ( #K )
                pnd_Counters_Pnd_Gross_Amt.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt());                                                       //Natural: ADD #FF3-GROSS-AMT TO #GROSS-AMT ( #K )
                pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt());                                                           //Natural: ADD #FF3-IVC-AMT TO #IVC-AMT ( #K )
                pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_K).nadd(pnd_Tax_Amt);                                                                                           //Natural: ADD #TAX-AMT TO #TAXABLE-AMT ( #K )
                pnd_Counters_Pnd_Int_Amt.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Int_Amt());                                                           //Natural: ADD #FF3-INT-AMT TO #INT-AMT ( #K )
                pnd_Counters_Pnd_Fed_Amt.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Fed_Wthld_Amt());                                                     //Natural: ADD #FF3-FED-WTHLD-AMT TO #FED-AMT ( #K )
                pnd_Counters_Pnd_Nra_Amt.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Nra_Wthld_Amt());                                                     //Natural: ADD #FF3-NRA-WTHLD-AMT TO #NRA-AMT ( #K )
                pnd_Counters_Pnd_Can_Amt.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt());                                                     //Natural: ADD #FF3-CAN-WTHLD-AMT TO #CAN-AMT ( #K )
                if (condition((((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("PR") || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("RQ"))  //Natural: IF ( #FF3-RESIDENCY-CODE = 'PR' OR = 'RQ' OR = '42' ) AND #FF3-STATE-WTHLD > 0
                    || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("42")) && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld().greater(getZero()))))
                {
                    pnd_Counters_Pnd_Pr_Amt.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld());                                                    //Natural: ADD #FF3-STATE-WTHLD TO #PR-AMT ( #K )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Rec_Process.equals(getZero())))                                                                                                                 //Natural: IF #REC-PROCESS = 0
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(40)," **** NO RECORDS PROCESSED **** ");                            //Natural: WRITE ( 1 ) //// 40T ' **** NO RECORDS PROCESSED **** '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM COMPANY-BRK
            sub_Company_Brk();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 1 )
        getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),"->- CONTROL TOTAL",NEWLINE,NEWLINE,"TOTAL NUMBER OF RECORDS READ      : ", //Natural: WRITE ( 1 ) *INIT-USER '-' *PROGRAM '->- CONTROL TOTAL' // 'TOTAL NUMBER OF RECORDS READ      : ' #REC-READ / 'TOTAL NUMBER OF RECORDS PROCESSED : ' #REC-PROCESS
            pnd_Rec_Read,NEWLINE,"TOTAL NUMBER OF RECORDS PROCESSED : ",pnd_Rec_Process);
        if (Global.isEscape()) return;
        //* *------------
        //*                  --------
        //*                  ----------
        //*                  ----------
        //*                    -------------
        //* *------------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 01 )
    }
    private void sub_Source_Brk() throws Exception                                                                                                                        //Natural: SOURCE-BRK
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ----------
                                                                                                                                                                          //Natural: PERFORM SOURCE-TO-PRT
        sub_Source_To_Prt();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(1,2).equals(pnd_Savers_Pnd_Sv_Srce_Code.getSubstring(1,2))))                       //Natural: IF SUBSTRING ( #FF3-SOURCE-CODE,1,2 ) = SUBSTRING ( #SV-SRCE-CODE,1,2 )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM MOVE-CTR2-CTR4
            sub_Move_Ctr2_Ctr4();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Savers_Pnd_Sv_Srce_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                                        //Natural: ASSIGN #SV-SRCE-CODE := #FF3-SOURCE-CODE
    }
    private void sub_Company_Brk() throws Exception                                                                                                                       //Natural: COMPANY-BRK
    {
        if (BLNatReinput.isReinput()) return;

        //*                  -----------
                                                                                                                                                                          //Natural: PERFORM SOURCE-BRK
        sub_Source_Brk();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Tot_Prt.getBoolean()))                                                                                                                          //Natural: IF #TOT-PRT
        {
            ignore();
            //*  PRINT TOTAL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM MOVE-CTR2-CTR4
            sub_Move_Ctr2_Ctr4();
            if (condition(Global.isEscape())) {return;}
            pnd_Occ.setValue(4);                                                                                                                                          //Natural: ASSIGN #OCC := 4
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
            sub_Print_Fields();
            if (condition(Global.isEscape())) {return;}
            //*  PRINT GRANDTOTAL
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Occ.setValue(3);                                                                                                                                              //Natural: ASSIGN #OCC := 3
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
        sub_Print_Fields();
        if (condition(Global.isEscape())) {return;}
        pnd_Savers_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                                         //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
        pnd_Counters.getValue("*").reset();                                                                                                                               //Natural: RESET #COUNTERS ( * )
        pnd_A.setValue(true);                                                                                                                                             //Natural: ASSIGN #A := TRUE
    }
    private void sub_Co_Desc() throws Exception                                                                                                                           //Natural: CO-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------
        //*  JB1011
        short decideConditionsMet274 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SV-COMPANY;//Natural: VALUE 'C'
        if (condition((pnd_Savers_Pnd_Sv_Company.equals("C"))))
        {
            decideConditionsMet274++;
            pnd_Print_Com.setValue("CREF");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'CREF'
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("T"))))
        {
            decideConditionsMet274++;
            pnd_Print_Com.setValue("TIAA");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'TIAA'
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("L"))))
        {
            decideConditionsMet274++;
            pnd_Print_Com.setValue("LIFE");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'LIFE'
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("S"))))
        {
            decideConditionsMet274++;
            pnd_Print_Com.setValue("TCII");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'TCII'
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("X"))))
        {
            decideConditionsMet274++;
            pnd_Print_Com.setValue("TRST");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'TRST'
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("F"))))
        {
            decideConditionsMet274++;
            pnd_Print_Com.setValue("FSB ");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'FSB '
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Print_Com.setValue("    ");                                                                                                                               //Natural: ASSIGN #PRINT-COM := '    '
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Move_Ctr2_Ctr4() throws Exception                                                                                                                    //Natural: MOVE-CTR2-CTR4
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Counters_Pnd_Trans_Cnt.getValue(4).setValue(pnd_Counters_Pnd_Trans_Cnt.getValue(2));                                                                          //Natural: ASSIGN #TRANS-CNT ( 4 ) := #TRANS-CNT ( 2 )
        pnd_Counters_Pnd_Gross_Amt.getValue(4).setValue(pnd_Counters_Pnd_Gross_Amt.getValue(2));                                                                          //Natural: ASSIGN #GROSS-AMT ( 4 ) := #GROSS-AMT ( 2 )
        pnd_Counters_Pnd_Ivc_Amt.getValue(4).setValue(pnd_Counters_Pnd_Ivc_Amt.getValue(2));                                                                              //Natural: ASSIGN #IVC-AMT ( 4 ) := #IVC-AMT ( 2 )
        pnd_Counters_Pnd_Taxable_Amt.getValue(4).setValue(pnd_Counters_Pnd_Taxable_Amt.getValue(2));                                                                      //Natural: ASSIGN #TAXABLE-AMT ( 4 ) := #TAXABLE-AMT ( 2 )
        pnd_Counters_Pnd_Int_Amt.getValue(4).setValue(pnd_Counters_Pnd_Int_Amt.getValue(2));                                                                              //Natural: ASSIGN #INT-AMT ( 4 ) := #INT-AMT ( 2 )
        pnd_Counters_Pnd_Fed_Amt.getValue(4).setValue(pnd_Counters_Pnd_Fed_Amt.getValue(2));                                                                              //Natural: ASSIGN #FED-AMT ( 4 ) := #FED-AMT ( 2 )
        pnd_Counters_Pnd_Nra_Amt.getValue(4).setValue(pnd_Counters_Pnd_Nra_Amt.getValue(2));                                                                              //Natural: ASSIGN #NRA-AMT ( 4 ) := #NRA-AMT ( 2 )
        pnd_Counters_Pnd_Can_Amt.getValue(4).setValue(pnd_Counters_Pnd_Can_Amt.getValue(2));                                                                              //Natural: ASSIGN #CAN-AMT ( 4 ) := #CAN-AMT ( 2 )
        pnd_Counters_Pnd_Pr_Amt.getValue(4).setValue(pnd_Counters_Pnd_Pr_Amt.getValue(2));                                                                                //Natural: ASSIGN #PR-AMT ( 4 ) := #PR-AMT ( 2 )
        pnd_Occ.setValue(2);                                                                                                                                              //Natural: ASSIGN #OCC := 2
    }
    private void sub_Reset_Ctrs() throws Exception                                                                                                                        //Natural: RESET-CTRS
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #J 1 #OCC
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Occ)); pnd_J.nadd(1))
        {
            pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_J).reset();                                                                                                           //Natural: RESET #TRANS-CNT ( #J ) #GROSS-AMT ( #J ) #IVC-AMT ( #J ) #TAXABLE-AMT ( #J ) #INT-AMT ( #J ) #FED-AMT ( #J ) #NRA-AMT ( #J ) #CAN-AMT ( #J ) #PR-AMT ( #J )
            pnd_Counters_Pnd_Gross_Amt.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Int_Amt.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Fed_Amt.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Nra_Amt.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Can_Amt.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Pr_Amt.getValue(pnd_J).reset();
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Print_Fields() throws Exception                                                                                                                      //Natural: PRINT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ------------
        //*  FOR DETAIL PER SOURCE-CODE
        short decideConditionsMet313 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #OCC;//Natural: VALUE 1
        if (condition((pnd_Occ.equals(1))))
        {
            decideConditionsMet313++;
            pnd_Print_Fields_Pnd_Prtfld1.setValue(pnd_New_Source);                                                                                                        //Natural: MOVE #NEW-SOURCE TO #PRTFLD1
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN
            sub_Print_Rtn();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN0
            sub_Print_Rtn0();
            if (condition(Global.isEscape())) {return;}
            //*   FOR TOTAL
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
            sub_Print_Rtn1();
            if (condition(Global.isEscape())) {return;}
            pnd_Tot_Prt.setValue(false);                                                                                                                                  //Natural: ASSIGN #TOT-PRT := FALSE
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_Occ.equals(4))))
        {
            decideConditionsMet313++;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            if (condition(pnd_Sv_Srce12.equals("ZZ")))                                                                                                                    //Natural: IF #SV-SRCE12 = 'ZZ'
            {
                pnd_Sv_Srce12.setValue("ML");                                                                                                                             //Natural: ASSIGN #SV-SRCE12 := 'ML'
            }                                                                                                                                                             //Natural: END-IF
            //*   TOPS3  01/11/05   RM
            if (condition(pnd_Sv_Srce12.equals("YY")))                                                                                                                    //Natural: IF #SV-SRCE12 = 'YY'
            {
                pnd_Sv_Srce12.setValue("NL");                                                                                                                             //Natural: ASSIGN #SV-SRCE12 := 'NL'
            }                                                                                                                                                             //Natural: END-IF
            pnd_Print_Fields_Pnd_Prtfld1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TOTAL      (", pnd_Sv_Srce12, ")"));                                   //Natural: COMPRESS 'TOTAL      (' #SV-SRCE12 ')' TO #PRTFLD1 LEAVING NO SPACE
            pnd_Sub_Skip.setValue(true);                                                                                                                                  //Natural: ASSIGN #SUB-SKIP := TRUE
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN
            sub_Print_Rtn();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN0
            sub_Print_Rtn0();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
            sub_Print_Rtn1();
            if (condition(Global.isEscape())) {return;}
            pnd_Counters.getValue(4).reset();                                                                                                                             //Natural: RESET #COUNTERS ( 4 )
            pnd_Tot_Prt.setValue(true);                                                                                                                                   //Natural: ASSIGN #TOT-PRT := TRUE
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_Occ.equals(3))))
        {
            decideConditionsMet313++;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Print_Fields_Pnd_Prtfld1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "GRANDTOTAL (", pnd_Print_Com, ")"));                                   //Natural: COMPRESS 'GRANDTOTAL (' #PRINT-COM ')' TO #PRTFLD1 LEAVING NO SPACE
            pnd_Page.setValue(true);                                                                                                                                      //Natural: ASSIGN #PAGE := TRUE
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN
            sub_Print_Rtn();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN0
            sub_Print_Rtn0();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
            sub_Print_Rtn1();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Print_Rtn() throws Exception                                                                                                                         //Natural: PRINT-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ---------
        pnd_Print_Fields_Pnd_Prtfld2.setValueEdited(pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_Occ),new ReportEditMask("ZZZZ,ZZ9"));                                         //Natural: MOVE EDITED #TRANS-CNT ( #OCC ) ( EM = ZZZZ,ZZ9 ) TO #PRTFLD2
        pnd_Print_Fields_Pnd_Prtfld3.setValueEdited(pnd_Counters_Pnd_Gross_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZZ,ZZZ,ZZ9.99-"));                               //Natural: MOVE EDITED #GROSS-AMT ( #OCC ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) TO #PRTFLD3
        pnd_Print_Fields_Pnd_Prtfld4.setValueEdited(pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                                    //Natural: MOVE EDITED #IVC-AMT ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD4
        pnd_Print_Fields_Pnd_Prtfld5.setValueEdited(pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZZ,ZZZ,ZZ9.99-"));                             //Natural: MOVE EDITED #TAXABLE-AMT ( #OCC ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) TO #PRTFLD5
        pnd_Print_Fields_Pnd_Prtfld6.setValueEdited(pnd_Counters_Pnd_Int_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                                    //Natural: MOVE EDITED #INT-AMT ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD6
        pnd_Print_Fields_Pnd_Prtfld7.setValueEdited(pnd_Counters_Pnd_Fed_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                                    //Natural: MOVE EDITED #FED-AMT ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD7
        pnd_Print_Fields_Pnd_Prtfld8.setValueEdited(pnd_Counters_Pnd_Nra_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                                    //Natural: MOVE EDITED #NRA-AMT ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD8
    }
    private void sub_Print_Rtn0() throws Exception                                                                                                                        //Natural: PRINT-RTN0
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Print0_Fields_Pnd_Prtfld9.setValueEdited(pnd_Counters_Pnd_Can_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                                   //Natural: MOVE EDITED #CAN-AMT ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD9
        pnd_Print0_Fields_Pnd_Prtfld10.setValueEdited(pnd_Counters_Pnd_Pr_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                                   //Natural: MOVE EDITED #PR-AMT ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD10
    }
    private void sub_Print_Rtn1() throws Exception                                                                                                                        //Natural: PRINT-RTN1
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,pnd_Print_Fields_Pnd_Prtfld1,pnd_Print_Fields_Pnd_Prtfld2,pnd_Print_Fields_Pnd_Prtfld3,pnd_Print_Fields_Pnd_Prtfld4,pnd_Print_Fields_Pnd_Prtfld5,pnd_Print_Fields_Pnd_Prtfld6,pnd_Print_Fields_Pnd_Prtfld7,pnd_Print_Fields_Pnd_Prtfld8,NEWLINE,new  //Natural: WRITE ( 1 ) #PRINT-FIELDS / 98T #PRINT0-FIELDS
            TabSetting(98),pnd_Print0_Fields_Pnd_Prtfld9,pnd_Print0_Fields_Pnd_Prtfld10);
        if (Global.isEscape()) return;
        pnd_Print_Fields.reset();                                                                                                                                         //Natural: RESET #PRINT-FIELDS #PRINT0-FIELDS
        pnd_Print0_Fields.reset();
        if (condition(pnd_Sub_Skip.getBoolean()))                                                                                                                         //Natural: IF #SUB-SKIP
        {
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Sub_Skip.setValue(false);                                                                                                                                 //Natural: ASSIGN #SUB-SKIP := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Page.getBoolean()))                                                                                                                             //Natural: IF #PAGE
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            pnd_Page.setValue(false);                                                                                                                                     //Natural: ASSIGN #PAGE := FALSE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Source_To_Prt() throws Exception                                                                                                                     //Natural: SOURCE-TO-PRT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Source12.setValue(pnd_Savers_Pnd_Sv_Srce_Code.getSubstring(1,2));                                                                                             //Natural: ASSIGN #SOURCE12 := SUBSTRING ( #SV-SRCE-CODE,1,2 )
        pnd_Source34.setValue(pnd_Savers_Pnd_Sv_Srce_Code.getSubstring(3,2));                                                                                             //Natural: ASSIGN #SOURCE34 := SUBSTRING ( #SV-SRCE-CODE,3,2 )
        short decideConditionsMet380 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SOURCE34;//Natural: VALUE '  '
        if (condition((pnd_Source34.equals("  "))))
        {
            decideConditionsMet380++;
            pnd_Source.setValue(pnd_Source12);                                                                                                                            //Natural: ASSIGN #SOURCE := #SOURCE12
        }                                                                                                                                                                 //Natural: VALUE 'OL', 'TM', 'ML', 'AL', 'IL','PL', 'NL'
        else if (condition((pnd_Source34.equals("OL") || pnd_Source34.equals("TM") || pnd_Source34.equals("ML") || pnd_Source34.equals("AL") || pnd_Source34.equals("IL") 
            || pnd_Source34.equals("PL") || pnd_Source34.equals("NL"))))
        {
            decideConditionsMet380++;
            pnd_Source.setValue(pnd_Source34);                                                                                                                            //Natural: ASSIGN #SOURCE := #SOURCE34
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Source.setValue(" ");                                                                                                                                     //Natural: ASSIGN #SOURCE := ' '
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet389 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SOURCE;//Natural: VALUE 'DS'
        if (condition((pnd_Source.equals("DS"))))
        {
            decideConditionsMet389++;
            pnd_New_Source.setValue("DSS");                                                                                                                               //Natural: ASSIGN #NEW-SOURCE := 'DSS'
        }                                                                                                                                                                 //Natural: VALUE 'GS'
        else if (condition((pnd_Source.equals("GS"))))
        {
            decideConditionsMet389++;
            pnd_New_Source.setValue("GSRA");                                                                                                                              //Natural: ASSIGN #NEW-SOURCE := 'GSRA'
        }                                                                                                                                                                 //Natural: VALUE 'MS'
        else if (condition((pnd_Source.equals("MS"))))
        {
            decideConditionsMet389++;
            pnd_New_Source.setValue("MSS");                                                                                                                               //Natural: ASSIGN #NEW-SOURCE := 'MSS'
        }                                                                                                                                                                 //Natural: VALUE 'SS'
        else if (condition((pnd_Source.equals("SS"))))
        {
            decideConditionsMet389++;
            pnd_New_Source.setValue("SSSS");                                                                                                                              //Natural: ASSIGN #NEW-SOURCE := 'SSSS'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_New_Source.setValue(pnd_Source);                                                                                                                          //Natural: ASSIGN #NEW-SOURCE := #SOURCE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  TRUE AT BREAK OF COMPANY
        if (condition(pnd_A.getBoolean()))                                                                                                                                //Natural: IF #A
        {
            pnd_Sv_Srce12.setValue(pnd_Source12);                                                                                                                         //Natural: ASSIGN #SV-SRCE12 := #SOURCE12
            pnd_A.setValue(false);                                                                                                                                        //Natural: ASSIGN #A := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Source12.equals(pnd_Sv_Srce12)))                                                                                                                //Natural: IF #SOURCE12 = #SV-SRCE12
        {
            pnd_Occ.setValue(1);                                                                                                                                          //Natural: ASSIGN #OCC := 1
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
            sub_Print_Fields();
            if (condition(Global.isEscape())) {return;}
            //*  PRINT TOTAL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Occ.setValue(4);                                                                                                                                          //Natural: ASSIGN #OCC := 4
            //*  PRINT DETAIL OF SV-SRCE-CDE
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
            sub_Print_Fields();
            if (condition(Global.isEscape())) {return;}
            pnd_Occ.setValue(1);                                                                                                                                          //Natural: ASSIGN #OCC := 1
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
            sub_Print_Fields();
            if (condition(Global.isEscape())) {return;}
            pnd_Sv_Srce12.setValue(pnd_Source12);                                                                                                                         //Natural: ASSIGN #SV-SRCE12 := #SOURCE12
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                                                                                                                                                                          //Natural: PERFORM CO-DESC
                    sub_Co_Desc();
                    if (condition(Global.isEscape())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 01 ) NOTITLE NOHDR / *INIT-USER '-' *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 121T 'PAGE' *PAGE-NUMBER ( 1 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 49T 'CANADIAN STATE TRANSACTIONS' 95T 'INTERFACE DATE' #INTF-DATE-FR 'THRU' #INTF-DATE-TO / 'RUNTIME : ' *TIMX 54T 'TAX YEAR ' #TAX-YEAR 95T ' PAYMENT  DATE' #PYMT-DATE-FR 'THRU' #PYMT-DATE-TO /// 'COMPANY : ' #PRINT-COM /// 'SYSTEM' 14X 'TRANS' 10X 'GROSS' 11X 'TAX FREE' 10X 'TAXABLE' 9X 'INTEREST' 8X 'FEDERAL' 11X 'NRA' / 'SRCE' 16X 'COUNT' 10X 'AMOUNT' 13X 'IVC' 13X 'AMOUNT' 10X 'AMOUNT' 8X 'WITHHELD' 8X 'WITHHELD' / 98T '--------------' 2X '--------------' / 101T 'CANADIAN' 11X 'PR' / 101T 'WITHHELD' 8X 'WITHHELD' //
                        TabSetting(121),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(49),"CANADIAN STATE TRANSACTIONS",new TabSetting(95),"INTERFACE DATE",pnd_Var_File1_Pnd_Intf_Date_Fr,"THRU",pnd_Var_File1_Pnd_Intf_Date_To,NEWLINE,"RUNTIME : ",Global.getTIMX(),new 
                        TabSetting(54),"TAX YEAR ",pnd_Var_File1_Pnd_Tax_Year,new TabSetting(95)," PAYMENT  DATE",pnd_Var_File1_Pnd_Pymt_Date_Fr,"THRU",pnd_Var_File1_Pnd_Pymt_Date_To,NEWLINE,NEWLINE,NEWLINE,"COMPANY : ",pnd_Print_Com,NEWLINE,NEWLINE,NEWLINE,"SYSTEM",new 
                        ColumnSpacing(14),"TRANS",new ColumnSpacing(10),"GROSS",new ColumnSpacing(11),"TAX FREE",new ColumnSpacing(10),"TAXABLE",new ColumnSpacing(9),"INTEREST",new 
                        ColumnSpacing(8),"FEDERAL",new ColumnSpacing(11),"NRA",NEWLINE,"SRCE",new ColumnSpacing(16),"COUNT",new ColumnSpacing(10),"AMOUNT",new 
                        ColumnSpacing(13),"IVC",new ColumnSpacing(13),"AMOUNT",new ColumnSpacing(10),"AMOUNT",new ColumnSpacing(8),"WITHHELD",new ColumnSpacing(8),"WITHHELD",NEWLINE,new 
                        TabSetting(98),"--------------",new ColumnSpacing(2),"--------------",NEWLINE,new TabSetting(101),"CANADIAN",new ColumnSpacing(11),"PR",NEWLINE,new 
                        TabSetting(101),"WITHHELD",new ColumnSpacing(8),"WITHHELD",NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");
    }
}
