/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:34:32 PM
**        * FROM NATURAL PROGRAM : Twrp3096
************************************************************
**        * FILE NAME            : Twrp3096.java
**        * CLASS NAME           : Twrp3096
**        * INSTANCE NAME        : Twrp3096
************************************************************
************************************************************************
*
* PROGRAM  : TWRP3096
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : CALIFORNIA QUARTERLY FILE PRINT DUMP REPORT.
* CREATED  : 08 / 20 / 2014.
*   BY     : COGNIZANT.
* FUNCTION : THIS PROGRAM IS CLONED AND MODIFIED FROM TWRP3095 TO READ
*          : THE CALIFORNIA QUARTERLY TAPE FILE WITH NEW FORMAT AND
*          : PRODUCE A PRINTED DUMP OF ALL THE RECORDS ON FILE.
*
* HISTORY  :
* 08/20/14 CTS - THIS PROGRAM WILL READ THE CALIFORNIA QUARTERLY TAPE
*                FILE (WITH NEW LAYOUT) AND PRODUCE A PRINTED DUMP
*                OF ALL THE REOCORDS FROM INPUT FILE. THIS IS A CLONED
*                PROGRAM FROM TWRP3095. CHANGES ARE TAGGED WITH CTS.
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3096 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Input_Parm;

    private DbsGroup pnd_Input_Parm__R_Field_1;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To;

    private DbsGroup pnd_Input_Parm__R_Field_2;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy;

    private DbsGroup pnd_Input_Parm__R_Field_3;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Cc;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Yy;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd;
    private DbsField pnd_Input_Parm_Pnd_Filler2;
    private DbsField pnd_Input_Parm_Pnd_Year_End_Adj_Ind;
    private DbsField pnd_Columns;
    private DbsField pnd_Read_Ctr;
    private DbsField i;
    private DbsField j;
    private DbsField k;
    private DbsField l;
    private DbsField re_In;

    private DbsGroup re_In__R_Field_4;
    private DbsField re_In_E_Filler_1;
    private DbsField re_In_E_Ret_Qtr;
    private DbsField re_In_E_Tax_Year;
    private DbsField re_In_E_State_Ein;
    private DbsField re_In_E_Buss_Name;
    private DbsField re_In_E_Add_Line_1;
    private DbsField re_In_E_Add_Line_2;
    private DbsField re_In_E_City;
    private DbsField re_In_E_Country;
    private DbsField re_In_E_Filler_2;
    private DbsField re_In_E_Zip_Code;
    private DbsField re_In_E_Filler_3;
    private DbsField re_In_E_Nbr_Employees;
    private DbsField re_In_E_Wh_Tot_Wages;
    private DbsField re_In_E_Wh_Tot_Income_Tax;
    private DbsField re_In_E_Wh_Taxable_Wages;
    private DbsField re_In_E_Month1_Employees;
    private DbsField re_In_E_Month2_Employees;
    private DbsField re_In_E_Month3_Employees;
    private DbsField re_In_E_Filler_4;
    private DbsField re_In_E_Transmitter;
    private DbsField re_In_E_Filler_5;

    private DbsGroup re_In__R_Field_5;
    private DbsField re_In_Rs_Soc_Sec_Nbr;
    private DbsField re_In_Rs_First_Name;
    private DbsField re_In_Rs_Middle_Name;
    private DbsField re_In_Rs_Last_Name;
    private DbsField re_In_Rs_Filler_1;
    private DbsField re_In_Rs_Wage_Plan_Code;
    private DbsField re_In_Rs_Filler_2;
    private DbsField re_In_Rs_Tot_Wages;
    private DbsField re_In_Rs_Filler_3;
    private DbsField re_In_Rs_Taxable_Wages;
    private DbsField re_In_Rs_Filler_4;
    private DbsField re_In_Rs_Tax_Withhold;
    private DbsField re_In_Rs_Filler_5;
    private DbsField re_In_Rs_Transmitter;
    private DbsField re_In_Rs_Filler_6;

    private DbsGroup re_In__R_Field_6;
    private DbsField re_In_Pnd_I500;
    private DbsField re_In_Pnd_I12;

    private DbsGroup pnd_Dump;
    private DbsField pnd_Dump_Pnd_D500;
    private DbsField pnd_Dump_Pnd_D12;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 10);

        pnd_Input_Parm__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Parm__R_Field_1", "REDEFINE", pnd_Input_Parm);
        pnd_Input_Parm_Pnd_Pymnt_Date_To = pnd_Input_Parm__R_Field_1.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To", "#PYMNT-DATE-TO", FieldType.STRING, 
            8);

        pnd_Input_Parm__R_Field_2 = pnd_Input_Parm__R_Field_1.newGroupInGroup("pnd_Input_Parm__R_Field_2", "REDEFINE", pnd_Input_Parm_Pnd_Pymnt_Date_To);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy = pnd_Input_Parm__R_Field_2.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy", "#PYMNT-DATE-TO-YYYY", 
            FieldType.NUMERIC, 4);

        pnd_Input_Parm__R_Field_3 = pnd_Input_Parm__R_Field_2.newGroupInGroup("pnd_Input_Parm__R_Field_3", "REDEFINE", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Cc = pnd_Input_Parm__R_Field_3.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Cc", "#PYMNT-DATE-TO-CC", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Yy = pnd_Input_Parm__R_Field_3.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Yy", "#PYMNT-DATE-TO-YY", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm = pnd_Input_Parm__R_Field_2.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm", "#PYMNT-DATE-TO-MM", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd = pnd_Input_Parm__R_Field_2.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd", "#PYMNT-DATE-TO-DD", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Filler2 = pnd_Input_Parm__R_Field_1.newFieldInGroup("pnd_Input_Parm_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Input_Parm_Pnd_Year_End_Adj_Ind = pnd_Input_Parm__R_Field_1.newFieldInGroup("pnd_Input_Parm_Pnd_Year_End_Adj_Ind", "#YEAR-END-ADJ-IND", FieldType.STRING, 
            1);
        pnd_Columns = localVariables.newFieldInRecord("pnd_Columns", "#COLUMNS", FieldType.STRING, 100);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 8);
        i = localVariables.newFieldInRecord("i", "I", FieldType.PACKED_DECIMAL, 4);
        j = localVariables.newFieldInRecord("j", "J", FieldType.PACKED_DECIMAL, 4);
        k = localVariables.newFieldInRecord("k", "K", FieldType.PACKED_DECIMAL, 4);
        l = localVariables.newFieldInRecord("l", "L", FieldType.PACKED_DECIMAL, 4);
        re_In = localVariables.newFieldInRecord("re_In", "RE-IN", FieldType.STRING, 512);

        re_In__R_Field_4 = localVariables.newGroupInRecord("re_In__R_Field_4", "REDEFINE", re_In);
        re_In_E_Filler_1 = re_In__R_Field_4.newFieldInGroup("re_In_E_Filler_1", "E-FILLER-1", FieldType.STRING, 12);
        re_In_E_Ret_Qtr = re_In__R_Field_4.newFieldInGroup("re_In_E_Ret_Qtr", "E-RET-QTR", FieldType.STRING, 1);
        re_In_E_Tax_Year = re_In__R_Field_4.newFieldInGroup("re_In_E_Tax_Year", "E-TAX-YEAR", FieldType.STRING, 4);
        re_In_E_State_Ein = re_In__R_Field_4.newFieldInGroup("re_In_E_State_Ein", "E-STATE-EIN", FieldType.STRING, 8);
        re_In_E_Buss_Name = re_In__R_Field_4.newFieldInGroup("re_In_E_Buss_Name", "E-BUSS-NAME", FieldType.STRING, 60);
        re_In_E_Add_Line_1 = re_In__R_Field_4.newFieldInGroup("re_In_E_Add_Line_1", "E-ADD-LINE-1", FieldType.STRING, 30);
        re_In_E_Add_Line_2 = re_In__R_Field_4.newFieldInGroup("re_In_E_Add_Line_2", "E-ADD-LINE-2", FieldType.STRING, 30);
        re_In_E_City = re_In__R_Field_4.newFieldInGroup("re_In_E_City", "E-CITY", FieldType.STRING, 28);
        re_In_E_Country = re_In__R_Field_4.newFieldInGroup("re_In_E_Country", "E-COUNTRY", FieldType.STRING, 3);
        re_In_E_Filler_2 = re_In__R_Field_4.newFieldInGroup("re_In_E_Filler_2", "E-FILLER-2", FieldType.STRING, 2);
        re_In_E_Zip_Code = re_In__R_Field_4.newFieldInGroup("re_In_E_Zip_Code", "E-ZIP-CODE", FieldType.STRING, 10);
        re_In_E_Filler_3 = re_In__R_Field_4.newFieldInGroup("re_In_E_Filler_3", "E-FILLER-3", FieldType.STRING, 2);
        re_In_E_Nbr_Employees = re_In__R_Field_4.newFieldInGroup("re_In_E_Nbr_Employees", "E-NBR-EMPLOYEES", FieldType.NUMERIC, 8);
        re_In_E_Wh_Tot_Wages = re_In__R_Field_4.newFieldInGroup("re_In_E_Wh_Tot_Wages", "E-WH-TOT-WAGES", FieldType.NUMERIC, 13, 2);
        re_In_E_Wh_Tot_Income_Tax = re_In__R_Field_4.newFieldInGroup("re_In_E_Wh_Tot_Income_Tax", "E-WH-TOT-INCOME-TAX", FieldType.NUMERIC, 13, 2);
        re_In_E_Wh_Taxable_Wages = re_In__R_Field_4.newFieldInGroup("re_In_E_Wh_Taxable_Wages", "E-WH-TAXABLE-WAGES", FieldType.NUMERIC, 13, 2);
        re_In_E_Month1_Employees = re_In__R_Field_4.newFieldInGroup("re_In_E_Month1_Employees", "E-MONTH1-EMPLOYEES", FieldType.NUMERIC, 7);
        re_In_E_Month2_Employees = re_In__R_Field_4.newFieldInGroup("re_In_E_Month2_Employees", "E-MONTH2-EMPLOYEES", FieldType.NUMERIC, 7);
        re_In_E_Month3_Employees = re_In__R_Field_4.newFieldInGroup("re_In_E_Month3_Employees", "E-MONTH3-EMPLOYEES", FieldType.NUMERIC, 7);
        re_In_E_Filler_4 = re_In__R_Field_4.newFieldInGroup("re_In_E_Filler_4", "E-FILLER-4", FieldType.STRING, 93);
        re_In_E_Transmitter = re_In__R_Field_4.newFieldInGroup("re_In_E_Transmitter", "E-TRANSMITTER", FieldType.STRING, 1);
        re_In_E_Filler_5 = re_In__R_Field_4.newFieldInGroup("re_In_E_Filler_5", "E-FILLER-5", FieldType.STRING, 160);

        re_In__R_Field_5 = localVariables.newGroupInRecord("re_In__R_Field_5", "REDEFINE", re_In);
        re_In_Rs_Soc_Sec_Nbr = re_In__R_Field_5.newFieldInGroup("re_In_Rs_Soc_Sec_Nbr", "RS-SOC-SEC-NBR", FieldType.STRING, 9);
        re_In_Rs_First_Name = re_In__R_Field_5.newFieldInGroup("re_In_Rs_First_Name", "RS-FIRST-NAME", FieldType.STRING, 15);
        re_In_Rs_Middle_Name = re_In__R_Field_5.newFieldInGroup("re_In_Rs_Middle_Name", "RS-MIDDLE-NAME", FieldType.STRING, 15);
        re_In_Rs_Last_Name = re_In__R_Field_5.newFieldInGroup("re_In_Rs_Last_Name", "RS-LAST-NAME", FieldType.STRING, 23);
        re_In_Rs_Filler_1 = re_In__R_Field_5.newFieldInGroup("re_In_Rs_Filler_1", "RS-FILLER-1", FieldType.STRING, 123);
        re_In_Rs_Wage_Plan_Code = re_In__R_Field_5.newFieldInGroup("re_In_Rs_Wage_Plan_Code", "RS-WAGE-PLAN-CODE", FieldType.STRING, 1);
        re_In_Rs_Filler_2 = re_In__R_Field_5.newFieldInGroup("re_In_Rs_Filler_2", "RS-FILLER-2", FieldType.STRING, 7);
        re_In_Rs_Tot_Wages = re_In__R_Field_5.newFieldInGroup("re_In_Rs_Tot_Wages", "RS-TOT-WAGES", FieldType.NUMERIC, 11, 2);
        re_In_Rs_Filler_3 = re_In__R_Field_5.newFieldInGroup("re_In_Rs_Filler_3", "RS-FILLER-3", FieldType.STRING, 62);
        re_In_Rs_Taxable_Wages = re_In__R_Field_5.newFieldInGroup("re_In_Rs_Taxable_Wages", "RS-TAXABLE-WAGES", FieldType.NUMERIC, 11, 2);
        re_In_Rs_Filler_4 = re_In__R_Field_5.newFieldInGroup("re_In_Rs_Filler_4", "RS-FILLER-4", FieldType.STRING, 1);
        re_In_Rs_Tax_Withhold = re_In__R_Field_5.newFieldInGroup("re_In_Rs_Tax_Withhold", "RS-TAX-WITHHOLD", FieldType.NUMERIC, 10, 2);
        re_In_Rs_Filler_5 = re_In__R_Field_5.newFieldInGroup("re_In_Rs_Filler_5", "RS-FILLER-5", FieldType.STRING, 63);
        re_In_Rs_Transmitter = re_In__R_Field_5.newFieldInGroup("re_In_Rs_Transmitter", "RS-TRANSMITTER", FieldType.STRING, 1);
        re_In_Rs_Filler_6 = re_In__R_Field_5.newFieldInGroup("re_In_Rs_Filler_6", "RS-FILLER-6", FieldType.STRING, 160);

        re_In__R_Field_6 = localVariables.newGroupInRecord("re_In__R_Field_6", "REDEFINE", re_In);
        re_In_Pnd_I500 = re_In__R_Field_6.newFieldArrayInGroup("re_In_Pnd_I500", "#I500", FieldType.STRING, 100, new DbsArrayController(1, 5));
        re_In_Pnd_I12 = re_In__R_Field_6.newFieldInGroup("re_In_Pnd_I12", "#I12", FieldType.STRING, 12);

        pnd_Dump = localVariables.newGroupInRecord("pnd_Dump", "#DUMP");
        pnd_Dump_Pnd_D500 = pnd_Dump.newFieldArrayInGroup("pnd_Dump_Pnd_D500", "#D500", FieldType.STRING, 100, new DbsArrayController(1, 5));
        pnd_Dump_Pnd_D12 = pnd_Dump.newFieldInGroup("pnd_Dump_Pnd_D12", "#D12", FieldType.STRING, 12);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3096() throws Exception
    {
        super("Twrp3096");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp3096|Main");
        OnErrorManager.pushEvent("TWRP3096", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parm);                                                                                       //Natural: INPUT #INPUT-PARM
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                if (condition(pnd_Input_Parm_Pnd_Year_End_Adj_Ind.equals("Y")))                                                                                           //Natural: IF #YEAR-END-ADJ-IND = 'Y'
                {
                    getReports().print(0, "End-Of-Year 5th Qtr Adjustments Only -","No Tape Will Be Generated");                                                          //Natural: PRINT ( 00 ) 'End-Of-Year 5th Qtr Adjustments Only -' 'No Tape Will Be Generated'
                    getReports().print(1, "End-Of-Year 5th Qtr Adjustments Only -","No Tape Will Be Generated");                                                          //Natural: PRINT ( 01 ) 'End-Of-Year 5th Qtr Adjustments Only -' 'No Tape Will Be Generated'
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
                    sub_End_Of_Program_Processing();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate();  if (true) return;                                                                                                               //Natural: TERMINATE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Columns.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "----+----1----+----2----+----3----+----4----+----5", "----+----6----+----7----+----8----+----9----+----0")); //Natural: COMPRESS '----+----1----+----2----+----3----+----4----+----5' '----+----6----+----7----+----8----+----9----+----0' INTO #COLUMNS LEAVING NO SPACE
                //*  CTS
                RD1:                                                                                                                                                      //Natural: READ WORK FILE 01 RECORD RE-IN
                while (condition(getWorkFiles().read(1, re_In)))
                {
                    pnd_Read_Ctr.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #READ-CTR
                    //*   << CTS
                    //*                                                                                                                                                   //Natural: DECIDE ON FIRST VALUE OF E-TRANSMITTER
                    short decideConditionsMet136 = 0;                                                                                                                     //Natural: VALUES 'T'
                    if (condition((re_In_E_Transmitter.equals("T"))))
                    {
                        decideConditionsMet136++;
                        re_In_E_Filler_1.moveAll(".");                                                                                                                    //Natural: MOVE ALL '.' TO E-FILLER-1
                        re_In_E_Filler_2.moveAll(".");                                                                                                                    //Natural: MOVE ALL '.' TO E-FILLER-2
                        re_In_E_Filler_3.moveAll(".");                                                                                                                    //Natural: MOVE ALL '.' TO E-FILLER-3
                        re_In_E_Filler_4.moveAll(".");                                                                                                                    //Natural: MOVE ALL '.' TO E-FILLER-4
                        re_In_E_Filler_5.moveAll(".");                                                                                                                    //Natural: MOVE ALL '.' TO E-FILLER-5
                    }                                                                                                                                                     //Natural: VALUES 'D'
                    else if (condition((re_In_E_Transmitter.equals("D"))))
                    {
                        decideConditionsMet136++;
                        if (condition(re_In_Rs_Transmitter.equals("D")))                                                                                                  //Natural: IF RS-TRANSMITTER = 'D'
                        {
                            re_In_Rs_Filler_1.moveAll(".");                                                                                                               //Natural: MOVE ALL '.' TO RS-FILLER-1
                            re_In_Rs_Filler_2.moveAll(".");                                                                                                               //Natural: MOVE ALL '.' TO RS-FILLER-2
                            re_In_Rs_Filler_3.moveAll(".");                                                                                                               //Natural: MOVE ALL '.' TO RS-FILLER-3
                            re_In_Rs_Filler_4.moveAll(".");                                                                                                               //Natural: MOVE ALL '.' TO RS-FILLER-4
                            re_In_Rs_Filler_5.moveAll(".");                                                                                                               //Natural: MOVE ALL '.' TO RS-FILLER-5
                            //*   >> CTS
                            re_In_Rs_Filler_6.moveAll(".");                                                                                                               //Natural: MOVE ALL '.' TO RS-FILLER-6
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    pnd_Dump_Pnd_D500.getValue("*").setValue(re_In_Pnd_I500.getValue("*"));                                                                               //Natural: ASSIGN #D500 ( * ) := #I500 ( * )
                    pnd_Dump_Pnd_D12.setValue(re_In_Pnd_I12);                                                                                                             //Natural: ASSIGN #D12 := #I12
                    j.setValue(1);                                                                                                                                        //Natural: ASSIGN J := 1
                    k.setValue(100);                                                                                                                                      //Natural: ASSIGN K := 100
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(7),pnd_Columns);                                                         //Natural: WRITE ( 01 ) NOTITLE NOHDR 07T #COLUMNS
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    FOR01:                                                                                                                                                //Natural: FOR I = 1 TO 5
                    for (i.setValue(1); condition(i.lessOrEqual(5)); i.nadd(1))
                    {
                        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),j, new ReportEditMask ("ZZZ9"),new TabSetting(7),pnd_Dump_Pnd_D500.getValue(i),new  //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T J ( EM = ZZZ9 ) 07T #D500 ( I ) 109T K ( EM = ZZZ9 )
                            TabSetting(109),k, new ReportEditMask ("ZZZ9"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        j.nadd(100);                                                                                                                                      //Natural: ASSIGN J := J + 100
                        k.nadd(100);                                                                                                                                      //Natural: ASSIGN K := K + 100
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    j.setValue(501);                                                                                                                                      //Natural: ASSIGN J := 501
                    k.setValue(512);                                                                                                                                      //Natural: ASSIGN K := 512
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),j, new ReportEditMask ("ZZZ9"),new TabSetting(7),pnd_Dump_Pnd_D12,new //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T J ( EM = ZZZ9 ) 07T #D12 109T K ( EM = ZZZ9 )
                        TabSetting(109),k, new ReportEditMask ("ZZZ9"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-WORK
                RD1_Exit:
                if (Global.isEscape()) return;
                //* *------
                if (condition(pnd_Read_Ctr.equals(getZero())))                                                                                                            //Natural: IF #READ-CTR = 0
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(6),"California Tape Extract File","(Work File 01) Is Empty",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) '***' 06T 'California Tape Extract File' '(Work File 01) Is Empty' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                        TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new TabSetting(77),"***");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate(90);  if (true) return;                                                                                                             //Natural: TERMINATE 90
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
                sub_End_Of_Program_Processing();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *------------
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( 01 )
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //* *------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
                //* *------------
                //* *-------                                                                                                                                              //Natural: ON ERROR
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),"Number Of California Quarterly Recs Found.............",pnd_Read_Ctr,            //Natural: WRITE ( 00 ) NOTITLE NOHDR 01T 'Number Of California Quarterly Recs Found.............' #READ-CTR
            new ReportEditMask ("ZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),"Number Of California Quarterly Recs Found.............",pnd_Read_Ctr,            //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T 'Number Of California Quarterly Recs Found.............' #READ-CTR
            new ReportEditMask ("ZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(48),"California quarterly Tape File Dump",new //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 48T 'California quarterly Tape File Dump' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1 LINES
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
        sub_Error_Display_Start();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
        sub_Error_Display_End();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
    }
}
